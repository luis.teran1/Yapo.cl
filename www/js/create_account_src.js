/* globals Yapo:true, register_events, jsevents, adjustMenuHeight, val_conf_ai, regionArray */
/* exported checkMinLength, rutCheckAndFormater, addressCheck, rutInit, verifyPasswordsMatch, format_account_only_numbers, format_account_email_check, check_on_error  */
/* eslint-disable camelcase */
// @TODO remove function from global scope, eslint-disable and use strict mode

window.Yapo = window.Yapo || {};
var emailRegExp = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

$(document).ready(function() {

  var phoneInput; // eslint-disable-line no-unused-vars
  var $commune = $('#account_commune');

  if (document.getElementById('createAccount-phoneInput')) {
    phoneInput = new Yapo.PhoneInput();
  }

  // Catch IE9 error
  try {
    register_events(jsevents.accounts, document);
  } catch (e) {} // eslint-disable-line no-empty

  if (is_edition()) {
    showHideCompanyParams($('input[name=is_company]'));
  }

  $('#is_company_p').focus();

  $('.success').parent().addClass('field-success');

  $('#email_input').blur(function() {
    format_account_login_email_check($(this));
  });

  if ($('input#accept_conditions')) {
    $('input#accept_conditions').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      increaseArea: '20%' // optional
    });
  }

  if ($('input[name=gender]')) {
    $('input[name=gender]').iCheck({
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%' // optional
    });
  }

  if ($('input[name=is_company]')) {
    $('input[name=is_company]').iCheck({
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%' // optional
    });
  }

  $('#account_create_form').on('ifChanged', 'input[name=is_company]', function() {
    var element = $(this);

    showHideCompanyParams($('input[name=is_company]'));
    cleanEmptyError(element);

    if ($('.status-account').length > 0) {
      adjustMenuHeight();
    }
  });

  $('#account_create_form').on( 'ifChecked', '#accept_conditions', function() {
    $('#err_accept_conditions').hide();
  });

  $('#password_input').keyup( function() {
    $('#err_form').hide();
  });

  $('.icon-form-ok').hide();

  $('.buttonPassword.change-password').on('click', showHidePasswords);
  $('#account_region').on('change', changeRegion);
  $commune.on('change', function() {
    changeCommune(phoneInput);
  });

  if ($commune.val() && phoneInput) {
    phoneInput.setAreaCodeByCommune($commune.val());
  }
});


function is_edition() {
  return $('#account_email').attr('type') === 'hidden';
}


function showHideCompanyParams(e) {
  var displayValue = $(e[0]).is(':checked') ? 'none' : '';

  var nodes = $('div[name=only_for_company]');

  for (var i = 0; i < nodes.length; i++) {
    nodes[i].style.display = displayValue;
  }

  var inputs = nodes.find('input');
  var disabled = displayValue === 'none' ? true : false;

  for (i = 0; i < inputs.length; i++) {
    $('#' + inputs[i].id).prop('disabled', disabled);
  }

  if (is_edition()) {

    var rutLabel = $('#label_rut_cell');
    var rutInput = $('#account_rut');
    var addrLabel = $('#label_address_cell');
    var addrInput = $('#account_address');

    rutLabel.html(rutLabel.html().replace('*', ''));
    addrLabel.html(addrLabel.html().replace('*', ''));

    if (displayValue !== 'none') {
      rutLabel.html(rutLabel.html() + '*');
      addrLabel.html(addrLabel.html() + '*');
      $('#err_address').show()
    } else {
      $('#err_address').hide()
    }

    rutInput.attr('is_required', displayValue !== 'none');
    addrInput.attr('is_required', displayValue !== 'none');
  }
}


function cleanEmptyError(e) {
  if ($(e).val() !== '') {
    errorHide($(e).attr('name'));
  }
}


function checkMinLength(e, args) {
  var minHtml = $(e).attr('minlength');
  var allowEmpty = (1 == args.allowEmpty);
  var isEmpty = $(e).val().length == 0;

  if (!minHtml) {
    minHtml = args.minLength
  }

  if ($(e).val().length < minHtml && !(allowEmpty && isEmpty)) {
      errorShow($(e).attr('name'), args.errMsg, minHtml);
  } else {
     errorHide($(e).attr('name'));
  }
}


function rutCheckAndFormater(e, args) {
  var $input = $(e);
  var required = $input.attr('is_required') === 'true';

  if ($input.val() === '' && required) {
    errorShow($(e).attr('name'), args.errMsgEmpty);
  } else if ($input.val() === '') {
    errorHide($(e).attr('name'));

  } else if (!$.Rut.validar($input.val())) {
    errorShow($(e).attr('name'), args.errMsgInvalid);

  } else {
    $input.val($.Rut.formatear($input.val(),true));
    errorHide($(e).attr('name'));
  }
}


function addressCheck(e, args) {
  var $input = $(e);
  var required = $input.attr('is_required') === 'true';

  if ($input.val() === '' && required) {
    errorShow($(e).attr('name'), args.errMsgEmpty);

  } else if ($input.val() === '') {
    errorHide($(e).attr('name'));

  } else if ($input.val().length > args.maxLength) {
    errorShow($(e).attr('name'), args.errMsgMaxLength);

  } else {
    errorHide($(e).attr('name'));
  }
}

function checkArea(e, args) {
  var searchValue = $(e).val();
  if (searchValue.length) {

    var keysRegion = Object.keys(regionArray);

    for (var x = 0; x < keysRegion.length; x++) {

      var regionKey = keysRegion[x];
      var keysCommune = Object.keys(regionArray[regionKey].commune);

      for (var y = 0; y < keysCommune.length; y++) {

        var communeKey = keysCommune[y];
        var code =  regionArray[regionKey].commune[communeKey].code;

        if (code === searchValue) {
          errorHide($(e).attr('name'));
          changePhoneLength(searchValue);
          return;
        }
      }
    }

    errorShow($(e).attr('name'), args.errMsg);
  } else {
    checkMinLength(e, {
      minLength: 1,
      errMsg: 'AREA_CODE_MISSING'
    });
  }

  changePhoneLength(searchValue);
}

function rutInit(e) {
  var $input = $(e);

  if ($input.val() !== '') {
    $input.val($.Rut.formatear($input.val(),true));
  }
}


function verifyPasswordsMatch(e, args) {
  var  passwd = $('#account_password');
  var  passwdVerify = $('#account_password_verify');

  if (passwdVerify.val() !== passwd.val()) {
    errorShow('password_verify', args.errMsg);
  } else {
    errorHide('password_verify');
  }
}


function format_account_only_numbers(e, args) {
  // Remove no numeric characters
  var $input = $(e);

  if ($input.val() !== '') {
    var value = $input.val();
    var valueRep = value.replace(/\D/g, '');

    if (typeof args !== 'undefined' && valueRep.length > args) {
      valueRep = valueRep.substring(0, args);
    }

    if (valueRep !== value) {
      $input.val(valueRep);
    }
  }
}


function format_account_email_check(e) {
  var name  = $(e).attr('name');
  var $input = $(e);

  if ($input.val() === '') {
    errorShow(name, 'EMAIL_EMPTY_ERROR');

  } else if (!emailRegExp.test($input.val())) {
    errorShow(name,'EMAIL_FORMAT_ERROR');

  } else {
    errorHide(name);
  }
}


function format_account_login_email_check(e) {
  var $input = $(e);
  var id  = $input.attr('id');

  if ($input.val() === '') {
    errorShow(id, 'EMAIL_EMPTY_ERROR');

  } else if (!emailRegExp.test($input.val())) {
    errorShow(id,'EMAIL_FORMAT_ERROR');

  } else {
    errorHide(id);
  }
}


function check_on_error(e, args) {
  var name = $(e).attr('name');

  if (hasError(name)) {
    // @FIXME remove `eval`
    eval(args.validator + '(e, args)'); // eslint-disable-line no-eval
  }
}

function errorShow(input , message, replace) {
  var errorDiv = $('#err_' + input);
  var messageToShow = getErrorMessage(message);
  var toggleErrors = ['area_code', 'phone'];

  // customise error message
  if (messageToShow.indexOf('{0}') >= 0 && replace) {
    messageToShow = messageToShow.replace('{0}', replace);
  }

  errorDiv.html(messageToShow);
  // Toggle error messages phone
  if (toggleErrors.indexOf(input) >= 0) {
    var position = toggleErrors.length - toggleErrors.indexOf(input) - 1;
    $('#err_' + toggleErrors[position]).hide();
  }

  if (errorDiv.is(':hidden')) {
    errorDiv.removeClass( 'success' ).addClass( 'error' );
    errorDiv.parent().removeClass( 'field-success' );
    errorDiv.show();
    errorDiv.parent().find('div').hide();
    $('input[name="' + input + '"]').addClass('__is-error');
  }
}


function hasError(inputName) {
  var errorDiv = $('#err_' + inputName);

  if (errorDiv.is(':visible')) {
    return true;
  }
  return false;
}


function errorHide(input) {
  var errorDiv = $('#err_' + input);

  errorDiv.hide();
  errorDiv.html('');
  errorDiv.removeClass( 'error' ).addClass( 'success' );
  errorDiv.parent().addClass( 'field-success' );
  errorDiv.parent().find('div').show();
  $('input[name="' + input + '"]').removeClass('__is-error');
}


function getErrorMessage(args) {
  var errorMsgs = val_conf_ai.aiform.error_msgs; // eslint-disable-line camelcase

  for (var i = 0; i < errorMsgs.length; i++) {
    // @FIXME Check functionality to use === instead of ==
    if (errorMsgs[i].cod == args) { // eslint-disable-line eqeqeq
      return errorMsgs[i].format;
    }
  }

  return '';
}


function showHidePasswords(event) {
  event.preventDefault();

  var nodes = $('div[name=div-account_password]');
  var isPwdVisible = false;

  for (var i = 0; i < nodes.length; i++) {
    if (nodes[i].style.display === 'none') {
      nodes[i].style.display = 'block';
      isPwdVisible = true;
    } else {
      nodes[i].style.display = 'none';
    }
  }

  $('#account_password').prop('disabled',!isPwdVisible);
  $('#account_password_verify').prop('disabled',!isPwdVisible);

  if (isPwdVisible) {
    $('.caret').removeClass('right');
    $('.caret').addClass('down');

  } else {
    $('.caret').removeClass('down');
    $('.caret').addClass('right');
  }

  if ($('.status-account').length > 0) {
    adjustMenuHeight();
  }
}


function changeRegion() {
  // called when you select the a new region and loads the communes
  var $selectCommunes = $('#account_commune');
  var $selectRegion = $('#account_region');
  var selectedRegion = $selectRegion.val();

  $selectCommunes.find('option[value!=""]').remove();

  if (selectedRegion !== '') {
    var communesArray = regionArray[selectedRegion].commune;

    $selectCommunes.val('');
    $.each(communesArray, function(key, val) {
      $selectCommunes.append(
        '<option value=' + key + '>' + val.name + '</option>'
      );
    });
    $selectCommunes.removeAttr('disabled');
    $selectRegion.removeClass('__is-error');
    $selectCommunes.attr('data-region',selectedRegion);
  }
}

function changeCommune(phoneInput) {
  var region = $('#account_region').val();
  var commune = $('#account_commune').val();

  if (region && commune) {
    var $errorAreaCode = $('#err_area_code');

    phoneInput.setAreaCodeByCommune(commune);
    $errorAreaCode.hide();
    $errorAreaCode.html('');
  }
}


function changePhoneLength(code) {
  var $phone = $('#account_phone');

  if (code !== 2 && $('input[name=phone_type]:checked').val() === 'f') {
    $phone.attr('maxlength', '7');
    $phone.attr('minlength', '7');

  } else {
    $phone.attr('maxlength', '8');
    $phone.attr('minlength', '8');
  }

  if ($('input[name=phone_type]:checked').val() === 'f') {
    $phone.attr('placeholder','Ej. 2823785');
  } else {
    $phone.attr('placeholder','Ej. 63068746');
  }
}
