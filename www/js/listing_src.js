loadDataFinish = false;

window.addEventListener('message', function validateLogginInStatus (evt){
  if(evt.origin === window.location.origin && evt.data ==='legacy_hub::dataupdated') {
    loadDataFinish = true;
  }
})

$(document).ready(function() {
  $('img.lazy').lazyload();
  if (window.fetch) {
    try {
      var idInterval = setInterval(function() {
        if(loadDataFinish) {
          initializeNotifications("DESKTOP", function(){
            clearInterval(idInterval);
          })
        }
      }, 200);
    } catch (e) {
      console.error(e);
    }
  }

  //Fixing injected style via js
  const gastosComunesElement = document.querySelector('#condominio');
  if(gastosComunesElement){
    if(gastosComunesElement.style.display === 'inline'){
      gastosComunesElement.style.display = 'block';
    }
  }
  
  const galleryHelpElement = document.querySelector('.gallery-wrap__help');
  if(galleryHelpElement){
    galleryHelpElement.addEventListener('click', () => {
      $.pgwModal({
        mainClassName: 'pgwModal productsInfo-modal',
        target: '#productsInfo-container',
        maxWidth: 920
      });
    })
  }
});

$(document).bind('PgwModal::PushContent', function() {
  if ($('#pgwModal').hasClass('productsInfo-modal')) {
    var sliderPos = [];
    var sliders = $('#productsInfo .productsInfo-slider').children();
    var iconCloseElem = $('#pgwModal .pm-close .pm-icon');
    var prodSlider = $('#pgwModal .productsInfo-slider').bxSlider({
      mode: 'fade',
      controls: false,
      pager: false,
      slideWidth: 920,
      onSliderLoad: function () {
        iconCloseElem.addClass('__grey');
      },
      onSlideAfter: function (el) {
        var className = el.attr('class');

        if (className.indexOf('__index') >= 0) {
          iconCloseElem.addClass('__grey');
        } else {
          iconCloseElem.removeClass('__grey');
        }
      }
    });

    $('body').on('click', '.productsInfo-slider_control.__prev', function (e) {
      e.preventDefault();
      prodSlider.goToPrevSlide();
    });

    $('body').on('click', '.productsInfo-slider_control.__next', function (e) {
      e.preventDefault();
      prodSlider.goToNextSlide();
    });

    sliders.each(function (index) {
      var op = $(this).attr('class').replace('productsInfo-slider_item __', '');
      sliderPos[op] = index;
    });

    $('.productsInfo-slider_item.__index .index-options a').click(function (e) {
      e.preventDefault();
      var op = $(this).attr('href').replace('#', '');
      var pos = sliderPos[op];

      prodSlider.goToSlide(pos);
    });
  }
});