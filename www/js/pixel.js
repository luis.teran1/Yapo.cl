/* --------------------------------------
 * Pixel 1x1 statistics
 * -------------------------------------- */
function Pixel(url, where, cp, ap, cc, ac,qh,cookie,lang)
{
	var src;
        var d = new Date();
    if ( !where ) { 
        where = 'failure_' + window.location.host.replace( /\.|:/gi ,'_' ) + window.location.pathname.replace(/\/|\./g, '_');
    }
	src = url+'/1x1_pages_' + where
	if ( cp ) { src += '_c'+cp; } else { src += '_cpuk'; }
	if ( ap ) { src += '_a'+ap; } else { src += '_apuk'; }
	if ( cc ) { src += '_'+cc; }
	if ( ac ) { src += '_'+ac; }
	if ( qh ) { src += '_qh'+qh; }
        if ( lang ) { src += '_l' + lang; }

	src = src+'.gif?r='+d.getTime();
	
	/* Add cookie at the end of the URI, if it exists */
	if (cookie && getCookie(cookie)) {
		src += '&'+cookie+'=1';				
	}

	if (document.write_org != null)
		document.write_org('<img src="'+src+'" height="1" width="1" id="stats_1x1">');
	else
		document.write('<img src="'+src+'" height="1" width="1" id="stats_1x1">');
}

function getCookie(name) {
        var start = document.cookie.indexOf(name + "=");
        var len = start + name.length + 1;

        if (!start && name != document.cookie.substring(0, name.length))
                return null;

        if (start == -1)
                return null;

        var end = document.cookie.indexOf( ';', len );
        if (end == -1)
                end = document.cookie.length;

        return unescape(document.cookie.substring(len, end));
}

