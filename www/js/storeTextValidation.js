var Yapo = Yapo || {};

Yapo.storeTextValidation = (function() {

  function validateText(textEvt) {
    // Characters code, add characters who are not allowed in this case 188="<" 190=">"
    var el = textEvt.currentTarget;

    el.value = el.value.replace(/[<>]/g, '');
  }

  function attachHandler(elem) {
    elem.addEventListener('input', validateText, false);
  }

  return function init() {
    var infoText = document.getElementById('info_text_input');
    attachHandler(infoText);
  }
})();
