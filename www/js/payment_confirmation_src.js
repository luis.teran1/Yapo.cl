$(document).ready( function(){
	
	$("#print").darkTooltip({
		gravity:'north',
		content:'&nbsp;&nbsp;&nbsp;Imprimir&nbsp;&nbsp;&nbsp;',
		animation:'flipIn'
	});

	$("#print").click( function(e){
		e.preventDefault();
		window.print();
	})

});