/*global isIE, formatPrice, product_list, Chart, data_inmorent, data_inmosell, userId, Typeahead, xt_med */
$(document).ready(function() {
 
  /**
   * Load and setup Statistics Charts
   */
 
  (function setupCharts() {

    /**
     * Get all weekdays from yesterday (relative to passed day)
     * @params (int) day - Day from which to count back
     */
    function getDays(day) {
      // Days dictionary
      var days = ['DOM', 'LUN', 'MAR', 'MIE', 'JUE', 'VIE', 'SAB'];

      // Starting day int (plus one)
      var startingDay = (new Date(day).getUTCDay()) + 1;

      // Transform array to fit past seven days
      var beggining = days.slice(startingDay, days.length);
      var end = days.slice(0, startingDay);

      return beggining.concat(end);
    }

    /**
     * Replace nulls for 0
     * @param (Array) arr - Array to map
     **/

    function replaceNulls(arr) {
      return arr.map(function(a) {
        if (a === null) {
          return 0;
        } else {
          return a;
        }
      });
    }

    // Get charts 
    var chart1 = document.getElementById('chart1');
    var chart2 = document.getElementById('chart2');

    if (typeof data_inmosell === 'undefined' || typeof data_inmorent === 'undefined' 
        || !chart1 || !chart2) {
      return false;
    }

    Chart.defaults.global.pointHitDetectionRadius = 0;

    var labels = getDays(data_inmorent.date || data_inmosell.date);

    // Generate bases (which use same properties) for both charts
    var yapoBase = {
      label: 'Avisos yapo.cl',
      fillColor: 'rgba(220, 220, 220, 0)',
      strokeColor: '#fe8800',
      pointColor: '#fe8800',
      pointStrokeColor: '#fff',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(220, 220, 220, 1)'
    };

    var userBase = {
      label: 'Tus avisos',
      fillColor: 'rgba(220, 220, 220, 0)',
      pointStrokeColor: '#fff',
      pointHighlightFill: '#fff',
      pointHighlightStroke: 'rgba(151, 187, 205, 1)'
    };

    // Clone objects (extend) to generate datasets and assign data
    var yapoSell = jQuery.extend({}, yapoBase);
    yapoSell.data = replaceNulls(data_inmosell.average_data);

    var yapoRent = jQuery.extend({}, yapoBase);
    yapoRent.data = replaceNulls(data_inmorent.average_data);

    var userSell = jQuery.extend({}, userBase);
    userSell.pointColor = '#359c37';
    userSell.strokeColor = '#359c37';
    userSell.data = replaceNulls(data_inmosell.user_data);

    var userRent = jQuery.extend({}, userBase);
    userRent.pointColor = '#0080ff';
    userRent.strokeColor = '#0080ff';
    userRent.data = replaceNulls(data_inmorent.user_data);

    // Generate data for each chart
    var sellsChart = {
      labels: labels,
      datasets: [yapoSell, userSell]
    };

    var rentChart = {
      labels: labels,
      datasets: [yapoRent, userRent]
    };


    // Get chart element and context
    var context1 = chart1.getContext('2d');
    var context2 = chart2.getContext('2d');

    // Set general config
    var config = {
      showTooltips: false,
      responsive: true,
      bezierCurve: false,
      scaleShowVerticalLines: false
    };

    // Initialize charts
    window.myLine = new Chart(context1).Line(sellsChart, config);
    window.myLine = new Chart(context2).Line(rentChart, config);
  })();

  /** 
   * Set everything needed to get a contact list from an ad.
   * @method checkValidInputs - If validEmail and validAd are true, enable submit 
   *                            button.
   * @method emailValidation  - Validate if email is a valid email address.
   * @method sendForm         - Send form with ad id and email address. 
   * @method requestUserAds   - Request json with user ad list.
   */

  (function contactList() {
    // "Globals"
    var $send = $('#contactListSend');
    var $email = $('#contactListEmail');
    var $search = $('#contactListSearch');
    var $month = $('#contactListPerMonthMonth');
    var $sendPerMonth = $('#contactListPerMonthSend');
    var $emailPerMonth = $('#contactListPerMonthEmail');

    // Messages
    var $message = $('.getContactList-message');
    var $messageMail = $('.getContactList-messageMail');
    var $messageText = $('.getContactList-messageText');
    var $messageIcon = $('.getContactList-messageIcon');

    var $messagePerMonth = $('.getContactListPerMonth-message');
    var $messagePerMonthMail = $('.getContactListPerMonth-messageMail');
    var $messagePerMonthText = $('.getContactListPerMonth-messageText');
    var $messagePerMonthIcon = $('.getContactListPerMonth-messageIcon');

    var $emailError = $('.getContactList-errorMessage');
    var $emailPerMonthError = $('.getContactListPerMonth-errorMessage');

    // Store id of chosen ad
    var chosenAd;
    var validAd = false;
    var validEmail = false;
    var validMonth = false;
    var validEmailPerMonth = false;

    /**
     * Enable or disable submit button based on validEmail or validAd
     * @method checkValidInputs
     */

    function checkValidInputs() {
      if (validEmail && validAd) {
        $send.removeClass('is-disabled').attr('disabled', false);
      } else {
        $send.addClass('is-disabled').attr('disabled', true);
      }
      if (validEmailPerMonth && validMonth) {
        $sendPerMonth.removeClass('is-disabled').attr('disabled', false);
      } else {
        $sendPerMonth.addClass('is-disabled').attr('disabled', true);
      }
    }

    /** Validate if email follows specific rules.
     * @method emailValidation
     * @param {Object} e - HTMLElement
     */

    function emailValidation(e) {
      var chunks;
      var domain;
      var noDomain = true;
      var text = e.target.value;

      validEmail = false;

      if (text.length > 2) {
        /** 
         * Allowed index of @ is 2, which means, it allows at least 3 letters
         * as a valid email before the @ symbol.
         *
         * The dot indexOf must be higher than @, so it actually has a domain.
         **/
        if (text.indexOf('@') > 1) {
          chunks = text.split('@');

          // Chunck it up
          domain = chunks[1].split('.');

          for (var i = 0; i < domain.length; i++) {
            if (domain[i].length < 2) {
              noDomain = false;
            }
          }

          if (chunks.length === 2 && chunks[1].indexOf('.') > -1 && noDomain) {
            validEmail = true;
          }
        }
      }

      checkValidInputs();
    }

    /** Validate if email follows specific rules.
     * @method emailPerMonthValidation
     * @param {Object} e - HTMLElement
     */

    function emailPerMonthValidation(e) {
      var chunks;
      var domain;
      var noDomain = true;
      var text = e.target.value;

      validEmailPerMonth = false;

      if (text.length > 2) {
        /** 
         * Allowed index of @ is 2, which means, it allows at least 3 letters
         * as a valid email before the @ symbol.
         *
         * The dot indexOf must be higher than @, so it actually has a domain.
         **/
        if (text.indexOf('@') > 1) {
          chunks = text.split('@');

          // Chunck it up
          domain = chunks[1].split('.');

          for (var i = 0; i < domain.length; i++) {
            if (domain[i].length < 2) {
              noDomain = false;
            }
          }

          if (chunks.length === 2 && chunks[1].indexOf('.') > -1 && noDomain) {
            validEmailPerMonth = true;
          }
        }
      }

      checkValidInputs();
    }

    function validateMonth(){
      validMonth = $('#contactListPerMonthMonth option').val() != '';
      checkValidInputs();
    }

    /**
     * Send request with form info (ad and email).
     * @params {Object} e - HTMLElement
     */

    var type;
    function sendMonthlyForm(e) {
      e.preventDefault();

      if (!validEmailPerMonth && !validMonth) {
        // The user shouldn't be able to click the button if validAd or
        // validEmail are false.
        return false;
      }

      // Add url here and uncomment Ajax Request.
      var url = '/ad_reply_report.json';
      var email = $emailPerMonth.val();
      var month = parseInt($month.val());
      var date = new Date();
      date.setMonth(date.getMonth() + month);

      function success(data) {
        var icon;
        var text;

        if (!data) {
          // Some error happened.
        }

        $messagePerMonth.removeClass(type);

        if (data.status === 'TRANS_OK') {
          text = 'Un documento CSV fue enviado al correo ';
          icon = 'icon-check-circled';
          type = '__success';
        } else {
          email = '';
          type = '__error';
          icon = 'icon-close-circled';

          if (data.error_message) {
            text = data.error_message;
          } else {
            text = 'Ha ocurrido un problema, intente de nuevo m&aacute;s tarde.';
          }
        }

        $messagePerMonthText.html(text);
        $messagePerMonthMail.text(email);
        $messagePerMonthIcon[0].className = 'icon-yapo ' + icon;

        $messagePerMonth.addClass('is-visible ' + type);

        if (type === '__success') {
          $('#contactListPerMonthMonth')[0].selectize.setValue('');
          $emailPerMonth.val('');
        }
      }

      data = {
        destination_mail: email,
        date: date.toISOString().substring(0, 10)
      }

      $.ajax({
        url: url,
        dataType: 'json',
        data: data,
        success: success
      });
    }
    /**
     * Send request with form info (ad and email).
     * @params {Object} e - HTMLElement
     */

    var type;
    function sendForm(e) {
      e.preventDefault();

      if (!validAd && !validEmail) {
        // The user shouldn't be able to click the button if validAd or
        // validEmail are false.
        return false;
      }

      // Add url here and uncomment Ajax Request.
      var url = '/ad_reply_report.json';
      var email = $email.val();

      function success(data) {
        var icon;
        var text;

        if (!data) {
          // Some error happened.
        }

        $message.removeClass(type);

        if (data.status === 'TRANS_OK') {
          text = 'Un documento CSV fue enviado al correo ';
          icon = 'icon-check-circled';
          type = '__success';
        } else {
          email = '';
          type = '__error';
          icon = 'icon-close-circled';

          if (data.error_message) {
            text = data.error_message;
          } else {
            text = 'Ha ocurrido un problema, intente de nuevo m&aacute;s tarde.';
          }
        }

        $messageText.html(text);
        $messageMail.text(email);
        $messageIcon[0].className = 'icon-yapo ' + icon;

        $message.addClass('is-visible ' + type);

        if (type === '__success') {
          $search.val('');
          $email.val('');
          chosenAd = null;
          validAd = false;
          validEmail = false;
          checkValidInputs(); 
        }
      }

      data = {
         list_id: chosenAd,
         destination_mail: email
      }

      $.ajax({
        url: url,
        dataType: 'json',
        data: data,
        success: success
      });
    }

    /**
     * Request ads and init Typeahead with returned data
     */
    function requestUserAds() {
      // userId comes from template pack_dashboard
      if (typeof userId === 'undefined') {
        // Here goes Frontend Error Handlers
        return false;
      }

      $.ajax({
        url: '/user_ads.json',
        dataType: 'json',
        data: {
          user_id: userId,
          // A big enough amount to bring most of them
          ads_quantity: 200
        },
        error: function() {
          // Not sure what should be done here
        },
        success: function(data) {
          // Generate a new Typeahead instance with returned data
          var elem = $search[0];

          if (!elem) {
            return false;
          }

          new Typeahead({
            element: $search[0],
            data: data,
            getElement: function(obj) {
              // Add to be sent
              chosenAd = obj.id;

              $('#contactListPerMonthMonth')[0].selectize.setValue('');

              // Set ad as valid
              validAd = true;

              // Change focus to email
              $email.focus();

              // Check inputs
              checkValidInputs();
            },
            
            // If Typeahead loses focus and has at least one element, choose
            // that one.
            onBlur: function(obj) {
              chosenAd = obj.id;

              $('#contactListPerMonthMonth')[0].selectize.setValue('');
              
              validAd = true;
              
              checkValidInputs();
            }
          });
        }
      });
    }

    function showEmailError() {
      if (validEmail) {
        $emailError.removeClass('is-visible');
      } else {
        $emailError.addClass('is-visible');
        $message.removeClass('is-visible');
      }
    }

    function showEmailPerMonthError() {
      if (validEmailPerMonth) {
        $emailPerMonthError.removeClass('is-visible');
      } else {
        $emailPerMonthError.addClass('is-visible');
        $messagePerMonth.removeClass('is-visible');
      }
    }

    /**
     * Set listeners
     * I need some feedback on this, what would be the best, most oredered way
     * of assigning listeners to elements?
     **/

    $email.on('keyup', emailValidation);
    $email.on('blur', showEmailError);
    $send.on('click', sendForm);

    $month.on('change', validateMonth);
    $emailPerMonth.on('keyup', emailPerMonthValidation);
    $emailPerMonth.on('blur', showEmailPerMonthError);
    $sendPerMonth.on('click', sendMonthlyForm);

    // Init
    requestUserAds();
  })();

  // Minform
  var $form = $('#form_call');
  $form.minform();
  $form.on('next', function(e, c) {
    try {
      var xtclickName = $(this).data('xtclick-name');
      xtclickName = xtclickName.replace('help_pack_auto_step', 'help_pack_auto_step' + c);
      xt_med('C', '', xtclickName, 'N');
    }
    catch(e) {
      return false;
    }
  });

  $('#pack_slots, #pack_period, #contactListPerMonthMonth').selectize({
    allowEmptyOption: true,
    create: false
  });
  //Fix select text key

  // This breaks IE8 <- Not sure what this is.
  if(isIE() != 8) {
    $('.selectize-input input').prop('disabled', true);
    $('#pack_slots')[0].selectize.setValue('');
    $('#pack_period')[0].selectize.setValue('');
  }

  var SecureUrl = $('.btn-pack-info').attr('href');
  $('.btn-pack-info').removeAttr('href');

  $('.btn-pack-info.disabled').click(function(){
    $('.pack-call-buy p.error').fadeIn('fast');
  });

  $('#pack_period , #pack_slots').change(function(){

    var period = $('#pack_period option').val();
    var slots = $('#pack_slots option').val();
    var pack_type = $('#pack_type').val();

    if (period != '' && slots != '' && pack_type != '') {

      $('.pack-call-buy p.error').fadeOut('fast');

      var PackValue = product_list[pack_type.substring(1) + slots + '_' + period].price;

      $('.btn-pack-info').addClass('animated icon-cart icon-yapo');
      $('.btn-pack-info').removeClass('disabled');
      $('.price-pack').empty();

      var PackValueFinal = formatPrice(PackValue);

      $('.price-pack').text(PackValueFinal);

      var Product = period+slots;

      $('.btn-pack-info').data('url', SecureUrl + pack_type.substring(0,1) + Product);

      $('.btn-pack-info').click(function(e){
        e.preventDefault();
        $('.pack-call-buy p.error').hide();
        var urlDest = $('.btn-pack-info').data('url');
        location.href = urlDest;
      });
    }
  });

  //Validador
  if ($form.length) {
    $($form).validate({
      onkeyup: function(element) {
        // This function enables next button.
        function next() {
          $('.next').show();
          $('.next').addClass('ok');
          $('.icon-close').hide();
        }

        if ($(element).val().length == 1) {
          next();
        }

        if ($(element).hasClass('error')) {
          var valid = $(element).valid();
          if (valid) {
            next();
          }
        }
      },
      submitHandler: function(form) {
        var requestData = {};
        var formValues = $(form).serializeArray();
        for (var i = 0; i < formValues.length; i++) {
          requestData[formValues[i]['name']] = formValues[i]['value'];
        }

        $.ajax({
          url: $(form).attr('action'),
          method: 'PUT',
          data: JSON.stringify(requestData)
        }).done(function(data) {
          if (data['status'] == 'ok') {
            $('.call-form-sucess.success').show();
          } else {
            $('.call-form-sucess.fail').show();
          }
        }).fail(function() {
          $('.call-form-sucess.fail').show();
        });
      }
    });

    $('#call_name').rules('add', {
      required: true,
      messages: {
        required: 'Escribe un nombre.'
      }
    });

    //Avoid being stuck in IE8
    if (isIE() == 8) {
      $('#call_name').keyup(function() {
        $('#call_name').validate();
      });
    }

    //Method
    $.validator.addMethod('EMAIL', function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
    }, 'Escribe un correo v&aacute;lido.');

    $('#call_mail').rules('add', {
      required: true,
      email: true,
      minlength: 6,
      EMAIL: 'EMAIL',
      messages: {
        required: 'Escribe un correo v&aacute;lido.',
        email: 'Escribe un correo v&aacute;lido.',
        minlength: 'Escribe un correo v&aacute;lido.'
      }
    });

    $('#call_phone').rules('add', {
      required: true,
      number: true,
      minlength: 6,
      messages: {
        required: 'Escribe un n&uacute;mero v&aacute;lido.',
        number: 'S&oacute;lo n&uacute;meros',
        minlength: 'Escribe un n&uacute;mero v&aacute;lido.'
      }
    });
  }
});

//Validador
jQuery.validator.setDefaults({
  debug: true,
  success: 'valid'
});

