$(function(){
	$("#accordion").accordion({
		heightStyle:"content",
		collapsible:!0,
		active:((typeof accordion_active === "undefined")?false:accordion_active)
		});
	$(".edit").editable("",{
		indicator:"Guardando...",
		tooltip:"Click para modificar...",
		style:"display: inline"
		});
});
