/* global googletag, bannersData, normalizeWordAccent, account_data */

/**
  *  @name GoogleGptDefineTags
  *  @description
  *
  *  Load configuration for Google Gpt Library.
  *
  *  @param {object} opts for Google Gpt
  *
  */
(function () {
  var totalBannersLoad = 0;
  var totalBannerServices = 0;
  var totalBannersRender = [];

  this.GoogleGptDefineTags = function (opts) {
    var section = bannersData.googleSections[opts.sectionType][opts.deviceType];
    var baseIdString = opts.userId + '/' + opts.deviceType + '/' + section + '/';
    var baseIdStringCat = baseIdString;
    var baseNameBanner = opts.bannerIdPrefix + '_' + opts.deviceType + '_' + opts.sectionType + '_';
    var categoryString = getCategoryString(opts.category);
    var bannersEnabled = getBannersForShow(opts.sectionType, opts.deviceType, [baseNameBanner]);
    var queryStringValues = setQueryStrings(opts.queryStrings, opts.sectionType, opts.deviceType);
    var queryStringsToSend = getQueryStrings(opts.sectionType, getCategory(opts.category));
    var bannerToListen = dataBannerListen(opts.sectionType, bannersEnabled);
    var bannerDefined = 0;

    if (bannersEnabled.length > 0) {
      googletag.cmd.push(function () {
        queryStringsToSend.forEach(function (queryString) {
          googletag.pubads().setTargeting(queryString, queryStringValues[queryString]);
        });

        bannersEnabled.forEach(function (position) {
          var sizesBanner = getBannerSizes(position, opts.deviceType);

          googletag.defineSlot(baseIdString + position, sizesBanner, baseNameBanner + position)
            .addService(googletag.pubads())
            .setCollapseEmptyDiv(true, true);
          bannerDefined++;
        });

        googletag.pubads().enableSingleRequest();
        googletag.pubads().collapseEmptyDivs();
        googletag.enableServices();

        addListeners(baseNameBanner, bannersEnabled, bannerToListen, bannerDefined);

        googletag.display(baseNameBanner + bannersEnabled[0]);
      });
    }
  };

/**
  *  @name AddListenersEvents
  *  @description
  *
  *  Add listerners for google events
  *
  *  @param {string} baseIdBanner
  *  @param {array} bannersEnabled all banner enabled for show
  *  @param {array} bannersToListen all banner with conditions
  *  @param {number} totalBannerDefined all AdUnit banners defined on call
  *  @returns {void}
  */
  function addListeners (baseIdBanner, bannersEnabled, bannerToListen, totalBannerDefined) {
    googletag.pubads().addEventListener('slotRenderEnded', function(event) {
      var bannerLoad = event.slot.getSlotElementId();
      totalBannersLoad++;
      if (!event.isEmpty) {
        totalBannersRender.push(bannerLoad);

        if (bannerLoad.indexOf('service_') > 0) {
          loadServicesBanners(bannerLoad);
        }
      }

      if (totalBannersLoad === totalBannerDefined) {
        var bannersToWillShow = selectBannersForShow(totalBannersRender, bannersEnabled, baseIdBanner);

        showCommonBanners(bannerToListen, bannersToWillShow, baseIdBanner);
      }
    });
  }

/**
  *  @name selectBannersForShow
  *  @description
  *
  *  Logic for select wich banners show beetween old BaseIdBanner and new PrefixIdBanner
  *
  *  @param {array} bannersRedered all banners ids rendered
  *  @param {array} bannersEnabled all banners name enabled for show
  *  @param {string} baseId for banners
  *  @returns {array} bannersIdsForShow
  */
  function selectBannersForShow(bannersRendered, bannersEnabled, baseId) {
    var bannersIdsForShow = [];
    var existLBanners = false;
    var isBannerL = false;
        
    if (searchBannerName(bannersRendered, baseId + 'skycraperright_L') && searchBannerName(bannersRendered, baseId + 'topboard_L')) {
      isBannerL = true;
    }

    bannersEnabled.map(function (bannerName) {
      if (!searchBannerName(bannersRendered, baseId  + bannerName)) {
        return;
      }

      if (isBannerL && (bannerName.indexOf('topboard') >= 0 || bannerName.indexOf('skycraperright') >= 0)) {
        if (bannerName != 'topboard' && bannerName != 'skycraperright') {
          bannersIdsForShow.push(baseId + bannerName);
        }
 
        return;
      }

      bannersIdsForShow.push(baseId + bannerName);
    });

    return bannersIdsForShow;
  }
/**
  *  @name searchBannerName
  *  @description
  *
  *  Get if there are a banner name or part, in array of bannersIds
  *
  *  @param {array} bannersArray
  *  @param {string} nameString to found
  *  @returns {boolean}
  */
  function searchBannerName(bannersArray, nameString) {
    var nameFounded = false;
        
    bannersArray.map(function (bannerName) {
      if (bannerName == nameString) {
        nameFounded = true;
      }
    });

    return nameFounded;
  }
/**
  *  @name showCommonBanners
  *  @description
  *
  *  Logic for load common banners
  *
  *  @param {array} bannersRedered all banners ids rendered
  *  @param {array} bannersWithConditions banner that need some conditions for show
  *  @returns void
  */
  function showCommonBanners(bannersWithConditions, bannersIdsForShow) {
      var rightBannerBottom = false;
      
      bannersIdsForShow.map(function (bannerId) {
        var elBanner = document.getElementById(bannerId);
        var topBoardBanner = false;

        if (bannersWithConditions.length > 0 && bannerId.indexOf('_L') < 0) {
          if (bannerId.indexOf('topboard') >= 0) {
            showElement('banners-top-wrapper');
            topBoardBanner = true;
          }

          if (bannerId.indexOf('skycraperright') >= 0) {
            showElement('banners-right-wrapper');
          }

          if (bannerId.indexOf('skyscraperright_2') >= 0) {
            showElement('banners-right-wrapper-bottom');
            rightBannerBottom = true;
          }
        }

        if (elBanner && elBanner.parentElement) {
          elBanner.parentElement.style.display = (topBoardBanner) ? 'table' : 'block';
        }
      }); 
      
      if (tagParams.sectionType === 'li' && tagParams.deviceType === 'web_desktop') {
        listingRightBanners(rightBannerBottom);
      }
  }
/**
  *  @name loadServicesBanners
  *  @description
  *
  *  Logic for load services banners and autofact
  *
  *  @param {string} bannerLoad name
  *  @returns void
  */
  function loadServicesBanners(bannerLoad) {
    var boardServices = document.querySelector('.board-services');
    var statusAutofact = Number(boardServices.dataset.autofact);
    var statusTrasatuauto = Number(boardServices.dataset.transatuauto);
    var el = document.getElementById(bannerLoad);

    if ((statusAutofact + statusTrasatuauto + totalBannerServices) == 3) {
      return false;
    } 

    boardServices.classList.add('board-services--show');

    el.closest('.board-services__item').style.display = 'block';

    totalBannerServices++;
  }
/**
  *  @name listingRightBanners
  *  @description
  *
  *  Add functionality for 2 banners same column
  *
  *  @param {bolean} bottomBannerRight is load
  *  @returns void
  */
  function listingRightBanners(bottomBannerRight) {
    var wrapperListing = document.getElementById('container_main');
    var bannerRight = document.getElementById('banners-right-container');
    var bannerRightTop = document.getElementsByClassName('banners-right-column__top');

    if (bannerRight && wrapperListing) {
      bannerRight.style.top = '-' + wrapperListing.offsetTop + 'px';
    }

    if (!bottomBannerRight) {
      var i;

      for (i = 0; i < bannerRightTop.length; i++) {
        bannerRightTop[i].style.bottom = '0%';
      }
    }
  }
/**
  *  @name showElement
  *  @description
  *
  *  Display block element
  *
  *  @param {string} elId element
  *  @returns void
  */
  function showElement(elId) {
    document.getElementById(elId).style.display = 'block';
  }
/**
  *  @name dataBannerListen
  *  @description
  *
  *  return banners for listeners
  *
  *  @param {string} section page
  *  @param {array} bannersEnabled
  *  @returns {array} bannersEnabledToListen
  */
  function dataBannerListen(section, bannersEnabled) {
    var bannersEnabledToListen = [];
    var bannersToListen = ['topboard', 'skycraperright', 'service_1', 'service_2', 'service_3'];
   
    if (section !== 'template' && section !== 'adnotfound') {
      bannersToListen.map(function (banner) {
        if (bannersEnabled.indexOf(banner) >= 0) {
          bannersEnabledToListen.push(banner); 
        }
      });
    }

    return bannersEnabledToListen;
  }

/**
  *  @name getBannersForShow
  *  @description
  *
  *  Return banners enabled for section
  *
  *  @param {string} section page
  *  @param {string} device id
  *  @returns {array} banners enabled
  */
  function getBannersForShow(section, device, bannersId) {
    var allBannersSection = bannersData.googleStatus[section][device];
    var bannersForShow = [];

    Object.keys(allBannersSection).forEach(function (keyName) {
      bannersId.forEach(function (bannerId) {
        if (parseInt(allBannersSection[keyName].enabled, 10) === 1 && 
            document.getElementById(bannerId+keyName) != null &&
            !searchBannerName(bannersForShow, keyName)) {
          bannersForShow.push(keyName);
        }
      });
    });

    return bannersForShow;
  }

/**
  *  @name getCategoryString
  *  @description
  *
  *  Return string for indetify category and subcategory
  *
  *  @param {string} categoryId of category
  *  @returns {string} category string id
  */
  function getCategoryString(categoryId) {
    var category = getCategory(categoryId);
    var subCategory = getCategoryChild(categoryId);
    var stringCategory = '';

    if (typeof categoryId !== 'undefined' && category !== '') {
      if (subCategory !== '') {
        stringCategory = category + '/' + subCategory;
      } else {
        stringCategory = category;
      }
    }

    return stringCategory;
  }

/**
  *  @name getCategory
  *  @description
  *
  *  Return parent name of id category
  *
  *  @param {string} categoryId of category
  *  @returns {string} category name
  */
  function getCategory(categoryId) {
    var id = Math.floor(categoryId / 1000);
    var category = bannersData.categories[id];

    if (typeof category === 'undefined') {
      category = bannersData.categories[0];
    }

    return category.code;
  }

/**
  *  @name getCategoryChild
  *  @description
  *
  *  Return name of id category
  *
  *  @param {string} categoryId of category
  *  @returns {string} subcategory Name
  *
  */
  function getCategoryChild(categoryId) {
    var id = parseInt(categoryId, 10);
    var category = bannersData.childCategories[id];

    if (typeof category === 'undefined') {
      category = bannersData.childCategories[0];
    }

    return category.name;
  }

/**
  *  @name setComune
  *  @description
  *
  *  Return a comune for some names with special characters
  *
  *  @param {string} comune selected
  *  @returns {string} comune name
  *
  */
  function setComune(comune) {
    var comuneSet = comune;

    if (bannersData.setComunes) {
      Object.keys(bannersData.setComunes).forEach(function eachKey (key) {
        if (bannersData.setComunes[key].old === comune) {
          comuneSet = bannersData.setComunes[key].new;
        }
      });
    }

    return comuneSet;
  }

/**
  *  @name getRegion
  *  @description
  *
  *  Return region name of id region
  *
  *  @param {string} regionId selected
  *  @returns {string} region name
  *
  */
  function getRegion(regionId) {
    var id = parseInt(regionId, 10);
    var region = bannersData.regions[id];

    if (typeof region === 'undefined') {
      region = bannersData.regions[0];
    }

    return region.name;
  }

/**
  *  @name getBannerSizes
  *  @description
  *
  *  Return array of sizes
  *
  *  @param {string} nameBanner of banner
  *  @param {string} device load
  *  @returns {array} of sizes banner
  */
  function getBannerSizes(nameBanner, device) {
    var objectSizes = {};
    var bannerSizes = [];
    var nameId = nameBanner;

    if (nameId.includes('cotizador-')) {
      nameId = 'cotizador';
    }

    objectSizes = bannersData.positions[nameId].size[device];

    Object.keys(objectSizes).forEach(function (key) {
      if (objectSizes[key] === 'fluid') {
        bannerSizes.push('fluid');
      } else {
        var sizes = objectSizes[key].split('x');
        var setSizes = [parseInt(sizes[0], 10), parseInt(sizes[1], 10)];

        bannerSizes.push(setSizes);
      }
    });

    return bannerSizes;
  }

/**
  *  @name getQueryStrings
  *  @description
  *
  *  Return query default, per section and category
  *
  *  @param {string} section page
  *  @param {string} category id
  *  @returns {array} list of query strings to send
  */
  function getQueryStrings(section, category) {
    var listQueryStrings = [];
    var listopts = ['default', section, category];

    listopts.forEach(function (option) {
      var queryStrings = bannersData.googleQueryStrings[option];

      if (typeof queryStrings !== 'undefined' && Object.keys(queryStrings).length > 0) {
        Object.keys(queryStrings).forEach(function (key) {
          listQueryStrings.push(queryStrings[key]);
        });
      }
    });

    return listQueryStrings;
  }

/**
  *  @name setQueryStrings
  *  @description
  *
  *  Return values set
  *
  *  @param {object} queryStrings per banner
  *  @param {string} page section id
  *  @param {string} device id
  *  @returns {object} list of query strings updated
  */
  function setQueryStrings(queryStrings, page, device) {
    var newQS = deepCopyObj(queryStrings);

    newQS.page_type = bannersData.googlePageTypes[page];
    newQS.supply_type = device;
    newQS['chile_region'] = getRegion(newQS['chile_region']);
    newQS['chile_comuna'] = (newQS['chile_comuna'].length)?
                            setComune(normalizeWordAccent(newQS['chile_comuna'])):'';
    newQS.pricemax = newQS.pricemax.replace(/ /g, '');
    newQS.pricemin = newQS.pricemin.replace(/ /g, '');
    newQS.rangoprecio = getPriceRank(newQS.price, newQS.pricemin, newQS.pricemax, page);
    newQS.ad_group = Adomik.randomAdGroup();

    if (typeof account_data != 'undefined') {
      newQS.birth = parseInt(account_data.birth).toString();
      newQS.rangoedad = account_data.age_ranges;
      newQS.genero = account_data.gender.replace(/\b\w/g, function(l){ return l.toUpperCase() });
    }

    return newQS;
  }

/**
  *  @name deepCopyObj
  *  @description
  *
  *  Return obj copy
  *
  *  @param {object} obj to clone
  *  @returns {object} copy of obj
  */
  function deepCopyObj(obj) {
    if (obj === null || typeof obj !== 'object') return obj;
    if (obj instanceof Object) {
      var copy = {};
      for (var attr in obj) {
        if (obj.hasOwnProperty(attr)) copy[attr] = deepCopyObj(obj[attr]);
      }
      return copy;
    }

    throw new Error('Unable to copy obj this object.');
  }

/**
  *  @name getPriceRank
  *  @description
  *
  *  Return set query_string rangoprecio
  *
  *  @param {string} price of product
  *  @param {string} pricemin to search
  *  @param {string} pricemax to search
  *  @param {string} page type
  *  @returns {string} with all values in the rank prices
  */
  function getPriceRank(price, pricemin, pricemax, page) {
    var min = parseInt(pricemin, 10) || 0;
    var max = parseInt(pricemax, 10) || 0;
    var rankConfig = bannersData.rankPrices;
    var rankLength = Object.keys(rankConfig).length;
    var rankFirst = rankConfig[0];
    var rankLast = rankConfig[rankLength - 1];
    var rankValues = '';
    var prices = {
      productPrice: parseInt(price, 10) || 0,
      minPrice: min,
      maxPrice: max
    };

    /* Price max with text like mas de 1000000 */
    if (max == 0 && pricemax.length > 0) {
      max = rankLast.until;
    }

    if (min == 0 && max == 0 && page == 'li') {
      return rankValues;
    } else if (prices.productPrice == 0 && page != 'li') {
      return '0-' + rankFirst.interval;
    }

    if (min < parseInt(rankFirst.interval, 10) && max < parseInt(rankFirst.interval, 10) && prices.productPrice == 0) {
      rankValues = '0-' + rankFirst.interval;
    } else if (min >= (parseInt(rankLast.until, 10) - parseInt(rankLast.interval, 10))) {
      rankValues = (parseInt(rankLast.until, 10) - parseInt(rankLast.interval, 10)) + '-' + rankLast.until;
    } else {
      rankValues = fillRankValues(page, prices, rankConfig);
    }

    rankValues = rankValues.replace(/(,)$/, '');

    return rankValues;
  }

/**
  *  @name fillRankValues
  *  @description
  *
  *  Return values for prices
  *
  *  @param {string} page type
  *  @param {object} prices strings obj
  *  @param {object} rankConfig data rank values for fill
  *  @returns {string} with all values in the rank prices
  */
  function fillRankValues(page, prices, rankConfig) {
    var min = prices.minPrice;
    var max = prices.maxPrice;
    var rankLength = Object.keys(rankConfig).length;
    var values = '';
    var floor = 0;

    if (page != 'li' && prices.productPrice != 0) {
      if (prices.productPrice >= (rankConfig[rankLength - 1].until - rankConfig[rankLength - 1].interval)) {
        return (rankConfig[rankLength - 1].until - rankConfig[rankLength - 1].interval) + '-' + rankConfig[rankLength - 1].until;
      }

      for (var i = 0; i < rankLength; i++) {
        if (prices.productPrice < rankConfig[i].until) {
          min = Math.floor(prices.productPrice/rankConfig[i].interval) * rankConfig[i].interval;
          max = min + parseInt(rankConfig[i].interval);

          return min + '-' + max;
        }
      }
    }

    if (min < parseInt(rankConfig[0].interval, 10)) {
      min = 0;
    }

    if (max == 0) {
      max = rankConfig[rankLength - 1].until;
    }

    for (var i = 0; i < rankLength; i++) {
      var interval = parseInt(rankConfig[i].interval, 10);
      var until = parseInt(rankConfig[i].until, 10);
      var minSet = Math.floor(min / interval) * interval;
      var maxSet = Math.ceil(max / interval) * interval;

      if (values !== '') {
        minSet = parseInt(rankConfig[i - 1].until, 10);
      }

      if (minSet < until && maxSet >= floor && max > minSet) {
        var j = minSet;
        var k = minSet + interval;

        do {
          values += j + '-' + k + ',';
          j = k;
          k = j + interval;
        }
        while (j < maxSet && j < until);
      }

      floor = until;
    }

    return values;
  }
}());
