function YapoFormValidator() {
  YapoFormValidator.handlers = new Array();

  // run: add the event handler for all input with the attribute 'data-regexp'
  YapoFormValidator.run = function() {
    $('input[data-validation-regexp]').each(function() {
      if ($(this).attr('type') == 'radio' || $(this).attr('type') == 'checkbox')
        $(this).on('click blur', function(){ YapoFormValidator.validateMe($(this)); });
      else
        $(this).on('keyup blur', function(){ YapoFormValidator.validateMe($(this)); });
    });
    $('select[data-validation-regexp]').each(function() {
      $(this).on('change blur', function(){ YapoFormValidator.validateMe($(this)); });
    });
  };

  // detele the old status in the controls.
  YapoFormValidator.resetValidation = function() {
    $('input[data-validation-regexp]').each(function() {
      $(this).removeAttr('data-validation-oldmatch');
    });
    $('select[data-validation-regexp]').each(function() {
      $(this).removeAttr('data-validation-oldmatch');
    });
  };

  // validateMe: return a boolean depending if the value match the data-validation_regexp pattern
  YapoFormValidator.validateMe = function(node) {
    var pattern = node.attr('data-validation-regexp');
    var isMatch;
    var value = node.val();
    if (pattern == '[:IS_COMPANY:]'){
      isMatch = ($('is_company_c').is(':checked') || $('#is_company_p').is(':checked'));
    }
    else if (pattern == '[:RUT:]'){
      isMatch = this.validateRut(value);
    }
    else if (pattern == '[:RUT_OPTIONAL:]')
      if (value == '')
        isMatch = true;
      else
        isMatch = this.validateRut(value);
    else if (pattern == '[:PASSWORD_VERIFY:]')
      isMatch = $('#account_password').val() == $('#account_password_verify').val();
    else if (pattern == '[:ACCEPT_CONDITIONS:]')
      isMatch = $('#accept_conditions').is(':checked');
    else {
      var regexp = RegExp(pattern, 'g');
      isMatch = regexp.test(value);
    }
    var oldMatch;
    if (node.attr('data-validation-oldmatch') != undefined)
      oldMatch = (node.attr('data-validation-oldmatch') == 'true');
    if (oldMatch != isMatch) {
      node.attr('data-validation-oldmatch', isMatch);
      YapoFormValidator.onStateChange(node, isMatch);
    }
  };

  // initial validation
  YapoFormValidator.validateAll = function() {
    $('input[data-validation-regexp]').each(function() {
      YapoFormValidator.validateMe($(this));
    });
    $('select[data-validation-regexp]').each(function() {
      YapoFormValidator.validateMe($(this));
    });
  };

  // this is an event handler that triggers when the validation state change, and should trigger the external handlers
  YapoFormValidator.onStateChange = function (node, isMatch) {
    YapoFormValidator.executeListeners(node, isMatch);
  };

  // methods to manage the listeners
  YapoFormValidator.addListener = function (handler) {
    var category = 'default';
    if (YapoFormValidator.handlers[category] == undefined)
      YapoFormValidator.handlers[category] = new Array();
    YapoFormValidator.handlers[category].push(handler);
    return YapoFormValidator.handlers[category].length - 1;
  };

  YapoFormValidator.removeListener = function (index) {
    var category = 'default';
    YapoFormValidator.handlers[category][index] = null;
  };

  YapoFormValidator.executeListeners = function (node, isMatch) {
    var category = 'default';
    if (YapoFormValidator.handlers[category] != undefined)
      for (var i = 0; i < YapoFormValidator.handlers[category].length; i++)
        if (YapoFormValidator.handlers[category][i] != null)
          YapoFormValidator.handlers[category][i](node, isMatch);
  };

  YapoFormValidator.validateRut= function (value) {
    var isMatch = false;
    var regexp = RegExp('^[1-9][0-9\.]{5,10}\-.$', 'g');
    isMatch = regexp.test(value);
    if (isMatch)
      isMatch = $.Rut.validar(value);
    return isMatch;
  };
}

var yfv = new YapoFormValidator();
yfv = null;
delete yfv;

$(document).ready(YapoFormValidator.run);

