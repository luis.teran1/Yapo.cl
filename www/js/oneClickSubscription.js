var Yapo = Yapo || {};

Yapo.oneClickSubscription = (function() {
  'use strict';

  var HIDDEN_CLASS = 'is-hidden';
  var VISIBLE_CLASS = 'paymentMethodField-text';

  var url = '/oneclick/unsubscribe/';
  var element;
  var errorElement;
  var successElement;
  var paymentMethod;
  var paymentMethodUnsubscribe;
  var paymentMethodUpdate;
  var paymentMethodSubscribe;
  var cardInfo;

  function oneClick() {
    url = '/oneclick/unsubscribe/';
    element = document.getElementById('unsubscribe');
    errorElement = document.getElementById('notificationError');
    successElement = document.getElementById('notificationSuccess');
    paymentMethodUnsubscribe = document.getElementById('paymentMethod-unsubscribe');
    paymentMethodUpdate = document.getElementById('paymentMethod-update');
    paymentMethodSubscribe = document.getElementById('paymentMethod-subscribe');
    cardInfo = document.getElementById('cardInfo');

    if (!element || !errorElement || !successElement) {
      return false;
    }

    setEventListener();
  }

  function setEventListener() {
    element.addEventListener('click', function() {
      sendRequest();
    });
  }
  
  function scrollUp() {
    $('html, body').animate({ scrollTop: 0 }, 300);
  }

  function success() {
    successElement.className = '';
    errorElement.className = HIDDEN_CLASS;
    paymentMethodUnsubscribe.className = HIDDEN_CLASS;
    paymentMethodUpdate.className = HIDDEN_CLASS;
    paymentMethodSubscribe.className = VISIBLE_CLASS;
    cardInfo.className = HIDDEN_CLASS;
  }

  function error() {
    errorElement.className = '';
    successElement.className = HIDDEN_CLASS;
  }

  function callback(data) {
    if (data) {
      if (data.status === 'ok') {
        success();
      }

      if (data.status === 'error') {
        error();
      }

      scrollUp();
    }
  }

  function sendRequest() {
    $.ajax({
      url: url,
      method: 'post',
      success: function(data) {
        callback(data);
      }
    });
  }

  return oneClick;
})();

