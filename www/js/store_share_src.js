function shareOnClick(facebook_sel, twitter_sel) {
	$(facebook_sel).click(function(event){
		event.preventDefault();
		var url = $(this).attr('data-facebook');
		xt_click(this, 'C', '', 'share_shop_fb\:\:share_shop_fb\:\:share_shop_fb\:\:share_shop_fb', 'A')
		window.open('https://www.facebook.com/sharer.php?t=&u='+url, 'facebook_popup', 'width=640, height=480');
		return false;
	});

	$(twitter_sel).click(function(event){
		event.preventDefault();
		var url = $(this).attr('data-tweet');
		xt_click(this, 'C', '', 'share_shop_tw\:\:share_shop_tw\:\:share_shop_tw\:\:share_shop_tw', 'A')
		window.open('https://twitter.com/share?text='+url, 'twitter_popup', 'width=640, height=480');
		return false;
	});
}
