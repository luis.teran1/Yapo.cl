$(document).ready(() => {
  $('.aiRadioButton, .aiUpselling-box').on('click', () => {
    const openEvent = new Event('ai_banner::open');
    window.dispatchEvent(openEvent);
  });
});
