var select_brand;
var select_model;
var select_version;
var select_year;
var category_group;
var prefix_original = '';

function on_change_car_modify_title() {
  cars_get_selects();
  /* IE fix for placeholder */
  if ($('#subject').val() == $('#subject').attr('placeholder')) {
    $('#subject').val('');
  }

  var input_subject = $('#subject');
  var input_text = input_subject.val();
  var prefix_subject = '';
  var modifySubject = true;
  var with_prefix, user_sufix;
  var category_group_value = category_group.val();
  var subject = generate_subject_prefix();
  if (category_group_value == '2020') {
    prefix_subject = subject[0];
    modifySubject = subject[1];

    if (jQuery.trim(input_text.substring(0, prefix_original.length)) == jQuery.trim(prefix_original)) {
      with_prefix = true;
      user_sufix = input_text.substring(prefix_original.length, input_text.length);
    } else {
      with_prefix = false;
      user_sufix = input_text;
    }

    input_subject.val((with_prefix || input_text == '' ? prefix_subject : '') + user_sufix);
    input_subject.removeClass('placeholder');
    prefix_original = prefix_subject;
  }
}

function on_change_car_modify_title_mobile() {
  if (waitingForAdparamsDraft) return false;

  cars_get_selects();
  /* IE fix for placeholder */
  if ($('#subject').val() == $('#subject').attr('placeholder')) {
    $('#subject').val('');
  }

  var input_subject = $('#subject');
  var input_text = input_subject.val();
  var with_prefix, user_sufix;
  var category_group_value = category_group.val();

  if (category_group_value == '2020') {
    var subject = generate_subject_prefix();
    var prefix_subject = jQuery.trim(subject[0]);
    var last_generated_subject = storageGetItem('yapoDraft.' + location.host + '/mai/form/.lastGeneratedSubject');

    if (jQuery.trim(input_text).length == 0) {
      input_subject.val(prefix_subject);
    } else if (input_text.indexOf(last_generated_subject) == 0) {
      subject = input_subject.val();
      if (last_generated_subject) {
        subject = subject.replace(last_generated_subject, prefix_subject);
      }
      input_subject.val(subject);
    }
    storageSetItem('yapoDraft.' + location.host + '/mai/form/.lastGeneratedSubject', prefix_subject);

    input_subject.removeClass('placeholder');
    saveDraft('#subject');
  }
}

function generate_subject_prefix() {
  cars_get_selects();

  var prefix_subject = '';
  var modifySubject = true;
  var brand_value = select_brand.val();
  var model_value = select_model.val();
  var brand_label = select_brand.find('option:selected').text();
  var model_label = select_model.find('option:selected').text();
  var reg_date_value = select_year.val();

  if (brand_value != '' && brand_value != '0') {
    prefix_subject += brand_label + ' ';
  } else {
    modifySubject = false;
  }

  if (model_value != '' && model_value != '0') {
    prefix_subject += model_label + ' ';
  } else {
    modifySubject = false;
  }

  if (brand_value == '0') {
    modifySubject = false;
  } else if (reg_date_value == '1900') {
    prefix_subject += ' ';
  } else if (reg_date_value != '') {
    prefix_subject += reg_date_value + ' ';
  } else {
    modifySubject = false;
  }

  return [prefix_subject, modifySubject];
}

function cars_get_selects() {

  if (typeof(car_get_selects_mobile) == 'function') {
    car_get_selects_mobile();
    return;
  }

  select_brand = $('#brand');
  select_model = $('#model');
  select_version = $('#version');
  select_year = $('#regdate');
  select_fuel = $('#fuel');
  select_gearbox = $('#gearbox');
  select_cartype = $('#cartype');
  category_group = $('#category_group');
}

function focus_car_params() {
  var prefix = generate_subject_prefix();
  prefix_original = prefix[0];
}
