function SetupAdviewDataLayer(adData, userId, referer)  {
  const gtmData = {
    object_name: adData.title || '',
    object_ad_id: adData.id,
    object_ad_type_id: adData.type,
    object_categories: adData.categories,
    object_location_address_region: adData.regionName || '',
    object_price: adData.price || 0,
    object_url: adData.url || '',
    object_publisher_type: adData.publisherType || '',
    origin_url: referer,
  }
  
  if (userId !== -1) {
    gtmData.user_id = userId;
  }

  if (window.dataLayer) {
    dataLayer.push(gtmData);
  }
}