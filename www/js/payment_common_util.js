/* globals regionArray */

function paymentChangeRegion() {
  'use strict';

  var selectRegionValue = $('#id_region').val();
  var selectCommunes = $('#id_communes');
  var communesArray;

  if (selectRegionValue !== '') {
    communesArray = regionArray[selectRegionValue].commune;
  }

  // Remove all options from select leaving tail
  var options = selectCommunes.find('option'); // select_communes.prop?select_communes.prop('options'):select_comunes.attr('options');

  for (var i = options.length - 1; i >= 1; i--) {
    $('#id_' + selectCommunes[0].name + ' option[value="' + $(options[i]).val() + '"]').remove();
  }

  selectCommunes.val('');

  if (selectRegionValue !== '') {
    // Put the options with de communes from selected region
    $.each(communesArray, function eachCommune(key, val) {
      if (typeof val !== 'undefined') {
        selectCommunes.append('<option value=' + key + '>' + val.name + '</option>');
      }
    });
  }
}

function paymentSubmit(id) {
  'use strict';
  var form = document.getElementById(id);
  form.submit();
}

