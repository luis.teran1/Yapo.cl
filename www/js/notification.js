(function() {
  'use strict';
  // @TODO do as component with the new structure


  $(document).on('click', '.dashboardHeader-messageClose', close);

  function close() {
    var $message = $(this).closest('.dashboardHeader-messagesItem');
    var $messageWrap = $('.dashboardHeader-messages');
    var messagesCount = $('.dashboardHeader-messagesItem').length;
    var $elementToHide = messagesCount > 1 ? $message : $messageWrap;

    $elementToHide.slideUp(function() {
      $(this).remove();
    });
  }

})();
