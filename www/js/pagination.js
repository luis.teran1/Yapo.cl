$(document).ready ( function(){

	//Mouse events to navigate pagination

	$("#slide-prev").mouseover( function(){
		this.iid = setInterval( function(){ slidePrev(); } , 10);
	}).mouseout(function(){
		this.iid && clearInterval(this.iid);
	});

	$("#slide-next").mouseover( function(){
		this.iid = setInterval( function(){ slideNext(); } , 10);
	}).mouseout( function(){
		this.iid && clearInterval(this.iid);
	});

	$("#slide-prev, #slide-next").click( function(e){
		e.preventDefault();
	});

	adjustSlide();

});

function slidePrev(){
	slide($(".pages-flow"), -1, 5);
}

function slideNext(){
	slide($(".pages-flow"), 1, 5);
}

function slide(elem, xdir, speed){
	var currentScroll = elem.scrollLeft();
	elem.scrollLeft(currentScroll + (xdir*speed));
}


//Adjust the width of the page flow element, and the left positioning, depending on the width
function adjustSlide(){
	try{
		var currentOffset = $(".current").offset().left - $("#slide-prev").offset().left - 150 ;
		$(".pages-flow").scrollLeft(currentOffset);
		var pfW = $(".pages-flow").width();
		var fcW = $(".flow-content").width();
		if(pfW > fcW){
			$(".pages-flow").width(fcW);
		}
		if(fcW <= 250){
			$("#slide-prev").hide();
			$("#slide-next").hide();
		}

		//adjust left margin according to width
		var pagWidth = $('#pagination').width();
		$('#pagination').css('margin-left', -(pagWidth/2) + 'px');
		
	}catch(e){

	}
}
