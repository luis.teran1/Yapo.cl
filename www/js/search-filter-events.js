/**
  *  @name countFilter
  *  @description
  *
  *  Load total count of ads, searched with filters params.
  *
  *  @param {object} with options
  *  debug(bolean): Show log for debug - optional
  *  countUrl(string): endPoint for get Count Search - optional
  *
  */
function countFilter (opts) {
  let filterOpts = (opts) ? opts : {};
  let debug = (filterOpts.debug) ? true : false;
  let countUrl = (filterOpts.countUrl) ? filterOpts.countUrl : '/count-filter';
  // Local variable filterSearchCount -> main object with all params for search.
  // Local variable waiting -> flag for call fetch and retrieve results
  let filterSearchCount = {};
  let filterSearchCountCopy = {};
  let waiting = 0;

  this.init = function () {
    let searchInput = document.getElementById('keyword');
    let categorySelect = document.getElementById('selectbox_categories');
    
    $('#searchbox').on('click', 'input', function () {
      let name = this.getAttribute('name');
      let type = this.getAttribute('type');
      let val = this.value;

      if (name == 'f') {
        addFilterParamPrivate('f', typePersonCompany());
      } else if (type == 'checkbox') {
        let array = [];
        let checkboxes = document.querySelectorAll('input[name=' + name + ']:checked');

        for (let i = 0; i < checkboxes.length; i++) {
          array.push(checkboxes[i].value);
        }

        addFilterParamPrivate(name, array);
      } else {
        addFilterParamPrivate(name, val);
      }
    });

    searchInput.addEventListener('input', debounceTool(function() {
      addFilterParamPrivate('q', this.value);
    }, 300));
    
    if (categorySelect.value == 0) {
      defaultParamsSearchCountPrivate(getDefaultParams());
    }
  };

  this.clearFiltersParams = function () {
    filterSearchCount = {};
  }

  this.showFiltersParams = function () {
    console.log(filterSearchCount);
  }

  this.addFilterParam = function (name, value) {
    addFilterParamPrivate(name, value);
  }
  
  this.defaultParamsSearchCount = function (defaultParams) {
    defaultParamsSearchCountPrivate(defaultParams);
  };

  /**
    *  @name addFilterParamPrivate
    *  @description
    *
    *  Add filter param to object filterSearchCount
    *
    *  @param {string} name of param
    *  @param {string} value of param
    *  @returns {void}
  */

  const addFilterParamPrivate = function (name, value, retry) {
    let objTmp = {};
    if (!retry) {
      objTmp['' + name  + ''] = value;
      filterSearchCount = setFiltersParams(filterSearchCount, objTmp);
      showLogs('Filter Added', objTmp);
    }

    if (waiting == 0) {
      filterSearchCountCopy = Object.assign(filterSearchCount);
      waiting = 1;
      setTimeout(function() {
        if (shallowEqual(filterSearchCountCopy, filterSearchCount)) {
          getCountSearch();
        } else {
          waiting = 0
          addFilterParamPrivate('retry', '1', true);
        }
      }, 400);
    }
  }

  /**
    *  @name defaultParamsSearchCountPrivate
    *  @description
    *
    *  Set default params for search like Region and Commune
    *
    *  @param {object} defaultParams (cg, creg, f, cmn, w)
    *  @returns {void}
  */
  const defaultParamsSearchCountPrivate = function (defaultParams) {
    let keyword = document.getElementById('keyword').value;
    let defParams = Object.assign({}, defaultParams);
    
    if (defParams.cg == 1000 || defParams.cg == 2000) {
      defParams.st = 'a';
    }

    defParams.q = keyword;
    delete defParams.ctype;

    Object.keys(defParams).map(function (item) {
      if (item == 'creg') {
        addFilterParamPrivate('ca', defParams[item] + '_s');
      } else {
        addFilterParamPrivate(item, defParams[item]);
      }
    });
  }
  
  /**
    *  @name setFiltersParams
    *  @description
    *
    *  Merge new Filters with old Filters, and set if are multiple choice
    *
    *  @param {object} oldFilters
    *  @param {object} newFiltersParams
    *  @returns {object} oldFiltersObj filters merge new
  */
  const setFiltersParams = function (oldFilters, newFiltersParams) {
    let oldFiltersObj = oldFilters;
   
    if (typeof newFiltersParams['br'] !== 'undefined') {
      newFiltersParams['mo'] = '';
    } else if (typeof newFiltersParams['fwgend'] !== 'undefined') {
      newFiltersParams['fwsize'] = '';
    } 

    Object.keys(newFiltersParams).map(function (filter) {
      oldFiltersObj[filter] = newFiltersParams[filter];
    });

    return oldFiltersObj;
  }

  /**
    *  @name getCountSearch
    *  @description
    *
    *  Fetch for get count number, and then set into search button 
    *
    *  @returns {void}
  */
  const getCountSearch = function() {
    let urlFilters = getFilterUrl();
    let searchButton = document.getElementById('search_filter');
    waiting = 0;

    fetch(countUrl + urlFilters)
    .then(function(response) {
      return response.json();
    })
    .then(function(myJson) {
      searchButton.innerHTML = 'Buscar ' + formatPrice(myJson.search_total) + ' Avisos'; 
    });
  }

  /**
    *  @name getFilterUrl
    *  @description
    *
    *  Get url string with all filter params
    *
    *  @returns {string} filterUrl
  */
  const getFilterUrl = function() {
    let filterUrl='?';

    Object.keys(filterSearchCount).map(function (key, ind) {
      let value = filterSearchCount[key];

      if (value == '') {
        return;
      }

      if (Array.isArray(value) && value.length > 0) {
        value.map(function (val, index) {
          filterUrl += ''+ key + '=' + encodeURIComponent(val) +'';
          filterUrl += ((Object.keys(value).length - 1) > index) ? '&' : ''; 
        });
      } else {
        filterUrl += ''+ key + '=' + encodeURIComponent(value) +'&';
      }
    });

    filterUrl = filterUrl.replace(/\&$/, '');

    return filterUrl;
  }

  /**
    *  @name typePersonCompany
    *  @description
    *
    *  Get person or professional option for search (p: private, c: company: a: both)
    *
    *  @returns {string} oldFiltersObj filters merge new
  */
  const typePersonCompany = function() {
    let type_p = document.getElementById("type_person").checked;
    let type_c = document.getElementById("type_company").checked;
    let typeSearch = 'a';

    if (!type_p || !type_c) {
      typeSearch = (type_p) ? 'p' : 'c';
    }
  
    return typeSearch;
  }

  /**
    *  @name debounceTool
    *  @description
    *
    *  forces a function to wait a certain amount of time before running again.
    *
    *  @param {Function} func to call
    *  @param {number} wait number
    *  @param {boolean} inmediate if you dont need to wait
    *  @returns {function} executedFunction
  */
  const debounceTool = function(func, wait, immediate) {
    let timeout;

    return function executedFunction() {
      let context = this;
      let args = arguments;

      let later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };

      let callNow = immediate && !timeout;
  
      clearTimeout(timeout);

      timeout = setTimeout(later, wait);

      if (callNow) func.apply(context, args);
    };
  };

  /**
    *  @name getDefaultParams
    *  @description
    *
    *  Get default params from HTML Elements
    *
    *  @returns {object} objData with default params
  */
  const getDefaultParams = function() {
    let radio = $('input[name=st]:checked');
    let category = $.trim($('#selectbox_categories').val());
    let f = typePersonCompany();
    let w = $.trim($('#w').val());
    let ret = $('select[name=ret]:visible').val();
    let region = $.trim($('#region').attr('data-region'));
    let cmn = $('#cmn_select').val();
    let regex = new RegExp('[\\?&]st=([^&#]*)');
    let results = regex.exec(location.search);
  
    let objData = {
      cg: category,
      creg: region,
      w: w,
      f: f
    };

    let allRet;

    if (typeof (ret) === 'undefined') {
      allRet = $('select[name=ret]');

      for (var i = 0; i < allRet.length; i++) {
        if (allRet[i].value != '') {
          ret = allRet[i].value;
        }
      }
    }

    if (typeof (ret) !== 'undefined' && ret != '') {
      objData.ret = ret;
    }

    if (cmn) {
      objData.cmn = cmn;
    }

    return objData
  }

  /**
    *  @name showLogs
    *  @description
    *
    *  Show console logs if debug is true
    *
    *  @param {string} name of console section
    *  @param {object} value object or string
    *  @returns {void}
  */
  const showLogs = function (name, value) {
    let val = (Object.keys(value).length > 0) ? JSON.stringify(value) : value;

    if (debug) {
      console.log(name + ': ' + val);
    }
  }

  /**
    *  @name shallowEqual
    *  @description
    *
    *  Compare two object (not deep), and values with arrays
    *
    *  @param {obeject} object1
    *  @param {obeject} object2
    *  @returns {boolean} if object are shallow equal
  */
  const shallowEqual = function (object1, object2) {
    const keys1 = Object.keys(object1);
    const keys2 = Object.keys(object2);

    if (keys1.length !== keys2.length) {
      return false;
    }

    for (let key of keys1) {
      if (Array.isArray(object1[key])) {
        if (object1[key].length !== object2[key].length) {
          return false;
        }

        if (!object1[key].sort().every(function(value, index) { return value === object2[key].sort()[index]})) {
          return false;
        }
      } else if (object1[key] !== object2[key]) {
        return false;
      }
    }
    return true;
  }
}
