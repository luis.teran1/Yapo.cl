var creditsEnabled = false;

$(document).ready( function(){

	var deletionConfirmMsg = $("#remove-ad-confirmation").val(); //'&iquest;Seguro quieres borrar este aviso de tu carro?'
	var deletionSuccessMsg = $("#remove-ad-success").val(); //'El aviso fue borrado de tu carro'
	var emptyNameMsg = $("#empty-name").val(); //'Escribe tu nombre'
	var emptyLobMsg = $("#empty-lob").val(); //'Escribe tu giro'
	var emptyAddressMsg = $("#empty-address").val(); //'Escribe tu direcci�n'
	var emptyRutMsg = $('#empty-rut').val(); //'Escribe tu rut'
	var invalidRutMsg = $('#invalid-rut').val(); //'Escribe un rut v�lido'
	var yesMsg = $('#remove-yes').val(); //'Si'
	var noMsg = $('#remove-no').val();//'No'

	if($("#credits_enabled").length > 0){
		creditsEnabled = true;
	}

	$('input.radio-blue').iCheck({
		radioClass: 'iradio_minimal-blue',
		increaseArea: '20%' // optional
	});

	$(".btn-show-form").click(function(e){
		e.preventDefault();
		try{
			//close deactivate popup if open
			closeDeactivatePopup();
		}catch(e){}
		var formId = $(this).data("form-id");
		$("#store_prod").val(formId);
		$("#store_form").find(".btn-store-buy").data("form-id",formId);
		removeHighlight();
		$(this).parent().parent().addClass("highlight");
		if($(this).parent().parent().hasClass("recommended-plan")){
			$(".recommended-plan-layer").addClass("highlight");
		}
	});

	revealContent();

	$(".btn-store-buy").click( function(e){
		e.preventDefault();
		var formId = $(this).data("form-id");
		$("#store_form form").submit();
	});

});

function removeHighlight(){
	$(".store-plan").removeClass("highlight");
	$(".recommended-plan-layer").removeClass("highlight");
}

function revealContent(){
	//If Ie9, reveal without animation
	if(isIE() == 9){
		revealWithoutAnimation();
		return false;
	}

	var errors = $("#err_in_payment").val();
	var prod = $("#prod").val();

	if(errors != "" && prod != ""){
		//errors present
		//check if its a transaction error or a form error
		if($("#transaction_error").length == 0){ //Form error
			$("#btn-" + prod).trigger("click");
			setTimeout( function(){
				$(document).scrollTop(680);
				adjustTooltip();
			}, 300);
		}else{
			$(document).scrollTop(580);
		}
		revealWithoutAnimation();
	}else{
		revealWithAnimation();
	}
}

function revealWithAnimation(){
	$(".store-buy-plans").addClass("go-down");
	$(".store-preview").addClass("flipInX");
	$(".pointer-box").addClass("appear");
	$(".pointer-line").removeClass("shorten");

	setTimeout( function(){
		adjustMenuHeight();
	}, 1000);
}

function revealWithoutAnimation(){
	$(".store-preview").addClass("fome");
	$(".store-buy-plans").addClass("fome");
	$(".pointer-box").addClass("fome");
	$(".pointer-box").addClass("fome");
	$(".pointer-box").removeClass("box-left");
	$(".pointer-box").removeClass("box-right");
	$(".pointer-line").removeClass("shorten");
}
