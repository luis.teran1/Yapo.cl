function GetUserId() {
    const dataAd = document.querySelector('#dataAd');

    if (dataAd) {
        const userId = dataAd.getAttribute('account-id');
        if (userId) {
            return parseInt(userId, 10);
        }
    }
    return -1;
}

function GetAdData() {
    const dataAd = document.querySelector('#dataAd');

    if (dataAd) {
        return {
            id: dataAd.getAttribute('data-id'),
            category: dataAd.getAttribute('data-category'),
            type: dataAd.getAttribute('data-adtype'),
            transactionType: dataAd.getAttribute('data-type'),
            title: dataAd.getAttribute('data-title'),
            region: dataAd.getAttribute('data-region'),
            regionName: dataAd.getAttribute('data-region-name'),
            comunaCode: dataAd.getAttribute('data-comuna'),
            url: window.location.href,
            price: dataAd.getAttribute('data-price'),
        }
    }
}

function ParseDataForGTM(adData) {
    return {
        ...adData,
        categories: [
            {
                level: 0,
                id: parseInt(adData.category[0] + '000'),
            },
            {
                level: 1,
                id: parseInt(adData.category, 10),
            }
        ],
        type: GetTransactionTypeNameFromId(adData.transactionType),
        id: parseInt(adData.id, 10),
        price: parseInt(CleanPrice(adData.price), 10),
        publisherType: GetAccountTypeNameFromId(adData.type),
    }
}

function GetAccountTypeNameFromId(accountTypeId) {
    if (accountTypeId === '0') {
        return 'private';
    }
    return 'pro';
}

function CleanPrice(price) {
    const priceWithoutDots = price.replace(/\./g, '');
    
    if (price.indexOf(' ') > -1) {
        return (priceWithoutDots).split(' ')[1];
    }

    return priceWithoutDots;
}

function GetTransactionTypeNameFromId(adTypeId) {
    switch (adTypeId) {
        case 's':
            return 'sell';
        case 'k':
            return 'buy';
        case 'Arriendo':
            return 'rent';
        case 'u':
            return 'let';
        case 'h':
            return 'lease';
    }
}