/* globals mainFormBconf, sendTealiumTagByAjax, Yapo*/

window.Yapo = window.Yapo || {};

(function() {
  'use strict';

  var validator;
  var phoneInput;
  var companyValidationRules = {
    email: {
      email: 'email'
    },
    rut: {
      required: true,
      rut: 'rut'
    },
    'password_verify': {
      equalTo: '#account_password'
    }
  };
  var generalValidationRules = {
    areaCode: {
      areaCode: 'areaCode'
    }
  };

  $(document).on('pageshow', function(e) {
    $('#account_create_form').attr('data-where', e.target.id);
  });

  $(document).ready( function() {
    if (typeof mainFormBconf === 'undefined') {
      // Prevent unused features on "Mi perfil" page.
      return false;
    }

    // mdTextField config
    $('.create-account-form input').mdTextField();

    var $region = $('#account_region');
    var $commune = $('#account_commune');

    // horizontalSelect config
    if ($commune.length) {
      $region.horizontalSelect({
        onSelect: function onSelectRegion(event, select) {
          updateCommunes(select.val());
          $.horizontalSelect.changePage('account_commune');
        }
      });

      $commune.horizontalSelect();
    }else {
      $region.horizontalSelect();
    }

    $('.icheck-radio').iCheck({
      radioClass: 'iradio_minimal-blue',
      increaseArea: '20%' // optional
    });

    $('.icheck').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      increaseArea: '20%' // optional
    });

    // Detect change on type of user
    $('input[name=is_company]').on( 'ifChanged', function() {
      toggleCompanyFields();
    });

    $('#account_commune').on('change', handleAreaCode);

    $('form.edit').dirrty({
      onDirty: function() {
        $('#confirm_btn').removeAttr('disabled');
      },
      onClean: function() {
        $('#confirm_btn').attr('disabled', 'disabled');
      },
      preventLeaving: false
    });

    $.validator.addMethod('email', function validateEmail(value) {
      return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
    });

    $.validator.addMethod('rut', function validateRut(value) {
      return $.Rut.validar(value);
    });

    $.validator.addMethod('areaCode', function validateAreaCode(value) {
      return phoneInput.isValidAreaCode(parseInt(value, 10));
    });

    // Validate form
    validator = $('#account_create_form').validate({
      onkeyup: false,
      errorElement: 'span',
      ignore: ':not([name])',
      errorPlacement: putErrorMessage,
      invalidHandler: invalidHandler,
      success: successValidation,
      messages: {
        'is_company': {
          required: mainFormBconf.messages.ERROR_IS_COMPANY_TOO_SHORT
        },
        name: {
          maxlength: mainFormBconf.messages.ERROR_NAME_TOO_LONG,
          required: mainFormBconf.messages.ERROR_NAME_MISSING
        },
        rut: {
          required: mainFormBconf.messages.ERROR_RUT_MISSING,
          rut: mainFormBconf.messages.ERROR_RUT_INVALID
        },
        'area_code': {
          maxlength: mainFormBconf.messages.ERROR_AREA_CODE_TOO_LONG,
          minlength: mainFormBconf.messages.ERROR_AREA_CODE_TOO_SHORT,
          required: mainFormBconf.messages.ERROR_AREA_CODE_MISSING,
          areaCode: mainFormBconf.messages.ERROR_AREA_CODE_INVALID
        },
        phone: {
          maxlength: mainFormBconf.messages.ERROR_NEW_PHONE_TOO_LONG,
          minlength: mainFormBconf.messages.ERROR_NEW_PHONE_TOO_SHORT,
          required: mainFormBconf.messages.ERROR_PHONE_MISSING
        },
        region: {
          required: mainFormBconf.messages.ERROR_REGION_MISSING
        },
        email: {
          email: mainFormBconf.messages.ERROR_EMAIL_INVALID,
          maxlength: mainFormBconf.messages.ERROR_EMAIL_TOO_LONG,
          required: mainFormBconf.messages.ERROR_EMAIL_MISSING
        },
        password: {
          maxlength: mainFormBconf.messages.ERROR_ACCOUNT_PASSWORD_TOO_LONG,
          minlength: mainFormBconf.messages.ERROR_ACCOUNT_PASSWORD_TOO_SHORT,
          required: mainFormBconf.messages.ERROR_PASSWORD_MISSING
        },
        'password_verify': {
          maxlength: mainFormBconf.messages.ERROR_PASSWORD_TOO_LONG,
          required: mainFormBconf.messages.ERROR_PASSWORD_MISSING,
          equalTo: mainFormBconf.messages.ERROR_PASSWORD_MISMATCH
        },
        'accept_conditions': {
          required: mainFormBconf.messages.ERROR_MUST_AGREE_TERMS
        },
        address: {
          maxlength: mainFormBconf.messages.ERROR_ADDRESS_LONG
        },
        lob: {
          maxlength: mainFormBconf.messages.ERROR_LOB_TOO_LONG
        },
        contact: {
          maxlength: mainFormBconf.messages.ERROR_CONTACT_TOO_LONG
        }
      }
    });

    toggleCompanyFields();

    if (typeof Yapo === 'object') {
      if (typeof Yapo.oneClickSubscription !== 'undefined') {
        Yapo.oneClickSubscription();
      }
      if (typeof Yapo.informativeNotification !== 'undefined') {
        Yapo.informativeNotification();
      }
    }

    phoneInput = new Yapo.PhoneInput(onPhoneTypeChange, true);
    cleanPhoneInputStyle();

    if ($commune && $commune.val()) {
      updateAreaCodeByCommune($commune.val());
    }
  });

  function toggleCompanyFields() {
    var rules = {
      rut: {},
      'password_verify': {},
      email: {}
    };

    var label = $('#account_address').attr('data-address-label');

    if ($('#is_company_c').is(':checked')) {
      $('.company-fields').show();

      rules.email = companyValidationRules.email;
      rules.rut = companyValidationRules.rut;
      label = label + " *";

    } else {
      $('.company-fields').hide();
    }

    $('#account_address').prev().text(label)

    rules.password_verify = companyValidationRules.password_verify; // eslint-disable-line
    validator.settings.rules.email = rules.email;
    validator.settings.rules.password_verify = rules.password_verify; // eslint-disable-line camelcase
    validator.settings.rules.rut = rules.rut;
  }


  function updateCommunes(region) {
    var options = $.map(mainFormBconf.communes[region], function(name, value) {
      return '<option value="' + value + '">' + name + '</option>';
    });

    var $communes = $('#account_commune');

    $communes.empty();
    $communes.html(options.join('\n'));
    $communes.val(0).trigger('change');
    $communes.attr('data-region',region);
    $.horizontalSelect.updatePage('account_commune');
    $('#hst-account_commune').show();
  }


  function putErrorMessage(error, input) {
    var parent = input.parent();
    var next = input.next();
    var errorId = error.attr('id');
    var oldError = $('#' + errorId);
    var wrap;
    var phoneErrorsIds = ['area_code-error', 'phone-error'];
    var phoneError = $('.phoneInput-errorMessage');
    $("#err_"+$(input).attr('name')).hide();

    parent.addClass('wrap-error');
    parent.removeClass('wrap-valid');

    if (oldError.length && phoneErrorsIds.indexOf(errorId) === -1) {
      return oldError.html(error.html());
    }

    if (next.hasClass('hs-trigger')) {
      return next.append(error);
    }

    // areaCode and phone number error
    if (phoneErrorsIds.indexOf(errorId) !== -1) {
      if (phoneErrorsIds[0] === errorId || !phoneError.html().length) {
        phoneError.html(error.html());
      }

      if (phoneErrorsIds[1] === errorId && phoneError.innerHTML !== error.html()) {
        phoneError.html(error.html());
      }

      return phoneError.removeClass('is-hidden');
    }

    if (parent.hasClass('iradio_minimal-blue')) {
      wrap = parent.closest('.input-row');
      wrap.addClass('wrap-error');
      error.addClass('iradio-error');
      return wrap.append(error);
    }

    if (parent.hasClass('icheckbox_minimal-blue')) {
      wrap = parent.parent();
      wrap.addClass('wrap-error');
      error.addClass('iradio-error');
      return wrap.append(error);
    }

    input.after(error);
  }


  function handleAreaCode(evt) {
    var commune = evt.target;

    if (commune.value) {
      updateAreaCodeByCommune(commune.value);
    }
  }

  function updateAreaCodeByCommune(communeId) {
    if (!phoneInput) {
      return;
    }

    phoneInput.setAreaCodeByCommune(communeId);
  }


  function invalidHandler() {
    var element = validator.findLastActive();
    var topPosition;

    if (!element) {
      element = validator.errorList[0] && validator.errorList[0].element;
      if (!element) {
        return;
      }
    }

    element = $(element);

    if (element[0].tagName === 'SELECT') {
      element = element.next().find('.hs-trigger-link');
    }

    topPosition = element.offset().top - 20;

    element.focus();
    $('html, body').animate({
      scrollTop: topPosition
    }, 500);

    sendTealiumTagByAjax(true);
  }


  function successValidation(error, input) {
    var parent = $(input).parent();
    var phoneErrorsIds = ['area_code-error', 'phone-error'];
    var phoneError = $('.phoneInput-errorMessage');
    var errorId = error.attr('id');
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
    var areaIsValid;

    parent.removeClass('wrap-error');
    parent.addClass('wrap-valid');
    $("#err_"+$(input).attr('name')).hide();

    if (parent.hasClass('iradio_minimal-blue')) {
      parent.closest('.input-row').removeClass('wrap-error');
    }

    if (parent.hasClass('icheckbox_minimal-blue')) {
      parent.parent().removeClass('wrap-error');
    }

    areaIsValid = areaCode.validity && areaCode.validity.valid ||
      !areaCode.parentNode.classList.contains('wrap-error');

    if (phoneErrorsIds[0] === errorId) {
      validator.element('.phoneInput-number');
    } else if (phoneErrorsIds[1] === errorId && areaIsValid) {
      phoneError.html('');
      input.classList.remove('error');
      return phoneError.addClass('is-hidden');
    }

    input.classList.remove('error');
    error.remove();
  }


  function onPhoneTypeChange(evt, isMobileNumber) {
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];
    var phone = document.getElementsByClassName('phoneInput-number')[0];
    var classes = [].concat(Array.from(areaCode.classList),
      Array.from(phone.classList));
    var rules = {};

    if (!isMobileNumber) {
      rules = generalValidationRules.areaCode;
    }

    validator.settings.rules.area_code = rules; // eslint-disable-line camelcase

    if (classes.indexOf('valid') === -1 && classes.indexOf('error') === -1) {
      return;
    }

    validator.element('.phoneInput-areaCode');
    validator.element('.phoneInput-number');
  }

  /**
   *
   * @name cleanPhoneInputStyle
   * @description
   *
   * Remove style attr and is-hidden class who are added by mdTExtField's plugin.
   *
   */
  function cleanPhoneInputStyle() {
    // @TODO this code is duplicate in new_ai.jquery.js
    var areaCode = document.getElementsByClassName('phoneInput-areaCode')[0];

    areaCode.classList.remove('is-hidden');
  }
})();
