/* globals Yapo, CartHandler, normalizeStringFrom, normalizeStringTo*/

$(document).ready(function init() {
  setStickyMenu();
  adjustMenuHeight();
  showOpMsg();

  if (typeof Yapo === 'object') {
    if (typeof Yapo.oneClickSubscription === 'function') {
      Yapo.oneClickSubscription();
    }

    if (typeof Yapo.informativeNotification === 'function') {
      Yapo.informativeNotification();
    }

    if (typeof Yapo.storeTextValidation === 'function') {
      Yapo.storeTextValidation();
    }
  }

  $('.delete-unpaid').on('click', function (event) {
    Yapo.deleteModal(deleteModalData, isMsite, event);
  });
});

function setStickyMenu() {
  $(document).scroll(setupStickyNav);
  $(window).resize(setupStickyNav);

  if (typeof CartHandler !== 'undefined') {
    CartHandler.addListener(updateCartIconValues);
    CartHandler.executeListeners();
  }
}

function adjustMenuHeight() {
  // This offset's defined by the space between the menu and the top of the document
  var offset = 203;

  /**
   * Fixes a small padding problem on the bottom of the dashboard menu.
   * Why this happens? Not sure.
   * Will this be fixed? I don't think so.
   * */
  var magicNumber = 24;

  var documentHeight = $(document).height();
  var actualHeight = documentHeight - offset + magicNumber + 'px';

  $('.status-account').height(actualHeight);
  $('.list-das').height(actualHeight);
}

function setupStickyNav() {
  var distance;
  var scrolled = $(document).scrollTop();
  var menu = $('.menu-dashboard');
  var width = $(window).outerWidth();

  if (!menu[0]) {
    return false;
  }

  distance = menu.offset().top;

  if (scrolled > distance && width > 970) {
    $('.menu-account').addClass('ma-fixed');
    $('.header-list-ads-cont').addClass('ma-fixed');
  } else {
    $('.menu-account').removeClass('ma-fixed');
    $('.header-list-ads-cont').removeClass('ma-fixed');
  }

  return true;
}

function updateCartIconValues() {
  var quantity = CartHandler.getTotalQuantity();
  var $shoppingCart = $('.shopping-cart');
  var $numberItemsCart = $('#number-items-cart');

  if (quantity > 0) {
    $shoppingCart.removeClass('empty');
  } else {
    $shoppingCart.addClass('empty');
  }

  if (quantity > 9999) {
    $numberItemsCart.html('+9999');
  } else {
    $numberItemsCart.html(quantity);
  }
}

function showOpMsg() {
  var r;
  var d;
  var expires;
  var $opMsg = $('.dashboardHeader-opMsgItem');
  var $opMsgHash;
  var $opMsgLocation;
  var $opMsgExpireDate;
  var $opMsgAccID;

  if ($opMsg.length > 0) {
    $opMsgHash = $opMsg.attr('data-msg-hash');
    $opMsgLocation = $opMsg.attr('data-msg-location');
    $opMsgExpireDate = $opMsg.attr('data-msg-expire-date');
    $opMsgAccID = $opMsg.attr('data-acc-id');

    r = $opMsgExpireDate.match(/^\s*([0-9]+)\s*\/\s*([0-9]+)\s*\/\s*([0-9]+)(.*)$/);
    d = new Date(r[2] + '/' + r[1] + '/' + r[3] + ' ' + r[4]);
    expires = 'expires=' + d.toUTCString();

    if (getCookie('opMsgHash_' + $opMsgAccID + '_' + $opMsgLocation) !== $opMsgHash) {
      $opMsg[0].style.display = 'block';
      document.cookie = 'opMsgHash_' + $opMsgAccID +
        '_' + $opMsgLocation +
        '=' + $opMsgHash +
        '; ' + expires;
    }
  }
}

function getCookie(name) {
  var end;
  var start = document.cookie.indexOf(name + '=');
  var len = start + name.length + 1;

  if (!start && name !== document.cookie.substring(0, name.length)) {
    return null;
  }

  if (start === -1) {
    return null;
  }

  end = document.cookie.indexOf(';', len);

  if (end === -1) {
    end = document.cookie.length;
  }

  return unescape(document.cookie.substring(len, end));
}

function normalizeWordAccent(str) {
	var from = normalizeStringFrom;
	var to = normalizeStringTo;
	var mapping = {};
	var ret = [];
	
	for (var i = 0; i < from.length; i++) {
		mapping[from.charAt(i)] = to.charAt(i);
	}

	for (i = 0; i < str.length; i++) {
		var c = str.charAt(i);
		
		if (mapping.hasOwnProperty(str.charAt(i))) {
			ret.push(mapping[c]);
		} else {
			ret.push(c);
		}
	}
	
	return ret.join('');
}
