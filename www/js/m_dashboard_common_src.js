/*globals Yapo*/
$(document).ready( function(){
  CartHandler.addListener(updateCartValue);
  updateCartValue();

  $('#da-delete.__unpaid, #da-delete.__pack2if').on('click', function (event) {
    Yapo.deleteModal(deleteModalData, isMsite, event);
  });
});

function updateCartValue(){
  $("#summary-bar-amount").text(CartHandler.getTotalQuantity());
}
