(function() {
  document.addEventListener('DOMContentLoaded', scrollActionInitialize);

  /**
   *
   * @name scrollActionInitialize
   * @private
   * @description
   *
   * Set `scrollAction` as callback for `touchstar`, `touchmove` and `scroll`
   * events.
   *
   */
  function scrollActionInitialize() {
    document.addEventListener('touchstart', scrollAction, false);
    document.addEventListener('touchmove', scrollAction, false);
    document.addEventListener('scroll', scrollAction, false);
  }
})();


/**
 *
 * @name scrollAction
 * @public
 * @description
 *
 * Fix dashboard's navbar if scroll top position is bigger than 55 and
 * `.regions_list` haven't `show-region` CSS's className.
 *
 * @return {Number} - Scroll top position
 *
 */
function scrollAction() {
  'use strict';

  var FIXED_BAR = 'bar-is-fixed';
  var FIXED_OPTIONS = 'fixed-options';

  var regionList = document.getElementsByClassName('regions_list')[0];
  var optionsWrap = document.getElementsByClassName('dashboard-options-wrap')[0];
  var msiteDashboard = document.getElementsByClassName('msite-dashboard')[0];

  var scroll = window.pageYOffset;

  if (!optionsWrap || !msiteDashboard) {
    return;
  }

  if (scroll > 55) {
    if (!regionList || !regionList.classList.contains('show-region')) {
      optionsWrap.classList.add(FIXED_OPTIONS);
      msiteDashboard.classList.add(FIXED_BAR);
    }
  } else {
    optionsWrap.classList.remove(FIXED_OPTIONS);
    msiteDashboard.classList.remove(FIXED_BAR);
  }
}
