var msgs = ["Can't find it? Better add it to the font!", "Nope", "No results found", "This is not the icon you are looking for", "Nothing", "Never heard of that", "LOL, WUT?", "Try with another word", "What's that?"];

$(document).ready( function(){
	makeIcons();

	$("#find_icons").keyup( function(){
		var text = $(this).val();
		filterIcons(text);
	});

});


function makeIcons(){
	$(".yapo-icons-list li").each(function(){
		var className = $(this).find("i").attr("class");
		var iconElement = "<div class='preview'><i class='icon-yapo " + className + "'></i></div>";
		var classNameElement = "<div class='class-name'>" + className + "</div>";
		var characterElement = "<div class='character " + className + "'></div>";
		$(this).html(iconElement + classNameElement + characterElement);
	});
}


function filterIcons(text){
	$(".yapo-icons-list li").show();
	var results = 0;
	$(".yapo-icons-list li").each(function(){
		if($(this).html().indexOf(text) == -1){
			$(this).hide();
		}else{
			$(this).show();
			results++;
		}
	});
	if(results == 0){
		var i = Math.floor(Math.random()*msgs.length);
		$("#no_results").html(msgs[i]);
		$("#no_results").show();
	}else{
		$("#no_results").hide();
	}
}

