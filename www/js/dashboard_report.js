function downloadReport () {
  var reportContactsVisible = document.getElementById('dashboard-header-link');
  if(reportContactsVisible){
    taggingReportContacts('myads_download_contact_report_display');
  }
  var clickReport = document.getElementById('dashboard-header-link');
  if (clickReport) {
    clickReport.addEventListener('click', function() {
      var x = document.getElementById("dashboard-header-link"); 
      var url = x.getAttribute('report');
      taggingReportContacts('myads_download_report_interaction');
      loadData(url);
    });
  }

}

function loadData(url) {
  fetch('/pro-reporting-ms/ad-replies', {
    method: "GET",
    headers: {"Content-type": "application/application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}
  })
    .then(function (response) {
      if (response.status === 200) {
        return response.blob();
      }
      return null;
    })
    .then(function(data) {
      if (data != null) {
        var date = new Date();
        var actualDate = date.getFullYear() + "-"
                  + (date.getMonth()+1)  + "-" 
                  + date.getDate() + "-"
                  + date.getHours() + "_"  
                  + date.getMinutes() + "_" 
                  + date.getSeconds();
        var titleReport = 'Reporte de contactos ';
        var filename = titleReport + actualDate + ".xlsx";
        a = document.createElement('a');
        a.href = window.URL.createObjectURL(data);
        a.download = filename;
        a.style.display = 'none';
        document.body.appendChild(a);
        a.click();
      } else {
        var id = 'splashNoContacts';
        var title = noContactsDashboard.noContactsTitle;
        var theme = 'perfect-blue';
        var type = 'large'; 
        var contentModal = yapoModal.initialize ({
          id: id,
          conf: {
            attrs: {
              title: title,
              theme: theme,
              type: type,
              initalState: 'shown',
              overlayClose: 'true'
            },
          },
        });
        var contentSplash = document.querySelector('.no-contacts');
        var cloneSplash = contentSplash.cloneNode(true);
        contentModal.insertAdjacentElement('afterbegin', cloneSplash);
      }
    })
}

function taggingReportContacts(eventName) {
  var extraData = { event_name: eventName};
  var utagData = (typeof utag_data !== 'undefined') ? Object.assign(utag_data, extraData) : extraData;

  if (typeof window.utag !== 'undefined') {
    utag.link(utagData);
  }else{
    console.log(utagData);
  }
}
