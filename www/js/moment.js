/*** TADAAAAAAA ... I am not momentjs, I am just a file with the same name =P ***/
function getLocalizedDate(o) {
    var res = '';
    var date = new Date(o.split(".")[0].replace(/-/g,'/'));

    var d = date.getDate();
    var months = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'];
    var M = months[date.getMonth()];
    var H = date.getHours();
    var HH = H < 10 ? '0' + H : H;
    var i = date.getMinutes();
    var ii = i < 10 ? '0' + i : i;

    var format = "d M HH:ii";
    res = format
        .replace(/d/g, d)
        .replace(/M/g, M)
        .replace(/ii/g, ii)
        .replace(/HH/g, HH);
    return res;
}
