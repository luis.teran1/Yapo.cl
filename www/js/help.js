//LOAD FUNCTIONS AT STARTUP HELP PAGES
$(document).ready(function() {
  
    sidebarOpenSubMenu();
  
    if ($('#help_pg').hasClass('faq_pg')){
        showhideFAQAnswers();  
    }

	if (location.hash){
		showSpecificFAQAnswer(location.hash);
	}

});

//ON CLICK ON ITEM OPEN THE SUBMENU WHEN A ITEM HAS ONE.
function sidebarOpenSubMenu(){    
    $('.sidebar .item.has-submenu .link').click(function(){
        var $item = $(this).parent();
        var $submenu = $(this).next('.submenu');
        if ( $item.hasClass('active') ){
            $submenu.slideUp('fast');
               $item.removeClass('active');
        } else {
            $submenu.slideDown('fast');
            $item.addClass('active');
        }
    });
}

//ON CLICK TOGGLE THE FAQ ANSWERS
function showhideFAQAnswers(){
    $('.group_question .question_title').click(function(){
        var $item = $(this).next();
        if ($item.hasClass('active'))
        {
            $item.removeClass('active');
            $('.answer').slideUp('fast');
        } 
        else 
        {
            $('.answer').slideUp('fast').removeClass('active');
            $item.slideDown('fast').addClass('active');
        }    
        return false;
    });
}

//GO STRAIGHT TO SPECIFIC FAQ
function showSpecificFAQAnswer(idSelector) {
	var $elem = $(idSelector);
	if ( $elem != undefined ) {
		$('body').animate(
			{ 'scrollTop': $elem.offset().top },
			"slow",
			"swing",
			function(){$elem.trigger('click');}
		);
	}
}
