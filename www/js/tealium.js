function sendTealiumTag(eventName, isView) {
	try {
		var originalEventName = utag_data.event_name;
		var requestDone = function () { utag_data.event_name = originalEventName; }
		utag_data.event_name = eventName;

		if (isView) {
			utag.view(utag_data, requestDone);
		} else {
			utag.link(utag_data, requestDone);
		}
	} catch (e) { }
}

function defineSpecialVariables(result, var_name, value_name) {
	var value = '';
	if (value_name == 'TE_JOB_CATEGORY') {
		jobs_categories = [];
		$.each($(".category-label input:checked").parent().text().trim().replace(/\n/g, "|").split("|"),
			function (k, v) {
				if (v) {
					jobs_categories.push(v);
				}
			});

		value = jobs_categories.join("|");
	}
	if (value) {
		result[var_name] = value.trim();
		return true;
	}
	return false;
}

function parseUtagDataAjax() {
	try {
		var result = { 'event_name': utag_data_ajax['event_name'] };

		$.each(utag_data_ajax['variables'], function (k, v) {
			var $element = $(v), value = '';
			if (!defineSpecialVariables(result, k, v) && $element.length > 0) {
				switch ($element.prop('tagName')) {
					case 'INPUT':
						value = $element.val();
						if ($element.attr('name') == 'type') {
							value = $element.parent().text();
						}
						break;
					case 'TEXTAREA':
						value = $element.val();
						break;
					case 'SELECT':
						value = $(v + ' option:selected').val();
						break;
					case 'OPTION':
						value = ($element.val() != "") ? $element.text() : '';
						if ($element.attr('id') == 'rooms') {
							value = $element.val();
						}
						break;
					default:
						value = $element.text()
						break;
				}
				if (value) {
					result[k] = value.trim();
				}
			}
		});
		return result;
	} catch (e) { }
}

function sendTealiumTagAddUtagData(utagDataNew, isView, cb) {
	try {
		var originalUtagData = utag_data;
		var requestDone = function () {
			utag_data = originalUtagData;
			if (typeof cb === 'function') {
				cb();
			}
		};

		if (utagDataNew) {
			$.each(utagDataNew, function (k, v) {
				utag_data[k] = v;
			});
		}
		
		if (isView) {
			utag.view(utag_data, requestDone);
		} else {
			utag.link(utag_data, requestDone);
		}
	} catch (e) {
		if (typeof cb === 'function') {
			cb();
		}
	}
}

function sendTealiumTagByAjax(isView) {
	var utagDataAjax = parseUtagDataAjax();
	sendTealiumTagAddUtagData(utagDataAjax, isView);
}

function sendTealiumTagWithAttrData($obj, isView) {
	var vars = $obj.attr('data-tealium_var').split(',');
	var to_utag = {};
	$.each(vars, function (index, value) {
		to_utag[value] = $obj.attr('data-' + value);
	});
	sendTealiumTagAddUtagData(to_utag, isView);
}

jQuery(document).ready(function () {
	$('.tealium-click').click(function (e) {
		sendTealiumTag($(this).attr('data-tealium-click'), false);
	});

	$('.tealium-view').click(function (e) {
		sendTealiumTag($(this).attr('data-tealium-view'), true);
	});

	$('.tealium-click-var').click(function (e) {
		sendTealiumTagWithAttrData($(this), false);
	});

	$('.tealium-view-var').click(function (e) {
		sendTealiumTagWithAttrData($(this), true);
	});
});

document.dispatchEvent(new CustomEvent('regress::tealiumcommon', {}));
