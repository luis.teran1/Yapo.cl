var Yapo = Yapo || {};

/**
 * @name Yapo.gallery
 * @description
 *
 * This pseudo plugin allow to use arrow keys to navigate between
 * Adview's photos.
 *
 * @returns {Object}
 *
 */
Yapo.gallery = function(opts) {
  var Gallery = {
    currentItem: 1,
    options: {},

    /**
     * @name init
     * @description
     *
     * Entry point to use the plugin, this function will initialize the options, and set
     * the listeners to each thumb.
     *
     * @param {Array} options.images - List with the relative image paths
     * @param {Number} options.imagesPerPage - Maximum images quantity per page
     * @param {String} options.baseURL - URL base to prepend to each image
     * @param {String} options.thumbSelector - CSS thumb selector
     * @param {String} options.arrowLeftSelector - CSS left arrow selector
     * @param {String} options.arrowRightSelector - CSS right arrow selector
     * @param {String} options.viewerSelector - CSS gallery viewer selector
     * @param {Function} options.onReset - Callback to go back to first page
     * @param {Function} options.onLeft - Callback to go back a page
     * @param {Function} options.onRight - Callback to go forward a page
     * @param {Function} options.onSetImage - Callback executed after image selection
     *
     * @returns {Object}
     */
    init: function(options) {
      this.options = $.extend({}, {
        baseURL: '',
        images: [],
        imagesPerPage: 7,
        thumbSelector: '.gallery-thumb',
        viewerSelector: '.gallery-viewer',
        arrowLeftSelector: '#arrow_left',
        arrowRightSelector: '#arrow_right',
        onReset: function() {},
        onLeft: function() {},
        onRight: function() {},
        onSetImage: function() {},
      }, options);

      if (this.options.images.length > 1) {
        [].forEach.call(
          document.querySelectorAll(this.options.thumbSelector),
          this.addThumbListener.bind(this)
        );
        document.querySelector(this.options.viewerSelector)
          .addEventListener('click', this.moveToRight.bind(this));
        document.addEventListener('keyup', this.setKeyUpListener.bind(this));
      }

      return this;
    },

    /**
     * @name addThumbListener
     * @description
     *
     * Attach click listener, to change the selected image using the given thumbIndex
     *
     * @param {HTMLElement} thumb - Thumb box element
     *
     */
    addThumbListener: function(thumb) {
      thumb.addEventListener('click', function(evt) {
        var target = evt.currentTarget;
        this.setImage(target.dataset.thumbIndex);
      }.bind(this));
    },

    /**
     * @name setKeyUpListener
     * @description
     *
     * Update current image when the event was triggered by body element.
     *
     * @param {Event} evt - Body event
     *
     */
    setKeyUpListener: function(evt) {
      var RIGHT_ARROW = 39;
      var LEFT_ARROW = 37;
      var keyCode = evt.keyCode;
      var ignoredNodes = ['INPUT', 'TEXTAREA', 'SELECT'];

      if (ignoredNodes.indexOf(evt.target.nodeName) !== -1) {
        return;
      }

      if (keyCode === RIGHT_ARROW) {
        this.moveToRight();
      }

      if (keyCode === LEFT_ARROW) {
        this.moveToLeft();
      }
    },

    /**
     * @name setImage
     * @description
     *
     * Set an specific image by the given position
     *
     * @param {Number} imageIndex - Next image position
     *
     */
    setImage: function(imageIndex) {
      var images = this.options.images;
      var currentIndex = parseInt(imageIndex, 10);
      var index;

      if (currentIndex < 0) {
        index = images.length - 1;
      } else if (currentIndex > images.length - 1) {
        index = 0;
      } else {
        index = currentIndex;
      }

      this.currentItem = index + 1;
      this.options.onSetImage(index);
    },

    /**
     * @name moveToRight
     * @description
     *
     * Selects as current image the image that is immediately to the right
     *
     */
    moveToRight: function() {
      var images = this.options.images;
      var hasMoreItems = images.length > this.currentItem;
      var isPageLimit = this.currentItem % this.options.imagesPerPage === 0;

      if (hasMoreItems && isPageLimit) {
        this.currentItem += 1;
        this.options.onRight(images.length);
      } else if (this.currentItem === images.length) {
        this.currentItem = 1;
        this.options.onReset();
      } else {
        this.setImage(this.currentItem);
      }
    },

    /**
     * @name hasMorePages
     * @description
     *
     * Indicates if a gallery has multiple pages
     *
     * @returns {Boolean}
     */
    hasMorePages: function() {
      return Math.floor(this.options.images.length / this.options.imagesPerPage) > 0;
    },

    /**
     * @name moveToLeft
     * @description
     *
     * Selects as current image the image that is immediately to the left
     *
     */
    moveToLeft: function() {
      var images = this.options.images;
      var perPage = this.options.imagesPerPage;
      var isFirstPage = this.currentItem > 1 && (this.currentItem - 1) % perPage === 0;

      if (isFirstPage) {
        this.options.onLeft(images.length);
      } else if (this.currentItem === 1 && this.hasMorePages()) {
        this.goToLastPage();
      }

      this.setImage(this.currentItem - 2);
    },

    /**
     * @name goToLastPage
     * @description
     *
     * Moves the current selection to the last page
     *
     */
    goToLastPage: function() {
      var perPage = this.options.imagesPerPage;
      var images = this.options.images;
      var firstItem = Math.floor(images.length / perPage) * perPage;
      var imageElement;

      images.forEach(function(image, index) {
        imageElement = document.getElementById('thumb' + index);

        if (index < firstItem) {
          imageElement.classList.add('hidden');
        } else {
          imageElement.classList.remove('hidden');
        }
      });

      if (images.length > perPage) {
        document.querySelector(this.options.arrowRightSelector).style.display = 'none';
        document.querySelector(this.options.arrowLeftSelector).style.display = 'block';
      }
    }
  };

  return Gallery.init(opts);
};
