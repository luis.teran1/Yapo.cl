/*
 * Does the fucking ad reply
 * (c)2014 Joaco
 * Released under the WTFPL license
 */

/* Figuring out if the browser supports placeholders,
	instead of guessing by browser name, version
*/
jQuery.support.placeholder = (function(){
	var i = document.createElement('input');
	return 'placeholder' in i;
})();
$( document ).ready(function() {
	$('.icon-check-circled').hide();
});
(function(jQuery) {

 	jQuery.fn.adReply= function(options) {
 		return this.each(function(){
			options = jQuery.extend({}, jQuery.fn.adReply.defaults, options);

			jQuery(this).submit(function(event){
				event.preventDefault();
				$form = jQuery(event.target);
				$form.find('.error').hide();
				$form.find('#send').attr('disabled', 'disabled');

				if(!jQuery.support.placeholder){
					// serializing for browser that does not support placeholders
					var data = {};
					$('#da_reply_form').find('input[name], checkbox[name], textarea').each(function(index){
						var element = jQuery(this);
						if(element.val() != element.attr('placeholder')){
						    data[element.attr('name')] = element.val();
						}
					});
				} else {
					data = $form.serialize();
				}

				jQuery.ajax({
					'url': '/send_ar',
					'data': data,
					'dataType': 'json',
					'type': 'post'
				}).done(function(data, status, jqXHR){
						if (data.status == 'OK'){
							var email = $.trim($form.find('#user_email').val()).toLowerCase();
							$form.hide();
							$form.get(0).reset();
							$('.contact-info p').hide();
							$("#ad-reply-success").show();
							$form.trigger('adreply.done', {"email":email});
							document.dispatchEvent(new CustomEvent ('adreply::ok'));
							$("#tealium_mail_submit").click();
						} else {
							var xtcustom_var = '';
							jQuery.each(data, function(key, msg){
								var $err = jQuery('#ar_err_' + key);
								if ($err.size()) {
									$err.html(msg).show();
								}
							});
							$("#tealium_mail_submit_error").click();
							var xtcustom = jQuery('#adreply_error').val();
							var xtcustom_var = get_page_custom_variables();
							xt_med('F', '', xtcustom + '&' + xtcustom_var);
						}
					}).always(function(){
						$form.find('#send').removeAttr('disabled');
					});
			});

			jQuery(this).find("#adreply_body").focus(
				function(){
					jQuery(this).css('height', '110px');
				})
			.blur(
				function(){
					if(!jQuery(this).val()){
						jQuery(this).css('height', '33px');
					}
				})
			.trigger('blur');

			jQuery(this).find('input.icheck').iCheck({
				checkboxClass: 'icheckbox_minimal-blue',
				increaseArea: '20%' // optional
			});

			jQuery("#your_name").keyup( function(){
				if(jQuery(this).val().length){
					jQuery("#name_ok").show();
					jQuery("#ar_err_name").hide();
				}else{
					jQuery("#name_ok").hide();
					jQuery("#ar_err_name").show();
				}
			});

			jQuery("#user_email").blur(function(){
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				
				if(re.test(jQuery(this).val())){
					jQuery("#email_ok").show();
					jQuery("#ar_err_email").hide();
				}else{
					jQuery("#email_ok").hide();
				}
			});

			jQuery("#phone").keyup( function(){
				format_phone();
				
				if(jQuery(this).val().length >= 6){
					jQuery("#phone_ok").show();
					jQuery("#ar_err_phone").hide();
				}else if(jQuery(this).val().length > 0){
					jQuery("#phone_ok").hide();
					jQuery("#ar_err_phone").show();
				}else{
					jQuery("#phone_ok").hide();
					jQuery("#ar_err_phone").hide();
				}
			});

			function placeholderBlur(event){
				var element = $(this);
				var placeholder = element.attr("placeholder");
				if(element.val().length == 0){
					element.addClass("placeholder");
					element.val(placeholder);
				}
			}

			function format_phone(){
				var b=document.getElementById("phone").value;
				var a=b.replace(/\D/g,"");
				if(a.length>11){
					a=a.substring(0,11)
				}
				if(a!=b){
					document.getElementById("phone").value=a
				}
			}

			if(!jQuery.support.placeholder){
				jQuery("#your_name, #user_email, #phone, #adreply_body").each(placeholderBlur);
				jQuery("#your_name, #user_email, #phone, #adreply_body").blur(placeholderBlur);
				jQuery("#your_name, #user_email, #phone, #adreply_body").focus(function(event){
					var element = jQuery(this);
					var placeholder = element.attr("placeholder");
					if(element.val() == placeholder){
						element.removeClass("placeholder");
						element.val("");
					}
				});
			}
 		});
 	};
})(jQuery);
