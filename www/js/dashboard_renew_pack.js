function showSplash() {

  var time = time_renew_pack;
  var cookieName = cookie_name_renew_pack;

  if(shouldShowSplash(time, cookieName)) {
    var id= 'splashRenuevaPack';
    var title= typePack == 'car' ? packExpired.car: packExpired.realState;
    var theme= 'primary';
    var type= 'medium'; 

    var contentModal = yapoModal.initialize( {
      id: id,
      conf: {
        attrs: {
          title: title,
          theme: theme,
          type: type,
          initalState: 'shown',
          overlayClose: 'true'
        },
      },
    });

    var contentSplash = document.querySelector('.splash-renuew-pack');
    var cloneSplash = contentSplash.cloneNode(true);
    contentModal.insertAdjacentElement('afterbegin', cloneSplash);
    openedModal(contentModal);
    var btnRenewPack = cloneSplash.querySelector('#button-splash-renuew-pack');
    var url = btnRenewPack.getAttribute('redirection');
    showBtnRenewPack(btnRenewPack, url);
    closeModal(contentModal);
  }
}

function openedModal (contentModal) {
  contentModal.addEventListener('opened', () => {
    document.addEventListener('utagjs::loaded', function() {
      taggingWithButton('my_ads_splash_renew_pack_display');
    });
  });
}

function closeModal (contentModal) {
  contentModal.addEventListener('closed', (event) => {
    var close = event.detail.origin;
    if(close === 'overlay') {
      var withButton = 'no';
    } else {
      var withButton = 'yes';
    }
    taggingWithButton('my_ads_splash_renew_pack_close', withButton);
  });
}

function showBtnRenewPack(btnRenewPack, url) {
  btnRenewPack.addEventListener("click", function() {
    sendTealiumTagAddUtagData({
      event_name: 'my_ads_splash_renew_pack_button',
    }, false, function() {
      setTimeout(function() {
        location.href= url;
      }, 0);
    });

  });
}

function shouldShowSplash(minTime, cookieName) {
  var lastSplashShowed = getCookie(cookieName);
  var now = new Date();

  if(!lastSplashShowed) {
      document.cookie = cookieName + "=" + now.getTime();
      return true;
  } else {
      var elapsedTimeMs = now.getTime() - lastSplashShowed;

      if(elapsedTimeMs > minTime) {
          document.cookie = cookieName + "=" + now.getTime();
          return true;
      } else {
          return false;
      }
  }
}

function taggingWithButton(eventName, withButton) {
  var extraData = { event_name: eventName, data: {with_button: withButton}};
  var utagData = (typeof utag_data !== 'undefined') ? Object.assign(utag_data, extraData) : extraData;

  if (typeof window.utag !== 'undefined') {
    utag.link(utagData);
  }else{
    console.log(utagData);
  }
}
