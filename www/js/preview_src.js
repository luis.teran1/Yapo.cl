$(document).ready( function(){

	$("#cc").iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		increaseArea: '20%' // optional
	});

	$('#edit_ad').on('click', function(event){
		event.preventDefault();
		var $input = $('<input type="hidden" name="back" value="1"/>');
		var $form = $('form[name="formular"]');
		$form.append($input).submit();
	});

	$('.map-info').css('pointer-events', 'none');

});
