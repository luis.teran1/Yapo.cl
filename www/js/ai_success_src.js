'../bower_components/darktooltip/js/jquery.darktooltip.js'

$(document).ready( function(){
	//
	$("#upselling_help").click( function(e){
		e.preventDefault();
		$.pgwModal({
			mainClassName: 'pgwModal upselling-modal',
			target: '#upselling_modal',
			maxWidth: 940
		});
		$('.pm-close .pm-icon').addClass('white');
	});

	$("#upselling_label_help").click( function(e){
		e.preventDefault();
		$.pgwModal({
			mainClassName: 'pgwModal upselling-modal',
			target: '#upselling_modal_label',
			maxWidth: 475
		});
		$('.pm-close .pm-icon').addClass('white');
	});

	//Tooltips 
	$("#bump4-open-popup").darkTooltip({
	  animation: 'flipIn',
	  gravity: 'south',
	  opacity: 0.7
	});

	$("#label-open-popup").darkTooltip({
	  animation: 'flipIn',
	  gravity: 'south',
	  opacity: 0.7
	});
	
});