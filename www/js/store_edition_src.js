/* global closeFormTooltip, shareOnClick */

var storeChanged = false;
var dragImageMode = false;
var copyEvent;
var MAX_LENGTH_DESCRIPTION = 500;

$(document).on('ready', function() {
  var mainImageUpload = document.getElementById('upload_image');
  var logoUpload = document.getElementById('logo_upload');

  initUploadBtn(mainImageUpload, 'main', '#main_image_input', $('#upload_image'));
  initUploadBtn(logoUpload, 'logo', '#logo_image_input');

  $('#name_input').on('input', function() {
    verifyUrl();
    setButtonCopyUrl();
  });

  $('#info_text_input').on('keyup', function(evt) {
    var charCode = evt.keyCode || evt.which;
    var $textArea = $(evt.currentTarget);
    var $errorField = $textArea.closest('.input-row').find('.error');
    
    updateCharCount(charCode);

    if (isValidDescription()) {
      storeChanged = true;
      $errorField.hide();
    } else {
      $errorField
        .text(loginBconf.messages.ERROR_INFO_TEXT_TOO_SHORT)
        .show();
      $('#save_button').attr('disabled', 'disabled');
    }
    
    setActionButton();
    positiveValidation($textArea);
  });

  $('#address_input').on('keyup', function() {
    validateAddress();
  });

  $('#go_to_renew_store').on('click', function(e) {
    e.preventDefault();
    $(document).scrollTop(1000);
  });

  $('#hide_phone_input').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    increaseArea: '20%' // optional
  });

  $('.store-popup input[type=checkbox]').iCheck({
    checkboxClass: 'iradio_blue-check',
    increaseArea: '20%' // optional
  });

  $('.store-popup input[type=checkbox]').on('ifChecked', function() {
    // Uncheck others
    uncheckOthers($(this).attr('id'));
    showImgAcceptButton();
    // Set preview image
    $('#upload_image').prepend('<img src="' + $(this).data('img') + '" class="stock">');
    $('#upload_image img').not('.stock').hide();

    // Set values on accept button
    $('#add_image').data('img', $(this).attr('data-no'));
    $('#add_image').data('img-full', $(this).attr('data-img'));
  });

  $('#address_number_input').on('keyup', function() {
    this.value = this.value.replace(/\D/g, '');
    validateAddressNumber();
  });

  $('#hide_phone_input').on('ifChanged', function() {
    storeChanged = true;
    setActionButton();
  });

  $('.store-popup input[type=checkbox]').on('ifUnchecked', function() {
    $('#upload_image img.stock').remove();
    // Remove image data from accept button
    $('#add_image').data('img', '');
    $('#add_image').data('img-full', '');
  });

  $('input, textarea').on('blur', function() {
    setActionButton();
  });
  
  $('.store-main-form input, .store-edit-desc textarea')
    .on('keyup keypress keydown', function() {
      setActionButton();
      storeChanged = true;
      positiveValidation($(this));
    });

  $('#add_image').on('click', function() {
    storeChanged = true;
    if ($(this).data('img-full').indexOf('/img/stores-') !== -1) {
      removeMainImage();
    }

    setMainImage($(this).data('img'), $(this).data('img-full'));
    closeMainImagePopup();
    storeChanged = true;
    setActionButton();
    toggleMoveImageButton();
    toggleDashed();

      // Reset main image offset
    $('#main_image_upload_area_input').val(0);
  });

  $('select').on('change', function() {
    storeChanged = true;
    setActionButton();
  });

  $('#region_input').on('change', function() {
    validateRegion();
    loadCommunes();
    setActionButton();
  });

  $('#commune_input').on('change', function() {
    validateCommune();
  });

  $('#upload_image_wrap').on('click', function() {
    if (!$('#form_disabled').length) {
      openMainImagePopup();
    }
  });

  $('.darktooltip-modal-layer').on('click', function() {
    closeMainImagePopup();
    closeDeactivatePopup();
  });

  $('.store-popup .close-popup').on('click', function() {
    closeMainImagePopup();
  });

  $('#phone_input').on('keyup', function() {
    justNumbers($(this));
  });

  $(document).on('keyup', function(e) { // escape key
    if (e.keyCode === 27) {
      closeMainImagePopup();
    }
  });

  $(document).on('click', function() {
    $('.idle-store').hide();
  });

  $('#deactivate_store').on('click', function(e) {
    e.preventDefault();
    openDeactivatePopup();
  });

  $('#deactivate_no').on('click', function(e) {
    e.preventDefault();
    closeDeactivatePopup();
  });

  $('#drag_main_image').on('click', function() {
    dragImageMode = true;
    $('#upload_image_wrap').esRePosition({ input: '#main_image_upload_area_input' });
    $(this).hide();
    $('#drag_done').show();
    $('#dragging_image').show();
    $('#upload_image_wrap').unbind('click');

    storeChanged = true;
    setActionButton();

    $('#upload_image_wrap img').removeClass('opacity-on-hover');
    $('#upload_image_wrap img').addClass('moving');
  });

  $('#drag_done').on('click', function() {
    $(this).hide();
    $('#dragging_image').hide();
    $('#upload_image_wrap').esRepositionDisable();
    $('#drag_main_image').show();

    $('#upload_image_wrap img').addClass('opacity-on-hover');
    $('#upload_image_wrap img').removeClass('moving');
  });

  $('#upload_image_wrap').on('click', function() {
    openMainImagePopup();
  });

  $('#remove_main_image').on('click', function() {
      // Remove image data from accept button
    $('#add_image').data('img', '');
    $('#add_image').data('img-full', '');

    $('#upload_image').find('img').remove();
    toggleRemoveImageButton();
  });

    // close buy store tooltip on escape key press
  $(document).on('keydown', function(evt) {
    var code = evt.keyCode ? evt.keyCode : evt.which;

    if (code === 27) {
      try {
        closeFormTooltip();
      } catch (e) {}
    }
  });

  $('#website_url_input').on('input', function(evt) {
    var targetValue = $(evt.currentTarget).val();
    var $error = $('#err_store_website_url');

    if (!checkUrlRegex(targetValue) && targetValue.length) {
      $error
        .removeClass('validation_msg success')
        .addClass('error validation_error')
        .text(loginBconf.messages.ERROR_URL_INVALID)
        .show();
    } else {
      $error.hide();
    }
  });

  updateCharCount();

  checkForTransactionError();

  toggleRemoveImageButton();
  toggleMoveImageButton();
  toggleDashed();
  setActionButton();
  shareOnClick('#share_facebook', '#share_twitter');

  var $error = $('.error:visible').first();
  if ($error.length) {
    var $input = $error.parent().find('textarea,input,select');
    $(document).scrollTop($input.offset().top - 10);
  }

  $('#copy_url').click(function() {
    copyToClipboard('url_input');
  });
});

// Set main image on form input and store preview
function setMainImage(image, imageFull) {
  // Is it a default image?
  if (image == '1' || image == '2' || image == '3') {
    $('#default_bg').val(image);
    $('#main_image_input').val('');
  } else {
    $('#default_bg').val('');
    $('#main_image_input').val(image);
  }

  $image = $('<img src="' + imageFull + '" class="opacity-on-hover"/>');
  $upload_area = $('#upload_image_wrap');
  $upload_area.find('img').remove();
  $upload_area.prepend($image);
}

function loadCommunes() {
  var $region_input = $('#region_input');
  var $commune_input = $('#commune_input');
  var region_id = parseInt($region_input.val());

  $commune_input.attr('disabled', 'disabled');
  $commune_input.html('');
  $commune_input.append($('<option value="">Comuna</option>'));

  if (region_id > 0) {
    $.each(regionArray[region_id]['commune'], function(key, value) {
      var commune_id = key;
      var commune_name = value['name'];

      $commune_input.append($('<option value="' + commune_id + '">' + commune_name + '</option>'));
    });

    $commune_input.removeAttr('disabled');
  }
}

function verifyUrl() {
  var name = $('#name_input').val();
  var id = $('#store_id_input').val();
  var url = $(location).attr('pathname').replace('form', 'build_url');
  var $errStore = $('#err_store_name');
  
  $.ajax({
    async: true,
    timeout: 30000,
    type: 'GET',
    url: url,
    data: {
      name: name,
      id: id
    },
    success: function(data, textStatus, jqXHR) {
      var response = $.parseJSON(jqXHR.responseText);
      
      $('#url_input').val(response['url']);

      if (response['status'] == 'OK') {
        $errStore
          .hide()
          .removeClass('error')
          .addClass('success')
          .html('');
        $('#name_info').hide();
        $('#name_changed').show();
      } else {
        $errStore
          .html(response['status'])
          .removeClass('success')
          .addClass('error')
          .show();

        $('#name_changed').hide();
        $('#name_info').show();
      }

      setActionButton();
      positiveValidation($('#name_input'));
    },
    error: function(jqXHR, textStatus) {
      // console.log("ERROR!");
    },
    dataType: 'json'
  });
}

function displayError(image_type, error) {
  var $container = $('#' + image_type + '_errors');
  $container.find('.error').addClass('is-hidden');
  var $error = $('#err_' + image_type + '_' + error.toLowerCase());

  if ($error.length > 0) {
    $error.removeClass('is-hidden');
    $error.show();
  } else {
    $container.find('#err_' + image_type + '_unknown').removeClass('is-hidden');
  }
}

function initUploadBtn(upload_button, image_type, input, image_container) {
  if (image_container == undefined) {
    image_container = $(upload_button);
  }

  ai_images_button = new qq.FineUploaderBasic({
    button: upload_button,
    request: {
      inputName: 'image',
      endpoint: '/store_edit/upload_image/0?image_type=' + image_type,
      forceMultipart: true
    },

    multiple: false,
    callbacks: {
      onSubmit: function(id, file) {
        if (!(file && /^.*\.(jpg|png|jpeg|gif)$/i.test(file))) {
          displayError(image_type, 'ERROR_IMAGE_FORMAT');
          console.error('Solamente im�genes en formato JPG, PNG o GIF');
          return false;
        }
      },

      onComplete: function(id, file, response) {
        // enable save button
        cleanImageErrors();
        storeChanged = true;
        setActionButton();

        if (image_type == 'logo') {
          var storeImagesUrl = $('#store_logo_url').val();
        } else {
          var storeImagesUrl = $('#store_images_url').val();
          // uncheck all stock images

          uncheckAllStock();
          toggleMoveImageButton();
        }

        res = response;


        if (this.is_disabled()) {
          this.enable();
        }

        if (res.status && res.status == 'ok') {
          dir = res.file.slice(0, 2);
          image_url = storeImagesUrl + '/' + dir + '/' + res.file;
          $image = $('<img src="' + image_url + '"/>');
          image_container.find('img').remove();
          image_container.prepend($image);
          $(input).val(res.file.slice(0, -4));

          if (image_type == 'main') {
            showImgAcceptButton();
            toggleRemoveImageButton();
            toggleDashed();
            $('#add_image').data('img', removeImgExtension(res.file));
            $('#add_image').data('img-full', image_url);
          }
        } else {
          displayError(image_type, res.status);
        }
      }
    }
  });

  return ai_images_button;
}

function updateCharCount(charCode) {
  $textarea = $('#info_text_input');
  // fix line break problem
  var newLines = $textarea.val().match(/(\r\n|\n|\r)/g);
  var addition = 0;
  if (newLines != null) {
    addition = newLines.length;
  }

  var text = $textarea.val();
  var n = $textarea.val().length + addition;
  var m = MAX_LENGTH_DESCRIPTION - n;

  // Trying to end with line break and 1 character left
  var n = parseInt($('#remaining_chars').html());
  if (n == 1 && charCode == '13') {
    $textarea.val(text.substring(0, text.length - 1));
    m = 1;
  }

  // Prevent negative char count on FF
  if (m < 0 && (isFirefox() || isIE())) {
    $textarea.val(text.substring(0, MAX_LENGTH_DESCRIPTION - addition));
    m = 0;
  }

  $('#remaining_chars').html(m);
}

function openMainImagePopup() {
  $('#default_image_popup').show();
  $('.darktooltip-modal-layer').show();
}

function closeMainImagePopup() {
  $('#default_image_popup').hide();
  $('.darktooltip-modal-layer').hide();
  $('#main_errors p').hide();
}

function removeMainImage() {
  $('#upload_image').find('img').remove();
  $('#upload_image_wrap').find('img').remove();
}

function uncheckOthers(id) {
  $('.store-popup input[type=checkbox]').not('#' + id).iCheck('uncheck');
}

function uncheckAllStock(id) {
  $('.store-popup input[type=checkbox]').iCheck('uncheck');
}

function removeImgExtension(img) {
  return img.replace('.jpg', '').replace('.png', '');
}

function isValidDescription() {
  var $textArea = $('#info_text_input');
  var n = $textarea.val().replace('\n', '').length;
  
  return n >= 20;
}

function validateDescription() {
  var $textArea = $('#info_text_input');
  var $errorField = $textArea.closest('.input-row').find('.error');
  var n = $textarea.val().replace('\n', '').length;

  if (n >= 20) {
    $errorField.hide();
  } else {
    $errorField.show();
  }

  setActionButton();
}

function showImgAcceptButton() {
  $('#default_image_popup .popup-foot').css('display', 'inline-block');
  $('#main_errors p').hide();
}

function setActionButton() {
  // count errors
  var errorsList = $('.error:visible').toArray();
  var hasErrors = errorsList.some(function(el) {
    return $.trim($(el).text()).length;
  });
  var phoneContent = $('#phone_input').val();

  // Decide if need to show "save and publish" button or "go to store" button
  if ($('#save_action').length && $('#go_to_store_action').length) {
    if (storeChanged || hasErrors) {
      $('#save_action').show();
      $('#go_to_store_action').hide();
    } else {
      $('#go_to_store_action').show();
      $('#save_action').hide();
    }
  }

  // Decide if button needs to be disabled or not
  if (
    !hasErrors && 
    $('#info_text_input').val().length >= 20 &&
    $('#name_input').val().length >= 2 &&
    $('#region_input').val().length !== 0 &&
    (phoneContent.length === 0 || (phoneContent.length !== 0 && phoneContent.length >= 7))
  ) {
    $('#save_button').removeAttr('disabled');
  } else {
    $('#save_button').attr('disabled', 'disabled');
  }
}

// some validations for store edition form
function validateCommune() {
  if ($('#commune_input').val().length) {
    $('#err_store_commune').hide();
  }

  setActionButton();
}

function validateRegion() {
  if ($('#region_input').val().length) {
    $('#err_store_region').hide();
  }

  setActionButton();
}

function validateAddressNumber() {
  if (!$('#address_number_input').val().length) {
    $('#err_store_address_number').hide();
  }
}

function validateAddress() {
  if (!$('#address_input').val().length) {
    $('#err_store_address').hide();
  }
}

function openDeactivatePopup() {
  $('#deactivate_popup').show();
  $('.store-create-wrap').addClass('deactivated');
  $('.store-create-wrap-overlay').show();
}

function closeDeactivatePopup() {
  if (!$('#form_disabled').length) {
    $('#deactivate_popup').hide();
    $('.store-create-wrap').removeClass('deactivated');
    $('.store-create-wrap-overlay').hide();
  }
}

function cleanImageErrors() {
  $('#logo_errors .error').hide();
  $('#main_errors .error').hide();
}

function toggleMoveImageButton() {
  // If there's no image or the image isa stock image, dont show the drag option
  var $uploadImage = $('#upload_image_wrap img');
  var hasImage = $uploadImage.length;

  if (hasImage && $uploadImage.attr('src').indexOf('img/stores-main-image-') === -1) {
    $('#drag_main_image').show();
  } else {
    $('#drag_main_image').hide();
  }
}

function toggleRemoveImageButton() {
  if ($('#upload_image img').length) {
    $('#remove_main_image').show();
  } else {
    $('#remove_main_image').hide();
  }
}

function setButtonCopied() {
  $('#copy_url').hide();
  $('#copy_url_done').show();

  clearTimeout(copyEvent);
  copyEvent = setTimeout(function() {
    $('#copy_url_done').hide();
    $('#copy_url').show();
  }, 1000);
}

function setButtonCopyUrl() {
  $('#copy_url_done').hide();
  $('#copy_url').show();
}

function validateStoreName($input) {
  var $inputRow = $input.closest('.input-row');
  var $validation = $inputRow.find('.validation');

  if ($inputRow.find('.error').is(':visible')) {
    $validation.hide();
  } else {
    $validation.show();
  }
}

function positiveValidation($input) {
  var inputId = $input.attr('id');

  if (inputId === 'name_input') {
    validateStoreName($input);
  } else if (inputId === 'info_text_input') {
    validateMinLength($input, 20);
  } else if (inputId === 'website_url_input') {
    validateWebsite($input);
  } else if (inputId === 'phone_input') {
    validatePhone($input);
  } else {
    validateMinLength($input, 1);
  }
}

function validateWebsite($input) {
  var targetValue = $input.val();
  var $error = $('#err_store_website_url');
  var $validation = $input.closest('.input-row').find('.validation');

  if (!checkUrlRegex(targetValue) && targetValue.length) {
    $error
      .removeClass('validation_msg success')
      .addClass('error validation_error')
      .text(loginBconf.messages.ERROR_URL_INVALID)
      .show();
    $validation.hide();
  } else {
    if (targetValue.length) {
      $validation.show();
    }

    $error.hide();
  }
}

function validateMinLength($field, minLength) {
  if ($field.val().length >= minLength) {
    $field.next('.icon-check-circled').show();
    $field.parent().find('.error').hide();
  } else {
    $field.next('.icon-check-circled').hide();
  }
}

function validateUrl($field) {
  var pattern = new RegExp(/((https|http):\/\/)?(([_a-z0-9-]+\.)+([a-z]{2,4})(\/)?([._a-z0-9-])*)$/);

  if (pattern.test($field.val())) {
    $field.next('.icon-check-circled').show();
    $field.parent().find('.error').hide();
  } else if (!$field.val().length) {
    $field.parent().find('.error').hide();
  } else {
    $field.next('.icon-check-circled').hide();
  }
}

function validatePhone($field) {
  var MIN_PHONE_LENGTH = 7;
  var $error = $('#err_store_phone');
  var fieldValue = $field.val();

  validateMinLength($field, MIN_PHONE_LENGTH);

  if (fieldValue.length >= MIN_PHONE_LENGTH || fieldValue.length === 0) {
    $error.hide();
  } else {
    $error
      .removeClass('validation_msg success')
      .addClass('error validation_error')
      .text(loginBconf.messages.ERROR_PHONE_TOO_SHORT)
      .show();
  }

  setActionButton();
}

function justNumbers($field) {
  var value = $field.val();

  value = value.replace(/\D/g, '');

  if (value.length > 11) {
    value = value.substring(0, 11);
  }

  $field.val(value);
}

function toggleDashed() {
  if ($('#upload_image_wrap > img[src*=h]').length) {
    $('#main_image_upload_area').removeClass('dashed-border');
  } else {
    $('#main_image_upload_area').addClass('dashed-border');
  }

  if ($('#logo_upload > img[src*=h]').length) {
    $('#logo_upload').removeClass('dashed-border');
  } else {
    $('#logo_upload').addClass('dashed-border');
  }
}

function copyToClipboard(ID) {
  var copyText = document.getElementById(ID);
  copyText.select();
  window.document.execCommand("copy");
}

function checkForTransactionError() {
  if ($('#transaction_error').length) {
    $(document).scrollTop(1000);
  }
}

function checkUrlRegex(urlIn){
  /* Detect www.-aaaa.cl or www.aaa-.cl that is forbidden */
  var URL_MINUS = /^(-|.*\.-|.*-\.)/;
  /* Detect www.aa--aaaa.cl that is forbidden */
  var URL_DOUBLE_MINUS = /^(..--|.*\...--)/;
  /* Represents basic website without dir like www.yapo.cl */
  var WEBSITE_SIMPLE = /^[A-Za-z0-9]+\.[a-z0-9\-\u00F1\u00E1\u00E9\u00ED\u00F3\u00FA\u00FC\u00D1\u00C1\u00C9\u00CD\u00D3\u00DA\u00DC]{1,63}\.[a-zA-Z\.]{2,7}$/;
  /* Represents basic website without host like yapo.cl */
  var WEBSITE_SIMPLE_SHORT = /^[a-z0-9\-\u00F1\u00E1\u00E9\u00ED\u00F3\u00FA\u00FC\u00D1\u00C1\u00C9\u00CD\u00D3\u00DA\u00DC]{1,63}\.[a-zA-Z\.]{2,7}$/;
  /* Represents the dir starting in / alfter basic url like /foo/var */
  var WEBSITE_DIR = /\/[A-Za-z0-9\u00F1\u00E1\u00E9\u00ED\u00F3\u00FA\u00FC\u00D1\u00C1\u00C9\u00CD\u00D3\u00DA\u00DC\/%&=\?_:;\-\.~]*$/;

  var splitPosition = urlIn.search('/');
  var dir = urlIn.substr(splitPosition);
  var domain = urlIn.split('/')[0];

  if(splitPosition>0 && !WEBSITE_DIR.test(dir)) {
    return false;
  }
  if (URL_DOUBLE_MINUS.test(domain) || URL_MINUS.test(domain)) {
    return false;
  }
  if (!WEBSITE_SIMPLE.test(domain) && !WEBSITE_SIMPLE_SHORT.test(domain)) {
    return false;
  }
  return true;
}
