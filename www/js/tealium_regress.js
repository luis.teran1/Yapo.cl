/*
 * The function used by tealium is replaced, for execute page tags and click tags in regress.
 */
utag = {};

function getHitTealiumuRL(){
	var http_port = $('#tealium_port_httpd').val();
	var ssl_port = $('#tealium_port_ssl').val();
	var prot_host = window.location.protocol + '//' + window.location.hostname;
	var host = prot_host + ':' + http_port;

	if(window.location.port == ssl_port){
		host = prot_host + ':' + ssl_port;
	}
	return host + '/hit.tealium';
}

utag.view = function(utag_data, requestDone){
	var host = getHitTealiumuRL();
	$.get(host, {'utag_data': utag_data, 'type': 'page'})
	.done(function( data ) {
		if(requestDone){
			requestDone();
		}
	});
};

utag.link = function(utag_data, requestDone){
	var host = getHitTealiumuRL();
	$.get(host, {'utag_data': utag_data, 'type': 'click'})
	.done(function( data ) {
		if(requestDone){
			requestDone();
		}
	});
};

/*
 * utag.view is executed when the page is loaded in tealium script.
 * */
jQuery(document).ready(function(){
	utag.view(utag_data);
});
