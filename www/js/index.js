/*
 * Index page scripts 
 */

/* 
 * Change area image onmouseover on index page 
 */
function highlight_map(region, out) {
        var HighlightItem = document.getElementById("area_highlight");
	var LinkItem = document.getElementById("region_name_" + region);

        if (!out) {
//                HighlightItem..backgroundImage = "url(/img/state_" + region + ".gif)";
                HighlightItem.className = "sprite-background sprite-state_" + region;
		LinkItem.style.textDecoration = "underline";
        } else {
                HighlightItem.className = "";
		LinkItem.style.textDecoration = "none";
        }

        return true;
}

function show_state(state, out) {

        if (!out) {
            document.getElementById("state_" + state).style.display = "block";
            document.getElementById("state_" + state).className = "show_state_name";
        } else {
            document.getElementById("state_" + state).style.display = "none";
        }

        return true;
}

/*
 * Popup new news window
 */
var newWin;
function newsPopUp(page)
{
	newWin=window.open(page, '_blank', ' width=800, height=' +Math.round(window.screen.availHeight * 0.8)+', scrollbars=yes, screenX = 0, screenY = 0, top = 0, left = 0');
	newWin.focus();    
	return false;  
}    

// Firefox startpage info box 
function startpage_ff() {
	document.getElementById('startpage_ff').style.display = 'none'; 
	document.getElementById('startpage_ff_info').style.display = 'block'; 		
	startpage_set(); 
}

// See if startpage-icon cookie is set 
function startpage_cookie_get() {
	var cookies = document.cookie.split("; ");	
	
	for(i = 0; i < cookies.length; i++) {
		if(cookies[i] == "b_sp=1") 
			return true;
	}	

	return false;
}

/* Register click in cookie and add statistics */ 
function startpage_set() {
	/* Set cookie */
	var cookie_expire = new Date();                                
	cookie_expire.setTime(cookie_expire.getTime() + 1000*60*60*24*365);
	cookie_expire=cookie_expire.toGMTString();
	document.cookie="b_sp=1; expires="+cookie_expire+";";

	/* Ajax */
	ajax_request("/redir?s=startpage_click&nc=1", null, startpage_callback, null, true, "GET"); 

	/* Hide icon */
	document.getElementById("startpage_ie").style.display = 'none'; 
}

/* Dummy callback */
function startpage_callback(result, xmlhttp, link) {
}

/* Hide FF-info box */
function startpage_ff_info_close() {
	document.getElementById("startpage_ff_info").style.display = 'none';
}

/* Save default ca */
function startpage_set_default_ca(ca, domain, xtn2, region) {
	setCookie('default_ca', ca, 365, '/', domain, false);
	if (xtn2 && region) {
		try{xt_med('C', xtn2, region, 'N');}
		catch(e){}
	}
	return true;
}

