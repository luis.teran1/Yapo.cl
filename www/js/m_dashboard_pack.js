var YapoToggle = (function YapoToggle() {
  function parseBoolean(strValue) {
    return strValue && strValue !== "" && JSON.parse(strValue.toLowerCase());
  }
  function getElementData(element) {
    return {
      element,
      id: element.getAttribute('data-ad-id'),
      toggle: !element.checked,
      status: element.getAttribute('data-toggle-status'),
      pack: parseBoolean(element.getAttribute('data-pack-expired')),
      products: parseBoolean(element.getAttribute('data-products-running')),
    }
  }

  function changePackStatus(data) {
    return $.ajax({
      type: 'PUT',
      url: '/pack/ad_status',
      data: JSON.stringify(data),
      dataType: 'json'
    })
  }

  function packDeactivateAd(data) {
    var adId = data.id;
    return changePackStatus({
      status: 'disabled',
      list_id: adId
    }).done(function(response) {
      if (response.status === 'ok') {
        window.location.reload();
      }
    });
  }

  function packActivateAd(data) {
    var adId = data.id;
    return changePackStatus({
      status: 'active',
      list_id: adId
    }).done(function(response) {
      if (response.status === 'ok') {
        window.location.reload();
      } else {
        noPackQuotaModal(data.element);
      }
    });
  }

  function getIFContainer(element) {
    return $(element).parent().parent().parent().parent().parent().next('.pack-if');
  }

  function noPackQuotaModal(element) {
    var ifContainer = getIFContainer(element);
    ifContainer.removeClass('pack-if--hidden');

    var avisoPremium = ifContainer.find('[sub-title="Premium"]');
    var temp = avisoPremium.attr('event-id');
    avisoPremium.attr('event-id', 'temp');
    setTimeout(function() {avisoPremium.attr('event-id', temp)}, 0)
  }

  function noPackQuotaModal(element) {
    var ifContainer = getIFContainer(element);
    ifContainer.removeClass('pack-if--hidden');
    var avisoPremium = ifContainer.find('[sub-title="Premium"]');
    var temp = avisoPremium.attr('event-id');
    avisoPremium.attr('event-id', 'temp');
    setTimeout(function() {avisoPremium.attr('event-id', temp)}, 0)
  }

  return {
    getElementData,
    showPack(e) {
      e.preventDefault();
      e.stopPropagation();
      var data = getElementData(e.currentTarget);
      if (data.toggle) {
        packDeactivateAd(data);
      } else {
        packActivateAd(data);
      }
    }
  }
})();

$(document).ready(
  function() {
    document.querySelectorAll('.yapo-toggle input').forEach(function (element) {
      var data = YapoToggle.getElementData(element);
      if (data.status !== 'disabled') {
        element.addEventListener('click', YapoToggle.showPack);
        element.removeAttribute('disabled');
      }
    });
  }
);