
$( document ).ready(function() {
  //Build menu
  $('.menu').each(function() {
      idElement = $(this).attr('id');
      nameElement = $(this).find('h2').first().text();
      $('#top-menu').append('<li class="'+idElement+'"><a class="item-menu" href="#'+idElement+'">'+nameElement+'</a></li>');  
      $('#top-menu').css('height', window.innerHeight); 
  });

  //Build colors
  $('.color').each(function(){
      colorElement = $(this).find('span').first().text();
      $(this).css('background-color', colorElement)
      //console.log(colorElement);
  });

  // Cache selectors
  var lastId,
      topMenu = $("#top-menu"),
      topMenuHeight = topMenu.outerHeight(),
      // All list items
      menuItems = topMenu.find("a"),
      menuItemSelect = topMenu.find("a.item-menu"),
      // Anchors corresponding to menu items
      scrollItems = menuItems.map(function(){
        var item = $($(this).attr("href"));
        if (item.length) { return item; }
      });

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href"),
        offsetTop = href === "#" ? 0 : $(href).offset().top - 15;
    $('html, body').stop().animate({ 
        scrollTop: offsetTop
    }, 300);
    e.preventDefault();
    parseInt(this.hash.slice(1));
  });

  // Bind to scroll
  $(window).scroll(function(){
     // Get container scroll position
     var fromTop = $(this).scrollTop()+40;
     
     // Get id of current scroll item
     var cur = scrollItems.map(function(){
       if ($(this).offset().top < fromTop)
         return this;
     });
     // Get the id of the current element
     cur = cur[cur.length-1];
     var id = cur && cur.length ? cur[0].id : "";
     
     if (lastId !== id) {
         lastId = id;
         // Set/remove active class
         menuItems.parent().removeClass("active").end().filter("[href=#"+id+"]").parent().addClass("active");
     }                   
  });

  $('#tab-container, #tab-container-check , #tab-container-radio').easytabs({
    animate: false
  });

  $('input.bump-check').iCheck({
    radioClass: 'iradio_green-check',
    increaseArea: '20%' // optional
  });

  $('input.gallery-check').iCheck({
    radioClass: 'iradio_blue-check',
    increaseArea: '20%' // optional
  });

  $('input.icheck').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      increaseArea: '20%' // optional
  });

  $('input.circle_check').iCheck({
        radioClass: 'iradio_minimal-green',
        increaseArea: '20%' // optional
    });
  //Tooltips 
  $(".darktooltip").darkTooltip({
      animation: 'flipIn',
      gravity: 'west',
      opacity: 0.7
  });

  $("#radios").radiosToSlider();

  $('#account_region').selectize({
      allowEmptyOption: true,
      create: false
    });
  //Fix select text key
  $('.selectize-input input').prop('disabled', true);
});
