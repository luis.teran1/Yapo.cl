function initializeNotifications(environment, callback){
    var userData = JSON.parse(localStorage.getItem('accountData'));
    if (userData !== null) {
        var userMail = userData.email;
        if (typeof userMail !== 'undefined' && userMail !== '') {
            saveTypes();
            getNotifications(userMail);

            /*Be careful before using the above function "getNotifications", 
            because it could show notifications simultaneously with the
            "getYapesosNotification" function */

            var dataTag = document.getElementById('account_is_pro_for');
            var time = dataTag.getAttribute("time");
            var storageName = dataTag.getAttribute("storage_name");
            var isEnabled = (dataTag.getAttribute("enabled") == "1") ? true : false;
            var isPro = (dataTag.getAttribute("is_pro") === "1") ? true : false;

            if (isPro && isEnabled && shouldShowYapesosSplash(time, storageName)) {
                getYapesosNotification(environment);
            }
        }
    }
    if (callback) {
        callback();
    }
}

function saveTypes () {
    fetch('/notifications/api/v1/types')
    .then(function(response) {
        if (response.status === 200){
            return response.json();
        }
        return '';
    })
    .then(function(response) {
        localStorage.setItem('types', JSON.stringify(response));
    });
}

function getNotifications(userMail){
    fetch(`/notifications/api/v1/notifications/${userMail}`)
    .then(function(response) {
        if( response.status === 200){
            return response.json();
        }
        return '';
    })
    .then(function(response) {
        if(response != '') {
            var acceptLink = response[0].payload.acceptLink;
            var rejectLink = response[0].payload.rejectLink;
            var types = JSON.parse(localStorage.getItem('types'));
            var type = response[0].type;
            var typeConf = '';
            var dataNotification = $.param(response[0].payload);
            if(types !== null) {
                typeConf = types.find(function(el){
                return el.type === type;
                });
                if(typeof typeConf !== 'undefined' && typeConf.view !== ''){
                redirect(acceptLink, rejectLink);
                }
            }
        }
    });
}

function redirect(acceptLink, rejectLink) {
    window.addEventListener('message', function (evt) {
        if (evt.origin === window.location.origin && evt.data === 'accepted') {
            window.location.assign(`${acceptLink}`);
        }
        if (evt.origin === window.location.origin && evt.data === 'rejected') {
            window.location.assign(`${rejectLink}`);
        }
    });
}

function shouldShowYapesosSplash(minTime, storageName) {
    var lastSplashShowed = parseInt(localStorage.getItem(storageName));
    var now = (new Date()).getTime();

    if (isNaN(lastSplashShowed) || now - lastSplashShowed > minTime) {
        localStorage.setItem(storageName, now);
        return true;
    }
    return false;
}

function getYapesosNotification(environment, storageName) {
    fetch('/creditos/api/balance').then(function (response) {
        if (response.status === 200) {
            return response.json();
        }
        return false;
    }).then(function (response) {
        if (response !== false) {
            if (response.Balance > 0) {
                var style = {};
                var defaultStyle = {};
    
                switch (environment) {
                    case 'DESKTOP':
                        style = { height: "572px", width: "100%", maxWidth: "800px" }
                        defaultStyle = { height : "506px", width : "410px", maxWidth : "410px" };
                        break;
                    case 'MSITE':
                    default:
                        style = { width: "100%" };
                        defaultStyle = { height : "518px", width : "250px", maxWidth : "250px" };
                        break;
                }
    
                showNotificationsModal("/notifications/app", "yapesos", "/", style, defaultStyle, "listing_page_yapesos_close");
            } else {
                localStorage.removeItem(storageName);
            }
        }
    });
}

function showNotificationsModal(url, notification, separator, style, defaultStyle, onCloseEventName) {
    var mSeparator = (separator) ? separator : "?";
    var urlNotification = `${url}${mSeparator}${notification}`;

    var mWidth = (style.width) ? style.width : (defaultStyle) ? defaultStyle.width : "250px";
    var mHeight = (style.height) ? style.height : (defaultStyle) ? defaultStyle.height : "548px";
    var mMaxWidth = (style.maxWidth) ? style.maxWidth : (defaultStyle) ? defaultStyle.maxWidth : "250px";

    $.pgwModal({
        content: `<iframe style="visibility:hidden; border:none" id="pdModalIframe" width="${mWidth}";" height="${mHeight}";" src="${urlNotification}"></iframe>`,
        mainClassName: 'pgwModal pdInfoModal',
        maxWidth: mMaxWidth,
        closeOnEscape: false,
        closeOnBackgroundClick : false
    });
    document.getElementById('pdModalIframe').onload = function() {
        $('#pdModalIframe')
            .contents()
            .find('html')
            .css('overflow', 'hidden');
        $('#pdModalIframe').css('visibility', 'visible');
    };
    $(document).bind('PgwModal::Close', function() {
        sendTealiumTagAddUtagData({
            event_name: onCloseEventName,
            data: {
                url,
            }
        }, false);
        $(document).unbind('PgwModal::Close')
    });
}