/* global messages, Yapo:true, Validator */

window.Yapo = window.Yapo || {};

$(document).ready(function onReady() {
  'use strict';

  var $editDataButton = $('#edit_data');
  var $saveDataButton = $('#save_data');
  var $phoneInput = $('#phoneInputWrapper');
  var $beginInformationPhoneHide = $('.beginInformation-phoneHide');
  var $phone = $('#phone_text');
  var $msgVisiblePhone = $('#msg_visible_phone');
  var $phoneHidden = $('#phone_hidden');
  var $phoneMsgError = $('#phone_error');
  var $email = $('#email');
  var $nameInput = $('#name');
  var $nameLabel = $('#name_label_data');
  var $nameMsgError = $('#name_error');
  var baseUrl = $phoneInput.data('base_url');
  var commune = document.getElementById('communes');
  var phoneInput = new Yapo.PhoneInput();
  var phoneError = document.getElementById('phone-error');

  if (commune) {
    commune.addEventListener('change', function onChangeCommune(event) {
      phoneInput.setAreaCodeByCommune(event.target.value);
    });

    if (commune.value) {
      phoneInput.setAreaCodeByCommune(commune.value);
    }
  }

  if (phoneError && phoneError.innerHTML.length) {
    enableEditParams();
  }

  $editDataButton.on('click', function onEditDataClick(e) {
    e.preventDefault();
    enableEditParams();
  });

  $saveDataButton.on('click', function onSaveDataClick(e) {
    var isValidPhone = phoneInput.validate();
    var nameInputValue = $nameInput.val();
    var areaCodeNumber = phoneInput.getArea();

    e.preventDefault();
    hideAllMsgError();

    if (!nameInputValue) {
      showMsgError($nameMsgError, $nameMsgError.data('error_name_missing'));
    }

    if (!isValidPhone || !nameInputValue) {
      return;
    }

    updateUserData({
      email: $email.val(),
      phone: areaCodeNumber + '' + phoneInput.getNumber(),
      name: nameInputValue
    });
  });


  /**
   *
   * Send phone and email for update the phone of the account.
   *
   * @param {String} Phone value.
   * @phpUrl /cuenta/update_params
   * @response {Boolean} response.ok True if the phone was changed.
   * @response {String} response.error.phone Can be `ERROR_PHONE_TOO_SHORT`, `ERROR_PHONE_INVALID`,
   *  `ERROR_PHONE_MISSING` or `ERROR_PHONE_TOO_LONG`,
   * @response {String} response.redirect_url Can be `/login` or `/logout?exit=1`.
   *
   */
  function updateUserData(data) {
    $.ajax({
      url: baseUrl + '/cuenta/update_params',
      data: data,
      type: 'POST',
      xhrFields: {
        withCredentials: true
      },
      success: function successUpdateData(response) {
        if (response.ok) {
          disableEditParams();

          $phone.text('+56 ' + data.phone);

          $nameLabel.text(data.name);
          // $nameInput.val(data.name);

          if (!$phoneHidden.is(':checked')) {
            $msgVisiblePhone.removeClass('__is-hidden');
          } else {
            $msgVisiblePhone.addClass('__is-hidden');
          }
        } else if (response.redirect_url) {
          window.location = baseUrl + response.redirect_url;
        } else if (response.error) {
          if (response.error.phone) {
            showMsgError($phoneMsgError, messages[response.error.phone]);
          }

          if (response.error.name) {
            showMsgError($nameMsgError, messages[response.error.name]);
          }
        }
      }
    });
  }


  /**
   *
   * Enable edit phone.
   *
   */
  function enableEditParams() {
    $editDataButton.addClass('__is-hidden');
    $saveDataButton.removeClass('__is-hidden');
    $phoneInput.removeClass('is-hidden');
    $beginInformationPhoneHide.removeClass('__is-hidden');
    $phone.addClass('__is-hidden');
    $nameInput.removeClass('__is-hidden');
    $nameLabel.addClass('__is-hidden');
    $msgVisiblePhone.addClass('__is-hidden');
  }


  /**
   *
   * Disable edit phone.
   *
   */
  function disableEditParams() {
    $editDataButton.removeClass('__is-hidden');
    $saveDataButton.addClass('__is-hidden');
    $phoneInput.addClass('is-hidden');
    $beginInformationPhoneHide.addClass('__is-hidden');
    $phone.removeClass('__is-hidden');
    $msgVisiblePhone.removeClass('__is-hidden');
    $nameInput.addClass('__is-hidden');
    $nameLabel.removeClass('__is-hidden');
    hideAllMsgError();
  }


  /**
   *
   * Hide message phone error.
   *
   */
  function hideAllMsgError() {
    var elementErrors = [$phoneMsgError, $nameMsgError];
    $.each(elementErrors, function onEachError(index, $element) {
      $element.text('');
      $element.parent('.formEdit').removeClass('__is-error');
      $element.parent('.formEdit').children().removeClass('__is-error');
    });
  }


  /**
   *
   * Show message phone error.
   *
   */
  function showMsgError($elementMsgError, text) {
    $elementMsgError.removeClass('__is-hidden');
    $elementMsgError.text(text);
    $elementMsgError.prev().addClass('__is-error');
    $elementMsgError.parent('.formEdit').addClass('__is-error');
  }
});

/**
 *
 * @name onBlurPassword
 * @public
 * @description
 *
 * Remove `__blockedAccount` CSS's class for change error message style.
 * Call it from Validator (conf/common/bconf/bconf.txt.jsvalidators:58) who needs a public function.
 * Params are settings in bconf.
 *
 * @param {Array} data - Array of data setting in bconf to pass to Validator.validate_string
 * @return void
 *
 */
function onBlurPassword(data) { // eslint-disable-line no-unused-vars
  var input = document.getElementById('passwd');
  var parent = input && input.parentElement;

  //parent.classList.remove('__blockedAccount');

  return Validator.validate_string.call(this, data);
}
