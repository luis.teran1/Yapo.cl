/* eslint-disable no-undefined */
function checkIframeLoaded(id) {
  try {
    // Get a handle to the iframe element
    var iframe = document.getElementById(id);
    var iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
    // Check if loading is complete
    if (iframeDoc.readyState === 'complete' && iframeDoc.script_ready === 'done') {
      return true;
    }
    return false;
  } catch (e) {
    return false;
  }
}

function createNewEvent (eventName) {
  var event;
  if (typeof (Event) === 'function') {
    event = new Event(eventName);
  } else {
    event = document.createEvent('Event');
    event.initEvent(eventName, true, true);
  }
  return event;
}

function legacyHubDataUpdated() {
  var counter = 0;
  var id = setInterval(function() {
    document.dispatchEvent(createNewEvent('account-service::islogged'));
    counter++;
    if (counter > 2) {
      clearInterval(id)
    }
  }, 100);
  if (!isMcCalled) {
    isMcCalled = !isMcCalled;
    var userSession = JSON.parse(localStorage.getItem('accountData')).accSession;
    getBadge(userSession, 6);
  }
}

function getBadge(session, attemps) {
  if (session && typeof session !== 'undefined' && attemps > 0) {
    try {
     if( typeof messagingService !== 'undefined' && messagingService && messagingService.default) {
         var service = messagingService.default.getService(session);
         service.get().then(result => {
           document.querySelector('yapo-header').messagesCounter = result.response.unread;
           document.querySelector('yapo-drawer').setAttribute('messages-counter', result.response.unread);
         });
        } else {
          setTimeout(() => { getBadge(session, --attemps) }, 100);
        }
      } catch(e) {
        console.warn(e);
    }
  } else {
    console.warn('No session id or number of attemps reached')
  }

}
var isMcCalled = false;
if (checkIframeLoaded('legacy_mobile_hub') ||
    checkIframeLoaded('legacy_www_hub') ||
    checkIframeLoaded('legacy_secure_hub')) {
  legacyHubDataUpdated();
} else {
  window.addEventListener('message', function validateLoggedInStatus (evt) {
    if (evt.origin === window.location.origin && evt.data === 'legacy_hub::dataupdated') {
      legacyHubDataUpdated();
    }
  });
}
// fix to $.mobile dom modifications :(
var deltaNameID = 'delta-sticky';
var modalID = 'yapo-search-modal';

if ($.mobile) {
  $.mobile.window.on('pagecontainercreate', function injectStickyAfterJQueryMobile () {
    document.getElementById(modalID).outerHTML = document.getElementById(modalID).outerHTML.replace(/temp-modal/g, modalID);
    document.getElementById(deltaNameID).outerHTML = document.getElementById(deltaNameID).outerHTML.replace(/temp/g, deltaNameID);
  })
} else {
  document.getElementById(modalID).outerHTML = document.getElementById(modalID).outerHTML.replace(/temp-modal/g, modalID);
  document.getElementById(deltaNameID).outerHTML = document.getElementById(deltaNameID).outerHTML.replace(/temp/g, deltaNameID);
}

