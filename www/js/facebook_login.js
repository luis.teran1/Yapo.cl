/* global FB, loginBconf */

var fbConnect = (function() { // eslint-disable-line no-unused-vars
  'use strict';

  var debug = false;
  var isConfigured = false;
  var user = {};

  var accessToken;
  var baseUrl;
  var source;

  var onConnect;
  var onError;
  var onRequireEmail;
  var onRequirePassword;
  var showLoading;

  return {
    changeEmail: changeEmail,
    config: config,
    joinAccount: joinAccount
  };


  /**
   *
   * @name fbConnect#config
   * @private false
   * @description
   *
   * Description.
   *
   * @param {Object} options A configuration object.
   *
   */
  function config(options) {
    if (isConfigured) { return; }
    isConfigured = true;

    debug = options.debug;
    baseUrl = options.baseUrl;
    source = options.source;

    log('config', options);

    if (options.selector) {
      $(document).on('click', options.selector, showLoginDialog);
    }

    onConnect = options.onConnect || function() {};
    onError = options.onError || function() {};
    onRequireEmail = options.onRequireEmail || function() {};
    onRequirePassword = options.onRequirePassword || function() {};
    showLoading = options.showLoading || function() {};
  }


  /**
   *
   * @name fbConnect#showLoginDialog
   * @private true
   * @description
   *
   * Call to FB.login to show a popup.
   *
   */
  function showLoginDialog() {
    log('ShowLoginDialog');

    showLoading(false, 'Facebook');

    if (isIncompatibleBrowser() && false) { // @FIXME Test if this it's necessary
      return onError({code: 'BAD_BROWSER_FOR_FACEBOOK'});
    }

    FB.login(function(response) {
      log('FB.login', response);
      if (processFbLogin(response)) { return login(); }
      onError({cancel: true});
    }, {
      scope: 'user_birthday, email, public_profile'
    } );
  }


  /**
   *
   * @name fbConnect#login
   * @private true
   * @description
   *
   * Send accessToken to API for get user's data from Facebook and do login if
   * is a registered user.
   *
   * @endpoint /facebook/login
   * @response {Boolean} response.ok True if the user is logged.
   * @response {String} response.code Can be `EMAIL_REQUIRED` or
   * `ERROR_ACCOUNT_CANNOT_REPLACE_FACEBOOK_ID`.
   *
   */
  function login() {
    log('login');

    $.ajax({
      url: baseUrl + '/facebook/login',
      method: 'POST',
      type: 'POST',
      data: {
        token: accessToken,
        source: source
      },
      xhrFields: {
        withCredentials: true
      },
      success: function(response) {
        log('login success:', response);

        if (response.code === 'EMAIL_REQUIRED') {
          return getUser(onRequireEmail);
        }
        if (!response.ok) { return onError(response); }
        getUser(onConnect);
      },
      error: function(response) {
        log('login error:', response);
        onError(response);
      }
    });
  }


  function changeEmail(email) {
    log('changeEmail', email);

    $.ajax({
      url: baseUrl + '/facebook/register',
      method: 'POST',
      type: 'POST',
      data: {
        token: accessToken,
        email: email,
        source: source
      },
      xhrFields: {
        withCredentials: true
      },
      success: function(response) {
        log('changeEmail success:', response);
        user.email = email;

        if (response.code === 'PASSWORD_REQUIRED') {
          return getUser(onRequirePassword);
        }

        if (!response.ok) { return onError(response); }
        getUser(onConnect);
      },
      error: function(response) {
        log('changeEmail error:', response);
        onError(response);
      }
    });
  }


  function joinAccount(password) {
    log('joinAccount', password);

    $.ajax({
      url: baseUrl + '/facebook/association',
      method: 'POST',
      type: 'POST',
      data: {
        token: accessToken,
        email: user.email,
        password: password,
        source: source
      },
      xhrFields: {
        withCredentials: true
      },
      success: function(response) {
        log('association success:', response);
        if (!response.ok) { return onError(response); }
        getUser(onConnect);
      },
      error: function(response) {
        log('association error:', response);
        onError(response);
      }
    });
  }


  /**
   *
   * @name fbConnect#processFbLogin
   * @private true
   * @description
   *
   * Get a FB API's response and if status is connected save data in memory and
   * call to `login();`.
   *
   * @param {Object} response Facebook API's response.
   * @return {Boolean} Return false when status isn't "connected".
   *
   */
  function processFbLogin(response) {
    log('processFbLogin', response);

    if (response.status !== 'connected') {
      return false;
    }

    accessToken = response.authResponse.accessToken;
    user.id = response.authResponse.userID;

    return true;
  }


  function getUser(done) {
    log('getUser', user);

    if (user.picture) { return done(user); }

    FB.api('/me', {fields: 'picture.height(130), name, email'}, function(response) {
      user.picture = response.picture.data.url;
      user.name = response.name;
      user.email = response.email;

      done(user);
    });
  }


  /**
   *
   * @name fbConnect#log
   * @private true
   * @description
   *
   * Just a wrapper to console.log for check if is debug is on/off.
   *
   */
  function log() {
    if (!debug) { return; }
    console.log.apply(console, arguments); // eslint-disable-line no-console
  }


  function isIncompatibleBrowser() {
    if (!window.navigator) { return true; }

    var ua = navigator.userAgent;
    var isSafari = /webkit\W(?!.*chrome).*safari\W/i.test(ua);
    var isIOS = /ip(hone|od|ad)/i.test(navigator.platform);
    var versionIOS = navigator.appVersion.match(/os (\d+)_(\d+)_?(\d+)?/i);
    var isIncompatible = false;

    if (isIOS) {
      versionIOS = (versionIOS[1] + versionIOS[2] + (versionIOS[3] || 0)) * 1;
    }

    isIncompatible = isSafari && isIOS && versionIOS < 930;

    return isIncompatible;
  }
})();

$(document).ready(function() {
  if (!loginBconf.enabled) { return; }

  window.fbAsyncInit = function() {
    FB.init({
      appId: loginBconf.facebookAppId,
      xfbml: true,
      version: 'v2.9',
      status: true
    });
  };

  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = '//connect.facebook.net/es_CL/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));
});
