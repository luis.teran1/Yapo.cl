/* global userAds, Typeahead */

!(function() {
  document.addEventListener('DOMContentLoaded', onLoad);

  function onLoad() {
    var listIdInput = document.querySelector('.jobTab-formInput.__hidden');
    var jobTabInput = document.querySelector('.jobTab-formInput.__da');
    var form = document.querySelector('.jobTab-form');

    if (!listIdInput || !jobTabInput) {
      return;
    }

    new Typeahead({
      element: jobTabInput,
      data: userAds,
      showNoResultsWarning: false,
      showAll: true,
      getElement: function(element) {
        listIdInput.value = element.id;
      }
    });

    form.addEventListener('submit', onFormSubmit);
    jobTabInput.addEventListener('keydown', function(evt) {
      if (evt.keyCode === 8) {
        listIdInput.value = '';
      }
    });
  }

  function onFormSubmit(evt) {
    evt.preventDefault();

    var form = evt.currentTarget;
    var formData = serialize(form);
    var formResponse = form.querySelector('.jobTab-formResponse');
    var emailRegex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!formData['list_id'].length || !emailRegex.test(formData['email'])) {
      formResponse.innerHTML = errors.JOBS_AR_REPORT_PARAM_ERROR;
      formResponse.classList.add('__error');
      formResponse.classList.remove('__success');
      return;
    }

    // I am sorry :(
    $.post(form.action, JSON.stringify(formData), function(result) {
      formResponse.innerHTML = result.message;

      formResponse.classList.add('__success');
      formResponse.classList.remove('__error');
      form.reset();
    }).fail(function(err) {
      var responseError = err.responseJSON;

      if (responseError.hasOwnProperty('redirect')) {
        window.location.href = responseError.redirect;
      }

      formResponse.classList.remove('__success');
      formResponse.classList.add('__error');
      formResponse.innerHTML = responseError.error;
    });
  }

  function serialize(form) {
    var data = {};

    [].forEach.call(form.elements, function(element) {
      if (element.name) {
        data[element.name] = element.value;
      }
    });

    return data;
  }
})();
