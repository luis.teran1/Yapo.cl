/* eslint no-unused-vars: "off" */

/**
 *
 * @name formatPrice
 * @public
 * @description
 *
 * Add thousand separator.
 *
 * @param {Number|String} price - number to convert into a string
 * @return {String}
 *
 */
function formatPrice(price) {
  return price.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1.');
}

function formatMoney (amount, decimalCount = 2, decimal = ".", thousands = ",") {
  try {
    decimalCount = Math.abs(decimalCount);
    decimalCount = isNaN(decimalCount) ? 2 : decimalCount;
    var negativeSign = amount < 0 ? "-" : "";
    var i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
    var j = (i.length > 3) ? i.length % 3 : 0;
    return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - Number(i)).toFixed(decimalCount).slice(2) : "");
  } catch (e) {
    console.log(e)
  }
}

function formatStrPrice(price, currency) {
  return currency + ' ' +  formatMoney(price, 0, ",", ".");
}
