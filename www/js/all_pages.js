/* global xt_click, xtcustom, fbConnect, googleConnect, loginBconf */
/* eslint no-unused-vars:0, no-useless-escape:0, wrap-iife:["error", "inside"],
  no-param-reassign:0 */

/*
 * Ajax presence check.
 */
function ajaxCheck() {
  var xmlhttp = window.XMLHttpRequest ?
    new window.XMLHttpRequest() : new window.ActiveXObject('Microsoft.XMLHTTP');
  return !!xmlhttp;
}


/*
 * Preload onmouseover images
 */
function preload_image(source) { // eslint-disable-line camelcase
  var image = new Image;
  image.src = source;
}


/*
 * Onload queue helper, first one uses setTimeout to allow browser rendering
 */
function onloadExecute() {
  var queue = window.onload_queue;
  var length = queue.length;
  var i;

  if (!queue || !length) { return; }

  for (i = 0; i < length; i++) {
    queue[i]();
  }
}


function Onload(func, highprio) {
  var oldonload;

  if (typeof func !== 'function') { return; }

  if (!window.onload_queue_set) {
    window.onload_queue_set = true;
    oldonload = window.onload;

    window.onload = function() {
      if (oldonload) {
        oldonload();
      }

      setTimeout(onloadExecute, 10);
    };
  }

  if (!window.onload_queue) {
    window.onload_queue = [];
  }

  if (highprio) {
    window.onload_queue.unshift(func);
  } else {
    window.onload_queue[window.onload_queue.length] = func;
  }
}

/*
 * Auto resize iframe
 */
function resize_eas_frame(self, maxWidth) { // eslint-disable-line camelcase
  var domain = location.host;
  var content;
  var domainArr;

  document.org_domain = document.org_domain || document.domain;

  /* Only domain, no subdomain */
  content = self.contentWindow ?
    self.contentWindow.document : self.contentDocument.document;

  if (domain.match(/^([0-9].){4}/)) {
    if (domain.indexOf(':')) {
      domain = domain.substr(0, domain.indexOf(':'));
    }
  } else {
    domainArr = domain.split('.');
    if (domainArr.length >= 2) {
      domain = domainArr[domainArr.length - 2] + '.' + domainArr[domainArr.length - 1];
    }
  }

  document.domain = domain;
  self.height = content.body.scrollHeight;

  if (self.height === 0) {
    self.parentNode.removeChild(self);
  } else if (content.body.scrollWidth < maxWidth) {
    self.width = content.body.scrollWidth;
  } else if (maxWidth) {
    self.width = maxWidth;
  }
}

/*
 * Pass request through redir.
 */
function clickcounter(name) {
  var httpreq;

  if (!ajaxCheck()) {
    return;
  }

  httpreq = window.XMLHttpRequest ?
    new window.XMLHttpRequest() :
    new window.ActiveXObject('Microsoft.XMLHTTP');

  httpreq.open('GET', '/redir?nc=1&s=' + name, true);
  httpreq.send(null);
}

/*
 * JS script include, with callback functionality
 */
function include_script(src, callback) { // eslint-disable-line camelcase
  var script = document.createElement('script');
  script.src = src;
  script.type = 'text/javascript';

  if (callback) {
    script.onload = callback;
    script.onreadystatechange = function () {
      if (this.readyState === 'complete' || this.readyState === 'loaded') {
        this.onload();
        this.onload = this.onreadystatechange = null;
      }
    };
  }

  document.getElementsByTagName('head')[0].appendChild(script);
}


function toggleClass(elem, className) {
  var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
  if (hasClass(elem, className)) {
    while (newClass.indexOf(' ' + className + ' ') >= 0) {
      newClass = newClass.replace(' ' + className + ' ', ' ');
    }

    elem.className = newClass.replace(/^\s+|\s+$/g, '');
    $('.form-group.has-error').hide();
  } else {
    elem.className += ' ' + className;
  }
}

function hasClass(elem, className) {
  return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
}

function try_xtc(name, type, source) { // eslint-disable-line camelcase
  xt_click(source, 'C', '', name, type);
  return true;
}

function showLoginModalClick() {
  var xtCustomAux;
  // sometimes when the page takes time to load
  // xtcustom is not defined, so try and catch should avoid failures
  try {
    xtCustomAux = xtcustom;
    xtCustomAux['page_type'] = 'Account_top_navbar'; // eslint-disable-line dot-notation
    xt_click(this, 'F', '', 'foo&stc=' + JSON.stringify(xtCustomAux));
  } catch(err) { // eslint-disable-line
  }
}

function addIEClasses() {
  var ua = navigator.userAgent;
  var doc = document.documentElement;

  if ((ua.match(/MSIE 10.0/i))) {
    doc.className = doc.className + ' ie10';
  } else if ((ua.match(/rv:11.0/i))) {
    doc.className = doc.className + ' ie11';
  }
}

function isIE() {
  var myNav = navigator.userAgent.toLowerCase();
  if (myNav.match(/rv:11.0/i)) { return true; }
  return myNav.indexOf('msie') !== -1 ? parseInt(myNav.split('msie')[1], 10) : false;
}

function isFirefox() {
  return navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
}

function numberLimit($input, limit) {
  $input.waitUntilExists(function() {
    $(this).on('keyup keypress blur change', function(e) {
      var valueLength;
      var limitNumber = parseInt(limit, 10);

      if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
        return false;
      }

      valueLength = $(this).val().length;

      if (valueLength >= limitNumber && (e.which !== 8 && e.which !== 0)) {
        return false;
      }

      return true;
    });
  });
}

function onGoogleSuccess(googleUser) {
  var session = getCookie('acc_session');
  
  if (session) {
    return;
  }
  
  var gprofile = googleUser.getBasicProfile();
  var id_token = googleUser.getAuthResponse().id_token;

  console.log('Logged on google as: ' + gprofile.getName());

  var xhr = new XMLHttpRequest();
  xhr.open('POST', loginBconf.baseUrl + '/google/login');
  xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
  xhr.onload = function() {
    console.log('Signed in as: ' + xhr.responseText);
    if (JSON.parse(xhr.responseText).code == "LOGIN_OK") {
        var redirect = getCookie('ref_go_back');
        if (redirect) {
          deleteCookie('ref_go_back');
          location.href = decodeURIComponent(redirect);
        }
    }
  };
  xhr.send('source=web&token=' + id_token);
}


(function() {
  'use strict';

  var baseUrl;

  // Listeners
  $(document).on('click', '#btn-location', locationToggle);
  $(document).on('click', '#login-account-link', showLoginModal);
  $(document).on('click', '.loginBox-createAccount-link', createAccountClick);
  $(document).on('click', '.loginBox-forgotPassword-link', forgotPasswordClick);
  $(document).on('click', '.loginBox-submit', loginClick);
  $(document).on('click', '.facebookBox-backBtn', changeFacebookModal);
  $(document).on('click', '#fbConnectBtn', fbConnectBtnClick);
  // @TODO remove closeDashboardFeedback and use notification.js
  $(document).on('click', '.dashboardHeader-messageClose', closeDashboardFeedback);
  $(document).on('click', '.header-yapremios', onYapremiosButtonClick);

  $(document).on('submit', '.facebookBox #facebook-email-form', changeEmail);
  $(document).on('submit', '.facebookBox #facebook-password-form', joinAccount);
  $(document).on('submit', '.loginBox form', login);
  $(document).on('click', '#customGoogleBtn', googleConnectBtnClick);

  $(document).on('PgwModal::Create', afterModalCreate);
  $(document).ready(onReady);

  // Functions
  function onReady() {
    var nua = navigator.userAgent;
    var isAndroid = (nua.indexOf('Mozilla/5.0') > -1 &&
      nua.indexOf('Android ') > -1 && nua.indexOf('AppleWebKit') > -1) &&
      !(nua.indexOf('Chrome') > -1);

    addIEClasses();

    baseUrl = loginBconf.baseUrl;
    
    if (isAndroid) { $('body').addClass('android-browser'); }

    googleConnect.config({
      baseUrl: baseUrl,
      onSuccess: onGoogleSuccess,
      onError: hideLoading,
    });
    
    var createAccountEl = document.querySelector('.create-account-form');
    
    if (createAccountEl) {
      googleConnect.attachSigninButton(document.querySelector('#customGoogleBtnForm'));
    }
    
    $(document).bind('PgwModal::PushContent', function() {
      googleConnect.attachSigninButton(document.querySelector('#pgwModal #customGoogleBtn'));
    });

    fbConnect.config({
      baseUrl: baseUrl,
      source: 'web',
      selector: '.loginBox #fbConnectBtn,.createAccountFb-button',
      showLoading: showLoading,
      onConnect: function onConnect(status) {
        hideLoading();
        var redirect = getCookie('ref_go_back');
        if (redirect) {
          deleteCookie('ref_go_back');
          location.href = decodeURIComponent(redirect);
        } else {
          location.href = baseUrl + '/dashboard';
        }
      },
      onRequireEmail: function onRequireEmail(user) {
        $('.facebookBox-header-userImage').attr('src', user.picture);
        $('.facebookBox-header-title').text('Hola ' + user.name);

        if (!user.email) {
          $('.facebookBox-header-description:first').text(
            loginBconf.messages.LOGIN_REQUIRE_EMAIL);
        }

        hideLoading();
        showFacebookModal();

        if (user.email) {
          $('.facebookBox #facebook-email').val(user.email);
        }
      },
      onRequirePassword: function onRequirePassword(user) {
        $('.facebookBox-header-userImage').attr('src', user.picture);
        $('.facebookBox-header-title').text('Hola ' + user.name);
        $('.facebookBox-header-title + p span').text(user.email);

        hideLoading();
        showFacebookModal('__requirePassword');
      },
      onError: function onErrorFB(response) {
        var errors;
        var error;
        var $element;

        hideLoading();

        if (response.cancel) {
          closeModal();
          return;
        }

        errors = {
          EMAIL_NOT_VERIFIED: loginBconf.messages.ERROR_EMAIL_NOT_VERIFIED,
          LOGIN_FAILED: loginBconf.messages.ERROR_LOGIN_FAILED,
          ERROR_ACCOUNT_CANNOT_REPLACE_FACEBOOK_ID:
            loginBconf.messages.ERROR_ACCOUNT_CANNOT_REPLACE_FACEBOOK_ID,
          ERROR_ACCOUNT_FACEBOOK_ID_IN_USE: loginBconf.messages.ERROR_ACCOUNT_FACEBOOK_ID_IN_USE,
          VALIDATION_FAILED: loginBconf.messages.ERROR_VALIDATION_FAILED,
          CONECTION_ERROR: loginBconf.messages.ERROR_CONECTION_ERROR,
          SOCIAL_ACCOUNT_EXISTS: loginBconf.messages.ERROR_SOCIAL_ACCOUNT_EXISTS
        };

        error = errors[response.code] || 'Mensaje de error generico';
        $element = $('.loginBox #facebookLoginError');

        if (response.code === 'EMAIL_NOT_VERIFIED') {
          $('.facebookBox .facebookBox-header-description:first').text(error);
          $('.facebookBox #facebook-email-form').remove();
          return;
        }


        if (!$('.loginBox').is(':visible')) {
          $element = $('.facebookBox .loginFeedback');
        }

        $element
          .text(error)
          .addClass('__error')
          .show();
      }
    });
  }


  function login() {
    var $email = $('.loginBox input[name=accbar_email], #myads_email');
    var $password = $('.loginBox input[name=accbar_password]');
    var email = $email.val();
    var password = $password.val();

    var emailError;
    var passwordError;

    // clean old errors
    $email.removeClass('__error');
    $password.removeClass('__error');
    $('.loginBox .loginFeedback').hide();

    if (email) {
      if (!isEmail(email)) {
        emailError = loginBconf.messages.ERROR_EMAIL_INVALID;
      }
    } else {
      emailError = loginBconf.messages.ERROR_EMAIL_MISSING;
    }

    if (password) {
      if (password.length > $password.attr('maxlength')) {
        passwordError = loginBconf.messages.ERROR_PASSWORD_TOO_LONG;
      }
    } else {
      passwordError = loginBconf.messages.ERROR_PASSWORD_MISSING;
    }

    if (emailError) {
      $email.addClass('__error');
      $email.next().text(emailError).show();
    }

    if (passwordError) {
      $password.addClass('__error');
      $password.next().text(passwordError).show();
    }

    if (emailError || passwordError) { return false; }

    $.ajax({
      url: baseUrl + '/login/validate',
      method: 'POST',
      type: 'POST',
      data: {
        email: $email.val(),
        password: $password.val()
      },
      xhrFields: {
        withCredentials: true
      },
      success: function loginSuccess(response) {
        if (response.redirectTo) {
          location.href = response.redirectTo;
          return;
        }

        if (response.status === 'ok') {
          location.reload();
          return;
        }

        showLoginError(response.errorText || '�Oh! algo fall�. Intenta nuevamente.');
      },
      error: function loginError() {
        showLoginError(loginBconf.messages.ACC_ERROR_LOGIN_INVALID);
      }
    });

    return false;
  }

  function showLoginError(error) {
    var $loginFeedback = $('.loginBox .loginFeedback:last');
    $('.loginBox input[name]').addClass('__error');
    $loginFeedback.text(error);
    $loginFeedback.show();
  }

  /**
   *
   * @name isEmail
   * @description
   *
   * Validates if string is a valid email
   * 
   * @param {String} email - String to validate
   *
   * @returns {Boolean}
   *
   **/
  function isEmail(email) {
    return /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(email);
  }

  function locationToggle() {
    $('.region-list').toggle();
    $('#content').toggle();
  }

  function changeEmail() {
    var $email = $('.facebookBox #facebook-email');
    var email = $email.val();
    var error;

    $email.removeClass('__error');
    $email.next().hide();

    if (email) {
      if (!isEmail(email)) {
        error = loginBconf.messages.ERROR_EMAIL_INVALID;
      }
    } else {
      error = loginBconf.messages.ERROR_EMAIL_MISSING;
    }

    if (error) {
      $email.addClass('__error');
      $email.next().text(error).show();
      return false;
    }

    showLoading(false, 'Facebook');
    fbConnect.changeEmail(email);
    return false;
  }


  function joinAccount() {
    var $password = $('.facebookBox #facebook-password');
    var password = $password.val();
    var error;

    $password.removeClass('__error');
    $password.next().hide();

    if (password) {
      if (password.length > $password.attr('maxlength')) {
        error = loginBconf.messages.ERROR_PASSWORD_TOO_LOGN;
      }
    } else {
      error = loginBconf.messages.ERROR_PASSWORD_MISSING;
    }

    if (error) {
      $password.addClass('__error');
      $password.next().text(error).show();
      return false;
    }

    showLoading(false, 'Facebook');
    fbConnect.joinAccount(password);

    xt_click(this, 'C', '', 'log_in\:\:log_in\:\:log_in\:\:Facebook_confirm_password', 'A');

    return false;
  }


  function showLoginModal(evt) {
    var email;

    evt.preventDefault();

    $.pgwModal({
      target: '#login-box',
      mainClassName: 'pgwModal loginBox'
    });

    // Focus on email input after clicking "Iniciar Sesion"
    if (typeof document.querySelector === 'function') {
      email = document.querySelector('.pm-content .formVertical-input[type=email]');

      if (email && typeof email === 'object') {
        email.focus();
      }
    }
  }


  function showFacebookModal(className) {
    var mainClassName = 'pgwModal facebookBox ' + className;

    $.pgwModal({
      target: '#facebook-box',
      mainClassName: mainClassName
    });
  }

  function closeModal() {
    $.pgwModal('close');
    hideLoading();
  }

  function createAccountClick() {
    xt_click(this, 'C', '', 'Register_page\:\:step_1\:\:step_1\:\:step_1', 'N');
  }


  function forgotPasswordClick() {
    xt_click(this, 'C', '',
      'password_forgotten\:\:password_forgotten\:\:password_forgotten\:\:password_forgotten', 'N');
  }


  function loginClick() {
    xt_click(this, 'C', '', 'log_in\:\:log_in\:\:log_in\:\:log_in', 'N');
  }

  function fbConnectBtnClick() {
    try {
      xt_click(this, 'C', '', 'log_in\:\:log_in\:\:log_in\:\:Facebook_login', 'A');
    } catch(err) { } // eslint-disable-line
  }

  function googleConnectBtnClick() {
    showLoading(false, 'Google');
    
    if (typeof window.utag != 'undefined') {
      window.utag.link({ event_name: 'user_login_with_google_account' });
    }
  }

  function changeFacebookModal() {
    $('.facebookBox.pgwModal').toggleClass('__requirePassword');
  }


  function afterModalCreate() {
    $('.loginFeedback').text('').removeAttr('style');
  }


  function closeDashboardFeedback() {
    var $message = $(this).closest('.dashboardHeader-messagesItem');
    var $messagesWrap = $('.dashboardHeader-messages');
    var messagesCount = $('.dashboardHeader-messagesItem').length;
    var $elementToHide = messagesCount > 1 ? $message : $messagesWrap;

    $elementToHide.slideUp(function onSlideUp() {
      $(this).remove();
    });
  }

  function onYapremiosButtonClick(){
    var element = document.getElementsByClassName('header-yapremios')[0];
    var subCategories = element.getAttribute('account_is_pro_for').split(',');
    var firstNumbers = subCategories.map(function(category){
      return category.charAt(0);
    });
    var mainCategories = firstNumbers.filter(function(fNumber, i){
      return firstNumbers.indexOf(fNumber) === i;
    }).map(function(fNumber){
      return fNumber + '000';
    });

    try{
      if (typeof window.utag != 'undefined') {
        window.utag.link({
          event_name: 'yapremios_landing_button_to_display',
          data: {
            category_level1_id: mainCategories,
            category_level2_id: subCategories,
          }
        });
      }
    } catch (e) {
      console.log(e);
    } finally {
        setTimeout(function(){
          window.location.assign('/yapremios/app');
        }, 800);
    }
  }
})();

