window.document.addEventListener("suggested-questions::logged_faq_success", function(e) {
  $("#form_response_ok").show();
  $("html,body").animate({scrollTop: $("#form_response_ok").offset().top - 10});
});

function getFaqsYapoQL(urlGraphQL, category) {
  const baseUrl = window.location.origin + urlGraphQL;
  const client = new YapoQL.default({ url: baseUrl });
  const queryFunc = YapoQL.queries.getFAQsByCategory;
  const faqs = client.query({query: queryFunc, variables: {id: category}});
  
  return faqs;
}

function loadSuggestedQuestions (faqs) {
  const questions = [];

  faqs.map(function (question) {
    questions.push( {'id': question.id, 'question': question.text, 'tag': question.tag});
  });

  return questions;
}

function renderSuggestedQuestionWidget(id, questions) {
  const elContainer = document.getElementById(id);
  const widget = document.createElement('suggested-questions');
  const attrs = elContainer.attributes;
  
  widget.setAttribute('suggestions', JSON.stringify(questions));
  
  Object.keys(elContainer.attributes).map(function (id) {
    if (attrs[id].name !== 'id') {
      const val = (!attrs[id].value) ? '' : attrs[id].value;
      const name = attrs[id].name.replace('data-', '');

      widget.setAttribute(name, val);
    }
  });

  elContainer.appendChild(widget);

  return widget;
}

