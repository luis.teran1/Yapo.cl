function activateAdDone() {
	$.pgwModal({
		mainClassName: 'pgwModal duplicated-modal',
		target: '#duplicated_modal',
		maxWidth: 700,
		closable: false
	});
}

function activateAdFailed() {
	$.pgwModal({
		mainClassName: 'pgwModal duplicated-modal',
		target: '#duplicated_modal',
		maxWidth: 700,
		closable: false
	});
   $(".pm-content .message_successfully").addClass("is-hidden");
   $(".pm-content .message_failed").removeClass("is-hidden");
}

function changeFailedMsg(text) {
  $(".pm-content .message_failed").html(text);
}

function changeFailedTitle(text) {
  $(".pm-content .h2").html(text);
}
