$(document).ready(function() {
  if ($('.boxContainer-cotiza').find('.services').children().length > 0) {
    $('.boxContainer-cotiza').find('.daview-params').addClass('bannerLoad');
  }

  $('.cotiza-link-google').click(function(e) {
    var $cotizaLink = $(this);
    var linkName = $cotizaLink.data("link-name");
    var cotizaModalIsHidden = $("#cotiza_modal_"+linkName).hasClass('is-hidden');

    if ($cotizaLink.data('onclick') == 'follow_url') {
      return this;
    }

    e.preventDefault();

    try {
      xt_med('C', $cotizaLink.data('xiti_xtn2'), $cotizaLink.data('xtclick_name'), $cotizaLink.data('xtclick_type'));
    } catch(err){};

    try {
      sendTealiumTag($cotizaLink.data('tealium-view'), true);
    } catch(err){};

    // Send xiti tag page in msite and desktop
    try {
      xt_med('F', '', $('#cotiza_link_xiti_' + linkName).data('xiti_xtpage'));
    } catch(err){};

    if (cotizaModalIsHidden) {
      $("#cotiza_modal_"+linkName).removeClass('is-hidden');
    }

    if (typeof(displayModalBanners) == "function") {
      displayModalBanners(linkName);
    }
  });

  $('.modal-cotiza-close, .modal-cotiza-overlay').click(function(e) {
    var modalOpen = $('.modal-open').data('type');
    hiddenModalBanner(modalOpen);
  });

  if ($('.cotiza-link-google').size() > 1) {
    $('.cotiza-link-google').each(function(index, element) {
      if (index > 0) {
        $(this).addClass('include-pipe');
      }
    });
  }
  
  $('.modal-cotiza-header a').click(function(){
    window.history.back();
  });

  $(".cotiza-banner-page").on("pageshow", function() {
    var linkName = $(this).data("link");
    $('#cotiza_help_' + linkName).data('onclick', 'display').trigger('click');
  });

  $(".cotiza-banner-page").on("pagehide", function() {
    var linkName = $(this).data("link");
    $('#cotiza_help_' + linkName).data('onclick', 'follow_url');
  });
});

function displayModalBanners(linkName) {
  var modal = $('#cotiza_modal_'+ linkName);
  var overlay = $('.modal-cotiza-overlay');
  var body = $('body');

  if (!modal.hasClass('is-created')) {
    modal.addClass('is-created');

    loadCotizadores(modal, linkName);
  }

  modal.removeClass('is-hidden');
  overlay.removeClass('is-hidden');
  modal.addClass('modal-open');
  body.addClass('modalCotizaOpen');

  setTimeout(function () {
    modal.addClass('--show');
    overlay.addClass('--show');
  }, 500);
}

function hiddenModalBanner(linkName) {
  var modal = $('#cotiza_modal_'+ linkName);
  var overlay = $('.modal-cotiza-overlay');
  var body = $('body');

  modal.removeClass('--show');
  modal.removeClass('modal-open');
  overlay.removeClass('--show');

  setTimeout(function () {
    modal.addClass('is-hidden');
    overlay.addClass('is-hidden');
    body.removeClass('modalCotizaOpen');
  }, 500);
}

function loadCotizadores(modal, linkName) {
  var content = '<iframe id="cotiza-iframe-' + linkName + '" src="/cotiza_banners.html#' + linkName + '" frameborder="0"></iframe>';
  modal.find('#modal_cotiza_' + linkName + '_wrap').append(content);
}

function loadBannersReady(dataCotiza) {
  var iframeCotiza = document.getElementById('cotiza-iframe-' + dataCotiza.linkName + '');

  iframeCotiza.style.height = dataCotiza.count * dataCotiza.height + 'px';
}

window.addEventListener('message',function(message){
  if (message.data.evt == "cotiza-ready") {
    loadBannersReady(message.data);
  }
});
