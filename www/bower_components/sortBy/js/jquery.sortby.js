/* 
 * sortby v0.1.0
 * jquery plugin to convert a 'sort by' by component
 * (c)2015 Rub�n Torres - rubentdlh@gmail.com
 */

(function($) {

 	function SortBy(select, options){
 		this.$select=select;
 		this.options=options;
 		this.defaultLabel = this.$select.find('option').html();
 		this.defaultValue = this.$select.find('option').val();
 	}

 	SortBy.prototype = {

 		init: function(){
 			var self = this;
 			self.$select.hide();
 			self.$select.after('<ul class="sort-by"></ul>');
			self.$list = self.$select.next('ul');
			self.$list.addClass(self.options.theme);

			var i = 0;
			$(self.$select).find('option').each( function(){
				if(i == 0){
					self.$list.append('<li class="sb-default" data-value="label"><span>' + $(this).html() + '</span><i class="icon-yapo icon-arrow-down-bold"></i></li>');
				}else{
					self.$list.append('<li class="sb-item" data-value="' + $(this).val() + '" data-label="' + $(this).data('label') + '"><span>' + $(this).html() + '</span></li>');
				}
				i++;
			});
			self.$list.find('.sb-item').hide();

			if(self.$select.is(':disabled')){
 				self.$list.addClass('sb-disabled');
 			}else{
 				self.setEvents();
 			}
 			self.preselect();
 		},

 		setEvents: function(){
 			var self = this;
 			
 			self.$list.find('li.sb-default').click( function(e){
 				e.stopPropagation();
 				self.toggle();
 			});

 			self.$list.find('li.sb-item').click( function(e){
 				e.stopPropagation();
 				self.close();
 				var value = $(this).data('value');
 				var label = $(this).data('label');
 				self.setSelected(value, label);
 			});

 			$(document).click( function(){
 				self.close();
 			})

 		},

 		toggle: function(){
 			if(this.isOpen()){
 				this.close();
 			}else{
 				this.open();
 			}
 		},

 		open: function(){
 			//Close all others
 			$('.sort-by').removeClass('open');
 			$('.sort-by').find('li.sb-item').hide();
 			$('.sort-by').find('li.sb-default').removeClass('open');
 			$('.sort-by').find('li.sb-default i').addClass('icon-arrow-down-bold');
 			$('.sort-by').find('li.sb-default i').removeClass('icon-arrow-up-bold');

 			this.$list.addClass('open');
 			this.$list.find('li.sb-item').show();
 			this.$list.find('li.sb-default').addClass('open');
 			this.$list.find('li.sb-default i').removeClass('icon-arrow-down-bold');
 			this.$list.find('li.sb-default i').addClass('icon-arrow-up-bold');
 		},

 		close: function(){
 			this.$list.removeClass('open');
			this.$list.find('li.sb-item').hide();
			this.$list.find('li.sb-default').removeClass('open');
			this.$list.find('li.sb-default i').addClass('icon-arrow-down-bold');
 			this.$list.find('li.sb-default i').removeClass('icon-arrow-up-bold');
 		},

 		isOpen: function(){
 			return this.$list.find('li.sb-item').is(':visible');
 		},

 		setSelected: function(value, label){
 			
 			this.$list.find('li.sb-default').html('<span>' + label + '<span><i class="icon-yapo icon-arrow-down-bold"></i>');
 			this.$select.find('option[value=' + value + ']').prop('selected', true).change();
 			
 		},

 		reset: function(){
 			this.$select.find('option[value=' + this.defaultValue + ']').prop('selected', true).change();
 			this.$list.find('li.sb-default .sb-label').html(this.defaultLabel);
 			this.$list.find('li.sb-default i').removeClass('icon-close-circled');
 			this.$list.find('li.sb-default i').addClass('icon-arrow-down-bold');
 			this.$list.find('li.sb-default').removeClass('sb-selected');
 			this.$list.find('.icon-yapo').off('click');
 		},

 		preselect: function(){
 			var value = this.$select.val();
 			if(value != this.defaultValue){
 				var label = this.$select.find('option[value=' + value + ']').data('label');
 				var value = this.$select.val();
 				this.setSelected(value, label);
 			}
 		}

 	}

 	$.fn.sortby = function(options) {

		if (typeof options == 'object' || !options) {
			
			return this.each(function(){
				options = $.extend({}, $.fn.sortby.defaults, options);
 				var sortby = new SortBy($(this), options);
 				sortby.init();
			});	
			
		}

 	}

 	$.fn.sortby.defaults = {};

 })(jQuery);
