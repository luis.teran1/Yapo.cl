/*
 * minform plugin v0.1.1
 * (c)2014 Marco Gallardo - mio@katunein.com
 * Released under the MIT license
 */
(function($){
	$.fn.minform = function(){
		$this = this;
		var fm = this.find('li');
		items = fm.length;
		var count = { c: 1 };
		$this.addClass('minform');
		$(fm).hide();
		$this.find('li:nth-child('+count.c+')').show();

		nextItem(count, $this);

		$('.current').append(count.c);
		$('.total-count').append(items-1);
	};
})(jQuery);

function nextItem(count, $element) {
	$('.minform input').on('keypress', function(e){
		var code = e.keyCode || e.which;
		if(code == 13) {
			eventNext(count, $element);
		}
	})

	$('.next').click(function(){
			eventNext(count, $element);
	})
}

function eventNext(count, $element){

	$('.next').show();
	$('.next').removeClass('ok');
	$('.icon-close').hide();

	var item = $this.find('li:nth-child('+count.c+')');
	var idInput = item.find('input').attr('id');
	var valid = $('#'+idInput).valid();
	if (valid) {
		$this.trigger('next', [count.c, item]);
		item.hide();
		count.c++;
		$this.find('li:nth-child('+count.c+')').fadeIn(function(){ $(this).find('input, select').focus(); });
		printCurrent(count);
		succesForm(count, $element);
	} else {
		$('.next').hide();
		$('.icon-close').show();
	}
}

function printCurrent(count) {
	$('.current').empty();
	$('.current').append(count.c);
}

function succesForm(count, $element) {
	if (count.c > (items-1)) {
		$('.count').hide();
		$('.next').hide();
		$element.trigger('submit');
	}
}
