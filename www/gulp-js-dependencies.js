/* eslint-env node */
/*
 * Source files needed to build each js file
 */
var jsDependencies = [
  {
    final: 'ad_delete.js',
    sources: [
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/jquery.radioSlider.js',
      'www/js/ad_deletion.js',
      'www/js/events.js',
      'www/3rdparty/radio-to-slider/js/jquery.radios-to-slider.js',
      'www/js/jquery.icheck.js'
    ]
  },
  {
    final: 'ad_delete_success.js',
    sources: [
      'www/js/jquery.icheck.js',
      'www/js/ad_deletion_success.js'
    ]
  },
  {
    final: 'adwatch.js',
    sources: [
      'php/adwatch/js/adwatch.js',
      'www/3rdparty/jquery.lazyload/jquery.lazyload.js',
      'www/js/common.js',
      'www/js/pixel.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/adwatch.src.js',
      'www/js/all_pages.js'
    ]
  },
  {
    final: 'ai.js',
    sources: [
      // Components
      'www/js/hectares.js',
      'www/js/aiUpsellingProducts.js',
      'www/js/aiUpselling.js',
      'www/js/ufPriceValidation.js',
      'www/js/jobAdvices.js',
      'www/js/aiCategoryLogin.js',
      'www/js/aiPlates.js',
      'www/js/aiJobs.js',

      'www/js/bubble_msgs.js',
      'www/js/common2.js',
      'www/js/pixel.js',
      'www/js/events.js',
      'www/js/index.js',
      'www/js/category_dropdown2.js',
      'www/js/jquery.placeholder-enhanced.min.js',
      'php/newad/js/functions.js',
      'php/newad/js/newad_dhtml.js',
      'php/newad/js/addelete.js',
      'www/js/Validator.js',
      'www/js/jquery.tools.min.js',
      'www/js/jquery-ui-1.9.1.js',
      'www/js/jeditable-1.7.1.js',
      'www/js/pixel.js',
      'www/js/deletion_reasons.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/bower_components/jQuery-ajaxTransport-XDomainRequest/jQuery.XDomainRequest.js',
      'www/bower_components/doT/doT.js',
      'www/js/car_subject.js',
      'www/js/format_number.js',
      'www/js/jquery-rut.js',

      'www/js/file-uploader/header.js',
      'www/js/file-uploader/util.js',
      'www/js/file-uploader/button.js',
      'www/js/file-uploader/handler.base.js',
      'www/js/file-uploader/handler.xhr.js',
      'www/js/file-uploader/handler.form.js',
      'www/js/file-uploader/uploader.basic.js',
      'www/js/file-uploader/jquery-plugin.js',
      'www/js/jquery.icheck.js',
      'www/js/ai_src.js',

      'www/3rdparty/waituntilexists/waitUntilExists.js',
      'www/3rdparty/nokia-maps/leaflet.js',
      'www/3rdparty/nokia-maps/Edit.SimpleShape.js',
      'www/3rdparty/nokia-maps/Edit.Circle.js',

      'www/js/ai_map_setup.js',
      'www/js/duplicated.js',
      'www/js/duplicated_desktop.js',
      'www/js/moment.js',
      'www/js/yapo-map.js',
      'www/js/ai_your_information.js',
      'www/js/components/phoneInput.js',
      'www/js/priceValidation.js',
      'www/js/ai_info_banner.js',
      'www/js/ai_car_info_common.js',
      'www/js/ai_car_info_load.js'
    ]
  },
  {
    final: 'ai_success.js',
    sources: [
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/js/ai_success_src.js'
    ]
  },
  {
    final: 'all_pages.js',
    sources: [
      'www/bower_components/jquery/dist/jquery.js',
      'www/bower_components/jquery-placeholder/jquery.placeholder.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/login_spinner.js',
      'www/js/all_pages.js',
      'www/js/facebook_login.js',
      'www/js/google_login.js',
      'www/js/formatPrice.js',
      'www/js/cookie.js',
      'www/js/modalLogin.js',
      'www/js/modalListener.js',
      'node_modules/@yapo/yapo-tag-manager/lib/yapo-tag-manager.js'
    ]
  },
  {
    final: 'controlpanel_pages.js',
    sources: [
      'www/js/ufPriceValidation.js',
      'www/js/file-uploader/header.js',
      'www/js/file-uploader/util.js',
      'www/js/file-uploader/uploader.basic.js',
      'www/js/file-uploader/button.js',
      'www/js/file-uploader/handler.base.js',
      'www/js/file-uploader/handler.xhr.js',
      'www/js/file-uploader/handler.form.js',
      'www/js/file-uploader/jquery-plugin.js',
      'www/js/all_pages.js'
    ]
  },
  {
    final: 'html5shiv.js',
    sources: ['www/bower_components/html5shiv/dist/html5shiv.js']
  },
  {
    final: 'banner-sticky.js',
    sources: ['www/js/banner-sticky.js']
  },
  {
    final: 'category_dropdown.js',
    sources: ['www/js/category_dropdown.js']
  },
  {
    final: 'common.js',
    sources: [
      'www/js/common.js',
      'www/js/pixel.js'
    ]
  },
  {
    final: 'create_account.js',
    sources: [
      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/js/common2.js',
      'www/js/events.js',
      'www/js/create_account_src.js',
      'www/js/components/phoneInput.js'
    ]
  },
  {
    final: 'dashboard.js',
    sources: [
      'www/3rdparty/jquery.lazyload/jquery.lazyload.js',
      'www/3rdparty/bx-slider.js/dist/jquery.bxslider.js',
      'www/js/dashboard_common.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/3rdparty/productSelect/js/jquery.productselect.js',
      'www/bower_components/sortBy/js/jquery.sortby.js',
      'www/bower_components/jquery-toggles/toggles.js',
      'msite/www/js/components/yapoModal.js',
      'www/js/dashboard_renew_pack.js',
      'www/js/dashboard_report.js',
      'www/js/dashboard_src.js',
      'www/js/pagination.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.js',
      'www/js/gallery.js',
      'www/js/components/deleteModal.js'
    ]
  },
  {
    final: 'dashboard_account.js',
    sources: [
      // Components
      'www/js/oneClickSubscription.js',
      'www/js/informativeNotification.js',

      // Other things
      'www/js/dashboard_common.js',
      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/js/common2.js',
      'www/js/events.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js'
    ]
  },
  {
    final: 'dashboard_bump.js',
    sources: [
      'www/js/informativeNotification.js',

      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/paymentMethod.js',
      'www/js/dashboard_bump_src.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/js/payment_common_util.js',
      'www/js/dashboard_common.js',
      'www/js/components/ShoppingCartModal.js'
    ]
  },
  {
    final: 'deletion.js',
    sources: [
      'www/js/jquery-ui-1.9.1.js',
      'www/js/jeditable-1.7.1.js',
      'www/js/pixel.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/deletion_reasons.js',
      'www/js/events.js'
    ]
  },
  {
    final: 'events.js',
    sources: ['www/js/events.js']
  },
  {
    final: 'help.js',
    sources: [
      'www/js/common.js',
      'www/js/pixel.js',
      'www/js/help.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/all_pages.js'
    ]
  },
  {
    final: 'home.js',
    sources: [
      'www/js/common.js',
      'www/js/home_src.js',
      'www/js/google-gpt-config.js'
    ]
  },
  {
    final: 'home_ie.js',
    sources: [
      'www/bower_components/raphael/raphael.js',
      'www/bower_components/raphael-svg-import-classic/raphael-svg-import.js',
      'www/js/home_ie_src.js'
    ]
  },
  {
    final: 'index.js',
    sources: ['www/js/index.js']
  },
  {
    final: 'icon_catalog.js',
    sources: ['www/js/icon_catalog_src.js']
  },
  {
    final: 'list.js',
    sources: [
      'www/js/jquery.icheck.js',
      'www/3rdparty/jquery.lazyload/jquery.lazyload.js',
      'www/js/common.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/pixel.js',
      'www/js/jquery.placeholder-enhanced.min.js',
      'modules/mod_list/js/searchbox.js',
      'www/js/gallery.js',
      'www/js/multiselect.js',
      'www/js/banner-sticky.js',
      'www/js/notifications.js',
      'www/js/listing_src.js',
      'www/js/list-xiti.js',
      'www/js/google-gpt-config.js',
      'www/3rdparty/bx-slider.js/dist/jquery.bxslider.js'
    ]
  },
  {
    final: 'list_mobile.js',
    sources: [
      'www/js/list-xiti.js'
    ]
  },
  {
    final: 'login.js',
    sources: ['www/js/jquery.icheck.js']
  },
  {
    final: 'pagination.js',
    sources: ['www/js/pagination.js']
  },
  {
    final: 'payment.js',
    sources: [
      'www/js/jquery-ui-1.11.4.js',
      'www/js/jeditable-1.7.1.js',
      'www/js/pixel.js',
      'www/js/payment_common.js',
      'www/js/payment_common_util.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/payment_src.js'
    ]
  },
  {
    final: 'payment_confirmation.js',
    sources: [
      'www/js/payment_confirmation_src.js',
      'www/js/form-validations.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js'
    ]
  },
  {
    final: 'preview.js',
    sources: [
      'www/js/common.js',
      'www/js/pixel.js',
      'www/js/jquery.icheck.js',
      'www/js/preview_src.js'
    ]
  },
  {
    final: 'store_buy.js',
    sources: [
      'www/js/dashboard_common.js',
      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/js/payment_common_util.js',
      'www/js/store_buy_src.js'
    ]
  },
  {
    final: 'store_edition.js',
    sources: [
      'www/js/file-uploader/header.js',
      'www/js/jquery.esreposition.js',
      'www/js/file-uploader/util.js',
      'www/js/jquery-ui-1.9.1.js',
      'www/js/try_open.js',
      'www/js/catch_empty.js',
      'www/js/file-uploader/button.js',
      'www/js/file-uploader/handler.base.js',
      'www/js/file-uploader/handler.xhr.js',
      'www/js/file-uploader/handler.form.js',
      'www/js/file-uploader/uploader.basic.js',
      'www/js/file-uploader/jquery-plugin.js',
      'www/js/jquery-rut.js',
      'www/js/payment_common_util.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/js/jquery.icheck.js',
      'www/js/store_share_src.js',
      'www/js/dashboard_common.js',
      'www/js/store_buy_src.js',
      'www/bower_components/zeroclipboard/dist/ZeroClipboard.js',
      'www/js/store_edition_src.js',
      'www/js/storeTextValidation.js'
    ]
  },
  {
    final: 'support.js',
    sources: ['www/js/help.js']
  },
  {
    final: 'viewda.js',
    sources: [
      'www/js/ad_view_common.js',
      'www/js/gtm_controller.js',
      'www/js/common.js',
      'www/js/pixel.js',
      'www/js/jquery.icheck.js',
      'www/js/ad_view.js',
      'www/js/ad.reply.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/bower_components/jquery-cookie/jquery.cookie.js',
      'www/3rdparty/nokia-maps/leaflet.js',
      'www/3rdparty/nokia-maps/Edit.SimpleShape.js',
      'www/3rdparty/nokia-maps/Edit.Circle.js',
      'www/js/yapo-map.js',
      'www/js/ufPriceValidation.js',
      'www/js/galleryAdview.js',
      'www/js/google-gpt-config.js',
      'www/js/cotiza-banners-google.js',
      'msite/www/js/components/yapoModal.js',
      'msite/www/js/components/quotaFinance.js'
    ]
  },
  {
    final: 'xiti_yapo.js',
    sources: ['www/js/xtcore.js']
  },
  {
    final: 'tag_tealium.js',
    sources: ['www/js/tealium.js']
  },
  {
    final: 'store_list.js',
    sources: [
      'www/js/common.js',
      'www/bower_components/jquery-infinite-scroll/jquery.infinitescroll.js',
      'www/bower_components/selectize/dist/js/standalone/selectize.js',
      'www/js/store_share_src.js',
      'www/bower_components/jquery.dotdotdot/src/js/jquery.dotdotdot.js',
      'www/3rdparty/jquery.lazyload/jquery.lazyload.js',
      'www/js/store_list.js'
    ]
  },
  {
    final: 'ui.js',
    sources: [
      'www/js/common.js',
      'www/bower_components/rainbow/js/rainbow.js',
      'www/bower_components/rainbow/js/language/generic.js',
      'www/bower_components/rainbow/js/language/html.js',
      'www/bower_components/rainbow/js/language/css.js',
      'www/bower_components/rainbow/js/language/javascript.js',
      'www/bower_components/easytabs/lib/jquery.easytabs.js',
      'www/js/jquery.icheck.js',
      'www/3rdparty/darkTooltip/js/jquery.darktooltip.js',
      'www/bower_components/radios-to-slider/js/jquery.radios-to-slider.js',
      'www/js/pagination.js',
      'www/bower_components/selectize/dist/js/standalone/selectize.js',
      'www/js/ui_src.js'
    ]
  },
  {
    final: 'not_found.js',
    sources: [
      'www/js/not_found_src.js'
    ]
  },
  {
    final: 'cars_pack.js',
    sources: [
      'www/bower_components/selectize/dist/js/standalone/selectize.js',
      'www/js/dashboard_common.js',
      'www/bower_components/Chart.js/Chart.js',
      'www/bower_components/minform/minform.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'www/bower_components/jquery.validation/dist/additional-methods.js',
      'www/3rdparty/typeahead/typeahead.js',
      'www/js/cars_packs_src.js'
    ]
  },

  {
    final: 'credits.js',
    sources: [
      'www/js/dashboard_common.js',
      'www/js/credits.js'
    ]
  },

  {
    final: 'refund_form.js',
    sources: [
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'www/bower_components/jquery.validation/dist/additional-methods.js',
      'www/js/refund_form_src.js'
    ]
  },
  {
    final: 'landing.js',
    sources: [
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'www/bower_components/jquery.validation/dist/additional-methods.js',
      'www/js/landing_src.js'
    ]
  },
  {
    final: 'm_accounts.js',
    sources: [
      // Components
      'www/js/oneClickSubscription.js',
      'www/js/informativeNotification.js',
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'msite/www/js/jquery.horizontalSelect.js',
      'www/js/jquery.icheck.js',
      'www/3rdparty/jquery.mdTextField/jquery.mdTextField.js',
      'www/js/jquery-rut.js',
      'www/3rdparty/dirrty/js/jquery.dirrty.js',
      'www/js/m_accounts_src.js',
      'www/js/m_dashboard_common_src.js',
      'www/js/notification.js',
      'www/js/components/phoneInput.js'
    ]
  },
  {
    final: 'm_dashboard.js',
    sources: [
      'www/js/jquery.icheck.js',
      'www/3rdparty/jscroll/js/jquery.jscroll.js',
      'msite/www/js/adjust_thumbs.js',
      'www/js/components/deleteModal.js',
      'www/js/m_dashboard_common_src.js',
      'www/js/m_dashboard.js',
      'www/js/m_dashboard_pack.js'
    ]
  },
  {
    final: 'm_dashboard_bump.js',
    sources: [
      'www/js/informativeNotification.js',
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'msite/www/js/jquery.horizontalSelect.js',
      'www/3rdparty/jquery.mdTextField/jquery.mdTextField.js',
      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/js/dashboard_bump_src.js',
      'www/js/paymentMethodMobile.js',
      'msite/www/js/adjust_thumbs.js',
      'www/js/payment_common_util.js',
      'www/js/pay_help_mobile.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/m_dashboard_bump_src.js',
      'msite/www/js/summaryTable.js',
      'www/js/components/ShoppingCartModal.js'
    ]
  }, {
    final: 'm_credits.js',
    sources: [
      'www/js/m_dashboard_scrollaction.js',
      'www/js/m_dashboard_common_src.js',
      'www/js/credits.js'
    ]
  },
  {
    final: 'm_login.js',
    sources: [
      'www/bower_components/jquery-mobile-bower/js/jquery.mobile-1.4.5.js',
      'www/3rdparty/jquery.mdTextField/jquery.mdTextField.js',
      'msite/www/js/jquery.horizontalSelect.js',
      'www/js/facebook_login.js',
      'www/js/google_login.js',
      'www/js/login_spinner.js',
      'msite/www/js/m_login_src.js',
      'www/js/aiCategoryLogin.js',
      'www/js/notification.js',
      'www/js/cookie.js'
    ]
  },
  {
    final: 'mobile_reset_password.js',
    sources: ['msite/www/js/mobile_reset_password.js']
  },
  {
    final: 'payment_mobile.js',
    sources: [
      'www/bower_components/pgwmodal/pgwmodal.js',
      'msite/www/js/adjust_thumbs.js',
      'www/js/payment_mobile.js',
      'www/js/pay_help_mobile.js',
      'www/js/jquery.icheck.js',
      'www/js/payment_common_util.js'
    ]
  },
  {
    final: 'pvf_mobile.js',
    sources: [
      'www/js/pay_help_mobile.js',
      'www/bower_components/pgwmodal/pgwmodal.js',
      'www/js/pvf_mobile.js'
    ]
  },
  {
    final: 'phantom.js',
    sources: [
      'www/bower_components/jquery/dist/jquery.js',
      'www/js/phantom_src.js'
    ]
  },
  {
    final: 'pay_apps.js',
    sources: [
      'www/bower_components/jquery/dist/jquery.js',
      'www/bower_components/jquery.validation/dist/jquery.validate.js',
      'www/js/jquery.icheck.js',
      'www/js/jquery-rut.js',
      'www/js/payment_common_util.js',
      'www/js/pay_apps_src.js'
    ]
  },
  {
    final: 'bumpfrequency.js',
    sources: [
      'www/bower_components/selectize/dist/js/standalone/selectize.js',
      'www/js/bumpfrequency.js'
    ]
  },
  {
    final: 'jobs.js',
    sources: [
      'www/3rdparty/typeahead/typeahead.js',
      'www/js/dashboard_common.js',
      'www/js/jobs_src.js'
    ]
  },
  {
    final: 'wc-info-box.js',
    sources: [
      'node_modules/@altiro-widgets/information-box/dist/info-box.bundled.js'
    ]
  },
  {
    final: 'altiro-bundle.js',
    sources: [
      'node_modules/@yapo/altiro/dist/static/js/manifest.js',
      'node_modules/@yapo/altiro/dist/static/js/vendor.js',
      'node_modules/@yapo/altiro/dist/static/js/app.js'
    ]
  },
  {
    final: 'yapo-hub.js',
    sources: ['node_modules/@yapo/altiro/dist/static/hub.min.js']
  },
  {
    final: 'google-gpt-config.js',
    sources: ['www/js/google-gpt-config.js']
  },
  {
    final: 'common.js',
    sources: ['www/js/common.js']
  },
  {
    final: 'yapo-header-utils.js',
    sources: ['www/js/yapo-header-utils.js']
  }
];

module.exports = jsDependencies;

