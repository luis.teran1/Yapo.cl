/* global CartHandler */

/*
 * productselect v0.1.0
 * jquery plugin to convert a select element into a more sofisticated component
 * to select a product
 * (c)2014 Rub?n Torres - rubentdlh@gmail.com
 * Released under the MIT license
 */

(function($) {
  function ProductSelect(select, options) {
    this.$select=select;
    this.options=options;
    this.defaultLabel = this.$select.find('option').html();
    this.defaultValue = this.$select.find('option').val();
 	}

  ProductSelect.prototype = {
    init: function() {
      var ps = this;
      var i = 0;
      ps.$select.hide();
      ps.$select.after('<ul class="product-select"></ul>');
      ps.$list = ps.$select.next('ul');
      ps.$list.addClass(ps.options.theme);

      $(ps.$select).find('option').each(function(index, option) {
        var optionData = ps.initialConfData(option);

        var liItem = document.createElement('li');
        var wrapItem = document.createElement('div');
        liItem.setAttribute('data-value', optionData.itemValue);
        wrapItem.className = optionData.wrapClass;

        if (optionData.optParent.classList.contains('bump-select')) {
          ps.customClassBump(optionData, ps);
        }

        if (i === 0) {
          wrapItem.innerHTML = '<div class="ps-label">' + optionData.itemContent + '</div>' +
            '<i class="icon-yapo icon-arrow-down-bold"></i>';
          liItem.className = 'ps-default';
          liItem.appendChild(wrapItem);
          ps.$list.append(liItem);
        } else {
          //Set default li parent
          liItem.className = optionData.liClass;
          liItem.style.display = 'none';

          if (optionData.itemHeight) {
            liItem.style.height = optionData.itemHeight + 'px';
          }

          //If no images add div with warning text and ad disabled class
          if (optionData.itemWarning) {
            wrapItem.appendChild(ps.addWarning(optionData));
          }

          if (optionData.imagesCount == 0 && (optionData.itemValue == 'at_combo2' || optionData.itemValue == 'at_combo3' || optionData.itemValue == 'discount_combo1')) {
            liItem.className += ' disabled-not-image';
          }

          wrapItem = ps.loadDefaultLayout(optionData, wrapItem);

          // Set options for li
          if (optionData.itemOptionsTitle || optionData.itemOptionsElements) {
            wrapItem.appendChild(ps.addOptions(optionData));
            liItem.className += ' ps-options';
          }

          //Set data link and custom classes
          if (optionData.itemLink) {
            liItem.className += ' ps-item-customBump';
            liItem.setAttribute('data-link', optionData.itemLink);
          }

          //Add data to li element
          liItem.appendChild(wrapItem);

          ps.$list.append(liItem);
        }

				i++;
			});

      if (ps.$select.is(':disabled')) {
        ps.$list.addClass('ps-disabled');
      }

      ps.setEvents();
      ps.preselect();
 		},

    /*
     * @name initialConfData
     *
     * @description
     *
     * Initial data for option
     *
     * @param {element} option element from select
     *
     * @return {Object} data option
     *
    */
    initialConfData: function(option) {
      var optionConf = {
        optParent: option.parentElement,
        imagesCount: option.parentElement.getAttribute('data-image-count'),
        listID: option.parentElement.getAttribute('data-list_id'),
        liClass: 'ps-item',
        wrapClass: 'ps-wrap',
        descClass: 'ps-desc',
        descTitleClass: 'ps-desc-title',
        nameClass: 'ps-name',
        priceClass: 'ps-price',
        warningClass: 'ps-warning',
        warningTextClass: 'ps-warning-text',
        itemDescTitle: option.getAttribute('data-desc-title'),
        itemPrice: option.getAttribute('data-price'),
        itemDesc: option.getAttribute('data-desc'),
        itemLink: option.getAttribute('data-link'),
        itemValue: option.value,
        itemHeight: option.getAttribute('data-height'),
        itemWarning: option.getAttribute('data-warning'),
        itemOptionsClass: option.getAttribute('data-options_class'),
        itemOptionsTitle: option.getAttribute('data-options_title'),
        itemOptionsElements: option.getAttribute('data-options_elements'),
        itemContent: option.innerHTML
      };

      return optionConf;
    },

    /*
     * @name customClassBump
     *
     * @description
     *
     * add custom class for bump select
     *
     * @param {data} data option and parent
     * @param {Object} ps global object
     *
     * @return {void}
     *
    */
    customClassBump: function (data, ps) {
      if (CartHandler.isProductSelected(CartHandler.BUMP, data.listID)) {
        data.optParent.setAttribute('data-selected', 'bump');
      } else if (CartHandler.isProductSelected(CartHandler.DAILY_BUMP, data.listID)) {
        data.optParent.setAttribute('data-selected', 'dayly_bump');
      } else if (CartHandler.isProductSelected(CartHandler.WEEKLY_BUMP, data.listID)) {
        data.optParent.setAttribute('data-selected', 'weekly_bump');
      } else if (CartHandler.isProductSelected(CartHandler.AUTOBUMP, data.listID)) {
        ps.$list.addClass('autobump');
        data.optParent.setAttribute('data-selected', 'autobump');
      }
    },
    /*
     * @name loadDefaultLayout
     *
     * @description
     *
     * Load default elements Name, Description and Price
     *
     * @param {data} data option and parent
     * @param {elemement} wrapper element of li
     *
     * @return {node} elements default layout
     *
    */
    loadDefaultLayout: function (data, wrapper) {
      var name = document.createElement('div');
      var price = document.createElement('div');
      var description = document.createElement('div');
      var descriptionTitle = document.createElement('div');

      // Set ps-name element
      name.className = data.nameClass;
      name.innerHTML = data.itemContent;
      wrapper.appendChild(name);

      // Set ps-price element
      if (data.itemPrice) {
        price.className = data.priceClass;
        price.innerHTML = data.itemPrice;
        wrapper.appendChild(price);
      }

      // Set ps-description element
      if (data.itemDesc) {
        description.className = data.descClass;
        description.innerHTML = data.itemDesc;

        if (data.itemLink) {
           description.className += ' ps-desc-customDescription';
        }

        wrapper.appendChild(description);
      }

      // this code sucks
      if (data.itemDescTitle) {
        descriptionTitle.className = data.descTitleClass;
        descriptionTitle.innerHTML = data.itemDescTitle;
        wrapper.appendChild(descriptionTitle);
      }

      return wrapper;
    },

    /*
     * @name addOptions
     *
     * @description
     *
     * Add a list options to li
     *
     * @param {Object} data from option
     *
     * @return {node} element with options in ul list
     *
    */
    addOptions: function(data) {
      var optionsClass = (data.itemOptionsClass) ? data.itemOptionsClass: 'ps-options';
      var optionsElements = data.itemOptionsElements.split(',');
      var optionsMainContainer = document.createElement('div');
      var optionsTitle = document.createElement('div');
      var optionsElementsContainer = document.createElement('ul');

      optionsMainContainer.className = optionsClass;
      optionsTitle.className = optionsClass + '-title';
      optionsTitle.innerHTML = data.itemOptionsTitle;
      optionsMainContainer.appendChild(optionsTitle);

      //Set options list
      optionsElementsContainer.className = optionsClass + '-elements';

      optionsElements.map(function (elem, indx) {
        var optionsElement = document.createElement('li');
        optionsElement.className = optionsClass + '-element';
        optionsElement.setAttribute('data-label', indx + 1);
        optionsElement.innerHTML = elem;

        optionsElementsContainer.appendChild(optionsElement);
      });

      optionsMainContainer.appendChild(optionsElementsContainer);

      return optionsMainContainer;
    },

    /*
     * @name addWarning
     *
     * @description
     *
     * Show a warning when the ad has not images and cant use gallery products
     *
     * @param {element} el option element from select
     *
     * @return {node} element with warning text and class
     *
    */
    addWarning: function (data) {
      var warning = document.createElement('div');
      var text = document.createElement('div');
      warning.className = data.warningClass;
      text.className = data.warningTextClass;
      text.innerHTML = data.itemWarning;
      warning.appendChild(text);

      return warning;
    },

    /*
    * @name xitiClick
    *
    * @description
    *
    * External xiti dependency. Not sure how to handle this.
    *
    * @param {string} tag with description element click
    *
    * @return {obejct} xt_click response
    */
    xitiClick: function(tag) {
      if (typeof xt_click === 'function' && tag) { // eslint-disable-line
       return xt_click(this, 'C', '', tag, 'A'); // eslint-disable-line
      }
    },

    setPromoMessage: function(hostElement, value) {
      if(value === 'discount_combo2') {
        var message = '<p class="product-cols-promo-msg">(*) El primero se aplica ahora y el segundo 24 hrs. despu&eacute;s</p>';
        $(hostElement[0]).append(message);
      } else {
        $(hostElement[0]).find('.product-cols-promo-msg').remove();
      }
    },

    setEvents: function() {
      var ps = this;

      ps.$list.find('li.ps-default').click(function(e) {
        e.stopPropagation();

        if (!ps.$list.hasClass('ps-disabled')) {
          ps.toggle();
        }
      });

      ps.$list.find('li.ps-item').click(function(e) {
        var label = $(this).find('.ps-name').html();
        var price = $(this).find('.ps-price').html();
        var value = $(this).data('value');
        var labelCombo = e.target.getAttribute('data-label');
        var validOption = true;
        var tag = '';
        ps.setPromoMessage($(this).parent().parent().parent().parent(), value);
        e.stopPropagation();
        ps.close();

        if ($(this).hasClass('disabled-not-image')) {
          validOption = false;
        }

        // Validate click hover options labels only
        if ($(this).hasClass('ps-options')) {
          if (labelCombo) {
            $(this).parent().siblings('.combo-at-select').attr('data-label_selected', labelCombo);
          } else {
            validOption = false
          }
        }

        if (validOption) {
          if (!$(this).data('link')) {
            ps.setSelected(label, price, value);
          } else {
            window.location.assign($(this).data('link'));
          }

          // Xiti tag only for combos
          if (typeof value != undefined && value.toString().indexOf('at_combo') >= 0) {
            tag = 'payment::combos::1260::' + normalizeWordAccent(label).toLowerCase().toString() + '';
            tag = tag.replace(/\s+/g, '');

            ps.xitiClick(tag);
          }
        }
      });

      $(document).click( function() {
        ps.close();
      });
 		},

    toggle: function() {
      if (this.isOpen()) {
        this.close();
      } else {
        this.open();
      }
    },

    open: function() {
      //Close all others
      $('.product-select').removeClass('open');
      $('.product-select').find('li.ps-item').hide();
      $('.product-select').find('li.ps-default').removeClass('open');
      $('.product-select').find('li.ps-default i').addClass('icon-arrow-down-bold');
      $('.product-select').find('li.ps-default i').removeClass('icon-arrow-up-bold');

      this.$list.addClass('open');
      this.$list.find('li.ps-item').show();
      this.$list.find('li.ps-default').addClass('open');
      this.$list.find('li.ps-default i').removeClass('icon-arrow-down-bold');
      this.$list.find('li.ps-default i').addClass('icon-arrow-up-bold');
    },

    close: function() {
      this.$list.removeClass('open');
      this.$list.find('li.ps-item').hide();
      this.$list.find('li.ps-default').removeClass('open');
      this.$list.find('li.ps-default i').addClass('icon-arrow-down-bold');
      this.$list.find('li.ps-default i').removeClass('icon-arrow-up-bold');
    },

    isOpen: function() {
      return this.$list.find('li.ps-item').is(':visible');
    },

    setSelected: function(label, price, value) {
      this.selectedProductLabel = '<div class="ps-name">' + label + '</div>';
      this.$list.find('li.ps-default').addClass('ps-selected');
      this.$list.find('li.ps-default .ps-label').html(this.selectedProductLabel);
      this.$list.find('li.ps-default i').removeClass('icon-arrow-up-bold');
      this.$list.find('li.ps-default i').removeClass('icon-arrow-down-bold');
      this.$list.find('li.ps-default i').addClass('icon-close-circled');
      this.$select.find('option[value=' + value + ']').prop('selected', true).change();
      this.setCloseEvent();
    },

    setCloseEvent: function() {
      var ps = this;

      this.$list.find('li.ps-default i.icon-close-circled').click(function(e) {
        $(this).parent().parent().parent().parent().parent().parent().find('.product-cols-promo-msg').remove();
        e.stopPropagation();
        ps.reset();
      });


      if (this.options.removeLabel !== undefined) {
        this.$list.find('li.ps-default i.icon-close-circled').mouseover(function(e) {
          ps.$list.find('.ps-selected .ps-label').html(ps.options.removeLabel);
          ps.$list.find('.ps-selected .ps-label').addClass('ps-remove');
        });
        this.$list.find('li.ps-default i.icon-close-circled').mouseout(function(e) {
          ps.$list.find('.ps-selected .ps-label').html(ps.selectedProductLabel);
          ps.$list.find('.ps-selected .ps-label').removeClass('ps-remove');
        });
      }
    },

    reset: function() {
      this.$select.find('option[value=' + this.defaultValue + ']').prop('selected', true).change();
      this.$select.removeData('label_selected');
      this.$list.find('li.ps-default .ps-label').html(this.defaultLabel);
      this.$list.find('li.ps-default i').removeClass('icon-close-circled');
      this.$list.find('li.ps-default i').addClass('icon-arrow-down-bold');
      this.$list.find('li.ps-default').removeClass('ps-selected');
      this.$list.find('.icon-yapo').off('click');
    },

    preselect: function() {
      var value = this.$select.val();

      if (value != this.defaultValue) {
        var label = this.$select.find('option[value=' + value + ']').html();
        var price = this.$select.find('option[value=' + value + ']').data('price');
        var value = this.$select.val();
        this.setSelected(label, price, value);
      }
    }
  }

  $.fn.productselect = function(options) {
    if (typeof options == 'object' || !options) {
      return this.each(function() {
        options = $.extend({}, $.fn.productselect.defaults, options);
        var productselect = new ProductSelect($(this), options);
        productselect.init();
      });
    }
  }

  $.fn.productselect.defaults = {};
})(jQuery);
