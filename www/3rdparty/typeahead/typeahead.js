var Typeahead = (function() {
  // Globals
  var list;
  var data; 
  var onBlur;
  var onNoResults;
  var element;
  var currentObj;
  var getElementCallback;
  var index = -1;
  var match = [];
  var noResults = false;
  var showNoResultsWarning;
  var showAll;

  /**
   * Text suggestion system.
   * @constructor
   * @param {Object}    settings            - Settings for Typeahead to work
   * @param {String}    settings.element    - Element on which to set up 
   * @param {Object[]}  settings.data       - Array containing objects for each
   *                                          searchable element
   * @param {Function}  settings.getElement - Choosing an element callback
   *
   * @example:
   *    new Typeahead({
   *      element: HTMLElement,
   *      data: Object (JSON),
   *      getElement: function(obj) {
   *         // returns object
   *      }
   *    })
   */

  function Typeahead(settings) {
    // Check for a valid element and data
    if (!settings.element || !settings.data) {
      throw new Error('Typeahead can\'t work without an element or data specified.');
    }

    element = settings.element;
    data = settings.data;
    getElementCallback = settings.getElement;
    onBlur = settings.onBlur;
    onNoResults = settings.onNoResults;

    showNoResultsWarning = typeof settings.showNoResultsWarning === 'undefined'
      ? true
      : settings.showNoResultsWarning;

    showAll = settings.showAll || false;

    createList();
    setListeners();
  }

  // Private Methods
  /**
   *  Create an UL list to store results.
   *  @method createList
   */

  function createList() {
    list = document.createElement('ul');
    list.className = 'getContactList-searchList';

    if (showAll) {
      list.className += ' __all';
    }

    element.parentNode.appendChild(list);
  }

  /**
   * Hide list if visible.
   * @method clearFocus
   * @param {Object} e - HTMLElement
   */

  function clearFocus(e) {
    if (e.target.className !== element.className) {
      list.className = list.className.replace(/is-visible/g, '');
    } else {
      if (list.className.indexOf('is-visible') > -1) {
        list.className += ' is-visible';
      }
    }
  }

  /**
   * Set all event listeners at once.
   * @method setListeners
   */
  function setListeners() {
    // Show select when clicking on the input only if something was written
    element.addEventListener('click', function() {
      if (list.className.indexOf('is-visible') === -1) {
        list.className += ' is-visible';
      }
    });

    // Handle user input on main element
    element.addEventListener('keyup', handleKeys);
    element.addEventListener('focus', handleList);

    // When input element loses focus
    element.addEventListener('blur', function(e) {
      clearFocus(e);

      if (currentObj || !element.value.length) {
        return false;
      }

      if (list.className.indexOf('is-visible') > -1) {
        list.className = list.className.replace(/is-visible/g, '');
      }

      if (!list.children.length || !list.children[0].attributes['data-id']) {
        onNoResults && onNoResults();
        return false;
      }

      var currentId = list.children[0].attributes['data-id'].value;
      var value = getCurrentValueById(data, currentId);
      element.value = value;

      currentObj = {
        id: currentId,
        value: value
      };

      if (onBlur) {
        onBlur(currentObj);
      }
    });

    list.addEventListener('click', getElement);

    // When user clicks outside of element
    document.addEventListener('click', clearFocus);
  }

  /**
   * Handle pressed keys.
   * @method handleKeys
   * @params {Object} e - HTMLElement
   */
  function handleKeys(e) {
    var key = e.keyCode;

    if (key === 13) {
      getElement(e);
    } else if (key === 38) {
      handleMovement('up');
    } else if (key === 40) {
      handleMovement('down');
    } else {
      handleList();
    }
  }

  /**
   * Handle arrow keys movement.
   * @method handleMovement
   * @params {string} direction - Receive either 'up' or 'down'
   */
  
  function handleMovement(direction) {
    direction = direction === 'up' ? -1 : 1;

    if ((direction === 1 && index === (match.length - 1)) || 
        (index === 0 && direction === -1)) {
      return false;
    }

    if (match[index]) {
      match[index].className = match[index].className.replace('is-focused');
    }

    // Increase index
    index += direction;

    match[index].className += ' is-focused';
  }

  /**
   * Find and wrap substring in string with a <strong> element.
   * @method increaseFontWeight
   * @params {String} string    - String to be searched
   * @params {String} substring - Substring to wrap in <strong> elements
   */

  function increaseFontWeight(string, substring) {
    var splitted;
    var reg = new RegExp(substring, 'ig');
    var matches = string.match(reg);
    var nextString = string;
    var saved = [];

    if (
      ((matches === null || !substring) && showNoResultsWarning) ||
      (matches === null && !showNoResultsWarning)
    ) {
      return false;
    }

    for (var i = 0; i < matches.length; i++) {
      var current = matches[i];

      var strong = document.createElement('strong');
      strong.textContent = current;

      var code = '0000000000000000';

      splitted = nextString.replace(current, code).split(code);
      splitted.splice(1, 0, strong);

      saved.push(splitted[0]);
      saved.push(splitted[1]);

      if ((matches.length - 1) === i) {
        saved.push(splitted[2]);
      }

      var index = nextString.indexOf(current);
      var subs = nextString.substring(0, index + current.length);
      nextString = nextString.replace(subs, '');
    }

    return saved;
  }

  /**
   * @name getListElement
   * @description
   *
   * Creates a li tag with hightlighted search
   *
   * @param {Array} result - List of letters with the font-weight modifier
   * @param {String} id - Unique identifier for each li tag
   *
   * @returns {HTMLElement}
   */
  function getListElement(result, id) {
    var li = document.createElement('li');
    li.className = 'getContactList-searchListElement';
    li.setAttribute('data-id', id);

    for (var j = 0; j < result.length; j++) {
      var res = result[j];
      if (typeof res === 'string') {
        res = document.createTextNode(res);
      }
      li.appendChild(res);
    }

    return li;
  }

  /**
   * @name getMatchesByObject
   * @description
   *
   * Creates a list with the found results using an object as incoming data.
   *
   * @param {Object} data - List of elements as object
   * @param {String} substring - Text to find coincidences in the list
   *
   * @returns {Array}
   */
  function getMatchesByObject(data, substring) {
    var match = [];

     // Search for substring on each element from data
    for (var id in data) {
      if (match.length > 2 && !showAll) {
        break;
      }

      result = increaseFontWeight(data[id], substring);

      // If no result was found, continue on next iteration
      if (!result) {
        continue;
      }

      match.push(getListElement(result, id));
    }

    return match;
  }

  /**
   * @name getMatchesByArray
   * @description
   *
   * Creates a list with the found results using an array as incoming data.
   *
   * @param {Object} data - List of elements as array
   * @param {String} substring - Text to find coincidences in the list
   *
   * @returns {Array}
   */
  function getMatchesByArray(data, substring) {
    var match = [];
    var li = null;

    data.forEach(function(el) {
      if (match.length > 2 && !showAll) {
        return false;
      }

      result = increaseFontWeight(el.value, substring);

      if (!result) {
        return false;
      }

      match.push(getListElement(result, el.id));
    });

    return match;
  }

  /**
   * Handle list of searches.
   * @method handleList
   */
  function handleList() {
    var result;
    var substring = element.value;

    // Reset noResults boolean
    noResults = false;

    // Clean list
    match.forEach(function(element) {
      element.parentNode.removeChild(element);
    });

    match = [];
    list.innerHTML = '';

    if (Array.isArray(data)) {
      match = getMatchesByArray(data, substring);
    } else {
      match = getMatchesByObject(data, substring);
    }

    if (match.length < 1 || (element.value.length === 0 && showNoResultsWarning)) {
      li = document.createElement('li');
      li.className = 'getContactList-searchListElement';
      li.innerHTML = 'No encontramos avisos activos para tu b&uacute;squeda';

      noResults = true;

      match.push(li);
    }

    if (list.className.indexOf('is-visible') === -1) {
      list.className += ' is-visible';
    }

    match.forEach(function(element) {
      list.appendChild(element);
    });

    // Maybe place a callback here?
  }

  /**
   * @name getCurrentValueById
   * @description
   *
   * Returns element value depending the incoming list type
   *
   * @param {Object|Array} data - List of elements to find the current value
   * @param {String} currentId - Identifier of the element in the list
   *
   * @returns {String}
   */
  function getCurrentValueById(data, currentId) {
    var current;

    if (!Array.isArray(data) && data.hasOwnProperty(currentId)) {
      return data[currentId];
    }

    current = data.filter(function(element) {
      return element.id == currentId;
    })[0];

    return current.value;
  }
  
  /**
   * Return chosen element.
   * @method getElement
   * @param {Object} e - HTMLElement
   */

  function getElement(e) {
    if (noResults || !e.target.attributes.hasOwnProperty('data-id')) {
      return false;
    }

    var currentId = getSomeListElement(e);
    var value = getCurrentValueById(data, currentId);

    currentObj = {
      id: currentId,
      value: value
    };

    // Set current input value to the chosen object
    element.value = value;
    list.className = list.className.replace(/is-visible/g, '');
    getElementCallback(currentObj);
  }

  function getSomeListElement(e) {
    var currentId;
    
    if (typeof e.target.attributes['data-id'] === 'undefined') {
      currentId = match[index].attributes['data-id'].value;
    } else {
      currentId = e.target.attributes['data-id'].value;
    }

    return currentId;
  }

  return Typeahead;
})();

