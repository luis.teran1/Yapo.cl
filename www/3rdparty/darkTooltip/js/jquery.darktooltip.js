/* 
 * DarkTooltip v0.3.1
 * Simple customizable tooltip with confirm option and 3d effects
 * Yapo.cl refactory
 * (c)2014 Rub�n Torres - rubentdlh@gmail.com
 * Released under the MIT license
 */

(function ($) {
  function DarkTooltip(element, options){
    this.bearer = element;
    this.options = options;
    this.hideEvent;
    this.mouseOverMode = ['hover', 'mouseover', 'onmouseover'].indexOf(this.options.trigger) >= 0;
  }

  DarkTooltip.prototype = {
    show: function () {
      var dt = this;
      var hideAll = this.options.hideAll;
      var modal = this.options.modal;

      // Close all tooltips
      if (hideAll) {
        this.hideAll();
      }

      if (modal) {
          this.modalLayer.css('display', 'block');
      }

      this.tooltip.css('display', 'block');
        
      // Set event to prevent tooltip from closig when mouse is over the tooltip
      if (dt.mouseOverMode) {
        this.tooltip.mouseover(function () {
          clearTimeout(dt.hideEvent);
        });
        this.tooltip.mouseout(function () {
          clearTimeout(dt.hideEvent);
          dt.hide();
        });
      }
    },

    hide: function () {
      var dt = this;
      var closable = dt.options.closable;
      var modal = dt.options.modal;

      if (!closable) {
        return;
      }

      this.hideEvent = setTimeout(function () {
        dt.tooltip.hide();
      }, 100);
        
      if (modal) {
        dt.modalLayer.hide();
      }

      this.options.onClose();
    },

    hideAll: function () {
      var tooltipSelector = '.' + this.options.boxClassName;
      var modalLayerSelector = '.' + this.options.layerClassName;

      $(tooltipSelector).hide();
      $(modalLayerSelector).hide();
    },

    toggle: function () {
      if (this.tooltip.is(':visible')) {
        this.hide();
      } else {
        this.show();
      }
    },

    addAnimation: function () {
      var animation = this.options.animation;

      switch (animation) {
        case 'none':
          break;
        case 'fadeIn':
          this.tooltip.addClass('animated');
          this.tooltip.addClass('fadeIn');
            break;
        case 'flipIn':
          this.tooltip.addClass('animated');
          this.tooltip.addClass('flipIn');
          break;
      }
    },

    setContent: function () {
      $(this.bearer).css('cursor', 'pointer');

      var content = this.options.content;
      var deleteContent = this.options.delete_content;
        
      // Get tooltip content
      if (content) {
        this.content = content;
      } else if (this.bearer.attr('data-tooltip')) {
        this.content = this.bearer.attr('data-tooltip');
      } else {
        // console.log("No content for tooltip: " + this.bearer.selector);
        return;
      }

      if (this.content.charAt(0) === '#') {
        if (deleteContent) {
          content = $(this.content).html();
          $(this.content).html('');
          this.content = content;
          delete content;
        } else {
          $(this.content).hide();
          this.content = $(this.content).html();
        }

        this.contentType = 'html';
      } else {
        this.contentType = 'text';
      }

      var data = this.options.data;
      var bearerId = this.bearer.attr('id') || this.bearer.data(data);
      var tooltipId = '';
      var layerClassName = this.options.layerClassName;
      var boxClassName = this.options.boxClassName;
      var theme = this.options.theme;
      var size = this.options.size;
      var gravity = this.options.gravity;
      var opacity = this.options.opacity;
      var confirm = this.options.confirm;
        
      if (bearerId !== undefined) {
        tooltipId = 'id="darktooltip-' + bearerId + '"';
      }

      // Create modal layer
      this.modalLayer = $('<ins class="' + layerClassName + '"></ins>');
      // Create tooltip container
      this.tooltip = $('<ins ' + tooltipId + ' class="' + boxClassName + ' ' + theme + ' ' + size + ' ' 
        + gravity + '"><div>' + this.content + '</div><div class="tip"></div></ins>');
      this.tip = this.tooltip.find('.tip');

      $('body').append(this.modalLayer);
      $('body').append(this.tooltip);

      // Adjust size for html tooltip
      if (this.contentType === 'html') {
        this.tooltip.css('max-width', 'none');
      }

      this.tooltip.css('opacity', opacity);
      this.addAnimation();

      if (confirm) {
        this.addConfirm();
      }
    },

    setPositions: function () {
      var leftPos = this.bearer.offset().left;
      var topPos = this.bearer.offset().top;
      var autoLeft = this.options.autoLeft;
      var autoTop = this.options.autoTop;
      var gravity = this.options.gravity;

      switch (gravity) {
        case 'south':
          leftPos += this.bearer.outerWidth() / 2 - this.tooltip.outerWidth() / 2;
          topPos += -this.tooltip.outerHeight() - this.tip.outerHeight() / 2;
          break;
        case 'west':
          leftPos += this.bearer.outerWidth() + this.tip.outerWidth() / 2;
          topPos += this.bearer.outerHeight() / 2 - (this.tooltip.outerHeight() / 2);
          break;
        case 'north':
          leftPos += this.bearer.outerWidth() / 2 - (this.tooltip.outerWidth() / 2);
          topPos += this.bearer.outerHeight() + this.tip.outerHeight() / 2;
          break;
        case 'east':
          leftPos += -this.tooltip.outerWidth() - this.tip.outerWidth() / 2;
          topPos += this.bearer.outerHeight() / 2 - this.tooltip.outerHeight() / 2;
          break;
      }

      if (autoLeft) {
        this.tooltip.css('left', leftPos);
      }

      if (autoTop) {
        this.tooltip.css('top', topPos);
      }
    },

    setEvents: function () {
      var dt = this;
      var delay = dt.options.hoverDelay;
      var setTimeoutConst;
      var trigger = this.options.trigger;

      if (dt.mouseOverMode) {
        this.bearer.mouseover(function () {
          // Timeout for hover mouse delay
          setTimeoutConst = setTimeout(function () {
            dt.setPositions();
            dt.show();
          }, delay);
        }).mouseout(function () {
          clearTimeout(setTimeoutConst);
          dt.hide();
        });
      } else if (trigger === 'click' || trigger === 'onclik') {
        this.tooltip.click(function (e) {
          e.stopPropagation();
        });

        this.bearer.click(function (e) {
          e.preventDefault();
          dt.setPositions();
          dt.toggle();
          e.stopPropagation();
        });

        $('html').click(function () {
          dt.hide();
        })
      }
    },

    activate: function () {
      this.setContent();
      
      if (this.content) {
        this.setEvents();
      }
    },

    addConfirm: function () {
      var no = this.options.no;

      this.tooltip.append('<ul class="confirm"><li class="darktooltip-yes">' 
        + this.options.yes + '</li><li class="darktooltip-no">' + no + '</li></ul>');
      this.setConfirmEvents();
    },

    setConfirmEvents: function () {
      var dt = this;

      this.tooltip.find('li.darktooltip-yes').click(function (e) {
        dt.onYes();
        e.stopPropagation();
      });

      this.tooltip.find('li.darktooltip-no').click(function (e) {
        dt.onNo();
        e.stopPropagation();
      });
    },

    finalMessage: function () {
      var finalMessage = this.options.finalMessage;

      if (finalMessage) {
        var dt = this;
        var finalMessageDuration = dt.options.finalMessageDuration;

        dt.tooltip.find('div:first').html(finalMessage);
        dt.tooltip.find('ul').remove();
        dt.setPositions();

        setTimeout(function () {
          dt.hide();
          dt.setContent();
        }, finalMessageDuration);
      } else {
        this.hide();
      }
    },

    onYes: function () {
      this.options.onYes(this.bearer);
      this.finalMessage();
    },

    onNo: function () {
      this.options.onNo(this.bearer);
      this.hide();
    }
  }

  $.fn.darkTooltip = function (options) {
    return this.each(function () {
      options = $.extend({}, $.fn.darkTooltip.defaults, options);
      var tooltip = new DarkTooltip($(this), options);
      tooltip.activate();
    });
  }

  $.fn.darkTooltip.defaults = {
    animation: 'none',
    confirm: false,
    content:'',
    finalMessage: '',
    finalMessageDuration: 1000,
    gravity: 'south',
    hoverDelay: 0,
    modal: false,
    no: 'No',
    closable: true,
    onNo: function () {},
    onYes: function () {},
    opacity: 0.9,
    size: 'medium',
    theme: 'dark',
    trigger: 'hover',
    yes: 'Yes',
    autoTop: true,
    autoLeft: true,
    hideAll: false,
    data: '',
    boxClassName: 'dark-tooltip',
    layerClassName: 'darktooltip-modal-layer',
    onClose: function () {}
  };
})(jQuery);
