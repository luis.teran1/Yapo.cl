(function() {
  'use strict';
  var WRAPPER_CLASS = 'mdTextField-wrapper';
  var INPUT_CLASS = 'mdTextField-input';
  var TEXTAREA_CLASS = 'mdTextField-textarea';
  var LABEL_CLASS = 'mdTextField-label';
  var COUNTER_CLASS = 'mdTextField-counter';
  var DIRTY_CLASS = 'mdTextField-input-dirty';

  $.fn.mdTextField = function mdTextField(options) {
    var inputs = $(this);

    var config = options || {};
    $.extend(config, {
      dirtyClass: DIRTY_CLASS,
      wrap: true,
      focus: true,
      keyup: true
    });

    inputs.each(function onEachInput() {
      var input = $(this);
      var ignoreTypes = ['hidden', 'checkbox', 'radio', 'button'];
      var type = input.attr('type');
      var val = input.val();
      var value = input.attr('value');

      if (ignoreTypes.indexOf(type) !== -1) {
        return true;
      }

      addId(input);
      input.addClass(INPUT_CLASS);

      if (input[0].nodeName === 'TEXTAREA') {
        input.addClass(TEXTAREA_CLASS);
      }

      input.on('blur focus change input paste', moveLabel);
      input.closest('form').on('reset', function onResetForm() {
        setTimeout(function triggerBlur() {
          input.trigger('blur');
        }, 1);
      });

      if (config.wrap) {
        wrapInput(input);
      }

      if (config.counter) {
        addCounter(input);
      }

      addLabel(input);

      if (value && val !== value) {
        input.val(value);
      }

      if (input.val()) {
        input.trigger('blur');
      }
    });


    function addLabel(input) {
      var id = input.attr('id');
      var label = $('label[for=' + id + ']');

      if (!label.length) {
        label = $('<label/>', { 'for': id });
      }

      label.text(input.attr('placeholder'));
      label.addClass(LABEL_CLASS);
      input.removeAttr('placeholder');
      input.before(label);
    }


    function addId(input) {
      // @TODO: create a unique ID
      var id = input.attr('id');

      if (id) {
        return id;
      }

      id = input.attr('name');
      input.attr('id', id);

      return id;
    }


    function addCounter(input) {
      var span = $('<span/>', { class: COUNTER_CLASS });
      span.text(getCounterText(input));
      input.after(span);
      input.on('keyup change input paste', function updateCounter() {
        span.text(getCounterText(input));
      });
    }


    function wrapInput(input) {
      var div = $('<div/>', { class: WRAPPER_CLASS });
      input.wrap(div);
    }


    function moveLabel(event) {
      var fn = event.type === 'blur' && !event.target.value ?
        'removeClass' : 'addClass';
      $('label[for=' + event.target.id  + ']')[fn](config.dirtyClass);
    }


    function getCounterText(input) {
      var count = parseInt(input.attr('maxlength'), 10);
      var len = input.val().replace(/\r(?!\n)|\n(?!\r)/g, '\r\n').length;

      if (len) {
        count -= len;
      }

      return count;
    }
  };

})();
