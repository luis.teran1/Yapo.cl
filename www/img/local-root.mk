TOPDIR?=../..

INSTALLDIR=/www-ssl

ifeq (${BUILD_STAGE},REGRESS)
INSTALL_TARGETS+=favicon.ico
endif

INSTALL_TARGETS+=apple-touch-icon.png
INSTALL_TARGETS+=apple-touch-icon-precomposed.png
INSTALL_TARGETS+=apple-touch-icon-114x114-precomposed.png
INSTALL_TARGETS+=apple-touch-icon-114x114.png
INSTALL_TARGETS+=apple-touch-icon-57x57-precomposed.png
INSTALL_TARGETS+=apple-touch-icon-57x57.png
INSTALL_TARGETS+=apple-touch-icon-72x72-precomposed.png
INSTALL_TARGETS+=apple-touch-icon-72x72.png
INSTALL_TARGETS+=apple-touch-icon-144x144-precomposed.png
INSTALL_TARGETS+=apple-touch-icon-144x144.png

include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
