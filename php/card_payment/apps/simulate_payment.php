<?php
/*
 * Card payment application
 */
require_once('blocket_application.php');
require_once('dyn_config.php');
require_once('bTransaction.php');

openlog("simulate_simulate_card_payment", LOG_NDELAY, LOG_LOCAL0);

class payex_filter extends blocket_filter {
	function f_order_id($val) {
		if (preg_match('/^[0-9a]+$/', $val))
			return $val;
		return null;
	}

	function f_reference_id($val) {
		if (preg_match('/^[0-9]+$/', $val))
			return $val;
		return null;
	}

	function f_signature($val) {
		if (preg_match('/^[A-Za-z0-9]+$/', $val))
			return $val;
		return null;
	}
	
	function f_dummy($val) {
		return $val;
	}
}

class payex_form_data extends payex_filter {
	var $CF__VERSION = 'f_integer';
	var $CF__MERCHANTID = 'f_integer';
	var $CF__ORDERID = 'f_order_id';
	var $CF__EMAIL = 'f_dummy';
	var $CF__RURL = 'f_dummy';
	var $CF__SURL = 'f_dummy';
	var $CF__FURL = 'f_dummy';
	var $CF__CURL = 'f_dummy';
	var $CF__LANG = 'f_dummy';
	var $CF__HASHALG = 'f_dummy';
	var $CF__AMOUNT = 'f_dummy';
	var $CF__CURRENCY = 'f_dummy';
	var $CF__PT = 'f_dummy';
	var $CF__SHIPPINGRATE = 'f_dummy';
	var $CF__SHIPPINGVAT = 'f_dummy';
	var $CF__VATFLAG = 'f_dummy';
	var $CF__DEPOSITFLAG = 'f_dummy';
	var $CF__DELIVERY = 'f_dummy';
	var $CF__RESPONSEFLAG = 'f_dummy';
	var $CF__ORDERDESCRIPTION = 'f_dummy';
	var $CF__SIGNATURE = 'f_dummy';
	var $CF__BNAME = 'f_dummy';
	var $CF__BADDRESS1 = 'f_dummy';
	var $CF__BADDRESS2 = 'f_dummy';
	var $CF__BEMAIL = 'f_dummy';
	var $CF__BPOSTCODE = 'f_dummy';
	var $CF__BCITY = 'f_dummy';
	var $CF__BCOUNTRY = 'f_dummy';
	var $CF__BSTATE = 'f_dummy';
	var $CF__BPHONE = 'f_dummy';
	var $CF__BMOBILE = 'f_dummy';
	var $CF__IQTY = 'f_dummy';
	var $CF__INAME = 'f_dummy';
	var $CF__ISKU = 'f_dummy';
	var $CF__IPRICE = 'f_dummy';
	var $CF__IVAT = 'f_dummy';
}

class payex_post_data extends payex_filter {
	var $PAN = 'f_dummy';
	var $CF__MERCHANTNUMBER = 'f_dummy';
	var $CF__ORDERNUMBER = 'f_dummy';
	var $CF__REFERENCENUMBER = 'f_dummy';
	var $CF__AMOUNT = 'f_dummy';
	var $CF__CURRENCY = 'f_dummy';
	var $cancelButton = 'f_dummy';
	var $CF__RURL = 'f_dummy';
	var $CF__SURL = 'f_dummy';
	var $CF__FURL = 'f_dummy';
	var $CF__CURL = 'f_dummy';
}

class simulate_card_payment extends blocket_application {
	var $merchant_id;
	var $order_id;
	var $reference_id;
	var $amount;
	var $currency;
	var $status;
	var $signature;

	function simulate_card_payment($init = true) {
		global $BCONF;

		$this->page_title = lang('PAGE_CARD_PAYMENT_TITLE');
		$this->page_name = lang('PAGE_CARD_PAYMENT_NAME');
		$this->store_data = array();

		$this->init('main', 0);
	}

	function get_state($statename) {
		switch ($statename) {

		case 'main':
			return array('function' => 'simulate_card_payment_main',
				     'params' => array('data' => 'payex_form_data'),
				     'method' => 'post');
		case 'form':	
			return array('function' => 'simulate_card_payment_form',
				     'method' => 'get');
		case 'submit':	
			return array('function' => 'simulate_card_payment_submit',
				     'params' => array('data' => 'payex_post_data'),
				     'method' => 'post');
		}
	}

	function simulate_card_payment_main($data) {
		
		global $BCONF;
		global $HTTP_RAW_POST_DATA;
		
		if ($this->validate_signature($data->CF__MERCHANTID, $data->CF__ORDERID, $data->CF__EMAIL, $data->CF__RURL, $data->CF__SURL, $data->CF__FURL, $data->CF__CURL, $data->CF__AMOUNT, $data->CF__CURRENCY, $data->CF__SIGNATURE)) {
			$this->merchant_id = $data->CF__MERCHANTID;
			$this->order_id = $data->CF__ORDERID;
			$this->reference_id = time();
			$this->amount = $data->CF__AMOUNT;
			$this->currency = $data->CF__CURRENCY;
			$this->rurl = $data->CF__RURL;
			$this->surl = $data->CF__SURL;
			$this->furl = $data->CF__FURL;
			$this->curl = $data->CF__CURL;
		} else {
			header('Location:' . $data->CF__FURL . '?merchantid=' . $data->CF__MERCHANTID . '&orderid=' . $data->CF__ORDERID);
			return 'FINISH';
		}

		$amount = null;
		$price = 0;
		/* Verify that iprice + ivat sum matches amount */
		foreach (explode('&', $HTTP_RAW_POST_DATA) as $post) {
			list ($key, $val) = explode('=', $post, 2);

			if ($key == 'CF__AMOUNT')
				$amount = intval($val);
			elseif (in_array ($key, array ('CF__IPRICE', 'CF__IVAT')))
				$price += intval($val);
		}
		if ($price !== $amount) {
			syslog(LOG_CRIT, "price $price != amount $amount");
			header('Location:' . $data->CF__FURL . '?merchantid=' . $data->CF__MERCHANTID . '&orderid=' . $data->CF__ORDERID);
		}

		return 'form';
	}
	
	function simulate_card_payment_form() {
		$this->display_layout('card_payment/simulate_card_payment_form.html');
	}	

	function simulate_card_payment_submit($data) {
		global $BCONF;
		
		$this->furl = $data->CF__FURL;
		$this->merchant_id = $data->CF__MERCHANTNUMBER;
		$this->order_id = $data->CF__ORDERNUMBER;

		if (!empty($data->cancelButton)) {
			$this->status = 300;	
			$this->signature = $this->create_signature($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY);
			$this->post_rurl($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY, $data->CF__RURL);
			header('Location:' . $data->CF__CURL . '?merchantid=' . $data->CF__MERCHANTNUMBER . '&orderid=' . $data->CF__ORDERNUMBER);
			return 'FINISH';
		} else if ($data->PAN == '4111111111111111') {
			$this->status = 200;	
			$this->signature = $this->create_signature($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY);
			$this->post_rurl($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY, $data->CF__RURL);
			header('Location:' . $data->CF__SURL . '?merchantid=' . $data->CF__MERCHANTNUMBER . '&orderid=' . $data->CF__ORDERNUMBER . '&status=' . $this->status);
			return 'FINISH';
		} else if ($data->PAN == '4010000000000000') {
			$this->status = 201;	
			$this->signature = $this->create_signature($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY);
			$this->post_rurl($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY, $data->CF__RURL);
			header('Location:' . $data->CF__SURL . '?merchantid=' . $data->CF__MERCHANTNUMBER . '&orderid=' . $data->CF__ORDERNUMBER . '&status=' . $this->status);
			return 'FINISH';

		} else if (substr($data->PAN,0,3) == '444') {
			$this->status = substr($data->PAN, -1, 3);
			$this->signature = $this->create_signature($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY);
			$this->post_rurl($data->CF__MERCHANTNUMBER, $data->CF__ORDERNUMBER, $data->CF__REFERENCENUMBER, $data->CF__AMOUNT, $data->CF__CURRENCY, $data->CF__RURL);
			header('Location:' . $data->CF__SURL . '?merchantid=' . $data->CF__MERCHANTNUMBER . '&orderid=' . $data->CF__ORDERNUMBER . '&status=' . $this->status);
			return 'FINISH';

		}

		header('Location:' . $data->CF__FURL . '?merchantid=' . $data->CF__MERCHANTNUMBER . '&orderid=' . $data->CF__ORDERNUMBER);
		return 'FINISH';

	}	
	
	function post_rurl($CF__MERCHANTNUMBER, $CF__ORDERNUMBER, $CF__REFERENCENUMBER, $CF__AMOUNT, $CF__CURRENCY, $CF__RURL) {
		global $BCONF;

		preg_match("/^http:\/\/([^\/]*)(.*)$/", $CF__RURL, $res);
		list($host, $port) = split(':', $res[1]);
		$uri = $res[2];
		
		$ReqBody = "CF__MERCHANTNUMBER=$CF__MERCHANTNUMBER&CF__ORDERNUMBER=$CF__ORDERNUMBER&CF__REFERENCENUMBER=$CF__REFERENCENUMBER&CF__AMOUNT=$CF__AMOUNT&CF__CURRENCY=$CF__CURRENCY&CF__STATUS={$this->status}&CF__SIGNATURE={$this->signature}";
		$ContentLength = strlen($ReqBody);

		$ReqHeader =
			"POST $uri HTTP/1.1\n".
			"Host: $host:$port\n".
			"Content-Type: application/x-www-form-urlencoded\n".
			"Content-Length: $ContentLength\n\n".
			"$ReqBody\n";

		$socket = fsockopen($host, $port, $errno, $errstr);
		if (!$socket) {
			$Result['status'] = 0;
			$Result["errno"] = $errno;
			$Result["errstr"] = $errstr;

			syslog(LOG_ERR, "no socket: {$host} - {$port} - {$errstr}");

			return $Result;
		}
		$idx = 0;
		fputs($socket, $ReqHeader);
		$Result['status_line'] = fgets($socket);
		$Result['status'] = substr($Result['status_line'], strpos($Result['status_line'], ' ') + 1, 3);
		$in_headers = 1;
		while (!feof($socket)) {
			$line = fgets($socket);
			if ($in_headers) {
				if ($line == "\r\n")
					$in_headers = 0;
				else {
					list ($key, $val) = explode(': ', $line, 2);
					$Result[$key] = substr($val, 0, -2);
				}
			} else
				$Result['body'] .= $line;
		}
		if ($Result['status'] != 200) {
			error_log("Failed to clear: " . serialize($Result));
			header('Location:' . $this->furl . '?merchantid=' . $this->merchant_id . '&orderid=' . $this->order_id . '&status=501');
			exit(1);
		}
		return $Result;
	}

	function validate_signature($merchantid, $orderid, $email, $rurl, $surl, $furl, $curl, $amount, $currency, $signature) {
		global $BCONF;

		$string = $merchantid . "&" . 
			$orderid . "&" .
			$email . "&" .
			$rurl . "&" .
			$surl . "&" .
			$furl . "&" .
			$curl . "&" .
			$amount . "&" .
			$currency . "&" .
			bconf_get($BCONF, "*.common.payex.merchantkey");

		return (sha1($string) === $signature); 
	}
	
	function create_signature($CF__MERCHANTNUMBER, $CF__ORDERNUMBER, $CF__REFERENCENUMBER, $CF__AMOUNT, $CF__CURRENCY) {
		global $BCONF;

		$string = $CF__MERCHANTNUMBER . "&" . 
			$CF__ORDERNUMBER . "&" .
			$CF__REFERENCENUMBER . "&" .
			$CF__AMOUNT . "&" .
			$CF__CURRENCY . "&" .
			$this->status . "&" .
			bconf_get($BCONF, "*.common.payex.merchantkey");

		return sha1($string); 
	}

	function validate_merchant_id() {
		global $BCONF;

		return $this->merchant_id == bconf_get($BCONF, "*.common.payex.merchantid");
	}

	function validate_currency() {
		global $BCONF;

		return $this->currency == bconf_get($BCONF, "*.common.payex.currency");
	}

}

$app = new simulate_card_payment();
$app->run_fsm();
?>
