<?php
/*
 * Card payment application
 */
require_once('blocket_application.php');
require_once('dyn_config.php');
require_once('bTransaction.php');

openlog("card_payment", LOG_NDELAY, LOG_LOCAL0);

syslog(LOG_INFO, $_SERVER['REMOTE_ADDR'] . 'post: ' . serialize($_POST));

class card_payment extends blocket_application {
	var $merchant_id;
	var $order_id;
	var $reference_id;
	var $amount;
	var $currency;
	var $status;
	var $signature;

	function card_payment($init = true) {
		global $BCONF;

		if (!$init)
			return;

		$this->init('main', 0);
	}

	function get_state($statename) {
		switch ($statename) {

		case 'main':
			return array('function' => 'card_payment_main',
				     'params' => array('CF__MERCHANT' => 'f_integer', 'CF__MERCHANTNUMBER' => 'f_integer', 'CF__ORDERNUMBER' => 'f_order_id', 'CF__REFERENCENUMBER' => 'f_reference_id', 'CF__AMOUNT' => 'f_integer', 'CF__CURRENCY' => 'f_integer', 'CF__STATUS' => 'f_integer', 'CF__SIGNATURE' => 'f_signature'),
				     'method' => 'post');
		}
	}

	function card_payment_main($CF__MERCHANT, $CF__MERCHANTNUMBER, $CF__ORDERNUMBER, $CF__REFERENCENUMBER, $CF__AMOUNT, $CF__CURRENCY, $CF__STATUS, $CF__SIGNATURE) {
		global $BCONF;

		if (($ra = bconf_get($BCONF, "card_payment.payex.remote_addr"))) {
			$allowed = false;
			foreach ($ra as $ip) {
				if ($_SERVER['REMOTE_ADDR'] == $ip) {
					$allowed = true;
					break;
				}
			}
			if (!$allowed) {
				syslog(LOG_CRIT, "(CRIT) Access from {$_SERVER['REMOTE_ADDR']} not allowed");
				header('HTTP/1.0 500 Internal Error');
				return;
			}
		}

		if ($CF__MERCHANTNUMBER)
			$this->merchant_id = $CF__MERCHANTNUMBER;
		else
			$this->merchant_id = $CF__MERCHANT;
		$this->order_id = $CF__ORDERNUMBER;
		$this->reference_id = $CF__REFERENCENUMBER;
		$this->amount = $CF__AMOUNT;
		$this->currency = $CF__CURRENCY;
		$this->status = $CF__STATUS;
		$this->signature = $CF__SIGNATURE;

		$status = bconf_get($BCONF, "card_payment.payex.status.$CF__STATUS");
		if(!$this->validate_signature())
			$status = 'ERR_SIGN_RECEIPT';
		elseif (!$this->validate_merchant_id())
			$status = 'ERR_MERCHANT_ID';
		elseif (!$this->validate_currency())
			$status = 'ERR_CURRENCY';
		if (!$status) {
			$status = bconf_get($BCONF, "card_payment.payex.status." . substr($CF_STATUS, 0, 1), "xx");
			if (!$status)
				$status = 'ERR_PAYEX_TECH';
		}

		if ($status == 'ERR_DUP_ORDER') {
			syslog(LOG_INFO, "(INFO) {$_SERVER['REMOTE_ADDR']} - Clear returned status {$reply['status']} for order_id:$CF__ORDERNUMBER reference:$CF__REFERENCENUMBER amount:$CF__AMOUNT status:{$CF__STATUS}:$status");
			header('HTTP/1.0 500 Internal Error');
		} else {
			$transaction = new bTransaction();
			$transaction->add_client_info();

			$transaction->add_data('order_id', $CF__ORDERNUMBER);
			$transaction->add_data('ref_type', 'payex');
			$transaction->add_data('reference', $CF__REFERENCENUMBER);
			$transaction->add_data('amount', $CF__AMOUNT / 100);
			$transaction->add_data('status', $status);
			$reply = $transaction->send_command('clear');

			if (!isset($reply['status'])) {
				$reply['status'] = 'TRANS_FATAL';
				syslog(LOG_CRIT, "(CRIT) {$_SERVER['REMOTE_ADDR']} - Did not get status from transaction server for order_id:$CF__ORDERNUMBER reference:$CF__REFERENCENUMBER amount:$CF__AMOUNT status:{$CF__STATUS}:$status");
			}
			if ($reply['status'] != 'TRANS_OK') {
				syslog(LOG_CRIT, "(CRIT) {$_SERVER['REMOTE_ADDR']} - Clear returned status {$reply['status']} for order_id:$CF__ORDERNUMBER reference:$CF__REFERENCENUMBER amount:$CF__AMOUNT status:{$CF__STATUS}:$status");

				/* Send user to furl instead of surl. */
				header('HTTP/1.0 500 Internal Error');
			}
		}
	}

	function validate_signature() {
		global $BCONF;

		$string = $this->merchant_id . "&" .
			$this->order_id . "&" .
			$this->reference_id . "&" .
			$this->amount . "&" .
			$this->currency . "&" .
			$this->status . "&" .
			bconf_get($BCONF, "*.common.payex.merchantkey");

		return (sha1($string) === $this->signature);
	}

	function validate_merchant_id() {
		global $BCONF;

		return $this->merchant_id == bconf_get($BCONF, "*.common.payex.merchantid");
	}

	function validate_currency() {
		global $BCONF;

		return $this->currency == bconf_get($BCONF, "*.common.payex.currency");
	}

	function f_order_id($val) {
		if (preg_match('/^[0-9a]+$/', $val))
			return $val;
		return null;
	}

	function f_reference_id($val) {
		if (preg_match('/^[0-9]+$/', $val))
			return $val;
		return null;
	}

	function f_signature($val) {
		if (preg_match('/^[A-Za-z0-9]+$/', $val))
			return $val;
		return null;
	}
}

$app = new card_payment();
$app->run_fsm();
?>
