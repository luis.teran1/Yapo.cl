<?php
require_once('blocket_application.php');
require_once('bTransaction.php');

openlog("BUMP", LOG_ODELAY, LOG_LOCAL0);

class blocket_bump extends blocket_application {
	function blocket_bump() {
		$this->init('bump', 1, true);
	}

	function get_state($statename) {
		switch($statename) {
		case 'bump':
			return array('function' => 'bump_bump',
				'params' => array('id'),
				'method' => 'get');
		}
	}

	function bump_bump($id) {
		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $id);
		$reply = $transaction->send_admin_command('bump_ad');
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
		}
		$data['ad_id'] = $id;
		$this->display_layout('bump/bump.html', $data);

		return 'FINISH';
	}

	function f_id($id) {
		return (int)$id;
	}
}

$bump = new blocket_bump();
$bump->run_fsm();
?>
