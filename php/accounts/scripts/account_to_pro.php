<?php

/*

This script detects pro users, changes it to pro and sends an e-mail to the user

*/

require_once('autoload_lib.php');
require_once('init.php');
require_once('JSON.php');

global $BCONF;
$key = 'accounts_to_check';
$proType = 'batch';
$redisConfGen = bconf_get($BCONF, "*.common.redis_accounts_db1");
$redisConfCon = $redisConfGen['host']['rad1'];
$redisConfdb  = 2;
$redis = new Redis();
$redis->connect($redisConfCon['name'], $redisConfCon['port'], $redisConfCon['timeout']);
$redis->select($redisConfdb);

$i = 0;
$number = 5000;
$value = $redis->lPop($key);

while ($i < $number && $value) {
	$json_data = yapo_json_decode($value);
	$email = $json_data->email;
	$ad_id = $json_data->ad_id;

	echo log_string()."processing email: ${email}, ad_id: ${ad_id}" . PHP_EOL;

	$trans = new bTransaction();
	$trans->add_data('log_string', log_string());
	$trans->add_data('ad_id', $ad_id);
	$trans->add_data('pro_type', $proType);
	$result = $trans->send_command('account_to_pro');

	if ($trans->has_error(true)) {
		syslog(LOG_ERR, log_string()."Error when trying to change ${email} to pro");
	}
        echo log_string()."email: ${email}, is pro: ${result['is_pro']}, just_became_pro: ${result['just_became_pro']}" . PHP_EOL;
        if ($result['is_pro'] == 't' && $result['just_became_pro'] == 't') {
                $i++;
        }
	$value = $redis->lPop($key);
}
$redis->close();
