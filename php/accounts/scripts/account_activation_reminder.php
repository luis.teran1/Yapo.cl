<?php

require_once('autoload_lib.php');
require_once('init.php');
require_once('AccountUtil.php');

openlog("accounts_reminder_email", LOG_ODELAY, LOG_LOCAL0);

$options = getopt("t:");

// just in case some idiot do something wrong
if ($options['t'] == 3) {
	exit();
}

$trans = new bTransaction();
$trans->add_data("type", $options['t']);
$result = $trans->send_command('get_pending_account');

if ($trans->has_error(true)) {
	exit();
}

if (!isset($result['email'])) {
	syslog(LOG_INFO, log_string()."no new accounts were found");
	exit();
}

if (!is_array($result['email'])) {
	$result['email'] = array($result['email']);
	$result['name'] = array($result['name']);
}

foreach ($result['email'] as $index => $email) {
	$account_util = new AccountUtil();
	$name = $result['name'][$index];
	$account_mail = $account_util->account_mail($email, $name, 'activate_account');
	if ($account_mail['status'] == "TRANS_OK" ) {
		syslog(LOG_INFO, log_string()."email sent to: ". $email);
	} else {
		syslog(LOG_ERROR, log_string()."there was an error when sending the email to: ". $email);
	}
}

?>
