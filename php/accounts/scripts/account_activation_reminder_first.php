<?php

require_once('autoload_lib.php');
require_once('init.php');
require_once('AccountUtil.php');
require_once('JSON.php');

openlog("accounts_reminder_email", LOG_ODELAY, LOG_LOCAL0);

$options = getopt("t:");

if (in_array($options['t'], array(1,2))) {
	die();
}

$key = 'pending_confirmation_list';
$key2 = 'done';
$redisConfGen = bconf_get($BCONF, "*.common.redis_accounts_db1");
$redisConfCon = $redisConfGen['host']['rad1'];
$redisConfdb  = 2;
$local_redis = new Redis();
$local_redis->connect($redisConfCon['name'], $redisConfCon['port'], $redisConfCon['timeout']);
// if the number of accounts is multiple of 5000, you will need to ad another e-mail because the data will be loaded twice or more
$number = 5000;
$local_redis->select($redisConfdb);

// check the key to know if we already send the e-mails
if ($local_redis->exists($key2)) {
	$local_redis->close();
	exit();
}

// send e-mails to 5000 or less
if($local_redis->exists($key)){
	$result = array('email' => array(), 'name' => array());
	$i = 0;
	while ($i < $number) {
		$value = $local_redis->lPop($key);
		if ($value) {
			$json_data = yapo_json_decode($value);
			$result['email'][$i] = $json_data->email;
			$result['name'][$i] = $json_data->name;
			$i++;
		} else {
			// if we are done, we set a flag to know that we finish
			$local_redis->set($key2, 'YOLO');
			break;
		}
	}
} else {
	// loading data in redis for next call
	$trans = new bTransaction();
	$trans->add_data("type", $options['t']);
	$result = $trans->send_command('get_pending_account');

	foreach ($result['email'] as $index => $email) {
		$name = $result['name'][$index];
		$value = yapo_json_encode(array('email' => $email, 'name' => $name));
		$local_redis->lPush($key, $value);
	}
	$result = array('email' => array(), 'name' => array());
}
$local_redis->close();

if (!is_array($result['email'])) {
	$result['email'] = array($result['email']);
	$result['name'] = array($result['name']);
}

foreach ($result['email'] as $index => $email) {
	$account_util = new AccountUtil();
	$name = $result['name'][$index];
	$account_mail = $account_util->account_mail($email, $name, 'activate_account');
	if ($account_mail['status'] == "TRANS_OK" ) {
		syslog(LOG_INFO, log_string()."email sent to: ". $email);
	} else {
		syslog(LOG_ERROR, log_string()."there was an error when sending the email to: ". $email);
	}
}

?>
