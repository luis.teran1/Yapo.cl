<?php

require_once('autoload_lib.php');
require_once('init.php');
require_once('JSON.php');

openlog("account_to_pro", LOG_ODELAY, LOG_LOCAL0);

global $BCONF;
$key = 'accounts_to_check';
$proType = 'batch';
$redisConfGen = bconf_get($BCONF, "*.common.redis_accounts_db1");
$redisConfCon = $redisConfGen['host']['rad1'];
$redisConfdb  = 2;
$redis = new Redis();
$redis->connect($redisConfCon['name'], $redisConfCon['port'], $redisConfCon['timeout']);
$redis->select($redisConfdb);

$lim = 5000;
$regions = bconf_get($BCONF,"*.common.region");
$categories = bconf_get($BCONF,"*.accounts.pro.${proType}");
$translate = array('sell' => 's', 'let' => 'u');

foreach ($regions as $regionId => $regionData) {
	foreach ($categories as $categoryId => $categoryData) {
		if (is_array($categoryData)) {
			foreach ($categoryData as $type => $typeData) {
				$multi = $redis->multi();
				$searchQuery = "0 lim:${lim} filter:region:${regionId} category_type:${categoryId}_${translate[$type]} *:*";
				$result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), $searchQuery);
				if (array_key_exists('email',$result) && is_array($result['email'])) {
					if (count($result) == $lim) {
						echo "Hit the ${lim} limit on query: ${searchQuery}\n";
					}
					foreach ($result['email'] as $index => $email) {
						$ad_id = $result['ad_id'][$index];
						$value = yapo_json_encode(array('email' => $email, 'ad_id' => $ad_id));
						$multi->lPush($key, $value);
					}
				} elseif (array_key_exists('email',$result)) {
					$ad_id = $result['ad_id'];
					$email = $result['email'];
					$value = yapo_json_encode(array('email' => $email, 'ad_id' => $ad_id));
					$multi->lPush($key, $value);
				}
				$multi->exec();
			}
		}
	}
}
$redis->close();
