<?php

namespace Yapo;

use bRESTful;

class AccountsAPI extends bRESTful
{
    public function __construct()
    {
        $this->restAPISettingsKey = "*.accounts_handler_rest_settings";
    }
}
