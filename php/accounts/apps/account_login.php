<?php

require_once('autoload_lib.php');
require_once('init.php');
require_once('JSON.php');

use Yapo\Bconf;

openlog("account_login", LOG_ODELAY, LOG_LOCAL0);

class account_login extends blocket_application {

	private $email;
	private $myads_email;
	private $user_id;
	private $acc_session;
	private $url_home;
	private $url_login;
	private $url_dashboard;
	private $url_referer;
	private $long_session;
	public  $only_myads;
	public  $is_mobile_app;

	function account_login() {
		$this->application_name  = "login";
 		$this->page_title = lang('PAGE_ACCOUNT_LOGIN_TITLE');
		$this->page_name = lang('PAGE_ACCOUNT_LOGIN_NAME');
		$this->headstyles = Bconf::bconfGet($BCONF,"*.common.login.headstyles");
		$this->headscripts = Bconf::bconfGet($BCONF,"*.common.login.headscripts");

		$this->url_home = Bconf::bconfGet($BCONF, "*.common.base_url.blocket");
		$this->url_ai   = Bconf::bconfGet($BCONF, "*.common.base_url.ai");
		$this->url_mobile   = Bconf::bconfGet($BCONF, "*.common.base_url.mobile");
		$this->url_dashboard = Bconf::bconfGet($BCONF, "*.common.base_url.secure")."/dashboard";
		$this->url_login = Bconf::bconfGet($BCONF, "*.common.base_url.secure")."/login";
		$this->url_referer = @$_SERVER['HTTP_REFERER'];
		$this->short_referer = '';
		if ($this->url_referer) {
			preg_match( '/http.*\/(.*)/', $this->url_referer, $matches);
			if (count($matches) >= 2) {
				$this->short_referer = $matches[1];
			}
		}

		$this->acc_session = new AccountSession();
		$keep_referer = Bconf::bconfGet($BCONF, "*.accounts.login.keep_page_referer");
		$info_url_ref = explode("?", $this->short_referer);
		if (in_array($info_url_ref[0], $keep_referer)) {
			setcookie(Bconf::bconfGet($BCONF, "*.accounts.login.cookie_referer"),
				$this->url_referer,
				time() + Bconf::bconfGet($BCONF, '*.accounts.session_timeout'), '/',
				Bconf::bconfGet($BCONF, '*.common.session.cookiedomain'),
				false,
				false
			);
		}

		header("Expires: Tue, 01 Jul 2001 06:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		if(isset($_GET['exit']) && $_GET['exit'] == 1){
			$this->init('logout', 0);
		}else{
		 	if(isset($_POST['acc_bar'])){
				$this->init('validate', 0);
			}else{
				$this->init('login_form', 0);
			}
		}
	}

	function get_state($statename) {
		switch($statename) {
		case 'login_form':
			return array(
				'function' => "login_form",
				'method' => "get",
				'params' => array("only_myads" => "f_generic",
						"is_mobile_app" => "f_generic"
				)
			);

		case 'logout':
			return array(
				'function' => "do_logout",
				'method' => "get",
			);
		case 'login_validate':
			return array(
				'params' => array(
					"email" => "f_generic",
					"password" => "f_acclogin_password",
					"long_session" => "f_generic",
					"is_mobile_app" => "f_generic"
					),
				'function' => "login_validate",
				'method' => "post"
			);

		case 'validate':
			return array(
				'params' => array(
					"email" => "f_generic",
					"password" => "f_accbar_password",
					"short_referer" => "f_generic",
	      ),
				'function' => "do_login_bar",
				'method' => "post"
			);

		case 'do_login':
			return array(
				'function' => "do_login",
				'method' => "post",
			);
		case 'mis_avisos':
			return array(
				'params' => array("myads_email" => "f_generic", "only_myads" => "f_generic" ),
				'function' => "my_ads",
				'method' => "post"
			);
		}
	}

	function f_accbar_password($var) {
		$max_length = (int)Bconf::bconfGet($BCONF, "*.common.passwd_length.max");
		if(strlen($var) > $max_length){
			return false;
		}
		else{
			return strip_tags($var);
		}
	}

	function f_generic($var) {
		return strip_tags($var);
	}

	function f_acclogin_password($var){
		$max_length = (int)Bconf::bconfGet($BCONF, "*.common.passwd_length.max");
		if(strlen($var) <= $max_length){
			return strip_tags($var);
		}
		return false;
	}
	//function to show the login form
	function login_form($only_myads = false, $is_mobile_app = false) {
		$data = array();

		if(isset($_COOKIE["sq"]) ){
			$sq = $_COOKIE["sq"];
			parse_str($sq, $string_query);	
			@$this->caller->parse_caller($string_query["ca"]);
			$this->data['region'] = $this->caller->region;
		}

		if($only_myads != false || false != $this->only_myads ){
			if($only_myads){
				$this->only_myads = $only_myads;
			}
		}
		if($is_mobile_app != false || false != $this->is_mobile_app){
			if($is_mobile_app){
				$this->is_mobile_app = $is_mobile_app;
			}
		}

		if(isset($this->myads_email))
			$data['myads_email'] = $this->myads_email;

		$data['session_id'] = $this->acc_session->get_session_id();
		$data['account_id'] = $this->acc_session->get_account_id();

		if (isset($_GET['cat']) && !empty($_GET['cat'])) {
			$data['category_referer'] = $_GET['cat'];
		}

		if (isset($_GET['list_id']) && !empty($_GET['list_id'])) {
			$data['list_id_referer'] = $_GET['list_id'];
		}

		if($this->acc_session->is_logged() && $this->only_myads == false){
			if($is_mobile_app != false || $this->is_mobile_app != false ){
				header('Location: '.$this->url_dashboard.'?ios_app=true');
				return 'FINISH';
			}
			else{
				header('Location: '.$this->url_dashboard);
				return 'FINISH';

			}
		}
		if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on"){
			header('Location: '.$this->url_login);
			return 'FINISH';
		}
		$this->display_layout('accounts/login.html', $data, null, null, 'mobile_select');
	}

	function login_validate($email, $password, $long_session = 0, $is_mobile_app = false) {
		$this->email = $email;
		$this->password = $password;
		$this->long_session = $long_session;
		if($is_mobile_app == true){
			$this->is_mobile_app = true;
		}
		if(empty($password)){
			$this->add_error('form', 'ACC_ERROR_LOGIN_INVALID');
			return 'login_form';
		}
		if(empty($email)) {
			$this->add_error('form', 'ACC_ERROR_LOGIN_INCOMPLETE');
			return 'login_form';
		}

		return 'do_login';
	}

	function get_post_redirect() {
		if (!empty($_COOKIE[Bconf::bconfGet($BCONF, "*.accounts.login.cookie_referer")])) {
			setcookie(Bconf::bconfGet($BCONF, "*.accounts.login.cookie_referer"),
				"",
				time() - 3600, '/',
				Bconf::bconfGet($BCONF, '*.common.session.cookiedomain'),
				false,
				false
			);
			return $_COOKIE[Bconf::bconfGet($BCONF, "*.accounts.login.cookie_referer")];
		}
		return "";
	}

	function do_login() {
		if($this->acc_session->is_logged()){
			$this->add_error('form', "ACC_ERROR_ALREADY_LOGGED");
			return 'login_form';
		}

		if (!$this->acc_session->login($this->email, $this->password, $this->long_session)) {
			if($this->acc_session->is_blocked($this->email))
				$this->add_error('form', "ACC_ERROR_ACCOUNT_BLOCKED");
			else
				$this->add_error('form', "ACC_ERROR_LOGIN_INVALID");
			return 'login_form';
		}


		$post_redirect = $this->get_post_redirect();
		if (!empty($post_redirect)) {
			header('Location: '.$post_redirect);
		} elseif ($this->is_mobile_app) {
			header('Location: '.$this->url_dashboard.'?ios_app=true');
		} else {
			header('Location: '.$this->url_dashboard);
		}
		return 'FINISH';
	}

	function do_logout() {
		$this->acc_session->expire();
		/* The following applications must redirect to login page */
		$exception_pages = explode(",",Bconf::bconfGet($BCONF, "*.accounts.pages.login_required"));
		foreach ($exception_pages as &$findme) {
			$pos = strpos($this->url_referer, $findme);
			if($pos !== false){
				header('Location: '.$this->url_login);
				return 'FINISH';
			}
		}
		if(!empty($this->url_referer) ){ 
			header('Location: '.$this->url_referer);
		}
		else {
			header('Location: '.$this->url_login);
		}
		return 'FINISH';
	}

	/** 
	 * Login for loginBar
	 * In this function dont show template only redirect 
	 */
	function do_login_bar($email, $password, $short_referer=null) {
		$this->email = $email;
		$this->password = $password;
		header('Content-type: application/json');

		if(empty($email) || empty($password)) {
			echo yapo_json_encode(array(
					'status' => 'error',
					'errorText' => Bconf::bconfGet($BCONF,"*.language.ACC_ERROR_LOGIN_INCOMPLETE.es")
			));
		}else{
			if($this->acc_session->is_logged()){
				echo json_encode(array(
						'status' => 'ok',
						'redirectTo' => $this->url_dashboard
				));
			}else{
				if ($this->acc_session->login($this->email, $this->password, $this->long_session)) {

					if(!empty($short_referer) && $short_referer === 'publica-un-aviso') {
						$redirect = $this->url_mobile . '/publica-un-aviso';
					}else{
						$redirect = $this->redirect_to_referer();
					}
					echo json_encode(array(
							'status' => 'ok',
							'redirectTo' => $redirect
					));
				}else{
					if ($this->acc_session->is_blocked($this->email)) {
						echo yapo_json_encode(array(
								'status' => 'error',
								'errorText' => Bconf::bconfGet($BCONF,"*.language.ACC_ERROR_ACCOUNT_BLOCKED.es")
						));
					} else {
						echo yapo_json_encode(array(
								'status' => 'error',
								'errorText' => Bconf::bconfGet($BCONF,"*.language.ACC_ERROR_LOGIN_INVALID.es")
							));
					}
				}
			}
		}
		return 'FINISH';
	}

	/* Only redirect to the referer.
	   if referer is empty redirect to home  */
	function redirect_to_referer($is_error = false){
		if(!empty($this->url_referer) ) {
			if (strpos($this->url_referer, 'login') !== false)
				$landing = $this->url_dashboard;
			elseif (strpos($this->url_referer, 'cuenta') !== false)
				$landing = $this->url_dashboard;
			elseif (strpos($this->url_referer, 'reset_password') !== false)
				$landing = $this->url_dashboard;
			elseif (strpos($this->url_referer, 'mis_avisos') !== false)
				$landing = $this->url_dashboard;
			elseif (strpos($this->url_referer, 'pvf') !== false)
				$landing = $this->url_dashboard;
			elseif (strpos($this->url_referer, '/ai/load/') !== false && strpos($this->url_referer, 'cmd=edit') !== false) {
				$params_pos = strpos($this->url_referer, '?');
				$landing = $this->url_ai . '/ai' . substr($this->url_referer, $params_pos);
			} else {
				$landing = $this->url_referer;
			}
		} else {
			$landing = $this->url_home;
		}
		$post_redirect = $this->get_post_redirect();
		if (!empty($post_redirect)) {
			$landing = $post_redirect;
		}
		return $landing;
	}

	function my_ads($myads_email, $only_myads = false) {
		$this->myads_email = $myads_email;
		if($only_myads != false)
			$this->only_myads = true;
		return 'login_form';
	}

}

$account_login = new account_login();
$account_login->run_fsm();

