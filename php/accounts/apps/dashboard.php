<?php
namespace Yapo;

$appName = "DashboardPage";

use bResponse;
use AccountSession;
use bTransaction;

// Syslog
openlog("DASHBOARD", LOG_ODELAY, LOG_LOCAL0);

require_once('autoload_lib.php');
require_once('register_time.php');
require_once('util.php');

$trans = new bTransaction();
$response = new bResponse();
$acc_session = new AccountSession();

$dashboard = new DashboardPage($trans, $response, $acc_session);
$dashboard->showPage();
