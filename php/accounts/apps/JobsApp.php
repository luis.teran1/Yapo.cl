<?php

namespace Yapo;

class JobsApp
{
    private $trans;
    private $acc_session;

    public function __construct($trans = null, $acc_session = null)
    {
        // Validating Session
        global $BCONF;
        $this->trans = $trans;
        $this->acc_session = $acc_session;
        if (!$this->acc_session->is_logged()) {
            $loginUrl = Bconf::bconfGet($BCONF, "*.common.base_url.secure") . "/login";
            $result = array('error' => 'AUTH_ERROR', 'redirect' => $loginUrl);
            die(yapo_json_encode($result));
        }
    }

    public function getAdreplyReport($url, $request)
    {
        global $BCONF;
        if (!isset($request->email, $request->list_id)) {
            jobs_api::setStatusCode(400);
            return array('error' => Bconf::lang($BCONF, 'JOBS_AR_REPORT_PARAM_ERROR'));
        }
        $list_id = sanitizeVar($request->list_id, 'int');
        $recipient_email = sanitizeVar($request->email, 'string');
        $account_email = sanitizeVar($this->acc_session->get_param('email'), 'string');
        if (empty($list_id) || empty($recipient_email) || empty($account_email)) {
            jobs_api::setStatusCode(400);
            return array('error' => Bconf::lang($BCONF, 'JOBS_AR_REPORT_PARAM_ERROR'));
        }
        return $this->doGetAdReplyReport($list_id, $recipient_email, $account_email);
    }

    private function doGetAdReplyReport($list_id, $recipient_email, $account_email)
    {
        global $BCONF;
        $reply = $this->transSendReport($list_id, $recipient_email, $account_email);

        if (array_key_exists('error', $reply)) {
            $message = Bconf::lang($BCONF, $reply['error']);
            if ($message) {
                    return array('message' => $message);
            }
            jobs_api::setStatusCode(500);
            return array('error' => Bconf::lang($BCONF, 'JOBS_AR_REPORT_ERROR'), 'extra' => $reply['error']);
        }

        return array('message' => Bconf::lang($BCONF, 'JOBS_AR_REPORT_SUCCESS'));
    }

    private function transSendReport($list_id, $recipient_email, $account_email)
    {
        $trans = $this->trans->reset();
        $trans->add_data('list_id', $list_id);
        $trans->add_data('recipient_email', $recipient_email);
        $trans->add_data('account_email', $account_email);
        return $trans->send_command("send_jobs_ad_adreply_report");
    }
}
