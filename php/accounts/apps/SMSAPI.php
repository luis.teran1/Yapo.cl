<?php

namespace Yapo;

use bRESTful;

class SMSAPI extends bRESTful
{
    public function __construct()
    {
        $this->restAPISettingsKey = "*.sms_handler_rest_settings";
    }
}
