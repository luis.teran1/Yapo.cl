<?php

namespace Yapo;

use Yapo\Bconf;

class AccountsApp
{
    private $trans;
    private $acc_session;

    public function __construct($trans = null, $acc_session = null)
    {
        // Validating Session
        global $BCONF;
        $this->trans = $trans;
        $this->acc_session = $acc_session;
        if (!$this->acc_session->is_logged()) {
            $loginUrl = Bconf::bconfGet($BCONF, "*.common.base_url.secure") . "/login";
            $result = array('error' => 'AUTH_ERROR', 'redirect' => $loginUrl);
            die(yapo_json_encode($result));
        }
    }

    public function deleteUnpaidAd($url, $request)
    {
        global $BCONF;
        if (!isset($request->ad_id)) {
            bRESTful::setStatusCode(400);
            return array('error' => Bconf::lang($BCONF, 'ACCOUNTS_DELETE_UNPAID_PARAM_ERROR'));
        }
        $ad_id = sanitizeVar($request->ad_id, 'int');
        $account_id = sanitizeVar($this->acc_session->get_param('account_id'), 'int');
        return $this->doDeleteUnpaidAd($account_id, $ad_id);
    }

    private function doDeleteUnpaidAd($account_id, $ad_id)
    {
        global $BCONF;
        $reply = $this->transArchiveUnpaidAd($account_id, $ad_id);

        if (array_key_exists('error', $reply)) {
            switch ($reply['error']) {
                case 'ERROR_ARCHIVE_UNPAID_AD_RESULT_EMPTY':
                    bRESTful::setStatusCode(404);
                    break;
                default:
                    bRESTful::setStatusCode(400);
            }
            return array('error' => Bconf::lang($BCONF, 'ACCOUNTS_DELETE_UNPAID_ERROR'));
        }

        return array('message' => Bconf::lang($BCONF, 'ACCOUNTS_DELETE_UNPAID_SUCCESS'));
    }

    private function transArchiveUnpaidAd($account_id, $ad_id)
    {
        $trans = $this->trans->reset();
        $trans->add_data('account_id', $account_id);
        $trans->add_data('ad_id', $ad_id);
        return $trans->send_command("archive_unpaid_ad");
    }
}
