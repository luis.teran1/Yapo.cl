<?php

require_once('autoload_lib.php');
require_once('util.php');
require_once('init.php');

openlog("accounts_forgot_pass", LOG_ODELAY, LOG_LOCAL0);


class reset_password extends blocket_application {
	private $email;
	private $data = array();
	private $result = array();
	private $result_status;

	function reset_password() {
		$this->headstyles = bconf_get($BCONF,"*.common.login.headstyles");
		$this->headscripts = bconf_get($BCONF,"*.common.login.headscripts");
		$this->init('load', 0);
		$this->application_name = 'reset_password';
	}

	function get_state($statename) {
		switch($statename) {
		case 'load':
			return array(
				'function' => 'reset_load',
				'method' => 'both'
			);
		case 'ask_email':
			return array(
				'function' => 'reset_ask_email',
					'params' => array(
					"op" => "f_generic",
					"mb" => "f_generic",
					"email" => "f_email",
					"is_mobile_app" => "f_boolean"),
				'method' => 'get'
			);
		case 'validate_email':
			return array(
				'function' => 'reset_validate_email',
					'params' => array(
					"email" => "f_email",
					"op" => "f_generic",
					"is_mobile_app" => "f_boolean"),
				'method' => 'both'
			);

		case 'ask_password':
			return array(
				'function' => 'reset_ask_password',
					'params' => array(
					"op" => "f_generic",
					"err" => "f_generic",
					"email" => "f_email",
					"h" => "f_generic"),
				'method' => 'get'
			);
		case 'change_password':
			return array(
				'function' => 'reset_change_password',
					'params' => array(
					"email" => "f_email",
					"op" => "f_generic",
					"hash" => "f_generic",
					"password" => "f_generic",
					"password_verification" => "f_generic"),
				'method' => 'post'
			);
		case 'show_result':
			return array(
				'function' => 'reset_show_result',
				'method' => 'get'
			);
		case 'finalize':
			return array(
				'function' => 'reset_finalize',
				'method' => 'get'
			);
		case 'show_activate':
			return array(
				'function' => 'reset_show_activate',
				'method' => 'get'
			);
		}
	}

	function reset_load(){
		syslog(LOG_INFO, log_string() . "LOAD");
		return 'ask_email';
	}
	function reset_ask_email($op, $mb, $email, $is_mobile_app){
		global $BCONF;
		syslog(LOG_INFO, log_string() . "ASK_EMAIL");
		$data = array();
		# zero means that we does not know where it comes from,
		# so we redirect it to home or something
		if(isset($mb))
			$data['mb'] = $mb;
		if(isset($email) && !empty($email))
			$data['email'] = $email;
		else
			$data['email'] = "";
		if(isset($op) && !empty($op) && is_numeric($op)){
			$data['op'] = $op;
		}elseif(isset($this->data['op']) && is_numeric($this->data['op'])){
			$data['op'] = $this->data['op'];
		}else{
			$data['op'] = 0;
		}
		if($is_mobile_app) {
			$data['display_for_app'] = "true";
		}
		$this->display_layout('accounts/reset_password.html', $data, null, null, 'mobile_select');
	}
	function reset_validate_email($email, $op, $is_mobile_app){
		global $BCONF;
		syslog(LOG_INFO, log_string() . "VALIDATE_EMAIL");

		if($is_mobile_app) {
			$this->data['display_for_app'] = "true";
			$this->result['display_for_app'] = "true";
		}

		// check if the account exists
		$this->data['op'] = $op;
		$this->data['email'] = $email;

		if(!isset($this->data['op']) || empty($this->data['email'])){
			syslog(LOG_ERR, log_string() . "USER SENT TO ASK_MAIL FOR MISSING ATTRIBUTES");
			$this->result['content'] = 'accounts/reset_password.html';
			$this->result['err_status'] = "ERROR_EMAIL_MISSING";
			$this->result['op'] = isset($this->data['op']) ? $this->data['op'] : 0;
			$this->result['nofinish'] = true;
			return 'show_result';
		}
		$get_account_trans = new bTransaction();
		$get_account_trans->add_data('email', $this->data['email']);
		/* Jose told me that is better to not use optional params, blame him */
		$get_account_reply = $get_account_trans->send_command('get_account');
		if (!isset($get_account_reply) || $get_account_reply['status'] != 'TRANS_OK' || $get_account_reply['account_status'] != 'active' ) {
			syslog(LOG_ERR, log_string()."ERROR: getting account values for " . $this->data['email']);
			if ($get_account_reply['status'] == 'TRANS_OK'){
			       	if($get_account_reply['account_status'] == 'inactive' )
					$this->result['err_status'] = 'FORGOT_PASSWORD_INVALID_ACCOUNT';
				else if($get_account_reply['account_status'] == 'pending_confirmation' ){
					$this->data['name'] =  $get_account_reply['name'];
					return 'show_activate';
				}

			}elseif (!empty($get_account_reply['email']))
				$this->result['err_status'] = $get_account_reply['email'];
			elseif (isset($get_account_reply['error'])) {
				$get_ads_trans = new bTransaction();
				$get_ads_trans->add_data('email', $this->data['email']);
				$get_ads_trans_reply = $get_ads_trans->send_command('get_ads_count');

				if($get_ads_trans_reply['count'] > 0) {
					$this->result['err_status'] = 'ACCOUNT_NOT_FOUND_WITH_ADS';
				}
				else {
					$this->result['err_status'] = $get_account_reply['error'];
				}

			}
			else
				$this->result['FORGOT_PASSWORD_INVALID_ACCOUNT'];
			$this->result['content'] = 'accounts/reset_password.html';
			$this->result['op'] = $this->data['op'];
			return 'show_result';
		}

		$trans = new bTransaction();
		$trans->add_data("email", $this->data['email']);
		$trans->add_data("nickname", $get_account_reply['name']);
		$trans->add_data("op", $this->data['op']);
		$trans->add_data("mail_type", "reset_account_password");
		$reply = $trans->send_command('accountmail');
		if ($trans->has_error(true) || $reply['status'] != "TRANS_OK" ) {
			syslog(LOG_ERR, log_string()."ERROR: reset account password email could not be sent for " . $this->data['email'] . " user");
			$this->result['err_status'] = 'FORGOT_PASSWORD_MAIL_TRANS_ERROR';
			$this->result['op'] = $this->data['op'];
			return 'show_result';
		}


		$this->result['status'] = 'FORGOT_PASSWORD_SENT_EMAIL';
		return 'show_result';
	}

	function reset_ask_password($op, $err, $email, $h){
		global $BCONF;
		syslog(LOG_INFO, log_string() . "ASK PASSWORD");
		if(!isset($op) || empty($email) || empty($h) ){
			syslog(LOG_ERR, log_string()."ERROR: data missing when trying to enter ask_password");
			$this->result['err_status'] = 'FORGOT_PASSWORD_MAIL_ERROR_INPUT_MISSING';
			$this->result['content'] = 'accounts/expired_password_mail.html';
			$this->reset_show_result();
			return 'FINISH';
		}
		$data['op'] = $op;
		$data['email'] = $email;
		$data['hashval'] = $h;
		// validate if the url is correct
		$rds_mgr = new RedisSessionManager();
		$prefix = bconf_get($BCONF, "*.accounts.session_redis_prefix");
		$hashval = $rds_mgr->rsm_get($prefix.'_forgot_password_'.$data['email']);
		syslog(LOG_INFO, $hashval);

		if (empty($hashval) || $hashval != $data['hashval']) {
			syslog(LOG_ERR, log_string()."ERROR: link for email " . $data['email'] . " invalid");
			$this->result['err_status'] = 'FORGOT_PASSWORD_MAIL_ERROR';
			$this->result['content'] = 'accounts/expired_password_mail.html';
			$this->reset_show_result();
			return 'FINISH';
		}

		switch ($err) {
			case 3:
				$data['err_status']= "FORGOT_PASSWORD_TOO_LONG";
				break;
			case 1:
				$data['err_status']= "FORGOT_PASSWORD_TOO_SHORT";
				break;
			case 2:
				$data['err_status']= "FORGOT_PASSWORD_DOES_NOT_MATCH";
				break;
		}

		$account_session = new AccountSession();
		if($account_session->is_logged()){
			$session_id = $account_session->get_session_id();
			$account_session->expire($session_id);
		}
		$this->display_layout('accounts/change_password.html', $data, null, null, 'mobile_select');
	}
	function reset_change_password($email, $op, $hash, $password, $password_verification){

		global $BCONF;
		syslog(LOG_INFO, log_string() . "CHANGE");
		if($_SERVER['REQUEST_METHOD'] != "POST"){
			syslog(LOG_INFO, log_string()."in change_password, If method is get, user is redirected to initial state");
			return 'ask_email';
		}

		if(!isset($op) || empty($op) ){
			syslog(LOG_ERR, log_string()."ERROR: op missing, setting to 0");
			$op = 0;
		}
		$pwd_len_min = (int)bconf_get($BCONF, "*.common.passwd_length.min");
		$pwd_len_max = (int)bconf_get($BCONF, "*.common.passwd_length.max");

		// validate the the passwords match
		if (empty($password) || strlen($password) < $pwd_len_min) {
			syslog(LOG_INFO, log_string()."ERROR: the password's format is not valid min length");
			$this->redirect('ask_password',"err=1&email=$email&op=$op&h=$hash");
			return 'FINISH';
		}
		if (strlen($password) > $pwd_len_max) {
			syslog(LOG_INFO, log_string()."ERROR: the password's format is not valid max length");
			$this->redirect('ask_password',"err=3&email=$email&op=$op&h=$hash");
			return 'FINISH';
		}
		if ($password != $password_verification){
			syslog(LOG_INFO, log_string()."ERROR: the password does not match");
			$this->redirect('ask_password',"err=2&email=$email&op=$op&h=$hash");
			return 'FINISH';
		}

		// check the redis key
		$rds_mgr = new RedisSessionManager();
		$prefix =  bconf_get($BCONF, "*.accounts.session_redis_prefix");
		if (empty($hash) || $rds_mgr->rsm_get($prefix.'_forgot_password_'.$email) != $hash)
		{
			syslog(LOG_ERR, log_string()."ERROR: redis hash invalid");
			$this->result['err_status'] = 'FORGOT_PASSWORD_CHANGE_ERROR';
			return 'show_result';
		}

		// call the trans
		$trans = new bTransaction();
		$hash_passwd = hash_password($password);
		$trans->add_data('salted_password', $hash_passwd);
		$trans->add_data('email', $email);
		$reply = $trans->send_command('reset_account_password');
		if ($trans->has_error(true) || $reply['status'] != "TRANS_OK" ) {
			syslog(LOG_ERR, log_string()."ERROR: the password could not be changed");
			$this->result['err_status'] = 'FORGOT_PASSWORD_CHANGE_ERROR';
			return 'show_result';
		}

		// delete redis key
		$rds_mgr->rsm_del($prefix.'_forgot_password_'.$email);

		syslog(LOG_INFO, log_string()."Reset account password succeeded for $email user");
		$this->result['status'] = 'FORGOT_PASSWORD_SUCCESS';
		$this->result['data'] = $email;
		$this->result['op'] = $op;
		return 'finalize';
	}
	function reset_show_result(){
		syslog(LOG_ERR, log_string()."SHOW RESULT");
		if (!empty($this->result['err_status'])) syslog(LOG_ERR, log_string()."ERROR STATUS: " . $this->result['err_status']);
		if (!empty($this->result['status'])) syslog(LOG_ERR, log_string()."STATUS: " . $this->result['status']);

		if (!empty($this->result['status']) && $this->result['status'] == "FORGOT_PASSWORD_SENT_EMAIL") {
			$this->result['result_page_name'] = bconf_get($BCONF,"*.language.PAGE_FORGOT_ASK_EMAIL.es");
			$this->result['result_message'] = bconf_get($BCONF,"*.language.FORGOT_PASSWORD_SUCCESS.es");
			$this->display_layout('accounts/forgot_password_result.html', $this->result, null, null, 'mobile_select');
			return 'FINISH';
		}
		$this->display_layout((empty($this->result['content']) ? 'accounts/change_password.html' : $this->result['content']), $this->result, null, null, 'mobile_select');
		if(isset($this->result['nofinish']) && $this->result['nofinish']){
			unset($this->result['nofinish']);
		}
		return 'FINISH';

	}

	function reset_show_activate(){
		syslog(LOG_ERR, log_string()."SHOW ACTIVATE");
		$email = $this->data['email'];
		$name = $this->data['name'];
		$account_util = new AccountUtil();
		$account_mail = $account_util->account_mail($email, $name);
		if ($account_mail['status'] != "TRANS_OK" ) {
			syslog(LOG_ERR, log_string()."ERROR: account email could not be sent for $email user");
	        	$this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_ERROR');
			$this->data['creation_status'] = "ACCOUNT_CONFIRMATION_MAIL_ERROR";
			//return 'result';
			$this->display_layout('accounts/create_account.html', $this->data, null, null, 'mobile_select');
		}

		$this->data['creation_status'] = 'ACCOUNT_CREATION_OK';
	        $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_SUCCESS');
		$this->display_layout('accounts/create_account.html', $this->data, null, null, 'mobile_select');

		return 'FINISH';
	}

	function reset_finalize(){
		global $BCONF;
		if(empty($this->result['status']) || $this->result['status'] != "FORGOT_PASSWORD_SUCCESS" || empty($this->result['data'])){
			syslog(LOG_INFO, log_string()."User redirected to load from finalize in forgot password");
			return 'load';
		}else{

			$acc_session = new AccountSession();
			if(!$acc_session->loginSpecial($this->result['data'])){
				//error, send him to his grave
				syslog(LOG_INFO, log_string()."couldn log user, so we are showing him we had success changing password w/o redirecting");
				$this->result['err_status'] = "ACCOUNT_ACTIVATION_ERROR";
				return 'show_result';
			}
			
			$short_user_name = $acc_session->get_param('name');
			$this->result['account_name'] = $short_user_name;
			$short_user_name = explode(" ", $short_user_name);
			$short_user_name = $short_user_name[0];
			if(strlen($short_user_name) > 10){
				$short_user_name = substr($short_user_name,0,7)."&hellip;";
			}
			$this->result['short_user_name'] = $short_user_name;
			$this->result['result_page_name'] = bconf_get($BCONF,"*.language.PAGE_FORGOT_CHANGE_PASSWORD.es");
			$this->result['result_message'] = bconf_get($BCONF,"*.language.FORGOT_PASSWORD_SUCCESS_MESSAGE.es");
			$this->result['result_page_destination'] = $this->get_original_page($this->result['op']);
			syslog(LOG_INFO, log_string()."User will be sent to ".$this->result['result_page_destination']." from finalize in forgot password");
			switch ($this->result['op']) {
				case 3:
					$this->result['result_button_name'] = bconf_get($BCONF,"*.language.FORGOT_PASSWORD_SUCCESS_TO_AI_BUTTON_MESSAGE.es");
					break;
				default:
					$this->result['result_button_name'] = bconf_get($BCONF,"*.language.FORGOT_PASSWORD_SUCCESS_TO_ACCOUNT_BUTTON_MESSAGE.es");
			}

			$this->account_name = $this->user_value_name = $acc_session->get_param('name');
			$this->display_layout('accounts/forgot_password_result.html', $this->result, null, null, 'mobile_select');

			return 'FINISH';
		}
	}


	function get_original_page($val){
		global $BCONF;
		//$current = @$_SERVER['HTTP_REFERER'];
		//$current = !empty($current) ? $current : bconf_get($BCONF, "*.common.base_url.blocket");
		//return $current;
		switch ($val){
			case 3:
				return bconf_get($BCONF, "*.common.base_url.secure")."/ai";
			default:
				return bconf_get($BCONF, "*.common.base_url.secure")."/dashboard";
		}
	}

	function f_generic($var) {
		return strip_tags($var);
	}

	function f_email($var) {
		return strtolower(strip_tags($var));
	}

	function f_boolean($var) {
		if(is_null($var) || !isset($var) || empty($var))
			return null;
		if(isset($var) && $var == "true")
			return true;
		else
			return false;
	}
}

$app = new reset_password();
$app->run_fsm();
