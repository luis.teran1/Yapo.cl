<?php

namespace Yapo;

use bRESTful;

class JobsAPI extends bRESTful
{
    public function __construct()
    {
        $this->restAPISettingsKey = "*.jobs_handler_rest_settings";
    }
}
