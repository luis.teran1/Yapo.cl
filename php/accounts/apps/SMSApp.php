<?php

namespace Yapo;

class SMSApp
{
    private $microservice_connector;

    public function __construct($microservice_connector = null)
    {
        $this->microservice_connector = $microservice_connector;
    }

    public function sendMessage($url, $request)
    {
        global $BCONF;
        $to = sanitizeVar($request->to, 'int');
        $code = sanitizeVar($request->code, 'string');
        if (empty($to) || empty($code)) {
            SMSAPI::setStatusCode(400);
            return array('error' => Bconf::lang($BCONF, 'SEND_MESSAGE_PARAM_ERROR'));
        }
        $data = array(
            "code" => $code,
            "to" => $to
        );
        $this->microservice_connector->setServiceName("super_message_service");
        $response = $this->microservice_connector->execute("send", $data);

        SMSAPI::setStatusCode($response->code);
        return json_decode($response);
    }
}
