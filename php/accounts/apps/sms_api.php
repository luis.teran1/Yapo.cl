<?php

namespace Yapo;

require_once('autoload_lib.php');
require_once('httpful.php');
require_once('JSON.php');

// Syslog
openlog('sms_api', LOG_ODELAY, LOG_LOCAL0);

$api = new SMSAPI();
$api->run();
