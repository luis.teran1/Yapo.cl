<?php

require_once('autoload_lib.php');

use Yapo\JobsAPI as JobsAPI;

// Syslog
openlog('jobs_api', LOG_ODELAY, LOG_LOCAL0);

$api = new JobsAPI();
$api->run();
