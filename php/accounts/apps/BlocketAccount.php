<?php

class BlocketAccount extends blocket_application
{
    public $data;
    public $on_edit_success;
    public $origin;
    public $account_util;
    public $acc_interface;
    public $reload_stop;

    public function __construct()
    {
        global $BCONF;
        global $session;
        $this->on_edit_success = false;
        $this->add_static_vars(array('origin'));
        $this->page_title = lang('PAGE_ACCOUNT_CREATE_TITLE');
        $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME');
        $this->headstyles = bconf_get($BCONF, "*.common.create_account.headstyles");
        $this->headscripts = bconf_get($BCONF, "*.common.create_account.headscripts");

        if (strlen(strstr($_SERVER['REQUEST_URI'], 'update_params'))) {
            $this->init('update_params');
        } else {
            $this->init('load', 0, true);
        }

        $this->account_util = new AccountUtil();
        $this->application = 'accounts';

        bLogger::logInfo(__METHOD__, "Calling account aplication");
        bLogger::logInfo(__METHOD__, "New session");
        bLogger::logInfo(__METHOD__, "Browser: " . $_SERVER['HTTP_USER_AGENT']);
    }

    /**
     * get_state is used by blocket application to obtain the data (params, function, etc)
     * to process in the currente state
     * (PLEASE, DO NOT CHANGE THE NAME)
     */
    public function get_state($statename)
    {
        switch ($statename) {
            case 'load':
                return array('function' => 'accountLoad',
                    'method' => 'get',
                    'reset_progress' => true,
                    'allowed_states' => array('load'));
            case 'form':
                return array('function' => 'accountForm',
                    'method' => 'get',
                    'allowed_states' => array('form', 'create'));
            case 'create':
                return array(
                    'function' => 'accountCreate',
                    'params' => array(
                        'name' => 'filterTags',
                        'rut' => 'filterRut',
                        'email' => 'filterTags',
                        'phone' => 'filterTags',
                        'is_company' => 'filterTags',
                        'region' => 'filterTags',
                        'password' => 'filterTags',
                        'password_verify' => 'filterTags',
                        'accept_conditions' => 'filterTags',
                        'address' => 'filterTags',
                        'contact' => 'filterTags',
                        'lob' => 'filterTags',
                        'commune' => 'filterTags',
                        'uid' => 'filterTags',
                        'or' => 'filterTags',
                        'gender' => 'filterTags',
                        'source' => 'filterTags',
                        'phone_type' => 'filterTags',
                        'area_code' => 'filterTags',
                        'business_name' => 'filterTags',
                    ),
                    'method' => 'both');
            case 'result':
                return array(
                    'function' => 'accountResult',
                    'method' => 'get');
            case 'confirm':
                return array(
                    'function' => 'accountConfirm',
                    'method' => 'get',
                    'params' => array(
                        'h' => 'filterTags',
                        'e' => 'filterTags',
                    ));
            case 'confirm_result':
                return array(
                    'function' => 'confirmResult',
                    'method' => 'get');
            case 'view':
                return array(
                    'function' => 'accountView',
                    'method' => 'get');
            case 'edit':
                return array(
                    'function' => 'accountEdit',
                    'method' => 'get');
            case 'update':
                return array(
                    'function' => 'accountUpdate',
                    'params' => array(
                        'name' => 'filterTags',
                        'rut' => 'filterRut',
                        'phone' => 'filterTags',
                        'is_company' => 'filterTags',
                        'region' => 'filterTags',
                        'address' => 'filterTags',
                        'contact' => 'filterTags',
                        'lob' => 'filterTags',
                        'commune' => 'filterTags',
                        'password' => 'filterTags',
                        'password_verify' => 'filterTags',
                        'gender' => 'filterTags',
                        'birth_day' => 'filterTags',
                        'birth_month' => 'filterTags',
                        'birth_year' => 'filterTags',
                        'source' => 'filterTags',
                        'phone_type' => 'filterTags',
                        'area_code' => 'filterTags',
                        'business_name' => 'filterTags',
                        'email' => 'filterTags',
                    ),
                    'method' => 'post');
            case 'update_params':
                return array('function' => 'accountUpdateAjax',
                    'params' => array(
                        'phone' => 'filterTags',
                        'name' => 'filterTags',
                        'email' => 'filterTags',
                    ),
                    'method' => 'post');
            case 'ocp_start':
                return array(
                    'function' => 'ocpStart',
                    'method' => 'get');
            case 'ocp_end':
                return array(
                    'function' => 'ocpEnd',
                    'method' => 'get');
        }
    }

    public function ocpStart()
    {
        global $BCONF;
        $account_session = new AccountSession();
        bLogger::logDebug(__METHOD__, "ONECLICK: register:");
        if ($account_session->is_logged()) {
            $email = $account_session->get_param('email');
            $ocp_client = new OneClickApiClient();
            $ocp_client->removeUser($email);

            $return_url = bconf_get($BCONF, '*.common.base_url.ms_oneclick_return_acc');
            $result = $ocp_client->initInscription($email, $return_url);

            if (isset($result) && $result->result == 'OK') {
                bLogger::logDebug(__METHOD__, "ONECLICK: result init_inscription:".print_r($result, true));
                $redirectData = array();
                $redirectData['url'] = $result->url;
                $redirectData['method'] = 'POST';
                $redirectData['params']['TBK_TOKEN'] = $result->token;
                $simple_data['no_footer'] = 1;
                $simple_data['no_header'] = 1;
                bLogger::logInfo(__METHOD__, "redirectData = " . var_export($redirectData, true));
                $oc_register_template = 'payment/redirect_oneclick_register.html';
                $this->display_layout($oc_register_template, $simple_data, $redirectData, null, 'html5_base');
                return 'FINISH';
            }
            $this->from_register = true;
            $this->oneclick_register_success = 0;
        }
        return 'edit';
    }

    public function ocpEnd()
    {
        if (empty($_POST['TBK_TOKEN'])) {
            return 'edit';
        }

        $token = $_POST['TBK_TOKEN'];
        bLogger::logDebug(__METHOD__, "EndInscription token $token");
        $account_session = new AccountSession();
        if ($account_session->is_logged()) {
            $ocp_client = new OneClickApiClient();
            $result = $ocp_client->endInscription($token);
            bLogger::logInfo(__METHOD__, "Result endInscription = " . var_export($result, true));
            $this->from_register = true;
            $this->oneclick_register_success = ($result) ? 1 : 0;
        }
        return 'edit';
    }

    public function accountLoad()
    {
        global $BCONF;
        bLogger::logInfo(__METHOD__, "LOAD");
        $account_session = new AccountSession();
        if ($account_session->is_logged()) {
            header("Location: /dashboard");
            return 'FINISH';
        }
        unset($this->data);
        return 'form';
    }

    public function accountForm()
    {
        global $BCONF;
        bLogger::logInfo(__METHOD__, "FORM");
        $this->data['appl'] = "accounts";
        if (isset($_COOKIE["sq"])) {
            $sq = $_COOKIE["sq"];
            parse_str($sq, $string_query);
            @$this->caller->parse_caller($string_query["ca"]);
            $this->data['region'] = $this->caller->region;
        }
        if ($this->origin == "multi" || @$_GET['or'] == "multi") {
            $this->display_layout('accounts/login.html', $this->data, null, null, 'mobile_select');
            unset($this->origin);
        } else {
            $this->display_layout('accounts/create_account.html', $this->data, null, null, 'mobile_select');
            unset($this->data['has_error']);
            unset($this->data['no_connection']);
        }
    }

    /**
     * update_data: I don't know where this is used
     * so i keep the original format
     */
    public function update_data($account_session)
    {
        $account_session->reload_account_info();
        $account_session->reload_redis_info();
        array_merge($this->data, $account_session->getAccountInfo());
    }

    public function accountCreate(
        $name,
        $rut,
        $email,
        $phone,
        $is_company,
        $region,
        $password,
        $password_verify,
        $accept_conditions,
        $address,
        $contact,
        $lob,
        $commune,
        $uid,
        $or,
        $gender,
        $source,
        $phone_type,
        $area_code,
        $business_name
    ) {
        global $BCONF;
        bLogger::logInfo(__METHOD__, "CREATE");

        $this->reload_stop = true;
        /* Persistence of values received */
        $this->data['appl'] = "accounts";
        $this->data['name'] = $name;
        $this->data['rut'] = $rut;
        $this->data['email'] = $email;
        //phone info
        $this->data['phone'] = $phone;
        $this->data['area_code'] = $area_code;
        $this->data['phone_type'] = $phone_type;

        $this->data['address'] = $address;
        $this->data['contact'] = $contact;
        $this->data['lob'] = $lob;
        $this->data['commune'] = $commune;
        $this->data['is_company'] = $is_company;
        $this->data['region'] = $region;
        $this->data['gender'] = $gender;
        $this->data['source'] = $source;
        $this->data['phone_type'] = $phone_type;
        $this->data['area_code'] = $area_code;
        $this->data['business_name'] = $business_name;
        $this->origin = $or;

        /*Do transaction, success page if ok - fail page if problem*/
        $trans = new bTransaction();
        if (!empty($name)) {
            $trans->add_data('name', $name);
        }
        if (!empty($rut)) {
            $trans->add_data('rut', $rut);
        }
        if (!empty($email)) {
            $trans->add_data('email', $email);
        }
        if (!empty($phone)) {
            if (!empty($phone_type)) {
                if ($phone_type == "m") {
                    $trans->add_data('phone', "9".$phone);
                } else {
                    $trans->add_data('phone', $area_code.$phone);
                }
            }
        }
        if (!empty($gender)) {
            $trans->add_data('gender', $gender);
        }
        if (!empty($source)) {
            $trans->add_data('source', $source);
        }

        if (!empty($business_name)) {
            $trans->add_data('business_name', $business_name);
        }

        $passwd_check = password_verify($password, $password_verify, false);
        if ($passwd_check === true) {
            $hash_passwd = hash_password($password);
            $trans->add_data('password', $hash_passwd);
        } else {
            $error_passwd = $passwd_check;
        }

        $trans->add_data('is_company', $is_company);
        $trans->add_data('region', $region);
        $trans->add_data('address', $address);
        $trans->add_data('contact', $contact);
        $trans->add_data('lob', $lob);
        $trans->add_data('commune', $commune);
        $trans->add_data('accept_conditions', $accept_conditions);
        if (!empty($uid)) {
            $trans->add_data('uid', $uid);
        }
        $reply = $trans->send_command('create_account');

        if ($trans->has_error(true) || $reply['status'] != "TRANS_OK") {
            bLogger::logError(__METHOD__, "account creation failed for $email user");
            bLogger::logError(__METHOD__, var_export($trans->get_errors(), true));
            if ($trans->_reply['error'] == 'ACCOUNT_ALREADY_EXISTS') {
                $trans_getacc = new bTransaction();
                $trans_getacc->add_data('email', $email);
                $reply_getacc = $trans_getacc->send_command('get_account');
                /* if the account is pending_confirmation send email again and show OK to the user */
                if ($reply_getacc['account_status'] == 'pending_confirmation') {
                    $this->setPostRedirectReferer($email);
                    $account_mail = $this->account_util->account_mail($email, $name);
                    if ($account_mail['status'] != "TRANS_OK") {
                        bLogger::logError(__METHOD__, "account email could not be sent for $email user");
                        $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_ERROR');
                        $this->data['creation_status'] = "ACCOUNT_CONFIRMATION_MAIL_ERROR";
                        $this->data['no_connection'] = 1;
                        return 'result';
                    }
                    $this->data['creation_status'] = 'ACCOUNT_CREATION_OK';
                    $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_SUCCESS');
                    return 'confirm_result';
                }
            }
            if (!empty($reply['error'])) {
                if ($trans->_reply['error'] == 'ACCOUNT_ALREADY_EXISTS') {
                    $text1 = lang('ACCOUNT_ALREADY_EXISTS');
                    $link = bconf_get($BCONF, "*.common.base_url.secure");
                    $link = $link."/reset_password/validate_email?op=1&email=".$email;
                    $text2 = str_replace("RECOVER_LINK", $link, lang('ACCOUNT_ALREADY_EXISTS_LINK'));
                    $this->data['err_creation_status'] = $text1." ".$text2;
                } else {
                    $this->data['err_creation_status'] = $reply['error'];
                }
            }
            $this->set_errors($trans->get_errors());
            $this->data['has_error'] = 1;

            if (empty($error_passwd)) {
                $this->remove_error('password');
            } elseif ($error_passwd == "ERROR_PASSWORD_MISMATCH") {
                $this->add_error("password_verify", $error_passwd);
                $this->remove_error('password');
            } else {
                $this->add_error("password", $error_passwd);
            }

            bLogger::logError(__METHOD__, var_export($this->errors, true));
            return 'form';
        }

        bLogger::logInfo(__METHOD__, "Account creation succeeded for $email user");
        $this->setPostRedirectReferer($email);
        $account_mail = $this->account_util->account_mail($email, $name);

        if ($account_mail['status'] != "TRANS_OK") {
            bLogger::logError(__METHOD__, "account email could not be sent for $email user");
            $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_ERROR');
            $this->data['creation_status'] = "ACCOUNT_CONFIRMATION_MAIL_ERROR";
            $this->data['no_connection'] = 1;
            return 'result';
        }


        $this->data['creation_status'] = 'ACCOUNT_CREATION_OK';
        $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_SUCCESS');
        return 'confirm_result';
    }

    public function accountView()
    {
        $this->page_title = lang('MY_ACCOUNT');
        bLogger::logInfo(__METHOD__, "Account View");
        $account_session = new AccountSession();

        // if some mandatory data is missing, the user should be redirected to edition
        if ($account_session->get_param('region') == "" ||
            $account_session->get_param('name') == "" ||
            $account_session->get_param('phone') == ""
        ) {
            return 'edit';
        }

        if ($account_session->is_logged()) {
            $reply = $account_session->getAccountInfo();
            if (isset($reply['password'])) {
                unset($reply['password']);
            }
            $this->setTemplateData($reply);

            // if coming from edition, display feedback
            if ($this->on_edit_success) {
                $this->data['edit_success'] = "ACCOUNT_DATA_UPDATED";
            } else {
                unset($this->data['edit_success']);
            }
            $ocp_client = new OneClickApiClient();
            $user_oneclick= $ocp_client->isRegistered($reply['email']);
            $this->data['user_oneclick'] = ($user_oneclick) ? '1' : '0';

            // used to display the correct styles and scripts (settings)
            $this->appl = "accounts";
            $this->display_layout('accounts/include/profile_info.html', $this->data, null, null, 'mobile_select');
        } else {
            header('Location: '.bconf_get($BCONF, "*.common.base_url.secure")."/cuenta");
            return 'FINISH';
        }
        return 'FINISH';
    }

    public function accountEdit()
    {
        $this->page_title = lang('MY_ACCOUNT');
        bLogger::logInfo(__METHOD__, "Edit Account Form");

        if (isset($_COOKIE["sq"])) {
            $sq = $_COOKIE["sq"];
            parse_str($sq, $string_query);
            @$this->caller->parse_caller($string_query["ca"]);
            $this->data['ca_region'] = $this->caller->region;
        }
        $account_session = new AccountSession();
        if ($account_session->is_logged()) {
            //	$account_session->reload_account_info();
            $reply=$account_session->getAccountInfo();
            if (isset($reply['password'])) {
                unset($reply['password']);
            }
            $this->headscripts = bconf_get($BCONF, "*.common.edit_account.headscripts");
            $this->headstyles = bconf_get($BCONF, "*.common.edit_account.headstyles");
            if (empty($this->is_edit) || !$this->is_edit) {
                $this->setTemplateData($reply);
            }

            $ocp_client = new OneClickApiClient();
            $user_oneclick= $ocp_client->isRegistered($reply['email']);
            if ($user_oneclick) {
                $respData = $ocp_client->registerData($reply['email']);
                if (isset($respData->subscriber)) {
                    $this->data['user_ocp_ld'] = $respData->subscriber->lastDigits;
                    $this->data['user_ocp_ct'] = $respData->subscriber->cardType;
                }
            }
            $this->data['user_oneclick'] = ($user_oneclick) ? '1' : '0';
        } else {
            header('Location: '.bconf_get($BCONF, "*.common.base_url.secure")."/cuenta");
            return 'FINISH';
        }
        if ($this->on_edit_success) {
            $this->data['edit_success'] = "ACCOUNT_DATA_UPDATED";
        } else {
            unset($this->data['edit_success']);
        }
        $this->data['is_pro_for'] = $account_session->get_param('is_pro_for');
        $this->on_edit_success = false;
        $this->is_edit = false;
        $this->data['appl'] = "accounts";
        $this->display_layout('accounts/edit_account.html', $this->data, null, null, 'mobile_select');
        unset($this->data['err_password_verify']);
        return 'FINISH';
    }

    public function accountUpdate(
        $name,
        $rut,
        $phone,
        $is_company,
        $region,
        $address,
        $contact,
        $lob,
        $commune,
        $password,
        $password_verify,
        $gender,
        $birth_day,
        $birth_month,
        $birth_year,
        $source,
        $phone_type,
        $area_code,
        $business_name,
        $email = ''
    ) {
        bLogger::logInfo(__METHOD__, "Update Account");
        /*
        Check if the session was started
         */
        $account_session = new AccountSession();
        if (!$account_session->is_logged()) {
            bLogger::logInfo(__METHOD__, "a user is trying to do bad things");
            header("Location: /login");
            return 'FINISH';
        } else {
            /*
            Check if the form email and session email are the same
             */
            $account_session_email = $account_session->get_param("email");

            if (!isset($account_session_email) || ($account_session_email != $email)) {
                bLogger::logInfo(__METHOD__, "email ".$account_session_email." is trying to change email ".$email);
                header("Location: /logout?exit=1");
                return 'FINISH';
            }
        }
        $is_pro_for = $account_session->get_param('is_pro_for');
        if (!empty($is_pro_for)) {
            $is_company = 1;
        }

        /* persist the received values that are passed to the template*/
        $this->data['appl'] = "accounts";
        $this->data['name'] = $name;
        $this->data['rut'] = $rut;
        $this->data['email'] = $account_session_email;
        $this->data['phone'] = $phone;
        $this->data['address'] = $address;
        $this->data['contact'] = $contact;
        $this->data['lob'] = $lob;
        $this->data['commune'] = $commune;
        $this->data['is_company'] = $is_company;
        $this->data['region'] = $region;
        $this->data['gender'] = $gender;
        $this->data['birth_day'] = $birth_day;
        $this->data['birth_month'] = $birth_month;
        $this->data['birth_year'] = $birth_year;
        $this->data['phone_type'] = $phone_type;
        $this->data['area_code'] = $area_code;
        $this->data['business_name'] = $business_name;

        //Set this to detect if we are editing the account or displaying the
        //form for the first time
        $this->is_edit = true;

        $trans = new bTransaction();
        $trans->add_data("name", $name);
        $trans->add_data("rut", $rut);

        $passwd_check = password_verify($password, $password_verify, true);
        if ($passwd_check === true) {
            $hash_passwd = hash_password($password);
            $trans->add_data('password', $hash_passwd);
        } else {
            $error_passwd = $passwd_check;
        }

        //Email temporaly not editable
        $trans->add_data("email", $account_session_email);
        $this->data['email'] = $account_session_email;

        if (!empty($phone)) {
            if (!empty($phone_type)) {
                if ($phone_type == "m") {
                    $trans->add_data('phone', "9".$phone);
                } else {
                    $trans->add_data('phone', $area_code.$phone);
                }
            }
        }

        $trans->add_data("is_company", $is_company);
        $trans->add_data("region", $region);
        $trans->add_data("address", $address);
        $trans->add_data("contact", $contact);
        $trans->add_data("lob", $lob);
        $trans->add_data("commune", $commune);
        if (!empty($business_name)) {
            $trans->add_data("business_name", $business_name);
        }
        if (!empty($source)) {
            $trans->add_data('source', $source);
        }

        if (!empty($gender)) {
            $trans->add_data("gender", $gender);
        }

        if (!empty($birth_year) || !empty($birth_month) || !empty($birth_day)) {
            $birth_day_p = (strlen($birth_day) == 1 && (int)$birth_day != 0) ? '0' . $birth_day : $birth_day;
            $birth_month_p = (strlen($birth_month) == 1 && (int)$birth_month != 0) ? '0' . $birth_month : $birth_month;
            $birthdate_p = '';
            if (strlen($birth_year) && strlen($birth_month_p && strlen($birth_day_p))) {
                $birthdate_p = $birth_year . '-' . $birth_month_p . '-' . $birth_day_p;
            }
            $trans->add_data(
                "birthdate",
                (strlen($birthdate_p)) ? $birthdate_p : $birth_year . '-' . $birth_month . '-' . $birth_day
            );
        }

        $reply = $trans->validate_command('edit_account');

        $error_in_form = false;
        $this->on_edit_success = false;
        $update_params = array();

        if ($trans->has_error(true) || $reply['status'] != "TRANS_OK") {
            bLogger::logError(__METHOD__, "Data could not be updated for this account");
            $this->set_errors($trans->get_errors());
            $error_in_form = true;
        }

        if (!empty($error_passwd)) {
            if ($error_passwd == "ERROR_PASSWORD_MISMATCH") {
                $this->data['err_password_verify'] = "ERROR_PASSWORD_MISMATCH";
            } else {
                $this->data['err_password'] = $error_passwd;
            }
            $error_in_form = true;
        }

        if (!$error_in_form) {
            $reply = $trans->send_command('edit_account');
            if ($trans->has_error(true) || $reply['status'] != "TRANS_OK") {
                bLogger::logError(__METHOD__, "Data could not be updated for this account");
                $this->set_errors($trans->get_errors());
                $error_in_form = true;
            }
        }

        if (!$error_in_form) {
            $this->on_edit_success = true;
        }

        /* It's important to reload the info stored in redis! */
        $account_session->reload_account_info();
        $account_session->reload_redis_info();

        // sorry guys, we need some way of knowing if we are in desktop or mobile
        // because the flows of these platforms are different
        if ($source == "web" || $error_in_form) {
            return 'edit';
        } else {
            return 'view';
        }
    }

    public function accountUpdateAjax($phone, $name, $email = '', $is_company = 0)
    {
        header('Content-type: application/json');
        /*
        Check if the session was started
         */
        $account_session = new AccountSession();
        if (!$account_session->is_logged()) {
            echo(json_encode(array(
                'ok'=>false,
                'redirect_url'=>'/login'
            )));
            return 'FINISH';
        } else {
            /*
            Check if the form email and session email are the same
             */
            $account_session_email = $account_session->get_param('email');

            if (!isset($account_session_email) || $account_session_email != $email) {
                echo(json_encode(array(
                    'ok'=>false,
                    'redirect_url'=>'/logout?exit=1'
                )));
                return 'FINISH';
            }
        }

        $trans = new bTransaction();
        $trans->add_data('email', $account_session_email);
        $trans->add_data('phone', $phone);
        $trans->add_data('name', iconv("UTF-8", "ISO-8859-1", $name));

        $reply = $trans->send_command('edit_account_by_param');

        if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
            $errors = array();
            foreach ($trans->get_errors() as $key => $value) {
                $errors[$key] = $value;
            }
            echo(json_encode(array(
                'ok'=>false,
                'error'=>$errors
            )));
            return 'FINISH';
        }

        /* It's important to reload the info stored in redis! */
        $account_session->reload_account_info();
        $account_session->reload_redis_info();

        echo(json_encode(array(
            'ok'=>true
        )));

        return 'FINISH';
    }

    public function accountResult()
    {
        $this->display_layout('accounts/create_account.html', $this->data, null, null, 'mobile_select');
        return 'FINISH';
    }

    public function filterPhone($info)
    {
        if (!empty($info['phone']) && strlen($info['phone']) == 9) {
            $f_char_phone = substr($info['phone'], 0, 1);
            if ($f_char_phone == "9") {
                $this->data['phone_type'] = "m";
                $this->data['area_code'] = "";
                $this->data['phone'] = substr($info['phone'], 1, 8);
            } else {
                $this->data['phone_type'] = "f";
                if ($f_char_phone == "2") {
                    $this->data['area_code'] = "2";
                    $this->data['phone'] = substr($info['phone'], 1, 8);
                } else {
                    $f_char_phone = substr($info['phone'], 0, 2);
                    $this->data['area_code'] = $f_char_phone;
                    $this->data['phone'] = substr($info['phone'], 2, 7);
                }
            }
        }
    }

    private function setTemplateData($info)
    {
        $this->data=$info;
        $this->filterPhone($info);
        $this->data['is_company'] = ($info['is_company'] == 't' ? 1 : 0);
        $this->data['action']='edit';
    }

    private function setPostRedirectReferer($email)
    {
        bLogger::logInfo(__METHOD__, "Check post redirect");
        if (!empty($_COOKIE[bconf_get($BCONF, "*.accounts.login.cookie_referer")])) {
            //Save on Redis
            $rds_mgr = new RedisSessionManager();
            $prefix =  bconf_get($BCONF, "*.accounts.session_redis_prefix");
            $ttl = bconf_get($BCONF, '*.accounts.session_timeout');
            $cookie_value = $_COOKIE[bconf_get($BCONF, "*.accounts.login.cookie_referer")];
            bLogger::logInfo(__METHOD__, "Saving post redirect to {$cookie_value} for {$email}");
            $rds_mgr->rsm_set($prefix.'_redirect_'.$email, $cookie_value, $ttl);
            //KillCookie
            setcookie(
                bconf_get($BCONF, "*.accounts.login.cookie_referer"),
                "",
                time() - 3600,
                '/',
                bconf_get($BCONF, '*.common.session.cookiedomain'),
                false,
                true
            );
        }
    }

    private function getPostRedirectReferer($email)
    {
        bLogger::logInfo(__METHOD__, "Checking to post redirect");
        //Get from Redis
        $rds_mgr = new RedisSessionManager();
        $prefix =  bconf_get($BCONF, "*.accounts.session_redis_prefix");
        $ttl = bconf_get($BCONF, '*.accounts.session_timeout');
        $url_redirect = $rds_mgr->rsm_get($prefix.'_redirect_'.$email);
        $rds_mgr->rsm_del($prefix.'_redirect_'.$email);
        return $url_redirect;
    }

    public function accountConfirm($hash, $email)
    {
        global $BCONF;
        $this->reload_stop = true;
        bLogger::logInfo(__METHOD__, "CONFIRM");

        $rds_mgr = new RedisSessionManager();
        $prefix =  bconf_get($BCONF, "*.accounts.session_redis_prefix");
        $value = $rds_mgr->rsm_get($prefix.'_validate_'.$email);

        if (empty($value) || $value != $hash) {
            bLogger::logError(__METHOD__, "validation key for email $email is not valid");
            $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_INVALID');
            $this->data['show_extended_message'] = 1;
            $this->data['creation_status'] = "ACCOUNT_VALIDATION_INVALID_KEY";
            return 'confirm_result';
        }

        $trans = new bTransaction();
        $trans->add_data("status", 'active');
        $trans->add_data("email", $email);
        $reply = $trans->send_command('manage_account');

        if ($trans->has_error(true) || $reply['status'] != "TRANS_OK") {
            bLogger::logError(__METHOD__, "account email could not be activated for $email user");
            $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_ERROR');
            $this->data['creation_status'] = "ACCOUNT_ACTIVATION_ERROR";
            return 'confirm_result';
        }
        $rds_mgr->rsm_del($prefix.'_validate_'.$email);

        $acc_session = new AccountSession();
        if (!$acc_session->loginSpecial($email, true)) {
            $this->page_name = lang('PAGE_ACCOUNT_CREATE_NAME_ERROR');
            $this->data['creation_status'] = "ACCOUNT_ACTIVATION_ERROR";
            return 'confirm_result';
        }

        $post_redirect_to = $this->getPostRedirectReferer($email);
        if (empty($post_redirect_to)) {
            header('Location: '.bconf_get($BCONF, "*.common.base_url.secure")."/dashboard");
        } else {
            header('Location: '.$post_redirect_to);
        }
        return 'FINISH';
    }

    public function confirmResult()
    {
        if (empty($this->reload_stop)) {
            header('Location: '.bconf_get($BCONF, "*.common.base_url.secure")."/login");
            return 'FINISH';
        }
        $this->data['confirm_result'] = 1;
        $this->data['appl'] = "accounts";

        $this->display_layout('accounts/create_account.html', $this->data, null, null, 'mobile_select');
        return 'FINISH';
    }

    public function filterRut($val)
    {
        $rut = strtoupper(str_replace('.', '', strip_tags($val)));
        if (strlen($rut)==0) {
            return null;
        } else {
            if (strpos($rut, '-') === false) {
                $dv = substr($rut, strlen($rut)-1);
                $r = substr($rut, 0, strlen($rut)-1);
                $rut = $r.'-'.$dv;
            }
        }
        return $rut;
    }

    /**
     * filterTags is used to replace old functions with the same content
     * f_name
     * f_email
     * f_phone
     * f_is_company
     * f_region
     * f_salt
     * f_password
     * f_addres
     * f_contact
     * f_lob
     * f_commune
     * f_uid
     * f_h
     * f_e
     * f_accept_conditions
     * f_or
     * f_gender
     * f_birth_day
     * f_birth_month
     * f_birth_year
     * f_source
     * f_area_code
     * f_phone_type
     *     */
    public function filterTags($val)
    {
        return strip_tags($val);
    }
}
