<?php

require_once('autoload_lib.php');

use Yapo\AccountsAPI as AccountsAPI;

// Syslog
openlog('accounts_api', LOG_ODELAY, LOG_LOCAL0);

$api = new AccountsAPI();
$api->run();
