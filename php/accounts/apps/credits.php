<?php

require_once('autoload_lib.php');
require_once('util.php');
require_once('RedisMultiproductManager.php');
require_once('httpful.php');

// Syslog
openlog("CREDITS", LOG_ODELAY, LOG_LOCAL0);

$credits = new CreditsPage();
$credits->showPage();
