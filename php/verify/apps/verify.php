<?php
require_once('autoload_lib.php');
require_once('util.php');
/*
 * Headers to disable caching
 */
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
 
/*
 * Syslog
 */
openlog("VERIFY", LOG_ODELAY, LOG_LOCAL0);


/*
 * Startup
 */
if (isset($_SERVER["PATH_INFO"])) {
        $action_id = sanitizeVar(substr($_SERVER["PATH_INFO"], 1), 'int'); /* Strip first slash */
} else {
        $action_id = sanitizeVar($_GET["id"], 'int');
}

$ad_id = sanitizeVar($_GET["id"], 'int');

// Response
$response = new bResponse();
$response->add_data('headscripts', array('common.js'));

$status='TRANS_ERROR';

if (preg_match("/^\w{1,40}\z/", $action_id)) {
        /* Use $action_id */

        if (preg_match("/^ad_/", $action_id)) {
                $action_id=substr($action_id, 3);
        }

        // Transaction handler
	$transaction = new bTransaction();
	$transaction->add_client_info();
	$transaction->add_data('action_id', $action_id);
	$transaction->add_data('ad_id', $ad_id);
	$reply = $transaction->send_command("clear", 1);
	$status = handle_reply($reply, $response);

} else {
        $response->add_error('last_error_message', 'ERROR_VERIFY_CODE_INVALID', "");
}

if ($status != 'TRANS_OK' || $response->has_error()) {
        $response->add_data("id", $action_id);
        $response->add_data("content", "vf/verify_error.html");
	$response->add_data('page_title', lang('PAGE_VERIFY_ERROR_TITLE'));
	$response->add_data('appl', 'vf_error');
} else {
	/* Display new subject if it has been changed */
	if (isset($reply["ad_new_subject"])) {
		$response->add_data("ad_subject", $reply["ad_new_subject"]);
	} else {
		$response->add_data("ad_subject", $reply["ad_subject"]);
	}
	
	if (isset($reply['action_type']))
		$response->add_data("action_type",  $reply["action_type"]);

	/* $response->add_data('headmetrics', array('common/head_tracking_scripts.html')); */
	$response->add_data("content", "vf/verify_confirm.html");
	$response->add_data('page_title', lang('PAGE_VERIFY_TITLE'));
	$response->add_data('appl', 'vf');
}
$response->add_data('page_name', lang('PAGE_VERIFY_NAME'));

/*
 * Show results using general templates
 */
$response->show_html_results('html5_base');


/*
 * Handle transaction replys
 */
function handle_reply(&$reply, &$response) {

	if (strpos($reply['status'], ':'))
		list($status, $message) = explode(':', $reply['status']);
	else
		list($status, $message) = array($reply['status'], "");

	syslog(LOG_INFO, log_string()."Retrieved status=$status $message");

        if ($status != 'TRANS_OK' && $status != 'TRANS_ERROR')
                die_with_template(__FILE__, log_string()."Transaction handler exited with errorcode: $status : $message");
        
        if ($status != 'TRANS_OK') {
                foreach ($reply as $key => $value) {
			if (strpos($value, ':'))
				list($code, $message) = explode(':', $value);
			else
				list($code, $message) = array($value, "");
                        if ($status != 'TRANS_OK' && is_error($code))
                                $response->add_error('err_'.$key, $code, $message);
                        $response->add_error('last_error_message', $code, $message);
                }
                
                if ($status != 'TRANS_OK' && !$response->has_error())
                        die_with_template(__FILE__, log_string()."Transaction handler returns unkown errors, errorcode: $status : $message");
        }
        return $status;
}

?>
