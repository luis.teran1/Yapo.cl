<?php

require_once('autoload_lib.php');
require_once('init.php');

// Syslog
openlog("SENDPASS", LOG_ODELAY, LOG_LOCAL0);

$session = new bSession();
$transaction = new bTransaction();
$response = new bResponse();

// Page steps
define('AD_NOT_FOUND',  0);
define('SENDPASS_FORM', 1);
define('SENDPASS_SENT', 2);

// Page templates
$page[AD_NOT_FOUND] = "list/view_missing.html";
$page[SENDPASS_FORM] = "sp/sendpass_page.html";
$page[SENDPASS_SENT] = "sp/sendpass_page_sent.html";

// Default step
$step = SENDPASS_FORM;
$recovery_type= '';
$css = "accounts.css";

// Ad id
if (!isset($_GET['id']) || !is_numeric($_GET['id']))
	$step = AD_NOT_FOUND;
if (isset($_GET['recovery_type']))
	$recovery_type = $_GET['recovery_type'];
	
// Post handling
if ((isset($_POST['sendpass']) || isset($_POST['email'])) && $step == SENDPASS_FORM) {
	$transaction->add_data('email', $_POST['email']);
	$transaction->add_data('id', $_GET['id']);
	$transaction->add_data('ip', $_SERVER['REMOTE_ADDR']);

	if (isset($_POST['recovery_type']))
		$transaction->add_data('recovery_type',$_POST['recovery_type']);

	$transaction->add_data('log_string', log_string());
	$reply = $transaction->send_command("sendpass");

	// Retrieve status
	if (strpos($reply['status'], ":")) {
		list ($status, $message) = explode(":", $reply['status']);
	} else {
		$status = $reply['status'];
		$message = "";
	}
	syslog(LOG_INFO, log_string()."Retrieved status=$status $message");
	if ($status == 'TRANS_OK') {
		$step = SENDPASS_SENT;
		syslog(LOG_INFO, log_string()."Sending password to {$_POST['email']} for ad id {$_GET['id']}");
	} else if ($status == 'TRANS_ERROR') {
		$step = SENDPASS_FORM;
		syslog(LOG_INFO, log_string()."DENIED: User requested password with email {$_POST['email']} for ad id {$_GET['id']}");
	} else die_with_template(__FILE__, log_string()."Transaction handler exited with errorcode: $status");

	// Handle reply
	foreach ($reply as $key => $str) {
		// Split the string into code and message
		if (strpos($str, ":")) {
			list($error_code, ) = explode(":", $str);
		} else {
			$error_code = $str;
		}

		// Ad not found in database
		if ($error_code == 'ERROR_AD_NOT_FOUND')
			syslog(LOG_INFO, log_string()."ERROR: Ad id {$_GET['id']} doesn't not exist in database");
		
		// Errors
		if ($key != 'status' && is_error($error_code) && $status == 'TRANS_ERROR')
			$response->add_error("err_" . $key, $error_code);
	}

	// Override id errors
	if (isset($reply['id']))
		$step = AD_NOT_FOUND;

	// Store extra values in response
	$response->add_data('id', $_GET['id']);
	$response->add_data('email', $_POST['email']);
}

// Page title
if ($step == SENDPASS_FORM) {
	$response->add_data('page_title', lang('PAGE_SENDPASS_FORM_TITLE'));
	$response->add_data('page_name', lang('PAGE_SENDPASS_FORM_NAME'));
} 
if ($step == SENDPASS_SENT) {
	$response->add_data('page_title', lang('PAGE_SENDPASS_SENT_TITLE'));
	$response->add_data('page_name', lang('PAGE_SENDPASS_SENT_NAME'));
}
// Show result
$response->add_data('content', $page[$step]);
$response->add_data('recovery_type',$recovery_type);
$response->add_data('headstyles', $css);
$response->add_data('REMOTE_ADDR', $_SERVER['REMOTE_ADDR']);
$response->show_html_results("html5_base");
?>
