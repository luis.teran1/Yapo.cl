<?php

namespace Yapo;

use Yapo\Logger;
use Yapo\Bconf;

/**
* PackApp
* applications used by dashboard's pack section
*/
class PackApp
{

    private $session_prefix;
    private $trans;
    private $redis;

    public function __construct($trans = null, $redis = null, $redisMP = null)
    {
        $this->trans = $trans;
        $this->redis = $redis;
        $this->redisMP = $redisMP;
        $this->prefix = Bconf::get($BCONF, '*.accounts.session_redis_prefix');
        $this->session_prefix = $this->prefix . Bconf::get($BCONF, '*.accounts.session_redis_key_prefix');
        $this->cookie_name = Bconf::get($BCONF, '*.accounts.session_cookie_name');
        $redis_conf = Bconf::get($BCONF, "*.redis.session.host.rsd1");
        if (!is_null($redis)) {
            $this->redis->connect($redis_conf['name'], $redis_conf['port'], $redis_conf['timeout']);
        }
    }


/**
* getRecommended
* Get the recommended pack depends on user's total ads
*/
    public function getRecommended($url_data, $request_data)
    {
        $session_id = $_COOKIE[$this->cookie_name];
        $session_email = $this->redis->hGet($this->session_prefix.$session_id, 'email');
        $session_user_id = $this->redis->hGet($this->prefix.sha1($session_email), 'user_id');
        $session_account_id = $this->redis->hGet($this->prefix.sha1($session_email), 'account_id');
        $session_pack_type = $this->redis->hGet($this->prefix.sha1($session_email), 'pack_type');
        $session_pack_slots = $this->redis->hGet($this->prefix.sha1($session_email), 'pack_slots');
        $session_pack_status = $this->redis->hGet($this->prefix.sha1($session_email), 'pack_status');
        $command = Bconf::get($BCONF, '*.accounts.dashboard.transaction');
        $trans = $this->trans->reset();
        $trans->add_data('user_id', $session_user_id);
        $trans->add_data('ads_quantity', 100);
        $trans->add_data('page_number', 0);
        $result = $trans->send_command($command);
        if ($trans->has_error(true)) {
            Logger::logError(__METHOD__, print_r($trans->get_errors(), true));
            $response['status'] = "error";
        } else {
            $recommended = $this->getRecommendedSlots($result['n_ads'], 0.1);
            if (($session_pack_slots >= intval($recommended)) ||
                ($session_pack_status == 'expired' && $result['n_ads'] == 0)) {
                $trans = $this->trans->reset();
                $trans->add_data('account_id', $session_account_id);
                if ($session_pack_status == 'expired') {
                    $trans->add_data('list_all', 1);
                }
                $result = $trans->send_command("get_packs_by_account");
                if ($trans->has_error(true)) {
                    Logger::logError(__METHOD__, print_r($trans->get_errors(), true));
                    $response['status'] = "error";
                } else {
                    if (isset($result['slots'])) {
                        if (count($result['slots']) > 1) {
                            $newest_pack_slots = $result['slots'][0];
                        } else {
                            $newest_pack_slots = $result['slots'];
                        }
                        if ($newest_pack_slots > 1) {
                            $recommended = $this->getRecommendedSlots($newest_pack_slots-1, 0);
                        } else {
                            $recommended = $this->getRecommendedSlots($newest_pack_slots, 0);
                        }
                    }
                }
            }

            if ($session_pack_type == 'cars') {
                $price = Bconf::get($BCONF, '*.payment.product.11'.$recommended.'.price');
                $name = Bconf::get($BCONF, '*.payment.product.11'.$recommended.'.description');
            } else {
                $price = Bconf::get($BCONF, '*.payment.product.21'.$recommended.'.price');
                $name = Bconf::get($BCONF, '*.payment.product.21'.$recommended.'.description');
            }

            $description_1 = Bconf::get($BCONF, '*.language.PACK_RECOMMENDED_DESCRIPTION_1.es');
            $description_2 = Bconf::get($BCONF, '*.language.PACK_RECOMMENDED_DESCRIPTION_2.es');

            $response['price'] = $price;
            $response['name'] = 'Pack '.$name;
            $response['description'] = array(
                str_replace('{slots}', intval($recommended), $description_1),
                $description_2
            );
            $response['status'] = "ok";
        }
        return $response;
    }

/**
* getRecommendedSlots
* Get recommended pack according to available slots or total ads
*/
    private function getRecommendedSlots($total, $factor)
    {
        if ($total > 99) {
            $recommended = '125';
        } elseif ($total > 74) {
            $recommended = '100';
        } elseif ($total > 49) {
            $recommended = '075';
        } else {
            $recommended = '0'.ceil($total/10+$factor)*10;
        }
        return $recommended;
    }

/**
* adStatus
* Changes the status of a pack ad between active and disabled
*/
    public function adStatus($url_data, $request_data)
    {

        $session_id = $_COOKIE[$this->cookie_name];
        $session_email = $this->redis->hGet($this->session_prefix.$session_id, 'email');
        $trans = $this->trans->reset();
        $trans->add_data('log_string', log_string());
        $trans->add_data('list_id', $request_data->list_id);
        $trans->add_data('email', $session_email);
        $trans->add_data('new_status', $request_data->status);
        $trans->add_data('remote_addr', get_remote_addr());
        $result = $trans->send_command('pack_change_ad_status');
        if ($trans->has_error(true)) {
            $response = array('available_slots' => 0, 'status' => 'error');
            Logger::logError(__METHOD__, "status couldnt be changed");
            Logger::logError(__METHOD__, print_r($trans->get_errors(), true));
        } else {
            $response = array('available_slots' => intval($result['available_slots']), 'status' => 'ok');
            if ($request_data->status == 'disabled') {
                $ad_id = $result['ad_id'];
                $cart_mngr = $this->redisMP;
                $cart_mngr->removeAllAdProducts($session_email, $ad_id);
                $cart_mngr->removeAllAdProducts($session_email, $request_data->list_id);
            } else {
                $this->redisMP->removeInsertingFeeProducts($session_email, $result['ad_id']);
                $response['update_cart'] = '1';
            }

            // Checking if the ad is published in the index to return the url
            $result = bsearch_search_vtree(Bconf::get($BCONF, "*.common.asearch"), "id:$request_data->list_id");
            if (!empty($result)) {
                $base_url = Bconf::get($BCONF, "*.common.base_url.vi");
                $response['url'] = $base_url . "/vi/" . $request_data->list_id . ".htm";
                ;
                $response['premium_products'] = 'active';
            }
        }
        return $response;
    }

/**
* supportEmail
* Sends an email to the support team
*/
    public function supportEmail($url_data, $request_data)
    {

        $session_id = $_COOKIE[$this->cookie_name];
        $session_email = $this->redis->hGet($this->session_prefix.$session_id, 'email');
        $body = "Un usuario de pack requiere asistencia telef�nica\n"
                . "\nDatos del usuario: "
                . "\nMail de esta cuenta de usuario: " . $session_email
                . "\nNombre: " . $request_data->name_contact
                . "\nTel�fono: " . $request_data->phone_contact
                . "\nMail: " . $request_data->mail_contact;

        $trans = $this->trans->reset();
        $trans->add_data('email', $request_data->mail_contact);
        $trans->add_data('name', $request_data->name_contact);
        $trans->add_data('support_subject', '41');
        $trans->add_data('support_body', $body);
        $trans->send_command('support', true, true);

        if ($trans->has_error(true)) {
            $response = array('status' => 'error');
            Logger::logError(__METHOD__, "support email was not sent");
            Logger::logError(__METHOD__, print_r($trans->get_errors(), true));
        } else {
            $response = array('status' => 'ok');
        }
        return $response;
    }
}
