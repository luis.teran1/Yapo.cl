<?php

require_once 'autoload_lib.php';

// Syslog
openlog('pack_api', LOG_ODELAY, LOG_LOCAL0);

class pack_api extends bRESTful {
	function __construct() {
		$this->restAPISettingsKey = "*.pack_handler_rest_settings";
	}
}

$api = new pack_api();
$api->run();
