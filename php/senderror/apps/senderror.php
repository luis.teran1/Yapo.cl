<?php
/* die_with_template ignores showing the formular */
define('ERROR_PAGE', 1);

require_once('autoload_lib.php');
require_once('init.php');

/*
 * Syslog
 */
openlog("SENDERROR", LOG_ODELAY, LOG_LOCAL0);

$session = new bSession();
$transaction = new bTransaction();
$response = new bResponse();

/*
 * Page to display
 */
define('FORM', 'common/error_page.html');
define('TEMPORARY', 'common/error_page_temporary.html');

// Default page
$page = FORM;

// Mod security error
if (isset($_GET['ms'])) {
	switch($_GET['ms']) {
		case '70007':
			$page = TEMPORARY;
			break;
	}
}

/*
 * Syslog entry
 */
if (isset($_GET['ms']))
	syslog(LOG_INFO, log_string()."From MOD_SECURITY");
if (isset($_SERVER['HTTP_REFERER']))
	syslog(LOG_INFO, log_string()."Referer: ".$_SERVER['HTTP_REFERER']);

/*
 * Post handling
 */
if (isset($_POST['send'])) {
	if (isset($_POST['error_id'])) {
		$transaction->add_data('error_id', $_POST['error_id']);

		$body  = "USER IP: {$_SERVER['REMOTE_ADDR']}\n";
		$body .= "SERVERNAME: {$_SERVER['SERVER_NAME']}\n";
		$body .= "HOSTNAME: {$_SERVER['HOSTNAME']}\n";
		$body .= "SESSIONID: {$session->session_id}\n";
		$body .= "ERRORMESSAGE: {$_SESSION['last_error_message']}";
		if (isset($_POST['ms'])) {
			$mslog = $GLOBALS['__memcached_client']->get(session_id() . '_mod_sec', true);
			if ($mslog) {
				$body .= $mslog;
				$GLOBALS['__memcached_client']->delete(session_id() . '_mod_sec', true);
			}
		}

		// Unset old error message
		unset($_SESSION['last_error_message']);

		$transaction->add_data('body', $body);
		$transaction->add_data('to', $BCONF['senderror']['recipient']);

		$transaction->add_data('log_string', log_string());
		$reply = $transaction->send_command("senderror");

		if ($reply['status'] != TRANS_OK)
			syslog(LOG_ERR, log_string()."ERROR: senderror transaction failed ".$reply['status']);
	}
	$ca = "";
	if (isset($_GET['ca'])) {
		$ca = "&ca=".$_GET['ca'];
	}
	$response->redirect("/support/form/0?l=0&id=3".$ca);
} 

// Show the error page
$response->add_data('page_title', lang('PAGE_ERROR_TITLE'));
$response->add_data('page_name', lang('PAGE_ERROR_NAME'));
$response->add_data('noinsight', 1);
if (isset($_GET['ms'])) {
	$response->add_data('ms', 1);
	if (bconf_get($BCONF, "senderror.display_mod_sec")) {
		$mslog = $GLOBALS['__memcached_client']->get(session_id() . '_mod_sec', true);
		if ($mslog)
			$response->add_data('ms_log', $mslog);
	}
}

/* Template */
$response->add_data('content', $page);
$response->show_html_results();
?>
