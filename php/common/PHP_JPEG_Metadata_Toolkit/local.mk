TOPDIR?=../../..

include ${TOPDIR}/mk/commonsrc.mk
VPATH:=${COMMONSRCDIR}:${VPATH}

INSTALLDIR=/include
INSTALL_TARGETS=EXIF.php EXIF_Tags.php JPEG.php EXIF_Makernote.php PIM.php Unicode.php IPTC.php Photoshop_IRB.php XMP.php pjmt_utils.php XML.php
SUBDIR=Makernotes
include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
