<?php
namespace Yapo;

use ProxyClient;

class HomeLinksService
{
    private $Host;
    private $Port;
    private $Proxy;
    private $Conf;


    public function __construct($proxyClient = null)
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.home_links_service.host");
        $this->Port = Bconf::get($BCONF, "*.home_links_service.port");
        $this->Conf = Bconf::get($BCONF, "*.home_links_service");
        $this->Proxy = isset($proxyClient)? $proxyClient : new ProxyClient();
    }

    private function replacePathValues($data, $path)
    {
        if (strpos($path, "$") === false) {
            return $path;
        }
        foreach ($data as $k => $v) {
            $path = str_replace("$".$k, $v, $path);
        }
        return $path;
    }

    private function getPath($action, $data = null)
    {
        if (empty($data)) {
            return $this->Conf[$action]["path"];
        }

        return $this->replacePathValues($data, $this->Conf[$action]["path"]);
    }

    private function getMethod($action)
    {
        return $this->Conf[$action]["method"];
    }


    private function callToService($action, $data = null)
    {
        $headers = array("Content-Type" => "application/json");
        $response = $this->Proxy->doRequest(
            $this->getMethod($action),
            $this->Host,
            $this->Port,
            $this->getPath($action, $data),
            $data,
            $headers
        );

        if ($response->code == 500) {
            Logger::logError(__METHOD__.'::'.$action, "Api error:". var_export($response, true));
            return null;
        }

        $json_response = json_decode($response);
        Logger::logDebug(__METHOD__.'::'.$action, "Response proxy:". var_export($json_response, true));

        return $json_response;
    }

    public function checkService()
    {
        $check = $this->callToService("healthcheck");
        if (!empty($check) && $check->status == "OK") {
            return true;
        }
        return false;
    }


    public function allGroups()
    {
        return $this->callToService("all_groups");
    }

    public function singleGroup($group)
    {
        $group = rawurlencode($group);
        return $this->callToService("single_group", array("group"=>$group));
    }

    public function delete($group, $id)
    {
        $group = rawurlencode($group);
        return $this->callToService("delete", array("group"=>$group, "id"=>$id));
    }

    public function create(array $data)
    {
        return $this->callToService("create", $data);
    }

    public function edit(array $data)
    {
        $data["group"] = rawurlencode($data["group"]);
        return $this->callToService("edit", $data);
    }

    public function export()
    {
        $url = $this->Host.":".$this->Port.$this->getPath("export");
        return file_get_contents($url);
    }

    public function import($filePath)
    {
        $url = $this->Host.":".$this->Port.$this->getPath("import");
        // initialise the curl request
        $request = curl_init($url);
        // send a file
        curl_setopt($request, CURLOPT_POST, true);
        curl_setopt($request, CURLOPT_POSTFIELDS, file_get_contents($filePath));
        curl_setopt($request, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
        $message = curl_exec($request);
        curl_close($request);
        return $message;
    }
}
