<?php

namespace Yapo;

use AccountSession;
use bTransaction;

class DuplicatedAd
{
    private $account_session;
    private $trans;

    public function __construct($account_session = null, $trans = null)
    {
        $this->account_session = is_null($account_session) ? new AccountSession(): $account_session;
        $this->trans = is_null($trans) ? new bTransaction(): $trans;
    }

    public function shouldCheck($ad_category, $skip_duplicated = "0")
    {
        if ($skip_duplicated == "1") {
            return false;
        }
        /*
        When the user is pro for a pack configured category
        also if the category is enable for DTB
        and that the user has active packs
        ... only them you'll have my permision to skip DTB
        */
        return should_search_dups(
            $this->account_session->get_param('is_pro_for'),
            $this->account_session->get_param('pack_status'),
            $ad_category
        );
    }

    public function check($array_data)
    {
        if (1 == Bconf::get($BCONF, "*.delete2bump.deactivated.duplicate.enable")) {
            $is_duplicated = $this->isDuplicated($array_data, 'deactivated');
            if (isset($is_duplicated['has_duplicates']) && $is_duplicated['has_duplicates'] == "1") {
                return $is_duplicated;
            }
        }

        if (1 == Bconf::get($BCONF, "*.delete2bump.disabled.duplicate.enable")) {
            $is_duplicated = $this->isDuplicated($array_data, 'disabled');
            if (isset($is_duplicated['has_duplicates']) && $is_duplicated['has_duplicates'] == "1") {
                return $is_duplicated;
            }
        }
        return array("has_duplicates" => "0");
    }

    public function isDuplicated($array_data, $status)
    {
        /*** Trans call to verify if an ad is suspect of being a duplicated. */
        $transaction = $this->trans->reset();

        /* UID checking */
        if (!empty($_COOKIE['uid'])) {
            // only use the cookie UID when it has a valid value
            $cookie_uid = (int)$_COOKIE['uid'];
            if ($cookie_uid > 0) {
                $transaction->add_data('uid', $cookie_uid);
            } else {
                unset($_COOKIE['uid']);
            }
        }

        $ad_email = $array_data['email'];
        $ad_subject = $array_data['subject'];
        $ad_body = $array_data['body'];

        if (array_key_exists("category", $array_data)) {
            $ad_category = $array_data['category'];
        } else {
            $ad_category = $array_data['category_group'];
        }
        $ad_price = 0;
        if (!empty($array_data['price'])) {
            if (!empty($array_data['currency']) && (strcmp($array_data['currency'], 'uf') === 0)) {
                $ad_price = intval(str_replace(",", ".", $array_data['price']) * 100.00);
            } else {
                $ad_price = $array_data['price'];
            }
        }
        $ad_currency = 'peso';
        if (!empty($array_data['currency'])) {
            $ad_currency = $array_data['currency'];
        }

        $optionals = array('ad_id', 'type', 'region');
        foreach ($optionals as $key) {
            if (!empty($array_data[$key])) {
                $transaction->add_data($key, $array_data[$key]);
            }
        }

        $transaction->add_data('email', $ad_email);
        $transaction->add_data('ad_subject', $ad_subject);
        $transaction->add_data('ad_body', $ad_body);
        $transaction->add_data('ad_category', $ad_category);
        $transaction->add_data('ad_price', $ad_price);
        $transaction->add_data('ad_currency', $ad_currency);
        $transaction->add_data('ad_status', $status);
        $response = $transaction->send_command('duplicated_ads', true, false);

        if (!$transaction->has_error()) {
            // Add url and prod info
            $base_payment = Bconf::get($BCONF, "*.common.base_url.payment");
            $product_price = Bconf::getProductPrice($BCONF, 1, $ad_category);
            $product_url = "{$base_payment}/dashboard";
            if (!empty($response["list_id"])) {
                $product_url =  "{$base_payment}/pagos?id={$response["list_id"]}&prod=1";
            }
            $response["products"] = array("bump" => array("price" => $product_price, "url" => $product_url));
            $mayus_status = strtoupper($status);
            $response["ad_status"] = Bconf::get($BCONF, "*.language.DASHBOARD_AD_STATUS_{$mayus_status}.es");
            // clean private info
            unset($response["ad_id"]);
            unset($response["status"]);
        }
        return $response;
    }
}
