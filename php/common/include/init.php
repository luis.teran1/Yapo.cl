<?php

/* SERVER ROOT */
if (!empty($_SERVER['DOCUMENT_ROOT']))
	define('SERVER_ROOT', substr($_SERVER['DOCUMENT_ROOT'], 0, strrpos($_SERVER['DOCUMENT_ROOT'], '/')));

/*
 * BCONF LANGUAGE
 */
setlocale(LC_ALL, $GLOBALS['BCONF']['*']['common']['locale']);
function lang($code) {
	$code_array = explode('.', $code);
	$message = $GLOBALS['BCONF']['*']['language'];

	/* XXX: WTF */
	foreach($code_array as $code_key)
		$message = @$message[$code_key];
	if ($message) {
		$locale = substr($GLOBALS['BCONF']['*']['common']['locale'], 0, 2);
		return $message[$locale];
	}

	return $code;
}

/*
 * Warning / error checking
 */
function is_error($code) {
	@list($c, $m) = explode(":", $code);
	return preg_match("/^[A-Z_;]*ERROR_[A-Z_0-9]+[A-Z]$/", $c);
}

function is_warning($code) {
	if (!isset($code)) {
		return null;
	}
	$c = explode(":", $code);

	return preg_match("/^WARN_[A-Z_0-9]+[A-Z]$/", $c[0]);
}

/*
 * Get log string
 */
function log_string() {
	$log = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
	if (strlen(session_id()))
		$log .= ' '.session_id();
	else if (isset($_COOKIE['s']))
		$log .= ' '.$_COOKIE['s'];
	else if (isset($_SERVER['REMOTE_PORT']))
		$log .= ' ' . $_SERVER['REMOTE_PORT'];

	if (isset($_SESSION['ad_type']))
		$log .= " [{$_SESSION['ad_type']}]";

	return $log.' - ';
}

/* Check if BCONF is available */
if (!isset($BCONF))
	die_with_template(__FILE__, "Bconf is not initialized!");

/* Layout manager */
if (!isset($_GET['l']) || !is_numeric($_GET['l']) || !array_key_exists($_GET['l'], $BCONF['*']['common']['layout']))
	$_GET['l'] = 0;

//incPrometheusMetric
//String -> void
//Takes a String as argument
//Exits without returning
//This function takes a String as argument,
//connects to redis (using the php-redis extension)
//to increas a redis counter named as the argument
//in the redis hash prometheus_metrics 
function incPrometheusMetric($hash_key){
  $redisConfGen = bconf_get($BCONF, "*.common.redis_prometheus");
  $redisConfCon = $redisConfGen['host']['rdb1'];
  $redisConfdb  = 0;
  $redis = new Redis();
  $redis->connect($redisConfCon['name'], $redisConfCon['port'], $redisConfCon['timeout']);
  $redis->select($redisConfdb);
  $redis_hash_name = 'prometheus_metrics';
  $redis->hIncrBy($redis_hash_name, $hash_key, 1);
  $redis->close();
}

function send_technical_error_mail($called_by = "", $message = "") {

	// Hide php
	if (preg_match("/.php$/", $called_by))
		$called_by = substr($called_by, 0, -4);

	// Create an error id
	list($micro_sec, $sec) = explode(" ", microtime());
	$time = (float)$micro_sec + (float)$sec;
	$error_id = substr($_SERVER['SCRIPT_NAME'], 1)."_".date("Y-m-d_H:i:s");

	//Prometheus
	//Uses the script name as the error type for a metric
	try{
		if (bconf_get($BCONF, "*.redis_prometheus.enabled")){		
  			incPrometheusMetric($_SERVER['SCRIPT_NAME']);
		}
	}
	catch(Exception $e){}

	// Log the errors
	$error_data  = "###### ERROR ID : $error_id\n";
	$error_data .= "###### ERROR CALLED BY : $called_by\n";
	$error_data .= "###### ERROR MESSAGE : $message\n";
	$error_data .= '###### STACK TRACE: ' . print_r(debug_backtrace(), true) . "\n";

	if ( isset($GLOBALS['dbg_trans_reply']) ) {
		$error_data .= '###### TRANS REPLY: ' . print_r($GLOBALS['dbg_trans_reply'], true) . "\n";
		unset($GLOBALS['dbg_trans_reply']);
	}

	// No one knows why the GLOBALS dump [is/should be] useful... - Zane
	$error_data .= "###### GLOBALS : \n"; //.print_r($GLOBALS, true);

	// Output to file
	$dump_file_name = SERVER_ROOT."/logs/$error_id.gz";
	if( !file_exists($dump_file_name) ){
		$error_file = gzopen($dump_file_name, 'w');
		if($error_file === false){
			syslog(LOG_CRIT, "$message - Critical error: couldn't write - called by: $called_by");
		} else {
			fwrite($error_file, $error_data);
			fclose($error_file);
			syslog(LOG_CRIT, "$message - Critical error dumped: $error_id By: $called_by");
		}
	} else {
		syslog(LOG_CRIT, "$message - Critical error: dump already exists - Error by: $called_by");
	}

	// Save last error message in session
	$_SESSION['last_error_message'] = $message;

	/*** BEGIN_SEND_ERROR ***/
	if( isset($GLOBALS['BCONF']) ){
		require_once('bTransaction.php');
		require_once('bResponse.php');

		$transaction = new bTransaction();
		$response = new bResponse();

		$mail_body = "PHP DUMP FILE: {$dump_file_name}\n";
		$mail_body .= "USER IP: {$_SERVER['REMOTE_ADDR']}\n";
		$mail_body .= "SERVERNAME: {$_SERVER['SERVER_NAME']}" . ' - IP: ' . $_SERVER['SERVER_ADDR'] . "\n";
		$mail_body .= "HOSTNAME: {$_ENV['HOSTNAME']} ({$_SERVER['SERVER_ADDR']})\n";
		$mail_body .= 'SESSIONID: '. session_id() . "\n";
		$mail_body .= "ERRORMESSAGE: {$_SESSION['last_error_message']}";
		#if( isset($_POST['ms']) ){
		#       $mslog = $GLOBALS['__memcached_client']->get(session_id() . '_mod_sec', true);
		#if($mslog){$body.=$mslog;$GLOBALS['__memcached_client']->delete(session_id().'_mod_sec',true); }
		#}

		unset($_SESSION['last_error_message']);

		$transaction->add_data('error_id', $error_id);
		$transaction->add_data('body', $mail_body);
		$transaction->add_data('to', $GLOBALS['BCONF']['senderror']['recipient']);
		$transaction->add_data('log_string', log_string());

		$reply = $transaction->send_command("senderror");
		if($reply['status'] != 'TRANS_OK'){
			syslog(LOG_ERR, log_string() .'ERROR: senderror transaction failed: ' . $reply['status']);
		}
	}
	/*** END_SEND_ERROR ***/
}

/*
 * Die function and show error template
 */
function die_with_template($called_by = "", $message = "", $error_template = "common/error_page.html") {
	global $b_caller, $b_place, $b_city, $b_type; /* BACKWARD_COMPAT */

	if (defined('ERROR_PAGE')) return;

	send_technical_error_mail($called_by, $message);

	// Show the error page
	if (isset($_REQUEST['ca'])) {
		$data['ca']  = $_REQUEST['ca'];
		if (preg_match("/[1-9]+_[1-9]+_[skubh]/", $data['ca']))
			list ($data['ca_region'], $data['ca_city'], $data['ca_type']) = explode('_', $data['ca']);
		else if (preg_match("/[1-9]+_[skubh]/", $data['ca']))
			list ($data['ca_region'], $data['ca_type']) = explode('_', $data['ca']);
	}

	$data['content'] = $error_template; 
	if (isset($GLOBALS['BCONF'])) {
		$data['page_title'] = lang('PAGE_ERROR_TITLE');
		$data['page_name'] = lang('PAGE_ERROR_NAME');
	}
	$data['error_id'] = $error_id;
	$data['l'] = $data['layout'] = intval($_GET['l']);

	/* BACKWARD_COMPAT */
	$data['b_caller'] = $b_caller;
	$data['b_place'] = $b_place;
	$data['b_city'] = $b_city;

	if (isset($_REQUEST['ca']))
		$data['query_string'] = "ca=".$_REQUEST['ca'];

	if (isset($_GET[session_name()])) {
		if (isset($data['query_string']))
			$data['query_string'] .= "&";
		$data['query_string'] .= session_name()."=".$_GET[session_name()];
	}

	/*Is user logged in*/
	if (isset($_SESSION['global']) && isset($_SESSION['global']['store_id']))
		$data['store_logged_in'] = 1;

	$data['noinsight'] = 1;

	if (!isset($GLOBALS['BCONF'])) {
		call_template($data, array(), array(), "common/general0.html", default_template_options());
	} else {
		call_template($data, array(), $GLOBALS['BCONF']['*'], "common/general".$GLOBALS['BCONF']['*']['common']['layout'][$_GET['l']]['template'].".html", default_template_options());
	}

	// Exit script
	session_write_close();
	exit(1);
}

/* Clean up newlines from strings */
function clean_and_trim($value) {
	$value = ltrim($value);
	$value = rtrim($value);
	$value = strip_tags($value);

	$value = str_replace("&nbsp;", " ", $value);
	$value = str_replace("\r\n", "\n", $value);
	$value = str_replace("\r", "\n", $value);
	$value = preg_replace("/[\n]{3,}/", "\n\n", $value);

	return $value;
}

/* Clean up newlines from strings */
function clean_and_trim_byref(&$value) {
	$value = clean_and_trim($value);
}


/* Clean up blob */
function clean_blob($value) {
	$value = ltrim($value);
	$value = rtrim($value);

	return $value;
}


/* Array walk recursive, From PHP 5 */
if (!function_exists('array_walk_recursive')) {
	function array_walk_recursive(&$elem, $funcname) {
		if (!is_array ($elem)) {
			call_user_func($funcname, $elem);
		} else {
			foreach ($elem as $key => $value)
				array_walk_recursive($value, $funcname);
		}

		return $elem;
	}
}

/* Extending array search for multiple dimension arrays */
function array_search_recursive($needle, $haystack, $trail = NULL) {
	foreach ($haystack as $key => $value) { 
		if (is_array($value)) {
			$match = array_search_recursive($needle, $value, $trail); 
			if ($match) {
				$trail[] = $key;
				$trail = array_merge($trail, $match); 
			}
		}

		if ($value == $needle) 
			$trail[] = $key;
	} 

	if ($trail) 
		return $trail; 

	return false; 
}

/* Extract info from caller */
function explode_ca($ca) {
	if (!preg_match("/^[[:digit:]]+_([[:digit:]]+_)?[[:alpha:]]+$/", $ca))
		$ca = $GLOBALS['BCONF']['*']['common']['default']['caller'];
	
	if (preg_match("/^[[:digit:]]+_[[:digit:]]+_[[:alpha:]]+$/", $ca)) {
		list($region, $city, $type) = explode("_", $ca);
	} else if (preg_match("/^[[:digit:]]+_[[:alpha:]]+$/", $ca)) {
		list($region, $type) = explode("_", $ca);
		$city = 0;
	}
	
	$region = (int)$region;
	$city = (int)$city;
	
	return array($region, $city, $type);
}

/* Implodes region, city, type and creates caller */
function implode_ca($region, $city, $type) {
	$ca = $region;

	if ($city > 0)
		$ca .= '_'.$city;

	$ca .= '_'.$type;
	
	
	return $ca;
}

/* BCONF API */
function bconf_get(&$array, $conf) {
	if (empty($array))
		return NULL;

	if (empty($conf))
		return NULL;

	$conf = explode('.', $conf);

	foreach($conf as $key) {
		if (!is_array($array)) {
			error_log("bconf_get() $array is not an array. Might be misconfigured bconf by a premature leaf node for $conf at $key.");
		}
		if (is_array ($array) && isset($array[$key])) {
			$array = &$array[$key];
		} else return NULL;
	}

	return $array;
}

function http_readfile($file, $path, $retdata = false, $chunk_size = 0) {
	global $BCONF;
	$dav_host = bconf_get($BCONF, '*.common.dav.host.1.name');

	foreach(bconf_get($BCONF, '*.common.dav.host') as $key => $value) {
		$dav_host = $value['name'];
		if (isset($value['get_port']))
			$dav_host .= ':' . $value['get_port'];
		$url = 'http://' . $dav_host . '/' . $path . '/' . $file;
		if ($retdata) {
			if (($buf = file_get_contents($url)))
				return $buf;
		} elseif ($chunk_size > 0) {
			readfile_chunked($url, $chunk_size);
			return true;
		} elseif (readfile($url))
			return true;
	}

	return false;
}

function http_sendfile($file, $path, $headers = null) {
	global $BCONF;
	$dav_host = bconf_get($BCONF, '*.common.dav.host.1.name');

	foreach(bconf_get($BCONF, '*.common.dav.host') as $key => $value) {
		$dav_host = $value['name'];
		if (isset($value['get_port']))
			$dav_port = $value['get_port'];
		else
			$dav_port = 80;
		$sock = fsockopen($dav_host, $dav_port);
		if ($sock) {
			fwrite($sock, "GET /$path/$file HTTP/1.0\r\n" .
				      "Host: $dav_host\r\n\r\n");
			while (($head = fgets($sock))) {
				if ($head == "\r\n")
					break;
				if ($headers) {
					$h = explode(':', $head, 2);
					if (!in_array(strtolower($h[0]), $headers))
						continue;
				}
				header(substr($head, 0, -2));
			}
			fpassthru($sock);
			return true;
		}
	}
	return false;
}

function array_copy(&$arr) {
	foreach ($arr as $key => $value) {
		if (is_array($value)) {
			$new_arr[$key] = array_copy($value);
		} else {
			$new_arr[$key] = $value;
		}
	}

	return $new_arr;
}

/* Get max_images from bconf */
function get_category_max_extra_images($category) {
	global $BCONF;

	if ($category == NULL)
		return NULL;

	$io = array('category' => $category,
		    'parent' => bconf_get($BCONF, "*.cat.$category.parent"));

	get_settings(bconf_get($BCONF,"*.category_settings"), "extra_images", 
		     create_function('$s,$k,$d', 'return @$d[$k];'),
		     create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
		     $io);

	return $io['max'];
}

/* Get current uri with stripped parts */
/* Always end with ? or & to make appending easier. */
function get_request_uri($strip = null, $keep = null) {
	if (is_string($strip))
		$strip = array($strip);
	if (is_string($keep))
		$keep = array($keep);
	$req = explode('&', $_SERVER['QUERY_STRING']);
	$res = $_SERVER['SCRIPT_NAME'] . '?';
	foreach ($req as $set) {
		$var = explode('=', $set);
		if ($keep && !in_array($var[0], $keep))
			continue;
		if ($strip && in_array($var[0], $strip))
			continue;
		$res .= $set . '&';
	}
	return $res;
}

/*
 * Red arrow price calculation
 */
function get_red_arrow_price($price) {
	if ($price < 100) {
		$lowprice = -1;
	} else if ($price < 20000) {
		$lowprice = $price * .9;
	} else if ($price < 40000) {
		$lowprice = $price - 2000;
	} else if ($price < 60000) {
		$lowprice = $price - 3000;
	} else if ($price < 80000) {
		$lowprice = $price - 4000;
	} else if ($price < 100000) {
		$lowprice = $price - 5000;
	} else if ($price < 150000) {
		$lowprice = $price - 6000;
	} else if ($price < 200000) {
		$lowprice = $price - 8000;
	} else if ($price < 250000) {
		$lowprice = $price - 10000;
	} else if ($price < 500000) {
		$lowprice = $price - 12500;
	} else if ($price < 600000) {
		$lowprice = $price - 16500;
	} else
		$lowprice = $price * (1 - .03);

	if ($lowprice > 0)
		return round($lowprice);

	return false;
}

/*
 * Access input in a filtered way.
 * $name is the name of the var.
 * $regex is either an array of valid values or an regexp of it.
 *        In the latter case the regex must match the whole value ('^' is prepended, '$' appended).
 * $type is optionally the type of input (one of 'get', 'post', 'request', or an array of input values).
 *        Default is 'request'.
 */
function get_input($name, $regex, $type = 'request') {
	switch ($type) {
	case 'get':
		$val = @$_GET[$name];
		break;
	case 'post':
		$val = @$_POST[$name];
		break;
	case 'request':
		$val = @$_REQUEST[$name];
		break;
	default:
		if (!is_array($type))
			return null;
		$val = $type[$name];
		break;
	}
	if (is_array($regex)) {
		if (!in_array($val, $regex))
			return null;
	} else {
		if (!preg_match("/^$regex\$/", $val))
			return null;
	}
	return $val;
}

function format_date($indate) {
	setlocale(LC_TIME, 'sv_SE');

	$timestamp = strtotime($indate);
	$current_time = strftime('%Y%m%d', time());

	if ((strftime('%Y%m%d', $timestamp) - $current_time) == 0)
		$date = lang('DATE.TODAY');
	else if (($current_time - strftime('%Y%m%d', $timestamp)) == 1)
		$date = lang('DATE.YESTERDAY');
	else
		$date = strftime('%e %b', $timestamp);

	$date .= ';' . date('H:i', $timestamp);

	return $date;
}

function push_value(&$data, $key, $value) {
	if (isset($data[$key])) {
		array_push($data[$key], $value);
	} else {
		$arr = array();
		$data[$key] = array();
		array_push($data[$key], $value);
	}       
}



if (!function_exists('get_headers')) {
	function get_headers($url) {

		$url_info=parse_url($url);
		if (isset($url_info['scheme']) && $url_info['scheme'] == 'https') {
			$port = 443;
			@$fp=fsockopen('ssl://'.$url_info['host'], $port, $errno, $errstr, 10);
		} else {
			$port = isset($url_info['port']) ? $url_info['port'] : 80;
			@$fp=fsockopen($url_info['host'], $port, $errno, $errstr, 10);
		}
		if($fp) {
			stream_set_timeout($fp, 10);
			$head = "HEAD ".@$url_info['path']."?".@$url_info['query'];
			$head .= " HTTP/1.0\r\nHost: ".@$url_info['host']."\r\n\r\n";
			fputs($fp, $head);
			while(!feof($fp)) {
				if($header=trim(fgets($fp, 1024))) {
					$sc_pos = strpos( $header, ':' );
					if( $sc_pos === false ) {
						$headers['status'] = $header;
					} else {
						$label = substr( $header, 0, $sc_pos );
						$value = substr( $header, $sc_pos+1 );
						$headers[strtolower($label)] = trim($value);
					}
				}
			}
			return $headers;
		}
		else {
			return false;
		}
	}
}

/* Readfile in chunks, default 50kb chunks */
function readfile_chunked($filename, $chunk_bytes = 50000) {
	$buffer = '';
	$cnt = 0;
	$handle = fopen($filename, 'rb');

	if ($handle === false)
		return false;

	while (!feof($handle)) {
		$buffer = fread($handle, $chunk_bytes);
		echo $buffer;

		ob_flush();
		flush();

		$cnt += strlen($buffer);
	}

	$status = fclose($handle);

	if ($status)
		return $cnt;

	return $status;
} 

if (!function_exists('file_put_contents')) {
	function file_put_contents($filename, $data, $mode = 'w') {
		$f = @fopen($filename, $mode);
		if ($f === false) return false; 
		fwrite($f, $data);
		fclose($f);
		return true;
	}
}

/* Log event to all statpoint hosts */
function stat_log($bconf_node, $event, $id, $parent) {
	$hosts = $bconf_node['host'];
	$buf = "event:$event id:$id";
	$ret = 1;

	if ($parent)
		$buf .= " parent:$parent";

	$buf .= "\n";

	if (is_array($hosts)) {
		$s = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
		foreach ($hosts as $host) {
			if (socket_sendto($s, $buf, strlen($buf), 0, $host['name'], $host['port']) >= 0) {
				$ret = 0;
				break;
			}
		}
	}

	return $ret;
}

if (!function_exists('array_combine')) {
	function array_combine($a1, $a2)
	{
		for ($i = 0 ; $i < count($a1) ; $i++)
			$ra[$a1[$i]] = $a2[$i];
		if(isset($ra))
			return $ra;
		return false;
	}
}

function get_pricelist_key_lookup($s, $k, $io) {
	return @$io[$k];
}

function get_pricelist_set_value($s, $k, $v, $io) {
	$io['value'] = $v;
}

function get_pricelist() {
	global $BCONF;

	foreach (bconf_get($BCONF, "*.cat_order") as $cat) {
		$io = array('category' => $cat, 'parent' => bconf_get($BCONF, "*.cat.$cat.parent"));
		get_settings(bconf_get($BCONF,"*.category_settings"), 'price', 'get_pricelist_key_lookup', 'get_pricelist_set_value', $io);

		$val = intval(@$io['value']);
		if (isset($price[$cat]))
			error_log("get_pricelist(): Category $cat is already set with value ". $price[$cat] .", duplicate entry for *.cat_order.*.$cat ??");
		$price[$cat] = $val;
		if ($io['parent'] && isset($io['value'])) {
			$parent = $io['parent'];
			if (!$price[$parent])
				$price[$parent] = $val;
			elseif (is_numeric($price[$parent]) && $price[$parent] != $val) {
				if ($val < $price[$parent])
					$price[$parent] = "$val - {$price[$parent]}";
				else
					$price[$parent] = "{$price[$parent]} - $val";
			}
		}
	}
	return $price;
}

function is_extended(&$value) {
	if (is_object($value))
		return true;

	if (!is_array($value))
		return false;

	foreach ($value as $k => $v) {
		if (is_string($k) || is_array($v) || is_object($v))
			return true;
	}
	
	return false;
}

function get_remote_addr() {
	if (!isset($_SERVER['REMOTE_ADDR']))
		return '';

	return $_SERVER['REMOTE_ADDR'];
}

function handle_search_comp_reply($reply) {
	$headers = explode("\t", $reply['hdrs']);

	if (is_array($reply['row'])) {
		$i = 0;
		foreach($reply['row'] as $row) {
				$stores[$i++] = explode("\t", $row);
		}	
	} else
		$stores[0] = explode("\t", $reply['row']);

	$res = array ();
	if ($reply['rows'] != 0) {
		foreach ($stores as $store_data) {
				for ($i = 0; $i < sizeof($headers);$i++) {
					$res[substr($headers[$i], 2)] = @$store_data[$i];
				}	
		}
	}

	return $res;
}

function get_cookie_domain() {
    return $GLOBALS['BCONF']['*']['common']['session']['cookiedomain'];
}

/* add PHP_TEMPLATES functions */
function template_has_cookie($name) {
    return isset($_COOKIE[$name]) ? 1 : 0;
}

function template_get_cookie($name, $regex) {
    if (!isset($_COOKIE[$name])) {
        return null;
    }
    $val = @$_COOKIE[$name];

    if (preg_match("/$regex/", $val))
        return $val;
    return null;
}

function template_clear_cookie($name) {
    setcookie($name, NULL, time() - 3600, '/', get_cookie_domain());
}

function template_redirect($loc) {
    header("Location: $loc");
}

function template_init_set_cookie($cookie, $type=null, $time=null, $httponly = 0) {
    global $template_set_cookie;

    $template_set_cookie = array('name' => $cookie, 'type' => $type, 'time' => $time, 'value' => '', 'httponly' => $httponly);
}

function template_outstring_set_cookie($str) {
    global $template_set_cookie;

    $template_set_cookie['value'] .= $str;
}

function template_flush_set_cookie() {
}

function template_fini_set_cookie() {
    global $template_set_cookie;
    global $BCONF;

    if (!is_array($template_set_cookie) || !isset($template_set_cookie['name']))
        return;

    setcookie(
		$template_set_cookie['name'],
		$template_set_cookie['value'],
		$template_set_cookie['value'] !== '' ? ($template_set_cookie['time'] ? time() + $template_set_cookie['time'] * ( $template_set_cookie['type'] == 'm' ? 60 : 86400) : 0):-time(), 
		'/',
		get_cookie_domain(),
		false,
		($template_set_cookie['httponly'])?true:false
	);
}

function default_template_options($lang = NULL) {
       global $BCONF;

       if (empty($lang) && bconf_get($BCONF, "*.common.multilang.enabled"))
           $lang = @$_COOKIE['lang'];

       if (empty($lang) || !bconf_get($BCONF, "*.common.lang.".$lang))
           $lang = bconf_get($BCONF, "*.common.default.lang");

       return array('lang' => $lang,
            'country' => bconf_get($BCONF, "*.common.site_country")
       );
}

