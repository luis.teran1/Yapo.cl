<?php
/*****************************************************************************
//
// PHP MemCached Client 
//
// Copyright ( C ) 2005  Dan Thrue, Weird Silence, www.weirdsilence.net
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or ( at your option ) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
// CHANGELOG
//  - Added extra option and variabel id_separator
//  - Removed hashing of key, and replaced it with => $index = substr($key, 0, strpos($key, $this->id_separator));
//  - BUGFIX: Added addcslashes and stripcslashes when storing and getting data into and from memcached.
//  - FIX: Added resource check when sending commands
//  - FIX: If response if a get command doesn't has a value then its expired and returns false
//  - Added delete function
//  - Added in _getCurrentSocket a check if we found id_separator, if not use key as index
//  - Added host_available() to check if a memcache host is available.
//
*****************************************************************************/
require_once("init.php");
@define("WS_BUFFER", 1024);
define("WS_OBJECT", 1);
define("WS_COMPRESSED", 2);

class WeirdMemCachedClient
{
	var $servers = array();
	var $sockets = array();
	var $debug = false;
	var $compress = false;
	var $values = array();
	var $id_separator;

	function WeirdMemCachedClient($options=array())
	{
		$this->servers = $options['servers'];
		$this->debug = $options['debug'];
		$this->compress = $options['compress'];
		$this->id_separator = $options['id_separator'];
	}

	function host_available($host)
	{
		$socket = $this->_getCurrentSocket($host);
		return is_resource($socket);
	}

	function set($key, $val, $expire=0)
	{
		$this->_sendCmd('set', $key, $val, $expire);
	}
	
	function delete($key, $nocache = false)
	{
		if (!isset($this->values[$key]) || $nocache)
		{
			$cmd = "delete {$key} \r\n";
			
			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd))
                        {
                                return false;
                        }
                        $response = $this->_rawResponse($sock, 9);

			unset($this->values[$key]);

			return $response == "DELETED\r\n";
		}

		return false;
	}

	function get($key, $nocache = false)
	{
		if ($nocache || !isset($this->values[$key]))
		{
			$cmd = "get {$key} \r\n";

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd))
			{
				return false;
			}
			$response = $this->_rawResponse($sock);
			$arr = $this->_clearArray(explode("\r\n", $response));
			if (!@list($header, $val, $dummy) = $arr)
			{
				$this->error = "Couldnt extract value";
				return false;
			}

                        // Unescape \r\n that were escaped when stored
                        $val = stripcslashes($val);
			@list($dummy, $key, $flags, $len) = explode(' ', $header);

			if ($len == 0 && !preg_match("/VALUE/", $header))
			{
				return false;
			}

			if (($flags & WS_COMPRESSED) == WS_COMPRESSED)
			{
				$val = gzuncompress($val);
			}
			if (($flags & WS_OBJECT) == WS_OBJECT)
			{
				$val = unserialize($val);
			}
			$this->values[$key] = $val;
		}
		return $this->values[$key];
	}

	function _clearArray($arr)
	{
		$ret = array();
		foreach ($arr as $val)
		{
			if (strlen($val) > 0) $ret[] = $val;
		}
		return $ret;
	}

	function _getCurrentSocket($key)
	{
		$index = 0;
		if (count($this->servers) > 0)
		{
			$index = substr($key, 0, strpos($key, $this->id_separator));
			// If haven't found the separator, use the key as index
			if ($index == "")
				$index = $key;
		}
		if (!is_resource(@$this->sockets[$index]))
		{
			$sock = $this->_connect(@$this->servers[$index]);
			if (is_resource($sock))
			{
				$this->sockets[$index] = $sock;
			}

		}
		
		return $this->sockets[$index];
	}

	function _hash($key)
	{
		$val = 0;
		for ($i=1; $i<=5; $i++)
		{
			$val += ord(substr($key, -$i, 1));
		}
		return $val;
	}

	function _sendCmd($cmd, $key, $val, $expire)
	{
		$flags = 0;
		if(!is_scalar($val))
		{
			$val = serialize($val);
			$flags += WS_OBJECT;
		}
		if ($this->compress)
		{
			$val = gzcompress($val);
			$flags += WS_COMPRESSED;
		}

                // Escapes \r\n and backslash to avoid conflict with memcached protocol
                $val = addcslashes($val, "\r\n\\");

		$raw = "%s %s %d %d %d \r\n%s\r\n";
		$raw = sprintf($raw, $cmd, $key, $flags, $expire, strlen($val), $val);
		
		$sock = $this->_getCurrentSocket($key);
		if (!$this->_rawCmd($sock, $raw))
		{
			return false;
		}
		$response = $this->_rawResponse($sock, 8, PHP_NORMAL_READ);

		if (strncmp($response, 'STORED', 6) == 0)
		{
			return true;
		}
		else
		{
			$this->error = "Didnt get STORED from server";
			return false;
		}
	}

	function _rawCmd(&$sock, $cmd)
	{
		if (!is_resource($sock)) return false;

		$len = strlen($cmd);
		$offset = 0;
		while ($res = socket_write($sock, substr($cmd, $offset, WS_BUFFER), WS_BUFFER))
		{
			if ($res === false) break;
			$offset += $res;
		}
		if ($offset < $len)
		{
			$this->error = "Failed to send raw command";
			return false;
		}
		return true;
	}

	function _rawResponse(&$sock, $len=0, $type=PHP_BINARY_READ)
	{
		if ($len > 0)
		{
			if ($buffer = socket_read($sock, $len, $type))
			{
				if ($buffer === false)
				{
					$this->error = "Failed to get response from server";
					return false;
				}
				return $buffer;
			}
			return '';
		}
		else
		{
			$buffer = '';
			while($buf = socket_read($sock, WS_BUFFER, $type))
			{
				$buffer .= $buf;
				if (strpos($buffer, "END\r\n")+5 == strlen($buffer)) break;
			}
			return $buffer;
		}
	}

	function _connect($server)
	{
		$arr = explode(':', $server);
		$host = $arr[0];
		$port = $arr[1];

		$sock = socket_create (AF_INET, SOCK_STREAM, getprotobyname("TCP"));

		if(!@socket_connect($sock, $host, $port))
		{
			$this->error = "Couldnt connect to server: $server";
			return false;
		}
		return $sock;
	}

	function disconnect()
	{
		foreach ($this->sockets as $id => $sock)
		{
			socket_close($sock);
			$this->sockets[$id] = null;
		}
	}

	function disconnect_all()
	{
		$this->disconnect();
	}
}

?>
