<?php

namespace Yapo;

class Partner
{
    public $errorMsg;
    private $services;
    private $data;

    public function __construct(array $data)
    {
        $this->errorMsg = null;
        $this->services = array();
        $attrs = array(
            'Name',
            'Protocol',
            'ImgProtocol',
            'ConnData',
            'ImgConnData',
            'PathFile',
            'FileName',
            'PathImg',
            'Delimiter',
            'Rules',
            'IRules',
            'ApiPoint',
            'ApiKey',
            'InsDelay',
            'MailTo',
            'KeyName',
            'Market',
            'CsvProtocol',
            'CsvConnData',
            'TdProtocol',
            'TdConnData',
            'RunTime',
            'ConfId',
            'Active'
        );

        $this->data = array();
        foreach ($attrs as $attr) {
            $this->data[$attr] = isset($data[$attr]) ? $data[$attr] : null;
        }
        if (!isset($this->data["ApiKey"]) || empty($this->data["ApiKey"])) {
            $this->createApiKey();
        }
    }

    /*
     * Creates a hash for the partner, to be used in blocket API
     */
    private function createApiKey()
    {
        $this->data['ApiKey'] = hash('sha256', openssl_random_pseudo_bytes(128));
        return $this;
    }

    /*
     * Creates the partner, by executing the createPartner method for every service
     * added to the Partner.
     */
    public function create()
    {
        foreach ($this->services as $service) {
            try {
                $result = $service->createPartner($this->data);
            } catch (\Exception $e) {
                $this->errorMsg = $e->getMessage();
                return false;
            }
        }
        return true;
    }

    /*
     * Adds a service to the Partner object
     *
     * @param $service Service to be added
     */
    public function addService($service)
    {
        $this->services[] = $service;
        return $this;
    }

    /*
     * Updates the partner, by executing the updatePartner method for every service
     * added to the Partner.
     *
     */
    public function update()
    {
        foreach ($this->services as $service) {
            try {
                $result = $service->updatePartner($this->data);
            } catch (\Exception $e) {
                $this->errorMsg = $e->getMessage();
                return false;
            }
        }
        return true;
    }

    /*
     * Returns ASCII string compatible strings
     * This function was taken from: http://cubiq.org/the-perfect-php-clean-url-generator
     */
    private function friendlyStr($str, $delimiter = '-')
    {
        setlocale(LC_ALL, 'en_US.UTF8');

        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower(trim($clean, '-'));
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

        return $clean;
    }
}
