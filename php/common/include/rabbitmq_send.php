<?php

require_once('bTransaction.php');

function rabbitmq_send($virtual_host, $exchange, $routing_key, $message) {
	$trans = new bTransaction();

	if (empty($virtual_host))	return -1;
	if (empty($exchange))		return -2;
	if (empty($routing_key))	return -3;
	if (empty($message))		return -4;

	$trans->populate(array(
		'virtual_host'	=> $virtual_host,
		'exchange'		=> $exchange,
		'routing_key'	=> $routing_key,
		'message'		=> $message
	));

	$reply = $trans->send_command('rabbitmq_send');
	
	if ($trans->has_error(true) || $reply['status'] != "TRANS_OK" ) {
		syslog(LOG_ERR, log_string()."ERROR: rabbitmq_send($virtual_host,$exchange,$routing_key,$message) failed");
		syslog(LOG_ERR, log_string()."ERROR: ".var_export($trans->get_errors(), true));
		return -5;
	}

	return 0;
}

