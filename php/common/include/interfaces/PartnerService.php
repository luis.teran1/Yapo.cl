<?php

namespace Yapo\Interfaces;

interface PartnerService
{
	public function createPartner($data);
	public function updatePartner($data);
}
