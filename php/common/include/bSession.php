<?php
// Include the memcached client
require_once('memcachedclient.php');
require_once('memcachedsession.php');

/*
 * Session manager
 */
class bSession
{
	var $new_session;
	var $session_id;
	var $has_cookie;
	var $domain;
	
	var $_prefix;
	var $_separator;
	var $_session_hosts;

	function _find_session_id() {
		// Check for session_id
		if (isset($_GET[session_name()])) { // GET
			$session_id = $_GET[session_name()];
		} else if (isset($_POST[session_name()])) { // POST
			$session_id = $_POST[session_name()];
		} else if (isset($_COOKIE[session_name()])) { // COOKIE
			$session_id = $_COOKIE[session_name()];
		} else if (isset($_SERVER['PATH_INFO']) && preg_match("/\/s=([a-z0-9]+)/", $_SERVER['PATH_INFO'], $matches) > 0) { // PATH_INFO CHECK
			$session_id = $matches[1];
		} else {
			// NO SESSION
			$session_id = NULL;
		}

		return $session_id;
	}

	function bSession() { 
		// Set variables
		$this->_prefix = @$GLOBALS['BCONF']['*']['common']['session']['server'];
		$this->_separator = $GLOBALS['BCONF']['*']['common']['session']['id_separator'];
		$this->_session_hosts = $GLOBALS['BCONF']['*']['common']['session']['servers'];
		$this->domain = $GLOBALS['BCONF']['*']['common']['session']['cookiedomain'];

		// Check for session_id
		$session_id = $this->_find_session_id();

		if ($this->_validate_session_id($session_id)) {
			$session_host = substr($session_id, 0, strpos($session_id, $this->_separator));
			// Override default session host
			$this->_prefix = $session_host;
		}

		// Is memcache client alive
		if (!memcached_session_available($this->_prefix)) {
			// Log this critical error
			syslog(LOG_CRIT, "Session host down {$this->_prefix} => {$this->_session_hosts[$this->_prefix]}");
			// Unset current prefix
			unset($this->_prefix);
			// Get next available host
			foreach($this->_session_hosts as $host_id => $host_address) {
				if (memcached_session_available($host_id)) {
					syslog (LOG_INFO, "Using $host_id session host as secondary option.");
					$this->_prefix = $host_id;
					break;
				}
			}
			// Check if found a host
			if (!isset($this->_prefix)) {
				syslog(LOG_CRIT, "All session hosts are down!");
				die_with_template(__FILE__, "All session hosts are down!");
			}
		}

		// Cookie support
		$this->has_cookie = true;

		// Kill session
		if (isset($_REQUEST["die"])) {
			$this->_destroy_session();
		}

		// Start the session
		$this->_do_session();
	}
	
	// Generate session id
	function _generate_session_id() {
		return $this->_prefix.$this->_separator.sha1(microtime(true).uniqid(rand(), true));
	}

	// Destroy session
	function _destroy_session() {
		// Delete session data
		if ($this->_validate_session_id($this->session_id))
			memcached_session_delete($this->session_id);

		// Unsetting session variabel
		unset($_REQUEST[session_name()]);

		// Unset session variabel
		session_unset();

		// Destroying session
		@session_destroy();
	}

	// Check validity of session id
	function _validate_session_id($id) {
		// Check if any id
		if ($id === NULL || strlen($id) == 0)
			return false;
		
		// Check if id belongs to a valid host
		$host = substr($id, 0, strpos($id, $this->_separator));
		if (@!array_key_exists($host, $this->_session_hosts))
			return false;

		return true;
	}

	// Session start
	function _do_session() {
		$this->new_session = false;
		$this->session_id = NULL;

		// Get current sess id if exists
		$session_id = $this->_find_session_id();
		if ($session_id != NULL) {
			$this->session_id = $session_id;
			if ( !$this->_validate_session_id($this->session_id) || memcached_session_read($this->session_id) === false) {
				$this->session_id = NULL;
                	}
		}
		
		// Check if a session id is already present
		if ($this->session_id == NULL) {
			$this->session_id = $this->_generate_session_id();
			$this->new_session = true;
		}

		// Set session id
		@session_id($this->session_id);

		$sp = session_get_cookie_params();
		session_set_cookie_params($sp["lifetime"], $sp["path"], $this->domain);

		// Start session
		@session_start();

		// Add session id to query string of no cookie support
		if (!isset($_COOKIE[session_name()])) {
			$this->has_cookie = false;
		}
	}
	
	// Get session host
	function session_host() {
		return substr($this->session_id, 0, strpos($this->session_id, $this->_separator));		
	}

	// Get input form hidden
	function get_form_input() {
		return !$this->has_cookie?"<input type='hidden' name='s' value='{$this->session_id}' />":"";
	}
	
	// Get query string variabel
	function get_query_string($s_only = false) {
		if ($s_only)
			$query_string = '';
		else
			$query_string = $_SERVER['QUERY_STRING'];
                if(!$this->has_cookie) {
                        if (preg_match("/(^|&)s=[^&]+/", $query_string)) {
                                $query_string = preg_replace("/(^|&)s=[^&]+/", 's='.$this->session_id, $query_string);
                        } else {
                                if ($query_string != "")
                                        $query_string .= "&";
                                $query_string .= "s=" . $this->session_id;
                        }
                }

                return $query_string;
	}

	/* Check if session is initialized */
	function is_initialized() {
		return isset($_SESSION['init']);
	}

	/* Store in session that the session has been properly initialized, and the method */
	function initialize() {
		$_SESSION['init'] = $this->has_cookie?'cookie':'url';
	}

	/* Check if session is cookie handled or uri */
	function session_init_method() {
		if ($this->is_initialized())
			return $_SESSION['init'];
		return false;
	}
		
	/* Get the next free instance id */
	function new_instance_id($name, $max) {
		$old_id = null;
		$max_id = -1;
		$old_time = time() + 1;
		$num = 0;
		if (isset($_SESSION['instance_data'][$name])) {
			foreach ($_SESSION['instance_data'][$name] as $id => $data) {
				$num++;
				if ($id > $max_id)
					$max_id = $id;
				$ts = $data['?stored_timestamp'];
				if ($ts < $old_time) {
					$old_id = $id;
					$old_time = $ts;
				}
			}
		}

		if ($num >= $max) {
			unset($_SESSION['instance_data'][$name][$old_id]);
			$id = $old_id;
		} else {
			$id = $max_id + 1;
		}
		$_SESSION['instance_data'][$name][$id] = array('?stored_timestamp' => time()); 
		return $id;
	}
	
	/* Load instance for current object */

  	function load_active_instance(&$obj, $name, $instance_id, $authenticate = true) {
		if (!isset($_SESSION['instance_data'][$name][$instance_id]))
			return false;


		$objcopy = $obj;

		foreach($_SESSION['instance_data'][$name][$instance_id] as $var => $value) {
			if ($var != '?stored_timestamp')
				$objcopy->$var = $value;
		}

		if (!$objcopy->is_active($authenticate)) {
			$obj->set_errors($objcopy->errors);
			$obj->set_messages($objcopy->messages);
			return false;
		}

		foreach($_SESSION['instance_data'][$name][$instance_id] as $var => $value) {
			if ($var != '?stored_timestamp')
				$obj->$var = $objcopy->$var;
		}

		return true;
	}

	function load_instance(&$obj, $max) {
		$name = get_class($obj);
		$instance_id = $obj->instance_id;

		if (!$this->load_active_instance($obj, $name, $instance_id, false)) {
			$instance_id = $this->new_instance_id($name, $max);
			$obj->instance_id = $instance_id;
			if (isset($_SESSION['static_data'][$name])) {
				foreach($_SESSION['static_data'][$name] as $var => $value)
					$obj->$var = $value;
			}
		}
	}

	/* Store instance */
	function store_instance($obj) {
		$name = get_class($obj);
		$instance_id = $obj->instance_id;
		$static_vars = $obj->static_vars;
		$volatile_vars = $obj->volatile_vars;
		if (!is_numeric($instance_id))
			return false;

		/* Store static variables both in static and instance */
		$statics = array();
		$instance = array();
		foreach($obj as $var => $value) {
			if (in_array($var, $volatile_vars))
				continue;
			if (in_array($var, $static_vars))
				$statics[$var] = $value;
			$instance[$var] = $value;
		}
		$instance['?stored_timestamp'] = time();

		$_SESSION['instance_data'][$name][$instance_id] = $instance;
		if (count($statics))
			$_SESSION['static_data'][$name] = $statics;

		return true;
	}

	/* Destroy the instance */
	function destroy_instance($obj) {
		$name = get_class($obj);
		$instance_id = $obj->instance_id;
		$static_vars = $obj->static_vars;

		if (!is_numeric($instance_id))
			return false;
		
		/* Store static data in case it's changed. */
		$statics = array();
		foreach($obj as $var => $value) {
			if (in_array($var, $static_vars))
				$statics[$var] = $value;
		}
		if (count($statics))
			$_SESSION['static_data'][$name] = $statics;

		unset($_SESSION['instance_data'][$name][$instance_id]);

		return true;
	}
}

