<?php
class CreditsService
{
    private $Path;
    private $ReportPath;
    private $Host;
    private $Port;
    private $Proxy;

    const DAY = 2;
    const MONTH = 1;
    const YEAR = 0;

    public function __construct()
    {
        global $BCONF;
        $this->Host = bconf_get($BCONF, "*.credits.host");
        $this->Port = bconf_get($BCONF, "*.credits.port");
        $this->Path = bconf_get($BCONF, "*.credits.path");
        $this->ReportPath = bconf_get($BCONF, "*.credits.report.path");
        $this->AssignedReportPath = bconf_get($BCONF, "*.credits.assigned_report.path");
        $this->ListPath = bconf_get($BCONF, "*.credits.list.path");
        $this->EditPath = bconf_get($BCONF, "*.credits.edit.path");
        $this->Proxy = new ProxyClient();
    }

    public function getBalance($userId)
    {
        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $this->Path."/{$userId}");
        $code = $response->code;
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        //on error this should be returned
        $error_response = (object) array(
            "Balance" => "-1",
            "CreditToExpire" => (object) array(
                "Credits" => "0",
                "ExpirationDate" => "0/0/0"
            )
        );
        if ($code != 200) {
            bLogger::logError(__METHOD__, "Credits Api Error:". var_export($response, true));
            return $error_response;
        }
        $json_response = json_decode($response);
        //if for some reason the json cannot be decoded
        if ($json_response == null) {
            return $error_response;
        }
        return $json_response;
    }

    public function addCredits(Array $data)
    {
        if (empty($data["UserId"]) || strlen(trim($data["Credits"])) == 0 || empty($data["ExternalId"])) {
            bLogger::logError(__METHOD__, "Credits Error: Missing params.");
            return null;
        }
        $response = $this->Proxy->doRequest('post', $this->Host, $this->Port, $this->Path, $data);
        $code = $response->code;
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            bLogger::logError(__METHOD__, "Api Credits Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function consumeCredits($data)
    {
        if (!isset($data["UserId"]) || !isset($data["Credits"]) || !isset($data["ExternalId"])) {
            bLogger::logError(__METHOD__, "Credits Error: Missing params.");
            return null;
        }
        $response = $this->Proxy->doRequest('put', $this->Host, $this->Port, $this->Path, $data);
        $code = $response->code;
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            bLogger::logError(__METHOD__, "Credits Api Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function getReport($startDate, $endDate, $userId = "")
    {
        //on error this should be returned
        $error_response = array();

        if (!self::validateDate($startDate) && !self::validateDate($endDate)) {
            return $error_response;
        }
        $full_path = "{$this->ReportPath}/{$startDate}/{$endDate}/{$userId}";
        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $full_path);
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($response->code != 200) {
            bLogger::logError(__METHOD__, "Credits Api Error:". var_export($response, true));
            return $error_response;
        }
        $json_response = json_decode($response, true);
        //if for some reason the json cannot be decoded
        if ($json_response == null) {
            return $error_response;
        }
        return $json_response;
    }

    public function getAssignedReport($startDate, $endDate)
    {
        //on error this should be returned
        $error_response = array();

        if (!self::validateDate($startDate) && !self::validateDate($endDate)) {
            return $error_response;
        }
        $full_path = "{$this->AssignedReportPath}/{$startDate}/{$endDate}";
        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $full_path);
        bLogger::logDebug(__METHOD__, "Response Proxy ({$full_path}):". var_export($response, true));

        if ($response->code != 200) {
            bLogger::logError(__METHOD__, "Credits Api Error:". var_export($response, true));
            return $error_response;
        }
        $json_response = json_decode($response, true);
        //if for some reason the json cannot be decoded
        if ($json_response == null) {
            return $error_response;
        }
        return $json_response;
    }

    public function listCredits($startDate, $endDate, $userId)
    {
        //on error this should be returned
        $error_response = array();

        if (!self::validateDate($startDate) && !self::validateDate($endDate) && is_numeric($userId)) {
            return $error_response;
        }
        $full_path = "{$this->ListPath}/{$startDate}/{$endDate}/{$userId}";
        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $full_path);
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($response->code != 200) {
            bLogger::logError(__METHOD__, "Credits Api Error:". var_export($response, true));
            return $error_response;
        }
        $json_response = json_decode($response, true);
        //if for some reason the json cannot be decoded
        if ($json_response == null) {
            return $error_response;
        }
        return $json_response;
    }

    public function editCredits($data)
    {
        $required_params = array("CreditId", "UserId", "AdminId", "Credits", "ExternalId");
        $num_params = 0;
        foreach ($data as $key => $value) {
            if (!is_numeric($value)) {
                bLogger::logError(__METHOD__, "Credits Error: Invalid value,  'data[{$key}]={$value}'");
                return null;
            }
            if (in_array($key, $required_params)) {
                $num_params++;
            }
        }
        if ($num_params != count($required_params)) {
            bLogger::logError(__METHOD__, "Credits Error: Mising Params.");
            return null;
        }
        $response = $this->Proxy->doRequest('put', $this->Host, $this->Port, $this->EditPath, $data);
        $code = $response->code;
        bLogger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            bLogger::logError(__METHOD__, "Api Credits Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        //if for some reason the json cannot be decoded
        if ($json_response == null) {
            return $error_response;
        }
        return $json_response;
    }

    private static function validateDate($date)
    {
        $test_arr  = explode('-', $date);
        if (count($test_arr) == 3) {
            if (checkdate($test_arr[self::MONTH], $test_arr[self::DAY], $test_arr[self::YEAR])) {
                return true;
            }
        }
        bLogger::logError(__METHOD__, "The date '{$date}' is not a valid date.");
        return false;
    }
}
