<?php
namespace Yapo;


class PaymentDeliveryQueryService extends MicroserviceConnector
{
    public function __construct()
    {
        $account_session = new \AccountSession();
        $yapo_request = new YapoRequest($account_session);
        parent::__construct("payment_delivery_query_service", $yapo_request);
    }

    public function responseHandler($response) {
        if ($response->code != 200) {
            return null;
        }
        return $response;
    }

    public function errorHandler($response = array()) {
        return json_decode($response, true);
    }
}
