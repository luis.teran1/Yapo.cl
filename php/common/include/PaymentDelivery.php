<?php
namespace Yapo;

use AccountSession;

class PaymentDelivery extends MicroserviceConnector
{
    public function __construct()
    {
        $account_session = new AccountSession();
        $yapo_request = new YapoRequest($account_session);
        parent::__construct("PaymentDelivery", $yapo_request);
    }

    public function responseHandler($response)
    {
        if ($response->code == 200 || $response->code == 204) {
            return $response;
        }
        return null;
    }

    public function errorHandler($response = array())
    {
        return json_decode($response, true);
    }
}
