<?php

class bImageStream {
    var $position;
    var $varname;
	var $ctx;

    function stream_open($path, $mode, $options, &$opened_path)
    {
		$url = parse_url($path);
		$this->varname = $url["host"];

		$validate=file_get_contents($path,false,$ctx);
		$this->ctx = stream_context_get_options($this->context);

		if ($mode[0] == 'a')
			$this->position = strlen($this->ctx['data']['image']->_image_buffer);
		else	
			$this->position = 0;

		return true;
	}

    function stream_read($count)
    {
        $ret = substr($this->ctx['data']['image']->_image_buffer, $this->position, $count);
        $this->position += strlen($ret);
        return $ret;
    }

    function stream_write($data)
    {
        $left = substr($this->ctx['data']['image']->_image_buffer, 0, $this->position);
        $right = substr($this->ctx['data']['image']->_image_buffer, $this->position + strlen($data));
        $this->ctx['data']['image']->_image_buffer = $left . $data . $right;
        $this->position += strlen($data);
        return strlen($data);
    }

    function stream_tell()
    {
        return $this->position;
    }

    function stream_eof()
    {
        return $this->position >= strlen($this->ctx['data']['image']->_image_buffer);
    }

    function stream_seek($offset, $whence)
    {
        switch ($whence) {
            case SEEK_SET:
                if ($offset < strlen($this->ctx['data']['image']->_image_buffer) && $offset >= 0) {
                     $this->position = $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_CUR:
                if ($offset >= 0) {
                     $this->position += $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            case SEEK_END:
                if (strlen($this->ctx['data']['image']->_image_buffer) + $offset >= 0) {
                     $this->position = strlen($this->ctx['data']['image']->_image_buffer) + $offset;
                     return true;
                } else {
                     return false;
                }
                break;

            default:
                return false;
        }
    } 
}

stream_wrapper_register("bimagestream", "bImageStream")
    or die("Failed to register protocol");
?>
