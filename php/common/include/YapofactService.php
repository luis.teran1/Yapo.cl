<?php
namespace Yapo;

use ProxyClient;

class YapofactService
{
    private $PathRequest;
    private $Host;
    private $Port;
    private $Proxy;

    const DAY = 2;
    const MONTH = 1;
    const YEAR = 0;

    public function __construct()
    {
        global $BCONF;
        $this->Host = bconf_get($BCONF, "*.yapofact.host");
        $this->Port = bconf_get($BCONF, "*.yapofact.port");
        $this->PathHealthCheck = bconf_get($BCONF, "*.yapofact.healthcheck.path");
        $this->PathRequest = bconf_get($BCONF, "*.yapofact.request.path");
        $this->PathAdminReport = bconf_get($BCONF, "*.yapofact.admin_report.path");
        $this->PathAdminResend = bconf_get($BCONF, "*.yapofact.admin_resend.path");
        $this->Proxy = new ProxyClient();
    }

    public function healthCheck()
    {

        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $this->PathHealthCheck);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            Logger::logError(__METHOD__, "Yapofact Api Error:". var_export($response, true));
            return null;
        }
        return json_decode($response);
    }

    public function requestReport($data)
    {

        $response = $this->Proxy->doRequest('post', $this->Host, $this->Port, $this->PathRequest, $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            Logger::logError(__METHOD__, "Yapofact Api Error:". var_export($response, true));
            return null;
        }
        return json_decode($response);
    }

    public function adminReport($startDate, $endDate, $params)
    {
        if (!$this->validateDate($startDate) || !$this->validateDate($endDate) || !is_array($params)) {
            return null;
        }
        $query_params = array();
        foreach ($params as $key => $value) {
            // if not empty then we should add this as a query param
            if (!empty($value)) {
                $query_params[] = "{$key}={$value}";
            }
        }
        $query = "?".implode("&", $query_params);

        $response = $this->Proxy->doRequest(
            'get',
            $this->Host,
            $this->Port,
            $this->PathAdminReport."/{$startDate}/{$endDate}{$query}"
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            Logger::logError(__METHOD__, "Yapofact Api Error:". var_export($response, true));
            return null;
        }
        return yapo_json_decode($response);
    }

    public function adminResend($reportID, $email)
    {
        $response = $this->Proxy->doRequest(
            'post',
            $this->Host,
            $this->Port,
            $this->PathAdminResend."/{$reportID}",
            array("User_email" => $email)
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code != 200) {
            Logger::logError(__METHOD__, "Yapofact Api Error:". var_export($response, true));
            return null;
        }
        return json_decode($response);
    }

    private function validateDate($date)
    {
        $test_arr  = explode('-', $date);
        if (count($test_arr) == 3) {
            if (checkdate($test_arr[self::MONTH], $test_arr[self::DAY], $test_arr[self::YEAR])) {
                return true;
            }
        }
        Logger::logError(__METHOD__, "The date '{$date}' is not a valid date.");
        return false;
    }
}
