<?php
/*
 * Memcached PHP session handler
 *
 * Description:
 * Replaces PHP's own session handler
 *
 * Changelog:
 * - memcached_session_close:
 *   Remove time() from $expire = time() + session_expire * 60
 *   Now it uses relative time and it uses memcached server time instead of local time
 *
 * - Added memcached_session_delete
 *
 * - In functions directly access the global variabel __mnemcached_client
 */
require_once('memcachedclient.php');

$__memcached_client = null;
	
// Start a memcached client
function memcached_client() {
	$obj =& $GLOBALS['__memcached_client'];
	if (!is_object($obj)) {
		$options = array(
			'servers' => $GLOBALS['BCONF']['*']['common']['session']['servers'],
			'debug' => false,
			'compress' => false,
			'id_separator' => $GLOBALS['BCONF']['*']['common']['session']['id_separator']
		);

		$obj = new WeirdMemCachedClient($options);
	}
	return $obj;
}

// Close session
function memcached_session_close() {
	if (($id = session_id())) {
		global $session_read_only;
		
		memcached_client();
		$expire = $GLOBALS['BCONF']['*']['common']['session']['expire'] * 60; 
		if (!isset($session_read_only) || !$session_read_only)
			$GLOBALS['__memcached_client']->set(session_id(), $GLOBALS['__session'], $expire);
		if (isset($_SESSION['global']) && !empty($_SESSION['global'])) {
			foreach ($_SESSION['global'] as $key => $value) {
				if (isset($global_session))
					$global_session .= "&";
				else
					$global_session = "";
				$global_session .= urlencode($key) . "=" . urlencode($value);
			}
			$GLOBALS['__memcached_client']->set(session_id() . "_global", 
							    $global_session, 
							    $expire);
		} else {
			$GLOBALS['__memcached_client']->delete(session_id() . "_global");
		}

		$GLOBALS['__memcached_client']->disconnect_all();
	}
}

// Read session data
function memcached_session_read($id) {
	memcached_client();
	return $GLOBALS['__memcached_client']->get($id);
}

// Write session data
function memcached_session_write($id, $data) {
	memcached_client();
	$GLOBALS['__session'] = $data;
}

// Delete session data
function memcached_session_delete($id) {
	memcached_client();
	return $GLOBALS['__memcached_client']->delete($id);
}

// Open session
function memcached_session_open($path, $name) {
}

// Destroy session
function memcached_session_destroy() {
}

// Garbage collection
function memcached_session_gc($maxlt) {
	return true;
}

// Check availability of server
function memcached_session_available($host) {
	memcached_client();
	return $GLOBALS['__memcached_client']->host_available($host);
}

// Replace the session management
session_set_save_handler('memcached_session_open','memcached_session_close','memcached_session_read','memcached_session_write','memcached_session_destroy','memcached_session_gc') or die('');
?>
