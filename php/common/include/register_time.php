<?php
namespace Yapo;

$timeStart = microtime(true);
$appName = isset($appName) ? $appName : 'RegisterTime';
$appTime = Bconf::get($BCONF, "*.php_execution_time.{$appName}");
if (is_null($appTime)) {
    $appTime = Bconf::get($BCONF, "*.php_execution_time.default");
}

$maxTime = isset($maxTime) ? $maxTime : $appTime;

Logger::logDebug('RegisterTime', "Working for: {$appName} max execution time: {$maxTime}s");

register_shutdown_function(
    function () use ($timeStart,$maxTime, $appName) {
        $timer = new Timer($maxTime);
        $timer->shutdownInfo($timeStart, $appName);
    }
);
