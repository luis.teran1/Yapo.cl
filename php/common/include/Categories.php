<?php
namespace Yapo;

class Categories
{
    /**
     * isPackCategory
     *
     * Determines whether or not given category is a pack category.
     *
     * @param array $BCONF Array of available bconf
     * @param int   $category Category to be tested
     * @return bool Whether or not given category is a pack category according to the configuration
     */
    public static function isPackCategory($BCONF, $category)
    {
        $packType = bconf_get($BCONF, '*.packs.type');
        foreach ($packType as $pack) {
            if (bconf_get($pack, 'enabled') == 1 && bconf_get($pack, "category.{$category}") == 1) {
                return true;
            }
        }
        return false;
    }

    /**
     * isInsertingFeeCategory
     *
     * Determines whether or not given category is an inserting fee category.
     *
     * @param array $BCONF Array of available bconf
     * @param int   $category Category to be tested
     * @return bool Whether or not given category is an inserting fee category according to
     * the configuration
     */
    public static function isInsertingFeeCategory($BCONF, $category)
    {
        return bconf_get($BCONF, "*.cat.{$category}.inserting_fee") === '1';
    }
}

