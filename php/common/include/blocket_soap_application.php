<?php
require_once("blocket_application.php");
require_once("bXML.php");

define('XMLNS_SOAPENV', 'http://www.w3.org/2003/05/soap-envelope');

function escape_xml($key, &$value) {
	global $BCONF;
	if (!is_array($value))
		$value = htmlentities($value, ENT_COMPAT, $BCONF['*']['common']['charset']);
}

/*
 * Similar to blocket_application, but for soap requests.
 *
 * TODO: Fuller array support, stricter validation.
 *
 * This class has no security checking or session support.
 * To support the automatic documentations via HTTP GET, you need to
 * add a 'result' return parameter to the get_state function describing the result,
 * and get_states() which should return all valid functions.
 *
 * The result returned from the function will be sent to the client.
 * If it is an array the first element will be given in the <rpc:result> tag,
 * and all the elements added like <key>value</key>.
 * If it's a scalar it will be returned in a <value> tag.
 *
 * See soap_test.php for an example app.
 */
class blocket_soap_application extends blocket_application {
	var $namespaces = array('' => 'http://www.yapo.cl/ws');
	var $headstyles = array('soap.css');

	function init() {
	}

	function get_state($name) {
		return null;
	}

	function get_headers($state) {
		return array();
	}

	function get_states() {
		return array();
	}

	function get_post_data() {
		global $HTTP_RAW_POST_DATA;

		if(@$HTTP_RAW_POST_DATA)
			return $HTTP_RAW_POST_DATA;

		return file_get_contents("php://input");
	}
	
	function allowed_doc_ip() {
		global $BCONF;
		
		if (bconf_get($BCONF, 'services.doc_ip_allowed_override'))
			return true;
		
		foreach (bconf_get($BCONF, 'services.ad.user.import_partner') as $partner => $node) {
			foreach ($node['allowed_ip'] as $k => $ip) {
				if ($ip == $_SERVER['REMOTE_ADDR'])
					return true;
			}
		}
		
		foreach (bconf_get($BCONF, '*.common.ip_allowed_list.blocket') as $k => $ip) {
			if ($ip == $_SERVER['REMOTE_ADDR'])
				return true;
		}

		return false;
	}


	function run_fsm() {		
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$this->handle();
		} elseif (!empty($_GET['f'])) {
			$this->caller = new blocket_caller();
			$state = $this->get_state(strtoupper($_GET['f']));
			if ($state) {
				if (!$this->allowed_doc_ip()){
					syslog(LOG_ERR, log_string().'Ip ' . $_SERVER['REMOTE_ADDR'] . ' not allowed for documentation');
					return false;
				}
				$this->soap_function = $state['function'];
				if (!empty($state['params']))
					$this->params = $this->flatten_params($state['params']);
				$this->headers = $this->get_headers(strtoupper($_GET['f']));
				if (!empty($state['result_descr_tmpl']))
					$this->result_descr_tmpl = $state['result_descr_tmpl'];
				else
					$this->result = $state['result'];
				if (!empty($state['descr_tmpl']))
					$this->descr_tmpl = $state['descr_tmpl'];
				if (!empty($_GET['wsdl'])) {
					header ("Content-Type: text/xml");
					$this->display('soap/wsdl_state_v1.1.xml');
				} else
					$this->display_simple('soap/describe_state.html');
			} else
				$this->display_simple('soap/no_such_state.html');
		} else {			
			if (!$this->allowed_doc_ip()){
				syslog(LOG_ERR, log_string().'Ip ' . $_SERVER['REMOTE_ADDR'] . ' not allowed for documentation');
				return false;
			}

			$this->caller = new blocket_caller();
			$data['state'] = $this->get_states();
			$this->display_simple('soap/list_states.html', $data);
			$states = $this->get_states();
		}
	}

	/**
		Raise a SOAP fault.

		Examples: 
			$this->fault(array('Sender', $transaction->get_errors()), 'Failed to insert ad');
				or
			$this->fault(array('MustUnderstand', 'SomeScalar'), 'Incomplete protocol implementation.');

		XXX Should return reply in post way, fir current_method == POST
 	*/
	function fault($code, $reason, $details = array(), $messages = array()) {
		global $BCONF;

		$sub_codes = array();
		if (is_array($code)) {
			$main_code = $code[0];
			if(is_array($code[1])) {
				foreach($code[1] as $err_field => $err_msgs)
				{
					if(is_array($err_msgs)) {
						foreach($err_msgs as $err_msg_line) {
							$sub_codes[] = $err_msg_line;
						}
					} else {
						$sub_codes[] = $err_msgs;
					}
				}
			} else {
				$sub_codes[] = $code[1];
			}
		} else {
			$main_code = $code;
		}

		if (!in_array($main_code, array ('VersionMismatch', 'MustUnderstand', 'DataEncodingUnknown', 'Sender', 'Receiver'))) {
			$this->fault('Receiver', 'Programmer error');
		}
		$data['code'] = $main_code;
		$data['subcode'] = $sub_codes;
		$data['reason'] = htmlentities($reason, ENT_COMPAT, $BCONF['*']['common']['charset']);

		$this->display('soap/fault.xml', $data, array('detail' => $details, 'message' => $messages));
		exit(0);
	}

	function extract_soap_data($data) {
		if (count($data) > 1)
			$this->fault('Sender', 'This should not happen');
		$root_tag = $data[0];

		if ($root_tag['!tag'] != 'ENVELOPE')
			$this->fault('Sender', 'Root tag is not envelope');

		$data = $root_tag['!contents'];
		if (count($data) == 2 && $data[0]['!tag'] == 'HEADER' && $data[1]['!tag'] == 'BODY') {
			$this->header = $data[0]['!contents'];
			$this->body = $data[1]['!contents'];
		} elseif (count($data) == 1 && $data[0]['!tag'] == 'BODY') {
			$this->header = null;
			$this->body = $data[0]['!contents'];
		} else
			$this->fault('Sender', 'Invalid envelope contents');
	}

	function handle_header($header) {
		return false;
	}

	function is_xml_rpc($data) {
		return (strncmp("<?xml", $data, 5) == 0 || strncmp("<SOAP", $data, 5) == 0);
	}

	function handle() {
		$data = $this->get_post_data();

		if ($this->is_xml_rpc($data)) {
			$xml = new bXMLns($data, 'all');
			if (!$xml->parse_ok())
				$this->fault(array('Sender', 'ERROR_XML_PARSE'), $xml->get_error_message());
			$this->extract_soap_data($xml->get_result());

			if (is_array($this->header)) {
				foreach ($this->header as $header) {
					$res = $this->handle_header($header);
					if (!$res && isset($header['MUSTUNDERSTAND']) && $xml->get_ns_value($header['MUSTUNDERSTAND'], XMLNS_SOAPENV) == 'true')
						$this->fault('MustUnderstand', 'did not understand');
				}
			}

			/* Assume RPC call for now */
			$this->handle_rpc();
		} else {
			/* HTTP POST */
			$this->handle_post();
		}
	}

	function handle_post() {
		/* State function to run */
		$func = strtoupper(substr($_SERVER['REQUEST_URI'], strlen($_SERVER['SCRIPT_NAME']) + 1));

		/* Handle headers */
		$state = $this->get_state($func);
		$headers = $this->get_headers($func);
		foreach ($headers as $header => $check) {
			if ($check && isset($_POST[$header]))
				$this->handle_header(strtoupper($header), $_POST[$header]);
		}

		/* Set current method and function */
		$this->soap_function = $state['function'];

		/* XXX This is a lie, should be POST, fix this when post reply is implemented */
		$this->current_method = 'SOAP';

		if (!$state)
			$this->fault(array('Sender', 'ProcedureNotPresent'), "Function $func does not exist");

		$data = array();
		if (!empty($state['params'])) {
			/* XXX This is even uglier, fix so that we don't need to base encode the file */
			$files = array();
			if (count($_FILES)) {
				foreach ($_FILES as $file_param => $file) {
					if (file_exists($file['tmp_name']))
						$files[$file_param] = base64_encode(file_get_contents($file['tmp_name']));
				}
			}
			$args = $this->parse_params($state['params'], $_POST + $files);
		} else {
			$args = array();
		}

		$this->handle_response($state, $args);
	}

	function handle_rpc() {
		if (count ($this->body) != 1 || $this->body[0]['!tag'] == '!contents')
			$this->fault('Sender', 'Invalid RPC call');
		$rpc = $this->body[0];
		$func = $rpc['!tag'];

		$state = $this->get_state($func);
		if (!$state)
			$this->fault(array('Sender', 'rpc:ProcedureNotPresent'), "Function $func does not exist");

		$this->soap_function = $state['function'];
		$this->current_method = 'SOAP';

		$data = array();

		if (!empty($state['params'])) {
			foreach ($rpc['!contents'] as $arg) {
				if ($arg['!tag'] == '!contents') {
					/* XXX to be strict, only whitespace contents should be allowed. */
					continue;
				}
				$c = '';
				$x = array();
				foreach ($arg['!contents'] as $cont) {
					if ($cont['!tag'] != '!contents') {
						// XXX: Arrays; we only go one level deep.
						if ($cont['!contents'][0]['!tag'] == '!contents')
							$x[] = $cont['!contents'][0]['!contents'];
					} else {
						$c .= $cont['!contents'];
					}
				}
				if(!empty($x))
					$data[$arg['!tag']] = $x;
				else
					$data[$arg['!tag']] = $c;
			}
			$args = $this->parse_params($state['params'], $data);
		} else
			$args = array();

		$this->handle_response($state, $args);
	}
	
	function handle_response($state, $args) {
		/* XXX should check current_method for POST OR SOAP */

		if (isset($state['auth_required']) && !$this->validate_state()) {
			$this->fault('Sender', $state['auth_required']);
		}

		$result = call_user_func_array(array(&$this, $state['function']), $args);
		if (is_null($result))
			$this->fault('Receiver', 'Function returned null result');
		if (!is_array($result))
			$result = array('value' => $result);
		if ($this->current_method == "SOAP")
			array_walk_recursive($result, 'escape_xml');
		$ext_data = array();
		$ext_data['result'] = $result;
		if (!empty($state['result_tmpl']))
			$this->result_tmpl = $state['result_tmpl'];
		$this->display('soap/result.xml', null, $ext_data);
	}

	function get_tag($x) {
		return $x['!tag'];
	}
	function get_contents($x) {
		return $x['!contents'][0]['!contents'];
	}

}
?>
