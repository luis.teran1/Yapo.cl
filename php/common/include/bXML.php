<?php
require_once('util.php');

/*
 * Blocket XML parse module
 *
 * Usage:
 *   Create object with XML data and optional array with tags to treat as arrays
 *   Get result using get_result();
 *
 *   Example:
 * 
 *	require_once("bXML.php");
 *
 *	$xml = file_get_contents("test.xml");
 *	$x = new bXML($xml);
 *
 *	if ($x->parse_ok())
 *		var_dump($x->get_result());
 *	else
 *		print "Error message: ".$x->get_error_message()."\n";
 */

class bXML  {
	protected $xml_data;
	protected $xml_current_tag;
	protected $xml_tag_stack = array();
	protected $use_xml_stack = true;
	protected $error_message = "";
	protected $array_tags = false;
	protected $xml_tree = array();
	protected $parse_ok = false;
	protected $xml_element_data = '';

	function get_error_message() {
		return $this->error_message;
	}

	function parse_ok() {
		return $this->parse_ok;
	}

	function get_result() {
		return $this->xml_tree;
	}

	function element_content($parser, $data) {
		$this->xml_element_data .= $data;
	}

	function start_element($parser, $name, $attrs) {
		if ($this->xml_current_tag !== null)
			array_push($this->xml_tag_stack, $this->xml_current_tag);
		if ($this->use_xml_stack && $this->array_tags && in_array($name, $this->array_tags)) {
			$key = &$this->xml_tree;
			foreach ($this->xml_tag_stack as $path)
				$key = &$key[$path];
			if (!empty($key[$name])) {
				$this->xml_current_tag = count($key[$name]);
				array_push($this->xml_tag_stack, $name);
			} else {
				array_push($this->xml_tag_stack, $name);
				$this->xml_current_tag = 0;
			}
		} else
			$this->xml_current_tag = $name;
		$this->xml_element_data = '';
	}

	function end_element($parser, $name) {
		if ($this->xml_element_data !== '') {
			$key = &$this->xml_tree;
			$data = $this->xml_element_data;
			if (valid_utf8($data))
				$data = utf8_decode($data);

			if (@$this->use_xml_stack) {
				foreach ($this->xml_tag_stack as $path) {
					$key = &$key[$path];
				}
			}

			if ($this->array_tags && in_array($this->xml_current_tag, $this->array_tags)) {
				if (!isset($key[$this->xml_current_tag]))
					$key[$this->xml_current_tag] = array();
				array_push($key[$this->xml_current_tag], $data);
			} else {
				if (isset($key[$this->xml_current_tag]))
					$key[$this->xml_current_tag] .= $data;
				else
					$key[$this->xml_current_tag] = $data;
			}
			$this->xml_element_data = '';
		}

		if ($this->xml_tag_stack) {
			if (is_numeric($this->xml_current_tag)) /* Numeric index indicates an array element */
				array_pop($this->xml_tag_stack);
			$this->xml_current_tag = array_pop($this->xml_tag_stack);
		}
	}

	function bXML($xml, $array_tags = false) {

		$xml = preg_replace("/>"."[[:space:]]+"."</i","><", $xml);
		$xml = ltrim($xml);
		if (!$xml) {
			$this->error_message = "XML input empty.";
			return false;
		}

		$xmlparser = xml_parser_create();
 		if (!$xmlparser) {
			$this->error_message = "Couldn't create XML parser.";
			return false;
		}

		/* Add xml header if missing */
                if (strncmp($xml, "<?xml", 5) != 0) {
                        $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $xml;
                }

		if(isset($array_tags) && $array_tags)
			$this->array_tags = $array_tags;

		$this->xml_data = $xml;

		xml_set_object($xmlparser, $this);
		xml_set_element_handler($xmlparser, "start_element", "end_element");
		xml_set_character_data_handler($xmlparser, "element_content");
		xml_parser_set_option($xmlparser, XML_OPTION_SKIP_WHITE, 1);
		xml_parser_set_option($xmlparser, XML_OPTION_TARGET_ENCODING, "ISO-8859-1");

		$this->parse_ok = true;
		if (!xml_parse($xmlparser, $this->xml_data, true)) {
			$this->error_message = "ERROR_XML_PARSE: line " . xml_get_current_line_number($xmlparser) . 
				" col " . xml_get_current_column_number($xmlparser) . " : " . xml_error_string(xml_get_error_code($xmlparser));
			$this->parse_ok = false;
			return;
		}
	}

}

define ('XMLNS_XMLNS', 'http://www.w3.org/2000/xmlns/');

/*
 * XML parser that handles namespaces and gives a more structured result.
 * The result is an array describing the root tag, as such:
 *
 * array (
 *        '!tag' => 'tag name'
 *        '!ns' => 'namespace'
 *        '!contents' => array of tag arrays
 *        'attribute' => array ('ns' => 'value')
 *        ...
 * );
 * The attribute values are most easily fetchs with the get_ns_value function.
 *
 * All sub tags are similar to this. Text contents has a special kind of tag:
 * array (
 *        '!tag' => '!contents'
 *        '!contents' => 'text'
 * );
 *
 *
 * For example, lets parse this xml:
 * <test xmlns:t='http://example.org/ns'>
 *    <t:bing tjo='hej'>bong</bing>
 *    bang
 *    <t:bing>beng</bing>
 * </test>
 *
 * The result will be:
 * array (
 *        '!tag' => 'test',
 *        '!ns' => '',
 *        '!contents' => array (
 *                 array (
 *                        '!tag' => 'bing',
 *                        '!ns' => 'http://example.org/ns',
 *                        'tjo' => array ('' => 'hej'),
 *                        '!contents' => array (
 *                                 array (
 *                                        '!tag' => '!contents',
 *                                        '!contents' => 'bong'
 *                                 )
 *                        )
 *                 ),
 *                 array (
 *                        '!tag' => '!contents',
 *                        '!contents' => 'bang'
 *                 ),
 *                 array (
 *                        '!tag' => 'bing',
 *                        '!ns' => 'http://example.org/ns',
 *                        '!contents' => array (
 *                                 array (
 *                                        '!tag' => '!contents',
 *                                        '!contents' => 'beng'
 *                                 )
 *                        )
 *                 )
 *        )
 * );
 *
 * As you can see, the result is very structured, but quite verbose.
 */
class bXMLns extends bXML {
	var $xml_current;
	var $namespaces = array();

	function bXMLns($xmldata) {
		$this->xml_current = &$this->xml_tree;

		parent::bXML($xmldata);
	}

	function ns_split($name) {
		$col = strpos($name, ':');
		if ($col == -1 || $col === false)
			return array ('', $name);
		return array (substr ($name, 0, $col), substr ($name, $col + 1));
	}

	function nsname($name) {
		$n = $this->ns_split($name);
		return $n[1];
	}

	function namespace($name) {
		$n = $this->ns_split($name);
		$ns = $n[0];
		foreach ($this->namespaces as $nss) {
			if (isset($nss[$ns]))
				return $nss[$ns];
		}
		if ($ns == 'XMLNS')
			return XMLNS_XMLNS;
		if (substr($ns, 0, 3) == 'XML')
			return null;
		if ($ns == '')
			return '';
		$this->parse_ok = false;
		$this->error_message = "Namespace $ns not defined";
	}

	function push_namespaces($attrs) {
		$ns = array();
		foreach ($attrs as $name => $val) {
			$n = $this->ns_split($name);
			if ($n[0] == 'XMLNS')
				$ns[$n[1]] = $val;
			elseif ($n[0] == '' && $n[1] == 'XMLNS')
				$ns[''] = $val;
		}
		array_unshift($this->namespaces, $ns);
	}

	function pop_namespaces() {
		array_shift($this->namespaces);
	}

	function add_contents() {
		if ($this->xml_element_data !== '') {
			$data = $this->xml_element_data;
			if (valid_utf8($data))
				$data = utf8_decode($data);
			$this->xml_current[] = array('!tag' => '!contents', '!contents' => trim($data));
			$this->xml_element_data = '';
		}
	}

	function start_element($parser, $name, $attrs) {
		$this->add_contents();
		$this->push_namespaces($attrs);
		$tag = array();
		$tag['!tag'] = $this->nsname($name);
		$tag['!ns'] = $this->namespace($name);
		$tag['!contents'] = array();
		foreach($attrs as $name => $value) {
			$nsname = $this->nsname($name);
			$ns = $this->namespace($name);

			if (isset($tag[$nsname])) {
				if (isset($tag[$nsname][$ns])) {
					$this->error_message = "Duplicate attribute $nsname in namespace $ns";
					$this->parse_ok = false;
				}
				$tag[$nsname][$ns] = $value;
			} else
				$tag[$nsname] = array($ns => $value);
		}

		$ref = &$this->xml_current[];
		$ref = $tag;
		$this->xml_tag_stack[] = &$ref;
		unset($this->xml_current);
		$this->xml_current = &$ref['!contents'];
	}

	function end_element($parser, $name) {
		$this->add_contents();
		array_pop($this->xml_tag_stack);
		unset($this->xml_current);
		if (count($this->xml_tag_stack))
			$this->xml_current = &$this->xml_tag_stack[count($this->xml_tag_stack) - 1]['!contents'];
		$this->pop_namespaces();
	}

	function get_ns_value($ns_array, $ns = '') {
		if (isset($ns_array[$ns]))
			return $ns_array[$ns];
		/* Assume we want the value if there's only one. */
		if (count($ns_array) == 1)
			return reset($ns_array);
		/* Default to empty ns, if it exists */
		if (isset($ns_array['']))
			return $ns_array[''];
		return null;
	}
}

?>
