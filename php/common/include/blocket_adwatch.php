<?php

class blocket_adwatch extends blocket_application
{
	var $page_title;
	var $page_name;
	var $head_styles;
	var $headscripts;
	
	var $watch_unique_id;
	var $selected_tab;
	var $adw_sp;
	var $failover_data;

	/* Cache */
	var $watch_queries;
	var $watch_queries_cachetimeout;
	var $watch_ads;
	var $watch_ads_cachetimeout;
	var $last_view;
	var $all_has_new;

	public function __construct() {
		global $BCONF;
		global $script_list;

		$this->page_title = lang('PAGE_ADWATCH_TITLE');
		$this->page_name = lang('PAGE_ADWATCH_NAME');
		$this->headstyles = bconf_get($BCONF,"*.common.adwatch.headstyles");
		$this->headscripts = bconf_get($BCONF,"*.common.adwatch.headscripts");

		if ($this->show_sms_watch()) {
			$this->headscripts[] = 'adwatch_sms.js';
		}

		$this->last_view = array();
		$this->application_name = 'adwatch';
		$this->add_volatile_vars(array('adw_sp'));
		$this->extra_bconf = bconf_get($BCONF, "unwire_sms");
		$this->init('start', 1, true);

		if (isset($_GET['ca'])) {
			$this->caller = new blocket_caller();
		}
	}

	public function get_state($statename) {
		switch($statename) {
		case 'start':
			return array('function' => 'aw_start',
				     'method' => 'get');
		case 'no_cookies':
			return array('function' => 'aw_no_cookies',
				     'method' => 'get');
		case 'presentation':
			return array('function' => 'aw_presentation',
				     'method' => 'get',
				     'allowed_states' => array('presentation'));
		case 'list_ads':
			return array('function' => 'aw_list_ads',
				     'method' => 'get',
				     'params' => array ('sp' => 'f_integer_or_null', 'md' => 'f_md', 'o' => 'f_integer_or_null', 'mp' => 'f_integer_or_null'));
		case 'list_all':
			return array('function' => 'aw_list_all',
				     'method' => 'get',
				     'params' => array ('sp' => 'f_integer', 'md' => 'f_md', 'o' => 'f_integer_or_null', 'mp' => 'f_integer_or_null'),
				     'allowed_states' => array('list', 'list_ads', 'list_all'));
		case 'list':
			return array('function' => 'aw_list',
				     'method' => 'get',
				     'params' => array ('query_id' => 'f_integer', 'sp' => 'f_integer_or_null', 'md' => 'f_md', 'o' => 'f_integer_or_null', 'sms' => 'f_integer_or_null', 'from_save' => 'f_integer_or_null', 'mp' => 'f_integer_or_null'),
				     'allowed_states' => array('presentation', 'list', 'list_ads', 'list_all'));
		case 'ajax_gencode':
			return array('function' => 'aw_ajax_gencode',
				     'method' => 'get',
				     'params' => array ('query_id' => 'f_integer'));
		case 'save':
			return array('function' => 'aw_save',
				     'params' => array ('query_id' => 'f_integer', 'sms' => 'f_integer_or_null'),
				     'method' => 'both');
		case 'watch_ad':
			return array('function' => 'aw_watch_ad',
				     'params' => array('aid' => 'f_integer'),
				     'method' => 'get');
		case 'delete_ad':
			return array('function' => 'aw_delete_ad',
				     'params' => array('aid' => 'f_integer'),
				     'method' => 'get');
		case 'delete':
			return array('function' => 'aw_delete',
				     'params' => array ('query_id' => 'f_integer'),
				     'method' => 'get');
		case 'info':
			return array('function' => 'aw_info',
				     'method' => 'get');
		case 'sms_stop':
			return array('function' => 'aw_sms_stop',
				     'params' => array('query_id' => 'f_integer'),
				     'method' => 'get');
		}
	}

	public function load($from_save = NULL) {
		global $BCONF;

		$expiration = bconf_get($BCONF, "*.adwatch.delete_before");

		/* Deleting saved queries from old yapo */
		if (!empty($expiration) && isset($this->watch_unique_id) && !empty($this->watch_unique_id)) {
			$this->delete_old_watch_queries();
		}

		if ($this->check_cache_expired($this->watch_queries_cachetimeout)) {
				$this->load_watch_queries();
		}
		$this->store_cookie_watch_ads();
		if ($this->check_cache_expired($this->watch_ads_cachetimeout)) {
			$this->load_watch_ads();
		}

		$this->all_has_new = 0;
		if (is_array($this->watch_queries)) {
			foreach ($this->watch_queries as $idx => $query) {
				if (!isset($query['title'])) {
					/* Generate the title, truncate only the query */
					list($this->watch_queries[$idx]['title'], $this->watch_queries[$idx]['title_full']) = $this->generate_title($query['query_string']);
					/* Get query data */
					$this->watch_queries[$idx]['query_lookup_data'] = $this->lookup_query($query['query_string']);
				}
				$this->watch_queries[$idx]["result_size"]=0;

				$query_data = $this->watch_queries[$idx]['query_lookup_data'];		
				/* Check for new ads */
				@bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "{$query_data['sq_offset']} lim:1 {$query_data['sq_count_level']} {$query_data['sq_filter']} {$query_data['sq_params']} *:* {$query_data['sq_query']}", array($this, 'check_new_ads_cb'), $this->watch_queries[$idx]);
				$enabled_fail_over = (bconf_get($BCONF,"adwatch.failover.enabled")=="1");
				$limit_fail_over = bconf_get($BCONF,"adwatch.failover.search");
				$this->watch_queries[$idx]["failover_search"]=0;

				// If fail_over is enabled and result_size is insufficient we do failover_search for the aw
				if ($enabled_fail_over && (intval($limit_fail_over) > intval($this->watch_queries[$idx]["result_size"]))) {
					$this->aw_failover($idx);
				}

				//break;
				if (@$this->watch_queries[$idx]['has_new_ads']) {
					$this->all_has_new = 1;
				}

				if (isset($from_save) && $from_save == $query['watch_query_id']) {
					$trans = new bTransaction();
					$trans->add_data('watch_query_id', $query['watch_query_id']);
					$trans->add_data('watch_unique_id', $this->watch_unique_id);
					if ($query['watch_query_sms_status'] == 'active' && $this->watch_queries[$idx]['result_size'] > bconf_get($BCONF, "*.common.sms_watch_result_limit")) {
						$trans->add_data('sms_status', 'paused');
						$res = $trans->send_command("set_watch_query_sms_status");
						if (!$trans->has_error()) {
							$this->watch_queries[$idx]['watch_query_sms_status'] = 'paused';
						}
					} else if ($query['watch_query_sms_status'] == 'paused' && $this->watch_queries[$idx]['result_size'] <= bconf_get($BCONF, "*.common.sms_watch_result_limit")) {
						$trans->add_data('sms_status', 'active');
						$res = $trans->send_command("set_watch_query_sms_status");
						if (!$trans->has_error()) {
							$this->watch_queries[$idx]['watch_query_sms_status'] = 'active';
						}
					}
				}
			}
		}
	}

	public function aw_failover($idx) {
		global $BCONF;
		$limit_fail_over = intval(bconf_get($BCONF,"adwatch.failover.search"));
		$this->watch_queries[$idx]["original_query_string"]=$this->watch_queries[$idx]["query_string"];	
		$qs_params = array();
		parse_str($this->watch_queries[$idx]["query_string"],$qs_params);
		$where = intval($qs_params["w"]);
		/* who means company, private or both if not defined*/
		if(isset($qs_params["f"])) {
			$who = $qs_params["f"];
		}
		else {
			$who = "a";
		}

		if ($where <= 0) {
			$where = 1;
		}

		$category="0";
		if(isset($qs_params["cg"])) {
			$category = intval($qs_params["cg"]);
		}

		$this->watch_queries[$idx]["original_w"] = $where;	
		$this->watch_queries[$idx]["original_cg"] = $category;
		$this->watch_queries[$idx]["original_f"] = $who;
		$total = 0;
		$query = "";

		while(1){
			if ($who != "a") {
				// Failover over f
			  	$who = "a";
			} else if ($where!=3) {
			  	switch($where){
				case -1:
					$where = 1;
					break;
				default:
					$where = 3;
					break;
				}
			} else if ($category !=0) {
			  	/*Else, if we have not iterated over the categories*/
			  	syslog(LOG_INFO,log_string()."Este es el valor de la categoria $category");

				//nothing found expanding region so we look up category parent			
				$category_level = intval(bconf_get($BCONF,"*.cat.$category.level"));	
				syslog(LOG_INFO,log_string()."Este es el valor de categoria.level $category_level");

				//nothing found expanding region so we look up category parent			
				if ($category_level > 0) {
					$category = intval(bconf_get($BCONF,"*.cat.$category.parent"));	
					syslog(LOG_INFO,log_string()."Este es el valor de la nueva categoria $category");
				} else {
					$category = 0;
				}
				$where = $this->watch_queries[$idx]["original_w"];
			} else {
				break;
			}

			/* Get query data */
			$qs_params["w"]=$where;	
			$qs_params["cg"]=$category;
			$qs_params["f"]=$who;
			$query = http_build_query($qs_params);
			$query_data = $this->lookup_query($query);
			$this->watch_queries[$idx]["query_lookup_data"]=$query_data;	
			/* Check for new ads on failover*/
			$this->watch_queries[$idx]["query_string"]=$query;
			@bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "{$query_data['sq_offset']} lim:1 {$query_data['sq_count_level']} {$query_data['sq_filter']} {$query_data['sq_params']} *:* {$query_data['sq_query']}", array($this, 'check_new_ads_cb'), $this->watch_queries[$idx]);
			$total= intval($this->watch_queries[$idx]["result_size"]);
			if($total>= $limit_fail_over){
				$this->watch_queries[$idx]["failover_search"] = 1;
				break;
			}
		}

		$this->watch_queries[$idx]["search_failover_category"] = -1;
		$this->watch_queries[$idx]["search_failover_where"] = -1;
		$this->watch_queries[$idx]["search_failover_f"] = -1;

		if ($total > 0 && ($this->watch_queries[$idx]["original_cg"] != $category)) {
			$this->watch_queries[$idx]["search_failover_category"] = $category;
		}
		if ($total > 0 && ($this->watch_queries[$idx]["original_w"] != $where)) {
			$this->watch_queries[$idx]["search_failover_where"] = $where;
		}
		if ($total > 0 && ($this->watch_queries[$idx]["original_f"] != $who)) {
			$this->watch_queries[$idx]["search_failover_f"] = 1;
		}
	}
	
	public function aw_start() {
		global $session;

		if ($session->session_init_method() == 'url') {
			return 'no_cookies';
		}

		/* Load user cookie */
		$this->watch_unique_id = $this->get_watch_cookie();

		$this->load();
		$this->get_selected_tab();
		return 'presentation';
	}

	public function aw_no_cookies () {
		$this->display_layout ('adwatch/no_cookie_support.html');
		return 'FINISH';
	}

	public function aw_presentation() {
		$this->page_title = lang('PAGE_ADWATCH_TITLE');
		$this->page_name = lang('PAGE_ADWATCH_NAME');

		/* If we are redirected here with a cookie we must delete it. */
		if($this->get_watch_cookie() !== false) {
			$this->delete_watch_cookie();
		}

		$this->display_layout("adwatch/presentation.html");
	}

	public function aw_list_ads($sp = NULL, $md = NULL, $o = NULL, $mp = NULL) {
		global $BCONF;
		
		/* Load cookie user */
		$this->watch_unique_id = $this->get_watch_cookie();

		$this->load();

		$data = array();
		$data['md'] = $this->get_listing_mode();

		if (!is_null($sp) && $sp == 0) {
			$this->delete_sp_cookie();
		} else if (!is_null($sp) && $sp != 0) {
			$data['sp'] = $sp;
			$this->adw_sp = $sp;
		} else if ($sp = $this->get_sp_cookie()) {
			$data['sp'] = $sp;
			$this->adw_sp = $sp;
		}
		
		if (!is_null($md)) {
			$this->set_list_mode_cookie($md);
			$data['md'] = $md;
		}
		
		if (!is_null($mp)) {
			$data['mp'] = $mp;
		}

		$data['page'] = is_null($o) ? 0 : $o;

		/* Update cookie expiry */
		$this->set_watch_cookie();

		if (!$this->watch_ads) {
			return array('start');
		}
		$this->set_selected_tab('list_ads', $sp);

		$this->set_page_title_name();
		
		$this->display_layout("adwatch/list_ads.html", $data);
		$this->set_errors(null);
	}

	public function aw_list_all($sp = NULL, $md = NULL, $o = NULL, $mp = NULL) {
		global $BCONF;

		$this->load();

		$data = array();
		$data['md'] = $this->get_listing_mode();

		if (!is_null($sp)) {
			$data["sp"] = $sp;
		}

		if (!is_null($md)) {
			$this->set_list_mode_cookie($md);
			$data['md'] = $md;
		}
		
		if (!is_null($mp)) {
			$data['mp'] = $mp;
		}

		$data['page'] = is_null($o) ? 0 : $o;

		/* Update cookie expiry */
		$this->set_watch_cookie();

		if (!$this->watch_queries || count($this->watch_queries) < 2) {
			return array('start');
		}

		$this->set_selected_tab('list_all', $sp);

		$lv = time();
		foreach ($this->watch_queries as $idx => $query) {
			$tlv = strtotime($query['last_view']) - bconf_get($BCONF, 'adwatch.new_visible_time') * 60;
			if ($tlv < $lv) {
				$lv = $tlv;
			}
			$this->update_last_view($query['watch_query_id']);
		}
		$data['last_view'] = date("Y-m-d H:i:s", $lv);

		$this->set_page_title_name();
		$this->display_layout("adwatch/list_all.html", $data);
		$this->set_errors(null);
	}

	/* AJAX request handling*/
	public function aw_ajax_gencode($query_id) {
		$this->load();

		if ($this->show_sms_watch()) {
			$code = $this->return_sms_code($query_id);
			if($code === false || $code === null) {
				echo "{reload: true}";
			}
			else {
				echo "{code:$code}";
			}
		}
	}

	public function aw_list($query_id, $sp = NULL, $md = NULL, $o = NULL, $sms = NULL, $from_save = NULL, $mp = NULL) {
		global $BCONF;
		$this->load($from_save);

		$data = array();
		$data['md'] = $this->get_listing_mode();

		if (!is_null($sp) && $sp == 0) {
			$this->delete_sp_cookie();
		} else if (!is_null($sp) && $sp != 0) {
			$data['sp'] = $sp;
			$this->adw_sp = $sp;
		} else if ($sp = $this->get_sp_cookie()) {
			$data['sp'] = $sp;
			$this->adw_sp = $sp;
		}

		if (!is_null($md)) {
			$this->set_list_mode_cookie($md);
			$data['md'] = $md;
		}
		
		if (!is_null($mp)) {
			$data['mp'] = $mp;
		}

		$data['page'] = is_null($o) ? 0 : $o;

		/* Update cookie expiry */
		$this->set_watch_cookie();

		if ($this->watch_queries) {
			/* Set selected tab */
			if ($query_id) {
				$this->set_selected_tab($query_id, $sp);
			}
			else {
				$query_id = $this->get_selected_tab($sp);
			}

			$selected_tab_ok = false;
			$first_query_id = null;
			foreach ($this->watch_queries as $idx => $query) {
				if ($this->selected_tab == $query['watch_query_id']) {
					$selected_tab_ok = true;
				}
				if ($first_query_id === null) {
					$first_query_id = $query['watch_query_id'];
				}
			}

			if (!$selected_tab_ok) {
				$this->set_selected_tab($first_query_id, $sp);
			}

			foreach ($this->watch_queries as $idx => $query) {
				if ($query['watch_query_id'] == $this->selected_tab) {
					/* Get last view time */
					$data['last_view'] = date("Y-m-d H:i:s", strtotime($query['last_view']) - bconf_get($BCONF, 'adwatch.new_visible_time') * 60);

					/* Get the failover info */
					$data["total"]=$this->watch_queries[$idx]["result_size"];
					if(isset($this->watch_queries[$idx]["search_failover_category"]) &&
							$this->watch_queries[$idx]["search_failover_category"] != -1)
						$data["search_failover_category"]=$this->watch_queries[$idx]["search_failover_category"];
					if(isset($this->watch_queries[$idx]["search_failover_where"]) &&
						$this->watch_queries[$idx]["search_failover_where"] != -1 )
						$data["search_failover_where"	]=$this->watch_queries[$idx]["search_failover_where"];
					if(isset($this->watch_queries[$idx]["search_failover_f"]) &&
						$this->watch_queries[$idx]["search_failover_f"] != -1 )
						$data["search_failover_f"]=$this->watch_queries[$idx]["search_failover_f"];
					if(!isset($this->watch_queries[$idx]["original_query_string"]))
						$this->watch_queries[$idx]["original_query_string"] = $this->watch_queries[$idx]["query_string"];
					break;
				}
			}
			$this->update_last_view($this->selected_tab);
		}
		$this->set_page_title_name();
		$this->display_layout("adwatch/list.html", $data);
		$this->set_errors(null);
	}

	public function check_new_ads_cb(&$data, $key, $value) {
		global $BCONF;

		if ($key == 'filtered') {
			$data['result_size'] = $value;
		}
		if ($key == 'date') {
			$date = strtotime($value);
			$last_view = strtotime($data['last_view']);
			if ($date > ($last_view - bconf_get($BCONF, 'adwatch.new_visible_time') * 60)) {
				$data['has_new_ads'] = 1;
			}
			else {
				$data['has_new_ads'] = 0;
			}
		}
	}

	public function watch_ad($list_id, $dont_get_cookie = false) {
		if (!$dont_get_cookie) {
			$this->watch_unique_id = $this->get_watch_cookie();
		}
		
		$transaction = new bTransaction();
		if ($this->watch_unique_id) {
			$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		}

		$transaction->add_data('list_id', $list_id);
		$reply = $transaction->send_command('watch_ad', true, true);
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return false;
		}
		
		$unique_id = $reply['watch_ad'][0]['watch_unique_id'];
		if ($unique_id && $unique_id != $this->watch_unique_id) {
			$this->invalidate_watch_queries_cache();
			$this->watch_unique_id = $reply['watch_ad'][0]['watch_unique_id'];
		}
		/* Set saved query to selected */
		$this->set_selected_tab('list_ads');

		$this->invalidate_watch_ads_cache(); /* Invalidate cache */
		return true;
	}

	public function aw_watch_ad($list_id) {
		if ($this->watch_ad($list_id)) {
			return array('list_ads');
		}
		return array('start');
	}

	public function watch_query($query_string, $query_id = 0, $dont_get_cookie = false) {
		if (!$dont_get_cookie) {
			$this->watch_unique_id = $this->get_watch_cookie();
		}
		
		$qs = $this->filter_query_string($query_string);
		$transaction = new bTransaction();
	
		if ($this->watch_unique_id) {
			$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		}

		if ($query_id) {
			$transaction->add_data('watch_query_id', $query_id);
		}

		$transaction->add_data('query_string', $qs);
		$reply = $transaction->send_command('watch_query', true, true);

		if ($transaction->has_error()) {
			// this is technically not an error
			if ($reply['error'] == 'ERROR_WATCH_QUERY_EXISTS') {
				return true;
			}
			$this->set_errors($transaction->get_errors());
			return false;
		}

		$unique_id = $reply['watch_query'][0]['watch_unique_id'];
		if ($unique_id && $unique_id != $this->watch_unique_id) {
			$this->invalidate_watch_ads_cache(); /* Invalidate cache */
			/* Save unique id */
			$this->watch_unique_id = $reply['watch_query'][0]['watch_unique_id'];
		}
		/* Set saved query to selected */
		$this->set_selected_tab($reply['watch_query'][0]['watch_query_id']);

		$this->invalidate_watch_queries_cache(); /* Invalidate cache */
		return true;
	}

	public function aw_save($query_id, $sms = null) {
		$this->watch_unique_id = $this->get_watch_cookie();

		if ($this->watch_query($_SERVER['QUERY_STRING'], $query_id)) {
			if ($sms) {
				return array('list', 'from_save' => $query_id, 'sms' => $sms);
			}
			return array('list', 'from_save' => $query_id);			
		}
		return array('start');
	}

	public function aw_delete_ad($list_id) {
		$this->watch_unique_id = $this->get_watch_cookie();
		$transaction = new bTransaction();
		
		if ($this->watch_unique_id)  {
			$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		}

		$transaction->add_data('list_id', $list_id);		
		$reply = $transaction->send_command('delete_watch_ad', true, true);

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return array('start');
		}

		$this->invalidate_watch_ads_cache(); /* Invalidate cache */
		
		// Delete cookies if there are no watch-queries left (watch_user_id returned as 0)
		if(@$reply['delete_watch_ad'][0]['watch_user_id'] == 0) {
			return array('presentation');
		}

		return array('list_ads');
	}

	public function aw_delete($query_id) {
		$this->watch_unique_id = $this->get_watch_cookie();
		$transaction = new bTransaction();
		if($this->watch_unique_id) {
			$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		}
		if($query_id) {
			$transaction->add_data('watch_query_id', $query_id);	
		}

		$reply = $transaction->send_command('delete_watch_query', true, true); 	

		if($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return array('start');
		}

		$this->invalidate_watch_queries_cache(); /* Invalidate cache */
		return array('list');
	}

	public function aw_info() {
		$this->page_title = lang('PAGE_ADWATCH_MORE_INFO');
		$this->display_layout("adwatch/presentation.html");
	}

	public function aw_sms_stop($query_id) {
		$this->load();

		$transaction = new bTransaction();
		$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		$transaction->add_data('watch_query_id', $query_id);	

		$reply = $transaction->send_command('adwatch_sms_stop');

		if($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return array('list');
		}

		$this->invalidate_watch_queries_cache();

		return array('list', 'query_id' => $query_id);
	}

	public function delete_watch_cookie() {
		global $BCONF;

		setcookie(bconf_get($BCONF, 'adwatch.cookie_name'), '', 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		setcookie(bconf_get($BCONF, 'adwatch.cookie_selected_tab'), '', 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		$this->delete_sp_cookie();
	}	

	public function get_sp_cookie() {
		global $BCONF;

		$cookie = @$_COOKIE[bconf_get($BCONF, 'adwatch.cookie_price_sorting')];
		if (!empty($cookie)) {
			return $cookie;
		}
		return NULL;
	}
	
	public function delete_sp_cookie() {
		setcookie(bconf_get($BCONF, 'adwatch.cookie_price_sorting'), '', 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		$this->adw_sp = 0;
	}

	public function get_watch_cookie() {
		global $BCONF;

		$cookie = @$_COOKIE[bconf_get($BCONF, 'adwatch.cookie_name')];
		if (!empty($cookie)) {
			return $cookie;
		}
		return false;
	}

	public function set_list_mode_cookie($md) {
		global $BCONF;

		setcookie('md', "", 0, '/');
		setcookie('md', $md, time () + bconf_get($BCONF, "*.common.listing_mode.cookie_expire_days") * 86400, '/', bconf_get($BCONF, '*.common.cookie.domain'));
	}

	public function set_watch_cookie() {
		global $BCONF;

		if ($this->watch_unique_id) {
			setcookie(bconf_get($BCONF, 'adwatch.cookie_name'), $this->watch_unique_id, time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		}
	}

	public function set_page_title_name() {
		if(!empty($this->watch_queries)) {
			foreach($this->watch_queries as $idx => $query) {
				if($this->selected_tab == $query['watch_query_id']) {
					if (strlen($this->watch_queries[$idx]['title_full']) > 70) {
						$this->page_name = substr($this->watch_queries[$idx]['title_full'], 0, 70);
					}
					else {
						$this->page_name = $this->watch_queries[$idx]['title_full'];
					}

					$this->page_name = trim($this->page_name, ', ');
					$this->page_title = trim($this->watch_queries[$idx]['title_full']);
					return;
				}
			}
		}
		$this->page_title = lang('PAGE_ADWATCH_TITLE');
		$this->page_name = lang('PAGE_ADWATCH_NAME');
	}

	public function check_cache_expired($timeout) {
		global $BCONF;
		if (!isset($timeout) || is_null($timeout)) {
			return true;
		}

		if (@$_COOKIE[bconf_get($BCONF, "adwatch.cookie_invalidate_cache")]) {
			setcookie(bconf_get($BCONF, "adwatch.cookie_invalidate_cache"), '', 0);
			return true;
		}

		if ((time() - bconf_get($BCONF, "adwatch.cache_expire_min") * 60) > $timeout) {
			return true;
		}

		return false;
	}

	public function lookup_query($query_string) {
		$result = array();
		$response = new bResponse();
		$response->add_data('qs', $query_string);
		$data = explode("\n", trim($response->call_template('adwatch/lookup_query.txt', true)));
		foreach ($data as $line) {
			list ($key, $value) = explode("=", $line, 2);
			$result[$key] = $value;
		}
		return $result;
	}

	public function generate_title($qs) {
		$response = new bResponse();

		parse_str($qs, $vars);

		if (isset($vars['q'])) {
			$vars['q'] = trim($vars['q']);
		}

		$response->add_data('qs', http_build_query($vars));
		$title_full = htmlentities(trim($response->call_template('adwatch/title.txt', true)));
		if (isset($vars['q']) && strlen($vars['q']) > 30) {
			$vars['q'] = trim(substr($vars['q'], 0, 30), ', ');
		}
		$response->add_data('qs', http_build_query($vars));
		$title = preg_replace('/,,/', ',', trim($response->call_template('adwatch/title.txt', true)));

		$title = str_replace('Vendo vendo', 'Comprar',$title);
		$title = str_replace('Arriendo arriendo', 'Arrendar',$title);
		$title = str_replace('Arriendo vendo', 'Arrendar',$title);
		
		$title_full = str_replace('Vendo vendo', 'Comprar',$title_full);
		$title_full = str_replace('Arriendo arriendo', 'Arrendar',$title_full);
		$title_full = str_replace('Arriendo vendo', 'Arrendar',$title_full);
		return array($title, $title_full);
	}

	public function filter_query_string($qs){
		global $BCONF;

		$vars = bconf_get($BCONF, '*.qs.vars');
		$query = array();
		foreach(explode("&",$qs) as $num => $key_value){
			$arr = explode("=",$key_value);
			$key = $arr[0];
			$value = $arr[1];
			/* This ugly if avoid XSS injection */
			if ($key == "q") {
				$value = urlencode(sanitizeVar(urldecode($value), 'string', FILTER_FLAG_NO_ENCODE_QUOTES));
			}
			$ignore_in_watch_query_regex = @$vars[$key]['ignore_in_watch_query_regex'];
			if ($ignore_in_watch_query_regex && preg_match("/$ignore_in_watch_query_regex/", $value)){
				continue;
			}
			$regex = @$vars[$key]['regex'];
			if ($regex && preg_match("/$regex/", $value)){
				$str[] = $key.'='.$value;
			}
		}
		return implode("&",$str);
	}

	/* Invalidate cache */
	public function invalidate_watch_queries_cache() {
		$this->watch_queries = null;
		$this->watch_queries_cachetimeout = null;
	}

	public function invalidate_watch_ads_cache() {
		$this->watch_ads = null;
		$this->watch_ads_cachetimeout = null;
	}

	public function delete_old_watch_queries() {
		$transaction = new bTransaction();
	
		$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		$data = $transaction->send_command('delete_watch_queries', true, true);

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return null;
		}
	
		if (isset($data['o_deleted_queries']) && $data['o_deleted_queries'] > 0) {
			$this->deleted_old_watch_queries = $data['o_deleted_queries'];
		}
		
		if (isset($data['o_watch_user_id']) && $data['o_watch_user_id'] > 0) {
			$this->deleted_old_user_id = $data['o_watch_user_id'];
		}
		
		/* If the user is in adwatch now just reset cache */
		if (isset($this->watch_queries_cachetimeout) && !empty($this->watch_queries_cachetimeout)) {
			$this->watch_queries_cachetimeout = NULL;
		}
	}

	/* Load current watch queries from db. */
	public function load_watch_queries() {
		$transaction = new bTransaction();

		$this->watch_queries = null;

		$transaction->add_data('watch_unique_id', $this->watch_unique_id);

		$data = $transaction->send_command('get_watch_queries', true, true);

		if (isset($data['get_watch_queries'])) {
			$this->watch_queries = $data['get_watch_queries'];
		}

		$this->watch_queries_cachetimeout = time();
	}

	/*
	 * Function to store ads to watch_ads db
	 * if the ads has been stored to a cookie
	 * instead of directly to db
	 *
	 * The storage to cookie instead of db directly
	 * is configured by:
	 * bconf.txt:*.adwatch.save_ads.cookie_storage=1
	 */
	public function store_cookie_watch_ads() {
		global $BCONF;
		$cookie_name = bconf_get($BCONF, '*.common.adwatch.save_ads.cookie_name');
		if (!empty($cookie_name) && isset($_COOKIE[$cookie_name]) && !empty($_COOKIE[$cookie_name])) {
			/* Save cookie save ads to db */
			$ads = explode(',', $_COOKIE[$cookie_name]);
			if (is_array($ads)) {
				foreach ($ads as $idx => $list_id)
					$ads[$idx] = intval($list_id);

				/* Run transaction to save ads */
				/* And clean cookie */
				if ($this->watch_ad($ads)) {
					setcookie($cookie_name, '', 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
				}
			}
		}
	}

	/* Load currents saved ads from db */
	public function load_watch_ads() {
		$transaction = new bTransaction();

		$transaction->add_data('watch_unique_id', $this->watch_unique_id);

		$data = $transaction->send_command('get_watch_ads', true, true);
		$this->watch_ads = @$data['get_watch_ads'];

		$this->watch_ads_cachetimeout = time();
	}

	public function set_selected_tab($tab_id, $sp = null) {
		global $BCONF;
		
		if (!$tab_id) {
			return;
		}
		
		$this->selected_tab = $tab_id;

		// Store price sorting in cookie
		if ($this->adw_sp == 1) {
			setcookie(bconf_get($BCONF, 'adwatch.cookie_price_sorting'), 1, 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		}
		else {
			setcookie(bconf_get($BCONF, 'adwatch.cookie_price_sorting'), '', 0, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
		}

		if ($tab_id == "list_ads") {
			// Search query cookie
			setrawcookie(bconf_get($BCONF, 'adwatch.current_search_query_cookie'),
				  "ca=" . $this->caller->to_string() . '&wid=' . $tab_id,
				  time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400,
				  '/',
				  bconf_get($BCONF, 'adwatch.cookie_domain'));
		} elseif ($tab_id == "list_all") {
			$queries = array();
			foreach ($this->watch_queries as $idx => $query) {
				//FIX to avoid setting a modified query_string done in aw_fail_over
			        $queries["wq$idx"] = $query['query_string'];
			}
			$sq = http_build_query($queries);
			if ($sp) {
				$sq .= "&sp=1";
			}
			setrawcookie(bconf_get($BCONF, 'adwatch.current_search_query_cookie'),
				  $sq . "&wid=list_all",
				  time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400,
				  '/',
				  bconf_get($BCONF, 'adwatch.cookie_domain'));
		} elseif ($this->watch_queries) {
			foreach ($this->watch_queries as $idx => $query) {
				if ($tab_id == $query['watch_query_id']) {
					setrawcookie(bconf_get($BCONF, 'adwatch.current_search_query_cookie'),
						$query['query_string'] . '&wid=' . $tab_id . ($this->adw_sp ? '&sp=1' : ''),
						time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400,
						'/',
						bconf_get($BCONF, 'adwatch.cookie_domain'));
					break;
				}
			}
		}

		// Current selected query tab
		setcookie (bconf_get ($BCONF, 'adwatch.cookie_selected_tab'), $tab_id, time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400, '/', bconf_get($BCONF, 'adwatch.cookie_domain'));
	}

	public function get_selected_tab ($sp = null) {
		global $BCONF;

		$query_id = intval(@$_COOKIE[bconf_get($BCONF, 'adwatch.cookie_selected_tab')]);
		$this->set_selected_tab($query_id, $sp);
		return $query_id;
	}

	public function get_listing_mode() {
		global $BCONF;

		if(isset($_GET['th']))
			return $_GET['th'] ? "th" : "li";

		if (isset($_COOKIE['md'])) {
			if(in_array($_COOKIE['md'], bconf_get($BCONF, '*.common.listing_mode.modes')))
				return ($_COOKIE['md']);
			else
				return bconf_get($BCONF, '*.common.listing_mode.default');
		}

		/* Remove in some future */
		if(isset($_COOKIE['th']))
			return $_COOKIE['th'] ? "th" : "li";

		return bconf_get($BCONF, '*.common.listing_mode.default');
	}

	public function update_last_view($query_id) {
		global $BCONF;

		if (isset($this->last_view[$query_id]) &&
		    $this->last_view[$query_id] > time() - bconf_get($BCONF, 'adwatch.last_view_interval')) {
			return;
		}

		$transaction = new bTransaction();

		$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		$transaction->add_data('watch_query_id', $query_id);
		$transaction->add_client_info();

		$command = $transaction->serialize('view_watch_query');

		$transaction->reset();
		$transaction->add_data('sub_queue', 'view_watch_query');
		$transaction->add_data('info', "{$this->watch_unique_id}:{$query_id}");
		$transaction->send_command('at_clear');

		$transaction->reset();
		$transaction->add_data('command', $command);
		$transaction->add_data('rel', sprintf("00:%02d", bconf_get($BCONF, "adwatch.new_visible_time")));
		$transaction->add_data('sub_queue', 'view_watch_query');
		$transaction->add_data('info', "{$this->watch_unique_id}:{$query_id}");
		$transaction->send_command('at');

		$this->last_view[$query_id] = time();
	}

	public function get_active_query($query_id) {
		if(isset($this->watch_queries)) {
			if(count($this->watch_queries) == 1 || $query_id == 0)
				return $this->watch_queries[0];

			foreach($this->watch_queries as $query) {
				if($query['watch_query_id'] == $query_id)
					return $query;
			}
		}

		return null;
	}

	public function return_sms_code($query_id) {
		$this->invalidate_watch_queries_cache();
		$this->load();

		$query = $this->get_active_query($query_id);

		if($query == null)
			return null;

		if($query['adwatch_sms_active'] == 't') {
			return false;
		}

		if($query_id == 0)
			$query_id = 1;

		$transaction = new bTransaction();
		$transaction->add_data('watch_unique_id', $this->watch_unique_id);
		$transaction->add_data('watch_query_id', $query_id);

		$reply = $transaction->send_command('reserve_adwatch_activation_code', true, true);
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return null;
		}
		$this->invalidate_watch_queries_cache();
		$this->load();

		return @$reply['reserve_adwatch_activation_code'][0]['activation_code'];
	}

	/* XXX IP address restriction */
	private function show_sms_watch() {
		global $BCONF;
		if (bconf_get($BCONF, "*.common.adwatch_sms.enabled")) {
			return 1;
		}
		if (bconf_get($BCONF, "*.common.ip_allowed_override")) {
			return 1;
		}
		$allowed_ips = bconf_get($BCONF, "adwatch.allowed_ip.blocket");
		if ($allowed_ips === NULL) {
			return 0;
		}

		if (isset($_SERVER['REMOTE_ADDR'])) {
			return in_array($_SERVER['REMOTE_ADDR'], array($allowed_ips));
		}
		else {
			return false;
		}
	}

	public function f_md($md) {
		if(in_array($md, array("th", "li", "mp"))) {
			return $md;
		}
	}
}
