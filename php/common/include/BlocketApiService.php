<?php

namespace Yapo;
use Yapo\interfaces\PartnerService;

class BlocketApiService extends \bRedisClass implements PartnerService
{
    private $redisKey;
    private $redisPrefix;
    private $maps;

    public function __construct()
    {
        $this->redisPrefix = 'mcp1xapp_id_';
        $this->redisKeys = array('api_key'=>'');

        $params = array(
            'expire_time',
            'region_limit',
            'ad_type_limit',
            'request_limit',
            'appl_limit',
            'cat_limit',
            'logging'
        );
        foreach ($params as $param) {
            $this->redisKeys[$param] = bconf_get($BCONF, "*.partner.blocket_api.$param");
        }
        $this->maps = array(
            'ApiKey' => 'api_key',
        );

        parent::__construct();
    }

    protected function getRedisRootNode()
    {
        return "*.common.redis_mobile_api.host.mcp1";
    }

    /**
     * Creates a partner entry in blocket api redis
     */
    public function createPartner($data)
    {
        $this->redisKey = $this->redisPrefix.$data['KeyName'];

        foreach($this->maps as $k1 => $k2) {
            if (isset($data[$k1])) {
                $this->redisKeys[$k2] = $data[$k1];
            }
        }

        $r = $this->redis->hMSet($this->redisKey, $this->redisKeys);

        if (!$r) {
            Logger::logError(__METHOD__, 'BlocketRedis Error: when try to create partner');
            throw new \Exception('BlocketRedis Error: when try to create partner');
        }

        return true;
    }

    /**
     * Updates partner configuration
     *
     * @todo add proper implementation (Next sprint)
     */
    public function updatePartner($data)
    {
        return true;
    }
}
