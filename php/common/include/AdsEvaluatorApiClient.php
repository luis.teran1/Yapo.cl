<?php

require_once("JSON.php");
require_once('bRedisClass.php');
require_once('APIClient.php');

use Yapo\AdsEvaluatorService;

class AdsEvaluatorApiClient implements AdsEvaluatorService
{
    protected $client;
    public $reasonID;
    public $reasonName;
    public $editedReasons;

    private $bconfApi = "*.common.base_url.ms_ads_evaluator_api";
    private $bconfMethods = "*.ms_ads_evaluator_api_client.methods.";

    public function __construct()
    {
        $this->client = new APIClient($this->bconfApi, $this->bconfMethods, true);
    }

    public function insertReasons()
    {
        try {
            $newData = array(
                "reasonID" => $this->reasonID,
                "reasonName" => utf8_encode($this->reasonName)
            );
            $formattedData = array("reason" => $newData);
            $resp = $this->client->call('insert', $formattedData);
            $result = json_decode($resp);
            if (isset($result->status) && $result->status == "OK") {
                return array(true, "OK");
            }
            bLogger::logError(__METHOD__, "ERROR MS ADS EVALUATOR: insert reasons response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS ADS EVALUATOR Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "ERROR MS ADS EVALUATOR: DATA NOT CREATED");
    }

    public function  getReasons()
    {
        try {
            $resp = $this->client->call('get', null);
            $result = json_decode($resp, true);
            // decode the UTF-8 response fields to ISO-8859-1
            array_walk_recursive($result, function (&$item, $key) {
                $item = utf8_decode($item);
            });
            if (isset($result['reason'])) {
                return array(true, $result);
            }
            bLogger::logError(__METHOD__, "ERROR MS ADS EVALUATOR: get reasons response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS ADS EVALUATOR Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "ERROR MS ADS EVALUATOR: DATA NOT FOUND");
    }

    public function editReasons()
    {
        try {
            $createData = array("reason" => $this->editedReasons);
            $jsonData = json_encode($createData);
            $resp = $this->client->call('edit', $jsonData);
            $result = json_decode($resp);
            if (isset($result->status) && $result->status == "OK") {
                return array(true, "OK");
            }
            bLogger::logError(__METHOD__, "ERROR MS ADS EVALUATOR: change priority response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS ADS EVALUATOR Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "ERROR MS ADS EVALUATOR: DATA NOT EDITED");
    }

    public function deleteReasons()
    {
        try {
            $delData = array(
                "reasonID" => $this->reasonID
            );
            $formattedData = array("reason" => $delData);
            $resp = $this->client->call('delete', $formattedData);
            $result = json_decode($resp);
            if (isset($result->status) && $result->status == "OK") {
                return array(true, "OK");
            }
            bLogger::logError(__METHOD__, "ERROR MS ADS EVALUATOR: delete reason response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS ADS EVALUATOR Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "ERROR MS ADS EVALUATOR: DATA NOT DELETED");
    }

}
