<?php
require_once('RedisSessionClient.php');

/**
 * This function return the color for the price alert
 */
function price_alert($ad_appraisal, $price, $category) {
	$appraisal = get_appraisal($ad_appraisal);
	$color = "notfound";

	if ($appraisal < 1) return $color;

	$ref_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.refuse.min");
	$ref_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.refuse.max");
	$war_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.warning.min");
	$war_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.warning.max");
	$ok_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.ok.min");
	$ok_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.ok.max");

	$percent = ($price - $appraisal)*100 / $appraisal;

	if ($percent >= $ref_min && $percent < $ref_max) {
		$color = "refuse";
	}

	else if ($percent >= $war_min && $percent < $war_max) {
		$color = "warning";
	}
	else if ($percent >= $ok_min && $percent < $ok_max) {
		$color = "ok";
	}

	return $color;
}

/**
 * This function look for the appraisal for a brand, model, version, year, fuel, gearbox
 * @return Number the appraisal value
 */
function get_appraisal($ad_appraisal) {

	/* TODO In the future, fix this to get the categories dynamically */
	if ($ad_appraisal["category"] != "2020")
		return 0;

	$category = $ad_appraisal["category"];
	$brand = @$ad_appraisal["brand"];
	$model = @$ad_appraisal["model"];
	$version = @$ad_appraisal["version"];
	$year = @$ad_appraisal["regdate"];
	$fuel = @$ad_appraisal["fuel"];
	$gearbox = @$ad_appraisal["gearbox"];

	$redis_cardata_server_name = bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.name");
	$redis_cardata_server_port = bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.port");
	$redis_options = array(
		'debug' => false,
		'id_separator' => NULL,
		'timeout' => 500,
	);

	/* The 'nice' case */
	if ($brand != "0" && $model != "0" && $year != "") {
		if($version != 0) {
			$redis_attr_key = "rcd1xapp_id_cardata_attr_$brand"."_$model"."_$version";
			$server = array(
				$redis_attr_key => array(
					"name" => $redis_cardata_server_name,
					"port" => $redis_cardata_server_port
				)
			);
			$redis_options["servers"] = $server;
			$redis_cardata = new RedisSessionClient($redis_options);
			$json_data = $redis_cardata->hgetall("$redis_attr_key","$brand");

			$btree = array();
			foreach ($json_data as $key => $val) {
				$btree[$key] = yapo_json_decode($val, true);
			}

			if (is_array($btree)) {
				foreach ($btree as $bt) {
					if ($bt['year'] == $year && $bt['fuel'] == $fuel && $bt['gearbox'] == $gearbox) {
							return $bt['appraisal'];
					}
				}
			}
		}
		else {
			// we need all versions for a model
			$redis_versions_key = "rcd1xapp_id_cardata_versions_$brand"."_$model";
			$server = array(
				$redis_versions_key => array (
					"name" => $redis_cardata_server_name,
					"port" => $redis_cardata_server_port
				)
			);
			$redis_options["servers"] = $server;
			$redis_cardata = new RedisSessionClient($redis_options);
			$redis_versions_data = $redis_cardata->hgetall("$redis_versions_key");

			// get the attrs for each versions
			$json_data = array();
			foreach ($redis_versions_data as $redis_version_id => $v) {
				$redis_attr_key = "rcd1xapp_id_cardata_attr_$brand"."_$model"."_$redis_version_id";

				$server = array(
					$redis_attr_key => array (
						"name" => $redis_cardata_server_name,
						"port" => $redis_cardata_server_port
					)
				);
				$redis_options["servers"] = $server;
				$redis_attrs = new RedisSessionClient($redis_options);
				$attr_data = $redis_attrs->hgetall("$redis_attr_key","$brand");

				// create a "big" array with all attrs
				$json_data = array_merge($json_data, $attr_data);
			}

			$btree = array();
			foreach ($json_data as $key => $val) {
				$btree[$key] = yapo_json_decode($val, true);
			}
			if (is_array($btree)) {
				$sum_appr = 0;
				$i_appr = 0;

				/* look for prices for current year, otherwise the next, or the previous year */
				foreach(array(0, 1, -1) as $delta) {
					foreach ($btree as $bt) {
						if ($bt['year'] == ($year + $delta)) {
							$i_appr++;
							$sum_appr += $bt['appraisal'];
						}
					}
					/* return the average*/
					if ($i_appr > 0) { return ($sum_appr / $i_appr); }
				}
			}
		}
	}

	return 0;
}

