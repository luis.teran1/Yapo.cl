<?php
namespace Yapo;

class DuplicatedAdMobile extends DuplicatedAd
{

    public function check($array_data)
    {
        $is_duplicated = parent::check($array_data);
        if ($is_duplicated['has_duplicates'] == "1") {
            echo(\yapo_json_encode(array(
                'ok' => false,
                'errors' => $is_duplicated
            )));
            return 'FINISH';
        }
        return;
    }
}
