<?php
require_once 'httpful.php';
require_once('JSON.php');

use Yapo\Logger;

class ProxyClient
{
    private $account_session;

    public function __construct()
    {
        $this->account_session = new AccountSession();
    }

    private function checkUserLogged()
    {
        if (!$this->account_session->is_logged()) {
            $login = bconf_get($BCONF, '*.common.base_url.secure')."/login";
            header('Location: ' .$login);
            exit(0);
        }
    }

    private function requestHandler($request_data)
    {
        $method = (isset($request_data['method']) ? $request_data['method'] : 'get');
        $host = (isset($request_data['host']) ? $request_data['host'] : '');
        $uri = (isset($request_data['uri']) ? $request_data['uri'] : '');
        $headers = (isset($request_data['headers']) ? $request_data['headers'] : array());
        $body = (isset($request_data['body']) ? $request_data['body'] : '');

        unset($response);
        $response = \Httpful\Request::$method($host.$uri)
            ->addHeaders($headers)
            ->body($body);
        try {
            $response = $response->send();
        } catch (Exception $e) {
            Logger::logError(__METHOD__, "exception: ".var_export($e, true));
            return (object)array('code' => '500');
        }

        return $response;
    }

    public function doRequest($method, $host, $port, $path, $body = array(), $headers = array())
    {
        $service_request = array(
            "method" => $method,
            "host" => $host.':'.$port,
            "uri" => $path,
            "headers" => $headers,
            "body" => yapo_json_encode($body)
        );
        $response = $this->requestHandler($service_request);
        return $response;
    }
}
