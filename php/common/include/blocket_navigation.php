<?php

class blocket_navigation
{

	function blocket_navigation($offset, $total, $limit, $page_max_count, $offset_uri = null) {
		$this->offset = $offset;
		if ($offset_uri)
			$this->offset_uri = $offset_uri;
		else
			$this->offset_uri = get_request_uri('o');

		$page_count = intval(ceil($total / $limit));
		$start_page_count = (int)max(0, floor($offset/$limit) - $page_max_count/2);
		$end_page_count = (int)min($page_count, $start_page_count + $page_max_count);

 		$this->offset_page = array();
 		$this->offset_page_nr = array();

		for ($i = $start_page_count; $i < $end_page_count; $i++) {
			$this->offset_page[] = $i * $limit;
			$this->offset_page_nr[] = $i + 1;
		}

		// Previous page
		if ($offset - $limit >= 0) {
			$this->offset_page_prev = $offset - $limit;
		}

		// Next and last page link
		if ($offset + $limit < $total) {
			$this->offset_page_next = $offset + $limit;
			$this->offset_page_last = ($page_count - 1) * $limit;
		}
	}

	function display() {
		$simple_data = get_object_vars($this);
		call_template($simple_data, array(), array(), "common/navigation.html", default_template_options());
	}
}
