<?php

interface bsessionclient
{
	function set($key, $val, $expire=0);
	function add($key, $val, $expire=0);
	function delete($key, $nocache=false);
	function get($key, $nocache=false);

	function disconnect();
	function disconnect_all();

	function host_available($host);
	function get_fallback_host();

	function validate_session_id($id);
}

?>
