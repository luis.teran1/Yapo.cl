<?php
spl_autoload_register(function ($class) {

    // project-specific namespace prefix
    $prefix = 'Yapo\\';

    // does the class use the namespace prefix?
    $len = strlen($prefix);

    // get the relative class name
    if (strstr($class, $prefix)) {
        $relative_class = substr($class, $len);
    } else {
        $relative_class = $class;
    }


    // replace the namespace prefix with the base directory, replace namespace
    // separators with directory separators in the relative class name, append
    // with .php
    $file = str_replace('\\', '/', $relative_class) . '.php';

    $dirs = array(
        __DIR__.'/',
        __DIR__.'/lib/',
        __DIR__.'/../apps/'
    );

    foreach ($dirs as $d) {
        if (file_exists($d.$file)) {
            include($d.$file);
            break;
        }
    }
});
