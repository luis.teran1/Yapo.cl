<?php
require_once('init.php');
require_once('bStandardClass.php');
require_once('imagecreatefrombmp.php');
require_once('bImageStream.php');
require_once('JPEG.php');
require_once('EXIF.php');
/*
 * Blocket image creator
 */
class bImage extends bStandardClass {
	// Images
	var $_image;
	var $_image_original;

	// Image properties
	var $_image_original_name;
	var $_image_buffer;
	var $_width;
	var $_height;
	var $_size;
	var $_thumb_digest;

	// Default settings
	var $_quality;
	var $_thumb_quality;
	var $_min_width;
	var $_min_height;
	var $_max_width;
	var $_max_height;
	var $_max_size;
	var $_thumb_width;
	var $_thumb_height;
	var $_watermark;
	var $_watermark_small;
	var $_exif;

	function bImage($image = false) {
		// Default settings

		// This settings has set functions
		$this->_quality = $GLOBALS['BCONF']['*']['common']['image']['quality'];
		$this->_thumb_quality = $GLOBALS['BCONF']['*']['common']['thumbnail']['quality'];
		$this->_max_size = $GLOBALS['BCONF']['*']['common']['image']['maxsize'] * 1024;
		$this->_max_res = $GLOBALS['BCONF']['*']['common']['image']['maxres'] * 1048576;

		// Public function watermark can override watermarks
		$this->_watermark = $_SERVER['DOCUMENT_ROOT']?$_SERVER['DOCUMENT_ROOT'] ."/".$GLOBALS['BCONF']['*']['common']['watermark']['standard']:$GLOBALS['BCONF']['*']['common']['watermark']['standard'];
		$this->_watermark_small = $_SERVER['DOCUMENT_ROOT']."/".$GLOBALS['BCONF']['*']['common']['watermark']['small'];
		$this->_max_width = $GLOBALS['BCONF']['*']['common']['image']['width'];
		$this->_max_height = $GLOBALS['BCONF']['*']['common']['image']['height'];
		$this->_min_width = $GLOBALS['BCONF']['*']['common']['image']['min_width'];
		$this->_min_height = $GLOBALS['BCONF']['*']['common']['image']['min_height'];

		//Just a precaution
		$this->_min_width = $this->_min_width < 1 ? 1 : $this->_min_width;
		$this->_min_height = $this->_min_height < 1 ? 1 : $this->_min_height;

		// Doesn't have a set function
		$this->_thumb_width = $GLOBALS['BCONF']['*']['common']['thumbnail']['width'];
		$this->_thumb_height = $GLOBALS['BCONF']['*']['common']['thumbnail']['height'];
		$this->_thumb_digest = '';

		// Load the image
		if (!($image === false))
			$this->load_image($image);
	}

	function destruct() {
		// Clean all images
		if ($this->_image) @imagedestroy($this->_image);
		if ($this->_image_original) @imagedestroy($this->_image_original);
		unset($this->_image);
	}

	/*
	 * Set quality of image and thumbnail
	 */
	function set_image_quality($quality) {
		if ($quality >= 0 && $quality <= 100)
			$this->_quality = $quality;
	}

	function set_thumb_quality($quality) {
		if ($quality >= 0 && $quality <= 100)
			$this->_thumb_quality = $quality;
	}

	/*
	 * Set max size
	 */
	function set_max_filesize($size) {
		if ($size > 0)
			$this->_max_size = $size * 1024;
	}

	/*
	 * Get image size
	 */
	function get_image_size() {
		return array($this->_size, $this->_width, $this->_height);
	}

	/* Get thumbnail digest */
	function get_thumbnail_digest () {
		return $this->_thumb_digest;
	}

	/*
	 * Check if image is initialized
	 */
	function is_ready() {
		return (!$this->has_error() && !($this->_image === null));
	}

	/*
	 * Clean creator data
	 */
	function clean_creator_data(&$buffer) {
		$pos = strpos($buffer, "CREATOR:") + strlen("CREATOR:");
		$pos2 = strpos($buffer, "\n") - 1;
		while ($pos < $pos2) {
			$buffer{++$pos} = " ";
		}
	}

	/*
	 * Loading image
	 */
	function load_image($image) {
		// Store the filename
		$this->_image_original_name = $image;
		// If file was uploaded get real name
		foreach ($_FILES as $key => $value) {
			if ($image == $_FILES[$key]["tmp_name"]) {
				$this->_image_original_name = $_FILES[$key]["name"];
				break;
			}
		}

		// Check if image exists
		if (!is_file($image)) {
			syslog(LOG_ERR, log_string()."Can't load image ({$this->_image_original_name})");
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		// Get image information
		list($width, $height, $type, $attr) = @getimagesize($image);

		// Check if image isn't bigger than the allowed resolution
		if ($width * $height > $this->_max_res) {
			return $this->_error('ERROR_IMAGE_TOO_BIG');
		}

		// Check that image isn't smaller than the mininum size
		if ($width && $width < $this->_min_width) {
			return $this->_error('ERROR_IMAGE_TOO_SMALL');
		}
		if ($height && $height < $this->_min_height) {
			return $this->_error('ERROR_IMAGE_TOO_SMALL');
		}

		// Load image into GD format
		switch ($type) {
			case IMAGETYPE_GIF:
				$this->_image = @imagecreatefromgif($image);
				$truecolor = imagecreatetruecolor(imagesx($this->_image), imagesy($this->_image));
				imagecopy($truecolor, $this->_image, 0, 0, 0, 0, imagesx($this->_image), imagesy($this->_image));
				imagedestroy($this->_image);
				$this->_image = $truecolor;
				break;

			case IMAGETYPE_JPEG:
				autoRotateImage($image);
				$ImagickHelper = new Imagick($image);
				$properties = $ImagickHelper->getImageProperties();
				$this->_image = @imagecreatefromjpeg($image);
				$this->_exif = $properties;
				if (isset($properties['exif:Orientation']))
					$this->_exif_orient = $properties['exif:Orientation'];
				if (isset($properties['unknown'])) $thumb_digest = $properties['unknown'];
				if (isset($thumb_digest) && strlen($thumb_digest) == 32 && ctype_xdigit($thumb_digest)){
					syslog(LOG_INFO, log_string()."EXIF: digest found: " . $thumb_digest);
					$this->_thumb_digest = $thumb_digest;
				}

				break;

			case IMAGETYPE_PNG:
				/*
				FT 463: ad detail> picture shown is diferent from the selected miniature.

				With some PNG images with alpha channel, it is not show properly.

				The problem appears when the image size is the same like the "resized size".
				The images will not be resized, and it's copied directly to jpeg buffer.

				The solution is to duplicate the image adding an white background before coping.
				*/
				$png_img = @imagecreatefrompng($image);
				$new_png_img = @imagecreatetruecolor($width, $height);

				$white = imagecolorallocate ($new_png_img, 255, 255, 255);
				imagefill($new_png_img, 0, 0, $white);
				imagecopyresampled($new_png_img, $png_img, 0, 0, 0, 0, $width, $height, $width, $height);

				$this->_image = $new_png_img;
				imagedestroy($png_img);
				break;

			case IMAGETYPE_BMP:
				$this->_image = @imagecreatefrombmp($image);
				break;

			default:
				syslog(LOG_INFO, log_string()."Not supported image ({$this->_image_original_name}) with wrong filetype");
				return $this->_error('ERROR_IMAGE_TYPE');
		}

		// Check if image was created
		if (!$this->_image) {
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		// Keep a link to the original image
		$this->_image_original = $this->_image;

		// Set image size
		if(isset($this->_exif_orient)){
			$this->_width = $this->_original_width = @imagesx($this->_image);
			$this->_height = $this->_original_height = @imagesy($this->_image);
		}else{
			$this->_width = $this->_original_width = $width;
			$this->_height = $this->_original_height = $height;
		}

		return true;
	}

	/*
	 * Load an image from image server.
	 */
	function load_from_server($id) {
		global $BCONF;

		$data = http_readfile(substr($id, 0, 2) . "/$id.jpg", bconf_get($BCONF, "*.common.imagedir"), true);
		if (!$data)
			return false;

		$tmpdir = ini_get('upload_tmp_dir');
		if (!$tmpdir)
			$tmpdir = '/tmp';
		$file = tempnam($tmpdir, "image_load_server");
		if (!$file)
			return false;

		file_put_contents($file, $data);
		$res = $this->load_image($file);
		unlink($file);

		return $res;
	}

	/*
	 * Resize image
	 */
	// help function
	function _resize_image($new_width, $new_height) {
		// skip resizing to same size
		if ($this->_width == $new_width && $this->_height == $new_height) {
			return true;
		}

		// Create new image
		if (!($new_image = @imagecreatetruecolor($new_width, $new_height))) {
			return false;
		}

		// Background set to white 
		$white = imagecolorallocate ($new_image, 255, 255, 255);
		imagefill($new_image, 0, 0, $white);

		// Resize the image
		if (!(@imagecopyresampled($new_image, $this->_image_original, 0, 0, 0, 0, $new_width, $new_height, $this->_original_width, $this->_original_height))) {
			return false;
		}

		// Destroy old image
		if ($this->_image != $this->_image_original) @imagedestroy($this->_image);
		// Set the new image
		$this->_image = $new_image;
		// Set the new sizes
		$this->_width = $new_width;
		$this->_height = $new_height;

		return true;
	}

	// Get the new image size and hold aspect ratio
	function _calculate_aspect_ratio($width, $height, $max_width, $max_height) {
		// Fit width
		if ($width > $max_width) {
			$factor = $width / $max_width;
			$width /= $factor;
			$height /= $factor;
		}

		// Fit height
		if ($height > $max_height) {
			$factor = $height / $max_height;
			$width /= $factor;
			$height /= $factor;
		}

		// No floats
		$width = floor($width);
		$height = floor($height);


		return array($width, $height);
	}

	// Resize image to fit max width and height
 	function resize($max_width = false, $max_height = false) {
		// Don't resize an image if it isn't loaded
		if (!$this->is_ready()) {
			syslog(LOG_ERR, log_string()."Image ({$this->_image_original_name}) is not initialized");
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		// If width and height are false, load from BCONF
		if ($max_width === false) $max_width = $this->_max_width;
		if ($max_height === false) $max_height = $this->_max_height;

		// Get aspect ratio width and height
		list($new_width, $new_height) = $this->_calculate_aspect_ratio($this->_width, $this->_height, $max_width, $max_height);

		// Resize image
		if (!$this->_resize_image($new_width, $new_height)) {
			syslog(LOG_ERR, log_string()."Error resizing image ({$this->_image_original_name}) to new size {$new_width}x{$new_height}");
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		// Check if file size is ok
		if ($this->_max_size > 0) {
			// Create image buffer
			$this->_image_to_buffer();

			// Check if we have max size
			if ($this->_size > $this->_max_size) {
				// Image is still to big, trying to estimate a smaller width and height
				$factor = 1.05 * sqrt($this->_size / $this->_max_size);
				$new_width = floor($this->_width / $factor);
				$new_height = floor($this->_height / $factor);
				// Resize the image again
				if (!$this->_resize_image($new_width, $new_height)) {
					syslog(LOG_ERR, log_string()."Error resizing image ({$this->_image_original_name}) to new size {$new_width}x{$new_height}");
					return $this->_error('ERROR_IMAGE_LOAD');
				}
				// Write image to buffer
				$this->_image_to_buffer();

				// Check size of buffer
				if ($this->_size > $this->_max_size) {
					syslog(LOG_ERR, log_string()."Image couldn't be resized to fit size");
					return $this->_error('ERROR_IMAGE_RESIZE');
				}
			}
		}

//		syslog(LOG_INFO, log_string()."Image resize success, name:'{$this->_image_original_name}', size:'{$new_width}x{$new_height}'");

		return true;
	}

	/*
	 * Watermark
	 */
	function watermark($image = false, $centered = false, $partially_centered = true) {
		syslog(LOG_ERR, log_string()."Watermark on PHP has been deprecated. If you are calling this, you'll get nothing");
		return;

		// Don't watermark an image if it isn't loaded
		if (!$this->is_ready()) {
			syslog(LOG_ERR, log_string()."Image ({$this->_image_original_name}) is not initialized");
			return false;
		}

		// If no watermark override, use default watermark
		if ($image === false) {
			if ($this->_watermark_small && ($this->_width < 250 || $this->_height < 250)) {
				$image = $this->_watermark_small;
			} else {
				$image = $this->_watermark;
			}
		}

		// Check if watermark exists
		if (!is_file($image)) {
			syslog(LOG_CRIT, log_string()."Can't find watermark ($image)");
			return false;
		}

		// Get image information
		list($width, $height, $type, $attr) = getimagesize($image);

		// Only allow PNG watermarks
		if ($type != IMAGETYPE_PNG) {
			syslog(LOG_CRIT,
			       log_string().
			       "Watermark image $image needs to be PNG format");
			return false;
		}

		// Load watermark
		if (!($watermark = @imagecreatefrompng($image))) {
			syslog(LOG_CRIT, log_string()."Can't read watermark ($image)");
			return false;
		}

		// Width and height of watermark
		$width = imagesx($watermark);
		$height = imagesy($watermark);

		// Watermark offset
		$offset = 10;
		if($partially_centered)
			$offset = 0;

		// Check if image is larger than watermark
		if ($this->_width < $width * 2 || $this->_height < $height * 2)
			$offset = 0;

		/* Center watermark to image */
		if($centered) {
			$dest_x = ($this->_width / 2) - ($width / 2);
			$dest_y = ($this->_height / 2) - ($height / 2);
		}
		else if($partially_centered) {
			// FT 373: Leave watermark halfway the center and a corner. This is based on the 'else' block below
			switch (rand(0, 3)) {
				case 0: // Lower right corner
					$dest_x = ($this->_width * 3 / 4) - ($width / 2) - $offset;
					$dest_y = ($this->_height * 3 / 4) - ($height / 2) - $offset;
					break;
				case 1: // Upper right corner
					$dest_x = ($this->_width * 3 / 4) - ($width / 2) - $offset;
					$dest_y = ($this->_height / 4) - ($height / 2) + $offset;
					break;
				case 2: // Lower left corner
					$dest_x = ($this->_width / 4) - ($width / 2) + $offset;
					$dest_y = ($this->_height * 3 / 4) - ($height / 2) - $offset;
					break;
				case 3: // Upper left corner
					$dest_x = ($this->_width / 4) - ($width / 2) + $offset;
					$dest_y = ($this->_height / 4) - ($height / 2) + $offset;
					break;
			}
		}
		else {
			// Choose position of watermark
			switch (rand(0, 3)) {
				case 0: // Lower right corner
					$dest_x = $this->_width - $width - $offset;
					$dest_y = $this->_height - $height - $offset;
					break;
				case 1: // Upper right corner
					$dest_x = $this->_width - $width - $offset;
					$dest_y = $offset;
					break;
				case 2: // Lower left corner
					$dest_x = $offset;
					$dest_y = $this->_height - $height - $offset;
					break;
				case 3: // Upper left corner
					$dest_x = $offset;
					$dest_y = $offset;
					break;
			}
		}

		// Watermark the image
		imagecopy($this->_image, $watermark, $dest_x, $dest_y, 0, 0, $width, $height);

		@imagedestroy($watermark);

//		syslog(LOG_INFO, log_string()."Image watermark success, name:'{$this->_image_original_name}'");

		return true;
	}

	/*
	 * Image generators
	 */
	// Generates a jpeg buffer of the image and stores the size
	function _image_to_buffer() {
		ob_start();
		@imagejpeg($this->_image, '', $this->_quality);
		$this->_size = ob_get_length();
		$this->_image_buffer = ob_get_contents();
		ob_end_clean();

		$this->clean_creator_data($this->_image_buffer);

		return $this->_image_buffer;
	}

	// Generate the image and store on disc
	function _image_to_file($name) {
		return imagejpeg($this->_image, $name, $this->_quality);
	}

	// Create image
	function create_image($name = "") {
		// Don't create image if an image if it isn't loaded
		if (!$this->is_ready()) {
			syslog(LOG_ERR, log_string()."Image ({$this->_image_original_name}) is not initialized");
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		// Write image to file
		if ($name == "") {
			return $this->_image_to_buffer();
		} else if (!$this->_image_to_file($name)) {
			syslog(LOG_WARNING, log_string()."Could not store image ({$this->_image_original_name}) as ($name)");
			return $this->_error('ERROR_IMAGE_LOAD');
		}

		return true;
	}

	function resize_crop($new_width, $new_height) {
		// Don't create thumbnail if an image if it isn't loaded
		if (!$this->is_ready()) {
			syslog(LOG_ERR, log_string()."Image ({$this->_image_original_name}) is not initialized");
			return false;
		}

		// Calc new size
		$orig_factor = $this->_original_width / $this->_original_height;
		$new_factor = $new_width / $new_height;

		if ($orig_factor < $new_factor) {
			$width = $this->_original_width;
			// Clip height
			$height = ($this->_original_width / $new_width) * $new_height;
		} else if ($orig_factor > $new_factor) {
			$height = $this->_original_height;
			// Clip width
			$width = ($this->_original_height / $new_height) * $new_width;
		} else {
			$width = $this->_original_width;
			$height = $this->_original_height;
		}

		// Centering
		$cutoff_x = ($this->_original_width - $width) / 2;
		$cutoff_y = ($this->_original_height - $height ) / 2;
		
		// Create new image for holding the thumbnail
		if (!($thumbnail = imagecreatetruecolor($new_width, $new_height))) {
			syslog(LOG_CRIT, log_string()."Error creating holder for cropped thumbnail ({$this->_image_original_name}) with new size {$new_width}x{$new_height}");
			return false;
		}

		// Background set to white 
		$white = imagecolorallocate ($thumbnail, 255, 255, 255);
		imagefill($thumbnail, 0, 0, $white);
		// Resize the image
		if (!(@imagecopyresampled($thumbnail, $this->_image, 0, 0, $cutoff_x, $cutoff_y, $new_width, $new_height, $width, $height))) {
			@imagedestroy($thumbnail);
			syslog(LOG_CRIT, log_string()."Error creating cropped thumbnail ({$this->_image_original_name}) with new size {$new_width}x{$new_height}");
			return false;
		}
		
		// Sharpen image
		if (function_exists("imagefilter")) imagefilter($thumbnail, IMG_FILTER_SMOOTH, -50);
		
		@imagedestroy($this->_image);
		$this->_image = $thumbnail;
		// Set the new sizes
		$this->_width = $new_width;
		$this->_height = $new_height;

		return true;
	}

	// Create thumbnail
	function create_thumbnail($name = "") {
		// Don't create thumbnail if an image if it isn't loaded
		if (!$this->is_ready()) {	
			syslog(LOG_ERR, log_string()."Image ({$this->_image_original_name}) is not initialized");
			return false;
		}
		
		// Get aspect ratio size
		list($new_width, $new_height) = $this->_calculate_aspect_ratio($this->_original_width, $this->_original_height, $this->_thumb_width, $this->_thumb_height);

		// Create new image for holding the thumbnail
		if (!($thumbnail = imagecreatetruecolor($new_width, $new_height))) {
			syslog(LOG_CRIT, log_string()."Error creating holder for thumbnail ({$this->_image_original_name}) with new size {$new_width}x{$new_height}");
			return false;
		}

		// Background set to white (transparent gifs get black background otherwise)
		$white = imagecolorallocate ($thumbnail, 255, 255, 255);
		imagefill($thumbnail, 0, 0, $white);
		
		// Resize image to thumbnail
		if (!(imagecopyresampled($thumbnail, $this->_image_original, 0, 0, 0, 0, $new_width, $new_height, $this->_original_width, $this->_original_height))) {
			@imagedestroy($thumbnail);
			syslog(LOG_CRIT, log_string()."Error creating thumbnail ({$this->_image_original_name}) with new size {$new_width}x{$new_height}");
			return false;
		}
		
		// Sharpen image
		if (function_exists("imagefilter")) imagefilter($thumbnail, IMG_FILTER_SMOOTH, -50);
		
		// Save image
		ob_start();
		if (!imagejpeg($thumbnail, $name, $this->_thumb_quality)) {
			@imagedestroy($thumbnail);
			ob_end_clean();
			syslog(LOG_CRIT, log_string()."Error storing thumbnail ({$this->_image_original_name}) as ($name) with new size {$new_width}x{$new_height}");
			return false;
		}
		
		$thumbnail_buffer = ob_get_contents();
		ob_end_clean();
		@imagedestroy($thumbnail);

		if ($name == "") {
			$this->clean_creator_data($thumbnail_buffer);
			if (!$this->_thumb_digest )
				$this->_thumb_digest = md5($thumbnail_buffer);
			return $thumbnail_buffer;
		}
                
		if (!$this->_thumb_digest )
			$this->_thumb_digest = md5($thumbnail_buffer);
	
		return true;
	}
}

/*
 * Utility functions.
 */

function autoRotateImage($imagePath) { 
	syslog(LOG_DEBUG, log_string()."image path = $imagePath");
	$image = new Imagick($imagePath);
	$orientation = $image->getImageOrientation(); 
	switch($orientation) { 
		case imagick::ORIENTATION_BOTTOMRIGHT: 
			$image->rotateimage("#000", 180); // rotate 180 degrees 
			break; 
		case imagick::ORIENTATION_RIGHTTOP: 
			$image->rotateimage("#000", 90); // rotate 90 degrees CW 
			break; 
		case imagick::ORIENTATION_LEFTBOTTOM: 
			$image->rotateimage("#000", -90); // rotate 90 degrees CCW 
			break; 
	} 
	$image->setImageOrientation(imagick::ORIENTATION_TOPLEFT); 
	$image->writeImage($imagePath);
} 

?>
