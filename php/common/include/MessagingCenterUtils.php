<?php

class MessagingCenterUtils
{

    public function getSignatureHeaders($request)
    {
        $current_date = date('Ymd\THis\Z');
        $current_day = explode('T', $current_date);
        $current_day = $current_day[0];

        $method = (isset($request['method']) ? $request['method'] : '');
        $host = (isset($request['host']) ? $request['host'] : '');
        $uri = (isset($request['uri']) ? $request['uri'] : '');
        $body = (isset($request['body']) ? $request['body'] : '');

        $canonical_uri = '';
        $query_string = '';

        $parts = parse_url($uri);

        if (isset($parts['path']) && !empty($parts['path'])) {
            $canonical_uri = $parts['path'];
        }
        if (isset($parts['query'])) {
            $query_string = $parts['query'];
        }

        $signed_headers =
            'content-type;'
            .'host;'
            .'x-scm-date';

        $canonical_headers = ""
            ."content-type:application/x-www-form-urlencoded\n"
            ."host:$host\n"
            ."x-scm-date:$current_date";

        $canonical_request =
            "$method\n"
            ."$canonical_uri\n"
            ."$query_string\n"
            ."$canonical_headers\n"
            ."$signed_headers\n"
            .hash('sha256', $body, false);

        $hash_canonical_request = hash('sha256', $canonical_request);
        $algorithm = 'SCM-HMAC-SHA256';
        $credential_scope = "$current_day/messaging/scm_request";

        $string_to_sign = ""
            ."$algorithm\n"
            ."$current_date\n"
            ."$credential_scope\n"
            ."$hash_canonical_request";

        $secret = bconf_get($BCONF, '*.messaging_center.secret');
        $date = hash_hmac('sha256', $current_date, "SCM$secret", true);
        $service = hash_hmac('sha256', 'messaging', $date, true);
        $derived_signing_key = hash_hmac('sha256', 'scm_request', $service, true);
        $signature = hash_hmac('sha256', $string_to_sign, $derived_signing_key);
        $key = bconf_get($BCONF, '*.messaging_center.key');
        $authorization_key = "SCM-HMAC-SHA256 Credential={$key}/{$current_day}/messaging/scm_request";
        $authorization_key .= ", SignedHeaders={$signed_headers}, Signature={$signature}";

        $headers = array(
            'Content-type' => 'application/x-www-form-urlencoded',
            'Authorization' => $authorization_key,
            'x-scm-date' => $current_date
        );
        return $headers;
    }
}
