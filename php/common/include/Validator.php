<?php

class Validator
{
	public static function cpf($cpf)
	{
		// Verifica se um n�mero foi informado
		if(empty($cpf)) {
			return false;
		}

		// Elimina possivel mascara
		$cpf = preg_replace('/[^0-9]/', '', $cpf);
		$cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

		// Verifica se o numero de digitos informados � igual a 11 
		if (strlen($cpf) != 11) {
			return false;
		}

		// Verifica se nenhuma das sequ�ncias invalidas abaixo 
		// foi digitada. Caso afirmativo, retorna falso
		else if ($cpf == '00000000000' || 
				$cpf == '11111111111' || 
				$cpf == '22222222222' || 
				$cpf == '33333333333' || 
				$cpf == '44444444444' || 
				$cpf == '55555555555' || 
				$cpf == '66666666666' || 
				$cpf == '77777777777' || 
				$cpf == '88888888888' || 
				$cpf == '99999999999') {
			return false;
			// Calcula os digitos verificadores para verificar se o
			// CPF � v�lido
		} else {   

			for ($t = 9; $t < 11; $t++) {

				for ($d = 0, $c = 0; $c < $t; $c++) {
					$d += $cpf{$c} * (($t + 1) - $c);
				}
				$d = ((10 * $d) % 11) % 10;
				if ($cpf{$c} != $d) {
					return false;
				}
			}

			return true;
		}
	}

	public static function cnpj($cnpj)
	{
		// Verifica se um n�mero foi informado
		if(empty($cnpj)) {
			return false;
		}

		// Elimina possivel mascara
		$cnpj = preg_replace('/[^0-9]/', '', $cnpj);
		$cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);

		// Verifica se o numero de digitos informados � igual a 14 
		if (strlen($cnpj) != 14) {
			return false;
		}

		//Etapa 1: Cria um array com apenas os digitos num�ricos, isso permite receber o cnpj em diferentes formatos como "00.000.000/0000-00", "00000000000000", "00 000 000 0000 00" etc...
		$j=0;
		for($i=0; $i<(strlen($cnpj)); $i++)
		{
			if(is_numeric($cnpj[$i]))
			{
				$num[$j]=$cnpj[$i];
				$j++;
			}
		}

		//Etapa 2: Conta os d�gitos, um Cnpj v�lido possui 14 d�gitos num�ricos.
		if(count($num)!=14)
		{
			$isCnpjValid=false;
		}

		//Etapa 3: O n�mero 00000000000 embora n�o seja um cnpj real resultaria um cnpj v�lido ap�s o calculo dos d�gitos verificares e por isso precisa ser filtradas nesta etapa.
		if ($num[0]==0 && $num[1]==0 && $num[2]==0 && $num[3]==0 && $num[4]==0 && $num[5]==0 && $num[6]==0 && $num[7]==0 && $num[8]==0 && $num[9]==0 && $num[10]==0 && $num[11]==0)
		{
			$isCnpjValid=false;
		}

		//Etapa 4: Calcula e compara o primeiro d�gito verificador.
		else
		{
			$j=5;
			for($i=0; $i<4; $i++)
			{
				$multiplica[$i]=$num[$i]*$j;
				$j--;
			}
			$soma = array_sum($multiplica);
			$j=9;
			for($i=4; $i<12; $i++)
			{
				$multiplica[$i]=$num[$i]*$j;
				$j--;
			}
			$soma = array_sum($multiplica);	
			$resto = $soma%11;			
			if($resto<2)
			{
				$dg=0;
			}
			else
			{
				$dg=11-$resto;
			}
			if($dg!=$num[12])
			{
				$isCnpjValid=false;
			} 
		}

		//Etapa 5: Calcula e compara o segundo d�gito verificador.
		if(!isset($isCnpjValid))
		{
			$j=6;
			for($i=0; $i<5; $i++)
			{
				$multiplica[$i]=$num[$i]*$j;
				$j--;
			}
			$soma = array_sum($multiplica);
			$j=9;
			for($i=5; $i<13; $i++)
			{
				$multiplica[$i]=$num[$i]*$j;
				$j--;
			}
			$soma = array_sum($multiplica);	
			$resto = $soma%11;			
			if($resto<2)
			{
				$dg=0;
			}
			else
			{
				$dg=11-$resto;
			}
			if($dg!=$num[13])
			{
				$isCnpjValid=false;
			}
			else
			{
				$isCnpjValid=true;
			}
		}

		//Etapa 6: Retorna o Resultado em um valor booleano.
		return $isCnpjValid;			
	}

	public static function cpfCnpj($cpf_cnpj) {
		if(strlen($cpf_cnpj)==14) {
			return self::cnpj($cpf_cnpj);
		} elseif(strlen($cpf_cnpj)==11) {
			return self::cpf($cpf_cnpj);
		} else {
			return false;
		}
	}

	public static function bconf($key) {
		global $BCONF;
		$conf = bconf_get($BCONF, $key);
		return (!empty($conf));
	}

	public static function ip($ip) {
		return (preg_match("/^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$/", $ip) == 1);
	}

	public static function email($email) {
		$valid = 
			(preg_match("/^.{5,60}$/", $email) == 1)
			&& (preg_match("/^[[:alnum:]_+-]+(\.[[:alnum:]_+-]*)*@([[:alnum:]_+-]+\.)+[[:alpha:]]{2,4}$/", $email) == 1)
			&& (preg_match("/^www/", $email) != 1);
		return $valid;
	}

	public static function monthYear($monthYear) {
		return (preg_match("/^(0[1-9]|1[0-2])\/([[:digit:]]{4})$/", $monthYear) == 1);
	}

	public static function date($date) {
		return (strtotime($date)!==FALSE);
	}

	public static function creditCard($number, $type = null) {

		if(!self::luhn($number))
			return false;

		switch ($type) {
			case 'MASTER':
				return (preg_match("/^5[1-5]\d{14}$/", $number) == 1); // 16 digitos come�ando com 51, 52, 53, 54 ou 55
				break;
			case 'VISA':
				return (preg_match("/^(4\d{12}|4\d{15})$/", $number) == 1); // 13 ou 16 digitos come�ando com 4
				break;
			case 'AMEX':
				return (preg_match("/^3[47]\d{13}$/", $number) == 1); // 15 digitos come�ando com 34 ou 37
				break;
			case 'DINERS':
				return (preg_match("/^3[068]\d{12}$/", $number) == 1); // 14 digitos come�ando com 30, 36 ou 38
				break;
			case 'DISCOVER':
				return (preg_match("/^6011\d{12}$/", $number) == 1); // 16 digitos come�ando com 6011
				break;
			case 'ENROUTE':
				return (preg_match("/^(2014|2149)\d{11}$/", $number) == 1); // 15 digitos come�ando com 2014 ou 2149
				break;
			case 'JCB':
				return (preg_match("/^(3\d{16}|(2131|1800)\d{11})$/", $number) == 1); // 16 digitos come�ando com 3 ou 15 digitos come�ando com 2131 ou 1800
				break;
			default:
				return true;
		}
	}

	private static function luhn($str) {
		if(!is_numeric($str)) return false;
		$len = strlen($str);
		$sum = 0;
		for($i=1;$i<$len+1;$i++) {
			$x = (int)$str{$len-$i}*(2-($i%2));
			$sum+=$x>9?$x-9:$x;
		}
		return(($sum%10)==0);
	}
}

?>
