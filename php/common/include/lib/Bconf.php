<?php
namespace Yapo;

class Bconf
{

    public static function bconfGet(&$array, $conf)
    {
        return self::get($array, $conf);
    }
    /**
     * Return sub array contained on $BCONF array
     *
     * @param array  $array This should be BCONF global
     * @param string $conf  Key to get the tree node
     *
     * @return array node
     */
    public static function get(&$array, $conf)
    {
        if (empty($array)) {
            return null;
        }

        if (empty($conf)) {
            return null;
        }

        $conf = explode('.', $conf);

        foreach ($conf as $key) {
            if (!is_array($array)) {
                $error_msj  = "bconf_get() {$array} is not an array. ";
                $error_msj .= "Might be misconfigured bconf by a premature leaf";
                $error_msj .= "node for {$conf} at {$key}.";
                error_log($error_msj);
            }
            if (is_array($array) && isset($array[$key])) {
                $array = &$array[$key];
            } else {
                return null;
            }
        }

        return $array;
    }

    /**
     * Return key form language
     * @param array  $array This should be BCONF global
     * @param string $conf  Sub Key of language to get the tree node
     *
     * @return array node
     */
    public static function lang(&$array, $key)
    {
        $conf = "*.language.{$key}.es";
        return self::get($array, $conf);
    }

    /**
     * Return some key from given array
     * @param cmdConf array is the node from Bconf
     * @param prod    Product id
     * @param paramType type of param
     */
    public static function getParams(&$cmdConf, $prod, $paramType)
    {
        if (isset($cmdConf[$prod][$paramType])) {
            return $cmdConf[$prod][$paramType];
        }
        Logger::logError(__METHOD__, "Configuration {$prod} and {$paramType} not found in ".var_export($cmdConf, true));
        return array();
    }

    /**
     * Return product conf as object
     * @param conf    array is the node from Bconf
     * @param prod    Product id
     */
    public static function getProductConf(&$conf, $prod)
    {
        $data = self::get($conf, "*.payment.product.{$prod}");
        if (!empty($data)) {
            return (object) $data;
        }
        return false;
    }

    /**
     * Return array of conf filtered
     * @param conf    array is the node from Bconf
     * @param filter  array with filters to search on setting
     */
    public static function getPaymentSettings(&$conf, $filter)
    {
        $key_lookup = function ($s, $k, $d) {
            return $d[$k];
        };

        $set_values = function ($s, $k, $v, $d) {
            $d[$k] = $v;
        };

        get_settings(
            self::get($conf, "*.payment_settings"),
            "product",
            $key_lookup,
            $set_values,
            $filter
        );
        return $filter;
    }

    /**
     * Returns product price by category
     * @param conf    array is the node from Bconf
     * @param product integer is the product id
     * @param category integer is the ad category
     */
    public static function getProductPrice(&$conf, $product = null, $category = null)
    {
        $referenced_product = self::get($conf, "*.payment.product.{$product}.referenced_product");
        if (!empty($referenced_product)) {
            $product = $referenced_product;
        }
        $payment_settings = array('product' => $product, 'category' => $category);
        $payment_settings = self::getPaymentSettings($conf, $payment_settings);

        return $payment_settings['price'];
    }

    /**
     * Returns product name by category (Using settings)
     * @param conf    array is the node from Bconf
     * @param product integer is the product id
     * @param category integer is the ad category
     */
    public static function getProductName(&$conf, $product = null, $category = null)
    {
        $name = self::get($conf, "*.payment.product.{$product}.name");
        $payment_settings = array('product' => $product, 'category' => $category);
        $payment_settings = self::getPaymentSettings($conf, $payment_settings);
        if (array_key_exists('name', $payment_settings) && !empty($payment_settings['name'])) {
            $name = $payment_settings['name'];
        }
        return $name;
    }
}
