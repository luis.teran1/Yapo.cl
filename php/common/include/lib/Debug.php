<?php

namespace Yapo;

/**
 * Debug is a collection of utility methods that can help on debugging code
 */
class Debug {
    /**
     * Returns the name of the calling function of the invoker
     * @return: String formatted by self::stackFrameToString
     */
    public static function getCallerName() {
        list(,,$caller) = debug_backtrace(false);
        return self::stackFrameToString($caller);
    }
    /**
     * Returns a simplified stack trace string from the caller onwards
     * @return: String of the form: bt -> bt -> ...
     */
    public static function getCallChain() {
        $chain = array_map(array("self", "stackFrameToString"), debug_backtrace(false));
        array_shift($chain);
        return join(array_reverse($chain), ' -> ');
    }
    /**
     * Nicely format a stack frame basic information
     * @return: String of the form: className::functionName
     */
    public static function stackFrameToString($frame) {
        return "{$frame['class']}::{$frame['function']}";
    }
}
