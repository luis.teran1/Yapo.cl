<?php

namespace Yapo\Mutex;

interface NonceGenerator
{
    public function generate();
}
