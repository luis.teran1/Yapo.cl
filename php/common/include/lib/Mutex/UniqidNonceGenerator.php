<?php

namespace Yapo\Mutex;

class UniqidNonceGenerator implements NonceGenerator
{
    public function generate() {
        return uniqid();
    }
}
