<?php

namespace Yapo\Mutex;

/**
 * Implementations must provide mechanisms to safely lock/unlock resources
 * with mutual exclussion guarantees at a distributed level
 */
interface DistributedMutex
{
    /**
     * Tries to acquire the lock.
     * @param resource: Name of the lock to acquire
     * @return: On success, an opaque lock object that must be passed to unlock.
     *          False, otherwise.
     */
    public function lock($resource);

    /**
     * Releases an acquired lock.
     * @param lock: A lock object received from a previous call to lock.
     */
    public function unlock(DistributedMutexLock $lock);
}
