<?php

namespace Yapo\Mutex;

class StaticNonceGenerator implements NonceGenerator
{
    public function __construct($nonce = '1337') {
        $this->nonce = $nonce;
    }
    public function generate() {
        return $this->nonce;
    }
}
