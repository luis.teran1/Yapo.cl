<?php

namespace Yapo\Mutex;

/**
 * Lock object used by SingleRedisMutex.
 * Probably it's standard enough that can be reused.
 */
class SingleRedisMutexLock implements DistributedMutexLock
{
    public function __construct($resource, $token, $requested, $acquired)
    {
        $this->resource = $resource;
        $this->token = $token;
        $this->requested = $requested;
        $this->acquired = $acquired;
    }
    public function getResource()
    {
        return $this->resource;
    }
    public function getToken()
    {
        return $this->token;
    }
    public function getRequestedTime()
    {
        return $this->requested;
    }
    public function getAcquiredTime()
    {
        return $this->acquired;
    }
}
