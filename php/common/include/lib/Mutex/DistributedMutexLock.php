<?php

namespace Yapo\Mutex;

/**
 * Opaque lock object for the DistributedMutex to communicate context
 * between calls. The lock call should return a DistributedMutexLock on
 * success. Such lock should be passed to unlock to release the resource
 */
interface DistributedMutexLock
{
    /**
     * Returns the name of the locked resource
     * @return: String value representing resource name
     */
    public function getResource();
    /**
     * Returns the token used when locking the resource
     * @return: String value with the token
     */
    public function getToken();
    /**
     * Returns what time this lock request begun
     * @return: float time in milliseconds
     */
    public function getRequestedTime();
    /**
     * Returns what time this lock was acquired
     * @return: float time in milliseconds
     */
    public function getAcquiredTime();
}
