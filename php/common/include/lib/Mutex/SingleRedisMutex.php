<?php

namespace Yapo\Mutex;

use Yapo\Bconf;
use Yapo\Debug;
use Yapo\Logger;
use Yapo\RedisInstance;

/**
 * Redis implementation of a mutual exclusion lock. It works when there is only
 * a single redis instance. Better implementations, that support multiple
 * redises are possible. Should multiple redis support become a requirement,
 * please consider the docs available here https://redis.io/topics/distlock
 *
 * Dependency injection:
 * - controlpanel.Mutex.SingleRedisMutex.Redis
 *
 * Bconf:
 * - controlpanel.Mutex.SingleRedisMutex.redisConf
 * - controlpanel.Mutex.SingleRedisMutex.retryCount
 * - controlpanel.Mutex.SingleRedisMutex.retryDelay
 * - controlpanel.Mutex.SingleRedisMutex.ttl
 */
class SingleRedisMutex implements DistributedMutex
{
    /**
     * Redis connection object
     */
    private $redis;
    /**
     * Nonce generation object
     */
    private $nonce;
    /**
     * Configuration pointer
     */
    private $bconf;
    /**
     * Configuration node for this class
     */
    private $conf;
    /**
     * Number of retries for each operation
     */
    private $retryCount;
    /**
     * Maximum interval to wait between retries
     */
    private $retryDelay;
    /**
     * Class base conf node
     */
    const CONF = "controlpanel.Mutex.SingleRedisMutex";

    public function __construct(array $bconf = null, RedisInstance $redisInstance = null, NonceGenerator $nonce = null)
    {
        $this->bconf = isset($bconf) ? $bconf : $BCONF;
        $this->conf = Bconf::get($this->bconf, self::CONF);
        $this->retryCount = Bconf::get($this->conf, "retryCount"); // -1
        $this->retryDelay = Bconf::get($this->conf, "retryDelay"); // 200
        $this->ttl        = Bconf::get($this->conf, "ttl");        // 30000
        $NonceGenerator   = Bconf::get($this->conf, "NonceGenerator"); // UniqidNonceGenerator
        $this->nonce = isset($nonce) ? $nonce : new $NonceGenerator();
        $this->redis = $redisInstance;
    }
    /**
     * Lock a resource. The algorithm with give up after a configured number
     * of retries. Between each retry, a random delay would be introduced
     * between (retryDelay / 2, retryDelay) milliseconds.
     * @param resource: Resource name to use as key on Redis
     * @return: A DistributedLock object on succes. Boolean false on failure
     */
    public function lock($resource)
    {
        $token = $this->nonce->generate();
        $retry = $this->retryCount;
        $ttl   = $this->ttl;
        do {
            $startTime = microtime(true) * 1000;
            if (self::lockInstance($this->getRedis(), $resource, $token, $ttl)) {
                $acquiredTime = microtime(true) * 1000;
                $delta = $acquiredTime - $startTime;
                $validityTime = $ttl - $delta;

                Logger::logInfo(__METHOD__,  "Acquired lock [{$resource}] after {$delta}ms, {$validityTime}ms left for use");
                return new SingleRedisMutexLock($resource, $token, $startTime, $acquiredTime);
            }
            $delay = mt_rand(floor($this->retryDelay / 2), $this->retryDelay);
            usleep($delay * 1000);
            $retry--;
            Logger::logInfo(__METHOD__,  "Retrying lock acquisition for [{$resource}] (retries: {$retry})");
        } while ($retry != 0);
        Logger::logWarning(Debug::getCallChain(), "Retries ({$this->retryCount}) expired for [{$resource}], couldn't acquire lock");
        return false;
    }
    /**
     * Unlock a resource. The provided lock contains the information required
     * to identify and release the lock.
     * @param lock: A lock returned by the lock function
     */
    public function unlock(DistributedMutexLock $lock)
    {
        $resource = $lock->getResource();
        $token    = $lock->getToken();
        $result   = self::unlockInstance($this->getRedis(), $resource, $token);

        $releasedTime = microtime(true) * 1000;
        $deltaStart = $releasedTime - $lock->getRequestedTime();
        $deltaUsed = $releasedTime - $lock->getAcquiredTime();
        $validityTime = $this->ttl - $deltaStart;

        Logger::logInfo(__METHOD__,  "Released lock [{$resource}] after {$deltaUsed}ms, {$validityTime}ms left for use");
        return $result;
    }
    /**
     * Retrieve the redis object to use. It will initialise a new one if
     * required.
     * @return: A ready to use redis object
     */
    private function getRedis()
    {
        if (!isset($this->redis)) {
            $Redis = Bconf::get($this->conf, "Redis"); // EasyRedis
            $redisConf = Bconf::get($this->conf, "redisConf"); // redis_accounts.cp_locks
            $this->redis = new $Redis($redisConf);
        }
        return $this->redis;
    }
    /**
     * Issue a lock command via the redis object to the requested resource.
     * @param redis: Redis object to use
     * @param resource: Key to use as lock
     * @param token: Single use token to guarantee who the lock owner is
     * @param ttl: Expire ttl for the key in milliseconds
     * @return: 1 on success, 0 otherwise
     */
    private static function lockInstance($redis, $resource, $token, $ttl)
    {
        return $redis->set($resource, $token, array('NX', 'PX' => intval($ttl)));
    }
    /**
     * Call the unlock lua script via the given redis object.
     * @param redis: Redis object to use
     * @param resource: Key to use as lock
     * @param token: Single use token to guarantee the releaser owns the lock
     */
    private static function unlockInstance($redis, $resource, $token)
    {
        $script =
            'if redis.call("GET", KEYS[1]) == ARGV[1] then
                return redis.call("DEL", KEYS[1])
            else
                return 0
            end';
        return $redis->eval($script, array($resource, $token), 1);
    }
}
