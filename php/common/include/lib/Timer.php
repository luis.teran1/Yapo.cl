<?php
namespace Yapo;

class Timer
{
    private $elapsedMax;

    public function __construct($elapsedMax = null, $logger = null)
    {
        $this->elapsedMax = isset($elapsedMax) ? $elapsedMax : 30;
        $this->logger = $logger;
    }

    public function shutdownInfo($timeStart, $info = null)
    {
        if (!isset($info)) {
            $info = __METHOD__;
        }
        $lastError =  error_get_last();
        if ($lastError != null) {
            $fileName = $lastError['file'];
            if ($this->logger) {
                $fileName = basename($fileName);
            }
            $this->logError(
                $info,
                "Last error on:{$fileName} line:{$lastError['line']} msj: {$lastError['message']}"
            );
        }
        $elapsed = microtime(true) - $timeStart;
        if ($elapsed >= $this->elapsedMax) {
            $executionTime = number_format($elapsed, 3, ',', '.');
            $this->logWarning($info, "Execution time: {$executionTime}s");
        }
    }

    private function logError($info, $msj)
    {
        if ($this->logger) {
            $this->logger->logError($info, $msj);
        } else {
            Logger::logError($info, $msj);
        }
    }

    private function logWarning($info, $msj)
    {
        if ($this->logger) {
            $this->logger->logWarning($info, $msj);
        } else {
            Logger::logWarning($info, $msj);
        }
    }
}
