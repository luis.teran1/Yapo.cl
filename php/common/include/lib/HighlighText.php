<?php

namespace Yapo;

class HighlighText
{
    public $frame_ad;
    public $warning_type;
    
    private $bconf;
    private $text;
    private $all_matches;

    public function __construct($bconf)
    {
        $this->bconf = $bconf;

        $this->all_matches = array();
        $this->frame_ad = false;
        $this->warning_type = '';
    }

    public function getHtml($key, $text, $word_marks)
    {
        $this->all_matches = array();
        $this->text = $text;
        $this->findIt($key, $text, $word_marks);

        foreach ($this->all_matches as $match => $color) {
            $ht = sprintf('<span class="WordBorderSolid" style="border-color: %s">%s</span>', $color, $match);
            $text = str_replace($match, $ht, $text);
        }
        return $text;
    }

    private function findIt($key, $text, $word_marks)
    {

        foreach ($word_marks as $mark) {
            $newtext = $text;
            $oldtext = $text;

            if (isset($mark['where']) && $mark['where'] != '*') {
                $where = explode(',', $mark['where']);
                if (!in_array($key, $where)) {
                    continue;
                }
            }

            if (isset($mark['color'])) {
                $color = $this->bconf['controlpanel']['modules']['adqueue']['highlight']['color'][$mark['color']];
            } else {
                $color = null;
            }

            $span = '<span style="border-color:'. $color .'" class="WordBorderSolid">';

            if (isset($mark['pure_regex']) && $mark['pure_regex'] == 1) {
                $regex = $mark['regex'];
            } else {
                $invalid_chars = array('\\', '.' , '*', '^', '$');
                $valid_chars = array('\\\\', '\.' , '\*', '\^', '\$');
                $regex = str_replace($invalid_chars, $valid_chars, $mark['regex']);
            }
            $regex = "($regex)";

            if (@$mark['word_boundary']) {
                $regex = '\b'.$regex.'(\b|$)';
            }

            /* Only match outside of tags */
            $regex = "/($regex)([^>]*(\\<|\$))/i";
            $res = '';
            // added @ to void PHP warnings if the preg match fails return false and stop the while
            while (@preg_match($regex, $newtext, $regs, PREG_OFFSET_CAPTURE)) {
                $this->all_matches[$regs[1][0]] = $color;
                $pos = $regs[1][1];
                $res .= substr($newtext, 0, $pos) . $span . $regs[1][0] . '</span>';
                $newtext = substr($newtext, $pos + strlen($regs[1][0]));
            }
            $newtext = $res . $newtext;

            if (isset($mark['frame_ad']) && $mark['frame_ad'] == 1 && $oldtext != $newtext) {
                $this->frame_ad = true;
            }

            if (isset($mark['warning_type']) && $oldtext != $newtext) {
                $this->warning_type = $mark['warning_type'];
            }
        }
    }
}
