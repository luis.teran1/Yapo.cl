<?php

/**
 * \interface Yapo\Metrics\MetricsAgent
 * \brief Represents metrics intrumentation agents that may be used to track
 * application performance.
 */

namespace Yapo\Metrics;

interface MetricsAgent
{
    /*
     * setTransactionName defines how this transaction should be registered at
     * by the metrics collector. A common schema for MVC is: controller/action.
     * For the blocket scenario, we'll use ClassName/StateFunctionName
     */
    public function setTransactionName($name);
}
