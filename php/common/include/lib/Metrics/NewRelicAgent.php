<?php

/**
 * \class Yapo\Metrics\NewRelicAgent
 * \brief Uses the New Relic API to instrument performance metrics
 * https://docs.newrelic.com/docs/agents/php-agent/features/php-custom-instrumentation
 */

namespace Yapo\Metrics;

class NewRelicAgent implements MetricsAgent
{
    /*
     * setTransactionName Sets the name of the transaction to the specified name.
     */
    public function setTransactionName($name)
    {
        if (extension_loaded('newrelic')) {
            newrelic_name_transaction($name);
        }
    }
}


