<?php

/**
 * \class Yapo\Metrics\MetricsAgentFactory
 * \brief Provides a factory method to instantiate MetricsAgent objects based
 * on configuration.
 * https://docs.newrelic.com/docs/agents/php-agent/features/php-custom-instrumentation
 */

namespace Yapo\Metrics;

use Yapo\Bconf;
use Yapo\Logger;

class MetricsAgentFactory
{
    const DEFAULT_AGENT = '\Yapo\Metrics\NoAgent';
    public static function getMetricsAgent($bconf, $baseclass)
    {
        $MetricsAgent = Bconf::get($bconf, "*.${baseclass}.MetricsAgent");
        if (!class_exists($MetricsAgent)) {
            Logger::logWarning(__METHOD__, "MetricsAgent [{$MetricsAgent}] not found. Falling back to default");
            $MetricsAgent = self::DEFAULT_AGENT;
        }
        $agent = new $MetricsAgent();
        if (!$agent instanceof MetricsAgent) {
            Logger::logWarning(__METHOD__, "MetricsAgent [{$MetricsAgent}] does not implement MetricsAgent. Falling back to default");
            $MetricsAgent = self::DEFAULT_AGENT;
            $agent = new $default();
        }
        Logger::logWarning(__METHOD__, "MetricsAgent = [{$MetricsAgent}]");
        return $agent;
    }
}
