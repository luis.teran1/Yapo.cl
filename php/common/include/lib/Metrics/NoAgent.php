<?php

/**
 * \class Yapo\Metrics\NoAgent
 * \brief Represents the absence of metrics intrumentation.
 */

namespace Yapo\Metrics;

use Yapo\Logger;

class NoAgent implements MetricsAgent
{
    /*
     * setTransactionName does nothing
     */
    public function setTransactionName($name)
    {
        Logger::logDebug(__METHOD__, "setTransactionName -> {$name}");
    }
}

