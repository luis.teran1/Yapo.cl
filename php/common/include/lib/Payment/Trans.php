<?php

namespace Yapo\Payment;

use bTransaction;

class Trans
{

    public static function getPaymentItems($externalOrderId)
    {
        $transaction = new bTransaction();
        $transaction->add_data('payment_group_id', $externalOrderId);
        $reply = $transaction->send_command('get_items_by_payment');
        if ($transaction->has_error(true)) {
            $trans_errors = $transaction->get_errors();
            Logger::logError(__METHOD__, "trans errors : ".var_export($trans_errors, true));
            return false;
        }
        return $reply["get_items_by_payment"];
    }

    public static function getAutoBumpPrice($listId, $frequency, $numDays, $useNight = 0)
    {
        $transaction = new bTransaction();
        $transaction->add_data('list_id', $listId);
        $transaction->add_data('frequency', $frequency);
        $transaction->add_data('num_days', $numDays);
        $transaction->add_data('use_night', $useNight);

        $reply = $transaction->send_command('price_autobump');
        if ($transaction->has_error(true)) {
            return false;
        }
        return $reply;
    }
}
