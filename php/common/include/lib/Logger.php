<?php

namespace Yapo;

use Yapo\Bconf;

class Logger
{

    static private $levels = array(
        LOG_EMERG => 0,
        LOG_ALERT => 1,
        LOG_CRIT => 2,
        LOG_ERR => 3,
        LOG_WARNING => 4,
        LOG_NOTICE => 5,
        LOG_INFO => 6,
        LOG_DEBUG => 7
    );

    private static function writeLog($fromFunction, $message, $level = LOG_INFO)
    {
        $min_log_level = intval(Bconf::bconfGet($BCONF, "*.php.log_level"));
        if (self::$levels[$level] <= $min_log_level) {
            $message = preg_replace("/\s+/", " ", $message);
            syslog($level, Logger::logString() . ": {$fromFunction}: {$message}");
        }
    }

    public static function logError($fromFunction, $message)
    {
        self::writeLog($fromFunction, $message, LOG_ERR);
    }

    public static function logWarning($fromFunction, $message)
    {
        self::writeLog($fromFunction, $message, LOG_WARNING);
    }

    public static function logNotice($fromFunction, $message)
    {
        self::writeLog($fromFunction, $message, LOG_NOTICE);
    }

    public static function logInfo($fromFunction, $message)
    {
        self::writeLog($fromFunction, $message, LOG_INFO);
    }

    public static function logDebug($fromFunction, $message)
    {
        self::writeLog($fromFunction, $message, LOG_DEBUG);
    }

    public static function logString()
    {
        $log = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
        if (strlen(session_id())) {
            $log .= ' '.session_id();
        } elseif (isset($_COOKIE['s'])) {
            $log .= ' '.$_COOKIE['s'];
        } elseif (isset($_SERVER['REMOTE_PORT'])) {
            $log .= ' ' . $_SERVER['REMOTE_PORT'];
        }

        if (isset($_SESSION['ad_type'])) {
            $log .= " [{$_SESSION['ad_type']}]";
        }
        return $log.' - ';
    }
}
