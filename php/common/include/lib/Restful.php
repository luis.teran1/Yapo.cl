<?php

namespace Yapo;

use ReflectionClass;
use Yapo\Metrics\MetricsAgentFactory;

abstract class Restful
{

    private $resource;
    private $verb;
    protected $metrics_agent;

    abstract public function __construct();

    protected function methodNotAllowed()
    {
        $this->setStatusCode(405);
    }

    protected function resourceNotFound()
    {
        $this->setStatusCode(404);
    }

    protected function serverError()
    {
        $this->setStatusCode(500);
    }

    protected function badRequest()
    {
        $this->setStatusCode(400);
    }
    protected function setStatusCode($code)
    {
        header(' ', true, $code);
    }

    public function run()
    {
        $this->application_name = get_class($this);
        header('Content-type: application/json');

        global $BCONF;
        $data = array();

        $path_info = explode("/", $_SERVER["PATH_INFO"]);
        //get url data
        array_shift($path_info);

        //get request data
        $request_data = file_get_contents('php://input');
        Logger::logDebug(__METHOD__, "Request data: " . var_export($request_data, true));

        $this->resource = array_shift($path_info);
        $this->verb = $_SERVER['REQUEST_METHOD'];
        get_settings(
            Bconf::bconfGet($BCONF, $this->restAPISettingsKey),
            "methods",
            array($this, "keyLookup"),
            array($this, "setValues"),
            $data
        );

        // Track the specific application
        $this->setup_metrics();
        $this->set_transaction_name($data);

        // Pseudo dependency injection
        // (beware class with constructor arguments)
        $args = array();
        if (array_key_exists('inject', $data)) {
            foreach (explode(',', $data['inject']) as $inj) {
                $args[] = new $inj;
            }
        }

        // get the class in charge on handling the resource
        $class = new ReflectionClass($data['class']);
        $instance = $class->newInstanceArgs($args);

        $validationErrors = $this->validateParams($request_data, $data['method']);
        if ($validationErrors != null) {
            Logger::logDebug(__METHOD__, "Result: " . print_r($validationErrors, true));
            echo json_encode($validationErrors);
            $this->bad_request();
            return;
        }

        // call the method of the verb
        $url_data = $this->parseQueryString();
        Logger::logDebug(__METHOD__, "Url data: " . var_export($url_data, true));
        $result = null;
        try {
            $result = $instance->$data["method"]($url_data, json_decode($request_data));
        } catch (Exception $e) {
            $error_msj  = "Calling method: ".$data["method"];
            $error_msj .= " with url_data:" .print_r($url_data, true);
            $error_msj .= " (Check url_data)\n".var_export($e->getMessage(), true);
            Logger::logError(__METHOD__, $error_msj);
        }
        //Return the result as json
        Logger::logDebug(__METHOD__, "Result: " . print_r($result, true));
        // if the result is NULL return an empty json '{}'
        if ($result == null) {
            echo '{}';
        } else {
            echo json_encode($result, true);
        }
    }

    //this function returns true if the params are valid, otherwise return the array with the param errors
    private function validateParams($params_data, $app_method)
    {
        return null;
    }

    private function parseQueryString()
    {

        $data = array();
        $items = explode('&', $_SERVER['QUERY_STRING']);
        // get all the items from query string
        foreach ($items as $item) {
            if (empty($item)) {
                continue;
            }

            // decode the values from url encoding
            list($name, $value) = explode('=', $item);
            $name = urldecode($name);
            $value = urldecode($value);
            // if it has multiple params, put it in an array
            if (array_key_exists($name, $data)) {
                if (is_array($data[$name])) {
                    array_push($data[$name], $value);
                } else {
                    $data[$name] = array($data[$name], $value);
                }
            } else {
                $data[$name] = $value;
            }
        }

        return $data;
    }

    private function setValues($setting, $key, $value, $data)
    {
        $data[$key] = $value;
    }

    private function keyLookup($setting, $key, $data)
    {
        $ret;
        switch ($key) {
            case "resource":
                $ret = $this->resource;
                break;
            case "verb":
                $ret = $this->verb;
                break;
        }
        return $ret;
    }
    protected function setup_metrics()
    {
        $this->metrics_agent = MetricsAgentFactory::getMetricsAgent($BCONF, 'Restful');
    }

    protected function set_transaction_name($data)
    {
        $name = str_replace("\\", "/", $data['class']);
        $name = preg_replace (",^/?Yapo/,", "", $name);
        $txnName = sprintf('/%s/%s', $name, $data['method']);
        $this->metrics_agent->setTransactionName($txnName);
    }
}
