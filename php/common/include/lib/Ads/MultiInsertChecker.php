<?php

namespace Yapo\Ads;

use Yapo\Bconf;
use Yapo\EasyRedis;

// Multichecker used to block multi tabs insertion
class MultiInsertChecker
{

    const REDIS = "redis_session";
    const CONF = "multi_insert_checker";

    private $redis;
    private $config;

    public function __construct($bconf, $redisInstance = null)
    {
        $this->redis = isset($redisInstance) ? $redisInstance : new EasyRedis(self::REDIS);
        $this->config = Bconf::bconfGet($bconf, "*.".self::CONF);
    }

    private function increaseAttempts($key, $ttl)
    {
        $attempts = $this->redis->incr($key);
        if ($attempts == 1) {
            $this->redis->setTimeout($key, $ttl);
        }
        return $attempts;
    }

    private function getParam($category, $param)
    {
        $specific = Bconf::get($this->config, "category.{$category}.{$param}");
        if ($specific != null) {
            return $specific;
        }
        return Bconf::get($this->config, $param);
    }

    private function generateKey($category, $param)
    {
        return self::CONF.":{$category}:{$param}";
    }

    public function allowInsertAttempt($category, $params, $is_pro = false)
    {
        if ($this->config["enabled"] != "1" || $is_pro || empty($category) || empty($params)) {
            return true;
        }
        if (Bconf::get($this->config, "category.{$category}.enabled") == 1) {
            foreach ($params as $one_param) {
                if (!empty($one_param)) {
                    $ttl = $this->getParam($category, "ttl");
                    $attempts = $this->getParam($category, "attempts");
                    $redis_key = $this->generateKey($category, $one_param);
                    $counter = $this->increaseAttempts($redis_key, $ttl);
                    if ($counter > $attempts) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
