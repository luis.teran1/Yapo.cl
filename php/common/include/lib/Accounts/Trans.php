<?php

namespace Yapo\Accounts;

use bTransaction;

class Trans
{

    public static function getAccount($account_id)
    {
        $transaction = new bTransaction();
        $transaction->add_data('account_id', $account_id);
        $reply = $transaction->send_command('get_account');
        if ($transaction->has_error(true)) {
            return false;
        }
        return $reply;
    }
}
