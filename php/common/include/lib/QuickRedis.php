<?php
namespace Yapo;

use \Redis;

class QuickRedis extends Redis
{
    private $prepend;

    public function __construct($bconf)
    {
        global $BCONF;

        //load the user from the Redis
        $key = $this->getRedisRootNode($bconf);
        $redisConf = Bconf::bconfGet($BCONF, $key);

        //connect to redis
        parent::__construct();
        $this->connect($redisConf['name'], $redisConf['port'], $redisConf['timeout']);
        if (isset($redisConf['db'])) {
            $this->select($redisConf['db']);
        }
    }

    public function __destruct()
    {
        if (isset($this->redis)) {
            $this->close();
        }
    }

    protected function getRedisRootNode($key)
    {
        $this->prepend = Bconf::bconfGet($BCONF, "*.{$key}.redis.prepend");
        return   "*.common.{$key}.host.{$this->prepend}";
    }

    public function getPrepend()
    {
        return $this->prepend;
    }
}
