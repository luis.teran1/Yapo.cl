<?php

namespace Yapo\ControlPanel;

/**
 * Implementations must provide a way to store/retrieve authorization tokens.
 * The actual token should be treated as an Opaque string that must be returned
 * back unaltered since the last time it was stored.
 */
interface TokenManager
{
    /**
     * Lock requested key and retrieve its latest token.
     * @param key: Name of the memcached resource that holds the token
     */
    public function getToken($key);
    /**
     * Put a token back and release the lock.
     * @param key: Named key to use as lock
     * @param token: Fresh token to store
     * @param expire: Expiration ttl for the token
     */
    public function putToken($key, $token, $expire);
    /**
     * Notify the manager that the key wouldn't be used anymore by this client.
     * It's intended to be used on error cases when no new token is available.
     * @param key: Named key to unlock
     */
    public function relinquish($key);
}
