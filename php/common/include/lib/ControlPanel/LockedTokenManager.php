<?php

namespace Yapo\ControlPanel;

use Yapo\Bconf;

/**
 * A Token Manager with mutex protection guarantees. It takes any other
 * TokenManager and wraps it with a mutex to ensure unique access to the
 * tokens.
 *
 * Dependency injection:
 * - controlpanel.ControlPanel.LockedTokenManager.TokenManager
 * - controlpanel.ControlPanel.LockedTokenManager.DistributedMutex
 */
class LockedTokenManager implements TokenManager
{
    /**
     * TokenManager to wrap protect with the mutex
     */
    private $manager;
    /**
     * Mutual exclusion lock to proctect token access
     */
    private $mutex;
    /**
     * Associative array of the held locks (key => lock)
     */
    private $locks;
    /**
     * Key prefix to identify locks
     */
    const LOCK_PREFIX = "lock_";
    /**
     * Class base conf node
     */
    const CONF = "controlpanel.ControlPanel.LockedTokenManager";
    /**
     * Construct with a particular DistributedMutex implementation. The default
     * SingleRedisMutex would be used if none is provided.
     * @param mutex: DistributedMutex implementation
     */
    public function __construct(array $bconf = null, TokenManager $manager = null, DistributedMutex $mutex = null)
    {
        $this->bconf = isset($bconf) ? $bconf : $BCONF;
        $this->conf = Bconf::get($this->bconf, self::CONF);
        $TokenManager = Bconf::get($this->conf, "TokenManager"); // BasicTokenManager
        $DistributedMutex = Bconf::get($this->conf, "DistributedMutex"); // SingleRedisMutex

        $this->manager = isset($manager) ? $manager : new $TokenManager();
        $this->mutex = isset($mutex) ? $mutex : new $DistributedMutex();
        $this->locks = array();
    }
    /**
     * Ensure to release all locks held on destruction.
     */
    public function __destruct()
    {
        foreach ($this->locks as $key => $lock) {
            $this->unlock($key);
        }
    }
    /**
     * Lock requested key and retrieve its latest token.
     * @param key: Name of the memcached resource that holds the token
     */
    public function getToken($key)
    {
        $this->lock(self::LOCK_PREFIX . $key);
        return $this->manager->getToken($key);
    }
    /**
     * Put a token back and release the lock.
     * @param key: Named key to use as lock
     * @param token: Fresh token to store
     * @param expire: Expiration ttl for the token
     */
    public function putToken($key, $token, $expire)
    {
        $this->manager->putToken($key, $token, $expire);
        $this->unlock(self::LOCK_PREFIX . $key);
    }
    /**
     * Release a lock without putting a token back. This method is intended
     * to be used on error scenario, in order to always be able to release the lock.
     * @param key: Named key to unlock
     */
    public function relinquish($key)
    {
        $this->unlock(self::LOCK_PREFIX . $key);
    }
    /**
     * Locks the associated $key with class' mutex.
     * @param key: Named key to lock
     */
    private function lock($key)
    {
        $lock = $this->mutex->lock($key);
        $this->locks[$key] = $lock;
    }
    /**
     * Releases the lock associated with $key.
     * @param key: Named key to unlock
     */
    private function unlock($key)
    {
        if (array_key_exists($key, $this->locks)) {
            $lock = $this->locks[$key];
            unset($this->locks[$key]);
            $this->mutex->unlock($lock);
        }
    }
}
