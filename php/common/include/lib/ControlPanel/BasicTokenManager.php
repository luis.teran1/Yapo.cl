<?php

namespace Yapo\ControlPanel;

/**
 * A simple memcached based token manager. It stores and retrieves tokens
 * on a single memcached instance. There is not any concurrent usage
 * guarantees offered by this implementation. For a concurrent implementation,
 * please check LockedTokenManager.
 */
class BasicTokenManager implements TokenManager
{
    /**
     * Retrieve latest token for a key.
     * @param key: Name of the memcached resource that holds the token
     */
    public function getToken($key)
    {
        $client = memcached_client();
        return $client->get($key, true);
    }
    /**
     * Put a fresh token on the shared storage.
     * @param key: Name of the memcached resource that holds the token
     * @param token: Fresh token to store
     * @param expire: Expiration ttl for the token
     */
    public function putToken($key, $token, $expire)
    {
        $client = memcached_client();
        $client->set($key, $token, $expire);
    }
    /**
     * Doesn't do anything.
     * @param key: Named key to unlock
     */
    public function relinquish($key)
    {
    }
}
