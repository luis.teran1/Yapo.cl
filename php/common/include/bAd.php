<?php
require_once('init.php');

class bAd {
	var $region;
	var $area;
	var $hide_address;
	var $image_text;
	var $type;
	var $category;
	var $sub_category;
	var $category_group;
	var $name;
	var $email;
	var $phone_type;
	var $area_code;
	var $phone;
	var $phone_hidden;
	var $company_ad;
	var $store;
	var $subject;
	var $body;
	var $infopage;
	var $infopage_title;
	var $price;
	var $passwd;
	var $coords;
	var $state;
	var $images;
	var $lang;
	var $digests_presents;
	var $thumbnails;
	var $thumbnail_digests;
	var $list_time;
	var $can_edit_ad;
	var $contract_type;
	var $working_day;

	/* XXX still needed? */
	var $old_cat; 	
	var $city;

	function bAd($ca = NULL) {
		$this->reset($ca);
	}

	function reset($ca = NULL) {
		global $BCONF;

		// First reset all variables
		foreach ($this as $key => $value)
                        $this->$key = NULL;

		if (!is_null($ca))
			list($this->region, $this->city) = explode_ca($ca);

		// Default to s 
                $this->type = "s";

		// Default to default language if multilang not enabled
		if (!bconf_get($BCONF, "*.multilang.enabled"))
			$this->lang = bconf_get($BCONF, "*.common.default.lang");
	}

	function is_populated() {
		foreach($this as $key => $value) {
			switch ($key) {
				case 'region': 
				case 'city': 
				case 'type': 
				break;
				default:
					if (isset($this->$key))
						return true;
			}
		}
		return false;
	}
	
	/*
	   flags
	   0 = don't call cleanup
	   1 (BAD_CLEANUP) = call cleanup
	   2 (BAD_UNSET) = do not return these vars (XXX - Ugly like the rest of bAd)
	*/
	function get_data_array($flags = 1) {
		if (($flags & 1) == 1)
			$this->cleanup();

		$obj_cpy = get_object_vars($this);
		if (($flags & 2) == 2) {
			unset($obj_cpy['category_group']);
			unset($obj_cpy['sub_category']);
			unset($obj_cpy['area']);
		}
		
		return $obj_cpy;
	}
	
	// New format from trans_loadad. Populate arr to be compatible with old format
	function loadad_recreate(&$arr) {
		if (is_array($arr['ad']))
			$tmp= $arr['ad'];

		if (is_array(@$arr['params'])) {
			foreach ($arr['params'] as $key => $val) {
				if (preg_match('/^(.*)([0-9]+)$/', $key, $match)) {
					/* Fill up an array of numbered params. */
					$idx = intval($match[2]) - 1;
					if (is_array(@$tmp[$match[1]]))
						$a = $tmp[$match[1]];
					else
						$a = array();
					while (count($a) < $idx)
						$a[] = null;
					$a[$idx] = $val;
					$tmp[$match[1]] = $a;
				} else
					$tmp[$key] = $val;
			}
		}

		if (isset($arr['ad']['type']))
			$tmp['type'] = $GLOBALS['BCONF']['*']['common']['type'][$arr['ad']['type']]['short_name'];
		if (is_array($arr['users']))
			$tmp['email'] = $arr['users']['email'];
		$arr = $tmp;
	}

	function populate($arr, $data_from_db = false, $cleanup_args = NULL) {
		global $BCONF;

		// BLACK LISTED WORD
		if (isset($arr['external_ad_id'])) {
			unset($arr['external_ad_id']);
		}

		if (isset($arr['ad']))
			$this->loadad_recreate($arr);

		/* Dynamically create variables for features. */
		$ad_params = array();
		$checkboxes = array();
		foreach (bconf_get($BCONF, "*.common.ad_params") as $param) {
			if (isset($param['name']))
				$ad_params[$param['name']] = $param;
			if (@$param['default'] && $param['type'] == 'checkbox')
				$checkboxes[] = $param['name'];
		}
		if (isset($arr['category_group'])) {
			/* Reset category */
			unset($this->category);
			unset($this->sub_category);

			/* If no sub category exists default this->category */
			$this->category = intval($arr['category_group']);

			/* Check if exists */
			if (bconf_get($BCONF, "*.cat." . $arr['category_group'])) {
				$this->category_group = intval($arr['category_group']);

				/* Check for sub category */
				$has_children = false;
				foreach (bconf_get($BCONF, '*.cat') as $cat) {
					if (isset($cat['parent']) && $cat['parent'] == $arr['category_group']) {
						$has_children = true;
						break;
					}
				}
				if ($has_children) {
					/* If arr[sub_category] and is correct then set this category */
					if ($arr['sub_category']) {
						$sub_parent = bconf_get($BCONF, '*.cat.' . $arr['sub_category'] . '.parent');
						if ($sub_parent == $arr['category_group']) {
							$this->sub_category = intval($arr['sub_category']);
							$this->category = intval($arr['sub_category']);
						}
					}
				}
			}
		} else if (isset($arr['category'])) {
			$this->category = $arr['category'];

			if ($arr['category'] > 0 && $arr['category'] < 1000) {

				/* US1169 ET7 */
				foreach (bconf_get($BCONF, "*.cat") as $key => $cat) {
					   if (array_key_exists('old', $cat) && array_key_exists('cat', $cat['old'])
					       && intval($arr['category']) == $cat['old']['cat']) {

						   if ($cat['level'] >= 2)
							   $this->category_group = intval($cat['parent']);
						   else
							   $this->category_group = intval($key);

						   $this->sub_category = 0;
						   break;
					   }
				}
			} else {
				$this->category_group = $arr['category'];

				if (bconf_get($BCONF, "*.cat.{$this->category_group}")) {
					if (bconf_get($BCONF, "*.cat.{$this->category_group}.level") == 1) {
						$this->sub_category = 0;
					} else {
						$this->sub_category = $this->category_group;
						$this->category_group = bconf_get($BCONF, "*.cat.{$this->category_group}.parent");
					}

					$arr['category'] = $this->sub_category == 0 ? $this->category_group : $this->sub_category;
				}
			}
		}

		$this->lookup_data = $arr;
		$features = '';

		get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $features);
		unset($this->lookup_data);

		$features = explode(',', $features);
		if ($features) {
			syslog(LOG_DEBUG, "bAd populating features " . implode(', ', $features));
			foreach ($features as $feat) {
				$stripped_feat = explode(':', $feat);
				if (isset($ad_params[$stripped_feat[0]])) {
					if (!isset($this->$stripped_feat[0]))
						$this->$stripped_feat[0] = null;
					if (in_array($ad_params[$stripped_feat[0]]['type'], array('checkbox', 'checklist')))
						$checkboxes[] = $stripped_feat[0];
				}
			}
		}

		foreach ($this as $key => $value) {
			if ($key == 'category')
				continue; /* Handled above. */

			if (isset($arr[$key])) {
				$value = $arr[$key];
			} elseif ($data_from_db && isset($arr["params.$key"])) {
				$value = $arr["params.$key"];
			}
			if ($data_from_db && !is_array($value) &&  (is_error($value) || is_warning($value)))
				$this->$key = null;
			else
				$this->$key = $value;
		}
		if (isset($arr['store_id']))
			$this->store = intval($arr['store_id']);

		// Only update from step 1
		$this->phone_hidden = isset($arr['phone_hidden'])?(int)$arr['phone_hidden']:0;

		if (!isset($arr['passwd']) && !$data_from_db) {
			$this->available_weeks_offseason = $this->create_week_string("week_off", $arr);
			$this->available_weeks_peakseason = $this->create_week_string("week_peak", $arr);
			$this->available_weeks = $this->create_week_string("week", $arr);

			/* Checkboxes are not sent if not checked, so clear any checkboxes not received. */
			foreach ($checkboxes as $checkbox) {
				if (preg_match('/^available_weeks/', $checkbox)) /* Handled above */
					continue;
				if (!isset($arr[$checkbox]))
					$this->$checkbox = null;
			}
		}

		/* If data is from db, we have a body but category wants item descs, fill desc 1 with the body. */
		if ($data_from_db && in_array('item_desc', $features) && empty($this->item_desc) && $this->body) {
			$this->item_desc[0] = $this->body;
			$this->body = null;
		}

		// Converting to UF. In db the price is kept in UF cents		
		if ($data_from_db && isset($this->price) && isset($this->currency) && $this->currency == 'uf') {
			$this->price /= 100;
		}

		if(isset($arr['ad']['list_time'])) {
			$this->list_time = $arr['ad']['list_time'];
		}

		// Clean up data
		$this->cleanup($cleanup_args);
	}

	function create_week_string($prefix, &$arr) {
		$available_weeks = NULL;
		$available_weeks_next_year = NULL;
		$last_year = 0;
		for ($i = 1; $i <= 53; $i++) {
			$key = $prefix . $i;

			if (isset($arr[$key])) {
				if (preg_match("/[0-9]{4}/", $arr[$key])) {
					$value = $arr[$key] . ":" . $i;

					if ($arr[$key] < $last_year) {
						$available_weeks_next_year = $available_weeks;
						$available_weeks = NULL;
					}

					$last_year = $arr[$key];

					if ($available_weeks == NULL)
						$available_weeks = $value;
					else
						$available_weeks .= "," . $value;
				}
			}
		}

		if ($available_weeks == NULL) 
			return NULL;

		return $available_weeks . ($available_weeks_next_year ? ",$available_weeks_next_year" : "");
	}

	function set_values($setting, $key, $value, $data = null) {
		if ($data)
			$data .= ',' . $key;
		else
			$data = $key;

		if ($value)
			$data .= ":" . $value;
	}

	function key_lookup($setting, $key, $data = null) {
		global $BCONF;
	
		$cat = (!isset($this->sub_category) || empty($this->sub_category)) ? $this->category_group : $this->sub_category;
	
		if($key == "parent") {
			if (isset($cat)) {
				$parent = bconf_get($BCONF, '*.cat.' . $cat  . '.parent');
				if (!empty($parent))
					return $parent;
				else
					return $cat;
			}

			if (isset($this->lookup_data['category_group']))
				return $this->lookup_data['category_group'];

			return NULL;
		}

		if($key == "category") {
			if (isset($cat))
				return $cat;

			if(isset($this->lookup_data['sub_category']))
				return $this->lookup_data['sub_category'];

			if (isset($this->lookup_data['category_group']))
				return $this->lookup_data['category_group'];

			return NULL;
		}

		if (isset($this->lookup_data) && is_array(@$this->lookup_data[$key])) {
			return null;
		} else {
			if (isset($this->lookup_data) && isset($this->lookup_data[$key])) {
				return $this->lookup_data[$key];
			}
		}	

		if (is_array(@$this->$key)) {
			return NULL;
		}

		return @$this->$key;
	}


	function cleanup($cleanup_args = NULL) {
		global $BCONF;

		foreach ($this as $key => $value) {
			if (is_array($value)) {
				if(!empty($value)) {
					// FT3578: Strip arrays, fixes XSS
					array_walk_recursive($this->$key, 'clean_and_trim_byref');
				}
				continue;
			}
			if (isset($value)) {
				if (($key == 'mileage' || $key == 'brand' || $key == 'model' || $key == 'version') && $value === "") {
					$this->$key = null;
				} else if ($key == 'sub_category' && $value === 0) {
					$this->$key = null;
				} else if ($value === "" && $key != 'mileage'  && $key != 'brand' && $key != 'model' && $key != 'version') {
					$this->$key = null;
				} else if (is_numeric($value) && strcmp(intval($value), $value) == 0) {
					$this->$key = (int)$value;
				} else if ($key != 'passwd') {
					// No tabs in input fields
					$value = preg_replace("/\t/", " ", $value);
					if ($key != 'body') {
						$value = preg_replace("/ +/", " ", $value);
						// $80 is euro in FF latin1
						$value = preg_replace("/[!\$�\x80]/", '', $value);
						$value = preg_replace('/\*+/', '*', $value);
						if ($key != 'geoposition')
							$value = preg_replace('/^[*#<>~-]+/', '', $value);
						if (!isset($cleanup_args['noclean']['subject_tail']))
							$value = preg_replace('/[*#<>~.]$/', '', $value);
						$value = preg_replace('/\(+/', '(', $value);
						$value = preg_replace('/\)+/', ')', $value);

						if (!in_array($key, array('email', 'infopage'))) {
							$value = preg_replace('/-+/', '-', $value);
							$value = preg_replace('/_+/', '_', $value);
						}
						if (!isset($cleanup_args['noclean']['periods']))
							$value = preg_replace('/\.+/', '.', $value);
						$value = preg_replace('/~+/', '~', $value);
						$value = preg_replace("/\\\\+/", "\\", $value);
						//$value = preg_replace('/^"(.+)"$/', '\1', $value);
												
						if (in_array($key, explode(',', bconf_get($BCONF,"*.newad.cleanup.value_to_upper_case")))) {
							$value = strtoupper($value);
						} else if ($value == strtoupper($value))
							$value = strtolower($value);
						if ($key == 'subject') {
							preg_match('/^(\.? ?)(.*)/', $value, $match);
							$value = $match[1] . ucfirst($match[2]);
						}
						if ($key == 'zipcode') {
							$value = str_replace(' ', '', $value);
						}
					}
					// Clean newlines
					$this->$key = clean_and_trim($value);
				}
			} else $this->$key = null;
		}

		$features = '';
		get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $features);
		syslog(LOG_DEBUG, "bAd cleaning up with features $features");
		$features = explode(',', $features);
		
		foreach ($features as $k => $v) {
			if( ($offset = strpos($v, ':')) !== false ) {
				$features[$k] = substr($v, 0, $offset);
			}
		}

		foreach (bconf_get($BCONF, "*.common.ad_params") as $param) {
			if (isset($param['name'])) {
				$key = $param['name'];

				if (isset($this->$key) && !in_array($key, $features) && (!isset($param['no_feature']) || $param['no_feature'] != 1)) {
					$this->$key = null;
				}
			}
		}

		// Check if chosen city belongs to the chosen region
		if (isset($this->city) && !isset($GLOBALS['BCONF']['*']['common']['region'][$this->region]['cities'][$this->city]))
			$this->city = null;

		// Check if chosen category should have price
		if (isset($this->price) && in_array('noprice', $features))
			$this->price = null;

		if (isset($this->infopage) && substr($this->infopage, 0, 7) != "http://") {
			$this->infopage = "http://" . $this->infopage;
		}

		// If not company ad, clear chosen store
		if (@$this->company_ad == "0") {
			$this->store = null;
			$this->company_ad = "0";
		}


		// Price 0 is not allowed also make sure that we are not maintaining the value for categories that do not accept price especially on edits
		if (isset ($this->price)) {
			if ($this->price === 0 || $this->price === '')
				$this->price = null;
			else {
				$noprice = '';
				get_settings(bconf_get($BCONF,"*.category_settings"), "noprice", array($this, "key_lookup"), array($this, "set_values"), $noprice);
				syslog(LOG_DEBUG, "bAd cleaning up price $noprice");
				$noprice = explode(',', $noprice);
				foreach ($noprice as $key => $value) {
					if ( $value === 'enabled:1')
						$this->price = null;
				}
			}
		}

		/* Remove city if area is set */
		if (isset($this->area) && isset($this->city))
		  $this->city = null;
	}

	/**
	 * Populates the job ans services ad params
	 *
	 * @param array $post Parameters received from the form
	 */
	function populate_jobs_and_services($post) {
		/* Add job categories*/
		if (!empty($post["jc"])){
			$job_category = implode("|",array_values($post["jc"]));
			$this->job_category = $job_category;
		}

		/* Add service type */
		if (!empty($post["sct"])){
			$service_type = implode("|",array_values($post["sct"]));
			$this->service_type = $service_type;
		}

		/* Add services real estate */
		if (!empty($post["srv"])){
			$services = implode("|",array_values($post["srv"]));
			$this->services = $services;
		}

		/* Add footwear type */
		if (!empty($post["fwtype"])){
			$footwear_type = implode("|",array_values($post["fwtype"]));
			$this->footwear_type = $footwear_type;
		}

		/* Add equipment real estate */
		if (!empty($post["eqp"])){
			$equipment = implode("|",array_values($post["eqp"]));
			$this->equipment = $equipment;
		}
	}
}
?>
