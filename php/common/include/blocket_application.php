<?php

require_once('autoload_lib.php');

use Yapo\Metrics\MetricsAgentFactory;

/**
 \class blocket_application
 \brief Standard application class. Automatically handles fsm, session and forms

 \section Basics

 Extend this class and define function get_state($statename)
 It should return an array for the given state or null if no such state.

 Array elements:
  - function  The name of the function that handles this state.
  - params    An array of params, described below.
  - method    'post' or 'get'. If 'post', a redirect will be done
               when the function returns and state progress will be set to return value.
  - allowed_states   An array of valid state progress for this state.
 
  The params array is in 'name' => 'filter' form or just 'name'. In the latter case a default
  filter called f_name is used. The filter is a function or blocket_filter subclass
  that will filter the param for valid values.
 
  What state is requested will be determined by the URL. There is thus no good way to set the
  next state to be used. Instead the state progress is set and each state can have a check
  on the allowed state progress. This is so the user is allowed more freedom with the back
  and forward buttons.
 
  Each time a 'post' method is used, the return value of the function (if not null) will be
  the new state progress. A redirect is then done to that state.
  The return value of a get method is ignored, except for 'FINISH' (see SESSIONS).
 
  Example: - Application has a form and a result page.
           - The result page is only allowed if the form has been posted, and that state thus
             only have itself as allowed state.
           - The user is however allowed to click back to go back to the form, and thus the
             form state should have both itself and the result state as allowed progress.

 \section Sessions
 
 Your class should call the init function from its constructor. It takes two arguments
 representing the start state and the maximum number of session instances allowed.
 If the max is 0 no data will be saved in sessions and you can thus only use the
 parameters for data.
 If the max is higher, each session will allow that many instances of the application
 to be stored. All class variables are automatically stored in the session and restored
 upon the next call, except for those marked as volatile.
 
 Once you wish to destroy an instance, return 'FINISH' from its state function.
 Static variables are used to initialize a new instance with default values.
 When a new instance is created they will be set to the last stored values from another instance.

*/
class blocket_application extends blocket_filter
{
	var $state_progress;
	var $state_params;
	var $caller;
	var $current_state;
	var $instance_id;
	var $static_vars = array();
	var $volatile_vars = array('static_vars', 'redirect_done', 'volatile_vars', 'current_state', 'use_session', 'state_params', 'current_method', 'navigation', 'init_data', 'extra_bconf');
	var $redirect_done;
	var $use_session;
	var $errors;
	var $warnings;
	var $messages;
	var $application_name;
	var $single_instance;
	var $current_method;
	var $navigation;
	var $init_data;
	var $extra_bconf;
	protected $metrics_agent;
	
	/* Default constructor */
	function blocket_application() {
	}
	
	/* Initialize the application by assigning it to a session and settings fsm state */
	function init($start_state, $max_instances = 0, $single_instance = false) {
		global $session;
		
		$this->setup_metrics();
		/* State and session instance id are stored in PATH_INFO */
		@list ($dummy, $this->current_state, $this->instance_id) = explode('/', $_SERVER['PATH_INFO']);

		/* For single instance applications - set the id to 0 */
		if ($single_instance) {
			$this->single_instance = 1;
			$max_instances = 1;
			$this->instance_id = "0";
		}
	
		/* Settings default current state */
		if (!$this->current_state)
			$this->current_state = $start_state;
		
		/* Getting the caller */
		if (!$this->caller)
			$this->caller = new blocket_caller();

		$this->use_session = ($max_instances > 0);

		if ($this->use_session) {
			if (!$session)
				$session = new bSession();

			/* Session control */
			if (!$session->is_initialized() && $this->current_state != 'init') {
				$this->redirect('init', $this->init_data);
			} else {
				$get_instance = $this->instance_id;

				/* Loading session instance and overwriting data on current object */
				$session->load_instance($this, $max_instances);
				$this->redirect_done = false;

				/* If no progress made then settings the default progress to the default state */
				$query = null;
				if (!isset($this->state_progress) || !$this->state_progress) {
					$query = $this->init_data;
					$this->state_progress = $start_state;
				}

				/* If instance in url is nonexistant or wrong, redirect to our instance id. */
				if ($get_instance !== "$this->instance_id" && $this->current_state != 'init') 
					$this->redirect(null, $query);
			}
		} elseif ($max_instances < 0) {
			/* No session, but disable browser cache */
			header ('Expires: Thu, 19 Nov 1981 08:52:00 GMT');
			header ('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
			header ('Pragma: no-cache');
		}
	}
	
	/* Abstract function for getting state data */
	function get_state($name) {
		return NULL;
	}

	function validate_state() {
		return true;
	}


	/* Initialize session */
	function session_init($request_state = null) {
		global $session;
		
		$session->initialize();
		$this->redirect($request_state);
	}
	
	/* State machine for the application, handles request, validates state and state progressing */
	function run_fsm() {

		global $session;
		/* Check if we've already been redirected (from init for example) */
		if ($this->redirect_done) {
			if ($this->use_session) {
				$session->store_instance($this);
			}
			return;
		}

		/* Run the current state */
		if ($this->current_state == 'init') {
			$state = array('function' => 'session_init', 'params' => array('request_state' => 'f_state'), 'method' => 'get');
		} else {
			$state = $this->get_state($this->current_state);
		}

		if (!$state || (isset($state['allowed_states']) && !in_array($this->state_progress, $state['allowed_states']))) {
			/* Unknown/Unallowed state. Redirect to last known ok state. */
			$this->redirect();
		} elseif (isset($state['auth_required']) && !$this->validate_state()) {
			$this->state_progress = $state['auth_required'];
			$this->redirect();
		} else {
			/* Handle the request */
			$args = $this->parse_request(@$state['params'], $state['method']);

			if (in_array($state['method'], array('both', 'post')) || $this->current_state != $this->state_progress) {
				$this->set_errors(null);
				$this->set_messages(null);
			}

			if (isset($state['reset_progress'])) {
				$this->state_progress = $this->current_state;
			}

			$txnName = sprintf('/%s/%s', get_called_class(), $state['function']);
			$this->metrics_agent->setTransactionName($txnName);

			$this->current_method = $state['method'];
			$new_state = call_user_func_array(array(&$this, $state['function']), $args);

			if ($new_state != 'FINISH' && $new_state != 'STORE_INSTANCE' && ($new_state || $this->use_session)) {
				/*
				 * Check for array('state', 'arg1' => '2') format.
				 */
				if (is_array($new_state)) {
					$args = $new_state;
					unset($args[0]);
					$new_state = $new_state[0];
				} else
					$args = null;
				if (in_array($state['method'], array('both', 'post'))) {
					/*
					 * When posting, redirect to the new state.
					 * If not using session, get the state immediately.
					 */
					if ($new_state) {
						$this->state_progress = $new_state;
					}
					if ($this->use_session) {
						$session->store_instance($this);
						$session_stored = 1;
						session_write_close();
						$this->redirect(null, $args);
					} else {
						$this->current_state = $this->state_progress;
						$this->run_fsm();
					}
				} else if ($state['method'] == 'get' && !empty($new_state) && $new_state != $this->current_state) {
					$this->state_progress = $new_state;
					if ($this->use_session) {
						$session->store_instance($this);
						$session_stored = 1;
						session_write_close();
					}
					$this->redirect($new_state, $args);
				}
			}
		}
		
		if ($this->use_session && !isset($session_stored)) {
			/* On finish, destroy session instance */
			if (isset($new_state) && $new_state == 'FINISH') {
				$session->destroy_instance($this);
			} else {
				$session->store_instance($this);
			}
			session_write_close();
		}
	}


	/* Display using templates
	 * ATT! Only allowed in 'get' states.
	 * Simple data is % and @ variables. Atomic and array class variables are automatically added.
	 * Extended data is an array accessed with $(). Bconf plus object class variables are
	 * automatically added. If nonnull, the template cache is flushed. You can pass true if you want the
	 * cache flushed without adding additional data.
	 * bconf_appl is what part of bconf to use in addition to '*'. By default the class name is used,
	 * with an optional 'blocket_' prefix stripped.
	 */
	function display($template, $simple_data = null, $extended_data = null, $bconf_appl = null, $dont_check_post = false) {
		global $BCONF;
		global $session;

		if (!$dont_check_post) {
			if (in_array($this->current_method, array('both', 'post')))
				trigger_error('display() not allowed in POST step', E_USER_ERROR);
		}

		if (!is_array($simple_data))
			$simple_data = array();

		if (!is_array($extended_data))
			$extended_data = array();

		if (isset($_SESSION) && isset($_SESSION['global'])) {
			foreach ($_SESSION['global'] as $key => $value) {
				$simple_data['session_' . $key] = $value;
			}
		}

		/* Object data */
		foreach(get_object_vars($this) as $key => $value) {
			/* Put objects in extended data, rest in simple. */
			if (is_extended($value))
				$extended_data[$key] = is_object($value) ? get_object_vars($value) : $value;
			else if (!is_null($value)) {
				if (@$simple_data[$key] !== null)
					syslog(LOG_WARNING, "simple_data[{$key}] set, so ignoring this->{$key}");
				else
					$simple_data[$key] = $value;
			}
		}

		/* BCONF */
		$bconf_data = $BCONF['*'];
		if (empty($bconf_appl))
			$bconf_appl = str_replace('blocket_', '', get_class($this));
		if (isset($BCONF[$bconf_appl]))
			$bconf_data += $BCONF[$bconf_appl];
		if (!empty($this->extra_bconf))
			$bconf_data += $this->extra_bconf;

		/* Errors and warnings */
		if (is_array($this->errors)) {
			foreach ($this->errors as $key => $code) {
				$keys = explode(";", $key);

				if (!is_array($code))
					$codes = explode(";", $code);

				foreach ($keys as $tmpkey) {
					$tmppos = array_search($tmpkey, $keys);
					$err_key = 'err_' . str_replace('.', '_', $tmpkey);
					if (is_array($code)) {
						$simple_data[$err_key] = $code;
						if (!empty($this->messages[$tmpkey]))
							$simple_data['msg_' . str_replace('.', '_', $tmpkey)] = $this->messages[$tmpkey];
					} else {
						$simple_data[$err_key] = lang((end($codes) == $codes[$tmppos] ? "" : (end($codes) . "_")) . $codes[$tmppos]);
						$simple_data[$err_key] = str_replace('#caller', $this->caller->to_string(), $simple_data[$err_key]);
						if (!empty($this->messages[$tmpkey])) {
							$simple_data[$err_key] .= ' "' . $this->messages[$tmpkey] . '"';
							$simple_data['msg_' . str_replace('.', '_', $tmpkey)] = $this->messages[$tmpkey];
						}
					}
				}
			}
		}

		if (is_array($this->warnings)) {
			foreach ($this->warnings as $key => $code) {
				$err_key = 'warn_' . str_replace('.', '_', $key);
				$simple_data[$err_key] = lang($code);
				$simple_data[$err_key] = str_replace('#caller', $this->caller->to_string(), $simple_data[$err_key]);

				if (!empty($this->messages[$key]))
					$simple_data[$err_key] .= ' "' . $this->messages[$key] . '"';
			}
		}
		
		/* Caller */
		if (!isset($simple_data['b_caller']) && isset($this->caller))
			$simple_data['b_caller'] = $this->caller->to_string();

		if (isset($this->application_name))
			$simple_data['appl'] = $this->application_name;

		/* Session */
		if ($this->use_session && $session) {
			$simple_data['session_input'] = $session->get_form_input();
			$qs = $session->get_query_string(true);
			if ($qs) {
				$simple_data['session_q'] = "?$qs";
				$simple_data['session_a'] = "&$qs";
				$simple_data['session_p'] = "/$qs";
			}
		}

		if ($session && bconf_get($BCONF, "php_debugger.active") == "1" && get_class($this) != "blocket_debug") {
			require_once('blocket_debug.php');
			$debugger = new blocket_debug(false);
			$session->load_instance($debugger, 1);
			$debugger->add_template($template);
			$debugger->add_simple_data($simple_data);
			$debugger->add_extended_data($bconf_data + $extended_data);
			$session->store_instance($debugger);
		}
		$simple_data['NOW'] = strftime("%Y-%m-%d %T");
		$simple_data['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
		$simple_data['REQUEST_HOST'] = $_SERVER['HTTP_HOST'];
		$simple_data['REMOTE_BROWSER'] = $_SERVER['HTTP_USER_AGENT'];
		
		$request_uri_array = explode("?",$_SERVER['REQUEST_URI']);
		$request_uri = $request_uri_array[0];
		if(strpos($request_uri,"/") == 0) $request_uri = substr($request_uri,1);
		$simple_data['REQUEST_URI'] = $request_uri;
		$simple_data['QUERY_STRING'] = $_SERVER['QUERY_STRING'];
		
		call_template($simple_data, $extended_data, $bconf_data, $template, default_template_options());
	}

	function display_simple($template = null, $simple_data = null, $extended_data = null, $bconf_appl = null) {
		$this->display_layout($template, $simple_data, $extended_data, $bconf_appl, 'simple');
	}

	function display_popup($template = null, $simple_data = null, $extended_data = null, $bconf_appl = null) {
		$this->display_layout($template, $simple_data, $extended_data, $bconf_appl, 'popup');
	}

	function display_layout($template = null, $simple_data = null, $extended_data = null, $bconf_appl = null, $wrapper = 'general') {
		global $BCONF;

		$l = $this->caller->layout;
		if (!is_array($simple_data))
			$simple_data = array();
		$simple_data += array('l' => $l, 'ca' => $this->caller->to_string(), 'ca_region' => $this->caller->region, 'ca_city' => $this->caller->city, 'content' => $template);
		
		if ($wrapper == 'general')
			#$tmpl = 'common/general' . bconf_get($BCONF, "*.common.layout.$l.template") . '.html';
			$tmpl = 'common/general0.html';
		else
			$tmpl = 'common/'.$wrapper.'.html';

		$this->display($tmpl, $simple_data, $extended_data, $bconf_appl);
	}
	
	/* Store rendered template in variable and return */
	function get_template_result($template = null, $simple_data = null, $extended_data = null, $bconf_appl = null) {
		ob_start();
		$this->display($template, $simple_data, $extended_data, $bconf_appl, true);
		$buffer = ob_get_contents();
		ob_clean();
		return $buffer;
	}
	
	/* Minor error handling for simpler template output. */
	function set_messages($msgs) {
		$this->messages = $msgs;
	}
	
	function set_errors($errs) {
		$this->errors = $errs;
	}

	function add_error($key, $err) {
		$this->errors[$key] = $err;
	}
	
	function remove_error($key) {
		unset($this->errors[$key]);
	}

	function set_warnings($warns) {
		/*
		 * Warnings should only be set once, so unset identical
		 * warnings.
		 */
		if (is_array($this->warnings)) {
			foreach($this->warnings as $key => $code) {
				if (isset($warns[$key]) && $warns[$key] == $code)
					unset($warns[$key]);
			}
		}
		$this->warnings = $warns;
	}
	
	function has_error() {
		return is_array($this->errors) && count($this->errors);
	}
	
	function has_warning() {
		return is_array($this->warnings) && count($this->warnings);
	}

	/* Input filtering */
	function parse_request($params, $method) {
		$args = array ();
		
		if ($method == 'both')
			$data = $_REQUEST;
		else if ($method == 'post')
			$data = $_POST;
		else
			$data = $_GET;

		return $this->parse_params($params, $data);
	}

	function parse_params($params, $data) {
		$args = array ();

		if (is_array($params)) {
			foreach ($params as $param => $filter) {
				$value = $this->parse_param($param, $filter, $data);
				$args[] = $value;

				$this->state_params[$param] = $value;
			}
		}
		

		return $args;
	}

	function flatten_params($params) {
		$res = array();

		if (is_array($params)) {
			foreach ($params as $param => $filter) {
				$p = $this->flatten_param($param, $filter);
				$res = array_merge($res, $p);
			}
		}
		return $res;
	}

	/* Redirect application to requested state, and handle PATH_INFO */
	function redirect($state = null, $query = null) {
		global $session;

		if (empty($state))
			$state = $this->state_progress;

		if (is_null($query)) {
			/* Remove session from query */
			$query = preg_replace('/(^|&)s=[^&]+/', '', $_SERVER['QUERY_STRING']);
			if (substr($query, 0, 1) == '&')
				$query = substr($query, 1);

			/* Remove request_state from query */
			$query = preg_replace('/(^|&)request_state=[^&]+/', '', $query);
			if (substr($query, 0, 1) == '&')
				$query = substr($query, 1);
		} elseif (is_array($query)) {
			$qs = array();
			foreach ($query as $arg => $val)
				$qs[] = urlencode($arg) . '=' . urlencode($val);
			$query = implode('&', $qs);
		}
		if (empty($query))
			$query = '';
		else
			$query = '?' . $query;
		
		if (empty($this->application_name))
			$uri = $_SERVER['SCRIPT_NAME'] . "/$state/" . $this->instance_id . $query;
		else
			$uri = "/" . $this->application_name . "/$state/" . $this->instance_id . $query;

		if ($state == 'init' && !empty($this->current_state))
			$uri .= ($query ? '&' : '?') . 'request_state='.$this->current_state;

		if ($this->use_session && $session->session_init_method() == 'url')
			$uri .= ($query ? '&' : '?') . 's='.$session->session_id;

		header("Location: $uri");
		$this->redirect_done = true;
	}

	/*
	 * A copy of static variables are stored outside of instance.
	 * They are then loaded when a new instance is created.
	 * Example usage is name and email in newad.
	 */
	function add_static_vars($names) {
		$this->static_vars = array_merge($names, $this->static_vars);
	}

	/*
	 * Volitile vars are never stored in session.
	 */
	function add_volatile_vars($names) {
		$this->volatile_vars = array_merge($names, $this->volatile_vars);
	}

	function is_active($authenticate) {
		return true;
	}

	function add_navigation($offset, $total, $limit, $page_max_count) {
		$uri = $_SERVER['SCRIPT_NAME'] . "/{$this->current_state}/" . $this->instance_id . '?';
		$this->navigation = new blocket_navigation($offset, $total, $limit, $page_max_count, $uri);
	}

	protected function setup_metrics() {
		$this->metrics_agent = MetricsAgentFactory::getMetricsAgent($BCONF, 'blocket_application');
	}
}

