<?php

namespace Yapo;

interface AdsEvaluatorService
{
	public function insertReasons();
	public function getReasons();
	public function editReasons();
	public function deleteReasons();
}
