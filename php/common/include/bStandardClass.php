<?php
/*
 * Standard class that has error handling
 */
class bStandardClass {
	var $_error_code = 'ERROR_OK';
	
	function _error($code) {
                if ($this->_error_code == 'ERROR_OK')
                        $this->_error_code = $code;
		return false;
	}

	function clear_error() {
		$error = $this->_error_code;
		$this->_error_code = 'ERROR_OK';

		return $error;
	}
	
	function get_error() {
		return $this->_error_code;
	}

        function has_error() {
                return $this->_error_code != 'ERROR_OK';
        }
}
?>
