<?php

use Yapo\Bconf;
use Yapo\ControlPanel\TokenManager;

require_once('init.php');

/**
 * Provide access to Trans and all its power
 *
 * Dependency injection:
 * - controlpanel.bTransaction.TokenManager
 *
 * Bconf:
 * - `appl`.common.transaction
 * - *.common.transaction
 * - *.common.session.expire
 * - *.service.bconfd
 */
class bTransaction
{
	var $_trans_node;
	var $_data;
	var $_reply;
	var $_status;
	var $_errors;
	var $_warnings;
	var $_messages;
	var $_send_newline;
	var $_fn_trans_commit = 'trans_commit';
	var $_appl;

	var $auth_type;
	var $TokenManager;

	function bTransaction($appl = null, array $bconf = null, TokenManager $tokenManager = null) {
		$this->_appl = $appl;
		$this->bconf = isset($bconf) ? $bconf : $BCONF;
		$this->reset();
		$TokenManager = Bconf::get($this->bconf, "controlpanel.bTransaction.TokenManager");
		$this->TokenManager = isset($tokenManager) ? $tokenManager : new $TokenManager();
	}

	// Prefer passing a node to send_command if necessary. This is useful for backwards compatibility and script hacks.
	function set_connect_info($hostname, $port) {
		// synthesize a bconf node based on hostname + port
		$this->_trans_node = array(
			'host' => array(1 => array('name' => $hostname, 'port' => $port))
		);
	}

	function reset() {
		$this->_data = array();
		$this->_errors = null;
		$this->_warnings = null;
		$this->_messages = null;
		$this->_send_newline = null;
		$this->_reply = null;
		$this->_status = null;
		$this->_trans_node = null;

		if (!is_null($this->_appl)) {
			$this->_trans_node = Bconf::get($this->bconf, $this->_appl.".common.transaction");
		}

		if (is_null($this->_trans_node)) {
			$this->_trans_node = Bconf::get($this->bconf, "*.common.transaction");
			$this->_appl = '*';
		}
		return $this;
	}

	function add_client_info() {
		$this->add_data('remote_addr', get_remote_addr());
		$this->add_data('remote_browser', !empty($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "No browser set");
	}

	function add_data($key, $value, $clean = true) {
		if (is_array($value)) {
			$this->add_array($key, $value);
			return;
		}

		if (!preg_match("/^blob:[0-9]+:/", $key) && preg_match("/\n|\r/", $value)) {
			if ($clean)
				$value = clean_blob($value);
			$this->_data['blob:'.strlen($value).':'.$key] = $value;
		} else {
			$this->_data[$key] = $value;
		}
	}

	function remove_data($key, $remove_all=false) {

		/* In case remove_all flag is ON, we will also delete all 'blob' data */
		unset($this->_data[$key]) ;
		if ($remove_all==false) {
			return;
		}

		/* Loop all keys, preg'ing for blobbed ones */
		foreach ($this->_data as $k=>$v ) {
			if (preg_match("/^blob:[0-9]+:".$key."/", $k)) {
				unset($this->_data[$k]);
				continue;
			}
		}
	}

	function add_array($key, $array) {
		$isblob = preg_match("/^blob:[0-9]+:/", $key);

		foreach ($array as $value) {
			if (!$isblob && preg_match("/\n|\r/", $value)) {
				$value = clean_blob($value);
				$this->_data['blob:'.strlen($value).':'.$key][] = $value;
			} else {
				$this->_data[$key][] = $value;
			}
		}
	}

	function add_command_blob($value){
		$this->_data["blob:".strlen($value).":command\n"] = $value;
	}

	function populate($data, $array_list = null, $array_comma_list = null) {
		if (!is_array($data)) return;
		foreach ($data as $key => $value) {
			if (is_array($array_list) && in_array($key, $array_list)) {
				if (is_array($array_comma_list) && in_array($key, $array_comma_list)) {
					// For now we'll have only one key, but that'll likely change so we loop.
					if (count($value)) {
						for ($i = 0 ; $i < sizeof($key) ; ++$i) {
							$this->add_data($key . ($i + 1), join(',', $value));
						}
					}
				} else {
					for ($i = 0 ; $i < sizeof($value) ; ++$i) {
						if ($value[$i] !== null && $value[$i] !== '')
							$this->add_data($key . ($i + 1), $value[$i]);
					}
				}
			} else {
				$this->add_data($key, $value);
			}
		}
	}

	function add_file($key, $file) {
		$filesize = strlen($file);
		$this->add_data('blob:'.$filesize.':'.$key, $file);
		$this->_send_newline = true;
	}

	function add_file_array($key, $array) {
		foreach ($array as $value) {
			$this->add_array('blob:'.strlen($value).':'.$key, array($value));
		}
		$this->_send_newline = true;
	}

	function validate_command($cmd) {
		return $this->send_command($cmd, false);
	}

	function store_token($_, $val) {
		$at = !empty($this->auth_type) ? $this->auth_type : 'admin';
		$key = session_id() . "_token_$at";

		if (is_error($val)) {
			$this->TokenManager->relinquish($key);
			return;
		}

		$expire = Bconf::get($this->bconf, "*.common.session.expire") * 60;
		$this->TokenManager->putToken($key, $val, $expire);

		if ($at == 'admin') {
			setcookie('token_expire', time() + Bconf::get($this->bconf, "*.common.session.expire") * 60,
						time() + Bconf::get($this->bconf, "*.common.session.expire") * 60);
		}
	}

	function send_command($cmd, $commit = true, $user_data = false, $perform_callbacks = true, $trans_bconf = NULL, $retry_wait = 0, $skip_logstring = 0) {
		if ($this->_send_newline)
			$header['newline'] = '';
		$header['cmd'] = $cmd;
		if (function_exists('log_string') && !$skip_logstring)
			$header['log_string'] = log_string();
		if ($commit)
			$header['commit'] = 1;

		if ($user_data) {
			$this->add_data('remote_addr', get_remote_addr());
			$this->add_data('remote_browser', substr($_SERVER['HTTP_USER_AGENT'], 0, 348 ));
		}

		$fn_trans_commit = $this->_fn_trans_commit;
		$callbacks = $perform_callbacks ? array('token' => array(&$this, 'store_token')) : array();
		$reply = $fn_trans_commit(!is_null($trans_bconf) ? $trans_bconf : $this->_trans_node, $header, $this->_data, $callbacks);

		/* Retry on failure. */
		if ( $retry_wait > 0 ) {
			/* If the transaction gave a TRANS_FATAL reply or trans was uncontactable, try again in a litle while. */ 
			if ( $reply < 0 || strstr($reply['status'], "TRANS_FATAL") ) {
				time_sleep_until(time() + $retry_wait) ;
				$reply = $fn_trans_commit(!is_null($trans_bconf) ? $trans_bconf : $this->_trans_node, $header, $this->_data, $callbacks);
			}
		}

		$this->_reply = $reply;

		/* XXX Array support in transaction protocol, should be implemented in php_trans module */
		foreach ($reply as $key => $value) {
			if (preg_match("/\./", $key)) {
				// Remove old key and value
				unset($reply[$key]);

				// Add array into reply
				$key_tree = explode('.', $key);
				$node = array_shift($key_tree);
				$root =& $reply[$node];
				foreach ($key_tree as $node)
					$root =& $root[$node];

				$root = $value;
			}
		}

		$this->status = null;

		return $reply;
	}

	function send_admin_command($cmd, $commit = true, $user_data = true, &$trans_bconf = NULL) {
		if ($this->auth_type) {
			$at = $this->auth_type;
		} else {
			$at = 'admin';
		}

		$key = session_id() . "_token_$at";
		$token = $this->TokenManager->getToken($key);
		$this->add_data('token', $token);

		return $this->send_command($cmd, $commit, $user_data, true, $trans_bconf);
	}

	function add_error($key, $code, $message) {
		if (empty($this->_errors[$key])) {
			$this->_errors[$key] = $code;
			$this->_messages[$key] = $message;
		} else if (!is_array($this->_errors[$key])) {
			$this->_errors[$key] = array($this->_errors[$key], $code);
			$this->_messages[$key] = array($this->_messages[$key], $message);
		} else {
			$this->_errors[$key][] = $code;
			$this->_messages[$key][] = $message;
		}
	}

	function add_warning($key, $code, $message) {
		if (empty($this->_warnings[$key])) {
			$this->_warnings[$key] = $code;
			$this->_messages[$key] = $message;
		} else if (!is_array($this->_warnings[$key])) {
			$this->_warnings[$key] = array($this->_warnings[$key], $code);
			$this->_messages[$key] = array($this->_messages[$key], $message);
		} else {
			$this->_warnings[$key][] = $code;
			$this->_messages[$key][] = $message;
		}
	}

	function reply_explode($str) {
		$code = $str;
		$msg = null;
		$str_exp = explode(":", $str);
		if (count($str_exp) > 1) {
			$code = $str_exp[0];
			$msg = $str_exp[1];
		}
		return array($code, $msg);
	}

	function handle_reply($nodie = false) {
		if (!$this->_reply)
			return;
		syslog(LOG_INFO, log_string().'Handling reply from transaction handler');

		$reply = $this->_reply;

		// First get the status
		if (isset($reply['status'])) {
			list($status, $smessage) = $this->reply_explode($reply['status']);
			syslog(LOG_INFO, log_string()."Retrieved status=$status $smessage");
			if ($status != 'TRANS_OK' && $status != 'TRANS_ERROR' && !$nodie) {
				$GLOBALS['dbg_trans_reply'] = $reply;
				die_with_template(__FILE__, log_string() .
					'PHP::' . substr(__FILE__, strrpos(__FILE__, '/') + 1 ) .
					'::handle_reply() === Transaction handler exited with status: >>>' .
					$reply['status'] . "<<<\n");
			}
		} else {
			$status = 'TRANS_FATAL';
			$smessage = 'no status';
			syslog(LOG_ERR, log_string()."Did not receive transaction status");
		}

		// Set error and warning messages
		foreach ($reply as $key => $str) {
			// Split the string into code and message
			if (is_array($str)) {
				foreach ($str as $s) {
					list($c, $m) = $this->reply_explode($s);
					if (is_warning($c)) {
						syslog(LOG_INFO, log_string()."Retrieved warning $c $m on $key");
						$this->add_warning($key, $c, $m);
					} else if ($status != 'TRANS_OK' && is_error($c)) {
						syslog(LOG_INFO, log_string()."Retrieved error $c $m on $key");
						$this->add_error($key, $c, $m);
					}
				}
			} else {
				list($code, $message) = $this->reply_explode($str);
				if (is_warning($code)) {
					syslog(LOG_INFO, log_string()."Retrieved warning $code $message on $key");
					$this->add_warning($key, $code, $message);

				} else if ($status != 'TRANS_OK' && is_error($code)) {
					syslog(LOG_INFO, log_string()."Retrieved error $code $message on $key");
					$this->add_error($key, $code, $message);
				}
			}
		}
		$this->_status = $status;

		// Check if we have unknown errors
		if ($status != 'TRANS_OK' && !$this->has_error()) {
			if ($nodie)
				$this->add_error('status', $status, $smessage);
			else {
				syslog(LOG_INFO, log_string().print_r(debug_backtrace(), true));
				die_with_template(__FILE__, log_string()."Transaction handler returns unkown errors, errorcode: $status : $smessage, reply: " . serialize($reply));
			}
		}
	}

	function at_serialize ($cmd, $commit = true) {
		$header['cmd'] = $cmd;
		if ($commit)
			$header['commit'] = 1;

		$data = '';
		foreach ($header as $key => $val) {
			if (is_array($val)) {
				foreach ($val as $v)
					$data .= "$key:$v\n";
			} else
				$data .= "$key:$val\n";
		}
		foreach ($this->_data as $key => $val) {
			if (is_array($val)) {
				foreach ($val as $v)
					$data .= "$key:$v\n";
			} else
				$data .= "$key:$val\n";
		}
		return $data;
	}

	function serialize ($cmd, $commit = true) {
		$header['cmd'] = $cmd;
		if (function_exists('log_string'))
			$header['log_string'] = log_string();
		if ($commit)
			$header['commit'] = 1;

		$data = '';
		foreach ($header as $key => $val) {
			if (is_array($val)) {
				foreach ($val as $v)
					$data .= "$key:$v\n";
			} else
				$data .= "$key:$val\n";
		}
		foreach ($this->_data as $key => $val) {
			if (is_array($val)) {
				foreach ($val as $v)
					$data .= "$key:$v\n";
			} else
				$data .= "$key:$val\n";
		}
		return $data;
	}

	function get_reply($key) {
		if ($key) {
			return array_key_exists($key, $this->_reply) ? $this->_reply[$key] : null;
		}
		return $this->_reply;
	}

	function get_status() {
		if (!$this->_status)
			$this->handle_reply();
		return $this->_status;
	}

	function has_error($nodie=false) {
		if (!$this->_status)
			$this->handle_reply($nodie);
		return is_array($this->_errors);
	}

	function has_warning() {
		if (!$this->_status)
			$this->handle_reply();
		return is_array($this->_warnings);
	}

	function has_message() {
		return (($this->has_error() || $this->has_warning()) && is_array($this->_messages));
	}

	function add_prefix($new_prefix, $data){
		foreach ($data as $key => $str) {
				$data[$new_prefix.$key] = $str;
				unset($data[$key]);
		}
		return $data;
	}

	function get_errors($prefix = null) {
		if ($this->has_error()){
			if(!isset($prefix))
				return $this->_errors;
			else{
				return $this->add_prefix($prefix,$this->_errors);
			}
		}
		return array();
	}

	function get_warnings($prefix = null) {
		if ($this->has_warning()){
			if(!isset($prefix))
				return $this->_warnings;
			else{
				return $this->add_prefix($prefix,$this->_warnings);
			}
		}
		return array();
	}

	function get_messages($prefix = null) {
		if ($this->has_message()){
			if(!isset($prefix))
				return $this->_messages;
			else{
				return $this->add_prefix($prefix,$this->_messages);
			}
		}
		return array();
	}

	function get_error($key) {
		if ($this->has_error())
			return $this->_errors[$key];
		return null;
	}

	function get_warning($key) {
		if ($this->has_warning())
			return $this->_warnings[$key];
		return null;
	}

	function get_message($key) {
		if (!$this->_status)
			$this->handle_reply();
		return $this->_messages[$key];
	}

	function get_data($key) {
		/* Check the simple case where the key is found directly. */
		if ( isset($this->_data[$key]) ) {
			return $this->_data[$key];
		}
		/* Check the complex case where the key is blobbed. */
		else {
			foreach ( $this->_data as $k => $v ) {
				if (preg_match("/^blob:[0-9]+:".$key."/", $k)) {
					return $v ;
				}
			}
		}	
		/* Fall through to returning null. */
		return null ;
	}

	static public function run($command, $data = null, &$trans = null, &$reply = null) {
		// create transaction, add data
		$trans = new bTransaction();
		if (is_array($data))
			foreach ($data as $key=>&$value)
				$trans->add_data($key, $value);

		// sends the command, processes the result
		$reply = $trans->send_command($command);
		if (!strstr($reply['status'], "TRANS_OK")) return false;
		return $reply;
	}

	static public function run_other_trans($trans_node, $command, $data = null, &$trans = null, &$reply = null) {
		// create transaction, add data
		$trans = new bTransaction();
		$trans->_trans_node = array('host' => $trans_node);
		if (is_array($data))
			foreach ($data as $key=>&$value)
				$trans->add_data($key, $value);

		// sends the command, processes the result
		$reply = $trans->send_command($command, true, false, false, null, 0, true);
		if ($reply['status'] != 'TRANS_OK') return false;
		return $reply;
	}

	static public function bconfd_get($data = null) {
		$bconfd_conninfo = Bconf::get($BCONF, "*.service.bconfd");
		$reply = bTransaction::run_other_trans($bconfd_conninfo, 'bconf', $data);
		return $reply;
	}
}

