<?php
function get_week_description($year, $from_week, $to_week, &$weeks, &$checked, $cnt) {
	$consecutive = false;
	$descr = NULL;
	$start_at = 0;
	$j = 0;

	for ($i = $from_week; $i < $to_week; $i++) {
		$selected = in_array($i, $weeks);

		if ($selected) {
			if (!$consecutive) 
				$start_at = $i;

			$consecutive = true;
		} else {
			if ($consecutive) {
				$end_at = $i - 1;

				if ($end_at == $start_at)
					$str = "v" . $start_at;
				else
					$str = "v" . $start_at . "-" . $end_at;

				if (!isset($descr))
					$descr = $year . ": " . $str;
				else
					$descr = $descr . ", " . $str;

			}

			$consecutive = false;
		}

		$checked[$cnt] = $selected ? "1" : "0";
		$cnt++;
	}

	// Yes! Duplicated code! Not pretty!
	if ($consecutive) {
		$end_at = $i - 1;

		if ($end_at == $start_at)
			$str = "v" . $start_at;
		else
			$str = "v" . $start_at . "-" . $end_at;

		if (!isset($descr))
			$descr = $year . ":" . $str;
		else
			$descr = $descr . "," . $str;
	}

	return $descr; 
}

function create_avail_weeks($avail, $varname = null, &$response) {
	if (isset($avail)) {
		list($descr, $checked) = get_avail_weeks($avail);
		if ($response) {
			$response->add_data($varname . "_descr", $descr);
			$response->add_data($varname, $checked);
		}
		return array($descr, $checked);
	}
}

function get_avail_weeks($avail) {
	$week = strftime("%V", time());
	$year = strftime("%G", time());
	$consecutive = false;
	$descr = "";
	$j = 0;
	
	if (isset($avail)) {
		$checked = array();
		$arr = explode(",", $avail);

		foreach($arr as $key) {
			$weeks[$j] = substr($key, 5);
			$j++;
		}	
		
		$descr_curr_year = get_week_description($year, (int)$week, strftime("%V", mktime(0, 0, 0, 12, 31, $year)) + 1, $weeks, $checked, 0);
		$descr_next_year = get_week_description($year + 1, 1, (int)$week, $weeks, $checked, count($checked));
	
		return array($descr_curr_year . " " . $descr_next_year, $checked);
	}
}

function mk_week_to_dates($week, $year){
	global $BCONF;

	if (empty($week) || empty($year))
		return '';

	$searchdate = mktime(0,0,0,12,20,$year-1);

	$searchdate = strtotime("+".($week-1)." week",$searchdate);

	$found=false;
	while ($found==false){
		if (date("W",$searchdate) == $week)
			$found = true;
		else
			$searchdate = strtotime("+1 day",$searchdate);
	}

	$first = explode("/", date("j/n/Y", $searchdate)); /* Y=yyyy n=mon (no 0) j=date (no 0) */
	$last = explode("/", date("j/n/Y", strtotime("+6 day", $searchdate)));

	/* date */
	$date = $first[0];

	/* first month */
	if ($first[1] != $last[1]) {
		$date .= " " . bconf_get($BCONF, "*.language.DATE.MONTHS." . $first[1] . ".short." . bconf_get($BCONF, '*.common.language'));
	}

	/* First year */
	if ($first[2] != $last[2]) {
		$date .= " " . $first[2] ;
	}

	$date .= " - ";

	/* End date */
	$date .= $last[0];

	/* End month */
	$date .= " " . bconf_get($BCONF, "*.language.DATE.MONTHS." . $last[1] . ".short." . bconf_get($BCONF, '*.common.language'));

	/* End year */
	$date .= " " . $last[2] ;

	return $date;
}


function generate_weeks(&$response) {
	$weekno_break =  array();
	
	$week = strftime("%V", time());
	$year = strftime("%G", time());

	$weeks = array();
	$j = 0;
	$col_count=0;

	for ($i = (int)$week; $i <= intval(strftime("%V", mktime(0,0,0,12, 31, $year))); $i++) {
		$weeks['weekno'][$j] = $i;
		$weeks['weekyear'][$j] = $year;
	       	$weeks['week_range'][$j] = mk_week_to_dates($i, $year);
		$weekno_break[$j] = ($col_count % 8 == 0) ? 1 : 0;
		$col_count++;
		$j++;
	}

	$col_count=0;
	for ($i = 1; $i < $week; $i++) {
		$weeks['weekno'][$j] = $i;
		$weeks['weekyear'][$j] = $year+1;
	       	$weeks['week_range'][$j] = mk_week_to_dates($i, $year+1);
		$weekno_break[$j] = ($col_count % 8 == 0) ? 1 : 0;
		$col_count++;
		$j++;
	}
	
	if ($response) {
		$response->add_data("weekno", $weeks['weekno']);
		$response->add_data("weekyear", $weeks['weekyear']);
		$response->add_data("week_range", $weeks['week_range']);
		$response->add_data("weekno_break", $weekno_break);
	}
	$weeks['weekno_break'] = $weekno_break;
	return $weeks;
}
?>
