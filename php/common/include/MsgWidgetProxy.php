<?php

class MsgWidgetProxy extends blocket_application
{
    private $account_session;
    private $msg_center_host;
    private $user_uuid;
    private $mc_utils;
    private $max_retry;
    private $img_host;
    private $ads_host;
    const PREFIX = "MSG-PROXY";

    public function __construct()
    {
        $this->application_name  = "msg_proxy";
        header('Content-type: application/json');
        $this->account_session = new AccountSession();
        $this->init('api', 0);
        $this->msg_center_host = str_replace('http://', '', bconf_get($BCONF, '*.messaging_center.url'));
        $this->user_uuid = $this->account_session->get_param('uuid');
        $this->mc_utils = new MessagingCenterUtils();
        $this->max_retry = bconf_get($BCONF, '*.messaging_center.max_retry');
        $this->img_host = bconf_get($BCONF, '*.common.base_url.images');
        $this->ads_host = bconf_get($BCONF, '*.common.base_url.vi');
    }

    private function checkUserLogged()
    {
        if (!$this->account_session->is_logged()) {
            $login = bconf_get($BCONF, '*.common.base_url.secure')."/login";
            header('Location: ' .$login);
            exit(0);
        }
    }

    public function get_state($statename)
    {
        switch ($statename) {
            case 'api':
                return array(
                    'function' => "api",
                    'method' => "both",
                    'params' => array(
                        "conversationId" => "f_generic",
                        "messageId" => "f_generic"
                    )
                );
            case 'get_ads_list_data':
                return array(
                    'function' => "get_ads_list_data",
                    'method' => "get",
                    'params' => array("ids" => "f_generic")
                );
        }
    }

    public function f_generic($var)
    {
        return strip_tags($var);
    }

    private function getPayloadParams()
    {
        $content_type = (isset($_SERVER['CONTENT_TYPE']) ? $_SERVER['CONTENT_TYPE'] : null);

        if ($content_type === "application/json") {
            $body = file_get_contents("php://input");
            $body_params = json_decode($body, true);
            return empty($body_params) ? array() : $body_params;
        }
        return array();
    }

    private function requestHandler($request_data)
    {
        $this->checkUserLogged();
        $method = (isset($request_data['method']) ? $request_data['method'] : 'get');
        $host = (isset($request_data['host']) ? $request_data['host'] : '');
        $uri = (isset($request_data['uri']) ? $request_data['uri'] : '');
        $headers = (isset($request_data['headers']) ? $request_data['headers'] : array());
        $body = (isset($request_data['body']) ? $request_data['body'] : '');

        $response = \Httpful\Request::$method($host.$uri)
            ->addHeaders($headers)
            ->body($body);
        try {
            $response = $response->send();
        } catch (Exception $e) {
            $this->setResponseCode(500);
            return (object)array('code' => '500');
        }

        $this->setResponseCode($response->code);
        return $response;
    }

    private function getRequestData($variable = null)
    {
        $request = $_SERVER;

        if (!is_null($variable)) {
            $variable = strtoupper($variable);
            return isset($request[$variable]) ? $request[$variable] : null;
        }

        return $request;
    }

    private function setResponseCode($code)
    {
        $protocol = $this->getRequestData('server_protocol');
        header("{$protocol} {$code}");
    }

    public function api($conversation_id, $message_id)
    {
        $search_string = "/mc_widget/api/users/xuser/";
        $replace_string = '/api/users/' . $this->user_uuid . '/';
        $uri = str_replace($search_string, $replace_string, $this->getRequestData('request_uri'));
        $request_method = $this->getRequestData('request_method');

        $payload_params = $this->getPayloadParams();
        $message_text = (isset($payload_params['messageText']) ? $payload_params['messageText'] : '');
        $body = '';
        if (!empty($conversation_id) && !empty($message_id)) {
            $body = 'conversationID=' . $conversation_id . '&message_id=' . $message_id;
        } elseif (!empty($message_text)) {
            $body = 'message=' . urlencode($message_text);
        }

        $request = array(
            'method' => $request_method,
            'host' => $this->msg_center_host,
            'uri' => $uri,
            'body' => $body
        );
        $request['headers'] = $this->mc_utils->getSignatureHeaders($request);
        $response = $this->requestHandler($request);
        $regex = '/(20[0-9])/';
        $is_valid_response_code = preg_match($regex, $response->code);

        $retry_count = 0;

        while (!$is_valid_response_code && $retry_count++ < $this->max_retry) {
            switch ($response->code) {
                case '307':
                    $request['host'] = $response->headers['location'];
                    $request['uri'] = '';
                    header('Content-type: charset=UTF-8');
                    break;
                case '401':
                    bLogger::logInfo(__METHOD__, self::PREFIX.": The authentication header was missing or invalid.");
                    break;
                case '434':
                    bLogger::logInfo(__METHOD__, self::PREFIX.": Missing credential data.");
                    break;
                case '435':
                    bLogger::logInfo(__METHOD__, self::PREFIX.": Incorrect signature.");
                    break;
                case '437':
                    bLogger::logInfo(__METHOD__, self::PREFIX.": The x-scm-date header is missing or invalid.");
                    break;
                case '500':
                    bLogger::logInfo(__METHOD__, self::PREFIX.": Internal server error.");
                    break;
                default:
                    $request_string = var_export($request, true);
                    bLogger::logInfo(
                        __METHOD__,
                        self::PREFIX.": Unexpected error, return code: {$response->code}, request:\n{$request_string}"
                    );
            }
            $request['headers'] = $this->mc_utils->getSignatureHeaders($request);
            $response = $this->requestHandler($request);
            $is_valid_response_code = preg_match($regex, $response->code);
        }

        if ($is_valid_response_code) {
            echo $response;
        } else {
            bLogger::logInfo(__METHOD__, self::PREFIX.": Failed Response : ".print_r($response, true));
            $this->setResponseCode(400);
            return;
        }
    }

    private function getImgUrl($img_path, $category)
    {
        $image = '';
        if (!empty($img_path)) {
            $folder = substr(str_pad($img_path, 14, "0", STR_PAD_LEFT), 0, 2);
            $image = $this->img_host . '/thumbsli/' . $folder . '/' . $img_path;
        } else {
            $parent_category = bconf_get($BCONF, "*.cat.{$category}.parent");
            $image = $this->ads_host . bconf_get($BCONF, "*.cat.{$parent_category}.icon");
        }
        return $image;
    }

    public function get_ads_list_data($ids)
    {
        if ($ids == '') {
            $this->setResponseCode(400);
            bLogger::logError(__METHOD__, self::PREFIX.": Missing ad ids.");
            return;
        }

        $result_as = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "id:".$ids);
        $ads_array = array();
        $ads_response = array('status' => 'OK');
        if (!empty($result_as) && isset($result_as['list_id'])) {
            if (!is_array($result_as['list_id'])) {  // asearch returned a single result
                $list_ids = $result_as['list_id'];
                $subjects = $result_as['subject'];
                $ad = array(
                    'id' => $result_as['list_id'],
                    'subject' => $result_as['subject'],
                    'image' => $this->getImgUrl($result_as['image'], $result_as['category']),
                    'url' => $this->ads_host . '/vi/' . $result_as['list_id'] . '.htm',
                    'price' => !empty($result_as['price']) ? $result_as['price'] : '0',
                    'userId' => '',
                );
                array_push($ads_array, $ad);
            } else {
                for ($i = 0; $i < count($result_as['list_id']); $i++) {
                    $ad = array(
                        'id' => $result_as['list_id'][$i],
                        'subject' => $result_as['subject'][$i],
                        'image' => $this->getImgUrl($result_as['image'][$i], $result_as['category'][$i]),
                        'url' => $this->ads_host . '/vi/' . $result_as['list_id'][$i] . '.htm',
                        'price' => !empty($result_as['price']) ? $result_as['price'][$i] : '0',
                        'userId' => '',
                    );
                    array_push($ads_array, $ad);
                }
            }
        } else {
            bLogger::logError(__METHOD__, self::PREFIX.": No ads where found that match this ids: " . $ids);
        }
        $ads_response['ads'] = $ads_array;
        echo yapo_json_encode($ads_response);
    }
}
