<?php
/*****************************************************************************
//
// PHP Redis Client 
// Tightly modelled around existing memcachedclient for easy integration.
//
// TO-DO: Shouldn't we have a destructor calling disconnect?
//
//       XXX Consider a rewrite of redissessionclient...
//	   redissessionclient has grown ugly/confusing and uses:
//	   * a. session/rack/mc?1 in some functions and in others not
//	   * b. implicitly using redisstat if using _getCurrentSocket(NULL)
//
//         The {b,j}19 apaches dies in production if the connection to redis
//         times out. Take a look at the connection handling (+ TODO above?)
//
*****************************************************************************/
require_once("init.php");
require_once('binterfaces.php');
@define("WS_BUFFER", 1024);

class RedisSessionClient implements bsessionclient
{
	var $servers = array();
	var $sockets = array();
	var $debug = false;
	var $timeout = 3;
	var $values = array();
	var $id_separator;

	function RedisSessionClient($options=array())
	{
		$this->servers = $options['servers'];
		$this->debug = $options['debug'];
		$this->id_separator = $options['id_separator'];
		$this->timeout = $options['timeout'];
	}

	function host_available($host)
	{
		$socket = $this->_getCurrentSocket($host);
		return is_resource($socket);
	}

	/* @param $cmds Array of command and arguments.
	 * @return string Returns a command as a string.
	 */

	private function build_command($cmds) {
		$res = "";
		$len = count($cmds);
		$res .= '*' . $len . "\r\n";
		foreach($cmds as $cmd) {
			$res .= '$' . strlen($cmd) . "\r\n";
			$res .= "$cmd\r\n";
		}
		return $res;
	}

	function set($key, $val, $expire=0)
	{
		$raw = false;

		if(empty($expire)) {
			$raw = $this->build_command(array('SET', $key, $val));
		} else{
			$raw = $this->build_command(array('SETEX', $key, $expire, $val));
		}

		$sock = $this->_getCurrentSocket($key);

		if (!$this->_rawCmd($sock, $raw))
		{
			return false;
		}

		list($status, $msg) = $this->_handleResponse($sock);

		return $status;
	}

    function hset($hash, $key, $val, $expire=0)
    {
        $raw = $this->build_command(array('HSET', $hash, $key, $val));
        $sock = $this->_getCurrentSocket($hash);

        if (!$this->_rawCmd($sock, $raw))
        {
            return false;
        }

        list($status, $msg) = $this->_handleResponse($sock);

        if (!empty($expire)) {
             /* TODO error handling */
             $this->expire($hash, $expire);
        }

        return $status;
    }

    function expire($key, $expire = 0) {
        if (empty($expire))
            $expire = 0;
        $raw = $this->build_command(array('EXPIRE', $key, $expire));
        $sock = $this->_getCurrentSocket($key);


        if (!$this->_rawCmd($sock, $raw)) {
            return false;
        }

        list($status, $msg) = $this->_handleResponse($sock);

        return $status;
    }

	/*
	 * Set a value in redisstat. (See XXX in the header...)
	 */
	function set_redisstat($key, $val, $expire = 0)
	{
		$raw = false;

		if(empty($expire)) {
			$raw = $this->build_command(array('SET', $key, $val));
		} else{
			$raw = $this->build_command(array('SETEX', $key, $expire, $val));
		}

		$sock = $this->_getCurrentSocket(NULL);

		if (!$this->_rawCmd($sock, $raw))
		{
			return false;
		}

		list($status, $msg) = $this->_handleResponse($sock);

		return $status;
}

	function setkey($key, $val, $expire=0)
	{
		return $this->set($key, $val);
	}

	function exists($key) {
		$raw = $this->build_command(array('EXISTS', $key));

		$sock = $this->_getCurrentSocket($key);
		if (!$this->_rawCmd($sock, $raw))
		{
			return false;
		}

		list($status,$msg) = $this->_handleResponse($sock);

		return $msg > 0;
	}

	function add($key, $val, $expire=0)
	{
		if ($this->exists($key))
			return false;

		return $this->set($key, $val, $expire);
	}


	function delete($key, $nocache = false)
	{
		if (!isset($this->values[$key]) || $nocache)
		{
			$raw = $this->build_command(array('DEL', $key));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $raw))
			{
				return false;
			}

			list($status,$msg) = $this->_handleResponse($sock);

			if ($status && $msg == "1") {
				unset($this->values[$key]);
				return true;
			}
		}

		return false;
	}

	function get($key, $nocache = false)
	{
		if ($nocache || !isset($this->values[$key]))
		{
			$cmd = $this->build_command(array('GET', $key));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd)) {
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function hgetall($key, $nocache = false)
	{
		if ($nocache || !isset($this->values[$key]))
		{
			$cmd = $this->build_command(array('HGETALL', $key));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd)) {
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function hget($key, $field, $nocache = false)
	{
		if ($nocache || !isset($this->values[$key][$field]))
		{
			$field_length = strlen($field);
			$cmd = $this->build_command(array('HGET', $key, $field));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd)) {
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function hincrby($key, $field, $value, $nocache = false)
	{
		if ($nocache || !isset($this->values[$key][$field]))
		{
			$field_length = strlen($field);
			$cmd = $this->build_command(array('HINCRBY', $key, $field, $value));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd)) {
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function zscore($key, $member)
	{
		if (!isset($this->values[$key][$member]))
		{
			$cmd = $this->build_command(array('ZSCORE', $key, $member));

			$sock = $this->_getCurrentSocket(NULL);

			if (!$this->_rawCmd($sock, $cmd))
			{
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key][$member] = $msg;
		}
		return $this->values[$key][$member];
	}

	function zrevrange($key, $start_elem, $lim)
	{
		if (!isset($this->values[$key]))
		{
			$cmd = $this->build_command(array('ZREVRANGE', $key, $start_elem, $lim, 'WITHSCORES'));

			$sock = $this->_getCurrentSocket(NULL);
			if (!$this->_rawCmd($sock, $cmd))
			{
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function keys($key)
	{
		if (!isset($this->values[$key]))
		{
			$cmd = $this->build_command(array('KEYS', $key.'*'));

			$sock = $this->_getCurrentSocket($key);
			if (!$this->_rawCmd($sock, $cmd))
			{
				return false;
			}

			list($status, $msg) = $this->_handleResponse($sock, $key);

			if (!$status)
				return false;

			$this->values[$key] = $msg;
		}
		return $this->values[$key];
	}

	function build_and_send_command($cmd, $key_args)
	{
		$key = '';
		if(is_array($key_args)) {
			$key = $key_args[0];
		} else {
			$key = $key_args;
		}

		if (is_array($key_args)) {
			array_unshift($key_args, $cmd);
			$cmd = $this->build_command($key_args);
		} else {
			$cmd = $this->build_command(array($cmd, $key));
		}

		$sock = $this->_getCurrentSocket($key);
		if (!$this->_rawCmd($sock, $cmd))
		{
			return false;
		}

		list($status, $msg) = $this->_handleResponse($sock, $key);

		if (!$status)
			return false;

		return $msg;
	}

	function rpush($key, $value) {
		return $this->build_and_send_command('RPUSH', array($key, $value));
	}

	function lpush($key, $value) {
		return $this->build_and_send_command('LPUSH', array($key, $value));
	}

	function incr($key) {
		return $this->build_and_send_command('INCR', $key);
	}

	function llen($key) {
		return $this->build_and_send_command('LLEN', $key);
	}

	function rpop($key) {
		return $this->build_and_send_command('RPOP', $key);
	}

	function _clearArray($arr)
	{
		$ret = array();
		foreach ($arr as $val)
		{
			if (strlen($val) > 0) $ret[] = $val;
		}
		return $ret;
	}

	function _getCurrentSocket($session_key=NULL) {
		$index = 0;

		if ($session_key == NULL) {
			$index = $GLOBALS['BCONF']['*']['common']['redisstat']['master'];
		}
		elseif (count($this->servers) > 0) {
			$index = substr($session_key, 0, strpos($session_key, $this->id_separator));
			// If haven't found the separator, use the session_key as index
			if ($index == "")
				$index = $session_key;
		}

		if (!is_resource(@$this->sockets[$index])) {
			$sock = $this->_connect(@$this->servers[$index]['name'], @$this->servers[$index]['port'], $this->timeout);
			if (is_resource($sock)) {
				$this->sockets[$index] = $sock;
			}
		}

		if (is_array($this->sockets) && isset($this->sockets[$index])) {
			return $this->sockets[$index];
		}
		return false;
	}

	function _handleResponse($sock, $key = NULL) {
		if (!$sock)
			$sock = $this->_getCurrentSocket($key);
		$response = trim(fgets($sock)); // To find size
		if (true)
			bLogger::logDebug(__METHOD__, "Session RESPONSE: '".$response."'");
		if (empty($response))
			return false;
		$c = $response[0];
		$data = substr($response, 1);

		switch($c) {
			case '-':
				return array(false, $data);
			case ':':
			case '+':
				return array(true, $data);
			case '$': {
				if ($data == "-1")
					return array(false, NULL);
				$response = '';
				$left = (int)$data;
				while ($left > 0) {
					$data = fread($sock, $left);
					$left -= strlen($data);
					$response .= $data;
				}
				$crlf = fread($sock, 2);
				return array(true, $response);
			}
			case '*':
				if ($data == "-1")
					return array(false, NULL);
				$rows_left = (int)$data * 2;

				$arData = Array();
				$i = 0;
				if ($key == NULL) {
					while ($rows_left > 0) {
						$rows_left = $rows_left - 4;
						$data = trim(fgets($sock));
						$data = trim(fgets($sock));
						$list_id = $data;		
						$data = trim(fgets($sock));
						$data = trim(fgets($sock));
						$arData[$list_id] = $data;
						$i++;
					}
				} else {
					while ($rows_left > 0) {
						$rows_left = $rows_left - 2;
						$data = trim(fgets($sock));
						$data = trim(fgets($sock));
						array_push($arData, $data);
						$i++;
					}
				}
				return array(true, $arData);
		}
		return array(false, "Unknown error");
	}

	function _sendCmd($cmd, $key, $val = NULL, $expire = 0)
	{
		// NOP in redis client
	}

	function _rawCmd(&$sock, $cmd)
	{
		bLogger::logDebug(__METHOD__, "Session COMMAND: '".$cmd."'");
		if (!is_resource($sock)) return false;

		$len = strlen($cmd);
		$offset = 0;
		while ($res = fwrite($sock, substr($cmd, $offset, WS_BUFFER), WS_BUFFER))
		{
			if ($res == 0) break;
			$offset += $res;
		}
		if ($offset < $len)
		{
			$this->error = "Failed to send raw command";
			return false;
		}
		return true;
	}

	function _connect($host, $port, $timeout)
	{
		bLogger::logDebug(__METHOD__, "Session connecting to $host:$port");

		if(($sock = @fsockopen($host, $port, $errno, $errstr, $timeout)) === false)
		{
			$this->error = "Couldn't connect to server: $host:$port";
			bLogger::logDebug(__METHOD__, $this->error);
			return false;
		}
		if($sock)
			stream_set_timeout($sock, $timeout);
		bLogger::logDebug(__METHOD__, "Session connection to $host:$port established.");
		return $sock;
	}

	function disconnect()
	{
		bLogger::logDebug(__METHOD__, "Session disconnecting");

		foreach ($this->sockets as $id => $sock)
		{
			fclose($sock);
			$this->sockets[$id] = NULL;
		}
	}

	function disconnect_all()
	{
		$this->disconnect();
	}

	//TODO This apparently isn't used anywhere, check and delete
	function get_fallback_host()
	{
		foreach($this->servers as $prefix => $host) {
			bLogger::logDebug(__METHOD__, "Considering session host $prefix (".$host['name'].':'.$host['port'].") as fallback candidate.");
			if ($this->host_available($prefix)) {
				if ($this->debug)
					return $prefix;
			}
		}
		return NULL;
	}

	function validate_session_id($id)
	{
		// Check if id belongs to a valid host
		$host = substr($id, 0, strpos($id, $this->id_separator));
		if (@!array_key_exists($host, $this->servers))
			return false;
		return true;
	}

	function ping($session = NULL) {
		$cmd = $this->build_command(array('PING'));
		$sock = $this->_getCurrentSocket($session);

		if (!$this->_rawCmd($sock, $cmd)) {
			$log_msg = "Redis server connection fail.";
			$log_msg .= ($session != NULL)?" Session: $session":'';

			bLogger::logDebug(__METHOD__, $log_msg);
			return false;
		}

		list($status, $msg) = $this->_handleResponse($sock);

		if (!$status || $msg != 'PONG') {
			$log_msg = "Ping to redis server fail.";
			$log_msg .= ($session != NULL)?" Session: $session":'';
			bLogger::logDebug(__METHOD__, $log_msg);

			return false;
		}

		return true;
	}
}
