<?php
namespace Yapo;

use Yapo\Logger;

class YapoRequest
{
    private $account_session;

    public function __construct(\AccountSession $account_session)
    {
        $this->account_session = $account_session;
    }

    private function checkUserLogged()
    {
        if (!$this->account_session->is_logged()) {
            $login = bconf_get($BCONF, '*.common.base_url.secure')."/login";
            header('Location: ' .$login);
            exit(0);
        }
    }

    private function send($service_conf, $action, $data)
    {
        $request_data = $this->parseConfiguration($service_conf, $action, $data);
        $method = (isset($request_data['method']) ? $request_data['method'] : 'get');
        $host = (isset($request_data['host']) ? $request_data['host'] : '');
        $uri = (isset($request_data['uri']) ? $request_data['uri'] : '');
        $headers = (isset($request_data['headers']) ? $request_data['headers'] : array());
        $body = (isset($request_data['body']) ? $request_data['body'] : '');
        $timeout = (!empty($request_data['timeout']) ? intval($request_data['timeout']) : 0);
        $response = \Httpful\Request::$method($host.$uri)
            ->addHeaders($headers)
            ->body($body);
        try {
            $response = $response->timeout($timeout)->send();
        } catch (\Httpful\Exception\ConnectionErrorException $ex) {
            Logger::logError(__METHOD__, "exception: ".var_export($ex, true));
            return (object)array('code' => '500');
        } catch (Exception $e) {
            Logger::logError(__METHOD__, "exception: ".var_export($e, true));
            return (object)array('code' => '500');
        }
        return $response;
    }

    public function parseConfiguration($service_conf, $action, $data)
    {
        $host = $service_conf["host"].':'.$service_conf["port"];
        $action_params = $service_conf[$action];
        $timeout = isset($service_conf["timeout"]) ? $service_conf["timeout"] : 0;
        $uri = $action_params["path"];
        $method = $action_params["method"];
        if (array_key_exists("params", $action_params)) {
            $params = $action_params["params"];
            $path_params = array();
            $query_params = array();
            foreach ($params as $key => $value) {
                if (array_key_exists("type", $value) && !strcmp($value["type"], "path")
                    && array_key_exists($key, $data)) {
                    $path_params[$key] = $data[$key];
                    unset($data[$key]);
                }
                if (array_key_exists("type", $value) && !strcmp($value["type"], "query")
                    && array_key_exists($key, $data)) {
                    $query_params[$key] = $data[$key];
                    unset($data[$key]);
                }
            }
            if (sizeof($path_params) > 0) {
                $uri_params = explode('/', $uri);
                foreach ($uri_params as &$param) {
                    if (!empty($path_params[$key])) {
                        $param = $path_params[$key];
                    }
                }
                $uri = join('/', $uri_params);
            }
            if (sizeof($query_params) > 0) {
                $uri = $uri ."?". http_build_query($query_params);
            }
        }
        $request_data = array(
            "method"  => $method,
            "host"    => $host,
            "uri"     => $uri,
            "headers" => array("Accept" => "application/json"),
            "body"    => !empty($data) && strtolower($method) != "get" ? yapo_json_encode($data): null,
            "timeout" => $timeout
        );
        return $request_data;
    }

    public function sendRequest($service_name, $action, $data)
    {
        global $BCONF;
        $service_conf = bconf_get($BCONF, "*.".$service_name);
        $response = $this->send($service_conf, $action, $data);
        return $response;
    }
}
