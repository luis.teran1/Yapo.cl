<?php

namespace Yapo;

use Yapo\interfaces\PartnerService;

class BifrostService implements PartnerService
{
    private $Path;
    private $Host;
    private $Port;
    private $Proxy;

    public function __construct($pc)
    {
        global $BCONF;
        $this->Path = Bconf::get($BCONF, "*.bifrost.partner.path");
        $this->Host = Bconf::get($BCONF, "*.bifrost.host");
        $this->Port = Bconf::get($BCONF, "*.bifrost.port");
        $this->ListPath = Bconf::get($BCONF, "*.bifrost.partner.list.path");
        $this->ConvertPath = Bconf::get($BCONF, "*.bifrost.partner.convert.path");
        $this->Proxy = $pc;
    }

    /*
     * Creates a partner in Bifrost service
     *
     * @param array $data Contains required data to create partner
     */
    public function createPartner($data)
    {
        $response = $this->Proxy->doRequest('post', $this->Host, $this->Port, $this->Path, $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code == 500) {
            throw new \Exception('BIFROST_UNAVAILABLE');
        } elseif ($code != 200) {
            Logger::logError(__METHOD__, "Bifrost Error:". var_export($response, true));
            $param = isset($response->body->ErrorParam) ? (" " . $response->body->ErrorParam) : "";
            $message = isset($response->body->ErrorMessage) ? (" " . $response->body->ErrorMessage) : $response->body->error;
            throw new \Exception($message. $param);
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    /*
     * Updates partner configuration
     *
     * @todo add proper implementation (Next sprint)
     */
    public function updatePartner($update)
    {
        $response = $this->Proxy->doRequest('put', $this->Host, $this->Port, $this->Path, $update);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code == 500) {
            throw new \Exception('BIFROST_UNAVAILABLE');
        } elseif ($code != 200) {
            Logger::logError(__METHOD__, "Bifrost Error:". var_export($response, true));
            $param = isset($response->body->ErrorParam) ? (" " . $response->body->ErrorParam) : "";
            $message = isset($response->body->ErrorMessage) ? (" " . $response->body->ErrorMessage) : $response->body->error;
            throw new \Exception($message. $param);
        }

        $json_response = json_decode($response);
        return $json_response;
    }

    public function listPartners()
    {
        $response = $this->Proxy->doRequest('get', $this->Host, $this->Port, $this->ListPath);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));

        if ($code == 500) {
            throw new \Exception('BIFROST_UNAVAILABLE');
        } elseif ($code != 200) {
            Logger::logError(__METHOD__, "Bifrost Error:". var_export($response, true));
            $param = isset($response->body->ErrorParam) ? (" " . $response->body->ErrorParam) : "";
            throw new \Exception($response->body->ErrorMessage . $param);
        }
        $json_response =json_decode($response, true);
        // decode the UTF-8 response fields to ISO-8859-1
        array_walk_recursive($json_response, function (&$item, $key) {
            $item = utf8_decode($item);
        });
        $partner_list = array();
        foreach ($json_response as $status => $partners) {
            foreach ($partners as $key => $partner) {
                $partners[$key]["Active"] = ($status == "Active") ? "true" : "false";
                // encode the double quotes in html entities
                $partners[$key]["Rules"] = htmlspecialchars($partners[$key]["Rules"], ENT_COMPAT, 'ISO-8859-1', false);
                $partners[$key]["IRules"] = htmlspecialchars($partners[$key]["IRules"], ENT_COMPAT, 'ISO-8859-1', false);
                $partners[$key]["ConnData"] = htmlspecialchars($partners[$key]["ConnData"], ENT_COMPAT, 'ISO-8859-1', false);
                $partners[$key]["ImgConnData"] = htmlspecialchars($partners[$key]["ImgConnData"], ENT_COMPAT, 'ISO-8859-1', false);
                $partners[$key]["CsvConnData"] = htmlspecialchars($partners[$key]["CsvConnData"], ENT_COMPAT, 'ISO-8859-1', false);
                $partners[$key]["TdConnData"] = htmlspecialchars($partners[$key]["TdConnData"], ENT_COMPAT, 'ISO-8859-1', false);
            }
            //Sort the partners on each status alphabetically
            usort($partners, function ($a, $b) {
                return strcasecmp($a['KeyName'], $b['KeyName']);
            });
            $partner_list[$status] = $partners;
        }
        return  $partner_list;
    }

    public function buildCURLHandle($file, $separator, $encoding)
    {
        Logger::logDebug(__METHOD__, $this->Host.":".$this->Port.$this->ConvertPath);
        $filename = $file['name'];
        $filedata = $file['tmp_name'];
        $filesize = $file['size'];
        $headers = array("Content-Type:multipart/form-data");
        $postfields = array(
            "UploadFile" => "@$filedata".";filename=$filename"."",
            "Separator" => $separator,
            "Encoding" => $encoding
        );
        $ch = curl_init();
        $options = array(
            CURLOPT_URL => $this->Host.":".$this->Port.$this->ConvertPath,
            CURLOPT_HEADER => 0,
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_INFILESIZE => $filesize,
            CURLOPT_RETURNTRANSFER => true
        );
        curl_setopt_array($ch, $options);
        return $ch;
    }

    /*
     * Call bifrost with the file, separator and encoding setted in the form
     *
     * @param file the _FILE object of the form field
     * @param separator the separator field from the form
     * @param encoding the encoding field from the form
     *
     * @returns An array (
     *   "ok"		=> true if the operations was succesfull
     *   "data"		=> the file data,
     *   "filename"	=> the file name for the download
     * )
     */
    public function convertFile($file, $separator, $encoding)
    {
        $ch = $this->buildCURLHandle($file, $separator, $encoding);
        $data = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return array(
            "ok" => $http_code == "200",
            "data" => $data,
            "filename" => $file['name']
        );
    }
}
