<?php

require_once('bTransaction.php');

class AccountUtil {

	static function is_pro_for_cat($is_pro_for, $cat) {
		return in_array($cat, explode(',', $is_pro_for));
	}

	function account_mail ($email, $name, $mail_type = 'account_confirmation'){

		$trans = new bTransaction();
		$trans->add_data("email", $email);
		$trans->add_data("nickname", $name);
		$trans->add_data("mail_type", $mail_type);
		$reply = $trans->send_command('accountmail');

		return $reply;
	}
	/**
	* Edit account data.
	*/
	function edit_account_by_param($email, $data_to_update, $account_session=null){
		if (!($account_session instanceof AccountSession)) {
			$account_session = new AccountSession();
		}

		$account_session_email = @$account_session->get_param('email');

		if($account_session->is_logged() && (isset($account_session_email) && ($account_session_email === $email))){
			$trans = new bTransaction();

			$trans->add_data('email', $account_session_email);

			foreach($data_to_update as $key => $value) {
				$trans->add_data($key, $value);
			}

			$reply_edit_account = $trans->send_command('edit_account_by_param');

			if ($trans->has_error(true) || $reply_edit_account['status'] != "TRANS_OK" ){
				return false;
			}
		}
		return true;
	}
	/**
	* Edit account data, only if are empty.
	*/
	function edit_account_data_if_empty($email, $data_to_update, $account_session){
		$data = array();

		foreach($data_to_update as $key => $value) {
			$param_account_value = @$account_session->get_param($key);
			if(empty($param_account_value) && !empty($value)){
				$data[$key] = $value;
			}
		}

		if(!$this->edit_account_by_param($email, $data, $account_session)){
			return false;
		}

		return true;
	}
}
