<?php
namespace Yapo;

class DuplicatedAdDesktop extends DuplicatedAd
{

    public function check($array_data)
    {
        $is_duplicated = parent::check($array_data);
        if ($is_duplicated['has_duplicates'] == "1") {
            // because templates love data structures ':)
            foreach ($is_duplicated['products'] as $product => $data) {
                foreach ($data as $key => $value) {
                    $is_duplicated["products_${product}_${key}"] = $value;
                }
            }
            unset($is_duplicated['products']);
            return $is_duplicated;
        }
        return;
    }
}
