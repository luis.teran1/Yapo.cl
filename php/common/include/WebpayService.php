<?php
namespace Yapo;


class WebpayService extends MicroserviceConnector
{
    public function __construct()
    {
        $account_session = new \AccountSession();
        $yapo_request = new YapoRequest($account_session);
        parent::__construct("webpay_service", $yapo_request);
    }

    public function responseHandler($response) {
        if ($response->code == 500) {
            return null;
        }
        return $response;
    }

    public function errorHandler($response = array()) {
        return json_decode($response, true);
    }
}
