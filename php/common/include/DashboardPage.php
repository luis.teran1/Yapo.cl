<?php
namespace Yapo;

class DashboardPage
{
    private $response;
    private $trans;
    private $acc_session;
    private $account_id;
    private $content = "accounts/dashboard.html";
    private $wrapper = "mobile_select";
    private $search = '';
    private $sort = '';
    private $page = 1;
    private $only_data = 0;

    public function __construct($trans, $response, $acc_session)
    {
        $this->response = $response;
        $this->trans = $trans;
        $this->acc_session = $acc_session;
    }

    private function receiveGetParams()
    {
        $params = array(
            "od" => array("only_data", "int"),
            "search" => array("search", "string"),
            "s" => array("sort", "string"),
            "page" => array("page", "string")
        );

        foreach ($params as $key => $value) {
            if (isset($_GET[$key])) {
                $this->$value[0] = sanitizeVar($_GET[$key], $value[1]);
            }
        }

        if ($this->only_data == 1) {
            $this->wrapper = "empty";
            $this->content = "mobile/accounts/include/dashboard_ads.html";
        }
    }

    public function showPage()
    {
        global $BCONF;
        if (!$this->acc_session->is_logged()) {
            $url_base = Bconf::get($BCONF, "*.common.base_url.secure");
            header("Location:  {$url_base}/login");
            return false;
        }

        $this->account_id = $this->acc_session->get_param("account_id");
        $this->receiveGetParams();
        $this->addPageContent();

        $acc_ads = $this->getAccountAds(
            $this->account_id,
            $this->page - 1,
            $this->search,
            $this->sort
        );

        $this->populateAds($acc_ads);
        $this->response->show_html_results($this->wrapper);
        return $acc_ads;
    }

    private function addPageContent()
    {

        global $BCONF;
        // Adding Css/Js/Titles
        $this->response->add_data('headstyles', Bconf::get($BCONF, '*.common.dashboard.headstyles'));
        $this->response->add_data('headscripts', Bconf::get($BCONF, '*.common.dashboard.headscripts'));
        $this->response->add_data('page_title', Bconf::lang($BCONF, 'PAGE_ACCOUNT_DASHBOARD_TITLE'));
        $this->response->add_data('page_name', Bconf::lang($BCONF, 'PAGE_ACCOUNT_DASHBOARD_NAME'));
        $this->response->add_data('page', $this->page);
        $this->response->add_data('content', $this->content);
        $this->response->add_data('appl', "dashboard");
        $this->response->add_data('mobile_appl', "dashboard");
        $this->response->add_data('REQUEST_URI', $_SERVER['REQUEST_URI']);
        $this->response->add_data("REMOTE_BROWSER", $_SERVER['HTTP_USER_AGENT']);
        $this->response->add_data("REMOTE_ADDR", $_SERVER['REMOTE_ADDR']);

        if (!empty($this->search)) {
            $this->response->add_data('search', $this->search);
        }

        if (!empty($this->sort)) {
            $this->response->add_data('sort', $this->sort);
        }
    }

    private function populateAds($acc_ads)
    {
        unset($acc_ads['status']);
        foreach ($acc_ads as $key_ad => $val_ad) {
            if (!is_array($val_ad)) {
                $this->response->fill_array("ads_{$key_ad}", $val_ad);
            } else {
                foreach ($val_ad as $data) {
                    $this->response->fill_array("ads_{$key_ad}", $data);
                }
            }
        }

        //Define last page used by mobile on jscroll
        if ($acc_ads['n_ads'] <= (Bconf::get($BCONF, "*.accounts.dashboard.ads_per_page")  * $this->page )) {
            $this->response->add_data('last_page', 1);
        }
    }

    private function getAccountAds($account_id, $page_number = 0, $query = '', $sort = '')
    {
        $query  = strtolower($query);
        $this->trans = $this->trans->reset();
        $this->trans->add_data('account_id', $account_id);
        $this->trans->add_data('page_number', $page_number);
        $this->trans->add_data('all_ad_params', 1);
        if (!empty($query)) {
            $this->trans->add_data('query', $query);
        }
        if (in_array($sort, array("0","1"))) {
            $this->trans->add_data('pack_sort', $sort);
        }
        $transaction = Bconf::get($BCONF, "*.accounts.dashboard.transaction");
        $reply = $this->trans->send_command($transaction);

        if (!$this->trans->has_error(true)) {
            return $reply;
        }

        return array();
    }
}
