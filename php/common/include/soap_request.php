<?php

function is_seq_array($php_val) {
	return is_array($php_val) && ($php_val === array_values($php_val));
}

function object_to_soap($obj, $level = 0, $key_prefix = "") {
	if (is_null($obj))
		return '';
	if (is_object($obj))
		$obj = get_object_vars($obj);
	if (!is_array($obj) || empty($obj))
		return '';

	$output = '';


	$tabs = str_pad("", $level, "\t");

	foreach ($obj as $key => $value) {
		if (is_object($value))
			$value = get_object_vars($value);
		if (is_seq_array($obj))
			$key = $key_prefix . $key;

		$output .= "{$tabs}<{$key}>\n";
		if (is_array($value)) {
			$output .=  object_to_soap($value, $level + 1, $key);
		} else {
			$output .=  "{$tabs}\t{$value}\n";
		}
		$output .=  "{$tabs}</{$key}>\n";
	}

	return $output;
}

class soap_request {
	private $url;
	private $method;
	private $parameters;
	private $ns = "http://www.yapo.cl/ws";
	private $envelope;
	private $headers;

	function __construct ($url = NULL, $method = NULL, $parameters = NULL, $envelope = NULL, $headers = NULL) {
		$this->parameters = array();

		$this->set_url($url);
		$this->set_method($method);
		$this->set_parameters($parameters);
		$this->set_envelope($envelope);
		$this->set_headers($headers);
	}

	function set_envelope($envelope) {
		if(!is_null($envelope) && is_array($envelope)) {
			$this->envelope = $envelope;
		} else {
			$this->envelope = array(
				"xmlns:SOAP-ENV" => "http://www.w3.org/2003/05/soap-envelope",
				"xmlns" => $this->ns,
				"SOAP-ENV:encodingStyle" => "http://www.w3.org/2003/05/soap-encoding"
			);
		}
	}

	function set_url($url) {
		if (!is_null($url))
			$this->url = $url;
	}

	function set_method($method) {
		if (!is_null($method))
			$this->method = $method;
	}

	function set_parameters($parameters) {
		if (!is_null($parameters)) {
			$this->parameters = array();
			foreach ($parameters as $k => $val) {
				if (is_array ($val)) {
					foreach ($val as $v)
						$this->add_parameter($k, $v);
				} else
					$this->add_parameter ($k, $val);
			}
		}
	}

	function set_headers($headers) {
		if (!is_null($headers))
			$this->headers = $headers;
	}

	function add_parameter($key, $value, $reset = false) {
		if (!isset($this->parameters[$key]) || $reset) {
			$this->parameters[$key] = htmlspecialchars($value);
		} else {
			if (!is_array($this->parameters[$key]))
				$this->parameters[$key] = array ($this->parameters[$key]);
			$this->parameters[$key][] = htmlspecialchars($value);
		}
		return true;
	}

	function is_ready() {
		return isset($this->url) && isset($this->method);
	}

	function get_request() {
		$request = "";

		$request .= "<SOAP-ENV:Envelope";
		foreach ($this->envelope as $key => $value) {
			$request .= "\n".$key.'="'.$value.'"';
		}
		$request .= ">\n\n";
		$request .= "<SOAP-ENV:Header>\n";
		$request .= object_to_soap($this->headers);
		$request .= "</SOAP-ENV:Header>\n";
		$request .= "<SOAP-ENV:Body>\n";
		$request .= object_to_soap(array($this->method => $this->parameters));
		$request .= "</SOAP-ENV:Body>\n";
		$request .= "</SOAP-ENV:Envelope>\n";
		return $request;
	}

	function send_request() {
		if (!$this->is_ready())
			return false;

		$ch = curl_init($this->url);
		curl_setopt($ch, CURLOPT_USERAGENT, "Blocket SOAP test-client");
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HEADER, false);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: text/xml" /*; charset=utf-8"*/, "SOAPAction: ".$this->ns.$this->method));

		curl_setopt($ch, CURLOPT_POSTFIELDS, $this->get_request());

		$result = curl_exec($ch);

		$c_err = curl_error($ch);
		if (!empty($c_err)) {
			print "ERROR: Curl post failed (".$c_err.")\n";
			return "";
		}
		curl_close($ch);

		// XXX: convert soap to array/object data
		return $result;
	}
}
//$soap = new soap_request('http://192.168.4.32:21604/importads/', 'alert', array('test'=> array(1, 2, 3), 'kaka' => 'bulle', 'oscar' => array('image1' => '2312432.jpg', 'jdfjd' => '334.jpg')));
//echo $soap->send_request();
?>
