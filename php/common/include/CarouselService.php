<?php
namespace Yapo;

use ProxyClient;

class CarouselService
{
    private $healthcheck;
    private $Host;
    private $Port;
    private $Proxy;
    public function __construct()
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.carousel.host");
        $this->Port = Bconf::get($BCONF, "*.carousel.port");
        $this->Path = Bconf::get($BCONF, "*.carousel.healthcheck.path");
        $this->AssignCarousel = Bconf::get($BCONF, "*.carousel.assign");
        $this->EditCarousel = Bconf::get($BCONF, "*.carousel.edit");
        $this->FetchCarousel = Bconf::get($BCONF, "*.carousel.fetch");
        $this->ActivateCarousel = Bconf::get($BCONF, "*.carousel.activate");
        $this->CarouselReport = Bconf::get($BCONF, "*.carousel.report");
        $this->Proxy = new ProxyClient();
    }

    public function checkService()
    {
        $response = $this->Proxy->doRequest("get", $this->Host, $this->Port, $this->Path, null);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function assignCarousel(Array $data)
    {
        $method=$this->AssignCarousel["method"];
	$path=$this->AssignCarousel["path"];
	if (array_key_exists ("ID", $data) && !empty($data['ID'])) {
            $method=$this->EditCarousel["method"];
            $path=str_replace('%product_id', $data["ID"], $this->EditCarousel["path"]);
	}
	$response = $this->Proxy->doRequest($method, $this->Host, $this->Port, $path, $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        // The response might be emty, in which case, we return an empty array
        if ($json_response == null)
            return (object)array();
        return $json_response;
    }

    public function fetchAssigns(array $data)
    {
	$full_path=$this->FetchCarousel["path"];
	$uri_params=http_build_query($data);
	if ($uri_params!= "") {
	    $full_path=$full_path."?".$uri_params;
	}
	$response = $this->Proxy->doRequest($this->FetchCarousel["method"], $this->Host, $this->Port, $full_path);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        if ($json_response == null)
        return (object)array();
        return $json_response;
    }

    public function activateAssign($product_id)
    {
        $data["status"] = "ACTIVE";
        $parsed_path = str_replace('%product_id', $product_id, $this->ActivateCarousel["path"]);
        $response = $this->Proxy->doRequest($this->ActivateCarousel["method"], $this->Host, $this->Port, $parsed_path, $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200 && $code != 204) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        if ($json_response == null)
            return (object)array();
        return $json_response;
    }

    public function deactivateAssign($product_id)
    {
        $data["status"] = "INACTIVE";
        $parsed_path = str_replace('%product_id', $product_id, $this->ActivateCarousel["path"]);
        $response = $this->Proxy->doRequest($this->ActivateCarousel["method"], $this->Host, $this->Port, $parsed_path, $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200 && $code != 204) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        if ($json_response == null)
            return (object)array();
        return $json_response;
    }

    public function generateReport($start_date, $end_date)
    {
	$parsed_path = str_replace('%start_date', $start_date, $this->CarouselReport["path"]);
	$parsed_path = str_replace('%end_date', $end_date, $parsed_path);
	$response = $this->Proxy->doRequest($this->CarouselReport["method"], $this->Host, $this->Port, $parsed_path);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Carousel Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        if ($json_response == null)
        return (object)array();
        return $json_response;
    }
}
