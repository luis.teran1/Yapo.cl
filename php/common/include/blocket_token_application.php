<?php

class blocket_token_application extends blocket_application {
	var $token;

	function blocket_token_application() {
	}

	function auth_type() {
		return str_replace('blocket_', '', class_name($this));
	}

	function validate_state() {
		return $this->is_active(true);
	}

	function deauthenticate() {
		$client = memcached_client();
		$client->delete(session_id() . '_token_' . $this->auth_type());
	}

	function is_active($authenticate) {
		$client = memcached_client();
		$token = $client->get(session_id() . '_token_' . $this->auth_type());

		if (!$token) {
			if ($authenticate)
				return false;
			else
				return true;
		}
		if (!$authenticate)
			return true;

		$transaction = new bTransaction();
		$transaction->auth_type = $this->auth_type();
		$transaction->add_data('log_string', log_string());
		$reply = $transaction->send_admin_command('validate_token', false);
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
		  	$this->deauthenticate();
			return false;
		}
		return true;
	}
}
