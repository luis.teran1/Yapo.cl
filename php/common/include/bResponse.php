<?php

/*
 * Class for basic response handling
 */
class bResponse extends bStandardClass
{
        /* Contains all data used in the template */
        var $data;
	var $extended_array;
        var $bconf_application;

        function bResponse() {
		global $session;

		if (isset($_GET['ca'])) {
			/* Extract region, city and type */
			list ($ca_region, $ca_city, $ca_type) = explode_ca($_GET['ca']);
			
			/* Generate the caller from given data */
			$ca = implode_ca($ca_region, $ca_city, $ca_type);
			
			/* Add caller to response */
			$this->add_data('ca_region', $ca_region);
			$this->add_data('ca_city', $ca_city);
			$this->add_data('ca_type', $ca_type);
			$this->add_data('ca', $ca);
		}

		/* Layout */
		if (isset($_GET['l'])) {
			if ($_GET['l'] > 0)
				$this->add_data("layout", $_GET['l']);
			$this->add_data("l", $_GET['l']);
		}

		/* All post and get data, automatically to templates */
		foreach ($_POST as $key => $value) {
			$this->add_data('post_'.$key, $value);
		}
		foreach ($_GET as $key => $value)
			$this->add_data('get_'.$key, $value);

		/*Is user logged in*/
		if (isset($_COOKIE['sli']) && $_COOKIE['sli'] == '1') {
			if (!$session)
				$session = new bSession();

			if (isset($_SESSION['global']['store_id'])) {
				$this->data['session_store_id'] = $_SESSION['global']['store_id'];
				$this->data['session_store_username'] = $_SESSION['global']['store_username'];
				$this->data['session_store_name'] = $_SESSION['global']['store_name'];
				if (isset($_SESSION['global']['store_noads']))
					$this->data['session_store_noads'] = $_SESSION['global']['store_noads'];
			}
		}

		/* New extended array */
		$this->extended_array = array();
        }

	/* Adds array to the extended array */
	function add_extended_array($name = NULL, $arr = NULL) {
		if (is_array($name))
			$this->extended_array += $name;
		else
			$this->extended_array += array($name => $arr);
	}

	function reset_extended_array() {
		$this->extended_array = array();
	}
        
        /* Extra bconf variables */
        function set_bconf_application($appl) {
                $this->bconf_application = $appl;
        }
        
	/* Renders the response data with given template */
        function call_template($template, $buffer = false) {
		/* Query string */
		if (isset($_SERVER['QUERY_STRING']))
			$this->add_data("query_string", $_SERVER['QUERY_STRING']);

		if ($buffer) {
			ob_start();
		}
                
                $bconf_data = $GLOBALS['BCONF']['*'];
                if ($this->bconf_application && isset($GLOBALS['BCONF'][$this->bconf_application]))
                        $bconf_data += $GLOBALS['BCONF'][$this->bconf_application];
                call_template($this->data, $this->extended_array, $bconf_data, $template, default_template_options());
		if ($buffer) {
			$buffer = ob_get_contents();
			ob_clean();
			return $buffer;
		}
        }

	/* Render the html page from template type */
	function show_html_results( $wrapper = null ) {
		if(is_null($wrapper))
			$this->call_template("common/general" . $GLOBALS['BCONF']['*']['common']['layout'][$_GET['l']]['template'] . ".html");
	 	else
			$this->call_template("common/".$wrapper.".html");
	}
        
	/* Redirect the request */
        function redirect($url) {
                header("Location: $url");
		exit(0);
        }

	/* Add data with possibility to html encode the message */
	function add_data($name, $data, $html_encode = false, $reset = false) {
		if ($html_encode)
			array_walk_recursive ($data, create_function('&$v, $k', '$v = htmlentities($v, ENT_COMPAT, $GLOBALS[BCONF][\'*\'][\'common\'][\'charset\']);'));

		if ($reset)
			unset($this->data[$name]);
		$this->data[$name] = $data;
	}

	/* Translate an errorcode to a message and ads it to response */
	function add_error($key, $code, $message = "") { 
		$error_message = lang($code);
		$error_message = preg_replace("/#caller/", @$this->data['ca'], $error_message);

		if (strlen(rtrim(trim($message)))) {
			$error_message .= " \"{$message}\" ";
		}

		$this->add_data($key, $error_message);
		$this->_error($code);
	}

	/* Ads an element to a given array in the response data */
	function fill_array($array_name, $data_key, $data = NULL, $reset = false) {
		if ($reset)
			unset($this->data[$array_name]);

		if (is_null($data))
			$this->data[$array_name][] = $data_key;
		else
			$this->data[$array_name][$data_key] = $data;
	}

	/* Merges an array data into response block, with possibility to encode */
	function merge_array($array, $html_encode = false) {
		foreach ($array as $key => $value)
                        if (isset($value))
                                $this->add_data($key, $value, $html_encode);
	}

	/* Add category specific data */
        function add_category_data($category = NULL, $adtype = NULL) { 
		/* Default types */
		foreach($GLOBALS['BCONF']['*']['common']['type']['default'] as $id => $type)
			$type_list[$id] = $type;


		if ($category != NULL) {
			$cat = $GLOBALS['BCONF']['*']['common']['category'][$category];

			/* Specific ad type for this category */
			if (isset($cat['type'])) {
				$type_list = explode(',', $cat['type']);
			}

			/* Show features */
			foreach(explode(',', $cat['features']) as $key)
				$this->data[$key.'_visible'] = 1;

			if ($adtype != NULL) {
				$features = @$cat[$adtype]['features'];
			
				if ($features) {	
					foreach(explode(',', $features) as $key)
						$this->data[$key.'_visible'] = 1;
				}
			}
		}

		$this->data['type_list'] = $type_list;
	}

	function reset() {
		$this->data = array();
	}	

	function get_errors() {
		$result = array();
		foreach ($this->data as $key => $value) {
			if (preg_match("/^err_/", $key))
				$result[$key] = $value;
		}
		return $result;
	}
}

