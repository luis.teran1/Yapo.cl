<?php
namespace Yapo;

class PdfManager extends \FPDF
{
    public $B;
    public $I;
    public $U;
    public $HREF;

    public function __construct($orientation = 'P', $unit = 'mm', $format = 'A4')
    {
        //Call parent constructor
        parent::__construct($orientation, $unit, $format);
        //Initialization
        $this->B=0;
        $this->I=0;
        $this->U=0;
        $this->HREF='';
    }

    public function writeHTML($html)
    {
        //HTML parser
        // $html=str_replace("\n",' ',$html);
        $a=preg_split('/<(.*)>/U', $html, -1, PREG_SPLIT_DELIM_CAPTURE);
        foreach ($a as $i => $e) {
            if ($i%2==0) {
                //Text
                if ($this->HREF) {
                    $this->putLink($this->HREF, $e);
                } else {
                    $this->Write(5, $e);
                }
            } else {
                //Tag
                if ($e{0}=='/') {
                    $this->closeTag(strtoupper(substr($e, 1)));
                } else {
                    //Extract attributes
                    $a2=explode(' ', $e);
                    $tag=strtoupper(array_shift($a2));
                    $attr=array();
                    foreach ($a2 as $v) {
                        if (preg_match('/^([^=]*)=["\']?([^"\']*)["\']?$/', $v, $a3)) {
                            $attr[strtoupper($a3[1])]=$a3[2];
                        }
                    }
                    $this->openTag($tag, $attr);
                }
            }
        }
    }

    public function openTag($tag, $attr)
    {
        //Opening tag
        if ($tag=='B' or $tag=='I' or $tag=='U') {
            $this->setStyle($tag, true);
        }
        if ($tag=='A') {
            $this->HREF=$attr['HREF'];
        }
        if ($tag=='BR') {
            $this->Ln(5);
        }
    }

    public function closeTag($tag)
    {
        //Closing tag
        if ($tag=='B' or $tag=='I' or $tag=='U') {
            $this->setStyle($tag, false);
        }
        if ($tag=='A') {
            $this->HREF='';
        }
    }

    public function setStyle($tag, $enable)
    {
        //Modify style and select corresponding font
        $this->$tag+=($enable ? 1 : -1);
        $style='';
        foreach (array('B','I','U') as $s) {
            if ($this->$s>0) {
                $style.=$s;
            }
        }
        $this->SetFont('', $style);
    }

    public function putLink($URL, $txt)
    {
        //Put a hyperlink
        $this->SetTextColor(0, 0, 255);
        $this->setStyle('U', true);
        $this->Write(5, $txt, $URL);
        $this->setStyle('U', false);
        $this->SetTextColor(0);
    }

    //Page footer
    public function footer()
    {
        //Position at 1.5 cm from bottom
        $this->SetY(-15);
        //Arial italic 8
        $this->SetFont('Arial', 'I', 8);
        //Page number
        $this->Cell(0, 10, 'Page '.$this->PageNo().'/{nb}', 0, 0, 'C');
    }
}
