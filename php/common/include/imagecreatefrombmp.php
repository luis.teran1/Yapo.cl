<?php
require_once('init.php');

// http://getid3.sourceforge.net/module.graphic.bmp.phps
function stoc($s) {
	$b1 = ord($s[0]);
	$b2 = ord($s[1]);

	return ($b2*256+$b1);
}

function stoi($s) {
	$b1 = ord($s[0]);
	$b2 = ord($s[1]);
	$b3 = ord($s[2]);
	$b4 = ord($s[3]);

	return ($b4*256+$b3)*65536 + ($b2*256+$b1);
}

function littleEndian2Int($s) {
	switch(strlen($s)) {
		case 4:
			return stoi($s);
		case 2:
			return stoc($s);
		case 1:
			return ord($s);
	}

	return $s;
}


function decbinx($d,$n) {
        $bin=decbin($d);
        $sbin=strlen($bin);
        for($j=0;$j<$n-$sbin;$j++)
                $bin="0$bin";
        return $bin;
}

function ret_bits($byte,$start,$len) {
        $bin = decbinx($byte, 8);
        $r = bindec(substr($bin,$start,$len));
        return $r;
}

function read_bits($byte, $count, &$current_bit, &$skip) {
        $LastCBit=$current_bit;
        $current_bit+=$count;
        if($current_bit==8) {
                $current_bit=0;
        } else {
		$skip = false;
        }
        return ret_bits($byte, $LastCBit, $count);
}

function imagecreatefrombmp($filename) {
	$f = fopen($filename, "r");
	$bmp_header = fread($f, 14 + 40);
	$identifier = substr($bmp_header, 0, 2);
	$offset = 2;

	// If file was uploaded get real name
	foreach ($_FILES as $key => $value) {
		if ($filename == $_FILES[$key]["tmp_name"]) {
			$filename_real = $_FILES[$key]["name"];
			break;
		}
	}

	if($identifier=="BM") {
		$size = stoi(substr($bmp_header, $offset, 4));
		$offset+=4;
		$offset+=2;
		$offset+=2;
		$data_offset = stoi(substr($bmp_header, $offset, 4));
		$offset+=4;

		$header_size = stoi(substr($bmp_header, $offset, 4));
		$offset+=4;

		// check if the hardcoded-to-1 "planes" is at offset 22 or 26
		$planes22 = stoc(substr($bmp_header, 22, 2));
		$planes26 = stoc(substr($bmp_header, 26, 2));
		if (($planes22 == 1) && ($planes26 != 1)) {
			$type_os = 'OS/2';
			$type_version = 1;
		} elseif (($planes26 == 1) && ($planes22 != 1)) {
			$type_os= 'Windows';
			$type_version = 1;
		} elseif ($header_size == 12) {
			$type_os = 'OS/2';
			$type_version = 1;
		} elseif ($header_size == 40) {
			$type_os = 'Windows';
			$type_version = 1;
		} elseif ($header_size == 84) {
			$type_os = 'Windows';
			$type_version = 4;
		} elseif ($header_size == 100) {
			$type_os = 'Windows';
			$type_version = 5;
		} else {
			syslog(LOG_INFO, log_string().'BMP: $filename_real  Unknown BMP subtype');
			return false;
		}

		if ($type_os == 'OS/2') {
			$width = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;
			$height = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;
			$planes = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;
			$bits = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;

			if ($type_version >= 2) {
				$compression = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$bmp_data_size = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$res_h = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$res_v = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$colors_used = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$colors_important = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$res_units = stoc(substr($bmp_header, $offset, 2));
				$offset+=2;
				$reserved = stoc(substr($bmp_header, $offset, 2));
				$offset+=2;
				$recording = stoc(substr($bmp_header, $offset, 2));
				$offset+=2;
				$rendering = stoc(substr($bmp_header, $offset, 2));
				$offset+=2;
				$size1 = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$size2 = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$color_encoding = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$identifier = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;

				// Compression check
				//0 => 'BI_RGB',
				//1 => 'BI_RLE8',
				//2 => 'BI_RLE4',
				//3 => 'Huffman 1D',
				//4 => 'BI_RLE24',
				if ($compression < 0 || $compression > 4) {
					syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown compression $type_os v$type_version : $compression");
					return false;
				}
			}
		} else if ($type_os == 'Windows') {
			$width = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$height = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$planes = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;
			$bits = stoc(substr($bmp_header, $offset, 2));
			$offset+=2;
			$compression = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$bmp_data_size = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$res_h = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$res_v = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$colors_used = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;
			$colors_important = stoi(substr($bmp_header, $offset, 4));
			$offset+=4;

			// Compression check
			// 0 => 'BI_RGB',
			// 1 => 'BI_RLE8',
			// 2 => 'BI_RLE4',
			// 3 => 'BI_BITFIELDS',
			// 4 => 'BI_JPEG',
			// 5 => 'BI_PNG'
			if ($compression < 0 || $compression > 5) {
				syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown compression $type_os v$type_version : $compression");
				return false;
			}

			if ($type_version == 1 && $compression == 3) {
				$bmp_header .= fread($f, 28);
				$red_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$green_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$blue_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;

				$no_palette = true;
			} else if ($type_version >= 4 || $compression == 3) {
				$bmp_header .= fread($f, 44);
				$red_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$green_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$blue_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$alpha_mask = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$cs_type = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$ciexyz_red = substr($bmp_header, $offset, 4);
				$offset+=4;
				$ciexyz_green = substr($bmp_header, $offset, 4);
				$offset+=4;
				$ciexyz_blue = substr($bmp_header, $offset, 4);
				$offset+=4;
				$gamma_red = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$gamma_green = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$gamma_blue = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;

				/* XXX
				   $ciexyz_red = FixedPoint2_30(strrev($ciexyz_red));
				   $ciexyz_green = FixedPoint2_30(strrev($ciexyz_green));
				   $ciexyz_blue = FixedPoint2_30(strrev($ciexyz_blue));
				 */    
			}

			if ($type_version >= 5) {
				$bmp_header .= fread($f, 16);
				$intent = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$profile_data_offset = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$profile_data_size = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
				$reserved3 = stoi(substr($bmp_header, $offset, 4));
				$offset+=4;
			}
			
		} else {
			syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown type as $type_os v$type_version");
			return false;
		}

		syslog(LOG_INFO, log_string()." - BMP: $filename_real Is known as $type_os v$type_version");

		/* Creating image holder */
		list($width, $height) = getimagesize($filename);
		$width = abs($width);
		$height = abs($height);
		$img = imagecreatetruecolor($width, $height);

		/* Extracting palette */
		if (!isset($no_palette)) {
			$PaletteEntries = 0;
			if ($bits < 16) {
				$PaletteEntries = pow(2, $bits);
			} elseif (isset($colors_used) && ($colors_used > 0) && ($colors_used <= 256)) {
				$PaletteEntries = $colors_used;
			}

			if ($PaletteEntries > 0) {
				$BMPpalette = fread($f, 4 * $PaletteEntries);
				$paletteoffset = 0;
				for ($i = 0; $i < $PaletteEntries; $i++) {
					$blue  = ord(substr($BMPpalette, $paletteoffset++, 1));
					$green = ord(substr($BMPpalette, $paletteoffset++, 1));
					$red   = ord(substr($BMPpalette, $paletteoffset++, 1));
					if ($type_os != 'OS/2' || $type_version != 1) {
						$paletteoffset++; // padding byte
					}
					$palette[$i] = imagecolorallocate($img, $red, $green, $blue);
				}
			}
		}

		/* Extracting data */
		fseek($f, $data_offset, SEEK_SET);
		$RowByteLength = ceil(($width * ($bits / 8)) / 4) * 4; // round up to nearest DWORD boundry
		$BMPpixelData = fread($f, filesize($filename));
		$pixeldataoffset = 0;
		switch ($compression) {
			case 0: // BI_RGB
				syslog(LOG_INFO, log_string()." - BMP: $filename_real RGB compression:$compression");
				switch ($bits) {
					case 1:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col = $col) {
								$paletteindexbyte = ord($BMPpixelData{$pixeldataoffset++});
								for ($i = 7; $i >= 0; $i--) {
									$paletteindex = ($paletteindexbyte & (0x01 << $i)) >> $i;
									$color = $palette[$paletteindex];
									imagesetpixel($img, $col, $row, $color);
									$col++;
								}
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					case 4:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col = $col) {
								$paletteindexbyte = ord($BMPpixelData{$pixeldataoffset++});
								for ($i = 1; $i >= 0; $i--) {
									$paletteindex = ($paletteindexbyte & (0x0F << (4 * $i))) >> (4 * $i);
									$color = $palette[$paletteindex];
									imagesetpixel($img, $col, $row, $color);
									$col++;
								}
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					case 8:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col++) {
								$paletteindex = ord($BMPpixelData{$pixeldataoffset++});
								$color = $palette[$paletteindex];
								imagesetpixel($img, $col, $row, $color);
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					case 16:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						$red_mask = 0x1f << 10;
						$green_mask = 0x1f << 5;
						$blue_mask = 0x1f;
						$redshift = 10;
						$greenshift = 5;
						syslog(LOG_INFO, log_string().sprintf("Red %b Green %b blue %b %b", $red_mask, $green_mask, $blue_mask, 122));
						
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col++) {
								$pixelvalue = littleEndian2Int(substr($BMPpixelData, $pixeldataoffset, $bits / 8));
								$pixeldataoffset += $bits / 8;

								$red = intval(round(((($pixelvalue & $red_mask) >> $redshift) / ($red_mask >> $redshift)) * 255));
								$green = intval(round(((($pixelvalue & $green_mask) >> $greenshift) / ($green_mask >> $greenshift)) * 255));
								$blue = intval(round(((($pixelvalue & $blue_mask)) / ($blue_mask)) * 255));
								
								$color = imagecolorallocate($img, $red, $green, $blue);
								imagesetpixel($img, $col, $row, $color);
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					case 24:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col++) {
								$color = imagecolorallocate($img, ord($BMPpixelData{$pixeldataoffset+2}) , ord($BMPpixelData{$pixeldataoffset+1}), ord($BMPpixelData{$pixeldataoffset}));
								imagesetpixel($img, $col, $row, $color);
								$pixeldataoffset += 3; 
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					case 32:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col++) {
								// XXX $thisfile_bmp['data'][$row][$col] = (ord($BMPpixelData{$pixeldataoffset+3}) << 24) | (ord($BMPpixelData{$pixeldataoffset+2}) << 16) | (ord($BMPpixelData{$pixeldataoffset+1}) << 8) | ord($BMPpixelData{$pixeldataoffset}); 
								$color = imagecolorallocate($img, ord($BMPpixelData{$pixeldataoffset+2}), ord($BMPpixelData{$pixeldataoffset+1}), ord($BMPpixelData{$pixeldataoffset}));
								imagesetpixel($img, $col, $row, $color);
								$pixeldataoffset += 4; 
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					default:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown bits-per-pixel value ($bits) on compression ($compression) cannot read pixel data");
						return false;
						break;
				}
				break;

			case 1: // BI_RLE8 - http://msdn.microsoft.com/library/en-us/gdi/bitmaps_6x0u.asp
				syslog(LOG_INFO, log_string()." - BMP: $filename_real RLE8 compression:$compression");
				switch ($bits) {
					case 8:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						$pixelcounter = 0;
						while ($pixeldataoffset < strlen($BMPpixelData)) {
							$firstbyte  = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							$secondbyte = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							if ($firstbyte == 0) {
								// escaped/absolute mode - the first byte of the pair can be set to zero to
								// indicate an escape character that denotes the end of a line, the end of
								// a bitmap, or a delta, depending on the value of the second byte.
								switch ($secondbyte) {
									case 0:
										// end of line
										// no need for special processing, just ignore
										break;

									case 1:
										// end of bitmap
										$pixeldataoffset = strlen($BMPpixelData); // force to exit loop just in case
										break;

									case 2:
										// delta - The 2 bytes following the escape contain unsigned values
										// indicating the horizontal and vertical offsets of the next pixel
										// from the current position.
										$colincrement = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
										$rowincrement = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
										$col = ($pixelcounter % $width) + $colincrement;
										$row = ($height - 1 - (($pixelcounter - $col) / $width)) - $rowincrement;
										$pixelcounter = ($row * $width) + $col;
										break;

									default:
										// In absolute mode, the first byte is zero and the second byte is a
										// value in the range 03H through FFH. The second byte represents the
										// number of bytes that follow, each of which contains the color index
										// of a single pixel. Each run must be aligned on a word boundary.
										for ($i = 0; $i < $secondbyte; $i++) {
											$paletteindex = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
											$col = $pixelcounter % $width;
											$row = $height - 1 - (($pixelcounter - $col) / $width);
											imagesetpixel($img, $col, $row, $palette[$paletteindex]);
											$pixelcounter++;
										}
										while (($pixeldataoffset % 2) != 0) {
											// Each run must be aligned on a word boundary.
											$pixeldataoffset++;
										}
										break;
								}
							} else {
								// encoded mode - the first byte specifies the number of consecutive pixels
								// to be drawn using the color index contained in the second byte.
								for ($i = 0; $i < $firstbyte; $i++) {
									$col = $pixelcounter % $width;
									$row = $height - 1 - (($pixelcounter - $col) / $width);
									imagesetpixel($img, $col, $row, $palette[$secondbyte]);
									$pixelcounter++;
								}

							}
						}
						break;

					default:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown bits-per-pixel value ($bits) on compression ($compression)- cannot read pixel data");
						return false;
						break;
				}
				break;

			case 2: // BI_RLE4 - http://msdn.microsoft.com/library/en-us/gdi/bitmaps_6x0u.asp
				syslog(LOG_INFO, log_string()." - BMP: $filename_real RLE4 compression:$compression");
				switch ($bits) {
					case 4:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						$y = $height;
						$pocetb = $pixeldataoffset;

						while($pixeldataoffset < strlen($BMPpixelData)) {
							$y--;
							$prefix=ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							$suffix=ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							$pocetb+=2;

							if(($prefix==0)and($suffix==1)) break;

							while(!(($prefix==0)and($suffix==0))) {
								if($prefix==0) {
									$pocet=$suffix;

									$current_bit=0;
									for($h=0;$h<$pocet;$h++) {
										$skip_data = true;
										$byte = ord(substr($BMPpixelData, $pixeldataoffset, 1));
										$Data[] = read_bits($byte, 4, $current_bit, $skip_data);
										if ($skip_data) {
											$pixeldataoffset++;
										}
									} 
									if($current_bit!=0) {
										$skip_data = true;
										$byte = ord(substr($BMPpixelData, $pixeldataoffset, 1));
										read_bits($byte, 4, $current_bit, $skip_data);
										if ($skip_data) {
											$pixeldataoffset++;
										}
									}
									$pocetb+=ceil(($pocet/2));
									if($pocetb % 2 == 1) {
										$pixeldataoffset++;
										$pocetb++;
									}
								}
								if($prefix>0) {
									$pocet=$prefix;
									$i=0;
									for($r=0;$r<$pocet;$r++) {
										if($i % 2==0) {
											$Data[]=$suffix%16;
										} else {
											$Data[]=floor($suffix/16);
										}
										$i++;
									}
								}
								$prefix=ord(substr($BMPpixelData, $pixeldataoffset++, 1));
								$suffix=ord(substr($BMPpixelData, $pixeldataoffset++, 1));
								$pocetb+=2;
							}

							for($x=0;$x<count($Data);$x++) {
								imagesetpixel($img,$x,$y,$palette[$Data[$x]]);
							}
							$Data=array();
						}
						/* XXX Below is a better script but has a bug
						$pixelcounter = 0;
						while ($pixeldataoffset < strlen($BMPpixelData)) {
							$firstbyte  = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							$secondbyte = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
							if ($firstbyte == 0) {
								// escaped/absolute mode - the first byte of the pair can be set to zero to
								// indicate an escape character that denotes the end of a line, the end of
								// a bitmap, or a delta, depending on the value of the second byte.
								switch ($secondbyte) {
									case 0:
										// end of line
										// no need for special processing, just ignore
										break;

									case 1:
										// end of bitmap
										$pixeldataoffset = strlen($BMPpixelData); // force to exit loop just in case
										break;

									case 2:
										// delta - The 2 bytes following the escape contain unsigned values
										// indicating the horizontal and vertical offsets of the next pixel
										// from the current position.
										$colincrement = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
										$rowincrement = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
										$col = ($pixelcounter % $width) + $colincrement;
										$row = ($height - 1 - (($pixelcounter - $col) / $width)) - $rowincrement;
										$pixelcounter = ($row * $width) + $col;
										break;

									default:
										// In absolute mode, the first byte is zero. The second byte contains the number
										// of color indexes that follow. Subsequent bytes contain color indexes in their
										// high- and low-order 4 bits, one color index for each pixel. In absolute mode,
										// each run must be aligned on a word boundary.
										unset($paletteindexes);
										for ($i = 0; $i < ceil($secondbyte / 2); $i++) {
											$paletteindexbyte = ord(substr($BMPpixelData, $pixeldataoffset++, 1));
											$paletteindexes[] = ($paletteindexbyte & 0xF0) >> 4;
											$paletteindexes[] = ($paletteindexbyte & 0x0F);
										}
										while (($pixeldataoffset % 2) != 0) {
											// Each run must be aligned on a word boundary.
											$pixeldataoffset++;
										}
										foreach ($paletteindexes as $paletteindex) {
											$col = $pixelcounter % $width;
											$row = $height - 1 - (($pixelcounter - $col) / $width);
											imagesetpixel($img, $col, $row, $palette[$paletteindex]);
											$pixelcounter++;
										}
										break;
								}
							} else {
								// encoded mode - the first byte of the pair contains the number of pixels to be
								// drawn using the color indexes in the second byte. The second byte contains two
								// color indexes, one in its high-order 4 bits and one in its low-order 4 bits.
								// The first of the pixels is drawn using the color specified by the high-order
								// 4 bits, the second is drawn using the color in the low-order 4 bits, the third
								// is drawn using the color in the high-order 4 bits, and so on, until all the
								// pixels specified by the first byte have been drawn.
								$paletteindexes[0] = ($secondbyte & 0xF0) >> 4;
								$paletteindexes[1] = ($secondbyte & 0x0F);
								for ($i = 0; $i < $firstbyte; $i++) {
									$col = $pixelcounter % $width;
									$row = $height - 1 - (($pixelcounter - $col) / $width);
									imagesetpixel($img, $col, $row, $palette[$paletteindexes[($i % 2)]]);
									$pixelcounter++;
								}

							}
						}
						*/
						break;

					default:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown bits-per-pixel value ($bits) on compression ($compression)- cannot read pixel data");
						return false;
						break;
				}
				break;

			case 3: // BI_BITFIELDS
				syslog(LOG_INFO, log_string()." - BMP: $filename_real BITFIELDS compression:$compression");
				switch ($bits) {
					case 16:
					case 32:
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Bits:$bits");
						$redshift   = 0;
						$greenshift = 0;
						$blueshift  = 0;
						while ((($red_mask >> $redshift) & 0x01) == 0) {
							$redshift++;
						}
						while ((($green_mask >> $greenshift) & 0x01) == 0) {
							$greenshift++;
						}
						while ((($blue_mask >> $blueshift) & 0x01) == 0) {
							$blueshift++;
						}
						for ($row = ($height - 1); $row >= 0; $row--) {
							for ($col = 0; $col < $width; $col++) {
								$pixelvalue = littleEndian2Int(substr($BMPpixelData, $pixeldataoffset, $bits / 8));
								$pixeldataoffset += $bits / 8;

								$red   = intval(round(((($pixelvalue & $red_mask)   >> $redshift)   / ($red_mask   >> $redshift))   * 255));
								$green = intval(round(((($pixelvalue & $green_mask) >> $greenshift) / ($green_mask >> $greenshift)) * 255));
								$blue  = intval(round(((($pixelvalue & $blue_mask)  >> $blueshift)  / ($blue_mask  >> $blueshift))  * 255));

								$color = imagecolorallocate($img, $red, $green, $blue);
								imagesetpixel($img, $col, $row, $color);
							}
							while (($pixeldataoffset % 4) != 0) {
								// lines are padded to nearest DWORD
								$pixeldataoffset++;
							}
						}
						break;

					default: 
						syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown bits-per-pixel value ($bits) on compression ($compression)- cannot read pixel data");
						return false;
						break;
				}
				break;


			default: // unhandled compression type
				syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown compression ($compression)- cannot read pixel data");
				return false;
				break;
		}
	} else {
		syslog(LOG_INFO, log_string()." - BMP: $filename_real Unknown type, not BMP");
		return false;
	}

	fclose($f);
	return $img;
}
?>
