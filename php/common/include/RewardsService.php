<?php
namespace Yapo;
use ProxyClient;
class RewardsService
{
    private $healthcheck;
    private $Host;
    private $Port;
    private $Proxy;
    public function __construct()
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.rewards.host");
        $this->Port = Bconf::get($BCONF, "*.rewards.port");
        $this->Path = Bconf::get($BCONF, "*.rewards.healthcheck.path");
        $this->CreateReward = Bconf::get($BCONF, "*.rewards.create");
        $this->SearchRewards = Bconf::get($BCONF, "*.rewards.search");
        $this->GetGroup = Bconf::get($BCONF, "*.rewards.get_group");
        $this->UpdateGroup = Bconf::get($BCONF, "*.rewards.update_group");
        $this->SetGroupStatus = Bconf::get($BCONF, "*.rewards.set_group_status");
        $this->DeleteGroup = Bconf::get($BCONF, "*.rewards.delete_group");
        $this->CreateGroup = Bconf::get($BCONF, "*.rewards.create_group");
        $this->Proxy = new ProxyClient();
    }
    public function checkService()
    {
        $response = $this->Proxy->doRequest("get", $this->Host, $this->Port, $this->Path, null);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Rewards Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }
    public function createReward(Array $data)
    {
        $response = $this->Proxy->doRequest($this->CreateReward["method"], $this->Host, $this->Port, $this->CreateReward["path"], $data);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Rewards Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }
    public function getRewards(Array $data)
    {
	$full_path=$this->SearchRewards["path"];
	$uri_params=http_build_query($data);
	if ($uri_params!= "") {
	    $full_path=$full_path."?".$uri_params;
	}
        Logger::logDebug(__METHOD__, "PATH: ". var_export($full_path, true));
        $response = $this->Proxy->doRequest($this->SearchRewards["method"], $this->Host, $this->Port, $full_path, null);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Rewards Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }
    
    public function getGroups(){
       $path = $this->GetGroup["path"];
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       $response = $this->Proxy->doRequest($this->GetGroup["method"], $this->Host, $this->Port, $path, null);
       return json_decode($response, true);
    }

    public function getGroup($groupID){
       $path = $this->GetGroup["path"]."/".$groupID;
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       $response = $this->Proxy->doRequest($this->GetGroup["method"], $this->Host, $this->Port, $path, null);
       $code = $response->code;
       Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
       if ($code != 200) {
               Logger::logError(__METHOD__, "Api Rewards Error:". var_export($response, true));
               return null;
       }
       return json_decode($response);
    }

    public function updateGroup($groupID, $groupData){
       $path = $this->UpdateGroup["path"]."/".$groupID;
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       array_walk_recursive($groupData, function(&$item) { if (is_string($item)) $item = utf8_decode($item); });
       $response = $this->Proxy->doRequest($this->UpdateGroup["method"], $this->Host, $this->Port, $path, $groupData);
       return json_decode($response, true);
    }

    public function setGroupStatus($groupID, $groupStatus){
       $groupData['status'] = $groupStatus;
       $path = $this->SetGroupStatus["path"]."/".$groupID;
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       $response = $this->Proxy->doRequest($this->SetGroupStatus["method"], $this->Host, $this->Port, $path, $groupData);
       return json_decode($response, true);
    }

    public function deleteGroup($groupID){
       $path = $this->DeleteGroup["path"]."/".$groupID;
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       $response = $this->Proxy->doRequest($this->DeleteGroup["method"], $this->Host, $this->Port, $path, null);
       return json_decode($response, true);
    }

    public function createGroup($groupData){
       $path = $this->CreateGroup["path"];
       Logger::logDebug(__METHOD__, "PATH: ".var_export($path, true));
       array_walk_recursive($groupData, function(&$item) { if (is_string($item)) $item = utf8_decode($item); });
       $response = $this->Proxy->doRequest($this->CreateGroup["method"], $this->Host, $this->Port, $path, $groupData);
       return json_decode($response, true);
    }
}
