<?php
require_once('fpdf.php');

class PDF extends FPDF {
	//Page footer
	function Footer() {
		//Position at 1.5 cm from bottom
		$this->SetY(-15);
		//Arial italic 8
		$this->SetFont('Arial','I',8);
		//Page number
		$this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C');
	}
	var $B;
	var $I;
	var $U;
	var $HREF;

	function PDF($orientation='P',$unit='mm',$format='A4') {
		//Call parent constructor
		$this->FPDF($orientation,$unit,$format);
		//Initialization
		$this->B=0;
		$this->I=0;
		$this->U=0;
		$this->HREF='';
	}

	function WriteHTML($html)
	{
		//HTML parser
//		$html=str_replace("\n",' ',$html);
		$a=preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
		foreach($a as $i=>$e)
		{
			if($i%2==0)
			{
				//Text
				if (@$this->right_pos)
					$this->right_pos_text .= $e;
				else if (@$this->textlimit)
					$this->textlimit_text .= $e;
				else if ($this->HREF)
					$this->PutLink($this->HREF,$e);
				else
					$this->Write(5,$e);
			}
			else
			{
				//Tag
				if($e{0}=='/')
					$this->CloseTag(strtoupper(substr($e,1)));
				else
				{
					//Extract attributes
					$a2=explode(' ',$e);
					$tag=strtoupper(array_shift($a2));
					$attr=array();
					foreach($a2 as $v)
						if(preg_match('/^([^=]*)=["\']?([^"\']*)["\']?$/',$v,$a3))
							$attr[strtoupper($a3[1])]=$a3[2];
					$this->OpenTag($tag,$attr);
				}
			}
		}
	}

	function OpenTag($tag,$attr)
	{
		//Opening tag
		if($tag=='B' or $tag=='I' or $tag=='U')
			$this->SetStyle($tag,true);
		else if($tag=='A')
			$this->HREF=$attr['HREF'];
		else if($tag=='BR')
			$this->Ln(5);
		else if ($tag == 'SETPOS') {
			if (isset($attr['X']))
				$this->SetX($attr['X']);
				//$this->SetX($attr['X'] - $this->GetX());
			if (isset($attr['Y']))
				$this->SetY($attr['Y']);
		}
		else if ($tag == 'RIGHTPOS') {
			$this->right_pos = $attr['X'];
			$this->right_pos_text = '';
		}
		else if ($tag == 'IMAGE') {
			$this->Image($attr['FILENAME'], $attr['X'], $attr['Y']);
		}
		else if ($tag == 'SETFONTSIZE') {
			$this->SetFontSize($attr['SIZE']);
		}
		else if ($tag == 'TEXTLIMIT') {
			$this->textlimit = $attr['WIDTH'];
			$this->textlimit_elipsis = @$attr['ELIPSIS'] ? $attr['ELIPSIS'] : '...';
			$this->textlimit_text = '';
		}
		else {
			$this->Write(5, "Unknown tag: $tag.");
		}
	}

	function CloseTag($tag)
	{
		//Closing tag
		if($tag=='B' or $tag=='I' or $tag=='U')
			$this->SetStyle($tag,false);
		else if($tag=='A')
			$this->HREF='';
		else if ($tag == 'RIGHTPOS') {
			$this->SetX($this->right_pos - $this->GetStringWidth($this->right_pos_text));
			$this->Write(5, $this->right_pos_text);
			unset($this->right_pos);
		}
		else if ($tag == 'TEXTLIMIT') {
			if($this->textlimit_text && $this->GetStringWidth($this->textlimit_text) > $this->textlimit) {
				while ($this->textlimit_text && $this->GetStringWidth($this->textlimit_text.$this->textlimit_elipsis) > $this->textlimit)
					$this->textlimit_text = substr($this->textlimit_text, 0, -1);
				$this->textlimit_text .= $this->textlimit_elipsis;
			}
			$this->Write(5, $this->textlimit_text);
			unset($this->textlimit);
		}
	}

	function SetStyle($tag,$enable)
	{
		//Modify style and select corresponding font
		$this->$tag+=($enable ? 1 : -1);
		$style='';
		foreach(array('B','I','U') as $s)
			if($this->$s>0)
				$style.=$s;
		$this->SetFont('',$style);
	}

	function PutLink($URL,$txt)
	{
		//Put a hyperlink
		$this->SetTextColor(0,0,255);
		$this->SetStyle('U',true);
		$this->Write(5,$txt,$URL);
		$this->SetStyle('U',false);
		$this->SetTextColor(0);
	}
}

?>
