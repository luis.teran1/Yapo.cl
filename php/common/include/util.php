<?php

/* Converts a time string (eg. 2000-01-01 12:12:12.1234) to UNIX time format */
function pgsql_mktime($date) {
	$year = substr($date, 0, 4);
	$month = substr($date, 5, 2);
	$day = substr($date, 8, 2);
	$hour = substr($date, 11, 2);
	$minute = substr($date, 14, 2);

	return mktime($hour, $minute, 0, $month, $day, $year);
}

function valid_utf8($str) {
	$l = strlen($str);

	for ($i = 0; $i < $l; $i++) {
		$ch = ord($str[$i]);

		if (($ch & 0x80) == 0)
			continue;
		if (($ch & 0xE0) == 0xC0) {
			if (++$i >= $l)
				return false;
			$ch = ord($str[$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			continue;
		}
		if (($ch & 0xF0) == 0xE0) {
			if ($i >= $l - 2)
				return false;
			$ch = ord($str[++$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			$ch = ord($str[++$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			continue;
		}
		if (($ch & 0xF8) == 0xF0) {
			if ($i >= $l - 3)
				return false;
			$ch = ord($str[++$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			$ch = ord($str[++$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			$ch = ord($str[++$i]);
			if (($ch & 0xC0) != 0x80)
				return false;
			continue;
		}
		return false;
	}
	return true;
}

/* Convert ht (a number) to the source string to send into transactions */
function ht_to_source($ht) {
	global $BCONF;
	$ht = (int)$ht;

	return bconf_get($BCONF, '*.ai.source.'.$ht);
}

/* Hash password */
function hash_password($passwd = false, $salt = false) {
	global $BCONF;
	if ( !$passwd )
		return false;

	$loops = bconf_get($BCONF, "ai.passwd.stretch_loops");
	if ( !$salt ) {
		$salt = '';
		for ($i = 0; $i < 16; $i++)
			$salt .= chr(mt_rand(32, 126));
	} else {
		$salt = preg_replace('/\$'.$loops.'\$/', '', $salt, 1);
	}

	for ($i = 0; $i < $loops; $i++)
		$passwd = sha1($salt . $passwd);

	return "\$$loops\$" . $salt . $passwd;
}

/**
 * Verify password length
 * Return true if the password is ok and match with passwd verification
 * else return error msj
 */
function password_verify($password, $password_verify, $allow_missing = false) {
	if (!empty($password)) {
		if ($password == $password_verify) {
			$pwd_len_min = (int)bconf_get($BCONF, "*.common.passwd_length.min");
			$pwd_len_max = (int)bconf_get($BCONF, "*.common.passwd_length.max");
			$pwd_check = strlen($password);

			if ($pwd_check < $pwd_len_min) {
				return "ERROR_ACCOUNT_PASSWORD_TOO_SHORT";
			} elseif ($pwd_check > $pwd_len_max) {
				return "ERROR_ACCOUNT_PASSWORD_TOO_LONG";
			} else {
				return True;
			}
		} else {
			return "ERROR_PASSWORD_MISMATCH";
		}
	} elseif(!$allow_missing) {
		return "ERROR_PASSWORD_MISSING";
	}
	return "";
}

function payment_enabled() {
	if (bconf_get($BCONF, "*.payment.enabled") == "1" || bconf_get($BCONF, "*.payment.allowedip.".getenv('REMOTE_ADDR')) == "1")
		return true;
	return false;
}

function split_settings($value) {
	if (!isset($value) || empty($value))
		return NULL;
	$result = array();
	$main_array = explode(',',$value);
	foreach($main_array as $var) {
		$child_array = explode(':',$var);
		if (count($child_array) == 2) {
			$result[$child_array[0]] = $child_array[1];
		} elseif (isset($child_array[0])) {
			$result[$child_array[0]] = '';
		}
	}
	return $result;
}

function decode_api_response($action, $response_json) {
	$callers = debug_backtrace();
	$line    = $callers[0]['line'];

	if (empty($response_json)) {
		syslog(LOG_INFO, log_string()." ERROR: Received empty response when trying to $action an ad on redis. On file ".__FILE__." in line $line.");
		return NULL;
	}
	else {
		$response = yapo_json_decode($response_json, true);

		if (empty($response) || empty($response['status'])) {
			syslog(LOG_INFO, log_string()." ERROR: Response could not be decoded when trying to $action ad on redis. Raw response: ".var_export($response_json, TRUE).' On file '.__FILE__." in line $line.");
			return NULL;
		}
		elseif($response['status'] == 'ERROR') {
			syslog(LOG_INFO, log_string().' ERROR('.$response['code']."): Could not $action ad on redis. ".$response['message'].' On file '.__FILE__." in line $line.");
			return NULL;
		}
	}

	return $response;
}

function pretty_var_dump($variable){
		echo "<pre>";
		print_r($variable);
		echo "</pre>";
}

function check_password_hash($hash, $salted_passwd) {
	if (!empty($hash) && !empty($salted_passwd)) {
		$passwd_hashes = array(
			sha1($salted_passwd),
			sha1($salted_passwd . date("Ymd")),
			sha1($salted_passwd . date("Ymd", strtotime("-1 day"))),
			sha1($salted_passwd . date("Ymd", strtotime("-2 day"))));
		if (in_array($hash, $passwd_hashes)) {
			return true;
		}
	}
	return false;
}
/**
 * check user is pro in pack configured enabled.
 * param: is_pro_for must be STRING
 * */
function is_user_pro($is_pro_for){
	global $BCONF;

	if(empty($is_pro_for)){
		return false;
	}

	foreach (bconf_get($BCONF, "*.packs.type") as $key => $value) {
		if($value["enabled"] == "1") {
			foreach (explode( ',', $is_pro_for) as $usr_pro_cat) {
				if(array_key_exists($usr_pro_cat, $value["category"])){
					bLogger::logInfo(__METHOD__, "User is pro in pack: {".$value["name"]."} with category $usr_pro_cat}");
					return true;
				}
			}
		}
	}
	return false;
}

/**
 * check if a pro user could skip dtp
 * 
 * param: is_pro_for must be STRING
 * PRO | ACTIVE_PACK | DTB | SHOULD 
 * --------------------------------
 * 0   | X           | X   | TRUE
 * 1   | 0           | X   | TRUE
 * 1   | 1           | 0   | FALSE
 * 1   | 1           | 1   | TRUE
 * */
function should_search_dups($is_pro_for, $active_pack, $ad_category){

	global $BCONF;

	if (empty($is_pro_for) || empty($active_pack) || empty($ad_category) || $active_pack == "expired") {
		return true;
	}

	$usr_pro_cat = explode( ',', $is_pro_for);
	foreach($usr_pro_cat as $first_pro_cat) {
		foreach (bconf_get($BCONF, "*.packs.type") as $key => $value) {
			if($value["enabled"] == "1" && $value["dtb_enabled"] == "0") {
				$usr_pro_cat = explode( ',', $is_pro_for);
				if(array_key_exists($first_pro_cat, $value["category"]) && array_key_exists($ad_category, $value["category"])){
					bLogger::logInfo(__METHOD__, "User skips DTB: {".$value["name"]."} with category {".$first_pro_cat."} on category {$ad_category}");
					return false;
				}
			}
		}
	}

	return true;
}

function allow_delete_ad($is_pro_for, $active_pack, $ad_category, $has_if = null){
	// If the ad has inserting return true
	if ($has_if == 1){
		return true;
	}
	$should_search = should_search_dups($is_pro_for, $active_pack, $ad_category);
	return !$should_search;
}



function sanitizeVar($var = null, $type = "", $flags = null) {
	$filters = array(
		"string" => FILTER_SANITIZE_STRING,
		"email"  => FILTER_SANITIZE_EMAIL,
		"float"  => FILTER_SANITIZE_NUMBER_FLOAT,
		"int"    => FILTER_SANITIZE_NUMBER_INT,
		"url"    => FILTER_SANITIZE_URL
	);

	switch($type) {
		case 'array':
			if (is_array($var)) {
				return $var;
			}
			else {
				return false;
			}
			break;
		case 'array_string':
			return filter_var_array($var, FILTER_SANITIZE_STRING);
			break;
		case 'array_email':
			return filter_var_array($var, FILTER_SANITIZE_EMAIL);
			break;
		case 'array_float':
			return filter_var_array($var, FILTER_SANITIZE_NUMBER_FLOAT);
			break;
		case 'array_int':
			return filter_var_array($var, FILTER_SANITIZE_NUMBER_INT);
			break;
		case 'array_url':
			return filter_var_array($var, FILTER_SANITIZE_URL);
			break;
		default:
			if (isset($filters[$type])) {
				if (isset($flags)) {
					return filter_var($var, $filters[$type], $flags);
				}
				else {
					return filter_var($var, $filters[$type]);
				}
			}
			else {
				return $var;
			}
			break;
	}
}

/**
 * Gets final part of URL of pack by category.
 **/
function pack_url_suffix($category) {
	$packs = bconf_get($BCONF,"*.packs.type");
	foreach ($packs as $type => $attr) {
		if (!isset($attr["enabled"]) || $attr["enabled"] != "1") continue;
		foreach ($attr["category"] as $cat => $val) {
			if ($cat == $category and $val == "1") {
				return $attr["url_suffix"];
			}
		}
	}
	return NULL;
}

function format_birthdate($birth_year, $birth_month, $birth_day) {
	if (strlen($birth_day) || strlen($birth_month) || strlen($birth_year)) {
		$birth_day_p = (strlen($birth_day) == 1 && (int)$birth_day != 0) ? '0' . $birth_day : $birth_day;
		$birth_month_p = (strlen($birth_month) == 1 && (int)$birth_month != 0) ? '0' . $birth_month : $birth_month;
		return $birth_year . '-' . $birth_month_p . '-' . $birth_day_p;
	}
	return '';
}

function aes_cbc_encode($key, $data) {
	$key = base64_decode($key);
	$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');
	$ivSize = mcrypt_enc_get_iv_size($cipher);
	$tmpIV = bin2hex(mcrypt_create_iv($ivSize));
	$iv = substr($tmpIV, 0, $ivSize);

	mcrypt_generic_init($cipher, $key, $iv);
	$result = mcrypt_generic($cipher, $data);
	mcrypt_generic_deinit($cipher);
	return base64_encode($iv.$result);
}

function aes_cbc_decode($key, $data) {
	$data = base64_decode($data);
	$key = base64_decode($key);

	$iv = substr($data, 0, 16);
	$info = substr($data, 16);
	$cipher = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

	mcrypt_generic_init($cipher, $key, $iv);
	$result = mdecrypt_generic($cipher, $info);

	return $result;
}

/**
 * @param $conversionFactor
 * @param $price
 */
function convertUFToPesos($conversionFactor, $price) {
	$commaPosition = strpos($conversionFactor, ',');

	// 24548.4567 to 24548.45
	$conversionFactor = $commaPosition
		? substr($conversionFactor, 0, $commaPosition + 3)
		: $conversionFactor;

	// 254,45 to 254.45
	$price = str_replace(',', '.', $price);
	$conversionFactor = str_replace(',', '.', $conversionFactor);

	return intval($price * $conversionFactor);
}

/** Function to know if a user is pro for certain category*/
function is_pro_for($pro_cat, $is_pro_for) {
	if (empty($is_pro_for)) {
		return false;
	}
	return in_array($pro_cat, explode(',', $is_pro_for));
}
