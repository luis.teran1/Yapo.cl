<?php

use Yapo\Bconf;

require_once('init.php');

class bLogger {
	static private $levels = array(
		LOG_EMERG => 0,
		LOG_ALERT => 1,
		LOG_CRIT => 2,
		LOG_ERR => 3,
		LOG_WARNING => 4,
		LOG_NOTICE => 5,
		LOG_INFO => 6,
		LOG_DEBUG => 7
	);

	private static function writeLog( $fromFunction, $message, $level = LOG_INFO ) {
		$min_log_level = intval(Bconf::bconfGet($BCONF, "*.php.log_level"));
		if (self::$levels[$level] <= $min_log_level) {
			$message = preg_replace("/\s+/", " ", $message);
			syslog($level, log_string() . ": {$fromFunction}: {$message}");
		}
	}

	public static function logError( $fromFunction, $message ) {
		self::writeLog( $fromFunction, $message, LOG_ERR );
	}

	public static function logWarning( $fromFunction, $message) {
		self::writeLog( $fromFunction, $message, LOG_WARNING );
	}

	public static function logNotice( $fromFunction, $message ) {
		self::writeLog( $fromFunction, $message, LOG_NOTICE );
	}

	public static function logInfo( $fromFunction, $message) {
		self::writeLog( $fromFunction, $message, LOG_INFO );
	}

	public static function logDebug( $fromFunction, $message ) {
		self::writeLog( $fromFunction, $message, LOG_DEBUG );
	}
}

?>
