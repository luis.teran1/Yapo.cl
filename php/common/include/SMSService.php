<?php
namespace Yapo;


class SMSService extends MicroserviceConnector
{
    public function __construct()
    {
        $account_session = new \AccountSession();
        $yapo_request = new YapoRequest($account_session);
        parent::__construct("super_message_service", $yapo_request);
    }

    public function responseHandler($response) {
        return $response;
    }

    public function errorHandler($response = array()) {
        return json_decode($response, true);
    }
}
