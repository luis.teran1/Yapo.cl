<?php
/*
 * Caller object
 * Extracts caller information and validate it
 */
class blocket_caller
{
	var $region;
	var $city;
	var $type;
	var $default_set;
	var $cookie_set;

	var $layout;

	function blocket_caller() {
		global $BCONF;

		/* Parse caller argument */
		if ($this->parse_caller()) {
			$this->default_set = false;
			$this->cookie_set = false;
		} elseif (!empty($_COOKIE['default_ca']) && $this->parse_caller($_COOKIE['default_ca'])) {
			$this->cookie_set = true;
			$this->default_set = false;
		} else {
			/* Default caller is set */
			$default_caller = bconf_get($BCONF, '*.common.default.caller');
			list ($this->region, $this->city, $this->type) = $this->extract_caller($default_caller);
			$this->default_set = true;
			$this->cookie_set = false;
		}

		/* Layout */
		if (!is_null(bconf_get($BCONF, "*.common.layout.{$_GET['l']}")))
			$this->layout = intval($_GET['l']);
		else 
			$this->layout = 0;
	}

	/* Extract caller */
	function extract_caller($ca = null) {
		if (empty($ca)) {
			if (!isset($_GET['ca']))
				return false;
			$ca = $_GET['ca'];
		}

		if (preg_match('/^[[:digit:]]+_[[:digit:]]+_[[:alpha:]]+$/', $ca)) {
			list($region, $city, $type) = explode('_', $ca);
		} else if (preg_match('/^[[:digit:]]+_[[:alpha:]]+$/', $ca)) {
			list($region, $type) = explode('_', $ca);
			$city = 0;
		} else {
			return false;
		}

		return array($region, $city, $type);
	}

	/* Get caller from request */
	function parse_caller($ca = null) {
		global $BCONF;

		if (empty($ca))
			$ca = @$_GET['ca'];

		// Validate structure
		if (!preg_match('/^[[:digit:]]+_([[:digit:]]+_)?[[:alpha:]]+$/', $ca))
			return false;

		// Extract caller
		list ($region, $city, $type) = $this->extract_caller($ca);

		// Validate region
		$conf_region = bconf_get($BCONF, "*.common.region.{$region}" . (($city > 0)?".cities.{$city}" : ""));
		if (is_null($conf_region)) {
			return false;
		}

		// Validate type
		$conf_type = bconf_get($BCONF, "*.common.type.default");
		if (!in_array($type, $conf_type))
			return false;

		// Update object
		$this->region = intval($region);
		$this->city = intval($city);
		$this->type = $type;

		return true;
	}

	/* Implodes region, city, type and creates caller */
	function to_string() {
		$ca = $this->region;

		if ($this->city > 0)
			$ca .= '_'.$this->city;

		if (!empty($this->type))
			$ca .= '_'.$this->type;

		return $ca;
	}
}

