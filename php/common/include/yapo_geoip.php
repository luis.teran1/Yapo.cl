<?php
require_once('geoip.inc');

function get_geoip_country($ip_address) {
        $gi = geoip_open("/usr/share/GeoIP/GeoIP.dat", GEOIP_STANDARD);
        $country = geoip_country_name_by_addr($gi, $ip_address);
        geoip_close($gi);

        if (isset($country) && !empty($country))
                return $country;
        return "Country unknown";
}

function get_isoname_geoip_countr($ip_address) {
        $gi = geoip_open("/usr/share/GeoIP/GeoIP.dat", GEOIP_STANDARD);
        $country = geoip_country_code_by_addr($gi, $ip_address);
        geoip_close($gi);

        if (isset($country) && !empty($country))
                return strtolower($country);

        return "";
}

?>
