<?php
namespace Yapo;

use ProxyClient;

class ClaimProductService
{
    private $healthcheck;
    private $Host;
    private $Port;
    private $Proxy;
    public function __construct()
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.claim_product.host");
        $this->Port = Bconf::get($BCONF, "*.claim_product.port");
        $this->HealthcheckPath = Bconf::get($BCONF, "*.claim_product.healthcheck.path");
        $this->SearchClaim = Bconf::get($BCONF, "*.claim_product.search");
        $this->PartialEditClaim = Bconf::get($BCONF, "*.claim_product.edit");
        $this->Proxy = new ProxyClient();
    }

    function arrayResponse($response) {
        $json_response = json_decode($response);
        if ($json_response == null){
            return (object)array();
        }
        return $json_response;
    }

    public function checkService()
    {
        $response = $this->Proxy->doRequest("get", $this->Host, $this->Port, $this->HealthcheckPath, null);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Claim Product Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function searchClaims(array $data)
    {
        $full_path=$this->SearchClaim["path"];
        $uri_params=http_build_query($data);
        Logger::logDebug(__METHOD__, "uri params:". var_export($uri_params, true));
        if ($uri_params!= "") {
            $full_path=$full_path."?".$uri_params;
        }
        $response = $this->Proxy->doRequest($this->SearchClaim["method"], $this->Host, $this->Port, $full_path);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api Claim Product Error:". var_export($response, true));
            return null;
        }
        return $this->arrayResponse($response);
    }

    public function editClaim($claim_id, Array $changes)
    {
        $parsed_path = str_replace('%claim_id', $claim_id, $this->PartialEditClaim["path"]);
        $response = $this->Proxy->doRequest($this->PartialEditClaim["method"], $this->Host, $this->Port, $parsed_path, $changes);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Proxy response:". var_export($response, true));
        if ($code != 200 && $code != 204) {
            Logger::logError(__METHOD__, "Api Claim Product Error:". var_export($response, true));
            return null;
        }
        return $this->arrayResponse($response);
    }
}
