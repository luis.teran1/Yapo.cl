<?php
namespace Yapo;
use ProxyClient;
class LoanManagerService
{
    private $healthcheck;
    private $Host;
    private $Port;
    private $Proxy;
    public function __construct()
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.loan_manager.host");
        $this->Port = Bconf::get($BCONF, "*.loan_manager.port");
        $this->Path = Bconf::get($BCONF, "*.loan_manager.healthcheck.path");
        $this->CreateSetting = Bconf::get($BCONF, "*.loan_manager.create");
        $this->GetSettingsByUserID = Bconf::get($BCONF, "*.loan_manager.search");
        $this->DeleteSetting = Bconf::get($BCONF, "*.loan_manager.delete");
        $this->UpdateSetting = Bconf::get($BCONF, "*.loan_manager.edit");
        $this->Proxy = new ProxyClient();
    }
    public function checkService()
    {
        $response = $this->Proxy->doRequest("get", $this->Host, $this->Port, $this->Path, null);
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api LoanManager Error:". var_export($response, true));
            return null;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function createSetting(Array $data)
    {
        $response = $this->Proxy->doRequest(
            $this->CreateSetting["method"], 
            $this->Host,
            $this->Port,
            $this->CreateSetting["path"],
            $data
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api LoanManager Error:". var_export($response, true));
            $json_response = json_decode($response);
            return $json_response;
        }
        return null;
    }

    public function getSettingByUserID(Array $data)
    {
        $full_path = $this->replacePathValues($data, $this->GetSettingsByUserID["path"]);
        Logger::logDebug(__METHOD__, "PATH: ". var_export($full_path, true));
        $response = $this->Proxy->doRequest(
            $this->GetSettingsByUserID["method"],
            $this->Host,
            $this->Port,
            $full_path,
            null
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api LoanManager Error:". var_export($response, true));
            $json_response = json_decode($response);
            return $json_response;
        }
        $json_response = json_decode($response);
        return $json_response;
    }

    public function updateSetting(Array $data)
    {
        $response = $this->Proxy->doRequest(
            $this->UpdateSetting["method"],
            $this->Host,
            $this->Port,
            $this->UpdateSetting["path"],
            $data
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api LoanManager Error:". var_export($response, true));
            $json_response = json_decode($response);
            return $json_response;
        }
        return null;
    }

    public function deleteSetting(Array $data)
    {
        $full_path = $this->replacePathValues($data, $this->DeleteSetting["path"]);
        Logger::logDebug(__METHOD__, "PATH: ". var_export($full_path, true));
        $response = $this->Proxy->doRequest(
            $this->DeleteSetting["method"],
            $this->Host,
            $this->Port,
            $full_path,
            null
        );
        $code = $response->code;
        Logger::logDebug(__METHOD__, "Response Proxy:". var_export($response, true));
        if ($code != 200) {
            Logger::logError(__METHOD__, "Api LoanManager Error:". var_export($response, true));
            $json_response = json_decode($response);
            return $json_response;
        }
        return null;
    }

    private function replacePathValues($data, $path) {
        foreach ($data as $k => $v) {
            $path = str_replace("$".$k, $v, $path);
        }
        return $path;
    }
}
