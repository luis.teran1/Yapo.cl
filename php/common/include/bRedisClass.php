<?php

use Yapo\Bconf;

/*This class should be inherited for class that want to use one BN redis server*/
abstract class bRedisClass {

    protected $redis;
    abstract protected function getRedisRootNode();

    function __construct() {
        global $BCONF;

        //load the user from the Redis
        $redisConf = Bconf::bconfGet($BCONF, $this->getRedisRootNode());

        //connect to redis
        $this->redis = new Redis();
        $this->redis->connect($redisConf['name'], $redisConf['port'], $redisConf['timeout']);
        if (isset($redisConf['db'])) {
            $this->redis->select($redisConf['db']);
        }
    }

    function __destruct() {
        if ( isset($this->redis) ) {
            $this->redis->close();
        }
    }
}
