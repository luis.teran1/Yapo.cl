<?php

class CreditsPage
{
    public $response;
    public $credits;
    public $credits_to_expire;
    public $expiration_date;

    public function __construct()
    {
        $this->response = new bResponse();
    }

    public function showPage()
    {
        $acc_session = new AccountSession();
        if (!$acc_session->is_logged()) {
            $login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
            header('Location: ' .$login);
            exit(0);
        }

        $credits = new CreditsService();
        $credits_response = $credits->getBalance($acc_session->get_param('account_id'));
        $this->credits = $credits_response->Balance;
        $this->credits_to_expire = $credits_response->CreditToExpire->Credits;
        $this->expiration_date = $credits_response->CreditToExpire->ExpirationDate;
        $this->addPageContent();
        $this->addCreditsContent();
        $this->response->show_html_results("mobile_select");
    }

    public function addPageContent()
    {
        $this->response->add_data('headstyles', 'credits.css');
        $this->response->add_data('headscripts', array('/tjs/arrays_v2.js', 'credits.js'));
        $this->response->add_data('page_title', lang('PAGE_ACCOUNT_CREDITS_TITLE'));
        $this->response->add_data('page_name', lang('PAGE_ACCOUNT_CREDITS_NAME'));
        $this->response->add_data('content', "accounts/credits.html");
        $this->response->add_data('appl', "credits");
        $this->response->add_data('mobile_appl', "credits");
        $this->response->add_data('REQUEST_URI', $_SERVER['REQUEST_URI']);
        $this->response->add_data("REMOTE_BROWSER", $_SERVER['HTTP_USER_AGENT']);
        $this->response->add_data("REMOTE_ADDR", $_SERVER['REMOTE_ADDR']);
    }

    public function addCreditsContent()
    {
        $this->response->add_data("user_credits", $this->credits);

        if ($this->credits > 0) {
            $this->response->add_data("credits_to_expire", $this->credits_to_expire);
            list($days_diff, $formated_date) = $this->getDaysDiff($this->expiration_date);

            switch ($days_diff) {
                case 0:
                    $this->response->add_data("near_expiration", 1);
                    $this->response->add_data("expiration_date", lang("DATE.TODAY"));
                    break;
                case 1:
                    $this->response->add_data("near_expiration", 1);
                    $this->response->add_data("expiration_date", lang("DATE.TOMORROW"));
                    break;
                default:
                    $this->response->add_data("expiration_date", $formated_date);
            }
        }
    }

    public function getDaysDiff($end_date, $start_date = null)
    {
        if (!isset($end_date)) {
            return array(0,"");
        }
        $end_date = date_parse($end_date);
        $end = new DateTime("{$end_date["year"]}-{$end_date["month"]}-{$end_date["day"]}");

        $start = new DateTime(date("Y-m-d"));
        if (isset($start_date)) {
            $start_date = date_parse($start_date);
            $start = new DateTime("{$start_date["year"]}-{$start_date["month"]}-{$start_date["day"]}");
        }
        return array(
            $start->diff($end)->days,
            "{$end_date["day"]}/{$end_date["month"]}/{$end_date["year"]}"
        );
    }
}
