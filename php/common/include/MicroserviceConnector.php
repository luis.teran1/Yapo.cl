<?php

namespace Yapo;

abstract class MicroserviceConnector
{
    private $yapo_request;
    private $service_name;
    private $action;

    public function __construct($service_name = null, $yapo_request = null)
    {
        $this->yapo_request = $yapo_request;
        $this->service_name = $service_name;
    }

    public function execute($action, $data = array())
    {
        if (!$action) {
            return errorHandler();
        }
        $this->action = $action;
        $response = $this->yapo_request->sendRequest($this->service_name, $action, $data);

        return $this->responseHandler($response);
    }

    abstract public function responseHandler($response);

    abstract public function errorHandler($response = array());

    public function setServiceName($service_name)
    {
        $this->service_name = $service_name;
    }

    public function getServiceName()
    {
        return $this->service_name;
    }

    public function getAction()
    {
        return $this->action;
    }
}
