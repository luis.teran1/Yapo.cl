<?php

/**
 * Class responsible for filtering data.
 *
 * Allow both format array('param' => 'filter') and array('param')
 * In the second case, use a default filter based on param name.
 * The filter is the name of a function or 
 * Now also array(array('filter', 'param1', 'param2')).
 * Filter will then get an array of param1, param2.
 * This is to decrease the number of args.
 */

class blocket_filter
{
	function blocket_filter($param, $value) {
	}

	function populate($data) {
		foreach ($this as $param => $filter) {
			$this->$param = $this->parse_param($param, $filter, $data);
		}
	}

	function parse_param($param, $filter, $data) {
		if (is_array($filter)) {
			$value = array();
			$ps = $filter;
			$filter = array_shift($ps);
			foreach ($ps as $p) {
				if (isset($data[$p]))
					$value[$p] = $data[$p];
				elseif (isset($data[strtoupper($p)]))
					$value[$p] = $data[strtoupper($p)];
				else
					$value[$p] = null;
			}
		} elseif (is_numeric($param)) {
			if (isset($data[$filter]))
				$value = $data[$filter];
			elseif (isset($data[strtoupper($filter)]))
				$value = $data[strtoupper($filter)];
			else
				$value = null;
			$filter = "f_$filter";
		} else {
			if (isset($data[$param]))
				$value = $data[$param];
			elseif (isset($data[strtoupper($param)]))
				$value = $data[strtoupper($param)];
			else
				$value = null;
		}

		if (is_string($filter) && class_exists($filter) && is_subclass_of($filter, 'blocket_filter')) {
			$value = new $filter($param, $value);
			$value->populate($data);
	  	} elseif ($filter) {
			$value = $this->$filter($value);
	  	}
		else {
			trigger_error("Parameter '" . $param . "' is unfiltered", E_USER_WARNING);
		}

		return $value;
	}

	function flatten_param($param, $filter) {
		if (is_array($filter)) {
			/* XXX support this case. */
			return array();
		}
		if (is_numeric ($param)) {
			$filter = "f_$filter";
			$param = $filter;
		}
		if (is_string($filter) && class_exists($filter) && is_subclass_of($filter, 'blocket_filter')) {
			$res = array();
			$obj = new $filter($param);
			foreach (get_object_vars($obj) as $p => $f)
				$res = array_merge($res, $this->flatten_param($p, $f));
			return $res;
		} else
			return array($param => $filter);
	}
	
	/*
	 * Common filters
	 */
	function f_integer($val) {
		return intval($val);
	}

	function f_integer_or_null($val = NULL) {
		if (!isset($val) || is_null($val))
			return NULL;
		return $this->f_integer($val);
	}

	function f_bool($val) {
		return $val != null;
	}

	function f_state($val) {
		if (preg_match("/[^a-z0-9_]/i", $val))
			return null;

		return $val;
	}

}
