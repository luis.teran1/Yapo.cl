<?php
namespace Yapo;

use ProxyClient;

class SmartbumpService
{
    private $Path;
    private $Host;
    private $Port;
    private $Proxy;

    public function __construct()
    {
        global $BCONF;
        $this->Host = Bconf::get($BCONF, "*.smartbump.host");
        $this->Port = Bconf::get($BCONF, "*.smartbump.port");
        $this->Conf = Bconf::get($BCONF, "*.smartbump");
        $this->Proxy = new ProxyClient();
    }

    private function replacePathValues($data, $path)
    {
        if (strpos($path, "%") === false) {
            return $path;
        }
        foreach ($data as $k => $v) {
            $path = str_replace("%".$k, $v, $path);
        }
        return $path;
    }

    private function getPath($action, $data = null)
    {
        if (empty($data)) {
            return $this->Conf[$action]["path"];
        }

        return $this->replacePathValues($data, $this->Conf[$action]["path"]);
    }

    private function getMethod($action)
    {
        return $this->Conf[$action]["method"];
    }

    private function callToService($action, $data = null)
    {
        $headers = array("Content-Type" => "application/json");
        $response = $this->Proxy->doRequest(
            $this->getMethod($action),
            $this->Host,
            $this->Port,
            $this->getPath($action, $data),
            $data,
            $headers
        );

        if ($response->code != 200) {
            Logger::logError(__METHOD__.'::'.$action, "Api error:". var_export($response, true));
            return null;
        }

        $json_response = json_decode($response);
        Logger::logDebug(__METHOD__.'::'.$action, "Response proxy:". var_export($json_response, true));

        return $json_response;
    }

    public function createCustomer(Array $data)
    {
        return $this->callToService("createcustomer", $data);
    }

    public function getReport($data)
    {
        return $this->callToService("fetchreport", $data);
    }

    public function getSubscriptions($data)
    {
        return $this->callToService("getsubscriptions", $data);
    }

    public function expireSubscription(Array $data)
    {
        return $this->callToService("expiresubscription", $data);
    }

}
