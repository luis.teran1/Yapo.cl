<?php
//
// Find diff between text1 (orginal) and text2 (new text).
// Return text with all changes marked (using html-span tags).
//
function text_diff($t1, $t2)
{
	// Shortcut
        if ($t1 == $t2)
                return($t1);

        $result = array();
        $a1 = array();
        $a2 = array();
        $all1 = array();
        $all2 = array();
  
        // Split both texts
        $reg = '/([^ \n\r]+ *)|([\n\r]+)/';
        $all1 = preg_split($reg, $t1, -1, 
                           PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
        $all2 = preg_split($reg, $t2, -1,
                           PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);

        // Clean up insignificant chars.
        foreach ($all1 as $o) {
                array_push($a1, preg_replace("/[^\d\w]/", "", $o));
        }
        foreach ($all2 as $o) {
                array_push($a2, preg_replace("/[^\d\w]/", "", $o));
        }

        $i1 = 0;
        $i2 = 0;

        // Traverse the words
        while ($i1 < count($a1)) {
                $o1 = $a1[$i1]; // Get the original word

                if (($o1 == "" && $i2 >= count($a2)) ||
		    ($i2 < count($a2) && $o1 == $a2[$i2])) {
                        // Keep identical words
                        array_push($result,
                                   ($i2 >= count($a2) || count($all1[$i1]) > count($all2[$i2])) ? $all1[$i1] : $all2[$i2]);
                        $i1++;
                        $i2++;
                } else {
                        // Words differ

                        // Find position for the next "sync"

                        list($n1, $n2) = text_diff_find_sync($a1, $i1,
							     $a2, $i2,
							     1);

                        // Skipped text is a diff
                        if ($n1 != $i1) {
                                $tmp = array();
                                while ($i1 != $n1) {
                                        array_push($tmp, $all1[$i1]);
                                        $i1++;
                                }
                                $text = join("", $tmp);
                                // strip ending white-space
                                $text = preg_replace('/^(.+?)( *)\z/s',
                                                     "<span class='TextDiffOld'>$1</span>$2",
                                                     $text);
                                array_push($result, $text);
                                
                        }

                        if ($n2 != $i2) {
                                $tmp = array();
                                while ($i2 != $n2 && $i2 < count($a2)) {
                                        array_push($tmp, $all2[$i2]);
                                        $i2++;
                                }
                                $text = join("", $tmp);
                                // strip ending white-space
                                $text = preg_replace('/^(.+?)( *)\z/s',
                                                     "<span class='TextDiffNew'>$1</span>$2",
                                                     $text);
                                array_push($result, $text);
                        }
                }
        }

        // Leftowers in the new text
        if ($i2 < count($a2)) {
                array_push($result, "<span class='TextDiffNew'>");
                while ($i2 < count($a2)) {
                        array_push($result, $all2[$i2]);
                        $i2++;
                }
                array_push($result, "</span>");
        }

        // merge and return
        return(join(" ", $result));
}


// Function to find sync between two arrays of words.
// Input: array1, offset1, array2, offset2 and the least
// number of words identifying a sync.
// Returns two offsets indicating sync.
function text_diff_find_sync($a1, $i1, $a2, $i2, $minimum_words)
{
        $min_array = count($a1);
        if ($min_array > count($a2)) {
                $min_array = count($a2);
        }
                
        for ($o = 0; $o < count($a1)-$i1; $o++) {
                for ($i = $o; $i < $min_array; $i++) {
                        $c=0;

                        while ($i1 + $c + $i < $min_array &&
                               $i2 + $c + $i < $min_array &&
                               $a1[$i1 + $c + $i] == $a2[$i2 + $o + $c]) {
                                $c++;
                                if ($c == $minimum_words) {
                                        return(array($i1 + $i, $i2 + $o));
                                }
                        }
                        $c = 0;
                        while ($i1 + $c + $i < $min_array &&
                               $i2 + $c + $i < $min_array &&
                               $a1[$i1 + $c + $o] == $a2[$i2 + $i + $c]) {
                                $c++;
                                if ($c == $minimum_words) {
                                        return (array($i1 + $o, $i2 + $i));
                                }
                        }
                }
        }
        return(array(count($a1), count($a2)));
}

// Test 
function text_diff_test()
{
        $text1 = "kl�dsel;Halvskinn beige. Panelinl�gg valn�t, h�llare f�r\n".
                "skr�pp�se bak,Alu.hjul Lysithea 197/6, B�rkasseh�llare\n".
                "bak, B�lteskudar int.ytterplatser, L�sbara hjulbultar,\n".
                "Telefon ing�r men saknar fj�rranslutning, Fj�rrkontroll i ratten, radio, STC-antispinnsystem,\n".
                ",Larm Volvo Guard m MMS, Stereoop. HU-603 RDS m CD, Belysta\n".
                "makeup speglar,Insynsskydd i lastutrymme, F�rddator.Lastn�t.\n".
                "Vinter och sommard�ck.";
        
        $text2 = "Nyservad feb-06, inv�ndig/utv�ndig.\n".
                "kl�dsel;Halvskinn beige. Panelinl�gg valn�t, h�llare\n".
                "f�r skr�pp�se bak,Alu.hjul Lysithea 195/6, B�rkasseh�llare\n".
                "bak, B�lteskuddar int.ytterplatser, L�sbara hjulbultar,\n".
                "Telefon exkl.fj�rr, Fj�rrkontroll i ratten, radio, STC-antispinnsystem,\n".
                ",Larm med Volvo Pro Guard m MMS, Stereoop. HU-603 RDS m CD, Belysta\n".
                "makeup speglar,Insynsskydd i lastutrymme, F�rddator.Lastn�t.\n".
                "Vinter och sommard�ck.Vid snabb aff�r bjuder vi �ven\n".
                "p� helrekonditionering (in/utv�ndig) v�rd ca 3000kr.";

	/*
$text1 = "nu t�nkte jag s�lja min mobiltelefon

det �r en samsung X660
jag k�pte den f�r en m�nad sedan den  �r lite anv�nd inga reper fins p� den

den fungerar ut m�rkt den �r silver och svart.

kan eventuellt lite prut";

$text2 = "nu t�nkte jag s�lja min mobiltelefon

det �r en samsung X660
jag k�pte den f�r en m�nad sedan den  �r lite anv�nd inga reper fins p� den

den fungerar ut m�rkt den �r silver och svart.";

 $text1 = ". .";
 $text2 = "i . j";
*/

        echo "<html><head><link rel='stylesheet' type='text/css' href='/css/controlpanel.css'></head>\n";
        echo "<body>\n";
        $newtext = text_diff($text1, $text2);
        echo "<pre>";
        echo $newtext;
        echo "</pre>";
        echo "</body></html>\n";
}

$floffa = array();

// text_diff_test();
?>
