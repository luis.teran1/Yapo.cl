<?php

namespace Yapo;

use Redis;
/**
 * Class to manage redis conections
 */
class EasyRedis extends Redis implements RedisInstance
{
    public $key;
    public $prepend;

    public function __construct($key)
    {
        global $BCONF;
        parent::__construct();
        $this->key = $key;
        $this->prepend = Bconf::get($BCONF, "*.$key.redis.prepend");

        //load the user from the Redis
        $redisConf = Bconf::get($BCONF, $this->getRedisRootNode());

        //connect to redis
        $this->connect($redisConf['name'], $redisConf['port'], $redisConf['timeout']);
        if (isset($redisConf['db'])) {
            $this->select($redisConf['db']);
        }
    }

    public function __destruct()
    {
        $this->close();
    }
    //this function is used by the parent class to identify the redis server we want to use
    protected function getRedisRootNode()
    {
        return   "*.common.".$this->key.".host.".$this->prepend;
    }
}
