<?php
require_once("RedisSessionClient.php");

class RedisSessionManager extends RedisSessionClient
{
	var $redis_conection = 'session_transbank';

	function RedisSessionManager($redis_conection = null){
		global $BCONF;		
		if(!is_null($redis_conection)){
			$this->redis_conection = $redis_conection;
		}
	}

	function retrieve_session($session_id) {
		global $BCONF;
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		return $redis->hgetall($session_id);
	}

	function store_session($session_id, $key, $val, $expire = NULL) {
		global $BCONF;
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		if ($expire == NULL) {
			$expire = bconf_get($GLOBALS['BCONF'], "*.redis.session.ttl");
		}
		return $redis->hset($session_id, $key, $val, $expire);
	}
	function rsm_hset($hash,$key,$val){
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		return $redis->hset($hash,$key,$val);
	}

	function rsm_hget($hash,$key){
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		return $redis->hget($hash,$key);
	}

	function rsm_set($key,$val,$expire = NULL){
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		if ($expire === NULL) {
			$expire = bconf_get($GLOBALS['BCONF'], "*.redis.session.ttl");
		}
		return $redis->set($key,$val,$expire);
	}
	function rsm_get($key){
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		return $redis->get($key);
	}
	function rsm_del($key){
		$roptions = $this->session_server_options();
		$redis = new RedisSessionClient($roptions);
		return $redis->delete($key);
	}
	function session_server_options() {
		global $BCONF;
		$roptions = array( 'session_transbank' => array(
					'servers' =>  $GLOBALS['BCONF']['*']['redis']['session']['host'], 
					'debug' => true,
					'id_separator' => bconf_get($GLOBALS['BCONF'], "*.redis.session.id_separator"),
					'timeout' => bconf_get($GLOBALS['BCONF'], "*.redis.session.connect_timeout")
					),
				'session_cars' => array(
					'servers' => 
					array(  'name' => bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.name"),
						'port' => bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.port")
						) ,
					'debug' => false,
					'id_separator' => NULL,
					'timeout' => 500 
					)
				);

		return $roptions[$this->redis_conection];
	}
}
