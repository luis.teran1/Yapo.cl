<?php
require_once("bLogger.php");
require_once("init.php");

/**
 * Base class for servipag
 */
class ServipagBase
{

	function get_key($type)
	{
		global $BCONF;
		$file = bconf_get($BCONF, "*.servipag.$type");
		$fp = fopen($file, "r");
		$txtfirma = fread($fp, 8192);
		fclose($fp);
		return $txtfirma;
	}

	function generate_signature($data)
	{
		$result = "";
		$llavePrivada = $this->get_key("privada");
		try
		{
			openssl_sign($data, $result, $llavePrivada, OPENSSL_ALGO_MD5);
		}
		catch( Exception $e )
		{
			bLogger::logError(__METHOD__, "Error on signature generation: ".$e->getMessage());
		}
		$result = base64_encode($result);
		bLogger::logDebug(__METHOD__, "Generated singature :".$result);
		$this->signature = $result;
		return $result;
	}

	function verify_signature()
	{
		$verify_signature = bconf_get($BCONF, "*.servipag.verify_signature");
		if($verify_signature == "0"){ 
			bLogger::logError(__METHOD__, "SERVIPAG, Verify Signature DISABLED");
			return true;
		}

		$llave = $this->get_key("publica");
		$base64 = base64_decode($this->signature);
		$data = $this->get_signature_data();
		if(openssl_verify($data, $base64, $llave, OPENSSL_ALGO_MD5))
		{
			bLogger::logInfo(__METHOD__, "$this->xml_name Signature is correct");
			return true;
		}
		else
		{
			bLogger::logError(__METHOD__, "$this->xml_name Signature $base64 is not correct");
			return false;
		}
	}

	function get_signature_data()
	{
		$xml_name = isset($this->xml_name) ? $this->xml_name : "xml1";
		$data = "";
		$nodes = bconf_get($BCONF, "*.servipag.nodo_$xml_name");
		foreach($nodes as $key)
		{
			$data .= (string)$this->$key;
		}

		bLogger::logInfo(__METHOD__, "Signature data ($data)");
		return $data;
	}

	/**
	 * Returns the signature for the xml(1,3)
	 */
	function get_signature()
	{
		global $BCONF;
		$data = $this->get_signature_data();
		return $this->generate_signature($data);
	}

}

/**
 * Helper to generate the first XML for servipag
 */
class ServipagXML1 extends ServipagBase
{

	var $CodigoCanalPago;
	var $IdTxPago;
	var $FechaPago;
	var $MontoTotalDeuda;
	var $NumeroBoletas;
	var $NombreCliente;
	var $RutCliente;
	var $EmailCliente;
	var $Version;
	var $IdSubTx;
	var $Identificador;
	var $Boleta;
	var $Monto;
	var $FechaVencimiento;
	var $signature;

	var $xml_name = "xml1";

	function __construct($purchase_id, $amount)
	{
		global $BCONF;
		$current_date = strftime("%Y%m%d");
		$this->CodigoCanalPago = bconf_get($BCONF, "*.servipag.CodigoCanalPago");
		$this->IdTxPago = $purchase_id;
		$this->FechaPago = $current_date;
		$this->MontoTotalDeuda = $amount;
		$this->NumeroBoletas = "1";
		$this->NombreCliente = "";
		$this->RutCliente = "";
		$this->EmailCliente = "";
		$this->Version = 2;
		$this->IdSubTx = "1";
		$this->Identificador = $purchase_id;
		$this->Boleta = "1";
		$this->Monto = $amount;
		$this->FechaVencimiento = $current_date;
	}

	function build_xml()
	{
		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument("1.0", "iso-8859-1");
		$xml->startElement("Servipag");
			$xml->startElement("Header");
				$xml->writeElement("FirmaEPS", $this->get_signature());
				$xml->writeElement("CodigoCanalPago", $this->CodigoCanalPago);
				$xml->writeElement("IdTxPago", $this->IdTxPago);
				$xml->writeElement("FechaPago", $this->FechaPago);
				$xml->writeElement("MontoTotalDeuda", $this->MontoTotalDeuda);
				$xml->writeElement("NumeroBoletas", $this->NumeroBoletas);
				$xml->writeElement("NombreCliente", $this->NombreCliente);
				$xml->writeElement("RutCliente", $this->RutCliente);
				$xml->writeElement("EmailCliente", $this->EmailCliente);
				$xml->writeElement("Version", $this->Version);
			$xml->endElement();
			$xml->startElement("Documentos");
				$xml->writeElement("IdSubTx", $this->IdSubTx);
				$xml->writeElement("Identificador", $this->Identificador);
				$xml->writeElement("Boleta", $this->Boleta);
				$xml->writeElement("Monto", $this->Monto);
				$xml->writeElement("FechaVencimiento", $this->FechaVencimiento);
			$xml->endElement();
		$xml->endElement();
		$xml->endDocument();
		return $xml->outputMemory(TRUE);
	}
}


/**
 * Helper to VERIFY that the XML provided by Servipag has a valid
 * signature
 */
class ServipagXML2 extends ServipagBase
{
	var $xml_name = "xml2";
	var $signature;
	var $IdTrxServipag;
	var $IdTxCliente;
	var $FechaPago;
	var $CodMedioPago;
	var $FechaContable;
	var $CodigoIdentificador;
	var $Boleta;
	var $Monto;

	function __construct($xml_string)
	{
		bLogger::logError(__METHOD__, "SERVIPAG XML2 : $xml_string");
		$xml = new SimpleXMLElement($xml_string);

		$this->signature = (String)$xml->FirmaServipag;
		$this->IdTrxServipag = (String)$xml->IdTrxServipag;
		$this->IdTxCliente = (String)$xml->IdTxCliente;
		$this->FechaPago = (String)$xml->FechaPago;
		$this->CodMedioPago = (String)$xml->CodMedioPago;
		$this->FechaContable = (String)$xml->FechaContable;
		$this->CodigoIdentificador = (String)$xml->CodigoIdentificador;
		$this->Boleta = (String)$xml->Boleta;
		$this->Monto = (String)$xml->Monto;
	}
}


/**
 * Wrapper to generate the xml3 for Servipag.
 *
 * Please note that this XML isn't signed, and the sole
 * purpose of this class is to complete the API wrappers.
 */
class ServipagXML3 extends ServipagBase
{
	var $CodigoRetorno;
	var $MensajeRetorno;
	var $xml_name = "xml3";

	function __construct($code, $message)
	{
		$this->CodigoRetorno = $code;
		$this->MensajeRetorno = $message;
	}

	/**
	 * Builds and returns XML3 as string
	 */
	function build_xml()
	{
		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument("1.0", "iso-8859-1");
		$xml->startElement("Servipag");
			$xml->writeElement("CodigoRetorno", $this->CodigoRetorno);
			$xml->writeElement("MensajeRetorno", $this->MensajeRetorno);
		$xml->endElement();
		$xml->endDocument();
		return $xml->outputMemory(true);
	}
}

/**
 * Wrapper for the XML4 response from Servipag.
 *
 * This XML only needs signature verification.
 */
class ServipagXML4 extends ServipagBase
{
	var $IdTrxServipag;
	var $IdTxCliente;
	var $EstadoPago;
	var $MensajePago;
	var $xml_name = "xml4";

	function __construct($xml_string)
	{
		$xml = new SimpleXMLElement($xml_string);
		$this->signature = str_replace(" ","+", (String)$xml->FirmaServipag);
		$this->IdTrxServipag = (String)$xml->IdTrxServipag;
		$this->IdTxCliente = (String)$xml->IdTxCliente;
		$this->EstadoPago = (String)$xml->EstadoPago;
		$this->MensajePago = (String)$xml->MensajePago;
	}
}


/** SERVIPAG SIMULATOR **
 *
 * Helper to Verify the first XML for servipag simulator
 */
class ServipagXML1Simulator extends ServipagXML1
{

	function __construct($xml_string)
	{
		$xml = new SimpleXMLElement($xml_string);
		$this->signature = (String)$xml->Header->FirmaEPS;
		$this->CodigoCanalPago = (String)$xml->Header->CodigoCanalPago;
		$this->IdTxPago = (String)$xml->Header->IdTxPago;
		$this->FechaPago = (String)$xml->Header->FechaPago;
		$this->MontoTotalDeuda = (String)$xml->Header->MontoTotalDeuda;
		$this->NumeroBoletas = (String)$xml->Header->NumeroBoletas;
		$this->NombreCliente= (String)$xml->Header->NombreCliente;
		$this->RutCliente = (String)$xml->Header->RutCliente;
		$this->EmailCliente = (String)$xml->Header->EmailCliente;
		$this->Version = (String)$xml->Header->Version;
		$this->IdSubTx = (String)$xml->Documentos->IdSubTx;
		$this->Identificador = (String)$xml->Documentos->Identificador;
		$this->Boleta = (String)$xml->Documentos->Boleta;
		$this->Monto = (String)$xml->Documentos->Monto;
		$this->FechaVencimiento = (String)$xml->Documentos->FechaVencimiento;
	}
}

/**
 * Helper to generate that the XML2 servipag simulator
 */
class ServipagXML2Simulator extends ServipagXML2
{

	function __construct($IdTxCliente, $IdTrxServipag, $CodigoIdentificador, $FechaPago, $Boleta, $Monto)
	{
		$this->IdTrxServipag = $IdTrxServipag;
		$this->IdTxCliente = $IdTxCliente;
		$this->FechaPago = $FechaPago;
		$this->CodMedioPago = "Simulador";
		$this->FechaContable = $FechaPago;
		$this->CodigoIdentificador = $CodigoIdentificador;
		$this->Boleta = $Boleta;
		$this->Monto = $Monto;
	}

	function build_xml()
	{
		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0','ISO8859-1');
		$xml->startElement('Servipag');
		   $xml->writeElement('FirmaServipag',$this->get_signature());
		   $xml->writeElement('IdTrxServipag', $this->IdTrxServipag);
		   $xml->writeElement('IdTxCliente', $this->IdTxCliente);
		   $xml->writeElement('FechaPago', $this->FechaPago);
		   $xml->writeElement('CodMedioPago',$this->CodMedioPago);
		   $xml->writeElement('FechaContable', $this->FechaPago);
		   $xml->writeElement('CodigoIdentificador', $this->CodigoIdentificador);
		   $xml->writeElement('Boleta', $this->Boleta);
		   $xml->writeElement('Monto', $this->Monto);
		$xml->endElement();
		$xml->endDocument();
		return $xml->outputMemory(TRUE);
	}
}

/**
 * Helper to verify the xml3 for Servipag simulator.
 *
 */
class ServipagXML3Simulator extends ServipagBase
{

	function __construct($xml_string)
	{
		$xml = new SimpleXMLElement($xml_string);
		$this->CodigoRetorno = (String) $xml->CodigoRetorno;
		$this->MensajeRetorno = (String) $xml->MensajeRetorno;
	}

}

/**
 * Helper to generate the XML4 Servipag Simulator.
 *
 */
class ServipagXML4Simulator extends ServipagXML4
{
	function __construct($IdTrxServipag, $IdTxCliente, $EstadoPago, $MensajePago)
	{
		$this->IdTrxServipag = $IdTrxServipag;
		$this->IdTxCliente = $IdTxCliente;
		$this->EstadoPago = $EstadoPago;
		$this->MensajePago = $MensajePago;
	}

	function build_xml()
	{
		$xml = new XMLWriter();
		$xml->openMemory();
		$xml->startDocument('1.0','ISO8859-1');
		$xml->startElement('Servipag');
		   $xml->writeElement('FirmaServipag',$this->get_signature());
		   $xml->writeElement('IdTrxServipag',$this->IdTrxServipag);
		   $xml->writeElement('IdTxCliente', $this->IdTxCliente);
		   $xml->writeElement('EstadoPago', $this->EstadoPago);
		   $xml->writeElement('MensajePago',$this->MensajePago);
		$xml->endElement();
		$xml->endDocument();

		return $xml->outputMemory(TRUE);
	}
}
// $spg = new ServipagXML1("123", "1990");
// echo $spg->build_xml();

/*
$xml2 = <<<EOF
<Servipag>
	<FirmaServipag>FIRMASERVIPAG</FirmaServipag>
	<IdTrxServipag> IDTRXSERVIPAG </IdTrxServipag>
	<IdTxCliente>IDTXCLIENTE</IdTxCliente>
	<FechaPago>FECHAP AGO</FechaPago>
	<CodMedioPago>CODMEDIOP AGO</CodMedioPago>
	<FechaContable>FECHACONT ABLE</FechaContable>
	<CodigoIdentificador>CODIGOIDENTIFICADOR</CodigoIdentificador>
	<Boleta>BOLETA</Boleta>
	<Monto>MONTO</Monto>
</Servipag>
EOF;
 */

// $spg = new ServipagXML4("12312" , "ola ke ase");
// echo $spg->build_xml();
// echo ((string)$spg->verify_signature());
