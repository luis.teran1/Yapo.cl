<?php
require_once("util.php");
require_once("init.php");

use Yapo\Bconf;
use Yapo\Logger;

class AccountSession {
	private $redis;

	private $prefix;
	private $cookie_name;
	private $account_params;
	private $session_params;

	private $retry_prefix;
	private $retry_ttl;
	private $retry_max;

	public $account_id;
	public $session_id;
	public $email_hash;
	private $error;
	private $session_prefix;

	private $data;

	function __construct() {
		$this->prefix =  Bconf::get($BCONF, "*.accounts.session_redis_prefix");
		$this->session_prefix = $this->prefix.Bconf::get($BCONF, "*.accounts.session_redis_key_prefix");
		$this->retry_prefix = $this->prefix.Bconf::get($BCONF, "*.accounts.retry_login.key_prefix");
		$this->retry_ttl = Bconf::get($BCONF, "*.accounts.retry_login.ttl");
		$this->retry_max = Bconf::get($BCONF, "*.accounts.retry_login.max");
		$this->cookie_name =  Bconf::get($BCONF, "*.accounts.session_cookie_name");
		$this->cookie_domain = Bconf::get($BCONF, "*.common.session.cookiedomain");
		$this->account_params = explode(",",Bconf::get($BCONF, "*.accounts.session_data"));
		$this->session_params = explode(",",Bconf::get($BCONF, "*.accounts.session_params"));

		$roptions = array(
			'servers' =>  Bconf::get($BCONF, "*.redis.session.host"),
			'debug' => false,
			'id_separator' => Bconf::get($BCONF, "*.redis.session.id_separator"),
			'timeout' => Bconf::get($BCONF, "*.redis.session.connect_timeout")
		);

		Logger::logDebug(__METHOD__, "create redis session client");
		$redis_conf = Bconf::get($BCONF, "*.redis.session.host.rsd1");
		$this->redis = new Redis();
		$this->redis->connect($redis_conf['name'], $redis_conf['port'], $redis_conf['timeout']);

		Logger::logDebug(__METHOD__, "loading session from cookie");

		$this->load();
	}

	/**
	 * Verify if email is blocked in redis
	 */
	function is_blocked($email) {
		$email = strtolower($email);
		$retries = $this->redis->get($this->retry_prefix.$email);
		if ( $retries !== false && $retries >= $this->retry_max )
			return true;
		return false;
	}

	/**
	 * Increment counter of login attempts only for existents account with status distinct to pending_confirmation
	 */
	function incr_retry_login($email) {
		$account_exist = $this->redis->hexists($this->prefix.sha1($email), 'account_id');
		if ($account_exist && $this->redis->hget($this->prefix.sha1($email), 'account_status') != 'pending_confirmation'){
			$retries = $this->redis->get($this->retry_prefix.$email);
			# Incr counter attempts of login
			$this->redis->multi();
			$this->redis->incr($this->retry_prefix.$email);
			if( $retries === false )
				$this->redis->expire($this->retry_prefix.$email, $this->retry_ttl);
			$this->redis->exec();

			// Send email to recover passwd
			if (($retries + 1) == $this->retry_max) {
				$email = $this->redis->hget($this->prefix.sha1($email), 'email');
				$name = $this->redis->hget($this->prefix.sha1($email), 'name');
				$trans = new bTransaction();
				$trans->add_data("email", $email);
				$trans->add_data("nickname", $name);
				$trans->add_data("mail_type", "login_max_attempts" );
				$reply = $trans->send_command('accountmail');
			}
		}
	}

	/**
	 * Set counter of login attempts to retry_max only for existents accounts
	 * Used by CP to lock directly without send email
	 * return true: user was blocked
	 * return false: account not exists
	 * return null: user was not blocked, by some error
	 */
	function lock_account($email) {
		$account_exist = $this->redis->hexists($this->prefix.sha1($email), 'account_id');
		if($account_exist){
			try{
				$this->redis->multi();
				$this->redis->incrby($this->retry_prefix.$email, $this->retry_max);
				$this->redis->expire($this->retry_prefix.$email, $this->retry_ttl);
				$this->redis->exec();
				return true;
			}catch(Exception $e) {
				return null;
			}
		}
		return false;
	}

	/**
	 * Unlock account, deleting key in redis only for existents accounts
	 * return true: user was unlocked
	 * return false: account not exists
	 * return null: user was not unlocked, by some error
	*/
	function unlock_account($email) {
		$account_exist = $this->redis->hexists($this->prefix.sha1($email), 'account_id');
		if($account_exist){
			try{
				$this->redis->del($this->retry_prefix.$email);
				return true;
			}catch(Exception $e) {
				return null;
			}
		}
		return false;
	}

	function login($email, $password, $long_session = 0) {
		$email = strtolower($email);
		if (!empty($email) && !empty($password)) {
			if( !$this->is_blocked($email) && $this->authenticate($email, $password ) ) {
				$this->data['email'] = $email;
				$this->session_id = $this->generate_session_id();
				$this->email_hash = $this->generate_email_hash($email);
				$this->store($this->session_id, $long_session);
				$this->redis->del($this->retry_prefix.$email);
				return true;
			}
			$this->incr_retry_login($email);
		}
		return false;
	}

	function loginSpecial($email, $first_time = false){
        Logger::logDebug(__METHOD__, "loginSpecial for: {$email}");

		$email = strtolower($email);

		if($this->authenticateSpecial($email, $first_time)) {
			$this->data['email'] = $email;
			$this->data['first_time'] = $first_time;
			$this->session_id = $this->generate_session_id();
			$this->email_hash = $this->generate_email_hash($email);
			$this->store($this->session_id, 0);
			$this->unlock_account($email);
			return true;
		}
		return false;
	}


	private function generate_email_hash($email) {
		return sha1($email);
	}

	private function generate_session_id() {
		return sha1(microtime(true).uniqid(rand(), true));
	}

	public function sendCookie($session_id, $expire){
		$timeToExpire = time() + $this->data['ttl'];
		if ($expire) {
			$timeToExpire = time() - 3600;
		}

		setcookie(
			$this->cookie_name,          //name
			$session_id,                 // value
			$timeToExpire, // expire
			'/',                         // path
			$this->cookie_domain,        // domain
			false, // secure
			true   // httponly
		);
	}

	function store($session_id, $long_session) {
		$this->data['ttl'] = Bconf::get($BCONF, '*.accounts.session_timeout');
		if(!empty($long_session)){
			$this->data['ttl'] = Bconf::get($BCONF, '*.accounts.session_long_timeout');
		}
		$this->sendCookie($session_id, false);

		$profile = array();
		foreach($this->session_params as $key_param) {
			$account_parameter = @$this->data[$key_param];
			if(isset($account_parameter) and !empty($account_parameter)) {
				$profile[$key_param] = $account_parameter;
			}
		}

		$this->redis->hMSet($this->session_prefix.$session_id, $profile);
		$this->redis->expire($this->prefix.$session_id, $this->data['ttl']);
	}

	function expire($session_id = 0){
		if(empty($session_id)){
			$session_id = $this->session_id;
		}
		$this->sendCookie($session_id, true);
		$this->redis->del($session_id);
		$this->redis->del($this->session_prefix.$session_id);
		unset($this->session_id);
		unset($this->data);
	}

	function load() {
		if (empty($_COOKIE[$this->cookie_name])) {
			Logger::logDebug(__METHOD__, "Account cookie not found!");
			if(!empty($this->session_id)) {
				$this->expire($session_id);
			}
			return false;
		}
		Logger::logDebug(__METHOD__, "Account cookie: ".$_COOKIE[$this->cookie_name]);

		$session_id = $_COOKIE[$this->cookie_name];
		$session_email = $this->redis->hGet($this->session_prefix.$session_id, "email");

		if(!empty($session_email)) {
			$this->session_id = $session_id;
			$this->email_hash = $this->generate_email_hash($session_email);
			$this->data = $this->redis->hGetAll($this->prefix.$this->email_hash);
			Logger::logDebug(__METHOD__, "redis hash: ".$this->session_prefix.$session_id);
			if (! isset($this->data['email'])){
				$trans = new bTransaction();
				$trans->add_data('email', $session_email);
				$this->data = $trans->send_command('get_account');
				unset($this->data["status"]);
			}
			foreach($this->session_params as $key_param) {
				$this->data[$key_param] = $this->redis->hGet($this->session_prefix.$session_id, $key_param);
			}
		}
		else {
			/* If the session id doesn't exist in Redis remove the user cookie  */
			$this->expire($session_id);
		}
	}

	function is_logged() {
		return isset($this->data['account_id']);
	}

	function get_account_id() {
		return @$this->data['account_id'];
	}

	function get_session_id() {
		if(isset($this->session_id))
			return 	$this->session_id;
		return null;
	}

	function authenticate($email, $password) {
		$reply = $this->obtainAccountInfo($email);
		Logger::logDebug(__METHOD__, "reply is: ".print_r($reply, true));
		if($reply['status'] != "TRANS_OK") {
			if(!empty($reply['error']))
				$this->error=$reply['error'];
			else
				$this->error=$reply['email'];
			return false;
		}

		$salt =  $reply['salt'];
		$salted_pass =  $reply['password'];
		if ($reply['account_status'] != "active") {
			Logger::logError(__METHOD__, "ERROR: account authentication failed for: $email. Account not active");
			$this->error = "ACCOUNT_NOT_ACTIVE";
			return false;
		} else if($this->verify_passwd($password,$salted_pass,$salt)) {
			$this->set_account_info($reply);
			return true;
		} else {
			Logger::logDebug(__METHOD__, "ERROR: account authentication failed. Password didn't match");
			return false;
		}

	}

	function authenticateSpecial($email, $first_time = false) {
		$reply = $this->obtainAccountInfo($email);
		if($reply['status'] != "TRANS_OK") {
			$this->error=@$reply['error'];
			return false;
		}
		if ($reply['account_status'] != "active" && !$first_time) {
			Logger::logNotice(__METHOD__, "ERROR: account authentication failed for: $email. Account not active");
			$this->error = "ACCOUNT_NOT_ACTIVE";
			return false;
		}
		$this->set_account_info($reply);
		return true;
	}

	function verify_passwd($user_passwd,$salted_passwd, $salt){
		$loops = Bconf::get($BCONF, "ai.passwd.stretch_loops");
		if (preg_match('/^\$([0-9]+)\$(.*)/', $salt, $regs)) {
			$loops = $regs[1];
			$salt = $regs[2];
		}
		$passwd = $user_passwd;
		for ($i = 0; $i < $loops; $i++)
			$passwd = sha1($salt . $passwd);
		$passwd = "\$$loops\$$salt$passwd";
		return $passwd == $salted_passwd;
	}

	public function obtainAccountInfo($email) {
		$trans = new bTransaction();
		$trans->add_data('email', $email);
		$reply = $trans->send_command('get_account');
		if ($reply['status'] != "TRANS_OK" ) {
			Logger::logWarning(__METHOD__, "failed to get account info for: $email");
		}

		if (isset($reply['total_ads']) && $reply['inactive_ads']) {
			$this->data['total_ads'] = $reply['total_ads'];
			$this->data['inactive_ads'] = $reply['inactive_ads'];
			$this->data['total_active_ads'] = $this->data['total_ads'] - $reply['inactive_ads'];
		} else {
			$this->data['total_ads'] = 0;
			$this->data['inactive_ads'] = 0;
			$this->data['total_active_ads'] = 0;
		}

		return $reply;
	}

	function reload_account_info() {
		Logger::logDebug(__METHOD__, "Reloading account data for {$this->get_param('email')}");
		$reply = $this->obtainAccountInfo($this->get_param('email'));
		Logger::logDebug(__METHOD__, print_r($reply, true));
		$this->set_account_info($reply);

	}

	function reload_redis_info() {
		$this->store($this->session_id, 0);
	}

	function set_account_info($info) {
		foreach($this->account_params as $key_param){
			if($key_param != "ttl" && isset($info[$key_param]))
				$this->data[$key_param] = $info[$key_param];
		}
	}

	function set_param($key, $value, $save_on_redis = true){
		$this->data[$key] = $value;
		if($save_on_redis) {
			$long_session = ($this->data['ttl'] == Bconf::get($BCONF, '*.accounts.session_timeout') ? 0 : 1);
			$this->store($this->session_id, $long_session);
		}
	}

	public function getAccountInfo(){
		return $this->data;
	}

	function get_param($key){
		return @$this->data[$key];
	}

	function get_error(){
		return @$this->error;
	}

}

