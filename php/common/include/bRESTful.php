<?php

require_once('JSON.php');

use Yapo\Metrics\MetricsAgentFactory;

abstract class bRESTful {

	private $resource;
	private $verb;
	protected $metrics_agent;

	abstract public function __construct();

	protected function method_not_allowed() {
		self::setStatusCode(405);
	}

	protected function resource_not_found() {
		self::setStatusCode(404);
	}

	protected function server_error() {
		self::setStatusCode(500);
	}

	protected function bad_request() {
		self::setStatusCode(400);
	}
	public static function setStatusCode($code) {
		header(' ', true, $code);
	}

	public function run() { 
		$this->application_name = get_class($this);
		
		header('Content-type: application/json');

		global $BCONF;
		$data = array();

		$path_info = explode("/", $_SERVER["PATH_INFO"]);
		//get url data
		array_shift($path_info);

		//get request data
		$request_data = file_get_contents('php://input');
		bLogger::logDebug( __METHOD__, "Request data: " . $request_data );

		$this->resource = array_shift($path_info);
		$this->verb = $_SERVER['REQUEST_METHOD'];
		get_settings(bconf_get($BCONF, $this->restAPISettingsKey), "methods", array($this, "keyLookup"), array($this, "setValues"), $data);

		// Track the specific application
		$this->setup_metrics();
		$this->set_transaction_name($data);

		// Pseudo dependency injection (beware class with constructor arguments)
		$args = array();
		if (array_key_exists('inject', $data)) {
			foreach (explode(',', $data['inject']) as $inj) {
				$args[] = new $inj;
			}
		}

		// get the class in charge on handling the resource
		$class = new ReflectionClass($data['class']);
		$instance = $class->newInstanceArgs($args);

		$validationErrors = $this->validateParams($request_data, $data['method']);
		if ( $validationErrors != null ) {
			bLogger::logDebug( __METHOD__, "Result: " . print_r($validationErrors, true));
			echo yapo_json_encode( $validationErrors );
			$this->bad_request();
			return;			
		}

		// call the method of the verb
		$url_data = $this->parseQueryString();
		$result = null;
		try{
			$result = $instance->$data["method"]($url_data, json_decode($request_data));
		}catch(Exception $e){
			bLogger::logError( __METHOD__, "ERROR:Calling method: ".$data["method"]." with url_data:" . print_r( $url_data, true )." (Check url_data)\n".var_export($e->getMessage(),true) );
		}
		//Return the result as json
		bLogger::logDebug( __METHOD__, "Result: " . print_r( $result, true ) );
		// if the result is NULL return an empty json '{}'
		if ($result == null)
			echo '{}';
		else
			echo yapo_json_encode($result, true);
	}

	//this function returns true if the params are valid, otherwise return the array with the param errors
	private function validateParams($params_data, $app_method) {

		// get the schema template using the bResponse class
		$response = new bResponse();
		$response->add_data("app_method", $app_method);
		$schema_data = yapo_json_decode($response->call_template("schema_generator.json", true), true);

		// if there is no schema, consider the json valid
		if ( empty($schema_data) ) return null;

		$validator = new JsonSchema();
		if ( $validator->validate( $schema_data, yapo_json_decode( $params_data ) ) ) {
			return null;
		} 	
		
		return $validator->getErrors();

	}

	private function parseQueryString() {

		$data = array();
		$items = explode('&', $_SERVER['QUERY_STRING']);
		// get all the items from query string
		foreach($items as $item) {
			if(empty($item)) continue;

			// decode the values from url encoding
			list($name, $value) = explode('=', $item);
			$name = urldecode($name);
			$value = urldecode($value);
			// if it has multiple params, put it in an array
			if (array_key_exists($name, $data)) {
				if (is_array($data[$name])) {
					array_push($data[$name], $value);
				} else {
					$data[$name] = array($data[$name], $value);
				}
			} else {
				$data[$name] = $value;
			}
		}

		return $data;
	}

	private function setValues($setting, $key, $value, $data) {
		$data[$key] = $value;
	}

	private function keyLookup($setting, $key, $data) {
		$ret;
		switch($key) {
			case "resource":
				$ret = $this->resource;
				break;
			case "verb":
				$ret = $this->verb;
				break;
		}
		return $ret;
	}

	protected function setup_metrics() {
		$this->metrics_agent = MetricsAgentFactory::getMetricsAgent($BCONF, 'bRESTful');
	}

	protected function set_transaction_name($data) {
		$name = str_replace("\\", "/", $data['class']);
		$name = preg_replace (",^/?Yapo/,", "", $name);
		$txnName = sprintf('/%s/%s', $name, $data['method']);
		$this->metrics_agent->setTransactionName($txnName);
	}
}
