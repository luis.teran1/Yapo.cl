<?php
openlog("upload_logo", LOG_ODELAY, LOG_LOCAL0);

require_once('blocket_application.php');

class blocket_upload_logo extends blocket_application {

	function blocket_upload_logo() {
		global $BCONF;

		$this->page_title = "Ladda upp bild";
		$this->page_name = "Ladda upp bild";
		
		$this->init('form', 1);
	}

	function get_state($name) {
		switch ($name) {
		case 'form':
			return array('function' => 'form',
				     'method' => 'get');
		case 'upload':
			return array('function' => 'upload',
				     'method' => 'post',
				     'params' => array('name', 'couple' => 'f_name'));
		case 'done':
			return array('function' => 'done',
				     'method' => 'get',
				     'allowed_states' => array('done'));
		}
	}

	function form() {
		$this->display_layout('upload_logo/form.html');
	}

	function upload($name, $couple) {
		global $BCONF;

		$errors = array();
		if (strlen($name) < 2)
			$errors['name'] = 'ERROR_NAME_TOO_SHORT';
		if (strlen($name) < 2)
			$errors['couple'] = 'ERROR_NAME_TOO_SHORT';
		if (!isset($_FILES['image']) || !$_FILES['image']['name'])
			$errors['image'] = 'ERROR_IMAGE_MISSING';

		if ($errors) {
			$this->set_errors($errors);
			return 'form';
		}

		ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
		ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));


		$image = $_FILES['image']['tmp_name'];
		list($width, $height, $type, $attr) = @getimagesize($image);

		$filename = str_replace(' ', '_', "$name $couple") . '_' . strftime("%Y%m%d%H%M%S") . '.jpg';
		switch ($type) {
		case IMAGETYPE_GIF:
			$image = @imagecreatefromgif($image);
			break;
		case IMAGETYPE_JPEG:
			$image = @imagecreatefromjpeg($image);
			break;
		case IMAGETYPE_PNG:
			$image = @imagecreatefrompng($image);
			break;
		case IMAGETYPE_BMP:
			$image = @imagecreatefrombmp($image);
			break;
		default:
			$image = null;
			break;
		}
		if (!$image) {
			$this->set_errors(array('image' => 'ERROR_IMAGE_TYPE'));
			return 'form';
		}
		ob_start();
		imagejpeg($image, '', 85);
		$image = ob_get_contents();
		ob_end_clean();

		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->add_file('image', $image);
		$transaction->add_data('date', strftime("%Y%m%d"));
		$transaction->add_data('directory', 'logos');
		$transaction->add_data('filename', $filename);
		$reply = $transaction->send_command('imgput_single');
		if ($transaction->has_error()) {
			die_with_template(__FILE__, log_string() . ": failed to upload logo");
			/* Not reached */
		}
		return 'done';
	}

	function done() {
		$this->display_layout('upload_logo/done.html');
	}

	function f_name($val) {
		return preg_replace('/[.\/]/', '', $val);
	}
}

$upload_logo = new blocket_upload_logo();
$upload_logo->run_fsm();
?>
