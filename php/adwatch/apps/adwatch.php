<?php
/*
 * Adwatch application
 */

include_once('init.php');
include_once('util.php');
include_once('autoload_lib.php');
require_once('dyn_config.php');

openlog("adwatch", LOG_ODELAY, LOG_LOCAL0);
$adwatch = new blocket_adwatch();
$adwatch->run_fsm();

