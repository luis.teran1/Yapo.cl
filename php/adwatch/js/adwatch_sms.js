function adwatch_sms_code_cb(result, xmlhttp, link) {
	if (result) {
		var code_span = document.getElementById("sms_code");

		if (result.code && typeof(result.code) == "number") {
			code_span.innerHTML=result.code;
			display_confirm_box(null, 'sms_row', 'block');
		}
		else if(result.reload) {
			document.location = document.location;
		}
		
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		var newbody = xmlhttp.responseText;
		newbody = newbody.replace(/(.|\n)*\<body\>/, "");
		newbody = newbody.replace(/\<\/body\>[^<]*\<\/html\>[^<]*$/m, "");
		document.body.innerHTML = newbody;
		return;
	}
}

function adwatch_sms_code(url, query_id) {
	ajax_request(url + "?query_id=" + query_id, '', adwatch_sms_code_cb, undefined, true, "GET");
	return false;
}

