/*
 * Displays a confirm delete adwatch div to the user
 */
function display_confirm_box(link, id, display) {
	var containers = getElementsByClassName(document, '*', 'adwatch_msg_box');

	for(var i = 0; i < containers.length; i++) {
		containers[i].style.display = 'none';
	}

	var container = document.getElementById(id);
	if(!container)
		return true;

	containers = container.getElementsByTagName('a');
	if(containers.length > 0 && link)
		containers[0].href = link.href;

	container.style.display = display;

	return false;
}

function hide_sms_box(id) {
	var row = document.getElementById(id);
	if (!row)
		return false;
	row.style.display='none';
	return false;
		
}

function watchFavoritesCount() {

	var el = document.getElementById('watch-favorites');
	if(el !== null) {
        var favObj = JSON.parse(window.localStorage.getItem('lastFavs'));
        el.innerHTML(favObj.length);
	}
}

watchFavoritesCount();
