TOPDIR?=../../..

include ${TOPDIR}/mk/commonsrc.mk
VPATH:=${COMMONSRCDIR}:${VPATH}

INSTALLDIR=/www-ssl/img

INSTALL_TARGETS=add_watch.gif
INSTALL_TARGETS+=favourites_search.gif favourites_save.gif
#INSTALL_TARGETS+=icon_presentation.gif icon_save.gif icon_mobile.gif ikon_sokhjalp.gif
INSTALL_TARGETS+=tab-*.png
INSTALL_TARGETS+=crea_busqueda.gif
INSTALL_TARGETS+=header_sms.jpg sms_example_bg.gif sms.gif

include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
