<?php
	require_once("blocket_application.php");
	require_once("JSON.php");
	require_once('bRedisClass.php');
	require_once("Servipag.php");
	require_once('ConfirmPayment.php');

	class ServipagConfirmation {

		const AUTHORIZED =  'AUTHORIZED';

		public function authorize($xml){
			bLogger::logDebug( __METHOD__ , "Received xml2 from servipag: " . print_r($xml, true));
			$xml = new ServipagXML2($xml);
			$payment_group_id = (int)$xml->IdTxCliente;
			$svpg_code_status = -1;
			$svpg_msg_status = "Transaction NOK";

			$status = $this->authorize_svpg($xml);
			if ($status == 'authorized') {
				$confirmation = new \Yapo\ConfirmPayment();
				$clear_status = $confirmation->validatePayment($payment_group_id);
				if($clear_status == 'CLEAR_SUCCESS'){
					$apply_products = $confirmation->applyProducts($payment_group_id);
					if ($apply_products['status'] == self::AUTHORIZED) {
						$svpg_code_status = 0;
						$svpg_msg_status = "Transaction OK";
						if ($confirmation->hasError()) {
							$confirmation->markAsError($payment_group_id);
						}
					}
				}
			}
			if($status == 'reauthorized') {
				$svpg_code_status = 0;
				$svpg_msg_status = "Transaction OK";
			}
			$servipag_xml3 = new ServipagXML3($svpg_code_status, $svpg_msg_status);
			return $servipag_xml3->build_xml();
		}
		
		
		public function authorize_svpg($xml){
			if($xml->verify_signature()){
				$payment_group_id = (int)$xml->IdTxCliente;
				$purchase = $this->get_purchase($payment_group_id);
				$returnPayment = $purchase['purchase_get'][0];
				$status = 'Unauthorized';

				if (count((array) $returnPayment) == 0) {
					$status = "Unauthorized";
					bLogger::logError( __METHOD__ , "Servipag payment group not exists: " . print_r($xml, true));
					return $status;
				}
				$tbk_state = $this->get_tbk_state($payment_group_id);
				if(is_null($tbk_state)) {
					bLogger::logDebug(__METHOD__,  "Servipag purchase state not found: ". print_r($xml, true));
					$status = "Unauthorized";
					return $status;
				}
				if(!is_array($tbk_state['payment_groups_status']))
					$tbk_state['payment_groups_status'] = Array($tbk_state['payment_groups_status']);
				if(!is_array($tbk_state['total_price']))
					$tbk_state['total_price'] = Array($tbk_state['total_price']);
				if(!is_array($tbk_state['pay_log_status']))
					$tbk_state['pay_log_status'] = Array($tbk_state['pay_log_status']);

				for($i = 0; $i < count($tbk_state['payment_groups_status']) ; $i++) {
					if($tbk_state['payment_groups_status'][$i] != 'unverified' || $tbk_state['pay_log_status'][$i] != 'SAVE') {
						if($tbk_state['payment_groups_status'][$i] == 'paid' && $tbk_state['pay_log_status'][$i] == 'OK'){
							bLogger::logDebug(__METHOD__,  "SERVIPAG RE-Authorized order $payment_group_id");
							return "reauthorized";
						}
						bLogger::logDebug(__METHOD__,  "Order Duplicated");
						$status = "Unauthorized";
						return $status;
					}
				}

				$trans = new bTransaction();
				$trans->add_data('tbk_purchase_order', $payment_group_id);
				$trans->add_data('tbk_ref_auth_code', (string)$xml->IdTrxServipag);
				$trans->add_data('tbk_state', "authorized");
				$trans->send_command("tbk_add_state");
				if ($trans->has_error()) {
					bLogger::logError( __METHOD__ , "Trans error add state servipag with payment_group $payment_group_id");
				}
				else {
					$status = "authorized";
					bLogger::logDebug( __METHOD__ , "Authorizing servipag payment");
				}
			} 
			else {
				$status = "Unauthorized";
				bLogger::logError( __METHOD__ , "Invalid xml signature: " . print_r($xml, true));
			}
			bLogger::logDebug( __METHOD__ , "Finalización authorize");
			return $status;
		}

		
		function get_purchase($orden) {
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			if(isset($orden) && !empty($orden))
				$transaction->add_data('payment_group_id', $orden);
			$reply = $transaction->send_command('purchase_get');

			if($transaction->has_error() || empty($reply['status']) )
				return null;

			return $reply;
		}

		function get_tbk_state($TBK_ORDEN_COMPRA) {
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			if(isset($TBK_ORDEN_COMPRA) && !empty($TBK_ORDEN_COMPRA))
				$transaction->add_data('payment_group_id', $TBK_ORDEN_COMPRA);

			$reply = $transaction->send_command('tbk_get_state');

			if($transaction->has_error() || empty($reply['status']) )
				return null;

			return $reply;
		}
	}
?>
