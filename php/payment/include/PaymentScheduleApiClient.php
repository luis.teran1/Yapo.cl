<?php

require_once("JSON.php");
require_once('bRedisClass.php');
require_once('APIClient.php');

class PaymentScheduleApiClient
{
    protected $client;
    public $email;
    public $accSession;
    public $accountId;
    public $productId;
    public $billingType;
    public $adminEmail;
    public $accountName;

    public $subscriptionId;
    public $newStatus = "active";
    public $searchType = "all";

    private $bconfApi = "*.common.base_url.ms_payment_schedule_api";
    private $bconfMethods = "*.ms_payment_schedule_api_client.methods.";

    public function __construct()
    {
        $this->client = new APIClient($this->bconfApi, $this->bconfMethods);
    }

    public function createSubscription()
    {
        try {
            $createData = array(
                "SubscriberID" => $this->accountId,
                "Email" => $this->email,
                "Products" => array($this->productId),
                "AdminEmail"=> $this->adminEmail,
                "BillingType"=> $this->billingType,
                "Name"=> $this->accountName
            );
            $resp = $this->client->call('create', $createData);
            $result = json_decode($resp);
            if ($result->subscription) {
                return array(true, $result->subscription);
            }
            bLogger::logError(__METHOD__, "ERROR MS pay response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "DATA NOT FOUND");
    }

    public function searchSubscriptions()
    {
        try {
            if (isset($this->accSession)){
               $search = "acc/{$this->accSession}/status/{$this->searchType}";
            } else {
               $search = "{$this->email}/status/{$this->searchType}";
            }
            $resp = $this->client->call('search', $search);
            $result = json_decode($resp);
            if ($result->subscriptions) {
                return array(true, $result->subscriptions);
            }
            bLogger::logError(__METHOD__, "ERROR MS search response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "DATA NOT FOUND");
    }

    /**
     * Func to handle payment schedule report MS calls and return its response
     */
    public function reportByRangeSubscription($hasEmail = false)
    {
        $endpoint = 'report';
        if ($hasEmail) {
            $endpoint .= '_email';
        }
        try {
            $resp = $this->client->call($endpoint, $this->params);
            $result = json_decode($resp);
            if (count($result->report) > 0) {
                return array(true, $result->report);
            }
            bLogger::logError(__METHOD__, "ERROR MS search response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            $err = json_decode($e->getMessage());
            if (isset($err->error)) {
                return array(false, "ERROR MS " . $err->error);
            }
            return array(false, "ERROR MS");
        }
        return array(false, "DATA NOT FOUND");
    }

    public function statusSubscription()
    {
        try {
            $createData = array(
                "id" => intval($this->subscriptionId),
                "newStatus"=> $this->newStatus
            );
            $resp = $this->client->call('status', $createData);
            $result = json_decode($resp);
            if ($result->subscription) {
                return array(true, $result->subscription);
            }
            bLogger::logError(__METHOD__, "ERROR MS change status response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "DATA NOT FOUND");
    }

    public function reactivateSubscription()
    {
        try {
            $createData = array(
                "id" => intval($this->subscriptionId)
            );
            $resp = $this->client->call('reactivate', $createData);
            $result = json_decode($resp);
            if ($result->subscription) {
                return array(true, $result->subscription);
            }
            bLogger::logError(__METHOD__, "ERROR MS change status response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "DATA NOT FOUND");
    }

    public function paySubscription()
    {
        try {
            $createData = array(
                "subscriptionID" => intval($this->subscriptionId),
            );
            $resp = $this->client->call('pay', $createData);
            $result = json_decode($resp);
            if ($result->payment) {
                return array(true, $result->payment);
            }
            bLogger::logError(__METHOD__, "ERROR MS change status response: ".print_r($resp, true));
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, "MS Exception".print_r($e, true));
            return array(false, $e->getMessage());
        }
        return array(false, "DATA NOT FOUND");
    }
}
