<?php

namespace Yapo;

use TransbankConfirmation;

class WebpayApply
{
    const AUTHORIZED = "AUTHORIZED";
    const CLEAR_SUCCESS = "CLEAR_SUCCESS";

    private $tbkData = array();
    private $tbkConfirmation;
    private $confirmPayment;

    public function __construct($tbkConfirmation = null, $confirmPayment = null)
    {
        $this->tbkConfirmation = isset($tbkConfirmation) ? $tbkConfirmation : new TransbankConfirmation();
        $this->confirmPayment = isset($confirmPayment) ? $confirmPayment : new ConfirmPayment();
    }

    public function populate($body)
    {
        // Convert and save response as tbkData
        // - Default values
        $this->tbkData["TBK_MONTO"] = 0;
        $this->tbkData["TBK_ID_SESION"] = 0;
        $this->tbkData["TBK_RESPUESTA"] = 9;
        $this->tbkData["TBK_ORDEN_COMPRA"] = 0;
        $this->tbkData["TBK_CODIGO_AUTORIZACION"] = "0000";
        $this->tbkData["TBK_MAC"] = "";
        $TBK_ORDEN_COMPRA = 0;

        if (!empty($body->amount)) {
            //We need to add `00` at the end of the amount
            $this->tbkData["TBK_MONTO"] = intval($body->amount) * 100;
        } elseif (!empty($body->detailsOutput->amount)) {
            $this->tbkData["TBK_MONTO"] = intval($body->detailsOutput->amount) * 100;
        }
        //Generate date info
        if (!empty($body->transactionDate)) {
            $dateFullTbk = date_parse($body->transactionDate);
            $dateTbk = $dateFullTbk['year'].$dateFullTbk['month'].$dateFullTbk['day'];
            $timeTbk = $dateFullTbk['hour'].$dateFullTbk['minute'].$dateFullTbk['second'];
            $this->tbkData["TBK_FECHA_TRANSACCION"] = $dateTbk;
            $this->tbkData["TBK_FECHA_EXPIRACION"] = $dateTbk;
            $this->tbkData["TBK_HORA_TRANSACCION"] = $timeTbk;
        }
        //Other params optional
        $optionalKeys = array(
            "TBK_ORDEN_COMPRA" => "buyOrder",
            "TBK_ID_SESION" => "sessionId",
            "TBK_RESPUESTA" => "responseCode",
            "TBK_CODIGO_COMERCIO" => "commerceCode",
            "TBK_CODIGO_COMERCIO_ENC" => "commerceCode",
            "TBK_CODIGO_AUTORIZACION" => "authorizationCode",
            "TBK_FINAL_NUMERO_TARJETA" => "cardNumber",
            "TBK_TIPO_PAGO" => "paymentTypeCode",
            "TBK_NUMERO_CUOTAS" => "sharesNumber",
            "TBK_VCI" => "vci"
        );
        foreach ($optionalKeys as $keyTbk => $keyResp) {
            if (isset($body->$keyResp)) {
                $this->tbkData[$keyTbk] = $body->$keyResp;
            } elseif (isset($body->detailsOutput->$keyResp)) {
                $this->tbkData[$keyTbk] = $body->detailsOutput->$keyResp;
            }
            if (!isset($this->tbkData[$keyTbk])) {
                $this->tbkData[$keyTbk] = "";
            }
        }
    }

    public function run()
    {
        $TBK_ORDEN_COMPRA = $this->tbkData["TBK_ORDEN_COMPRA"];
        Logger::logDebug(__METHOD__, var_export($this->tbkData, true));

        $result = $this->tbkConfirmation->authorize((object)$this->tbkData, false, false);
        Logger::logDebug($TBK_ORDEN_COMPRA, "Tbk result".var_export($result, true));
        $fullSuccess = false;
        if ($result->status == 'authorized') {
            $clear_status = $this->confirmPayment->validatePayment($TBK_ORDEN_COMPRA);
            if ($clear_status == self::CLEAR_SUCCESS) {
                $apply_products = $this->confirmPayment->applyProducts($TBK_ORDEN_COMPRA);
                if ($apply_products['status'] == self::AUTHORIZED && !$this->confirmPayment->hasError()) {
                    $fullSuccess = true;
                }
            }
        }
        return $fullSuccess;
    }

    public function getOrder()
    {
        return $this->tbkData["TBK_ORDEN_COMPRA"];
    }

    public function markAsError($order)
    {
        return $this->confirmPayment->markAsError($order);
    }
}
