<?php
namespace Yapo;

use \Exception;
use Yapo\Payment\Trans as PaymentTrans;
use Yapo\Accounts\Trans as AccountTrans;

class GetVoucher
{
    const EMPTY_CONTENT = '--';
    const SUB_MAX_LENGTH = 52;

    public $v_doc;

    public function __construct($add_image = true)
    {
        $this->v_doc = new VoucherDocument($add_image);
    }

    /**
     * Set the pdf variables from Payment and Account data
     * @param   order_id numeric
     * @param   hash     sha1 from timestamp
     */
    public function populateVoucher($order_id, $hash)
    {
        global $BCONF;
        // get the doc from trans
        $document = $this->getDocument($order_id, $hash);

        // populate user information
        $this->v_doc->user_data = $this->getUserInfo($document['account_id'], $document['timestamp']);

        // populate static parameters
        $this->v_doc->doc_num = $order_id;
        $this->v_doc->total = number_format($document['amount'], 0, ',', '.');

        // populate details
        foreach ($document as $key => $val) {
            if (is_array($val)) {
                $price = number_format($val['price'], 0, ',', '.');
                $subject = $val['subject'];
                $product_id = $val['product_id'];
                $product_name = Bconf::bconfGet($BCONF, "*.payment.product.{$product_id}.name");
                $description = $product_name;
                if (!empty($subject)) {
                    $description .= " - {$subject}";
                }
                if (strlen($description) > self::SUB_MAX_LENGTH) {
                    $description = substr($description, 0, self::SUB_MAX_LENGTH)."...";
                }
                $this->v_doc->detail_data[] = array($description, 1, $price, $price);
            }
        }
    }

    /**
     * Get the document from Trans
     * @param   order_id numeric
     * @param   hash     sha1 from timestamp
     *
     * @return $documet array
     */
    public function getDocument($order_id, $hash)
    {
        $result = PaymentTrans::getPaymentItems($order_id);
        if ($result === false) {
            echo "Purchase not found";
            exit(0);
        }

        $document = YapoPaymentApp::sortPaymentData($result);
        // Validate hash
        if (strcmp(sha1($document['timestamp']), $hash) != 0) {
            echo "Security check fail";
            exit(0);
        }
        // Validate document type
        if (strcmp($document['doc_type'], "voucher") != 0) {
            echo "Invalid Document";
            exit(0);
        }

        return $document;
    }

    /**
     * Get account info from trans
     * @param $id           account_id
     * @param $timestamp    date to format
     *
     * @return array
     */
    public function getUserInfo($id, $timestamp)
    {
        $acc_data = AccountTrans::getAccount($id);
        $user_data = array();
        $user_data[] = empty($acc_data['name']) ? self::EMPTY_CONTENT : $acc_data['name'];
        $user_data[] = empty($acc_data['rut']) ? self::EMPTY_CONTENT : $this->getFormattedRut($acc_data['rut']);
        $user_data[] = $this->getFormattedDate($timestamp);
        if (empty($acc_data['commune'])) {
            $user_data[] = self::EMPTY_CONTENT;
        } else {
            $user_data[] = $this->getCommuneName($acc_data['region'], $acc_data['commune']);
        }

        return $user_data;
    }

    /**
     * Method to generate the Output from VoucherDocument
     */
    public function run()
    {
        $this->v_doc->generate();
    }

    //-------------Utils to format dat to format dataa--------------
    /**
     * Format timestamp to Chilean date
     */
    public function getFormattedDate($timestamp)
    {
        global $BCONF;
        $datetime = strtotime($timestamp);
        $day=date("d", $datetime);
        $month=date("n", $datetime);
        $month_name = Bconf::bconfGet($BCONF, "*.language.DATE.MONTHS.{$month}.long.es");
        $year=date("Y", $datetime);
        return "{$day} de {$month_name} de {$year}";
    }

    /**
     * Get the name of commune from Bconf
     */
    public function getCommuneName($region, $commune)
    {
        global $BCONF;
        $key = "*.common.region.{$region}.commune.{$commune}.name";
        return Bconf::bconfGet($BCONF, $key);
    }

    /**
     *  Format RUT
     */
    public function getFormattedRut($unformattedRut)
    {
        if (strpos($unformattedRut, '-') !== false) {
            $splittedRut = explode('-', $unformattedRut);
            $number = number_format($splittedRut[0], 0, ',', '.');
            $verifier = strtoupper($splittedRut[1]);
            return $number . '-' . $verifier;
        }
        return number_format($unformattedRut, 0, ',', '.');
    }
}
