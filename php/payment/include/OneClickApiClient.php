<?php
require_once("blocket_application.php");
require_once("JSON.php");
require_once('bRedisClass.php');
require_once('APIClient.php');

class OneClickApiClient {
	protected $client;
	public $email;
	public $orderId;
	public $amount;
	public $oneClickTransactionId;
	public $oneClickAuthCode;
	public $cardLastDigits;
	public $oneClickOrderId;

	public function __construct() {
		$this->client = new APIClient();
	}

	public function pay() {

		try {
			$payData = array(
					'email'=>(string)$this->email,
					'order_id'=> (string)$this->orderId,
					'amount'=>$this->amount
					);
			$resp = $this->client->call('pay', $payData);
			$result = json_decode($resp);
			if($result->result == 'OK'){
				$this->oneClickTransactionId = $result->transaction_data->transaction_id;
				$this->oneClickAuthCode = $result->transaction_data->auth_code;
				$this->cardLastDigits = $result->transaction_data->last_digits;
				$this->oneClickOrderId = $result->transaction_data->oneclick_order_id;
				return true;
			}
			$this->oneClickOrderId = $result->oneclick_order_id;
			bLogger::logError( __METHOD__ , "ERROR MS pay response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return false;
	}

	public function reverse() {

		try {
			$reverseData = array('order_id'=>(string)$this->orderId);
			$resp = $this->client->call('reverse', $reverseData);
			$result = json_decode($resp);
			if($result->result == 'OK'){
				return true;
			}
			bLogger::logError( __METHOD__ , "ERROR MS reverse response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return false;
	}

	public function initInscription($email, $url) {

		try {
			$data = array('email' => $email, 'url' => $url);
			$resp = $this->client->call('init_inscription', $data);
			$result = json_decode($resp);
			if($result->result == 'OK') {
				return $result;
			}
			bLogger::logError( __METHOD__ , "ERROR MS init_inscription response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return null;
	}

	public function endInscription($token) {

		try {
			$data = array('token' => $token);
			$resp = $this->client->call('end_inscription', $data);
			$result = json_decode($resp);

			if($result->result == 'OK') {
				return true;
			}
			bLogger::logError( __METHOD__ , "ERROR MS init_inscription response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return false;
	}

	public function registerData($email ='') {

		try {
			if ($email != '') {
				$this->email = $email;
			}
			$data = array('email' => $this->email);
			$resp = $this->client->call('isregistered', $data);
			$result = json_decode($resp);
			if($result->result == 'OK'){
                return $result;
			}
			bLogger::logError( __METHOD__ , "ERROR MS isregistered response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return "";
	}
	public function isRegistered($email ='') {

		try {
			if ($email != '') { 
				$this->email = $email;
			}
			$data = array('email' => $this->email);
			$resp = $this->client->call('isregistered', $data);
			$result = json_decode($resp);
			if($result->result == 'OK'){
				return true;
			}
			bLogger::logError( __METHOD__ , "ERROR MS isregistered response: " . print_r($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return false;
	}

	public function removeUser($email = '') {

		try {
			if ($email != '') {
				$this->email = $email;
			}

			$data = array('email' => $this->email);
			$resp = $this->client->call('remove_user', $data);
			$result = json_decode($resp);

			if ($result->result == 'OK') {
				return true;
			}
			bLogger::logError( __METHOD__ , "ERROR MS removeUser response: ".var_export($resp, true));
		} catch(Exception $e) {
			bLogger::logError( __METHOD__ , "MS Exception".print_r($e, true));
		}

		return false;
	}
}

