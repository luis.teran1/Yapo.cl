<?php
require_once('autoload_lib.php');
require_once('util.php');
require_once('RedisMultiproductManager.php');
require_once("Servipag.php");
require_once("httpful.php");

use Yapo\ApiClient;
use Yapo\ConfirmPayment;
use Yapo\WebpayService;
use Yapo\Products;
use Yapo\Bconf;

class common_payment_application extends blocket_application {

    const SINGLEPRODUCT_TYPE = 1;
    const SINGLEPRODUCT_TYPE_NEW = 2;
    const MULTIPRODUCT_TYPE = 3;
    const SINGLEPRODUCT_TYPE_MOBILE = 4;
    const PRODUCT_AUTOBUMP = '10';
    const PRODUCT_AUTOFACT = '20';
    const PRODUCT_INSERTING_FEE = '60';
    const PAY_METHOD_ERR_PREF = 'PAYMENT_METHOD_ERROR: ';

    var $account_session;
    var $form_type = 1;
    var $product_list;

    var $document;
    var $name;
    var $lob;   // Line Of Business (giro comercial)
    var $rut;
    var $address;
    var $region;
    var $communes;
    var $contact;
    var $email;
    var $list_id;
    var $ad_id;
    var $prod;
    var $company_buyer;
    var $email_buyer;
    var $user_id_buyer;
    var $buyer_name;
    var $payment_method;

    var $payment_group_id;
    var $pay_code;
    var $page_title = "";
    var $error_message = "";
    var $error_title = "";
    var $v2 = 0;

    var $product_name;
    var $product_price;
    var $label_type;

    var $has_loaded = false;
    var $session_expired = false;
    var $tbk = array();

    var $from_register = false;
    var $oneclick_register_succes;
    var $oneclick_payment_error;
    var $oneclick_user_email;
    var $error_order_id;

    public $list_ids = array();
    public $ad_ids = array();
    public $list_id_prod_params = array();
    public $ad_id_prod_params = array();
    public $other_products_ids = array();
    
    function saveOneClickID($order_id, $oc_id)
    {
        $trans = new bTransaction();
        $trans->add_data("payment_group_id", $order_id);
        $trans->add_data("name", "oneclick_id");
        $trans->add_data("value", $oc_id);
        $trans->send_command("add_purchase_param");
    }


    function common_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email, $prod = 0, $payment_method=NULL, $platform) {
        global $BCONF;
        $this->payment_method = $payment_method;

        if ($this->has_loaded === false) {
            return 'load';
        }
        if (!empty($prod)) {
            $this->prod = $prod;
        }

        if ($document == NULL) {
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
            return 'error';
        }

        $document_data = bconf_get($BCONF, "*.payment.document.$document.data");
        $doc_type = bconf_get($BCONF, "*.payment.document.$document.doc_type");

        if ($doc_type == "") {
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
            return 'error';
        }

        bLogger::logDebug(__METHOD__, "Platform : $platform");
        if (empty($platform)) {
            $platform = "unknown";
        }

        $this->document = $document;
        $this->name = html_entity_decode($name);
        $name = $this->name;

        $this->lob = $lob;
        $this->rut = $rut;
        $this->address = $address;
        $this->region = $region;
        $this->communes = $communes;
        $this->contact = $contact;

        $load_email_from_acc = false;

        if (isset($this->account_session)) {
            if (empty($email) && $this->account_session->is_logged()) {
                $email = $this->account_session->get_param('email');
                $load_email_from_acc = true;
            }

            if ($this->payment_method == 'oneclick') {
                $ocp_client = new OneClickApiClient();
                $this->oneclick_user_email = $this->account_session->get_param('email');
                $user_oneclick = $ocp_client->isRegistered($this->oneclick_user_email);
                if(!$user_oneclick) {
                    return 'oc_register';   
                }
            }

        }
        $this->email = $email;

        $transaction = new bTransaction();
        foreach ($document_data as $key => $val) {
            if ($val['name'] == 'email' && empty($$val['name'])) {
                $transaction->add_data('email', $this->ad['email']);
            } else {
                $transaction->add_data($val['name'], $$val['name']);
            }
        }

        $transaction->add_data('payment_platform', $platform);
        $transaction->add_data('payment_method', $payment_method);
        $transaction->add_data('doc_type', $doc_type);
        $transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);

        if (isset($this->account_session) && $this->account_session->is_logged() && $this->form_type != self::SINGLEPRODUCT_TYPE_MOBILE) {
            if (count($this->list_ids) || count($this->ad_ids) || count($this->other_products_ids)) {
                $result = get_subjects_n_categories(array_keys($this->list_ids));

                $products_price = Array();
                $products_name = Array();
                $mp_list_id = Array();
                $mp_prod_id = Array();
                $mp_prod_param = Array();
                $this->product_list = Array();
                //append normal products
                foreach ($this->list_ids as $l_id => $products) {
                    $position = array_search($l_id, $result['list_id']);
                    $category = $result['category'][$position];
                    foreach ($products as $id_prod) {
                        $param_prod = $this->list_id_prod_params[$l_id][$id_prod];
                        if (bconf_get($BCONF, "*.payment.product.$id_prod.param.label_type") == "lt"){
                            $param_prod = bconf_get($BCONF, "*.product_config.label.options.$param_prod");
                        }
                        if ($id_prod == self::PRODUCT_AUTOFACT) {
                            $param_prod = $result['plates'][$position];
                        }

                        $price = Bconf::getProductPrice($BCONF, $id_prod, $category);
                        if ($id_prod == self::PRODUCT_AUTOBUMP) {
                            $price = get_autobump_price($l_id, $param_prod);
                        }

                        if (!empty($price)) {
                            array_push($mp_list_id, $l_id);
                            array_push($mp_prod_id, $id_prod);
                            array_push($mp_prod_param, $param_prod);
                            array_push($products_price, $price);
                            array_push($products_name, bconf_get($BCONF, "*.payment.product.$id_prod.codename"));
                            array_push($this->product_list, array($id_prod, $price));
                        }
                    }
                }

                //append upselling products
                foreach ($this->ad_ids as $ad_id => $products) {
                    $trans_load = new bTransaction();
                    $trans_load->add_data('ad_id', $ad_id);
                    $load_reply = $trans_load->send_command('get_ad');

                    if ($trans_load->has_error(true) || empty($load_reply['subject']) || empty($load_reply['category'])) {
                    //  $this->cart_mngr->removeAllAdProducts($this->account_session->get_param('email'),  $id);
                    } else {
                        $subject = $load_reply['subject'];
                        $category = $load_reply['category'];
                        foreach ($products as $id_prod) {
                            $param_prod = $this->ad_id_prod_params[$ad_id][$id_prod];
                            if (bconf_get($BCONF, "*.payment.product.$id_prod.param.label_type") == "lt") {
                                $param_prod = bconf_get($BCONF, "*.product_config.label.options.$param_prod");
                                if (empty($param_prod)) {
                                    $this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
                                    $this->page_title = "ERROR_AD_NOT_AVAILABLE";
                                    $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
                                    $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";
                                    return 'error';
                                }
                            }
                            $name = bconf_get($BCONF, "*.payment.product.$id_prod.codename");
                            $price = Bconf::getProductPrice($BCONF, $id_prod, $category);
                            if (!empty($price)) {
                                array_push($mp_list_id, $ad_id);
                                array_push($mp_prod_id, $id_prod);
                                array_push($mp_prod_param, $param_prod);
                                array_push($products_price, $price);
                                array_push($products_name, $name);
                                array_push($this->product_list, array($id_prod, $price));
                            }
                        }
                    }
                }

                //append others products (pack, store)
                foreach ($this->other_products_ids as $other_product) {
                    $other_product_id = $other_product[0];
                    $other_product_price = $other_product[1];
                    $name = bconf_get($BCONF, "*.payment.product.$other_product_id.codename");
                    array_push($mp_list_id, 0);
                    array_push($mp_prod_id, $other_product_id);
                    array_push($mp_prod_param, '0'); //Other products does not have params
                    array_push($products_price, $other_product_price);
                    array_push($products_name, $name);
                    array_push($this->product_list, array($other_product_id, $other_product_price));
                }

                $transaction->add_data('product_id', implode("|", $mp_prod_id));
                $transaction->add_data('list_id', implode("|", $mp_list_id));
                $transaction->add_data('product_params', implode("|", $mp_prod_param));
                $transaction->add_data('product_price', implode("|", $products_price));
                $transaction->add_data('product_name', implode("|", $products_name));
            } else {
                bLogger::logError(__METHOD__, "User logged trying to pay without products selected");
            }
        } else {
            //When the user is not logged
            if (isset($this->prod) && (isset($this->list_id) || isset($this->ad_id)) && isset($this->category)) {

                $this->product_price = Bconf::getProductPrice($BCONF, $this->prod, $this->category);
                $this->product_name = bconf_get($BCONF, "*.payment.product.$this->prod.codename");
                $transaction->add_data('product_id', $this->prod);
                $transaction->add_data('product_price', $this->product_price);
                $transaction->add_data('product_name', $this->product_name);

                if (bconf_get($BCONF, "*.payment.product.".$this->prod.".extra_params") == 1) {
                    if (bconf_get($BCONF, "*.payment.product.".$this->prod.".param.label_type") == "lt") {
                        $transaction->add_data('product_params', bconf_get($BCONF,"*.product_config.label.options.".$this->label_type));
                    }
                } else {
                    $transaction->add_data('product_params', '0');
                }
                $transaction->add_data('list_id', empty($this->list_id)? $this->ad_id : $this->list_id);
            }
        }

        // create or retrieve the store, so it gets activated on payment
        if ($this->is_buying_store()) {
            $trans = new bTransaction();
            $trans->add_data('account_id', $this->account_session->get_param('account_id'));
            $reply = $trans->send_command('create_store');
            $transaction->add_data('store_id', $reply['store_id']);
            //TODO Handle error cases
        }

        if (isset($this->account_session) && $this->account_session->is_logged()) {
            $account_id = $this->account_session->get_param('account_id');
            $transaction->add_data('account_id', $account_id);
        }

        $reply = $transaction->send_command('purchase_insert');
        if (strstr($reply['status'], 'TRANS_DATABASE_ERROR')) {

            if (strstr($reply['status'], 'ERROR_AD_NOT_ACTIVE')) {
                $this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
                $this->page_title = "ERROR_AD_NOT_AVAILABLE";
                $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
                $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";
                return 'error';
            } else {
                die_with_template(__FILE__, log_string() . "PAYMENT: TRANS_DATABASE_ERROR ");
            }
        }

        if ($transaction->has_error()) {
            $trans_errors = $transaction->get_errors();
            $errs = array_merge($trans_errors, array('in_payment' => "1"));
            $this->set_errors($errs);
            if ($load_email_from_acc) {
                $this->email = null;
            }
            return 'form';
        }

        $this->payment_group_id = $reply['o_payment_group_id'];
        $this->pay_code = $reply['o_pay_code'];
        bLogger::logDebug(__METHOD__, "payment method $payment_method");
        if (Products::isComboUpselling($this->prod) || Products::isInsertingFeePremium($this->prod)) {
            $trans = $transaction->reset();
            $trans->add_data('payment_group_id', $this->payment_group_id);
            $trans->validate_command('apply_products');
            if ($trans->has_error(true)) {
                $this->page_title = "ERROR_AD_NOT_AVAILABLE";
                $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
                $this->error_message = "ERROR_PRODUCT_CANT_BE_APPLIED";
                return 'error';
            }
        }
        return $payment_method;
    }

    function get_total_price() {
        $elements = $this->get_product_list();
        $total_price = 0;
        foreach ($elements as $item) {
            $total_price += $item['price']; 
        }
        return $total_price;
    }

    function webpay_plus_payment($head_style, $headscripts, $redirect_data = NULL) {
        $WebpayService = new WebpayService();
        $response = $WebpayService->execute("healthCheck");
        if (!$response) {
            bLogger::logError(__METHOD__, self::PAY_METHOD_ERR_PREF."Webpay plus service not available");
        }
        $response = $WebpayService->execute(
            "startPayment",
            array(
                "amount" => $this->get_total_price(),
                "sessionId" => "{$this->payment_group_id}",
                "buyOrder" => $this->payment_group_id,
                "returnURL" => bconf_get($BCONF, "*.payment.transbank_plus.url_return"),
                "finishURL" => bconf_get($BCONF, "*.payment.transbank_plus.url_finish")
            )
        );
        $redirectData = Array();
        if (!$response) {
            bLogger::logError(__METHOD__, self::PAY_METHOD_ERR_PREF."Error initiating payment with Webpay plus");
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;
            return 'form';
        }

        $redirectData['url'] = $response->body->url;
        $redirectData['method'] = 'POST';

        $redirectData['params']['token_ws'] = $response->body->token_ws;

        bLogger::logInfo(__METHOD__, "payment_webpay payment_group_id = " . $this->payment_group_id);
        bLogger::logInfo(__METHOD__,"startPayment->redirectData = " . var_export($redirectData, true));
        bLogger::logDebug(__METHOD__, "payment_method:Webpay_plus");

        $simple_data = array();
        if (isset($redirect_data)) {
            $simple_data['no_footer'] = 1;
            $simple_data['no_header'] = 1;
        }

        $payment_template = bconf_get($BCONF, "*.payment.options.5.template");
        $this->display_layout($payment_template, $simple_data, $redirectData, null, 'html5_base');
        return 'FINISH';
    }

    function webpay_payment($head_style, $headscripts, $redirect_data = NULL) {
        global $BCONF;
        if (isset($head_script)) {
            $this->headscripts = $head_script;
        }
        if (isset($head_style)) {
            $this->headstyles = $head_style;
        }

        $redirectData = Array();
        $redirectData['url'] =  bconf_get($BCONF, "*.payment.transbank.url_cgi");
        $redirectData['method'] = 'POST'; //bconf_get($BCONF, "*.payment_api_client.params.method_code");

        $check_result=$this->check_payment_url($redirectData['url']);
        if ($check_result) {
            bLogger::logError(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."error checking webpay url: " . $check_result
            );
        }

        //Add two zeros at the end of TBK_MONTO
        $redirectData['params']['TBK_MONTO'] = $this->get_total_price() * 100;
        $redirectData['params']['TBK_ORDEN_COMPRA'] = $this->payment_group_id;
        $redirectData['params']['TBK_ID_SESION'] = sha1($this->payment_group_id);
        // TBK_TIPO_TRANSACCION should be HARDCODED
        $redirectData['params']['TBK_TIPO_TRANSACCION'] = 'TR_NORMAL';
        // Urls comes from Bconf
        $redirectData['params']['TBK_URL_EXITO'] = bconf_get($BCONF, "*.payment.transbank.url_success");
        $redirectData['params']['TBK_URL_FRACASO'] = bconf_get($BCONF, "*.payment.transbank.url_failure");

        bLogger::logInfo(__METHOD__, "payment_webpay payment_group_id = " . $this->payment_group_id);
        bLogger::logInfo(__METHOD__,"startPayment->redirectData = " . var_export($redirectData, true));
        bLogger::logDebug(__METHOD__, "payment_method:Webpay");

        $simple_data = array();
        if (isset($redirect_data)) {
            $redirectData['params']['TBK_URL_EXITO'] = $redirect_data['exito'];
            $redirectData['params']['TBK_URL_FRACASO'] = $redirect_data['fracaso'];
            $simple_data['no_footer'] = 1;
            $simple_data['no_header'] = 1;
        }

        $payment_template = bconf_get($BCONF, "*.payment.options.5.template");
        $this->display_layout($payment_template, $simple_data, $redirectData, null, 'html5_base');
        return 'FINISH';
    }

    function servipag_payment($payment_method=NULL, $head_style = NULL , $head_script = NULL, $redirect_data = NULL) {
        global $BCONF;

        if (isset($head_script)) {
            $this->headscripts = $head_script;
        }
        if( isset($head_style)) {
            $this->headstyles = $head_style;
        }

        bLogger::logInfo(__METHOD__, "payment_webpay payment_group_id = " . $this->payment_group_id);

        $redirectData['url'] = bconf_get($BCONF, "*.payment.servipag.url");
        $redirectData['method'] = 'POST';
        
        $check_result=$this->check_payment_url($redirectData['url']);
        if ($check_result) {
            bLogger::logError(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."error checking servipag url: " . $check_result
            );
        }

        $servipag_xml1 = new ServipagXML1($this->payment_group_id, $this->get_total_price());
        $data = array();
        $data['xml'] = $servipag_xml1->build_xml();
        $redirectData['params'] = $data;
        //
        bLogger::logInfo(__METHOD__,"startPayment->redirectData = " . var_export($redirectData, true));
        bLogger::logInfo(__METHOD__,"startPayment->Data = " . var_export($data, true));
        bLogger::logDebug(__METHOD__, "payment_method:Servipag");

        $payment_template = bconf_get($BCONF, "*.payment.options.7.template");
        bLogger::logDebug(__METHOD__, "payment_template:$payment_template");
        $simple_data = array();
        if (isset($redirect_data)) {
            //$redirectData['params']['TBK_URL_EXITO'] = $redirect_data['exito'];
            //$redirectData['params']['TBK_URL_FRACASO'] = $redirect_data['fracaso'];
            //$simple_data['no_footer'] = 1;
            //$simple_data['no_header'] = 1;
        }

        $this->display_layout($payment_template, $simple_data, $redirectData, null, 'html5_base');
        return 'FINISH';
    }

    function khipu_payment($payment_method=NULL, $head_style = NULL , $head_script = NULL, $redirect_data = NULL) {
        global $BCONF;

        if (isset($head_script)) {
            $this->headscripts = $head_script;
        }
        if (isset($head_style)) {
            $this->headstyles = $head_style;
        }

        $this->headstyles = $head_style;
        $confUrl = "*.common.base_url.payment_rest_api";
        $confMethods = "*.payment_rest_api_client.methods.";

        $this->apiClient = new ApiClient($confUrl, $confMethods, true);

        $params ['id'] = $this->payment_group_id;
        $params ['amount'] = $this->get_total_price();
        $params ['subject'] = 'Pago yapo';
        $product_list = $this->get_product_list();
        $body_str = '';
        $first = true;
        foreach ($product_list as $product_params) {
            if (!$first) {
                $body_str .= ', ';
            }
            $body_str .= "{$product_params['name']} {$product_params['price']}";
            $first = false;
        }
        $params ['body'] = $body_str;

        try {
            $response = $this->apiClient->call("postKhipu", $params);
        } catch (Exception $e) {
            bLogger::logError(__METHOD__, self::PAY_METHOD_ERR_PREF."Error connecting to Khipu: ".$e->getMessage());
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;
            return 'form';
        }
        bLogger::logDebug(__METHOD__, "Payment rest response: ".$response);

        $response_decoded = json_decode($response, true);
        //check if the decoded object ended up as a string. This happens with double encoded strings
        if (gettype($response_decoded) == 'string') {
            $response_decoded = json_decode($response_decoded, true);
        }
        if (isset($response_decoded['payment_url'])) {
            bLogger::logDebug(__METHOD__, "follow payment URL [".$response_decoded['payment_url']."] from response decoded");
            header('location:'.$response_decoded['payment_url']);
        } else {
            bLogger::logError(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."no payment url returned from Khipu. Response: ".$response
            );
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;
            return 'form';
        }
        return 'FINISH';
    }

    public function oc_payment() {
        global $BCONF;

        $ocp_client = new OneClickApiClient();
        $ocp_client->email = $this->oneclick_user_email;
        $ocp_client->orderId = $this->payment_group_id;
        $ocp_client->amount = $this->get_total_price();

        //authorize payment in yapo
        $confirmation = new ConfirmPayment();

        $applyReverse = false;
        try {
            if (!$ocp_client->isRegistered()) {
                throw new Exception('Error pay in OneClick user not registered');
            }
            //pay on oneclick microservice
            $paySuccess = $ocp_client->pay();
            $this->error_order_id = $ocp_client->oneClickOrderId;
            if (!$paySuccess) {
                throw new Exception('Error paying with OneClick');
            }
            $applyReverse = true;

            // Save OneClickOrderId
            $this->saveOneClickID($this->payment_group_id, $ocp_client->oneClickOrderId);

            //If this method fails throw an exeption
            $confirmation->authorizeExecute(
                $this->payment_group_id,
                $ocp_client->oneClickTransactionId,
                $ocp_client->cardLastDigits,
                $ocp_client->oneClickAuthCode
            );

            $this->saveIdSession(
                array(
                    'payment_group_id' => $this->payment_group_id,
                    'special_order_id' => $this->error_order_id,
                    'special_method' => $this->payment_method
                )
            );
            //redirect to payment success page
            header('Location: '.bconf_get($BCONF, "*.payment.transbank.url_success"));

        } catch (Exception $e) {
            bLogger::logError(__METHOD__, self::PAY_METHOD_ERR_PREF."Error connecting to OneClick: ".$e->getMessage());

            if ($applyReverse) {
                if (!$ocp_client->reverse()) {
                    bLogger::logError(__METHOD__, "Error in reverse :".print_r($ocp_client, true));
                }
                if (!$confirmation->refusePayment($this->payment_group_id)) {
                    bLogger::logError(__METHOD__, "Error in refuse doc: {$this->payment_group_id}");
                }
            }

            $this->oneclick_payment_error = 1;
            return 'form';
        }

        return 'FINISH';
    }

    public function credits_payment() {
        global $BCONF;
        $confirmation = new ConfirmPayment();

        try {
            $credits = new CreditsService();
            $price = $this->get_total_price();
            $account_id = $this->account_session->get_param('account_id');
            $balance = $credits->getBalance($account_id)->Balance;
            // The user has credits available?
            if ($balance < $price) {
                throw new Exception('Error insuficient credits');
            }
            $creditsTransactionId = 0;
            //If this method fails throw an exeption
            $confirmation->authorizeExecute(
                $this->payment_group_id,
                $creditsTransactionId,
                0000,
                123123123
            );

            $body = array(
                'UserId' => (integer)$account_id,
                'Credits' => (integer)$price,
                'ExternalId' => (integer)$this->payment_group_id
            );
            $response = $credits->consumeCredits($body);
            if (!$response) {
                bLogger::logError(__METHOD__, "Error no response trying to use credits");
                throw new Exception('Error paying with credits');
            }

            $confirmation->sendCreditVoucher($this->payment_group_id, $this->account_session->get_param('email'), $price, $balance);

            $this->saveIdSession(
                array(
                    'payment_group_id' => $this->payment_group_id,
                    'special_order_id' => $creditsTransactionId,
                    'special_method' => $this->payment_method
                )
            );

            //redirect to payment success page
            header('Location: '.bconf_get($BCONF, "*.payment.transbank.url_success"));

        } catch (Exception $e) {
            bLogger::logError(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."Error connecting to Credits service: ".$e->getMessage()
            );
            $this->credits_payment_error = 1;
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;

            if (!$confirmation->refusePayment($this->payment_group_id)) {
                bLogger::logError(__METHOD__, "Error in refuse doc: {$this->payment_group_id}");
            }

            return 'form';
        }

        return 'FINISH';
    }

    public function saveIdSession($ids, $prefix = "oneclick") {
        foreach ($ids as $key => $val) {
            $_SESSION['global'][$key] = $val;
            bLogger::logDebug(__METHOD__, "{$prefix}: {$key}:{$val}");
        }
    }

    function payment_disabled() {
        $this->headscripts = bconf_get($BCONF, "*.payment.headscripts");
        $this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
        $this->display_layout('payment/payment_disabled.html', null, null, null, 'html5_base');
        return 'FINISH';
    }

    // check if the given url is active and can be used
    // returns the http code if not valid or the error message if any
    function check_payment_url($url) {
        bLogger::logDebug(__METHOD__, "call for check payment url: ".$url);
        $allowed_codes = array("200", "401", "301", "302");
        $ch = curl_init($url);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 6);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_REDIR_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);

        try {
            $data = curl_exec($ch);
            if(curl_errno($ch)){   // should be 0
                $error_code = curl_errno($ch);
                $error = curl_error($ch);
                curl_close($ch);
                return "curl error ".$error_code.":".$error;
            }
            $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            bLogger::logDebug(__METHOD__, "code: ".$code);

            if (!in_array($code, $allowed_codes)) {
                return strval($code);
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return NULL;
    }

    /*GENERIC ERROR OF THIS FLOW */
    function payment_error() {
        global $BCONF;
        $this->headscripts = bconf_get($BCONF, "*.payment.headscripts");
        $this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
        if (empty($this->page_title)) {
            $data['page_title'] = bconf_get($BCONF, "*.payment.fail.page_title");
        } else {
            $data['page_title'] = bconf_get($BCONF, "*.language.".$this->page_title.".es");
        }

        if (empty($this->error_title)) {
            $data['error_title']= 'ERROR_TRANSACTION_FAIL_TITLE';
        }

        $data['error'] = $this->error_message;

        if (empty($this->error_message_id)) {
            $this->error_message_id = $this->error_message;
        }
        //this must be corrected when errors are rectified XITI xiti
        $data['appl'] = "pagos";

        $data['err_in_payment'] =  "1";
        $this->display_layout('payment/pay_error.html', $data, null, null, 'mobile_select');
        return 'FINISH';
    }

    function payment_expired() {
        $this->session_expired = true;
        return 'load';
    }

    function get_purchase($orden) {
        $transaction = new bTransaction();
        $transaction->add_data('log_string', log_string());
        if (isset($orden) && !empty($orden)) {
            $transaction->add_data('payment_group_id', $orden);
        }
        $reply = $transaction->send_command('purchase_get');

        if ($transaction->has_error() || empty($reply['status'])) {
            return null;
        }

        return $reply;
    }

    function get_error_status($orden_compra, $ftype) {
        $purchase = $this->get_purchase($this->tbk['TBK_ORDEN_COMPRA']);
        $data = array();
        if (isset($purchase["purchase_get"][0]['status'])) {
            $data['payment_status'] = $purchase["purchase_get"][0]['status'];
            $data['pay_log_status'] = $purchase["purchase_get"][0]['pay_log_status'];
            $data['pay_group_status'] = $purchase["purchase_get"][0]['pay_group_status'];
        }

        if (!isset($data['payment_status'])) {
            bLogger::logError(__METHOD__, "Payment: data not found, orden_compra = $orden_compra");
            $this->page_title = "ERROR_SESSION_EXPIRED_PAGE_TITLE";
            $this->error_title = "ERROR_SESSION_EXPIRED_OOPS";
            $this->error_message = str_replace("#order_id#", $orden_compra, $this->get_transaction_fail_html_code());
            $this->error_message_id = 'ERROR_TRANSACTION_FAIL_MESSAGE';
            return null;
        }

        if ($data['payment_status'] == 'refused' && $data['pay_log_status'] != "ERR_CANCELLED" && $data['pay_group_status'] == "unverified") {
            bLogger::logError(__METHOD__, "Payment: status=".$data['payment_status']);
            $this->page_title = "PAGE_FAILURE_BUMP_INCORRECT_DATA_TITLE";
            $this->error_title = "ERROR_SESSION_EXPIRED_OOPS";
            $this->error_message = "PAGE_FAILURE_BUMP_INCORRECT_DATA";
            return null;
        }

        // get the payment
        if (!isset($orden_compra)) {
            bLogger::logError(__METHOD__,"pay auth: get_state call failed: ".var_export($transaction->get_errors(), true));
            $this->error_message = "ERROR_TBK_ID_SESION_NOT_FOUND";
            return null;
        }

        // get the products
        $transaction = new bTransaction();
        $transaction->add_data('payment_group_id', $orden_compra);
        $reply = $transaction->send_command('get_items_by_payment');
        if ($transaction->has_error(true)) {
            bLogger::logError(__METHOD__,"problems getting the items by payment");
            $this->error_message = "PAGE_FAILURE_BUMP_INCORRECT_DATA";
            return null;
        }
        $data = (object) array_merge((array)$data, (array)$reply['get_items_by_payment'][0]);

        if (!isset($data->ad_id) && $ftype != self::MULTIPRODUCT_TYPE) {
            bLogger::logError(__METHOD__, "Id or Prod doesn't exist");
            $this->page_title = "ERROR_AD_DOES_NOT_EXISTS";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";
            return null;
        }
        bLogger::logDebug(__METHOD__, print_r($data, true));
        return $data;
    }

    /* All functions behind this line are UTILITIES  */

    function product_is_store($product_id) {
        global $BCONF;
        $is_store = bconf_get($BCONF, "*.payment.product.".$product_id.".is_store");
        return $is_store == "1";
    }

    function is_buying_store() {
        $redis = new RedisMultiproductManager();
        return isset($this->account_session)
            && $this->account_session->is_logged()
            && $redis->isBuyingStore($this->account_session->get_param('email'));
    }

    function get_product_list() {
        if (!isset($this->account_session) || !$this->account_session->is_logged() || $this->form_type == self::SINGLEPRODUCT_TYPE_MOBILE) {
            bLogger::logDebug(__METHOD__, "Getting products from thisprod");
            $product_description = bconf_get($BCONF, "*.payment.product.$this->prod.description");
            return array(
                array(
                    'code' => $this->prod,
                    'product_id' => $this->prod,
                    'name' => $this->product_name,
                    'description' => $product_description,
                    'qtt' => 1,
                    'price' => $this->product_price
                )
            );
        } else {
            bLogger::logDebug(__METHOD__, "Getting products from thisproduct_list");
            $output = Array();
            foreach ($this->product_list as $product) {
                $prod = $product[0];
                $product_price = $product[1];
                $product_name = bconf_get($BCONF, "*.payment.product.$prod.name");
                $product_description = bconf_get($BCONF, "*.payment.product.$prod.description");
                $tmp = array(
                    'code' => $prod,
                    'product_id' => $prod,
                    'name' => $product_name,
                    'description' => $product_description,
                    'qtt' => 1,
                    'price' => $product_price
                );
                array_push($output, $tmp);
            }
            return $output;
        }
    }

    function get_transaction_fail_html_code() {
        $fail_message = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE.es");
        $fail_message_suggest_causes = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE_SUGGEST_CAUSES.es");
        $fail_message_cause_1 = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE_CAUSE_1.es");
        $fail_message_cause_2 = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE_CAUSE_2.es");
        $fail_message_cause_3 = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE_CAUSE_3.es");
        $fail_message_action  = bconf_get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE_ACTION.es");

        return "<p>$fail_message</p>
          <div id = 'error-details'>
              <p class='error-p'>$fail_message_suggest_causes</p>
              <div class='error-box'>
                <ul class='error-form clearfix'>
                  <li class='icon-yapo'>$fail_message_cause_1</li>
                  <li class='icon-yapo'>$fail_message_cause_2</li>
                  <li class='icon-yapo'>$fail_message_cause_3</li>
                </ul>
                <div class='info'>
                    <i class='info-ico icon-yapo icon-info-circled'></i>
                    $fail_message_action
                </div>
              </div>
          </div>";
    }

    function populate_form_data($data = NULL) {
        $res = array();
        if (! @is_array($data)) {
            return false;
        }

        foreach (bconf_get($BCONF, "*.payment.document") as $node => $leaf) {
            foreach ($leaf['data'] as $elm => $val) {
                $key = $val['name'];
                if (isset($data[$key])) {
                    $res[$key] = $data[$key];
                }
            }
        }
        return $res;
    }

    function set_error_msj($page_title, $error_title, $message) {
        $this->page_title = $page_title;
        $this->error_title = $error_title;
        $this->error_message = $message;
    }

    /* ALL function in behind this line are Filters */
    function f_list_id($val) {
        if (preg_match('/^[0-9]+$/', $val))
            return $val;
        if (preg_match('/^[0-9]{4}.[0-9]+$/', $val))
            return $val;
        if (!empty($val))
            return false;
        return NULL;
    }

    /* Product code */
    function f_prod($val) {
        global $BCONF;
        if (bconf_get($BCONF,  "*.payment.product." . $val))
            return $val;
        return NULL;
    }

    function f_v2($val) {
        if (is_numeric($val))
            return $val;
        return NULL;
    }

    function f_ftype($val){
        if (!empty($val))
            return $val;
        return NULL;
    }
    
    function f_ab_freq($val){
        if (!empty($val) && is_numeric($val))
            return $val;
        return NULL;
    }
    function f_ab_num_days($val){
        if (!empty($val) && is_numeric($val))
            return $val;
        return NULL;
    }

    function f_document($val) {
        global $BCONF;
        if (bconf_get($BCONF, "*.payment.document." . $val . ".doc_type") != NULL) {
            return $val;
        }
        return NULL;
    }
    function f_lt($val) {
        global $BCONF;
        if (bconf_get($BCONF, "*.product_config.label.options." . $val ) != NULL) {
            return $val;
        }
        return NULL;
    }

    function f_rut($val) {
        $val = preg_replace('/[^[a-zA-Z0-9]��������&,:; -]/i', '', $val);
        $val = str_replace('.','',$val);
        return strtoupper($val);
    }

    function f_source_link($val) {
        return $val;
    }

    function f_mb            ($val) { return strip_tags($val); }
    function f_name          ($val) { return strip_tags($val); }
    function f_lob           ($val) { return strip_tags($val); }
    function f_address       ($val) { return strip_tags($val); }
    function f_region        ($val) { return strip_tags($val); }
    function f_communes      ($val) { return strip_tags($val); }
    function f_contact       ($val) { return strip_tags($val); }
    function f_email         ($val) { return strip_tags($val); }
    function f_from          ($val) { return strip_tags($val); }
    function f_want_bump     ($val) { return strip_tags($val); }
    function f_pr            ($val) { return strip_tags($val); }
    function f_create_account($val) { return strip_tags($val); }
    function f_payment_method($val) { return strip_tags($val); }
    function f_platform      ($val) { return strip_tags($val); }

    /* Stuff of Transbank */
    function f_TBK_ACCION               ($val) { return strip_tags($val); }
    function f_TBK_ORDEN_COMPRA         ($val) { return strip_tags($val); }
    function f_TBK_CODIGO_COMERCIO      ($val) { return strip_tags($val); }
    function f_TBK_CODIGO_COMERCIO_ENC  ($val) { return strip_tags($val); }
    function f_TBK_TIPO_TRANSACCION     ($val) { return strip_tags($val); }
    function f_TBK_RESPUESTA            ($val) { return strip_tags($val); }
    function f_TBK_MONTO                ($val) { return strip_tags($val); }
    function f_TBK_CODIGO_AUTORIZACION  ($val) { return strip_tags($val); }
    function f_TBK_FINAL_NUMERO_TARJETA ($val) { return strip_tags($val); }
    function f_TBK_FECHA_CONTABLE       ($val) { return strip_tags($val); }
    function f_TBK_FECHA_TRANSACCION    ($val) { return strip_tags($val); }
    function f_TBK_FECHA_EXPIRACION     ($val) { return strip_tags($val); }
    function f_TBK_HORA_TRANSACCION     ($val) { return strip_tags($val); }
    function f_TBK_ID_SESION            ($val) { return strip_tags($val); }
    function f_TBK_ID_TRANSACCION       ($val) { return strip_tags($val); }
    function f_TBK_TIPO_PAGO            ($val) { return strip_tags($val); }
    function f_TBK_NUMERO_CUOTAS        ($val) { return strip_tags($val); }
    function f_TBK_VCI                  ($val) { return strip_tags($val); }
    function f_TBK_MAC                  ($val) { return strip_tags($val); }
}


function get_autobump_price($list_id, $ab_params){
    $ab_num_days = 0;
    $ab_frequency = 0;
    $ab_use_night = 0;
    $p_explo = explode(";", $ab_params);
    foreach ($p_explo as $ab_param) {
        $explo = explode(":", $ab_param);
        $k = $explo[0];
        $v = $explo[1];
        switch ($k) {
            case "frequency":
                $ab_frequency = $v;
                break;
            case "num_days":
                $ab_num_days = $v;
                break;
            case "use_night":
                $ab_use_night = $v;
                break;
            default:
                bLogger::logError(__METHOD__, "unkown parameter for ab_price".$k." => ".$v);
        }
    }

    $trans_load = new bTransaction();
    $trans_load->add_data('list_id', $list_id);
    $trans_load->add_data('frequency', $ab_frequency);
    $trans_load->add_data('num_days', $ab_num_days);
    $trans_load->add_data('use_night', $ab_use_night);
    $result_tr = $trans_load->send_command('price_autobump');
    if (isset($result_tr['total_price'])) {
        return $result_tr['total_price'];
    }
    return 0;
}



function get_subjects_n_categories($param_list_ids) {
    $result['list_id'] = array();
    $result['subject'] = array();
    $result['category'] = array();
    $result['plates'] = array();

    $list_ids_to_string = implode(',',$param_list_ids);
    if ($list_ids_to_string != "") {
        $result_as = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "id:".$list_ids_to_string);
        if (!empty($result_as) && isset($result_as['list_id'])) {
            if(!is_array($result_as['list_id'])) {  // asearch returned a single result
                $result['list_id'][] = $result_as['list_id'];
                $result['subject'][] = $result_as['subject'];
                $result['category'][] = $result_as['category'];
                $result['plates'][] = isset($result_as['plates'])?$result_as['plates']:'';
            } else {
                // Here $result_as['list_id'] is an array
                $result['list_id'] = $result_as['list_id'];
                $result['subject'] = $result_as['subject'];
                $result['category'] = $result_as['category'];
                $result['plates'] = isset($result_as['plates'])?$result_as['plates']:array_fill(0, count($result_as['list_id']),'');
            }
        }
    }

    $ids_not_in_asearch = array();
    foreach($param_list_ids as $id) {
        if (!in_array($id, $result['list_id'])) {
            $ids_not_in_asearch[] = $id;
        }
    }

    if (!empty($ids_not_in_asearch)) {
        $trans_load = new bTransaction();
        $trans_load->add_data('list_id', implode('|',$ids_not_in_asearch));
        $trans_load->add_data('ad_status', 'active|deactivated');
        $result_tr = $trans_load->send_command('get_ads_by_listid');

        if (!$trans_load->has_error(true) && !empty($result_tr) && !empty($result_tr['ad_status'])) {
            if (!is_array($result_tr['list_id'])) {  // asearch returned a single result
                $result['list_id'][] = $result_tr['list_id'];
                $result['subject'][] = $result_tr['subject'];
                $result['category'][] = $result_tr['category'];
            } else {
                $result['list_id'] = array_unique(array_merge($result['list_id'], $result_tr['list_id']));
                $result['subject'] = array_unique(array_merge($result['subject'], $result_tr['subject']));
                $result['category'] = array_unique(array_merge($result['category'], $result_tr['category']));
            }
        }
    }

    return $result;
}

function get_single_ad($param_list_id, $allowed_statuses = 'active|deactivated') {
    if (is_null($param_list_id) || empty($param_list_id)) {
        return NULL;
    }
    $result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "id:".$param_list_id);
    if (!empty($results) && isset($result['list_id'])) {
        return $result;
    }

    $trans_load = new bTransaction();
    $trans_load->add_data('list_id', $param_list_id);
    $trans_load->add_data('ad_status', $allowed_statuses);
    $trans_load->add_data('with_user', '1');
    $result = $trans_load->send_command('get_ads_by_listid');
    if (!$trans_load->has_error(true) && !empty($result) && !empty($result['list_id'])) {
        return $result;
    }

    return NULL;
}
