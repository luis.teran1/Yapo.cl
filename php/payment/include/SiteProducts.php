<?php

namespace Yapo;

class SiteProducts
{
    const PARAM_PROVIDED = "provided_params";
    const PARAM_DEFAULT = "default_params";
    private $skipDefaults = array("commit");
    private $trans;
    private $cmdConf;
    private $commit;

    public function __construct($trans, $commit = true)
    {
        global $BCONF;
        $this->trans = $trans;
        $this->commit = $commit;
        $this->cmdConf = Bconf::bconfGet($BCONF, "*.upselling_settings.command");
    }

    public function genericApply(array $productInfo, array $extraData = array())
    {
        $this->trans->reset();
        $prod_id = $productInfo['product_id'];
        Logger::logDebug(__METHOD__, "Product id to apply {$prod_id}");
        // Pack special case
        if (Products::isPack($prod_id)) {
            $prod_id = Products::PACK_GROUP;
        }
        if (Products::isCombo($prod_id)) {
            $prod_id = Products::COMBO_GROUP;
        }
        if (Products::isPromo($prod_id)) {
            $prod_id = Products::PROMO_BUMP;
        }
        // Populate purchase info for product
        foreach (Bconf::getParams($this->cmdConf, $prod_id, self::PARAM_PROVIDED) as $node) {
            $key = $node['value'];
            if (isset($productInfo[$key])) {
                $this->trans->add_data($key, $productInfo[$key]);
            }
        }
        // Populate default params for product
        $default_params = Bconf::getParams($this->cmdConf, $prod_id, self::PARAM_DEFAULT);
        foreach ($default_params as $node) {
            $default = explode(':', $node['value']);
            if (in_array($default[0], $this->skipDefaults)) {
                continue;
            }
            $this->trans->add_data($default[0], $default[1]);
        }
        // Populate extraData (Optional) for product
        foreach ($extraData as $key => $value) {
            $this->trans->add_data($key, $value);
        }

        $command = Bconf::getParams($this->cmdConf, $prod_id, "cmd");
        Logger::logDebug(__METHOD__, "Trans to call {$command}");

        if ($this->commit) {
            $reply = $this->trans->send_command($command);
        } else {
            $reply = $this->trans->validate_command($command);
        }

        if ($this->trans->has_error(true)) {
            $errors = $this->trans->get_errors();
            Logger::logError(
                __METHOD__,
                "error applying {$command} get_error:".var_export($errors, true)." Reply:".var_export($reply, true)
            );
            return array(false, $reply);
        }
        return array(true, $reply);
    }

    public function markAsError($paymentGroup, $error)
    {
        $this->trans->reset();
        $this->trans->add_data('payment_group_id', $paymentGroup);
        $this->trans->add_data('name', 'error');
        $this->trans->add_data('value', $error);
        if ($this->commit) {
            $this->trans->send_command("add_purchase_detail_param");
        } else {
            $this->trans->validate_command("add_purchase_detail_param");
        }
    }
}
