<?php
namespace Yapo;

use bTransaction;
use Exception;

class ConfirmPayment
{

    const AUTHORIZED = "AUTHORIZED";
    const CLEAR_SUCCESS = "CLEAR_SUCCESS";
    const ERR_CANCELLED = "ERR_CANCELLED";
    const CREDITS = "credits";
    const ERR_CLEAR_ERROR = "ERR_CLEAR_ERROR";

    private $applyProduct;
    private $pTrans;
    private $trans;

    public function __construct()
    {
        $this->trans = new bTransaction();
        $siteProducts = new SiteProducts($this->trans);
        $this->applyProduct = new ApplyProducts($siteProducts);
        $this->pTrans = new PurchaseTrans();
    }

    public function validatePayment($externalOrderId)
    {

        $tbk_states = $this->pTrans->getTbkState($externalOrderId);
        Logger::logDebug(__METHOD__, "External order id :".$externalOrderId);

        //if trans return only one row convert the data into array
        if (!is_array($tbk_states['price'])) {
            $tbk_states['price'] = array($tbk_states['price']);
        }
        if (!is_array($tbk_states['payment_detail'])) {
            $tbk_states['payment_detail'] = array($tbk_states['payment_detail']);
        }
        if (!is_array($tbk_states['product_id'])) {
            $tbk_states['product_id'] = array($tbk_states['product_id']);
        }

        Logger::logDebug(__METHOD__, "tbk_states: ".print_r($tbk_states, true));

        //Clear each action of detail
        for ($i = 0; $i < count($tbk_states['payment_detail']); $i++) {
            $product = $tbk_states['product_id'][$i];
            $is_store = bconf_get($BCONF, "*.payment.product.$product.is_store");
            $is_for_ads = bconf_get($BCONF, "*.payment.product.$product.is_for_ads");

            if ($is_for_ads || $is_store) {
                Logger::logInfo(
                    __METHOD__,
                    "Calling clear transaction for payment_detail_id: ".$tbk_states['payment_detail'][$i]
                );
                $clear = $this->pTrans->doClear(
                    $tbk_states['payment_detail'][$i],
                    (int)$tbk_states['price'][$i]
                );
                if (!$clear) {
                    return self::ERR_CLEAR_ERROR;
                }
            }
        }

        //Pay purchase
        if (count($tbk_states['payment_detail']) > 0) {
            $trans = $this->trans->reset();
            $trans->add_data('tbk_purchase_order', (int)$externalOrderId);
            $trans->add_data('tbk_state', 'payment_ok');
            $reply = $trans->send_command('tbk_add_state');
            if ($trans->has_error(true)) {
                Logger::logInfo(__METHOD__, "Fail confirm purchase Order:$externalOrderId");
                return self::ERR_CLEAR_ERROR;
            }
        }

        return self::CLEAR_SUCCESS;
    }

    public function applyProducts($externalOrderId)
    {
        $result = array();
        $purchase = $this->pTrans->getPurchase($externalOrderId);

        $user_email = $this->pTrans->getInfo('email');
        $account_id = $this->pTrans->getInfo('account_id');
        $user_name = $this->pTrans->getInfo('name');
        $payment_method = $this->pTrans->getInfo('payment_method');
        $purchase_id = $this->pTrans->getInfo('purchase_id');

        $result = $this->applyProduct->run($externalOrderId, $purchase);

        if ($this->applyProduct->hasValidItems()) {
            if ($this->applyProduct->hasPacks() || $this->applyProduct->hasStores()) {
                $trans = $this->trans->reset();
                $trans->add_data('account_id', $account_id);
                $account = $trans->send_command('get_account');
                if ($account['status'] == "TRANS_OK") {
                    $acc_email = $account['email'];
                    $acc_name = $account['email'];
                    if ($this->applyProduct->hasPacks()) {
                        list($packs_product_ids, $packs_expires) = $this->applyProduct->getPackInfoMail();
                        PaymentMails::sendPackPurchaseMail(
                            $acc_email,
                            $acc_name,
                            $packs_product_ids,
                            $packs_expires
                        );
                    }
                    if ($this->applyProduct->hasStores()) {
                        PaymentMails::sendStorePurchaseMail(
                            $account_id,
                            $acc_email,
                            $acc_name,
                            $this->applyProduct->getStoreProduct()
                        );
                    }
                } else {
                    Logger::logInfo(__METHOD__, "ERROR Sending email for account_id: $account_id");
                }
            }
            if ($this->applyProduct->mustMail() && $payment_method != self::CREDITS) {
                list($product_ids, $ad_prices, $ad_subjects) = $this->applyProduct->getProductInfo();
                list($creds_balance, $creds_added) = $this->applyProduct->getCreditsInfo();
                PaymentMails::sendMultiproductMail(
                    $user_email,
                    $product_ids,
                    $ad_prices,
                    $this->applyProduct->getDocType(),
                    $creds_balance,
                    $creds_added
                );
            }

            $this->pTrans->updatePurchase($purchase_id, 'paid');
        }

        return $result;
    }


    public function refusePayment($payment_group_id)
    {
        return $this->pTrans->updatePurchase($payment_group_id, 'refused');
    }

    public function authorize($payment_group_id, $transaction_id, $card_last_digits, $authorization_code)
    {
        $purchase = $this->pTrans->getPurchase($payment_group_id);
        $status = 'Unauthorized';

        if ($this->pTrans->isEmpty()) {
            $status = "Unauthorized";
            Logger::logError(__METHOD__, "Oneclick payment group id not exists: " . print_r($payment_group_id, true));
            return $status;
        }
        $tbk_state = $this->pTrans->getTbkState($payment_group_id);
        if (is_null($tbk_state)) {
            Logger::logDebug(__METHOD__, "Oneclick purchase state not found: ". print_r($payment_group_id, true));
            $status = "Unauthorized";
            return $status;
        }
        if (!is_array($tbk_state['payment_groups_status'])) {
            $tbk_state['payment_groups_status'] = array($tbk_state['payment_groups_status']);
        }
        if (!is_array($tbk_state['total_price'])) {
            $tbk_state['total_price'] = array($tbk_state['total_price']);
        }
        if (!is_array($tbk_state['pay_log_status'])) {
            $tbk_state['pay_log_status'] = array($tbk_state['pay_log_status']);
        }

        for ($i = 0; $i < count($tbk_state['payment_groups_status']); $i++) {
            if ($tbk_state['payment_groups_status'][$i] != 'unverified' || $tbk_state['pay_log_status'][$i] != 'SAVE') {
                if ($tbk_state['payment_groups_status'][$i] == 'paid' && $tbk_state['pay_log_status'][$i] == 'OK') {
                    Logger::logDebug(__METHOD__, "OneClick RE-Authorized order $payment_group_id");
                    return "reauthorized";
                }
                Logger::logDebug(__METHOD__, "Order Duplicated");
                $status = "Unauthorized";
                return $status;
            }
        }

        $data_authorization = array(
            'tbk_ref_auth_code' => $authorization_code,
            'tbk_ref_pay_type' => 'credit',
            'tbk_ref_placements_type' => 'VN',//Sin cuotas
            'tbk_ref_placements_number' => '0',
            'tbk_ref_external_id' => $transaction_id,
            'tbk_ref_cc_number' => $card_last_digits
        );

        if ($this->pTrans->authorizePayment($payment_group_id, 'authorized', $data_authorization)) {
            $status = "authorized";
            Logger::logDebug(__METHOD__, "Authorizing OneClick payment");
        } else {
            Logger::logError(__METHOD__, "Trans error add state OneClick with payment_group $payment_group_id");
        }
        return $status;
    }

    public function hasError()
    {
        return $this->applyProduct->hasError();
    }

    public function authorizeExecute($payment_group_id, $transaction_id, $card_last_digits, $authorization_code)
    {
        $authorize = $this->authorize($payment_group_id, $transaction_id, $card_last_digits, $authorization_code);
        if (!$authorize) {
            throw new Exception('Error authorize in yapo payment');
        }
        //immediately payment is confirmed
        $clearStatus = $this->validatePayment($payment_group_id);
        if ($clearStatus != self::CLEAR_SUCCESS) {
            throw new Exception('Error clear purchase yapo payment');
        }
        //apply products
        $applyProducts = $this->applyProducts($payment_group_id);
        if (($applyProducts['status'] != self::AUTHORIZED) || ($this->hasError())) {
            $this->markAsError($payment_group_id);
        }
        return true;
    }

    public function sendCreditVoucher($payment_group_id, $email, $credits_used, $credits_balance)
    {
        list($product_ids, $ad_prices, $ad_subjects) = $this->applyProduct->getProductInfo();
        return PaymentMails::sendCreditVoucher(
            $payment_group_id,
            $email,
            $credits_used,
            $credits_balance,
            $product_ids,
            $ad_subjects,
            $this->pTrans->getInfo('receipt')
        );
    }

    public function markAsError($orden)
    {
        $this->trans->reset();
        $this->trans->add_data("payment_group_id", $orden);
        $this->trans->add_data("name", "fail_on_yapo");
        $this->trans->add_data("value", "1");
        $this->trans->send_command("add_purchase_param");
    }
}
