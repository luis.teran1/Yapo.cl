<?php
namespace Yapo;

use TransbankConfirmation;
use bResponse;
use bTransaction;

class WebpayVerify
{

    const REDIS = 'redis_payment';
    private $token = "";
    private $redis;
    private $trans;
    private $webpayService;
    private $webpayApply;

    public function __construct($redisInstance = null, $trans = null, $webpayService = null, $webpayApply = null)
    {
        if (!empty($_POST['token_ws'])) {
            $this->token = $_POST['token_ws'];
        }

        $this->redis = isset($redisInstance) ? $redisInstance : new EasyRedis(self::REDIS);
        $this->trans = isset($trans) ? $trans : new bTransaction();
        $this->webpayService = isset($webpayService) ? $webpayService : new WebpayService();
        $this->webpayApply = isset($webpayApply) ? $webpayApply : new WebpayApply();
    }

    public function verify()
    {

        if (empty($this->token)) {
            $this->redirectToErrorPage();
        }

        // Call to webpay plus using Tyrion
        $response = $this->webpayService->execute(
            "verifyPayment",
            array(
                "token_ws" => $this->token
            )
        );

        if (!$response || !isset($response->body->result) || !isset($response->body->responseCode)) {
            $this->redirectToErrorPage();
            return;
        }

        Logger::logDebug(__METHOD__, var_export($response->body, true));

        // Authorize and apply products
        $this->webpayApply->populate($response->body);
        $fullSuccess = $this->webpayApply->run();

        // Save info
        $this->saveTokenInRedis($this->token, $response->body->buyOrder);

        if ($fullSuccess && $response->body->responseCode == 0) {
            $this->redirectToVoucher($response->body->url);
        } elseif ($response->body->responseCode == 0) {
            //This should be an edge condition
            // - Means transbank payment is ok but yapo fails
            // - So we going to save the info in a new table to report it later
            // - And show pvf to the user with a new message.
            $this->webpayApply->markAsError($response->body->buyOrder);
            $this->redirectToVoucher($response->body->url);
        } else {
            $url = Bconf::get($BCONF, "*.common.base_url.secure");
            $url .= "/pagos";
            $this->redirectToVoucher($url);
        }
    }

    public function redirectToVoucher($url = "")
    {
        $redirectData = array();
        $simple_data = array();
        if (isset($redirect_data)) {
            $simple_data['no_footer'] = 1;
            $simple_data['no_header'] = 1;
        }

        $payment_template = Bconf::get($BCONF, "*.payment.options.5.template");
        $response = new bResponse();
        $response->add_data('appl', "pvf");
        $response->add_data('url', $url);
        $redirect_data = array(
            "method" => "POST",
            "url" => $url,
            "params" => array(
                "token_ws" => $this->token
            )
        );
        $response->add_extended_array($redirect_data);
        $response->add_data("REMOTE_BROWSER", $_SERVER['HTTP_USER_AGENT']);
        $response->add_data("content", $payment_template);
        $response->show_html_results("responsive_base");
    }

    public function redirectToErrorPage()
    {
        header('Location: /pagos/expired/0');
        exit();
    }

    private function saveTokenInRedis($token, $order)
    {
        $redis_key = $this->redis->prepend.$token;
        $redis_ttl = 600;
        $this->redis->setex($redis_key, $redis_ttl, $order);
    }
}
