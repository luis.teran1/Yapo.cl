<?php
require_once('blocket_application.php');
require_once('util.php');
use Yapo\Logger;

class TransbankConfirmation {

	var $purchase_order;
	var $price;
	var $state;
	var $ad_id;
	var $product_id;
	var $purchase_id;
	var $action_id;
	var $pay_log_status;
	var $callbackData = array();
	var $acceptedPayment = true;
	var $authorizedPayment = true;
	var $statusPayment = 'authorized';

	public function authorize($tbkData, $is_telesales_payment = false, $validateMac = true) {
		Logger::logDebug(__METHOD__,  "Starting authorize".var_export($tbkData,true));
		$this->tbkData = $tbkData;

		/* Validate all the relevant params */
		if(!isset($tbkData->TBK_ID_SESION) || empty($tbkData->TBK_ID_SESION)
			|| !isset($tbkData->TBK_ORDEN_COMPRA) || empty($tbkData->TBK_ORDEN_COMPRA)
			|| !isset($tbkData->TBK_RESPUESTA) 
			|| !isset($tbkData->TBK_MONTO) || empty($tbkData->TBK_MONTO)) {
			$this->setPaymentStatus('ERROR_MISSING_TBK_DATA');
			return $this->rejectPayment();
		}

		$purchase = $this->get_purchase($tbkData->TBK_ORDEN_COMPRA);
		Logger::logDebug(__METHOD__,  "Purchase by OrdenCompra:".var_export($purchase,true));
		$this->returnPayment = $purchase['purchase_get'][0];
		if (count((array) $this->returnPayment) == 0) {
			$this->setPaymentStatus('ERR_GET_TBK_STATE');
			return $this->rejectPayment();
		}

		$tbk_state = $this->get_tbk_state($tbkData->TBK_ORDEN_COMPRA);
		if(is_null($tbk_state)) {
			Logger::logDebug(__METHOD__,  "Purchase by OrdenCompra:pay auth: purchase order:{$tbkData->TBK_ORDEN_COMPRA} not found");
			$this->setPaymentStatus('ERR_GET_TBK_STATE');
			return $this->rejectPayment();
		}

		if(strval($tbkData->TBK_RESPUESTA) != "0") { 
			Logger::logDebug(__METHOD__,  "Purchase Cancelled");
			$this->setPaymentStatus('ERR_CANCELLED');
			return $this->deAuthorize();
		}
		else {
			if($validateMac && !$this->tbkMacValidation($tbkData, $is_telesales_payment)) {
				Logger::logDebug(__METHOD__,  "MacValidation Failed!");
				$this->setPaymentStatus('ERR_MAC_INVALID');
				return $this->rejectPayment();
			}
		}
		Logger::logDebug(__METHOD__,  "MacValidation Success!");
		
		if(!is_array($tbk_state['payment_groups_status']))
			$tbk_state['payment_groups_status'] = Array($tbk_state['payment_groups_status']);
		if(!is_array($tbk_state['total_price']))
			$tbk_state['total_price'] = Array($tbk_state['total_price']);
		if(!is_array($tbk_state['pay_log_status']))
			$tbk_state['pay_log_status'] = Array($tbk_state['pay_log_status']);

		for($i = 0; $i < count($tbk_state['payment_groups_status']) ; $i++) {
			if($tbk_state['payment_groups_status'][$i] != 'unverified' || $tbk_state['pay_log_status'][$i] != 'SAVE') {
				Logger::logDebug(__METHOD__,  "Order Duplicated");
				$this->setPaymentStatus('ERR_DUP_ORDER');
				return $this->rejectPayment();
			}
		}
		if ($tbkData->TBK_MONTO != ($tbk_state['total_price'][0] * 100)) {
			Logger::logDebug(__METHOD__,  "Price Wrong");
			$this->setPaymentStatus('ERR_PRICE_INCORRECT');
			return $this->rejectPayment();
		}
		
		return $this->completeTransbankPayment();
	}

	function getPaymentStatus() { return $this->statusPayment; }
	function setPaymentStatus($status) { $this->statusPayment = $status; }
	function isPaymentOK() { return $this->acceptedPayment; }

	function acceptPayment($completePayment = true) {
		$this->acceptedPayment = true;
		if ($completePayment)
			return $this->completeTransbankPayment();
	}

	function rejectPayment($completePayment = true) { 
		$this->acceptedPayment = false;
		if ($completePayment)
			return $this->completeTransbankPayment();
	}

	function deAuthorize($completePayment = true) { 
		$this->acceptedPayment = true;
		$this->authorizedPayment = false;
		if ($completePayment)
			return $this->completeTransbankPayment();
	}

	function cancelPayment($completePayment = true) { 
		return $this->completeTransbankPayment(true);
	}

	function completeTransbankPayment($accept = null) {
		if ($accept == null)
			$accept = $this->isPaymentOK();

		if (!$this->callTransAddState()) {
			$this->setPaymentStatus('ERR_ADD_STATE_FAILED');
			$accept = false;
		}

		$result = new stdClass;
		$result->isAccepted = $accept;
		$result->isAuthorized = $this->authorizedPayment;
		$result->status = $this->getPaymentStatus();
		return $result;
	}

	function get_tbk_state($TBK_ORDEN_COMPRA) {
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		if(isset($TBK_ORDEN_COMPRA) && !empty($TBK_ORDEN_COMPRA))
			$transaction->add_data('payment_group_id', $TBK_ORDEN_COMPRA);

		$reply = $transaction->send_command('tbk_get_state');

		if($transaction->has_error() || empty($reply['status']) )
			return null;

		return $reply;
	}

	function get_purchase($orden) {
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		if(isset($orden) && !empty($orden))
			$transaction->add_data('payment_group_id', $orden);
		$reply = $transaction->send_command('purchase_get');

		if($transaction->has_error() || empty($reply['status']) )
			return null;

		return $reply;
	}

	function callTransAddState() {
		Logger::logDebug(__METHOD__, "Starting AddState");
		if(!isset($this->returnPayment) || !isset($this->returnPayment['payment_group_id']) || !isset($this->tbkData)) {
			Logger::logError(__METHOD__,"Failed to add state : ".var_export($this->returnPayment, true)." - ".var_export($this->tbkData, true));
			return false;
		}

		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->add_client_info();
		$transaction->add_data('tbk_ref_auth_code', $this->tbkData->TBK_CODIGO_AUTORIZACION);

		/* Format data of date and hour to save this information  */
		if (!empty($this->tbkData->TBK_FECHA_TRANSACCION) && !empty($this->tbkData->TBK_HORA_TRANSACCION)) {
			$tmp_tbk_date = substr($this->tbkData->TBK_FECHA_TRANSACCION,-2).'/'.substr($this->tbkData->TBK_FECHA_TRANSACCION,0,2).'/'.date('Y');
			$tmp_tbk_hour = substr($this->tbkData->TBK_HORA_TRANSACCION,0,2).':'.substr($this->tbkData->TBK_HORA_TRANSACCION,2,2).':'.substr($this->tbkData->TBK_HORA_TRANSACCION,-2);
			$transaction->add_data('tbk_ref_trans_date', 		$tmp_tbk_date.' '.$tmp_tbk_hour);
		}
		if (!empty($this->tbkData->TBK_TIPO_PAGO)) {
			$transaction->add_data('tbk_ref_placements_type', 	$this->tbkData->TBK_TIPO_PAGO);
		}

		if (!empty($this->tbkData->TBK_NUMERO_CUOTAS)) {
			$transaction->add_data('tbk_ref_placements_number', 	$this->tbkData->TBK_NUMERO_CUOTAS);
		}
		if (!empty($this->tbkData->TBK_ID_TRANSACCION)) {
			$transaction->add_data('tbk_ref_external_id', 		$this->tbkData->TBK_ID_TRANSACCION);
		}
		if (!empty($this->tbkData->TBK_FINAL_NUMERO_TARJETA)) {
			$transaction->add_data('tbk_ref_cc_number', 		$this->tbkData->TBK_FINAL_NUMERO_TARJETA);
		}
		if($this->tbkData->TBK_TIPO_PAGO == 'VD')
			$transaction->add_data('tbk_ref_pay_type', 'debit');
		else
			$transaction->add_data('tbk_ref_pay_type', 'credit');
			
		$transaction->add_data('tbk_purchase_order', (int)$this->returnPayment['payment_group_id']);

		if(isset($this->tbkData->TBK_RESPUESTA))
			$transaction->add_data('tbk_response', $this->tbkData->TBK_RESPUESTA);

		$transaction->add_data('tbk_state', $this->getPaymentStatus());
		$reply = $transaction->send_command('tbk_add_state');

		if($transaction->has_error())
			return false;
		return true;
	}

	function tbkMacValidation($tbkData,$is_telesales_payment = false) {
		global $BCONF;
		$is_regress = bconf_get($BCONF, "*.common.is_regress");
		if(empty($is_regress)) {

			// Path to File generated for MAC validation 
			$filename_txt = bconf_get($BCONF, "*.payment.transbank.tempfile")."$tbkData->TBK_ORDEN_COMPRA_$tbkData->TBK_ID_SESION.txt";
			// Parh to Checkmac
			$cmdline = bconf_get($BCONF, "*.payment.transbank.check_mac")." ".$filename_txt;
			if($is_telesales_payment)
				$cmdline = bconf_get($BCONF, "*.payment.telesales.check_mac")." ".$filename_txt;

			/* Write parameters in file */
			$fp=fopen($filename_txt,"wt");
			fwrite($fp, "TBK_ORDEN_COMPRA=$tbkData->TBK_ORDEN_COMPRA&");
			fwrite($fp, "TBK_TIPO_TRANSACCION=$tbkData->TBK_TIPO_TRANSACCION&");
			fwrite($fp, "TBK_RESPUESTA=$tbkData->TBK_RESPUESTA&");
			fwrite($fp, "TBK_MONTO=$tbkData->TBK_MONTO&");
			fwrite($fp, "TBK_CODIGO_AUTORIZACION=$tbkData->TBK_CODIGO_AUTORIZACION&");
			fwrite($fp, "TBK_FINAL_NUMERO_TARJETA=$tbkData->TBK_FINAL_NUMERO_TARJETA&");
			fwrite($fp, "TBK_FECHA_CONTABLE=$tbkData->TBK_FECHA_CONTABLE&");
			fwrite($fp, "TBK_FECHA_TRANSACCION=$tbkData->TBK_FECHA_TRANSACCION&");
			fwrite($fp, "TBK_HORA_TRANSACCION=$tbkData->TBK_HORA_TRANSACCION&");
			fwrite($fp, "TBK_ID_SESION=$tbkData->TBK_ID_SESION&");
			fwrite($fp, "TBK_ID_TRANSACCION=$tbkData->TBK_ID_TRANSACCION&");
			fwrite($fp, "TBK_TIPO_PAGO=$tbkData->TBK_TIPO_PAGO&");
			fwrite($fp, "TBK_NUMERO_CUOTAS=$tbkData->TBK_NUMERO_CUOTAS&");
			if(!$is_telesales_payment)
				fwrite($fp, "TBK_VCI=$tbkData->TBK_VCI&");
			fwrite($fp, "TBK_MAC=$tbkData->TBK_MAC");
			fclose($fp);

			exec ($cmdline, $result, $retint);
			/* Removing file  */
			unlink($filename_txt);

			if ($result [0] =="CORRECTO") {
				return true;
			} else {
				return false;
			}

		}else{
			return true;	
		}
	}

	function getExtraDataArrayOnValidation($requestData, $data)	{
		if($requestData->TBK_TIPO_PAGO == 'VD')
			$paymentType = 'debit';
		else
			$paymentType = 'credit';

		$redisData = array();
		$redisData["cc_number"] = "**** **** **** ".$requestData->TBK_FINAL_NUMERO_TARJETA;
		// $redisData["commerce"] = "Yapo.cl";
		// $redisData["operation_type"] = $requestData->TBK_TIPO_TRANSACCION;
		$redisData["payment_type"] = $paymentType;
		$redisData["installments_type"] = $requestData->TBK_TIPO_PAGO;
		$redisData["installments_number"] = $requestData->TBK_NUMERO_CUOTAS;
		$redisData["authorization_code"] = $requestData->TBK_CODIGO_AUTORIZACION;
		$redisData["payment_status"] = $data->status;
		$redisData["payment_accepted"] = $data->isAccepted;
		$redisData["payment_authorized"] = $data->isAuthorized;
		return $redisData;
	}

	function getExtraDataArrayOnStartPayment($data, $resultData) {
		$redisData = array();
		$redisData["payment_status"] = "unverified";
		$redisData["region"] = $data->region;
		$date = strtotime($resultData->getOperationDate());
		$redisData["operation_date"] = date('d/m/Y G:i:s', $date); 
		return $redisData;
	}

}
?>
