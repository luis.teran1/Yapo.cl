<?php

namespace Yapo;

use \Exception;

class VoucherDocument
{
    protected $pdf;
    private $logo;
    public $user_data;
    public $detail_data;
    public $total;
    public $doc_num;

    const LINE_HEIGHT = 6;
    const PREFIX_LANG = 'V';

    public function __construct($add_image = true)
    {
        $this->pdf = new PdfManager();
        $this->pdf->AliasNbPages();
        $this->pdf->AddPage();
        if ($add_image) {
            $this->logo = SERVER_ROOT."/www-ssl/img/logo_pdf.jpg";
            $this->pdf->Image($this->logo, 10, 10, 50);
        }
    }

    /**
     * Method validate if the data needed was setted before generate the document
     */
    public function validate()
    {
        if (empty($this->doc_num)) {
            throw new Exception("NUM_DOC_MISSING");
        }
        if (empty($this->user_data)) {
            throw new Exception("USER_DATA_MISSING");
        }
        if (empty($this->detail_data)) {
            throw new Exception("DETAIL_DATA_MISSING");
        }
        if (empty($this->total)) {
            throw new Exception("TOTAL_MISSING");
        }
    }

    /**
     * Public method to conect all functions and generate the new PDF
     *  By default $output type is "I"
     * - "I" display the pdf in the same browser
     * - "D" produce the file download
     */
    public function generate($output_type = "I")
    {
        $this->validate();
        $this->docNumTable($this->doc_num);
        $this->userDataTable($this->user_data);
        $this->detailDataTable($this->detail_data);
        $this->footerTable($this->total);

        // Generate PDF Output
        $this->pdf->Output("voucher_{$this->doc_num}.pdf", $output_type);
    }

    /**
     * Write the Document type and number table on pdf output
     */
    protected function docNumTable($numDoc)
    {
        $width = 60;
        $left = 130;
        $this->changeColors("red");
        $this->updateFont('courier', 13);
        $this->pdf->Cell($left);
        $this->pdf->Cell($width, self::LINE_HEIGHT, $this->lang("VOUCHER"), "LTR", 0, "C", false);
        $this->pdf->Ln();
        $this->pdf->Cell($left);
        $this->pdf->Cell($width, self::LINE_HEIGHT, $this->lang("CREDITS_USE"), "LR", 0, "C", false);
        $this->pdf->Ln();
        $this->pdf->Cell($left);
        $this->pdf->Cell($width, (self::LINE_HEIGHT + 3), "N� {$numDoc}", "LBR", 0, "C", false);
        $this->pdf->Ln();
    }

    /**
     * Write the user information table on pdf output
     */
    protected function userDataTable($data)
    {
        $this->pdf->SetY(40);
        $w = array(30, 80);

        $this->changeColors();

        $keys = array(
            $this->lang("DEAR"),
            $this->lang("RUT"),
            $this->lang("EMISSION"),
            $this->lang("COMMUNE")
        );

        // Data
        $loop = 0;
        foreach ($data as $val) {
            $this->updateFont();
            $this->pdf->Cell($w[0], self::LINE_HEIGHT, $keys[$loop], 1, 0, 'L', true);
            $this->updateFont('courier');
            $this->pdf->Cell($w[1], self::LINE_HEIGHT, $val, 1, 0, 'L', false);
            $this->pdf->Ln();
            $loop++;
        }
        // End line
        $this->pdf->Cell(array_sum($w), self::LINE_HEIGHT, '', 'T');
        $this->pdf->Ln();
    }

    /**
     * Write the detail table on pdf output
     */
    protected function detailDataTable($data)
    {

        $this->changeColors();

        // Define the column width
        $w = array(95, 25, 30, 40);

        // Header
        $this->updateFont();
        $this->pdf->Cell(array_sum($w), self::LINE_HEIGHT, $this->lang("DETAIL"), 1, 0, 'C', true);
        $this->pdf->Ln();
        $this->pdf->Cell($w[0], self::LINE_HEIGHT, $this->lang("DESCRIPTION"), 1, 0, 'C', true);
        $this->pdf->Cell($w[1], self::LINE_HEIGHT, $this->lang("QUANTITY"), 1, 0, 'C', true);
        $this->pdf->Cell($w[2], self::LINE_HEIGHT, $this->lang("UNIT_PRICE"), 1, 0, 'C', true);
        $this->pdf->Cell($w[3], self::LINE_HEIGHT, $this->lang("TOTAL"), 1, 0, 'C', true);
        $this->pdf->Ln();

        // Body
        $this->updateFont('courier');
        $loop = 1;
        foreach ($data as $row) {
            $this->pdf->Cell($w[0], self::LINE_HEIGHT, $row[0], 1, 0, 'L', false);
            $this->pdf->Cell($w[1], self::LINE_HEIGHT, $row[1], 1, 0, 'R', false);
            $this->pdf->Cell($w[2], self::LINE_HEIGHT, $row[2], 1, 0, 'R', false);
            $this->pdf->Cell($w[3], self::LINE_HEIGHT, $row[3], 1, 0, 'R', false);
            $this->pdf->Ln();
            $loop++;
            if ($loop == 29) {
                $this->pdf->AddPage();
            }
        }
        $this->pdf->Cell(array_sum($w), self::LINE_HEIGHT, '', 0);
        $this->pdf->Ln();
    }

    /**
     * Write the footer table on pdf output
     */
    protected function footerTable($total)
    {
        $width = 60;
        $left = 130;
        $this->changeColors();
        $this->updateFont('arial');
        $this->pdf->Cell($left, self::LINE_HEIGHT, $this->lang("INFO_TRANSACTION"), 0, 0, "L", false);
        $this->updateFont();
        $this->pdf->Cell($width, self::LINE_HEIGHT, $this->lang("TOTALS"), 1, 0, "C", true);
        $this->pdf->Ln();
        $this->updateFont('arial');
        $this->textChangeColor("red");
        $this->pdf->Cell($left, self::LINE_HEIGHT, $this->lang("NOT_VALID_DOC"), 0, 0, "L", false);
        $this->updateFont();
        $this->textChangeColor("black");
        $this->pdf->Cell($width/2, self::LINE_HEIGHT, $this->lang("TOTAL_CREDITS"), 1, 0, "L", true);
        $this->updateFont('courier');
        $this->pdf->Cell($width/2, self::LINE_HEIGHT, "$total", 1, 0, "R", false);
        $this->pdf->Ln();
    }

    /**
     * Change the color of each Cell and set the line width
     * - default is Grey with line thin
     */
    private function changeColors($color = null)
    {
        switch ($color) {
            case 'red':
                $this->pdf->SetDrawColor(255, 0, 0);
                $this->pdf->SetLineWidth(.6);
                break;
            default:
                $this->pdf->SetFillColor(211, 211, 211);
                $this->pdf->SetDrawColor(169, 169, 169);
                $this->pdf->SetLineWidth(.1);
                break;
        }
    }

    /**
     * Change the text color on pdf output
     * - default is black
     */
    private function textChangeColor($color)
    {
        switch ($color) {
            case 'red':
                $this->pdf->SetTextColor(255, 0, 0);
                break;
            default:
                $this->pdf->SetTextColor(0, 0, 0);
                break;
        }
    }

    /**
     * Change the font option on pdf output
     * - default is Arial Bold with size 8
     */
    private function updateFont($op = null, $size = 8)
    {
        switch ($op) {
            case 'courier':
                $this->pdf->SetFont('Courier', '', $size);
                break;
            case 'arial':
                $this->pdf->SetFont('Arial', '', $size);
                break;
            default:
                $this->pdf->SetFont('Arial', 'B', $size);
                break;
        }
    }

    /**
     *  Return the key from language using voucher prefix
     */
    private function lang($key)
    {
        global $BCONF;
        return Bconf::lang($BCONF, self::PREFIX_LANG."_{$key}");
    }
}
