<?php

namespace Yapo;

use bTransaction;

class PurchaseTrans
{
    private $purchase;

    public function getPurchase($orden)
    {
        $transaction = new bTransaction();
        $transaction->add_data('log_string', log_string());
        if (isset($orden) && !empty($orden)) {
            $transaction->add_data('payment_group_id', $orden);
        }
        $reply = $transaction->send_command('purchase_get');

        if ($transaction->has_error() || empty($reply['status'])) {
            return null;
        }

        $this->purchase = $reply;

        return $reply;
    }

    public function updatePurchase($purchase_id, $status)
    {
        $transaction = new bTransaction();
        $transaction->add_data('purchase_id', $purchase_id);
        $transaction->add_data('status', $status);
        $trans_reply = $transaction->send_command("purchase_update");
        if ($transaction->has_error()) {
            Logger::logError(
                __METHOD__,
                "Pay auth: updatePurchase failed: "
                .var_export($transaction->get_errors(), true)
                ."\n"
                .var_export($trans_reply, true)
            );
            return false;
        }
        return true;
    }

    public function getInfo($param)
    {
        if (isset($this->purchase['purchase_get'][0]) && is_array($this->purchase['purchase_get'][0])) {
            Logger::logError(__METHOD__, "Param:$param value:".print_r($this->purchase['purchase_get'][0][$param],true));
            return $this->purchase['purchase_get'][0][$param];
        }
        return null;
    }

    public function isEmpty()
    {
        if (isset($this->purchase['purchase_get'][0]) && is_array($this->purchase['purchase_get'][0])) {
            if (count((array)$this->purchase['purchase_get'][0]) == 0) {
                return true;
            }
        }
        return false;
    }

    public function getTbkState($TBK_ORDEN_COMPRA)
    {
        $transaction = new bTransaction();
        $transaction->add_data('log_string', log_string());
        if (isset($TBK_ORDEN_COMPRA) && !empty($TBK_ORDEN_COMPRA)) {
            $transaction->add_data('payment_group_id', $TBK_ORDEN_COMPRA);
        }
        $reply = $transaction->send_command('tbk_get_state');

        if ($transaction->has_error() || empty($reply['status'])) {
            Logger::logError(
                __METHOD__,
                "Pay auth: get_state call failed: "
                .var_export($transaction->get_errors(), true)
                ."\n"
                .var_export($reply, true)
            );
            return null;
        }

        return $reply;
    }

    public function authorizePayment($payment_group_id, $new_state = "authorized", $params = null)
    {
        $trans = new bTransaction();
        $trans->add_data('tbk_purchase_order', $payment_group_id);
        $trans->add_data('tbk_state', $new_state);
        if (isset($params)) {
            foreach ($params as $key => $val) {
                if (!empty($val)) {
                    $trans->add_data($key, $val);
                }
            }
        }

        $trans->send_command("tbk_add_state");
        if ($trans->has_error()) {
            Logger::logError(__METHOD__, "Trans error add state with payment_group $payment_group_id");
            return false;
        }

        return true;
    }

    public function doClear($order_id = 0, $amount = 0)
    {
        $transaction = new bTransaction();
        $transaction->add_data('order_id', '00'.$order_id.'a');
        $transaction->add_data('status', 'OK');
        $transaction->add_data('ref_type', 'webpay');
        $transaction->add_data('reference', '1');
        $transaction->add_data('amount', $amount);

        $clear_reply = $transaction->send_command("clear", 1);
        if ($transaction->has_error(true)) {
            Logger::logInfo(
                __METHOD__,
                "Fail clear transaction for order_id: "
                .$order_id
                ." with status: '"
                .$clear_reply['status']
                ."'."
            );
            return false;
        }
        return true;
    }
}
