<?php

namespace Yapo;

class Products
{
    // For ads
    const BUMP = 1;
    const UPSELLING_BUMP = 105;
    const AUTOBUMP = 10;
    const DAILY_BUMP = 8;
    const UPSELLING_DAILY_BUMP = 103;
    const WEEKLY_BUMP = 2;
    const UPSELLING_WEEKLY_BUMP = 101;
    const GALLERY_7 = 3;
    const GALLERY_1 = 31;
    const GALLERY_30 = 32;
    const UPSELLING_GALLERY_7 = 102;
    const LABEL = 9;
    const UPSELLING_LABEL = 104;
    const AUTOFACT = 20;
    const INSERTING_FEE = 60;
    const PACK2IF_CARS = 61;
    const PACK2IF_INMO = 62;
    const IF_PACK_CAR_PREMIUM = 1080;
    const AT_COMBO1 = 1051;
    const AT_COMBO2 = 1052;
    const AT_COMBO3 = 1053;
    const DISCOUNT_COMBO1 = 1062;
    const DISCOUNT_COMBO2 = 1065;
    const DISCOUNT_COMBO3 = 1030;
    const DISCOUNT_COMBO4 = 1063;
    const PROMO_BUMP = 1064;

    // For accounts
    const CREDITS = 50;
    const STORE_MONTHLY = 4;
    const STORE_QUARTERLY = 5;
    const STORE_BIANUAL = 6;
    const STORE_ANNUAL = 7;

    // Product groups
    const PACK_GROUP = 10000;
    const COMBO_GROUP = 1000;

    // Group Names
    const GROUP_COMBO_UPSELLING = "COMBO_UPSELLING";
    const GROUP_COMBO_DASHBOARD = "COMBO_DASHBOARD";
    const GROUP_COMBO_DISCOUNT = "COMBO_DISCOUNT";
    const GROUP_PROMO = "PROMO";
    const GROUP_STORE = "STORE";
    const GROUP_PACK = "PACK";
    const GROUP_CREDITS = "CREDITS";
    const GROUP_INSERTING_FEE = "INSERTING_FEE";
    const GROUP_INSERTING_FEE_PREMIUM = "INSERTING_FEE_PREMIUM";

    // related Galleries
    public static function getAllGalleries()
    {
        return array(self::GALLERY_7, self::GALLERY_1, self::GALLERY_30, self::UPSELLING_GALLERY_7);
    }

    // related Labels
    public static function getAllLabels()
    {
        return array(self::LABEL, self::UPSELLING_LABEL);
    }

    public static function getAllPack2if()
    {
        return array(self::PACK2IF_CARS, self::PACK2IF_INMO);
    }

    public static function getAllStores()
    {
        return array(
            self::STORE_MONTHLY,
            self::STORE_QUARTERLY,
            self::STORE_BIANUAL,
            self::STORE_ANNUAL
        );
    }

    public static function getProductOrder()
    {
        return array(
            self::AUTOBUMP,
            self::BUMP,
            self::DAILY_BUMP,
            self::UPSELLING_DAILY_BUMP,
            self::WEEKLY_BUMP,
            self::UPSELLING_WEEKLY_BUMP,
            self::LABEL,
            self::UPSELLING_LABEL,
            self::GALLERY_1,
            self::GALLERY_7,
            self::GALLERY_30,
            self::UPSELLING_GALLERY_7,
            self::INSERTING_FEE,
            self::PACK2IF_CARS,
            self::PACK2IF_INMO,
            self::IF_PACK_CAR_PREMIUM,
            self::AT_COMBO1,
            self::AT_COMBO2,
            self::AT_COMBO3,
            self::DISCOUNT_COMBO1,
            self::DISCOUNT_COMBO2,
            self::DISCOUNT_COMBO3,
            self::DISCOUNT_COMBO4,
            self::PROMO_BUMP,
            self::STORE_MONTHLY,
            self::STORE_QUARTERLY,
            self::STORE_BIANUAL,
            self::STORE_ANNUAL
        );
    }

    public static function sortProducts(array $products)
    {
        $sorted = array();
        $packs = array();

        //Packs at the end
        foreach ($products as $item) {
            if (self::isPack($item["product_id"])) {
                $packs[] = $item;
            }
            // for upselling combos, return only this item
            if (self::isComboUpselling($item["product_id"]) || self::isPromo($item["product_id"])) {
                return array($item);
            }
        }

        $order = self::getProductOrder();
        foreach ($order as $key) {
            foreach ($products as $item) {
                if ($item["product_id"] == $key) {
                    $sorted[] = $item;
                }
            }
        }

        $sorted = array_merge($sorted, $packs);
        return $sorted;
    }

    public static function getExtraData(array $product)
    {
        global $BCONF;
        $extraData = array();
        $product_id = $product["product_id"];
        $group = Bconf::bconfGet($BCONF, "*.payment.product.$product_id.group");

        if ($product_id == self::AUTOBUMP || $product_id == self::PROMO_BUMP) {
            $ab_params = explode(";", $product["detail_params"]);
            foreach ($ab_params as $ab_param) {
                $parts = explode(":", $ab_param);
                $extraData[$parts[0]] = $parts[1];
            }
            return $extraData;
        }

        if ($group == self::GROUP_PACK) {
            $pack_type = substr($product_id, 0, 1);
            $pack_period = substr($product_id, 1, 1);
            $pack_slots = substr($product_id, 2, 3);
            $pack_type_name = Bconf::bconfGet($BCONF, "*.packs.type.$pack_type.name");
            $extraData = array(
                "type" => $pack_type_name,
                "period" => $pack_period,
                "slots" => $pack_slots
            );
            return $extraData;
        }
        if ($group == self::GROUP_STORE) {
            $expire_time = Bconf::bconfGet($BCONF, "*.payment.product.{$product_id}.expire_time");
            $extraData = array("expire_time" => $expire_time);
        }
        return $extraData;
    }

    public static function isStore($product_id)
    {
        return in_array($product_id, self::getAllStores());
    }

    public static function isPack($product_id)
    {
        global $BCONF;
        return Bconf::get($BCONF, "*.payment.product.{$product_id}.group") == self::GROUP_PACK;
    }

    public static function isCombo($product_id)
    {
        global $BCONF;
        $productGroup = Bconf::get($BCONF, "*.payment.product.{$product_id}.group");
        return $productGroup == self::GROUP_COMBO_UPSELLING || $productGroup == self::GROUP_COMBO_DASHBOARD 
        || $productGroup == self::GROUP_COMBO_DISCOUNT || $productGroup == self::GROUP_INSERTING_FEE_PREMIUM;
    }

    public static function isPromo($product_id)
    {
        global $BCONF;
        $productGroup = Bconf::get($BCONF, "*.payment.product.{$product_id}.group");
        return $productGroup == self::GROUP_PROMO;
    }

    public static function isComboUpselling($product_id)
    {
        global $BCONF;
        $productGroup = Bconf::get($BCONF, "*.payment.product.{$product_id}.group");
        return $productGroup == self::GROUP_COMBO_UPSELLING;
    }

    public static function isInsertingFeePremium($product_id)
    {
        global $BCONF;
        $productGroup = Bconf::get($BCONF, "*.payment.product.{$product_id}.group");
        return $productGroup == self::GROUP_INSERTING_FEE_PREMIUM;
    }

    public static function isLabel($product_id)
    {
        return in_array($product_id, self::getAllLabels());
    }

    public static function isCredits($product_id)
    {
        return $product_id == self::CREDITS;
    }

    public static function isAutofact($product_id)
    {
        return $product_id == self::AUTOFACT;
    }

    public static function isPack2if($product_id)
    {
        return in_array($product_id, self::getAllPack2if());
    }
    public static function isInsertingFee($product_id)
    {
        return $product_id == self::INSERTING_FEE;
    }

    public static function getLabelTypeName($number)
    {
        global $BCONF;
        return Bconf::get($BCONF, "*.product_config.label.options.{$number}");
    }
}
