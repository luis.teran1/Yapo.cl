<?php

namespace Yapo;

use bTransaction;
use bResponse;
use AccountSession;
use CreditsService;
use RedisMultiproductManager;
use RedisSessionClient;
use ServipagXML4;

class PaymentVerify
{

    private $accountData = array();
    private $accountSession;
    private $bResponse;
    private $cartMngr;
    private $comboUpselling;
    private $creditsService;
    private $paymentGroupId;
    private $paymentMethod;
    private $productsPending;
    private $purchasedProducts = array();
    private $redisPayment;
    private $specialOrderId = '';
    private $store;
    private $svpgResponse;
    private $trans;

    public $payApp = "payment";

    const CREDITS = 'credits';
    const REDIS_PAYMENT = 'redis_payment';
    const REDIS_SESSION = 'redis_session';

    public function __construct(
        $payApp,
        $redisPayment = null,
        $transInstance = null,
        $accountSession = null,
        $bResponse = null,
        $creditsService = null,
        $cartMngr = null,
        $redisSession = null
    ) {
        $this->accountSession = isset($accountSession) ? $accountSession : new AccountSession();
        $this->bResponse = isset($bResponse) ? $bResponse : new bResponse();
        $this->cartMngr = isset($cartMngr) ? $cartMngr : new RedisMultiproductManager();
        $this->creditsService = isset($creditsService) ? $creditsService : new CreditsService();
        $this->redisPayment = isset($redisPayment) ? $redisPayment : new EasyRedis(self::REDIS_PAYMENT);
        $this->redisSession = isset($redisSession) ? $redisSession : new EasyRedis(self::REDIS_SESSION);
        $this->trans = isset($transInstance) ? $transInstance : new bTransaction();
        $this->store = 0;
        $this->comboUpselling = 0;
        if (!empty($payApp)) {
            $this->payApp = $payApp;
        }
        // get the payment data from different sources
        session_start();
        if (!empty($_REQUEST['token_ws'])) {
            Logger::logInfo(__METHOD__, "Response from webpay plus");
            $ws_orden = $this->redisPayment->get($this->redisPayment->prepend.$_REQUEST['token_ws']);
            if (!empty($ws_orden)) {
                $this->paymentMethod = "webpay_plus";
                $this->paymentGroupId = $ws_orden;
            }
        } elseif (!empty($_POST['TBK_TOKEN'])) {
            $order = $_POST['TBK_ORDEN_COMPRA'];
            header('Location: /pagos?kid='.$order);
            exit(1);
        } elseif (!empty($_POST['TBK_ORDEN_COMPRA'])) {
            $this->paymentGroupId = $_POST['TBK_ORDEN_COMPRA'];
            $this->paymentMethod = "webpay";
        } elseif (!empty($_REQUEST['xml'])) {
            Logger::logInfo(__METHOD__, "Response from servipag");
            $this->svpgResponse = new ServipagXML4(urldecode($_REQUEST['xml']));
            $this->paymentMethod = "servipag";
            if ($this->svpgResponse->verify_signature()) {
                $this->paymentGroupId = $this->svpgResponse->IdTxCliente;
            } else {
                Logger::logError(__METHOD__, "Invalid servipag signature xml4: ".print_r($this->svpgResponse, true));
            }
        } elseif (!empty($_REQUEST['kid'])) {
            Logger::logInfo(__METHOD__, "Response from khipu");
            $this->paymentMethod = "khipu";
            $this->paymentGroupId = $_REQUEST['kid'];
        } elseif (!empty($_SESSION['global']['payment_group_id'])) {
            Logger::logDebug(__METHOD__, "oneclick pvf: payment_group_id:".$_SESSION['global']['payment_group_id']);
            Logger::logDebug(__METHOD__, "oneclick pvf: special_order_id:".$_SESSION['global']['special_order_id']);
            $this->paymentGroupId = $_SESSION['global']['payment_group_id'];
            $this->specialOrderId = $_SESSION['global']['special_order_id'];
            $this->paymentMethod = $_SESSION['global']['special_method'];
        } else {
            Logger::logError(__METHOD__, "No payment id received POST ".var_export($_REQUEST));
        }
    }

    public function start()
    {
        if (empty($this->paymentGroupId) || empty($this->paymentMethod)) {
            $this->redirectToErrorPage();
        }

        global $BCONF;
        $this->productsPending = array();

        $response = $this->bResponse;
        $response->add_data('appl', "pvf");

        if ($this->payApp == "payment") {
            $response->add_data('headscripts', Bconf::get($BCONF, "*.common.pay_multiproduct_confirm.headscripts"));
            $response->add_data('headstyles', Bconf::get($BCONF, "*.common.pay_multiproduct_confirm.headstyles"));
        }

        $this->validateResponseData($response, $this->paymentGroupId);
        $returnData = $this->addExtraData($response, $this->paymentGroupId);

        if ($this->paymentMethod == "servipag" &&
            ($this->svpgResponse->EstadoPago != "0" || !in_array($returnData['status'], array('pending', 'paid')))
        ) {
            Logger::logError(
                __METHOD__,
                "Servipag xml4, payment status failed: ".print_r($this->svpgResponse, true)
            );
            Logger::logError(__METHOD__, "Yapo payment, status No PAID payment_group_id: " .$this->paymentGroupId);
            $this->redirectToErrorPage();
        }

        // copy the vars into the response object
        $this->setBConfFieldsInResponse($response, $returnData, '*.payment.completion_page.billing');
        $this->setBConfFieldsInResponse($response, $returnData, '*.payment.completion_page.vars');
        $this->completeResponseData($response, $returnData);


        $response->add_data("REMOTE_BROWSER", $_SERVER['HTTP_USER_AGENT']);
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");

        Logger::logDebug(__METHOD__, 'call_template data: ' . var_export($response, true));
        $response->add_data("show_html5", "1");
        if ($this->payApp == "pay_apps") {
            $response->add_data("no_footer", 1);
            $response->add_data("no_header", 1);
            $response->add_data("hook_layout", 0);
            $response->add_data('headstyles', Bconf::get($BCONF, "*.pay_apps.headstyles"));
            $response->add_data('headscripts', Bconf::get($BCONF, "*.pay_apps.headscripts"));
            $response->call_template('common/responsive_base.html');
        } else {
            $response->call_template("common/mobile_select.html");
        }
    }

    public function completeResponseData(&$response, &$returnData)
    {
        /**
         * For transbank we mantain the behavior that the payment is authorized only if
         * redis "payment_status" says so.
         * For servipag, we are not using redis shit, so we rely on the payment_platform's
         * 'status'.
         */
        $authorized = false;
        if (isset($response->payment_status)) {
            $authorized = ($returnData['payment_status'] == "authorized");
        } elseif ($returnData['status'] == "paid") {
                $authorized = true;
        } elseif ($returnData['status'] == "pending" && $this->paymentMethod == "servipag") {
                $authorized = true;
                Logger::logError(
                    __METHOD__,
                    "Fail in servipag process, xml2 not received: ".print_r($returnData, true)
                );
                $response->add_data('servipag_failed', 1);
                $transaction = $this->trans->reset();
            if ($this->accountSession->is_logged() || !empty($this->accountData)) {
                $account_email = $this->getAccountParam('email');
                $account_name = $this->getAccountParam('name');
                $transaction->add_data('email', $account_email);
                $transaction->add_data('name', $account_name);
            } else {
                Logger::logError(__METHOD__, "Servipag payment from unlogged user. Are we supporting this now?");
                $transaction->add_data('email', 'no_email_especified@yapo.cl');
                $transaction->add_data('name', 'no_name_user');
            }
                $transaction->add_data('support_subject', '40');
                $transaction->add_data(
                    'support_body',
                    "\nSe ha generado una compra con Servipag con atraso en la activación \nde servicios.".
                    "\nPayment_group_id: {$this->paymentGroupId}"
                );
                $transaction->send_command('support', true, true);
        } elseif ($returnData['status'] == "pending" && $this->paymentMethod == "webpay_plus") {
            $order = $_POST['TBK_ORDEN_COMPRA'];
            header('Location: /pagos?kid='.$order);
            exit(1);
        }

        $response->add_data('fail_on_yapo', $returnData['fail_on_yapo']);
        if ($authorized && $returnData['fail_on_yapo'] == '0') {
            $response->add_data('page_name', lang('PAGE_SUCCESS_BUMP_NAME'));
            $response->add_data('page_title', lang('PAGE_SUCCESS_BUMP_TITLE'));
            $response->add_data("content", $this->payApp."/pay_ok_prod_ok.html");
        } else {
            $response->add_data('page_title', lang('PAGE_SUCCESS_BUMP_FAILED_TITLE'));
            $response->add_data("content", $this->payApp."/pay_ok_prod_fail.html");
        }

        if ($authorized) {
            if ($this->paymentMethod == self::CREDITS) {
                $h = sha1($returnData['receipt']);
                $response->add_data('link_voucher', "get_voucher?oid={$this->paymentGroupId}&h={$h}");
            }
            $credits_response = $this->creditsService->getBalance($returnData['account_id']);
            $response->add_data("credits_end", $credits_response->Balance);

            if ($this->accountSession->is_logged() || !empty($this->accountData)) {
                //Update session info of new store
                if ($this->store == 1) {
                    $trans_load_store = $this->trans->reset();
                    $account_id = $this->getAccountParam('account_id');
                    $trans_load_store->add_data('account_id', $account_id);
                    $reply_load_store = $trans_load_store->send_command('load_store');

                    if (!$trans_load_store->has_error(true)) {
                        $this->accountSession->set_param('store_status', $reply_load_store['store_status']);
                        $this->accountSession->set_param('url', $reply_load_store['url']);
                        $this->accountSession->set_param('store_id', $reply_load_store['store_id']);
                    } else {
                        Logger::logError(__METHOD__, "Can't load store for user ".$account_id);
                    }
                }

                if ($this->comboUpselling != 1) {
                    // Here we are cleaning the shopping cart after a successful payment process
                    $account_email = $this->getAccountParam('email');
                    $account_id = $this->getAccountParam('account_id');

                    //clear only the products the user bought in the given purchase
                    $this->cartMngr->unselect_multiple($account_email, $this->purchasedProducts);
                    $this->cartMngr->addAllPendingProducts($account_email, $this->productsPending);
                }
            }
        }
    }

    public function addExtraData(&$response, $paymentGroupId)
    {
        $order_id = ($this->specialOrderId != '') ? $this->specialOrderId : $paymentGroupId;
        $response->add_data("order_id", $order_id);
        $transaction = $this->trans->reset();
        $transaction->add_data('payment_group_id', $paymentGroupId);
        $reply = $transaction->send_command('purchase_get');
        if ($transaction->has_error(true)) {
            $this->redirectToErrorPage();
        }
        if (!isset($reply['purchase_get'])) {
            $this->redirectToErrorPage();
        } else {
            foreach ($reply['purchase_get'] as $row) {
                if (isset($row['ref_type'])) {
                    if ($row['ref_type'] == 'cc_number') {
                        $row['reference'] = "**** **** **** ".$row['reference'];
                    }
                    $response->add_data($row['ref_type'], $row['reference']);
                }
            }
        }
        //Put here info needed
        $resultData = array(
            "status" => $reply['purchase_get'][0]['status'],
            "receipt" => $reply['purchase_get'][0]['receipt'],
            "account_id" => $reply['purchase_get'][0]['account_id'],
            "fail_on_yapo" => $reply['purchase_get'][0]['fail_on_yapo']
        );

        $response->add_data("payment_method", $this->paymentMethod);
        return $resultData;
    }

    public function validateResponseData(&$response, $paymentGroupId)
    {
        global $BCONF;

        $transaction = $this->trans->reset();
        $transaction->add_data('payment_group_id', $paymentGroupId);
        $reply = $transaction->send_command('get_items_by_payment');

        if ($transaction->has_error(true)) {
            $this->redirectToErrorPage();
        }

        $purchase_email = "";
        $user_account_id = "";
        $user_galleries = array();
        foreach ($reply['get_items_by_payment'] as $detail_payment) {
            $product = "";
            $list_id = "";
            $ad_id = "";
            $region = "";
            $category = "";
            foreach ($detail_payment as $key_column => $value_column) {
                if (in_array($key_column, array("subject", "price", "product_id"))) {
                    $response->fill_array("list_detail_".$key_column, $value_column);
                }
                if (empty($purchase_email) && $key_column == "email") {
                    $purchase_email = $value_column;
                }
                if (empty($user_account_id) && $key_column == "account_id") {
                    $user_account_id = $value_column;
                }
                if ($key_column == "list_id") {
                    $list_id = $value_column;
                }
                if ($key_column == "ad_id") {
                    $ad_id = $value_column;
                }
                if ($key_column == "region") {
                    $region = $value_column;
                }
                if ($key_column == "category") {
                    $category = $value_column;
                }
                if ($key_column == "product_id") {
                    $product = $value_column;
                    $product_group = Bconf::get($BCONF, "*.payment.product.".$value_column.".group");
                    if (!empty($product_group)) {
                        if ($product_group == "STORE") {
                            $this->store = 1;
                        } elseif ($product_group == "COMBO_UPSELLING") {
                            $this->comboUpselling = 1;
                        }
                    }
                }
                if ($key_column == "payment_method") {
                    $this->paymentMethod = $value_column;
                }
            }
            //add the products to the array of purchased products (some products refer to an ad and others don't)
            if ($product != "") {
                $get_by_ad_id = Bconf::get($BCONF, "*.payment.product.$product.get_ad_by") == "ad_id";
                $this->purchasedProducts[$product][] = $get_by_ad_id ? $ad_id : $list_id;
            }

            if ($product != "" && $list_id != "") {
                $this->productsPending[$product][] = $list_id;
            }
            if ($product != "" && $ad_id != "") {
                $referenced_product = Bconf::get($BCONF, "*.payment.product.$product.referenced_product");
                if (!empty($referenced_product)) {
                    $product = $referenced_product;
                }
                if (Bconf::get($BCONF, "*.payment.product.$product.group")  == "GALLERY") {
                    array_push($user_galleries, array('ad_id' => $ad_id, 'region' => $region, 'category' => $category));
                }
            }
        }
        if (!empty($user_account_id)) {
            //load the account info from the user account id
            $transaction = $this->trans->reset();
            $transaction->add_data('account_id', $user_account_id);
            $account_reply = $transaction->send_command('get_account');
            //Load the acount data used in the payment verify
            $this->accountData['account_id'] = $user_account_id;
            $this->accountData['name'] = $account_reply['name'];
            $this->accountData['email'] = $account_reply['email'];
        }

        if (count($user_galleries)) {
            $this->setCookieGallery($purchase_email, $user_galleries);
        }
        if ($transaction->has_error(true)) {
            $this->redirectToErrorPage();
        }
    }

    public function setCookieGallery($purchase_email, $user_galleries)
    {
        global $BCONF;

        $cookie_name = Bconf::get($BCONF, "*.common.gallery.cookie_name");
        $ttl = Bconf::get($BCONF, "*.common.gallery.redis_ttl");
        $cookie_hash = "";

        if (!isset($_COOKIE[$cookie_name]) or empty($_COOKIE[$cookie_name])) {
            $cookie_hash = sha1($purchase_email);
            setcookie($cookie_name, $cookie_hash, time() + $ttl, '/', Bconf::get($BCONF, 'adwatch.cookie_domain'));
        } else {
            $cookie_hash = $_COOKIE[$cookie_name];
        }

        $prefix = Bconf::get($BCONF, "*.common.gallery.redis_prefix");
        $glry_qty = Bconf::get($BCONF, "*.common.gallery.show_user.qty");

        foreach ($user_galleries as $gallery) {
            $redis_hash = 'rsd1x'.$prefix.$cookie_hash."_".$gallery["region"]."_".$gallery["category"];
            Logger::logInfo(__METHOD__, "REDIS set hash: $redis_hash");
            $this->redisSession->hset($redis_hash, $gallery["ad_id"], $glry_qty);
            $this->redisSession->expire($redis_hash, $ttl);
        }
    }

    public function setBConfFieldsInResponse(&$response, &$originData, $bconfKey)
    {
        global $BCONF;
        $fields = Bconf::get($BCONF, $bconfKey);
        foreach ($fields as $field) {
            if (isset($field['var'])) {
                if (isset($originData->$field['var'])) {
                    $response->add_data(
                        $field['var'],
                        $this->getBConfFieldMappedValue($field, $originData->$field['var'])
                    );
                } elseif (isset($field['default'])) {
                    if ($field['default'] == "NOW") {
                        $response->add_data($field['var'], date("d/m/Y H:i:s"));
                    } else {
                        $response->add_data($field['var'], $field['default']);
                    }
                }
            }
        }
    }

    public function getBConfFieldMappedValue($bconf, $value)
    {
        if (isset($bconf["map"])) {
            $value = $bconf["map"][$value];
        }
        return $value;
    }

    public function redirectToErrorPage()
    {
        header('Location: /pagos/expired/0');
        exit();
    }

    public function getAccountParam($param)
    {
        $result = $this->accountSession->get_param($param);
        return empty($result) ? $this->accountData[$param] : $result;
    }
}
