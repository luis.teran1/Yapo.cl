<?php

namespace Yapo;

use CreditsService;

class ApplyProducts
{

    private $product_ids = array();
    private $ad_prices = array();
    private $ad_subjects = array();

    private $successful_items = array();

    private $packs_info_mail_product_ids = array();
    private $packs_info_mail_expires = array();

    private $purchase_with_stores = false;
    private $purchase_with_packs = false;
    private $purchase_must_mail = false;

    private $has_error = false;
    private $is_valid_items = true;

    private $store_product = 0;

    private $doc_type = "bill";

    private $creds_balance = 0;
    private $creds_added = 0;

    private $purchase_id = 0;

    private $siteProducts;

    public function __construct(SiteProducts $siteProducts)
    {
        $this->siteProducts = $siteProducts;
    }

    public function mustMail()
    {
        return $this->purchase_must_mail;
    }

    public function hasError()
    {
        return $this->has_error;
    }

    public function hasValidItems()
    {
        return $this->is_valid_items;
    }

    public function hasStores()
    {
        return $this->purchase_with_stores;
    }

    public function hasPacks()
    {
        return $this->purchase_with_packs;
    }

    public function getPackInfoMail()
    {
        return array($this->packs_info_mail_product_ids, $this->packs_info_mail_expires);
    }

    public function getProductInfo()
    {
        return array($this->product_ids, $this->ad_prices, $this->ad_subjects);
    }

    public function getCreditsInfo()
    {
        return array($this->creds_balance, $this->creds_added);
    }
    public function getStoreProduct()
    {
        return $this->store_product;
    }

    public function getDocType()
    {
        return $this->doc_type;
    }

    /**
     *  Main function to apply products
     */
    public function run($externalOrderId, $purchase)
    {
        $result = array();
        if (!isset($purchase['purchase_get']) || $purchase['purchase_get'][0]['status'] != 'pending') {
            $result['status'] = "UNAUTHORIZED";
            Logger::logDebug(__METHOD__, "UNAUTHORIZED");
            return $result;
        }
        $result['status'] = "ERROR";
        $payment_items = Payment\Trans::getPaymentItems($externalOrderId);
        $this->is_valid_items = $this->paymentHasValidItems($payment_items);
        Logger::logDebug(__METHOD__, "Payment items : " .print_r($payment_items, true));

        //Call to this methods
        $function_names = array("Ordered", "Credits", "Autofact");

        if ($this->is_valid_items) {
            foreach ($function_names as $key) {
                $fn_exec = "apply{$key}";
                $this->$fn_exec($payment_items);
            }
            $result['status'] = ConfirmPayment::AUTHORIZED;
        }
        $result['successful_items'] = $this->successful_items;

        return $result;
    }

    private function applyOrdered($payment_items)
    {
        $sorted = Products::sortProducts($payment_items);
        foreach ($sorted as $item) {
            $extraData = Products::getExtraData($item);
            list($success, $reply) = $this->siteProducts->genericApply($item, $extraData);
            if (!$success) {
                $this->has_error = true;
                $this->markDetailAsError($item, $reply);
            }
            Logger::logError(__METHOD__, "Data item: ".var_export($item, true));
            $this->saveSpecialInfo($item);
        }
    }

    private function saveSpecialInfo($item)
    {
        $this->saveSuccess($item);
        $product_id = $item["product_id"];
        if (Products::isStore($product_id)) {
            $this->purchase_with_stores = true;
            $this->store_product = $product_id;
        } elseif (Products::isPack($product_id)) {
            $this->purchase_with_packs = true;
            $this->packs_info_mail_product_ids[] = $product_id;
            $pack_period = substr($product_id, 1, 1);
            $pack_period_value = bconf_get($BCONF, "*.packs.expire_time.$pack_period.value");
            $this->packs_info_mail_expires[] = date('d/m/Y', strtotime("+".$pack_period_value));
        } else {
            $this->savePayData($item);
        }
    }

    private function markDetailAsError($item, $reply)
    {
        $jsonReply = json_encode($reply);
        $paymentGroup = $item['payment_group_id'];
        Logger::logError(__METHOD__, "Mark detail id:{$paymentGroup} with error:{$jsonReply}");
        $this->siteProducts->markAsError($paymentGroup, $jsonReply);
    }
    private function applyCredits($payment_items)
    {
        global $BCONF;
        foreach ($payment_items as $k => $v) {
            $product_id = $v['product_id'];
            $detail_payment_group = $v['payment_group_id'];
            if (bconf_get($BCONF, "*.payment.product.$product_id.group") == Products::GROUP_CREDITS) {
                if (bconf_get($BCONF, "*.payment.product.$product_id.skip_apply_process") == "1") {
                    Logger::logError(__METHOD__, "Apply product {$product_id} skipped");
                    continue;
                }
                $account_id = $v['account_id'];
                $price = $v['price'];
                $external_id = $v['payment_group_id'];
                $expire_time = bconf_get($BCONF, "*.payment.product.$product_id.expire_time");
                $ttl =  strtotime("+{$expire_time}") - strtotime("now"); //Time to live in seconds

                $body = array(
                    'UserId' => (integer)$account_id,
                    'Credits' => (integer)$price,
                    'ExternalId' => (integer)$external_id,
                    'Ttl' => (integer)$ttl
                );
                $credits = new CreditsService();
                $response = $credits->addCredits($body);
                if (!$response) {
                    $this->has_error = true;
                } else {
                    $this->purchase_must_mail = true;
                    $this->savePayData($v);
                    $this->creds_balance = $response->Balance;
                    $this->creds_added = $v["price"];
                }
            }
        }
    }

    private function applyAutofact($payment_items)
    {
        global $BCONF;
        foreach ($payment_items as $k => $v) {
            $product_id = intval($v['product_id']);
            $detail_payment_group = $v['payment_group_id'];
            if ($product_id == Products::AUTOFACT) {
                $account_id = $v['account_id'];
                $price = $v['price'];
                $external_id = $v['payment_group_id'];

                $params = explode(":", $v["detail_params"]);
                $plates = $params[count($params)-1];
                $account = Accounts\Trans::getAccount($account_id);
                $body = array(
                    "User_id" => intval($account_id),
                    "User_email" => $account["email"],
                    "User_name" =>  $account["name"],
                    "List_id" =>  intval($v["list_id"]),
                    "Ad_category" => intval($v["category"]),
                    "Ad_subject" => $v["subject"],
                    "Pay_type" => $v["payment_method"],
                    "Plates" => $plates
                );
                $yapofact = new YapofactService();
                $response = $yapofact->requestReport($body);
                if (!$response) {
                    $this->has_error = true;
                    Logger::logError(__METHOD__, "Error on Autofact:".var_export($body, true));
                } else {
                    $this->purchase_must_mail = true;
                    $this->savePayData($v);
                    $health_check = $yapofact->healthCheck();

                    PaymentMails::sendAutofactConfirm(
                        $v["email"],
                        $account["email"],
                        $v["subject"],
                        date('Y-m-d H:i:s'),
                        $v["product_id"],
                        $v["price"],
                        $health_check
                    );
                }
            }
        }
    }

    private function paymentHasValidItems($payment_items)
    {
        if ($payment_items === false) {
            return false;
        }
        $counter_products = 0;
        foreach ($payment_items as $item) {
            if ($this->purchase_id == 0) {
                $this->purchase_id = $item['purchase_id'];
                $this->doc_type = $item["doc_type"];
            } else {
                if ($this->purchase_id != $item['purchase_id']) {
                    Logger::logError(__METHOD__, "The same purchase has diferents IDs ".$this->purchase_id);
                    return false;
                }
            }
            // Ad deactivated with pack has not valid items
            if (isset($item['ad_status'])
                && isset($item['has_pack'])
                && $item['ad_status'] == 'deactivated'
                && (bool)$item['has_pack']
            ) {
                Logger::logError(
                    __METHOD__,
                    'error applying product to ad deactivated with pack: '.var_export($item, true)
                );
                return false;
            }
            $counter_products++;
        }

        if ($counter_products == 0) {
            Logger::logError(__METHOD__, "This purchase ($this->purchase_id) does not have products");
        }
        return $counter_products > 0;
    }

    private function savePayData(array $item)
    {
        $this->product_ids[] = $item["product_id"];
        $this->ad_subjects[] = isset($item["subject"])? $item["subject"]: "";
        $this->ad_prices[] = isset($item["price"])? $item["price"]: "";
        $this->purchase_must_mail = true;
    }

    private function saveSuccess(array $item)
    {
        $this->successful_items[] = $item["purchase_detail_id"];
    }
}
