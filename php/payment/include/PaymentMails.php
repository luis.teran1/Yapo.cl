<?php
namespace Yapo;

use bTransaction;

class PaymentMails
{


    public static function sendPackPurchaseMail($email, $name, $packs_product_ids, $packs_expires)
    {

        Logger::logInfo(__METHOD__, "Sending packs success email");
        $trans_pack_mail = new bTransaction();
        $trans_pack_mail->add_data('to', $email);
        $trans_pack_mail->add_data('name', $name);
        $trans_pack_mail->add_data('product_ids', implode("|", $packs_product_ids));
        $trans_pack_mail->add_data('expires', implode("|", $packs_expires));
        $trans_pack_mail->send_command("pack_purchased_mail");
        return !$trans_pack_mail->has_error();
    }

    public static function sendStorePurchaseMail($account_id, $email, $name, $store_product)
    {
        Logger::logInfo(
            __METHOD__,
            "Sending store success email for product id: ".var_export($store_product, true)
        );
        $trans_store_mail = new bTransaction();
        $trans_store_mail->add_data('product_id', $store_product);
        $trans_store_mail->add_data('to', $email);
        $trans_store_mail->add_data('name', $name);

        $trans_load_store = new bTransaction();
        $trans_load_store->add_data('account_id', $account_id);
        $reply_load_store = $trans_load_store->send_command('load_store');

        if (!$trans_load_store->has_error(true)) {
            $trans_store_mail->add_data('store_id', $reply_load_store['store_id']);
            $trans_store_mail->add_data('date_start', $reply_load_store['date_start']);
        } else {
            Logger::logError(__METHOD__, "Can't load store for user ".$email);
        }

        $trans_store_mail->send_command("store_purchased_mail");
        return !$trans_store_mail->has_error();
    }

    public static function sendMultiproductMail(
        $email,
        $product_ids,
        $ad_prices,
        $doc_type,
        $creds_balance,
        $creds_added
    )
    {
        $trans_mail = new bTransaction();
        $trans_mail->add_data('to', $email);
        $trans_mail->add_data('doc_type', $doc_type);
        $trans_mail->add_data('product_ids', implode('|', $product_ids));
        $trans_mail->add_data('ad_prices', implode('|', $ad_prices));
        if (isset($creds_balance)) {
            $trans_mail->add_data('creds_balance', $creds_balance);
            $trans_mail->add_data('creds_added', $creds_added);
        }
        $trans_mail->send_command('multi_product_mail');
        return !$trans_mail->has_error();
    }

    public static function sendCreditVoucher(
        $payment_group_id,
        $email,
        $credits_used,
        $credits_balance,
        $product_ids,
        $ad_subjects,
        $purchase_receipt
    )
    {
        $hash = sha1($purchase_receipt);
        $trans = new bTransaction();
        $trans->add_data('to', $email);
        $trans->add_data('product_ids', implode('|', $product_ids));
        $trans->add_data('ad_names', implode('|', $ad_subjects));
        $trans->add_data('credits_used', $credits_used);
        $trans->add_data('credits_balance', $credits_balance);
        $trans->add_data('oid', $payment_group_id);
        $trans->add_data('hash', $hash);

        $trans->send_command("send_credits_voucher");
        if ($trans->has_error()) {
            Logger::logError(__METHOD__, "Trans error sending credit voucher for payment_group $payment_group_id");
            return false;
        }

        return true;
    }

    public static function sendAutofactConfirm(
        $user_email,
        $send_to,
        $ad_subject,
        $payment_date,
        $product_id,
        $price,
        $health_check
    ){
        Logger::logDebug(
            __METHOD__,
            "Sending email autofact payment"
            ." user_email: {$user_email}"
            ." send_to: {$send_to}"
            ." subject: {$ad_subject}"
            ." payment_date: {$payment_date}"
        );
        $trans = new bTransaction();
        $trans->add_data('user_email', $user_email);
        $trans->add_data('send_to', $send_to);
        $trans->add_data('ad_subject', $ad_subject);
        $trans->add_data('payment_date', $payment_date);
        $trans->add_data('product', $product_id);
        $trans->add_data('price', $price);
        if (!is_null($health_check) && $health_check->AutofactAPI != "OK") {
            $trans->add_data('slow_process', '1');
        }
        $trans->send_command("send_autofact_confirm");
        return $trans->has_error();
    }
}
