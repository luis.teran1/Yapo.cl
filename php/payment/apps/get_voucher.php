<?php
require_once 'autoload_lib.php';
require_once 'fpdf.php';
require_once 'init.php';
require_once 'util.php';

if (empty($_GET['oid']) || empty($_GET['h'])) {
    echo "Missing parameters";
    exit(0);
}

$order_id = sanitizeVar($_GET['oid'], 'int');
$hash = sanitizeVar($_GET['h'], 'string');

$voucher = new Yapo\GetVoucher();
$voucher->populateVoucher($order_id, $hash);
$voucher->run();
