<?php

require_once('autoload_lib.php');
require_once('util.php');
require_once('payment_common.php');

openlog("TELESALES_PAYMENT", LOG_ODELAY, LOG_LOCAL0);

use Yapo\Cpanel;
use Yapo\Logger;
use Yapo\Bconf;
use Yapo\WebpayService;
use Yapo\WebpayApply;

require_once('memcachedsession.php');

class blocket_telesales_payment extends blocket_application
{
    var $list_id;
    var $prod;
    var $ad;
    var $company_buyer;
    var $email_buyer;
    var $user_id_buyer;
    var $buyer_name;

    var $document;
    var $name;
    var $lob;   // Line Of Business (giro comercial)
    var $rut;
    var $address;
    var $region;
    var $communes;
    var $contact;
    var $email;

    // Webpay plus
    var $cardNumber;
    var $cvv;
    var $expMonth;
    var $expYear;

    var $payment_group_id;
    var $pay_code;
    var $page_title = "";
    var $error_message = "";
    var $error_title = "";
    var $scripts;
    var $styles_bump;
    var $has_loaded = false;
    var $session_expired = false;
    var $tbk = array();

    var $form_type = 1;
    var $sl;
    var $mb;
    var $manual_xiti_tag;
    var $from;
    var $want_bump;

    var $styles_pay_multiproducts;
    var $scripts_pay_multiproducts;
    var $mp_ad_list_id;
    var $mp_ad_subject;
    var $mp_prod_id;
    var $mp_prod_name;
    var $mp_prod_price;
    var $product_list;
    const SINGLEPRODUCT_TYPE = 1;
    const SINGLEPRODUCT_TYPE_NEW = 2;
    const MULTIPRODUCT_TYPE = 3;
    const SINGLEPRODUCT_TYPE_MOBILE = 4;
    const PAY_METHOD_ERR_PREF = 'PAYMENT_METHOD_ERROR: ';
    var $is_logged_on_load = false;
    var $first_referer;

    function blocket_telesales_payment()
    {
        global $BCONF;
        global $session;
        session_start();
        $this->first_referer = @$_SERVER['HTTP_REFERER'];
        syslog(LOG_INFO, log_string() . "Calling payment application");

        $this->headscripts = Bconf::get($BCONF, "*.payment.headscripts");
        $this->headstyles = Bconf::get($BCONF, "*.payment.headstyles");
        $this->styles_bump = Bconf::get($BCONF, "*.common.pay_bump.headstyles");

        $this->scripts_pay_multiproducts = Bconf::get($BCONF, "*.common.pay_multiproduct.headscripts");
        $this->styles_pay_multiproducts = Bconf::get($BCONF, "*.common.pay_multiproduct.headstyles");

        if (isset($_POST['TBK_ID_SESION']) && isset($_POST['TBK_ORDEN_COMPRA'])) {
            $this->tbk['TBK_ID_SESION']  = $_POST['TBK_ID_SESION'];
            $this->tbk['TBK_ORDEN_COMPRA'] = $_POST['TBK_ORDEN_COMPRA'];
        }

        $this->init('load', 0);
    }

    function get_state($statename)
    {
        switch ($statename) {
            case 'load':
                return array('function' => 'payment_load',
                    'params' => array(),
                    'method' => 'both',
                );
            case 'validate':
                return array('function' => 'payment_validate',
                    'params' => array(
                        'document',
                        'name',
                        'lob',
                        'rut',
                        'address',
                        'region',
                        'communes',
                        'contact',
                        'email',
                        'user_id' => 'f_integer',
                        'search_email' => 'f_email',
                        'account_id' => 'f_integer',
                        'store_id' => 'f_integer',
                        'cardNumber' => 'f_string',
                        'shareNumber' => 'f_string',
                        'expMonth' => 'f_string',
                        'expYear' => 'f_string',
                        'cvv' => 'f_string'
                    ),
                    'method' => 'post');
            case 'webpay':
                return array('function' => 'payment_webpay',
                    'method' => 'get');
            case 'webpay_plus':
                return array('function' => 'payment_webpay_plus',
                    'method' => 'get');
            case 'disabled':
                return array('function' => 'payment_disabled',
                    'method' => 'get');
            case 'error':
                return array('function' => 'payment_error',
                    'params' => array('TBK_ID_SESION', 'TBK_ORDEN_COMPRA'),
                    'method' => 'both');
        }
    }

    function payment_load()
    {
        global $BCONF;
        //syslog(LOG_DEBUG, log_string() . "payment_load document=$document, name=$name");

        if (!payment_enabled()) {
            syslog(LOG_INFO, log_string() . "Calling payment aplication_disabled");
            return 'disabled';
        }

        if (!empty($this->tbk['TBK_ID_SESION']) && !empty($this->tbk['TBK_ORDEN_COMPRA'])) {
            syslog(LOG_INFO, log_string() . "Entering in payment aplication from fail of tbk");

            $payment_group_id = $this->tbk['TBK_ORDEN_COMPRA'];
            $data = $this->get_error_status($payment_group_id);
            if (is_null($data)) {
                return 'error';
            }
            $this->id = $data->list_id;
            $this->prod = $data->product_id;
            $id = $data->list_id;
            $prod = $data->product_id;
            return 'error';
        }

        if ($this->session_expired === true) {
            syslog(LOG_DEBUG, log_string() . "Payment: Form expired");
            $this->page_title = "ERROR_SESSION_EXPIRED_PAGE_TITLE";
            $this->error_title = "ERROR_SESSION_EXPIRED_OOPS";
            $this->error_message = "ERROR_SESSION_EXPIRED";
            return 'error';
        }

        $this->has_loaded = true;
        //return 'validate';
        //return $this->payment_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email);
    }

    function cp_user_valid()
    {
        return (Cpanel::checkLogin() && (Cpanel::hasPriv('telesales') || Cpanel::hasPriv('telesales.sell_products')));
    }

    function payment_validate(
        $document,
        $name,
        $lob,
        $rut,
        $address,
        $region,
        $communes,
        $contact,
        $email,
        $user_id,
        $search_email,
        $account_id,
        $store_id,
        $cardNumber,
        $shareNumber,
        $expMonth,
        $expYear,
        $cvv
    ) {
        global $BCONF;
        global $session;

        if (!$this->cp_user_valid()) {
            bLogger::logError(__METHOD__, "Cpanel has no permission to telesell");
            return 'error';
        }

        $this->get_email = $search_email;

        syslog(LOG_DEBUG, log_string() . "Payment Validate");
        syslog(LOG_DEBUG, log_string() . "payment_load document=$document, name=$name");
        //This restore the headstuff if in some point were changed
        $this->headscripts = Bconf::get($BCONF, "*.payment.headscripts");
        $this->headstyles = Bconf::get($BCONF, "*.payment.headstyles");

        $this->email_buyer = $email;
        $this->company_buyer = 'PRIVATE';
        $this->user_id_buyer = $user_id;
        $this->buyer_name = $name;

        if (empty($document)) {
            syslog(LOG_DEBUG, log_string() . "Error document is empty");
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
            return 'error';
        }

        $document_data = Bconf::get($BCONF, "*.payment.document.$document.data");
        $doc_type = Bconf::get($BCONF, "*.payment.document.$document.doc_type");
        $product_price = Bconf::get($BCONF, "*.payment.product.$this->prod.price");
        $product_name = Bconf::get($BCONF, "*.payment.product.$this->prod.codename");

        if (empty($doc_type)) {
            syslog(LOG_DEBUG, log_string() . "Error doc_type is empty");
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
            return 'error';
        }


        $this->document = $document;
        $this->name = $name;
        $this->lob = $lob;
        $this->rut = $rut;
        $this->address = $address;
        $this->region = $region;
        $this->communes = $communes;
        $this->contact = $contact;

        $this->email = $email;

        $this->cardNumber = $cardNumber;
        $this->cvv = $cvv;
        $this->expMonth = $expMonth;
        $this->expYear = $expYear;
        $this->shareNumber = $shareNumber;

        if (Bconf::get($BCONF, "*.payment.telesales.webpay_plus.enabled")
            && (empty($cardNumber)
            || empty($cvv)
            || empty($expMonth)
            || empty($expYear))
        ) {
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_CARD_PARAMS_MISSING";
            return 'error';
        }


        $transaction = new bTransaction();
        foreach ($document_data as $key => $val) {
            if ($val['name'] == 'email' && empty($$val['name'])) {
                $transaction->add_data('email', $this->ad['email']);
            } else {
                $transaction->add_data($val['name'], $$val['name']);
            }
        }
        $transaction->add_data('doc_type', $doc_type);
        $transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);

        if (!empty($_POST['mpad']) && !empty($_POST['mpid'])) {
            $products_price = array();
            $products_params = array();
            $products_name  = array();
            // used to get the category of every ad
            $listIds = array_filter($_POST['mpad']);
            if (count($listIds) > 0) {
                $list_ids_to_string = implode(",", $listIds);
                $result = bsearch_search_vtree(Bconf::get($BCONF, "*.common.asearch"), "id:".$list_ids_to_string);
                if (!is_array($result['list_id'])) {
                    $result['list_id'] = array($result['list_id']);
                }
                if (!is_array($result['category'])) {
                    $result['category'] = array($result['category']);
                }
            }
            $is_store = false;

            $this->product_list = array();
            foreach ($_POST['mpid'] as $key => $id_prod) {
                $list_id = $_POST['mpad'][$key];
                $name = Bconf::get($BCONF, "*.payment.product.$id_prod.codename");
                $bconf_is_store = Bconf::get($BCONF, "*.payment.product.$id_prod.is_store");
                if (((int)$bconf_is_store) == 1) {
                    $is_store = true;
                    $price = Bconf::get($BCONF, "*.payment.product.$id_prod.price");
                } elseif (substr($name, 0, 5) === "PCAR_" || substr($name, 0, 5) === "PRES_") {
                    $price = Bconf::get($BCONF, "*.payment.product.$id_prod.price");
                } else {
                    $position = array_search($list_id, $result['list_id']);
                    $category = $result['category'][$position];
                    $price = Bconf::getProductPrice($BCONF, $id_prod, $category);
                }
                if (empty($price)) {
                    bLogger::logError(__METHOD__, "mpad or mpid is empty");
                    return 'error';
                } else {
                    array_push($products_price, $price);
                    array_push($products_params, "0");
                    array_push($products_name, $name);
                    array_push($this->product_list, array($id_prod, $price));
                }
            }

            $mp_list_id = implode("|", $_POST['mpad']);
            $mp_prod_id = implode("|", $_POST['mpid']);
            $mp_prices  = implode("|", $products_price);
            $mp_params  = implode("|", $products_params);
            $mp_names  = implode("|", $products_name);
            $transaction->add_data('product_id', $mp_prod_id);

            $webpay_type = 'webpay';
            if (Bconf::get($BCONF, "*.payment.telesales.webpay_plus.enabled")) {
                $webpay_type = 'webpay_plus';
            }
            $transaction->add_data('payment_method', $webpay_type);
            $transaction->add_data('payment_platform', 'desktop');
            $transaction->add_data('list_id', $mp_list_id);
            $transaction->add_data('product_price', $mp_prices);
            $transaction->add_data('product_params', $mp_params);
            $transaction->add_data('product_name', $mp_names);
            if (!empty($account_id)) {
                $transaction->add_data('account_id', $account_id);
            }

            $transaction->add_data('seller_id', $_SESSION['controlpanel']['admin']['id']);
            if ($is_store) {
                if (empty($store_id)) {
                    $trans_create_store = new bTransaction();
                    $trans_create_store->add_data('account_id', $account_id);
                    $reply_create_store = $trans_create_store->send_admin_command('create_store');
                    $transaction->add_data('store_id', $reply_create_store['store_id']);
                } else {
                    $transaction->add_data('store_id', $store_id);
                }
            }
        } else {
            bLogger::logError(__METHOD__, "mpad or mpid is empty");
            return 'error';
        }

        $reply = $transaction->send_admin_command('purchase_insert');

        if (strstr($reply['status'], 'TRANS_DATABASE_ERROR')) {
            $this->log_error($_SERVER['REMOTE_ADDR'], $this->list_id, $this->prod, $doc_type, $product_price, "purchase_insert", "TRANS_DATABASE_ERROR");

            if (strstr($reply['status'], 'ERROR_AD_NOT_ACTIVE')) {
                $this->headstyles = Bconf::get($BCONF, "*.payment.headstyles");
                $this->page_title = "ERROR_AD_NOT_AVAILABLE";
                $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
                $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";

                syslog(LOG_DEBUG, log_string() . "Error ad not active");
                return 'error';
            } else {
                die_with_template(__FILE__, log_string() . "PAYMENT: TRANS_DATABASE_ERROR ");
            }
        }

        if ($transaction->has_error()) {
            $trans_errors = $transaction->get_errors();
            $errs = array_merge($trans_errors, array('in_payment' => "1"));
            $this->set_errors($errs);

            syslog(LOG_DEBUG, log_string() . "Error linea 263: $errs");
            $this->page_title = "ERROR_AD_NOT_AVAILABLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";
            return 'error';
        }

        $this->payment_group_id = $reply['o_payment_group_id'];
        $this->pay_code = $reply['o_pay_code'];

        return $webpay_type;
    }


    function get_product_list()
    {
        $output = array();
        foreach ($this->product_list as $product) {
            $prod = $product[0];
            $product_price = $product[1];
            $product_name = Bconf::get($BCONF, "*.payment.product.$prod.name");
            $product_description = Bconf::get($BCONF, "*.payment.product.$prod.description");
            $tmp = array('code' => $prod, 'product_id' => $prod, 'name' => $product_name, 'description' => $product_description, 'qtt' => 1, 'price' => $product_price);
            array_push($output, $tmp);
        }
        return $output;
    }

    function set_telesales_cookie($code)
    {
        $prefix = Bconf::get($BCONF, '*.payment.telesales.common.cookie.name_prefix');
        $email = str_replace(array("@","."), "_", $this->get_email);
        setcookie(
            $prefix.$email,
            $code,
            time() + Bconf::get($BCONF, '*.payment.telesales.common.cookie.ttl'),
            '/',
            Bconf::get($BCONF, '*.common.session.cookiedomain') . '; '
        );
    }

    function payment_webpay()
    {
        global $BCONF;

        if (isset($head_script)) {
            $this->headscripts = $head_script;
        }
        if (isset($head_style)) {
            $this->headstyles = $head_style;
        }

        $redirectData = array();
        $redirectData['url'] =  Bconf::get($BCONF, "*.payment.telesales.url_cgi");
        # add url check and error log
        $check_result=$this->check_payment_url($redirectData['url']);
        if ($check_result) {
            bLogger::logInfo(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."error checking webpay url in telesales: " . $check_result
            );
        }
        $redirectData['method'] = 'POST'; //Bconf::get($BCONF, "*.payment_api_client.params.method_code");

        //Add two zeros at the end of TBK_MONTO
        $redirectData['params']['TBK_MONTO'] = $this->get_total_price() * 100;
        $redirectData['params']['TBK_ORDEN_COMPRA'] = $this->payment_group_id;
        $redirectData['params']['TBK_ID_SESION'] = sha1($this->payment_group_id);
        // TBK_TIPO_TRANSACCION should be HARDCODED
        $redirectData['params']['TBK_TIPO_TRANSACCION'] = 'TR_NORMAL';
        // Urls comes from Bconf
        $redirectData['params']['TBK_URL_EXITO'] = Bconf::get($BCONF, "*.payment.telesales.url_success");
        $redirectData['params']['TBK_URL_FRACASO'] = Bconf::get($BCONF, "*.payment.telesales.url_failure");

        bLogger::logInfo(__METHOD__, "payment_webpay payment_group_id = " . $this->payment_group_id);
        bLogger::logInfo(__METHOD__, "startPayment->redirectData = " . var_export($redirectData, true));
        bLogger::logDebug(__METHOD__, "payment_method:Webpay");

        $this->set_telesales_cookie($this->payment_group_id);
        $simple_data = array();
        if (isset($redirect_data)) {
            $redirectData['params']['TBK_URL_EXITO'] = $redirect_data['exito'];
            $redirectData['params']['TBK_URL_FRACASO'] = $redirect_data['fracaso'];
            $simple_data['no_footer'] = 1;
            $simple_data['no_header'] = 1;
        }

        $payment_template = Bconf::get($BCONF, "*.payment.options.5.template");
        $this->display_layout($payment_template, $simple_data, $redirectData, null, 'html5_base');
        return 'FINISH';
    }


    function payment_webpay_plus()
    {
        global $BCONF;
        $this->set_telesales_cookie($this->payment_group_id);

        $WebpayService = new WebpayService();
        $response = $WebpayService->execute(
            "completePayment",
            array(
                "amount" => $this->get_total_price(),
                "sessionId" => "{$this->payment_group_id}",
                "buyOrder" => $this->payment_group_id,
                "cardExpirationDate" => "{$this->expYear}/{$this->expMonth}",
                "cvv" => $this->cvv,
                "cardNumber" => $this->cardNumber,
                "shareNumber" => intval($this->shareNumber),
                "gracePeriod" => false,
                "deferredPeriodIndex" => 0
            )
        );
        $redirectData = array();
        if (!$response) {
            bLogger::logInfo(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."webpay-plus service not available"
            );
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;
            return 'error';
        }

        if (!isset($response->body->result) || $response->body->result != "OK") {
            $this->err_in_payment = 1;
            $this->error_order_id = $this->payment_group_id;
            if (!isset($response->body->result)) {
                $this->error_message = $response->body->ERROR;
            } elseif (isset($response->body->error->init->detail)) {
                $this->error_message = $response->body->error->init->detail;
            } elseif (isset($response->body->error->authorize->detailsOutput->responseCode)) {
                $code = abs($response->body->error->authorize->detailsOutput->responseCode);
                $this->error_message = Bconf::get($BCONF, "*.webpay_error_response.{$code}");
            }
            bLogger::logInfo(
                __METHOD__,
                self::PAY_METHOD_ERR_PREF."Error completing payment with webpay-plus ".$this->error_message
            );
            return $this->payment_error();
        }

        // Authorize and apply products
        $webpayApply = new WebpayApply();
        $webpayApply->populate($response->body->responses->authorize);
        $fullSuccess = $webpayApply->run();

        if (!$fullSuccess) {
            $this->error_message = Bconf::get($BCONF, "*.webpay_error_response.912");
            if ($response->body->responses->authorize->responseCode == 0) {
                $this->error_message = Bconf::get($BCONF, "*.webpay_error_response.911");
            }
            return $this->payment_error();
        }

        //SUCCESS
        $redirectData = array();
        $redirectData['url'] =  Bconf::get($BCONF, "*.payment.telesales.url_success");
        $redirectData['method'] = 'POST';

        $redirectData['params']['TBK_ORDEN_COMPRA'] = $this->payment_group_id;
        $redirectData['params']['TBK_ID_SESION'] = sha1($this->payment_group_id);

        bLogger::logInfo(__METHOD__, "payment_webpay payment_group_id = " . $this->payment_group_id);
        bLogger::logInfo(__METHOD__, "startPayment->redirectData = " . var_export($redirectData, true));
        bLogger::logDebug(__METHOD__, "payment_method:Webpay_plus");

        $payment_template = Bconf::get($BCONF, "*.payment.options.5.template");
        $this->display_layout($payment_template, array(), $redirectData, null, 'html5_base');
        return 'FINISH';
    }

    function get_total_price()
    {
        $elements = $this->get_product_list();
        $total_price = 0;
        foreach ($elements as $item) {
            $total_price += $item['price'];
        }
        return $total_price;
    }

    function payment_disabled()
    {
        $this->headscripts = Bconf::get($BCONF, "*.payment.headscripts");
        $this->headstyles = Bconf::get($BCONF, "*.payment.headstyles");
        $this->display_layout('payment/payment_disabled.html', null, null, null, "html5_base");
        return 'FINISH';
    }

    /*GENERIC ERROR OF THIS FLOW */
    function payment_error($TBK_ID_SESION = null, $TBK_ORDEN_COMPRA = null)
    {
        global $BCONF;
        $data = array();

        if (!empty($TBK_ORDEN_COMPRA)) {
            bLogger::logDebug(__METHOD__, "Got error from Transbank TBK_ORDEN_COMPRA: $TBK_ORDEN_COMPRA TBK_ID_SESION: $TBK_ID_SESION");
            $payment_data = $this->get_error_status($TBK_ORDEN_COMPRA);
        }

        if (empty($this->page_title)) {
            $data['page_title'] = Bconf::get($BCONF, "*.payment.fail.page_title");
        } else {
            $data['page_title'] = Bconf::get($BCONF, "*.language.".$this->page_title.".es");
        }

        if (empty($this->error_title)) {
            $data['error_title']= 'ERROR_TRANSACTION_FAIL_TITLE';
        }

        $data['error'] = $this->error_message;
        //this must be corrected when errors are rectified XITI xiti
        $data['appl'] = "pagos";

        $data['err_in_payment'] =  "1";

        //Build link to the CP email search, appending the errors
        $cp_url = Bconf::get($BCONF, "common.base_url.controlpanel");
        $error_params = "error_title=".$this->error_title;
        $error_params .= "&error_message=".$this->error_message;
        $email_of_cookie = $_COOKIE['telesales_last_search'];
        $email_param = "&search_email=".urlencode($email_of_cookie);
        $cp_error_url = $cp_url."/controlpanel?m=telesales&a=sell_products".$email_param."&".$error_params;

        header('Location: '.$cp_error_url);

        return 'FINISH';
    }

    function get_transaction_fail_html_code()
    {
        return Bconf::get($BCONF, "*.language.ERROR_TRANSACTION_FAIL_MESSAGE.es");
    }

    function get_purchase($orden)
    {
        $transaction = new bTransaction();
        $transaction->add_data('log_string', log_string());
        if (isset($orden) && !empty($orden)) {
            $transaction->add_data('payment_group_id', $orden);
        }
        $reply = $transaction->send_command('purchase_get');

        if ($transaction->has_error() || empty($reply['status'])) {
            return null;
        }

        return $reply;
    }

    function get_error_status($payment_group_id)
    {
        syslog(LOG_DEBUG, log_string() . "Payment: get_error_status");

        $purchase = $this->get_purchase($payment_group_id);
        $data = array();
        if (isset($purchase["purchase_get"][0]['status'])) {
            $data['payment_status'] = $purchase["purchase_get"][0]['status'];
            $data['pay_log_status'] = $purchase["purchase_get"][0]['pay_log_status'];
            $data['pay_group_status'] = $purchase["purchase_get"][0]['pay_group_status'];
        }


        if (!isset($data['payment_status'])) {
            syslog(LOG_ERR, log_string() . "Payment: data not found, payment_id = $payment_id");
            $this->page_title = "ERROR_SESSION_EXPIRED_PAGE_TITLE";
            $this->error_title = "ERROR_SESSION_EXPIRED_OOPS";
            $this->error_message = str_replace("#order_id#", $payment_id, $this->get_transaction_fail_html_code());
            bLogger::logDebug(__METHOD__, "Error payment 1-- data: ".var_export($data, true));
            return null;
        }

        if ($data['payment_status'] == 'refused' && $data['pay_log_status'] != "ERR_CANCELLED" && $data['pay_group_status'] == "unverified") {
            syslog(LOG_ERR, log_string() . "Payment: status=$data->payment_status");
            $this->page_title = "PAGE_FAILURE_BUMP_INCORRECT_DATA_TITLE";
            $this->error_title = "ERROR_SESSION_EXPIRED_OOPS";
            $this->error_message = "PAGE_FAILURE_BUMP_INCORRECT_DATA_ORDER_ID";
            bLogger::logDebug(__METHOD__, "Error payment 2 -- data: ".var_export($data, true));
            return null;
        }

        if ($data['pay_log_status'] == "ERR_CANCELLED" && $data['pay_group_status'] == "unverified") {
            $this->error_title = 'PAYMENT_OK_PRODUCT_FAIL_APOLOGIZE';
            $this->error_message =  'ERROR_TRANSACTION_FAIL_MESSAGE';
            bLogger::logError(__METHOD__, "Error payment 3 rejected -- data: ".var_export($data, true));
            return null;
        }
        // get the products
        $transaction = new bTransaction();
        $transaction->add_data('payment_group_id', $payment_group_id);
        $reply = $transaction->send_command('get_items_by_payment');
        if ($transaction->has_error(true)) {
            bLogger::logError(__METHOD__, "problems getting the items by payment");
            $this->error_message = "PAGE_FAILURE_BUMP_INCORRECT_DATA";
            return null;
        }
        $data = (object) array_merge((array)$data, (array)$reply['get_items_by_payment'][0]);

        if (!isset($data->ad_id)) {
            syslog(LOG_ERR, log_string() . "Id or Prod doesn't exist");
            $this->page_title = "ERROR_AD_DOES_NOT_EXISTS";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_ID_PROD_DOESNT_EXIST";
            bLogger::logError(__METHOD__, "Error payment 5 -- data: ".var_export($data, true));
            return null;
        }
        $this->get_email = $_COOKIE['telesales_last_search'];
        return $data;
    }

    function log_error($remote_add, $list_id, $prod, $doc_type, $price, $error, $transaction_failed)
    {
            closelog();
            openlog("product_err", LOG_ODELAY, LOG_LOCAL0);
            syslog(LOG_INFO, log_string()." yapo_payment: failed $transaction_failed (list_id, remote_addr, product, error) ($list_id, $remote_add, $prod, $error)");
            closelog();
            openlog("PAYMENT", LOG_ODELAY, LOG_LOCAL0);
    }

    function f_list_id($val)
    {
        if (preg_match('/^[0-9]+$/', $val)) {
            return $val;
        }
        if (!empty($val)) {
            return false;
        }
        return null;
    }

    /* Product code */
    function f_prod($val)
    {
        global $BCONF;
        if (Bconf::get($BCONF, "*.payment.product." . $val)) {
            return $val;
        }
        return null;
    }

    function f_ftype($val)
    {
        if (!empty($val)) {
            return $val;
        }
        return null;
    }

    function f_document($val)
    {
        global $BCONF;
        syslog(LOG_DEBUG, log_string() . "f_document= $val");
        if (Bconf::get($BCONF, "*.payment.document." . $val . ".doc_type") != null) {
            return $val;
        }
        return null;
    }

    function f_rut($val)
    {
        $val = preg_replace('/[^[a-zA-Z0-9]��������&,:; -]/i', '', $val);
        return strtoupper($val);
    }

    function f_source_link($val)
    {
        return $val;
    }


    function f_string($v) {
        return strip_tags($v);
    }

    function f_mb($val)
    {
        return strip_tags($val);
    }

    function f_name($val)
    {
        return strip_tags($val);
    }
    function f_lob($val)
    {
        return strip_tags($val);
    }

    function f_address($val)
    {
        return strip_tags($val);
    }
    function f_region($val)
    {
        return strip_tags($val);
    }
    function f_communes($val)
    {
        return strip_tags($val);
    }
    function f_contact($val)
    {
        return strip_tags($val);
    }
    function f_email($val)
    {
        return strip_tags($val);
    }
    function f_from($val)
    {
        return strip_tags($val);
    }
    function f_want_bump($val)
    {
        return strip_tags($val);
    }

    /* Validations of parameters from transbank
	   TODO Implement logic of validations */
    function f_TBK_ACCION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_ORDEN_COMPRA($val)
    {
        return strip_tags($val);
    }
    function f_TBK_CODIGO_COMERCIO($val)
    {
        return strip_tags($val);
    }
    function f_TBK_CODIGO_COMERCIO_ENC($val)
    {
        return strip_tags($val);
    }
    function f_TBK_TIPO_TRANSACCION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_RESPUESTA($val)
    {
        return strip_tags($val);
    }
    function f_TBK_MONTO($val)
    {
        return strip_tags($val);
    }
    function f_TBK_CODIGO_AUTORIZACION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_FINAL_NUMERO_TARJETA($val)
    {
        return strip_tags($val);
    }
    function f_TBK_FECHA_CONTABLE($val)
    {
        return strip_tags($val);
    }
    function f_TBK_FECHA_TRANSACCION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_FECHA_EXPIRACION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_HORA_TRANSACCION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_ID_SESION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_ID_TRANSACCION($val)
    {
        return strip_tags($val);
    }
    function f_TBK_TIPO_PAGO($val)
    {
        return strip_tags($val);
    }
    function f_TBK_NUMERO_CUOTAS($val)
    {
        return strip_tags($val);
    }
    function f_TBK_VCI($val)
    {
        return strip_tags($val);
    }
    function f_TBK_MAC($val)
    {
        return strip_tags($val);
    }
}

$payment = new blocket_telesales_payment();
$payment->run_fsm();
