<?php
require_once('autoload_lib.php');

$fInscription = new OneClickApiClient();
$redirection = Yapo\Bconf::get($GLOBALS["BCONF"], "*.oneclick.finish_inscription.redirect_to");
if (isset($_POST["TBK_TOKEN"])) {
        $fInscription->endInscription($_POST['TBK_TOKEN']);
}
header("location: {$redirection}");
