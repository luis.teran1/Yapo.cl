<?php

require_once('init.php');
require_once('util.php');
require_once('autoload_lib.php');

openlog('oneclick_api', LOG_ODELAY, LOG_LOCAL0);

class oneclick_api extends bRESTful {
	function __construct() {
		$this->restAPISettingsKey = '*.oneclick_handler_rest_settings';
	}
}

$api = new oneclick_api();
$api->run();

