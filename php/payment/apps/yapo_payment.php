<?php

$appName = "YapoPayment";
require_once('autoload_lib.php');
require_once('register_time.php');
require_once('util.php');
require_once('payment_common.php');

openlog("PAYMENT", LOG_ODELAY, LOG_LOCAL0);

use Yapo\EasyRedis;
use Yapo\WebpayService;
use Yapo\Bconf;

class blocket_payment extends common_payment_application {

	var $ad;

	var $scripts;
	var $styles_bump;

	var $sl;
	var $mb;
	var $manual_xiti_tag;
	var $from;
	var $want_bump;
	var $pr; //Pack result from ad insert

	var $styles_pay_multiproducts;
	var $scripts_pay_multiproducts;
	var $mp_ad_list_id;
	var $mp_ad_subject;
	var $mp_prod_id;
	var $mp_prod_name;
	var $mp_prod_price;
	var $forms_logged = array(3,5);
	private $cart_mngr;
	var $cart_status;
	var $is_logged_on_load = false;
	var $first_referer;
	var $validated = false;
	var $is_upselling_click = 0;

	public $tbk = array();

	function blocket_payment() {
		global $BCONF;
		global $session;
		$this->first_referer = @$_SERVER['HTTP_REFERER'];
		$this->headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
		$this->styles_bump = bconf_get($BCONF, "*.common.pay_bump.headstyles");
		$this->scripts_pay_multiproducts = bconf_get($BCONF, "*.common.pay_multiproduct.headscripts");
		$this->styles_pay_multiproducts = bconf_get($BCONF, "*.common.pay_multiproduct.headstyles");
		// Instance Session and RedisMngr
		$this->account_session = new AccountSession();
		$this->cart_mngr = new RedisMultiproductManager();
		if ($this->account_session->is_logged()) {
			$this->headscripts = bconf_get($BCONF, "*.common.pay_multiproduct.headscripts");
			$this->headstyles  = bconf_get($BCONF, "*.common.pay_multiproduct.headstyles");
		}

		if (isset($_POST['token_ws'])) {
			$redis_payment = new EasyRedis("redis_payment");
			$ws_orden = $redis_payment->get($redis_payment->prepend.$_POST['token_ws']);
			if (!empty($ws_orden)) {
				$this->tbk['TBK_ORDEN_COMPRA'] = $ws_orden;
				$this->tbk['TBK_ID_SESION']  = $_POST['token_ws'];
			}
		}

		if (isset($_POST['TBK_ID_SESION']) && isset($_POST['TBK_ORDEN_COMPRA'])) {
			$this->tbk['TBK_ID_SESION']  = $_POST['TBK_ID_SESION'];
			$this->tbk['TBK_ORDEN_COMPRA'] = $_POST['TBK_ORDEN_COMPRA'];
		}

		if (isset($_POST['TBK_TOKEN'])) {
			$this->tbk['TBK_TOKEN']  = $_POST['TBK_TOKEN'];
		}

        if (isset($_GET['kid'])) {
            $this->tbk['TBK_ORDEN_COMPRA'] = $_GET['kid'];
            $this->tbk['TBK_ID_SESION'] = $_GET['kid'];
        }

		$this->init('load', bconf_get($BCONF, "*.common.session.instances.ai"));
	}

	function get_state($statename) {
		switch($statename) {
			case 'load':
				return array('function' => 'payment_load',
					'params' => array(
						'id' => 'f_list_id',
						'prod',
						'ftype',
						'sl' => 'f_source_link',
						'mb',
						'from',
						'want_bump',
						'lt',
						'pr' => 'f_pr',
						'cc' => 'f_create_account',
						'ab_freq',
						'ab_num_days',
						'ab_use_night',
						'credits'
					),
					'method' => 'both',
					'reset_progress' => true,
					'allowed_states' => array('load')
				);
			case 'form':
				return array('function' => 'payment_form',
						'params' => array('id' => 'f_list_id', 'prod', 'mb', 'from', 'want_bump'),
						'method' => 'get',
						'allowed_states' => array('form', 'validate', 'oc_finish'));
			case 'validate':
				return array('function' => 'payment_validate',
						'params' => array('document', 'name', 'lob', 'rut', 'address', 'region', 'communes', 'contact', 'email', 'prod', 'payment_method', 'platform'),
						'method' => 'post',
						'allowed_states' => array('validate', 'form'));
			case 'webpay':
				return array('function' => 'payment_webpay',
						'method' => 'get');
			case 'webpay_plus':
				return array('function' => 'payment_webpay_plus',
						'method' => 'get');
			case 'servipag':
				return array('function' => 'payment_servipag',
						'method' => 'get');
			case 'khipu':
				return array('function' => 'payment_khipu',
						'method' => 'get');
			case 'oneclick':
				return array('function' => 'payment_one_click',
						'method' => 'get');
			case 'credits':
				return array('function' => 'payment_credits',
						'method' => 'post');
			case 'disabled':
				return array('function' => 'payment_disabled',
						'method' => 'get');
			case 'error':
				return array('function' => 'payment_error',
						'method' => 'get');
			case 'expired':
				return array('function' => 'payment_expired',
						'method' => 'get');
			case 'oc_register':
				return array('function' => 'oneclick_register',
						'method' => 'get');
			case 'oc_finish':
				return array('function' => 'oneclick_endinscription',
						'method' => 'post');
		}
	}

	function payment_load($id, $prod, $ftype, $sl = NULL, $mb = NULL, $from = NULL, $want_bump = NULL, $lt = NULL, $pr=NULL, $cc=NULL, $ab_freq = NULL, $ab_num_days = NULL, $ab_use_night = NULL, $credits = NULL) {

		global $BCONF;
		bLogger::logDebug(__METHOD__,  "id={$id}, prod={$prod}, ftype={$ftype}, sl={$sl}, mb={$mb}, pack_result={$pr}, create_account={$cc}");

		if (!is_null($mb)) {
			$this->mb = $mb;
		}

		if (!is_null($from)) { $this->from = $from; }
		if (!is_null($want_bump)) { $this->want_bump = $want_bump; }
		if (!is_null($pr)) { $this->pr = $pr; }
		if (!is_null($cc)) { $this->cc = $cc; }

		if (!payment_enabled()) {
			bLogger::logError(__METHOD__, "Payment aplication is disabled");
			return 'disabled';
		}

		if (!empty($this->tbk['TBK_ID_SESION']) && !empty($this->tbk['TBK_ORDEN_COMPRA'])) {
			bLogger::logDebug(__METHOD__, "Entering in payment aplication from fail of tbk");
			$data = $this->get_error_status($this->tbk['TBK_ORDEN_COMPRA'], $ftype);
			bLogger::logDebug(__METHOD__, "get_error_status data: ".print_r($data, true));
			if (is_null($data))
				return 'error';

			$this->prod = $data->product_id;
			$prod = $data->product_id;
			if (bconf_get($BCONF, "*.payment.product.$prod.group") == "COMBO_UPSELLING" ||
				bconf_get($BCONF, "*.payment.product.$prod.group") == "PROMO" ||
				bconf_get($BCONF, "*.payment.product.$prod.group") == "INSERTING_FEE_PREMIUM") {
				$this->id = $data->ad_id;
				$id = $data->ad_id;
				$this->ad_id = $data->ad_id;
				$this->prod = $prod;
			}
			if (bconf_get($BCONF, "*.payment.product.$prod.group") == "UPSELLING") {
				$this->id = $data->category.".".$data->ad_id;
				$id = $this->id;
			}
			else{
				$this->id = $data->list_id;
				$id = $data->list_id;
			}
		}

		if ($this->session_expired === true) {
			bLogger::logDebug(__METHOD__, "Payment: Form expired");
			$this->set_error_msj("ERROR_SESSION_EXPIRED_PAGE_TITLE", "ERROR_SESSION_EXPIRED_OOPS", "ERROR_SESSION_EXPIRED");
			return 'error';
		}

		/* If the user is not logged in */
		if (!$this->account_session->is_logged()) {
			//IF the user is not logged but is trying to get a url accesible only as a logged user
			//is redirected to loggin page
			if (in_array($ftype,$this->forms_logged) || $prod == self::PRODUCT_AUTOBUMP) {
				bLogger::logDebug(__METHOD__, "Session expired in dashboard -> redirecting to login");
				$url_login = bconf_get($BCONF, "*.common.base_url.secure")."/login";
				header('Location: '.$url_login);
				return 'FINISH';
			}

			//All other forms need ID and PROD
			if (!$id || !$prod) {
				if (strpos($this->first_referer,'dashboard') === false &&
					strpos($this->first_referer,'cuenta') === false &&
					$ftype != self::MULTIPRODUCT_TYPE) {
					bLogger::logError(__METHOD__, "Id or Prod doesn't exist");
					$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE","ERROR_ID_PROD_DOESNT_EXIST");
					$this->has_loaded = true;
					return 'error';
				}
			}

			// If the $id has one dot is not a list_id is an ad_id followed by category
			if (strpos((string)$id,'.') !== false) {
				// Execute trans to get ad info	
				$id_splited = explode('.',$id);
				// id_splited[0] -- category
				// id_splited[1] -- ad_id
				$trans_load = new bTransaction();
				$trans_load->add_data('ad_id',$id_splited[1]);
				$load_reply = $trans_load->send_command('get_ad_info');
				if ($trans_load->has_error(true)) {
					bLogger::logDebug(__METHOD__, "Ad $id not found");
					$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
					return 'error';
				} else {
					$this->ad_id = $id_splited[1];
					if (empty($load_reply['email']) || empty($load_reply['user_id']) || empty($load_reply['name'])) {
						bLogger::logDebug(__METHOD__, "Necesary data missing on Trans for $id ");
						$this->set_error_msj("ERROR_AD_INFO_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_INFO_DOESNT_EXIST");
						return 'error';
					}

					$this->company_buyer = ($load_reply['company_ad'] == 1)?'COMPANY':'PRIVATE';
					$this->email_buyer = $load_reply['email'];
					$this->user_id_buyer = $load_reply['user_id'];
					$this->buyer_name = $load_reply['name'];
					$this->category = $id_splited[0];
					$this->is_upselling_click = 1;
					$this->is_upselling_prod = bconf_get($BCONF, "*.payment.product." . $prod . ".referenced_codename");
					$result = $load_reply;
				}
			} elseif (bconf_get($BCONF, "*.payment.product.{$prod}.get_ad_by") == "ad_id") {
				$this->ad_id = $id;
				$this->prod = $prod;
				bLogger::logDebug(__METHOD__, "upselling combo unlogged");
				$trans_load = new bTransaction();
				$trans_load->add_data('ad_id', $this->ad_id);
				$load_reply = $trans_load->send_command('get_ad_info');
				if ($trans_load->has_error(true)) {
					bLogger::logDebug(__METHOD__, "Ad $id not found");
					$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
					return 'error';
				}
				else {
					if (empty($load_reply['email']) || empty($load_reply['user_id']) || empty($load_reply['name'])) {
						bLogger::logDebug(__METHOD__, "Necesary data missing on Trans for $id ");
						$this->set_error_msj("ERROR_AD_INFO_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_INFO_DOESNT_EXIST");
						return 'error';
					}

					$this->company_buyer = ($load_reply['company_ad'] == 1)?'COMPANY':'PRIVATE';
					$this->email_buyer = $load_reply['email'];
					$this->user_id_buyer = $load_reply['user_id'];
					$this->buyer_name = $load_reply['name'];
					$this->category = $load_reply["category"];
					$result = $load_reply;
				}
			} else {
				$result = get_single_ad($id);
				if (empty($result)) {
					bLogger::logDebug(__METHOD__, "Ad $id not found");
					$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
					return 'error';
				}

				if (empty($result['email']) || empty($result['user_id']) || empty($result['name'])) {
					bLogger::logDebug(__METHOD__, "Necesary data missing on Asearch for $id ");
					$this->set_error_msj("ERROR_AD_INFO_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_INFO_DOESNT_EXIST");
					return 'error';
				}

				$this->company_buyer = ($result['company_ad'] == 1)?'COMPANY':'PRIVATE';
				$this->email_buyer = $result['email'];
				$this->user_id_buyer = $result['user_id'];
				$this->buyer_name = $result['name'];
				$this->category = $result['category'];
				if (!empty($id)) {
					$this->list_id = $id;
				}
			}

			$this->prod = $prod;

			// When the product is label OR autobump
			if (bconf_get($BCONF, "*.payment.product.".$prod.".extra_params") == 1) {
				if (bconf_get($BCONF, "*.payment.product.".$prod.".param.label_type") == "lt") {
					if (!is_null($lt)) {
						$this->label_type = $lt;
						setcookie(bconf_get($BCONF,'*.payment.label_type.cookie.name'),
								  $lt,
								  time() + bconf_get($BCONF, '*.payment.label_type.cookie.ttl'), '/',
								  bconf_get($BCONF, '*.common.session.cookiedomain') . '; ');
					} else if (isset($_COOKIE[bconf_get($BCONF,'*.payment.label_type.cookie.name')])) {
						$this->label_type = $_COOKIE['label_type'];
					} else {
						bLogger::logError(__METHOD__, "Label Type is empty");
					}
				}
				if (bconf_get($BCONF, "*.payment.product.".$prod.".is_autobump") == "1") {
					if (!is_null($ab_freq) && !is_null($ab_num_days) && !is_null($ab_use_night)) {
						$this->ab_freq = $ab_freq;
						$this->ab_num_days = $ab_num_days;
						$this->ab_use_night = $ab_use_night;
					} else {
						bLogger::logError(__METHOD__, "Missing parameters for autobump");
					}
				}
			}

			if (!empty($ftype))
				$this->form_type = $ftype;
			if (!is_null($sl)) {
				$this->sl = $sl;
			}
			bLogger::logDebug(__METHOD__, "populating ad");
			$this->ad = $this->populate_form_data($result);

		}
		else {
			/* This is the case when the user is logged in */
			//if the params id and prod have info this need to be added in shopping cart
			$this->form_type = self::MULTIPRODUCT_TYPE;

			if (empty($this->tbk['TBK_ID_SESION']) && empty($this->tbk['TBK_ORDEN_COMPRA'])) {
				if (bconf_get($BCONF, "*.payment.product.{$prod}.is_for_ads") == 1 && $id && $prod) {
					if (bconf_get($BCONF, "*.payment.product.".$prod.".extra_params") == 1) {
						if (bconf_get($BCONF, "*.payment.product.".$prod.".param.label_type") == "lt") {
							$this->list_id = $id;
							$this->prod = $prod;
							$this->label_type = $lt;
						}
						if (bconf_get($BCONF, "*.payment.product.".$prod.".is_autobump") == "1") {
							$this->list_id = $id;
							$this->prod = $prod;
							$this->ab_freq = $ab_freq;
							$this->ab_num_days = $ab_num_days;
							$this->ab_use_night = $ab_use_night;
						}
					}

					if (bconf_get($BCONF, "*.payment.product.{$prod}.group") == "COMBO_UPSELLING" || bconf_get($BCONF, "*.payment.product.{$prod}.group") == "PROMO") {
						// With COMBO UPSELLING we do not use shopping cart
						$this->ad_id = $id;
						$this->prod = $prod;
						bLogger::logDebug(__METHOD__, "upselling combo");
					} elseif (bconf_get($BCONF, "*.payment.product.{$prod}.get_ad_by") == "ad_id") {
						if (bconf_get($BCONF, "*.payment.product.{$prod}.group") == "UPSELLING") {
							$id_splited = explode('.',$id);
							$this->ad_id = $id_splited[1];
							$this->prod = $prod;
							$this->is_upselling_click = 1;
							$this->is_upselling_prod = bconf_get($BCONF, "*.payment.product." . $prod . ".referenced_codename");
							$this->category = $id_splited[0];
						} else {
							$this->ad_id = $id;
							$this->prod = $prod;
							$trans_load = new bTransaction();
							$trans_load->add_data('ad_id', $this->ad_id);
							$load_reply = $trans_load->send_command('get_ad_info');
							if ($trans_load->has_error(true)) {
								bLogger::logDebug(__METHOD__, "Ad $id not found");
								$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
								return 'error';
							} else {
								$this->category = $load_reply["category"];
							}
						}

						$values['p'] = Bconf::getProductPrice($BCONF, $this->prod, $this->category);
						if (isset($this->label_type)) {
							$values['lt'] = $this->label_type;
						}
						// Add the ad to redis
						$json_values = yapo_json_encode($values);
						bLogger::logDebug(__METHOD__,  "SELECT1:".print_r($json_values, true));
						$this->cart_status = $this->cart_mngr->select($this->account_session->get_param('email'), $this->prod, $this->ad_id, $json_values, true);
					} else {
						$this->list_id = $id;
						$this->prod = $prod;
						$result = get_subjects_n_categories(array($id));
						$this->category = $result['category'][0];
						$values['p'] = Bconf::getProductPrice($BCONF, $prod, $this->category);
						if (!empty($values['p'])) {
							if (isset($this->label_type)) {
								$values['lt'] = $this->label_type;
							}
							if (isset($this->ab_freq) && isset($this->ab_num_days) && isset($this->ab_use_night)) {
								$values['p'] = get_autobump_price($id, 'frequency:'.$this->ab_freq.';num_days:'.$this->ab_num_days.';use_night:'.$this->ab_use_night);
								$values['abf'] = $this->ab_freq;
								$values['abd'] = $this->ab_num_days;
								$values['abn'] = $this->ab_use_night;
							}
							// Get status of the ad and pack_status of the ad params
							$trans_ad_status = new bTransaction();
							$trans_ad_status->add_data('list_id', $id);
							$reply = $trans_ad_status->send_command('get_ad_info');

							// Know if ad is deactivated with pack
							$ad_deactivated_with_pack = false;

							if ($trans_ad_status->has_error(true)) {
								bLogger::logDebug(__METHOD__, "Ad $id not found");
								$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
							} else {
								// Ad is pack
								if (isset($reply['ad_status']) && isset($reply['ad_param']) && $reply['ad_status'] == 'deactivated') {
									foreach ($reply['ad_param'] as $_ => $param_value) {
										if ((bool)strstr($param_value, "pack_status")) {
											$ad_deactivated_with_pack = true;
											break;
										}
									}
								}
							}

							// Add the list_id to redis if ad status is not deactivated and not is pack or trans error
							if (!$ad_deactivated_with_pack) {
								$json_values = yapo_json_encode($values);
								bLogger::logDebug(__METHOD__,  "SELECT2:".print_r($json_values, true));
								$this->cart_status = $this->cart_mngr->select($this->account_session->get_param('email'), $prod, $id, $json_values, true);
							}
						}
					}
				} elseif ($prod) { // Prods not for ads
					$this->prod = $prod;
					if (bconf_get($BCONF, "*.payment.product.{$prod}.group") !== "CREDITS") {
						$values['p'] = bconf_get($BCONF, "*.payment.product.{$prod}.price");
						$json_values = yapo_json_encode($values);
						bLogger::logDebug(__METHOD__,  "SELECT3:".print_r($json_values, true));
						$this->cart_status = $this->cart_mngr->select($this->account_session->get_param('email'), $prod, 0, $json_values, true);
					} else {
						if (!empty($credits)) {
							$exchange_rate = bconf_get($BCONF, "*.payment.product.{$prod}.exchange_rate");
							$values['p'] = $exchange_rate * $credits;
							$json_values = yapo_json_encode($values);
							bLogger::logDebug(__METHOD__,  "SELECT3:".print_r($json_values, true));
							$this->cart_status = $this->cart_mngr->select($this->account_session->get_param('email'), $prod, 0, $json_values, true);
						}
					}
				}
			}

			//Populate vars with account info
			$this->email_buyer = $this->account_session->get_param('email');
			$this->company_buyer = ($this->account_session->get_param('is_company') == 't')?'COMPANY':'PRIVATE';
			$this->user_id_buyer = $this->account_session->get_param('user_id');
			$this->buyer_name = $this->account_session->get_param('name');
			$this->is_logged_on_load = true;
		}
		$this->has_loaded = true;

		if (empty($this->tbk['TBK_ID_SESION']) && !empty($this->tbk['TBK_TOKEN'])) {
			bLogger::logDebug(__METHOD__, "LOAD -> OC_FINISH");
			return 'oc_finish';
		}

		return 'form';
	}

 	function payment_form($id = NULL, $prod = NULL, $mb, $from = NULL, $want_bump = NULL) {
		global $BCONF;
		bLogger::logDebug(__METHOD__,  "id={$id}, prod={$prod}, mb={$mb}, form={$from}, want_bump={$want_bump}");
		if (!is_null($mb)) { $this->mb = $mb; }

		if (!payment_enabled()) {
			return 'disabled';
		}

		if ($this->has_loaded === false)
			return 'load';

		$data = array();
		//$data['headscripts'] = $this->headscripts;
		//$data['headstyles'] = $this->headstyles;


		if ($this->session_expired || ($this->form_type != self::MULTIPRODUCT_TYPE && ((!$this->list_id && !$this->ad_id) || !$this->prod))) {
			if ($this->session_expired) {
				bLogger::logDebug(__METHOD__, "session expired");
				$data['show_error_tbk'] = 1;
				$this->set_error_msj("ERROR_SESSION_EXPIRED_PAGE_TITLE", "ERROR_SESSION_EXPIRED_OOPS", "ERROR_SESSION_EXPIRED");
			} else {
				$this->error_message = "ERROR_ID_PROD_DOESNT_EXISTS";
			}

			$this->display_layout('payment/pay_form.html', $data, null, null, 'mobile_select');
			return 'FINISH';
		}

		if (!$this->has_error() && $this->ad) {
			foreach($this->ad as $key => $val) {
				if (!empty($val) && $key != "email")
					$data[$key] = $val;
			}
		}

		if (!empty($this->tbk['TBK_ORDEN_COMPRA'])) {
			$data['show_error_message'] = str_replace("#order_id#",$this->tbk['TBK_ORDEN_COMPRA'], $this->get_transaction_fail_html_code());
			$data['error_message_id'] = 'ERROR_TRANSACTION_FAIL_MESSAGE';
			$data['error'] = 'TBK_ORDEN_COMPRA_NOT_EMPTY';
			$data['show_error_title'] =  "ERROR_SESSION_EXPIRED_OOPS";
			$data['error_order_id'] =  $this->tbk['TBK_ORDEN_COMPRA'];
			$data['err_in_payment'] =  "1";
			unset($this->tbk);
		}

		$this->page_title = lang("PAGE_BUMP_FORM_TITLE");
		$data['appl'] = "pagos";

		//if user is logged in, get account information.
		if ($this->account_session->is_logged()) {
			if (!$this->has_error()) {
				$data['account_logged_in'] = true;
				$data['name'] = $this->account_session->get_param('business_name');
				if (empty($data['name'])) {
					$data['name'] = $this->account_session->get_param('name');
				}
				if ($this->account_session->get_param('is_company') == 't')
					$data['document'] = 2;
				$data['phone'] = $this->account_session->get_param('phone');
				$data['rut'] = $this->account_session->get_param('rut');
				$data['contact'] = $this->account_session->get_param('contact');
				$data['lob'] = $this->account_session->get_param('lob');
				$data['address'] = $this->account_session->get_param('address');
				$data['region'] = $this->account_session->get_param('region');
				$data['communes'] = $this->account_session->get_param('commune');
			}
			$email =  $this->account_session->get_param('email');
			$ocp_client = new OneClickApiClient();
			$user_oneclick= $ocp_client->isRegistered($email);
			$data['user_oneclick'] = ($user_oneclick) ? '1' : '0'; 

			if ($this->from_register) {
				$data['oneclick_subscription'] = $this->oneclick_register_success; 
			}
			//if user from Ad insert with pack, show pack info
			if (isset($this->pr) && $this->pr == "0") {
				$data['pack_result'] = 0;
			}
		}

		if (isset($this->cc) && $this->cc == "1" && $from == "newad") {
			$data['SHOWCREATEACCOUNT'] = 1;
		}

		if ($want_bump == "1") {
			$data['want_bump'] = $want_bump;
			$data['from'] = $from;
			if (!$this->account_session->is_logged()) {
				$this->form_type = 1;
			}
		}

		if (!$this->is_logged_on_load && $this->account_session->is_logged() && $this->form_type != self::SINGLEPRODUCT_TYPE_MOBILE) {
			/* This is the case when the user is logged in */
			//if the params id and prod have info this need to be added in shopping cart
			if ($this->list_id && $this->prod) {
				// Add the list_id to redis
				bLogger::logDebug(__METHOD__,  "id=$id, prod=$prod");
				$product_price = Bconf::getProductPrice($BCONF, $this->prod, $this->category);
				bLogger::logDebug(__METHOD__,  "SELECT4:".print_r($product_price, true));
				$this->cart_status = $this->cart_mngr->select($this->account_session->get_param('email'), $this->prod, $this->list_id, $product_price, true);
			}
			$this->form_type = self::MULTIPRODUCT_TYPE;
		}
		if ($this->form_type == self::SINGLEPRODUCT_TYPE_NEW || $this->form_type == self::SINGLEPRODUCT_TYPE_MOBILE) {
			$this->headstyles = $this->styles_bump;
			$this->display_layout('payment/pay_bump.html', $data, null, null, 'mobile_select');
		}
		elseif (bconf_get($BCONF, "*.payment.product." . $this->prod . ".group") == "COMBO_UPSELLING" || bconf_get($BCONF, "*.payment.product." . $this->prod . ".group") == "PROMO") {
			$template = 'payment/pay_form.html';
			if ($this->form_type != self::SINGLEPRODUCT_TYPE) {
				$template = 'payment/pay_multiproduct.html';
				$this->headstyles    = $this->styles_pay_multiproducts;
				$this->headscripts   = $this->scripts_pay_multiproducts;
			}

			if (!$this->show_combo_on_summary($data, $this->ad_id, $this->prod, $template)) {
				return 'error';	
			}
		}
		elseif ($this->form_type == self::MULTIPRODUCT_TYPE ||	(bconf_get($BCONF, "*.payment.product.".$this->prod.".is_store"))) {
			$this->show_shopping_cart($data);
		}else{
			$this->display_layout('payment/pay_form.html', $data, null, null, 'mobile_select');
		}
	}


	function payment_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email, $prod = 0, $payment_method, $platform) {
		global $BCONF;
		//This restore the headstuff if in some point were changed
		$this->headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$this->headstyles = bconf_get($BCONF, "*.payment.headstyles");
	
		if (bconf_get($BCONF, "*.payment.product." . $this->prod . ".group") != "COMBO_UPSELLING" && bconf_get($BCONF, "*.payment.product." . $this->prod . ".group") != "PROMO") {
			$this->load_products_from_cart();
		}
		$this->validated = true;
		return $this->common_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email, $prod, $payment_method, $platform);
	}

	function payment_webpay_plus() {
		if (!$this->validated)
			return 'form';
		$headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$headstyles = bconf_get($BCONF, "*.payment.headstyles");
		return $this->webpay_plus_payment($headstyles, $headscripts);
	}

	function payment_webpay() {
		if (!$this->validated)
			return 'form';
		$headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$headstyles = bconf_get($BCONF, "*.payment.headstyles");
		return $this->webpay_payment($headstyles, $headscripts);
	}

	function payment_servipag() {
		if (!$this->validated)
			return 'form';
		$headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$headstyles = bconf_get($BCONF, "*.payment.headstyles");
		return $this->servipag_payment($headstyles, $headscripts);
	}

	function payment_khipu() {
		if (!$this->validated)
			return 'form';
		$headscripts = bconf_get($BCONF, "*.payment.headscripts");
		$headstyles = bconf_get($BCONF, "*.payment.headstyles");
		return $this->khipu_payment($headstyles, $headscripts);
	}

	function payment_one_click() {
		if (!$this->validated)
			return 'form';
		return $this->oc_payment();
	}

	function payment_credits() {
		if (!$this->validated)
			return 'form';
		return $this->credits_payment();
	}

	function oneclick_register() {
		global $BCONF;

		bLogger::logDebug(__METHOD__,  "ONECLICK: register:");
		if ($this->account_session->is_logged() && !$this->has_error()) {
			$email = $this->account_session->get_param('email');
			$ocp_client = new OneClickApiClient();
			$user_oneclick= $ocp_client->isRegistered($email);
			if ($user_oneclick) {
				return 'form';	
			}

			$return_url = bconf_get($BCONF, '*.common.base_url.ms_oneclick_return_url');
			$result = $ocp_client->initInscription($email, $return_url);

			if (isset($result) && $result->result == 'OK') {
				bLogger::logDebug(__METHOD__,  "ONECLICK: result init_inscription:".print_r($result, true));
				$redirectData = Array();
				$redirectData['url'] = $result->url; 
				$redirectData['method'] = 'POST'; 
				$redirectData['params']['TBK_TOKEN'] = $result->token;
				$simple_data['no_footer'] = 1;
				$simple_data['no_header'] = 1;
				bLogger::logInfo(__METHOD__,"redirectData = " . var_export($redirectData, true));
				$oc_register_template = 'payment/redirect_oneclick_register.html';//bconf_get($BCONF, "*.payment.options.5.template");
				$this->display_layout($oc_register_template, $simple_data, $redirectData, null, 'html5_base');
				return 'FINISH';
			} else {
				bLogger::logError(
					__METHOD__,
					self::PAY_METHOD_ERR_PREF."error initiating OneClick inscription. received result: ". var_export($result, true)
				);
			}
			$this->from_register = true;
			$this->oneclick_register_success = 0; 
		} 
		return 'form';
	}

	function oneclick_endinscription() {
		if (empty($this->tbk['TBK_TOKEN'])) {
			return 'form';
		}

		$token = $this->tbk['TBK_TOKEN'];
		bLogger::logDebug(__METHOD__,  "EndInscription token $token");
		if ($this->account_session->is_logged() && !$this->has_error()) {
			$ocp_client = new OneClickApiClient();
			$result = $ocp_client->endInscription($this->tbk['TBK_TOKEN']);
			bLogger::logInfo(__METHOD__,"Result endInscription = " . var_export($result, true));
			if (!$result) {
				bLogger::logError(
					__METHOD__,
					self::PAY_METHOD_ERR_PREF."error ending OneClick inscription."
				);
			}
			$this->from_register = true;
			$this->oneclick_register_success = ($result) ? 1 : 0; 
			return 'form';
		} 
		return 'error';
	}

	function show_combo_on_summary($data, $ad_id, $prod, $template) {
		global $BCONF;
		$this->page_title = lang("PAGE_UPSELLING_PAYMENT_FORM_TITLE");
		$this->mp_ad_list_id = array();
		$this->mp_ad_subject = array();
		$this->mp_prod_id    = array();
		$this->mp_prod_name  = array();
		$this->mp_prod_price = array();

		if (!$this->add_upselling_combo($ad_id, $prod)) {
			return false;
		}	
		if (count($this->mp_prod_price) != 1) {
			return false;	
		}

		$data['appl_step'] = 'summary';		// Step in payment, so it can be identified by xiti settings
		$data['appl'] = 'pagos_logged';
		$data['hide_footer'] = 1;
		$data['payment_method'] = $this->payment_method;
		$data['combo_process_price'] = $this->mp_prod_price[0];
		$WebpayService = new WebpayService();
		$response = $WebpayService->execute("healthCheck");
		if (!$response) {
			bLogger::logError(
				__METHOD__,
				self::PAY_METHOD_ERR_PREF."Webpay plus service not available"
			);
			$data['webpay_plus_disabled'] = 1;
		}
		$credits = new CreditsService();
		$data['user_credits'] = $credits->getBalance($this->account_session->get_param('account_id'))->Balance;
		if ($data['user_credits'] == "-1") {
			# add error log
			bLogger::logError(
				__METHOD__,
				self::PAY_METHOD_ERR_PREF."error fetching credit balance from user: " . $this->account_session->get_param('account_id')
			);
		}

		unset($this->cart_status);
		bLogger::logInfo(__METHOD__,"Show combo upselling product (ad_id, product) => ($ad_id, $prod)");
		$this->display_layout($template, $data, null, null, 'mobile_select');
		return true;
	}

	/* Populate Shopping Cart from redis */
	function show_shopping_cart($data) {
		global $BCONF;
		$this->headstyles    = $this->styles_pay_multiproducts;
		$this->headscripts   = $this->scripts_pay_multiproducts;
		$this->page_title = lang("PAGE_MULTIPRODUCT_FORM_TITLE");
		$this->mp_ad_list_id = array();
		$this->mp_ad_subject = array();
		$this->mp_prod_id    = array();
		$this->mp_prod_name  = array();
		$this->mp_prod_price = array();

		$this->load_products_from_cart();
		if (count($this->list_ids) > 0) {
			$this->add_products_ads_on_asearch();	
		}
		if (count($this->ad_ids) > 0) {
			$this->add_upselling_products();	
		}
		if (count($this->other_products_ids) > 0) {
			$this->add_other_products();	
		}

		$data['appl_step'] = 'summary';		// Step in payment, so it can be identified by xiti settings
		$data['appl'] = 'pagos_logged';
		$data['hide_footer'] = 1;
		$data['payment_method'] = $this->payment_method;
		$WebpayService = new WebpayService();
		$response =$WebpayService->execute("healthCheck");
		if (!$response) {
			$data['webpay_plus_disabled'] = 1;
			bLogger::logError(
				__METHOD__,
				self::PAY_METHOD_ERR_PREF."webpay service not available"
			);
		}
		$credits = new CreditsService();
		$data['user_credits'] = $credits->getBalance($this->account_session->get_param('account_id'))->Balance;
		if ($data['user_credits'] == "-1") {
			# add error log
			bLogger::logError(
				__METHOD__,
				self::PAY_METHOD_ERR_PREF."error fetching credit balance from user: " . $this->account_session->get_param('account_id')
			);
		}

		unset($this->cart_status);
		$this->display_layout('payment/pay_multiproduct.html', $data, null, null, 'mobile_select');
	}

	function add_product($id, $subject, $prod_id, $prod_name, $prod_price) {
		$this->mp_ad_list_id[] = $id;
		$this->mp_ad_subject[] = $subject;
		$this->mp_prod_id[]    = $prod_id;
		$this->mp_prod_name[]  = $prod_name;
		$this->mp_prod_price[] = $prod_price;
	}

	function load_products_from_cart() {
		$this->list_ids = array();
		$this->ad_ids = array();
		$this->list_id_prod_params = array();
		$this->ad_id_prod_params = array();
		$this->other_products_ids = array();

		if (!isset($this->cart_status)) {
			$this->cart_status = $this->cart_mngr->list_all($this->account_session->get_param('email'));
		}

		if (!isset($this->cart_status['ads'])) {
			return;
		}
		$shopping_cart = $this->cart_status['ads'];
		if (!empty($shopping_cart)) {
			foreach($shopping_cart as $key => $val) {
				foreach($val as $prod_item) {
					if (is_array($prod_item)) {
						$item = $prod_item[0];
						$params = $prod_item[1];
						$use_ad_id = bconf_get($BCONF, "*.payment.product.{$item}.get_ad_by") == "ad_id";
						$group = bconf_get($BCONF, "*.payment.product.{$item}.group");
						$enabled_in_dashboard = bconf_get($BCONF, "*.payment.product_group.{$group}.enabled_in_dashboard");

						if (bconf_get($BCONF, "*.payment.product.{$item}.is_for_ads") == 1) {
							if ($use_ad_id) {
								$this->ad_ids[$key][] = $item;
								$this->ad_id_prod_params[$key][$item] = $params;
							}
							else if ($enabled_in_dashboard == 1) {
								$this->list_ids[$key][] = $item;
								$this->list_id_prod_params[$key][$item] = $params;
							}
						} else {
							$this->other_products_ids[] = $prod_item;
						}
					}
				}
			}
		}
		bLogger::logDebug(__METHOD__,  "list_ids:".print_r($this->list_ids, true));
		bLogger::logDebug(__METHOD__,  "list_ids_param:".print_r($this->list_id_prod_params, true));
		bLogger::logDebug(__METHOD__,  "ad_ids:".print_r($this->ad_ids, true));
		bLogger::logDebug(__METHOD__,  "ad_ids_param:".print_r($this->ad_id_prod_params, true));
		bLogger::logDebug(__METHOD__,  "other_ids:".print_r($this->other_products_ids, true));
	}

	function add_upselling_combo($ad_id, $prod) {
		$this->list_ids = array();
		$this->ad_ids = array();
		$this->list_id_prod_params = array();
		$this->ad_id_prod_params = array();
		$this->other_products_ids = array();

		$trans_load = new bTransaction();
		$trans_load->add_data('ad_id', $ad_id);
		$load_reply = $trans_load->send_command('get_ad');

		if ($trans_load->has_error(true) || empty($load_reply['subject']) || empty($load_reply['category'])) {
				return false;
		}
		else {
			$subject = $load_reply['subject'];
			$category = $load_reply['category'];
			$product_name = Bconf::getProductName($BCONF, $prod, $category);
			$product_codename = bconf_get($BCONF, "*.payment.product.$prod.codename");
			$product_price = Bconf::getProductPrice($BCONF, $prod, $category);
			$this->add_product($ad_id, $subject, $prod, $product_name, $product_price);
			$this->ad_ids[$ad_id][] = $prod;
			$this->ad_id_prod_params[$ad_id][$prod] = !empty($this->label_type)?$this->label_type:0;
			if ($this->from == "edit") {
				if ($this->ad_id_prod_params[$ad_id][$prod] == 0) {
					$this->ad_id_prod_params[$ad_id][$prod] = "with_delay:0";
				} else {
					$this->ad_id_prod_params[$ad_id][$prod] .= ";with_delay:0";
				}
			}
			 bLogger::logDebug(__METHOD__,  "prod params:".print_r($this->ad_id_prod_params, true));
		}
		return true;
	}

	function add_upselling_products() {
		foreach ($this->ad_ids as $ad_id => $prods) {
			$trans_load = new bTransaction();
			$trans_load->add_data('ad_id', $ad_id);
			$load_reply = $trans_load->send_command('get_ad');

			if ($trans_load->has_error(true) || empty($load_reply['subject']) || empty($load_reply['category'])) {
				$this->cart_mngr->removeAllAdProducts($this->account_session->get_param('email'),  $ad_id);
			}
			else {
				foreach ($prods as $prod) {
					$has_same_product = false;
					//verified ad_id has not same Upselling Products
					$list_id = $load_reply['list_id'];
					if (!empty($list_id) && array_key_exists($list_id, $this->list_ids)) {

						foreach($this->list_ids[$list_id] as $ad_prod) {
							if ($prod == $ad_prod || $ad_prod == bconf_get($BCONF, "*.payment.product.$prod.referenced_product")) {
								$this->cart_mngr->unselect($this->account_session->get_param('email'), $prod, $ad_id);
								$has_same_product = true;
							}
						}
					}
					if (!$has_same_product) {
						$subject = $load_reply['subject'];
						$category = $load_reply['category'];
			                        $product_name = Bconf::getProductName($BCONF, $prod, $category);
						$product_codename = bconf_get($BCONF, "*.payment.product.$prod.codename");
						$product_price = Bconf::getProductPrice($BCONF, $prod, $category);
						$this->add_product($ad_id, $subject, $prod, $product_name, $product_price);
					}
				}
			}
		}
	}

	function add_products_ads_on_asearch() {
		$result = get_subjects_n_categories(array_keys($this->list_ids));

		foreach($this->list_ids as $id => $prods) {
			if (!in_array($id, $result['list_id'])) {
				//The ad is not in asearch or deactivated so we remove them from all products on redis
				$this->cart_mngr->removeAllAdProducts($this->account_session->get_param('email'),  $id);
			}
			else{
				foreach($prods as $prod) {
					$position = array_search($id, $result['list_id']);
					$subject = $result['subject'][$position];
					$category = $result['category'][$position];
			                $product_name = Bconf::getProductName($BCONF, $prod, $category);
					$product_codename = bconf_get($BCONF, "*.payment.product.$prod.codename");
					if ($prod == self::PRODUCT_AUTOBUMP) {
						$product_price = get_autobump_price($id, $this->list_id_prod_params[$id][$prod]);
					} else {
						$product_price = Bconf::getProductPrice($BCONF, $prod, $category);
					}
					$this->add_product($id, $subject, $prod, $product_name, $product_price);
				}
			}
		}
	}

	function add_other_products() {
		foreach ($this->other_products_ids as $product) {
			$product_id = $product[0];
			$price = $product[1];
			$product_group = bconf_get($BCONF, "*.payment.product.{$product_id}.group");

			$add_product = 0;
			if (in_array($product_group, array("STORE","CREDITS"))) {
				$add_product = 1;
			}
			if ($product_group == "PACK") {
				if (1 == bconf_get($BCONF, "*.packs.enabled")) {
					$pack_type = $product_id / 10000;
					$pack_type = round($pack_type, 0, PHP_ROUND_HALF_DOWN);
					if (1 == bconf_get($BCONF,"*.packs.type.".$pack_type.".enabled")) {
						$add_product = 1;
					}
				}
			}

			if ($add_product != 0) {
				$product_type = bconf_get($BCONF, "*.payment.product.$product_id.type");
				$product_name = bconf_get($BCONF, "*.payment.product.$product_id.description");
				$product_price = $price;
				$this->add_product(0, $product_type, $product_id, $product_name, $product_price);
			}
		}
	}

	function f_ab_use_night($val) {
		return $val ? "1" : "0";
	}

	function f_credits($val) {
		$min = bconf_get($BCONF, "*.payment.product.50.min_quantity");
		$max = bconf_get($BCONF, "*.payment.product.50.max_quantity");
		if (is_numeric($val) && ($val >= $min && $max >= $val)) {
			return $val;
		}
		bLogger::logDebug(__METHOD__, "Value not in range.");
		return null;
	}
}

$payment = new blocket_payment();
$payment->run_fsm();
