<?php

namespace Yapo;
require_once('autoload_lib.php');
require_once('util.php');
require_once('RedisMultiproductManager.php');
require_once('Servipag.php');
require_once('RedisSessionClient.php');

require_once('memcachedsession.php');
openlog("payment", LOG_ODELAY, LOG_LOCAL0);

$pvf = new PaymentVerify($pay_application = "payment");
if(!empty($_REQUEST)) {
    $pvf->start();
}else {
	$pvf->redirectToErrorPage();
}

