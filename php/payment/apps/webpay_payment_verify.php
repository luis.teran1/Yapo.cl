<?php
namespace Yapo;

$appName = "WebpayVerify";

openlog("PAYMENT", LOG_ODELAY, LOG_LOCAL0);
require_once('autoload_lib.php');
require_once('register_time.php');
require_once('httpful.php');
require_once('JSON.php');

$pvf = new WebpayVerify();
if (!empty($_REQUEST)) {
    $pvf->verify();
} else {
    $pvf->redirectToErrorPage();
}
