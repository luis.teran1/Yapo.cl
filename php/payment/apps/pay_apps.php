<?php

require_once('autoload_lib.php');
require_once('payment_common.php');
require_once('util.php');

openlog("PAYMENT_APPS", LOG_ODELAY, LOG_LOCAL0);

class blocket_pay_apps extends common_payment_application {

	var $subject;

	function blocket_pay_apps() {
		global $BCONF;
		$this->application_name = 'pay_apps';
		$this->page_title = lang('PAGE_APPS_TITLE');
		$this->page_name = lang('PAGE_APPS_NAME');
		$this->headstyles = bconf_get($BCONF, "pay_apps.headstyles");
		$this->headscripts = bconf_get($BCONF, "pay_apps.headscripts");
		$this->add_static_vars(array('tbk'));

		if(isset($_POST['TBK_ID_SESION']) && isset($_POST['TBK_ORDEN_COMPRA'])){
			$this->tbk['TBK_ID_SESION']  = $_POST['TBK_ID_SESION'];
			$this->tbk['TBK_ORDEN_COMPRA'] = $_POST['TBK_ORDEN_COMPRA'];
		}

		if(isset($_REQUEST['v2']))
			$this->v2 = 1;

		$this->init('load', bconf_get($BCONF, "*.common.session.instances.ai"));
	}

	function get_state($statename) {
		switch($statename) {
			case 'load':
				return array('function' => 'pay_apps_load',
						'params' => array('id' => 'f_list_id', 'prod'),
						'method' => 'both',
						'reset_progress' => true,
						'allowed_states' => array('load'));
			case 'form':
				return array('function' => 'pay_apps_form',
						'method' => 'get');
			case 'validate':
				return array('function' => 'pay_apps_validate',
						'params' => array('document', 'name', 'lob', 'rut', 'address', 'region', 'communes', 'contact', 'email', 'prod', 'payment_method', 'platform'),
						'method' => 'post',
						'allowed_states' => array('validate', 'form'));
			case 'success':
				return array('function' => 'pay_apps_success',
						'method' => 'get');
			case 'error':
				return array('function' => 'pay_apps_error',
						'method' => 'get');
			case 'disabled':
				return array('function' => 'payment_disabled',
						'method' => 'get');
			case 'webpay':
				return array('function' => 'pay_apps_webpay',
						'method' => 'get');
		}
	}

	function pay_apps_load($id, $prod) {
		global $BCONF;
		bLogger::logDebug(__METHOD__,  "pay_apps_load id=$id, prod=$prod");

		if (!payment_enabled()) {
			bLogger::logError(__METHOD__, "Payment aplication is disabled");
			return 'disabled';
		}

		if (!empty($this->tbk['TBK_ID_SESION']) && !empty($this->tbk['TBK_ORDEN_COMPRA'])) {
			bLogger::logDebug(__METHOD__, "Entering in payment aplication from fail of tbk");

			$payment_id = $this->tbk['TBK_ORDEN_COMPRA'];
			$data = $this->get_error_status($payment_id, $ftype);
			bLogger::logDebug(__METHOD__, "get_error_status data: ".print_r($data, true));
			if (is_null($data))
				return 'error';

			$this->prod = $data->product_id;
			$this->list_id = $data->list_id;
			$id = $this->list_id;
			$prod = $this->prod;
		}

		//All other forms need ID and PROD
		if (!$id || !$prod) {
			$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE","ERROR_ID_PROD_DOESNT_EXIST");
			return 'error';
		}

		$result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "id:$id");
		if (empty($result)) {
			bLogger::logDebug(__METHOD__, "Ad $id not found");
			$this->set_error_msj("ERROR_AD_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_PROD_DOESNT_EXIST");
			return 'error';
		}

		if( empty($result['email'] ) || empty($result['user_id']) || empty($result['name'])){
			bLogger::logDebug(__METHOD__, "Necesary data missing on Asearch for $id ");
			$this->set_error_msj("ERROR_AD_INFO_DOES_NOT_EXISTS", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_ID_INFO_DOESNT_EXIST");
			return 'error';
		}

		$this->company_buyer = ($result['company_ad'] == 1)?'COMPANY':'PRIVATE';
		$this->email_buyer = $result['email'];
		$this->user_id_buyer = $result['user_id'];
		$this->buyer_name = $result['name'];
		$this->category = $result['category'];
		$this->subject = $result['subject'];
		$this->ad_id = $result['ad_id'];
		if(!empty($id)) {
			$this->list_id = $id;
		}
		if(!empty($prod)) {
			$this->prod = $prod;
		}


		$product_allowed = bconf_get($BCONF, "*.pay_apps.products_allowed.".$this->prod);
		if($product_allowed != 1){
			$this->set_error_msj("ERROR_PRODUCT_NOT_ALLOWED", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_PRODUCT_CANT_BE_USED_BY_THIS_APP");
			return 'error';
		}


		$this->has_loaded = true;
		bLogger::logDebug(__METHOD__, "populating ad");
		$this->ad = $this->populate_form_data($result);
		return 'form';	
	}
	
	function pay_apps_form() {
		global $BCONF;
		bLogger::logDebug(__METHOD__,  "Loading Form");
		if ($this->has_loaded === false)
			return 'load';

		if (!$this->list_id || !$this->prod){
			$this->set_error_msj("ERROR_SESSION_EXPIRED_PAGE_TITLE", "ERROR_SESSION_EXPIRED_OOPS", "ERROR_SESSION_EXPIRED");
			return 'error';
		}

		if (!empty($this->tbk['TBK_ORDEN_COMPRA'])) {
			$data['show_error_message'] = str_replace("#order_id#",$this->tbk['TBK_ORDEN_COMPRA'], $this->get_transaction_fail_html_code());
			$data['error_message_id'] = 'ERROR_TRANSACTION_FAIL_MESSAGE';
			$data['error'] = 'TBK_ORDEN_COMPRA_NOT_EMPTY';
			$data['show_error_title'] =  "ERROR_SESSION_EXPIRED_OOPS";
			$data['error_order_id'] =  $this->tbk['TBK_ORDEN_COMPRA'];
			$data['err_in_payment'] =  "1";
			unset($this->tbk);
		}
		$data['no_header'] = 1;
		$data['no_footer'] = 1;
		$data['platform'] = 'android';
		$this->headstyles = bconf_get($BCONF, "*.pay_apps.headstyles");
		$this->headscripts = bconf_get($BCONF, "*.pay_apps.headscripts");
		$this->display_layout('pay_apps/pay_apps_form.html', $data, null, null, 'responsive_base');
	}
	
	function pay_apps_check_product_on_aps($prod,$ad_id){
		if($prod == 8 || $prod == 2) {
			$transaction = new bTransaction;
			$transaction->add_data('ad_id', $ad_id);
			if($prod == 8){
				$transaction->add_data('counter', bconf_get($BCONF, '*.payment.product_specific.daily_bump.iterations'));
				$transaction->add_data('daily_bump', '1');
			}
			else{
				$transaction->add_data('counter', bconf_get($BCONF, '*.payment.product_specific.weekly_bump.iterations'));
			}
			$transaction->add_data('batch', '1');
			$product_reply = $transaction->validate_command('weekly_bump_ad');
			if ($transaction->has_error(true)) {
				return false;
			}
		}
		return true;
	}
	
	function pay_apps_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email, $prod = 0, $payment_method, $platform) {
		global $BCONF;
		$this->validated = true;
		if(!$this->pay_apps_check_product_on_aps($this->prod,$this->ad_id)){
			$this->set_error_msj("ERROR_PROD_INVALID_FOR_AD", "ERROR_TRANSACTION_CANT_BE_DONE", "ERROR_PROD_INVALID_FOR_AD");
			return 'error';
		}
		return $this->common_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email, $prod, $payment_method, $platform);
	}

	function pay_apps_webpay() {
		if(!$this->validated)
			return 'form';
		$redirect_data['exito'] = bconf_get($BCONF, "*.common.base_url.secure")."/pay_apps_success";
		$redirect_data['fracaso'] = bconf_get($BCONF, "*.common.base_url.secure")."/pay_apps";
		return $this->webpay_payment(null, null, $redirect_data);
	}

	/*GENERIC ERROR OF THIS FLOW */
	function pay_apps_error() {
		global $BCONF;

		if (empty($this->error_title))
			$data['error_title']= 'ERROR_TRANSACTION_FAIL_TITLE';

		$data['error'] = $this->error_message;

		if(empty($this->error_message_id)){
			$this->error_message_id = $this->error_message;
		}
		$data['show_error_message'] = str_replace("#order_id#","", $this->get_transaction_fail_html_code());
		//this must be corrected when errors are rectified XITI xiti
		$data['appl'] = "pagos";
		$data['err_in_payment'] =  "1";
		$data['no_footer'] = "1";
		$data['no_header'] = "1";
		$this->headstyles = bconf_get($BCONF, "*.pay_apps.headstyles");
		$this->display_layout('pay_apps/pay_apps_error.html', $data, null, null, 'responsive_base');
		return 'FINISH';
	}

}
$pay_apps = new blocket_pay_apps();
$pay_apps->run_fsm();
?>
