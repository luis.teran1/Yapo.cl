<?php

require_once('autoload_lib.php');
require_once('init.php');

openlog("BILL_INVOICE", LOG_ODELAY, LOG_LOCAL0);

class blocket_get_bill extends blocket_application {
	var $bill_number;
	var $email;
	var $ad_id;
	var $passwd;
	var $doc_type;

	var $headscripts;
	var $headstyles;

	function blocket_get_bill() {
		global $BCONF;

		syslog(LOG_INFO, log_string() . "Calling get_bill application");

		$this->headscripts = bconf_get($BCONF, "*.get_bill.headscripts");
		$this->headstyles = bconf_get($BCONF, "*.get_bill.headstyles");
		$this->doc_type = "bill";

		$this->init('load', 1);
	}

        function get_state($statename) {
                switch($statename) {
			case 'load':
				return array('function' => 'get_bill_load',
						'params' => array('bill_number', 'email', 'p' => 'f_passwd', 'doc_type'),
						'method' => 'get',
						'allowed_states' => array('load', 'validate', 'error', 'validate_link'));
			case 'form':
				return array('function' => 'get_bill_form',
						'method' => 'get',
						'allowed_states' => array('form', 'validate', 'error'));
 			case 'validate':
 				return array('function' => 'get_bill_validate',
 						'params' => array('bill_number', 'email', 'doc_type'),
 						'method' => 'both',
 						'allowed_states' => array('validate', 'form', 'error'));
			case 'validate_link':
 				return array('function' => 'get_bill_validate_link',
 						'method' => 'get',
 						'allowed_states' => array('validate_link', 'validate', 'form', 'error', 'download_page'));
			case 'download_page':
 				return array('function' => 'get_bill_download_page',
						'params' => array('bill_number', 'email', 'p' => 'f_passwd', 'doc_type'),
 						'method' => 'get',
 						'allowed_states' => array('download_page', 'validate_link'));
			case 'error':
				return array('function' => 'get_bill_error',
 						'method' => 'get',
						'allowed_states' => array('error'));
        	}
	}

	function get_bill_load($bill_number, $email, $passwd, $doc_type = 'bill') {
		global $BCONF;

		$this->bill_number = $bill_number;
		$this->email = $email;

		syslog(LOG_INFO, log_string() . "get_bill: Loading data for bill_number: $bill_number and email: $email");

		/*
		If the required params exist in the link, just validate !
		- The link format is:
			<domain>/consultaboleta?bill_number=<(int)doc num>&p=<sha1(email+doc num)>&doc_type=<(invoice|bill)>
		*/
		if ($bill_number != '' and $passwd != '') {
			$this->passwd = $passwd;
			$this->doc_type = $doc_type;
			return 'download_page';
		}
		return 'form';
	}

 	function get_bill_form() {
		global $BCONF;

		$data = array();
		$data['headscripts'] = bconf_get($BCONF, "*.get_bill.headscripts");
		$data['headstyles'] = $this->headstyles;
		$data['doc_type'] = $this->doc_type;

		$this->application_name = 'consultaboleta';
		$this->display_layout('payment/get_bill_form.html', $data, null, null, "html5_base");
	}

	function get_bill_validate($bill_number, $email, $doc_type) {
		global $BCONF;

		if ($bill_number != '')
			$this->bill_number = $bill_number;
		if ($email != '')
			$this->email = $email;
		if ($doc_type != '')
			$this->doc_type = $doc_type;

		syslog(LOG_INFO, log_string() . "get_bill: Validating data for bill_number: $bill_number and email: $email");

		$transaction = new bTransaction();
		$transaction->add_data('email', $this->email);
		$transaction->add_data('doc_num', $this->bill_number);

		/* Always is bill when it comes from the form */
		$transaction->add_data('doc_type', $doc_type);

		$reply = $transaction->send_command('purchase_detail_get');

		if($transaction->has_error()) {
			syslog(LOG_ERR, log_string() . "get_bill: Error ".var_export($transaction->get_errors(), true));
			$this->doc_type = $doc_type;
			$this->set_errors($transaction->get_errors());
			return 'form';
		}

		$this->external_doc_id = $reply["external_doc_id"];
		syslog(LOG_INFO, log_string() . "get_bill: External doc id found: ".$this->external_doc_id);

		/* Authenticate with Nubox */
		if ($this->auth() !== true) {
			return 'error';
		}

		/* Get pdf */
		if ($this->get_pdf() !== true) {
			return 'error';
		}

		exit();
	}

	function get_bill_download_page() {
		$bill_params = '?bill_number='.$this->bill_number.'&p='.$this->passwd.'&doc_type='.$this->doc_type;

		$data = array(
			'bill_number' => $this->bill_number,
			'passwd' => $this->passwd,
			'doc_type' => $this->doc_type,
			'headstyles' => bconf_get($BCONF, '*.payment.headstyles'),
			'bill_params' => $bill_params,
			'page_title' => bconf_get($BCONF, '*.language.GET_BILL_DOWNLOAD_PAGE_TITLE.es'),
			'headscripts' => bconf_get($BCONF, "*.get_bill.headscripts")
		);

		$this->application_name = 'consultaboleta';
		$this->display_layout('payment/get_bill_download.html', $data, null, null, "html5_base");
	}

	function get_bill_validate_link() {
		$transaction = new bTransaction();
		$transaction->add_data('doc_num', $this->bill_number);
		$transaction->add_data('doc_type', $this->doc_type);

		$reply = $transaction->send_command('purchase_detail_get');

		if($transaction->has_error()) {
			syslog(LOG_ERR, log_string() . "get_bill link used to get a bill/invoice is invalid. bill_number:".$this->bill_number.", not found");
			header('HTTP/1.0 404 Not Found');
			$this->display_layout('common/not_found.html', array(), null, null, "html5_base");
			exit();
		}

		$this->external_doc_id = $reply["external_doc_id"];
		$p = sha1($reply['email'].$this->bill_number);

		if ($this->passwd != $p) {
			syslog(LOG_ERR, log_string() . "get_bill link used to get a bill/invoice has an incorrect hash. bill_number:".$this->bill_number);
			header('HTTP/1.0 404 Not Found');
			$this->display_layout('common/not_found.html', null, null, null, "html5_base");
			exit();
		}
		$this->email = $reply['email'];

		/* Authenticate with Nubox */
		if ($this->auth() !== true) {
			return 'error';
		}

		/* Get the PDF from Nubox */
		if ($this->get_pdf() !== true) {
			return 'error';
		}
		exit();
	}

	function get_bill_error() {
		$this->display_layout('payment/get_bill_3rd_party_down.html', null, null, null, "html5_base");
	}

	function auth() {
		global $BCONF;

		syslog(LOG_INFO, log_string() . "get_bill authenticating against nubox");
		/* Nubox Autenticar */
		$domain = bconf_get($BCONF, "*.payment.nubox.domain");
		$auth_url = $domain . bconf_get($BCONF, "*.payment.nubox.auth_url");
		$load_url = $domain . bconf_get($BCONF, "*.payment.nubox.load_url");

		try {
			$client = new SoapClient($auth_url);
			$password = base64_decode(bconf_get($BCONF, "*.payment.nubox.password"));
			$params = array( "rutCliente" => bconf_get($BCONF, "*.payment.nubox.client_id"),
				"rutUsuario" => bconf_get($BCONF, "*.payment.nubox.user_id"),
				"contrasena" => $password,
				"sistema" => bconf_get($BCONF, "*.payment.nubox.system"),
				"numeroSerie" => bconf_get($BCONF, "*.payment.nubox.serie"));

			/* time? */
			$auth_response = $client->Autenticar($params);

			$this->auth_token = $auth_response->AutenticarResult;

			return true;
		}
		catch (Exception $e) {
			syslog(LOG_ERR, log_string().'get_bill nubox auth exception: '.$e->getMessage());
			return false;
		}
	}

	function get_pdf() {
		/* Nubox ObtenerPDF */
		$domain = bconf_get($BCONF, "*.payment.nubox.domain");
		$load_url = $domain . bconf_get($BCONF, "*.payment.nubox.load_url");
		syslog(LOG_INFO, log_string() . "get_bill getting pdf file from nubox");

		try {
			$client = new SoapClient($load_url);
			$params = array(
			    "token" => $this->auth_token,
			    "identificadorArchivo" => $this->external_doc_id);
			$pdf = $client->ObtenerPDF($params);

			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="'.$this->bill_number.'.pdf"');

			echo $pdf->ObtenerPDFResult;
			return true;
		}
		catch(Exception $e) {
			syslog(LOG_ERR, log_string().'get_bill get_pdf exception: '.$e->getMessage());
			syslog(LOG_ERR, log_string().'get_bill get_pdf error for exteral_doc_id: '.$this->external_doc_id);
			return false;
		}
	}

	function f_email  ($val) { return strip_tags($val); }
	function f_passwd ($val) { return strip_tags($val); }
	function f_bill_number ($val) { return $val; }

	function f_doc_type ($val) {
		$dt = array('bill' => true, 'invoice' => true);
		if (isset($dt[$val]))
			return $val;
		return null;
	}
}

$get_bill= new blocket_get_bill();
$get_bill->run_fsm();
