<?php
require_once('bLogger.php');
require_once('bTransaction.php');
require_once('RedisMultiproductManager.php');
require_once('OneClickApiClient.php');
require_once('util.php');

openlog("onclick_api", LOG_ODELAY, LOG_LOCAL0);

class oneclick_app {

	private $session_prefix;
	public $cookie_name;

	public function __construct($client = NULL) {
		$prefix = bconf_get($BCONF, '*.accounts.session_redis_prefix');
		$this->session_prefix = $prefix.bconf_get($BCONF, '*.accounts.session_redis_key_prefix');
		$this->cookie_name = bconf_get($BCONF, '*.accounts.session_cookie_name');

		$roptions = array(
			'servers' => bconf_get($BCONF, "*.redis.session.host"),
			'debug' => false,
			'id_separator' => bconf_get($BCONF, "*.redis.session.id_separator"),
			'timeout' => bconf_get($BCONF, "*.redis.session.connect_timeout")
		);

		$redis_conf = bconf_get($BCONF, "*.redis.session.host.rsd1");
		$this->redis = new Redis();
		$this->redis->connect($redis_conf['name'], $redis_conf['port'], $redis_conf['timeout']);
	}

	public function unsubscribe() {
		$session_id = $_COOKIE[$this->cookie_name];
		$session_id = sanitizeVar($session_id, 'string');
		$session_email = $this->redis->hGet($this->session_prefix.$session_id, 'email');

		if (!empty($session_email)) {
			bLogger::logDebug(__METHOD__,  "email $session_email");
			$ocp_client = new OneClickApiClient();
			$result = $ocp_client->removeUser($session_email);

			if ($result) {
				$response = array( 'status' => 'ok', 'message' => 'remove success');
			}
			else {
				$response = array( 'status' => 'error', 'message' => 'remove fail');
			}
		}
		else {
			$response = array('status' => 'error', 'message' => 'session not found');
		}
		return $response;
	}

}
