<?php
/*
 Gifting products in CP
 */

use Yapo/Logger;
require_once('autoload_lib.php');

openlog("CP_PRODUCTS", LOG_ODELAY, LOG_LOCAL0);

$script_list = bconf_get($BCONF,"*.common.newad.headscripts");

use Yapo\Cpanel;

class blocket_cp_product extends blocket_application {
	var $ad_id;
	var $cmd;

	function blocket_cp_product() {
		global $BCONF;
		global $session;

		$this->add_volatile_vars(array('product_type'));
		$this->init('load', bconf_get($BCONF, "*.common.session.instances.ai"));
		$this->headstyles = bconf_get($BCONF,"*.common.newad.headstyles");
		$this->application_name = 'cp_product';
	}

	function get_state($statename) {
		switch($statename) {
		case 'load':
			return array('function' => 'cp_prod_load',
				     'params' => array('id' => 'f_id', 'cmd', 'product_type' => 'f_prod_type'),
				     'method' => 'get',
				     'allowed_states' => array('load', 'prod_applied'));
		case 'prod_applied':
			return array('function' => 'cp_prod_applied',
				     'allowed_states' => array('prod_applied'),
				     'method' => 'get');
		}
	}

	function cp_prod_load($id, $cmd, $product_type = NULL) {
		global $BCONF;
		$this->ad_id = $id;
		Logger::logDebug(__METHOD__, "command  = " . var_export($cmd, true));
		if (!Cpanel::checkLogin() || !Cpanel::hasPriv("adminad.bump")) {
			echo lang('ERROR_NO_PERMISSION');
			return 'FINISH';
		}

		$one_step_cmd = bconf_get($GLOBALS['BCONF'], "controlpanel.services_one_step_cmd");
		if (!empty($one_step_cmd)) {
			$one_step_cmd = explode(',',$one_step_cmd);
		}
		if (in_array($cmd, $one_step_cmd)) {
			return $this->cp_apply_product($cmd);
		} else if ($cmd == 'label') {
			$this->cmd = $cmd;
			if (is_null($product_type)) {
				return $this->display_action_page($cmd);
			} else {
				return $this->cp_apply_product($cmd, $product_type);
			}
		}
	}

	function display_action_page($cmd = null) {
		global $BCONF, $script_list;
		$data = array();
		$data['headscripts'] = $script_list;
		$data['page_title'] = lang('PAGE_ACTION_TITLE');
		$data['page_name'] = lang('PAGE_ACTION_NAME');
		$data['ad_id'] = $this->ad_id;
		$data['cmd'] = $this->cmd;
		$this->display_layout('controlpanel/product_options.html', $data, null, null, 'html5_base');
	}

	/* User is trying to bump the ad. */
	function cp_apply_product( $product = 'bump' , $product_type = null ) {
		global $session;
		global $BCONF;
		$code_prod = 0;
		$code_uprod = 0;
		$command = 'bump_ad';
		$transaction = new bTransaction();
		if (!Cpanel::checkLogin() || !Cpanel::hasPriv("adminad.bump")) {
			echo lang('ERROR_NO_PERMISSION');
			return 'FINISH';
		}

		// if the product exists, get the product code
		$prod_codes = bconf_get($BCONF, "*.payment.product_code.".$product);
		if (!empty($prod_codes)) {
			$prod_codes= explode(',',$prod_codes);
			$code_prod = $prod_codes[0];
			if (sizeof($prod_codes) > 1) {
				$code_uprod = $prod_codes[1];
			}
			$command = bconf_get($BCONF,"*.upselling_settings.command.".$code_prod.".cmd");
			//load the default params
			$product_config = bconf_get($GLOBALS['BCONF'], "*.upselling_settings.command.{$code_prod}");
			if (isset($product_config["default_params"])) {
				$def_params = $product_config["default_params"];
				foreach ($def_params as $def_param) {
					$def_param_values=explode(':', $def_param["value"]);
					if ($def_param_values[0] != "commit" && $def_param_values[0] != "batch") {
						$transaction->add_data( $def_param_values[0], $def_param_values[1]);
					}
				}
			}
			//if the product applies a label, get the label
			$apply_label = bconf_get($GLOBALS['BCONF'], "controlpanel.services_apply_label");
			if (!empty($apply_label)) {
				$apply_label = explode(',', $apply_label);
				if (in_array($product, $apply_label)) {
					$transaction->add_data('label_type', $product_type);
				}
			}
		} else {
			$command = 'bump_ad';
			$code_prod = 9;
		}

		$transaction->add_client_info();
		$transaction->auth_type = 'admin';
		$transaction->add_data('ad_id', $this->ad_id);
		$reply = $transaction->send_admin_command($command);
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
		}
		else {
			if ($code_prod != 0) {
				// Get ad info
				$trans_load = new bTransaction();
				$trans_load->add_data('ad_id',$this->ad_id);
				$load_reply = $trans_load->send_command('get_ad');
				if ($trans_load->has_error(true)) {
					$this->set_errors($trans_load->get_errors());
				}
				else {
					// Clean the product from the user cart
					$this->rmm = new RedisMultiproductManager();
					if ($code_uprod != 0)
						$this->rmm->unselect($load_reply['email'], $code_uprod, $this->ad_id, true);
					$this->rmm->unselect($load_reply['email'], $code_prod, $load_reply['list_id'], true);
				}
			}
		}
		
		return 'prod_applied';
	}

	/* Ad has been bumped. */
	function cp_prod_applied() {
		$data['page_name'] = lang('PAGE_ACTION_BUMP_NAME');
		$data['page_title'] = lang('PAGE_ACTION_BUMP_TITLE');

		$this->display_layout('ai/bumped.html', $data, null, null, 'html5_base');
		return 'FINISH';
	}

	/* Filter for the ad_id. */
	function f_id($val) {
		if (preg_match('/^[0-9]+$/', $val))
			return $val;
		return NULL;
	}

	/* Filter the chosen command, returning null if invalid. */
	function f_cmd($val) {
		if (preg_match('/^(bump|gallery|gallery_1|gallery_30|addgallery|weekly|daily|label|rmv_label)$/', $val))
			return $val;
		return null;
	}

	/* Filter the product subtype, for now only "label" options are available. */
	function f_prod_type($val) {
		if (preg_match('/^(urgent|opportunity|little_use|new)$/', $val))
			return $val;
		return null;
	}

}

$cp_prod = new blocket_cp_product();
$cp_prod->run_fsm();
?>
