<?php

namespace Yapo;

$session_read_only = true;

require_once('autoload_lib.php');
require_once("init.php");
require_once("dyn_config.php");
require_once('memcachedsession.php');
require_once('util.php');
require_once('httpful.php');

use bSession;
use bTransaction;
use bResponse;
use RedisSessionManager;

/*
 * Headers to disable caching
 */
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");

/*
 * Syslog
 */
openlog("CONTROLPANEL", LOG_ODELAY, LOG_LOCAL0);

/*
 * Startup
 */
// Start the session
$session = new bSession();

// Transaction handler
$transaction = new bTransaction();

// Response
$response = new bResponse();
$response->set_bconf_application('controlpanel');

/*
 * Login control
 */
if (isset($_REQUEST['logout'])) {
    Cpanel::logout();
} elseif (isset($_REQUEST['login'])) {
    Cpanel::login($_REQUEST['username'], $_REQUEST['cpasswd']);
} elseif (isset($_SESSION['response'])) {
    $response = $_SESSION['response'];
    unset($_SESSION['response']);
    $session_read_only = false;
}

// User access
$response->add_data('login', Cpanel::checkLogin()?1:0);
if (isset($_SESSION['controlpanel']['admin']['username'])) {
    $response->add_data('login_username', $_SESSION['controlpanel']['admin']['username']);
    $response->add_data('login_date', $_SESSION['controlpanel']['admin']['login_date']);
}

/*
 * Page data
 */
$response->add_data('page_title', Bconf::get($BCONF, 'controlpanel.page_title'));

$response->fill_array('page_trail_link', $_SERVER['PHP_SELF']);
$response->fill_array('page_trail_name', Bconf::get($BCONF, 'controlpanel.page_name'));

// Load css
$response->fill_array('page_css', "/css/controlpanel.css");

// Load javascripts
$response->fill_array('page_js', "/js/jquery-1_4_2.js");
$response->fill_array('page_js', "/tjs/arrays_controlpanel.js");
$response->fill_array('page_js', "/js/common.js");
$response->fill_array('page_js', "/js/controlpanel.js");
$response->fill_array('page_js', "/js/right_click_menu.js");
$response->fill_array('page_js', "/js/events.js");

/*
 * Load modules
 */
if (Cpanel::checkLogin()) {
    if (Bconf::get($BCONF, "controlpanel.show_remaining_queue")) {
        $trans = new bTransaction();
        $reply = $trans->send_admin_command('adqueues');
        $response->add_data("remaining_queue", $reply['total']['new']);
    }

    $modules = array();
    $module_order = array();

    // Load modules from BCONF
    foreach (Bconf::get($BCONF, 'controlpanel.modules') as $module_id => $module) {
        $priv = Bconf::get($BCONF, "controlpanel.modules.$module_id.priv");
        if (!$priv) {
            $priv = $module_id;
        }
        $response->fill_array('module_privs_list', $priv);
        $response->fill_array('module_privs_name', Bconf::get($BCONF, "controlpanel.modules.$module_id.name"));
        if (Cpanel::hasPriv($priv)) {
            $module_file = "cpanel_module_${module_id}.php";
            $class = "Yapo\CpanelModule{$module_id}";
            if (file_exists('../include/'.$module_file) && require_once($module_file)) {
                $class = "cpanel_module_{$module_id}";
            }
            $modules[$module_id] = new $class();
            $module_order[$module['order']] = $module_id;
        }
    }

    ksort($module_order, SORT_NUMERIC);

    // Load module menu
    if (empty($modules) || !isset($_REQUEST['m']) || !isset($modules[$_REQUEST['m']])) {
        unset($_SESSION['controlpanel']['top_menu_list']);

        foreach ($module_order as $module_id) {
            $module = $modules[$module_id];
            $response->fill_array('module_name_list', $module->getName());
            $response->fill_array('module_id_list', $module_id);
            $menus = $module->getMenu();
            foreach ($menus as $menu => $name) {
                $priv = Bconf::get($BCONF, "controlpanel.modules.$module_id.$menu.priv");
                if ($priv == "" || Cpanel::hasPriv($priv)) {
                    $response->fill_array('module_menu_module_list', $module_id);
                    $response->fill_array('module_menu_id_list', $menu);
                    $response->fill_array('module_menu_name_list', $name);
                    $response->fill_array('module_menu_name_suffix_list', $module->getMenuNameSuffix($menu));
                    $module_array = &$_SESSION['controlpanel']['top_menu_list'][$module_id];
                    if (sizeof(@$module_array['menu_id']) < sizeof($menus)) {
                        if (empty($module_array['module_name'])) {
                            $module_array['module_name'][] = $module->getName();
                        }
                        $module_array['menu_id'][] = $menu;
                        $module_array['menu_name'][] = $name;
                    }
                }
            }
        }
        $session_read_only = false;
    }

    //Top menu
    if (isset($_SESSION['controlpanel']['top_menu_list'])) {
        $response->add_extended_array('top_menu_list', $_SESSION['controlpanel']['top_menu_list']);
    }

    // Show selected
    if (!empty($modules) && isset($_REQUEST['m']) && isset($modules[$_REQUEST['m']])) {
        $module = null;
        $contents = null;

        $module = $modules[$_REQUEST['m']];

        // Module name
        $response->add_data('module_name', $module->getName());
        $response->add_data('module_id', $_REQUEST['m']);

        // Page trail
        $response->fill_array('page_trail_link', htmlspecialchars($_SERVER['PHP_SELF']."?m=".$_REQUEST['m']));
        $response->fill_array('page_trail_name', $module->getName());

        if (isset($_REQUEST['a'])) {
            $priv = Bconf::get($BCONF, "controlpanel.modules.{$_REQUEST['m']}.{$_REQUEST['a']}.priv");
            $logTo = Bconf::get($BCONF, "controlpanel.modules.{$_REQUEST['m']}.log_to");
            if (!empty($logTo)) {
                openlog($logTo, LOG_ODELAY, LOG_LOCAL0);
            }

            if ($priv != "" && !Cpanel::hasPriv($priv)) {
                $response->add_error('error_message', 'ERROR_NO_PERMISSION');
            } elseif ($priv == "Partner.editPartner" && (!Cpanel::hasPriv("Partner.advancedPartner") || !Cpanel::hasPriv("Partners.addPartner"))){
                $response->add_error('error_message', 'ERROR_NO_PERMISSION');
			} else {
                // Page trail
                $module_menus = $module->getMenu();
                if (isset($module_menus[$_REQUEST['a']])) {
                    $response->fill_array(
                        'page_trail_link',
                        htmlspecialchars($_SERVER['PHP_SELF']."?m=".$_REQUEST['m']."&a=".$_REQUEST['a'])
                    );
                    $response->fill_array('page_trail_name', $module_menus[$_REQUEST['a']]);
                    $response->add_data('page_name', $module_menus[$_REQUEST['a']]);
                } elseif ($module->getMenuName($_REQUEST['a'])) {
                    $response->fill_array(
                        'page_trail_link',
                        htmlspecialchars($_SERVER['PHP_SELF']."?m=".$_REQUEST['m']."&a=".$_REQUEST['a'])
                    );
                    $response->fill_array('page_trail_name', $module->getMenuName($_REQUEST['a']));
                    $response->add_data('page_name', $module->getMenuName($_REQUEST['a']));
                }

                // Get contents
                $module->main($_REQUEST['a']);
            }
        } else {
            $module->main();
        }
    }
}

$response->call_template("controlpanel/controlpanel_top.html");
if (isset($response->data['template'])) {
    $response->call_template($response->data['template']);
} elseif (isset($module) && is_object($module)) {
    $module->displayResultSet();
}
$response->call_template("controlpanel/controlpanel_bottom.html");

unset($_SESSION['response']);
