<?php

namespace Yapo;

use Yapo\Cpanel;

class CpanelModuleHomeLinks extends CpanelModule
{
    public function __construct($homeLinksService = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.HomeLinks'));
        $this->homeLinksService = isset($homeLinksService)? $homeLinksService : new HomeLinksService();
        // Init this controlpanel module
        parent::init();
    }

    public function show()
    {
        $this->displayResults('controlpanel/home_links/main.html');
    }

    public function search()
    {
        $groups = $this->homeLinksService->allGroups();
        foreach ($groups as $k => $v) {
            $this->response->fill_array('groups', utf8_decode($v->name));
        }
        if (!empty($_REQUEST['group'])) {
            $singleGroup = $this->homeLinksService->singleGroup($_REQUEST['group']);
            $this->response->add_data('selected_group', $_REQUEST['group']);
            if (isset($singleGroup->status) && $singleGroup->status == 'OK') {
                foreach ($singleGroup->data as $k => $v) {
                    $this->response->fill_array('data_id', $v->id);
                    $this->response->fill_array('data_group', $v->group);
                    $this->response->fill_array('data_text', utf8_decode($v->text));
                    $this->response->fill_array('data_href', $v->href);
                    $this->response->fill_array('data_score', $v->score);
                }
            }
        }
        $this->displayResults('controlpanel/home_links/main.html');
    }

    private function validate($data)
    {
        $regexp = '/^[a-z0-9 ]+$/i';
        $resultado = preg_match($regexp, $data['group']);
        if (!$resultado) {
            $this->response->add_data('err_group', 'HOME_LINKS_GROUP_ERROR');
            return false;
        }

        return true;
    }

    private function populateOld($data)
    {
        foreach ($data as $k => $v) {
            $this->response->add_data('old_'.$k, $v);
        }
    }

    public function create()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $data = array(
                'group' => sanitizeVar($_POST['group'], 'string'),
                'text' => sanitizeVar($_POST['text'], 'string'),
                'href' => sanitizeVar($_POST['href'], 'string'),
                'score' => floatval(sanitizeVar($_POST['score'], 'int'))
            );
            if ($this->validate($data)) {
                $creation = $this->homeLinksService->create($data);
                if (isset($creation->status) && $creation->status == 'Created') {
                    $this->response->add_data('message', 'HOME_LINKS_CREATED');
                    $this->response->add_data('msg_style', 'success');
                } else {
                    $this->populateOld($data);
                    $this->response->add_data('message', 'HOME_LINKS_ERRORS_ON_CREATE');
                    $this->response->add_data('msg_style', 'error');
                }
            } else {
                $this->populateOld($data);
            }
        }
        $this->displayResults('controlpanel/home_links/main.html');
    }

    public function edit()
    {
        if ($_SERVER['REQUEST_METHOD'] != 'POST') {
            header('Location: /controlpanel?m=HomeLinks&a=search');
            return;
        }
        $data = array(
                'id' => intval(sanitizeVar($_POST['id'], 'int')),
                'group' => sanitizeVar($_POST['group'], 'string'),
                'text' => sanitizeVar($_POST['text'], 'string'),
                'href' => sanitizeVar($_POST['href'], 'string'),
                'score' => floatval(sanitizeVar($_POST['score'], 'int'))
        );
        $this->populateOld($data);

        if (isset($_POST['loaded'])) {
            $edition = $this->homeLinksService->edit($data);
            if (isset($edition->status) && $edition->status == 'Updated') {
                $this->response->add_data('message', 'HOME_LINKS_EDITED');
                $this->response->add_data('msg_style', 'success');
            } else {
                $this->response->add_data('message', 'HOME_LINKS_ERRORS_ON_EDIT');
                $this->response->add_data('msg_style', 'error');
            }
        }

        $this->displayResults('controlpanel/home_links/main.html');
    }

    public function delete()
    {
        if (!empty($_REQUEST['group']) && !empty($_REQUEST['id'])) {
            $deletion = $this->homeLinksService->delete($_REQUEST['group'], $_REQUEST['id']);
            if ($deletion = null || isset($deletion->ErrorMessage)) {
                $this->response->add_data('err_delete', 'HOME_LINKS_DELETE_FAILURE');
                $this->displayResults('controlpanel/home_links/main.html');
                return;
            }
        }
        header('Location: /controlpanel?m=HomeLinks&a=search&group='.$_REQUEST['group']);
    }

    public function serviceUnavailable()
    {
        $this->displayResults('controlpanel/home_links/service_unavailable.html');
    }


    public function export()
    {
        header('Content-Type: application/json');
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=export.json');
        echo $this->homeLinksService->export();

        die();
    }

    public function import()
    {
        $uploadfile = $_FILES['fileToUpload']['tmp_name'];
        $message = $this->homeLinksService->import($uploadfile);
        $data = yapo_json_decode($message);
        if (!empty($data)) {
            foreach ($data as $k => $v) {
                $this->response->fill_array('import_group', utf8_decode($v->Name));
                $this->response->fill_array('import_counter', $v->Counter);
            }
        } else {
            $this->response->add_data("err_fileToUpload", "HOME_LINKS_IMPORT_INVALID_FILE");
        }
        $this->displayResults('controlpanel/home_links/main.html');
    }

    public function main($function = null)
    {
        $this->response->add_data('priv_search', Cpanel::hasPriv('HomeLinks.search')? '1': '0');
        $this->response->add_data('priv_create', Cpanel::hasPriv('HomeLinks.create')? '1': '0');
        $this->response->add_data('priv_edit', Cpanel::hasPriv('HomeLinks.edit')? '1': '0');
        $this->response->add_data('priv_delete', Cpanel::hasPriv('HomeLinks.delete')? '1': '0');
        $this->response->add_data('priv_import', Cpanel::hasPriv('HomeLinks.import')? '1': '0');
        $this->response->add_data('priv_export', Cpanel::hasPriv('HomeLinks.export')? '1': '0');
        $this->response->add_data('page_name', 'HomeLinks');
        $this->response->fill_array('page_js', '/js/HomeLinks.js');
        if (!$this->homeLinksService->checkService()) {
            $this->serviceUnavailable();
            return;
        }
        if ($function != null) {
            $this->runMethod($function);
        } else {
            $this->show();
        }
    }
}
