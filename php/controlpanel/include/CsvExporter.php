<?php

class CsvExporter
{
    private $filename;
    private $headers;
    private $data;
    private $separator;

    public function __construct($filename = null, $separator = ';')
    {
        if (empty($filename)) {
            $filename = "report-".date("YmdHis").".csv";
        }
        $this->filename = $filename;
        $this->separator = $separator;
        $this->headers = array();
        $this->data = array();
    }

    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function run($inline = false)
    {
        $response = new bResponse();

        $response->add_data('separator', $this->separator);

        foreach ($this->headers as $head) {
            $response->fill_array('headers', $head);
        }

        $response->add_data('result_set', count($this->data));

        foreach ($this->data as $key => $result) {
            foreach ($result as $val) {
                $response->fill_array("data_{$key}", $val);
            }
        }

        if ($inline) {
            header('Content-type: text/txt');
            header('Content-Disposition: inline; filename="'.$this->filename.'"');
            header('Cache-Control: private, max-age=0, must-revalidate');
            header('Pragma: public');
        } else {
            header('Content-type: text/csv');
            header('Content-Disposition: attachment; filename="'.$this->filename.'"');
        }

        call_template(
            $response->data,
            $response->extended_array,
            $GLOBALS['BCONF']['*'],
            "controlpanel/generate.csv",
            default_template_options()
        );
        exit(0);
    }
}
