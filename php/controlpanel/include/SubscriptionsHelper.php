<?php
namespace Yapo;

use bTransaction;
use OneClickApiClient;

class SubscriptionsHelper
{
    public $CreditsService;

    const REGISTERED = 'registrado';
    const NOT_REGISTERED = 'no registrado';
    const ACCOUNT_NOT_FOUND = 'ACCOUNT_NOT_FOUND';

    public function __construct($trans = null, $ocpClient = null)
    {
        $this->trans = isset($trans) ? $trans : new bTransaction();
        $this->ocpClient = isset($ocpClient) ? $ocpClient : new OneClickApiClient();
    }

    public function populateAccount($email, &$response)
    {
        $transaction = $this->trans->reset();
        $transaction->add_data('email', $email);
        $reply = $transaction->send_command('get_account');
        if ($transaction->has_error(true)) {
            if (isset($reply['error']) && $reply['error'] == self::ACCOUNT_NOT_FOUND) {
                $response->add_data("err_account_not_found", $reply['error']);
            } else {
                foreach ($transaction->get_errors() as $param => $code) {
                    $response->add_data("err_$param", lang($code));
                };
            }
        } else {
            if (isset($reply['account_status']) && !empty($reply['account_status'])  && !empty($reply['email'])) {
                $user_oneclick= $this->ocpClient->isRegistered($email);
                $reply["oneclick"] = ($user_oneclick) ? self::REGISTERED : self::NOT_REGISTERED;
                $response->add_extended_array(array('account'=>$reply));
            }
        }
    }

    public function populateSubscriptionList($allSubs, &$response)
    {
        $dataToTmpl =  array();
        foreach ($allSubs as $sub) {
            $id= $sub->SubscriptionID;
            $dataToTmpl[$id]['Email'] = $sub->Subscriber->Email;
            $dataToTmpl[$id]['Admin'] = $sub->Admin->Email;
            $dataToTmpl[$id]['Name'] = $sub->SubscriptionDetails[0]->Name;
            $dataToTmpl[$id]['Price'] = $sub->SubscriptionDetails[0]->Price;
            $dataToTmpl[$id]['Product'] = $sub->SubscriptionDetails[0]->ProductID;
            $dataToTmpl[$id]['CreationDate'] = str_replace("T", " ", $sub->SubscriptionDate);
            $dataToTmpl[$id]['NextTryDate'] = str_replace("T", " ", $sub->NextRetry);
            $dataToTmpl[$id]['Status'] = $sub->Status;
            $dataToTmpl[$id]['LastTryStatus'] = $sub->LastPayment->Status;
            $dataToTmpl[$id]['LastTryDate'] = str_replace("T", " ", $sub->LastPayment->PaymentDate);
        }

        $response->add_extended_array("subscriptions", $dataToTmpl);
    }

    /**
     * Gets report data to parse it into a matrix to be returned in certain format
     * @param $allSubs object with payment shedule report data provided by its MS
     * @param &$response CpanelModuleSubscriptions class object
     * @return do not have a return but appends resultant matrix to response object
     */
    public function populateReportList($allSubs, &$response)
    {
        $dataToTmpl =  array();
        foreach ($allSubs as $sub) {
            $id = $sub->subscriptionID;
            $dataToTmpl[$id]['clientEmail'] = $sub->clientEmail;
            $dataToTmpl[$id]['adminEmail'] = $sub->adminEmail;
            $dataToTmpl[$id]['product'] = $sub->product;
            $dataToTmpl[$id]['lastFee'] = $sub->lastFee;
            $dataToTmpl[$id]['subscriptionDate'] = $sub->subscriptionDate;
            $dataToTmpl[$id]['lastPaymentDate'] = $sub->lastPaymentDate;
            $dataToTmpl[$id]['lastPaymentStatus'] = $sub->lastPaymentStatus;
            $dataToTmpl[$id]['subscriptionStatus'] = $sub->subscriptionStatus;
        }
        $response->add_extended_array("report", $dataToTmpl);
    }
}
