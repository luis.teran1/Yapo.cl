<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;
use Yapo\ClaimProductService;
use DateTime;

class CpanelModuleClaimProduct extends CpanelModule
{
    private $trans;
    private $redis;

    public function __construct($trans = null, $redis = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.ClaimProduct'));
        $this->trans = isset($trans)? $trans : new bTransaction();
        // Init this controlpanel module
        parent::init();
    }

    private function isAssociativeArray($array) {
        return array_keys($array) != range(0, count($array) - 1);
    }

    private function verifyEmail($email)
    {
        if (isset($email)) {
            $regex = Bconf::get($BCONF, "*.common.accounts.mobile.form_validation.email.regexp");
            if (empty($regex)) {
                $regex = "^[\w_+-]+(\.[\w_+-]+)*\.?@([\w_+-]+\.)+[\w]{2,4}$";
            }
            if (!preg_match("/{$regex}/", $email)) {
                $this->response->add_data("err_email", "ERROR_EMAIL_INVALID");
                return false;
            }

            return true;
        }
    }

    private function checkService()
    {
       $ClaimProductService = new ClaimProductService();
       $response = $ClaimProductService->checkService();
       if ($response == null) {
           $this->response->add_data("err_healthcheck", "CLAIM_PRODUCT_UNAVAILABLE");
       }
    }

    private function formatKeys($data){
        foreach ($data as $key => $value) {
            if(!array_key_exists("ClaimedAt", $value)){
                $value->ClaimedAt = "";
                $data[$key] = $value;
            }
            $value->CreatedAt = str_replace("T"," ", $value->CreatedAt);
            $value->ClaimedAt = str_replace("T"," ", $value->ClaimedAt);
            $data[$key] = $value;
        }
        return $data;
    }

    private function parseClaims($data)
    { 
        $data = $this->formatKeys($data);
        foreach ($data as $entry) {
            foreach ($entry as $key => $value) {
                $outputHeaders = Bconf::get($BCONF, "*.controlpanel.modules.ClaimProduct.search.output_headers");
                $tableHeaders = explode(",", $outputHeaders);
                $this->setClaim("", $key, $value, "claim_", $tableHeaders);
            }
        }
    }

    private function setClaim($origin, $key, $value, $prefix, $allowedArr)
    {
        if (in_array($key, $allowedArr) && (gettype($value) == "object")) {
            foreach ($value as $k => $v) {
                $this->setClaim($key, $k, $v, $origin.$key, $allowedArr);
            }
        } else if (is_array($value)) {
            if ($this->isAssociativeArray($value)){
                foreach ($value as $k => $v) {
                    $this->setClaim($key, $k, $v, $origin.$key, $allowedArr);
                }
            } else {
                $this->response->fill_array(lcfirst($prefix.$key), json_encode($value));
            }
        } else {
            $item = utf8_decode($value);
            $this->response->fill_array(lcfirst($prefix.$key), $item);
        }
    }

    public function search()
    {
        $data = array();
        // If we are searching for a given email
        if (array_key_exists ("clpr-search-email", $_REQUEST) && !empty($_REQUEST['clpr-search-email'])){
            $email = $_REQUEST['clpr-search-email'];
            if ($this->verifyEmail($email)) {
                $data["email"] = $email;
                $this->response->add_data("search_email", $email);
            } else {
                $this->response->add_data("err_form", "CLAIM_PRODUCT_EMAIL_INVALID");
                $this->show(true);
                return;
            }
        } else {
            $this->show(true);
            return;
        }
        $this->getClaims($data);
    }

    public function getClaims(Array $data)
    {
        $ClaimProductService = new ClaimProductService();
        $response = $ClaimProductService->searchClaims($data);
        if (!empty($response)) {
            if (array_key_exists ("claims", $response) && !empty($response->claims)) {
                $this->parseClaims($response->claims);
            }

            if (array_key_exists ("email", $data)) {
                $this->response->add_data("search_email", $data["email"]);
            }
            $this->show(false);
        }else{
            //show error
            $this->show(true);
        }
    }

    public function edit()
    {
        if (array_key_exists ("claim_id", $_REQUEST)) {
            $claim_id = (int)$_REQUEST['claim_id'];
            $status = $_REQUEST['edit_status'];
            $changes = array();
            $changes['status'] = $status;
            $ClaimProductService = new ClaimProductService();
            $response = $ClaimProductService->editClaim($claim_id, $changes);
            if ($response == null) {
                $this->response->add_data("err_edit", "CLAIM_PRODUCT_EDIT_FAILURE");
            } else {
                $this->response->add_data("edit_success", "CLAIM_PRODUCT_EDIT_SUCCESS");
                $cp_url = Bconf::get($BCONF, "common.base_url.controlpanel");
                header('Location: '.$cp_url.'/controlpanel?m=ClaimProduct&a=search');
            }
        }
        $this->show(true);
    }

    public function show($should_load)
    {
        if ((bool)$should_load) {
            $this->getClaims(array());
        }
        $this->response->add_data("page_name", "Search");
        $this->displayResults('controlpanel/claim_product/search.html');
    }

    public function main($function = null)
    {
        $this->response->add_data("priv_search", Cpanel::hasPriv("ClaimProduct.search")? "1": "0");
        $this->response->add_data("priv_edit", Cpanel::hasPriv("ClaimProduct.edit")? "1": "0");
        $this->response->fill_array('page_js', '/js/moment.min.js');
        $this->response->fill_array('page_js', '/js/pikaday.js');
        $this->response->fill_array('page_js', '/js/ClaimProduct.js');
        $this->response->fill_array('page_css', '/css/pikaday.css');
        if ($function != null) {
            $this->checkService();
            if (array_key_exists ("err_healthcheck", $this->response->data))  {
                    $this->response->add_data("page_name", "Search");
                    $this->response->add_data('page_name_hide', '1');
                if ($function == "search") {
                    $this->displayResults('controlpanel/claim_product/search.html');
                } else {
                    $this->response->add_data("page_name", "Search");
                    $this->response->add_data('page_name_hide', '1');
                    $this->show(false);
                }
                return;
            }
            $this->runMethod($function);
        } else {
            $this->response->add_data("page_name", "Search");
            $this->show(true);
        }
    }

}
