<?php

namespace Yapo;

use RedisSessionClient;
use bImage;
use bTransaction;
use Yapo\Cpanel;
use DateTime;

class CpanelModuleStores extends CpanelModule
{
    private $trans;
    private $redisSession;
    private $arrStatus = array('enable' => 'active', 'disable' => 'admin_deactivated');

    public function __construct($trans = null, $redisSession = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Stores'));
        $this->trans = isset($trans)? $trans : new bTransaction();

        if (isset($redisSession)) {
            $this->redisSession = $redisSession;
        } else {
            $redis_options = array(
                'servers' => Bconf::get($BCONF, '*.redis.session.host'),
                'debug' => false,
                'id_separator' => Bconf::get($BCONF, "*.redis.session.id_separator"),
                'timeout' => Bconf::get($BCONF, "*.redis.session.connect_timeout"),
            );
            $this->redisSession = new RedisSessionClient($redis_options);
        }
        // Init this controlpanel module
        parent::init();
    }


    public function createStore()
    {
        if ($this->validateEmail()) {
            $this->loadInfo();

            if (isset($this->response->data["acc_account_id"])
                && !isset($this->response->data["store_found"])
                && isset($_REQUEST['product_id'])
            ) {
                $trans = $this->trans->reset();
                $trans->add_data('account_id', $this->response->data["acc_account_id"]);
                $response = $trans->send_admin_command('create_store');

                if ($trans->has_error(true)) {
                    $this->response->add_data("create_store_error", "Error al crear la tienda");
                    Logger::logError(__METHOD__, "Creating store failed");
                    foreach ($trans->get_errors() as $param => $code) {
                        $this->response->add_data("err_$param", lang($code));
                    }
                } else {
                    $days = $this->applyProduct($response["store_id"], $_REQUEST['product_id']);

                    if (isset($days)) {
                        $this->response->add_data(
                            "create_store_success",
                            "Exito:Tienda creada exitosamente por un per�odo de $days"
                        );
                        // put the store info on redis session
                        $trans = $this->trans->reset();
                        $trans->add_data('email', $this->response->data['acc_email']);
                        $info = $trans->send_command('load_store');
                        if ($info['status'] != "TRANS_OK") {
                            Logger::logError(__METHOD__, "Cannot load store info");
                        }
                    } else {
                        $this->response->add_data(
                            "create_store_error",
                            "Tienda creada, error al aplicar productos."
                        );
                    }
                }
                $this->loadInfo();
            }
        }
        $this->displayResults('controlpanel/stores/search.html');
    }

    public function extendStore()
    {
        if ($this->validateEmail()) {
            $this->loadInfo();
            if (isset($this->response->data["store_found"]) && isset($_REQUEST['product_id'])) {
                $days = $this->applyProduct($this->response->data["store_id"], $_REQUEST['product_id']);
                if ($days != null) {
                    $this->response->add_data(
                        "extend_store_success",
                        "Tienda extendida exitosamente por un per�odo de {$days}"
                    );
                } else {
                    $this->response->add_data(
                        "extend_store_error",
                        "No se pudo extender periodo de la tienda."
                    );
                }
            }
        }
        $this->displayResults('controlpanel/stores/search.html');
    }

    public function editStore()
    {
        if ($this->validateEmail()) {
            $this->loadInfo();
        }
        $this->displayResults('controlpanel/stores/search.html');
    }

    public function changeStatus()
    {
        if ($this->validateEmail()) {
            $this->loadInfo();
            if (isset($_REQUEST['status'])
                && array_key_exists($_REQUEST['status'], $this->arrStatus)
                && $this->response->data["store_status"] != $this->arrStatus[$_REQUEST['status']]
            ) {
                $trans = $this->trans->reset();
                $trans->add_data("store_id", $this->response->data["store_id"]);
                $trans->add_data("status", $this->arrStatus[$_REQUEST['status']]);
                $response = $trans->send_admin_command('update_store_status');

                if ($trans->has_error(true)) {
                    $this->response->add_data("change_status_error", "Error al cambiar de estado.");
                } else {
                    $hash = Cpanel::createProfileCacheHash($_REQUEST['search_email']);
                    $this->redisSession->hset($hash, 'store_status', $this->arrStatus[$_REQUEST['status']]);
                    $this->response->add_data("store_status", $response["store_status"]);
                    $this->response->add_data("change_status_success", "Tienda cambiada de estado.");
                }
            }
        }

        $this->displayResults('controlpanel/stores/search.html');
    }

    public function storeHistory()
    {
        if ($this->validateEmail()) {
            $this->loadInfo();
            $trans = $this->trans->reset();
            $trans->add_data('store_id', $this->response->data["store_id"]);
            $response = $trans->send_command("store_history");
            if ($trans->has_error(true)) {
                Logger::logError(__METHOD__, "Cannot fetch the store history");
            } else {
                $i = 0;
                foreach ($response['store_history'] as $num => $ad_state) {
                    $this->response->fill_array('action_type', $ad_state['action_type']);
                    $this->response->fill_array('state', $ad_state['state']);
                    $this->response->fill_array('timestamp', $ad_state['timestamp']);
                    $this->response->fill_array('admin', $ad_state['admin']);
                    $this->response->fill_array('changes', $ad_state['changes']);
                    $this->response->fill_array('colored', ++$i % 2);
                }
                $this->response->add_data('content', 'controlpanel/stores/store_history.html');
                $this->response->call_template('controlpanel/simple.html');
                exit(0);
            }
        }
    }

    private function validateEmail()
    {
        if (isset($_REQUEST['search_email'])) {
            $email = $_REQUEST['search_email'];
            $this->response->add_data("search_email", $email);
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->response->add_data("err_email", "Email no v�lido");
                return false;
            }
            $this->response->add_data("valid_email", "true");
            return true;
        }
    }

    private function loadInfo()
    {
        $email = $_REQUEST['search_email'];
        $this->response->add_data("search_email", "$email");
        $trans = $this->trans->reset();
        $trans->add_data('email', $email);
        $acc_info = $trans->send_command('get_account');

        if ($acc_info['status'] != "TRANS_OK") {
            // the account does not exist.
            $this->response->add_data("err_account", "Usuario sin cuenta.");
            Logger::logWarning(__METHOD__, "failed to get account for: {$email}");
        } else {
            // try to find the store
            foreach ($acc_info as $key => $value) {
                $this->response->add_data("acc_"."$key", "$value");
            }

            $trans = $this->trans->reset();
            $trans->add_data('email', $email);
            $info = $trans->send_command('load_store');
            // If store does not exist
            if ($info['status'] != "TRANS_OK") {
                Logger::logWarning(__METHOD__, "failed to get store for: {$email}");
            } else {
                // The store exist
                $this->response->add_data("store_found", "true");
                // put data into respose, so template can see it.
                foreach ($info as $key => $value) {
                     $this->response->add_data("$key", "$value");
                }
            }
        }
    }


    private function applyProduct($storeId, $productId)
    {
        global $BCONF;
        $expireTime = Bconf::get($BCONF, "*.payment.product.{$productId}.expire_time");
        $trans = $this->trans->reset();
        $trans->add_data("store_id", $storeId);
        $trans->add_data("expire_time", $expireTime);
        $trans->send_admin_command('apply_product_to_store');

        if ($trans->has_error(true)) {
            $errors = var_export($trans->get_errors(), true);
            Logger::logError(__METHOD__, "extend store failed: {$errors}");
            return null;
        }
        return $expireTime;
    }

    public function main($function = null)
    {
        $this->response->add_data("priv_create", Cpanel::hasPriv("Stores.createStore")?"1":"0");
        $this->response->add_data("priv_edit", Cpanel::hasPriv("Stores.editStore")?"1":"0");
        $this->response->add_data("priv_extend", Cpanel::hasPriv("Stores.extendStore") ? "1":"0");

        if ($function != null) {
            $this->response->add_data("page_name", "Buscar Tienda");
            $this->runMethod($function);
        } else {
            $this->displayResults('controlpanel/stores/links.html');
        }
    }
}
