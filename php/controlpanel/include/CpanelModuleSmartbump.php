<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;
use Yapo\SmartbumpService;
use DateTime;

class CpanelModuleSmartbump extends CpanelModule
{
    private $trans;
    private $redis;
    private $form_keys;
    private $date_keys;
    private $date_keys_2;
    private $date_input_format = "Y-m-d\TH:i:s.u\Z";
    private $date_input_format_2 = "Y-m-d\TH:i:s\Z";
    private $date_output_format = "Y-m-d H:i";
    public function __construct($trans = null, $redis = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Smartbump'));

        $this->trans = isset($trans)? $trans : new bTransaction();

        $this->form_subscribe_keys = Bconf::get($BCONF, '*.smartbump.subscribe.form_keys');
        $this->form_report_keys = Bconf::get($BCONF, '*.smartbump.report.form_keys');
        $this->date_keys = array("ExecutedAt", "SubscribedAt");
        $this->date_keys_2 = array("ExpireAt");
        // Init this controlpanel module
        parent::init();
    }

    private function populateErrors($trans)
    {
        foreach ($trans->get_errors() as $param => $code) {
            $this->response->add_data("err_$param", lang($code));
        }
    }

    private function validateSubscribeForm()
    {

        foreach (explode(",", $this->form_subscribe_keys) as $v) {
            if (!array_key_exists ($v, $_REQUEST)) {
                $this->response->add_data("err_form", "SMARTBUMP_SUBSCRIPTION_EMPTY_FORM");
                return false;
            }
            switch ($v) {
                case "sb-subscribe-amount":
                    if (!is_numeric($_REQUEST[$v]) || (int)$_REQUEST[$v] < 0) {
                        $this->response->add_data("err_form", "SMARTBUMP_INVALID_AMOUNT");
                        return false;
                    }
                    break;
                default:
                    if(empty($_REQUEST[$v])) {
                        $this->response->add_data("err_form", "SMARTBUMP_SUBSCRIPTION_EMPTY_FORM");
                        return false;
                    }
            }
        }
        return true;
    }

    public function subscribe()
    {
        if (array_key_exists ("submit", $_REQUEST) && $this->validateSubscribeForm()) {
            $email = $_REQUEST['sb-subscribe-email'];
            if ($this->verifyEmail($email)) {
                $expiration = $_REQUEST['sb-subscribe-expiration'];
                $type = $_REQUEST['sb-subscribe-type'];
                $order = $_REQUEST['sb-subscribe-order'];
                $amount = $_REQUEST['sb-subscribe-amount'];

                $date = new DateTime($expiration);
                $expiration_date = date_format($date, "Y-m-d\T23:59:59\Z");

                $trans = $this->trans->reset();
                $trans->add_data("email", $email);
                $response = $trans->send_command('get_user_by_email');
                if ($trans->has_error()) {
                    $this->populateErrors($trans);
                    $this->response->add_data("err_subscription", "SMARTBUMP_SUBSCRIPTION_USER_SEARCH_ERR");
                } elseif (!empty($response["user_id"])) {
                    $data["user_id"] = (int)$response["user_id"];
                    $data["kind"] = $type;
                    $data["expire_at"] = $expiration_date;
                    $data["requested_by"] = $_SESSION['controlpanel']['admin']['username'];
                    $data["is_admin"] = true;
                    $data["work_order"] = $order;
                    $data["amount"] = (int)$amount;
                    $data["email"] = $email;

                    $SmartbumpService = new SmartbumpService();
                    $response = $SmartbumpService->createCustomer($data);

                    if ($response == null) {
                        $this->response->add_data("err_subscription", "SMARTBUMP_SUBSCRIPTION_FAILURE");
                    } else {
                        $this->response->add_data("subscription_success", "SMARTBUMP_SUBSCRIPTION_SUCCESS");
                    }
                } else {
                    $this->response->add_data("err_subscription", "SMARTBUMP_SUBSCRIPTION_USER_NOT_FOUND");
                }
            }
        }
        $this->displayResults('controlpanel/smartbump/subscribe.html');
    }

    private function verifyEmail($email)
    {
        if (isset($email)) {
            $regex = Bconf::get($BCONF, "*.common.accounts.mobile.form_validation.email.regexp");
            if (empty($regex)) {
                $regex = "^[\w_+-]+(\.[\w_+-]+)*\.?@([\w_+-]+\.)+[\w]{2,4}$";
            }
            if (!preg_match("/{$regex}/", $email)) {
                $this->response->add_data("err_email", "ERROR_EMAIL_INVALID");
                return false;
            }

            return true;
        }
    }

    private function validateReportForm()
    {
        foreach (explode(",", $this->form_report_keys) as $v) {
            if (!array_key_exists ($v, $_REQUEST)) {
                $this->response->add_data("err_form", "SMARTBUMP_REPORT_EMPTY_FORM");
                return false;
            }
            if(empty($_REQUEST[$v])) {
                $this->response->add_data("err_form", "SMARTBUMP_REPORT_EMPTY_FORM");
                return false;
            }
        }
        return true;
    }

    private function parseReport($data)
    {
        foreach ($data as $entry) {
            foreach ($entry as $key => $value) {
                $objects = array("LastBumpAt", "CreatedAt");
                $this->setEntry("", $key, $value, "report_", $objects);
            }
        }
    }

    private function setData($data)
    {
       foreach ($data as $i => $entry) {
           foreach ($entry as $key => $value) {
               $this->response->fill_array($key, $i, $value);
           }
       }
    }

    private function setEntry($origin, $key, $value, $prefix, $allowedArr)
    {
        if (in_array($key, $allowedArr) && (gettype($value) == "object" || is_array($value))) {
            foreach ($value as $k => $v) {
                $this->setEntry($key, $k, $v, $origin.$key, $allowedArr);
            }
        } else if (in_array($key, $this->date_keys)) {
            $date = DateTime::createFromFormat($this->date_input_format, $value);
            if ($date) {
                $date = $date->format($this->date_output_format);
	    } else {
                $date = NULL;
	    }
	    $this->response->fill_array(lcfirst($prefix.$key), $date);
        } else if (in_array($key, $this->date_keys_2)) {
            $date = DateTime::createFromFormat($this->date_input_format_2, $value);
            if ($date) {
                $date = $date->format($this->date_output_format);
	    } else {
                $date = NULL;
	    }
            $this->response->fill_array(lcfirst($prefix.$key), $date);
        } else {
            $item = utf8_decode($value);
            Logger::logDebug(__METHOD__, "set report key:". $key. "value: ".$value);
            $this->response->fill_array(lcfirst($prefix.$key), $item);
        }
    }

    public function report()
    {
        if (array_key_exists ("submit", $_REQUEST) && $this->validateReportForm()) {
            $start_date =$_REQUEST['sb-report-start-date'];
            $end_date =$_REQUEST['sb-report-end-date'];
            $type = $_REQUEST['sb-report-type'];

            $SmartbumpService = new SmartbumpService();
            $data["start_date"] = $start_date;
            $data["end_date"] = $end_date;
            $data["kind"] = $type;
            $response = $SmartbumpService->getReport($data);
            if ($response == null) {
                $this->response->add_data("err_report", "SMARTBUMP_REPORT_FAILURE");
            } else if (empty($response) || empty($response->smartbumps)) {
                $this->response->add_data("err_report", "SMARTBUMP_REPORT_EMPTY_RESULT");
            } else {

                $this->response->add_data("start_date", $start_date);
                $this->response->add_data("end_date", $end_date);
                $this->response->add_data("type", $type);
                $this->parseReport($response->smartbumps);
            }
        }
        $this->displayResults('controlpanel/smartbump/report.html');
    }

    public function search()
    {
        if (array_key_exists ("email", $_POST)) {
            $this->response->add_data("email", $_POST["email"]);
            $SmartbumpService = new SmartbumpService();
            $data["email"] = $_POST["email"];
            $response = $SmartbumpService->getSubscriptions($data);
            if ($response == null) {
                $this->response->add_data("err_search", "SMARTBUMP_SEARCH_FAILURE");
            } else {
                if (count($response->subscriptions) > 0) {
                    $this->response->add_data("has_data", 1);
                    $this->setData($response->subscriptions);
                } else {
                    $this->response->add_data("err_search", "SMARTBUMP_SEARCH_EMPTY");
                }
            }
        }
        $this->displayResults('controlpanel/smartbump/search.html');
    }

    public function expire()
    {
        $data["user_id"] = (int)$_REQUEST["sb_expire_user"];
        $data["kind"] = $_REQUEST['sb_expire_type'];
        $data["admin"] = $_SESSION['controlpanel']['admin']['username'];
        $data["expire"] = true;
        $SmartbumpService = new SmartbumpService();
        $response = $SmartbumpService->expireSubscription($data);

        if ($response == null) {
            $this->response->add_data("err_expire", "SMARTBUMP_EXPIRE_FAILURE");
        } else {
            $this->response->add_data("expire_success", "SMARTBUMP_EXPIRE_SUCCESS");
        }
        $this->displayResults('controlpanel/smartbump/expire.html');
    }

    public function edit()
    {
        if (array_key_exists ("submit", $_REQUEST)) {
            if (!empty($_REQUEST["sb_edit_expiration"])) {
                $email = $_REQUEST['sb_edit_email'];
                $expiration = $_REQUEST['sb_edit_expiration'];
                $type = $_REQUEST['sb_edit_type'];

                $date = new DateTime($expiration);
                $expiration_date = date_format($date, "Y-m-d\T23:59:59\Z");

                $data["user_id"] = (int)$_REQUEST["sb_edit_user_id"];
                $data["kind"] = $type;
                $data["expire_at"] = $expiration_date;
                $data["requested_by"] = $_SESSION['controlpanel']['admin']['username'];
                $data["is_admin"] = true;
                $data["email"] = $email;

                $SmartbumpService = new SmartbumpService();
                $response = $SmartbumpService->createCustomer($data);

                if (is_null($response)) {
                    $this->response->add_data("err_subscription", "SMARTBUMP_SUBSCRIPTION_FAILURE");
                } else {
                    $this->response->add_data("subscription_success", "SMARTBUMP_SUBSCRIPTION_SUCCESS");
                }
            } else {
                $this->response->add_data("email", $_REQUEST["sb_edit_email"]);
                $this->response->add_data("sb_user_id", $_REQUEST["sb_edit_user_id"]);
                $this->response->add_data("type", $_REQUEST["sb_edit_type"]);
                $this->response->add_data("expiration_date", date('Y-m-d'));
                $this->response->add_data("err_form", "SMARTBUMP_SUBSCRIPTION_FAILURE");
            }
        } else {
            $this->response->add_data("email", $_REQUEST["sb_edit_email"]);
            $this->response->add_data("sb_user_id", $_REQUEST["sb_edit_user_id"]);
            $this->response->add_data("type", $_REQUEST["sb_edit_type"]);
            $this->response->add_data("expiration_date", $_REQUEST["sb_edit_expiration"]);
        }
        $this->displayResults('controlpanel/smartbump/edit.html');
    }

    public function show()
    {
        $this->displayResults('controlpanel/smartbump/subscribe.html');
    }

    public function main($function = null)
    {
        $this->response->add_data("priv_subscribe", Cpanel::hasPriv("Smartbump.subscribe")? "1": "0");
        $this->response->add_data("priv_report", Cpanel::hasPriv("Smartbump.report")? "1": "0");
        $this->response->add_data("priv_search", Cpanel::hasPriv("Smartbump.search")? "1": "0");
        $this->response->add_data("priv_edit", Cpanel::hasPriv("Smartbump.edit")? "1": "0");
        $this->response->add_data("priv_expire", Cpanel::hasPriv("Smartbump.expire")? "1": "0");
        if ($function != null) {
            $this->response->add_data("page_name", "Smartbump");
            $this->response->add_data('page_name_hide', '1');
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_js', '/js/Smartbump.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->runMethod($function);
        } else {
            $this->show();
        }
    }

}
