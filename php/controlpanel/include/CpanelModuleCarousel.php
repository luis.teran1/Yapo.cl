<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;
use Yapo\CarouselService;
use DateTime;

class CpanelModuleCarousel extends CpanelModule
{
    private $trans;
    private $redis;
    private $assign_form_keys;
    private $report_form_keys;

    public function __construct($trans = null, $redis = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Carousel'));
        $this->trans = isset($trans)? $trans : new bTransaction();
        $this->assign_form_keys = Bconf::get($BCONF, '*.carousel.assign_form_keys');
        $this->report_form_keys = Bconf::get($BCONF, '*.carousel.report_form_keys');
        // Init this controlpanel module
        parent::init();
    }

    private function populateErrors($trans)
    {
        foreach ($trans->get_errors() as $param => $code) {
            $this->response->add_data("err_$param", lang($code));
        }
    }

    private function isDate($value)
    {
        if (!$value) {
            return false;
        }
        try {
            new \DateTime($value);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    private function validateForm()
    {
        foreach (explode(",", $this->assign_form_keys) as $v) {
            if (!array_key_exists ($v, $_REQUEST)) {
                $this->response->add_data("err_form", "CAROUSEL_ASSIGN_EMPTY_FORM");
                return false;
            }
            if (strcmp ( $v, "crsl-assign-price-range" ) == 0) {
                if ($_REQUEST[$v] == ""){
                    return true;
                }
                if (!is_numeric($_REQUEST[$v]) || (int)$_REQUEST[$v] < 0) {
                    $this->response->add_data("err_form", "CAROUSEL_INVALID_PRICE_RANGE");
                    return false;
                }
            }
        }
        return true;
    }

    private function validateReportForm()
    {
        foreach (explode(",", $this->report_form_keys) as $v) {
            if (!array_key_exists ($v, $_REQUEST)) {
                $this->response->add_data("err_form", "CAROUSEL_REPORT_EMPTY_FORM");
                return false;
            }
            if (empty($_REQUEST[$v])) {
                $this->response->add_data("err_form", "CAROUSEL_REPORT_EMPTY_FORM");
                return false;
            }
        }

        $start_date = $_REQUEST['crsl-report-start-date'];
        $end_date = $_REQUEST['crsl-report-end-date'];
        if (!$this->isDate($start_date) || !$this->isDate($end_date)) {
            $this->response->add_data("err_form", "CAROUSEL_REPORT_INVALID_DATE");
            return false;
        }
        $start_date = new DateTime($start_date);
        $end_date = new DateTime($end_date);
	if ($start_date > $end_date) {
            $this->response->add_data("err_form", "CAROUSEL_REPORT_INVALID_DATE_RANGE");
            return false;
        }
        return true;
    }

    public function assign()
    {
        if (array_key_exists ("submit", $_REQUEST) && $this->validateForm()) {
            $email = $_REQUEST['crsl-assign-email'];
            $is_edit=(array_key_exists ("product_id", $_REQUEST) && !empty($_REQUEST['product_id']));
            if ($this->verifyEmail($email)) {
                $expiration = $_REQUEST['crsl-assign-expiration'];
                $keywords = $_REQUEST['crsl-assign-keywords'];
                $categories = $_REQUEST['crsl-assign-categories'];
                $limit = $_REQUEST['crsl-assign-limit'];
                $purchase_number = $_REQUEST['crsl-assign-purchase-number'];
                $purchase_price = $_REQUEST['crsl-assign-purchase-price'];
                $price_range = $_REQUEST['crsl-assign-price-range'];
                $comment = $_REQUEST['crsl-assign-comment'];
                $fill_random = false;
                if (array_key_exists ("crsl-assign-fill-random", $_REQUEST)) {
                    $fill_random = true;
                }
                $date = new DateTime($expiration);
                $expiration_date = date_format($date, "Y-m-d\T23:59:59\Z");
                if ($is_edit) {
                    $data["ID"] = (int)$_REQUEST['product_id'];
                }

                $trans = $this->trans->reset();
                $trans->add_data("email", $email);
                $response = $trans->send_command('get_user_by_email');
                if ($trans->has_error()) {
                    $this->populateErrors($trans);
                    $this->response->add_data("err_subscription", "CAROUSEL_ASSIGN_USER_SEARCH_ERR");
                } elseif (!empty($response["user_id"])) {
                    $data["user_id"] = (int)$response["user_id"];
                    $data["expiration"] = $expiration_date;
                    $data["keywords"] = $keywords;
                    $data["categories"] = $categories;
                    $data["comment"] = $comment;
                    $data["limit"] = (int)$limit;
                    $data["price_range"] = (int)$price_range;
                    $data["email"] = $email;
                    $data["purchase_number"] = (int)$purchase_number;
                    $data["purchase_price"] = (int)$purchase_price;
                    $data["fill_random"] = $fill_random;
                    $CarouselService = new CarouselService();
                    $response = $CarouselService->assignCarousel($data);

                    if ($response == null) {
                        $this->response->add_data("err_assign", $is_edit ? "CAROUSEL_EDIT_FAILURE" : "CAROUSEL_ASSIGN_FAILURE");
                    } else {
                        $this->response->add_data("assign_success", $is_edit ? "CAROUSEL_EDIT_SUCCESS" : "CAROUSEL_ASSIGN_SUCCESS");
                    }
                } else {
                    $this->response->add_data("err_assign", "CAROUSEL_ASSIGN_USER_NOT_FOUND");
                }
            }
        }
        $this->show(true);
    }

    private function verifyEmail($email)
    {
        if (isset($email)) {
            $regex = Bconf::get($BCONF, "*.common.accounts.mobile.form_validation.email.regexp");
            if (empty($regex)) {
                $regex = "^[\w_+-]+(\.[\w_+-]+)*\.?@([\w_+-]+\.)+[\w]{2,4}$";
            }
            if (!preg_match("/{$regex}/", $email)) {
                $this->response->add_data("err_email", "ERROR_EMAIL_INVALID");
                return false;
            }

            return true;
        }
    }
    private function checkService()
    {
       $CarouselService = new CarouselService();
       $response = $CarouselService->checkService();

       if ($response == null) {
           $this->response->add_data("err_healthcheck", "CAROUSEL_ASSIGN_UNAVAILABLE");
       }
    }

    private function parseAssigns($data)
    {
        foreach ($data as $entry) {
            foreach ($entry as $key => $value) {
                $objects = array("id", "email", "user_id", "expiration", "purchase_number", "purchase_price",
                    "status", "limit","comment", "categories", "keywords", "price_range", "fill_with_random");
                $this->setAssign("", $key, $value, "assign_", $objects);
            }
        }
    }

    private function setAssign($origin, $key, $value, $prefix, $allowedArr)
    {
        if (in_array($key, $allowedArr) && (gettype($value) == "object" || is_array($value))) {
            foreach ($value as $k => $v) {
                $this->setAssign($key, $k, $v, $origin.$key, $allowedArr);
            }
        } else {
            $item = utf8_decode($value);
            Logger::logDebug(__METHOD__, "set assign key:". $key. "value: ".$value);
	    if (strtolower($key) == 'fill_random') {
                $item = ($value) ? 'true' : 'false';
            }
            $this->response->fill_array(lcfirst($prefix.$key), $item);
        }
    }

    public function search()
    {
        if (array_key_exists ("page", $_REQUEST)) {
            $data["page"] = $_REQUEST['page'];
        } else {
            $data["page"] = 1;
        }
        // If we are searching for a given email
        if (array_key_exists ("crsl-search-email", $_REQUEST) && array_key_exists ("search", $_REQUEST) ){
            $email = $_REQUEST['crsl-search-email'];
            if ($this->verifyEmail($email)) {
                $data["email"] = $email;
                $this->response->add_data("search_email", $email);
        } else {
            $this->show(true);
            return;
        }
        // If we are searching for an empty or already validated email
        } else if (array_key_exists ("page-email", $_REQUEST)) {
             $email = $_REQUEST['page-email'];
	     if (!empty($email)) {
                 $data["email"] = $email;
	     }
        }
        $this->getAssigns($data);
    }    

    public function getAssigns(Array $data)
    {
	// if the page is not present, add it
	if (!array_key_exists ("page", $data)) {
            $data["page"]=1;
	}
        $CarouselService = new CarouselService();
        $response = $CarouselService->fetchAssigns($data);
	if (!empty($response)) {
            if (array_key_exists ("assigns", $response)) {
      	        $this->parseAssigns($response->assigns);
            }
	    if (array_key_exists ("metadata", $response)) {
	        $current_page=$response->metadata->current_page;
	        $total_pages=$response->metadata->total_pages;
	        $prev_page=$current_page > 1 ? $current_page - 1 : 1;
                $next_page=$current_page < $total_pages ? $current_page + 1 : $total_pages;
                $this->response->add_data("current_page", $current_page);
                $this->response->add_data("total_pages", $total_pages);
                $this->response->add_data("prev_page", $prev_page);
                $this->response->add_data("next_page", $next_page);
	    }
            if (array_key_exists ("email", $data)) {
                $this->response->add_data("search_email", $data["email"]);
            }
            $this->show(false);
	}else{
            //show error
            $this->show(true);
	}
    }

    public function activate()
    {
        if (array_key_exists ("id", $_REQUEST)) {
            $assign_id = (int)$_REQUEST['id'];
            $CarouselService = new CarouselService();
            $response = $CarouselService->activateAssign($assign_id);
            if ($response == null) {
                $this->response->add_data("err_activate", "CAROUSEL_ACTIVATE_FAILURE");
            } else {
                $this->response->add_data("activate_success", "CAROUSEL_ACTIVATE_SUCCESS");
            }
        }
        $this->show(true);
    }

    public function deactivate()
    {
        if (array_key_exists ("id", $_REQUEST)) {
            $assign_id = (int)$_REQUEST['id'];
            $CarouselService = new CarouselService();
            $response = $CarouselService->deactivateAssign($assign_id);
            if ($response == null) {
                $this->response->add_data("err_deactivate", "CAROUSEL_DEACTIVATE_FAILURE");
            } else {
                $this->response->add_data("deactivate_success", "CAROUSEL_DEACTIVATE_SUCCESS");
            }
        }
        $this->show(true);
    }

    public function report()
    {
	if (array_key_exists ("cmd", $_REQUEST) && $_REQUEST["cmd"] == "report" && $this->validateReportForm()) {
            //get the date range for the report
            $start_date = $_REQUEST['crsl-report-start-date'];
            $end_date = $_REQUEST['crsl-report-end-date'];
            $this->response->add_data("start_date", $start_date);
            $this->response->add_data("end_date", $end_date);
            $start_date = new DateTime($start_date);
            $start_date = date_format($start_date, "Y-m-d\T00:00:00\Z");
            $end_date = new DateTime($end_date);
            $end_date = date_format($end_date, "Y-m-d\T23:59:59\Z");
            $CarouselService = new CarouselService();
            $response = $CarouselService->generateReport($start_date, $end_date);
            if ($response == null) {
                $this->response->add_data("err_report", "CAROUSEL_REPORT_FAILURE");
            } else {
	        // received data from carousel service correctly
                if (array_key_exists ("assigns", $response)) {
                    if (empty($response->assigns)) {
                        $this->response->add_data("err_empty", "CAROUSEL_REPORT_EMPTY");
		    } else {
		        $this->parseAssigns($response->assigns);
                    }
                }
            }
        }
        $this->displayResults('controlpanel/carousel/report.html');
    }

    public function edit()
    {
        $this->response->add_data("product_id", $_REQUEST["id"]);
        $this->response->add_data("email", $_REQUEST["email"]);
        $this->response->add_data("expiration", $_REQUEST["expiration"]);
        $this->response->add_data("keywords", $_REQUEST["keywords"]);
        $this->response->add_data("purchase_number", $_REQUEST["purchase_number"]);
        $this->response->add_data("purchase_price", $_REQUEST["purchase_price"]);
        $this->response->add_data("categories", $_REQUEST["categories"]);
        $this->response->add_data("limit", $_REQUEST["limit"]);
        $this->response->add_data("comment", $_REQUEST["comment"]);
        $this->response->add_data("price_range", $_REQUEST["price-range"]);
        $this->response->add_data("fill_random", $_REQUEST["fill-random"]);
        $this->show((bool)true);
    }



    public function show($should_load)
    {
        $this->response->add_data("priv_assign", Cpanel::hasPriv("Carousel.assign")? "1": "0");
	if ((bool)$should_load) {	
	    $this->getAssigns(array());
	}
        $this->displayResults('controlpanel/carousel/assign.html');
    }

    public function main($function = null)
    {
	$this->response->add_data("priv_assign", Cpanel::hasPriv("Carousel.assign")? "1": "0");
	$this->response->add_data("priv_report", Cpanel::hasPriv("Carousel.report")? "1": "0");
	if ($function != null) {
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_js', '/js/Carousel.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->checkService();
            if (array_key_exists ("err_healthcheck",$this->response->data))  {
                if ($function == "report") {
                    $this->response->add_data("page_name", "Report");
                    $this->response->add_data('page_name_hide', '1');
                    $this->displayResults('controlpanel/carousel/report.html');
                } else {
                    $this->response->add_data("page_name", "Assign");
                    $this->response->add_data('page_name_hide', '1');
                    $this->show(false);
                }
		return;
            }
            $this->runMethod($function);
        } else {
            $this->show(true);
        }
    }

}
