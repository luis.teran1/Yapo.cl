<?php

namespace Yapo;

use bTransaction;

class PpageBanner
{
    public $pp_id;
    public $url;
    public $start_date;
    public $end_date;
    public $categories;
    public $regions;
    public $image;
    public $key;
    public $banner_id;
    public $platform;
    public $position;
    public $is_draft;
    public $status;

    /*
     * Ppage constructor. Set as private because a
     * Ppage is only created using fromForm and
     * fromTrans methods.
     */
    private function __construct(array $data)
    {
        foreach ($this as $k => $v) {
            if (isset($data[$k])) {
                $this->$k = Util::sanitize(
                    $data[$k],
                    self::validatorType($k)
                );
            } else {
                $error = self::errorFor($k, $v);
                if ($error != null) {
                    throw new \InvalidArgumentException($error);
                }
            }
        }
    }

    /*
     * Returns date type for validator sanitize
     */
    private static function validatorType($parameter)
    {
        switch ($parameter) {
            case 'pp_id':
                return 'int';
            default:
                return 'string';
        }
    }

    /*
     * Returns the error string when a parameter is wrong
     */
    private static function errorFor($parameter, $value)
    {
        $valid_platform = array("desktop", "msite");
        $valid_position = array("home", "listing");
        switch ($parameter) {
            case 'pp_id':
                return "ERROR_NO_PP_SELECTED";
            case 'url':
            case 'start_date':
                return "ERROR_NO_START_DATE";
            case 'platform':
                return in_array($value, $valid_platform) ? null : "ERROR_INVALID_PLATFORM";
            case 'position':
                return in_array($value, $valid_position) ? null : "ERROR_INVALID_POSITION";
            case 'image':
                return "ERROR_IMAGE_NOT_FOUND";
            default:
                return null;
        }
    }

    /*
     * Saves the current object as a promo page in trans.
     */
    public function save()
    {
        $trans = new bTransaction();
        foreach ($this as $k => $v) {
            if (isset($v) && $v != null) {
                $trans->add_data($k, $v);
            }
        }
        $reply = $trans->send_command('set_pp_banner');
        if ($reply['status'] == "TRANS_OK") {
            return true;
        } else {
            $fields = array('error') + array_keys(get_object_vars($this));
            return Util::getErrors($reply, 'BANNER_DATE_OVERLAP', $fields);
        }
    }

    /*
     * Deletes given banner_id.
     * @banner_id: platform:position:id
     */
    public static function delete($banner_id)
    {
        if (!$banner_id) {
            return false;
        }
        $trans = new bTransaction();
        $trans->add_data('banner_id', $banner_id);
        $reply = $trans->send_command('delete_pp_banner');
        return $reply['status'] === "TRANS_OK";
    }

    /*
     * Returns the error from a trans response.
     * It's used only by other methods from the class.
     */
    public static function fromForm(array $form)
    {
        $data = array();
        $data = array_filter($form, function ($v) {
            return isset($v) && $v != null;
        });
        try {
            $banner = new PpageBanner($data);
        } catch (\InvalidArgumentException $e) {
            return $e->getMessage();
        }

        if ($banner->position == "listing") {
            if (!$banner->categories || count($banner->categories) == 0) {
                return "ERROR_NO_CATEGORY_SELECTED";
            }
            if (!$banner->regions || count($banner->regions) == 0) {
                return "ERROR_NO_REGION_SELECTED";
            }
        }
        return $banner;
    }

    public static function fromTrans($key = null)
    {
        $trans = new bTransaction();
        if ($key) {
            $trans->add_data('key', $key);
        }
        $reply = $trans->send_command('get_promo_banners');
        if ($reply['status'] == "TRANS_OK" && isset($reply['promo_banners'])) {
            return PpageBanner::parseTransResponse($reply['promo_banners']);
        }
    }

    private static function parseTransResponse(array $reply)
    {
        $banners = array();
        foreach ($reply as $j => $banner) {
            foreach ($banner as $k => $v) {
                if (is_array($v)) {
                    $banners[$k][$j] = $v;
                } else {
                    $banners[$k][] = $v;
                }
            }
        }
        return new PpageBanner($banners);
    }
}
