<?php

use \Yapo\HighlighText as HighlighText;

require_once('autoload_lib.php');

require_once('util.php');
require_once('text_diff.php');
require_once('weeks.php');
require_once('yapo_geoip.php');
require_once('redis_cardata.php');
require_once('AdsEvaluatorService.php');

use Yapo\Cpanel;

/* Sort duplicates percent descending */
function dupper_sort($a, $b) {
	$pa = $a['duplicates_percent'];
	$pb = $b['duplicates_percent'];

	if ($pa == $pb)
		return 0;
	return ($pa > $pb) ? -1 : 1;
}

class cpanel_module_adqueue extends cpanel_module {

	private $ads_evaluator;
	var $redis_conection = 'session_transbank';

	function init_redis_connection() {
		global $BCONF;
		$redis_conn= array(
			'servers' =>  $GLOBALS['BCONF']['*']['redis']['session']['host'], 
			'debug' => true,
			'id_separator' => bconf_get($GLOBALS['BCONF'], "*.redis.session.id_separator"),
			'timeout' => bconf_get($GLOBALS['BCONF'], "*.redis.session.connect_timeout")
		);
		$this->rsc = new RedisSessionClient($redis_conn);
	}

	function cpanel_module_adqueue() {

		/* Get configuration for easy access */
		$this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.adqueue'));

		/* Get dynamic configuration */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$root = '*.controlpanel.modules.adqueue.settings';
			$settings = bconf_get($this->dyn_bconf, $root);
			if (isset($settings)) {
				if (is_string($settings))
					$settings = array($settings);
				foreach ($settings as $key => $value) {
					$this->config['settings'][$key] = $value;
				}
			}
		} else
			$this->dyn_bconf = false;
		$this->ads_evaluator = new AdsEvaluatorApiClient();

		// Init this controlpanel module
		$this->init();
	}

	function get_ad_key($ad_id, $action_id) {
		if (!isset($_SESSION['controlpanel']['adqueue']['show_ad']['ad_id']))
			return false;
		foreach ($_SESSION['controlpanel']['adqueue']['show_ad']['ad_id'] as $key => $s_ad_id) {
			if ($ad_id == $s_ad_id && $action_id == $_SESSION['controlpanel']['adqueue']['show_ad']['action_id'][$key])
				return $key;
		}
		return false;
	}

	function unregister_ad($ad_id, $action_id) {
		global $session_read_only;
		$session_read_only = false;

		$key = $this->get_ad_key($ad_id, $action_id);
		if ($key !== false) {
			unset($_SESSION['controlpanel']['adqueue']['show_ad']['ad_id'][$key]);
			unset($_SESSION['controlpanel']['adqueue']['show_ad']['action_id'][$key]);
		}
	}

	/*
	 * Commiting ads
	 */
	function commit_ad() {
		global $BCONF;
		/*
		 * Handle ad review
		 */
		$status = 1;
		$message = lang('ADMIN_APPROVED');
		$error_message = false;
		$ad = NULL;

		$email = $_POST['users_email'];
		$ad_id = $_POST['ad_id'];
		$action = $_POST['queue_action'];

		/* Handle notices, delete, add or change. */
		if (@$_REQUEST['notice']) {
			foreach ($_REQUEST['notice'] as $note_type => $note_id) {
				$transaction = new bTransaction();
				if ($_REQUEST["body_$note_type"] == "")
					continue;

				if (isset ($_REQUEST['del_' . $note_type])) {
					$transaction->add_data('delete', 1);
				} else {
					$value = @iconv('UTF-8', bconf_get($BCONF, "common.charset") . '//IGNORE', $_REQUEST['body_' . $note_type]);
					$transaction->add_data('body', $value);
				}

				if ($note_type == 'ad') {
					$transaction->add_data('ad_id', $ad_id);
				} else {
					if ($note_type == 'email') {
						$transaction->add_data('user_id', intval(@$_POST['user_id']));
					} else {
						$transaction->add_data('uid', intval(@$_POST['uid']));
					}

					if (isset($_REQUEST["abuse_$note_type"]) && $_REQUEST["abuse_$note_type"] == '1')
						$transaction->add_data('abuse', '1');
				}

				$reply = $transaction->send_admin_command('setnotice');
				$response = new bResponse();
				Cpanel::handleReply($reply, $response);
				if ($response->has_error()) {
					$status = false;
					$error = $response->get_error();
					$message = lang('ADMIN_NOTICE_ERROR');
					$error_message = lang($error);

					if ($error == 'ERROR_INVALID_NOTICE_ID') {
						// Assume deleted by someone else
						$status = true;
					} elseif ($error == 'ERROR_NOTICE_EXISTS') {
						$status = -1;
						$error_message = str_replace('%uid', $_POST['uid'], $error_message);
					}
					if ($status !== true) {
						if (isset($_REQUEST['ajax'])) {
							$this->unregister_ad($ad_id, $_POST['action_id']);

							if ($status === false)
								exit("false");
							else
								exit(json_encode(array('status' => $status, 'message' => $message, 'error_message' => $error_message)));
						} else {
							$this->show_ad(null, $error_message);
							return;
						}
					}
				} else {
					//A note has been succesfully added, nothing to do
				}
				unset($_POST['type_' . $note_id]);
				unset($_POST['del_' . $note_id]);
				unset($_POST['body_' . $note_id]);
			}
			unset($_POST['notice']);
		}

		/* Review the ad */
		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $ad_id);
		$transaction->add_data('action_id', $_POST['action_id']);

		switch ($_POST['queue_action']) {
			case 'accept':
			case 'acceptEdit':
				$command = 'review';
				if ($_POST['queue_action'] == 'acceptEdit') {
					$transaction->add_data('action', 'accept_with_changes');
					$transaction->add_data('reason', $_POST['reasonAccept']);
				} else {
					$transaction->add_data('action', 'accept');
				}

				foreach($_POST as $key => $value) {
					switch ($key) {
						case 'currency':
						case 'type':
						case 'company_ad':
						case 'filter_name':
						case 'subject':
						case 'body':
							$value = @iconv('UTF-8', bconf_get($BCONF, "common.charset"). '//IGNORE', $value);
							$transaction->add_data($key, $value);
							break;
						default:
							break;
					}
				}

				if (bconf_get($BCONF, "controlpanel.modules.adqueue.suppress_email.{$_POST['filter_name']}") == 1) {
					$transaction->add_data("do_not_send_mail", "1");
					$filter_name = $_POST['filter_name'];

					/* This is very specific for the launch date please do not touch before that :)*/
					/* Send category change email if the queue is such and they are not spidered and
					 * have list_time earlier that 6 months */
					if ($filter_name == 'category_change' && !isset($_POST['link_type']) && isset($_POST['list_time'])) {
						$diff = abs(time() - strtotime($_POST['list_time']));
						if ($diff < 15552000) {
							$send_mail_trans = new bTransaction;
							$send_mail_trans->add_data('ad_id', $ad_id);
							$send_mail_trans->add_data('mail_type', 'category_changed');
							$send_mail_trans->send_command('admail');
							if ($send_mail_trans->has_error()) {
								die_with_template(__FILE__, log_string() . ": failed to send admail");
								/* Not reached */
							}
						}
					}
				}


				$cat_group = intval($_POST['category_group']);
				$sub_cat = intval($_POST['sub_category']);

				if ($sub_cat && bconf_get($BCONF, "*.cat.{$sub_cat}.parent") == $cat_group) {
					$category = $sub_cat;
				} else {
					$category = $cat_group;
				}
				$io = array('category' => $category,
						'parent' => bconf_get($BCONF, "*.cat.$category.parent"),
						'type' => $_POST['type']);

				get_settings(bconf_get($BCONF,"*.category_settings"), "params",
						create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
						create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
						$io);
				$adp_list = array_keys($io);

				if ($category)
					$transaction->add_data('category', $category);


				/* New for Yapo*/
				/* Try to get the extra params values from the post */
				$cat_extra_pars_node = bconf_get($BCONF, "*.adqueue_extraparams.$category");
				if($cat_extra_pars_node != NULL) {
					foreach($cat_extra_pars_node as $key=>$param) {
						$type = $param["type"];
						$name = $param["name"];
						switch ($type) {
							case "checkbox":
								/*FT 436: It's very important check if the index of array exist !!! */
								if (isset($_POST[$name])) $values = $_POST[$name];
								else unset($value);

								if(isset($values)) {
									$index_array = array();
									foreach($values as $index=>$value) {
										$index_array[] = $index;
									}
									$transaction->add_data($name, implode("|", $index_array));
								}
								break;
						}
					}
				}

				/* CHUS 254: Make certain params editable on CP */
				/* FT 372: In CP ad review, when changing category and some "old" adparams do not belong in the new category,
					TRANS_ERROR_NO_SUCH_PARAMETER appears */
				$cat_editable_pars_node = bconf_get($BCONF, "*.adqueue_editable_params.$category");

				if($cat_editable_pars_node != NULL) {
					foreach($cat_editable_pars_node as $key=>$param) {
						if (isset($_POST[$param]) && $_POST[$param] !="") {
							$transaction->add_data($param, $_POST[$param]);
						}
					}
				}

				if (isset($_POST['price'])  && !empty($_POST['price'])) {
					$transaction->add_data('price', $this->prepare_price($_POST['price']));
				} elseif (isset($_POST['monthly_rent']) &&
						in_array('monthly_rent', $adp_list)) {
					$transaction->add_data('monthly_rent', $_POST['monthly_rent']);
				} elseif (isset($_POST['periodic_fee']) &&
						in_array('periodic_fee', $adp_list)) {
					$transaction->add_data('periodic_fee', $_POST['periodic_fee']);
				} elseif (isset($_POST['max_rent']) &&
						in_array('max_rent', $adp_list)) {
					$transaction->add_data('max_rent', $_POST['max_rent']);
				} elseif (isset($_POST['findersfee']) &&
					  in_array('findersfee', $adp_list)) {
					$transaction->add_data('findersfee', $_POST['findersfee']);
				} elseif (in_array('country', $adp_list) && $_POST['category_changed'] == 1) {
					$transaction->add_data('country', 15);
				}

				/* Image handling */
				if (isset($_POST['set_first']) && count($_POST['set_first'])==1 && $_POST['set_first'][0] != 0) {
					$transaction->add_data("set_first", $_POST['set_first'][0]);
				}
				if (isset($_POST['delete_images'])) {
					foreach($_POST['delete_images'] as $value) {
						if ($value == 0)
							$main_image_deleted = 1;
						$transaction->add_data("remove_image$value", '1');
						$imagevar = "image_name$value";
						unset($_POST[$imagevar]);
					}
					for ($i = 0; $i < bconf_get($BCONF, '*.common.default.max_limit_extra_images'); $i++) {
						$imagevar = 'image_name' . $i;
					}
				}

				/* Info page */
				if (isset($_POST['delete_infopage']))
					$transaction->add_data('remove_infopage', '1');

				break;

			case 'refuse' :
				$command = 'review';
				$message = lang('ADMIN_REFUSED');
				$transaction->add_data('action', $_POST['queue_action']);
				$transaction->add_data('reason', $_POST['reason']);
				$transaction->add_data('filter_name', $_POST['filter_name']);
				if (bconf_get($BCONF, "controlpanel.modules.adqueue.suppress_email.{$_POST['filter_name']}") === 1)
					$transaction->add_data("do_not_send_mail", "1");
				if (!empty($_POST['sub_category_refusal']) && !empty($_POST['category_group_refusal']) && bconf_get($BCONF, "*.cat.{$_POST['sub_category_refusal']}.parent") == $_POST['category_group_refusal']) {
					$_POST['refusal_category'] = $_POST['sub_category_refusal'];
				} elseif (!empty($_POST['category_group_refusal'])) {
					$_POST['refusal_category'] = $_POST['category_group_refusal'];
				}


				if (bconf_get($BCONF, "controlpanel.refusal.supress_email_on_fraud.flag") == "1") {
					$reasonname = bconf_get($this->dyn_bconf, "*.*.common.refusal.{$_POST['reason']}.name");
					if (preg_match('/^.*(Fraud|fraud|FRAUD).*$/', $reasonname)) {
						$transaction->add_data("do_not_send_mail", "1");
					}
				}

				if (bconf_get($this->dyn_bconf, "*.*.common.refusal.{$_POST['reason']}.extra_msg")) {
					$refusal_text = "";
					foreach (bconf_get($this->dyn_bconf, "*.*.common.refusal.{$_POST['reason']}.extra_msg") as $key => $value) {
						if (!isset($value['no_space']) && $key != 1)
							$refusal_text .= " ";
						if (isset($value['string']))
							$refusal_text .= $value['string'];
						if (isset($value['var'])) {
							if (empty($_POST[$value['var']])) {
								$refusal_text = '';
								break;
							}
							$catnum = intval($_POST[$value['var']]);
							$catname =  bconf_get($BCONF, "*.cat.$catnum.name");
							while (bconf_get($BCONF, "*.cat.$catnum.level") > 1) {
								$catnum = intval(bconf_get($BCONF, "*.cat.$catnum.parent"));
								$catname = bconf_get($BCONF, "*.cat.$catnum.name") . ", $catname";
							}
							$refusal_text .= $catname;
						}
					}
					if ($refusal_text)
						$transaction->add_data('refusal_text', $refusal_text);
				}
				break;
			case 'region':
				$command = 'unlock';
				$message = lang('ADMIN_REGION_QUEUE');
				$transaction->add_data('new_queue', 'region');
				break;
			case 'abuse':
				$command = 'unlock';
				$message = lang('ADMIN_ABUSE_QUEUE');
				$transaction->add_data('new_queue', 'abuse2');
				break;
			case 'unsolved':
				$command = 'unlock';
				$message = lang('ADMIN_UNSOLVED_QUEUE');
				$transaction->add_data('new_queue', 'unsolved');
				break;

			case 'none':
				if ($this->get_ad_key($ad_id, $_POST['action_id']) !== false) {
					$command = 'unlock';
					$message = lang('ADMIN_UNLOCK');
				} else {
					$command = false;
					$message = lang('ADMIN_NO_ACTION');
				}
				break;
		}
		if ($command) {
			$reply = $transaction->send_admin_command($command);
			if ($reply['status'] == 'TRANS_OK' && $command == 'review') {
				$this->increment_reviewed_ads();
			}
			Cpanel::handleReply($reply, $this->response);
		}

		/* Check review result */
		if ($command && $this->response->has_error()) {
			$status = 0;
			$error = $this->response->get_error();
			$message = lang('ADMIN_TRY_AGAIN');
			$error_message = lang($error);
			$remove_ad = false;

			if (substr($error,0, 17) == 'ERROR_AUTO_REFUSE') {
				$message = lang('ADMIN_REFUSED');
			}
			if ($error == 'ERROR_INVALID_STATE') {
				$status = -1;
				$message = lang('ADMIN_ALREADY_REVIEWED');
				if (isset($_REQUEST['ajax'])) {
					$error_message = "";
				}
			} else if ($error == $error_message) {
				/* Error message is not translated, move to technical error */
				$remove_ad = true;
				$status = -1;
				$message = lang('ADMIN_ERROR_QUEUE');

				$transaction = new bTransaction();
				$transaction->add_data('ad_id', $ad_id);
				$transaction->add_data('action_id', $_POST['action_id']);
				$transaction->add_data('new_queue', 'technical_error');
				$reply = $transaction->send_admin_command('unlock');
				if ($reply['status'] != 'TRANS_OK') {
					/* Technical error */
					$status = false;
				}
			}
		} else {
			$remove_ad = true;
			$this->refresh_account_cache($email);
		}

		if ($remove_ad) {
			$this->unregister_ad($ad_id, $_POST['action_id']);

			// Now that we're sure the ad passed thru the review process, we can update the info on the CP toolbar
			if(in_array($_POST['queue_action'], array("accept", "acceptEdit", "refuse"))){
				$this->redis_review_action($_POST['queue_action']);
			}

			$cat_group = intval($_POST['category_group']);
			$main_cat_id = bconf_get($BCONF, "*.cat.$cat_group.parent");
			$main_cat_name = utf8_encode(bconf_get($BCONF, "*.cat.$main_cat_id.name"));
		}

		/* If ajax, quit and display result! */
		if (isset($_REQUEST['ajax'])) {
			if ($status === false)
				exit("false");
			else
				exit(json_encode(array('status' => $status, 'message' => $message, 'error_message' => $error_message)));
		} else {
			/* Either show error or remove ad from session */
			if ($status === false) {
				$this->unregister_ad($ad_id, $_POST['action_id']);
			}

			$this->show_ad(@$_POST['users_email'], $error_message);
		}
	}

	/* Unlock all ads in session  */
	function unlock_adqueue() {
		global $BCONF;
		global $session_read_only;
		$session_read_only = false;

		foreach ($_SESSION['controlpanel']['adqueue']['show_ad']['ad_id'] as $key => $ad_id) {
			$transaction = new bTransaction();
			$transaction->add_data('ad_id', $ad_id);
			$transaction->add_data('action_id', $_SESSION['controlpanel']['adqueue']['show_ad']['action_id'][$key]);
			$reply = $transaction->send_admin_command('unlock');
		}
		unset($_SESSION['controlpanel']['adqueue']['show_ad']);
		$this->response->redirect(preg_replace("/unlock_adqueue/", "show_adqueues", bconf_get($BCONF, '*.common.base_url.controlpanel').$_SERVER['REQUEST_URI']));
	}

	function lists_by_type($list_type) {
		static $list_cache = array();

		if ( array_key_exists($list_type, $list_cache) ) {
			$get_lists_reply = $list_cache[$list_type];
		} else {
			/* Get filter lists for adqueue */
			$get_lists_trans = new bTransaction();
			$get_lists_trans->add_data('action', 'list');
			$get_lists_trans->add_data('list_type', $list_type);
			$get_lists_reply = $get_lists_trans->send_admin_command('block_list');

			if ( isset($get_lists_reply['block_list']) ) {
				$list_cache[$list_type] = $get_lists_reply;
			}
		}

		if (isset($get_lists_reply['block_list'])) {
			foreach ($get_lists_reply['block_list'] as $list) {
				$this->response->fill_array("${list_type}_lists", $list['list_name']);
				$this->response->fill_array("${list_type}_lists_id", $list['list_id']);
				$this->response->fill_array("${list_type}_lists_type", $list['list_type']);
			}
		}
	}

	/*
	 * Makes a request to lock an ad for review
	 * If an ad is already in session and still locked, it just reloads the ad data
	 */
	function show_ad($commit_email = null, $error_message = null) {
		global $BCONF;
		
		if (isset($_REQUEST['queue']) && $_REQUEST['queue'] != 'all') {
			$trans_ = new bTransaction();
			$trans_->add_data('queue', $_REQUEST['queue']);
			$queue_info = $trans_->send_command("get_queue_info");
			if (isset($queue_info['unreviewed_ads']) && isset($queue_info['locked_unreviewed_ads'])){
				$this->response->add_data('toolbar_queue', $queue_info['unreviewed_ads'] - $queue_info['locked_unreviewed_ads'] );
			}
		}
		$pricelist = array_merge(get_pricelist());

		/* Queue names as page name */
		$this->response->add_data('page_name', bconf_get($BCONF, "controlpanel.modules.adqueue.queue.{$_GET['queue']}"));
		$this->response->add_data('page_name_hide', '1');

		$this->response->fill_array('page_trail_link', $_SERVER['REQUEST_URI']);
		$this->response->fill_array('page_trail_name', bconf_get($BCONF, "controlpanel.modules.adqueue.queue.{$_GET['queue']}"));

		/* Admin ads limit */
		$limit = Cpanel::hasPriv('adqueue.show_num_ads') ? $_SESSION['controlpanel']['admin']['privs']['adqueue.show_num_ads'] : 1;

		$this->response->fill_array('page_js', "/tjs/arrays_v2.js");
		$this->response->fill_array('page_js', "/js/adqueues.js");
		$this->response->fill_array('page_js', "/js/category_dropdown.js");
		$this->response->fill_array('page_js', "/js/events.js");
		$this->response->add_data('limit', $limit);

		/* Create a new transaction */
		$transaction = new bTransaction();
		/* Request and lock */
		$transaction->add_data('lock', @intval($_GET['lock'])); // XXX Permission check
		$transaction->add_data('get_associated_ads', 1);
		$transaction->add_data('limit', $limit);
		$transaction->add_data('filter_name', $_REQUEST['queue']);

		if (isset ($_REQUEST['offset']))
			$transaction->add_data('offset', $_REQUEST['offset']);

		/* Search ads in queue by email */
		if (@intval($_GET['lock']) == 0 && !empty($_GET['email'])) {
			$transaction->add_data('email', strtolower($_GET['email']));
		}

		// Verify if into query url ad_id and action_id are initialized together
		// and they are greater than 0
		if((empty($_GET['ad_id']) && !empty($_GET['action_id']))
			|| (!empty($_GET['ad_id']) && empty($_GET['action_id']))
			|| (isset($_GET['ad_id']) && ((int)$_GET['ad_id'])<=0 )
			|| (isset($_GET['action_id']) && ((int)$_GET['action_id'])<=0)
			|| ((!isset($_GET['ad_id']) && !isset($_GET['action_id'])) && isset($_GET['single']) && $_GET['single']=='1')
		) {
			$this->response->add_error('err_error', 'ERROR_URL_INVALID');
			$this->display_results('controlpanel/adqueue_error.html');
			return;
		}

		/* Get specific ad_action */
		if (@intval($_GET['lock']) == 0 && !empty($_GET['ad_id']) && !empty($_GET['action_id'])) {
			$transaction->add_data('ad_id', $_GET['ad_id']);
			$transaction->add_data('action_id', $_GET['action_id']);
		}

		$reply = $transaction->send_admin_command('adqueues');
		Cpanel::handleReply($reply, $this->response);

		if (!$this->response->has_error()) {
			/* Bind the transaction results to the response object */
			/* Transaction returns in the format ad.0.ad.name:pelle, so we have to select these out. */

			/* Sort the ads */
			ksort($reply['ad'], SORT_NUMERIC);

			/* Get the last ad id */
			$last_ad = end($reply['ad']);
			$last_ad_id = $last_ad['ad']['ad_id'];
			/* Get all ads and create a response for each one */
			foreach ($reply['ad'] as $ad) {
				$temp_response = $this->response;
				$this->response = new bResponse();
				$this->response->add_data('appl', 'controlpanel');
				$this->response->add_data('filter_name', $_GET['queue']);
				$this->response->add_data('ad_id', $ad['ad']['ad_id']);
				$this->response->add_data('action_id', $ad['actions']['action_id']);
				$this->response->add_data('limit', $limit);
				$this->response->add_data('lock', @intval($_GET['lock']));
				$this->response->add_data('pricelist', $pricelist);

				/* Add the evaluation results, if any */
				if (isset($ad['params']['ad_evaluation_result']))
					$this->response->add_data('evaluation_result', $ad['params']['ad_evaluation_result']);
				if (isset($ad['params']['ad_evaluation_reason']))
					$this->response->add_data('evaluation_reason', $ad['params']['ad_evaluation_reason']);
				if (isset($ad['action_params']['ad_evaluation_result']))
					$this->response->add_data('evaluation_result', $ad['action_params']['ad_evaluation_result']);
				if (isset($ad['action_params']['ad_evaluation_reason']))
					$this->response->add_data('evaluation_reason', $ad['action_params']['ad_evaluation_reason']);


				if (isset($ad['params']['link_type']))
					$this->response->add_data('is_bulkload', '1');

				if ($ad['ad']['category'] == "2020"){

					$ad_appraisal = array(
						"category" => $ad['ad']['category'],
						"price" => @$ad['ad']['price']
					);

					foreach(array('brand','model','version','regdate','fuel','gearbox') as $par) {
						if(isset($ad['params'][$par])) {
							$ad_appraisal[$par]=$ad['params'][$par];
						}
					}

					/*
					 * FT 409: we remove the valition for "list_id" because when an ads is rejectect, this, doesn't have "list_id"
					 */
					foreach(array('brand','model','version','regdate','fuel','gearbox') as $par) {
						if(isset($ad['ad_change_params']["param_$par"]) && $ad['ad_change_params']["param_$par"]!="") {
							$ad_appraisal[$par]=$ad['ad_change_params']["param_$par"];
						}
					}

					if(isset($ad['ad_change_params']['category'])) {
						$ad_appraisal['category']=$ad['ad_change_params']['category'];
					}
					if(isset($ad['ad_change_params']['price'])) {
						$ad_appraisal['price']=$ad['ad_change_params']['price'];
					}

					/* TODO In the future, fix this to get the categories dynamically */
					if(isset($ad_appraisal['price'])){
						$price_color = price_alert($ad_appraisal, $ad_appraisal['price'], $ad_appraisal['category']);
						$this->response->add_data('price_color', $price_color);

						if (isset($ad['params']) && isset($ad['params']['country'])){
							$this->response->add_data('country', $ad['params']['country']);
						}

						/* CHUS 296: ET 1: Add variable for template if brand or model are 'other'  */
						if(isset($ad_appraisal['brand']) && $ad_appraisal['brand'] == "0"){
							$this->response->add_data('brand_model_warn', 'Brand');
						} else if(isset($ad_appraisal['model']) && $ad_appraisal['model'] == "0"){
							$this->response->add_data('brand_model_warn', 'Model');
						}
					}
				}
				/* Get filter lists */
				if (Cpanel::hasPriv('filter.lists')) {
					$this->lists_by_type("email");
					$this->lists_by_type("general");
					$this->lists_by_type("ip");

					$this->response->add_data('filter_lists_priv', '1');
				}

				if (isset($ad['reg_action']['remote_addr'])) {
					$this->response->add_data('reg_action_remote_addr', $ad['reg_action']['remote_addr']);
					$this->response->add_data('reg_action_remote_addr_country', get_geoip_country($ad['reg_action']['remote_addr']));
					$this->response->add_data('reg_action_remote_addr_isocountry', get_isoname_geoip_countr($ad['reg_action']['remote_addr']));
				}

				if ($ad['uid']['count'] > 1)
					$this->response->add_data('uid_count', $ad['uid']['count']);
				$this->response->add_data('last_ad_id', $last_ad_id);

				if (isset($ad['ad']['store_id']))
					$this->response->add_data('store', $ad['ad']['store_id']);

				unset($refusals);
				if ($this->dyn_bconf = get_dynamic_bconf()) {
					/* Update settings array */
					$refusals = bconf_get($this->dyn_bconf, '*.*.common');
					$this->response->add_extended_array($refusals);
				} else {
					$this->dyn_bconf = false;
				}

				if (isset($ad['prev_refused']['reason']))
					$this->response->add_data('prev_refused_reason', $ad['prev_refused']['reason']);

				if (isset($ad['pre_refuse_action_type']['action_type']))
					$this->response->add_data('pre_refuse_action_type_action_type',
							$ad['pre_refuse_action_type']['action_type']);

				if (isset($ad['refused']['reason']))
					$this->response->add_data('refused_reason', $ad['refused']['reason']);

				if (array_key_exists('upselling',$ad) || array_key_exists('upselling_in_app',$ad)){
					$prefix_upselling = 'upselling';
					if (array_key_exists('upselling_in_app',$ad)){
						$prefix_upselling = 'upselling_in_app';
					}

					$this->response->add_data('upselling_action_id', $ad[$prefix_upselling]['action_id']);
					$this->response->add_data('upselling_action_type', $ad[$prefix_upselling]['action_type']);
					$this->response->add_data('upselling_current_state', $ad[$prefix_upselling]['current_state']);
					$this->response->add_data('upselling_receipt', $ad[$prefix_upselling]['receipt']);
					$this->response->add_data('upselling_status', $ad[$prefix_upselling]['status']);
					$this->response->add_data('upselling_product_id', $ad[$prefix_upselling]['product_id']);
					$this->response->add_data('upselling_payment_group_id', $ad[$prefix_upselling]['payment_group_id']);
				}
				if (array_key_exists('inserting_fee', $ad)) {
					$prefix = 'inserting_fee';
					$this->response->add_data('inserting_fee_action_id', $ad[$prefix]['action_id']);
					$this->response->add_data('inserting_fee_action_type', $ad[$prefix]['action_type']);
					$this->response->add_data('inserting_fee_current_state', $ad[$prefix]['current_state']);
					$this->response->add_data('inserting_fee_receipt', $ad[$prefix]['receipt']);
					$this->response->add_data('inserting_fee_status', $ad[$prefix]['status']);
					$this->response->add_data('inserting_fee_product_id', $ad[$prefix]['product_id']);
					$this->response->add_data('inserting_fee_payment_group_id', $ad[$prefix]['payment_group_id']);
					$this->response->add_data('inserting_fee_state', $ad[$prefix]['state']);
				}
				/* Determine the queue_description */
				$queue_description = null;
				if ($ad['ad']['status'] == 'unpublished' || $ad['ad']['status'] == 'active') {
					$queue_description = 'published';
				} else if ($ad['ad']['status'] == 'deleted') {
					$queue_description = 'deleted';
				} else if ($ad['actions']['state'] == 'refused') {
					$queue_description = 'refused';
				} else if ($ad['actions']['queue'] == 'abuse') {
					$queue_description = 'abuse';
				} else if ($ad['actions']['queue'] == 'unsolved') {
					$queue_description = 'unsolved';
				} else if ($ad['actions']['queue'] == 'technical_error') {
					$queue_description = 'technical_error';
				}

				if ($queue_description) {
					$this->response->add_data('queue_description', $queue_description);
				}

				if (isset($ad['params']['inserting_fee'])) {
					$this->response->add_data('is_inserting_fee', $ad['params']['inserting_fee']);
				}

				/* CHUS681 Verifing if the ad comes from whitelist */
				$this->response->add_data('is_in_whitelist', $ad['is_in_list']['whitelist']);


				/* Set notice_abuse information */
				$this->response->add_data("priv_notice_abuse",
						Cpanel::hasPriv('notice_abuse') ? 1 : 0);

				/* Add an admin flag to the template */
				$this->response->add_data('is_admin', 1);
				/* Deactivated ads*/
				if(isset($ad['deactivated'])){
					foreach ($ad['deactivated'] as $count => $subarr) {
						foreach ($subarr as $key => $value) {
							$this->response->fill_array('deactivated_'.$key, $value);
						}
					}
				}

				/* AD CHANGES */
				$this->handle_show_ad_reply($ad);
				$duplicates = $this->find_duplicates($ad);

				usort($duplicates, 'dupper_sort');

				foreach ($duplicates as $count => $subarr) {
					foreach ($subarr as $key => $value) {
						$this->response->fill_array($key, $value);
					}
				}

				if (isset($ad['user_ads']['list_id'])) {
					/* Show un maximum of 10 published ads */
					$published_user_ads = 0;
					$publish_ads_limit = bconf_get($BCONF, '*.controlpanel.common.publish_ads_limit');
					foreach ($ad['user_ads']['list_id'] as $list_id) {
						$this->response->fill_array('user_ads', $list_id);
						if (++$published_user_ads >= $publish_ads_limit)
							break;
					}
				}

				/* Add controlpanel bconf-data to template. */ 
				$this->response->set_bconf_application('controlpanel');

				if (intval(@$_GET['lock'])) {
					global $session_read_only;
					$session_read_only = false;

					/* Store ad in session */
					$_SESSION['controlpanel']['adqueue']['show_ad']['ad_id'][] = $ad['ad']['ad_id'];
					$_SESSION['controlpanel']['adqueue']['show_ad']['action_id'][] = $ad['actions']['action_id'];
				}

				if ($error_message) {
					$this->response->add_data('error_message', $error_message);
					$error_message = null;
				}

				/*Get refusal reasons*/
				unset($refusals);
				$debug_start = microtime(true);
				if ($this->dyn_bconf = get_dynamic_bconf()) {
					$refusals = bconf_get($this->dyn_bconf, '*.*.common');
					$debug_end = microtime(true) - $debug_start;
					bLogger::logError(__METHOD__, "Fetched common bconf for showad: " . count($refusals) . " on " . $debug_end. " seconds");
					$this->response->add_extended_array($refusals);
				} else {
					$debug_end = microtime(true) - $debug_start;
					bLogger::logError(__METHOD__, "Failed fetch common bconf for showad. duration: " . $debug_end);
					$this->dyn_bconf = false;
				}
				$debug_end = microtime(true) - $debug_start;
				bLogger::logError(__METHOD__, "Total fetch common bconf time: " . $debug_end);

				/* Fix to set nl2br for item_desc. Could not be done in template since its used for both vi and cp-vi */
				$word_marks = $this->get_word_marks($this->response->data['category'], $this->response->data['type']);
				foreach($this->response->data as $key => $value) {
					if(strpos($key, 'item_desc') === 0) {
						$this->response->data[$key] = nl2br($this->apply_word_marks("body", $value, $word_marks));
					}
				}
				$this->add_results($this->response, 'controlpanel/adqueue_show_ad.html');
				$this->response = $temp_response;
			}

			/* Recieved ad, store the ad id and lock time in session */
			if (!@intval($_GET['lock'])) {
				/* Add footer page navigation. */
				$total = (isset($reply['total']))?$reply['total'][$_GET['queue']]:0;
				$offset = $reply['offset'];
				$this->response->add_data('total', $total);
				$this->response->add_data('offset', $offset);
				$this->response->add_data('offset_uri', get_request_uri('offset'));

				$page_max_count = bconf_get($BCONF, 'controlpanel.modules.adqueue.max_page_count');
				$page_count = intval(ceil($total / $limit));
				$start_page_count = (int)max(0, floor($offset/$limit) - $page_max_count/2);
				$end_page_count = (int)min($page_count, $start_page_count + $page_max_count);
				for ($i = $start_page_count; $i < $end_page_count; $i++) {
					$this->response->fill_array('offset_page', $i * $limit);
					$this->response->fill_array('offset_page_nr', $i + 1);
				}

				// Previous page
				if ($offset - $limit >= 0) {
					$this->response->add_data('offset_page_prev', $offset - $limit);
				}

				// Next and last page link
				if ($offset + $limit < $total)  {
					$this->response->add_data('offset_page_next', $offset + $limit);
					$this->response->add_data('offset_page_last', ($page_count - 1) * $limit);
				}
			}

			/* Show the email of the last ad */
			if($commit_email) { 
				$this->response->add_data('last_commited_ad_email', $commit_email);
			}

			unset($refusals);
			if ($this->dyn_bconf = get_dynamic_bconf()) {
				$refusals = bconf_get($this->dyn_bconf, '*.*.common');
				$this->response->add_extended_array($refusals);
			} else {
				$this->dyn_bconf = false;
			}
			/* Display footer */
			$this->add_results($this->response, 'controlpanel/adqueue_show_ad_footer.html');
		} else {
			/* Check for error type */
			$error = $this->response->get_error();
			$total = $reply['total'][$_GET['queue']];
			if ($error == 'ERROR_QUEUE_EMPTY' && isset($_GET['offset']) && $_GET['offset'] - $limit >= 0) {
				$new_offset = $total - $limit;
				if ($new_offset < 0)
					$new_offset = 0;
				$this->response->redirect(preg_replace("/&offset=[0-9]+/", "&offset=$new_offset", bconf_get($BCONF, '*.common.base_url.controlpanel').$_SERVER['REQUEST_URI']));
			}
			$this->response->add_data("error_message", lang($this->response->get_error()));
			$this->show_adqueues();
		}
	}

	function get_word_marks($category, $type) {
		global $BCONF;
		$res = array();
		$category_array = array();
		$root = '*.controlpanel.modules.adqueue.highlight.text';
		$arr = bconf_get($this->dyn_bconf, $root);
		$type_short = bconf_get($BCONF, '*.common.type.' . $type . '.short_name');

		if(!$arr)
			return $res;

		/* Check for old category */
		if($category < 1000)
			$category_array = $this->get_new_category($category);
		else 
			$category_array[] = $category;

		foreach($category_array as $category_value) {
			foreach ($arr as $word) {
				if (isset($word['categories']) && $word['categories'] != '*') {
					$cats = explode(',', $word['categories']);
					if (!in_array($category_value, $cats))
						continue;
				}
				if (isset($word['types']) && $word['types'] != '*') {
					$types = explode(',', $word['types']);
					if (!in_array($type_short, $types))
						continue;
				}
				$res[] = $word;
			}
		}

		return $res;
	}

	/* Displays two ads next to eachother */
	function show_ad_split() {
		if (empty($_GET['ad_id']) || empty($_GET['action_id']))
			exit(0);

		$transaction = new bTransaction();
		/* Request and lock */
		$transaction->add_data('lock', 0);
		$transaction->add_data('filter_name', 'all');
		$transaction->add_data('ad_id', $_GET['ad_id']);
		$transaction->add_data('action_id', $_GET['action_id']);

		$reply = $transaction->send_admin_command('adqueues');

		Cpanel::handleReply($reply, $this->response);

		if (!$this->response->has_error()) {
			$ad = $reply['ad']['0'];

			$orig_ad_response = new bResponse();
			$orig_ad_response->add_data('ad_id', $ad['ad']['ad_id']);
			$orig_ad_response->add_data('action_id', $ad['actions']['action_id']);

			/* Determine the queue_description */
			$queue_description = null;
			if ($ad['ad']['status'] == 'unpublished' || $ad['ad']['status'] == 'active') {
				$queue_description = 'published';
			} else if ($ad['ad']['status'] == 'deleted') {
				$queue_description = 'deleted';
			} else if ($ad['actions']['state'] == 'refused') {
				$queue_description = 'refused';
			} else if ($ad['actions']['queue'] == 'abuse') {
				$queue_description = 'abuse';
			} else if ($ad['actions']['queue'] == 'unsolved') {
				$queue_description = 'unsolved';
			} else if ($ad['actions']['queue'] == 'technical_error') {
				$queue_description = 'technical_error';
			}

			if ($queue_description) {
				$orig_ad_response->add_data('queue_description', $queue_description);
			}

			/* Add an admin flag to the template */
			$orig_ad_response->add_data('is_admin', 1);

			/* ORIGINAL AD */
			$orig_ad  = new bAd();
			$orig_ad->populate($ad);

			$orig_ad_response->merge_array($orig_ad);
			/* Justify original ad - i.e. convert \n to <br>... */
			$this->justify_ad($orig_ad_response->data);

			/* Add controlpanel bconf-data to template. */
			$orig_ad_response->set_bconf_application('controlpanel');

			/* Copy the response. */
			if (PHP_VERSION >= "5")
				$this->response = clone ($orig_ad_response);
			else
				$this->response = $orig_ad_response;

			$orig_ad_response->add_data('orig_ad_view', '1');
			if (isset ($ad['ad']['image']))
				$orig_ad_response->fill_array('images', substr($ad['ad']['image'], 0, 2).'/'.$ad['ad']['image']);
			if (@$ad['images']) {
				foreach ($ad['images'] as $image)
					$orig_ad_response->fill_array('images', substr($image['name'], 0, 2).'/'.$image['name']);
			}
			foreach($ad['users'] as $key => $value)
				$orig_ad_response->add_data("users_".$key, $value);

			/* AD CHANGES */
			$this->handle_show_ad_reply($ad);
			$this->add_data_orgad($orig_ad_response, $ad);

			$changed_ad = $this->response->call_template('controlpanel/adqueue_ad_view.html', true);
			$original_ad = $orig_ad_response->call_template('controlpanel/adqueue_ad_view.html', true);

			$this->response->add_data('changed_ad', $changed_ad);
			$this->response->add_data('original_ad', $original_ad);
			$this->response->call_template('controlpanel/show_ad_split.html');
		}
		exit(0);
	}

	function justify_ad(&$ad) {
		$ad['body'] = nl2br($ad['body']);
	}

	function set_values($setting, $key, $value, $data = null) {
		if ($data)
			$data .= ',' . $key;
		else
			$data = $key;

		if ($value)
			$data .= ":" . $value;
	}

	function key_lookup($setting, $key, $data = null) {
		global $BCONF;

		/* Need special case for type */
		if ($key == "parent") {
			return bconf_get($BCONF, "*.cat.{$this->temp_ad['ad']['category']}.parent");
		}
		elseif ($key == "type" && isset($this->temp_ad['ad'][$key])) {
			return bconf_get($BCONF, '*.common.type.' . $this->temp_ad['ad'][$key] . '.short_name');
		}
		else if (isset($this->temp_ad['ad'][$key])) {
			return $this->temp_ad['ad'][$key];
		}
		else {
			return @$this->temp_ad['params'][$key];
		}
	}

	/* XXX Bits out of handle_show_ad_reply to fix data for orig_ad_response in show_ad_split */
	/* Add ad data to response object  */
	function add_data_orgad(&$response, &$ad) {
		global $BCONF;

		$cat = $ad['ad']['category'];
		$type = $ad['ad']['type'];

		/* Export ad_params as extended array to template */

		$type_shortname = bconf_get($BCONF, '*.common.type.' . $type . '.short_name');

		$ext_params = array();
		$ext_params = array_merge($ext_params, @$ad['params']);


		/* Ad data */
		foreach($ad['ad'] as $key => $value) {
			if ($key == 'body')
				$value = nl2br($value);
		       	$response->add_data($key, $value);
		}

		/* Ad params */
		foreach($ad['params'] as $key => $value)
		       	$response->add_data($key, $value);

		if (isset($ext_params) && is_array($ext_params))
			$response->add_extended_array('ext_params', $ext_params);
	}

	function handle_show_ad_reply(&$ad) {
		global $BCONF;

		$org_ad = array();
		$weekd_off = "";
		$weekd_peak = "";

		$cat = $ad['ad']['category'];
		if (isset($ad['ad_change_params']['category']))
			$cat = $ad['ad_change_params']['category'];
		$type = $ad['ad']['type'];
		if (isset($ad['ad_change_params']['type']))
			$type = $ad['ad_change_params']['type'];
		$word_marks = $this->get_word_marks($cat, $type);

		if (($cat_node = bconf_get($BCONF, "*.old_cat.$cat"))) {
			/*
			 * Since we no longer keep track of params for the old categories,
			 * just render everything.
			 */
			$adp_list = array();
			foreach (bconf_get($BCONF, "*.common.ad_params") as $adp)
				$adp_list[] = $adp['name'];
		} else {
			$this->temp_ad = $ad;

			get_settings(bconf_get($BCONF,"*.category_settings"), "types", array($this, "key_lookup"), array($this, "set_values"), $types);
			$types = explode(',', $types);
			$this->response->add_data('category_types', $types);
			get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $adp_list);
			$adp_list = explode(',', $adp_list);

			unset($this->temp_ad);

		}

		$ext_params = array();
		if (isset($ad['params'])) {
			if (is_array($ad['params']))
				$ext_params = array_merge($ext_params, $ad['params']);
			else
				$ext_params = array_merge($ext_params, array($ad['params']));
		}

		/* Strip _param from keys to get common key names */
		if (isset($ad['ad_change_params'])) {
			foreach ($ad['ad_change_params'] as $key => $value) {
				/* Check if key is an ad param */
				$key = str_replace('param_', "", $key);

				if (in_array($key, $adp_list)) {
					$ext_params[$key] = $value;
					/* If it's an old param store it */
					if (isset($ad['params']) && array_key_exists($key, $ad['params']))
						$ext_params_old[$key] = $ad['params'][$key];
				}
			}
		}

		if (isset($ext_params) && is_array($ext_params))
			$this->response->add_extended_array('ext_params', $ext_params);
		if (isset($ext_params_old) && is_array($ext_params_old))
			$this->response->add_extended_array('ext_params_old', $ext_params_old);

		/* Ad data */
		foreach($ad['ad'] as $key => $value) {
			if (isset($ad['ad_change_params'][$key])) {
				switch ($key) {
					case 'infopage_title':
						$this->response->add_data('new_infopage_title', $ad['ad_change_params'][$key]);
					case 'subject':
					case 'body':
						$new_text = htmlspecialchars($ad['ad_change_params'][$key]);
						$old_text = htmlspecialchars($value);
						$text = text_diff($old_text, $new_text);
						if ($key != 'infopage_title')
							$text = $this->apply_word_marks($key, $text, $word_marks);
						if ($key == 'body')
							$text = nl2br($text);
						$this->response->add_data($key, $text);
						$this->response->add_data($key.'_new', $new_text);
						break;

					default:

						if ($value != $ad['ad_change_params'][$key]) {
							$this->response->add_data('old_'.$key, $value);
						}

						if (!empty($ad['ad_change_params'][$key])) {
							$this->response->add_data($key, $ad['ad_change_params'][$key]);
						}
						break;
				}
                                /* Unset variable so we don't reuse it later */
                                unset($ad['ad_change_params'][$key]);
			} else {
				if (in_array($key, array('body', 'subject')))
					$value = $this->apply_word_marks($key, $value, $word_marks);
				if ($key == 'body')
					$value = nl2br($value);
			       	$this->response->add_data($key, $value);
				if (in_array($key, array('body', 'subject')))
					$this->response->add_data($key.'_new', htmlentities(strip_tags($value)));
			}
		}

                /* Check if new parameters is added */
		if (isset($ad['ad_change_params'])) {
			foreach ($ad['ad_change_params'] as $key => $value) {
				if (substr($key, 0, 6) == 'param_')
					continue;
				switch ($key) {
				case 'infopage_title':
				case 'subject':
				case 'body':
					$new_text = htmlspecialchars($value);
					$text = text_diff('', $new_text);
					if (in_array($key, array('body', 'subject')))
						$text = $this->apply_word_marks($key, $text, $word_marks);
					if ($key == 'body')
						$text = nl2br($text);
					$this->response->add_data($key, $text);
					break;

				default:
					$this->response->add_data('old_'.$key, '');
					$this->response->add_data($key, $value);
					break;
				}
			}
		}

		/* Ad params */
		if (isset($ad['params']) && is_array($ad['params'])) {
			foreach($ad['params'] as $key => $value) {
				if (isset($ad['ad_change_params']['param_' . $key])) {
					$this->response->add_data('old_' . $key, $value);
					if (!empty($ad['ad_change_params']['param_' . $key]))
						$this->response->add_data($key, $ad['ad_change_params']['param_' . $key]);
				} else
					$this->response->add_data($key, $value);
			}
		}

		/* If there's new ad_params*/
		if (isset($ad['ad_change_params']) && is_array($ad['ad_change_params'])) {
			foreach($ad['ad_change_params'] as $key => $value) {
				$param_name = str_replace("param_", "", $key);
				if (!isset($ad['params'][$param_name])) {
					$this->response->add_data('old_' . $param_name, "");
					if (!empty($ad['ad_change_params'][$key]))
						$this->response->add_data($param_name, $ad['ad_change_params'][$key]);

				}
			}
		}

		/* Ad actions */
		foreach($ad['actions'] as $key => $value) {
			if ($key == "paid_at") {
				$value = date("j M H:i", pgsql_mktime($value));
			}
                        if ($key == 'action_type' &&
                            $value == 'editrefused' &&
                            $ad['ad']['status'] != 'active') {
				$this->response->add_data("actions_show_refused", 1);
                                /* An editrefused is treated as new unless active on site. */
                                $value = 'new';
                        }

			$this->response->add_data("actions_".$key, $value);
		}

		if (isset($ad['payment'])) {
			foreach($ad['payment'] as $key => $value) {
				if (is_array($value)) {
					foreach ($value as $key => $val)
						$this->response->add_data("payment_" . $key, $val);
				} else
					$this->response->add_data("payment_" . $key, $value);
			}
		}

		foreach($ad['users'] as $key => $value) {
			$this->response->add_data("users_".$key, $value);
		}

		if (isset($ad['user_params']) && isset($ad['user_params']['refusal_divide_credit'])) {
			$this->response->add_data('refusal_divide_credit', 1);
		}

		if (isset($ad['previous_actions'])) {
			foreach($ad['previous_actions'] as $key => $value) {
				$this->response->add_data("previous_actions_".$key, $value);
			}
		}

		/* Ad Images */
		$images = array();

		/*
		 * FT1014: if the add is not accepted, then show the $ad['ad_image_changes']
		 * Otherwise only use $ad["images"]
		 */
		if (isset($ad['ad_image_changes']) && $ad['actions']['state'] != 'accepted') {
			for ($i = 0; isset($ad['ad_image_changes']["image$i"]["name"]); $i++) {
				array_push($images, $ad['ad_image_changes']["image$i"]["name"]);

				if ($ad['actions']['action_type'] == "edit" ||
				    $ad['actions']['action_type'] == "renew" ||
				    $ad['actions']['action_type'] == "editrefused" ||
				    $ad['actions']['action_type'] == "extra_images") {
					/* Fill in an array with changed images */
					$this->response->fill_array("new_images", $ad['ad_image_changes']["image$i"]["is_new"]);
				}
			}
		} elseif (@$ad['images']) {
			foreach ($ad['images'] as $key => $value)
				array_push($images, $value['name']);
		}


		foreach ($images as $key) {
			$this->response->fill_array("images",  substr($key, 0, 2) . "/" . $key);
		}

		/* Notices */
		Cpanel::addNoticesToResponse($ad, $this->response);

		/* Add an admin flag to the template */
		$this->response->add_data('is_admin', 1);
	}

	function show_adqueues() {
		global $BCONF;

		// Request always gets thru here

		//if($this->is_reviewer_data_pending()){
		$this->set_reviewer_data_toolbar();
		//}

		/* Hide page name */
		$this->response->add_data('page_name_hide', '1');

		/*
		 * Queue is not set, get queue menu
		 */
		if (!isset($_GET['queue']) || $this->response->has_error()) {
			/* Create a new transaction */
			$transaction = new bTransaction();
			$reply = $transaction->send_admin_command('adqueues');
			Cpanel::handleReply($reply, $this->response);

			if (isset($reply['total'])) {
				foreach($reply['total'] as $key => $val)
					$reply['unlocked'][$key] = $val - $reply['locked'][$key];
				$this->response->add_extended_array($reply);
			}

			if (Cpanel::hasPriv('adqueue.admin_queue')) {
				$this->response->add_data('show_no_lock_queues', 1);
			}

			$this->response->add_extended_array('cpanel_settings', $this->config['settings']);

			$this->display_results('controlpanel/adqueue_show_adqueues.html');
			return;
		}

		/*
		 * Show chosen queue
		 */
		$this->show_ad();
	}

	function apply_word_marks($key, $text, $word_marks) {
		global $BCONF;

		$ht = new HighlighText($BCONF);
		$text = $ht->getHtml($key, $text, $word_marks);

		if ($ht->frame_ad) {
			$this->response->add_data('frame_ad', 1);
		}

		if (!empty($ht->warning_type)) {
			$this->response->add_data('warning_type', $ht->warning_type);
		}

		return $text;
	}

	/*
	 * Highlighting of words 
	 */
	function highlight() {
		global $BCONF;
		$highlights = array();
		$root = '*.controlpanel.modules.adqueue.highlight.text';

		/* Get all highlights */
		if ($this->dyn_bconf) {
			$highlights = bconf_get($this->dyn_bconf, $root);
			if (is_array($highlights))
				ksort($highlights);
		}

		switch (@$_GET['action']) {
			case 'save':
				$page = 'controlpanel/highlight_form.html';

				if (!empty($_POST)) {
					/*
					 * Validator post input
					 */
					if (!isset($_GET['id'])) {
						/* Get the last highlight id */
						@end($highlights);
						/* Increment to get enw highlight id*/
						$id = @key($highlights) + 1;
					} else if (array_key_exists($_GET['id'], $highlights)) {
						$id = intval($_GET['id']);
					} else {
						$this->response->add_error('err_error', 'ERROR_HIGHLIGHT_ID_INVALID');
					}

					if (!empty($_POST['categories'])) {
						$all = true;
						$str_cats = "";

						foreach ($BCONF['*']['cat'] as $cat_id => $cat) {
							if ($cat['level'] >= 1 && !in_array($cat_id, $_POST['categories'])) {
								$all = false;
								break;
							}
						}

						$str_cats = implode(',', $_POST['categories']); 
						$new_highlight['categories'] = $all?'*':$str_cats;
					} else {
						$this->response->add_error('err_categories', 'ERROR_HIGHLIGHT_CATEGORIES_MISSING');
					}

					if (!empty($_POST['types'])) {
						$all = true;
						foreach ($BCONF['*']['common']['type']['list'] as $short => $type) {
							if ($short != 'a' && !in_array($short, $_POST['types'])) {
								$all = false;
								break;
							}
						}
						$new_highlight['types'] = $all?'*':implode(',', $_POST['types']);
					} else {
						$this->response->add_error('err_types', 'ERROR_HIGHLIGHT_TYPES_MISSING');
					}

					if (!empty($_POST['where'])) {
						$new_highlight['where'] = implode(',', $_POST['where']);
					} else {
						$this->response->add_error('err_where', 'ERROR_HIGHLIGHT_WHERE_MISSING');
					}

					if (empty($_POST['regex'])) {
						$this->response->add_error('err_regex', 'ERROR_HIGHLIGHT_REGEX_MISSING');
					} elseif (substr($_POST['regex'], -1) == '|' || substr($_POST['regex'], 0, 1) == '|' || strpos($_POST['regex'], '||') > 0) {
						$this->response->add_error('err_regex', 'ERROR_HIGHLIGHT_REGEX_PIPE');
					} else {
						$new_highlight['regex'] = $_POST['regex'];
					}

					$new_highlight['word_boundary'] = intval($_POST['word_boundary']);
					$new_highlight['frame_ad'] = intval($_POST['frame_ad']);

					if (!empty($_POST['warning_type'])) {
						$new_highlight['warning_type'] = sanitizeVar($_POST['warning_type'], 'string');
					}
					$new_highlight['pure_regex'] = intval($_POST['pure_regex']);
					if (isset($_POST['color']))
						$new_highlight['color'] = @intval($_POST['color']);
					else
						$this->response->add_error('err_color', 'ERROR_HIGHLIGHT_COLOR_MISSING');

					if (!$this->response->has_error()) {
						if (!save_dynamic_bconf($new_highlight, $root.'.'.$id)) {
							// Could not save config
							$this->response->add_error('err_error', 'ERROR_HIGHLIGHT_FAILED');
						} else {
							// Saved config
							$this->response->redirect(preg_replace("/&action=save/", "", bconf_get($BCONF, '*.common.base_url.controlpanel').$_SERVER['REQUEST_URI']));
						}
					}
				} else if (isset($_GET['id'])) {
					if (array_key_exists($_GET['id'], $highlights)) {

						/* Add dynbconf values to template variables */
						foreach ($highlights[$_GET['id']] as $key => $value) {
							/* Categories */
							if($key == 'categories') {
								if($value == '*') {
									$this->response->add_data('post_categories', '*');
								}

								$category_list = array();

								/* Check for new or old categories, if old, map to new values */
								foreach(explode(",", $value) as $inner_key => $inner_value) {
									if($inner_value < 1000)
										$category_list = array_merge($category_list, $this->get_new_category($inner_value));
									else 
										$category_list[] = $inner_value;
								}	

								$category_list = array_unique($category_list);
								sort($category_list);

								if(!empty($category_list)) 	
									$this->response->add_data('post_'.$key, $category_list);
							} elseif ($key == 'regex') {
								$this->response->add_data('post_'.$key, $value);
							} else {
								if (preg_match("/,/", $value)) {
									$this->response->add_data('post_'.$key, explode(",", $value));
								} else {
									$this->response->add_data('post_'.$key, $value);
								}
							}
						}
					} else {
						$this->response->redirect(preg_replace("/&action=save/", "", bconf_get($BCONF, '*.common.base_url.controlpanel').$_SERVER['REQUEST_URI']));
					}
				}

				break;
			case 'delete':
				$page = 'controlpanel/highlight.html';

				if (!empty($_POST["delete"]) && !save_dynamic_bconf(NULL, $root, $_POST['delete'])) {
					// Could not save config
					$this->response->add_error('err_error', 'ERROR_HIGHLIGHT_FAILED');
				} else {
					// Saved config
					$this->response->redirect(preg_replace("/&action=delete/", "", bconf_get($BCONF, '*.common.base_url.controlpanel').$_SERVER['REQUEST_URI']));
				}
				break;
			default:
				if (is_array($highlights))
				foreach ($highlights as $id => $highlight) {
					$this->response->fill_array('regex_id', $id);
					$this->response->fill_array('regex', $highlight['regex']);

					$translate = array('subject' => 'Heading', 'body' => 'Ad text');
					$this->response->fill_array('where', strtr($highlight['where'], $translate));

					$translate = array();
					foreach ($BCONF['*']['common']['type']['list'] as $k => $v)
						$translate[$k] = $v;
					$this->response->fill_array('types', strtr($highlight['types'], $translate));
					$this->response->fill_array('color', $highlight['color']);
					$this->response->fill_array('word_boundary', $highlight['word_boundary']);
					$this->response->fill_array('categories', $highlight['categories']);
				}
				$this->response->add_data('mod', array(0, 1));

				$page = 'controlpanel/highlight.html';
				break;
		}

		$this->display_results($page);
	}

	function get_new_category($category) {
		global $BCONF;
		$new_cat = array();

		if ($category  > 0 && $category < 1000) {
			foreach (bconf_get($BCONF, "*.cat") as $key => $cat) {
				if (array_key_exists('old', $cat) &&
				    array_key_exists('cat', $cat['old']) &&
				    $category == $cat['old']['cat']) {

					$new_cat[] = intval($key);

					/* Add parent */
					if ($cat['level'] >= 2)
						$new_cat[] = intval($cat['parent']);
				}
			}
		}

		return $new_cat;
	}

	function is_positive_integer($str) {
		return (is_numeric($str) && $str >= 0 && $str == round($str));
	}

	function find_duplicates(&$ad) {
		$dup_ret = array();
		if (@$ad['associated']) {
			foreach ($ad['associated'] as $count => $dup_ad) {
				foreach ($dup_ad as $key => $value) {
					$dup_ret[$count]["duplicates_".$key] = $value;

					/* runs only for same category dup ads */
					if ($dup_ad['category'] == @$ad['ad']['category'] &&
							$key == "params") {
						$dup_params = array();
						foreach(explode(",", $value) as $p) {
							$v = explode(":", $p);
							$dup_params[$v[0]] = $v[1];
						}

						foreach($dup_params as $p => $v) {
							if (isset($ad['params'][$p])) {
								$score = ($v == @$ad['params'][$p]) ? "true" : "false";
								$dup_ret[$count]["duplicates_param_".$p] = $score;
							}
						}
					}
				}
			}
		}
		return $dup_ret;
	}

	/*
	 * Settings
	 */
	function settings() {
		$page = 'controlpanel/settings.html';
		$root = '*.controlpanel.modules.adqueue.settings';

		foreach ($this->config['settings'] as $key => $value)
			$this->response->add_data($key, $value);

		if (count($_POST)) {
			$new_conf['auto_abuse'] = isset($_POST["auto_abuse"])?1:0;
			$new_conf['autoreview'] = isset($_POST["autoreview"])?1:0;
			$new_conf['lock_time'] = intval($_POST["lock_time"]) > 0?intval($_POST['lock_time']):30;
			if (save_dynamic_bconf($new_conf, $root)) {
				$this->response->add_data('status', lang('SETTINGS_SAVED'));
				$this->config['settings']['auto_abuse'] = $new_conf['auto_abuse'];
				$this->config['settings']['autoreview'] = $new_conf['autoreview'];
				$this->config['settings']['lock_time'] = $new_conf['lock_time'];
				foreach ($this->config['settings'] as $key => $value)
					$this->response->add_data($key, $value);
			} else {
				$this->response->add_data('status', lang('SETTINGS_ERROR'));
				$this->response->add_error('error', 1);
				foreach ($_POST as $key => $value)
					$this->response->add_data($key, $value);
			}
		}

		$this->display_results($page);
	}

	/*
	 *Refusals
	 */
	function refusals_add($new_subject , $text , $below_id , $new_id, $refusals) {

		if (isset($below_id) && !empty($below_id)) {
			if (!in_array($below_id, $refusals['refusal_order'])) {
				$this->response->add_error('err_error', 'ERROR_BELOW_SUBJECT_MISSING');
				return;
			}
		}

		if (empty($new_subject)) {
			$this->response->add_error('err_error', 'ERROR_SUBJECT_EMPTY');
			return;
		}

		if (!preg_match('/^[a-z0-9_]+$/', $new_id)) {
			$this->response->add_error('err_error', 'ERROR_INVALID_CHARS_IN_ID_OR_EMPTY');
			return;
		}

		if (empty($text)) {
			$this->response->add_error('err_error', 'ERROR_TEXT_EMPTY');
			return;
		}

		if (isset($refusals['refusal']) && is_array($refusals['refusal'])) {
			foreach ($refusals['refusal'] as $key => $value) {
				if ($new_subject == $value['name']) {
					$this->response->add_error('err_error', 'ERROR_SUBJECT_EXISTS');
					return;
				}
				if ($new_id == $key) {
					$this->response->add_error('err_error', 'ERROR_ID_EXISTS');
					return;
				}
			}
		}

		// I don't think it's needed, since we iterate above, but I don't have time to check - Zane
		if (is_array($refusals['refusal']) && isset($refusals['refusal_order']) && in_array($new_id, $refusals['refusal_order'])) {
			$this->response->add_error('err_error', 'ERROR_ID_EXISTS');
			return;
		}

		$this->ads_evaluator->reasonID = (int)$new_id;
		$this->ads_evaluator->reasonName = $new_subject;
		list($status, $msg) = $this->ads_evaluator->insertReasons();
		if (!$status) {
			$this->response->add_error('err_error', 'ADS_EVALUATOR_CREATE_ERROR');
			return;
		}

		$counter = 0;
		$position = 0;

		/* Move all elements */
		$refusal_orders = array();
		if(empty($below_id)){
			$refusal_orders[$counter] = $new_id;
			$counter++;
		}	
		if(!empty($refusals['refusal_order'])){
			foreach ($refusals['refusal_order'] as $key => $value) {
				if ($below_id != $value){
					$refusal_orders[$counter] = $value;
					$counter++;
				}
				else{
					$refusal_orders[$counter] = $value;
					$counter++;
					$refusal_orders[$counter] = $new_id;
					$counter++;
				}
			}
		}
		$refusals['refusal_order'] = $refusal_orders;

		if (!save_dynamic_bconf($refusals['refusal_order'], '*.*.common.refusal_order', NULL, NULL, true)) {
			/* Could not add */
			$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
		}

		$new_refusal['name'] = $new_subject;
		$new_refusal['text'] = $text;
		if (!save_dynamic_bconf($new_refusal, '*.*.common.refusal.' . $new_id)) {
			/* Could not save config */
			$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
			list($status, $msg) = $this->ads_evaluator->deleteReasons();
			if (!$status) {
				$this->response->add_error('err_error', 'ADS_EVALUATOR_SYNC_ERROR');
			}
			return;
		}
		$this->response->add_error('ok_okey', 'OKEY_REFUSAL_REASON_ADD');
	}

	function refusals_remove($id, $refusals) {
		$counter = 0;

		if (isset($refusals['refusal']) && is_array($refusals['refusal'])) {
			$refusal_orders = array();
			foreach ($refusals['refusal_order'] as $key => $value) {
				if ($id != $value){
					$refusal_orders[$counter] = $value;
					$counter++;
				}
				else
					$position = $counter;
			}
		}

		if (isset($position)) {
			$error_saving = false;

			$refusals['refusal_order'] = $refusal_orders;
			$refusal_subject = $refusals['refusal'][$id]['name'];
			unset($refusals['refusal'][$id]);

			$this->ads_evaluator->reasonID = (int)$id;
			list($status, $msg) = $this->ads_evaluator->deleteReasons();
			if (!$status) {
				$this->response->add_error('err_error', 'ADS_EVALUATOR_DELETE_ERROR');
				return;
			}

			if (!save_dynamic_bconf(NULL, '*.common.refusal_order', NULL, NULL, true)) {
				/* Could not delete */
				$this->response->add_error('err_error', 'ERROR_REFUSALS_DELETE_FAILED');
				$error_saving = true;
			}
			if (!save_dynamic_bconf($refusals['refusal_order'], '*.*.common.refusal_order', NULL, NULL, true)) {
				/* Could not add */
				$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
				$error_saving = true;
			}
			if (!save_dynamic_bconf(NULL, '*.*.common.refusal.' . $id)) {
				/* Could not delete */
				$this->response->edd_error('err_error', 'ERROR_REFUSALS_DELETE_FAILED');
				$error_saving = true;
			}

			if (!$error_saving) {
				$this->response->add_error('ok_okey', 'OKEY_REFUSAL_REASON_REMOVE');
			} else {
				$this->ads_evaluator->reasonName = $refusal_subject;
				list($status, $msg) = $this->ads_evaluator->insertReasons();
				if (!$status) {
					$this->response->add_error('err_error', 'ADS_EVALUATOR_SYNC_ERROR');
				}
			}
		}
	}

	function refusals() {
		global $BCONF;
		/* Hide page name */
		$this->response->add_data('page_name_hide', '1');

		/* Get dynamic bconf */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$refusals = bconf_get($this->dyn_bconf, '*.*.common');
		} else {
			$this->dyn_bconf = false;
		}
		if (isset($_POST['refusal_submit'])) {
			$refusal_submit=$_POST['refusal_submit'];
			if (isset($_POST['refusal_text']))
				$refusal_text=$_POST['refusal_text'];
			if (isset($_POST['refusal_subject']))
				$refusal_id=$_POST['refusal_subject'];
			if (isset($_POST['refusal_id']))
				$refusal_new_id=$_POST['refusal_id'];
			if (isset($_POST['refusal_new_subject']))
				$refusal_new_subject=$_POST['refusal_new_subject'];
			if ($refusal_submit == 'Save') {
				if (isset($refusal_text) && !empty($refusal_text) && isset($refusal_id) && !empty($refusal_id) && isset($refusals['refusal'][$refusal_id]['name'])) {
					$new_refusal['name'] = $refusals['refusal'][$refusal_id]['name'];
					if (isset($refusals['refusal'][$refusal_id]['extra_msg']))
						$new_refusal['extra_msg'] = $refusals['refusal'][$refusal_id]['extra_msg'];
					$clean_text = strip_tags($refusal_text);
					$clean_text = preg_replace("/[\r\n\t]+/", " ", $clean_text);
					$new_refusal['text'] = $clean_text;
					if (!save_dynamic_bconf($new_refusal, '*.*.common.refusal.' . $refusal_id)) {
						// Could not save config
						$this->response->add_error('err_error', 'ERROR_SAVE_REFUSAL_FAILED');
					}
					else {
						$this->response->add_error('ok_okey', 'OKEY_REFUSAL_REASON_MODIFY');
					}
				} else {
					$this->response->add_error('err_error', 'ERROR_TEXTFIELD_EMPTY');
				}
			} elseif ($refusal_submit == 'Delete') {
				if (isset($refusal_id))
					$this->refusals_remove($refusal_id, $refusals);
			} elseif (preg_match("/Add/",$refusal_submit, $regs)) {
				if(isset($refusal_text) && isset($refusal_id) && isset($refusal_new_id)){
					$clean_text = strip_tags($refusal_text);
					$clean_text = preg_replace("/[\r\n\t]+/", " ", $clean_text);
					$this->refusals_add($refusal_new_subject , $clean_text , $refusal_id , $refusal_new_id, $refusals);
				}
			}
		}

		if (!isset($_GET['refusal_action']) ) {
			$this->response->add_data('edit_refusal', '1');
		} else {
			$refusal_action=$_GET['refusal_action'];
			if ($refusal_action =='new_refusal') {
				$this->response->add_data('new_refusal', '1');
			} elseif ($refusal_action == 'delete_refusal') {
				$this->response->add_data('delete_refusal', '1');
			} else {
				$this->response->add_data('edit_refusal', '1');
			}
		}

		unset($refusals);
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$refusals = bconf_get($this->dyn_bconf, '*.*.common');
			$this->response->add_extended_array($refusals);
		} else {
			$this->dyn_bconf = false;
		}
		$this->display_results('controlpanel/refusals.html');
	}
/*
Methods for edit refusal reasons score
*/
	function refusals_score() {
		global $BCONF;
		/* Hide page name */
		$this->response->add_data('page_name_hide', '1');

		list($ok, $refusals) = $this->ads_evaluator->getReasons();
		if (!$ok) {
			$this->response->add_error('err_error', 'ADS_EVALUATOR_GET_ERROR');
		}

		if (isset($_POST['refusal_submit']) && $ok) {
			$refusal_submit = $_POST['refusal_submit'];
			$refusal_scores = array();
			$textfield_int = True;
			foreach ($_POST as $key => $value) {
				if (preg_match("/refusal_scores:/", $key)) {
					list($resource_type, $refusal_id) = preg_split('/:/', $key);
					$textfield_int = ($this->is_positive_integer($value)) ? $textfield_int : False;
					array_push($refusal_scores, array('reasonID' => (int)$refusal_id, 'reasonScore' => (int)$value));
				}
			}
			if ($refusal_submit == 'Save') {
				if (isset($refusal_scores) && !empty($refusal_scores) && isset($refusals['reason']) && $textfield_int) {
					$this->ads_evaluator->editedReasons = $refusal_scores;
					list($status, $msg) = $this->ads_evaluator->editReasons();
					if ($status) {
						$this->response->add_error('ok_okey', 'ADS_EVALUATOR_OK');
					} else {
						$this->response->add_error('err_error', 'ADS_EVALUATOR_EDIT_ERROR');
					}
				} elseif (!$textfield_int){
					$this->response->add_error('err_error', 'ERROR_INVALID_POSITIVE_INT');
				} else {
					$this->response->add_error('err_error', 'ERROR_INVALID_CHARS_IN_ID_OR_EMPTY');
				}
			}
		}
		unset($refusals);
		list($ok, $refusals) = $this->ads_evaluator->getReasons();
		if (!$ok){
			$this->response->add_error('err_error', 'ADS_EVALUATOR_GET_ERROR');
		}
		$this->response->add_extended_array($refusals);
		$this->display_results('controlpanel/refusals_score.html');
	}

	/*
	Methods for accepted reasons
	*/
	function accepted() {
		global $BCONF;
		/* Hide page name */
		$this->response->add_data('page_name_hide', '1');

		/* Get dynamic bconf */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$accepteds = bconf_get($this->dyn_bconf, '*.*.common');
		} else {
			$this->dyn_bconf = false;
		}

		if (isset($_POST['accepted_submit'])) {
			$accepted_submit=$_POST['accepted_submit'];
			if (isset($_POST['accepted_text']))
				$accepted_text=$_POST['accepted_text'];
			if (isset($_POST['accepted_subject']))
				$accepted_id=$_POST['accepted_subject'];
			if (isset($_POST['accepted_id']))
				$accepted_new_id=$_POST['accepted_id'];
			if (isset($_POST['accepted_new_subject']))
				$accepted_new_subject=$_POST['accepted_new_subject'];

			if ($accepted_submit == 'Save') {
				if (isset($accepted_text) && !empty($accepted_text) && isset($accepted_id) && !empty($accepted_id) && isset($accepteds['accepted'][$accepted_id]['name'])) {
					$new_accepted['name'] = $accepteds['accepted'][$accepted_id]['name'];
					if (isset($accepteds['accepted'][$accepted_id]['extra_msg']))
						$new_accepted['extra_msg'] = $accepteds['accepted'][$accepted_id]['extra_msg'];
					$clean_text = strip_tags($accepted_text);
					$clean_text = preg_replace("/[\r\n\t]+/", " ", $clean_text);
					$new_accepted['text'] = $clean_text;
					if (!save_dynamic_bconf($new_accepted, '*.*.common.accepted.' . $accepted_id)) {
						// Could not save config
						$this->response->add_error('err_error', 'ERROR_SAVE_REFUSAL_FAILED');
					}
					else {
						$this->response->add_error('ok_okey', 'OKEY_ACCEPT_REASON_MODIFY');
					}
				} else {
					$this->response->add_error('err_error', 'ERROR_TEXTFIELD_EMPTY');
				}
			} elseif ($accepted_submit == 'Delete') {
				if (isset($accepted_id))
					$this->accepted_remove($accepted_id, $accepteds);
			} elseif (preg_match("/Add/",$accepted_submit, $regs)) {
				if(isset($accepted_text) && isset($accepted_id) && isset($accepted_new_id)){
					$clean_text = strip_tags($accepted_text);
					$clean_text = preg_replace("/[\r\n\t]+/", " ", $clean_text);
					$this->accepted_add($accepted_new_subject , $clean_text , $accepted_id , $accepted_new_id, $accepteds);
				}
			}
		}

		if (!isset($_GET['accepted_action']) ) {
			$this->response->add_data('edit_accepted', '1');
		} else {
			$accepted_action=$_GET['accepted_action'];
			if ($accepted_action =='new_accepted') {
				$this->response->add_data('new_accepted', '1');
			} elseif ($accepted_action == 'delete_accepted') {
				$this->response->add_data('delete_accepted', '1');
			} else {
				$this->response->add_data('edit_accepted', '1');
			}
		}

		unset($accepteds);

		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$accepteds = bconf_get($this->dyn_bconf, '*.*.common');
			$this->response->add_extended_array($accepteds);
		} else {
			$this->dyn_bconf = false;
		}

		$this->display_results('controlpanel/accepted.html');
	}

	function accepted_add($new_subject , $text , $below_id , $new_id, $accepteds) {
		if (isset($below_id) && !empty($below_id)) {
			if (!in_array($below_id, $accepteds['accepted_order'])) {
				$this->response->add_error('err_error', 'ERROR_BELOW_SUBJECT_MISSING');
				return;
			}
		}

		if (empty($new_subject)) {
			$this->response->add_error('err_error', 'ERROR_SUBJECT_EMPTY');
			return;
		}

		if (!preg_match('/^[a-z0-9_]+$/', $new_id)) {
			$this->response->add_error('err_error', 'ERROR_INVALID_CHARS_IN_ID_OR_EMPTY');
			return;
		}

		if (empty($text)) {
			$this->response->add_error('err_error', 'ERROR_TEXT_EMPTY');
			return;
		}

		if (isset($accepteds['accepted']) && is_array($accepteds['accepted'])) {
			foreach ($accepteds['accepted'] as $key => $value) {
				if ($new_subject == $value['name']) {
					$this->response->add_error('err_error', 'ERROR_ACCEPT_SUBJECT_EXISTS');
					return;
				}
				if ($new_id == $key) {
					$this->response->add_error('err_error', 'ERROR_ID_EXISTS');
					return;
				}
			}
		}

		// I don't think it's needed, since we iterate above, but I don't have time to check - Zane
		if (isset($accepteds['accepted']) && is_array($accepteds['accepted']) && isset($accepteds['accepted_order']) && in_array($new_id, $accepteds['accepted_order'])) {
			$this->response->add_error('err_error', 'ERROR_ID_EXISTS');
			return;
		}

		$counter = 0;
		$position = 0;

		/* Move all elements */
		$accepted_orders = array();
		if(empty($below_id)){
			$accepted_orders[$counter] = $new_id;
			$counter++;
		}
		if(!empty($accepteds['accepted_order'])){
			foreach ($accepteds['accepted_order'] as $key => $value) {
				if ($below_id != $value){
					$accepted_orders[$counter] = $value;
					$counter++;
				}
				else{
					$accepted_orders[$counter] = $value;
					$counter++;
					$accepted_orders[$counter] = $new_id;
					$counter++;
				}
			}
		}
		$accepteds['accepted_order'] = $accepted_orders;

		if (!save_dynamic_bconf($accepteds['accepted_order'], '*.*.common.accepted_order', NULL, NULL, true)) {
			/* Could not add */
			$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
		}

		$new_accepted['name'] = $new_subject;
		$new_accepted['text'] = $text;
		if (!save_dynamic_bconf($new_accepted, '*.*.common.accepted.' . $new_id)) {
			/* Could not save config */
			$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
			return;
		}

		$this->response->add_error('ok_okey', 'OKEY_ACCEPT_REASON_ADD');
	}

	function accepted_remove($id, $accepteds) {
		$counter = 0;

		if (isset($accepteds['accepted']) && is_array($accepteds['accepted'])) {
			$accepted_orders = array();
			foreach ($accepteds['accepted_order'] as $key => $value) {
				if ($id != $value){
					$accepted_orders[$counter] = $value;
					$counter++;
				}
				else
					$position = $counter;
			}
		}

		if (isset($position)) {
			$error_saving = false;

			$accepteds['accepted_order'] = $accepted_orders;
			unset($accepteds['accepted'][$id]);

			if (!save_dynamic_bconf(NULL, '*.common.accepted_order', NULL, NULL, true)) {
				/* Could not delete */
				$this->response->add_error('err_error', 'ERROR_REFUSALS_DELETE_FAILED');
				$error_saving = true;
			}
			if (!save_dynamic_bconf($accepteds['accepted_order'], '*.*.common.accepted_order', NULL, NULL, true)) {
				/* Could not add */
				$this->response->add_error('err_error', 'ERROR_REFUSALS_SAVE_FAILED');
				$error_saving = true;
			}
			if (!save_dynamic_bconf(NULL, '*.*.common.accepted.' . $id)) {
				/* Could not delete */
				$this->response->add_error('err_error', 'ERROR_REFUSALS_DELETE_FAILED');
				$error_saving = true;
			}
			if (!$error_saving) {
				$this->response->add_error('ok_okey', 'OKEY_ACCEPT_REASON_REMOVE');
			}
		}
	}
	/*
	End methods for accepted reasons
	*/

	function prepare_price($price) {
		$result = str_replace(".","",$price);
		return $result;
	}

	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->display_results('controlpanel/adqueue.html');
	}

	/**
	 * CHUS 295: price alert
	 * This function look for the appraisal for a brand, model, version, year, fuel, gearbox
	 * @return Number the appraisal value
	 */
	function get_appraisal($ad_appraisal)
	{
		/* TODO In the future, fix this to get the categories dynamically */
		if ($ad_appraisal["category"] != "2020")
			return 0;

		$category = $ad_appraisal["category"];
		$brand = @$ad_appraisal["brand"];
		$model = @$ad_appraisal["model"];
		$version = @$ad_appraisal["version"];
		$year = @$ad_appraisal["regdate"];
		$fuel = @$ad_appraisal["fuel"];
		$gearbox = @$ad_appraisal["gearbox"];

		$redis_cardata_server_name = bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.name");
		$redis_cardata_server_port = bconf_get($GLOBALS['BCONF'], "*.common.redis_cardata.host.rcd1.port");
		$redis_options = array(
			'debug' => false,
			'id_separator' => NULL,
			'timeout' => 500,
		);

		/* The 'nice' case */
		if ($brand != "0" && $model != "0" && $year != "") {
			if($version != 0) {
				/* CHUS 447: use redis to get the cardata */
				$redis_attr_key = "rcd1xapp_id_cardata_attr_$brand"."_$model"."_$version";
				$server = array(
					$redis_attr_key => array(
						"name" => $redis_cardata_server_name,
						"port" => $redis_cardata_server_port
					)
				);
				$redis_options["servers"] = $server;
				$redis_cardata = new RedisSessionClient($redis_options);
				$json_data = $redis_cardata->hgetall("$redis_attr_key","$brand");

				$btree = array();
				foreach ($json_data as $key => $val) {
					$btree[$key] = yapo_json_decode($val, true);
				}
				if (is_array($btree)) {
					foreach ($btree as $bt) {
						if ($bt['year'] == $year && $bt['fuel'] == $fuel && $bt['gearbox'] == $gearbox) {
								return $bt['appraisal'];
						}
					}
				}
			}
			/* CHUS: 295-B : if version == 'other' calculate the alert based on average of all version prices for brand, model, year */
			else {
				/* CHUS 447 get the cardata from redis*/
				// we need all versions for a model
				$redis_versions_key = "rcd1xapp_id_cardata_versions_$brand"."_$model";
				$server = array(
					$redis_versions_key => array (
						"name" => $redis_cardata_server_name,
						"port" => $redis_cardata_server_port
					)
				);
				$redis_options["servers"] = $server;
				$redis_cardata = new RedisSessionClient($redis_options);
				$redis_versions_data = $redis_cardata->hgetall("$redis_versions_key");

				// get the attrs for each versions
				$json_data = array();
				foreach ($redis_versions_data as $redis_version_id => $v) {
					$redis_attr_key = "rcd1xapp_id_cardata_attr_$brand"."_$model"."_$redis_version_id";

					$server = array(
						$redis_attr_key => array (
							"name" => $redis_cardata_server_name,
							"port" => $redis_cardata_server_port
						)
					);
					$redis_options["servers"] = $server;
					$redis_attrs = new RedisSessionClient($redis_options);
					$attr_data = $redis_attrs->hgetall("$redis_attr_key","$brand");

					// create a "big" array with all attrs
					$json_data = array_merge($json_data, $attr_data);
				}

				$btree = array();
				foreach ($json_data as $key => $val) {
					$btree[$key] = yapo_json_decode($val, true);
				}
				if (is_array($btree)) {
					$sum_appr = 0;
					$i_appr = 0;

					/* look for prices for current year, otherwise the next, or the previous year */
					foreach(array(0, 1, -1) as $delta) {
						foreach ($btree as $bt) {
							if ($bt['year'] == ($year + $delta)) {
								$i_appr++;
								$sum_appr += $bt['appraisal'];
							}
						}
						/* return the average*/
						if ($i_appr > 0) { return ($sum_appr / $i_appr); }
					}
				}
			}
		}
		return 0;
	}

	/**
	 * CHUS 295: price alert
	 * This function return the color for the price alert
	 */
	function price_alert($appraisal, $price, $category)
	{
		$color = "notfound";

		if ($appraisal < 1) return $color;

		$ref_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.refuse.min");
		$ref_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.refuse.max"); 
		$war_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.warning.min");
		$war_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.warning.max");
		$ok_min = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.ok.min");
		$ok_max = bconf_get($GLOBALS['BCONF'], "*.price_alert.$category.ok.max");

		$percent = ($price - $appraisal)*100 / $appraisal;

		if ($percent >= $ref_min && $percent < $ref_max) {
			$color = "refuse";
		}
		else if ($percent >= $war_min && $percent < $war_max) {
			$color = "warning";
		}
		else if ($percent >= $ok_min && $percent < $ok_max) {
			$color = "ok";
		}

		return $color;
	}

	function redis_review_action($reason){
		$this->init_redis_connection();
		$admin_user = $_SESSION['controlpanel']['admin']['username'];
		$admin_ip   = $_SESSION['controlpanel']['admin']['ip'];
		$admin_login= $_SESSION['controlpanel']['admin']['login_date'];
		$hashkey = 'rsd1xAdmin_'.$admin_user.'_';

		$this->rsc->lpush($hashkey.$reason, microtime(true));
		$this->rsc->incr($hashkey.'totalreviewed');
	}

	/**
	 * Deletes from the redis list the entries with a stamp older than
	 * a timestamp
	 *
	 * $key : string with the name of redis list
	 * $old_limit: the method will only keep the entries newr tan this
	 */
	function expire_old_data($key, $old_limit) {

		// if last ad is within the last hour, do nothing
		$last = (float)$this->rsc->rpop($key);
		if($last > $old_limit) {
			$this->rsc->rpush($key, $last);
			return $last;
		}

		while ($last && $last < $old_limit) {
			$last = (float)$this->rsc->rpop($key);
		}
		//push the last popped value back because it is inside the last hour
		return $last;
	}

	function get_semaphore_color($condition_name, $value) {

		foreach(array('red', 'yellow', 'green') as $color) {
			$bconf_key = "controlpanel.toolbar.semaphore.".$condition_name.'.'.$color;
			$top = bconf_get($GLOBALS['BCONF'], $bconf_key.'.top');
			$bottom = bconf_get($GLOBALS['BCONF'], $bconf_key.'.bottom');

			//skip if value is greather than the top limit
			if(!is_null($top) && $value >= $top) {
				continue;
			}

			//skip if value is lesser than the bottom limit
			if(!is_null($bottom) && $value <= $bottom) {
				continue;
			}

			return $color;
		}
	}

	function set_reviewer_data_toolbar(){
		$time_window = bconf_get($GLOBALS['BCONF'], 'controlpanel.toolbar.time_window');
		$old_limit = (float)microtime(true) - $time_window;
		$this->init_redis_connection();
		$admin_user = $_SESSION['controlpanel']['admin']['username'];
		$admin_login= $_SESSION['controlpanel']['admin']['login_date'];
		$hashkey = 'rsd1xAdmin_'.$admin_user.'_';

		$accept = $this->expire_old_data($hashkey."accept", $old_limit);
		$refuse = $this->expire_old_data($hashkey."refuse", $old_limit);
		$acceptEdit = $this->expire_old_data($hashkey."acceptEdit", $old_limit);

		$accept = $this->rsc->llen($hashkey."accept");
		$refuse = $this->rsc->llen($hashkey."refuse");
		$acceptEdit = $this->rsc->llen($hashkey."acceptEdit");

		$time_window_total = $accept + $refuse + $acceptEdit;
		$session_total = $this->rsc->get($hashkey.'totalreviewed');

		// protect ourselves against division by 0 warnings
		if ($time_window_total > 0) {
			$accept /= $time_window_total;
			$refuse /= $time_window_total;
			$acceptEdit /= $time_window_total;
		} else {
			$accept = 0;
			$refuse = 0;
			$acceptEdit = 0;
		}

		$time_window_speed = (float)$time_window_total;
		$elapsed_seconds = (float)time() - strtotime($admin_login);
		$time_factor = ($elapsed_seconds/$time_window);
		$time_factor = $time_factor < 1 ? 1 : $time_factor;
		$session_speed = (float)$session_total / $time_factor;

		$accept = 				number_format($accept*100, 2);
		$refuse =               number_format($refuse*100, 2);
		$acceptEdit =           number_format($acceptEdit*100, 2);
		$time_window_speed =    $time_window_speed;
		$session_speed =        number_format($session_speed, 2);

		$priority = array(
			'green' => 1,
			'yellow' => 2,
			'red' => 3,
		);

		$color1 = $this->get_semaphore_color('time_window_speed', $time_window_speed);
		$color2 = $this->get_semaphore_color('refused_percentage', $refuse);
		$color3 = $this->get_semaphore_color('edit_accept_percentage', $acceptEdit);

		$semaphore_color = $color1;
		foreach(array($color1, $color2, $color3) as $color) {
			if(@$priority[$color] > @$priority[$semaphore_color]) {
				$semaphore_color = $color;
			}
		}

		$this->response->add_data('toolbar_accept', $accept);
		$this->response->add_data('toolbar_refuse', $refuse);
		$this->response->add_data('toolbar_acceptEdit', $acceptEdit);
		$this->response->add_data('toolbar_timeWindowSpeed', $time_window_speed);
		$this->response->add_data('toolbar_sessionSpeed', $session_speed);
		$this->response->add_data('toolbar_semaphore_color', $semaphore_color);
		//Set current hour and previous hour reviewed ads
		$this->response->add_data('reviewed_ads_current', $this->get_reviewed_ads_current());
		$this->response->add_data('reviewed_ads_previous', $this->get_reviewed_ads_previous());
		$this->response->add_data('current_hour', date('H:i'));
		$this->response->add_data('previous_hour', date('H:00'));
	}

	/*
	 * Increment the redis value for the ads reviewed in the current
	 * hour 
	 */
	function increment_reviewed_ads(){
		$this->incr_redis_value('reviewed_ads_'.date('H'));
	}

	function get_reviewed_ads_current(){
		return $this->get_redis_value('reviewed_ads_'.date('H'));
	}

	function get_reviewed_ads_previous(){
		return $this->get_redis_value('reviewed_ads_'.date('H', strtotime('-1 hour')) );
	}

	function get_redis_value($key){
		$this->init_redis_connection();
		$admin_user = $_SESSION['controlpanel']['admin']['username'];
		$hashkey = 'rsd1xAdmin_'.$admin_user.'_';

		$number_reviewed_ads=intval( $this->rsc->get($hashkey.$key) );
		$this->rsc->disconnect();
		return $number_reviewed_ads;
	}

	function incr_redis_value($key){
		$this->init_redis_connection();
		$admin_user = $_SESSION['controlpanel']['admin']['username'];
		$hashkey = 'rsd1xAdmin_'.$admin_user.'_';

		// can reach redis?
		if ($this->rsc->ping($hashkey)) {
			$val = 0;
			if ($this->rsc->exists($hashkey.$key)) {
				$val=intval( $this->rsc->get($hashkey.$key) );
			}
			$this->rsc->set($hashkey.$key, $val+1);
			$this->rsc->expire($hashkey.$key, '7200');
		}
	}

	function refresh_account_cache($email){
		global $BCONF;
		$trans = new bTransaction();
		$trans->add_data('email', $email);
		$reply = $trans->send_command('refresh_account_cache');
		if ($reply['status'] != "TRANS_OK" ) {
			bLogger::logWarning(__METHOD__, "failed refresh_account_cache for: $email");
		}
	}
}
