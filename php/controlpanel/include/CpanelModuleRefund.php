<?php

namespace Yapo;

use bTransaction;

class CpanelModuleRefund extends CpanelModule
{

    private $trans;

    public function __construct($trans = null)
    {
        $this->config = array_copy(Bconf::get($GLOBALS['BCONF'], 'controlpanel.modules.Refund'));
        $this->trans = isset($trans) ? $trans : new bTransaction();
        // Init this controlpanel module
        parent::init();
    }

    private function persistParams()
    {
        $params = array('date_start','date_end','filter','emailto');
        foreach ($params as $key) {
            if (isset($_REQUEST[$key])) {
                $this->response->add_data($key, $_REQUEST[$key]);
            }
        }
        if (!isset($_REQUEST['date_start'])) {
            $this->response->add_data('date_start', strftime('%Y-%m-%d %T', time() - 86400));
        }
        if (!isset($_REQUEST['date_end'])) {
            $this->response->add_data('date_end', strftime('%Y-%m-%d %T', time()));
        }
    }

    private function populateErrors($trans)
    {
        foreach ($trans->get_errors() as $param => $code) {
            $this->response->add_data("err_$param", lang($code));
        }
    }

    private function updateRefundStatus()
    {
        if (isset($_REQUEST['rf_id']) && isset($_REQUEST['rf_ac'])) {
            $trans = $this->trans->reset();
            $trans->add_data("refund_id", $_REQUEST["rf_id"]);
            $trans->add_data("refund_status", $_REQUEST["rf_ac"]);
            $response = $trans->send_admin_command('refund_update');
            if ($trans->has_error()) {
                $this->populateErrors($trans);
            }
        }
    }

    public function searchRefund()
    {
        $this->persistParams();
        $this->updateRefundStatus();

        if (isset($_REQUEST['search']) || isset($_REQUEST['sendemail'])) {
            $trans = $this->trans->reset();
            if (!empty($this->response->data["date_start"])) {
                $trans->add_data("date_start", $this->response->data["date_start"]);
            }
            if (!empty($this->response->data["date_end"])) {
                $trans->add_data("date_end", $this->response->data["date_end"]);
            }
            $trans->add_data("filter", $this->response->data["filter"]);
            if (isset($_REQUEST['emailto'])) {
                $trans->add_data("emailto", $this->response->data["emailto"]);
            }
            $response = $trans->send_admin_command('refund_search');

            if ($trans->has_error()) {
                $this->populateErrors($trans);
                $this->response->add_data('result_found', 0);
            } else {
                if (isset($response['refund_search'])) {
                    $this->response->add_data('result_found', 1);
                    $this->response->add_extended_array(array('result' => $response['refund_search']));
                    if (isset($_REQUEST['emailto'])) {
                        $this->response->add_data('mail_sent', 1);
                    }
                } else {
                    $this->response->add_data('result_found', 0);
                }
            }
        }
        $this->displayResults('controlpanel/refund/search.html');
    }

    private function links()
    {
        $this->displayResults('controlpanel/refund/links.html');
    }

    public function main($function = null)
    {
        Cpanel::hasPriv("Refund.searchRefund") ? $this->response->add_data("priv_searchrefund", "1") : $this->response->add_data("priv_searchrefund", "0");
        if ($function) {
            $this->response->add_data("page_name", "Buscar");
            $this->response->fill_array('page_js', '/js/Refund.js');
            $this->runMethod($function);
        } else {
            $this->links();
        }
    }
}
