<?php
require_once('cpanel_module.php');
require_once('util.php');
require_once('bTransaction.php');
require_once('bAd.php');

use Yapo\Cpanel;

class cpanel_module_scarface extends cpanel_module {
	function cpanel_module_scarface() {
		/* Get configuration for easy access */
		$this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.scarface'));

		/* Get dynamic configuration */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$root = '*.controlpanel.modules.scarface.settings';
			$settings = bconf_get($this->dyn_bconf, $root);
			if (isset($settings)) {
				if (is_string($settings))
					$settings = array($settings);
				foreach ($settings as $key => $value) {
					$this->config['settings'][$key] = $value;
				}
			}
		} else
			$this->dyn_bconf = false;

		// Init this controlpanel module
		$this->init();
	}

	function show_queues() {
		$transaction = new bTransaction();
		$transaction->add_data('admin_id', $_SESSION['controlpanel']['admin']['id']);	
		$reply = $transaction->send_command('scarface_queues');
		Cpanel::handleReply($reply, $this->response); 

		if (isset($reply['scarface_queues'])) {
			foreach ($reply['scarface_queues'] as $queue) {
				foreach ($queue as $key => $value) {
					$this->response->fill_array($key, $value);
				}
			}
		}
		$this->display_results('controlpanel/scarface_queues.html');
	}

	function reports_list() {
		global $BCONF;

		/* Get reports data */
		$transaction = new bTransaction();
		$transaction->add_data('list_id', $_GET['list_id']);
		//$transaction->add_data('ad_id', $_GET['ad_id']);
		$transaction->add_data('report_type', $_GET['report_type']);
		$transaction->add_data('admin_id', $_SESSION['controlpanel']['admin']['id']);
		$reply = $transaction->send_command('scarface_get_reports_list');
		/* Fill reports data for the template */
		if (isset($reply['scarface_get_reports_list'])) {
			ksort($reply['scarface_get_reports_list']);	
			foreach ($reply['scarface_get_reports_list'] as $report) {
				$this->response->fill_array('report_id', $report['o_report_id']);
				$this->response->fill_array('report_type', $report['o_report_type']);
				$this->response->fill_array('reporter', $report['o_user_id']);
				$this->response->fill_array('reporter_accepted', $report['o_accepted_reports']);
				$this->response->fill_array('reporter_rejected', $report['o_rejected_reports']);
				$this->response->fill_array('report_text', $report['o_text']);
				$fake_reporters_domain = bconf_get($BCONF, '*.*.common.reporters_fake_email_domain');
				$this->response->fill_array('omit_reply_area', $report['o_email'] && preg_match("/\d+\.\d+@{$fake_reporters_domain}/", $report['o_email'] )? "1" : "0");
				$this->response->fill_array('report_date', $report['o_date']);
			}
		} else {
			$this->show_ad();
			return;
		}
		$this->response->add_data('list_id', $_GET['list_id']);
		$this->response->add_data('ad_id', $_GET['ad_id']);
		$this->response->call_template('controlpanel/scarface_reports_list.html');
		exit;
	}

	function show_ad() {
		global $BCONF;

		/* Get reports data */
		$transaction = new bTransaction();
		if (isset($_GET['skip_ad']) && $_GET['skip_ad'] == "1") {
			$transaction->add_data('skip_ad', "1");
			$transaction->add_data('ad_id', $_GET['ad_id']);
		}

		$transaction->add_data('report_type', $_GET['report_type']);
		$transaction->add_data('admin_id', $_SESSION['controlpanel']['admin']['id']);
		$reply = $transaction->send_command('scarface_get_reports_list');
		/* Fill reports data for the template */
		if (isset($reply['scarface_get_reports_list'])) {
			ksort($reply['scarface_get_reports_list']);
			$domain = bconf_get($BCONF, "*.*.common.brand.domain");
			foreach ($reply['scarface_get_reports_list'] as $report) {
				$this->response->fill_array('report_id', $report['o_report_id']);
				$this->response->fill_array('report_type', $report['o_report_type']);
				$this->response->fill_array('reporter', $report['o_reporter_id']);
	                        $this->response->fill_array('user', $report['o_user_id']);
				$this->response->fill_array('reporter_accepted', $report['o_accepted_reports']);
				$this->response->fill_array('reporter_rejected', $report['o_rejected_reports']);
				$this->response->fill_array('report_text', $report['o_text']);
				$this->response->fill_array('mail_reply', $report['o_email'] && !preg_match('/\d+\.\d+@'.$domain.'/', $report['o_email']) ? $report['o_email']:"");
				$this->response->fill_array('report_date', $report['o_date']);
 
			}
		} else {
			//$this->display_results('controlpanel/scarface_queues.html');
			$this->show_queues();
			return;
		}

		/* Get ad data */
		$transaction = new bTransaction();
		$transaction->add_data('id', $reply['scarface_get_reports_list'][0]['o_list_id']);
		$transaction->add_client_info();
		$reply = $transaction->send_admin_command('loadad');

		/*Check for errors in transaction*/
		/*Array ( [token] => ERROR_TOKEN_OLD [status] => TRANS_ERROR )*/
	
		if($reply['status']=="TRANS_ERROR"){ 
			$this->response->add_error('err_token', $reply['token']);
                        $_SESSION['response'] = $this->response;	
			switch($reply['token']){
				case "ERROR_TOKEN_OLD":
					syslog(LOG_ERR, log_string()."{$_SESSION['controlpanel']['admin']['username']}'s session expired.");
					setcookie('token_expire', time() - 1, time() - 1);
					Cpanel::logout();
				;
				case "ERROR_TOKEN_REPLACED":
					syslog(LOG_ERR, log_string()."{$_SESSION['controlpanel']['admin']['username']}'s token is not valid anymore, user is logged in with another session from $message.");
					setcookie('token_expire', time() - 1, time() - 1);
					Cpanel::logout();
				;
				default:
				;
			
			}
			
		}	
		
		/* Fill ad data for the template */
		foreach ($reply['ad'] as $key => $value) {
			$this->response->add_data($key, $value);
			if (in_array($key, array('body', 'subject')))
				$this->response->add_data($key.'_new', $value);
		}
		if (isset($reply['params'])) {
			foreach ($reply['params'] as $key => $value) {
				$this->response->add_data($key, $value);
				if (in_array($key, array('body', 'subject')))
					$this->response->add_data($key.'_new', $value);
			}
		}
		$this->response->add_data('users_email', $reply['users']['email']);

		/* Category defined types */
		$types = bconf_get($BCONF, "*.common.category." . $reply['ad']['category'] . ".type");
		if (strlen($types)) {
			$types = explode(',', $types);
			$this->response->add_data('category_types', $types);
		}

		$ext_params = array();
		$ext_params = @array_merge($ext_params, $reply['params']);
		if (isset($ext_params) && is_array($ext_params))
			$this->response->add_extended_array('ext_params', $ext_params);
	
		/* Get filter lists */
		if (Cpanel::hasPriv('filter.lists')) {
			$this->lists_by_type("email");
			$this->response->add_data('filter_lists_priv', '1');
		}

		/* Display template */
		$this->response->fill_array('page_js', "/js/category_dropdown.js");
		$this->response->fill_array('page_js', "/js/adqueues.js");
		$this->response->fill_array('page_js', "/tjs/arrays_v2.js");
		$this->response->add_data('is_admin', "1");
		if (Cpanel::hasPriv('scarface.warning')) {
			$this->response->add_data('can_send_warning', 1);
		}
		$this->display_results('controlpanel/scarface_review.html');
	}

	function resolve() {
		global $BCONF;
		$message = '';
		$status = 'true';
		$error = '';
		$action = $_REQUEST["action"];
		$transaction = new bTransaction();
		$transaction->add_data('list_id', $_GET['list_id']);
		$transaction->add_data('report_id', $_GET['report_id']);		
		$transaction->add_data('resolution', $action);

		$action_found = false;
		$delete_ad = false;
		$save_ad = false;
		$refuse_ad = false;

		switch ($action) {
			case 'accept':
				$save_ad = true;
				$action_found = true;
				break;
			case 'accept_and_delete':
				$action_found = true;
				$delete_ad = true;
				break;
			case 'accept_delete_and_warn':
				$action_found = true;
				$delete_ad = true;
				break;
			case 'reject':
				$save_ad = true;
				$action_found = true;
				break;		
			case 'accept_and_refuse':
				$refuse_ad = true;
				$action_found = true;
				break;
			case 'accept_and_notify_owner':
				$action_found = true;
				$transaction->add_data('notify_type', (int)$_GET['notify_type']);
				break;
			case 'reject_and_notify_reporter':
				$action_found = true;
				$transaction->add_data('notify_type', (int)$_GET['notify_type']);
				break;
		}

		if ($action_found) {
			$transaction->add_data('score', '1');
			if ($action == 'reject') {
				$transaction->add_data('action', 'resolve');
				$reply = $transaction->send_admin_command('scarface_insertreport');
			} else if ($action == 'accept' || $action == 'accept_and_refuse' || $action == 'accept_and_delete' || $action == 'accept_delete_and_warn' || $action == 'accept_and_notify_owner' || $action == 'reject_and_notify_reporter') {
				$reply = $transaction->send_admin_command('scarface_multiple_resolve');
			}
			$response = new bResponse();
			Cpanel::handleReply($reply, $response);
			if ($response->has_error()) {
				$status = 'false';
				$error = $response->get_error();

			} else {
				$status = 'true';
				syslog(LOG_INFO, log_string() . "Report accepted: " . $_GET['report_id']);

				if ($delete_ad) {
					syslog(LOG_INFO, log_string()."scarface report review delete_ad");
					$transaction->reset();
					$transaction->add_client_info();
					$transaction->add_data('log_string', log_string());
					$transaction->add_data('id', $_GET['list_id']);
					$transaction->auth_type = 'admin';

					$reply = $transaction->send_admin_command('deletead');
					Cpanel::handleReply($reply, $response);

					if ($response->has_error()) {
						$status = 'false';
						$error = $response->get_error();
					}

				} else if ($save_ad) {
					syslog(LOG_INFO, log_string()."scarface report review save ad");
					$transaction->reset();
					$transaction = new bTransaction();
					$transaction->add_data('id', $_GET['list_id']);
					$transaction->add_client_info();
					$reply = $transaction->send_admin_command('loadad');
					Cpanel::handleReply($reply, $response);

					if ($response->has_error()) {
						$status = 'false';
						$error = $response->get_error();
					} else { 
						$reply['ad']['type'] = bconf_get($BCONF, "*.common.type." . $reply['ad']['type'] . ".short_name");
						if ($_POST['category'] != $reply['ad']['category'] || $_POST['type'] != $reply['ad']['type'] || 
							$_POST['company_ad'] != $reply['ad']['company_ad'] || ( isset($_POST['price']) && isset($reply['ad']['price'])  && ( $_POST['price'] != $reply['ad']['price'])) ) {
								$transaction->reset();
								$transaction->add_data('type', bconf_get($BCONF, "*.common.type." . $_POST['type'] . ".short_name"));

								$transaction->populate($_POST);
								$transaction->add_data('ad_id', $reply['ad']['ad_id']);

								/* CHUS 207 (UF killer): no longer assume UFs for real estate categories */
								/*if (isset($_POST['category']) && bconf_get($BCONF, "*.cat." . $_POST['category'] . ".parent") == "1000")  {
									$transaction->add_data('currency', "uf");
								}*/

								if (isset($_POST['price'])) {
									$transaction->add_data('price', $this->prepare_price($_POST['price']));
								}

								$reply = $transaction->send_admin_command('scarface_edit');
								Cpanel::handleReply($reply, $response);

								if ($response->has_error()) {
									$status = 'false';
									$error = $response->get_error();
								}
							}
					}
				} else if ($refuse_ad) {
					/* We need ad_id AND action_id to send the refuse email */
					$transaction->reset();
					$transaction = new bTransaction();
					$transaction->add_data('list_id', $_GET['list_id']);
					$transaction->add_client_info();
					$reply = $transaction->send_admin_command('ad_info');
					Cpanel::handleReply($reply, $response);

					if ($response->has_error()) {
						$status = 'false';
						$error = $response->get_error();
					} else {
						$transaction->reset();
						$transaction = new bTransaction();
						$transaction->add_client_info();
						$transaction->add_data('ad_id', $reply['ad'][0]['ad_id']);
						$transaction->add_data('action_id', $reply['ad'][0]['action_id']);

						$root = '*.abusetool.wrong_category_email';
						$reply = $transaction->send_admin_command('scarface_rejected');

						Cpanel::handleReply($reply, $response);
						if ($response->has_error()) {
							$status = 'false';
							$error = $response->get_error();
						}
					}	
				}
			}
		} else {
			$error ='ERROR_NO_ACTION_FOUND';
			$status='false';
		}

		exit(json_encode(array('status' => $status, 'message' => $message, 'error_message' => $error)));
	}

	function askinfo() {
		global $BCONF;

		$status = 'true';
		$error = '';

		/* Get reports data */
		$transaction = new bTransaction();
		$transaction->add_data('action', $_GET['a']);
		$transaction->add_data('list_id', $_GET['list_id']);
		$transaction->add_data('report_id', $_GET['report_id']);
		$transaction->add_data('admin_name', $_SESSION['controlpanel']['admin']['username']);
		$transaction->add_data('admin_message',iconv('UTF-8', bconf_get($BCONF, "common.charset"), $_POST['reply']));

		$reply = $transaction->send_admin_command('scarface_insertreport');

		$response = new bResponse();
		Cpanel::handleReply($reply, $response);

		if ($response->has_error()) {
			$status = 'false';
			$error = $response->get_error();
		}

		exit(json_encode(array('status' => $status, 'error_message' => $error)));
	}

	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->display_results('controlpanel/scarface_queues.html');
	}

	function prepare_price($price) {
		$result = str_replace(".","",$price);
		return $result;
	}


	function lists_by_type($list_type) {
		/* Get filter lists for adqueue */
		$get_lists_trans = new bTransaction();
		$get_lists_trans->add_data('action', 'list');
		$get_lists_trans->add_data('list_type', $list_type);
		$get_lists_reply = $get_lists_trans->send_admin_command('block_list');

		if (isset($get_lists_reply['block_list'])) {
			foreach ($get_lists_reply['block_list'] as $list) {
				$this->response->fill_array("${list_type}_lists", $list['list_name']);
				$this->response->fill_array("${list_type}_lists_id", $list['list_id']);
				$this->response->fill_array("${list_type}_lists_type", $list['list_type']);
			}
		}
	}
}
?>
