<?php
require_once('cpanel_module.php');
require_once('util.php');
require_once('bTransaction.php');
require_once('CsvExporter.php');

use Yapo\Cpanel;

class cpanel_module_yapesos extends cpanel_module
{

    public function cpanel_module_yapesos()
    {
        $this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.yapesos'));
        $this->init();
        $this->populateVars();
    }

    private function populateVars()
    {
        foreach ($_REQUEST as $key => $val) {
            if (is_numeric($val)) {
                $this->$key = intval(sanitizeVar($val, 'int'));
            } elseif ($key == 'email') {
                $this->$key = sanitizeVar($val, 'email');
            } else {
                $this->$key = sanitizeVar($val, 'string');
            }
        }
        $this->admin_id = intval($_SESSION['controlpanel']['admin']['id']);
    }

    public function show()
    {
        $this->response->add_data("priv_yapesos_report", Cpanel::hasPriv('yapesos.report') ? 1 : 0);
        $this->response->add_data("priv_yapesos_assign", Cpanel::hasPriv('yapesos.assign') ? 1 : 0);
        $this->response->add_data("priv_yapesos_history", Cpanel::hasPriv('yapesos.history') ? 1 : 0);
        $this->response->add_data("priv_yapesos_edit", Cpanel::hasPriv('yapesos.edit') ? 1 : 0);
        $this->response->add_data("priv_yapesos_assigned_report", Cpanel::hasPriv('yapesos.assigned_report') ? 1 : 0);
        $this->display_results('controlpanel/yapesos/yapesos.html');
    }

    private function reportHasErrors()
    {
        $has_error = false;
        if (!isset($this->rfrom) || strtotime($this->rfrom) === false) {
            $this->response->add_data("err_rfrom", "INVALID_DATE");
            $has_error = true;
        }
        if (!isset($this->rto) || strtotime($this->rto) === false) {
            $this->response->add_data("err_rto", "INVALID_DATE");
            $has_error = true;
        }
        return $has_error;
    }

    private function searchHasErrors()
    {
        if (empty($this->email)) {
            $this->response->add_data("err_email", "ERROR_EMAIL_INVALID");
            return true;
        }
        $this->response->add_data("YR_email", $this->email);
        return false;
    }

    private function assignHasErrors()
    {

        if (!isset($this->yapesos)) {
            return true;
        }

        $max = bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.yapesos.MAX_QUANTITY');
        $min = bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.yapesos.MIN_QUANTITY');
        $has_error = false;
        // check only if user_id is setted
        if (!empty($this->user_id) && isset($this->assign_submit)) {
            if ($this->assign_submit != "Enviar") {
                return true;
            }
            $this->response->add_data("YR_user_id", $this->user_id);
            if (!is_numeric($this->service_order)) {
                $this->response->add_data("err_service_order", "ERROR_SERVICE_ORDER_INVALID");
                $has_error = true;
            }
            if (!is_numeric($this->yapesos)) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_CP");
                $has_error = true;
            }
            if (!is_numeric($this->bonus)) {
                $this->response->add_data("err_bonus", "ERROR_BONUS_CP");
                $has_error = true;
            }
            if (!$has_error && $this->yapesos > $max) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_QUANTITY_TOO_HIGH");
                $has_error = true;
            }
            if (!$has_error && $this->yapesos < $min) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_QUANTITY_TOO_LOW");
                $has_error = true;
            }
            if (!$has_error && $this->bonus > $max) {
                $this->response->add_data("err_bonus", "ERROR_BONUS_BIGGER_THAN_MAX");
                $has_error = true;
            }
            if (!$has_error && $this->yapesos + $this->bonus <= 0) {
                $this->response->add_data("err_total", "ERROR_YAPESOS_TOTAL_TOO_LOW");
                $has_error = true;
            }
            $this->response->add_data("YR_service_order", $this->service_order);
            $this->response->add_data("YR_yapesos", $this->yapesos);
            $this->response->add_data("YR_bonus", $this->bonus);
        }
        return $has_error;
    }

    private function historyHasErrors()
    {
        $has_error = false;
        if (isset($this->history_submit) && $this->history_submit != "Enviar") {
            return true;
        }
        if (!isset($this->hfrom) || strtotime($this->hfrom) === false) {
            $this->response->add_data("err_hfrom", "INVALID_DATE");
            $has_error = true;
        }
        if (!isset($this->hto) || strtotime($this->hto) === false) {
            $this->response->add_data("err_hto", "INVALID_DATE");
            $has_error = true;
        }
        return $has_error;
    }

    private function setData($data)
    {
        foreach ($data as $entry) {
            foreach ($entry as $k => $v) {
                if (in_array($k, array('Created_at', 'Start_date', 'Expiration_date')) && !empty($v)) {
                    // Kill possible milliseconds
                    $v = preg_replace('/\.\d+Z/', 'Z', $v);
                    $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $v);
                    $v = $date->format('Y-m-d H:i');
                }
                $this->response->fill_array("YR_".$k, $v);
            }
        }
    }

    public function generate()
    {
        if (!array_key_exists($this->r, $this->config) || !array_key_exists('headers', $this->config[$this->r])) {
            $this->redirect($this->r, array('m' => 'yapesos'));
        }

        switch ($this->r) {
        case 'report':
            $YapesosReport = new YapesosReport();
            $report = $YapesosReport->buildFinancialReport($this->rfrom, $this->rto);
            break;
        case 'history':
            $report = $this->getInfoFromService($this->hfrom, $this->hto, $this->user_id);
            break;
        case 'assigned_report':
            $YapesosReport = new AssignedYapesosReport();
            $report = $YapesosReport->getReport($this->rfrom, $this->rto);
            $this->populateAdminNames($report);
            break;
        default:
            $this->redirect($this->r, array('m' => 'yapesos'));
        }

        $headerNames = array();
        $headers = $this->config[$this->r]['headers'];

        $data = array();
        foreach ($report as $row) {
            $new_row = array();
            foreach ($headers as $val_conf) {
                $key = $val_conf['key'];
                if (!in_array($val_conf['name'], $headerNames)) {
                    $headerNames[] = $val_conf['name'];
                }

                if (strpos($key, ',') !== false) {
                    //If the key has 2 posible parameters then
                    // add the second if the first is empty
                    $exp_key = explode(',', $key);
                    $new_row[] = !empty($row[$exp_key[0]])?$row[$exp_key[0]]:$row[$exp_key[1]];
                } else {
                    if (!empty($val_conf['format'])) {
                        //To format date
                        $date = new DateTime($row[$key]);
                        $new_row[] = $date->format('Y-m-d H:i');
                    } elseif (!empty($val_conf['depends'])) {
                        //To format credits depends on type
                        if ($row[$val_conf['depends']] === 'add') {
                            $new_row[] = "+ {$row[$key]}";
                        } else {
                            $new_row[] = "- {$row[$key]}";
                        }
                    } else {
                        $new_row[] = $row[$key];
                    }
                }
            }
            $data[] = $new_row;
        }

        $filename = "report-".date("Ymd").".csv";
        $csvExport = new CsvExporter($filename);
        $csvExport->setHeaders($headerNames);
        $csvExport->setData($data);
        $csvExport->run();
    }

    private function getUserData($email)
    {
        $trans = new bTransaction();
        $trans->add_data('email', $email);
        $reply = $trans->send_command('get_account');
        if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
            bLogger::logError(__METHOD__, "trans failed to return the user account params");
            return array();
        }
        return array($reply['account_id'], $reply['name']);
    }

    private function checkInfoUser($func = "historyHasErrors")
    {
        if (!isset($this->email)) {
            return false;
        }
        $email = $this->email;
        if ($this->hasLoaded() && !$this->searchHasErrors()) {
            $user_data = $this->getUserData($email);
            if (empty($user_data)) {
                $this->response->add_data("err_email", "ACCOUNT_NOT_FOUND");
            } else {
                list($user_id, $user_name) = $user_data;
                $this->response->add_data("YR_user_id", $user_id);
                $this->response->add_data("YR_user_name", $user_name);
                if (!$this->$func()) {
                    return $user_id;
                }
            }
        }
        return false;
    }

    private function hasLoaded()
    {
        return isset($this->cmd) && $this->cmd == 'load';
    }

    private function addAdminYapesos($user_id, $external_id, $yapesos, $bonus, $installments = 1, $ttl = "0s")
    {

        $body = array(
            'UserId' => (integer)$user_id,
            'TTL' => $ttl,
            'ExternalId' => (integer)$external_id,
            'Params' => array(
                array(
                    'key' => 'adminId',
                    'value' => "{$this->admin_id}"
                )
            )
        );

        $credits = new CreditsService();

        if ($bonus > 0) {
            $body['Credits'] = $bonus;
            $body['CreditType'] = "bonus";
            $body['Installments'] = $installments;
            $response = $credits->addCredits($body);
        }
        $body['Credits'] = $yapesos;
        $body['CreditType'] = "admin";
        $body['Installments'] = $installments;
        return  $credits->addCredits($body);
    }

    private function getInfoFromService($from, $to, $user_id = "", $func = "getReport")
    {
        $credits = new CreditsService();
        $history = $credits->$func($from, $to, $user_id);
        return $history;
    }

    private function populateAdminNames(&$data)
    {
        $admin_ids = array();
        foreach ($data as $row) {
            if (!empty($row['Admin_id'])) {
                $admin_ids[$row['Admin_id']] = $row['Admin_id'];
            }
        }
        if (empty($admin_ids)) {
            return;
        }

        $admins = implode(",", $admin_ids);

        $trans = new bTransaction();
        $trans->add_data("admin_ids", $admins);
        $reply = $trans->send_admin_command('get_adminnames');
        if ($trans->has_error()) {
            return;
        }

        $admin_ids = array();
        foreach ($reply['get_adminnames'] as $names) {
            $name = $names['email'] ? "{$names['name']} ({$names['email']})" : $names['name'];
            $admin_ids[$names['admin_id']] = $name;
        }

        for ($i = 0; $i < count($data); $i++) {
            if (!empty($data[$i]['Admin_id']) && isset($admin_ids[$data[$i]['Admin_id']])) {
                $data[$i]['Admin_name'] = $admin_ids[$data[$i]['Admin_id']];
            } else {
                $data[$i]['Admin_name'] = '';
            }
        }
    }

    // Public functions
    public function report()
    {
        if ($this->hasLoaded() && !$this->reportHasErrors()) {
            $YapesosReport = new YapesosReport();

            $report = $YapesosReport->buildFinancialReport($this->rfrom, $this->rto);
            // AD DATA FOR REPORT TMPL
            $this->setData($report);
        }
        $this->show();
    }

    public function assign()
    {
        $user_id = $this->checkInfoUser("assignHasErrors");
        if ($user_id) {
            $response = $this->addAdminYapesos(
                $user_id,
                $this->service_order,
                intval($this->yapesos),
                intval($this->bonus),
                intval($this->installments),
                $this->ttl
            );
            if (!$response) {
                $this->response->add_data("err_api", "ERROR_YAPESOS_API");
            } else {
                $this->response->add_data("success", $response->Balance);
            }
        }
        $this->show();
    }

    public function history()
    {
        $user_id = $this->checkInfoUser();
        if ($user_id) {
            $data = $this->getInfoFromService($this->hfrom, $this->hto, $user_id);
            $this->setData($data);
        }
        $this->show();
    }

    public function edit()
    {
        $user_id = $this->checkInfoUser();
        if ($user_id) {
            $this->execEdit();

            $data = $this->getInfoFromService($this->hfrom, $this->hto, $user_id, "listCredits");
            $this->populateAdminNames($data);
            $this->setData($data);
        }

        $this->show();
    }

    public function assigned_report()
    {
        if ($this->hasLoaded() && !$this->reportHasErrors()) {
            $YapesosReport = new AssignedYapesosReport();

            $report = $YapesosReport->getReport($this->rfrom, $this->rto);
            // AD DATA FOR REPORT TMPL
            $this->populateAdminNames($report);
            $this->setData($report);
        }
        $this->show();
    }

    private function editHasErrors()
    {

        if (!isset($this->yapesos)) {
            return true;
        }

        $max = bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.yapesos.MAX_QUANTITY');
        $has_error = false;
        // check only if user_id is setted
        if (!empty($this->user_id) && isset($this->edit_submit)) {
            if ($this->edit_submit != "Enviar") {
                return true;
            }

            if (!isset($this->action_on_credit)
                || !in_array($this->action_on_credit, array('remove','add'))) {
                $this->response->add_data("err_action_on_credit", "ERROR_YAPESOS_SELECT_ACTION");
                $has_error = true;
            } else {
                $this->response->add_data("action_on_credit", $this->action_on_credit);
            }

            if (!is_numeric($this->yapesos)) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_CP");
                $has_error = true;
            }
            if (!$has_error && $this->yapesos > $max) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_QUANTITY_TOO_HIGH");
                $has_error = true;
            }
            if (!$has_error
                && $this->action_on_credit == 'remove'
                && $this->yapesos > $this->available) {
                $this->response->add_data("err_yapesos", "ERROR_YAPESOS_QUANTITY_IS_BIGGER_THAN_AVAILABLE");
                $has_error = true;
            }
        }

        if ($has_error) {
            $this->response->add_data("has_error_on", $this->Credit_id);
        }

        return $has_error;
    }
    private function execEdit()
    {
        if (!$this->editHasErrors()) {
            $credit_id =        $this->Credit_id;
            $user_id =          $this->User_id;
            $external_id =      $this->External_id;
            $action_on_credit = $this->action_on_credit;
            $credits =          $this->yapesos;

            if ($action_on_credit == 'remove') {
                $credits = $credits * -1;
            }

            $data = array(
                "CreditId" => $credit_id,
                "UserId" => $user_id,
                "AdminId" => $this->admin_id,
                "Credits" => $credits,
                "ExternalId" => $external_id
            );

            $creditsSrv = new CreditsService();
            $response = $creditsSrv->editCredits($data);
        }
    }

    public function main($function = null)
    {
        if ($function != null) {
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_js', '/js/yapesos.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->run_method($function);
        } else {
            $this->show();
        }
    }
}
