<?php

namespace Yapo;

use bTransaction;

class CpanelModuleWebsql extends CpanelModule
{

    private $trans;
    private $query;

    public function __construct($trans = null, $response = null, $query = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Websql'));
        $this->init();
        $this->trans = isset($trans) ? $trans : new bTransaction();
        if ($response) {
            $this->response = $response;
        }
        $this->query = isset($_GET['query']) ? $_GET['query'] : $query;
    }

    public function websql()
    {
        $this->response->add_extended_array('Websql', $this->config);

        if (isset($_GET['load'])) {
            $this->load();
        } elseif (isset($_GET['run'])) {
            $this->run();
        }

        if (isset($_GET['export_xml'])) {
            $this->exportXml();
        } else {
            $this->displayResults('controlpanel/Websql.html');
        }
    }

    public function main($function = null)
    {
        if ($function != null) {
            $this->runMethod(lcfirst($function));
        } else {
            $this->displayResults('controlpanel/Websql.html');
        }
    }

    private function load()
    {
        $this->response->add_data('query', $this->query);
        $this->response->add_data('yesterday', date('Y-m-d', strtotime("yesterday")));
        $this->response->add_data('today', date('Y-m-d', strtotime("today")));
    }

    private function run()
    {
        $this->response->add_data('query', $this->query);

        if (!empty($_GET["recipient"])) {
            $this->response->add_data('recipient', $_GET["recipient"]);
        }

        if (isset($this->config['queries'][$this->query])) {
            $this->trans->add_data('query', $this->query);

            if (!empty($_GET["recipient"])) {
                $this->trans->add_data("recipient", $_GET["recipient"]);
            }

            if (isset($this->config['queries'][$this->query]['params'])) {
                foreach ($this->config['queries'][$this->query]['params'] as $id => $param) {
                    $params[$id] = $_GET[$id];
                    $this->trans->add_data($id, $_GET[$id]);

                    if (in_array($id, array('timestamp_from', 'timestamp_to'))) {
                        $date = date_parse($_GET[$id]);
                        //validate date as 2014-09-31
                        if ($date['warning_count'] > 0 || $date['error_count'] > 0) {
                            $date_errors[$id] = 'ERROR_TIME_FORMAT';
                        }
                    }
                }
            }

            if (isset($params)) {
                $this->response->add_extended_array('params', $params);
            }

            if (!isset($date_errors)) {
                $reply = $this->trans->send_admin_command('websql');
                $this->trans->handle_reply();

                if ($this->trans->has_error()) {
                    $errors = $this->trans->get_errors();
                    $this->response->add_extended_array('errors', $errors);
                } elseif (isset($reply['result'])) {
                    $this->response->add_extended_array('result', $reply['result']);
                    $this->response->add_data('result_set', 1);
                } elseif (isset($reply['timeout'])) {
                    $this->response->add_data('timeout', 1);
                    $this->response->add_data('recipient', $reply['timeout']);
                    $this->response->add_data('result_set', 0);
                } else {
                    $this->response->add_data('result_set', 0);
                }
            } else {
                $this->response->add_extended_array('errors', $date_errors);
            }
        }
    }
    
    private function exportXml()
    {
        global $BCONF;
        $filename = $this->config['queries'][$this->query]['name'];
        if (isset($this->config['queries'][$this->query]['params'])) {
            foreach ($this->config['queries'][$this->query]['params'] as $id => $param) {
                if (array_key_exists('name', $param) && isset($_GET[$id])) {
                    $filename .= " ({$param['name']} {$_GET[$id]})";
                }
            }
        }
        $filename .= ".csv";
        $this->response->add_data('tab_name', $this->config['queries'][$this->query]['name']);
        $this->response->add_data('query_string', $_SERVER['QUERY_STRING']);
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename={$filename}");
        $this->response->call_template("stats/include/excel_start.xml");
        $this->response->call_template("controlpanel/websql.xml");
        $this->response->call_template("stats/include/excel_end.xml");
        exit(0);
    }
}
