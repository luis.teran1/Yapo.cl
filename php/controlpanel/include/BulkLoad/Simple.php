<?php

namespace Yapo\BulkLoad;

class Simple extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'fids',
            'targets',
            'fun',
            'fun_arg',
            'regex',
            'comment'
        );

        parent::__construct($data);
    }
}
