<?php

namespace Yapo\BulkLoad;

class OneToMany extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'fids',
            'targets',
            'fun',
            'fun_arg',
            'regex',
            'comment'
        );

        parent::__construct($data);
    }

    public function applyFunction($funcName) {
        switch ($funcName) {
            case 'createCommuneDict':
                $this->createCommuneDict();
                break;
            case 'createTypeDict':
                $this->createTypeDict();
            default:
                break;
        }
    }

    private function createCommuneDict() {
        if (!is_array($this->fun_arg)) {
            throw new \Exception('ERROR_COMMUNE_DICT_MISSING');
        }

        $communeDict = array();
        foreach ($this->fun_arg as $partnerCommune => $yapoCommune) {
            $regionsBconf = bconf_get($GLOBALS['BCONF'], "*.common.commune.$yapoCommune.region");
            if (isset($regionsBconf)) {
                $communeDict[] = array(
                    'partnerCode' => iconv("ISO-8859-1", "UTF-8", $partnerCommune),
                    'communes' => (int)$yapoCommune,
                    'region' => (int)$regionsBconf
                );
            }
        }

        if (count($communeDict) > 0) {
            $this->fun_arg = $communeDict;
        }
    }

    private function createTypeDict() {
        if (!is_array($this->fun_arg)) {
            throw new \Exception('ERROR_TYPE_DICT_MISSING');
        }

        $typeDict = array();
        foreach ($this->fun_arg as $partnerCategory => $yapoCategory) {
            $type = bconf_get($GLOBALS['BCONF'], "*.category_settings.types.1.$yapoCategory.value");
			if (isset($type)) {
				$typeDict[] = array(
					'partnerCode' => iconv("ISO-8859-1", "UTF-8", $partnerCategory),
					'types' => $type,
					'category' => $yapoCategory
				);
			}
        }

        if (count($typeDict) > 0) {
            $this->fun_arg = $typeDict;
        }
    }
}
