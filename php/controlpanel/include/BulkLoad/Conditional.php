<?php

namespace Yapo\BulkLoad;

class Conditional extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'fids',
            'targets',
            'fun',
            'fun_arg',
            'other_field',
            'map_fun',
            'regex',
            'comment'
        );

        parent::__construct($data);
    }
}
