<?php

namespace Yapo\BulkLoad;

class ValidatorFactory
{
    public function getValidator(array $params)
    {
        switch ($params['class'])
        {
            case 'Simple':
                $params['fids'] = $params['ind'][0];
                return new Simple($params);
            case 'Multi':
                $params['fids'] = $params['ind'];
                return new Multi($params);
            case 'Conditional':
                $params['fids'] = $params['ind'];
                return new Conditional($params);
            case 'Constrain':
                return new Constrain($params);
            case 'Remove':
                return new Remove($params);
            case 'OneToMany':
                $params['fids'] = $params['ind'][0];
                return new OneToMany($params);
            default:
                return NULL;
        }
    }
}
