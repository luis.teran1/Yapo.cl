<?php

namespace Yapo\BulkLoad;

class BulkLoadRule
{
    public $rules;

    public function __construct()
    {
        $this->rules = array();
    }

    public function addRule($ruleName, BulkLoadValidator $validator)
    {
        $this->rules[] = $validator;
    }

    public function getJson() {
        $dataArray = array('rules' => array());
        foreach ($this->rules as $r) {
            $dataArray['rules'][] = array('class' => $r->getName(), 'args' => $r);
        }
        return json_encode($dataArray);
    }
}
