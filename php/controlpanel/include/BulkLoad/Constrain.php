<?php

namespace Yapo\BulkLoad;

class Constrain extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'to_check',
            'fun',
            'fun_arg',
            'qto_check',
            'qfun',
            'qfun_arg',
            'message'
        );

        parent::__construct($data);
    }
}
