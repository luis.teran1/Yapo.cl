<?php

namespace Yapo\BulkLoad;

class Remove extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'to_check',
            'fun',
            'fun_arg',
            'to_remove'
        );

        parent::__construct($data);
    }
}
