<?php

namespace Yapo\BulkLoad;

class Multi extends BulkLoadValidator
{

    public function __construct(Array $data) {
        $this->attrs = array (
            'fids',
            'targets',
            'fun',
            'fun_arg',
            'fun_init',
            'map_fun',
            'regex',
            'comment'
        );

        parent::__construct($data);
    }
}
