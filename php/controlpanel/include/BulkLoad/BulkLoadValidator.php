<?php

namespace Yapo\BulkLoad;

class BulkLoadValidator
{
    protected $attrs;
    public function __construct(Array $data) {

        if (is_array($this->attrs)) {
            foreach($this->attrs as $attr) {
                if (isset($data[$attr])) {
                    $this->$attr = $data[$attr];
                } else {
                    $this->$attr = NULL;
                }
            }
        }
    }

    public function getName() {
        return str_replace(__NAMESPACE__.'\\', '', get_class($this));
    }
}
