<?php
namespace Yapo;

use bLogger;
use bImage;
use bTransaction;
require_once ('JSON.php');

class PpImage
{

    private $res;
    private $image;
    private $input_name;
    private $img_type;

    public function __construct($input_name, $input_type)
    {
        $this->res = null;
        $this->image = null;
        $this->input_name = $input_name;
        $this->img_type = $input_type;
    }

    private function isValidImageType($type)
    {
        global $BCONF;
        $validTypes = bconf_get($BCONF, "*.ppages.images.$this->img_type.types");
        return in_array($type, $validTypes);
    }

    private function upload()
    {
        global $BCONF;
        if (!isset($_FILES[$this->input_name])) {
            $this->res = 'ERROR_IMAGE_NOT_PRESENT';
            return false;
        }

        $file = $_FILES[$this->input_name];
        $width = (int)bconf_get($BCONF, "*.ppages.images.$this->img_type.width");
        $height = (int)bconf_get($BCONF, "*.ppages.images.$this->img_type.height");

        $filename = isset($file['name'])?$file['name']:null;
        $filetype = isset($file['type'])?$file['type']:null;
        $filesize = isset($file['size'])?$file['size']:null;
        $error = (isset($file['error']))?$file['error']:null;

        if (!$this->isValidImageType($filetype)) {
            $this->res = 'ERROR_IMAGE_TYPE_INVALID';
            return false;
        }

        if (!$filesize || $filesize === 0) {
            $this->res = 'ERROR_IMAGE_SIZE_INVALID';
            return false;
        }

        if (is_uploaded_file($file['tmp_name'])) {

            $image = new bImage($file['tmp_name']);
            $quality = (int)bconf_get($BCONF, "*.common.ppages_image.quality");
            $image->set_image_quality($quality);
            $image->create_image();
            list($imgWidth, $imgHeight) = getimagesize($file['tmp_name']);

            if ($width != $imgWidth || $height != $imgHeight) {
                $this->res = 'ERROR_IMAGE_SIZES';
            } elseif ($image->has_error()) {
                $this->res = $image->get_error();
            } else {
                $this->image = $image->_image_buffer;
                return true;
            }
            $image->destruct();
        } elseif (strlen($filename) > 0) {
            bLogger::logInfo(__METHOD__, "Image upload went wrong");
            $this->res = 'ERROR_IMAGE_UPLOAD';
        }
        return false;
    }

    public function save()
    {
        global $BCONF;
        $response = $this->upload();
        if ($response) {
            $trans = new bTransaction();
            $trans->add_file('image', $this->image);
            $reply = $trans->send_command('imgput_ppage');
            if (!$trans->has_error() && !empty($reply['file'])) {
                bLogger::loginfo(__METHOD__, "Adding image {$reply['file']} to form via ppage edit ajax");
                $result['status'] = 'ok';
                $result['file'] = $reply['file'];
            } else {
                $result['trans'] = $trans->get_errors();
            }
        } else {
            $result = array(
                'status' => lang($this->res)
            );
        }
        die(yapo_json_encode($result));
    }
}
