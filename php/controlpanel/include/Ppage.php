<?php

namespace Yapo;

use bTransaction;

class Ppage
{
    public $pp_id;
    public $title;
    public $url;
    public $image;
    public $start_date;
    public $end_date;
    public $category;
    public $search_query;
    public $is_draft;
    public $feature;

    /*
     * Ppage constructor. Set as private because a
     * Ppage is only created using fromForm and
     * fromTrans methods.
     */
    private function __construct(array $data)
    {
        foreach ($this as $k => $v) {
            if (isset($data[$k])) {
                $this->$k = Util::sanitize(
                    $data[$k],
                    $k == 'category' ? 'int' : 'string'
                );
            }
        }
    }

    /*
     * Returns a new instance of Ppage with the sanitized
     * data from $form
     */
    public static function fromForm(array $form)
    {
        $data = array();
        $data = array_filter($form, function ($v) {
            return isset($v) && $v != null;
        });

        if (isset($form['category'])) {
            $form_category = $form['category'];
            unset($form['category']);

            $category = array();
            foreach ($form_category as $key => $val) {
                if ($val['id'] != '') {
                    array_push($category, sanitizeVar($val['id'], 'int'));
                }
            }

            $query = array();
            foreach ($form_category as $k => $v) {
                $query[] = Ppage::buildQuery($v);
            }
            $data['category'] = $category;
            $data['search_query'] = $query;
        }
        return new Ppage($data);
    }

    /*
     * Returns a new instance of Ppage, depending on pp_id.
     * If the argument is present, returns a Ppage object with
     * the information of a single promo page, fetched from trans.
     * If the argument is not present, returns a list of all the
     * promotional pages available.
     */
    public static function fromTrans($pp_id = null)
    {
        $pp_id = (int)$pp_id;
        $trans = new bTransaction();
        if ($pp_id && $pp_id > 0) {
            $trans->add_data('pp_id', $pp_id);
        }
        $trans->add_data('status', 'all');
        $reply = $trans->send_command('get_promotional_pages');
        if ($reply['status'] == "TRANS_OK") {
            if (isset($reply['promo']) && !empty($reply['promo'])) {
                return Ppage::parseTransResponse($reply['promo']);
            } else {
                return 'PP_LIST_EMPTY';
            }
        }
        $fields = array('error') + array_keys(get_object_vars($this));
        return Util::getErrors($reply, 'PP_LIST_ERROR', $fields);
    }

    /*
     * Deletes a promo page with given pp_id.
     */
    public static function delete($pp_id, $type = 'all')
    {
        $pp_id = (int)$pp_id;
        if ($pp_id === 0) {
            return false;
        }
        $trans = new bTransaction();
        $trans->add_data('pp_id', $pp_id);
        $trans->add_data('type', $type);
        $reply = $trans->send_command('delete_promotional_page');
        return $reply['status'] === "TRANS_OK";
    }

    /*
     * Saves the current object as a promo page in trans.
     */
    public function save()
    {
        $trans = new bTransaction();
        foreach ($this as $k => $v) {
            if (isset($v) && $v != null) {
                $trans->add_data($k, $v);
            }
        }
        $reply = $trans->send_command('set_promotional_page');
        if ($reply['status'] === "TRANS_OK" && isset($reply['pp_id'])) {
            $this->pp_id = $reply['pp_id'];
            return true;
        } else {
            $fields = array('error') + array_keys(get_object_vars($this));
            return Util::getErrors($reply, 'PP_LIST_ERROR', $fields);
        }
    }

    /*
     * Generates an asearch query, based on the given
     * query base, which contains categories and words to
     * add or remove from the query.
     * It's used only by other methods from the class.
     */
    private static function buildQuery($query_base)
    {
        if (empty($query_base) || !is_array($query_base)) {
            return null;
        }
        global $BCONF;
        $category = array_shift($query_base);
        $ads_limit = (int)Bconf::get($BCONF, "*.ppages.ads_limit");
        $img_param = (int)Bconf::get($BCONF, "*.ppages.ads_with_images") ? "img:1" : "";
        $keywords = "";
        $extra_filters = "";
        $extra_filters_arr = array();
        $flag = 0;
        foreach ($query_base as $key => $val) {
            if ($key == 'word_i' && $val != "") {
                foreach ($val as $index => $word) {
                    if ($flag == 0) {
                        $keywords = $keywords.$word." ";
                        $flag = 1;
                    } else {
                        $keywords = $keywords."OR ".$word." ";
                    }
                }
            } elseif ($key == 'word_f' && $val != "") {
                foreach ($val as $index => $word) {
                    $keywords = $keywords."NOT ".$word." ";
                }
            } elseif (is_array($val) and count($val) > 0) {
                if ($key == "email") {
                    foreach ($val as $i => $email) {
                        $val[$i] = "\"${email}\"";
                    }
                }
                $extra_filters_arr[$key] = join(",", $val);
            } elseif ($val != "") {
                $extra_filters_arr[$key] = $val;
            }
        }
        $extra_filters = "";
        if (count($extra_filters_arr)>0) {
            foreach ($extra_filters_arr as $key => $val) {
                $extra_filters = $extra_filters." ${key}:${val} ";
            }
        }
        return "0 lim:${ads_limit} category:${category} ${extra_filters} ${img_param} *:* ".$keywords;
    }

    private static function parseTransResponse(array $reply)
    {
        $ppages = array();
        foreach ($reply as $j => $ppage) {
            foreach ($ppage as $k => $v) {
                if (is_array($v)) {
                    $ppages[$k][$j] = $v;
                } else {
                    $ppages[$k][] = $v;
                }
            }
        }
        return new Ppage($ppages);
    }

    /**
    * Publishes a promo page draft with the given pp_id
    */
    public function publish($pp_id = null)
    {
        $pp_id = (int)$pp_id;
        $trans = new bTransaction();
        $trans->add_data('pp_id', $pp_id);
        $trans->add_data('queue', 1);
        $reply = $trans->send_command('publish_promotional_page');
        if ($reply['status'] == "TRANS_OK") {
            return 1;
        }
        return 0;
    }
}
