<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;
use Yapo\RewardsService;
use DateTime;

class CpanelModuleRewards extends CpanelModule
{
    private $trans;
    private $redis;

    public function __construct($trans = null, $redis = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Rewards'));
        $this->trans = isset($trans)? $trans : new bTransaction();
        parent::init();
    }

    private function checkService()
    {
       $RewardsService = new RewardsService();
       $response = $RewardsService->checkService();
       if ($response == null) {
           $this->response->add_data("err_healthcheck", "REWARDS_UNAVAILABLE");
       }
    }

    private function parseRewards($data)
    {
        $this->response->add_data("rewards_data", json_encode($data));
        foreach ($data as $entry) {
            foreach ($entry as $key => $value) {
                $objects = array("id", "niceName", "type", "creation", "start", "expiration",
                    "status", "contents","conditions");
                $this->setReward("", $key, $value, "reward_", $objects);
            }
        }
    }

    private function setReward($origin, $key, $value, $prefix, $allowedArr)
    {
        if (in_array($key, $allowedArr) && (gettype($value) == "object" || is_array($value))) {
            foreach ($value as $k => $v) {
                $this->setReward($key, $k, $v, $origin.$key, $allowedArr);
            }
        } else {
            $item = utf8_decode($value);
            Logger::logDebug(__METHOD__, "set assign key:". $key. "value: ".$value);
	    if (strtolower($key) == 'fill_random') {
                $item = ($value) ? 'true' : 'false';
            }
            $this->response->fill_array(lcfirst($prefix.$key), $item);
        }
    }
    public function search()
    {
        if (array_key_exists ("submit", $_REQUEST)) {
            $data = array();
            if (array_key_exists ("rwds-search-start-date", $_REQUEST) && !empty($_REQUEST['rwds-search-start-date'])) {
                $date = new DateTime($_REQUEST['rwds-search-start-date']);
                $data["startDate"] = date_format($date, "Y-m-d\T00:00:00");
                $this->response->add_data("start_date", $_REQUEST['rwds-search-start-date']);
            }
            if (array_key_exists ("rwds-search-end-date", $_REQUEST) && !empty($_REQUEST['rwds-search-end-date'])) {
                $date = new DateTime($_REQUEST['rwds-search-end-date']);
                $data["endDate"] = date_format($date, "Y-m-d\T23:59:59");
                $this->response->add_data("end_date", $_REQUEST['rwds-search-end-date']);
            }
            $RewardsService = new RewardsService();
            $response = $RewardsService->getRewards($data);
            if ($response == null) {
                 $this->response->add_data("err_create", "REWARDS_SEARCH_FAILURE");
            } else {
                $this->parseRewards($response->rewards);
            }
	}
        $this->displayResults('controlpanel/rewards/search.html');
    }

    public function administrateGroups(){
        $this->redirectListing();
    }

    public function listGroups(){
        $RewardsService = new RewardsService();
        $this->checkService();
        if (!array_key_exists ("err_healthcheck", $this->response->data)){
            $response = $RewardsService->getGroups();
            if (isset($_GET['msg']) && !empty($_GET['msg'])){
                $this->response->add_data('msg', $_GET['msg']);
            }
            if (isset($response['ErrorMessage'])){
                $this->response->add_data('error', $response['ErrorMessage']);
            }
            else {
                $this->response->add_data("rewards_groups_list", json_encode($response));
            }
        }
        $this->response->add_data("priv_admin", Cpanel::hasPriv("Rewards.administrateGroups")? "1":"0");
        $this->displayResults('controlpanel/rewards/list_groups.html');
    }

    public function createGroup(){
        if(isset($_GET['action']) && $_GET['action'] =='save' && isset($_GET['encodedJSON'])){
            $encodedJSON = $_GET['encodedJSON'];
            $RewardsService = new RewardsService();
            $groupData = json_decode(utf8_encode(base64_decode($encodedJSON)), true);
            $response = $RewardsService->createGroup($groupData);
            if (isset($response['status']) && $response['status'] == 'OK') {
                $this->redirectListing();
                return;
            }
            $errorStr = "error creating group";
            if (isset($response['ErrorMessage'])){
                    $errorStr.=": ".$response['ErrorMessage'];
            }
            $this->response->add_data('error', $errorStr);
        }
        $this->response->add_data("priv_admin", Cpanel::hasPriv("Rewards.administrateGroups")? "1": "0");
        $this->displayResults('controlpanel/rewards/create_group.html');
    }

    public function administrateGroup(){
        $groupID = $_GET['groupID'];
        $RewardsService = new RewardsService();
        if(isset($_GET['action']) && $_GET['action'] =='save' && isset($_GET['encodedJSON'])){
            $groupData = json_decode(utf8_encode(base64_decode($_GET['encodedJSON'])), true);
            $response = $RewardsService->updateGroup($groupID, $groupData);
            if (isset($response['status']) && $response['status'] == 'OK') {
                $this->redirectListing();
                return;
            }
            $errorStr = "error updating group ".$groupID;
            if (isset($response['ErrorMessage'])){
                    $errorStr.=": ".$response['ErrorMessage'];
            }
            $this->response->add_data('error', $errorStr);
            $groupData["id"] = $groupID;
            $this->response->add_data('rewards_groups', json_encode(array("groups" => array($groupData))));
        } else{
            $response = $RewardsService->getGroup($groupID);
            $this->response->add_data('rewards_groups', json_encode($response));
        }
        $this->displayResults('controlpanel/rewards/administrate_group.html');
    }

    public function deleteGroup(){
        if (isset($_GET['groupID']) && $_GET['groupID'] != 0){
            $RewardsService = new RewardsService();
            $response = $RewardsService->deleteGroup($_GET['groupID']);
            if (isset($response['ErrorMessage'])) $this->redirectListing($response['ErrorMessage']);
            else $this->redirectListing();
            return;
        }
        $this->redirectListing('missing groupID or status');
    }

    public function setGroupStatus(){
        if (isset($_GET['groupID']) && $_GET['groupID'] != 0 && isset($_GET['status'])){
            $RewardsService = new RewardsService();
            $response = $RewardsService->setGroupStatus($_GET['groupID'], $_GET['status']);
            if (isset($response['ErrorMessage'])) $this->redirectListing($response['ErrorMessage']);
            else $this->redirectListing();
            return;
        }
        $this->redirectListing('missing groupID or status');
    }

    private function redirectListing($msg = ""){
        if ($msg != "") $msg = "&msg=".$msg;
        header('Location: /controlpanel?m=Rewards&a=listGroups'.$msg);
    }

    private function redirectSearch(){
        header('Location: /controlpanel?m=Rewards&a=search');
    }

    public function main($function = null){
        if ($function == null) {
            if (Cpanel::hasPriv("Rewards.administrateGroups")) $this->redirectListing();
            else $this->redirectSearch();
            return;
        }
        $this->response->add_data("priv_admin", Cpanel::hasPriv("Rewards.administrateGroups")? "1": "0");
        $this->response->add_data("priv_search", Cpanel::hasPriv("Rewards.search")? "1": "0");
        $this->response->fill_array('page_js', '/js/moment.min.js');
        $this->response->fill_array('page_js', '/js/pikaday.js');
        $this->response->fill_array('page_js', '/js/Rewards.js');
        $this->response->fill_array('page_css', '/css/pikaday.css');
        $this->checkService();
        $this->runMethod($function);
    }
}
