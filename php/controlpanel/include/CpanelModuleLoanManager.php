<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;
use Yapo\LoanManagerService;
use DateTime;

class CpanelModuleLoanManager extends CpanelModule
{
    private $trans;
    private $form_keys;

    public function __construct($trans = null) {
        global $BCONF;
        $this->trans = isset($trans)? $trans : new bTransaction();
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.LoanManager'));
        parent::init();
    }

    public function main($function = null) {
        $this->response->add_data("priv_create", Cpanel::hasPriv("LoanManager.create")? "1": "0");
        $this->response->add_data("priv_search", Cpanel::hasPriv("LoanManager.search")? "1": "0");
        $this->response->add_data("priv_edit", Cpanel::hasPriv("LoanManager.edit")? "1": "0");
        $this->response->add_data("priv_delete", Cpanel::hasPriv("LoanManager.delete")? "1": "0");
        $this->response->add_data("page_name", "Create");
        $this->response->add_data('page_name_hide', '1');
        $this->response->fill_array('page_js', '/js/moment.min.js');
        $this->response->fill_array('page_js', '/js/pikaday.js');
        $this->response->fill_array('page_js', '/js/LoanManager.js');
        $this->response->fill_array('page_css', '/css/pikaday.css');
        $this->response->fill_array('page_css', '/css/LoanManager.css');
        if ($function != null) {
            $this->checkService();
            if (array_key_exists ("err_healthcheck",$this->response->data))  {
                $this->show();
                return;
            }
            $this->form_keys = Bconf::get($BCONF, "*.loan_manager.${function}.fields");
            $this->runMethod($function);
        } else {
            $this->show();
        }
    }

    public function show() {
        $this->response->add_data("priv_create", Cpanel::hasPriv("LoanManager.create")? "1": "0");
        $this->response->add_data("priv_search", Cpanel::hasPriv("LoanManager.search") ? "1" : "0");
        $this->displayResults('controlpanel/loan_manager/main.html');
    }

    public function create() {

        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->resetData();
        } else if (array_key_exists ("email", $_POST)) {
            $this->email = $_POST["email"];
            $current_user_id = $this->checkInfoUser();
        }

        if (isset($current_user_id) && !empty($current_user_id)) {
            if (array_key_exists ("submit", $_REQUEST) && $this->validateForm()) {
               $data = array();
               $data = $this->populateData($data, $_REQUEST);
               $data["user_id"] = intval($current_user_id);
               $response = $this->LoanManagerService->createSetting($data);
               if ($response != null) {
                    if (isset($response->result)) {
                        $this->response->add_data("err_create", "LOAN_MANAGER_CREATE_".$response->result);
                    } else {
                        $this->response->add_data("err_create", "LOAN_MANAGER_CREATE_FAILURE");
                    }
               } else {
                   $this->resetData();
                   $this->response->add_data("create_success", "LOAN_MANAGER_CREATE_SUCCESS");
               }
            }
        }
        $this->displayResults('controlpanel/loan_manager/main.html');
    }

    public function search() {
        if ($_SERVER['REQUEST_METHOD'] == "GET") {
            $this->resetUserData();
        } else if (array_key_exists ("email", $_POST)) {
            $this->email = $_POST["email"];
            $current_user_id = $this->checkInfoUser();
            if ($current_user_id) {
                $data["user_id"] = intval($current_user_id);
                $response = $this->LoanManagerService->getSettingByUserID($data);
                if ($response == null) {
                    $this->response->add_data("err_search", "LOAN_MANAGER_SEARCH_FAILURE");
                } else {
                    $this->setData($response->result);
                    $this->resetUserData();
                }
            }
        }
        $this->displayResults('controlpanel/loan_manager/main.html');
    }

    public function edit() {
        if (!array_key_exists("submit", $_REQUEST)) {
            foreach ($this->form_keys as $k => $v) {
                if (!empty($_REQUEST[$k])) {
                    switch($v["type"]){
                        case "int":
                            $this->response->add_data($k, number_format($_REQUEST[$k], 0, '', ''));
                            break;
                        case "float":
                            $this->response->add_data($k, number_format(floatval($_REQUEST[$k]), 2, '.', ''));
                            break;
                        default:
                            $this->response->add_data($k, $_REQUEST[$k]);
                    }
                }
            }
        } else if ($this->validateForm()){
            $data = array();
            $data = $this->populateData($data, $_REQUEST);
            $response = $this->LoanManagerService->updateSetting($data);
            if ($response != null) {
                 if (isset($response->result)) {
                     $this->response->add_data("err_edit", "LOAN_MANAGER_EDIT_".$response->result);
                 } else {
                     $this->response->add_data("err_edit", "LOAN_MANAGER_EDIT_FAILURE");
                 }
            } else {
                $this->resetData();
                $this->response->add_data("edit_success", "LOAN_MANAGER_EDIT_SUCCESS");
            }
        }
        if (array_key_exists("email", $_REQUEST) && !empty($_REQUEST["email"])) {
            $this->response->add_data("email", $_REQUEST["email"]);
        }
        $this->displayResults('controlpanel/loan_manager/edit.html');
    }

    public function delete() {
        if (array_key_exists ("id", $_GET)) {
            $data["id"] = intval($_GET["id"]);
            $response = $this->LoanManagerService->deleteSetting($data);
            if ($response != null) {
                $this->response->add_data("err_delete", "LOAN_MANAGER_DELETE_FAILURE");
            } else {
                $this->response->add_data("delete_success", "LOAN_MANAGER_DELETE_SUCCESS");
            }
        }
        $this->displayResults('controlpanel/loan_manager/delete.html');
    }

    private function setData($data) {
        if (is_array($data)) {
            foreach ($data as $i => $entry) {
                foreach ($entry as $k => $v) {
                    if (array_key_exists($k, $this->form_keys)) {
                        switch($this->form_keys[$k]["type"]){
                            case "int":
                                $this->response->fill_array($k, $i, number_format($v, 0, '', ''));
                                break;
                            case "float":
                                $this->response->fill_array($k, $i, number_format(floatval($v), 2, '.', ''));
                                break;
                            default:
                                $this->response->fill_array($k, $i, $v);
                        }
                    } else {
                        $this->response->fill_array($k, $i, $v);
                    }
                }
            }
        } else if (!empty($data)){
            $this->response->add_data("err_search", "LOAN_MANAGER_SEARCH_${data}");
        } else {
            $this->response->add_data("err_search", "LOAN_MANAGER_SEARCH_FAILURE");
        }
    }

    private function checkService() {
       $this->LoanManagerService = new LoanManagerService();
       $response = $this->LoanManagerService->checkService();
       if ($response == null) {
           $this->response->add_data("err_healthcheck", "LOAN_MANAGER_UNAVAILABLE");
       }
    }

    private function getUserData($email) {
        if (empty($_SESSION["userData"])) {
            $trans = $this->trans->reset();
            $trans->add_data('email', $email);
            $reply = $trans->send_command('get_account');
            if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
                Logger::logError(__METHOD__, "trans failed to return the user account params");
                return array();
            }
            return array($reply['user_id'], $reply['phone'], $reply['name']);
        }
        return $_SESSION["userData"];
    }

    private function checkInfoUser() {
        if (!isset($this->email)) {
            return false;
        }
        $email = $this->email;
        $_SESSION["userData"] = $this->getUserData($email);
        if (empty($_SESSION["userData"])) {
            $this->response->add_data("err_userdata", "LOAN_MANAGER_USER_NOT_FOUND");
            return false;
        } else {
            list($current_user_id, $user_phone, $user_name) = $_SESSION["userData"];
            $this->response->add_data("current_user_id", $current_user_id);
            $this->response->add_data("email", $this->email);
            $this->response->add_data("phone", $user_phone);
            $this->response->add_data("name", $user_name);
            return $current_user_id;
        }
        return false;
    }

    private function populateErrors($trans) {
        foreach ($trans->get_errors() as $param => $code) {
            $this->response->add_data("err_$param", lang($code));
        }
    }

    private function validateForm() {
        $valid = true;
        foreach ($this->form_keys as $fk => $fv) {
            $v = $_REQUEST[$fk];
            $this->response->add_data($fk, $v);
            if(!is_numeric($v)) {
                if(empty($v)){
                    $this->response->add_data("err_".$fk, "LOAN_MANAGER_REQUIRED_FIELD");
                } else {
                    $this->response->add_data("err_".$fk, "LOAN_MANAGER_NOT_NUMERIC");
                }
                $valid = false;
            } else if (floatval($v) <= 0){
                $this->response->add_data("err_".$fk, "LOAN_MANAGER_LOWER_THAN_ZERO");
                $valid = false;
            }
        }
        return $valid;
    }

    private function populateData($data, $input){
        foreach ($this->form_keys as $k => $v) {
            $data[$k] = floatval($input[$k]);
        }
        return $data;
    }

    private function resetData(){
        foreach ($this->form_keys as $k => $v) {
            unset($this->response->data[$k]);
        }
        $this->resetUserData();
    }

    private function resetUserData(){
        unset($_SESSION["userData"]);
    }
}
