<?php

class YapesosReport
{
    public $CreditsService;

    public function __construct()
    {
        $this->CreditsService = new CreditsService();
    }

    private function indexate($normal_array, $index)
    {
        $good_array = array();
        foreach ($normal_array as $entry) {
            if (isset($entry[$index])) {
                if (!isset($good_array[$entry[$index]])) {
                    $good_array[$entry[$index]] = array();
                }
                array_push($good_array[$entry[$index]], $entry);
            }
        }
        return $good_array;
    }

    private function parseReport($raw_report)
    {
        $report = array();
        $indexed_report = $this->indexate($raw_report, "payment_group_id");
        $indexed_aux_report = $this->indexate($raw_report, "detail_group_id");
        foreach ($raw_report as $data) {
            if (isset($data["Parent_id"]) && isset($indexed_report[$data["Parent_id"]])) {
                $data["Parent_doc_num"] = $indexed_report[$data["Parent_id"]][0]["doc_num"];
            } elseif (isset($data["Parent_id"]) && isset($indexed_aux_report[$data["Parent_id"]])) {
                $data["Parent_doc_num"] = $indexed_aux_report[$data["Parent_id"]][0]["doc_num"];
            } else {
                $data["Parent_doc_num"] = $data["Parent_id"];
            }
            $parsed_data = $this->$data['CreditType']($data);

            if (isset($parsed_data['total_price'])) {
                $iva = bconf_get($GLOBALS['BCONF'], "*.payment.iva");
                $parsed_data['neto'] = round($parsed_data['total_price']/(1 + ($iva/100)));
                $parsed_data['iva'] = $parsed_data['total_price'] - $parsed_data['neto'];

                array_push($report, $parsed_data);
            }
        }
        return $report;
    }

    private function normal($data)
    {
        $data['sent_status'] = '';
        switch ($data["Type"]) {
            case "add":
                $data['Parent_doc_num'] = "";
                $data['External_id'] = "";
                $data['Created_at'] = "";
                $data['Credits'] = "";
                // 'add' transactions without doc_num are waiting for a doc_num
                // the blank space is added so that the template show them empty
                if (isset($data['doc_num']) && $data['doc_num'] == '') {
                    $data['doc_num'] = ' ';
                }
                break;
            case "expired":
                $data['sent_status'] = 'confirmed';
                $data['Parent_doc_num'] = "";
                $data['Credits'] = $data['Available'];
                $data['External_id'] = "CADUCAR";
                $data['Available'] = "";
                break;
            case "consume":
                $data['doc_num'] = (array_key_exists('payment_group_id', $data)) ? $data['payment_group_id'] : "";
                $data['sent_status'] = 'confirmed';
                break;
        }
        if (isset($data['status']) && ($data['status'] == 'sent' || $data['status'] == 'confirmed')) {
            $data['sent_status'] = $data['status'];
            $data['status'] = 'paid';
        }
        return $data;
    }

    private function admin($data)
    {
        switch ($data["Type"]) {
            case "add":
                $data['doc_num'] = $data['External_id'].'a';
                $data['receipt'] = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $data['Created_at']);
                $data['receipt'] = $data['receipt']->format('Y-m-d H:i');
                $data['total_price'] = $data['Credits'];
                $data['Parent_doc_num'] = "";
                $data['doc_type'] = 'DIRECT_SELL';
                $data['status'] = 'paid';
                $data['purchase_id'] = "";
                $data['External_id'] = "";
                $data['sent_status'] = "";
                $data['Created_at'] = "";
                $data['Credits'] = "";
                break;
            case "expired":
                $data['doc_type'] = 'DIRECT_SELL';
                $data['doc_num'] = $data['External_id'].'a';
                $data['Parent_doc_num'] = "";
                $data['External_id'] = "CADUCAR";
                $data['Credits'] = $data['Available'];
                $data['Available'] = "";
                $data['total_price'] = $data['Credits'];
                $data['sent_status'] = 'confirmed';
                $data['status'] = 'paid';
                $data['purchase_id'] = "";
                $data['receipt'] = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $data['Created_at']);
                $data['receipt'] = $data['receipt']->format('Y-m-d H:i');
                break;
            case "consume":
                $data['doc_num'] = (array_key_exists('payment_group_id', $data)) ? $data['payment_group_id'] : "";
                $data['sent_status'] = 'confirmed';
                $data['Parent_doc_num'] = $data['Parent_doc_num'].'a';
                break;
        }
        $data['email'] = $this->getUserEmail($data['User_id']);
        if (isset($data['status']) && ($data['status'] == 'sent' || $data['status'] == 'confirmed')) {
            $data['sent_status'] = $data['status'];
            $data['status'] = 'paid';
        }
        return $data;
        $data["total_price"] = $data["Credits"];
    }

    private function bonus($data)
    {
        switch ($data["Type"]) {
            case "add":
                $doc_num = $data['External_id'].'b';
                $data = $this->admin($data);
                $data['doc_num'] = $doc_num;
                break;
            case "expired":
                $doc_num = $data['Parent_doc_num'].'b';
                $data = $this->admin($data);
                $data['doc_num'] = $doc_num;
                break;
            case "consume":
                $parent_doc_num = $data['Parent_doc_num'].'b';
                $data = $this->admin($data);
                $data['Parent_doc_num'] = $parent_doc_num;
                break;
        }
        return $data;
    }

    private function joinReports($credits_data, $yapo_data)
    {
        $credits_data = $this->indexate($credits_data, 'External_id');
        $yapo_aux_data = $this->indexate($yapo_data, 'detail_group_id');
        $yapo_data = $this->indexate($yapo_data, 'payment_group_id');
        $result = array();
        foreach ($credits_data as $ext_id => $data) {
            foreach ($data as $crdtrans) {
                if (isset($yapo_data[$ext_id][0])) {
                    $raw_data =  array_merge($yapo_data[$ext_id][0], $crdtrans);
                    array_push($result, $raw_data);
                } elseif (isset($yapo_aux_data[$ext_id][0])) {
                    $raw_data =  array_merge($yapo_aux_data[$ext_id][0], $crdtrans);
                    array_push($result, $raw_data);
                } else {
                    array_push($result, $crdtrans);
                }
            }
        }
        return $result;
    }

    private function getUserEmail($account_id)
    {
        $trans = new bTransaction();
        $trans->add_data('account_id', $account_id);
        $reply = $trans->send_command('get_account');
        if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
            bLogger::logError(__METHOD__, "trans failed to return the account data");
            return "";
        }
        return $reply['email'];
    }

    private function getYapoReport($credits_data)
    {
        $trans = new bTransaction();
        $pgids = array();
        $credits_data = $this->indexate($credits_data, 'External_id');
        foreach ($credits_data as $entry => $val) {
            array_push($pgids, $entry);
            foreach ($val as $key => $data) {
                array_push($pgids, $data["Parent_id"]);
            }
        }
        $pgids = array_unique($pgids);
        $trans->add_data('pgids', implode(',', $pgids));
        $reply = $trans->send_command('get_purchases');
        if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
            bLogger::logError(__METHOD__, "trans failed to return the purchases data");
            return array();
        }
        return $reply['get_purchases'];
    }

    public function buildFinancialReport($from, $to, $user_id = '')
    {
        // GET DATA FROM SERVICE
        $credits_data = $this->CreditsService->getReport($from, $to, $user_id);
        if (!$credits_data) {
            bLogger::logError(__METHOD__, "the credits api did not return any report data");
            return array();
        }

        // GET DATA FROM YAPO
        $yapo_data = $this->getYapoReport($credits_data);

        // JOIN DATA
        $raw_report = $this->joinReports($credits_data, $yapo_data);
        return $this->parseReport($raw_report);
    }
}
