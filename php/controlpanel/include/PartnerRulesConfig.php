<?php
namespace Yapo;

abstract class PartnerRulesConfig {

    /**
     * @return string with the representation of the json IRules
     * The IRules are the rules needed to process ads images when using TransmitData schema
     * The structure of the json should have the following keys:
     *   ref_field: refers to the first column in the csv file that will be processed (starts at index 0)
     *   expected_fields: is the number of columns that are filled with information of the partner in the csv file
     *   rules: array of rules that Heimdallr will use to validate the specified fields of the file as a valid image,
     *     each rule has to contain a 'class' key which refers to any of the different types of rules Heimdallr
     *     would accept, e.g: Simple, Conditional, Remove (please refer to Heimdallr documentation to see the classes)
     *     and another key called 'args' with an array of arguments that vary depending on the class chosen before
     */
    abstract public function getImageRules();

    /**
     * @return string with the representation of the json Rules
     * The Rules are the rules needed to process ads when using TransmitData schema
     * The structure of the json should have the following keys:
     *   ref_field: refers to the first column in the csv file that will be processed (starts at index 0)
     *   expected_fields: is the number of columns that are filled with information of the partner in the csv file
     *   rules: array of rules that Heimdallr will use to validate the specified fields of the file as a valid ad,
     *     each rule has to contain a 'class' key which refers to any of the different types of rules Heimdallr
     *     would accept, e.g: Simple, Conditional, Remove (please refer to Heimdallr documentation to see the classes)
     *     and another key called 'args' with an array of arguments that vary depending on the class chosen before
     */
    abstract public function getRules();

}
