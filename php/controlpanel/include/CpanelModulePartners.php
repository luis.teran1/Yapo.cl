<?php

namespace Yapo;

use \Yapo\BifrostService as BifrostService;
use Yapo\BulkLoad as BL;
use Yapo\Partner as Partner;
use Yapo\BlocketApiService as BlocketApiService;
use RedisSessionClient;
use ProxyClient;
use bTransaction;

use Yapo\Cpanel;

class CpanelModulePartners extends CpanelModule
{
    private $partner;
    private static $partner_list;

    public function __construct()
    {
        global $session_read_only;
        $session_read_only = false;
        $this->config = array_copy(Bconf::get($GLOBALS['BCONF'], 'controlpanel.modules.Partners'));
        parent::init();
    }

    public function partners()
    {
        $this->response->add_data("adv_priv", Cpanel::hasPriv('Partners.advancedPartner') ? '1' : '0');
        $this->displayResults('controlpanel/partners/partners.html');
    }

    /*
     * Returns an array with all the partners parameters that are obtained from input fields in form
     */
    public function getPartnerFromForm($formData = null)
    {
        $partnerArray = null;
        if (is_null($formData)) {
            $formData = $_POST;
        }
        if (isset($formData["KeyName"])) {
            //Check if advanced options are enabled
            $rules = $iRules = "";
            $delimiter = isset($formData["Delimiter"]) ? sanitizeVar($formData['Delimiter'], 'string') : "|";

            if ((isset($_POST["transmit"]) && $_POST["transmit"] === "on")) {
                //Assign tab character when user checks in screen the tab separator option
                $delimiter = (isset($formData["tab"]) && $formData['tab'] === "on") ?
                    "\\t" :
                    (isset($formData['Delimiter']) && !empty($FormData['Delimiter']) ?
                    trim(sanitizeVar($formData['Delimiter'], 'string')) :
                    $delimiter);
            }
            if (isset($formData["DoEdit"]) || ((!isset($formData["rules"]) || $formData["rules"] === "on") &&
                isset($formData['Rules']) && isset($formData['IRules']))) {
                $rules = sanitizeVar($formData['Rules'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES);
                $iRules = sanitizeVar($formData['IRules'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES);
            }

            $pathFile = isset($formData["PathFile"]) ? sanitizeVar($formData['PathFile'], 'string') : "";

            //Check images connection data available
            $imgConnData = "";
            $imgProtocol = "";
            $pathImg = "";

            if ((isset($formData["ImgUser"]) && $formData["ImgUser"] != "") ||
                (isset($formData["imgData"]) && $formData["imgData"] === "on")) {
                $imgConnData = array(
                    'user' => sanitizeVar($formData['ImgUser'], 'string'),
                    'password' => sanitizeVar($formData['ImgPass'], 'string'),
                    'domain' => sanitizeVar($formData['ImgDomain'], 'string')
                );
                $imgConnData = str_replace('\\', '', json_encode($imgConnData));
                $imgProtocol = sanitizeVar($formData['ImgProtocol'], 'string');
            }
            $pathImg = sanitizeVar($formData['PathImg'], 'string');
            //Check if require conversion
            $csvProtocol = $csvConnData = $tdProtocol = $tdConnData = "";
            if ((isset($formData["DownloadUser"]) && $formData["DownloadUser"] != "") ||
                (isset($formData["converter"]) && $formData["converter"] === "on")) {
                $csvProtocol = sanitizeVar($formData['DownloadProtocol'], 'string');
                $csvConnData = array(
                    'user' => sanitizeVar($formData['DownloadUser'], 'string'),
                    'password' => sanitizeVar($formData['DownloadPassword'], 'string'),
                    'domain' => sanitizeVar($formData['DownloadDomain'], 'string'),
                    'separator' => sanitizeVar($formData['DownloadSeparator'], 'string'),
                    'encoding' => sanitizeVar($formData['DownloadEncoding'], 'string'),
                    'filepath' => sanitizeVar($formData['DownloadFilePath'], 'string')
                );
                $csvConnData = str_replace('\\', '', json_encode($csvConnData));
                $tdProtocol = sanitizeVar($formData['UploadProtocol'], 'string');
                $tdConnData = array(
                    'user' => sanitizeVar($formData['UploadUser'], 'string'),
                    'password' => sanitizeVar($formData['UploadPassword'], 'string'),
                    'domain' => sanitizeVar($formData['UploadDomain'], 'string'),
                    'filepath' => sanitizeVar($formData['UploadFilePath'], 'string'),
                );
                $tdConnData = str_replace('\\', '', json_encode($tdConnData));
            }

            $partnerArray = array(
                'Name' => sanitizeVar($formData['Name'], 'string'),
                'Protocol' => sanitizeVar($formData['Protocol'], 'string'),
                'ImgProtocol' => $imgProtocol,
                'ConnData' => sanitizeVar($formData['ConnData'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES),
                'ImgConnData' => $imgConnData,
                'PathFile' => $pathFile,
                'FileName' => sanitizeVar($formData['FileName'], 'string'),
                'PathImg' => $pathImg,
                'Delimiter' => $delimiter,
                'Rules' => $rules,
                'IRules' => $iRules,
                'ApiKey' => sanitizeVar($formData['ApiKey'], 'string'),
                'ApiPoint' => sanitizeVar($formData['ApiPoint'], 'string'),
                'InsDelay' => sanitizeVar($formData['InsDelay'], 'string'),
                'MailTo' => sanitizeVar($formData['MailTo'], 'string'),
                'KeyName' => sanitizeVar($formData['KeyName'], 'string'),
                'Market' => sanitizeVar($formData['Market'], 'string'),
                'CsvProtocol' => $csvProtocol,
                'CsvConnData' => $csvConnData,
                'TdProtocol' => $tdProtocol,
                'TdConnData' => $tdConnData,
                'Active' => sanitizeVar($formData['Active'], 'bool'),
                'ConfId' => sanitizeVar($formData["ConfId"], 'int'),
                'RunTime' => sanitizeVar($formData['RunTime'], 'string')
            );

            $partnerArray['Active'] = $partnerArray['Active'] == 'true' ? true : false;

            if (isset($formData['RunTimeShort'])) {
                $partnerArray['RunTime'] = sanitizeVar($formData['RunTimeShort'], 'string');
            }
        }
        return $partnerArray;
    }

    /*
     * Returns an array with all the partners parameters for the partner form from a parter array
     */
    public function getFormDataFromPartner($partnerData)
    {
        $partnerArray = null;
        if (isset($partnerData["KeyName"])) {
            $partnerArray = array(
                'KeyName' => sanitizeVar($partnerData['KeyName'], 'string'),
                'Name' => sanitizeVar($partnerData['Name'], 'string'),
                'Market' => sanitizeVar($partnerData['Market'], 'string'),
                'ApiKey' => sanitizeVar($partnerData['ApiKey'], 'string'),
                'Rules' => sanitizeVar($partnerData['Rules'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES),
                'IRules' =>  sanitizeVar($partnerData['IRules'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES),
                'Protocol' => sanitizeVar($partnerData['Protocol'], 'string'),
                'PathFile' => sanitizeVar($partnerData['PathFile'], 'string'),
                'FileName' => sanitizeVar($partnerData['FileName'], 'string'),
                'ConnData' => sanitizeVar($partnerData['ConnData'], 'string', FILTER_FLAG_NO_ENCODE_QUOTES),
                'Delimiter' => trim(sanitizeVar($partnerData['Delimiter'], 'string')),
                'ApiPoint' => sanitizeVar($partnerData['ApiPoint'], 'string'),
                'InsDelay' => sanitizeVar($partnerData['InsDelay'], 'string'),
                'MailTo' => sanitizeVar($partnerData['MailTo'], 'string'),
                'Active' => sanitizeVar($partnerData['Active'], 'string'),
                'ConfId' => sanitizeVar($partnerData['ConfId'], 'int'),
                'RunTime' => sanitizeVar($partnerData['RunTime'], 'string')
            );

            if (isset($partnerData['RunTimeShort'])) {
                $partnerArray['RunTime'] = sanitizeVar($partnerData['RunTimeShort'], 'string');
            }

            //Check images connection data available
            if (isset($partnerData["ImgConnData"])) {
                $imgConnData = json_decode($partnerData["ImgConnData"], true);
                $partnerArray["ImgUser"] = sanitizeVar($imgConnData['user'], 'string');
                $partnerArray["ImgPass"] = sanitizeVar($imgConnData['password'], 'string');
                $partnerArray["ImgDomain"] = sanitizeVar($imgConnData['domain'], 'string');
                $partnerArray["ImgProtocol"] = sanitizeVar($partnerData['ImgProtocol'], 'string');
                $partnerArray["PathImg"] = sanitizeVar($partnerData['PathImg'], 'string');
            }

            //Check if require conversion
            $csvProtocol = $csvConnData = $tdProtocol = $tdConnData = "";
            if (isset($partnerData["CsvConnData"])) {
                $partnerArray["CsvProtocol"] = sanitizeVar($partnerData['CsvProtocol'], 'string');
                $csvConnData = json_decode($partnerData["CsvConnData"], true);
                $partnerArray["DownloadUser"] = sanitizeVar($csvConnData['user'], 'string');
                $partnerArray["DownloadPassword"] = sanitizeVar($csvConnData['password'], 'string');
                $partnerArray["DownloadDomain"] = sanitizeVar($csvConnData['domain'], 'string');
                $partnerArray["DownloadFilePath"] = sanitizeVar($csvConnData['filepath'], 'string');
                $partnerArray["DownloadEncoding"] = sanitizeVar($csvConnData['encoding'], 'string');
                $partnerArray["DownloadSeparator"] = sanitizeVar($csvConnData['separator'], 'string');
                $partnerArray["DownloadProtocol"] = sanitizeVar($partnerData['CsvProtocol'], 'string');
            }
            if (isset($partnerData["TdConnData"])) {
                $tdProtocol = sanitizeVar($partnerData['TdProtocol'], 'string');
                $tdConnData = json_decode($partnerData["TdConnData"], true);
                $partnerArray["UploadUser"] = sanitizeVar($tdConnData['user'], 'string');
                $partnerArray["UploadPassword"] = sanitizeVar($tdConnData['password'], 'string');
                $partnerArray["UploadDomain"] = sanitizeVar($tdConnData['domain'], 'string');
                $partnerArray["UploadFilePath"] = sanitizeVar($tdConnData['filepath'], 'string');
                $partnerArray["UploadProtocol"] = sanitizeVar($partnerData['TdProtocol'], 'string');
            }
            if (!empty($csvProtocol) && !empty($tdProtocol)) {
                $partnerArray["converter"] = "on";
            }
            if ($partnerArray["Delimiter"] === "\\t") {
                $partnerArray["tab"] = "on";
            }
        }
        return $partnerArray;
    }
    /**
     * Creates the partner and associates BlocketApi and Bifrost services to it
     * @param $partnerArray object with params introduced by user
     * @param $bifrostService instance of the service that connects with Bifrost to save the partner in BD
     * @param $blocketApiService instance of Blocket Api Service
     * @throws exception when occurs an error in the partner creation in any of the services
     */
    public function createPartner($partnerArray, $bifrostService, $blocketApiService)
    {
        $partner = new Partner($partnerArray);
        $partner
            ->addService($bifrostService)
            ->addService($blocketApiService);
        $result = $partner->create();

        if (!$result) {
            throw new \Exception($partner->errorMsg);
        }
        $this->response->add_data("create_msg", lang('CREATE_PARTNER_OK'));
        $this->response->add_data("create_msg_class", 'msg-success');
        return;
    }

    /*
     * Creates the partner and render results
     */
    public function addPartner()
    {
        $partnerArray = $this->getPartnerFromForm();
        if ($partnerArray != null) {
            try {
                $bifrostService = new BifrostService(new ProxyClient());
                $blocketApiService = new BlocketApiService('redis_mobile_api');

                if (empty($partnerArray['Rules'])) {
                    //Assign static rules from TransmitData
                    $rulesConfig = new StaticPartnerRulesConfig();
                    $partnerArray['Rules'] = $rulesConfig->getRules();
                    $partnerArray['IRules'] = $rulesConfig->getImageRules();
                }
                // use the partner info as an update
                if (isset($_POST["DoEdit"])) {
                    if (empty($partnerArray['RunTime'])) {
                        //remove the RunTime parameter
                        unset($partnerArray['RunTime']);
                        unset($partnerArray['RunTimeShort']);
                    }
                    $this->updatePartner($partnerArray, $bifrostService, $blocketApiService);
                    // Load the partners list for the response
                    $this->loadPartnerList($bifrostService);
                // use the partner info as a create
                } else {
                    $this->createPartner($partnerArray, $bifrostService, $blocketApiService);
                }
            } catch (\Exception $e) {
                $msg = $e->getMessage();
                if (strpos($msg, 'MISSING_PARAMETER') !== false || strpos($msg, 'BAD_PARAMETER') !== false) {
                    list($msg,$parameter) = preg_split("/\s/", $msg, -1, PREG_SPLIT_NO_EMPTY);
                    $this->response->add_data("create_parameter", lang($parameter));
                }
                $this->response->add_data("create_msg", lang($msg));
                $this->response->add_data("create_msg_class", 'msg-error');
                if (isset($_POST["DoEdit"])) {
                    $this->response->add_data("do_edit", "true");
                }
            }
        }
        $this->partners();
    }

    /* Allows the user add partner with different options */
    public function advancedPartner()
    {
        $this->response->add_data("partner_advanced", 'true');
        if (isset($_POST["KeyName"])) {
            $this->addPartner();
        } else {
            $this->partners();
        }
    }

    public function partnerDeleteAds()
    {
        if (isset($_POST["partner_to_delete"])) {
            $trans = new bTransaction();
            $trans->add_data('partner', sanitizeVar($_POST["partner_to_delete"], 'string'));
            $trans->add_data('check', isset($_POST["just_check"]) && $_POST["just_check"] == "on" ? 1 : 0);
            $reply = $trans->send_command('partner_deleteads');
            if ($reply['status']=='TRANS_OK') {
                $this->response->add_data("delete_ad_id", $reply['ad_id']);
                $this->response->add_data("delete_external_ad_id", $reply['external_ad_id']);
            }
        }
        $this->partners();
    }

    /**
     * Updates the partner and associates BlocketApi and Bifrost services to it
     * @param $partnerArray object with params introduced by user
     * @param $bifrostService instance of the service that connects with Bifrost to save the partner in BD
     * @param $blocketApiService instance of Blocket Api Service
     * @throws exception when occurs an error in the partner update in any of the services
     */
    public function updatePartner($partnerArray, $bifrostService, $blocketApiService)
    {
        $partnerArray["ConfId"] += 1;
        $partner = new Partner($partnerArray);
        $partner
            ->addService($bifrostService)
            ->addService($blocketApiService);
        $result = $partner->update();

        if (!$result) {
            throw new \Exception($partner->errorMsg);
        }
        $this->response->add_data("list_msg", lang('UPDATE_PARTNER_OK'));
        $this->response->add_data("list_msg_class", 'msg-success');
        return;
    }

    public function editPartner()
    {
        $bifrostService = new BifrostService(new ProxyClient());
        try {
            // if the user wants to edit an specific partner
            if (isset($_POST["edit"])) {
                $partnerArray = $this->getFormDataFromPartner($_POST);
                //decode the html entities
                $partnerArray["Rules"] = htmlspecialchars_decode($partnerArray["Rules"]);
                $partnerArray["IRules"] = htmlspecialchars_decode($partnerArray["IRules"]);
                $partnerArray["ConnData"] = htmlspecialchars_decode($partnerArray["ConnData"]);

                $this->response->add_data("create_msg", lang('EDIT_PARTNER'));
                $this->response->add_data("create_msg_class", 'msg-edit');
                foreach ($partnerArray as $partnerKey => $partnerValue) {
                    $this->response->add_data('post_'.$partnerKey, $partnerValue);
                }
                if (isset($partnerArray["ImgDomain"]) && !empty($partnerArray["ImgDomain"])) {
                    $this->response->add_data("post_imgData", "on");
                }
                if (isset($partnerArray["PathFile"]) && !empty($partnerArray["PathFile"])) {
                    $this->response->add_data("post_transmit", "on");
                }
                if (isset($partnerArray["DownloadFilePath"]) && !empty($partnerArray["DownloadFilePath"])) {
                    $this->response->add_data("post_converter", "on");
                }
                // indicate that the information to be sent by the user should be used to edit the partner
                $this->response->add_data("do_edit", "true");
            } elseif (isset($_POST["activate"])) {
                // if the user wants to activate or deactivate a partner
                $blocketApiService = new BlocketApiService('redis_mobile_api');
                $partnerArray = $this->getFormDataFromPartner($_POST);
                $partnerArray["Rules"] = htmlspecialchars_decode($partnerArray["Rules"]);
                $partnerArray["IRules"] = htmlspecialchars_decode($partnerArray["IRules"]);
                $partnerArray["ConnData"] = htmlspecialchars_decode($partnerArray["ConnData"]);
                $partnerArray = $this->getPartnerFromForm($partnerArray);
                $partnerArray["Active"] = $partnerArray["Active"] == true ? false : true;
                //decode the html entities
                $this->updatePartner($partnerArray, $bifrostService, $blocketApiService);
                $this->loadPartnerList($bifrostService);
                $this->response->add_data("list_msg", lang('UPDATE_PARTNER_OK'));
                $this->response->add_data("list_msg_class", 'msg-success');
            } else {
                // display the list of all partners
                $this->loadPartnerList($bifrostService);
            }
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            if (strpos($msg, 'MISSING_PARAMETER') !== false || strpos($msg, 'BAD_PARAMETER') !== false) {
                list($msg,$parameter) = preg_split("/\s/", $msg, -1, PREG_SPLIT_NO_EMPTY);
                $this->response->add_data("create_parameter", lang($parameter));
            }
            $this->response->add_data("list_msg", lang($msg));
            $this->response->add_data("list_msg_class", 'msg-error');
            if (isset($_POST["DoEdit"])) {
                $this->response->add_data("do_edit", "true");
            }
        }
        $this->partners();
    }

    public function loadPartnerList($bifrostService)
    {
        // List all the partners
        try {
            $listPartners=$bifrostService->listPartners();
            $this->response->add_extended_array(array('partners' => $listPartners));
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $this->response->add_data("list_msg", lang($msg));
            $this->response->add_data("list_msg_class", 'msg-error');
        }
    }

    public function convertionProcessResult($response)
    {
        if (isset($response['ok']) && $response['ok']) {
            header('Expires: 0'); // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');
            header('Cache-Control: private', false);
            header('Content-Type: application/force-download');
            header('Content-Disposition: attachment; filename="' .
                    preg_replace('/\..+$/', '.' . 'XML', $response['filename']) .
                    '"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . strlen($response['data'])); // provide file size
            header('Connection: close');
            echo $response['data'];
            exit(0);
        } else {
            $data = json_decode($response['data'], true);
            if (isset($data['ErrorMessage'])) {
                $this->response->add_data("err_msg", $data['ErrorMessage']);
            } else {
                $this->response->add_data("err_msg", "BIFROST_CONN_ERROR");
            }
            if (isset($data['ErrorParam'])) {
                $this->response->add_data("err_param", $data['ErrorParam']);
            }
            if (isset($data['ErrorReason'])) {
                $this->response->add_data("err_reason", $data['ErrorReason']);
            }
        }
    }

    /*
     * Calls the bifrost service convertFile method and
     * sets the response accordingly
     */
    public function convertFile()
    {
        if (isset($_POST["submit"])) {
            $bifrostService = new BifrostService(new ProxyClient());
            
            //Assign tab character when user checks in screen the tab separator option
            $delimiter =  (isset($_POST["tabConv"]) && $_POST['tabConv'] === "on") ?
                "\\t" : trim(sanitizeVar($_POST['Separator'], 'string'));
            $this->convertionProcessResult(
                $bifrostService->convertFile(
                    $_FILES['UploadFile'],
                    $delimiter,
                    sanitizeVar($_POST['Encoding'], 'string')
                )
            );
        }
        $this->partners();
    }

    public function main($function = null)
    {
        if (!is_null($function)) {
            $this->response->fill_array('page_js', '/js/Partners.js');
            $this->response->fill_array('page_css', '/css/Partners.css');
            $this->runMethod($function);
        } else {
            header('Location: '.$_SERVER['REQUEST_URI'].'&a=addPartner');
            die();
        }
    }
}
