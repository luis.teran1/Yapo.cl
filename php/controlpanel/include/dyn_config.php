<?php
/*
 * Create bconf string array from bconf array
 */
function array_to_bconf($array, $conf = '', $numeric_index = NULL) {
	$result = array();

	if(!is_null($numeric_index)){
		$key = 0;
		
		foreach ($array as $value) {
			if(!is_null($value) && !empty($value)){
				if (is_array($value))
					$result = array_merge($result, array_to_bconf($value, (!empty($conf)?$conf.'.'.$key:$key), $key));
				else
					$result = array_merge($result, array($conf . '.' .$key . '=' . $value));
				$key++;
			}	
		}
	}
	else{
		foreach ($array as $key => $value) {
			if (is_array($value))
				$result = array_merge($result, array_to_bconf($value, (!empty($conf)?$conf.'.'.$key:$key), $key));
			else
				$result = array_merge($result, array($conf . '.' .$key . '=' . $value));
		}
	
	}
	return $result;
}

$dyn_bconf_cache = null;
function get_dynamic_bconf() {
	global $dyn_bconf_cache;

	if ($dyn_bconf_cache != null) { return $dyn_bconf_cache; }

	$data = array('type' => 'conf');
	$reply = bTransaction::bconfd_get($data);

	if (preg_match('/TRANS_OK/', $reply['status'])) {
		$dyn_bconf = array();
		
		if (@is_array($reply['conf'])) {
			foreach ($reply['conf'] as $config) {
				list($name, $value) = explode('=', $config, 2);
				// Seperate config nodes
				$config_tree = explode('.', $name);
				// Set first config node
				$node = array_shift($config_tree);
				$conf =& $dyn_bconf[$node];
				// Go through the rest of the tree
				foreach ($config_tree as $node)
					$conf =& $conf[$node];
				// Set the config value
				$conf = $value;
			}
		}
		if ( !empty($dyn_bconf) ) {
			$dyn_bconf_cache = $dyn_bconf;
			return $dyn_bconf;
		}
	}

	return false;
}

function save_dynamic_bconf($bconf_array = NULL, $root = '', $delete_children = NULL, $file_data = NULL, $numeric_index = NULL) {
	global $dyn_bconf_cache;

	$transaction = new bTransaction();
	
	if (is_array($delete_children)) {
		foreach ($delete_children as $key => $value) {
			$delete_children[$key] = $root . '.' . $value;
		}	
		$transaction->add_array('del', $delete_children);
	} elseif ($delete_children != -1) {
		$transaction->add_data('del', $root);
	}

	if (is_array($bconf_array)) {
		$bconf_data = array_to_bconf($bconf_array, $root, $numeric_index);
		$transaction->add_array('set', $bconf_data);
	}

	if (is_array($file_data))
		$transaction->add_file_array('set_filedata', $file_data);
	
	$reply = $transaction->send_admin_command('setconf');

	$dyn_bconf_cache = null;
	
	return preg_match('/TRANS_OK/', $reply['status']);
}

