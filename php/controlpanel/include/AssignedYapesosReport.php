<?php

class AssignedYapesosReport
{
    private $AccountCache;
    private $CreditsService;
    private $Trans;

    public function __construct($credits=null, $trans=null)
    {
        $this->AccountCache = array();
        $this->CreditsService = $credits ? $credits : new CreditsService();
        $this->Trans = $trans ? $trans : new bTransaction();
    }

    private function getUserEmail($account_id)
    {
        if (array_key_exists($account_id, $this->AccountCache)) {
            return $this->AccountCache[$account_id];
        }

        $trans = $this->Trans->reset();
        $trans->add_data('account_id', $account_id);
        $reply = $trans->send_command('get_account');
        if ($trans->has_error(true) || $reply['status'] != 'TRANS_OK') {
            bLogger::logError(__METHOD__, "trans failed to return the account data");
            return "";
        }
        $this->AccountCache[$account_id] = $reply['email'];
        return $reply['email'];
    }

    public function getReport($from, $to)
    {
        // GET DATA FROM SERVICE
        $credits_data = $this->CreditsService->getAssignedReport($from, $to);
        if (!$credits_data) {
            bLogger::logError(__METHOD__, "the credits api did not return any report data");
            return array();
        }

        // UPDATE FIELDS WITH LOCAL INFORMATION
        $len = count($credits_data);
        for ($i = 0; $i < $len; $i++) {
            $email = $this->getUserEmail($credits_data[$i]['User_id']);
            if ($email) {
                $credits_data[$i]['User_email'] = $email;
            }
            $credits_data[$i]['Net_value'] = $credits_data[$i]['Credits'] * 81;
            $credits_data[$i]['Net_value_float'] = $credits_data[$i]['Credits'] * 0.81;
        }

        return $credits_data;
    }
}
