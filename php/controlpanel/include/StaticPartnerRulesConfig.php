<?php
namespace Yapo;

class StaticPartnerRulesConfig extends PartnerRulesConfig {

    private static $iRules = '{
            "expected_fields": 21,
            "ref_field": 0,
            "rules": [
                {
                    "args": {
                        "comment": "external_ad_id",
                        "fids": [
                            0
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^[A-Za-z0-9]{1,20}$",
                        "targets": [
                            "external_ad_id"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "images RuLE",
                        "fids": [
                            20
                        ],
                        "fun": "images_from_string",
                        "fun_arg": "|",
                        "regex": "^.*$",
                        "targets": [
                            "images"
                        ]
                    },
                    "class": "Simple"
                }
            ]
        }';
    private static $rules = '{
            "expected_fields": 21,
            "ref_field": 0,
            "rules": [
                {
                    "args": {
                        "comment": "external_ad_id",
                        "fids": [
                            0
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^[A-Za-z0-9]{1,20}$",
                        "targets": [
                            "external_ad_id"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "ext_code",
                        "fids": [
                            1
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^.+$",
                        "targets": [
                            "ext_code"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "types",
                        "fids": [
                            2
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^[a-z]{1}$",
                        "targets": [
                            "type"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "category",
                        "fids": [
                            3
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^[0-9]{1,5}$",
                        "targets": [
                            "category"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "estate_type",
                        "fids": [
                            4
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^[0-9]{1,2}$",
                        "targets": [
                            "estate_type"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "communes",
                        "fids": [
                            5
                        ],
                        "fun": "to_int",
                        "fun_arg": null,
                        "regex": "^[0-9]{1,4}$",
                        "targets": [
                            "communes"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "region",
                        "fids": [
                            6
                        ],
                        "fun": "to_int",
                        "fun_arg": null,
                        "regex": "^[0-9]{1,2}$",
                        "targets": [
                            "region"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "currency",
                        "fids": [
                            8
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "currency"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "price to peso",
                        "fids": [
                            7
                        ],
                        "fun": "son_iguales",
                        "fun_arg": "peso",
                        "map_fun": "to_int",
                        "other_field": "currency",
                        "regex": "^.+$",
                        "targets": [
                            "price"
                        ]
                    },
                    "class": "Conditional"
                },
                {
                    "args": {
                        "comment": "price to uf",
                        "fids": [
                            7
                        ],
                        "fun": "son_iguales",
                        "fun_arg": "uf",
                        "map_fun": "to_uf",
                        "other_field": "currency",
                        "regex": "^.+$",
                        "targets": [
                            "price"
                        ]
                    },
                    "class": "Conditional"
                },
                {
                    "args": {
                        "comment": "name",
                        "fids": [
                            9
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "name"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "phone",
                        "fids": [
                            10
                        ],
                        "fun": "clean_phone",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "phone"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "email",
                        "fids": [
                            11
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "email"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "bathrooms",
                        "fids": [
                            12
                        ],
                        "fun": "to_max",
                        "fun_arg": 4,
                        "regex": "^.*$",
                        "targets": [
                            "bathrooms"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "Images",
                        "fids": [
                            13,
                            14
                        ],
                        "fun": "concat",
                        "fun_arg": ",",
                        "fun_init": "",
                        "map_fun": "forward",
                        "regex": "^.*$",
                        "targets": [
                            "geoposition"
                        ]
                    },
                    "class": "Multi"
                },
                {
                    "args": {
                        "comment": "rooms",
                        "fids": [
                            15
                        ],
                        "fun": "to_max",
                        "fun_arg": 4,
                        "regex": "^.*$",
                        "targets": [
                            "rooms"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "Size",
                        "fids": [
                            16
                        ],
                        "fun": "to_int",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "size"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "subject",
                        "fids": [
                            17
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "subject"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "subject - fixer",
                        "fids": [
                            17
                        ],
                        "fun": "no_son_iguales",
                        "fun_arg": "",
                        "map_fun": "make_subject",
                        "other_field": "subject",
                        "regex": "^.*$",
                        "targets": [
                            "subject"
                        ]
                    },
                    "class": "Conditional"
                },
                {
                    "args": {
                        "comment": "util_size",
                        "fids": [
                            18
                        ],
                        "fun": "to_int",
                        "fun_arg": null,
                        "regex": "^.*$",
                        "targets": [
                            "util_size"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "body",
                        "fids": [
                            19
                        ],
                        "fun": "forward",
                        "fun_arg": null,
                        "regex": "^(.|\n)+$",
                        "targets": [
                            "body"
                        ]
                    },
                    "class": "Simple"
                },
                {
                    "args": {
                        "comment": "A comment on the rule",
                        "fids": [
                            19
                        ],
                        "fun": "no_son_iguales",
                        "fun_arg": "",
                        "map_fun": "clean_body",
                        "other_field": "body",
                        "regex": "^(.|\n)+$",
                        "targets": [
                            "body"
                        ]
                    },
                    "class": "Conditional"
                },
                {
                    "args": {
                        "comment": "Remove geopos if not valid",
                        "fun": "value_in",
                        "fun_arg": [
                            "0,0"
                        ],
                        "to_check": "geoposition",
                        "to_remove": "geoposition"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "Remove bathrooms if 0",
                        "fun": "value_in",
                        "fun_arg": [
                            "0",
                            0
                        ],
                        "to_check": "bathrooms",
                        "to_remove": "bathrooms"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "remove rooms if 0",
                        "fun": "value_in",
                        "fun_arg": [
                            "0",
                            0
                        ],
                        "to_check": "rooms",
                        "to_remove": "rooms"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "A comment on the rule",
                        "fun": "value_in",
                        "fun_arg": [
                            3,
                            4,
                            5,
                            6
                        ],
                        "to_check": "estate_type",
                        "to_remove": "bathrooms"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "A comment on the rule",
                        "fun": "value_in",
                        "fun_arg": [
                            5,
                            6
                        ],
                        "to_check": "estate_type",
                        "to_remove": "util_size"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "A comment on the rule",
                        "fun": "value_in",
                        "fun_arg": [
                            "0",
                            0
                        ],
                        "to_check": "size",
                        "to_remove": "size"
                    },
                    "class": "Remove"
                },
                {
                    "args": {
                        "comment": "A comment on the rule",
                        "fun": "value_in",
                        "fun_arg": [
                            "0",
                            0
                        ],
                        "to_check": "util_size",
                        "to_remove": "util_size"
                    },
                    "class": "Remove"
                }
            ]
        }';

    /**
     * @return string with the static representation of the json IRules
     * The IRules are the rules needed to process ads images when using
     * TransmitData schema
     */
    public function getImageRules() {
        return json_encode(json_decode(self::$iRules));
    }

    /**
     * @return string with the static representation of the json Rules
     * The Rules are the rules needed to process ads when using
     * TransmitData schema
     */
    public  function getRules() {
        return json_encode(json_decode(self::$rules));
    }

}
