<?php
require_once('cpanel_module.php');

use Yapo\Cpanel;

function sort_list_time($a, $b) {
	return strcmp(@$b['list_time'], @$a['list_time']);
}

function sort_event($a, $b) {
	if ($a['sort_event'] == 'VIEW')
		return $b['views'] - $a['views'];
	else	
		return $b['mails'] - $a['mails'];
}

function only_numeric($var) {
	return is_numeric($var);
}

function ad_id_filter($value) {
	return is_numeric($value);
}

class cpanel_module_popular_ads extends cpanel_module {
	function cpanel_module_popular_ads() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.popular_ads');

		// Init this controlpanel module
		$this->init();
	}

	function stats() {
		global $BCONF;

		if (!class_exists('Redis')) {
			syslog(LOG_ERR, log_string()."Redis not available. Can't proceed.");
			return;
		}

		$sort = isset($_GET['sort']) ? $_GET['sort'] : 1;
		$event = isset($_GET['event']) ? $_GET['event'] : 'view';
		if (in_array($event, array('view', 'mail')))
			$event = strtoupper($event);
		else
			$event = 'VIEW';

		$roptions = array(
			'servers' => $GLOBALS['BCONF']['*']['common']['redisstat']['host'],
			'debug' => false,
			'id_separator' => $GLOBALS['BCONF']['*']['common']['session']['id_separator'],
			'timeout' => $GLOBALS['BCONF']['*']['common']['redisstat']['connect_timeout']
		);

//		$redis = new RedisSessionClient($roptions);
		$redisConf = bconf_get($BCONF, "*.common.redisstat.host." . bconf_get($BCONF, "*.common.redisstat.master"));
		try {
			$redis = new Redis();
			$redis->connect($redisConf['name'], $redisConf['port']);
		} catch(RedisException $ex) {
			syslog(LOG_WARN, log_string()."Redis connection failed.");
			return;
		}

		$sort_key_trans = array(0 => 'hour', 1 => 'day');

		$event_sort = array();
		$event_sort['VIEW'] = $this->getStatpointSort('VIEW', $sort);
		$event_sort['MAIL'] = $this->getStatpointSort('MAIL', $sort);

		$master_list  = $redis->zrevrange($event.':'.$event_sort[$event], 0, 100, 1);

		$all_stats = array(array());
		$other_events = array_diff(array('VIEW', 'MAIL'), array($event));
		foreach($master_list as $list_id => $list_count) {
			$all_stats[$event][$list_id] = $list_count;
			foreach($other_events as $other_event) {
				$response = (int)$redis->zscore($other_event.':'.$event_sort[$other_event], $list_id);
				if(empty($response))
					$response = "0";
				$all_stats[$other_event][$list_id] = $response;
			}
		}

		$transaction = new bTransaction("controlpanel");
		$transaction->add_array('list_id', array_filter(array_keys($master_list), 'only_numeric'));
		$reply = $transaction->send_admin_command('ad_info');
		Cpanel::handleReply($reply, $this->response);

		if (is_array(@$reply['ad'])) {
			$result = array();

			foreach ($reply['ad'] as $key => $ad) {
				$view_count  = $all_stats['VIEW'][$ad['list_id']];
				$mail_count  = $all_stats['MAIL'][$ad['list_id']];

				$result[$key]['ad_id'] = $ad['ad_id'];
				$result[$key]['action_id'] = $ad['action_id'];
				$result[$key]['status'] = $ad['status'];
				$result[$key]['list_time'] = $ad['list_time'];
				$result[$key]['subject'] = $ad['subject'];
				$result[$key]['type'] = $ad['type'];
				$result[$key]['views'] = $view_count;
				$result[$key]['mails'] = $mail_count;
				$result[$key]['sort_event'] = $event;
			}

			/* Sorting */
			if (isset($_GET['lt']))
				usort($result, 'sort_list_time');
			else
				usort($result, 'sort_event');

			$this->response->add_data('sort', $sort);
			$this->response->add_data('event', strtolower($event));
			$this->response->add_extended_array('ad', $result);
		}

		$this->display_results('controlpanel/popular_ads.html');
	}
	
	protected function display_module_root() {
		$this->redirect('stats') ;
	}
	
	function main($function = NULL) {
		$this->response->add_data('page_name_hide', 1);
		$this->response->add_data('menu_name_list_hide', 1);
		parent::main($function) ;
	}

	function getStatpointSort($event, $sort)
	{
		global $BCONF;

		$eventConf = array();
		foreach(bconf_get($BCONF, '*.statpoints.event') as $key => $value) {
			if(strtoupper($value["name"]) == strtoupper($event)) {
				$eventConf=$value;
				break;
			}
		}

		if (count($eventConf) == 0) {
			return '';
		}

		$hourBucket=array();
		$minuteBucket=array();

		foreach($eventConf["buckets"] as $key => $value) {
			if($value["buck_name"] == "hour")
				$hourBucket=$value;
			else if($value["buck_name"] == "minute")
				$minuteBucket=$value;
		}

		$time = time();

		if ($sort == '1') {
			// 24 H
			$buck_in_sec = $hourBucket["time_in_sec"];
			$buck_size = $hourBucket["buck_size"];
			$buck_gran = $hourBucket["granularity"];
			$hour = ((int)((($time / $buck_in_sec) % $buck_size) / (int)$buck_gran)) * $buck_gran; 
			$_sort = 'hour:'.$hour;
		} else if ($sort == '0') {
			// 1 H
			$buck_in_sec = $minuteBucket["time_in_sec"];
			$buck_size = $minuteBucket["buck_size"];
			$buck_gran = $minuteBucket["granularity"];
			$minute = ((int)((($time / $buck_in_sec) % $buck_size) / (int)$buck_gran)) * $buck_gran; 
			$_sort = 'minute:'.$minute;
		}
		return $_sort;
	}
}
?>
