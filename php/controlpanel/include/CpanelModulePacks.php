<?php

namespace Yapo;

use Yapo\Cpanel;
use bTransaction;

class CpanelModulePacks extends CpanelModule
{
    const AUTO = 1;
    const INMO = 2;

    private $trans;

    public function __construct($trans = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Packs'));
        $this->trans = isset($trans)? $trabs : new bTransaction();
        // Init this controlpanel module
        parent::init();
    }

    private function verifyEmail()
    {
        if (isset($_REQUEST['search_email'])) {
            $_REQUEST['search_email'] = trim($_REQUEST['search_email']);
            $email_a = $_REQUEST['search_email'];
            $this->response->add_data("search_email", $email_a);
            if (!filter_var($email_a, FILTER_VALIDATE_EMAIL)) {
                $this->response->add_data("error_email", "Email no v�lido");
                return false;
            }

            $this->response->add_data("valid_email", "true");
            return true;
        }
    }

    private function getDateFromFormattedDate($formatted_date)
    {
        if (substr_count($formatted_date, "/") != 2) {
            return null;
        }
        $nodes = explode("/", $formatted_date);
        return @sprintf("%04d-%02d-%02d", $nodes[2], $nodes[1], $nodes[0]);
    }

    private function listAndAssign($acc, $email)
    {
        if (isset($_REQUEST['packs'])) {
            $trans = $this->trans->reset();
            $trans->add_data('account_id', $acc);
            if ($_REQUEST['packs'][0] == 1) {
                $trans->add_data('type', 'car');
            } else {
                $trans->add_data('type', 'real_estate');
            }
            $trans->add_data('product_id', $_REQUEST['packs']);

            $trans->add_data('slots', $_REQUEST['pack_slots']);

            $exp_date =  $this->getDateFromFormattedDate($_REQUEST['expiration_date']);
            if (isset($exp_date)) {
                $trans->add_data('expiration_date', $exp_date);
            }

            $service_order = '';
            $service_amount = '';
            if (isset($_REQUEST['service_order']) && strlen(trim($_REQUEST['service_order'])) > 0) {
                $trans->add_data("service_order", trim(strip_tags($_REQUEST['service_order'])));
            }
            if (isset($_REQUEST['service_amount']) && strlen(trim($_REQUEST['service_amount'])) > 0) {
                $trans->add_data('service_amount', trim(strip_tags($_REQUEST['service_amount'])));
            }

            $info = $trans->send_admin_command('insert_pack');
            if ($trans->has_error(true)) {
                $this->response->add_data("pack_assign_fail", "No se pudo asignar el Pack");
                foreach ($trans->get_errors() as $param => $code) {
                    if ($code == "PERIOD;EXPIRATION_DATE;ERROR_XOR_MISSING") {
                        $this->response->add_data("err_expiration_date", lang("ERROR_DATE_EXPIRATION_PACK_INVALID"));
                    } else {
                        $this->response->add_data("err_$param", lang($code));
                    }
                }
            } else {
                $this->response->add_data("pack_assign_success", "Pack asignado con exito");
            }
        }
        $this->listPacks($email);
    }

    private function listPacks($email)
    {
        // Packs listing
        $trans = $this->trans->reset();
        $trans->add_data('email', $email);
        $trans->add_data('list_all', '1');
        $info = $trans->send_command('get_packs_by_account');
        if (!$trans->has_error(true)) {
            foreach ($info as $key => $val) {
                $this->response->add_data($key, $val);
            }
        } else {
            $this->response->add_data("unexpected_error", "ha ocurrido un error inesperado!");
        }
    }

    public function fixSlots()
    {
        if ($this->verifyEmail()) {
            $email = sanitizeVar($_REQUEST['search_email'], 'email');
            $this->expirePacks();
            $this->listPacks($email);

            $trans = $this->trans->reset();
            $trans->add_data('email', $email);
            $result = $trans->send_admin_command("fix_pack_slots");
            if ($trans->has_error(true)) {
                $this->response->add_data('err_fix_slots', $result['email']);
            } else {
                $this->response->add_data("released_slots", $result['o_released_slots']);
                $this->response->add_data("total_slots", $result['o_total_slots']);
                $this->response->add_data("pack_ads_count", $result['o_pack_ads_count']);
            }
        }
        $this->response->add_data('pack_type', sanitizeVar($_REQUEST['pack_type'], 'string'));
        $this->displayResults('controlpanel/packs/search.html');
    }

    public function searchAndAssignInmo()
    {
        $this->searchAndAssign(self::INMO);
    }

    public function searchAndAssignAuto()
    {
        $this->searchAndAssign(self::AUTO);
    }

    private function expirePacks()
    {
        $trans = $this->trans->reset();
        if (isset($_REQUEST['pack_id'])) {
            $trans->add_data('pack_id', sanitizeVar($_REQUEST['pack_id'], 'int'));
            $expire = $trans->send_command('expire_packs');
            if (!$trans->has_error(true)) {
                $this->response->add_data("valid_search", 1);
                foreach ($expire as $key => $val) {
                    $this->response->add_data($key, $val);
                }
            } else {
                $this->response->add_data("message_error", "ha ocurrido un error inesperado!");
            }
        }
    }

    private function searchAndAssign($pack_type)
    {
        global $BCONF;
        if ($this->verifyEmail()) {
            $email = sanitizeVar($_REQUEST['search_email'], 'email');
            $this->expirePacks();

            $trans = $this->trans->reset();
            $trans->add_data('email', $email);
            $account = $trans->send_command('get_account');

            if ($trans->has_error(true)) {
                $this->response->add_data("email_error_message", "El usuario ".$email." no tiene cuenta");
            } else {
                $pro_categories = isset($account['is_pro_for']) ? explode(",", $account['is_pro_for']) : array();
                $cp_pack_name = Bconf::get($BCONF, "*.packs.type.{$pack_type}.cp_name");
                $bconf_categories = array_keys(Bconf::get($BCONF, "*.packs.type.{$pack_type}.category"));
                $is_pro = array_intersect($bconf_categories, $pro_categories);
                // NO PRO
                if ($account['account_status']  == "pending_confirmation") {
                    $this->response->add_data("email_error_message", "Cuenta pendiente de activaci�n");
                } elseif ($account['account_status'] == "inactive") {
                    $this->response->add_data("email_error_message", "Cuenta inactiva");
                } elseif (empty($is_pro)) {
                    $this->response->add_data(
                        "email_error_message",
                        "Este usuario no es un profesional de la categor�a {$cp_pack_name}"
                    );
                } else {
                    $this->response->add_data("account_id", $account['account_id']);
                    $this->response->add_data("acc_name", $account['name']);
                    $this->response->add_data("acc_phone", $account['phone']);
                    $this->listAndAssign($account['account_id'], $email);
                }
            }
        }
        $this->displayResults('controlpanel/packs/search.html');
    }

    private function verifySearch()
    {
        if (isset($_REQUEST['search_type']) && in_array($_REQUEST['search_type'], array("uid","email"))) {
            $search_type =  $_REQUEST['search_type'];

            if (isset($_REQUEST['search_box'])) {
                $_REQUEST['search_box'] = trim($_REQUEST['search_box']);
                $search_box = $_REQUEST['search_box'];

                if ($search_type == "uid") {
                    if (!filter_var($search_box, FILTER_VALIDATE_INT)) {
                        $this->response->add_data("message_error", "UID no es v�lido.");
                    }
                } elseif (!filter_var($search_box, FILTER_VALIDATE_EMAIL)) {
                    $this->response->add_data("message_error", "Email no es v�lido.");
                }
            } else {
                $this->response->add_data(
                    "message_error",
                    $search_type == "uid" ? "No ha ingresado UID." : "No ha ingresado el email de la cuenta"
                );
            }
        } else {
            return false;
        }
        return !isset($this->response->data['message_error']);
    }

    private function validateYear($year)
    {
        global $BCONF;
        $blocket_current_years = array_copy(Bconf::get($BCONF, 'controlpanel.modules.adqueue.archive.schemas'));
        return in_array("blocket_".$year, $blocket_current_years, true);
    }

    private function partnerAdReplyReport($email, $partner, $year, $month)
    {
        if (!$this->validateYear($year)) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_YEAR'));
            return;
        }
        if (!$month) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_MONTH'));
            return;
        }
        $trans = $this->trans->reset();
        $trans->add_data('email', $email);
        $trans->add_data('partner', $partner);
        $trans->add_data('year', $year);
        $trans->add_data('month', $month);
        $info = $trans->send_command('send_partner_adreply_report');
        if ($trans->has_error(true)) {
            if (isset($info['error']) && $info['error'] == "ERROR_SEND_PARTNER_ADREPLY_REPORT_RESULT_EMPTY") {
                $this->response->add_data("message_error", lang($info['error']));
            } else {
                $this->response->add_data("message_error", lang('PARTNER_REPORT_UNEXPECTED_ERROR'));
            }
            return;
        }
        $this->response->add_data("valid_search", 1);
    }

    private function partnerOtherReports($email, $partner, $report_type, $year, $month)
    {

        if (!$this->validateYear($year)) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_YEAR'));
            return;
        }

        if (!$month) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_MONTH'));
            return;
        }

        $trans = $this->trans->reset();
        $trans->add_data('email', $email);
        $trans->add_data('partner', $partner);
        $trans->add_data('year', $year);
        $trans->add_data('month', $month);
        $info = $trans->send_command('send_partner_'.$report_type.'_report');

        if (!$trans->has_error(true)) {
            $this->response->add_data("valid_search", 1);
            return;
        }

        if ($info['error'] == "ERROR_SEND_PARTNER_MONTHLY_REPORT_RESULT_EMPTY"
            || $info['error'] == "ERROR_SEND_PARTNER_ACTIVE_REPORT_RESULT_EMPTY") {
            $this->response->add_data("message_error", lang($info['error']));
            return;
        }

        $this->response->add_data("message_error", "codigo de error: ".$info['error']);
    }


    public function partnerAdReport()
    {
        global $BCONF;
        $blocket_current_years = array_copy(Bconf::get($BCONF, 'controlpanel.archive_group.current.schemas'));
        $blocket_current_years = array_diff($blocket_current_years, array("public"));
        rsort($blocket_current_years);
        $this->response->add_data("current_years", $blocket_current_years);
        $this->displayResults('controlpanel/packs/send_partner_report.html');

        if (!isset($_GET['submit'])) {
            /* Avoid validation if it's not a form submit */
            return;
        }

        if (!isset($_GET['email']) || !filter_var($_GET['email'], FILTER_VALIDATE_EMAIL)) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_EMAIL'));
            return;
        }

        if (!isset($_GET['report_type']) || !in_array($_GET['report_type'], array('replies', 'monthly', 'active'))) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_REPORT_TYPE'));
            return;
        }

        if (!isset($_GET['partner'])) {
            $this->response->add_data("message_error", lang('PARTNER_REPORT_INVALID_PARTNER'));
            return;
        }
        $this->valid_range = array(
            'year' => array(
                "options" => array(
                    "min_range" => $this->config['partnerAdReport']['year']['min'],
                    "max_range" => $this->config['partnerAdReport']['year']['max']
                )
            ),
            'month' => array(
                "options" => array(
                    "min_range" => $this->config['partnerAdReport']['month']['min'],
                    "max_range" => $this->config['partnerAdReport']['month']['max']
                )
            )
        );
        $year = false;
        if (isset($_GET['year'])) {
            $year = filter_var($_GET['year'], FILTER_VALIDATE_INT, $this->valid_range['year']);
        }
        $month = false;
        if (isset($_GET['month'])) {
            $month = filter_var($_GET['month'], FILTER_VALIDATE_INT, $this->valid_range['month']);
        }

        if ($_GET['report_type'] == 'replies') {
               $this->partnerAdReplyReport($_GET['email'], $_GET['partner'], $year, $month);
        } else {
             $this->partnerOtherReports($_GET['email'], $_GET['partner'], $_GET['report_type'], $year, $month);
        }
    }

    public function updateProParam()
    {
        $this->updateAccounts();
        if ($this->verifySearch()) {
            $trans = $this->trans->reset();
            $trans->add_data($_REQUEST['search_type'], $_REQUEST['search_box']);
            $info = $trans->send_command('get_accounts_params');
            if (!$trans->has_error(true)) {
                $this->response->add_data("valid_search", 1);
                foreach ($info as $key => $val) {
                    $this->response->add_data($key, $val);
                }
            } else {
                $this->response->add_data("message_error", "ha ocurrido un error inesperado!");
            }
        }
        $this->displayResults('controlpanel/packs/search_accounts.html');
    }

    private function updateAccounts()
    {
        if (!empty($_POST['pro_accounts']) && !empty($_POST['pro_types']) && !empty($_POST['pro_actions'])) {
            $accounts = implode(",", $_POST['pro_accounts']);
            $pro_types = implode(",", $_POST['pro_types']);
            $pro_actions = implode(",", $_POST['pro_actions']);

            $trans = $this->trans->reset();
            $trans->add_data('accounts', $accounts);
            $trans->add_data('pro_types', $pro_types);
            $trans->add_data('pro_actions', $pro_actions);
            $trans->send_admin_command('accounts_change_pro_status');
            $this->response->add_data("message", $trans->has_error(true)? "Error": "Exito");
        }
    }

    public function links()
    {
        $this->displayResults('controlpanel/packs/links.html');
    }

    public function main($function = null)
    {
        if ($function != null) {
            $this->response->add_data("page_name", "Buscar");
            $this->response->fill_array('page_js', '/js/packs.js');
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->runMethod($function);
        } else {
            $this->links();
        }
    }
}
