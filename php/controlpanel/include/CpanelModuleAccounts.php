<?php

namespace Yapo;

use bTransaction;
require_once('util.php');
require_once('autoload_lib.php');

use Yapo\Cpanel;

class CpanelModuleAccounts extends CpanelModule
{

    public function __construct()
    {
        $this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.Accounts'));
        $this->init();
    }

    public function displayAccounts()
    {
        $this->displayResults('controlpanel/accounts/accounts.html');
    }

    private function createRandomPass()
    {
        return hash_password(openssl_random_pseudo_bytes(128));
    }

    public function activateAccount()
    {
        if (isset($_POST["submit"])) {
            $name = sanitizeVar($_POST['nameAccount'], 'string');
            $email = sanitizeVar($_POST['mailAccount'], 'string');
            $phone = sanitizeVar($_POST['phoneAccount'], 'string');

            $trans = new bTransaction();
            $trans->add_data('password', $this->createRandomPass());

            //Add info from form
            if (!empty($name)) {
                $trans->add_data('name', $name);
            }
            if (!empty($phone)) {
                $trans->add_data('phone', $phone);
            }
            if (!empty($email)) {
                $trans->add_data('email', $email);
            }

            $reply = $trans->send_command('activate_account');

            if (!$trans->has_error(true)) {
                $this->response->add_data("err_msg", "Se ha activado la cuenta de usuario correctamente");
                $this->response->add_data("create_msg_class", 'msg-success');
            } else {
                $this->response->add_data("create_msg_class", 'msg-error');
                if (isset($trans->_reply['error']) && $trans->_reply['error'] == 'ACCOUNT_ALREADY_EXISTS') {
                    $this->response->add_data("err_msg", $trans->_reply['error']);
                } else {
                    foreach ($trans->get_errors() as $param => $code) {
                        $this->response->add_data("err_msg", lang($param));
                        $this->response->add_data("err_reason", lang($code));
                    }
                }
            }
        }
        $this->displayAccounts();
    }

    public function main($function = null)
    {
        if (!is_null($function)) {
            $this->response->fill_array('page_css', '/css/Account.css');
            $this->runMethod($function);
        } else {
            header('Location: '.$_SERVER['REQUEST_URI'].'&a=activateAccount');
            die();
        }
    }
}
