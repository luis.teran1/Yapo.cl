<?php

namespace Yapo;

use DateTime;
use bTransaction;
use OneClickApiClient;
use PaymentScheduleApiClient;

class CpanelModuleSubscriptions extends CpanelModule
{

    private $trans;
    const REDIS = 'redis_cardata';

    public function __construct(
        $trans = null,
        $helper = null,
        $ocpClient = null,
        $pshClient = null,
        $redisInstance = null
    ) {
        $this->config = array_copy(Bconf::get($GLOBALS["BCONF"], "controlpanel.modules.Subscriptions"));
        $this->trans = isset($trans) ? $trans : new bTransaction();
        $this->helper = isset($helper) ? $helper : new SubscriptionsHelper($this->trans);
        $this->ocpClient = isset($ocpClient) ? $ocpClient : new OneClickApiClient();
        $this->pshClient = $pshClient;
        if (!isset($this->pshClient)) {
            $this->pshClient =  new PaymentScheduleApiClient();
        }
        $this->redis = isset($redisInstance) ? $redisInstance : new EasyRedis(self::REDIS);
        // Init this controlpanel module
        parent::init();
    }

    private function getRedisKey($source, $location, $email = null)
    {
        global $BCONF;
        if (isset($source) && isset($location) && isset($email)) {
            $main_key = "xdirect_msg_{$email}";
            $ending= "{$main_key}_{$source}_{$location}";
            return Bconf::get($BCONF, "*.common.".self::REDIS.".master").$ending;
        }
        return "";
    }

    private function pshClientAction($action)
    {
        $ok = true;
        $resp = "";
        if ($action == "up") {
            return  $this->pshClient->statusSubscription();
        } elseif ($action == "down") {
            $this->pshClient->newStatus = "canceled";
            return $this->pshClient->statusSubscription();
        } elseif ($action == "remove") {
            $this->pshClient->newStatus = "deleted";
            return $this->pshClient->statusSubscription();
        } elseif ($action == "pay") {
            return $this->pshClient->paySubscription();
        } elseif ($action == "reactivate") {
            $this->pshClient->newStatus = "active";
            return $this->pshClient->reactivateSubscription();
        }
    }

    private function links()
    {
        $this->displayResults("controlpanel/subscriptions/links.html");
    }

    private function processActions()
    {
        global $BCONF;
        if (!empty($_REQUEST["action"]) && !empty($_REQUEST["id"])) {
            $this->pshClient->subscriptionId = $_REQUEST["id"];
            $ok = true;
            $resp = "";
            list($ok, $resp) = $this->pshClientAction($_REQUEST["action"]);

            if ($ok) {
                // Redirect to remove action from url
                $cp_url = Bconf::get($BCONF, "*.common.base_url.controlpanel");
                $cp_url .= "/controlpanel?m=Subscriptions&a=search&s_email=".$_REQUEST["s_email"];
                header('Location: '.$cp_url);
                exit(0);
            }
            $this->response->add_data("err_action", $resp);
        }
    }

    private function activateMsg($email)
    {
        global $BCONF;
        $sources = array("desktop", "msite");
        $opMsg = $this->config["create"]["op_msg"];
        $link = Bconf::get($BCONF, "*.common.base_url.blocket").$opMsg["link"];
        $msg = $opMsg["msg"];
        $msg = str_replace('HREF_REPLACE', $link, $msg);
        $days = $opMsg["days"];
        $expire = date('Y-m-d H:i:s', strtotime("+{$days} days"));
        $time = time();
        $ttl = $days * 24 * 60 * 60;
        foreach ($sources as $source) {
            $key = $this->getRedisKey($source, 'dashboard', $email);
            $this->redis->hset($key, 'source', $source);
            $this->redis->hset($key, 'location', 'dashboard');
            $this->redis->hset($key, 'msg', $msg);
            $this->redis->hset($key, 'email', $email);
            $this->redis->hset($key, 'time', $time);
            $this->redis->hset($key, 'expire', $expire);
            $this->redis->expire($key, $ttl);
        }
    }

    private function getAccountInfo()
    {
        if (!empty($_REQUEST["s_email"])) {
            $this->response->add_data("s_email", $_REQUEST["s_email"]);
            $this->helper->populateAccount($_REQUEST["s_email"], $this->response);
        }
    }

    public function search()
    {
        $this->getAccountInfo();
        if (!empty($_REQUEST["s_email"])) {
            $this->processActions();
            $this->response->add_data("s_email", $_REQUEST["s_email"]);
            $this->pshClient->email = $_REQUEST["s_email"];
            list($ok, $resp) = $this->pshClient->searchSubscriptions();
            if ($ok) {
                $this->helper->populateSubscriptionList($resp, $this->response);
            }
        }
        $this->displayResults("controlpanel/subscriptions/main.html");
    }

    public function create()
    {
        $this->getAccountInfo();

        if (isset($_REQUEST["submit"]) && $_REQUEST["submit"] == "Crear") {
            $this->pshClient->accountId = intval($_REQUEST["account_id"]);
            $this->pshClient->productId = intval($_REQUEST["product_id"]);
            $this->pshClient->billingType = $_REQUEST["billingType"];
            $this->pshClient->email = $_REQUEST["s_email"];
            $this->pshClient->accountName = $this->cleanString($_REQUEST["s_name"]);
            $this->pshClient->adminEmail = $_REQUEST["adminEmail"];
            $this->pshClient->adminName = $this->cleanString($_SESSION["controlpanel"]["admin"]["username"]);

            list($ok, $resp) = $this->pshClient->createSubscription();
            if (!$ok) {
                $err = json_decode($resp);
                if ($err) {
                    $this->response->add_data("err_create", $err->ErrorMessage);
                } else {
                    $this->response->add_data("err_create", $resp);
                }

                $this->displayResults("controlpanel/subscriptions/main.html");
                return;
            }
            $this->activateMsg($_REQUEST["s_email"]);
            header("Location:{$_SERVER['PHP_SELF']}?m={$_REQUEST['m']}&a=createSuccess");
        }
        $this->displayResults("controlpanel/subscriptions/main.html");
    }

    public function createSuccess()
    {
        $this->response->add_data("create_status", "success");
        $this->displayResults("controlpanel/subscriptions/main.html");
    }

    /**
     * Verifies format in date requests when its called and returns an array with date and validation
     * @param $typeDate string that contains the request var name to be find and validated
     * @param $days int to be used to generate default date in case there is not date assigned
     * @return array [ date, false or Format error msg ] false its intended to be requested like got error? false
     **/
    private function getOrValidateDate($typeDate, $days = "0")
    {
        $format = 'Y-m-d';
        if (isset($_REQUEST[$typeDate])) {
            $date = str_replace(' ', '', $_REQUEST[$typeDate]);
            $d = DateTime::createFromFormat($format, $date);
            $valid = $d && $d->format($format) == $date;
            return array($date, ($valid == false ? "Format Error" : false));
        }
        return array(date('Y-m-d', strtotime($days . ' days')), false);
    }

    /**
     * Sanitize var using email validation
     */
    private function getOrValidateEmail($email)
    {
        if (empty($_REQUEST[$email])) {
            return "";
        }
        return sanitizeVar($_REQUEST[$email], 'email');
    }
    /**
     * Func to call payment schedule report MS endpoint after validating params
     * @params do not accept params directly but it finds in $_REQUEST values required
     ** to call the MS if they are valid, so all the
     ** business logic is handled there.
     */
    public function reportByRange()
    {
        list($startDate, $startErr) = $this->getOrValidateDate("startDate", -30);
        list($endDate, $endErr) = $this->getOrValidateDate("endDate");

        $reportEmail = $this->getOrValidateEmail("rEmail");

        $this->response->add_data("startDate", $startDate);
        $this->response->add_data("startErr", $startErr);
        $this->response->add_data("endDate", $endDate);
        $this->response->add_data("endErr", $endErr);
        $this->response->add_data("rEmail", $reportEmail);

        if (isset($_REQUEST["submit"]) && $_REQUEST["submit"] == "Ejecutar"
            && $endErr === false && $startErr === false) {
            $this->pshClient->params = "{$startDate}/{$endDate}";
            if (!empty($reportEmail)) {
                $this->pshClient->params = "{$this->pshClient->params}/{$reportEmail}";
            }
            list($ok, $data) = $this->pshClient->reportByRangeSubscription(!empty($reportEmail));
            if ($ok) {
                if (!empty($reportEmail)) {
                    if ($data == "send") {
                        $this->response->add_data("success_send_email", $data);
                    } else {
                        $this->response->add_data("err_send_email", $data);
                    }
                } else {
                    $this->helper->populateReportList($data, $this->response);
                }
            } else {
                $this->response->add_data("err_create", $data);
            }
        }
    }

    /**
     * Intended to work as router between reports in case more reports are needed in future
     */
    public function report()
    {
        if (isset($_REQUEST["reportType"])) {
            switch ($_REQUEST["reportType"]) {
                case 'reportByRange':
                    $this->reportByRange();
            }
        }
        $this->displayResults("controlpanel/subscriptions/main.html");
        return;
    }

    public function main($function = null)
    {
        $createPriv = Cpanel::hasPriv("Subscriptions.create") ?  "1": "0";
        $reportPriv = Cpanel::hasPriv("Subscriptions.report") ?  "1": "0";
        $searchPriv = Cpanel::hasPriv("Subscriptions.search") ?  "1": "0";
        // Subprivs
        $activatePriv = Cpanel::hasPriv("Subscriptions.activate") ?  "1": "0";
        $deletePriv = Cpanel::hasPriv("Subscriptions.delete") ?  "1": "0";
        $unsubscribePriv = Cpanel::hasPriv("Subscriptions.unsubscribe") ?  "1": "0";

        $this->response->add_data("priv_subscriptions_create", $createPriv);
        $this->response->add_data("priv_subscriptions_search", $searchPriv);
        $this->response->add_data("priv_subscriptions_report", $reportPriv);

        $this->response->add_data("priv_subscriptions_activate", $activatePriv);
        $this->response->add_data("priv_subscriptions_delete", $deletePriv);
        $this->response->add_data("priv_subscriptions_unsubscribe", $unsubscribePriv);
        if ($function) {
            $this->response->add_data("page_name", "Suscripciones");
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->response->fill_array("page_js", "/js/Subscriptions.js");
            $this->runMethod($function);
        } else {
            $this->links();
        }
    }

    /**
    * Replace all accents with their equivalents without them
    *
    * @param $string
    *  string to clean
    *
    * @return $string
    *  string cleaned
    */
    private function cleanString($string)
    {
        $string = trim($string);
        $string = iconv('ISO-8859-1', 'ASCII//TRANSLIT', $string);
        return $string;
    }
}
