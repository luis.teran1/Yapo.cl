<?php
require_once('cpanel_module.php');

function name_sort($a, $b) {
	return strcasecmp($a['name'], $b['name']);
}

define('FAKE_PWD', '--------');

use Yapo\Cpanel;

class cpanel_module_admin extends cpanel_module {

	function cpanel_module_admin() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.admin');

		// Init this controlpanel module
		$this->init();
	}
	

	/*
	 * MENU: editadmin
	 * 
         * Present the dialog to add/change/delete a user
	 * 
	 */
	function editadmin() {

		if (isset($_REQUEST['save'])) {
			$this->saveadmin();
			return;
		}
		if (isset($_REQUEST['delete'])) {
			$this->deleteadmin();
			return;
		}

		$transaction = new bTransaction();

		/* Request and lock */
		if (isset($_REQUEST['admin_id'])) {
			$transaction->add_data('admin_id', $_REQUEST['admin_id']);
			$reply = $transaction->send_admin_command('listadmins');

			Cpanel::handleReply($reply, $this->response);

			if ($this->response->has_error()) {
				$this->response->add_data("error_message", lang("TRANS_UNEXPECTED_ERROR"));
			} else {                        

				$keys = array('admin_id', 'username',
					      'fullname', 'jabbername',
					      'email', 'mobile', 'valid_to');

				foreach ($keys as $key) {
					foreach ($reply['admin'] as $id => $arr)
						$this->response->fill_array($key, $arr[$key]);
				}
				$this->response->add_data('fake_pwd', FAKE_PWD);
			}

			$privs = explode(',', $reply['admin'][0]['privs']);
			$this->fillin_has_priv($privs);

			if ($this->response->has_error()) {
				$this->response->add_data("error_message", lang("TRANS_UNEXPECTED_ERROR"));
			}
		}

		$this->display_all_privs();
		$this->display_results('controlpanel/admin_editadmin.html');
	}


        /*
         * Function: fillin_has_priv
         * Fill the array "has_priv" with 0/1 for corresponding privs.
         */
        function fillin_has_priv(&$priv_array) {
		$admin_privs = array();

		foreach ($priv_array as $priv) {
			if (preg_match('/\./', $priv)) {
				list ($priv, $key) = explode('.', $priv, 2);
				if (preg_match('/=/', $key)) {
					list ($key, $val) = explode('=', $key, 2);
				} else {
					$val = 1;
				}

				$admin_privs['sub'][$priv][$key] = $val;
			} else {
				$admin_privs['main'][$priv] = 'checked';
			}
		}

		$this->response->add_extended_array('privs', $admin_privs);
        }

	/* Get all privs to sort by names */
	function display_all_privs() {
		global $BCONF;

		$modules_privs = array();
		foreach (bconf_get($BCONF, 'controlpanel.modules') as $module_id => $module) {
			$module_privs[$module_id]['name'] = $module['name'];
			if (isset($module['subprivs'])) {
				foreach ($module['subprivs'] as $key => $subpriv) {
					$module_privs[$module_id]['subprivs'][$key]['name'] = $subpriv['name'];
					$module_privs[$module_id]['subprivs'][$key]['type'] = $subpriv['type'];
				}

				uasort($module_privs[$module_id]['subprivs'], 'name_sort');
			}
		}
		foreach (bconf_get($BCONF, 'controlpanel.admin_privs') as $priv => $name) {
			$module_privs[$priv]['name'] = $name;
		}

		uasort($module_privs, 'name_sort');

		$this->response->add_extended_array('module_privs', $module_privs);
	}

	/*
	 * Function: saveadmin
	 * 
         * Handle edit or new admin
	 * 
	 */
	function saveadmin() {
		global $BCONF;
		$transaction = new bTransaction();
		$error = 0;

		$keys = array('admin_id', 'username', 'fullname', 
			      'jabbername', 'email', 'mobile');

		foreach ($keys as $key) {
			if (!empty($_REQUEST[$key]))
				$transaction->add_data($key, $_REQUEST[$key]);
		}

		if (isset($_POST["main_privs"])) {
			foreach ($_POST['main_privs'] as $main => $priv) {
				$admin_privs[] = $main;
			}
		}
		if (isset($_POST["sub_privs"])) {
			foreach ($_POST['sub_privs'] as $main_priv => $sub_privs) {
				if (is_array($sub_privs)) {
					foreach ($sub_privs as $priv => $value) {
						if (bconf_get($BCONF, "controlpanel.modules.$main_priv.subprivs.$priv.type") == "checkbox") {
							$admin_privs[] = $main_priv.'.'.$priv;
						} else if (preg_match("/^[a-zA-Z_0-9]+$/", $value)) {
							$admin_privs[] = $main_priv.'.'.$priv.'='.$value;
						}
					}
				}
			}
		}
		if (is_array($admin_privs)) {
			$transaction->add_data("privs", implode(",", $admin_privs));
		} else {
			$transaction->add_data("privs", "");
		}

		if (!empty($_REQUEST["pd"]) && $_REQUEST["pd"] != FAKE_PWD) {
			$transaction->add_data("passwd", sha1($_REQUEST["pd"]));
		}

		/* Check that password was confirmed */
		if ($_REQUEST["pd"] != $_REQUEST["pd_confirm"]) {
			syslog(LOG_INFO, log_string()."User entered mismatching passwords (creation)");
			$this->response->add_error("err_passwd", ERROR_PASSWORD_MISMATCH);
			$error = 1;
		} else {
			/* Send transaction */
			$reply = $transaction->send_admin_command('saveadmin');
			
			Cpanel::handleReply($reply, $this->response);
			if ($this->response->has_error() || strstr($reply['status'], 'ERROR') !== FALSE) {
				$error = 1;
			}
		}
		if ($error) {
			$errors = $this->response->get_errors();
			foreach ($errors as $error => $msg) {
				if ($error == 'err_database' && strpos($msg, 'duplicate key') !== false) {
					$this->response->add_data('duplicated_user', '1');
				}
			}

			foreach ($keys as $key) {
				// When a duplicated user is inserted there is no admin_id
				// this is the normal behaviour.
				if (isset($_REQUEST[$key]))
					$this->response->add_data($key, $_REQUEST[$key]);
			}

			if (isset ($admin_privs))
				$privs = $admin_privs;
			else {
				$transaction = new bTransaction();
				$transaction->add_data('admin_id', $_REQUEST['admin_id']);
				$reply = $transaction->send_admin_command('listadmins');

				Cpanel::handleReply($reply, $this->response);
				$privs = explode(',', $reply['admin'][0]['privs']);
			}
			$this->fillin_has_priv($privs);
			$this->display_all_privs();

			return $this->display_results('controlpanel/admin_editadmin.html');
		}
		$this->response->add_data("ok_message", lang("ADMIN_SAVED_OK"));

		$this->listadmins();
	}

	/*
	 * Function: deleteadmin
	 * 
	 */
	function deleteadmin() {
		$transaction = new bTransaction();
                $transaction->add_data("admin_id", $_REQUEST["admin_id"]);

                $reply = $transaction->send_admin_command('deleteadmin');

                Cpanel::handleReply($reply, $this->response);

                if ($this->response->has_error()) {
                        $this->response->add_data("error_message", lang("TRANS_UNEXPECTED_ERROR"));
                } else {
                        $this->response->add_data("ok_message", lang("ADMIN_DELETED_OK"));

                }

                $this->listadmins();
	}

	/*
	 * MENU: listadmins
	 * 
         * Present a list of all admins and their data.
	 * 
	 */
	function listadmins() {
		$transaction = new bTransaction();
		
                /* Request and lock */
                $reply = $transaction->send_admin_command('listadmins');

                Cpanel::handleReply($reply, $this->response);
		
		if (isset($_REQUEST["show_deleted"]))
			$this->response->add_data("show_deleted", $_REQUEST["show_deleted"]);
                if ($this->response->has_error()) {
                        $this->response->add_data("error_message", lang("TRANS_UNEXPECTED_ERROR"));
                } else {                        

                        $keys = array('admin_id', 'username', 'fullname', 
                                      'jabbername', 'email', 'mobile', 'valid_to', 'status');

                        foreach ($keys as $key) {
                                foreach ($reply['admin'] as $id => $arr)
                                        $this->response->fill_array("admin_".$key, $arr[$key]);
                        }

                        $this->display_results('controlpanel/admin_listadmins.html');

                }
	}
	
	function main($function = NULL) {
		if ($function != NULL) {
			$this->run_method($function);
		} else {
			$this->display_results('controlpanel/admin.html');
		}
	}
}
?>
