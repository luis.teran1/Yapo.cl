<?php
require_once('cpanel_module.php');

use Yapo\Cpanel;

class cpanel_module_filter extends cpanel_module {
	function cpanel_module_filter() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.filter');

		// Init this controlpanel module
		$this->init();
	}

	function edit_list_item() {
		$transaction = new bTransaction();
		if (@$_GET['item_id']) { 
			$transaction->add_data('action', 'edit');
			$transaction->add_data('item_id', $_GET['item_id']);
		} else 
			$transaction->add_data('action', 'add');

		$transaction->add_data('list_id', $_GET['list_id']);
		$transaction->add_data('value', trim($_GET['value']));
		$transaction->add_data('notice', trim($_GET['notice']));
		
		$reply = $transaction->send_admin_command('blocked_item');
		Cpanel::handleReply($reply, $this->response);
		
		$json_out = isset($_GET['json_out']) && $_GET['json_out'] == 't';

		if($json_out) {
			exit(json_encode(array('error_msg' => $transaction->get_errors(), 'success' => !$transaction->has_error())));
			return;
		}


		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code) {
				$this->response->add_data("err_$param", lang($code));
			}
		} else
			$this->response->add_data('entry_saved', lang('ENTRY_SAVED'));

		$this->list_item();
	}
	
	function edit_category_list() {
		/* Categories can be unassigned and in this case the system should do nothing */
		if(isset($_POST['categories'])) {
			$transaction = new bTransaction();
			$transaction->add_data('action', 'set_categories');
			$transaction->add_data('list_id', $_REQUEST['list_id']);
			$transaction->add_data('category', $_POST['categories']);
			
			$reply = $transaction->send_admin_command('blocked_item');
			Cpanel::handleReply($reply, $this->response);
			
			if ($transaction->has_error()) {
				/* Error */
				foreach ($transaction->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));	
				}
			} else
				$this->response->add_data('entry_saved', lang('ENTRY_SAVED'));
		}
		$this->list_item();
	}

	function edit_country_list() {
		$transaction = new bTransaction();
		$transaction->add_data('action', 'set_countries');
		$transaction->add_data('list_id', $_REQUEST['list_id']);
		$transaction->add_data('country', $_REQUEST['country']);
		$reply = $transaction->send_admin_command('blocked_item');
		Cpanel::handleReply($reply, $this->response);
		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code) {
				$this->response->add_data("err_$param", lang($code));
			}
		} else
			$this->response->add_data('entry_saved', lang('ENTRY_SAVED'));
		$this->list_item();
	}

	function edit_refusal_reason_list() {
		if(isset($_POST['refusals'])) {
			$transaction = new bTransaction();
			$transaction->add_data('action', 'set_refusals');
			$transaction->add_data('list_id', $_REQUEST['list_id']);
			$transaction->add_data('refusal_reason', $_POST['refusals']);

			$reply = $transaction->send_admin_command('blocked_item');
			Cpanel::handleReply($reply, $this->response);

			if ($transaction->has_error()) {
				/* Error */
				foreach ($transaction->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} else
				$this->response->add_data('entry_saved', lang('ENTRY_SAVED'));
		}
		$this->list_item();
	}
 
	function del_list_item() {
		if ($_GET['item_id']) { 
			$transaction = new bTransaction();
			$transaction->add_data('action', 'del');
			$transaction->add_data('item_id', $_GET['item_id']);

			$reply = $transaction->send_admin_command('blocked_item');
			Cpanel::handleReply($reply, $this->response);

			if ($transaction->has_error()) {
				/* Error */
				foreach ($transaction->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} else
				$this->response->add_data('entry_saved', lang('ENTRY_SAVED'));
		}

		$this->list_item();
	}

	function get_single_item() {
		$transaction = new bTransaction();

		$transaction->add_data('action', 'list');
		$transaction->add_data('list_id', $_REQUEST['list_id']);
		$transaction->add_data('item_id', $_REQUEST['item_id']);

		$reply = $transaction->send_admin_command('blocked_item');

		$transaction->handle_reply();

		if (isset($reply['blocked_item'])) {
			foreach ($reply['blocked_item'] as $item) {
				$this->response->add_data('value', @$item['value']);
				$this->response->add_data('notice', @$item['notice']);
			}
		}

		$this->list_item();
	}

	function send_blocked_item($list_id, $q = '') {
		$transaction = new bTransaction();
		$transaction->add_data('action', 'list');
		$transaction->add_data('list_id', $list_id);

		if ($q != '') {
			$transaction->add_data('search_string', $q);
		}

		$reply = $transaction->send_admin_command('blocked_item');
		Cpanel::handleReply($reply, $this->response);

		if (isset($reply['blocked_item'])) {
			$this->response->add_extended_array('blocked_item', $reply['blocked_item']);
		}
		$this->response->add_data('list_type', $reply['list_type']);
	}

	function list_item() {
		// Get list_name
		$this->lists();

		$q = ''; /* Required to set a value to the query before */

		$list_id = (int)$_REQUEST['list_id'];

		if (isset($_REQUEST['list_type']) && $_REQUEST['list_type'] == 'category') {
			$this->send_blocked_item($list_id);
		}
		elseif (isset($_REQUEST['list_type']) && $_REQUEST['list_type'] == 'country') {
			$this->send_blocked_item($list_id);
		}
		elseif (isset($_REQUEST['list_type']) && $_REQUEST['list_type'] == 'refusal_reason') {
			$this->send_blocked_item($list_id);
			if ($this->dyn_bconf = get_dynamic_bconf()) {
				$refusals = bconf_get($this->dyn_bconf, '*.*.common');
				$this->response->add_extended_array($refusals);
			}
		}
		else {
			$min_length = bconf_get($GLOBALS['BCONF'], "*.controlpanel.modules.filter.lists.min_search_query");
			$max_length = bconf_get($GLOBALS['BCONF'], "*.controlpanel.modules.filter.lists.max_search_query");

			if (isset($_GET['q'])) {
				$q       = trim($_GET['q']);

				if (($min_length == 0 || strlen($q) >= $min_length) && ($max_length == 0 || strlen($q) <= $max_length)) {
					$this->send_blocked_item($list_id, $q);
				}
				else {
					$this->response->add_data('error_invalid_search', 'CONTROLPANEL_ERROR_INVALID_QUERY_STRING');
					syslog(LOG_INFO, "Length of search string '$q' is below minimum ($min_length) or above maximum ($max_length).");
				}
				$this->response->add_data('q', $_GET['q']);
			}

			$this->response->add_data('max_length', $max_length);
		}	
		$this->response->add_data('list_id', $list_id);

		$this->display_results('controlpanel/filter_add_list_item.html');    
	}

	function del_list() {
		if (isset($_GET['list_id'])) {
			$list_id = intval($_GET['list_id']);
			$list_type = bconf_get($GLOBALS['BCONF'], "*.common.blocked_lists.$list_id.type");
			if (!empty($list_type) || $list_type != 'base_list') {
				/* Check if the list has associated a rule and throw an error if it happend. */
				$transaction = new bTransaction();
				$transaction->add_data('action', 'list');
				$transaction->add_data('list_id', $_GET['list_id']);
				$reply = $transaction->send_admin_command('block_rule');

				if(!isset($reply["block_rule"])) {
					$transaction = new bTransaction();
					$transaction->add_data('action', 'del');
					$transaction->add_data('list_id', intval($_GET['list_id']));
					$reply = $transaction->send_admin_command('block_list');
					$transaction->handle_reply();

					if ($reply['status'] == 'TRANS_OK') {
						$this->redirect('lists');
					}
				} else {
					$this->response->add_data('error_msg','A rule depend on this list');
				}
			}
		}

		$this->display_results('controlpanel/filter_del_error.html');    
	}


	function add_list() {
		if (isset($_GET['list_id'])) {
			$list_id = intval($_GET['list_id']);
			$list_type = bconf_get($GLOBALS['BCONF'], "*.common.blocked_lists.$list_id.type");
			if (!empty($list_type) && $list_type == 'base_list') {
				$this->redirect('lists');
			}
		}

		if (!empty($_POST['list_name'])) {
			$transaction = new bTransaction();
			$transaction->add_data('list_name', $_POST['list_name']);
			if (isset($_POST['list_type']))
				$transaction->add_data('list_type', $_POST['list_type']);
			else
				$transaction->add_data('list_type', "");
			if (isset($_GET['list_id'])) {
				$transaction->add_data('action', 'edit');
				$transaction->add_data('list_id', intval($_GET['list_id']));
			} else {
				$transaction->add_data('action', 'add');
			}
			$reply = $transaction->send_admin_command('block_list');
			$transaction->handle_reply();

			if ($reply['status'] == 'TRANS_OK')
				$this->redirect('lists');
			else {
				if ($transaction->has_error()) {
					$errors = $transaction->get_errors();
					foreach ($errors as $key => $value) {
						$this->response->add_error('err_' . $key, $value);
					}
				}
			}
		} else if (isset($_GET['list_id']))
			// Get list_name
			$this->lists();

		$this->display_results('controlpanel/filter_add_list.html');    
        }


	function get_lists($id = NULL) {
		$transaction = new bTransaction();
		$transaction->add_data('action', 'list');
		if (!empty($id))
			$transaction->add_data('list_id', $id);
		$reply = $transaction->send_admin_command('block_list');
		$transaction->handle_reply();
		
		if (isset($reply['block_list'])) {
			foreach ($reply['block_list'] as $list) {
				if ($list['base_list'] == 't') {
					$this->response->fill_array('base_lists', $list['list_name']);
					$this->response->fill_array('base_lists_id', $list['list_id']);
					$this->response->fill_array('base_lists_type', $list['list_type']);
				} else {
					$this->response->fill_array('lists', $list['list_name']);
					$this->response->fill_array('lists_id', $list['list_id']);
					$this->response->fill_array('lists_type', $list['list_type']);
				}
			}
		}
	}

	function lists() {
		$this->response->add_data('page_name_hide', true);

		$this->get_lists(@$_REQUEST['list_id']);

		$this->display_results('controlpanel/filter_lists.html');
	}

	function del_rule() {
		if (isset($_GET['rule_id'])) {
			$transaction = new bTransaction();
			$transaction->add_data('action', 'del');
			$transaction->add_data('rule_id', intval($_GET['rule_id']));
			$reply = $transaction->send_admin_command('block_rule');
			$transaction->handle_reply();

			if ($reply['status'] == 'TRANS_OK') {
				$this->redirect('rules');
			}
		}

		$this->display_results('controlpanel/filter_del_error.html');
	}

	function get_rules($rule_id = NULL) {
		$transaction = new bTransaction();
		$transaction->add_data('action', 'list');
		if (!empty($rule_id))
			$transaction->add_data('rule_id', $rule_id);
		$reply = $transaction->send_admin_command('block_rule');


		$transaction->handle_reply();

		if (isset($reply['block_rule'])) {
			foreach ($reply['block_rule'] as $rule) {
				$this->response->fill_array('rule_id', $rule['rule_id']);
				$this->response->fill_array('rule_name', $rule['rule_name']);
				$this->response->fill_array('combine_rule', $rule['combine_rule']);
				$this->response->fill_array('application', $rule['application']);
				$this->response->fill_array('action', $rule['action']);
				$this->response->fill_array('target', $rule['target']);
				$this->response->fill_array('count_yesterday', $rule['count_yesterday']);
				$this->response->fill_array('count_today', $rule['count_today']);
			}
		}

		if (isset($reply['block_rule_condition'])) {
			ksort($reply['block_rule_condition']);
			foreach ($reply['block_rule_condition'] as $condition) {
				$fields[] = explode(",", preg_replace("/{|}/", "", $condition['fields']));
			}
			$this->response->add_extended_array('block_rule_condition_fields', $fields);
			$this->response->add_extended_array('block_rule_condition', $reply['block_rule_condition']);
		}
	}

	function rules() {
		$this->response->add_data('page_name_hide', true);
		$this->get_rules(@$_GET['rule_id']);

		$this->display_results('controlpanel/filter_rules.html');
	}

	function add_rule() {
		$this->response->add_data('page_name_hide', true);
		$unpaid_rule_name = bconf_get($GLOBALS['BCONF'], "*.common.unpaid_list.name");
		$rule_name_check = true;

		if (isset($_POST['old_rule_name']) && isset($_POST['rule_name'])) {
			$old_rule_name = $_POST['old_rule_name'];
			$rule_name = $_POST['rule_name'];
			if (strpos($old_rule_name, $unpaid_rule_name) !== false) {
				if (strpos($rule_name, $unpaid_rule_name) === false) {
					$this->response->add_error('err_rule_name', 'ERROR_UNPAID_RULE');
					$rule_name_check = false;
				}
			}
		}


		if (!empty($_POST['combine_rule'])) {
			if ($rule_name_check) {
				$transaction = new bTransaction();
				$transaction->add_data('action', 'add');
				if (!empty($_GET['rule_id']))
					$transaction->add_data('rule_id', intval($_GET['rule_id']));

				$transaction->add_data('rule_name', $_POST['rule_name']);

				$transaction->add_data('application', $_POST['application']);
				$transaction->add_data('combine_rule', $_POST['combine_rule']);
				if (isset($_POST['action']))
					$transaction->add_data('rule_action', $_POST['action']);
				else
					$transaction->add_data('rule_action', "");

				if (!empty($_POST['target']))
					$transaction->add_data('rule_target', $_POST['target']);

				$field_names = bconf_get($GLOBALS['BCONF'], "*.common.filter_rule.option");
				$fields = array();
				foreach($field_names as $key => $value) {
					foreach($value as $key2 => $value2) {
						if($key2 == "name")
							$fields[] = $value2;
					}
				}

				/* Conditions */
				for ($i = 1; isset($_POST['whole_word'][$i]); $i++) {
					for ($j = 0; $j < sizeof($fields);$j++) {
						if (isset($_POST['check_' . $fields[$j]][$i]) && $_POST['check_' . $fields[$j]][$i] == 't') {
							if (!isset($field[$i]))
								$field[$i] = $fields[$j];
							else
								$field[$i] .= ',' . $fields[$j];
						}
					}
				}

				$transaction->add_array('list_id', $_POST['list_id']);
				$transaction->add_array('whole_word', $_POST['whole_word']);
				if (isset($field))
					$transaction->add_array('fields', $field);

				$reply = $transaction->send_admin_command('block_rule');
				$transaction->handle_reply();
				if ($reply['status'] == 'TRANS_OK') {
					$this->redirect('rules');
				} else {
					$errors = $transaction->get_errors();

					foreach ($errors as $key => $value) {
						if (preg_match("/ARRAY/", $value) ||
							preg_match("/FIELDS_MISSING/", $value) ||
							preg_match("/RULE_ACTION_INVALID/", $value)
						) {
						$this->response->add_error('err_condition', $value);
						} else {
							$this->response->add_error('err_' . $key, $value);
						}
					}
				}
			}
			$this->response->add_data('rule_name', $_POST['rule_name']);
			$this->response->add_data('combine_rule', $_POST['combine_rule']);
			$this->response->add_data('application', $_POST['application']);
			$this->response->add_data('action', $_POST['action']);
			$this->response->add_data('target', $_POST['target']);

			if (isset($_POST['whole_word'])) {
				foreach ($_POST['whole_word'] as $key => $value) {
					$condition_extended[$key]['list_id'] = $_POST['list_id'][$key];
					$condition_extended[$key]['whole_word'] = $value;
				}
			}

			if (isset($field)) {
				foreach ($field as $key => $value) {
					$condition_fields[] = explode(",", $value);
				}
				$this->response->add_extended_array('block_rule_condition_fields', $condition_fields);
			}
			$this->response->add_extended_array('block_rule_condition', $condition_extended);
		} else {
			if (isset($_GET['rule_id'])) {
				$this->get_rules($_GET['rule_id']);
			}
		}
		$this->get_lists();

		$this->display_results('controlpanel/filter_add_rule.html');
	}

	/*
	 * Spam overview
	 */
	function spamfilter() {
		$transaction = new bTransaction();
		$transaction->add_data('action', 'list');
		$reply = $transaction->send_admin_command('spamfilter');
		$transaction->handle_reply();

		if (!$transaction->has_error()) {
			ksort($reply['spamfilter'], SORT_NUMERIC);
			$last_ip = null;
			foreach ($reply['spamfilter'] as $key => $data) {
				if (is_array($data)) {
					if ($data['remote_addr'] == $last_ip) {
						$data['remote_addr'] = "";
						$data['min_date'] = "";
					} else {
						$last_ip = $data['remote_addr'];
						$data['min_date'] = substr($data['min_date'], 0, 16);
					}
				} else {
					$this->response->add_data($key, $data);
					unset($reply['spamfilter'][$key]);
				}
			}

			$this->response->add_extended_array('spamfilter', $reply['spamfilter']);
		}

		$this->display_results('controlpanel/filter_spamfilter_list.html');
	}

	/* For index page numbers */
	function get_suffix_replace_array($menu_id) {
		if ($menu_id == 'spamfilter') {
			$transaction = new bTransaction();
			$transaction->add_data('action', 'count');
			$reply = $transaction->send_admin_command('spamfilter');

			$transaction->handle_reply();
			Cpanel::handleReply($reply, $this->response);
			if (!$transaction->has_error())
				return array ('#num_of_mails#' => $reply['spamfilter']['num_of_mails'], '#num_of_ips#' => $reply['spamfilter']['num_of_ips']); 
		}
		return NULL;
	}
	
	function spamfilter_control() {
		/* The list of possible senders you can put notice on, the first should always be the remote_addr */
		$possible_senders = array($_GET['sender_ip']);
		
		$transaction = new bTransaction();

		/* Save notices */
		if (isset($_GET['action']) && $_GET['action'] == 'save_notices') {
			$delall = false;
			$numnotes = count($_POST['notice_item_id']);
			for ($i = 0; $i < $numnotes; $i++) {
				if (empty($_POST['notice_body'][$i]) && empty($_POST['notice_list_id'][$i])) {
					$this->response->fill_array('notice_error', '');
					continue;
				}

				$transaction->reset();
				$transaction->add_data('action', 'add');

				if ($_POST['notice_item_id'][$i] != 0)
					$transaction->add_data('item_id', $_POST['notice_item_id'][$i]);

				$transaction->add_data('notice', $_POST['notice_body'][$i]);
				$transaction->add_data('value', $_POST['notice_value'][$i]);
				if (!empty($_POST['notice_list_id'][$i]))
					$transaction->add_data('list_id', intval($_POST['notice_list_id'][$i]));

				$reply = $transaction->send_admin_command('blocked_item');
				$transaction->handle_reply();

				if ($transaction->has_error()) {
					/* XXX what about param errors? */
					$this->response->fill_array('notice_error', lang($reply['error']));
					$delall = false;
				} else {
					$this->response->fill_array('notice_error', '');

					if (isset($reply['updated'])) {
						if ($i == 0)
							$delall = true;
						elseif (!$delall) {
							$transaction->reset();
							$transaction->add_data('action', 'delete');
							$transaction->add_data('sender_ip', $_GET['sender_ip']);
							$transaction->add_data('email', $_POST['notice_value'][$i]);
							$transaction->send_admin_command('spamfilter');
							$transaction->handle_reply();
							if ($transaction->has_error()) {
								/* XXX how to handle error? */
							}
						}
					}
				}
			}
			if ($delall) {
				$transaction->reset();
				$transaction->add_data('action', 'delete');
				$transaction->add_data('sender_ip', $_GET['sender_ip']);
				$transaction->add_data('mail_queue_id', 'all');
				$transaction->send_admin_command('spamfilter');
				$transaction->handle_reply();
				if ($transaction->has_error()) {
					/* XXX how to handle error? */
				} else
					$this->redirect('spamfilter');
			}
		} elseif (isset($_GET['action']) && ($_GET['action'] == 'delete' || $_GET['action'] == 'send')) {
			/* Send / delete mail(s) */
			$transaction->add_data('action', $_GET['action']);
			$transaction->add_data('sender_ip', $_GET['sender_ip']);
			if ($_GET['mail_queue_id'] == 'all')
				$transaction->add_data('mail_queue_id', 'all');
			else
				$transaction->add_data('mail_queue_id', intval($_GET['mail_queue_id']));

			$reply = $transaction->send_admin_command('spamfilter');
			$transaction->handle_reply();
			if ($transaction->has_error()) {
				/* XXX how to handle the error? */
			}
		}


		/* Get all emails from this sender ip */
		$transaction->reset();
		$transaction->add_data('action', 'get');
		$transaction->add_data('sender_ip', $_GET['sender_ip']);
		$reply = $transaction->send_admin_command('spamfilter');
		$transaction->handle_reply();

		if (!$transaction->has_error()) {
			if (isset($reply['mail'])) {
				ini_set("memory_limit", "100M");
				ini_set("max_execution_time", 120);

				foreach ($reply['mail'] as $mail) {
					foreach ($mail as $key => $value) {
						if ($key == 'added_at')
							$value = substr($value, 0,16);
						elseif ($key == 'from') { /* Fill the possible sender list with unique emails */
							if (!in_array($value, $possible_senders))
								$possible_senders[] = $value;
						}
						elseif ($key == 'body') {
							if (isset($mail['reason']))
							    $value = preg_replace("/({$mail['reason']})/i", "<span class='RedText'>\\1</span>", $value);
							$value = nl2br($value);
						}
						$this->response->fill_array($key, $value);
					}
				}
				$this->response->add_data('count', $reply['count']);
			} else {
				/* No emails found */
				$this->redirect('spamfilter');
			}
		}

		/* Ip look up */
		$this->response->add_data('sender_host', gethostbyaddr($_GET['sender_ip']));

		/* Get notices for all possible senders */
		foreach ($possible_senders as $item) {
			$transaction->reset();
			$transaction->add_data('value', $item);
			$transaction->add_data('action', 'get');
			$reply = $transaction->send_admin_command('blocked_item');
			$transaction->handle_reply();

			if (isset($reply['blocked_item'])) {
				foreach ($reply['blocked_item'] as $key => $value) {
					if ($key == 'created_at')
						$value = substr($value, 0, 10);
					$this->response->fill_array('notice_'.$key, $value);
				}
			} else {
				$this->response->fill_array('notice_list_id', 0);
				$this->response->fill_array('notice_item_id', 0);
				$this->response->fill_array('notice_value', $item);
				$this->response->fill_array('notice_notice', '');
				$this->response->fill_array('notice_admin', '');
				$this->response->fill_array('notice_created_at', '');
			}
		}

		$this->display_results('controlpanel/filter_spamfilter_control.html');
	}

	function main($function = NULL) {
		if ($function != NULL) {
			return $this->run_method($function);
		}

		$this->display_results('controlpanel/filter_main.html');
	}
}
?>
