<?php

namespace Yapo;

use bResponse;

class CpanelModuleAdminuf extends CpanelModule
{
    const BCONF_BASE = "*.*.common";

    public function __construct()
    {
        global $BCONF;
        /* Get configuration for easy access */
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Adminuf'));

        /* Get dynamic configuration */
        if ($this->dyn_bconf = get_dynamic_bconf()) {
            /* Update settings array */
            $settings = Bconf::get($this->dyn_bconf, "*.controlpanel.modules.adminuf.settings");
            if (isset($settings)) {
                if (is_string($settings)) {
                    $settings = array($settings);
                }
                foreach ($settings as $key => $value) {
                    $this->config['settings'][$key] = $value;
                }
            }
        } else {
            $this->dyn_bconf = false;
        }
        /* Init this controlpanel module */
        $this->init();
    }

    private function getDynUf()
    {
        $conf = Bconf::get($this->dyn_bconf, self::BCONF_BASE);
        $result['uf_conversion_factor'] = $conf["uf_conversion_factor"];
        $result['uf_conversion_updated'] = $conf["uf_conversion_updated"];
        return $result;
    }

    public function showForm()
    {
        $conversion_data = $this->getDynUf();
        $this->response->add_data('conversion_factor', $conversion_data['uf_conversion_factor']);
        $this->response->add_data('conversion_updated', $conversion_data['uf_conversion_updated']);
        $this->displayResults('controlpanel/admin_uf.html');
    }

    public function updateUf()
    {
        if (isset($_REQUEST["conversion_factor"])) {
            $conversion_factor = $_REQUEST["conversion_factor"];
            if (!preg_match("/^[0-9]{1,8},[0-9]{1,2}$/", $conversion_factor)) {
                $this->response->add_data('status', 'ERROR');
                $this->response->add_data('error', 'ERROR_UF_INVALID');
                $conversion_factor = "";
            }
        } else {
            $conversion_factor = "";
        }

        $new_conf["uf_conversion_factor"] = $conversion_factor;
        $new_conf["uf_conversion_updated"] = date("Y-m-d H:i:s");
        $old_conf = array("uf_conversion_factor", "uf_conversion_updated");

        if ($conversion_factor != "") {
            if (save_dynamic_bconf($new_conf, self::BCONF_BASE, $old_conf)) {
                $this->response->redirect("/controlpanel");
            } else {
                $this->response->add_data('status', 'ERROR');
                $this->response->add_error('error', 'ERROR_UPDATING_UF');
            }
        } else {
            $conversion_data = $this->getDynUf();
            $this->response->add_data('conversion_factor', $conversion_data['uf_conversion_factor']);
            $this->response->add_data('conversion_updated', $conversion_data['uf_conversion_updated']);
            $this->displayResults('controlpanel/admin_uf.html');
        }
    }

    public function main($function = null)
    {
        if ($function != null) {
            $this->runMethod($function);
        } else {
            $this->showForm();
        }
    }
}
