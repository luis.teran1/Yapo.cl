<?php

namespace Yapo;

require_once('bResponse.php');
require_once('JSON.php');

use Yapo\Cpanel;

abstract class CpanelModule
{
    /**
     * Name of this module
     */
    private $name;
    /**
     * Array with this module's menu items
     */
    private $menu;
    /**
     * Retrieved results sets for display
     */
    protected $result_set;
    /**
     * Pointer to this module's config
     */
    protected $config;
    /**
     * Pointer to the global response object
     */
    protected $response;

    /**
     * Main function for the module. Implementations are expected to setup
     * any css/js/template they may need, and finish with a call to runMethod.
     */
    abstract public function main();

    /**
     * Basic initialization code for every module
     */
    protected function init()
    {
        // Generate menu items
        foreach ($this->config['menu'] as $id => $item) {
            if (isset($item['order'])) {
                $menu_order[$item['order']] = $id;
            }
        }
        ksort($menu_order);
        foreach ($menu_order as $id) {
            $this->menu[$id] = $this->config['menu'][$id]['name'];
        }

        // Global response object
        $this->response = &$GLOBALS['response'];

        // Module id
        $this->name = str_replace('Yapo\CpanelModule', '', get_class($this));
    }

    /**
     * Retrieve the name for this module.
     * @return: This module's name, according to the configuration
     */
    public function getName()
    {
        return $this->config['name'];
    }

    /**
     * Retrieve the complete menu array
     * @return: An array with menu entries
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Retrieve the name for a menu_id.
     * @param menu_id: Id to retrieve the name for
     * @return: The menu's name, should it exist. Null, otherwise.
     */
    public function getMenuName($menu_id)
    {
        return isset($this->config['menu'][$menu_id])?$this->config['menu'][$menu_id]['name']:null;
    }

    /**
     * Provides a replacement list to apply to the menu name suffixes.
     * @return: key => value replacement array to apply to the menu name suffixes
     */
    protected function getSuffixReplaceArray()
    {
        return null;
    }
    
    /**
     * Should a *name_suffix* bconf exist for a given menu_id, return it
     * after applying the pertinent replacements (based on getSuffixReplaceArray).
     * @param menu_id: Id of the menu item to request suffix for
     * return: Empty string or the corresponding suffix.
     */
    public function getMenuNameSuffix($menu_id)
    {
        $replace = CpanelModule::getSuffixReplaceArray($menu_id);
        if (!isset($this->config['menu'][$menu_id]['name_suffix'])) {
            return '';
        }
        
        $name = $this->config['menu'][$menu_id]['name_suffix'];

        if (is_array($replace)) {
            $name = strtr($name, $replace);
        }

        return $name;
    }

    /**
     * Adds a result set to the future output.
     * @param response: Data to display
     * @param template: How to format the data
     */
    protected function addResults($response, $template)
    {
        $this->result_set[] = array($response, $template);
    }

    /**
     * Sets the output template
     */
    public function displayResults($template = null)
    {
        if (!empty($template)) {
            $this->response->add_data('template', $template);
        }
    }

    /**
     * Render every result set using their corresponding templates
     */
    public function displayResultSet()
    {
        if (is_array($this->result_set)) {
            foreach ($this->result_set as $result) {
                $result[0]->call_template($result[1]);
            }
        }
    }

    /**
     * Given an action and its argument, redirect the user to the
     * corresponding *url*
     * @param action: A control panel module action
     * @param args: Array of arguments for the action
     */
    public function redirect($action, $args = null)
    {
        $uri = $_SERVER['SCRIPT_NAME'] . '?m=' . $_GET['m'] . '&a=' . $action;
        if (is_array($args)) {
            foreach ($args as $key => $value) {
                $uri .= '&'.urlencode($key).'='.urlencode($value);
            }
        }
        header("Location: $uri");
        exit(0);
    }

    /**
     * Given a function name, it runs the corresponding method on the caller class,
     * after checking admin privileges.
     * @param function: String name of the function to call
     * @return: The result of the invoked function, if found.
     */
    protected function runMethod($function)
    {
        if (in_array($function, get_class_methods($this))) {
            /* Check privs */
            if (isset($this->config['subprivs'])) {
                foreach ($this->config['subprivs'] as $action => $data) {
                    if (Cpanel::hasPriv($this->name . '.' . $action)) {
                        if (in_array($function, explode(',', @$data['php']['functions']))) {
                            return $this->$function();
                        }
                    }
                }
            }
            if (in_array($function, explode(',', @$this->config['php']['functions']))) {
                return $this->$function();
            }
        }
    }
}
