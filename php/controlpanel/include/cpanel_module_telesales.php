<?php
require_once('cpanel_module.php');
require_once('util.php');
require_once('bTransaction.php');
require_once('bAd.php');
require_once('RedisMultiproductManager.php');
require_once('payment_common.php');

use Yapo\Cpanel;
use Yapo\Bconf;
use Yapo\Logger;

class cpanel_module_telesales extends cpanel_module
{

    public function cpanel_module_telesales()
    {
        global $BCONF;
        /* Get configuration for easy access */
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.telesales'));

        /* Init this controlpanel module */
        $this->init();
    }

    public function links()
    {
        $this->display_results('controlpanel/telesales/links.html');
    }

    private function verifyEmail()
    {
        if (isset($_REQUEST['search_email'])) {
            $email_a = $_REQUEST['search_email'];
            $this->response->add_data("search_email", $email_a);
            $regex = Bconf::get($BCONF, "*.common.accounts.mobile.form_validation.email.regexp");
            if (empty($regex)) {
                $regex = "^[\w_+-]+(\.[\w_+-]+)*\.?@([\w_+-]+\.)+[\w]{2,4}$";
            }
            if (!preg_match("/{$regex}/", $email_a)) {
                $this->response->add_data("error_email", "Email no v�lido");
                return false;
            }

            $this->response->add_data("valid_email", "true");
            setcookie('telesales_last_search', $email_a, time() + 60*60*24);
            return true;
        }
    }

    public function edit_store()
    {
        $params_store = $this->config['data']['store_params'];
        if ($this->verifyEmail()) {
            $email = $_REQUEST['search_email'];
            $load_store = true;
            if (isset($_REQUEST['send'])) {
                $store_status = $_REQUEST["store_status"];
                foreach ($params_store as $key) {
                    $storeKey = 'store_'.$key;
                    global $$storeKey;
                    $$storeKey = $_REQUEST[$storeKey];
                }

                $trans = new bTransaction();
                $store_name = Cpanel::filterStoreChars($store_name);
                $store_url = Cpanel::slugify($store_name);
                $store_name = html_entity_decode($store_name);
                $store_hide_phone = $store_hide_phone == 't'? 1:0;
                $store_region_original = $_REQUEST['store_region_original'];

                $trans->add_data("url", $store_url);

                foreach ($params_store as $key) {
                    $storeKey = 'store_'.$key;
                    if (!empty($$storeKey) && $key != 'id') {
                        $trans->add_data($key, $$storeKey);
                    }
                }
                $trans->add_data('store_id', $store_id);
                if ($store_region == $store_region_original) {
                    if (!empty($_REQUEST['store_commune'])) {
                        $trans->add_data('commune', $_REQUEST['store_commune']);
                    }
                }

                if (Cpanel::checkLogin() &&  Cpanel::hasPriv('telesales.sell_products')) {
                    $reply = $trans->send_admin_command("edit_store");

                    if ($trans->has_error()) {
                        $load_store = false;
                        $errors = $trans->get_errors("store_");
                        if (array_key_exists('store_url', $errors) && !array_key_exists('store_name', $errors)) {
                            $errors['store_name'] = $errors['store_url'];
                        }
                        $messages = $trans->get_messages("store_");
                        foreach ($errors as $key => $value) {
                            if (isset($messages[$key])) {
                                $value = lang($value).' "'.$messages[$key].'"';
                            }
                            $this->response->add_data('err_'.$key, $value);
                        }
                        $this->response->add_data('edit_store_status', $store_status);
                        $this->response->add_data('edit_store_url', $store_url);

                        foreach ($params_store as $key) {
                            $storeKey = 'store_'.$key;
                            if ($key != 'hide_phone') {
                                $this->response->add_data('edit_'.$storeKey, $$storeKey);
                            } else {
                                $this->response->add_data('edit_'.$storeKey, $$storeKey == 1?'t':'f');
                            }
                        }
                        $this->response->add_data('edit_store_commune', $_REQUEST['store_commune']);
                    } else {
                        $this->response->add_data('edit_store_success', 1);
                    }
                }
            }

            if ($load_store) {
                // Try to find the account
                $trans = new bTransaction();
                $trans->add_data('email', $email);
                $info = $trans->send_command('load_store');
                if ($info['status'] == "TRANS_OK") {
                    $this->response->add_data('edit_store_status', $info['store_status']);
                    $this->response->add_data('edit_store_url', $info['url']);
                    foreach ($params_store as $key) {
                        $storeKey = 'store_'.$key;
                        if (isset($info[$key])) {
                            $this->response->add_data('edit_'.$storeKey, $info[$key]);
                        }
                    }

                    $this->response->add_data('edit_store_commune', $info['commune']);
                    $this->response->add_data('edit_store_id', $info['store_id']);
                } else {
                    syslog(LOG_ERR, log_string()."ERROR: Cannot fetch the store info");
                }
            }
            $this->response->add_data('content', 'controlpanel/telesales/edit_store.html');
            $this->response->call_template('controlpanel/simple.html');
            exit(0);
        }
    }

    public function sell_products()
    {
        if ($this->verifyEmail()) {
            $email = $_REQUEST['search_email'];
            // Try to find the account
            $trans = new bTransaction();
            $trans->add_data('email', $email);
            $info = $trans->send_command('get_account');
            // If account does not exist
            if ($info['status'] == "TRANS_OK") {
                $this->response->add_data("account_found", "true");
                // If only showing the data
                $account_params = explode(",", Bconf::get($BCONF, "*.accounts.session_data"));
                foreach ($account_params as $key_param) {
                    if ($key_param != "ttl" && isset($info[$key_param])) {
                        if ($key_param != 'commune') {
                            $this->response->add_data($key_param, $info[$key_param]);
                        } else {
                            $this->response->add_data('communes', $info[$key_param]);
                        }
                    }
                }
                $document=1;
                if ($info["is_company"]=="t") {
                    $document=2;
                }
                $this->response->add_data("document", $document);

                if ($info["account_status"] == "inactive") {
                    $this->response->add_data('store_message', lang("CONTROLPANEL_TELESALES_STORE_ACC_INACTIVE"));
                } elseif ($info["account_status"] == "pending_confirmation") {
                    $this->response->add_data('store_message', lang("CONTROLPANEL_TELESALES_STORE_ACC_PENDING"));
                } else {
                    if (isset($info["store_status"]) && $info["store_status"] == "admin_deactivated") {
                        $this->response->add_data('store_message', lang("CONTROLPANEL_TELESALES_STORE_DEACTIVATED"));
                    } else {
                        if (empty($info["store_id"])) {
                            $this->response->add_data('store_message', lang("CONTROLPANEL_TELESALES_STORE_NOT_FOUND"));
                        } else {
                            $trans_store = new bTransaction();
                            $trans_store->add_data('store_id', $info['store_id']);
                            $reply_store = $trans_store->send_command('load_store');

                            if ($trans_store->has_error(true)) {
                                $this->response->add_data(
                                    'store_message',
                                    lang("CONTROLPANEL_TELESALES_STORE_LOADING_ERROR")
                                );
                            } else {
                                $this->response->add_data('store_show_info', '1');
                                $this->response->add_data('store_info_status', $reply_store["store_status"]);
                                $this->response->add_data('store_info_name', $reply_store["name"]);
                                $this->response->add_data('store_info_date_end', $reply_store["date_end"]);
                                $this->response->add_data('store_info_type', $reply_store["store_type"]);
                            }
                        }
                    }
                }
            } else {
                syslog(LOG_WARNING, log_string()."failed to get account info for: $email");
                $this->response->add_data('store_message', lang("CONTROLPANEL_TELESALES_STORE_ACC_NOT_FOUND"));
            }

            if (isset($_REQUEST['document'])) {
                if ($this->paymentValidate()) {
                    $this->response->add_data("form_validated", 1);
                }
            }
        }

        $redis_multiproduct_manager = new RedisMultiproductManager();
        //Search for pending products
        if (!empty($email)) {
            $products_pending = $redis_multiproduct_manager->getAdsWithPendingProducts($email);
            if (!empty($products_pending)) {
                foreach ($products_pending as $prod => $values) {
                    foreach ($values as $list_id) {
                        $this->response->fill_array("pending_prod_".$prod, $list_id);
                    }
                }
            }
        }
        $this->display_results('controlpanel/telesales/search.html');
    }

    public function edit_account()
    {
        global $BCONF;
        $editable_params = $this->config['data']['acc_editable_params'];
        $account_params = explode(",", Bconf::get($BCONF, "*.accounts.session_data"));
        if ($this->verifyEmail()) {
            $email = $_REQUEST['search_email'];
            // Try to find the account
            $trans = new bTransaction();
            $trans->add_data('email', $email);
            $info = $trans->send_command('get_account');
            // If account does not exist
            if ($info['status'] != "TRANS_OK") {
                syslog(LOG_WARNING, log_string()."failed to get account info for: $email");
                $this->response->add_data("err_email", "Email sin cuenta");
            } else {
                //If account does exist
                $this->response->add_data("account_found", "true");

                //If is updating the info
                if (isset($_REQUEST['accept_conditions']) && $_REQUEST['accept_conditions'] == "1") {
                    syslog(LOG_WARNING, log_string()."Updating account info for: $email");
                    $trans = new bTransaction();
                    foreach ($editable_params as $key) {
                        if (isset($_REQUEST[$key])) {
                            $value = $_REQUEST[$key];
                            switch ($key) {
                                case 'rut':
                                    $value = $this->filterRut($value);
                                    $trans->add_data($key, $value);
                                    $this->response->add_data($key, $value);
                                    break;
                                case 'search_email':
                                    $trans->add_data("email", $value);
                                    $this->response->add_data($key, $value);
                                    break;
                                case 'is_company':
                                    $trans->add_data($key, $value);
                                    $this->response->add_data($key, ($value=="1"?"t":"f"));
                                    break;
                                case 'business_name':
                                    if (!empty($value)) {
                                        $trans->add_data($key, $value);
                                        $this->response->add_data($key, $value);
                                    }
                                    break;
                                default:
                                    $trans->add_data($key, $value);
                                    $this->response->add_data($key, $value);
                            }
                        }
                    }

                    if (!empty($info['is_pro_for'])) {
                        $trans->add_data("is_company", "1");
                        $this->response->add_data("is_company", "t");
                    }

                    $trans->add_data("source", "controlpanel");
                    $reply = $trans->send_admin_command('edit_account');
                    if ($trans->has_error(true) || $reply['status'] != "TRANS_OK") {
                        syslog(LOG_ERR, log_string()."ERROR: Data could not be updated for this account");
                        foreach ($trans->get_errors() as $param => $code) {
                            $this->response->add_data("err_$param", lang($code));
                        }
                    } else {
                            $this->response->add_data("edit_success", "La informaci�n ha sido actualizada!");
                    }
                    // Populate data not updated
                    foreach ($account_params as $key_param) {
                        if ($key_param != "ttl"
                            && isset($info[$key_param])
                            && !in_array($key_param, $editable_params)
                        ) {
                            $this->response->add_data($key_param, $info[$key_param]);
                        }
                    }
                } else {
                    // If only showing the data
                    foreach ($account_params as $key_param) {
                        if ($key_param != "ttl" && isset($info[$key_param])) {
                            $this->response->add_data($key_param, $info[$key_param]);
                        }
                    }
                }
            }
        }
        $this->display_results('controlpanel/telesales/search.html');
    }

    private function addExtraData(&$response, $payment_group_id)
    {
        $response->add_data("order_id", $payment_group_id);
        $transaction = new bTransaction();
        $transaction->add_data('payment_group_id', $payment_group_id);
        $reply = $transaction->send_command('purchase_get');
        if ($transaction->has_error(true)) {
            $this->redirectToErrorPage();
        }
        if (!isset($reply['purchase_get'])) {
            $this->redirectToErrorPage();
        } else {
            foreach ($reply['purchase_get'] as $row) {
                if (isset($row['ref_type'])) {
                    if ($row['ref_type'] == 'cc_number') {
                        $row['reference'] = "**** **** **** ".$row['reference'];
                    }
                    $response->add_data($row['ref_type'], $row['reference']);
                }
            }
        }
        //Put here info needed
        $resultData = array(
            "status" =>$reply['purchase_get'][0]['status'],
            "email"=>$reply['purchase_get'][0]['email']
        );

        return $resultData;
    }

    public function telesales_payment_verify()
    {
        if (!isset($_POST['TBK_ORDEN_COMPRA']) || !isset($_POST['TBK_ID_SESION'])) {
            $this->response->add_data("msg_error", 'Ha ocurrido un error: no existe orden de compra');
            syslog(LOG_INFO, log_string() . 'No existe TBK_ORDEN_COMPRA');
            $this->display_results('controlpanel/telesales/payment_info.html');
            return;
        }
        $payment_group_id = $_POST['TBK_ORDEN_COMPRA'];
        $id_session = $_POST['TBK_ID_SESION'];

        $sha_orden = sha1($payment_group_id);
        if ($sha_orden != $id_session || count($_POST) != 2) {
            $this->response->add_data(
                "msg_error",
                'Ha ocurrido un error: ".
                "Parametros no corresponden. Orden de compra recibida desde Webpay: '.$payment_group_id
            );
            syslog(LOG_INFO, log_string() . 'No existe TBK_ORDEN_COMPRA');
            $this->display_results('controlpanel/telesales/payment_info.html');
            return;
        }

        $returnData = $this->addExtraData($this->response, $payment_group_id);
        $this->response->add_data("order_id", $payment_group_id);
        $this->response->add_data("external_user_email", $returnData['email']);

        $transaction = new bTransaction();
        $transaction->add_data('payment_group_id', $payment_group_id);
        $reply = $transaction->send_command('get_items_by_payment');

        if ($transaction->has_error(true)) {
            $this->response->add_data("msg_error", 'Ha ocurrido un error.');
            syslog(LOG_INFO, log_string() . 'Trans Error: '.var_export($transaction->get_errors(), true));
            $this->display_results('controlpanel/telesales/payment_info.html');
            return;
        }

        $returnData = (object) array_merge((array)$returnData, (array)$reply['get_items_by_payment'][0]);
        syslog(
            LOG_INFO,
            log_string().
            'payment data from api and trans: '.
            preg_replace('/\*{4} [0-9]{4}/', '**** ----', var_export($returnData, true))
        );
        $products_pending = array();
        foreach ($reply['get_items_by_payment'] as $detail_payment) {
            $product = "";
            $list_id = "";
            foreach ($detail_payment as $key_column => $value_column) {
                if (in_array($key_column, array("subject","price","product_id","list_id"))) {
                    $this->response->fill_array("list_detail_".$key_column, $value_column);
                }
                if ($key_column == "list_id") {
                    $list_id = $value_column;
                }
                if ($key_column == "product_id") {
                    $product = $value_column;
                }
            }
            if ($product != "" && $list_id != "") {
                @$products_pending[$product][] = $list_id;
            }
        }

        // copy the vars into the response object
        $this->setBConfFieldsInResponse($this->response, $returnData, '*.payment.completion_page.billing');
        $this->setBConfFieldsInResponse($this->response, $returnData, '*.payment.completion_page.vars');

        $authorized = false;
        if (isset($returnData->status) && $returnData->status == "paid") {
            $authorized = true;
        } else {
            bLogger::logError(__METHOD__, "Telesales Unauthorized payment" . print_r($returnData, true));
        }

        if ($authorized) {
            bLogger::logError(__METHOD__, "Telesales authorized payment" . print_r($returnData, true));
            $this->response->add_data('page_title', lang('TELESALES_PAYMENT_CONFIRM_TITLE'));
            $this->response->add_data('page_message', lang('PAYMENT_CONFIRM_TITLE'));

            $email_of_cookie = $_COOKIE['telesales_last_search'];
            $this->response->add_data('user_email', $email_of_cookie);
            $email_of_cookie = str_replace("@", "_", $email_of_cookie);
            $email_of_cookie = str_replace(".", "_", $email_of_cookie);

            if ($_COOKIE['telesales_order_id_'.$email_of_cookie] == $_POST['TBK_ORDEN_COMPRA']) {
                setcookie('data_telesales_'.$email_of_cookie, '', time() - 1232123);
                setcookie('telesales_order_id_'.$email_of_cookie, '', time() - 1981723789);
                $cart_mngr = new RedisMultiproductManager();
                $cart_mngr->addAllPendingProducts($_COOKIE['telesales_last_search'], $products_pending);
                // put the store info on redis session
                $trans = new bTransaction();
                $trans->add_data('email', $_COOKIE['telesales_last_search']);
                $info = $trans->send_command('load_store');
                if ($info['status'] == "TRANS_OK") {
                    $prefix = Bconf::get($BCONF, "*.accounts.session_redis_prefix");
                    $email_hash = sha1($_COOKIE['telesales_last_search']);
                } else {
                    syslog(LOG_ERR, log_string()."ERROR: Cannot load store info");
                }
            } else {
                $cp_url = Bconf::get($BCONF, "common.base_url.controlpanel");
                $error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
                $error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
                $error_params = "error_title=".$error_title;
                $error_params .= "&error_message=".$error_message;
                $email_param = "&search_email=".urlencode($result['email']);
                $cp_error_url = $cp_url."/controlpanel?m=telesales&a=sell_products".$email_param."&".$error_params;
                header('Location: '.$cp_error_url);
                exit();
            }
        } else {
            $this->response->add_data('page_title', lang('PAGE_SUCCESS_BUMP_FAILED_TITLE'));
        }

        $this->display_results('controlpanel/telesales/payment_info.html');
    }


    private function setBConfFieldsInResponse(&$response, &$originData, $bconfKey)
    {

        global $BCONF;
        $fields = Bconf::get($BCONF, $bconfKey);
        foreach ($fields as $field) {
            if (isset($field['var'])) {
                if (isset($originData->$field['var'])) {
                    $response->add_data(
                        $field['var'],
                        $this->getBConfFieldMappedValue($field, $originData->$field['var'])
                    );
                } elseif (isset($field['default'])) {
                    if ($field['default'] == "NOW") {
                        $response->add_data($field['var'], date("d/m/Y H:i:s"));
                    } else {
                        $response->add_data($field['var'], $field['default']);
                    }
                }
            }
        }
    }

    private function getBConfFieldMappedValue($bconf, $value)
    {
        if (isset($bconf["map"])) {
            $value = $bconf["map"][$value];
        }
        return $value;
    }

    private function filterRut($val)
    {
        $val = preg_replace('/[^[a-zA-Z0-9]��������&,:; -]/i', '', $val);
        $val = str_replace('.', '', $val);
        return strtoupper($val);
    }

    private function paymentValidate()
    {
        global $BCONF;

        $data = array();

        foreach ($this->config['data']['doc_params'] as $param) {
            if (isset($_REQUEST[$param])) {
                if ($param != 'rut') {
                    $data[$param] = $_REQUEST[$param];
                } else {
                    $data[$param] = $this->filterRut($_REQUEST[$param]);
                }
                $this->response->add_data($param, $data[$param]);
            }
        }

        if ($data['document'] == null) {
            $this->page_title = "ERROR_TRANSACTION_FAILED_TITLE";
            $this->error_title = "ERROR_TRANSACTION_CANT_BE_DONE";
            $this->error_message = "ERROR_DOCUMENT_DOESNT_MATCH";
            return 'error';
        }

        $document_data = Bconf::get($BCONF, "*.payment.document.{$data['document']}.data");
        $doc_type = Bconf::get($BCONF, "*.payment.document.{$data['document']}.doc_type");

        if ($doc_type == "") {
            return 'false';
        }

        $transaction = new bTransaction();
        foreach ($document_data as $key => $val) {
            $transaction->add_data($val['name'], $data[$val['name']]);
        }

        $transaction->add_data('doc_type', $doc_type);
        $transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);

        if (!empty($_POST['mpad']) && !empty($_POST['mpid'])) {
            $products_price = array();
            $products_name  = array();
            $products_params  = array();
            $listIds = array_filter($_POST['mpad']);
            if (count($listIds) > 0) {
                $list_ids_to_string = implode(",", $listIds);
                $result = bsearch_search_vtree(Bconf::get($BCONF, "*.common.asearch"), "id:".$list_ids_to_string);
                if (!is_array($result['list_id'])) {
                    $result['list_id'] = array($result['list_id']);
                }
                if (!is_array($result['category'])) {
                    $result['category'] = array($result['category']);
                }
            }

            $this->product_list = array();
            foreach ($_POST['mpid'] as $key => $id_prod) {
                $list_id = $_POST['mpad'][$key];
                $name = Bconf::get($BCONF, "*.payment.product.$id_prod.codename");
                $bconf_is_store = Bconf::get($BCONF, "*.payment.product.$id_prod.is_store");
                if (((int)$bconf_is_store) == 1) {
                    $is_store = true;
                    $price = Bconf::get($BCONF, "*.payment.product.$id_prod.price");
                } elseif (substr($name, 0, 5) === "PCAR_" || substr($name, 0, 5) === "PRES_") {
                    $price = Bconf::get($BCONF, "*.payment.product.$id_prod.price");
                } else {
                    $position = array_search($list_id, $result['list_id']);
                    $category = $result['category'][$position];
                    $price = Bconf::getProductPrice($BCONF, $id_prod, $category);
                }
                if (empty($price)) {
                    bLogger::logError(__METHOD__, "mpad or mpid is empty");
                    return 'error';
                } else {
                    array_push($products_price, $price);
                    array_push($products_params, "0");
                    array_push($products_name, $name);
                    array_push($this->product_list, array($id_prod, $price));
                }
            }

            $mp_list_id = implode("|", $_POST['mpad']);
            $mp_prod_id = implode("|", $_POST['mpid']);
            $mp_prices  = implode("|", $products_price);
            $mp_params  = implode("|", $products_params);
            $mp_names  = implode("|", $products_name);
            $this->product_list =  $_POST['mpid'];

            if (isset($_POST['account_id']) && !empty($_POST['account_id'])) {
                $transaction->add_data('account_id', $_POST['account_id']);
            }
            $transaction->add_data('product_id', $mp_prod_id);

            if (Bconf::get($BCONF, "*.payment.telesales.webpay_plus.enabled")) {
                $transaction->add_data('payment_method', 'webpay_plus');
            } else {
                $transaction->add_data('payment_method', 'webpay');
            }
            $transaction->add_data('payment_platform', 'desktop');
            $transaction->add_data('list_id', $mp_list_id);
            $transaction->add_data('product_price', $mp_prices);
            $transaction->add_data('product_params', $mp_params);
            $transaction->add_data('product_name', $mp_names);
        } else {
            //TODO this is an error case
        }

        //This only execute validation using the same transaction
        $reply = $transaction->validate_command('purchase_insert');

        if ($transaction->has_error()) {
            $trans_errors = $transaction->get_errors();

            foreach ($trans_errors as $key => $value) {
                $this->response->add_data("err_".$key, $value);
            }
            return false;
        }

        return true;
    }
    public function main($function = null)
    {
        if (isset($function)) {
            $this->response->fill_array('page_js', '/js/telesales.js');
            $this->run_method($function);
        } else {
            $this->links();
        }
    }
}
