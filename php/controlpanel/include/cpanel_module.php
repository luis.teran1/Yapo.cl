<?php
require_once('bResponse.php');
require_once('JSON.php');

use Yapo\Cpanel;

class cpanel_module {
	var $name;
	var $config;
	var $menu;
	var $response;
	var $result_set;

	function init() {
		// Generate menu items
		foreach($this->config['menu'] as $id => $item)
			if (isset($item['order']))
				$menu_order[$item['order']] = $id;
		ksort($menu_order);
		foreach($menu_order as $id)
			$this->menu[$id] = $this->config['menu'][$id]['name'];

		// Global response object
		$this->response =& $GLOBALS['response'];

		// Module id
		$this->name = str_replace('cpanel_module_', '', get_class($this));
	}

	function get_name() {
		return $this->config['name'];
	}

	function get_menu() {
		return $this->menu;
	}

	function get_menu_name($menu_id) {
		return isset($this->config['menu'][$menu_id])?$this->config['menu'][$menu_id]['name']:NULL;
	}

	function get_suffix_replace_array() {
		return NULL;
	}
	
	function get_menu_name_suffix($menu_id) {
		$replace = $this->get_suffix_replace_array($menu_id);
		if (!isset($this->config['menu'][$menu_id]['name_suffix'])) {
			return '';
		}
		
		$name = $this->config['menu'][$menu_id]['name_suffix'];

		if (is_array($replace))
			$name = strtr($name, $replace);

		return $name;
	}

	function add_results($response, $template) {
		$this->result_set[] = array($response, $template);
	}

	function display_results($template = NULL) {
		if (!empty($template))
			$this->response->add_data('template', $template);
	}

	function display_result_set() {
		if (is_array($this->result_set)) {
			foreach ($this->result_set as $result) 
				$result[0]->call_template($result[1]);
		}
	}

	function redirect($action, $args = NULL) {
		$uri = $_SERVER['SCRIPT_NAME'] . '?m=' . $_GET['m'] . '&a=' . $action;
		if (is_array($args))
			foreach ($args as $key => $value)
				$uri .= '&'.urlencode($key).'='.urlencode($value);
		header("Location: $uri");
		exit(0);
	}

	function run_method($function) {
		if (in_array($function, get_class_methods($this))) {
			/* Check privs */
			if (isset($this->config['subprivs'])) {
				foreach ($this->config['subprivs'] as $action => $data) {
					if (Cpanel::hasPriv($this->name . '.' . $action)) {
						if (in_array($function, explode(',', @$data['php']['functions']))) {
							return $this->$function();      
						}
					}
				}
			}
			if (in_array($function, explode(',', @$this->config['php']['functions']))) {
				return $this->$function();      
			}
		}
	}

	function main() {
	}

	/* Compatibility */

	function getName() { return $this->get_name(); }
	function getMenu() { return $this->get_menu(); }
	function getMenuName($menu_id) { return $this->get_menu_name($menu_id); }
	function getSuffixReplaceArray() { return $this->get_suffix_replace_array(); }
	function getMenuNameSuffix($menu_id) { return $this->get_menu_name_suffix($menu_id); }
	function addResults($response, $template) { return $this->add_results($response, $template); }
	function displayResults($template = NULL) { return $this->display_results($template); }
	function displayResultSet() { return $this->display_result_set(); }
	function runMethod($function) { return $this->run_method($function); }
}

?>
