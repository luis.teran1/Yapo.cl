<?php
require_once('cpanel_module.php');
require_once('util.php');
require_once('bTransaction.php');
require_once('bAd.php');

class cpanel_module_landing_page extends cpanel_module {
	function cpanel_module_landing_page() {
		/* Get configuration for easy access */
		$this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.landing_page'));

		/* Get dynamic configuration */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$root = '*.controlpanel.modules.landing_page.settings';
			$settings = bconf_get($this->dyn_bconf, $root);
			if (isset($settings)) {
				if (is_string($settings))
					$settings = array($settings);
				foreach ($settings as $key => $value) {
					$this->config['settings'][$key] = $value;
				}
			}
		} else
			$this->dyn_bconf = false;

		// Init this controlpanel module
		$this->init();
	}

	function delete_ad_group () {
		if ( isset($_REQUEST['ad_group']) ) {
			if ($this->dyn_bconf = get_dynamic_bconf()) {
				$bconf_key_variations = '*.*.common.ad_group.code.' . $_REQUEST['ad_group']
					. '.ip_digit';
				$ad_group_variations = bconf_get($this->dyn_bconf, $bconf_key_variations);

				if(isset($ad_group_variations)){//added to avoid error when group has no variation
                    			foreach ($ad_group_variations as $k => $v) {
						$_REQUEST['ip_filter'] = $k;
						$this->delete_ad_group_variation();
					}
				}

				unset($_REQUEST['variation']);
			} else {
				$this->response->add_error('err_inserted_id', 'ERROR_AD_GROUP_DELETE_FAILED');
			}

			$bconf_key = "*.*.common.ad_group.code";

			/* Update conf table */
			if (!save_dynamic_bconf(NULL, $bconf_key, array( '0' => $_REQUEST['ad_group'] ))) {
				/* Could not save */
				$this->response->add_error('err_error', 'ERROR_AD_GROUP_DELETE_FAILED');
				$this->response->add_error('err_ad_group', $_REQUEST['ad_group']);
			} else {
				$this->response->add_data('ad_group_code_saved', 'AD_GROUP_DELETED');
			}
		} else {
			$this->response->add_error('err_inserted_id', 'ERROR_AD_GROUP_DELETE_FAILED');	
		}
		$this->show_form();
	}

	function delete_ad_group_variation () {
		if (isset($_REQUEST['ad_group']) //&& isset($_REQUEST['variation'])
		&& isset($_REQUEST['ip_filter'])) {
			$bconf_key = '*.*.common.ad_group.code.' . $_REQUEST['ad_group']
				. '.ip_digit';

			if (!save_dynamic_bconf(NULL, $bconf_key, array( '0' => $_REQUEST['ip_filter'] ))) {
				$this->response->add_error('err_error', 'ERROR_AD_GROUP_VARIATION_DELETE_FAILED');
				$this->response->add_error('err_ad_group', $_REQUEST['ad_group']);
			} else {
				$this->response->add_data('ad_group_code_saved', 'AD_GROUP_DELETED');
			}
		} else {
			$this->response->add_error('err_inserted_id', 'ERROR_AD_GROUP_DELETE_FAILED');	
		}

		$this->show_form();
	}

	/*function edit_ad_group() {
		if(isset($_REQUEST['ad_group_code']) && isset($_REQUEST['page'])) {
			$bconf_key = "*.*.common.ad_group.code";

			if (!save_dynamic_bconf( array( $_REQUEST['ad_group_code'] => $_REQUEST['page'] ), "*.*.common.ad_group.code", -1)) {
				// Could not save
				$this->response->add_error('err_error', 'ERROR_AD_GROUP_CODE_SAVE_FAILED');
				$this->response->add_error('err_ad_group', $_REQUEST['ad_group']);
			} else {
				$this->response->add_data('ad_group_code_saved', 'AD_GROUP_SAVE_INFO');
			}
		} else {
			$this->response->add_error('err_error', 'ERROR_AD_GROUP_DELETE_FAILED');
		}
		$this->show_form();
	}*/

	function save_ad_group() {
		if(isset($_REQUEST['ad_group']) && isset($_REQUEST['page']) 
			&& !empty($_REQUEST['ad_group']) && !empty ($_REQUEST['page'])) {
			if (!save_dynamic_bconf( array( 'page' => $_REQUEST['page'] ),
			'*.*.common.ad_group.code.' . $_REQUEST['ad_group'], -1)) {
				/* Could not save */
				$this->response->add_error('err_error', 'ERROR_AD_GROUP_CODE_SAVE_FAILED');
			}
		} else {
			$this->response->add_error('err_error_save', 'ERROR_EMPTY_AD_GROUP_OR_PAGE');
		}

		if (!$this->response->has_error()) {
			$this->response->add_data('ad_group_code_saved', 'AD_GROUP_SAVE_INFO');
		}
		$this->show_form();
	}

	function save_ad_group_variation() {
		if(isset($_REQUEST['variation']) && isset($_REQUEST['ip_filter'])
			&& isset($_REQUEST['ad_group'])
			&& !empty($_REQUEST['variation']) && ($_REQUEST['ip_filter'] != '')
			&& !empty($_REQUEST['ad_group'])
		) {
			foreach ($_REQUEST['ip_filter'] as $ipf_key => $ipf_value) {
				if (!save_dynamic_bconf( array('variation' => $_REQUEST['variation']),
					'*.*.common.ad_group.code.' . $_REQUEST['ad_group']
					. '.ip_digit.' . $ipf_value, -1)
				) {
					$this->response->add_error('err_error', 'ERROR_AD_GROUP_VARIATION_SAVE_FAILED');
				}
			}
		} else {
                        $this->response->add_error('err_error_save', 'ERROR_EMPTY_AD_GROUP_OR_PAGE');
                }

                if ( !$this->response->has_error() ) {
                        $this->response->add_data('ad_group_code_saved', 'AD_GROUP_SAVE_INFO');
                }

                $this->show_form();
	}

	function show_form() {
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$ad_group_codes = bconf_get($this->dyn_bconf, '*.*.common.ad_group');
			$this->response->add_extended_array($ad_group_codes);
		} else {
			$this->dyn_bconf = false;
		}

		$this->display_results('controlpanel/landing_page.html');
	}

	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->show_form();
	}
}
?>
