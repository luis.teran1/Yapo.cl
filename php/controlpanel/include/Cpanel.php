<?php

namespace Yapo;

use RedisSessionClient;
require_once('memcachedsession.php');

class Cpanel
{

    /*
     * User login
     */
    public static function checkLogin()
    {
        $client = memcached_client();
        if (isset($_SESSION['controlpanel']) &&
            !empty($_SESSION['controlpanel']) &&
            strlen($client->get(session_id() . '_token_admin')) > 0 &&
            isset($_COOKIE['token_expire']) &&
            $_COOKIE['token_expire'] >= time()) { /* This seems to be set somewhere so extra check added */
            /* Check if user has changed ip */
            if (isset($_SESSION['controlpanel']['admin']['ip']) &&
                $_SERVER['REMOTE_ADDR'] != $_SESSION['controlpanel']['admin']['ip']) {
                    syslog(LOG_ERR, log_string().
                        "User has changed ip from {$_SESSION['controlpanel']['admin']['ip']}".
                        " to {$_SERVER['REMOTE_ADDR']}");
                    return false;
            }
            if (!isset($_SERVER['HTTPS'])) {
                syslog(
                    LOG_ERR,
                    log_string().
                    "User {$_SESSION['controlpanel']['admin']['username']} tried adminfunction on non https server"
                );
                return false;
            }
            return true;
        }

        return false;
    }

    public static function login($username, $password)
    {
        global $transaction, $response;
        global $session_read_only;
        $session_read_only = false;

        $username = trim($username);
        $password = trim($password);

        $transaction->add_client_info();
        $transaction->add_data('username', $username);
        $transaction->add_data('passwd', sha1($password));
        $reply = $transaction->send_command('authenticate');

        self::handleReply($reply, $response);

        if ($response->has_error()) {
            $response->add_data("post_username", $username);
            $_SESSION['response'] = $response;
            syslog(LOG_ERR, log_string() . "Login failed! " . $_SERVER['REMOTE_ADDR'] . " " . $username);
        } else {
            $_SESSION['controlpanel']['admin']['username'] = $username;
            $_SESSION['controlpanel']['admin']['ip'] = $_SERVER['REMOTE_ADDR'];
            $_SESSION['controlpanel']['admin']['login_date'] = date('Y-m-d H:i:s');
            self::redisInitializeKeys();
            syslog(LOG_INFO, log_string() . "Login successful. " . $_SERVER['REMOTE_ADDR'] . " " . $username);
        }
        self::redirect();
    }

    public static function logout()
    {
        global $transaction;
        global $session_read_only;
        $session_read_only = false;

        if (!$transaction) {
            $transaction = new bTransaction();
        }

        $transaction->add_data('admin_id', @$_SESSION['controlpanel']['admin']['id']);
        $transaction->send_admin_command('deauthenticate');

        $client = memcached_client();
        $client->delete(session_id() . '_token_admin');

        self::resetSession('controlpanel');
        self::redirect();
    }

    /*
     * Veirfy Admin priv
     */
    public static function hasPriv($priv)
    {
        return (isset($priv) &&
            isset($_SESSION['controlpanel']) &&
            isset($_SESSION['controlpanel']['admin']) &&
            isset($_SESSION['controlpanel']['admin']['privs']) &&
            array_key_exists($priv, $_SESSION['controlpanel']['admin']['privs']));
    }

    /*
     * Veirfy Admin priv
     */
    public static function hasPrivileges(array $privs)
    {
        foreach ($privs as $key) {
            if (self::hasPriv($key)) {
                return true;
            }
        }
        return false;
    }
    /*
     * Redirect
     */
    public static function redirect()
    {
        global $session;

        // Application name
        $url = $_SERVER['PHP_SELF'];

        // Module
        if (isset($_GET['m'])) {
            $url .= "?m={$_GET['m']}";
        }

        // Action
        if (isset($_GET['a'])) {
            $url .= "&a={$_GET['a']}";
        }

        // Session id
        if (!$session->has_cookie) {
            $url .= "&".session_name()."=".session_id();
        }

        // Log redirect
        syslog(LOG_INFO, log_string()."Redirecting to $url");

        // Redirect
        header("Location: $url");
        exit(0);
    }

    /*
     * Resets the session and keeps some personal information
     */
    public static function resetSession($application = null)
    {
        global $session_read_only;
        $session_read_only = false;

        syslog(LOG_INFO, log_string().'Resetting session objects');
        if (empty($application)) {
            $_SESSION = array();
        } elseif (isset($_SESSION[$application])) {
            $_SESSION[$application] = array();
        }

        unset($_COOKIE['token_expire']);
    }

    /*
     * Handle transaction replys
     */
    public static function handleReply(&$reply, &$response)
    {
        @list ($status, $message) = explode(':', $reply['status'], 2);
        if (preg_match('/TRANS_.*ERROR/', $status) == 1) {
            if ($status == 'TRANS_DATABASE_ERROR') {
                @list($forgot1, $error, $m) = explode(':', $reply['status'], 3);
                $response->add_error('err_database', $error, $m);
            }
            $status = 'TRANS_ERROR';
        }

        syslog(LOG_INFO, log_string()."Retrieved status=$status $message");
        foreach ($reply as $key => $value) {
            @list($code, $message) = explode(":", $value);

            if ($status == 'TRANS_OK' && !is_warning($code)) {
                $response->add_data($key, $value);
            }

            switch ($key) {
                case 'status':
                    break;

                case 'token':
                    if ($status != 'TRANS_OK' && is_error($code)) {
                        $response->add_error('err_token', $code, $message);
                        $_SESSION['response'] = $response;

                        if ($code == 'ERROR_TOKEN_REPLACED') {
                            syslog(
                                LOG_ERR,
                                log_string().
                                "{$_SESSION['controlpanel']['admin']['username']}'s token is not valid anymore,".
                                " user is logged in with another session from $message."
                            );
                        }
                        setcookie('token_expire', time() - 1, time() - 1);

                        self::logout();
                    }
                    break;

                case 'admin_id':
                    $_SESSION['controlpanel']['admin']['id'] = $value;
                    break;

                case 'admin_privs':
                    $privs = explode(',', $value);
                    foreach ($privs as $priv) {
                        $val = true;
                        if (preg_match('/=/', $priv)) {
                            list ($priv, $val) = explode('=', $priv, 2);
                        }
                        $_SESSION['controlpanel']['admin']['privs'][$priv] = $val;
                    }
                    break;

                default:
                    if ($status != 'TRANS_OK') {
                        if (is_warning($code)) {
                            $response->add_data('warn_'.$key, lang($code));
                        }
                        if (is_error($code)) {
                            $response->add_error('err_'.$key, $code, $message);
                        }
                    }
            }
        }
        if ($status != 'TRANS_OK') {
            if ($status != 'TRANS_ERROR') {
                die_with_template(
                    __FILE__,
                    log_string()."Transaction handler exited with errorcode: $status : $message"
                );
            } elseif (!$response->has_error()) {
                die_with_template(
                    __FILE__,
                    log_string()."Transaction handler returns unkown errors, errorcode: $status : $message"
                );
            }
        }
    }

    public static function redisInitializeKeys()
    {
        global $BCONF;
        $redis_conn= array(
            'servers' =>  $GLOBALS['BCONF']['*']['redis']['session']['host'],
            'debug' => true,
            'id_separator' => Bconf::get($GLOBALS['BCONF'], "*.redis.session.id_separator"),
            'timeout' => Bconf::get($GLOBALS['BCONF'], "*.redis.session.connect_timeout")
        );
        $rsc = new RedisSessionClient($redis_conn);
        $admin_user = $_SESSION['controlpanel']['admin']['username'];
        $hashkey = 'rsd1xAdmin_'.$admin_user.'_';

        $rsc->delete($hashkey.'accept');
        $rsc->delete($hashkey.'refuse');
        $rsc->delete($hashkey.'acceptEdit');
        $rsc->delete($hashkey.'totalreviewed');
        $rsc->set($hashkey.'totalreviewed', 0);
    }

    public static function slugify($string)
    {
        $string = html_entity_decode($string);
        $clean = iconv('iso-8859-1', 'ASCII//TRANSLIT', $string);
        $clean = strtolower($clean);
        $clean = preg_replace("/[^a-z0-9]+/", '-', $clean);
        $clean = preg_replace("/(^-+|-+$)/", '', $clean);
        return $clean;
    }

    public static function filterStoreChars($value)
    {
        if (isset($value)) {
            $value = preg_replace('/</', ' ', $value);
            $value = preg_replace('/>/', ' ', $value);
            $value = preg_replace("/\t/", " ", $value);
            $value = preg_replace("/ +/", " ", $value);
            // $80 is euro in FF latin1
            $value = preg_replace("/[!\$�\x80]/", '', $value);
            $value = preg_replace('/\*+/', '*', $value);
            $value = preg_replace('/^[*#<>~-]+/', '', $value);
            $value = preg_replace('/\(+/', '(', $value);
            $value = preg_replace('/\)+/', ')', $value);

            $value = preg_replace('/-+/', '-', $value);
            $value = preg_replace('/_+/', '_', $value);
            $value = preg_replace('/~+/', '~', $value);
            $value = preg_replace("/\\\\+/", "\\", $value);
            //$value = preg_replace('/^"(.+)"$/', '\1', $value);
            $value = stripslashes($value);
            if ($value == strtoupper($value)) {
                $value = strtolower($value);
            }
            preg_match('/^(\.? ?)(.*)/', $value, $match);
            $value = $match[1] . ucfirst($match[2]);
            // Clean newlines
            $value = clean_and_trim($value);
            $value =  htmlentities($value);
        } else {
            $value = null;
        }
        return $value;
    }

    public static function addNoticesToResponse($ad, &$response, $only_note_type = null)
    {
        if (@$ad['notices']) {
            foreach ($ad['notices'] as $notice) {
                if (isset($notice['ad_id'])) {
                    $note_type = 'ad';
                }
                if (isset($notice['user_id'])) {
                    $note_type = 'email';
                }
                if (isset($notice['uid'])) {
                    $note_type = 'uid';
                }

                if ($only_note_type != null && $only_note_type != $note_type) {
                    continue;
                }

                $response->fill_array("notice_${note_type}", $notice['abuse'] == '0' ? 'normal' : 'abuse');
                $response->fill_array("notice_${note_type}_username", $notice['username']);
                $response->fill_array("notice_${note_type}_body", nl2br(htmlspecialchars($notice['body'])));
                $response->fill_array(
                    "notice_${note_type}_date",
                    strftime("%Y-%m-%d", strtotime(substr($notice['created_at'], 0, 19)))
                );
                if (preg_match("/^OBS!/", $notice['body'])) {
                    $response->fill_array("notice_${note_type}_obs", "1");
                }
            }
        }
    }

    public static function addPageData(&$response, $offset, $total, $limit, $page_max_count)
    {
        $response->add_data('offset', $offset);
        $response->add_data('offset_uri', get_request_uri('offset'));

        $page_count = intval(ceil($total / $limit));
        $start_page_count = (int)max(0, floor($offset/$limit) - $page_max_count/2);
        $end_page_count = (int)min($page_count, $start_page_count + $page_max_count);
        for ($i = $start_page_count; $i < $end_page_count; $i++) {
            $response->fill_array('offset_page', $i * $limit);
            $response->fill_array('offset_page_nr', $i + 1);
        }

        // Previous page
        if ($offset - $limit >= 0) {
            $response->add_data('offset_page_prev', $offset - $limit);
        }

        // Next and last page link
        if ($offset + $limit < $total) {
            $response->add_data('offset_page_next', $offset + $limit);
            $response->add_data('offset_page_last', ($page_count - 1) * $limit);
        }
    }

    public static function createProfileCacheHash($email = '')
    {
        global $BCONF;
        return Bconf::get($BCONF, "*.accounts.session_redis_prefix").sha1($email);
    }
}
