<?php
require_once('autoload_lib.php');


define('FPDF_FONTPATH',SERVER_ROOT.'/include/');
require_once('fpdf.php');

use Yapo\Cpanel;

function gallery_handle_line(&$data, $key, $value) {
	if ($key == 'total')
		push_value($data, $key, $value);
}

function timesort($a, $b) {
	return strcmp($a['timestamp'], $b['timestamp']);
}

class cpanel_module_search extends cpanel_module {

	function cpanel_module_search() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.search');

		// Init this controlpanel module
		$this->init();
	}


	/*
	 * MENU: Search
	 *
	 * Search for ads, users etc.
	 */
	function search() {
		$this->response->fill_array('page_js', '/js/scriptaculous/lib/prototype.js');
		$this->response->fill_array('page_js', '/js/scriptaculous/src/scriptaculous.js');
		$this->response->fill_array('page_js', '/tjs/arrays_v2.js');
		$this->response->fill_array('page_js', '/js/events.js');
		$this->response->fill_array('page_js', '/js/category_dropdown.js');

		if (!isset($_REQUEST['time_from']) && !isset($_REQUEST['time_to'])) {
			$this->response->add_data('get_time_from', date('Y-m-d', strtotime('-1 month')));
			$this->response->add_data('get_time_to', date('Y-m-d'));
		}

		if (isset($_REQUEST['queue']))
			$this->response->add_data('queue',$_REQUEST['queue']);
		if (isset($_REQUEST['category_group']))
			$this->response->add_data('category_group',$_REQUEST['category_group']);
		if (isset($_REQUEST['sub_category']))
			$this->response->add_data('sub_category',$_REQUEST['sub_category']);
		if (isset($_REQUEST['region']))
			$this->response->add_data('region',$_REQUEST['region']);
		if (isset($_REQUEST['refusal']))
			$this->response->add_data('refusal', $_REQUEST['refusal']);
		if (isset($_REQUEST['reviewer']))
			$this->response->add_data('reviewer', $_REQUEST['reviewer']);

		$this->list_admins();
		if (isset($_REQUEST['search'])) {
			$this->get_search();
			return;
		}



		$this->display_results('controlpanel/search_form.html');
	}

	function show_hide() {
		$transaction = new bTransaction();
		$transaction->add_data('ad_id', intval($_GET['id']));
		if ($_GET['cmd'] == 'show')
			$transaction->add_data('newstatus', 'active');
		else
			$transaction->add_data('newstatus', 'hidden');
		$reply = $transaction->send_admin_command('ad_status_change');
		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code)
				$this->response->add_data("err_$param", lang($code));
		}
		$this->display_results('controlpanel/show_hide_done.html');
	}

	function reviewers() {
		$this->response->fill_array('page_js', '/js/search_reviewers.js');

		if (isset($_REQUEST['ad_state']))
			$this->response->add_data('ad_state', $_REQUEST['ad_state']);
		if (isset($_REQUEST['refusal']))
			$this->response->add_data('refusal', $_REQUEST['refusal']);
		if (isset($_REQUEST['reviewer']))
			$this->response->add_data('reviewer', $_REQUEST['reviewer']);
		if (isset($_REQUEST['action_date']))	
			$this->response->add_data('action_date', $_REQUEST['action_date']);
		else
			$this->response->add_data('action_date', date('Y-m-d'));

		$this->list_admins();
		if (isset($_REQUEST['search'])) {
			$this->get_reviewers_search();
			return;
		}
		$this->display_results('controlpanel/search_form.html');	
	}

	function paylog() {
		$this->response->fill_array('page_js', '/js/scriptaculous/lib/prototype.js');
		$this->response->fill_array('page_js', '/js/scriptaculous/src/scriptaculous.js');

		if (!isset($_REQUEST['time_from']) && !isset($_REQUEST['time_to'])) {
			$this->response->add_data('get_time_from', date('Y-m-d', strtotime('-1 month')) . ' 00:00:00');
			$this->response->add_data('get_time_to', date('Y-m-d') . ' 23:59:59');
		}

		if (isset($_REQUEST['search'])) {
			/* WARN about timespan */
			if (empty($_REQUEST['q']) && empty($_REQUEST['warn_timespan'])) {
				if ($_REQUEST['timespan'] == "custom" && strtotime($_REQUEST['time_to']) - strtotime($_REQUEST['time_from']) > 1800) {
					$this->response->add_error('warn_timespan', "WARN_SEARCH_TIMESPAN_TOO_LONG");
					$this->display_results('controlpanel/search_form.html');
					return;
				}
			}

			$this->get_paylog_search();
			return;
		}

		$this->display_results('controlpanel/search_form.html');
	}	
 
	function maillog() {
		$this->response->fill_array('page_js', '/js/scriptaculous/lib/prototype.js');
		$this->response->fill_array('page_js', '/js/scriptaculous/src/scriptaculous.js');

		if (!isset($_REQUEST['time_from']) && !isset($_REQUEST['time_to'])) {
			$this->response->add_data('get_time_from', date('Y-m-d', strtotime('-1 month')));
			$this->response->add_data('get_time_to', date('Y-m-d'));
		}
		if (isset($_REQUEST['search'])) {
			$this->get_maillog_search('email',$_REQUEST['search_type']);
			return;
		}

		$this->display_results('controlpanel/search_form.html');
	}

	function account() {
		$this->response->fill_array('page_js', '/js/scriptaculous/lib/prototype.js');
		$this->response->fill_array('page_js', '/js/scriptaculous/src/scriptaculous.js');
		if (isset($_REQUEST['action'])) {
			$email = $_REQUEST["action_email"];
			if (in_array($_REQUEST['action'], array('Deactivate', 'Activate'))) {
				$transaction = new bTransaction();
				$transaction->add_data("email", $email);
				if ( $_REQUEST['action'] == 'Deactivate') {
					$transaction->add_data("status","inactive");
				}
				$reply = $transaction->send_admin_command("manage_account");
				if ($transaction->has_error(true)) {
					$this->response->add_data("deactivation_error",1);
					foreach ($transaction->get_errors() as $param => $code)
						$this->response->add_data("err_$param", lang($code));
				}
				$this->response->add_data("action",$_REQUEST['action'].'d');
			} else {
				$acc_session = new AccountSession();
				if ($_REQUEST['action'] == 'blocked') {
					$result = $acc_session->lock_account($email);
					if ($result !== null) {
						$this->response->add_data('blocked_success', ($result) ? 1 : 0);
					}
				} else if ($_REQUEST['action'] == 'unlock') {
					$result = $acc_session->unlock_account($email);
					if ($result !== null) {
						$this->response->add_data('unlock_success', ($result) ? 1 : 0);
					}
				}

				$this->response->add_data('action', $_REQUEST['action']);
			}
			$this->display_results('controlpanel/deactivate_for_users_account.html');
			return;
		}
		if (isset($_REQUEST['search']) && isset($_REQUEST['action_email'])) {
			$transaction = new bTransaction();
			$transaction->add_data('email',$_REQUEST['action_email']);
			$reply = $transaction->send_command('get_account');
			if ($transaction->has_error(true)) {
				$this->response->add_data("search_error",1);
				if (isset($reply['error']) and $reply['error'] == "ACCOUNT_NOT_FOUND") {
					$this->response->add_data("err_account_not_found", $reply['error']);
                }else {
                    foreach ($transaction->get_errors() as $param => $code) {
                        $this->response->add_data("err_$param", lang($code));
                    };
                }
			}else{
				if (isset($reply['account_status']) && !empty($reply['account_status'])  && !empty($reply['email'])) {
					$this->response->add_extended_array(array('account'=>$reply));
				}
			}

			$acc_session = new AccountSession();
			if ($acc_session->is_blocked($_REQUEST['action_email'])) {
				$this->response->add_data('lock_account', 1);
			}

			$this->display_results('controlpanel/search_account_display.html');
			return;
		}
		$this->display_results('controlpanel/search_form.html');
	}

	function apply_ad_changes($ad) {
		/* Ad changes */
		if (isset($ad['ad_change_params'])) {
			foreach($ad['ad_change_params'] as $key => $value) {
				if (substr($key, 0, 6) == 'param_')
					$ad['params'][substr($key, 6)] = $value;
				else
					$ad['ad'][$key] = $value;
			}
		}

		/* Ad image changes */
		if (isset($ad['ad_image_changes'])) {

			$new_images = array();

			/*
			add still existing old images
			*/
			for ($i = 0; isset($ad['images'][$i]); $i++) {
				for ($j = 0; isset($ad['ad_image_changes']['image'.$j]); $j++) {
					if (isset($ad['ad_image_changes']['image'.$j]['name']) &&
							$ad['images'][$i]['name'] == $ad['ad_image_changes']['image'.$j]['name']) {
						$new_images[] = $ad['images'][$i];
						break;
					}
				}
			}

			/*
			add new images
			*/
			for ($i = 0; isset($ad['ad_image_changes']['image'.$i]); $i++) {
				$found = 0;
				for ($j = 0; $j < count($new_images); $j++) {
					if ($ad['ad_image_changes']['image'.$i]['name'] == $new_images[$j]['name']) {
						$found = 1;
						break;
					}
				}
				if (!$found) {
					$new_images[] = $ad['ad_image_changes']['image'.$i];
				}
			}
			$ad['images'] = $new_images;
		}

		return $ad;
	}

	function get_paylog_search() {
		ini_set("memory_limit", "200M");
		ini_set("max_execution_time", 120);

		global $BCONF;

		$transaction = new bTransaction();

		$transaction->add_data('search_key', $_REQUEST['search_type']);
		$transaction->add_data('search_value', $_REQUEST['q']);


		switch (@$_REQUEST['timespan']) {
		case 'day':
			$time_from = strftime('%Y-%m-%d %T', time() - 86400);
			$time_to = strftime('%Y-%m-%d %T', time());
			break;
		case 'week':
			$time_from = strftime('%Y-%m-%d %T', time() - 7 * 86400);
			$time_to = strftime('%Y-%m-%d %T', time());
			break;
		case 'custom':
			$time_from = $_REQUEST['time_from'];
			if (strpos($time_from, ':') === false)
				$time_from .= ' 00:00:00';
			$time_to = $_REQUEST['time_to'];
			if (strpos($time_to, ':') === false)
				$time_to .= ' 23:59:59';
			break;
		case 'all':
		default:
			$time_from = null;
			$time_to = null;
			break;
		}
		if ($time_from)
			$transaction->add_data('timestamp_from', $time_from);
		if ($time_to)
			$transaction->add_data('timestamp_to', $time_to);

		$reply = $transaction->send_admin_command('search_pay_log');

		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code)
				$this->response->add_data("err_$param", lang($code));
			$this->display_results('controlpanel/search_form.html');
			return;
		}

		if (is_array($reply['result'])) {
			usort($reply['result'], "timesort");
			foreach ($reply['result'] as $key => $result) {
				$result['reference'] = explode(',', @$result['reference']);
				if (!isset($result['remote_addr']) || preg_match("/^10.0/", $result['remote_addr']))
					$result['remote_addr'] = '';
				$reply['result'][$key] = $result;
			}
			$this->response->add_extended_array(array('result' => $reply['result']));
		}

		$this->display_results('controlpanel/search_display_paylog.html');
	}

	function get_maillog_search() {
		ini_set("memory_limit", "200M");
		ini_set("max_execution_time", 120);

		global $BCONF;

		$transaction = new bTransaction();

		$transaction->add_data('search_key', $_REQUEST['search_type']);
		if (!empty($_REQUEST['q']))
			$transaction->add_data('search_value', $_REQUEST['q']);


		switch (@$_REQUEST['timespan']) {
		case 'day':
			$time_from = strftime('%Y-%m-%d %T', time() - 86400);
			$time_to = strftime('%Y-%m-%d %T', time());
			break;
		case 'week':
			$time_from = strftime('%Y-%m-%d %T', time() - 7 * 86400);
			$time_to = strftime('%Y-%m-%d %T', time());
			break;
		case 'custom':
			$time_from = $_REQUEST['time_from'];
			if (strpos($time_from, ':') === false)
				$time_from .= ' 00:00:00';
			$time_to = $_REQUEST['time_to'];
			if (strpos($time_to, ':') === false)
				$time_to .= ' 23:59:59';
			break;
		case 'all':
		default:
			$time_from = null;
			$time_to = null;
			break;
		}
		if ($time_from)
			$transaction->add_data('timestamp_from', $time_from);
		if ($time_to)
			$transaction->add_data('timestamp_to', $time_to);

		if ($_REQUEST["state"]) 
			$transaction->add_data('state', $_REQUEST["state"]);

		$reply = $transaction->send_admin_command('search_mail_queue');

		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code) {
				if ($param == "search_value;timestamp_from") {
					$param = 'q';
					$code = 'ERROR_TIME_MISSING';
				}
				$this->response->add_data("err_$param", lang($code));
			}
			$this->display_results('controlpanel/search_form.html');
			return;
		}

		if (isset($reply['result']) && is_array($reply['result'])) {
			foreach ($reply['result'] as $key => $result) {
				$result['body'] = nl2br($result['body']);
				if (isset($result['reason']))
					foreach (array('subject', 'body') as $what)
						$result[$what] = preg_replace("/({$result['reason']})/i", "<span class='RedText'>\\1</span>", $result[$what]);
				$reply['result'][$key] = $result;
			}
			$this->response->add_extended_array(array('result' => $reply['result']));
		}

		$this->display_results('controlpanel/search_display_maillog.html');
	}

	function list_admins() {

		$transaction = new bTransaction();
		$reply = $transaction->send_admin_command("listadmins");
		if ($this->response->has_error()) {
			$this->response->add_data("error_message", lang("TRANS_UNEXPECTED_ERROR"));
		}else{
			$admins_id = array();
			$admins_name = array();
			foreach($reply['admin'] as $key => $value) {
				$admins_id[] = $value['admin_id'];
				$admins_name[] = $value['fullname'];
			}	
			$this->response->add_data("admins_id", $admins_id);
			$this->response->add_data("admins_name", $admins_name);
		}
	}	
	function get_search() {
		global $BCONF;

		$search_types = array_keys(bconf_get($BCONF, "controlpanel.modules.search.filter"));

		$transaction = new bTransaction();

		$search_type = $_REQUEST['search_type'];
		if (!in_array($search_type, $search_types)) {
			/* Error */
			$this->response->add_data("error", lang("ERROR_SEARCH_TYPE_INVALID"));
			$this->display_results('controlpanel/search_form.html');
			return;
		}
		$transaction->add_data('search_type', $search_type);
		$transaction->add_data($search_type, $_REQUEST['q']);
		if (!empty($_REQUEST['queue']))
		    $transaction->add_data('filter_name', $_REQUEST['queue']);
		if (!empty($_REQUEST['region']))
		    $transaction->add_data('region', $_REQUEST['region']);
		if (!empty($_REQUEST['archive_group']))
		    $transaction->add_data('archive_group', $_REQUEST['archive_group']);
		if (!empty($_REQUEST['category_group']) && $_REQUEST['category_group'] > 0) {

			$cat_group = intval($_REQUEST['category_group']);
			$sub_cat = intval($_REQUEST['sub_category']);

			if ($sub_cat && bconf_get($BCONF, "*.cat.{$sub_cat}.parent") == $cat_group) {
				$category = $sub_cat;
			} else {
				$category = $cat_group;
			}
			$transaction->add_data('category', $category);
		}
		switch (@$_REQUEST['timespan']) {
		case 'day':
			$time_from = strftime('%Y-%m-%d %T', time() - 86400);
			$time_to = null;
			break;
		case 'week':
			$time_from = strftime('%Y-%m-%d %T', time() - 7 * 86400);
			$time_to = null;
			break;
		case 'custom':
			$time_from = $_REQUEST['time_from'];
			if (strpos($time_from, ':') === false)
				$time_from .= ' 00:00:00';
			$time_to = $_REQUEST['time_to'];
			if (strpos($time_to, ':') === false)
				$time_to .= ' 23:59:59';
			break;
		case 'all':
		default:
			$time_from = null;
			$time_to = null;
			break;
		}
		if ($time_from)
			$transaction->add_data('time_from', $time_from);
		if ($time_to)
			$transaction->add_data('time_to', $time_to);
		if (isset($_GET['offset']))
			$transaction->add_data('offset', intval($_GET['offset']));
		if (bconf_get($BCONF, "controlpanel.modules.search.count_all"))
			$transaction->add_data('count_all', 1);
		if (!empty($_GET['gallery_only']))
			$transaction->add_data('gallery_only', 1);

		$reply = $transaction->send_admin_command('search_ads');

		/*Check for errors in transaction*/
		/*Array ( [token] => ERROR_TOKEN_OLD [status] => TRANS_ERROR )*/
	
		if ($reply['status']=="TRANS_ERROR") { 
			$this->response->add_error('err_token', $reply['token']);
                        $_SESSION['response'] = $this->response;	
				switch($reply['token']) {
				case "ERROR_TOKEN_OLD":
					syslog(LOG_ERR, log_string()."{$_SESSION['controlpanel']['admin']['username']}'s session expired.");
					setcookie('token_expire', time() - 1, time() - 1);
					Cpanel::logout();
				;
				case "ERROR_TOKEN_REPLACED":
					syslog(LOG_ERR, log_string()."{$_SESSION['controlpanel']['admin']['username']}'s token is not valid anymore, user is logged in with another session from $message.");
					setcookie('token_expire', time() - 1, time() - 1);
					Cpanel::logout();
				;
				default:
				;
			
				}
			}


		if ($transaction->has_error()) {
			/* Error */
			foreach ($transaction->get_errors() as $param => $code)
				$this->response->add_data("err_$param", lang($code));
			$this->display_results('controlpanel/search_form.html');
			return;
		}


		$total = $reply['total'];
		$this->response->add_data('total', $total);
		if (isset($reply['subtotal'])) {
			$this->response->add_extended_array(array('ads_total' => $reply['subtotal']));
		}
		if (isset($_REQUEST['emails']) && isset($reply['ad'])) {
			$uid = $reply['ad'][0]['users']['uid'];
			/* List all emails for this uid */
			$transaction->reset();
			$transaction->add_data('uid', $uid);
			$reply = $transaction->send_admin_command('search_uid');
			if ($transaction->has_error()) {
				/* Error */
				$this->response->add_data("error", "search_uid");
			}
			$this->response->add_data('uid_emails', $reply['email']);
		}
		elseif (isset($reply['ad'])) {
			$search_queue_order = array();
			foreach (bconf_get($BCONF, "controlpanel.modules.search.queues") as $num => $queue)
				$search_queue_order[$queue['id']] = $num;

			/* Notices */
			$this->response->add_data("priv_notice_abuse", Cpanel::hasPriv('notice_abuse') ? 1 : 0);

			if ($_REQUEST['search_type'] == 'uid') {
				Cpanel::addNoticesToResponse($reply['ad']['0'], $this->response, 'uid');
			} else if ($_REQUEST['search_type'] == 'email') {
				Cpanel::addNoticesToResponse($reply['ad']['0'], $this->response, 'email');
			}

			$ads = array();
			foreach ($reply['ad'] as $num => $ad) {
				$ad = $this->apply_ad_changes($ad);
				if (isset($ad['actions']['timestamp'])) {
					$ts = strtotime(substr($ad['actions']['timestamp'], 0, 19));
					$ad['timestamp'] = date('ymd', $ts) . '<br />' . date('H:i', $ts);
				}
				if (isset($ad['payment']['paid_at'])) {
					$paid_at = $ad['payment']['paid_at'];
					if (is_array($paid_at)) {
						$paid_at = $paid_at[0];
					}
					$ts = strtotime(substr($paid_at, 0, 19));
					$ad['pay_time'] = date('ymd H:i', $ts);
				}
				if (isset($ad['actions']['regtime'])) {
					$ts = strtotime(substr($ad['actions']['regtime'], 0, 19));
					$ad['reg_time'] = date('ymd H:i', $ts);
				}
				if (isset($ad['ad']['list_time'])) {
					$ts = strtotime(substr($ad['ad']['list_time'], 0, 19));
					$ad['publish_time'] = date('ymd H:i', $ts);
				}
				if (isset($ad['notices'])) {
					$notices = array();
					foreach ($ad['notices'] as $num => $notice) {
						if (isset($notice['created_at']))
							$notice['created_at'] = substr($notice['created_at'], 0, 10);
						if (isset($notice['uid']))
							$notices['uid'] = $notice;
						elseif (isset ($notice['user_id']))
							$notices['user'] = $notice;
						else
							$notices['ad'] = $notice;
					}
					$ad['notices'] = $notices;
				}
				if (isset($ad['params']['gallery'])) {
					$gallery_stats = array();
					bsearch_search_vtree(bconf_get($BCONF, "*.common.statpoints"), "event:GALLERYVIEW,GALLERYCLICK id:{$ad['ad']['list_id']}", "gallery_handle_line", $gallery_stats);	
					$ad['gallery']['views'] = isset($gallery_stats['total'][0]) ? $gallery_stats['total'][0] : 0;
					$ad['gallery']['clicks'] = isset($gallery_stats['total'][1]) ? $gallery_stats['total'][1] : 0;
				}
				$has_label=isset($ad['params']['label']) && !empty($ad['params']['label']);
				$has_images=isset($ad['images']) && sizeof($ad['images'])>0;
				$prods = $this->get_product_items_for_ad($ad['ad']['category'], $ad['ad']['status'], $has_images, $has_label);
				$ad['catalog_products'] = $prods;
				$ads[$search_queue_order[$ad['filter']]][] = $ad;
			}
			ksort($ads);
			$this->response->add_extended_array(array('ads' => $ads));
			$this->response->add_data('today', date('ymd'));

			if (!empty($_REQUEST['queue']))
				$total = $reply['subtotal'][$_REQUEST['queue']];

			if (!empty($_REQUEST['search_type']))
				$max_limit = bconf_get($BCONF, 'controlpanel.modules.search.' . $_REQUEST['search_type'] . '.max_limit');
			if (!isset($max_limit))
				$max_limit = bconf_get($BCONF, 'controlpanel.modules.search.max_limit');
			Cpanel::addPageData(
				$this->response,
				intval(@$_GET['offset']),
				$total,
				$max_limit,
				bconf_get($BCONF, 'controlpanel.modules.search.max_page_count')
			);
		}
		if (Cpanel::hasPriv('search.mass_delete'))
			$this->response->add_data('can_mass_delete', 1);

		if (Cpanel::hasPriv('search.undo_delete'))
			$this->response->add_data('can_undo_delete', 1);

		if (in_array($_GET['search_type'], array('email', 'uid', 'list_id')) && Cpanel::hasPriv('search.uid_emails'))
			$this->response->add_data('can_uid_emails', 1);

		if (in_array($_GET['search_type'], array('email', 'uid', 'ad_id', 'list_id')) && Cpanel::hasPriv('search.abuse_report'))
			$this->response->add_data('show_abuse_report_link', 1);

		if (Cpanel::hasPriv('Services.admin')) {
			$this->response->add_data('priv_bump', 1);
		}

		if (Cpanel::hasPriv('Services.packtoif')) {
			$this->response->add_data('priv_pack2if', 1);
		}

		if (Cpanel::hasPriv('Services.packstatus')) {
			$this->response->add_data('priv_packstatus', 1);
		}

		unset($refusals);
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$refusals = bconf_get($this->dyn_bconf, '*.*.common');
			$this->response->add_extended_array($refusals);
		} else {
			$this->dyn_bconf = false;
		}

		$this->display_results('controlpanel/search_display.html');
	}

	function get_reviewers_search() {

		$transaction = new bTransaction();
		$transaction->add_data('date', $_REQUEST['action_date']); 
		$transaction->add_data('reviewer', $_REQUEST['reviewer']);
		if (!empty($_REQUEST['ad_state'])) {
			$transaction->add_data('ad_state', $_REQUEST['ad_state']);
			if (!empty($_REQUEST['refusal']) && $_REQUEST['ad_state'] == 'refused')
				$transaction->add_data('refusal_reason', $_REQUEST['refusal']);
		}
		$reply = $transaction->send_command('reviewers_search');	
		if ($transaction->has_error()) {
			foreach($transaction->get_errors() as $param => $code) {
				$this->response->add_data("err_$param", $code);
			}
			$this->response->add_data("search_error", 1);
		}else {
			if (isset($reply['reviewers_search']) && !empty($reply['reviewers_search'])) {
				foreach($reply['reviewers_search'] as $ad) {
					$ads[] = $ad;
				} 
				$this->response->add_extended_array(array('reviews'=>$ads));
			}
		}
		$this->display_results('controlpanel/search_reviewer_display.html');
	}

	function auto_complete_email() {
		$response = new bResponse();
		$transaction = new bTransaction();
		$result = array();
		if (!empty($_REQUEST['email']) || !empty($_REQUEST['email_part'])) {
			if (!empty($_REQUEST['email_part'])) {
				$transaction->add_data('email', $_REQUEST['email_part']);
			} else {
				$transaction->add_data('email', $_REQUEST['email']);
			}
			$reply = $transaction->send_command('email_prefix');

			if ($reply["status"] == "TRANS_ERROR") {
				$response->add_data('error', $reply["email"]);
			}
			else if (isset($reply['email'])) {
				$result = $reply['email'];
			}
		}

		$response->add_data('emails', $result);

		$response->call_template('controlpanel/search_auto_complete.html');
		exit(1);
	}

	function ad_history() {
		$transaction = new bTransaction();
		if (!empty($_REQUEST['ad_id'])) {
			$transaction->add_data('ad_id', $_REQUEST['ad_id']);
			$reply = $transaction->send_command('ad_history');
		}

		if (isset($reply['ad_history'])) {
			$i = 0;
			foreach ($reply['ad_history'] as $num => $ad_state) {
				$this->response->fill_array('action_type', $ad_state['action_type']);
				$this->response->fill_array('state', $ad_state['state']);
				$this->response->fill_array('transition', $ad_state['transition']);
				$this->response->fill_array('timestamp', $ad_state['timestamp']);
				$this->response->fill_array('admin', $ad_state['admin']);
				$this->response->fill_array('colored', ++$i % 2);
			}
		}

		if (isset($_GET['popup'])) {
			$this->response->add_data('content', 'controlpanel/ad_history.html');
			$this->response->call_template('controlpanel/simple.html');
			exit(0);
		} else
			$this->display_results('controlpanel/ad_history.html');
	}

	/* ajax function for changing a notice */
	function setnotice() {
		$transaction = new bTransaction();

		if (isset($_POST['uid']))
			$transaction->add_data('uid', $_POST['uid']);
		else if (isset($_POST['email'])) {
			$transaction->add_data('email', $_POST['email']);
			$transaction->add_data('type', 'email');
		}

		$transaction->add_data('body', $_REQUEST['body']);

		$reply = $transaction->send_admin_command('setnotice');
		$response = new bResponse();
		Cpanel::handleReply($reply, $response);
		if ($response->has_error()) {
			echo "'false'";
			exit(1);
		}

		echo "'true'";
		exit(0);
	}

	function abuse_report() {
		ini_set("memory_limit", "200M");
		ini_set("max_execution_time", 360);

		$search_type = $_REQUEST['search_type'];
		$transaction = new bTransaction();

		$transaction->add_data('search_type', $search_type);
		$transaction->add_data($search_type, $_REQUEST['q']);

		if ($_REQUEST['timespan'] != 'all') {
			$transaction->add_data('time_from', $_REQUEST['time_from'] . ' 00:00:00');
			$transaction->add_data('time_to', $_REQUEST['time_to'] . ' 23:59:59');
		}
		
		$reply = $transaction->send_admin_command('abuse_report');
		if ($transaction->has_error()) {
			/* Error */
			$this->response->add_data("error", "abuse_report_error");
		}

		$ads = array();
		$min_time = false;
		$max_time = false;
		if (is_array($reply['abuse_report'])) {
			foreach ($reply['abuse_report'] as $uid => $uid_data) {
				$this->response->add_data('uid', $uid);
				foreach ($uid_data as $user_id => $user_data) {
					$email = $user_data['user']['email'];
					$num_emails = $user_data['user']['num_emails'];
					$this->response->fill_array('email', $email);
					$this->response->fill_array('email_ads', count($user_data['ads']));
					if (isset($user_data['ads'])) {
						foreach ($user_data['ads'] as $ad_id => $ad_data) {
							ksort($ad_data, SORT_NUMERIC);
							foreach ($ad_data as $action_id => $action_data) {
								$ads["$ad_id-$action_id"] = $action_data;
								$ads["$ad_id-$action_id"]['email'] = $email;
								$ads["$ad_id-$action_id"]['uid'] = $uid;
								$ads["$ad_id-$action_id"]['num_emails'] = $num_emails;
								$ads["$ad_id-$action_id"]['num_ad_actions'] = count($ad_data);
								if (isset($action_data['current']['timestamp'])) {
									$time = substr($action_data['current']['timestamp'], 0, 10);
									if (!$min_time || $min_time > $time)
										$min_time = substr($time, 0, 10);
									if (!$max_time || $max_time < $time)
										$max_time = substr($time, 0, 10);
								}
								if (isset($action_data['reg']['timestamp'])) {
									$time = substr($action_data['reg']['timestamp'], 0, 10);
									if (!$min_time || $min_time > $time)
										$min_time = substr($time, 0, 10);
									if (!$max_time || $max_time < $time)
										$max_time = substr($time, 0, 10);
								}
							}
						}
					}
				}
			}
		}

		if (!isset($_REQUEST['output']) || $_REQUEST['output'] != 'text') {

			//Instanciation of inherited class
			$pdf = new \Yapo\PdfManager();
			$pdf->SetFont('Times','',12);
			$pdf->AliasNbPages();

			$pdf->AddPage();
			$this->response->add_data('min_time', $min_time);
			$this->response->add_data('max_time', $max_time);
			$output = $this->response->call_template('controlpanel/abuse_report_first_page.html', true);
			$pdf->writeHTML($output);

			//Logo
			$pdf->Image(SERVER_ROOT.'/www-ssl/img/logo_pdf.jpg',160,10, 33);

			$pdf->AddPage();
			$output = $this->response->call_template('controlpanel/abuse_report_info.html', true);
			$pdf->writeHTML($output);


			$old_ad_id = false;
			foreach($ads as $action => $ad) {
				$this->response->reset_extended_array();
				$this->response->add_extended_array('ads', array($action => $ad));
				$output = $this->response->call_template('controlpanel/abuse_report.html', true);

				if ($old_ad_id === false || $old_ad_id != $ad['ads']['ad_id']) {
					$pdf->AddPage();
					$this->response->add_extended_array('new', 1);
					$old_ad_id = $ad['ads']['ad_id'];
				}
				$pdf->writeHTML($output);
			}

			$pdf->Output('abuse_report_'.$_REQUEST['q'].'.pdf', 'D');
		} else {
			$this->response->add_extended_array('ads', $ads);
			$this->response->add_data('content', 'controlpanel/abuse_report.html');
			$this->response->call_template('common/simple.html');
		}

		exit;
	}

	function invoke_all() {
		if (!isset($_REQUEST["selected"])) {
			// nothing to do; return to search
			header('Location: /controlpanel?'.$_REQUEST['return_to']);
			die();
		}
		// load data and action from ads to change
		$this->response->add_data('return_to', '/controlpanel?'.$_REQUEST['return_to']);
		$ads = array();
		foreach ($_REQUEST['selected'] as $item) {
			if (count($parts = explode(',', $item)) != 4) continue;
			list($ad_id, $action_id, $list_id, $email) = $parts;
			$ads[] = array(
				'ad_id'=>intval($ad_id),
				'action_id'=>intval($action_id),
				'list_id'=>intval($list_id),
				'email'=>$email
			);
		}

		$errs = array();
		$msgs = array();
		switch ($_REQUEST['op']) {
		case 'delete':
			// delete all the selected ads
			foreach ($ads as $ad) {
				if (empty($ad['list_id'])) {
					// only published ads can be deleted
					$errs[] = sprintf('Ad %s is not published; can\'t delete.', $ad['ad_id']);
					continue;
				}
				if ($this->delete_ad($ad['list_id']) === False) {
					// error deleting
					$errs[] = sprintf('Error deleting ad %s.', $ad['ad_id']);
					continue;
				}
				$msgs[] = sprintf('Deleted ad %s.', $ad['ad_id']);
			}	
			break;
		case 'undelete':
			// undelete all the selected ads
			foreach ($ads as $ad) {
				if ($this->undelete_ad($ad['ad_id']) === False) {
					$errs[] = sprintf('Error undeleting ad %s.', $ad['ad_id']);
					continue;
				}
				$msgs[] = sprintf('Undeleted ad %s.', $ad['ad_id']);
			}
			break;
		case 'refuse':
			$this->dyn_bconf = get_dynamic_bconf();
			foreach ($ads as $ad) {
				$refuse_reason = sanitizeVar($_REQUEST['reason'], 'string');
				if ($this->refuse_ad($ad['ad_id'],$ad['action_id'], $refuse_reason) === false) {
					// error refusing
					$errs[] = sprintf('Error refusing ad %s.', $ad['ad_id']);
					continue;
				}
				$reasonname = bconf_get($this->dyn_bconf, "*.*.common.refusal.{$refuse_reason}.name");
				$msgs[] = sprintf('Refused ad %s with reason "%s".', $ad['ad_id'], $reasonname);
			}
			break;
		}

		// display messages
		$this->response->add_data('has_errors', count($errs) ? '1' : '0');
		$this->response->fill_array('message_color', 'black');
		$this->response->fill_array('message', 'Executing batch <b>'.$_REQUEST['op'].'</b>:');
		foreach ($msgs as $m) {
			$this->response->fill_array('message_color', 'navy');
			$this->response->fill_array('message', $m);
		}
		foreach ($errs as $m) {
			$this->response->fill_array('message_color', 'red');
			$this->response->fill_array('message', $m);
		}
		$this->display_results('controlpanel/search_invoke_all_results.html');
	}
	
	/* User is trying to delete the ad. */
	function delete_ad($id) {
		global $session;

		require_once('blocket_store.php');
		$store = new blocket_store(false);

		syslog(LOG_INFO, log_string()."invoke_all.delete.$id");
		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('id', $id);
		if (Cpanel::checkLogin() && Cpanel::hasPrivileges(array("adminedit", 'adminad.edit_ad'))) {
			$transaction->auth_type = 'admin';
		} elseif ($this->store_token) {
			if ($session->load_active_instance($store, get_class($store), "0")) {
 				$transaction->auth_type = 'store';
			}
		}

		if ($transaction->auth_type) {
			if ($transaction->auth_type == 'store')
				$transaction->add_data('reason', 'user_deleted');
			else
				$transaction->add_data('reason', 'admin_deleted');
			$reply = $transaction->send_admin_command('deletead', true, false);
		} else {
			$transaction->add_data('reason', 'user_deleted');
			$reply = $transaction->send_command('deletead');
		}
		if ($reply['status']!="TRANS_OK:ok, ad $id deleted") {
			return FALSE;
		}
		return TRUE;
	}

	function undelete_ad($id) {
		global $session;
		syslog(LOG_INFO, log_string()."invoke_all.undelete.$id");
		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('ad_id', $id);
		$transaction->auth_type = 'admin';
	
		$reply = $transaction->send_admin_command('undo_deletead', true, false);
		if ($reply['status']!="TRANS_OK") {
			return false;
		}
		return true;
	}

	public function refuse_ad($ad_id, $action_id, $reason) {
		$transaction = new bTransaction();
		$command = 'review';
		$transaction->add_data('ad_id', $ad_id);
		$transaction->add_data('action_id', $action_id);
		$transaction->add_data('action', 'refuse');
		$transaction->add_data('filter_name', 'all');
		$transaction->add_data('reason', $reason);

		if (bconf_get($BCONF, "controlpanel.refusal.supress_email_on_fraud.flag") == "1") {
			$reasonname = strtolower(bconf_get($this->dyn_bconf, "*.*.common.refusal.{$reason}.name"));
			if (preg_match('/^.*(fraud).*$/', $reasonname)) {
				$transaction->add_data("do_not_send_mail", "1");
			}
		}

		if (bconf_get($this->dyn_bconf, "*.*.common.refusal.{$reason}.extra_msg")) {
			$refusal_text = "";
			foreach (bconf_get($this->dyn_bconf, "*.*.common.refusal.{$reason}.extra_msg") as $key => $value) {
				if (!isset($value['no_space']) && $key != 1) {
					$refusal_text .= " ";
				}
				if (isset($value['string'])) {
					$refusal_text .= $value['string'];
				}
				if (isset($value['var'])) {
					if (empty($_POST[$value['var']])) {
						$refusal_text = '';
						break;
					}
					$catnum = intval($_POST[$value['var']]);
					$catname =  bconf_get($BCONF, "*.cat.$catnum.name");
					while (bconf_get($BCONF, "*.cat.$catnum.level") > 1) {
						$catnum = intval(bconf_get($BCONF, "*.cat.$catnum.parent"));
						$catname = bconf_get($BCONF, "*.cat.$catnum.name") . ", $catname";
					}
					$refusal_text .= $catname;
				}
			}
			if ($refusal_text) {
				$transaction->add_data('refusal_text', $refusal_text);
			}
		}
		$reply = $transaction->send_admin_command($command);
		if ($reply['status'] != 'TRANS_OK') {
			return false;
		}
		return true;
	}


	function settings_key_lookup($s,$k,$d) {
		return $d[$k];
	}

	function settings_set_values($s,$k,$v,$d) {
		$d[$k] = $v;
	}

	function get_product_items_for_ad($category, $ad_status, $has_images, $has_label) {
		global $BCONF;
		$params = array(
			'category' => $category,
			'ad_status' => $ad_status,
			'has_img' => (int)$has_images,
			'has_label' => (int)$has_label
		);
		get_settings(
			bconf_get($BCONF, "controlpanel.services_settings"),
			"product_list",
			array($this, "settings_key_lookup"),
			array($this, "settings_set_values"),
			$params
		);
		$ad_product_list = array();
		$product_groups = bconf_get($BCONF, "controlpanel.services_group");
		$product_list = bconf_get($BCONF, "controlpanel.services_list");
		foreach ($product_groups as $product_group_name => $product_group_conf) {
			if (isset($params[$product_group_name])) {
				$products = array();
				$subproducts = explode(',',$params[$product_group_name]);
				foreach ($subproducts as $product) {
					array_push($products, array("name" => $product_list[$product]["name"], "value" => $product));
				}
				$ad_product_list[$product_group_name] = $products;
			}
		}
		ksort($ad_product_list);
		return $ad_product_list;
	}

	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->display_results('controlpanel/search_form.html');
	}
}
?>
