<?php

namespace Yapo;

use Yapo\EasyRedis;
use Yapo\Cpanel;

class CpanelModuleBanners extends CpanelModule
{

    public function __construct()
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Banners'));
        $this->banners_config = array_copy(Bconf::get($BCONF, '*.admin_banners'));
        // Init this controlpanel module
        parent::init();
    }

    public function getFilters($thebanner)
    {
        $filtros = array();
        foreach ($this->banners_config[$_REQUEST['thebanner']]['filter'] as $k => $v) {
            if (isset($v['name'])) {
                array_push($filtros, $v['name']);
            }
        }
        return $filtros;
    }

    public function cleanKey($the_key)
    {
        return str_replace("!", "", $the_key);
    }

    public function redisDoThis($cmd, $the_keys)
    {
        syslog(LOG_INFO, $_SESSION['controlpanel']['admin']['username']." - <".$cmd.">");
        if (count($the_keys) == 0) {
        } else {
            $this->redis = new EasyRedis("redis_banners");
            if ($cmd == "turn_on") {
                $key_format = $this->banners_config[$_REQUEST['thebanner']]['key_format'];
                $format_keys = explode(",", $this->banners_config[$_REQUEST['thebanner']]['format_keys']);

                foreach ($the_keys as $combo) {
                    $values = explode(",", $combo);
                    $key = str_replace($format_keys, $values, $key_format);
                    $this->redis->hset('banner_'.$_REQUEST['thebanner'], $this->cleanKey($key), "1");
                    $this->redis->hdel($_REQUEST['thebanner'].'_empty_since', $this->cleanKey($key));
                    syslog(LOG_INFO, 'banner_'.$_REQUEST['thebanner']." 1- ".$this->cleanKey($key));
                }
            } else {
                foreach ($the_keys as $to_del) {
                    $this->redis->hset('banner_'.$_REQUEST['thebanner'], $this->cleanKey($to_del), "0");
                    syslog(LOG_INFO, 'banner_'.$_REQUEST['thebanner']." 0- ".$this->cleanKey($to_del));
                }
            }
            $this->redis->close();
            $this->response->add_data('success', 'Cambios realizados');
        }
    }

    public function doCmd($cmd)
    {
        $filtros = $this->getFilters($_REQUEST['thebanner']);
        $this->response->add_data('filtros', $filtros);
        $this->response->add_data('thebanner', $_REQUEST['thebanner']);
        $filtros = $this->getFilters($_REQUEST['thebanner']);
        $the_keys = array();
        if ($cmd == "turn_on") {
            $f_arr = array();
            foreach ($filtros as $f) {
                if (!isset($_REQUEST[$f]) || count($_REQUEST[$f]) == 0) {
                    $this->response->add_error('err_missing', 'Debese seleccionar algo en cada columna');
                    $the_keys = array();
                    break;
                }
                $new_keys=array();
                if (count($the_keys)==0) {
                    foreach ($_REQUEST[$f] as $v) {
                        array_push($new_keys, $v);
                    }
                } else {
                    foreach ($the_keys as $old_k) {
                        foreach ($_REQUEST[$f] as $v) {
                            array_push($new_keys, $old_k.",".$v);
                        }
                    }
                }
                $the_keys = $new_keys;
            }
        } else {
            if (!empty($_REQUEST['to_turn_off'])) {
                foreach ($_REQUEST['to_turn_off'] as $key) {
                    array_push($the_keys, $key);
                }
            } else {
                $this->response->add_error('err_missing', 'No hay elementos para apagar');
            }
        }
        $this->redisDoThis($cmd, $the_keys);
    }

    public function admin()
    {
        if (isset($_REQUEST['cmd'])) {
            $this->doCmd($_REQUEST['cmd']);
        } elseif (isset($_REQUEST['thebanner']) && $_REQUEST['thebanner'] != "") {
            $filtros = $this->getFilters($_REQUEST['thebanner']);
            $this->response->add_data('filtros', $filtros);
            $this->response->add_data('thebanner', $_REQUEST['thebanner']);
        }
        $this->displayResults('controlpanel/banners/admin.html');
    }

    public function links()
    {
        $this->displayResults('controlpanel/banners/links.html');
    }

    public function main($function = null)
    {
        $this->response->add_data("priv_admin", Cpanel::hasPriv("Banners.admin")? "1": "0");
        if ($function != null) {
            $this->response->add_data("page_name", "Buscar");
            $this->response->fill_array('page_js', '/js/banners.js');
            $this->runMethod($function);
        } else {
            $this->links();
        }
    }
}
