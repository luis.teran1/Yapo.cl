<?php

namespace Yapo;

class Util
{

    /*
     * Sanitizes recursively the components of an array,
     * with the given type.
     */
    public static function sanitize($arr, $type = 'string')
    {
        $sanitized = array();
        if (isset($arr)) {
            if (is_array($arr)) {
                foreach ($arr as $key => $val) {
                    $sanitized[$key]= self::sanitize($val, $type);
                }
            } else {
                $sanitized = sanitizeVar($arr, $type);
            }
        }
        return $sanitized;
    }

    /*
     * Returns the first error from a trans response
     * by checking the fields specified on $fields,
     * returns $default if $reply has no errors.
     */
    public static function getErrors($reply, $default, array $fields)
    {
        $err = $default;
        if (isset($reply)) {
            foreach ($fields as $error) {
                if (isset($reply[$error])) {
                    $err = $reply[$error];
                    break;
                }
            }
        }
        $prefix = "TRANS_ERROR:";
        if (strpos($err, $prefix) === 0) {
            $err = substr($err, strlen($prefix));
        }
        return $err;
    }
}
