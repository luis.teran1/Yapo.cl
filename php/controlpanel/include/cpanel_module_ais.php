<?php
require_once('cpanel_module.php');
require_once('util.php');
require_once('bTransaction.php');
require_once('bAd.php');

class cpanel_module_ais extends cpanel_module {
	function cpanel_module_ais() {
		/* Get configuration for easy access */
		$this->config = array_copy(bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.ais'));

		/* Get dynamic configuration */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$root = '*.controlpanel.modules.ais.settings';
			$settings = bconf_get($this->dyn_bconf, $root);
			if (isset($settings)) {
				if (is_string($settings))
					$settings = array($settings);
				foreach ($settings as $key => $value) {
					$this->config['settings'][$key] = $value;
				}
			}
		} else
			$this->dyn_bconf = false;

		// Init this controlpanel module
		$this->init();
	}

	function show_synonyms() {
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.synonyms');
			$this->response->add_extended_array($ais_words);
		} else {
			$this->dyn_bconf = false;
		}

		if (isset($_REQUEST['inserted_id'])) {
			$this->response->add_data('inserted_id',$_REQUEST['inserted_id'] );

		}
		$this->display_results('controlpanel/ais_synonyms.html');
	}

	function show_new_synonym() {
		$this->display_results('controlpanel/ais_new_synonym.html');
	}

	function show_misspellings() {
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			/* Update settings array */
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.misspellings');
			$this->response->add_extended_array($ais_words);
		} else {
			$this->dyn_bconf = false;
		}

		if (isset($_REQUEST['intended_id'])) {
			$this->response->add_data('intended_id',$_REQUEST['intended_id'] );

		}
		$this->display_results('controlpanel/ais_misspellings.html');
	}

	function show_new_intended() {
		$this->display_results('controlpanel/ais_new_intended.html');
	}

	/* Intended word and all related misspellings are deleted */
	function delete_intended_word() {
		if(isset($_REQUEST['intended_id'])) {
			/* Get misspelled bconf keys to delete */
			$bconf_phrase = "*.*.ais.misspellings.word.{$_REQUEST['intended_id']}.misspelled";
			$misspellings = bconf_get($this->dyn_bconf, $bconf_phrase);
			if (isset($misspellings)) {
				foreach ($misspellings as $key => $value ) {
					$bconf_key[] =  $key;
				}

				/* Update conf table */
				if (!save_dynamic_bconf(NULL, $bconf_phrase, $bconf_key)) {
					/* Could not save */
					$this->response->add_error('err_error', 'ERROR_AIS_MISSPELLED_DELETE_FAILED');
				} else if (!save_dynamic_bconf(NULL, "*.*.ais.misspellings.word.{$_REQUEST['intended_id']}.intended")) { 
					$this->response->add_error('err_error', 'ERROR_AIS_INTENDED_DELETE_FAILED');
				}
			}	

		} else {
			$this->response->add_error('err_intended_id', 'ERROR_AIS_MISSPELLED_DELETE_FAILED');
		}

		/* Update settings array */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.misspellings');
			$this->response->add_extended_array($ais_words);
		}
                if (!$this->response->has_error()) {
                        $this->response->add_data('word_saved', 'AIS_SAVE_INFO');
                } else {
                        $this->response->add_data('intended_id',$_REQUEST['intended_id']);
                }
		$this->display_results('controlpanel/ais_misspellings.html');
	}

	function save_misspellings() {
		$misspellings = trim($_REQUEST['misspellings']," \n\r");
		if(isset($_REQUEST['intended_id']) && isset($_REQUEST['misspellings']) && !empty($misspellings)) {
			/* Get misspelled bconf keys to delete */
			$bconf_phrase = "*.*.ais.misspellings.word.{$_REQUEST['intended_id']}.misspelled";
			$misspellings = bconf_get($this->dyn_bconf, $bconf_phrase);
			if (isset($misspellings)) {
				foreach ($misspellings as $key => $value ) {
					$bconf_key[] =  $key;
				}

				/* Update conf table */
				if (!save_dynamic_bconf(NULL, $bconf_phrase, $bconf_key)) {
					/* Could not save */
					$this->response->add_error('err_error', 'ERROR_AIS_MISSPELLED_DELETE_FAILED');
				} else {
					$misspellings = preg_split("/\r\n|\r|\n/", strtolower(trim($_POST['misspellings']," \n\r")), NULL, PREG_SPLIT_NO_EMPTY);
					if (!save_dynamic_bconf($misspellings, $bconf_phrase)) {
						$this->response->add_error('err_error', 'ERROR_AIS_MISSPELLED_DELETE_FAILED');
					}
				}
			}	

		} else {
			$this->response->add_error('err_error', 'ERROR_AIS_EMPTY_WORDS');
		}

		if ($this->response->has_error()) {
			$this->response->add_data('intended_id',$_REQUEST['intended_id']);
		} else {
			$this->response->add_data('word_saved', 'AIS_SAVE_INFO');
		}
		/* Update settings array */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.misspellings');
			$this->response->add_extended_array($ais_words);
		}

		$this->display_results('controlpanel/ais_misspellings.html');
	}

	function new_intended_word() {
		if(isset($_REQUEST['intended_word']) && isset($_REQUEST['misspellings']) 
			&& !empty($_REQUEST['intended_word']) && !empty ($_REQUEST['misspellings'])) {
			/* Get misspelled bconf keys */
			$misspellings = bconf_get($this->dyn_bconf, "*.*.ais.misspellings.word");
			if (isset($misspellings)) {
				/* Get the key of the last element */
				foreach ($misspellings as $key => $value);
			} else {
				$key = 0;
			}
			/* Update conf table */
			if (!save_dynamic_bconf(array("intended" => $_REQUEST['intended_word']), "*.*.ais.misspellings.word.".($key + 1))) {
				/* Could not save */
				$this->response->add_error('err_error', 'ERROR_AIS_INTENDED_SAVE_FAILED');
			} else {
				$bconf_phrase = "*.*.ais.misspellings.word.".($key + 1).".misspelled";
				$misspellings = preg_split("/\r\n|\r|\n/", strtolower(trim($_POST['misspellings']," \n\r")), NULL, PREG_SPLIT_NO_EMPTY);
				if (!save_dynamic_bconf($misspellings, $bconf_phrase)) {
					$this->response->add_error('err_error', 'ERROR_AIS_MISSPELLED_SAVE_FAILED');
				}
			}

		} else {
			$this->response->add_error('err_error', 'ERROR_AIS_EMPTY_WORDS');
		}

		if ($this->response->has_error()) {
			$this->display_results('controlpanel/ais_new_intended.html');
		} else {

			/* Update settings array */
			if ($this->dyn_bconf = get_dynamic_bconf()) {
				$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.misspellings');
				$this->response->add_extended_array($ais_words);
			}
			$this->response->add_data('word_saved', 'AIS_SAVE_INFO');
			$this->display_results('controlpanel/ais_misspellings.html');
		}
	}

	/* Inserted word and all related synonyms are deleted */
	function delete_synonym_word() {
		if(isset($_REQUEST['inserted_id'])) {
			/* Get synonym bconf keys to delete */
			$bconf_phrase = "*.*.ais.synonyms.word.{$_REQUEST['inserted_id']}.synonyms";
			$synonyms = bconf_get($this->dyn_bconf, $bconf_phrase);
			if (isset($synonyms)) {
				foreach ($synonyms as $key => $value ) {
					$bconf_key[] =  $key;
				}

				/* Update conf table */
				if (!save_dynamic_bconf(NULL, $bconf_phrase, $bconf_key)) {
					/* Could not save */
					$this->response->add_error('err_error', 'ERROR_AIS_SYNONYMS_DELETE_FAILED');
				} else if (!save_dynamic_bconf(NULL, "*.*.ais.synonyms.word.{$_REQUEST['inserted_id']}.word")) { 
					$this->response->add_error('err_error', 'ERROR_AIS_SYNONYMS_MAIN_DELETE_FAILED');
				}
			}	

		} else {
			$this->response->add_error('err_inserted_id', 'ERROR_AIS_SYNONYMS_DELETE_FAILED');
		}

		/* Update settings array */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.synonyms');
			$this->response->add_extended_array($ais_words);
		}
		if (!$this->response->has_error()) {
			$this->response->add_data('word_saved', 'AIS_SAVE_INFO');
		} else {
			$this->response->add_data('inserted_id',$_REQUEST['inserted_id']);
		}
		$this->display_results('controlpanel/ais_synonyms.html');
	}

	function save_synonyms() {
		$synonyms = trim($_REQUEST['synonyms']," \n\r");
		if(isset($_REQUEST['inserted_id']) && isset($_REQUEST['synonyms']) && !empty($synonyms)) {
			/* Get synonyms bconf keys to delete */
			$bconf_phrase = "*.*.ais.synonyms.word.{$_REQUEST['inserted_id']}.synonyms";
			$synonyms = bconf_get($this->dyn_bconf, $bconf_phrase);
			if (isset($synonyms)) {
				foreach ($synonyms as $key => $value ) {
					$bconf_key[] =  $key;
				}

				/* Update conf table */
				if (!save_dynamic_bconf(NULL, $bconf_phrase, $bconf_key)) {
					/* Could not save */
					$this->response->add_error('err_error', 'ERROR_AIS_SYNONYMS_DELETE_FAILED');
				} else {
					$synonyms = preg_split("/\r\n|\r|\n/", strtolower(trim($_POST['synonyms']," \n\r")), NULL, PREG_SPLIT_NO_EMPTY);
					if (!save_dynamic_bconf($synonyms, $bconf_phrase)) {
						$this->response->add_error('err_error', 'ERROR_AIS_SYNONYMS_DELETE_FAILED');
					}
				}
			}	

		} else {
			$this->response->add_error('err_error', 'ERROR_AIS_EMPTY_WORDS');
		}

		if ($this->response->has_error()) {
			$this->response->add_data('inserted_id',$_REQUEST['inserted_id']);
		}
		else {
			$this->response->add_data('word_saved', 'AIS_SAVE_INFO');
		}
		/* Update settings array */
		if ($this->dyn_bconf = get_dynamic_bconf()) {
			$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.synonyms');
			$this->response->add_extended_array($ais_words);
		}

		$this->display_results('controlpanel/ais_synonyms.html');
	}

	function new_synonym_word() {
		if(isset($_REQUEST['inserted_word']) && isset($_REQUEST['synonyms']) 
			&& !empty($_REQUEST['inserted_word']) && !empty ($_REQUEST['synonyms'])) {
			/* Get synonyms bconf keys */
			$synonyms = bconf_get($this->dyn_bconf, "*.*.ais.synonyms.word");
			if (isset($synonyms)) {
				/* Get the key of the last element */
				foreach ($synonyms as $key => $value);
			} else {
				$key = 0;
			}
			/* Update conf table */
			if (!save_dynamic_bconf(array("word" => $_REQUEST['inserted_word']), "*.*.ais.synonyms.word.".($key + 1))) {
				/* Could not save */
				$this->response->add_error('err_error', 'ERROR_AIS_INSERTED_SAVE_FAILED');
			} else {
				$bconf_phrase = "*.*.ais.synonyms.word.".($key + 1).".synonyms";
				$synonyms = preg_split("/\r\n|\r|\n/", strtolower(trim($_POST['synonyms']," \n\r")), NULL, PREG_SPLIT_NO_EMPTY);
				if (!save_dynamic_bconf($synonyms, $bconf_phrase)) {
					$this->response->add_error('err_error', 'ERROR_AIS_SYNONYMS_SAVE_FAILED');
				}
			}

		} else {
			$this->response->add_error('err_error', 'ERROR_AIS_EMPTY_WORDS');
		}

		if ($this->response->has_error()) {
			$this->display_results('controlpanel/ais_new_synonym.html');
		} else {

			/* Update settings array */
			if ($this->dyn_bconf = get_dynamic_bconf()) {
				$ais_words =  bconf_get($this->dyn_bconf, '*.*.ais.synonyms');
				$this->response->add_extended_array($ais_words);
			}
			$this->response->add_data('word_saved', 'AIS_SAVE_INFO');
			$this->display_results('controlpanel/ais_synonyms.html');
		}
	}

	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->display_results('controlpanel/ais_links.html');
	}
}
?>
