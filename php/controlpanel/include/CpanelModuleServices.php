<?php

namespace Yapo;

use RedisMultiproductManager;
use bTransaction;
use Yapo\Cpanel;

class CpanelModuleServices extends CpanelModule
{
    private $trans;
    private $redis;

    public function __construct($trans = null, $redis = null)
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Services'));

        $this->trans = isset($trans)? $trans : new bTransaction();
        $this->redis = isset($redis)? $redis : new RedisMultiproductManager();

        // Init this controlpanel module
        parent::init();
    }

    private function populateErrors($trans, $service, $ad)
    {
        foreach ($trans->get_errors() as $param => $code) {
            $this->response->add_data("err_$param", lang($code));
            if (!empty($service)) {
                $this->response->fill_array("error_{$ad}", lang($code));
                $this->response->add_data("servi_{$ad}", $service);
            }
        }
    }

    public function massiveassignment()
    {
        if ($this->verifyEmail()) {
            foreach ($_POST as $key => $value) {
                if (preg_match('/([A-Za-z]+)_(\d+)/', $key, $output_array)) {
                    $options = array();
                    $product_name = $output_array[1];
                    $ad_listid = $output_array[2];
                    switch ($product_name) {
                        case 'label':
                            $options = array(
                            'label_type' => $value
                            );
                            break;
                        case 'bump':
                            if ($value != 'bump') {
                                $product_name = str_replace("_bump", "", $value);
                            }
                            break;
                        case 'store':
                            if ($value != 0) {
                                $this->createStore($ad_listid, $value);
                            }
                            break;
                        default:
                            $product_name = $value;
                            break;
                    }
                    if ($product_name != "store") {
                        if ($this->cpApplyProduct($ad_listid, $product_name, $options) === true) {
                            $this->response->add_data("success_ads", "ok");
                        }
                    }
                }
            }
            $email = $_REQUEST['search_email'];
            $this->loadInfo($email);
        }

        //Search for pending products
        if (!empty($email)) {
            $products_pending = $this->redis->getAdsWithPendingProducts($email);
            if (!empty($products_pending)) {
                foreach ($products_pending as $prod => $values) {
                    foreach ($values as $list_id) {
                        $this->response->fill_array("pending_prod_".$prod, $list_id);
                    }
                }
            }
        }
        $this->displayResults('controlpanel/services/massiveassignment.html');
    }

    private function verifyEmail()
    {
        if (isset($_REQUEST['search_email'])) {
            $email_a = $_REQUEST['search_email'];
            $this->response->add_data("search_email", $email_a);
            $regex = Bconf::get($BCONF, "*.common.accounts.mobile.form_validation.email.regexp");
            if (empty($regex)) {
                $regex = "^[\w_+-]+(\.[\w_+-]+)*\.?@([\w_+-]+\.)+[\w]{2,4}$";
            }
            if (!preg_match("/{$regex}/", $email_a)) {
                $this->response->add_data("error_email", "Email no v�lido");
                return false;
            }

            $this->response->add_data("valid_email", "true");
            setcookie('massiveassignment_last_search', $email_a, time() + 60*60*24);
            return true;
        }
    }

    public function admin()
    {
        global $BCONF;
        if (isset($_REQUEST['ad_id']) && isset($_REQUEST['cmd'])) {
            $cmd = $_REQUEST['cmd'];
            $ad_id = $_REQUEST['ad_id'];
            $this->response->add_data('ad_id', $ad_id);
            $this->response->add_data('cmd', $cmd);
            $use_night = !empty($_REQUEST['use_night']) ? $_REQUEST['use_night'] : 0;
            $this->response->add_data('use_night', $use_night);

            $one_step_cmd = Bconf::get($BCONF, "controlpanel.services_one_step_cmd");
            if (!empty($one_step_cmd)) {
                $one_step_cmd = explode(',', $one_step_cmd);
            }
            $discount_combo_products = Bconf::get($BCONF, "controlpanel.services_discount_combo");
            if (!empty($discount_combo_products)) {
                $discount_combo_products = explode(',', $discount_combo_products);
            }
            $at_combo_products = Bconf::get($BCONF, "controlpanel.services_at_combo");
            if (!empty($at_combo_products)) {
                $at_combo_products = explode(',', $at_combo_products);
            }
            $requires_image = Bconf::get($BCONF, "controlpanel.services_require_img");
            if (!empty($requires_image)) {
                $requires_image = explode(',', $requires_image);
            }
            $apply_label = Bconf::get($BCONF, "controlpanel.services_apply_label");
            if (!empty($apply_label)) {
                $apply_label = explode(',', $apply_label);
            }
            $cmd_name = Bconf::get($BCONF, "controlpanel.services_list.{$cmd}.name");

            if (in_array($cmd, $one_step_cmd)) {
                if ($this->cpApplyProduct($ad_id, $cmd)) {
                    $this->response->add_data('product_applied', "{$cmd_name}");
                }
            } else {
                if ($cmd == "label" && isset($_REQUEST['label_type'])) {
                    $options['label_type'] = $_REQUEST['label_type'];
                    if ($this->cpApplyProduct($ad_id, $cmd, $options)) {
                        $label_name = lang($_REQUEST['label_type']);
                        $this->response->add_data('product_applied', "{$cmd_name} {$label_name}");
                    }
                }
                if ($cmd == "autobump" && isset($_REQUEST['frequency']) && isset($_REQUEST['num_days'])) {
                    $this->response->add_data('frequency', $_REQUEST['frequency']);
                    $this->response->add_data('num_days', $_REQUEST['num_days']);
                    if ($_REQUEST['apply'] == 'Aplicar') {
                        $options = array(
                            "frequency" => $_REQUEST['frequency'],
                            "num_days" => $_REQUEST['num_days'],
                            "use_night" => $use_night
                        );
                        if ($this->cpApplyProduct($ad_id, $cmd, $options)) {
                            $use_night_msj = "incluye horario nocturno";
                            if (empty($use_night)) {
                                $use_night_msj = "no {$use_night_msj}";
                            }
                            $msj = "{$cmd_name} Cada {$_REQUEST['frequency']}";
                            $msj .= " horas por {$_REQUEST['num_days']} d�as ({$use_night_msj})";
                            $this->response->add_data('product_applied', $msj);
                        }
                    } else {
                        $this->cpCalculateAutobump(
                            $ad_id,
                            $cmd,
                            $_REQUEST['frequency'],
                            $_REQUEST['num_days'],
                            $use_night
                        );
                    }
                } elseif ($cmd == "pack2if") {
                    $this->populateUserInfo($ad_id);
                    if (isset($_REQUEST['pack2if']) && $_REQUEST['pack2if'] == 1) {
                        $options = array(
                            "service_order" => sanitizeVar($_REQUEST['service_order'], "string"),
                            "service_amount" => sanitizeVar($_REQUEST['service_amount'], "int"),
                        );
                        if ($this->cpApplyProduct($ad_id, $cmd, $options)) {
                            $info_prod = "Inserting fee";
                            if (trim($options['service_order']) != "") {
                                $info_prod .= " nro de orden {$options['service_order']}";
                                $info_prod .= " y monto {$options['service_amount']}";
                            }
                            $this->response->add_data('product_applied', $info_prod);
                        }
                    }
                } elseif (in_array($cmd, $discount_combo_products)) {
                    $discount_combos_enabled = Bconf::get($BCONF, "*.combos_discount.enabled");
                    $discount_combos_enabled_msite = Bconf::get($BCONF, "*.combos_discount_mobile.enabled");
                    if ($discount_combos_enabled != 1 && $discount_combos_enabled_msite != 1) {
                        $this->response->add_data("err_error", lang("ERROR_DISCOUNT_COMBOS_DISABLED"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    $reply = $this->searchAd($ad_id);
                    if (!$reply || !isset($reply['ad'])) {
                        $this->response->add_data("err_ad_id", lang("ERROR_AD_ID_INVALID"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    $discount_combo_categories = Bconf::get($BCONF, "controlpanel.services_discount_combo_category");
                    $discount_combo_categories = explode(',', $discount_combo_categories);
                    //check if the ad is from the correct category for discount combos
                    if (!in_array($reply['ad'][0]['ad']['category'], $discount_combo_categories)) {
                        $this->response->add_data("err_error", lang("ERROR_DISCOUNT_COMBOS_INVALID_CATEGORY"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    //check if the user of the ad is a pro user
                    $trans = new bTransaction();
                    $trans->add_data('email', $reply['ad'][0]['users']['email']);
                    $account = $trans->send_command('get_account');
                    if (isset($account['is_pro_for'])) {
                        $is_pro_categories = explode(',', $account['is_pro_for']);
                        foreach($is_pro_categories as $is_pro_category) {
                            $discount_enable_pro_categories = Bconf::get($BCONF, "*.combos_discount.pro_categories.".$is_pro_category.".enabled");
                            if ($discount_enable_pro_categories != 1) {
                                $this->response->add_data("err_error", lang("ERROR_DISCOUNT_COMBOS_PRO_USER"));
                                $this->displayResults('controlpanel/services/search.html');
                                return;
                            }
                        }
                    }
                    // check if the ad has an image
                    if (in_array($cmd, $requires_image)
                        && (!isset($reply['ad'][0]['images'])
                        || sizeof($reply['ad'][0]['images']) == 0)
                    ) {
                        $this->response->add_data("err_error", lang("ERROR_NO_IMAGE"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    // check if the combo selected applies a label
                    if (in_array($cmd, $apply_label)) {
                        // check if the ad already has a label
                        if (isset($reply['ad'][0]['params']['label'])) {
                            $this->response->add_data("err_error", lang("ERROR_ALREADY_HAS_LABEL"));
                            $this->displayResults('controlpanel/services/search.html');
                            return;
                        }
                        //check if the label was selected
                        if (isset($_REQUEST['label_type'])) {
                            $options['label_type'] = $_REQUEST['label_type'];
                            $this->cpApplyComboProduct($ad_id, $cmd, $options);
                        }
                    } else {
                        $this->cpApplyComboProduct($ad_id, $cmd);
                    }
                } elseif (in_array($cmd, $at_combo_products)) {
                    $at_combos_enabled = Bconf::get($BCONF, "*.combos_at.enabled");
                    if ($at_combos_enabled != 1) {
                        $this->response->add_data("err_error", lang("ERROR_AT_COMBOS_DISABLED"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    $reply = $this->searchAd($ad_id);
                    if (!$reply || !isset($reply['ad'])) {
                        $this->response->add_data("err_ad_id", lang("ERROR_AD_ID_INVALID"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    $at_combo_categories = Bconf::get($BCONF, "controlpanel.services_at_combo_category");
                    $at_combo_categories = explode(',', $at_combo_categories);
                    //check if the ad is from the correct category for at combos
                    if (!in_array($reply['ad'][0]['ad']['category'], $at_combo_categories)) {
                        $this->response->add_data("err_error", lang("ERROR_AT_COMBOS_INVALID_CATEGORY"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    // check if the ad has an image
                    if (in_array($cmd, $requires_image)
                        && (!isset($reply['ad'][0]['images'])
                        || sizeof($reply['ad'][0]['images']) == 0)
                    ) {
                        $this->response->add_data("err_error", lang("ERROR_NO_IMAGE"));
                        $this->displayResults('controlpanel/services/search.html');
                        return;
                    }
                    // check if the combo selected applies a label
                    if (in_array($cmd, $apply_label)) {
                        // check if the ad already has a label
                        if (isset($reply['ad'][0]['params']['label'])) {
                            $this->response->add_data("err_error", lang("ERROR_ALREADY_HAS_LABEL"));
                            $this->displayResults('controlpanel/services/search.html');
                            return;
                        }
                        //check if the label was selected
                        if (isset($_REQUEST['label_type'])) {
                            $options['label_type'] = $_REQUEST['label_type'];
                            $this->cpApplyComboProduct($ad_id, $cmd, $options);
                        }
                    } else {
                        $this->cpApplyComboProduct($ad_id, $cmd);
                    }
                } elseif (in_array($cmd, array("pack_release", "pack_check"))) {
                    $new_status = "active";
                    if ($cmd == "pack_release") {
                        $new_status = "disabled";
                    }

                    if ($this->cpTogglePackSlot($ad_id, $new_status)) {
                        $this->response->add_data(
                            'product_applied',
                            "Nuevo estado del aviso: {$new_status}"
                        );
                    }
                }
            }
        }
        $this->displayResults('controlpanel/services/search.html');
    }

    public function links()
    {
        $this->displayResults('controlpanel/services/links.html');
    }

    private function cpTogglePackSlot($ad_id, $new_status)
    {

        if (!Cpanel::hasPriv("Services.packstatus")) {
            echo lang('ERROR_NO_PERMISSION');
            return 'FINISH';
        }

        $transaction = $this->trans->reset();
        $transaction->add_client_info();
        $transaction->auth_type = 'admin';
        $transaction->add_data('ad_id', $ad_id);
        $transaction->add_data('new_status', $new_status);
        $reply = $transaction->send_admin_command("pack_change_ad_status");
        if ($transaction->has_error()) {
            $this->populateErrors($transaction);
            return false;
        }
        return true;
    }

    private function cpApplyProduct($ad_id, $product, $options = array())
    {
        global $session;
        global $BCONF;

        $defaults = array(
            'label_type' => null,
            'frequency' => null,
            'num_days' => null,
            'use_night' => 0
        );
        $options = array_merge($defaults, $options);

        $code_prod = 0;
        $code_uprod = 0;
        $transaction = $this->trans->reset();
        if (!Cpanel::checkLogin()
            || !Cpanel::hasPriv("Services.admin")
            || ($product == "pack2if" && !Cpanel::hasPriv("Services.packtoif"))
        ) {
            if (!Cpanel::hasPriv("Services.massiveassignment")) {
                echo lang('ERROR_NO_PERMISSION');
                return 'FINISH';
            }
        }
        // if the product exists, get the product code
        $prod_codes = Bconf::get($BCONF, "*.payment.product_code.".$product);
        if (!empty($prod_codes)) {
            $prod_codes= explode(',', $prod_codes);
            $code_prod = $prod_codes[0];
            if (sizeof($prod_codes) > 1) {
                $code_uprod = $prod_codes[1];
            }
            $command = Bconf::get($BCONF, "*.upselling_settings.command.".$code_prod.".cmd");
            //load the default params
            $product_config = Bconf::get($BCONF, "*.upselling_settings.command.{$code_prod}");
            if (isset($product_config["default_params"])) {
                $def_params = $product_config["default_params"];
                foreach ($def_params as $def_param) {
                    $def_param_values=explode(':', $def_param["value"]);
                    if ($def_param_values[0] != "commit" && $def_param_values[0] != "batch") {
                        $transaction->add_data($def_param_values[0], $def_param_values[1]);
                    }
                }
            }
            // load the parameters given by the user
            switch ($product) {
                case 'label':
                    $transaction->add_data('label_type', $options['label_type']);
                    break;
                case 'autobump':
                    $transaction->add_data('frequency', $options['frequency']);
                    $transaction->add_data('num_days', $options['num_days']);
                    $transaction->add_data('use_night', $options['use_night']);
                    break;
                case 'pack2if':
                    if (!empty($options['service_order'])) {
                        $transaction->add_data('service_order', $options['service_order']);
                    }
                    if (!empty($options['service_amount'])) {
                        $transaction->add_data('service_amount', $options['service_amount']);
                    }
                    break;
                default:
                    break;
            }
        } else {
            $command = 'bump_ad';
            $code_prod = 1;
        }

        $transaction->add_client_info();
        $transaction->auth_type = 'admin';
        $transaction->add_data('ad_id', $ad_id);
        $reply = $transaction->send_admin_command($command);
        if ($transaction->has_error()) {
            $this->populateErrors($transaction, $command, $ad_id);
            return false;
        } else {
            if ($code_prod != 0) {
                $load_reply = $this->getAdInfo($ad_id);
                if ($load_reply) {
                    // Clean the product from the user cart
                    if ($code_uprod != 0) {
                        $this->redis->unselect($load_reply['email'], $code_uprod, $ad_id, true);
                    }
                    $this->redis->addPendingProducts($load_reply['email'], $code_prod, $ad_id);
                    $this->redis->unselect($load_reply['email'], $code_prod, $load_reply['list_id'], true);
                    $this->response->add_data("success_ad_{$ad_id}", $load_reply["subject"]);
                    $this->response->fill_array("success_serv_{$ad_id}", $code_prod);
                }
            }
        }
        return true;
    }

    private function createStore($account_id, $product_id)
    {
        global $BCONF;
        if (isset($account_id)  && isset($product_id)) {
            $trans = $this->trans->reset();
            $trans->add_data('account_id', $account_id);
            $response = $trans->send_admin_command('create_store');

            if ($trans->has_error(true)) {
                $this->response->add_data("store_message", "Error al crear la tienda");
                syslog(LOG_ERR, log_string()."ERROR: Creating store failed");
                foreach ($trans->get_errors() as $param => $code) {
                    $this->response->add_data("err_$param", lang($code));
                }
            } else {
                $days = $this->applyStoreProduct($response["store_id"], $product_id);

                if ($days != null) {
                    $this->response->add_data(
                        "store_message",
                        "�xito:Tienda creada exitosamente por un per�odo de $days"
                    );
                    // put the store info on redis session
                    $trans = $this->trans->reset();
                } else {
                    $this->response->add_data("store_message", "Error al aplicar producto de tienda.");
                }
            }

            $email = $_REQUEST['search_email'];
            $this->loadInfo($email);
        }
        $this->displayResults('controlpanel/services/massiveassignment.html');
    }

    private function applyStoreProduct($store_id, $product_id)
    {
        global $BCONF;
        $transaction = $this->trans->reset();
        $expire_time = Bconf::get($BCONF, "*.payment.product.". $product_id . ".expire_time");
        $transaction->add_data("store_id", $store_id);
        $transaction->add_data("expire_time", $expire_time);
        $transaction->send_admin_command('apply_product_to_store');

        if ($transaction->has_error(true)) {
            syslog(LOG_ERR, log_string()."ERROR: extend store failed \n".var_export($transaction->get_errors(), true));
            return null;
        }
        return $expire_time;
    }

    private function getAdInfo($ad_id)
    {
        return $this->getTransData(array("ad_id" => $ad_id), "get_ad");
    }

    private function searchAd($ad_id)
    {
        return $this->getTransData(array("search_type" => "ad_id", "ad_id" => $ad_id), "search_ads", true);
    }

    private function getAccountInfo($user_id)
    {
        return $this->getTransData(array("user_id" => $user_id), "get_account");
    }

    private function getTransData($data, $cmd, $admin = false)
    {
        $trans_load = $this->trans->reset();
        foreach ($data as $key => $value) {
            $trans_load->add_data($key, $value);
        }
        if ($admin) {
            $load_reply = $trans_load->send_admin_command($cmd);
        } else {
            $load_reply = $trans_load->send_command($cmd);
        }
        if ($trans_load->has_error(true)) {
            return false;
        }
        return $load_reply;
    }

    private function cpCalculateAutobump($ad_id, $cmd, $frequency, $num_days, $use_night)
    {
        $command = 'price_autobump';
        $transaction = $this->trans->reset();
        $transaction->add_data('frequency', $frequency);
        $transaction->add_data('num_days', $num_days);
        $transaction->add_data('use_night', $use_night);
        $transaction->auth_type = 'admin';
        $transaction->add_data('ad_id', $ad_id);
        $reply = $transaction->send_command($command);
        if (!$transaction->has_error()) {
            $this->response->add_data('total_bumps', $reply['total_bumps']);
            $this->response->add_data('total_price', $reply['total_price']);
            $this->response->add_data('unit_price', $reply['unit_price']);
            $this->response->add_data('ad_category', $reply['ad_category']);
            $this->response->add_data('date_start', $reply['date_start']);
            $this->response->add_data('date_end', $reply['date_end']);

            for ($i = 0; $i < intval($reply['total_bumps']); $i++) {
                $this->response->fill_array('executions', $reply["execution_{$i}"]);
            }
        } else {
            $this->populateErrors($transaction);
        }
    }

    private function cpApplyComboProduct($ad_id, $product, $options = array())
    {
        global $session;
        global $BCONF;
        $code_prod = 0;
        $code_uprod = 0;
        // if the product exists, get the product code
        $prod_codes = Bconf::get($BCONF, "*.payment.product_code.".$product);
        if (!empty($prod_codes)) {
            $prod_codes= explode(',', $prod_codes);
            $code_prod = $prod_codes[0];
            if (sizeof($prod_codes) > 1) {
                $code_uprod = $prod_codes[1];
            }
        } else {
            $command = 'bump_ad';
            $code_prod = 9;
        }
        $product_conf = Bconf::get($BCONF, "*.upselling_settings.product.{$code_prod}");
        $product_desc = Bconf::get($BCONF, "*.payment.product.{$code_prod}.long_description");
        $product_count = sizeof($product);
        if (!$product_conf) {
            return false;
        }
        foreach ($product_conf as $sub_product) {
            $transaction = $this->trans->reset();
            $sub_product_id = $sub_product["product_id"];
            $command = Bconf::get($BCONF, "*.upselling_settings.command.{$sub_product_id}");
            //if the sub_product is label, add the parameter
            if ($command["cmd"] == "add_label") {
                $transaction->add_data('label_type', $options['label_type']);
            }
            $def_params = $command["default_params"];
            $given_params = $command["provided_params"];
            foreach ($def_params as $def_param) {
                $def_param_values=explode(':', $def_param["value"]);
                if ($def_param_values[0] != "commit" && $def_param_values[0] != "batch") {
                    $transaction->add_data($def_param_values[0], $def_param_values[1]);
                }
            }
            foreach ($given_params as $given_param) {
                //get the provided param from the combo conf
                if (isset($sub_product[$given_param["value"]]) && $sub_product[$given_param["value"]] != "") {
                    $transaction->add_data($given_param["value"], $sub_product[$given_param["value"]]);
                }
            }
            $transaction->add_client_info();
            $transaction->auth_type = 'admin';
            $transaction->add_data('ad_id', $ad_id);
            $reply = $transaction->send_admin_command($command["cmd"]);
            if ($transaction->has_error()) {
                $this->populateErrors($transaction);
                return false;
            } else {
                if ($code_prod != 0) {
                    $load_reply = $this->getAdInfo($ad_id);
                    if ($load_reply) {
                        // Clean the product from the user cart
                        if ($code_uprod != 0) {
                            $this->redis->unselect($load_reply['email'], $code_uprod, $ad_id, true);
                        }
                        $this->redis->unselect($load_reply['email'], $code_prod, $load_reply['list_id'], true);
                    }
                }
            }
        }
        $response = $product_desc;
        if ($product == 'at_combo3') {
            $response =$response. ' '.lang($_REQUEST['label_type']);
        }
        $this->response->add_data('product_applied', " {$response}");
    }

    private function populateUserInfo($ad_id)
    {
        $ad_info = $this->getAdInfo($ad_id);
        if ($ad_info) {
            $this->response->add_data("ad_subject", $ad_info['subject']);
            $this->response->add_data("ad_category", $ad_info['category']);
            $this->response->add_data("ad_status", $ad_info['ad_status']);
            $account_info = $this->getAccountInfo($ad_info['user_id']);
            if ($account_info) {
                $this->response->add_data("account_name", $account_info['name']);
                $this->response->add_data("account_email", $account_info['email']);
                $this->response->add_data(
                    "pack_status",
                    isset($account_info['pack_status']) ? $account_info['pack_status'] : "Sin pack"
                );
                $this->response->add_data(
                    "pack_slots",
                    isset($account_info['pack_slots']) ? $account_info['pack_slots'] : "0"
                );
            }
        }
    }

    public function main($function = null)
    {
        $this->response->add_data("priv_services", Cpanel::hasPriv("Services.admin")? "1": "0");
        $this->response->add_data("priv_massiveassignment", Cpanel::hasPriv("Services.massiveassignment")? "1": "0");
        $this->response->add_data("priv_pack2if", Cpanel::hasPriv("Services.packtoif")? "1": "0");
        $this->response->add_data("priv_packstatus", Cpanel::hasPriv("Services.packstatus")? "1": "0");

        if ($function != null) {
            $this->response->add_data("page_name", "Asignar servicio");
            $this->response->fill_array('page_js', '/js/Services.js');
            $this->response->add_data('page_name_hide', '1');
            $this->runMethod($function);
        } else {
            $this->links();
        }
    }

    private function loadInfo($email)
    {
        $this->response->add_data("search_email", "$email");
        $trans = $this->trans->reset();
        $trans->add_data('email', $email);
        $acc_info = $trans->send_command('get_account');

        if ($acc_info['status'] != "TRANS_OK") {
            $this->response->add_data('store_message', lang("CONTROLPANEL_SERVICES_STORE_ACC_NOT_FOUND"));
            syslog(LOG_WARNING, log_string()."failed to get account for: $email");
        } else {
            $this->response->add_data("account_found", "true");
            foreach ($acc_info as $key => $value) {
                $this->response->add_data("$key", "$value");
            }

            if ($acc_info["account_status"] == "inactive") {
                $this->response->add_data('store_message', lang("CONTROLPANEL_SERVICES_STORE_ACC_INACTIVE"));
            } elseif ($acc_info["account_status"] == "pending_confirmation") {
                $this->response->add_data('store_message', lang("CONTROLPANEL_SERVICES_STORE_ACC_PENDING"));
            } else {
                if (isset($acc_info["store_status"]) && $acc_info["store_status"] == "admin_deactivated") {
                    $this->response->add_data('store_message', lang("CONTROLPANEL_SERVICES_STORE_DEACTIVATED"));
                } else {
                    if (empty($acc_info["store_id"])) {
                        $this->response->add_data('store_message', lang("CONTROLPANEL_SERVICES_STORE_NOT_FOUND"));
                    } else {
                        $trans_store = $this->trans->reset();
                        $trans_store->add_data('store_id', $acc_info['store_id']);
                        $reply_store = $trans_store->send_command('load_store');

                        if ($trans_store->has_error(true)) {
                            $this->response->add_data(
                                'store_message',
                                lang("CONTROLPANEL_SERVICES_STORE_LOADING_ERROR")
                            );
                        } else {
                            $this->response->add_data('store_show_info', '1');
                            $this->response->add_data('store_info_status', $reply_store["store_status"]);
                            $this->response->add_data('store_info_name', $reply_store["name"]);
                            $this->response->add_data('store_info_date_end', $reply_store["date_end"]);
                            $this->response->add_data('store_info_type', $reply_store["store_type"]);
                        }
                    }
                }
            }
        }
    }
}
