<?php

namespace Yapo;

class CpanelModulePPages extends CpanelModule
{
    public function __construct()
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.PPages'));
        // Init this controlpanel module
        parent::init();
    }

    public function main($function = null)
    {
        $this->response->fill_array('page_js', '/js/image_upload.js');
        $this->response->fill_array('page_js', '/js/PPages.js');
        $this->response->fill_array('page_js', '/js/moment.min.js');
        $this->response->fill_array('page_js', '/js/pikaday.js');
        $this->response->fill_array('page_css', '/css/pikaday.css');
        if (!is_null($function)) {
            $this->runMethod($function);
        } else {
            $this->displayResults('controlpanel/ppages/links.html');
        }
    }

    public function publishPage()
    {
        if (isset($_GET['pp_id'])) {
            $pp_id = sanitizeVar($_GET['pp_id'], 'int');
            $status = Ppage::publish($pp_id);
            if ($status) {
                $this->redirect('listPages', array('m' => 'PPages', 'st' => 'success'));
            } else {
                $this->redirect('editPage', array('m' => 'PPages', 'pp_id' => $pp_id));
            }
        }
        $this->redirect('listPages', array('m' => 'PPages'));
    }

    public function fillResponse(Ppage $ppage)
    {
        foreach ($ppage as $k => $v) {
            if (!empty($v)) {
                $this->response->add_data($k, $v);
            }
        }
    }

    public function displayList()
    {
        $error = $ppages = Ppage::fromTrans();
        if ($ppages instanceof Ppage) {
            if (empty($ppages->pp_id)) {
                $this->response->add_data("pp_list_msg", lang('PP_LIST_EMPTY'));
            } else {
                $this->fillResponse($ppages);
            }
        } else {
            $this->response->add_data("pp_list_msg", lang($error));
        }
        $this->displayResults('controlpanel/ppages/listPages.html');
    }

    public function listPages()
    {
        $this->response->add_data("page_name", lang('PP_LIST_TITLE'));
        $this->displayList();

        if (isset($_GET['st']) && $_GET['st'] == 'success') {
            $this->response->add_data('pp_list_msg', lang('PP_SUCCESS'));
            return ;
        }

        if (isset($_POST['delete'])) {
            if (!isset($_POST['pp_id'])) {
                $this->response->add_data('pp_list_msg', lang('PP_ERROR'));
                return ;
            }

            $pp_id = sanitizeVar($_POST['pp_id'], 'int');
            $type = $_POST['delete'] == 'draft' ? 'draft' : 'all';
            if (!$this->deletePage($pp_id, $type)) {
                $this->response->add_data('pp_list_msg', lang('PP_ERROR'));
            }

            $this->response->add_data('pp_list_msg', lang('PP_DELETE_SUCCESS'));
            $this->redirect('listPages', array('m' => 'PPages'));
        }
    }

    public function deletePage($pp_id, $type = 'all')
    {
        $deleteResult = Ppage::delete($pp_id, $type);
        $this->displayList();
        return $deleteResult;
    }

    public function editPage()
    {
        if (isset($_POST['delete']) && $_POST['delete'] == 'draft') {
            /* Deletion is handled on the list page */
            $this->listPages();
        }

        $this->response->add_data("page_name", lang('PP_LIST_TITLE'));
        $this->response->add_data("current_date", date('Y-m-d'));

        if (isset($_POST['submit'])) {
            $this->adminPpages();
        } elseif (!empty($_GET['pp_id'])) {
            $pp_id = sanitizeVar($_GET['pp_id'], 'int');
            $err = $ppage = Ppage::fromTrans($pp_id);
            if ($ppage instanceof Ppage) {
                $this->response->add_data("pp_id", $pp_id);
                $this->response->add_data("search_query", array_map("html_entity_decode", $ppage->search_query[$pp_id]));
                $this->response->add_data("category", $ppage->category[$pp_id]);
                $this->response->add_data("title", $ppage->title[0]);
                $this->response->add_data("image", $ppage->image[0]);
                $this->response->add_data("start_date", $ppage->start_date[0]);
                $this->response->add_data("end_date", @$ppage->end_date[0]);
                $this->response->add_data("is_draft", @$ppage->is_draft[0]);
                $this->response->add_data("feature", @$ppage->feature[0]);
            } else {
                $this->response->add_data('error', $err);
            }
        }
        $this->displayResults('controlpanel/ppages/adminPpages.html');
    }

    private function adminPpages()
    {
        global $BCONF;
        $ppage = Ppage::fromForm($_POST);
        if ($ppage instanceof Ppage) {
            $success = $ppage->save();
            if ($success === true) {
                $promo_path = Bconf::get($BCONF, '*.ppages.path');
                header("Location: /{$promo_path}/draft:{$ppage->url}");
            } else {
                $this->fillResponse($ppage);
                $this->response->add_data('status_message', lang('PP_ERROR'));
                $this->response->add_data('error', lang($success));
            }
        } else {
            $this->response->add_data('status_message', lang('PP_ERROR'));
            $this->response->add_data('error', lang($ppage));
        }
    }

    public function ppageInfo()
    {
        global $BCONF;
        $pp_id = 0;
        if (isset($_GET['pp_id'])) {
            $pp_id = sanitizeVar($_GET['pp_id'], 'int');
        }
        $ppages = Ppage::fromTrans($pp_id);
        if ($ppages instanceof Ppage) {
            if (empty($ppages->pp_id) || !isset($ppages->category[$pp_id])) {
                $response = "{}";
            } else {
                $categories = $ppages->category[$pp_id];
                $response_array = array();

                foreach ($categories as $v) {
                    $search_name = Bconf::get($BCONF, "*.cat.$v.search_name");
                    $cat_name = Bconf::get($BCONF, "*.cat.$v.name");
                    $response_array['categories'][$v] = !empty($search_name) ? $search_name : $cat_name;
                }
                $response = yapo_json_encode($response_array);
            }
        } else {
            $response = "{}";
        }
        header("Content-Type: application/json");
        echo $response;
        exit();
    }

    public function deleteBanner()
    {
        $args = array('m' => 'PPages');
        if (isset($_POST['banner_id'])) {
            $banner_id = sanitizeVar($_POST['banner_id'], 'string');
            $success = PpageBanner::delete($banner_id);
            $args['st'] = $success ? "DELETE_BANNER_OK" : "DELETE_BANNER_ERROR";
        }
        $this->redirect('ppageBanners', $args);
    }

    public function ppageBanners()
    {
        $this->response->fill_array('page_js', '/js/ppages_banners.js');

        if (isset($_GET['st'])) {
            $this->response->add_data("response", lang($_GET['st']));
        }

        if (isset($_GET["create"])) {
            $ppBanner = PpageBanner::fromForm($_GET);
            if ($ppBanner instanceof PpageBanner) {
                $result = $ppBanner->save();
                if ($result === true) {
                    $this->redirect('ppageBanners', array('m' => 'PPages', 'st' => 'SET_BANNER_OK'));
                } else {
                    $this->response->add_data("response", lang($result));
                }
            } else {
                $this->response->add_data("response", lang($ppBanner));
            }
        }

        $banners = PpageBanner::fromTrans();
        if ($banners instanceof PpageBanner) {
            if (!empty($banners->key)) {
                $this->response->add_data("banners_pp_id", $banners->pp_id);
                $this->response->add_data("banners_url", $banners->url);
                $this->response->add_data("banners_key", $banners->key);
                $this->response->add_data("banners_banner_id", $banners->banner_id);
                $this->response->add_data("banners_platform", $banners->platform);
                $this->response->add_data("banners_position", $banners->position);
                $this->response->add_data("banners_regions", $banners->regions);
                $this->response->add_data("banners_categories", $banners->categories);
                $this->response->add_data("banners_start_date", $banners->start_date);
                $this->response->add_data("banners_end_date", $banners->end_date);
                $this->response->add_data("banners_is_draft", $banners->is_draft);
            }
        } else {
            $this->response->add_data("pp_list_msg", lang($banners));
        }

        $ppages = Ppage::fromTrans();
        if ($ppages instanceof Ppage) {
            if (empty($ppages->pp_id)) {
                $this->response->add_data("pp_list_msg", lang('PP_LIST_EMPTY'));
            } else {
                $this->response->add_data("pp_id", $ppages->pp_id);
                $this->response->add_data("url", $ppages->url);
                $this->response->add_data("title", $ppages->title);
                $this->response->add_data("start_date", $ppages->start_date);
                $this->response->add_data("end_date", $ppages->end_date);
                foreach ($ppages->category as $k => $c) {
                    $this->response->add_data("category_$k", $c);
                }
            }
        } else {
            $this->response->add_data("pp_list_msg", lang($ppages));
        }
        if (isset($_GET['st'])) {
            $st = sanitizeVar($_GET['st'], 'string');
            $this->response->add_data("response", lang($st));
        }
        $this->displayResults('controlpanel/ppages/ppageBanners.html');
    }

    public function uploadImg()
    {
        if (isset($_FILES['img']) && isset($_POST['arg'])) {
            $arg = sanitizeVar($_POST['arg'], 'string');
            $ppImage = new \Yapo\PpImage('img', $arg);
            $ppImage->save();
            exit();
        }
    }

    public function editBanner()
    {
        if (!empty($_GET['key'])) {
            $key = sanitizeVar($_GET['key'], 'string');
            $err = $banner = PpageBanner::fromTrans($key);
            if ($banner instanceof PpageBanner) {
                $this->response->add_data("banner_pp_id", $banner->pp_id[0]);
                $this->response->add_data("banner_url", $banner->url[0]);
                $this->response->add_data("banner_categories", $banner->categories[0]);
                $this->response->add_data("banner_regions", $banner->regions[0]);
                $this->response->add_data("banner_key", $key);
                $this->response->add_data("banner_position", $banner->position[0]);
                $this->response->add_data("banner_platform", $banner->platform[0]);
                $this->response->add_data("banner_image", $banner->image[0]);
                $this->response->add_data("banner_start_date", $banner->start_date[0]);
                $this->response->add_data("banner_end_date", $banner->end_date[0]);
                $this->response->add_data("banner_is_draft", $banner->is_draft[0]);
            } else {
                $this->response->add_data('error', $err);
            }
        }
        $this->ppageBanners();
    }
}
