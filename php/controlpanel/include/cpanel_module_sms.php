<?php
require_once('cpanel_module.php');

use Yapo\Cpanel;

class cpanel_module_sms extends cpanel_module {

	function cpanel_module_sms() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.sms');

		// Init this controlpanel module
		$this->init();
	}

	function search_ajax() {
		$result = array();

		switch ($_REQUEST['action']) {
			case 'delete':
				$transaction = new bTransaction();
				$transaction->add_data('watch_unique_id', $_REQUEST['unique_id']); 
				$transaction->add_data('watch_query_id', $_REQUEST['query_id']);	
				$reply = $transaction->send_admin_command('adwatch_sms_stop');
				if ($transaction->has_error()) {
					$result['message'] = join(', ', $transaction->get_errors());
					$result['result'] = 2;
				} else {
					$result['message'] = lang('SMS_WATCH_QUERY_DELETED');
					$result['result'] = 1;
				}
				break;

		}

		print json_encode($result);
		exit;
	}

	function search() {
		$phone = get_input("phone", "[0-9 -]+");

		// Massage phone for better matching.
		$phone = preg_replace('/^[0]+/', '', $phone);
		$phone = preg_replace('/[ -]/', '', $phone);

		$this->response->add_data('phone', $phone);

		if (isset($_REQUEST['search'])) {
			$trans = new bTransaction();

			$trans->add_data('phone', $phone);
			$res = $trans->send_admin_command("search_sms_watches");

			if ($trans->has_error()) {
				foreach ($trans->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} else {
				$this->response->add_extended_array('adwatch_colors', bconf_get($GLOBALS['BCONF'], 'adwatch.colors'));
				$this->response->add_extended_array($res);
			}
		} 
		$this->display_results('controlpanel/sms/search.html');
	}

	function log_ajax() {
		$result = array();

		$transaction = new bTransaction();
		$transaction->add_data('sender', get_input('sender', '[0-9]+'));
		$transaction->add_data('sms_user_status', get_input('new_status', 'adminpaused|active'));
		$reply = $transaction->send_admin_command('adwatch_sms_pause');
		if ($transaction->has_error()) {
			$result['message'] = join(', ', $transaction->get_errors());
			$result['result'] = 2;
		} else {
			$result['result'] = 1;
			$result['sms_user_status'] = $reply['sms_user_status'];
		}

		print json_encode($result);
		exit;
	}


	function sms_log() {
		$phone = get_input("phone", "[0-9 -]+");
		$watch_query = get_input("watch_query", "[0-9]*");
		$data = array();

		if(isset($phone)) {
			$trans = new bTransaction();

			$trans->add_data('phone', $phone);
			if ($watch_query != 0) {
				$trans->add_data('watch_query_id', $watch_query);
				$this->response->add_data('watch_query_id', $watch_query);
			}

			$res = $trans->send_admin_command("search_sms_log");

			if ($trans->has_error()) {
				foreach ($trans->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} else {
				$trans = new bTransaction();
				$trans->add_data('phone', $phone);
				$user_info = $trans->send_admin_command("get_sms_user_info");
				$this->response->add_extended_array($user_info);
				$this->response->add_extended_array($res);
				$this->response->add_data("amount_spent_limit", bconf_get($GLOBALS['BCONF'], 'unwire_sms.adwatch.amount_spent_limit'));
				if (Cpanel::hasPriv('sms.credit')) {
					$this->response->add_data("has_sms_credit_priv", "1");
				}
			}
		} 
		$this->display_results('controlpanel/sms/sms_log.html');
	}

	function credit() {
		$phone = get_input("phone", "[0-9 -]+");

		// Massage phone for better matching.
		$phone = preg_replace('/^[0]+/', '', $phone);
		$phone = preg_replace('/[ -]/', '', $phone);
		$this->response->add_data('phone', $phone);


		if (isset($_REQUEST['increase_credit'])) {
			$credit = get_input("credit", "[0-9 -]+");
			syslog(LOG_DEBUG, log_string()."CREDIT $credit");
			$trans = new bTransaction();
			$trans->add_data("phone", $phone);
			$trans->add_data("free_sms_delta", $credit);
			$trans->send_admin_command('update_sms_user');
			if ($trans->has_error()) {
				foreach ($trans->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} 
		}
		
		if (isset($_REQUEST['search']) || isset($_REQUEST['increase_credit'])) {
			$trans = new bTransaction();
			$trans->add_data('phone', $phone);
			$res = $trans->send_admin_command("get_sms_user_info");
			if ($trans->has_error()) {
				foreach ($trans->get_errors() as $param => $code) {
					$this->response->add_data("err_$param", lang($code));
				}
			} else {
				$this->response->add_extended_array($res);
			}
		}
		$this->display_results('controlpanel/sms/credit_search_form.html');
	}
	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else {
			header('Location: /controlpanel?m=sms&a=search');
		}	
	}
}
?>
