<?php
require_once('cpanel_module.php');

use Yapo\Cpanel;

class cpanel_module_adminad extends cpanel_module {
	function cpanel_module_adminad() {
		// Get configuration for easy access
		$this->config =& bconf_get($GLOBALS['BCONF'], 'controlpanel.modules.adminad');

		// Init this controlpanel module
		$this->init();
	}
	
	/*
	 * MENU: Clear an ad
	 * 
	 * Makes a request to clear an ad.
	 * Set the ad to paid.
	 */
	function clear_ad() {
		if (isset($_REQUEST['display'])) {
			$this->get_clearad();
			return;	
		}
		
		$this->display_results('controlpanel/clearad.html');
	}
	
	function get_clearad() {
		$transaction = new bTransaction();
	
		if (isset($_REQUEST['id']) && isset($_REQUEST['action_id'])) {
			$transaction->add_data('ad_id', $_REQUEST['id']);
			$transaction->add_data('action_id', $_REQUEST['action_id']);
		} else if (isset($_REQUEST['paycode']))
			$transaction->add_data('pay_code', $_REQUEST['paycode']);
				
		$transaction->add_data('admin_clear', 1);

		$reply = $transaction->send_admin_command('clear');

		Cpanel::handleReply($reply, $this->response);

		if ($this->response->has_error()) {
			$this->response->add_data("error_message", lang($this->response->get_error()));
		} else
			$this->response->add_data("ok_message", lang("AD_CLEARED_OK"));
		
		$this->display_results('controlpanel/clearad_display.html');
	}
	

	/*
	 * MENU: Show Ad
	 * 
	 * Edit or delete ad as admin.
	 */
	function edit_ad($function = NULL) {
		$this->display_results('controlpanel/adminedit.html');
	}


	function main($function = NULL) {
		if ($function != NULL)
			$this->run_method($function);
		else
			$this->display_results('controlpanel/clearad_main.html');
	}
}
?>
