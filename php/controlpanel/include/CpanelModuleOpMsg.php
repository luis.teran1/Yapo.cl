<?php

namespace Yapo;

use Yapo\Bconf;
use Yapo\Cpanel;

class CpanelModuleOpMsg extends CpanelModule
{

    private $expirationDate;
    private $expirationTime;
    private $soruce;
    private $sourceDesktop;
    private $sourceMsite;
    private $location;
    private $email;
    private $msg;
    private $cmd;
    private $type;
    private $ttl;
    private $redisInstance;
    const REDIS = 'redis_cardata';
    const LANG = 'CONTROLPANEL_OP_MSG_';

    public function __construct(
        $expirationDate = null,
        $expirationTime = null,
        $source = null,
        $sourceDesktop = null,
        $sourceMsite = null,
        $location = null,
        $email = null,
        $msg = null,
        $cmd = null,
        $type = null,
        $ttl = null,
        $redisInstance = null
    ) {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.OpMsg'));
        $this->init();
        $this->populate('expiration_date', 'expirationDate', $expirationDate);
        $this->populate('location', 'location', $location);
        $this->populate('expiration_time', 'expirationTime', $expirationTime);
        $this->populate('email', 'email', $email);
        $this->populate('themsg', 'msg', $msg);
        $this->populate('source_msite', 'sourceMsite', $sourceMsite);
        $this->populate('source_desktop', 'sourceDesktop', $sourceDesktop);
        $this->populate('source', 'source', $source);
        $this->populate('cmd', 'cmd', $cmd);
        $this->populate('type', 'type', $type);
        if (empty($this->expirationTime)) {
            $this->expirationTime = '23:59';
        }
        $expirationDateFormat = strtotime(str_replace('/', '-', $this->expirationDate)." ".$this->expirationTime);
        $this->ttl = isset($ttl) ? $ttl : $expirationDateFormat - time();
        $this->redis = isset($redisInstance) ? $redisInstance : new EasyRedis(self::REDIS);
    }

    private function populate($request, $key, $value)
    {
        if (isset($_REQUEST[$request])) {
            $this->$key = isset($value) ? $value : $_REQUEST[$request];
        } else {
            $this->$key = isset($value) ? $value : null;
        }
    }

    private function getBconfLanguage($value)
    {
        global $BCONF;
        return Bconf::lang($BCONF, self::LANG.$value);
    }

    protected function getRedisKey($source, $location, $type, $email = null)
    {
        global $BCONF;
        $main_key = 'xop_msg';
        if (!empty($email) && $type == "direct")  {
            $main_key = "xdirect_msg_{$email}";
        }
        $ending = "{$main_key}_{$source}_{$location}";
        if (isset($source) && isset($location)) {
            return Bconf::get($BCONF, '*.common.redis_cardata.master').$ending;
        }
        return "";
    }

    protected function badSource($sourceDesktop, $sourceMsite)
    {
        if (!isset($sourceDesktop) && !isset($sourceMsite)) {
            $this->tmpl_errors['err_source'] = $this->getBconfLanguage('ERROR_SOURCE');
            return true;
        }
        return false;
    }

    protected function badEmail($email)
    {
        if (empty($email)) {
            $this->tmpl_errors['err_email'] = $this->getBconfLanguage('ERROR_EMAIL');
            return true;
        }

        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $this->tmpl_errors['err_email'] = $this->getBconfLanguage('ERROR_INVALID_EMAIL');
            return true;
        }
        return false;
    }

    protected function badDate($expirationTime, $expirationDate, $ttl, $minttl)
    {
        $errorDate = false;
        // NOT TOO CLOSE!
        if (!preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $expirationTime)) {
            $this->expirationTime = "23:59";
            $errorDate = true;
        }

        if (!isset($this->tmpl_errors['err_expiration_date']) && $ttl < $minttl) {
            $this->tmpl_errors['err_expiration_date'] = $this->getBconfLanguage('ERROR_EXPIRATION_DATE_OLD');
            $errorDate = true;
        }

        // BAD FORMAT
        if (!strtotime(str_replace('/', '-', $expirationDate))) {
            $this->tmpl_errors['err_expiration_date'] = $this->getBconfLanguage('ERROR_EXPIRATION_DATE_FORMAT');
            $errorDate = true;
        }
        return $errorDate;
    }

    protected function badMsg($msg, $sourceDesktop, $sourceMsite, $desktopMaxLen, $msiteMaxLen, $minLen)
    {
        $errorMsg = false;
        if (isset($msg)) {
            $msgClean = strip_tags($msg);
            if (isset($sourceDesktop) && strlen($msgClean) > $desktopMaxLen) {
                $this->tmpl_errors['err_themsg'] = $this->getBconfLanguage('ERROR_LARGE_MSG_DESKTOP');
                $errorMsg = true;
            }
            if (isset($sourceMsite) && strlen($msgClean) > $msiteMaxLen) {
                $this->tmpl_errors['err_themsg'] = $this->getBconfLanguage('ERROR_LARGE_MSG_MSITE');
                $errorMsg = true;
            }

            if (strlen($msgClean) <= $minLen) {
                $this->tmpl_errors['err_themsg'] = $this->getBconfLanguage('ERROR_SMALL_MSG');
                $errorMsg = true;
            }

            if (strlen($msgClean) == 0) {
                $this->tmpl_errors['err_themsg'] = $this->getBconfLanguage('ERROR_NULL_MSG');
                $errorMsg = true;
            }
        } else {
            $this->tmpl_errors['err_themsg'] = $this->getBconfLanguage('ERROR_NULL_MSG');
            $errorMsg = true;
        }
        return $errorMsg;
    }

    private function displayActionPage()
    {
        $this->displayResults('controlpanel/OpMsg/admin.html');
    }

    private function displayActionPageWithErrors()
    {
        foreach ($this->tmpl_errors as $key => $val) {
            $this->response->add_error($key, $val);
        }

        foreach ($this->options as $option) {
            if (isset($_REQUEST[$option]) && $_REQUEST[$option] != "") {
                $this->response->add_data($option, $_REQUEST[$option]);
            }
        }

        $this->displayResults('controlpanel/OpMsg/admin.html');
    }

    protected function hasErrors()
    {
        $this->tmpl_errors = array();
        if (isset($this->cmd) && $this->cmd == 'add') {
            $this->badMsg(
                $this->msg,
                $this->sourceDesktop,
                $this->sourceMsite,
                $this->config['desktopMaxlen'],
                $this->config['msiteMaxlen'],
                $this->config['minlen']
            );
            $this->badDate($this->expirationTime, $this->expirationDate, $this->ttl, $this->config['minttl']);
            $this->badSource($this->sourceDesktop, $this->sourceMsite);
            if ($this->type == "direct") {
                $this->badEmail($this->email);
            }
        }
        return count($this->tmpl_errors) > 0;
    }

    public function admin()
    {
        if ($this->hasErrors()) {
            $this->displayActionPageWithErrors();
            return;
        }
        if (isset($this->sourceDesktop)) {
            $this->source = $this->sourceDesktop;
            $this->adminAdd();
        }
        if (isset($this->sourceMsite)) {
            $errorMsg = true;
            $this->source = $this->sourceMsite;
            $this->adminAdd();
        }
        if (isset($this->cmd) && $this->cmd == 'del' && isset($this->source)) {
            $this->adminDel();
        }
        $this->displayActionPage();
    }

    protected function adminDel()
    {
        if (isset($this->location) && isset($this->cmd) && $this->cmd == 'del') {
            $key = $this->getRedisKey($this->source, $this->location, $this->type, $this->email);
            $status = $this->redis->del($key);
            return $status;
        }
        return 0;
    }

    protected function adminAdd()
    {
        // SET KEY AND EXPIRATION TIME
        $key = $this->getRedisKey($this->source, $this->location, $this->type, $this->email);
        if (!preg_match("/(2[0-3]|[01][0-9]):([0-5][0-9])/", $this->expirationTime)) {
            $this->expirationTime = "23:59";
        }
        $this->redis->hset($key, 'source', $this->source);
        $this->redis->hset($key, 'location', $this->location);
        $this->redis->hset($key, 'msg', $this->msg);
        if (!empty($this->email)) {
            $this->redis->hset($key, 'email', $this->email);
        }
        $this->redis->hset($key, 'time', time());
        $this->redis->hset($key, 'expire', $this->expirationDate." ".$this->expirationTime);
        return $this->redis->expire($key, $this->ttl);
    }
    
    public function main($function = null)
    {
        $this->options = array(
            'expiration_date',
            'expiration_time',
            'source_desktop',
            'source_msite',
            'location',
            'themsg',
            'email',
            'type'
        );
        if (Cpanel::hasPriv("OpMsg.admin")) {
            $this->response->add_data("priv_admin", "1");
        } else {
            $this->response->add_data("priv_admin", "0");
        }
        if ($function) {
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_js', '/js/op_msg.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->runMethod($function);
        }
    }
}
