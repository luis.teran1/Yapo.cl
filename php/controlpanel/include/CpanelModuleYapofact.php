<?php

namespace Yapo;

use Yapo\YapofactService;
use Yapo\Cpanel;
use DateTime;

class CpanelModuleYapofact extends CpanelModule
{
    public function __construct()
    {
        global $BCONF;
        $this->config = array_copy(Bconf::get($BCONF, 'controlpanel.modules.Yapofact'));
        parent::init();
        $this->populateVars();
    }

    private function populateVars()
    {
        foreach ($_REQUEST as $key => $val) {
            if (is_numeric($val)) {
                $this->$key = intval(sanitizeVar($val, 'int'));
            } elseif ($key == 'email') {
                $this->$key = sanitizeVar($val, 'email');
            } else {
                $this->$key = sanitizeVar($val, 'string');
            }
        }
        $this->admin_id = intval($_SESSION['controlpanel']['admin']['id']);
    }

    public function show()
    {
        $this->response->add_data("priv_yapofact_adminReport", Cpanel::hasPriv('Yapofact.adminReport') ? 1 : 0);
        $this->displayResults('controlpanel/yapofact/yapofact.html');
    }

    private function dateHasErrors()
    {
        $has_error = false;
        if (!isset($this->from) || strtotime($this->from) === false) {
            $this->response->add_data("err_from", "INVALID_DATE");
            $has_error = true;
        }
        if (!isset($this->to) || strtotime($this->to) === false) {
            $this->response->add_data("err_to", "INVALID_DATE");
            $has_error = true;
        }
        return $has_error;
    }

    private function adminReportHasErrors()
    {
        $has_error = false;
        if (isset($this->user_name) && preg_match("/[%^\/\\()<>=#\"+]/", $this->user_name)) {
            $this->response->add_data("err_user_name", "ERROR_NAME_INVALID");
            $has_error = true;
        }
        if (isset($this->ad_id) && !preg_match('/^$|^[0-9]+$/', $this->ad_id)) {
            $this->response->add_data("err_ad_id", "ID_INVALID");
            $has_error = true;
        }
        return $has_error;
    }

    private function reSendHasErrors()
    {
        if (!empty($this->Report_id) && isset($this->send_email_submit) && !empty($this->new_email)) {
            return false;
        }
        if (!empty($this->Report_id)) {
            $this->response->add_data("err_yapofact", "ERROR_EMAIL_INVALID");
            $this->response->add_data("show_row", $this->Report_id);
        }
        return true;
    }

    private function setData($data)
    {
        foreach ($data as $entry) {
            $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $entry->Receipt);
            $entry->Receipt = $date->format('Y-m-d H:i');

            $date = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $entry->Delivered);
            $entry->Delivered = $date->format('Y-m-d H:i');
            if ($entry->Receipt > $entry->Delivered) {
                $entry->Delivered = "";
            }
            foreach ($entry as $k => $v) {
                $this->response->fill_array($k, $v);
            }
        }
    }

    private function hasLoaded()
    {
        return isset($this->cmd) && $this->cmd == 'load';
    }

    // Public functions
    public function adminReport()
    {
        if ($this->hasLoaded() && !$this->dateHasErrors() && !$this->adminReportHasErrors()) {
            $yapofact = new YapofactService();
            $params = array(
                "Search_email" => $this->email,
                "Search_name" => $this->user_name,
                "Search_category" => $this->category,
                "Search_id" => $this->ad_id
            );
            $data = $yapofact->adminReport($this->from, $this->to, $params);
            if (!empty($data)) {
                $this->setData($data);
            }
            if (!$this->reSendHasErrors()) {
                $response = $yapofact->adminResend($this->Report_id, $this->new_email);
                $this->response->add_data("show_row", $this->Report_id);
                if (!$response) {
                    $this->response->add_data("err_yapofact", "ERROR_YAPOFACT_API");
                } else {
                    $this->response->add_data("success", "SUCCESS_RESEND");
                }
            }
        }
        $this->show();
    }

    public function main($function = null)
    {
        if ($function != null) {
            $this->response->fill_array('page_js', '/js/moment.min.js');
            $this->response->fill_array('page_js', '/js/pikaday.js');
            $this->response->fill_array('page_js', '/js/yapofact.js');
            $this->response->fill_array('page_css', '/css/pikaday.css');
            $this->runMethod($function);
        } else {
            $this->show();
        }
    }
}
