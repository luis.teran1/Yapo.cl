<?php

namespace Yapo;

use Yapo\Bconf;
use Yapo\Cpanel;
use Yapo\PaymentDelivery;

class CpanelModulePaymentDelivery extends CpanelModule
{
    public function __construct()
    {
        $this->config = array_copy(Bconf::get($GLOBALS['BCONF'], 'controlpanel.modules.PaymentDelivery'));
        $this->pydClient = new PaymentDelivery();
        parent::init();
    }
    
    public function main($function = null)
    {
        if (!is_null($function)) {
            $this->response->fill_array('page_js', '/js/PaymentDelivery.js');
            $this->response->fill_array('page_css', '/css/PaymentDelivery.css');
            $this->runMethod($function);
        } else {
            $this->show();
        }
    }
    public function show()
    {
        $this->response->add_data("priv_PaymentDelivery_search", Cpanel::hasPriv('PaymentDelivery.search') ? 1 : 0);
        $this->displayResults('controlpanel/payment_delivery/search.html');
    }
    private function searchHasErrors()
    {
        $has_error = false;
        if (!isset($this->response->data["get_search_text"])) {
            return true;
        }

        switch ($this->response->data["get_search_radio"]) {
            case "external_id":
                if (!preg_match('/^[0-9]+$/', $this->response->data["get_search_text"])) {
                    $this->response->add_data("err_escrow_id", "PAYMENT_DELIVERY_ERROR_ESCROW_ID_INVALID");
                    $has_error = true;
                }
                break;
            case "email":
                $regex = "/^[\\w_+-]+(\\.[\\w_+-]+)*\\.?@([\\w_+-]+\\.)+[\\w]{2,4}$/";
                if (!preg_match($regex, $this->response->data["get_search_text"])) {
                    $this->response->add_data("err_user_email", "PAYMENT_DELIVERY_ERROR_EMAIL_INVALID");
                    $has_error = true;
                }
                break;
            case "phone":
                if (!preg_match("/^[\b\\+\\-\\(\\)0-9]{8,11}$/", $this->response->data["get_search_text"])) {
                    $this->response->add_data("err_user_phone", "PAYMENT_DELIVERY_ERROR_PHONE_INVALID");
                    $has_error = true;
                }
                break;
            case "list_id":
                if (!preg_match('/^[0-9]+$/', $this->response->data["get_search_text"])) {
                    $this->response->add_data("err_list_id", "PAYMENT_DELIVERY_ERROR_LIST_ID_INVALID");
                    $has_error = true;
                }
                break;
        }
        return $has_error;
    }

    private function setData($data)
    {
        foreach ($data as $entry) {
            foreach ($entry as $key => $value) {
                $objects = array("Order", "Seller", "Buyer", "Ad", "BankAccount", "Address", "Shipping" ,"States");
                $this->setObjects("", $key, $value, "", $objects);
            }
        }
    }

    private function setObjects($origin, $key, $value, $prefix, $allowedArr)
    {
        if (in_array($key, $allowedArr) && (gettype($value) == "object" || is_array($value))) {
            foreach ($value as $k => $v) {
                $this->setObjects($key, $k, $v, $origin.$key, $allowedArr);
            }
        } else {
            $item = utf8_decode($value);
            if (strtolower($key) == 'date') {
                $item = strtotime($value);
            }
            $this->response->fill_array(lcfirst($prefix.$key), $item);
        }
    }

    private function hasLoaded()
    {
         return isset($this->cmd) && $this->cmd == 'load';
    }

    // Public functions
    public function search()
    {
        if (!$this->searchHasErrors()) {
            $response = $this->pydClient->execute(
                "search",
                array($this->response->data["get_search_radio"] => $this->response->data["get_search_text"])
            );
            if (!empty($response->body)) {
                $this->setData($response->body);
            } else {
                $this->response->add_data("err_no_data", "PAYMENT_DELIVERY_ERROR_EMPTY_LIST");
            }
        }
        $this->show();
    }

    public function reject()
    {
        $response = $this->pydClient->execute(
            "reject",
            array("id" => $this->response->data["get_hash"])
        );
        if ($response == null) {
            $this->response->add_data("err_reject_order", "PAYMENT_DELIVERY_ERROR_REJECT_ORDER");
        } else {
            $this->response->add_data("success_reject_order", $this->response->data["get_orderAdTitle"]);
        }
        $this->search();
    }
}
