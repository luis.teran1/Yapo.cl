function servicesFilterAds(){
	query = $("#ads_query").val().trim().split("+");
	list_ads = $(".list_ads");
	if(list_ads.length > 0 ){
		total_visible = 0;
		total_hidden = 0;
		persist =  $("#persist_checked").is(":checked");
		for( i = 0; i < list_ads.length; i++ ){
			id = list_ads[i].id.split("_")[1];
			found = false;
			for(q = 0;q< query.length; q++){
				if(list_ads[i].text.toUpperCase().indexOf(query[q].toUpperCase()) >= 0){
					found = true;
					break;
				}
			}
			if(persist && $("#row_"+id).find("input:checked").length > 0){
				found = true;
			}
			if(found){
				$("#row_"+id).show();
				$("#row_line_"+id).show();
				total_visible++;
			}
			else{
				$("#row_"+id).hide();
				$("#row_line_"+id).hide();
				total_hidden++;
			}
		}
		$("#counter_visible").text(total_visible);
		$("#counter_hidden").text(total_hidden);
	}
}

function validateSelected() {
  if ($(".ipt_radio:checked").length == 0 && $("#plan_none:checked").length == 1) {
    alert("No has seleccionado ning�n servicio.");
    return false;
  } else {
    return confirm('�Est� seguro de Aplicar estos productos?');
  }
}
