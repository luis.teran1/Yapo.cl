$(document).ready(function(){
  $("div.input input.int").keyup(function(){
    var id = $(this).attr("id");
    var val = $(this).val().replace(/[^0-9\,]/g,'');
    $('input[name ="' + id + '"]').val(val);
    if(val != "") {
      valArr = val.split(',');
      valArr[0] = (parseInt(valArr[0],10)).toLocaleString("es");
      val = valArr.join(',');
    }
    $(this).val(val);
  });
  $("div.input input.float").blur(function(){
    var re = /^(?:[0]{0,1}[\.|\,]{1}\d{1,2}|[1-9]+\d*[\.|\,]{1}\d{0,2})$/;
    var val = $(this).val();
    var id = $(this).attr("id");
    if (!re.test(val)) {
        $("span#err_"+id).removeClass('success');
        $("span#err_"+id).addClass('error');
        $("span#err_"+id).html('Valor debe ser decimal con m&aacute;ximo 2 d&iacute;gitos decimales');
        $("span#err_"+id).css('display', 'inline-block');
        $(this).val('');
    } else {
        $("span#err_"+id).css('display', 'none');
        $("span#err_"+id).html('');
        $("span#err_"+id).removeClass('error');
        $("span#err_"+id).addClass('success');
    }
  });
});
