/* Js for administrate promotional pages */

(function() {

  var categoryId = 0;
  var insertedCategories = [];

  function addCategory() {
    var $category = $('#category');
    if (!$category.val()
      || categoryId == maximumCat
      || insertedCategories.indexOf($category.val()) > -1) {
      return false;
    }

    var div = $('<div>', {
        class: 'cat-div'
    }).appendTo('#category_div');
    var inputContainer = $('<div />', {
        class: 'inputContainer'
    });

    var categoryText = $('#category>option:selected').text();
    var displayText = 'Categor�a: '+ categoryText +'\n';
    $('<p>', {
      class:'pp-text',
      text: displayText
    }).appendTo(inputContainer);

    $('<br>').appendTo(inputContainer);
    $('<button>', {
      class:'removeSearch',
      type: 'button',
      text:'Borrar',
    }).appendTo(inputContainer)
      .click(removeSearch);
    $('<input>', {
      class:'category_input',
      type:'text',
      name:'category[' + categoryId + '][id]',
      hidden: true,
      value:$category.val()
    }).appendTo(inputContainer);
    $('<input>', {
      class:'word_input',
      placeholder:'Palabras a a�adir o quitar de la b�squeda',
      type:'text'
    }).appendTo(inputContainer);
    $('<button>', {
      class:'add_button',
      type: 'button',
      text:'+',
      value:categoryId
    }).appendTo(inputContainer)
      .click(addWord);
    $('<button>', {
      class:'block_button',
      type: 'button',
      text:'-',
      value:categoryId
    }).appendTo(inputContainer)
      .click(blockWord);

    var categoryContainer = $('<div />', {
      class: 'categoryContainer'
    });

    $('<div>', {
      class:'add-div'
    }).appendTo(categoryContainer);
    $('<div>', {
      class:'block-div'
    }).appendTo(categoryContainer);

    div.append(inputContainer);
    div.append(categoryContainer);
    
    var filterInputContainer = $('<div />', {
      class: 'filterInputContainer'
    });
    $('<label>', {
      class:'filters_label',
      text:'Agrega filtros para esta categor�a (opcional):'
    }).appendTo(filterInputContainer); 

    var filterSelect = $('<select>', {
      class:'filter_select',
    }).appendTo(filterInputContainer).change(modifyFilterInputType).load(modifyFilterInputType);

    if (filterOptions[$category.val()] === undefined) {
          categoryId++;
          insertedCategories.push($category.val());
          $(".filter_select").trigger("change");
          return;
    }
    $.each(filterOptions[$category.val()],function(option, data) {
      var label = option;
      if(data.label_settings.label !== undefined){
        label = (data.label_settings.label);
        if (data.label_settings.suffix !== undefined){
          label += " (" + data.label_settings.suffix + ")";
        }
      }
      if (option == 'price' ) label += " ($)"
      filterSelect.append($("<option>").attr('value', option).
        text(label));
    });
    $('<select>', {
      class:'filter_input_select',
      hidden: true
    }).appendTo(filterInputContainer);
    $('<input>', {
      class:'filter_input',
      hidden: false,
      placeholder:'(Opcional)',
      type:'text'
    }).appendTo(filterInputContainer);
    $('<button>', {
      class: 'add_filter_button',
      type: 'button',
      title: 'agregar filtro',
      text:'+',
      value:categoryId
    }).appendTo(filterInputContainer).click(addFilter);
    $('<button>', {
      class: 'open_filter_button',
      type: 'button',
      text:'ac',
      title: 'abrir campo',
      value:categoryId
    }).appendTo(filterInputContainer).click(openFilterInputType);
    var filterContainer = $('<div />', {
      class: 'filterContainer'
    });
    $('<div>', {
      class:'add-filter-div'
    }).appendTo(filterContainer);
    $('<div>', {
      class:'block-filter-div'
    }).appendTo(filterContainer);
    $('<p>', {
        class:'pro-tip',
        html: 'Pro tip: puedes usar rangos con "-".<br>'+
        'Ej.: precio:1000-2000 � a�o:2010-2025<br>'
    }).appendTo(filterContainer);
    div.append(filterInputContainer);
    div.append(filterContainer);
    categoryId++;
    insertedCategories.push($category.val());
    $(".filter_select").trigger("change");
  }

  function validateFilterInput(position, input, type){
    if (input == "") return false;
    var alreadyExists = false;
    $("[name='category[" + position + "]["+type+"][]']").each(function() {
      if (input == $(this).val()){
        alreadyExists = true;
        return;
      }
    });
    if (alreadyExists){
      return false;
    }
    switch (type){
      case "email":
        var validEmailRegEx = /^[A-Z0-9_'%=+!`#~$*?^{}&|-]+([\.][A-Z0-9_'%=+!`#~$*?^{}&|-]+)*@\S+/i;
        return validEmailRegEx.test(input);
      default:
        return !(/\s|:/.test(input));
    }
  }

  function addFilter() {
    var categoryDiv = $(this).closest('.cat-div');
    var category = categoryDiv.find('.category_input').val();
    var filterInput = $(this).siblings('.filter_input');
    var filterInputSelect = $(this).siblings('.filter_input_select');
    var filterValue = filterInput.val()
    if (filterInput.attr("hidden")){
      filterValue = filterInputSelect.val()
    }
    var filterType = $(this).siblings('.filter_select').val();
    if (!validateFilterInput(this.value, filterValue, filterType)) {
      return false;
    }

    var div = categoryDiv.find('.add-filter-div');
    var divContainer = $('<div>', {
      class:'filterInputContainer'
    });
    var label = filterType;
    if(filterOptions[category][filterType].label_settings.label !== undefined){
      label = filterOptions[category][filterType].label_settings.label;
    }
    $('<label>', {
      class: 'add_filter_label',
      text: label
    }).appendTo(divContainer);

    var displayValue = filterValue;
    if(filterOptions[category][filterType]!== undefined &&
      filterOptions[category][filterType].options.length > 0 ){
      filterOptions[category][filterType].options.forEach(function(item) {
        if(filterValue == item.value) {
          displayValue = item.text
          if (displayValue != filterValue) displayValue = filterValue + " ("+displayValue+")";
        }
      });
    }
    $('<input>', {
      class: 'add_filter_input',
      type: 'text',
      readonly: true,
      value: displayValue
    }).appendTo(divContainer);
    $('<input>', {
      type: 'text',
      name: 'category[' + this.value + ']['+filterType+'][]',
      hidden: true,
      value: filterValue
    }).appendTo(divContainer);

    $('<button>', {
      class: 'remove_filter',
      type: 'button',
      text: 'x'
    }).appendTo(divContainer)
      .click(removeFilter);

    divContainer.appendTo(div);
    $(this)
      .closest('.cat-div')
      .find('.filter_input')
      .val('');
  }

  function addWord() {
    var preValue = this.previousSibling.value;
    if (!preValue)
      return false;
    var div = $(this).closest('.cat-div').find('.add-div');
    var divContainer = $('<div>', {
      class:'wordInputContainer'
    });

    $('<input>', {
      class: 'add_input',
      type: 'text',
      name: 'category[' + this.value + '][word_i][]',
      readonly: true,
      value: preValue
    }).appendTo(divContainer);

    $('<button>', {
      class: 'remove_word',
      type: 'button',
      text: 'x'
    }).appendTo(divContainer)
      .click(removeWord);

    divContainer.appendTo(div);
    $(this)
      .closest('.cat-div')
      .find('.word_input')
      .val('');
  }

  function blockWord() {
    var preValue = this.previousSibling.previousSibling.value;
    if (!preValue)
      return false;
    var div = $(this).closest('.cat-div').find('.block-div');
    var divContainer = $('<div>', {
      class:'wordInputContainer'
    });

    $('<input>', {
      class:'block_input',
      name:'category[' + this.value + '][word_f][]',
      readonly: true,
      value:preValue
    }).appendTo(divContainer);

    $('<button>', {
      class:'remove_word',
      type: 'button',
      text:'x'
    }).appendTo(divContainer)
      .click(removeWord);

    divContainer.appendTo(div);
    $(this).closest('.cat-div').find('.word_input').val('');
  }

  /**
  * Creates url for the promo page based on the inserted title,
  * deleting diacritical marks from the title
  */
  function friendlyUrlFill() {
    var url = $('#title').val().toLowerCase().trim();
    url = replaceAccents(url);
    url = url.replace(/[^A-Za-z0-9-\s]/g, '').replace(/\s/g, '-');
    $('#friendly-url').text(friendlyUrlBase + '/' + promoPath + '/' + url);
    $('#url').val(url);
  }

  function replaceAccents(str){
    var dict = [
      {letter: 'a', unicode: /[\340-\346]/g},
      {letter: 'e', unicode: /[\350-\353]/g},
      {letter: 'i', unicode: /[\354-\357]/g},
      {letter: 'o', unicode: /[\362-\370]/g},
      {letter: 'u', unicode: /[\371-\374]/g},
      {letter: 'n', unicode: /[\361]/g},
    ]

    dict.forEach(function(char){
      str = str.replace(char.unicode, char.letter);
    });

    return str;
  };

  function loadCategories(categories, queries) {
    categories.forEach(function(category, index) {
      $('#category').val(category);
      $('.add_cat').click();
      fillWords(queries[index]);
      fillFilters(queries[index]);
    });
  }

  /**
  * Receives an asearch formatted query, and gets
  * the keywords used to filter ads. Then, fills the promo pages
  * form accordingly.
  * Example:
  * fillWords('1 lim:10 category:1240 *:* add1 OR add2 NOT rm1')
  * The variables adds and removes (which contain the added / removed
  * words from the query) should contain:
  * adds = ['add1', 'add2']
  * removes = ['rm1']
  * Then, the page form is filled with these words.
  */
  function fillWords(query) {
    var text = query.split("*:* ")[1];
    var removes = text.split("NOT ");
    var $wordInput = $('.word_input');
    var adds = removes.shift().split(" OR ");
    var $categoryContainer = $wordInput.closest('.cat-div').last();

    adds.forEach(function(add) {
        $categoryContainer.find('.word_input').val(add.trim());
        $categoryContainer.find('.add_button').click();
        $categoryContainer.find('.word_input').val('');
    });
    removes.forEach(function(remove) {
        $categoryContainer.find('.word_input').val(remove.trim());
        $categoryContainer.find('.block_button').click();
        $categoryContainer.find('.word_input').val('');
    });

  }

  function fillFilters(query) {
    var filtersRegexp = /([\w\d_]+):([^\s]+)/g;
    var matches = query.match(filtersRegexp);
    if (matches.length == 0) {
        return
    }
    var filters = {}
    matches.forEach(function(value){
      var valArr = value.split(":")
      switch(valArr[0]){
        case "category":
        case "lim":
        case "img":
        case "*":
          return;
        default:
          filters[valArr[0]]=valArr[1].split(",")
      }
    });
    var categoryContainer = $('.filter_input').closest('.cat-div').last();
    $.each(filters, function(filterType, filterValues) {
      filterValues.forEach(function(filterValue){
        categoryContainer.find('.filter_input').val(filterValue.replace(/"/g,''));
        categoryContainer.find('.filter_select').val(filterType);
        categoryContainer.find('.add_filter_button').click();
        categoryContainer.find('.filter_input').val('');
      });
    });
  }

  function removeWord(evt) {
    var $ct = jQuery(evt.currentTarget);
    var $div = $ct.closest('.wordInputContainer');
    $div.remove();
  }

  function removeFilter(evt) {
    var $ct = jQuery(evt.currentTarget);
    var $div = $ct.closest('.filterInputContainer');
    $div.remove();
  }

  function openFilterInputType(evt) {
    var $ct = jQuery(evt.currentTarget);
    var $input = $ct.siblings('.filter_input');
    var $inputSelect = $ct.siblings('.filter_input_select');
    if ($inputSelect.children().length == 0) return;
    $input.attr("hidden",  !$input.is(":hidden"))
    $inputSelect.attr("hidden", !$inputSelect.is(":hidden"))
  }

  function modifyFilterInputType(evt) {
    var $ct = jQuery(evt.currentTarget);
    var $input = $ct.siblings('.filter_input');
    var $inputSelect = $ct.siblings('.filter_input_select');
    var categoryDiv = $ct.closest('.cat-div');
    var category = categoryDiv.find('.category_input').val();
    $inputSelect.attr("hidden", false);
    $inputSelect.find('option').remove();
    if(filterOptions[category][$ct.val()] !== undefined &&
      filterOptions[category][$ct.val()].options.length > 0 ){
      $input.attr("hidden", true);
      $inputSelect.attr("hidden", false);
      filterOptions[category][$ct.val()].options.forEach(function(item) {
          $inputSelect.append($('<option>', {
            value: item.value,
            text : item.text
          }))
      });
    }else{
      $input.attr("hidden", false)
      $inputSelect.attr("hidden", true)
    }
  }

  function removeSearch(evt) {
    if (confirm('�Est�s seguro que deseas eliminar la b�squeda?')) {
        var $ct = jQuery(evt.currentTarget);
        var categoryDiv = $ct.closest('.cat-div');
        var removedCategory = categoryDiv.find('.category_input').val();
        index = insertedCategories.indexOf(removedCategory);
        insertedCategories.splice(index, 1);
        categoryId--;
        categoryDiv.remove();
    }
  }

  function validateWord(evt) {
    var expectedRegex = /[a-zA-Z0-9\u00E0-\u00FC]+$/;
    var words = $(evt.currentTarget).val().split('').reduce(function(char, c) {
      return char += expectedRegex.test(c) ? c : '';
    }, '');
  }

  function validateChar(evt) {
    var $ct = jQuery(evt.currentTarget);
    var $wordInput = $ct.closest('.word_input');
    var value = $wordInput.val();

    var expectedRegex = /[a-zA-Z0-9\u00E0-\u00FC]+$/;
    var charCode = evt.which;
    var inputChar = String.fromCharCode(charCode);
    var title = evt.currentTarget.value + inputChar;

    // Firefox trick evt.which=0 => null char evt.which=8 => backspace
    var isEmptyChar = charCode === 0 || charCode === 8;

    if (!isEmptyChar && !expectedRegex.test(title)) {
      evt.preventDefault();
    }
  }

  function initPikaDay(ids) {
    ids.forEach(function(id) {
      new Pikaday({
        field: document.getElementById(id),
        format: 'YYYY-MM-DD'
      });
    });
  }

  function imgSubmit(e) {
    e.preventDefault();
    var request = new XMLHttpRequest();
    var formData = new FormData();
    formData.append('img', this.files[0]);
    formData.append('arg', 'main');

    request.onloadend = function(r) {
      var status = r.currentTarget.status;
      var response = r.currentTarget.response;
      if (status == 200) {
        var data = JSON.parse(response);
        if (data.status == "ok") {
          var imagesUrl = $("#img-div").attr('data-imgdir');
          var dir = data.file.slice(0, 2);
          var urlInput = '/ppageimages/' + dir + '/' + data.file;
          var imageUrl = imagesUrl + urlInput;
          var filename = data.file.slice(0, data.file.indexOf("."));
          $("#image-container").attr('src', imageUrl);
          $("#image-container").show(1000);
          $("#image").val(filename);
          $("#results").text("Imagen cargada");
        } else {
          $("#results").text(data.status);
          $("#image-container").hide(1000);
          $("#image-container").attr('src', '');
          $("#image").val("");
        }
      }
    };

    request.open("POST", "controlpanel?m=PPages&a=uploadImg");
    request.send(formData);
  };

  $(document).ready(function() {
    $('.add_cat').click(addCategory);
    jQuery(document).delegate('#title', 'keyup', friendlyUrlFill);
    jQuery(document).delegate('.word_input', 'keypress', validateChar);
    jQuery(document).delegate('.word_input', 'keyup', validateWord);
    if (typeof categories !== 'undefined' && categories.length) {
      loadCategories(categories, queries);
    }
    if (typeof updateUrl !== 'undefined' && updateUrl) {
      friendlyUrlFill();
    }
    $("#image-input").change(imgSubmit);
    $(".filter_select").trigger("change");
    initPikaDay(['end-date', 'start-date', 'start_date', 'end_date']);
  });

})();
