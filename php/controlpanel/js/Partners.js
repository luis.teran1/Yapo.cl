$(document).ready(function() {
 var trigger = document.getElementsByClassName('lineseparator');
  for (index = 0, len = trigger.length; index < len; ++index) {
    trigger[index].addEventListener('input', function() {
      var separator = document.getElementById('delimiter').value;
    });
  }

  jQuery(document).delegate('.yapoParam', 'change', setDictionaryVisiblity);
  
  $("#tab").click(function(){
    disableDelimiter("#Delimiter", "#tab", '|');
  });

  $("#tabConv").click(function(){
    disableDelimiter("#file_separator", "#tabConv", "");
  });

  disableDelimiter = function(sepInput, tabInput, delimiterValue) {
    var delimiterDisabled = false;
    var delimiterRequired = true;
    if ($(tabInput).is(':checked')) {
      delimiterDisabled = true;
      delimiterRequired = false;
      delimiterValue = '';
    }
    $(sepInput).attr('disabled', delimiterDisabled);
    $(sepInput).attr('required', delimiterRequired);
    $(sepInput).val(delimiterValue);
  };

  $("#imgData").click(function() {
    if ($(this).is(':checked')) {
      $("#imgDataContent").show();
      $("#PathImg").attr('required', true);
      $("#ImgProtocol").trigger("change");
    } else {
      $("#imgDataContent").hide();
      $("#ImgProtocol").attr('selectedIndex', 0);
      $("#PathImg").attr('required', false);
      $("#ImgUser").attr('required', false);
      $("#ImgDomain").attr('required', false);
      $("#ImgPass").attr('required', false);
    }
  });

  $("#Protocol").change(function() {
    if ($(this).val()!='http') {
      $("#divConnData").show();
    }else{
      $("#divConnData").hide();
    }
  });
  
  $("#ImgProtocol").change(function() {
    if ($(this).val()!='http') {
      $("#ImgUser").attr('required', true).attr('readonly',false);
      $("#ImgDomain").attr('required', true).attr('readonly',false);
      $("#ImgPass").attr('required', true).attr('readonly',false);
    }else{
      $("#ImgUser").attr('required', false).attr('readonly',true).val("");
      $("#ImgDomain").attr('required', false).attr('readonly',true).val("");
      $("#ImgPass").attr('required', false).attr('readonly',true).val("");
    }
  });

  $("#transmit").click(function() {
    if ($(this).is(':checked')) {
      $("#advanced_transmit").show();
    } else {
      $("#advanced_transmit").hide();
    }
  });

  $("#rules").click(function() {
    if ($(this).is(':checked')) {
      $("#advanced_rules").show();
      $("#Rules").attr('required', true);
      $("#IRules").attr('required', true);
    } else {
      $("#advanced_rules").hide();
      $("#Rules").attr('required', false);
      $("#IRules").attr('required', false);
    }
  });

   $("#converter").click(function() {
    var downloadSelector = $("#DownloadUser, #DownloadPassword, #DownloadDomain, #DownloadSeparator, #DownloadFilePath");
    var uploadSelector = $("#UploadUser, #UploadPassword, #UploadDomain, #UploadFilePath");
    if($(this).is(':checked')) {
      $("#converterContent").show();
      downloadSelector.attr('required', true);
      uploadSelector.attr('required', true);
    } else {
      $("#converterContent").hide();
      downloadSelector.attr('required', false);
      uploadSelector.attr('required', false);
    }
  });
});

function setDictionaryVisiblity(evt) {
  var $ct = jQuery(evt.currentTarget);
  var $row = $ct.closest('tr');
  var $dictDiv = $row.find('.dict');
  var $dictInput = $row.find('input[type=hidden]');

  var selectedValue  = evt.currentTarget.value;
  var status = selectedValue == 'empty' ? true : false;
  $dictDiv.attr("hidden", status);
  $dictInput.attr("disabled", status);
}

function addDictionary(index) {
  $('#dict-form-'+index).toggle();
  return false;
}

function addElement(index) {
  var $key = $('#dict-form-'+ index + ' input[name=key]');
  var $value = $('#dict-form-'+ index + ' input[name=value]');
  var $h = $('#dict-hidden-'+index);
  var $dictElements = $('#dict-elements-'+index);

  var keyVal = $key.val();
  if (keyVal == '' || keyVal.trim() == '') {
    alert('Ingrese un nombre y un valor');
    return false;
  }

  var dictEntry = $key.val() + ":" + $value.val();
  if (exists($h.val(), dictEntry)) {
    alert("Los valores ingresados ya existen");
    return false;
  }

  var rowCount = $('#dict-elements-'+index+' tr' ).length;
  $dictElements.append('<tr id="dict-elem-'+ index + '-' + rowCount + '">'
    + '<td id="dict-elem-index-' + index + '-' + rowCount + '">' + $key.val() + '</td>'
    + '<td id="dict-elem-val-' + index + '-' + rowCount + '">' + $value.val() + '</td>'
    + '<td><a class="dict-form-remove" href="" onclick="javascript: return removeElement('+ index + ',' + rowCount +')"> - </a></td></tr>');


  $h.val($h.val() + dictEntry + ";");
  $key.val('');
  $value.val('');
  return false;
}

function exists(s, kv) {
  var parts = s.split(";");
  for (var i = 0; i < parts.length; i++) {
    if (parts[i] == kv) {
      return true;
    }
  }
  return false;
}

function removeElement(index, row) {
  var $elem = $('#dict-elem-'+index+'-'+row);
  var $h = $('#dict-hidden-'+index);
  var elemIndex = $('#dict-elem-index-'+index+'-'+row).html();
  var elemValue = $('#dict-elem-val-'+index+'-'+row).html();

  var elemStr = elemIndex + ":" + elemValue + ";";
  $h.val($h.val().replace(elemStr, ''));
  $elem.html('').css('display', 'none');
  return false;
}
