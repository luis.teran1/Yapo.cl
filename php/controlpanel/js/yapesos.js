$(document).ready(function() {
  var inputs = [
    'report_from',
    'report_to',
    'history_from',
    'history_to',
    'edit_from',
    'edit_to'
  ];
  inputs.forEach(function(entry) {
    // Setup calendar
    new Pikaday({
      field: document.getElementById(entry),
      format: 'YYYY-MM-DD'
    });
  });

  $('#assign_send').click(function() {
    service_order = $('#service_order').val();
    yapesos = parseInt($('#yapesos').val()) || 0;
    bonus = parseInt($('#bonus').val()) || 0;
    email = $('#email').val();
    total = yapesos + bonus;
    return confirm("Est�s seguro de asignar \""+total+"\" Yapesos al usuario: \""+email+"\"?");
  });

  $('.edit-yapeso').click(function () {

    id = $(this).attr('data-id');
    $('.hidden:not(#form_'+id+')').hide();
    $('#form_'+id).toggle();

  });

  $(".yapesos-input.edit-send").click(function () {
    return confirm("Est�s seguro de modificar estos Yapesos?");
  });
});
//EOF
