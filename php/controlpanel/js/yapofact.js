$(document).ready(function() {
  var inputs = [
    'from',
    'to',
  ];
  inputs.forEach(function(entry) {
    // Setup calendar
    new Pikaday({
      field: document.getElementById(entry),
      format: 'YYYY-MM-DD'
    });
  });
  $('.show_report').click(function () {
    id = $(this).attr('data-id');
    $('.hidden:not(#form_'+id+')').hide();
    $('#form_'+id).toggle();
  });
});
//EOF
