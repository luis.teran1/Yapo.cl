$(document).ready(function(){
    //assignpack module
	$('#pack_assign_buttom').length && $('#pack_assign_buttom').attr('disabled', 'disabled');
	$('#pack_period , #pack_slots').change(function(){
		var period = $('#pack_period').val();
		var slots = $('#pack_slots').val();
		if (period != "" && slots != "") {
			$('#pack_assign_buttom').attr('disabled', '');
		}
		else {
			$('#pack_assign_buttom').attr('disabled', 'disabled');
		}
	});
	$('#cardNumber').keyup(telesalesOnlyNumbers);
	$('#cvv').keyup(telesalesOnlyNumbers);
});
    /* Functions to edit account on controlpanel */
function accountEditChangeType(obj){
	value = obj.getAttribute("value");
	disabled_inputs = false;
	label_rut = "Rut *";
	label_addr = "Direcci�n *";
	if (value == "0") {
		disabled_inputs = true;
		label_rut = "Rut ";
		label_addr = "Direcci�n ";
		$("#err_address").hide()
		$("#bNameRow").hide()
	} else {
		$("#err_address").show()
		$("#bNameRow").show()
	}
	$("#account_contact").attr("disabled", disabled_inputs);
	$("#account_lob").attr("disabled", disabled_inputs);
	$("#account_business_name").attr("disabled", disabled_inputs);
	$("#label_rut_cell").html(label_rut);
	$("#label_addr_cell").html(label_addr);
}

function paymentChangeRegion() {
	changeRegion("id","region","communes");
}

function accountEditChangeRegion(obj) {
	changeRegion("account","region","commune");
}

function changeRegion(prefix,region_id,commune_id){
	value = $("#"+prefix+"_"+region_id).val();
	select_communes = $("#"+prefix+"_"+commune_id);
	select_communes.val("");
	if(value == ""){
		select_communes.attr("disabled",true);
	}else{
		//Remove all options from select leaving tail
        var options = select_communes.find('option');//select_communes.prop?select_communes.prop('options'):select_comunes.attr('options');
        for(i=(options.length-1); i >= 1; i--) {
       	        $("#"+prefix+"_"+select_communes[0].name+" option[value='"+$(options[i]).val()+"']").remove();
        }
		if($("#"+prefix+"_"+region_id).val() != ""){
			var communesArray = regionArray[value]['commune'];
			select_communes.val("");
			$.each(communesArray, function(key, val) {
				if(val != undefined)
				select_communes.append('<option value='+key+'>'+val["name"]+'</option>');
			});
		}
		select_communes.attr("disabled",false);
	}
}

function telesalesToggleProduct(obj){
	check_id = obj.getAttribute("id");
	prod_id = obj.getAttribute("value");
	is_checked = obj.checked;
	hidden_fields = $("#hidden_fields");
	counter_products = $(".mpad_class").length;
	if(counter_products == undefined)
		counter_products = 0;
	id_splitted = check_id.split("-")
	prod_codename = id_splitted[0];

	if (id_splitted.length == 2){
		list_id = id_splitted[1];
		prod_price = parseInt($("#product_price_"+prod_codename).val());

		if(is_checked){
			if( (prod_id == 1 || prod_id == 8) && $("#mpad_"+list_id+"_2").length == 1){
				document.getElementById("mpad_"+ list_id + "_2" ).remove();
				document.getElementById("mpid_"+ list_id + "_2" ).remove();
				$("#weekly_bump-"+list_id).attr("checked",false);
				counter_products--;
			}
			if( (prod_id == 2 || prod_id == 8) && $("#mpad_"+list_id+"_1").length == 1){
				document.getElementById("mpad_"+ list_id + "_1" ).remove();
				document.getElementById("mpid_"+ list_id + "_1" ).remove();
				$("#bump-"+list_id).attr("checked",false);
				counter_products--;
			}
			if( (prod_id == 1 || prod_id == 2) && $("#mpad_"+list_id+"_8").length == 1){
				document.getElementById("mpad_"+ list_id + "_8" ).remove();
				document.getElementById("mpid_"+ list_id + "_8" ).remove();
				$("#daily_bump-"+list_id).attr("checked",false);
				counter_products--;
			}
			input_ad = '<input type="hidden" name="mpad['+counter_products+']" value="'+list_id+'" id="mpad_'+ list_id+'_'+prod_id+'"  class="mpad_class" >\n';
			input_pr = '<input type="hidden" name="mpid['+counter_products+']" value="'+prod_id+'" id="mpid_'+ list_id+'_'+prod_id+'"  class="mpid_class" >\n';
			hidden_fields.append(input_ad + input_pr);
			counter_products++;
		}
		else{
			//Remove inputs
			document.getElementById("mpad_"+ list_id + "_" + prod_id).remove();
			document.getElementById("mpid_"+ list_id + "_" + prod_id).remove();
			//Rewrite the names
			counter_products = rewrite_names_telesales();
		}
	}
	else {
		counter_products -= $("input[id*='mpad_store']").length;
		$("input[id*='mpad_store']").remove();
		$("input[id*='mpid_store']").remove();
		
		if(prod_id != 0){
			prod_price = parseInt($("#product_price_"+prod_codename).val());
			input_ad = '<input type="hidden" name="mpad['+counter_products+']" value="0" id="mpad_store_'+prod_id+'"  class="mpad_class" >\n';
			input_pr = '<input type="hidden" name="mpid['+counter_products+']" value="'+prod_id+'" id="mpid_store_'+prod_id+'"  class="mpid_class" >\n';
			hidden_fields.append(input_ad + input_pr)
		}
		counter_products = rewrite_names_telesales();
	}
	setCookieTelesales();
}

function telesalesPackRemove(){
		
	$("#select_pack").toggle();
	$("#selected_pack").toggle();
	$("input[id*='mpad_pack']").remove();
	$("input[id*='mpid_pack']").remove();

	$('#pack_cart').data('price', 0);
	$('#pack_cart').data('label', '');
	setCookieTelesales();
	$('#pack_period').val('');
	$('#pack_slots').val('');
	$('#pack_assign_buttom').attr('disabled', 'disabled');
}

function telesalesTogglePack(){

	$("#select_pack").toggle();
	$("#selected_pack").toggle();
	$("input[id*='mpad_pack']").remove();
	$("input[id*='mpid_pack']").remove();
	
	period = $("#pack_period").val();
	slots = $("#pack_slots").val();
	pack_type = $("#pack_type").val();
	listKey = pack_type + '_'+ slots + '_' + period;
	packElement = product_list[listKey];
	packValue = packElement.price;
	prodId = packElement.code; 
	description = packElement.description; 

	if(prodId.toString().length == 5){
		//prod_price = parseInt($("#product_price_"+prod_codename).val());
		var cont = rewrite_names_telesales();
		input_ad = '<input type="hidden" name="mpad['+cont+']" value="0" id="mpad_pack_'+prodId+'"  class="mpad_class" >\n';
		input_pr = '<input type="hidden" name="mpid['+cont+']" value="'+prodId+'" data-codename="' +listKey +'" id="mpid_pack_'+prodId+'"  class="mpid_class" >\n';
		$("#hidden_fields").append(input_ad + input_pr);
		$('#pack_cart').text("- Pack: "+ description);
		$('#pack_cart').data('price', packValue);
		$('#pack_cart').data('label', description);
		setCookieTelesales();
	}

}

function cart_to_cookie(){
	var elems_mpad = $('#hidden_fields .mpad_class');
	var elems_mpid = $('#hidden_fields .mpid_class');
	var str = [];
	for (var i = 0; i<elems_mpad.length; i++){
		str[i] = {pid: elems_mpid[i].value, lid:elems_mpad[i].value};
		if (elems_mpid[i].hasAttribute('data-codename'))
			str[i].code=elems_mpid[i].getAttribute('data-codename');
	}
	return JSON.stringify(str);
}

function cookie_to_cart(){
	var str = JSON.parse(getCookie(getCookieNameTelesales()));
	var str_html = '';
	if (str == null) 
		return str_html;
	for (var i = 0; i< str.length; i++){
		if ('code' in str[i]){
			str_html += '<input type="hidden" name="mpad['+i+']" value="0" id="mpad_pack_'+str[i].pid+'"  class="mpad_class" >';
			str_html += '<input type="hidden" name="mpid['+i+']" value="'+str[i].pid+'" data-codename="' +str[i].code +'" id="mpid_pack_'+str[i].pid+'"  class="mpid_class" >';
		} else if (str[i].lid == 0) {
			str_html += '<input type="hidden" name="mpad['+i+']" value="0" id="mpad_store_'+str[i].pid+'"  class="mpad_class" >';
			str_html += '<input type="hidden" name="mpid['+i+']" value="'+str[i].pid+'" id="mpid_store_'+str[i].pid+'"  class="mpid_class" >';
		} else {
			str_html += '<input type="hidden" name="mpad['+i+']" value="'+str[i].lid+'" id="mpad_'+str[i].lid+'_'+str[i].pid+'"  class="mpad_class" >';
			str_html += '<input type="hidden" name="mpid['+i+']" value="'+str[i].pid+'" id="mpid_'+str[i].lid+'_'+str[i].pid+'"  class="mpid_class" >';
		}
	}
	return str_html;
}
function setCookieTelesales() {

	var expiration_time = 10*365*24*60*60;
	setCookie(getCookieNameTelesales(),cart_to_cookie(),expiration_time);
	calculateTotals();
}

function rewrite_names_telesales(){
	//Rewrite the names
	counter_products = $(".mpad_class").length;
	if(counter_products == undefined)
		counter_products = 0;
	for(i = 0; i < counter_products ; i ++){
		$(".mpad_class")[i].name = "mpad["+i+"]";
		$(".mpid_class")[i].name = "mpid["+i+"]";
	}
	return counter_products;
}

function calculateTotals(){
	/* compute the totals for ads products and stores */

	// ads products totals
	var countProducts = 0;
	var totalPrice = 0;

	for (var i = 0; i < products_for_ads.length; i++) {
		var productId = products_for_ads[i];
		var prodTotal = 0;
		var $prod = $('input[type="checkbox"][value="' + productId + '"]:checked');
		$prod.each(function(){
			price = parseInt($(this).attr('data-price'));
			prodTotal += price;
		});
		$('#cnt_prod_' + productId).html($prod.size());
		$('#tot_prod_' + productId).html(addThousandSeparator(prodTotal));

		countProducts += $prod.size();
		totalPrice += prodTotal;
	}

	// store "total"
	$checkedStore = $('[name="store_plans"]:checked');
	var storePrice = parseInt($checkedStore.attr('data-price'));
	$("#cnt_prod_store").html($checkedStore.attr("data-label"));
	$("#tot_prod_store").html(addThousandSeparator(storePrice));
	totalPrice += storePrice;
	if (storePrice > 0) {
		countProducts += 1;
	}

	if ($('#selected_pack').is(':visible')) {
		$pack = $('#pack_cart');
		price = parseInt($pack.data('price')?$pack.data('price'):0);
		$("#cnt_prod_pack").html($pack.data("label"));
		$("#tot_prod_pack").html(addThousandSeparator(price));
		totalPrice += price;
		countProducts += 1;
	} else {
		$("#cnt_prod_pack").html('-');
		$("#tot_prod_pack").html('0');
	}

	// Set totals
	$("#count_products").html(countProducts);
	$("#total_products").html(addThousandSeparator(totalPrice));
}

function addThousandSeparator(num) {
	  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}

function loadSelectedTelesales(){
	$('#hidden_fields').html(cookie_to_cart());
	counter_products = $(".mpad_class").length;

	products = $(".mpid_class");
 	ads = $(".mpad_class");

	for(i = 0; i < counter_products ; i ++){
		tmp_prod = products[i].value;
		tmp_ad = ads[i].value;

		remove =false;

		if(tmp_prod=="1"){
			if(!$("#bump-"+tmp_ad).is(':disabled')){
				$("#bump-"+tmp_ad).attr("checked",true)
			}else{
		 		remove = true; 
			}
		}

		if(tmp_prod=="2"){
			if(!$("#weekly_bump-"+tmp_ad).is(':disabled')){
				$("#weekly_bump-"+tmp_ad).attr("checked",true)
			}else{
		 		remove = true; 
			}
		}

		if(tmp_prod=="8"){
			if(!$("#daily_bump-"+tmp_ad).is(':disabled')){
				$("#daily_bump-"+tmp_ad).attr("checked",true)
			}else{
		 		remove = true; 
			}
		}

		if(tmp_prod=="3"){ 
			if(!$("#gallery-"+tmp_ad).is(':disabled')){
				$("#gallery-"+tmp_ad).attr("checked",true)
			}else{
		 		remove = true; 
			}
		}
		if(tmp_prod > 3 && tmp_prod < 8){
			$('input[name="store_plans"][value="'+tmp_prod+'"]').attr("checked","true");
		}
		if(products[i].hasAttribute('data-codename')) {
		
			$("#select_pack").toggle();
			$("#selected_pack").toggle();
			var codename = products[i].getAttribute('data-codename');
			$('#pack_cart').text("- Pack: "+product_list[codename].description)+" ";
			$('#pack_cart').data('price', product_list[codename].price);
			$('#pack_cart').data('label', product_list[codename].description);
		}
		if(remove){
			document.getElementById("mpad_"+ tmp_ad + "_" + tmp_prod).remove();
			document.getElementById("mpid_"+ tmp_ad + "_" + tmp_prod).remove();
		}
	}

	setCookieTelesales();
}
function getCookieNameTelesales(){
	email = $("#telesales-email").val().replace(/@|\./g,"_");
	return "data_telesales_"+email;

}
function cleanCookieTelesales(){
	deleteCookie(getCookieNameTelesales());
}
function telesalesToggleInvoice(obj){
	if(obj.value == "2"){
			$("#invoice_doc").show();
	}
	else{
			$("#invoice_doc").hide();
	}
}
function telesalesFilterAds(){
	query = $("#ads_query").val().trim().split("+");
	list_ads = $(".list_ads");
	if(list_ads.length > 0 ){
		total_visible = 0;
		total_hidden = 0;
		persist =  $("#persist_checked").is(":checked");
		for( i = 0; i < list_ads.length; i++ ){
			id = list_ads[i].id.split("_")[1];
			found = false;
			for(q = 0;q< query.length; q++){
				if(list_ads[i].text.toUpperCase().indexOf(query[q].toUpperCase()) >= 0){
					found = true;
					break;
				}
			}
			if(persist && $("#row_"+id).find("input:checked").length > 0){
				found = true;
			}
			if(found){
				$("#row_"+id).show();
				$("#row_line_"+id).show();
				total_visible++;
			}
			else{
				$("#row_"+id).hide();
				$("#row_line_"+id).hide();
				total_hidden++;
			}
		}
		$("#counter_visible").text(total_visible);
		$("#counter_hidden").text(total_hidden);
	}
}

function telesalesOnlyNumbers() {
	var val = this.value.replace(/\D/g, '');
	this.value = val;
}
