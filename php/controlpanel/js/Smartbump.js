function dynamicallyLoadScript(url, callback) {
  var script = document.createElement("script");
  script.src = url;
  document.head.appendChild(script);
  script.addEventListener('load', () => {
    callback && callback();
  });
}

function dynamicallyLoadCSS(url) {
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", url);
  document.head.appendChild(fileref);
}

function reportFilename(){
  var dateStart =  $("#sb-report-start-date").val();
  var dateEnd =  $("#sb-report-end-date").val();
  var kind =  $("#sb-report-type").val();
  return 'smartbump_report_'+dateStart+"_"+dateEnd+"_"+kind;
}

function initialize() {
  var inputs = [
    'sb-subscribe-expiration',
    'sb-edit-expiration',
    'sb-report-start-date',
    'sb-report-end-date'
  ];
  inputs.forEach(function(entry) {
    // Setup calendar
    new Pikaday({
      field: document.getElementById(entry),
      format: 'YYYY-MM-DD'
    });
  });
  $(".readonly").keydown(function(e){
    e.preventDefault();
  });
  $('#sb-report-table').DataTable({
    dom: 'B<"clear">lfrtip',
    buttons: [
      {
        extend: 'csv',
        text: 'Export to CSV',
        filename: reportFilename
      },
      {
        extend:  'excel',
        text: 'export to XLSX',
        filename: reportFilename
      }
    ]


  });
}
document.addEventListener('DOMContentLoaded', function () {
  dynamicallyLoadCSS("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.css");
  dynamicallyLoadScript("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.js", initialize);
});
