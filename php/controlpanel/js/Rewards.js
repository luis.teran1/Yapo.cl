function dynamicallyLoadScript(url, callback){
    //url is URL of external file, callback is the code
    //to be called from the file
    var scriptTag = document.createElement('script');
    scriptTag.src = url;
    scriptTag.onload = callback;
    scriptTag.onreadystatechange = callback;
    document.head.appendChild(scriptTag);
};

function dynamicallyLoadCSS(url) {
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", url);
  document.head.appendChild(fileref);
}

function reportFilename(){
  var dateStart =  $("#rwds-search-start-date").val();
  var dateEnd =  $("#rwds-search-end-date").val();
  var name = 'rewards'
  if (dateStart != '')
    name = name+'_'+dateStart;
  if (dateEnd != '')
    name = name+'_'+dateEnd;

  return name;
}

function checkContent(settings){
  var api = this.api();
  tot = api.data().length;
  if (tot > 0) {
    api.buttons().enable();
  } else {
    api.buttons().disable();
  }
}

/*******  List groups methods ********/
function listGroups(ApiResponse){
  groups = ApiResponse.groups;
  groups.forEach(function(group){
    var creation =  new Date(group.creation);
    var start =  new Date(group.start);
    var expiration =  new Date(group.expiration);
    $('#rwds-group-list').append(`
        <tr>
             <td> 
                ${group.id}
             </td>
             <td>
                ${group.name}
             </td>
             <td>
                ${group.rewards.length}
             </td>
             <td>
                ${creation.toLocaleString()}
             </td>
             <td>
                ${start.toLocaleString()}
             </td>
             <td>
                ${expiration.toLocaleString()}
             </td>
             <td>
                <input type="hidden" value="${group.status}">
                <select id="selectEditStatus${group.id}" group-id="${group.id}"></select>
             </td>
             <td class="text-center">
                <a class="btn btn-primary btn-sm" href="/controlpanel?m=Rewards&a=administrateGroup&groupID=${group.id}">Editar</a>
                <a class="btn btn-danger btn-sm" onclick="if(confirm('�Desea eliminar este grupo?')){ window.location = '/controlpanel?m=Rewards&a=deleteGroup&groupID=${group.id}'}">X</a>
             </td>
        </tr>`);
        var selectEditStatus = $('#selectEditStatus'+group.id);
        selectEditStatus.append(new Option('Activo', 'ACTIVE'));
        selectEditStatus.append(new Option('Inactivo', 'INACTIVE'));
        selectEditStatus.append(new Option('Expirado', 'EXPIRED'));
        selectEditStatus.val(group.status);
        selectEditStatus.change(editStatus);
  });
}

function editStatus(){
  if (confirm("�Desea cambiar el estado a "+$(this).val()+"?")){
      window.location='/controlpanel?m=Rewards&a=setGroupStatus&groupID='+$(this).attr('group-id')+'&status='+$(this).val();
  }
  else{
    $(this).val($(this).prev().val());
  }
}

/*********  Create group methods ********/

function createGroup() {
  data = {
    'name': $('.name-in').val(),
    'start':  $('.date-start-in').val()+"T00:00:00Z",
    'expiration' : $('.date-expiration-in').val()+"T23:59:59Z",
  }
  $(".b64encoded-json").val(btoa(JSON.stringify(data)))
  $("#createGroupForm").submit();
}

/********   Edit group methods ********/
function fillEditGroupForm(ApiResponse){
  var group = ApiResponse['groups'][0];
  var rewards = group['rewards'];
  var start =  new Date(group.start);
  var expiration =  new Date(group.expiration);
  $('.group-name-in').val(group.name);
  $('.group-id-in').val(group.id);
  $('.date-start-in').val(start.toISOString().substr(0, 10));
  $('.date-expiration-in').val(expiration.toISOString().substr(0, 10));
  rewards.forEach(function(reward){
      addFloor(reward.id, reward.niceName, reward.description, reward.contents, reward.conditions);
    });
  }

function setFloorButtons(floorDiv){
    btnGroup = $('<div>', {class: 'btn-group  btn-group-sm mt-20 mb-20'}).appendTo(floorDiv)
    $('<a>', {class: 'up-floor btn btn-success',text:'Subir piso'}).appendTo(btnGroup).click(upFloor);
    $('<a>', {class: 'down-floor btn btn-success',text:'Bajar piso'}).appendTo(btnGroup).click(downFloor);
    $('<a>', {class: 'delete-floor btn btn-danger',text:'Borrar piso'}).appendTo(btnGroup).click(deleteFloor);
}

function addFloor(rewardID, name, description, contents, conditions){
    var newFloor = $('<div>', {class: 'floor', style:'border: solid 2px black'});
    newFloor.appendTo('#group-rewards-div');
    var seqNo = newFloor.index();
    newFloor.append(`<h4>Piso N� <b class='floor-number'>`+(seqNo+1)+`</b>: "${name}"</h4>`);
    setFloorButtons(newFloor);
    formGroup = $('<div>', {class: 'form-group'});
    formGroup.appendTo(newFloor);
    formGroup.append(`<input class='reward-id-in' value='${rewardID}' type="hidden">`);
    formGroup.append(`
      <label>Nombre</label><br>
      <input class="name-in form-control" value='${name}'>
      <br>
    `);
    formGroup.append(`
      <label> Descripci�n</label><br>
      <textarea class="description-in form-control" cols='50' rows='2'>${description}</textarea>
      <br>
    `);

    var contentsDiv = $('<div>', {class: 'contents-div form-group mt-20 mb-10 p-15'}).appendTo(newFloor);
    $('<h6>', {text:'Premio a entregar'}).appendTo(contentsDiv);
    contents.forEach(function(content,idx){
      var newContentDiv = $('<div>', {class:'content p-20 mt-20'});
      newContentDiv.appendTo(contentsDiv);
      newContentDiv.append(`<label>Producto</label>`)
      productTypeSelect = $('<select>', {class:'product-type-in form-control'});
      rewardProducts.forEach(function(product){
        productTypeSelect.append($('<option>', {value: product.value, text: product.name}));
      });
      newContentDiv.append(productTypeSelect);
      productTypeSelect.val(content.productType);
      newContentDiv.append(`
        <label>Cantidad</label>
        <input class="quantity-in form-control" value='${content.quantity}' type="number" min="0">
      `)
    });
    var conditionsDiv = $('<div>', {class: 'conditions-div form-group mt-20 mb-20 p-15'}).appendTo(newFloor);
    $('<h6>', {text:'Condiciones de entrega'}).appendTo(conditionsDiv);
    conditions.forEach(function(condition,idx){
      var newConditionDiv = $('<div>', {class:'condition p-20 mt-20'});
      newConditionDiv.appendTo(conditionsDiv);
      newConditionDiv.append(`
          <label>Descripci�n</label>
          <input class="condition-name-in form-control" value='${condition.niceName? condition.niceName:''}'>
      `);
      newConditionDiv.append(`
          <label>M�trica</label>
      `);
      metricTypeSelect = $('<select>', {class:'metric-type-in form-control'});
      conditionMetrics.forEach(function(metric){
        metricTypeSelect.append($('<option>', {value: metric.value, text: metric.name}));
      });
      metricTypeSelect.val(condition.metricType);
      newConditionDiv.append(metricTypeSelect);
      newConditionDiv.append(`<label>Operador</label>`);
      operatorSelect = $('<select>', {class:'operator-in form-control'});
      operators.forEach(function(operator){
        operatorSelect.append($('<option>', {value: operator.value, text: operator.name}));
      });
      operatorSelect.val(condition.operator);
      newConditionDiv.append(operatorSelect);
      newConditionDiv.append(`
          <label>Meta</label>
          <input class="expected-in form-control" value='${condition.expected}' type="number" min="o">
      `);
    });
}

function refreshFloorNumbers(){
    $(".floor").each(function(){
      $(this).find(".floor-number").text($(this).index()+1);
      $(this).find(".seq-in").val($(this).index());
    });
}

function deleteFloor(){
  if (confirm('�Est�s seguro que deseas eliminar el piso?')) {
    $(this).parent().parent().remove();
    refreshFloorNumbers();
  }
}

function downFloor(){
  var floor = $(this).parent().parent();
  floor.insertBefore(floor.prev());
  refreshFloorNumbers();
}

function upFloor(){
  var floor = $(this).parent().parent();
  floor.insertAfter(floor.next());
  refreshFloorNumbers();
}

function sendEdit(){
 var rewards = [];
  $(".floor").each(function(){
    var contents = [];
    $(this).find('.content').each(function(){
      var content = {
        'productType': $(this).find('.product-type-in').val(),
        'quantity':  parseInt($(this).find('.quantity-in').val(), 10),
      }
      contents.push(content);
    });
    var conditions = [];
    $(this).find('.condition').each(function(){
      var condition = {
        'niceName': $(this).find('.condition-name-in').val(),
        'metricType': $(this).find('.metric-type-in').val(),
        'operator':  $(this).find('.operator-in').val(),
        'expected': parseInt($(this).find('.expected-in').val(), 10),
      }
      conditions.push(condition);
    });
    var reward = {
      'rewardID': parseInt($(this).find('.reward-id-in').val(), 10),
      'sequence': $(this).index(),
      'niceName': $(this).find('.name-in').val(),
      'description': $(this).find('.description-in').val(),
      'contents' : contents,
      'conditions': conditions,
    };
    rewards.push(reward);
  });
  var data = {
    'name': $('.group-name-in').val(),
    'groupID': $('.group-id-in').val(),
    'start': $('.date-start-in').val()+'T00:00:00Z',
    'expiration': $('.date-expiration-in').val()+'T23:59:59Z',
    'rewards': rewards,
  };
  $(".b64encoded-json").val(btoa(JSON.stringify(data)))
  $("#editGroupForm").submit();
}

/**** End editGroupMethods *****/

function initialize() {
  var inputs = [
    'rwds-create-start-date',
    'rwds-create-expiration',
    'rwds-search-start-date',
    'rwds-search-end-date'
  ];
  inputs.forEach(function(entry) {
    // Setup calendar
    new Pikaday({
      field: document.getElementById(entry),
      format: 'YYYY-MM-DD'
    });
  });
  $(".readonly").keydown(function(e){
    e.preventDefault();
  });
  $('#rwds-search-table').DataTable({
    destroy: true,
    dom: 'B<"clear">lfrtip',
    bFilter: true,
    pageLength: 50,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
    buttons: [
      {
        extend: 'csv',
        text: 'Export to CSV',
        charset: 'utf-8',
        extension: '.csv',
        fieldSeparator: ';',
        fieldBoundary: '',
        filename: reportFilename,
        bom: true
      },
      {
        extend: 'excel',
        text: 'Export to Excel',
        filename: reportFilename
      }
    ],
    drawCallback:checkContent
  });
}

function load(){
  $(document).ready(initialize);
}

dynamicallyLoadCSS("//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css");
dynamicallyLoadCSS("/css/Rewards.css");
dynamicallyLoadCSS("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.css");
dynamicallyLoadScript("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.js", load);
