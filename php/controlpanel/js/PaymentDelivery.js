$(document).ready(function() {
    $(".pd_reject_sell").each(function(){
        $(this).submit(function(e) {
            var values = $(this).serializeArray();
            var orderAdTitle = getParam(values, "orderAdTitle");
            return confirm("Est�s seguro de rechazar la orden '" + orderAdTitle + "' ?");
        });
    });
    function getParam(params, searchParam){
        var value = "";
        $.each(params, function(i, fd){
            if(fd.name == searchParam) 
                value = fd.value;
        })
        return value;
    }
});
//EOF
