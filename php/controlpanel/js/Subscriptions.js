$(document).ready(function() {
    $(function() {
        $('.inputDate').each(function(index, element) {
            new Pikaday({
                      field: element,
                      position: 'bottom left',
                      reposition: false,
                      format: 'YYYY-MM-DD'
            });

        })
    });
    function checkInvoiceParameters() {
      // gets a json from blockets template and finds if there are the keys needed to generate an invoice
      // if so returns true else returns false  and alert will be displayed with items that needs to be completed 
        keys_to_find = ['rut', 'name',
                        'address', 'region',
                        'commune', 'lob',
                        'contact'];
        data = JSON.parse($("#acc_data").html());
        not_founds = [];
        keys_to_find.forEach(function(o) {
            data.hasOwnProperty(o) ? true : not_founds.push(o)
        });
        if (not_founds.length >= 1) {
            not_founds_txt = "Datos faltantes para poder crear factura: \n\n";
            not_founds.forEach(function(o) {
                not_founds_txt += "\t- " + o + "\n";
            });
            alert(not_founds_txt);
            return false;
        }
        return true;
    };

    function validateEmail(email) {
        // simple email validator based on a regex, if doesnt fit just disables submit button else billingType checker
        // function will be called to check if those fields are ok then enabling or disabled submit button.
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (re.test(String(email).toLowerCase()) === true) {
            checkBillingType(callback=false);
            $("#adminEmailError").html("");
        } else {
            $("#create").attr('disabled','disabled');
            if (email!='') {
                $("#adminEmailError").html("Formato no v�lido!").css("color", "red");
            } else {
                $("#adminEmailError").html("");
            }
        }
    }

    function checkBillingType(doValidateEmail) {
        // checks if billing type select matchs with its requirements, basically if bill type and email is ok
        // enables submit button else if an invoice is needed, check invoice parameters function will be called
        // to check if items needed are already filled.
        if (doValidateEmail === undefined) {
            doValidateEmail = true;
        }
        if ($("select[name='billingType']").val() == "invoice" && 
            checkInvoiceParameters() === false) {
            $("#create").attr('disabled','disabled');
        } else {
            $("#create").removeAttr('disabled');
            if (doValidateEmail) {
                validateEmail($("input[name='adminEmail']").val());
            }
        }
    }
    $("select[name='billingType']").change(function() {
        checkBillingType();
    });
    $("input[name='adminEmail']").change(function() {
        validateEmail($("input[name='adminEmail']").val());
    });
});
//EOF
