/*
 * AJAX
 */
function ajax_request(dest, post, callback, params, evaluate, method) {
	var xmlhttp = false;

	if (method == null){
		method = "POST";
	}

	if (typeof evaluate == "undefined")
		evaluate = true;

	try {
		xmlhttp = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
	} catch (e) {
		// browser doesn't support ajax. handle however you want
		// XXX ? callback(false, xmlhttp, params);
	}

	if (xmlhttp !== false) {
		xmlhttp.onreadystatechange = function () { ajax_callback(callback, params, xmlhttp, evaluate); };
		xmlhttp.open(method, dest, true);

		/* Set the transport enconding to utf, we want PHP to iconv from utf.... */
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
		xmlhttp.send(post);
	}
}

function ajax_callback(callback, params, xmlhttp, evaluate) {
	if (xmlhttp.readyState == 4) {
		if (xmlhttp.status == 200 && xmlhttp.responseText.indexOf('<!DOCTYPE') < 0) {
			if (evaluate)
				callback(eval("(" + xmlhttp.responseText + ")"), xmlhttp, params);
			else
				callback(xmlhttp.responseText, xmlhttp, params);
		} else {
			callback(false, xmlhttp, params);
		}
	}
}

function ajax_redirect( xmlhttp ) {
	/*
	 * Since we can't intercept 302 responses from the server
	 * indicating that something strange has happened (eg.
	 * that we need to log in again) because xmlhttp automagically
	 * follows the redirect, we try to detect that this has happened
	 * through finding a "<!DOCTYPE" in the document and replacing
	 * our body with what appears to be the body of the document
	 * we got.
	 */
	var newbody = xmlhttp.responseText;
	newbody = newbody.replace(/(.|\n)*\<body\>/, "");
	newbody = newbody.replace(/\<\/body\>[^<]*\<\/html\>[^<]*$/m, "");
	document.body.innerHTML = newbody;
	return;
}

/*
 * ADQUEUE REVIEWING
 */
function check_commit(result, xmlhttp, form) {
	var button = false;
	var message = false;

	for (var i = 0; i < form.elements.length; i++) {
		var temp = form.elements[i];
		if (temp.name && temp.name == 'commit') {
			button = temp;
		}
	}

	if (result) {
		if (navigator.userAgent.toLowerCase().indexOf('safari') > 0) {
			/* Safari doesn't allow colors and stuff in buttons, and the white text is too hard to read */
			button.type = "text";
			button.style.color="#000";
			button.onfocus = function() { this.blur(); };
			button.onclick = function() { commit_ad(this.form); };
		}
		if (result.status == 1) {
			button.className = "AjaxCommitOk";
			button.value = result.message;
		} else {
			message = document.createElement('div');
			message.innerHTML = result.error_message;
			message.className = "error";

			if (result.status == 0)
				button.disabled = false;
			button.className = "AjaxCommitError";
			button.value = result.message;
			button.parentNode.appendChild(message);
		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp);

	} else {
		if (navigator.userAgent.toLowerCase().indexOf('safari') > 0) {
			button.type = "text";
			button.style.color="#000";
		}
		button.className = "AjaxCommitError";
		button.value = js_info['TECHNICAL_ERROR'];
	}
	if (button.disabled) {
		for (i = 0; i < form.elements.length; i++) {
			form.elements[i].disabled = true;
		}
		for (i = 0; i < form.childNodes.length; i++) {
			var temp = form.childNodes[i];
			if (temp.tagName == "TABLE") {
				temp.style.background = "#DCDCC3";
				break;
			}
		}
	}
}

/*
	After taking an action in an ad, it sends a Xiti hit.
*/
function xiti_hit(form) {
	var ad_id = '';
	var ad_status = -1;

	for (var i = 0; i < form.elements.length; i++) {
		var temp = form.elements[i];

		/* Pick xiti hit values, if any */
		if (temp.name=='ad_id') {
			ad_id=temp.value;
		} else if (temp.name=='queue_action') {
			if (temp.checked)
				ad_status=(temp.value=="accept") ? 3 : 2;
		}
	}

	if (ad_id && ad_status!=-1) {
		var amount = (ad_status==3) ? 1 : 0 ;
		var xiti_hit_url = xitivars['document']+'.xiti.com/hit.xiti?s='+xitivars['site']+
			'&typehit=updateorder+&guid='+xitivars['guid']+'&cmd='+ad_id+'&st='+ad_status+'&roimt='+amount;
		var fakeimg = '<img width="1" height="1" id="fakeimg" src="'+xiti_hit_url+'" >';

		ajax_request(xiti_hit_url, '', null, form, null, 'GET');
	}
	return false;
}

function commit_ad(form) {
	var get = form.action;
	var post = '';
	var button = false;

	for (var i = 0; i < form.elements.length; i++) {
		var temp = form.elements[i];

		if (temp.name && ((temp.type != "radio" && temp.type != "checkbox") || temp.checked)) {
			if (temp.name == 'commit')
				button = temp;
			post += temp.name + '=' + encodeURIComponent(temp.value) + '&';
		}
	}

	post += 'ajax=';
	button.disabled = true;
	ajax_request(get, post, check_commit, form);
	window.scrollTo(0, findPosY(button));

	return false;
}

function check_bid_moderation(result, xmlhttp, form) {
	var message = false;

	if (result) {
		var button_active = document.getElementById("bid_active_" + result['bid_id']);
		var button_refused = document.getElementById("bid_refused_" + result['bid_id']);

		if (result['trans_bid_moderate_status'] == "TRANS_OK") {
			if (result['moderation'] == 'active') {
				if (result['trans_bid_mail_status'] == "TRANS_OK")
					button_active.className = "bid_ok";
				else
					button_active.className = "bid_warn";

				button_active.value = js_info_cp['BID_ACCEPTED'];
			} else {
				button_refused.value = js_info_cp['BID_REFUSED'];
				button_refused.className = "bid_ok";
			}
			button_active.disabled = true;
			button_refused.disabled = true;
		} else if (result['trans_bid_moderate_status'] == 'ERROR_BID_TOO_SMALL') {
			button_active.value = js_info_cp['BID_TOO_LOW'];
			button_active.className = "bid_err";
			button_active.disabled = true;
		} else	{
			if (result['moderation'] == 'active') {
				button_active.value = js_info['ERR'];
				button_active.className = "bid_err";
			} else {
				button_refused.value = js_info['ERR'];
				button_refused.className = "bid_err";
			}
			button_active.disabled = true;
			button_refused.disabled = true;
		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp);
	}
}

function send_bid_moderation(get) {
	var post = '';

	ajax_request(get, post, check_bid_moderation, null, false);

	return false;
}

function sms_adwatch_action_cb(result, xmlhttp, args) {
	var e = args.e;

	if (result) {
		var color_arr = {'0':'blue', '1':'green', '2':'red'};
		var color = color_arr['0'];
		if (result['result']) {
			color = color_arr[result['result']];
		} 
		e.innerHTML = result['message'];
		e.style.color = color;
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp);
	}
}

function sms_adwatch_action(e, action, unique_id, query_id) {
	var get = '?m=sms&a=search_ajax&action='+action+'&unique_id='+unique_id+'&query_id='+query_id;
	ajax_request(get, null, sms_adwatch_action_cb, { "e":e });

	return false;
}

function sms_adwatch_user_status_cb(result, xmlhttp, args) {
	var link = args.link;

	if (result) {
		var color_arr = {'0':'blue', '1':'green', '2':'red'};
		var color = color_arr['0'];
		if (result['result']) {
			color = color_arr[result['result']];
		}
		link.style.color = color;
		if (result['message'])
			link.innerHTML = result['message'];
		else {
			label = document.getElementById('status_label');
			if (label && result['sms_user_status']) {
				label.innerHTML = sms_user_statuses[result['sms_user_status']] + ':';
				if (result['sms_user_status'] == 'adminpaused') {
					link.innerHTML = sms_user_statuses['activate'];
					link.setAttribute('new_status', 'active');
				} else {
					link.innerHTML = sms_user_statuses['adminpause'];
					link.setAttribute('new_status', 'adminpaused');
				}
			}
		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp);
	}
}

function sms_adwatch_user_status(link, sender) {
	var get = '?m=sms&a=log_ajax&sender=' + sender + '&new_status=' + link.getAttribute('new_status');
	ajax_request(get, null, sms_adwatch_user_status_cb, { "link" : link });
}

/*
 * LAYER FUNCTIONALITY
 */
function showHideLayers(e, obj_id, display, check_id_isnot_obj) {
	/* If we hide the object then really check if we are outside the layer */
	if (display == 'none') {
		if (!e) var e = window.event;
		var tg = (window.event) ? e.srcElement : e.target;

		if (tg.id != obj_id && check_id_isnot_obj) return;

		var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
		while (reltg && reltg != tg && reltg.nodeName != 'BODY')
			reltg = reltg.parentNode;
		if (reltg == tg) return;
	}

	var obj = document.getElementById(obj_id);
	obj.style.display = display;
}

/*
 * NOTICES
 */

function save_notice_cb(resp, xmlhttp, note_div) {
	var note_children = note_div.getElementsByTagName('div');

	var head_div = note_children[0];
	var text_div = note_children[1];
	var foot_div = note_children[2];

	head_div.innerHTML = js_info['SAVED'];
}

function save_notice(note_div) {
	var note_children = note_div.getElementsByTagName('div');

	var head_div = note_children[0];
	var text_div = note_children[1];
	var foot_div = note_children[2];

	var url = "/controlpanel?m=search&a=setnotice";

	var postdata = "";
	postdata += "body=" + encodeURIComponent(text_div.getElementsByTagName('textarea')[0].value);
	if (queryString('search_type') == 'email') {
		postdata += "&email=" + encodeURIComponent(queryString('q'));
	} else {
		postdata += "&uid=" + encodeURIComponent(queryString('q'));
	}
	ajax_request(url, postdata, save_notice_cb, note_div)
}

var label_count = 0;
function edit_notice(note_div, type, abuse, priv_notice_abuse, show_save_button, uid) {
	var note_children = note_div.getElementsByTagName('div');

	var head_div = note_children[0];
	var text_div = note_children[1];
	var foot_div = note_children[2];
	var text = text_div.innerHTML.replace(/<br *\/?>/ig, "");

	head_div.innerHTML = js_info_cp['EDIT_NOTICE'];
	html = "<textarea name='body_" + type + "' rows='5'>" + text + "</textarea>";
	if (type != "ad" && priv_notice_abuse) {
		html += "<br /><input type='checkbox' name='abuse_" + type + 
			"' value='1' id='abuse_" + label_count + "' onClick='flag_abuse_notice(this.parentNode.parentNode, this.checked)' " + (abuse == 1 ? "checked" : "") + "/>" +
			"<label for='abuse_" + label_count + "'>" +  js_info_cp['ABUSE_NOTICE'] + "</label>";
	}

	html += "<br />";

	if (show_save_button) {
		html += "&nbsp;<input type='button' name='save_notice' onClick='save_notice(this.parentNode.parentNode)' value='" +  js_info['SAVE'] + "' />";
	} else {
		html += "<input type='checkbox' name='del_" + type + "' value='1' id='del_" + label_count + "' />" +
			"<label for='del_" + label_count + "'>"  + js_info['DELETE'] + "</label>" + 
			"<input type='hidden' name='notice[" + type + "]' value='edit' />";
	}
	if (uid) {
		if (type == "uid")
			html += "<input type='hidden' name='uid' value='" + uid + "' />";
		else
			html += "<input type='hidden' name='user_id' value='" + uid + "' />";
	}

	text_div.innerHTML = html;
	foot_div.innerHTML = "";
	label_count++;	
}

	function flag_abuse_notice(note_div, alert) {
		if (alert)
			note_div.className = 'Notice abuse';
		else
			note_div.className = 'Notice';
	}

function new_notice(newnotice_div, type, priv_notice_abuse, uid) {
	if (type == 'ad')
		longtype = js_info_cp['AD_NOTICE'];
	if (type == 'email')
		longtype = js_info_cp['EMAIL_NOTICE'];
	if (type == 'uid')
		longtype = js_info_cp['USER_GROUP_NOTICE'];

	html =
		"<div class='Notice' id='noticediv_" + type + "'>\n" +
		"<div class='NoticeHeader'>" +  js_info['NEW'] + " " + longtype + "</div>\n" + 
		"<div class='NoticeText'><textarea name='body_" + type + "' rows='5'></textarea>\n";
	if (priv_notice_abuse) {
		if (type == "ad") {
			html += "<br />";
		} else {
			html += "<br /><input type='checkbox' name='abuse_" +
				type + 
				"' value='1' id='abuse_" +
				label_count +
				"' onClick='flag_abuse_notice(this.parentNode.parentNode, this.checked)' />" +
				"<label for='abuse_"
				+ label_count +
				"'>" +  js_info_cp['ABUSE_NOTICE'] + "</label>";
		}
	}
	if (uid) {
		if (type == "uid")
			html += "<input type='hidden' name='uid' value='" + uid + "' />";
		else
			html += "<input type='hidden' name='user_id' value='" + uid + "' />";
	}
	html += "</div><div class='NoticeFooter'><input type='hidden' name='notice[" + type + "]' value='new' /></div>\n" +
		"</div>";
	newnotice_div.innerHTML = html;
	label_count++;
}


function category_has_changed(category, cat_obj, sel_id) {
	if (category != cat_obj.value)
		document.getElementById("category_changed"+sel_id).value = 1;
	else	
		document.getElementById("category_changed"+sel_id).value = 0;
}

function company_remove(categoryId, features) {
	var privateButton = document.getElementById("company_ad_0_" + categoryId);
	var companyButton = document.getElementById("company_ad_1_" + categoryId);

	/* Check if company_subscription exists in array */
	for(var element in features) {
		if(features[element] == "company_subscription") {
			companyButton.disabled = true;
			privateButton.checked = true;	
			return;
		}
	}

	companyButton.disabled = false;
	return;

}

function checkbox_toggle_all(check) {
	var form = document.forms[0];
	var i = 0;

	for (i = 0; i < form.length; i++) {
		var element = form[i];
		element.checked = check;
	}
}

/* WORD HIGHLIGHTING */

function multiple_checkbox(parent) {
	var form = document.forms[0];
	var i = 0;

	for (i = 0; i < form.length; ++i) {
		var element = form[i];
		var cat_id = element.id.substring(0, element.id.indexOf('_'));
		var par_id = parent.id;

		par_id = par_id.substring(par_id.indexOf('_') + 1);

		if (cat_id == par_id) {
			element.checked = parent.checked;
			if (element.onclick)
				multiple_checkbox(element);
		}
	}
}

function update_color_border() {
	var form = document.forms[0];
	var i = 0;

	for (i = 0; i < form.length; i++) {
		var element = form[i];

		if (element.type == 'radio' && element.name == 'color') {
			if (element.checked) {
				element.parentNode.className = 'ad_border_solid_black';
			} else {
				element.parentNode.className = 'ad_border_solid_grey';
			}	
		}
	}

}

var global_color;

/* 
 * Set the initial color of the marked keyword 
 */
function set_color(color) {
	global_color = color;
}

/*
 * Update style of marked keyword 
 */
function update_keyword(keyword, color, boxed) {

	/* Set keyword preview */
	var container = document.getElementById("keyword_place");
	if (keyword)
		container.innerHTML = keyword;

	/* Decide whether we should have a border or not */
	if (typeof(boxed) == 'undefined' &&  (container.className != 'WordBorderSolid' || boxed == 0) || boxed == 0)
		boxed = 0;
	else
		boxed = 1

			/* A color is clicked */
			if (color) {
				global_color = color;
				container.style.color = color;
				container.style.borderColor = color;
			} 


	/* Show border is clicked */
	if (boxed == 1) {
		container.className = 'WordBorderSolid';
		container.style.color= '';	
	} else {
		alert(container.style.borderColor);
		container.style.color = global_color; 
		container.className = '';
	}
}

function handle_radio(e, o) {
	e = e || window.event;

	e.cancelBubble = true;
	if (e.stopPropagation) e.stopPropagation();

	switch(e.type) {
	case 'keydown':
		if (e.keyCode != 13) return;
		var targ;
		if (e.target) targ = e.target;
		else if (e.srcElement) targ = e.srcElement;
		if (targ.nodeType == 3) // defeat Safari bug
			targ = targ.parentNode;
		var radio = targ.id.substring(0, targ.id.length - 3);
		document.getElementById(radio).checked = true;
		/* no 'break', it must follow the normal 'click' behavior */
	case 'click':
		var inputs = document.getElementsByName("queue_action");
		var i = 0;
		while (inputs[i]) {
			if (inputs[i] && inputs[i].type) {
				if (inputs[i].checked) {
					inputs[i].parentNode.style.backgroundColor = '#dcdcc3';
					inputs[i].parentNode.style.border = '2px inset #ccc';
				}
				else {
					inputs[i].parentNode.style.backgroundColor = '';
					inputs[i].parentNode.style.border = '2px outset #ccc';
				}
				i++;
			}
		}
		break;			
	case 'mouseover':
		o.style.backgroundColor='#dcdcc3';
		break;
	case 'mouseout':
		if (!o.childNodes[1].checked && !o.childNodes[0].checked)	
			o.style.backgroundColor='';
		break;
	default:
		break;
	}
}

function checkSelect(ddSel, ddUnsel, id) {
	var sel = document.getElementById(ddSel + '_' + id + '_dd');
	var unsel = document.getElementById(ddUnsel + '_' + id + '_dd');
	unsel.value = "";
	if (sel.value != "") {
		document.getElementById(ddSel + '_' + id).checked = true;
	} else {
		document.getElementById('queue_action_none_' + id).checked = true;
	}
}

function resetSelect(id) {
	document.getElementById('acceptEdit_' + id + '_dd').value = "";
	document.getElementById('refuse_' + id + '_dd').value = "";
	var reasonAcceptDiv = document.getElementById("reasonAcceptDetail_" + id);
	reasonAcceptDiv.innerHTML = "";
 	reasonAcceptDiv.style.display = 'none';
	var reasonDiv = document.getElementById("reasonDetail_" + id);
	reasonDiv.innerHTML = "";
 	reasonDiv.style.display = 'none';
}

function openLargeWin(url) {
	var params = 'width='+(window.screen.availWidth-10)+',height='+(window.screen.availHeight-60)+',left=0px,top=0px,resizable=yes,status=yes,scrollbars=yes';
	window.open(url, '_blank', params);
}

function inArray(needle, arr) {
	if (typeof(arr) != 'object')
		return false;

	var i;
	for (i = 0; i < arr.length; i++)
		if (arr[i] == needle)
			return true;
	return false;
}

function toggleRadio(id, state, except_list) {
	var radiogroup = document.getElementById(id).getElementsByTagName('input');
	var first_not_disabled = -1;
	var checked_disabled = false;

	for (var i = 0; i < radiogroup.length; i++) {
		if (radiogroup[i].type != 'radio') continue;
		radiogroup[i].disabled = inArray(radiogroup[i].id, except_list)?state:!state;
		if (first_not_disabled < 0 && !radiogroup[i].disabled)
			first_not_disabled = i;
		if (radiogroup[i].checked && radiogroup[i].disabled)
			checked_disabled = true;
	}

	if (checked_disabled && first_not_disabled >= 0) {
		radiogroup[first_not_disabled].checked = true;
	}
}

function show_city(region, city_select, city_cont) {
	var city = document.getElementById(city_select);

	if (city_cont.indexOf(':') == -1) {	
		var city_row = document.getElementById(city_cont);
		var city_display = "";
	} else {
		var city_disp_arr = city_cont.split(':');
		var city_row = document.getElementById(city_disp_arr[0]);
		var city_display = "inline";
	}
	city.options.length = 1;

	if (typeof(region_cities[region]) == "undefined") {
		city_row.style.display = "none";	
		return;
	}

	for (var i in region_cities[region]) {
		city.options[i] = new Option(region_cities[region][i], i);	
		if (city.value == region_cities[region][i]) city.options[i].selected = true;
	}

	city_row.style.display = city_display;	
	city.focus();
}

function check_imported(import_types, import_partner) {
	var import_types = import_types.split(',');
	var elements = document.getElementById('import_type'+import_types[0]).parentNode.getElementsByTagName('input');

	for(var i = 0; i < elements.length; i++) {
		elements[i].checked = isInArray(elements[i].name.replace('import_type', ''), import_types);
	}
}

function show_response(partner, message, color) {
	var elem = document.getElementById(partner + "_message");
	
	if (elem) {
		elem.innerHTML = message;	
		elem.style.color = color;	
	}	
}

/*
 * Store functions
 */
function setUsername(val) {
	var comma = val.indexOf(',');

	if (comma != -1)
		val = val.substring(0, comma);

	if (val.indexOf('@') == -1)
		return;

	/* Remove spaces */
	val = val.replace(/\s+/g, '');

	document.getElementById('username').value = val;
	document.getElementById('username_dummy').value = val;
}

function gen_url_path() {
	var name = document.getElementById('name').value;

	name = name.replace(/ AB$/, '');
	name = name.replace(/[����]/g, 'a');
	name = name.replace(/[��]/g, 'o');
	name = name.replace(/[��]/g, 'e');
	name = name.replace(/ +/g, '-');
	name = name.toLowerCase();
	name = name.replace(/[^a-z0-9-]/g, '');

	document.getElementById('url_path').value = name;
}	

/*
 * Util functions
 */
function rand_passwd(id) {
	var chars = "abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXY23456789";
	var passwd_elem = document.getElementById(id);

	var pwd = "";
	for (i = 0; i < 6; i++)
		pwd += chars.charAt(Math.floor(Math.random() * chars.length));
	passwd_elem.value = pwd;
}

function select_all(form) {
	var boxes = form.getElementsByTagName('input');
	for (i = 0; i < boxes.length; i++)
		if (boxes[i].type == 'checkbox')
			boxes[i].checked = true;
}

function deselect_all(form) {
	var boxes = form.getElementsByTagName('input');
	for (i = 0; i < boxes.length; i++)
		if (boxes[i].type == 'checkbox')
			boxes[i].checked = false;
}

function check_input_latin() {
	var arr = [];

	arr[0] = document.getElementsByTagName('textarea');
	arr[1] = document.getElementsByTagName('input');

	var res = true;

	for (var ai = 0; ai < arr.length; ai++) {
		var elements = arr[ai];

		for (var i = 0; i < elements.length; i++) {
			var elem = elements[i];
			var text = elem.value;
			var newtext = text.replace(/[\x80-\x9F\u0100-\uFFFF]/g, "??");
			if (newtext != text) {
				alert("'"+elem.name+"' " + js_info_cp['BAD_CHARS_REPLACED']);
				elem.value = newtext;
				res = false;
			}
		}
	}

	return res;
}



var isMenu      = false ;
var overpopupmenu = false;

function mouseSelect(e) {
	var ie  = document.all
		var ns6 = document.getElementById&&!document.all
		var obj = ns6 ? e.target.parentNode : event.srcElement.parentElement;
	if( isMenu )
	{
		if( overpopupmenu == false ) {
			isMenu = false ;
			overpopupmenu = false;
			document.getElementById('menudiv').style.display = "none" ;
			return true;
		}
		return true ;
	}
	return true;
}

function click_links(name, _this) {
	var pattern = new RegExp("^" + name + "_");
	var links = document.links;
	var queue = new Array();

	_this.disabled = true;

	for (var i = 0; i < links.length; i++) {
		if (pattern.test(links[i].id)) {
			 queue.push(links[i]);
		}	
	}

	pop_click_links(queue);
}

function pop_click_links(queue) {
	var link = queue.shift();
	if (link) {
		link.innerHTML = "Importeras, var god v�nta";
		link.style.color = "#BE7D0F";
		setTimeout(function () { if (typeof link.onclick == 'function') link.onclick(); else link.click(); pop_click_links(queue); }, 1000);
	}	
}	

// begins editing an item
function beginEdit(div, field, id, ev)
{
	if (!ev || !ev.altKey) return;
	var elm = document.getElementById(div + '_' + id);
	var inp = document.getElementById(field + '_' + id);
	if (!elm) return;
	if (!inp) return;
	elm.style.display = 'none';
	inp.name = field;
	inp.style.display = 'block';
}

function scarface_update_listing_cb(result, xmlhttp, rubbish) {
	if (xmlhttp.responseText) {
		if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
			document.getElementsByTagName("body")[0].innerHTML = xmlhttp.responseText;
		} else {
			document.getElementById("report_list_div").innerHTML = xmlhttp.responseText;
		}
	}
	else {
		document.getElementById("report_list_div").innerHTML = "No reports found or error happened";
	}
}

function scarface_update_listing(list_id, report_type, ad_id) {
	ajax_request('?m=scarface&a=reports_list&list_id=' + list_id + "&report_type=" + report_type + "&ad_id=" + ad_id, null, scarface_update_listing_cb, null, false);
}

function scarface_resolve (report_id, report_type, list_id, action, ad_id) {
	var post = '';
	var category_changed = '0';
	var notify_type = '';
	original_category  = document.getElementById("original_category"+ad_id+'_').value;	
	selected_category = document.getElementById("category_group"+ad_id+'_').options[document.getElementById("category_group"+ad_id+'_').selectedIndex].value;	
	original_type  = document.getElementById("original_type"+ad_id+'_').value;	
	selected_type = document.getElementById("type"+ad_id+'_').options[document.getElementById("type"+ad_id+'_').selectedIndex].value;	
	category_changed = ( ($('#currency_changed_msg'+ad_id).is(":visible")==true) || (original_category!=selected_category)||(original_type!=selected_type)) ? '1' : '0';

	if (action == 'accept_delete_and_warn' && !confirm("Are you sure? This option will send a mail warning to everyone who contacted this person!")) {
		return false;
	}

	if (action == 'accept' || action == 'accept_and_delete' || action == 'reject' || action == 'accept_and_refuse' || action == 'accept_delete_and_warn' || action == 'accept_and_notify_owner' || action == 'reject_and_notify_reporter') {

		original_company_ad  = document.getElementById("original_company_ad"+ad_id+'_').value;	
		company_ad = document.getElementById("company_ad_1_"+ad_id+'_').checked;
		if (report_type == 'wrong_category' && action == 'accept' &&  category_changed == '0' && company_ad == original_company_ad) {
			alert('You must change the ad category before accept the wrong category report');

		} else {
			var category =  document.getElementById('category_group' + ad_id + '_').value;
			var type =  document.getElementById('type' + ad_id + '_').value;
			var companyButton = document.getElementById("company_ad_1_" + ad_id + '_');
			var company_ad = '';
			var price = document.getElementById('price' + ad_id + '_');
			var price_query_string = '';
			if(price){
				price_query_string = '&price='+price.value;	
			}
			if (companyButton.checked == true)
				company_ad = '1';
			else 
				company_ad = '0';
			
			if (action == 'accept_and_notify_owner') {
				var notify_select =  document.getElementById('notify_owner_' + ad_id);
				if(notify_select == null || notify_select.value == '') {
					alert('You must select an email option for the owner of the ad');
					return false;
				}
				notify_type = '&notify_type=' + notify_select.value;
			}
			if (action == 'reject_and_notify_reporter') {
				var notify_select =  document.getElementById('notify_reporter_' + ad_id);
				if(notify_select == null || notify_select.value == '') {
					alert('You must select an email option for the reporter');
					return false;
				}
				notify_type = '&notify_type=' + notify_select.value;
			}

			post ='category='+ category + '&type=' + type + '&company_ad=' + company_ad + price_query_string; 

			ajax_request('?m=scarface&a=resolve&list_id=' + list_id + '&report_id=' + report_id + '&action=' + action + '&report_type=' + report_type + notify_type, post, scarface_resolve_cb, { "list_id":list_id, "report_type":report_type, "ad_id":ad_id });
		}
	}

	return false;
}

function scarface_resolve_cb(result, xmlhttp, data) {
	if (result) {
		if (result.status == 'true' ) {
			/*No action defined*/
			scarface_update_listing(data.list_id, data.report_type, data.ad_id);
		} else {
			/* Error display */	
			var error_div =  document.getElementById('error_report');
			var error_msg =  document.getElementById('err_msg');
			error_msg.innerHTML = result.error_message;
			error_div.style.display = 'block';
		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp); 
	}
}


function scarface_send_reply (report_id, report_type, list_id, ad_id) {
	var post = '';
	var reply = document.getElementById("reply_text_"+report_id).value;
	var text_entered = (reply != '');

	if (!text_entered) {
		alert('You must enter some message before submitting a reply');
	} else {
		post = 'reply=' + reply;
		ajax_request('?m=scarface&a=askinfo&report_id=' + report_id + '&list_id=' + list_id + '&report_type=' + report_type, post, scarface_send_reply_cb, { "list_id":list_id, "report_type":report_type, "ad_id":ad_id });
	}

	return false;
}

function scarface_send_reply_cb(result, xmlhttp, data) {
	if (result) {
		if (result.status == 'true' ) {
			/*No action defined*/
			scarface_update_listing(data.list_id, data.report_type, data.ad_id);
		} else {
			/* Error display */	
			var error_div =  document.getElementById('error_report');
			var error_msg =  document.getElementById('err_msg');
			error_msg.innerHTML = result.error_message;
			error_div.style.display = 'block';
		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		return ajax_redirect(xmlhttp);
	}
}

function add_to_list (select_id, value) {
	var list_id = document.getElementById(select_id).value;
	var list_selected = (list_id != '');

	if (!list_selected) {
		alert('Select list to add');
	} else {
		ajax_request('?m=filter&a=edit_list_item&json_out=t&notice=Added%20from%20adqueues&list_id='+list_id+'&value='+value, '', add_to_list_cb, { "list_id":list_id, "value":value },true,'GET');
	}
}

	function add_to_list_cb(result, xmlhttp, data) {
		if(result.success)
			alert(data.value + " added!");
		else
			alert(result.errors);
	}

function landingPage_checkAllIps(id) {
        f = document.getElementById(id);

        for (i = (f.elements.length - 1); i > 0; i--) {
                e = f.elements[i];
                if ((e.name != 'ip_filter[]') || (e.disabled)){ continue; }
                e.checked = true;
        }
}

jQuery(document).delegate('.currency-type', 'change', setCurrencyValue);
jQuery(document).delegate('.ad-price', 'keypress', validatePrice);
jQuery(document).delegate('.ad-price', 'keyup', updateOtherCurrency);
jQuery(document).delegate('.ad-price', 'focus', deleteUFPriceFormat);
jQuery(document).delegate('.ad-price', 'blur', formatUFPrice);

function validatePesosPrice(evt) {
  var expectedRegex = /(?:^\d{0,10}$)/;
  var charCode = evt.which;
  var inputChar = String.fromCharCode(charCode);
  var priceText = evt.currentTarget.value + inputChar;
  // Firefox trick evt.which=0 => null char evt.which=8 => backspace
  var isEmptyChar = charCode === 0 || charCode === 8;

  if (!expectedRegex.test(priceText) && !isEmptyChar) {
    evt.preventDefault();
  }
};

function validatePrice(evt) {
  var $ct = jQuery(evt.currentTarget);
  var $form = $ct.closest('.ad-review');
  var currency = $form.find('.currency-type:checked').val();
  if (currency === 'uf') {
    Yapo.validateUFPrice(evt);
  } else {
    validatePesosPrice(evt);
  }
}
function formatUFPrice(evt) {
  //Calls function in Yapo here, because its not available on load to delegate
  Yapo.formatUFPrice(evt);
}

function deleteUFPriceFormat(evt) {
  //Calls function in Yapo here, because its not available on load to delegate
  Yapo.deleteUFPriceFormat(evt);
}

function convertTo(currency, price) {
  return currency === 'uf'
    ? Yapo.convertPesosToUF(uf_conversion_factor, price)
    : Yapo.convertUFToPesos(uf_conversion_factor, price);
}

function processPrice(evt, sameCurrency) {
  var $ct = jQuery(evt.currentTarget);
  var $form = $ct.closest('.ad-review');
  var $priceSelector = $form.find('.ad-price');
  var price = $priceSelector.val().replace(/\./g, '');
  var currency = $form.find('.currency-type:checked').val();

  var currencyPrefix;
  var convertedPrice = 0;
  var $prefixSelector = null;

  if (sameCurrency) {
    currencyPrefix = currency === 'uf' ? 'UF ' : '$ ';
    convertedPrice = convertTo($ct.val(), price);
    $prefixSelector = $form.find('.currency-prefix');
  } else {
    currencyPrefix = currency === 'peso' ? 'UF ' : '$ ';
    convertedPrice = convertTo(currency === 'peso'? 'uf' : 'peso', price);
    $priceSelector = $form.find('.other-price');
  }

  return {
    price : {
      selector: $priceSelector,
      value: parseFloat(price.replace(/,/g, '.')) > 0 ? Yapo.setMoneyFormat(convertedPrice) : ''
    },
    prefix : {
      selector: $prefixSelector,
      value: currencyPrefix
    }
  };
}

/* Changes value of price when changing currency type */
function setCurrencyValue(evt) {
  var priceChanges = processPrice(evt, true);
  var price = priceChanges.price.value;
  var prefix = priceChanges.prefix.value;
  priceChanges.price.selector.val(price);
  priceChanges.prefix.selector.text(prefix);
  updateOtherCurrency(evt);
}

/* Updates other currency value when changing price */
function updateOtherCurrency(evt) {
  var priceChanges = processPrice(evt, false);
  var price = priceChanges.price.value;
  var prefix = priceChanges.prefix.value;
  prefix = price !== "" ? prefix : "";
  priceChanges.price.selector.text(prefix + price);
}

/* Check when we move in/from a category with UFs */
function check_currency(e, args) {

	/* XXX: Test thoroughly with multiple visible ads */
	var sel_id = this.getAttribute("data-sel_id");
	var cat_id = form_key_lookup('category', sel_id);
	var price_id = 'price'+sel_id ;

	if (document.getElementById(price_id) == undefined ||
		document.getElementById('original_category'+sel_id) == undefined) {
		/* If there is no price, neither an original cat, do not do anything */
		return;
	}

	var original_cat_id = document.getElementById('original_category'+sel_id).value ;
	var currency_id = split_setting(get_settings("currency", form_key_lookup, category_settings, sel_id))["id"] ;
	var old_currency_id = "peso" ;

	if (category_settings["currency"]["1"][original_cat_id] != undefined && 
		category_settings["currency"]["1"][original_cat_id]["value"] != undefined ) {
		old_currency_id = split_setting(category_settings["currency"]["1"][original_cat_id]["value"])["id"] ;
	}
	if (old_currency_id==currency_id) {
		$("#currency_changed_msg"+sel_id).css('display', 'none');
		return; /* Nothing to do, move along...*/
	}

	/* If new currency is "uf", show equivalency, else hide it */
	$('#currency_changed_msg'+sel_id).css('display', 'block');

	var current_price = document.getElementById(price_id) ;
	var f_price = parseFloat(current_price.value.replace(/\./g, '').replace(/,/, '.'));
	var f_conversion_factor = parseFloat(uf_conversion_factor.replace(/,/g, '.'));

	if (currency_id=="uf") {
		/* Right now price shown is in pesos, so we need to divide and round */
		new_price = f_price / f_conversion_factor ;
		document.getElementById('currency_input_label'+sel_id).innerHTML = "Precio: UF";
		$('#currency_converted'+sel_id).css('display', 'block');
		//$('#currency_converted'+sel_id).innerHTML = "&nbsp;$;&nbsp;(" + current_price.value + ")" ;
		document.getElementById('currency_converted'+sel_id).innerHTML = "&nbsp;$&nbsp;(" + current_price.value + ")" ;
		current_price.value = new_price.toFixed(2).replace(/\./g, ',');

		if (!document.getElementById('currency'+sel_id)) {
			document.getElementById('currency_input_label'+sel_id).innerHTML = document.getElementById('currency_input_label'+sel_id).innerHTML + "<input type=\"hidden\" id=\"currency"+sel_id+"\" name=\"currency\" value=\"uf\" />";
		}

		document.getElementById('currency'+sel_id).value="uf";
	} else {
		/* Right now price shown is in UFs, so we need to multiply and round */
		new_price = f_price * f_conversion_factor ;
		document.getElementById('currency_input_label'+sel_id).innerHTML = "Precio: $";
		$('#currency_converted'+sel_id).css('display', 'none');
		current_price.value = Math.round(new_price);

		if (!document.getElementById('currency'+sel_id)) {
			document.getElementById('currency_input_label'+sel_id).innerHTML = document.getElementById('currency_input_label'+sel_id).innerHTML + "<input type=\"hidden\" name=\"currency\""+sel_id+" value=\"\" />";
		}
		document.getElementById('currency'+sel_id).value="";
	}
	document.getElementById('original_category'+sel_id).value = cat_id;
}

function setTime(){
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	// add a zero in front of numbers<10
	m=checkTime(m);
	document.getElementById('current-time').innerHTML=h+":"+m;
	t=setTimeout(function(){setTime()},500);
	document.getElementById('last-hour').innerHTML=h + ":00";
}

function checkTime(i){
	if (i<10){
  		i="0" + i;
  	}
	return i;
}

function actionToApplyProduct(obj){
	id = obj.getAttribute("id").replace("apply_product_","");
	product_select = document.getElementById("products_to_apply_"+id);
	product_name = product_select.options[product_select.selectedIndex].text;
	confirmation = confirm("�Est�s seguro que deseas hacer '"+product_name+"' a este aviso sin pagar?");
	if(confirmation){
		url = "?m=Services&a=admin&ad_id="+id+"&cmd="+product_select.value;
		window.open(url,"_blank");
	}
}

function confirmStoreCreation(obj){
	return confirm("�Est�s seguro que deseas crear esta tienda sin pagar?");
}

function confirmStoreExtension(obj){
	return confirm("�Est�s seguro que deseas extender esta tienda sin pagar?");
}

function confirmMassiveRefuse(obj){
  if (document.getElementById("refuse_reason").value === "") {
    alert("Selecciona una raz�n de rechazo")
    return false;
  }
  document.getElementById("massive_op").value = "refuse";
  return true;
}
