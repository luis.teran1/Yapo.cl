$(document).ready(function() {
  // Setup calendar
  new Pikaday({ 
    field: document.getElementById('expiration_date'),
    format: 'DD/MM/YYYY'
  });

  // Assignpack module
  $('#pack_assign_buttom').length && $('#pack_assign_buttom').attr('disabled', 'disabled');

  //pripro module
  $('#submit_pro_accounts').click(function() {
    $('#form_pro_accounts input[type=hidden]').remove();
    var cont = 0;

    $.each($('#table_account_pripro input[type=checkbox]'), function(i, value ) {
      var account_id = value.getAttribute('data-account');
      var pro_type = value.getAttribute('data-type');
      var pro_type_id = value.getAttribute('value');
      var initial_state = value.getAttribute('data-initial');

      if (value.checked != initial_state) {
        $('<input>').attr({
          type: 'hidden',
          name: 'pro_accounts['+cont+']',
          value: account_id
        }).appendTo('#form_pro_accounts');

        $('<input>').attr({
          type: 'hidden',
          name: 'pro_types['+cont+']',
          value: pro_type + '-' + pro_type_id
        }).appendTo('#form_pro_accounts');

        $('<input>').attr({
          type: 'hidden',
          name: 'pro_actions['+cont+']',
          value: (value.checked? 1:0)
        }).appendTo('#form_pro_accounts');
        cont++;
      }
    });
  });

  $('#form_pro_accounts').submit(function( event ) {
    if ($('#form_pro_accounts input[type=hidden]').length) {
      return;
    }
    event.preventDefault();
  });

  setTimeout(function() {
    $('#pack_message').fadeOut();
  }, 3000);

  if ($('#packs').length > 0) {
    var chosen;
    var activate;
    var changeClass;

    var $serviceOrder = $('#service_order');
    var $serviceOrderLabel = $('#service_order_label');

    var $serviceAmount = $('#service_amount');
    var $serviceAmountLabel = $('#service_amount_label');

    $('#packs').change(function() {

      var getFormatedDate = function(date) { 
        return date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
      };
      
      var code = $('#packs').val().substr(1,4);
      var today = new Date();
      var $expirationDate = $('#expiration_date');
      var $pack_slots = $('#pack_slots');

      if (code == '0000') {
        $('#pack_slots,#expiration_date').attr('readonly', '');
        $expirationDate.val(getFormatedDate(today));
        $pack_slots.val('');
      } else {
        var slotsType = code[0];
        var expirationDate = null;
        
        switch (slotsType) {
          case '1': // mensual
            expirationDate = new Date(today.getFullYear(), today.getMonth() + 1 ,today.getDate());
            break;
          case '2': // trimestral
            expirationDate = new Date(today.getFullYear(), today.getMonth() + 3 ,today.getDate());
            break;
          case '3': // semestral
            expirationDate = new Date(today.getFullYear(), today.getMonth() + 6 ,today.getDate());
            break;
          case '4': // anual
            expirationDate = new Date(today.getFullYear() + 1, today.getMonth() ,today.getDate());
            break;
        }
        
        $expirationDate.val(getFormatedDate(expirationDate));
        
        $pack_slots.val(parseInt(code.substr(1)));
        
        $('#pack_slots, #expiration_date').attr('readonly', true);
      }
      enableAssignButton();
    });
  }

  var $packSlots = $('#pack_slots');
  if ($packSlots.length > 0) {

    $packSlots.change(function() { 
      enableAssignButton(); 
    });
  }

  var $expirationDate = $('#expiration_date');

  if ($expirationDate.length > 0) {
    $expirationDate.change(function(){ 
      enableAssignButton(); 
    });
  }

  var enableAssignButton = function() {
    var code = $('#packs').val().substr(1,4);
    var slots = $packSlots.val();
    var expiration_date = $expirationDate.val();

    if (code == '0000') {
      if (slots > 0 && expiration_date.length > 0){
        $('#pack_assign_buttom').attr('disabled', '');
      } else {
        $('#pack_assign_buttom').attr('disabled', 'disabled');
      }
    } else {
      $('#pack_assign_buttom').attr('disabled', '');
    }
  };

  var linkExpireOnClick = function(e) {
    if (confirm(messages.EXPIRE_CONFIRMATION_MESSAGE)){
      window.location = e.target.getAttribute('data-href');
    }
    return false;
  };

  if ($('.expire_link').length > 0){
    $('.expire_link').click(function (e) { 
      return linkExpireOnClick(e);
    });
  }

});
//EOF
