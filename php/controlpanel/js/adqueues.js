/* global get_settings, category_features, form_key_lookup,
 cp_cat_extra_pars */

function showType(sel_id) { // eslint-disable-line no-unused-vars
  var privateButton = document.getElementById('company_ad_0_' + sel_id);
  var companyButton = document.getElementById('company_ad_1_' + sel_id);
  var selectedType = document.getElementById('type' + sel_id).options[document.getElementById('type' + sel_id).selectedIndex].value;
  var settings = get_settings('company_subscription', form_key_lookup,
    category_features, sel_id);

  if (settings == '1') {
    companyButton.disabled = true;
    privateButton.checked = true;
  } else {
    companyButton.disabled = false;
  }

  if (selectedType == 'k' || selectedType == 'h')
    document.getElementById('buy_label').style.display = 'inline';
  else
    document.getElementById('buy_label').style.display = 'none';
}

function on_category_change(ad_id) { // eslint-disable-line no-unused-vars
  var mainDiv = document.getElementById('catparams_' + ad_id);
  if(mainDiv) {
    var selected_category = document.getElementById('category_group' + ad_id)
      .options[document.getElementById('category_group'+ad_id)
      .selectedIndex].value;
    var original_category = document.getElementById('original_category' + ad_id)
      .value;

    var cat_extra_params = cp_cat_extra_pars[selected_category];

    var adCategoryGroup = document.getElementById('category_group' + ad_id);
    var isreq = adCategoryGroup
      .options[adCategoryGroup.selectedIndex]
      .dataset.reqprice;

    var adPrice = document.getElementById('price'+ad_id);
    if (isreq) {
      adPrice.setAttribute("required", true);
    } else {
      adPrice.removeAttribute("required");
    }
    if(cat_extra_params && (original_category != selected_category)) {
      var div_content = '';
      for ( var i in cat_extra_params) {
        var curr_param = cat_extra_params[i];
        var type = curr_param.type;
        var name = curr_param.name;
        var title = curr_param.title;

        div_content += '<div>';
        div_content += '<h2>' + title + '</h2>';
        switch(type) {
        case 'checkbox':
          var list_name = curr_param.list;
          var list = eval(list_name);
          div_content += write_checkbox_param(type, name, list, ad_id);
          break;
        }
        div_content += '</div>';
      }
      mainDiv.innerHTML = div_content;
      if (selected_category == '7020' || selected_category == '7040' ||
          selected_category == '7060') {
        document.getElementById('type'+ad_id).value = 's';
      }
    } else {
      mainDiv.innerHTML = '';
    }
	checkCurrencies(selected_category, ad_id);
  }
}

function checkCurrencies(cat, ad_id) {
	var currencySelector = document.querySelector('.currency_' + ad_id);
	var otherPrice = document.querySelector('.other_price_' + ad_id);
	var peso = document.getElementById('currency_peso_' + ad_id);
	var cond = category_settings["currency"]["1"][cat];

	if (!cond || cond["value"].search("uf:1") == -1) {
		peso.click();
		currencySelector.style.display = "none";
		otherPrice.style.display = "none";
	} else {
		currencySelector.style.display = "block";
		otherPrice.style.display = "inline";
	}
}

function write_checkbox_param(type, name, list, ad_id) {
  var div_content = '<ul>';
  for ( var i in list) {
    var list_item = list[i];
    var field_name = name + '[' + i + ']';
    var field_id = name + '_' + i + '_' + ad_id;
    div_content += '<li><input type="checkbox" name="' + field_name + '" id="' +
      field_id + '" >';
    div_content += '<label for="'+ field_id +'">'+ list_item +'</label></li>';
  }
  div_content += '</ul>';
  return div_content;
}

function handler_set_first_change(e, t) { // eslint-disable-line no-unused-vars
  var check = t.form['set_first[]'];
  var delete_images = t.form['delete_images[]'];
  for(var i=0; i<check.length; ++i) {
    check[i].checked=t==check[i]&&t.checked;
    if(t==check[i] && t.checked && delete_images[i].checked) delete_images[i].checked=false;
  } 
}

function handler_delete_images_change(e, t) { // eslint-disable-line no-unused-vars
  var check = t.form['set_first[]'];
  var delete_images = t.form['delete_images[]'];
  for(var i=0; i<check.length; ++i) {
    if(t==delete_images[i] && t.checked && check[i].checked) check[i].checked=false;
  } 
}

function next_image(obj, ad_id) { // eslint-disable-line no-unused-vars
  var parent = $(obj).closest('form');
  var thumbnailSrc = obj.src.replace('images', 'thumbs');
  var currentThumbnail = $('img[src=' + thumbnailSrc + ']', parent);
  var nextThumbnail = currentThumbnail.closest('.ad-image').next().find('img');
  var nextSrc;

  if (!nextThumbnail.length) {
    nextThumbnail = $('.ad-image:first-child img', parent);
  }

  nextSrc = nextThumbnail.attr('src').replace('thumbs', 'images');
  obj.src = nextSrc;
}

(function() {
  var debounceScroll = debounce(listenWheel, 250, true);
  var currentImage;

  $(document).ready(function() {
    $('.ad_thumb').click(showImage);
    $('.ad-delete-all').change(toggleDeleteAll);
    $('.ad_pict').hover(disableScroll, enableScroll);
    $(document).keypress(listenKeyCommands);
  });


  function prevImage(image) {
    var src = image.attr('src').replace('images', 'thumbs');
    var parent = image.closest('form');
    var thumbnail = $('img[src=' + src + ']');
    var prevThumbnail = thumbnail.closest('.ad-image').prev().find('img');
    var prevSrc;

    if (!prevThumbnail.length) {
      prevThumbnail = $('.ad-image:last-child img', parent);
    }

    prevSrc = prevThumbnail.attr('src').replace('thumbs', 'images');
    image.attr('src', prevSrc);
  }


  function showImage(event) {
    var thumbnail = event.target;
    var parent = $(thumbnail).closest('form');
    var image = $('.ad_pict img', parent);

    if (thumbnail.tagName !== 'IMG') thumbnail = thumbnail.querySelector('img');
    image.attr('src', thumbnail.src.replace('thumbs', 'images'));
  }


  function toggleDeleteAll(event) {
    var input = $(this);
    var parent = input.closest('form');
    var inputs = $('[name="delete_images[]"]', parent);
    var isChecked = event.target.checked;

    inputs = inputs.not(function(i, element) {
      return element.checked === isChecked;
    });

    inputs.trigger('click');
  }


  function listenKeyCommands(event) {
    if (event.which === 95) {
      $('.ad_pict img').trigger('click');
    }
  }


  function listenWheel(e) {
    if (e.wheelDelta / 120 > 0) {
      currentImage.trigger('click');
    } else {
      prevImage(currentImage);
    }
  }


  function preventDefault(e) {
    debounceScroll(e);
    e = e || window.event;
    if (e.preventDefault) e.preventDefault();
    e.returnValue = false;
  }


  function disableScroll() {
    currentImage = $('img', $(this));

    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = preventDefault;
    document.onmousewheel = preventDefault;
    window.ontouchmove  = preventDefault; // mobile
  }


  function enableScroll() {
    window.onwheel = null;
    window.onmousewheel = null;
    document.onmousewheel = null;
    window.ontouchmove = null;
  }


  function debounce(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this;
      var args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;

      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  }
})();
