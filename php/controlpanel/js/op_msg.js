$(document).ready(function() {
	// Setup calendar
	new Pikaday({ 
		field: document.getElementById('expiration_date'),
		format: 'DD/MM/YYYY'
	});
});
//EOF
