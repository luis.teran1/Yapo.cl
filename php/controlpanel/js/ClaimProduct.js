function dynamicallyLoadScript(url, callback){
    //url is URL of external file, callback is the code
    //to be called from the file
    var scriptTag = document.createElement('script');
    scriptTag.src = url;
    scriptTag.onload = callback;
    scriptTag.onreadystatechange = callback;
    document.head.appendChild(scriptTag);
};

function dynamicallyLoadCSS(url) {
  var fileref=document.createElement("link");
  fileref.setAttribute("rel", "stylesheet");
  fileref.setAttribute("type", "text/css");
  fileref.setAttribute("href", url);
  document.head.appendChild(fileref);
}

function reportFilename(){
  var email =  $("#clpr-search-email").val();
  if (email != ""){
    return 'claim_product_report_'+email;
  } else {
    return 'claim_product_report';
  }
}

function checkContent(settings){
  var api = this.api();
  tot = api.data().length;
  if (tot > 0) {
    api.buttons().enable();
  } else {
    api.buttons().disable();
  }
}

function initialize() {
  var inputs = [
    'crsl-assign-expiration',
    'crsl-report-start-date',
    'crsl-report-end-date'
  ];
  inputs.forEach(function(entry) {
    // Setup calendar
    new Pikaday({
      field: document.getElementById(entry),
      format: 'YYYY-MM-DD'
    });
  });
  $(".readonly").keydown(function(e){
    e.preventDefault();
  });
  $('.clpr-table-list  [data-toggle="toggle"]').change(function(){
      $(this).parents().next('.hide').toggle();
      if($(this).is(":checked")){
        $("label[for='"+$(this).attr('id')+"']").text('\u229F');
      }else{
        $("label[for='"+$(this).attr('id')+"']").text('\u229E');
      }
   });
  $('.clpr-config-list input').each(function(){
     if ($(this).val() == '0' || $(this).val() == '') {
          console.log($(this).val())
          $(this).parent().addClass("hide");
     }
  });
  var searchTable = $('#clpr-search-table').DataTable({
    destroy: true,
    dom: 'B<"clear">lfrtip',
    bFilter: true,
    lengthMenu: [[10, 25, 50, -1], [10, 25, 50, "All"]],
    columnDefs: [
      {
        targets: [7],
        type: 'string',
        render: function(data, type, full, meta){
          if (type === 'filter' || type === 'sort') {
	    var api = new $.fn.dataTable.Api(meta.settings);
	    var td = api.cell({row: meta.row, column: meta.col}).node();
	    data = $('select, #claim_status', td).val();
          }
          return data;
        }
      }
    ],
    buttons: [
      {
        extend: 'csv',
        text: 'Export to CSV',
        charset: 'utf-8',
        extension: '.csv',
        fieldSeparator: ';',
        fieldBoundary: '',
        filename: reportFilename,
        bom: true
      },
      {
        extend: 'excel',
        text: 'Export to Excel',
        filename: reportFilename
      }
    ],
    drawCallback:checkContent
  });
  $('#clpr-search-table').on('change', 'tbody select, tbody #claim_status', function(){
    searchTable.cell($(this).closest('td')).invalidate();
    // Redraw table (optional)
    // table.draw(false);
  });

}

function load(){
  $(document).ready(initialize);
}

dynamicallyLoadCSS("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.css");
dynamicallyLoadScript("//cdn.datatables.net/v/dt/jqc-1.12.4/jszip-2.5.0/dt-1.10.20/b-1.6.1/b-html5-1.6.1/datatables.min.js", load);
