/* Js for administrate promotional pages banners*/

(function() {

  function imgSubmit(e) {
    e.preventDefault();
    var imageForm = document.getElementById("img_form");
    var request = new XMLHttpRequest();
    var formData = new FormData();
    formData.append('img', this.files[0]);
    formData.append('arg',
      $('[name=platform]:checked').val() + "." +
      $('[name=position]:checked').val()
    )

    request.onloadend = function(r) {
      var status = r.currentTarget.status;
      var response = r.currentTarget.response;
      if (status == 200) {
        var data = JSON.parse(response);
        if (data.status == "ok") {
          var imagesUrl = $("#img_container").attr('data-imgdir');
          var dir = data.file.slice(0, 2);;
          var urlInput = '/ppageimages/' + dir + '/' + data.file;
          var imageUrl = imagesUrl + urlInput;
          $("#img").attr('src', imageUrl);
          $("#img_container").show();
          $("#image").val(urlInput);
          $("#message h2").text("Imagen cargada");
        } else {
          $("#message h2").text(data.status);
          $("#img_container").hide(1000);
          $("#image").val("");
        }
      }
    };

    request.open("POST", "controlpanel?m=PPages&a=uploadImg");
    request.send(formData);
  };

  function imgClean() {
    bannerImg = $("#banner_img");
    $("#image").val("");
    $("#img_container").hide(1000);
    if (bannerImg.length) {
      bannerImg.val("");
    }
  };

  function displayImageSize() {
    var plat = $('[name="platform"]:checked:visible').val();
    var pos  = $('[name="position"]:checked:visible').val();
    $('.img-size').hide();
    if (plat && pos) {
      var position = plat+':'+pos;
      $('.img-size[data-position="'+position+'"]').show();
    }
  }

  function posChange(evt) {
    var value = evt.target.value;
    if (value === "listing") {
      $("#listing, #region, #image_div").css("display", "block");
      $("input[type=checkbox]").removeAttr("disabled");
    } else {
      $("#listing, #region").css("display", "none");
      $("#image_div").css("display", "block");
      $("input[type=checkbox]").attr("disabled", "disabled");
    }
    $("#img_form #pos").remove();
    $("#img_form").append('<input type="hidden" id="pos" name="pos" value="'+value+'">');
    imgClean();
    displayImageSize();
  };

  function platChange(evt){
    var value = evt.target.value;
    var pos  = $('[name="position"]:checked:visible').val();
    $(".position").css("display", "none");
    $(".position.__"+value).css("display", "block");
    if (typeof pos !== "undefined") {
      $('[value="'+ pos +'"]:visible').click();
    }
    $("#img_form #plat").remove();
    $("#img_form").append('<input type="hidden" id="plat" name="plat" value="'+value+'">');
    imgClean();
    displayImageSize();
  };


  function regionsChange() {
    if($(this).is(":checked")) {
      $("#region input[type=checkbox]").attr("checked", "checked");
    } else {
      $("#region input[type=checkbox]").removeAttr("checked");
    }
  };

  function getPpageInfo(ppId) {
    $.ajax({
      url: '/controlpanel?m=PPages&a=ppageInfo',
      dataType: 'json',
      data: {
      pp_id: ppId,
      },
      success: function(data) {
        $("#listing").empty();
        Object.keys(data.categories).forEach(function(key){
          var dynDiv = document.createElement("DIV");
          var chBox = document.createElement("INPUT");
          var chText = document.createTextNode(data.categories[key]);
          chBox.setAttribute("type", "checkbox");
          chBox.setAttribute("name", "categories[]");
          chBox.setAttribute("value", key);
          $(dynDiv).append(chBox).append(chText);
          $("#listing").append(dynDiv);
        });
        if (edition) {
            fillInput(banner_categories);
            edition = false;
        }
      }
    });
  }

  function bannerSubmit() {
    var pp_id = $("#pp_id option:selected");
    var start_date = pp_id.attr("data-start-date");
    var end_date = pp_id.attr("data-end-date");
    var url = pp_id.attr("data-url");
    var form = $("#banner_form");
    form.append('<input name="pp_start_date" value="'+ start_date +'" type="hidden">');
    form.append('<input name="pp_end_date" value="'+ end_date +'" type="hidden">');
    form.append('<input name="url" value="'+ url +'" type="hidden">');
  };

  function fillInput(input) {
    if (typeof input !== 'undefined') {
      var values = input.split(",");
      for (i in values) {
        $('input[value="' + values[i] + '"]')
          .click();
      }
    }
  }

  function fillPage() {
    $('#pp_id')
      .val(banner_pp_id)
      .trigger('change');
    $('#start_date').val(banner_start_date);
    $('#end_date').val(banner_end_date);
    $('input[name=platform][value="'+ banner_platform + '"]')
      .click()
      .trigger('change');
    $('input[name=position][value="' + banner_position + '"]')[0]
      .click();

    fillInput(banner_regions);

    if (banner_image != "") {
      var imageUrl = $("#img_container").attr('data-imgdir') + banner_image;
      $("#img").attr('src', imageUrl);
      $("#img_container").show();
      $("#image").val(banner_image);
    }
  }

  $(document).ready(function() {
    $("input[type=radio][name=platform]").change(platChange);
    $("input[type=radio][name=position]").change(posChange);
    $("#all_regions").change(regionsChange);
    $("#send_button").click(bannerSubmit);
    $("#banner_img").change(imgSubmit);

    $("#pp_id").change(function(){
      getPpageInfo($(this).val());
    });

    $("#banner_img").change(function() {
      $("#img_form").submit();
    }).click(function() {
      $("#banner_img").val("");
    });

    if (edition) {
      fillPage();
    }
  });

})();

