var selected = true;
function checkSomethingSelected(){
  selected = true;
  var rows = $('table[border=1]').find('td').toArray();
  rows.forEach(checkSelected);
  if(!selected)
    alert('Debe seleccionar al menos un elemento en cada columna');
  return selected;
}

function checkSelected(item){
  var obj = $(item).find('input:checked');
  if(obj.length == 0){
    selected = false;
  }
}
$(document).ready(function (){
  $('#banner_selector').submit(checkSomethingSelected);
});
