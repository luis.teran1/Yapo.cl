/* globals extensionErrorImage, qq, errorImage, sizeErrorImage*/

function initUploadBtn(uploadButton, imageType, input, imageContainer, result) {
  var aiImagesButton = new qq.FineUploaderBasic({
    button: uploadButton,
    request: {
      inputName: 'image',
      endpoint: '/ppages/upload_image/0?image_type=' + imageType,
      forceMultipart: true
    },

    multiple: false,
    callbacks: {
      onSubmit: function(id, file) {
        if (!(file && /^.*\.(jpg|png|jpeg)$/i.test(file))) {
          result.text(extensionErrorImage);
          return false;
        }
      },

      onComplete: function(id, file, response) {
        var imagesUrl = $(input).attr('data-imgdir');
        var dir;
        var imageUrl;
        if (response.status && response.status === 'ok') {
          dir = response.file.slice(0, 2);
          imageUrl = imagesUrl + '/ppageimages/' + dir + '/' + response.file;
          imageContainer.attr('src', imageUrl);
          imageContainer.show(1000);
          $(input).val(response.file.slice(0, -4));
          result.text('Imagen cargada con �xito');
        } else {
          imageContainer.hide(1000);
          if (response.status === 'ERROR_IMAGE_SIZES') {
            result.text(sizeErrorImage);
          }
          if (response.trans.error) {
            result.text(errorImage);
          }
        }
      }
    }
  });

  return aiImagesButton;
}
