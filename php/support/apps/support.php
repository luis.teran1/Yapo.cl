<?php
/*
 * Support application
 */
require_once('autoload_lib.php');
require_once('dyn_config.php');
require_once('bImage.php');

/*
 * Syslog
 */
openlog("SUPPORT", LOG_ODELAY, LOG_LOCAL0);

class blocket_support extends blocket_application {
	var $page_title;
	var $page_name;
	var $headstyles;
	var $headscripts;
	var $support_id;
	var $support_data;
	var $ad_title;
	var $ad_name;
	var $list_id;
	var $meta_template;

	function blocket_support() {
		global $BCONF;
		global $session;

		$this->application_name = "support";
		$this->page_title = lang('PAGE_SUPPORT_TITLE');
		$this->page_name = lang('PAGE_SUPPORT_NAME');
		$this->headstyles = array('support.css');
		$this->headscripts[] = "common.js";
		$this->support_data = array();

		$this->init('list', bconf_get($BCONF, "*.common.session.instances.support"));
	}

	function get_state($statename) {
		switch($statename) {
		case 'list':
			return array('function' => 'support_list',
				     'params' => array('id' => 'f_integer'),
				     'method' => 'get');
		case 'form':
			return array('function' => 'support_form',
				     'params' => array('id' => 'f_integer', 'ad' => 'f_integer', 'mq_id' => 'f_integer', 'pwd' => 'f_passwd'),
				     'method' => 'get');
		case 'send':
			return array('function' => 'support_send_form',
				     'params' => array(array('f_data', 'name', 'email', 'support_subject', 'support_body',
						       'file', 'url', 'company', 'url', 'phone_called',
						       'when_called',
						       'address', 'zipcode', 'city', 'phone', 'employer', 'comment',
						       'phone_contact', 'mobile', 'answer1', 'answer2', 'answer3')
						       ),
				     'method' => 'post');
		case 'sent':
			return array('function' => 'support_sent_mail',
				     'method' => 'get',
				     'allowed_states' => array('sent'));
		}
	}

	function support_list($id) {
		if ($id)
			return 'form';

		header("Location: /ayuda/preguntas_frecuentes.html");
	}

	function support_form($id, $ad = null, $mq_id = null, $pwd = null) {
		global $BCONF;

		if ($id != 0) {
			/* Id is always != 0 first time entering a form. Clear existing data. */
			$this->support_data = array();
			$this->set_errors(array());
		} elseif (!$this->support_id) /* No page to display, redirect to list. */
			return 'list';

		if (!$this->has_error()) {
			$this->support_id = $id;
		}
		/* No subject selected.Hides direct trans errors*/

		if (!empty($ad)) {
			$transaction = new bTransaction();
			$transaction->add_data('list_id', $ad);
			$reply = $transaction->send_command('ad_status');
			if (!isset($reply['ad_status']) || $reply['ad_status']['0']['status'] != 'active') {
				$this->support_list(null);
				return;
			}
			$this->list_id = $ad;
		}
		
		if (($page_title = bconf_get($BCONF, "*.support.category.{$this->support_id}.page_title"))) {
			$this->page_title = lang($page_title);
		} else {
			$this->page_title = bconf_get($BCONF, '*.support.category.' . $this->support_id . '.name');
			if (empty($this->page_title))
				$this->page_title = lang('PAGE_SUPPORT_TITLE');
		}

		if (($page_name = bconf_get($BCONF, "*.support.category.{$this->support_id}.page_name"))) {
			$this->page_name = lang($page_name);
		} else  {
			$this->page_name = lang('PAGE_SUPPORT_NAME');
		}

		$this->headstyles = array ('support.css');
		if (($styles = bconf_get($BCONF, "*.support.category.{$this->support_id}.css"))) {
			foreach ($styles as $css)
				$this->headstyles[] = $css;
		}

		//reset scripts so we dont load scripts from previous support_id
		$this->headscripts = array ('common.js');
		if (($scripts = bconf_get($BCONF, "*.support.category.{$this->support_id}.js"))) {
			foreach ($scripts as $js)
				$this->headscripts[] = $js;
		}

		/* NOINDEX, NOFOLLOW for google robot */
		if (($meta_template = bconf_get($BCONF, "*.support.category.{$this->support_id}.meta_template")))
			$this->meta_template = $meta_template;
		else 
			$this->meta_template = NULL;

		/* CHUS 228: verify parameters for adreply abuse report */
		if ($id == 10) {	// 10 = id for adreply abuse form, change this so it is read from bconf
			if ($mq_id == null || $pwd == null) {
				$this->support_list(null);
				return;
			}
			$transaction = new bTransaction();
			$transaction->add_data('mail_queue_id', $mq_id);
			$transaction->add_data('action', 'get');
			$reply = $transaction->send_command('admin_mail_queue');
			if (!isset($reply['status']) || $reply['status'] != 'TRANS_OK') {
				$this->support_list(null);
				return;
			}
			$mq = $reply['result']['0'];
			if ($mq['ad_status'] != 'active' || $mq['state'] != 'sent') {
				$this->support_list(null);
				return;
			}

			if (!$this->check_password_hash($pwd, $mq_id . $mq['to'] . $mq['from'] )) {
				$this->support_list(null);
				return;
			}
			$transaction->reset();
			$transaction->add_data('mail_queue_id', $mq_id);
			$transaction->add_data('action', 'update');
			$transaction->add_data('state', 'abuse');
			$reply = $transaction->send_command('admin_mail_queue');

			/* mail of the supposed 'offender' to be used in the near future, in the 'report abuse' form */
			$this->support_data['email'] = $mq['from'];
		}

		$display_form = bconf_get($BCONF, "*.support.category.{$this->support_id}.template");
		$this->display_layout('support/' . $display_form, array_merge(array('id' => $this->support_id), $this->support_data));
	}

	function support_send_form($data) {
		global $BCONF;

		$transaction = new bTransaction();
		$this->support_data = $data;

		$errors = array ();

		// Set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc)
		ini_set("memory_limit", ($BCONF['*']['common']['image']['maxres'] * 4 * 2)."M");
		ini_set("max_execution_time", $BCONF['*']['common']['image']['max_execution_time']);

		if (@$_FILES['file']['error'] == UPLOAD_ERR_OK && strlen(@$_FILES['file']['name'])) {
			$image = new bImage($_FILES['file']['tmp_name']);

			// Resizes if too big
			if($image->_width >= 1024 || $image->_height >= 768) {
				$image->resize(1024, 768);
			}

			$image_buffer = $image->create_image();

			$transaction->add_file("file", $image_buffer);
			$transaction->add_data("filename_file", $_FILES['file']['name']);
			$image->destruct();
		} else if (strlen(@$_FILES['file']['name']) > 0) {
			syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
			$errors['file'] = 'ERROR_IMAGE_LOAD';
		}

		if (@$data['support_subject'] === false) {
                        $this->set_errors(array('support_subject' => 'ERROR_SUPPORT_SUBJECT_MISSING'));
                        return 'form';               
		}

		// Doesn't seem to be used anywhere, 05/12 by rcaldeira/rsouza
		$files = bconf_get($BCONF, "*.support.category.{$this->support_id}.file");
		if ($files) {
			foreach ($files as $num => $file) {
				$idx = 'file_' . $file['name'];
				if (@$_FILES[$idx]['error'] == UPLOAD_ERR_OK && strlen(@$_FILES[$idx]['name'])) {
					$transaction->add_file($idx, file_get_contents($_FILES[$idx]['tmp_name']));
					$transaction->add_file('filename_' . $file['name'], $_FILES[$idx]['name']);
				} else if (strlen(@$_FILES[$idx]['name']) > 0) {
					syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
					$errors[$idx] = 'ERROR_IMAGE_LOAD';
				}
			}
		}

		if (($filt_func = bconf_get($BCONF, "*.support.category.{$this->support_id}.filter_func")))
			$filt_func($data);

		/* If support subject doesn't exist add to data the subject key from the first subject associated to support id obteined from bconf  */
		if (!array_key_exists('support_subject', $data)  || empty($data['support_subject']) || is_null($data['support_subject']) ) {
			$data['support_subject'] = bconf_get($BCONF, "*.support.category.{$this->support_id}.subject.0");
		}

		/* IMPORTANT! 
			This foreach populate data in transaction overwriting all 
			parameters existing in with the same name of key in data 
		*/
		foreach ($data as $key => $value) {
			$transaction->add_data($key, $value);
		}

		if (!is_null($this->list_id))
			$transaction->add_data('list_id', $this->list_id);

		/* Removed mar08 FT4200. These should be added only if in featurelist in bconf 
		if (!is_null($this->ad_title))
			$transaction->add_data('ad_title', $this->ad_title);

		if (!is_null($this->ad_name))
			$transaction->add_data('ad_name', $this->ad_name);
		*/
		/* Use default for missing email */
		if ((!array_key_exists('email', $data)  || empty($data['email'])) &&
				bconf_get($BCONF, "*.support.category.{$this->support_id}.default.email"))   {
			$transaction->add_data('email', bconf_get($BCONF, "*.support.category.{$this->support_id}.default.email"));
		}

		if ($errors)
			$this->set_errors($errors);
		else { 
			$reply = $transaction->send_command('support', true, true);
			$this->set_errors($transaction->get_errors());
		}

		if ($this->has_error()){
			return 'form';
		}
		return 'sent';
	}

	function support_sent_mail() {
		global $BCONF;

		$data = array();
		$sent_template = bconf_get($BCONF, "*.support.category.{$this->support_id}.sent_template");
		if (!$sent_template)
			$sent_template = "sent.html";

		if (($page_title = bconf_get($BCONF, "*.support.category.{$this->support_id}.page_title"))) {
			$this->page_title = lang($page_title);
		}
		if (($page_name = bconf_get($BCONF, "*.support.category.{$this->support_id}.page_name"))) {
			$this->page_name = lang($page_name);
		}
		if (($page_name = bconf_get($BCONF, "*.support.category.{$this->support_id}.no_reply"))) {
			$data['no_reply'] = '1';
		}

		$this->display_layout("support/{$sent_template}", $data);

		return 'FINISH';
	}

	function check_password_hash($hash, $str) {
		if (!empty($hash) && !empty($str)) {
			$passwd_hashes = array(sha1($str));
			if (in_array($hash, $passwd_hashes)) {
				return true;
			}
		}
		return false;
	}

	function f_data($val) {
		$filtered_value = array();
		foreach ($val as $key => $value) {
			$filter = "f_" . $key;
			if (method_exists($this, $filter)) {
				$filtered_value[$key] = $this->$filter($value);
			}
			else {
				$filtered_value[$key] = $this->f_clean($value);
			}
		}
		return $filtered_value;
	}

	function f_clean($val) {
		if (!empty($val)) {
			$clean_val = strip_tags($val);
			$val = preg_replace("/[\(\)<>:;*#{}]/", "", $clean_val);

		}
		return $val;
	}

	function f_url($val) {
		if (!empty($val)) {
			$val = strip_tags($val);
		}
		return $val;
	}

	function f_support_body($val) {
		if (!empty($val)) {
			return clean_and_trim($val);
		}
		else {
			return NULL;
		}
	}

	function f_passwd($val) {
		return (string)$val;
	}

	function f_support_subject($val) {
		global $BCONF;
		if (	(!empty($this->support_id) 
			&& ( 1 == @$BCONF["*"]["support"]["category"][$this->support_id]["no_default_subject"] )
			) && (!isset($val) || empty($val)))  {
			return false;
		}
		return $val;
	}
}

$support = new blocket_support();
$support->run_fsm();
?>
