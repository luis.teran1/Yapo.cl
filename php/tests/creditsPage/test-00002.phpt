--TEST--
Test CreditsPage - addCreditsContent  - check credits content added to page
--FILE--
<?php

require_once 'autoload_lib.php';

$app = new CreditsPage();
$app->credits = 500001;
$app->credits_to_expire = 100001;
$app->expiration_date = "2016-12-31T00:00:01Z";
$app->addCreditsContent();
print_r($app->response);

?>
--EXPECT--
bResponse Object
(
    [data] => Array
        (
            [user_credits] => 500001
            [credits_to_expire] => 100001
            [expiration_date] => 31/12/2016
        )

    [extended_array] => Array
        (
        )

    [bconf_application] => 
    [_error_code] => ERROR_OK
)
