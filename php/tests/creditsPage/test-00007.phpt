--TEST--
Test CreditsPage - getDaysDiff - check response when "$end_date" is null.
--FILE--
<?php

require_once 'autoload_lib.php';

$app = new CreditsPage();

$diff = $app->getDaysDiff(null);
print_r($diff);

?>
--EXPECTF--
Array
(
    [0] => 0
    [1] => 
)
