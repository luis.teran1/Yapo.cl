--TEST--
Test CreditsPage - addCreditsContent  - check content added to creditsPage when credits=0
--FILE--
<?php

require_once 'autoload_lib.php';
include_once 'init.php';

$app = new CreditsPage();
$app->credits = 0;
$app->credits_to_expire = 0;

$date = new DateTime('+1 day');
$app->expiration_date = $date->format('Y-m-d');

$app->addCreditsContent();
print_r($app->response);

?>
--EXPECT--
bResponse Object
(
    [data] => Array
        (
            [l] => 0
            [get_l] => 0
            [user_credits] => 0
        )

    [extended_array] => Array
        (
        )

    [bconf_application] => 
    [_error_code] => ERROR_OK
)
