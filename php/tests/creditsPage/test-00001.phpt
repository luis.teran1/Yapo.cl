--TEST--
Test CreditsPage - addPageContent - check content added to page
--FILE--
<?php

require_once 'autoload_lib.php';
include_once 'init.php';

$_SERVER['REQUEST_URI'] = "/yapesos";
$_SERVER['HTTP_USER_AGENT'] = "testzila";
$_SERVER['REMOTE_ADDR'] = "127.0.0.1";

$app = new CreditsPage();
$app->addPageContent();
print_r($app->response);

?>
--EXPECT--
bResponse Object
(
    [data] => Array
        (
            [l] => 0
            [get_l] => 0
            [headstyles] => credits.css
            [headscripts] => Array
                (
                    [0] => /tjs/arrays_v2.js
                    [1] => credits.js
                )

            [page_title] => Yapesos | yapo.cl
            [page_name] => Yapesos
            [content] => accounts/credits.html
            [appl] => credits
            [mobile_appl] => credits
            [REQUEST_URI] => /yapesos
            [REMOTE_BROWSER] => testzila
            [REMOTE_ADDR] => 127.0.0.1
        )

    [extended_array] => Array
        (
        )

    [bconf_application] => 
    [_error_code] => ERROR_OK
)
