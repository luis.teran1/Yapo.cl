--TEST--
Test CreditsPage - addCreditsContent  - check "today" added to creditsPage
--FILE--
<?php

require_once 'autoload_lib.php';
include_once 'init.php';

$app = new CreditsPage();
$app->credits = 500001;
$app->credits_to_expire = 100001;
$app->expiration_date = date("Y-m-d");
$app->addCreditsContent();
print_r($app->response);

?>
--EXPECT--
bResponse Object
(
    [data] => Array
        (
            [l] => 0
            [get_l] => 0
            [user_credits] => 500001
            [credits_to_expire] => 100001
            [near_expiration] => 1
            [expiration_date] => Hoy
        )

    [extended_array] => Array
        (
        )

    [bconf_application] => 
    [_error_code] => ERROR_OK
)
