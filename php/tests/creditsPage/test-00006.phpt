--TEST--
Test CreditsPage - getDaysDiff - check the number of days between two dates.
--FILE--
<?php

require_once 'autoload_lib.php';

$app = new CreditsPage();
$date = new DateTime('+1 day');
$start = $date->format('Y-m-d');

$date = new DateTime('+20 day');
$end = $date->format('Y-m-d');

$diff = $app->getDaysDiff($end, $start);
print_r($diff);

?>
--EXPECTF--
Array
(
    [0] => 19
    [1] => %d/%d/%d
)
