--TEST--
Test VoucherDocument - validate - Verify throw exception when total missing
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../../regress_final';
$app = new VoucherDocument();
$app->doc_num = 10;
$app->user_data = array("Juanito", "10000-4", "24 de Junio de 2016", "Melipilla");
$app->detail_data = array();
$app->detail_data[] = array("INFO","1","1.000","1.000");
try{
    $error = $app->validate();
}catch (Exception $e) {
    print_r($e);
}
?>
--EXPECTF--
Exception Object
(
    [message:protected] => TOTAL_MISSING
    [string:Exception:private] => 
    [code:protected] => 0
    [file:protected] => %s/regress_final/include/VoucherDocument.php
    [line:protected] => 45
    [trace:Exception:private] => Array
        (
            [0] => Array
                (
                    [file] => %s/php/tests/voucher/test-00004.php
                    [line] => 15
                    [function] => validate
                    [class] => Yapo\VoucherDocument
                    [type] => ->
                    [args] => Array
                        (
                        )

                )

        )

    [previous:Exception:private] => 
)
