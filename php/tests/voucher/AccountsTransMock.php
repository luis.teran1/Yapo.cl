<?php
namespace Yapo\Accounts;

class Trans
{
    public static function getAccount($id)
    {
        $conf = array(
            1 => array("name" => "Daniberto", "rut"  => "100000-4", "commune" => 1, "region" => 1),
            2 => array("name" => "", "rut"  => "", "commune" => null, "region" => 1),
        );
        return $conf[$id];
    }
}
