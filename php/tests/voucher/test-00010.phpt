--TEST--
Test GetVoucher - getCommuneName - Verify commune name in text
--FILE--
<?php

namespace Yapo;

require_once 'BconfMock.php';

use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getCommuneName(1,1);
print_r($data);
$data = $app->getCommuneName(1,2);
print_r($data);
$data = $app->getCommuneName(2,2);
print_r($data);

?>
--EXPECT--
MelipillaTocopillaAntofagasta
