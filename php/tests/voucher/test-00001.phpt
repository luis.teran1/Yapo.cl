--TEST--
Test VoucherDocument - validate - Verify throw exception when doc_num missing
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../../regress_final';
$app = new VoucherDocument();
try{
    $error = $app->validate();
}catch (Exception $e) {
    print_r($e);
}
?>
--EXPECTF--
Exception Object
(
    [message:protected] => NUM_DOC_MISSING
    [string:Exception:private] => 
    [code:protected] => 0
    [file:protected] => %s/regress_final/include/VoucherDocument.php
    [line:protected] => 36
    [trace:Exception:private] => Array
        (
            [0] => Array
                (
                    [file] => %s/php/tests/voucher/test-00001.php
                    [line] => 11
                    [function] => validate
                    [class] => Yapo\VoucherDocument
                    [type] => ->
                    [args] => Array
                        (
                        )

                )

        )

    [previous:Exception:private] => 
)
