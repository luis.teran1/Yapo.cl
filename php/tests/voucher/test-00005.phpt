--TEST--
Test VoucherDocument - validate - Verify don't throw exception when all data is present
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../../regress_final';
$app = new VoucherDocument();
$app->doc_num = 10;
$app->user_data = array("Juanito", "10000-4", "24 de Junio de 2016", "Melipilla");
$app->detail_data = array();
$app->detail_data[] = array("INFO","1","1.000","1.000");
$app->total = 1000;
try{
    $error = $app->validate();
    echo "All validations success";
}catch (Exception $e) {
    print_r($e);
}

?>
--EXPECT--
All validations success