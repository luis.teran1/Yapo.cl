--TEST--
Test GetVoucher - getFormattedDate - Verify output date for pdf voucher
--FILE--
<?php

namespace Yapo;

class Bconf
{
    public static function bconfGet($BCONF, $key)
    {
        return "Febrero";
    }
}


use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getFormattedDate('2016-02-01 12:12:12.1232');
print_r($data);

?>
--EXPECT--
01 de Febrero de 2016
