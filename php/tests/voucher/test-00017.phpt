--TEST--
Test GetVoucher - populateVoucher - Verify data complete on v_doc
--FILE--
<?php

namespace Yapo;

require_once 'BconfMock.php';
require_once 'AccountsTransMock.php';
require_once 'PaymentTransMock.php';
require_once 'YapoPaymentAppMock.php';

use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$app->populateVoucher(4,"88ea39439e74fa27c09a4fc0bc8ebe6d00978392");
print_r($app->v_doc);

?>
--EXPECT--
Yapo\VoucherDocument Object
(
    [pdf:protected] => Yapo\PdfManager Object
        (
            [B] => 0
            [I] => 0
            [U] => 0
            [HREF] => 
            [page:protected] => 1
            [n:protected] => 2
            [offsets:protected] => 
            [buffer:protected] => 
            [pages:protected] => Array
                (
                    [1] => 2 J
0.57 w

                )

            [state:protected] => 2
            [compress:protected] => 1
            [k:protected] => 2.8346456692913
            [DefOrientation:protected] => P
            [CurOrientation:protected] => P
            [StdPageSizes:protected] => Array
                (
                    [a3] => Array
                        (
                            [0] => 841.89
                            [1] => 1190.55
                        )

                    [a4] => Array
                        (
                            [0] => 595.28
                            [1] => 841.89
                        )

                    [a5] => Array
                        (
                            [0] => 420.94
                            [1] => 595.28
                        )

                    [letter] => Array
                        (
                            [0] => 612
                            [1] => 792
                        )

                    [legal] => Array
                        (
                            [0] => 612
                            [1] => 1008
                        )

                )

            [DefPageSize:protected] => Array
                (
                    [0] => 210.00155555556
                    [1] => 297.00008333333
                )

            [CurPageSize:protected] => Array
                (
                    [0] => 210.00155555556
                    [1] => 297.00008333333
                )

            [CurRotation:protected] => 0
            [PageInfo:protected] => Array
                (
                )

            [wPt:protected] => 595.28
            [hPt:protected] => 841.89
            [w:protected] => 210.00155555556
            [h:protected] => 297.00008333333
            [lMargin:protected] => 10.00125
            [tMargin:protected] => 10.00125
            [rMargin:protected] => 10.00125
            [bMargin:protected] => 20.0025
            [cMargin:protected] => 1.000125
            [x:protected] => 10.00125
            [y:protected] => 10.00125
            [lasth:protected] => 0
            [LineWidth:protected] => 0.200025
            [fontpath:protected] => 
            [CoreFonts:protected] => Array
                (
                    [0] => courier
                    [1] => helvetica
                    [2] => times
                    [3] => symbol
                    [4] => zapfdingbats
                )

            [fonts:protected] => Array
                (
                )

            [FontFiles:protected] => Array
                (
                )

            [encodings:protected] => Array
                (
                )

            [cmaps:protected] => Array
                (
                )

            [FontFamily:protected] => 
            [FontStyle:protected] => 
            [underline:protected] => 
            [CurrentFont:protected] => 
            [FontSizePt:protected] => 12
            [FontSize:protected] => 
            [DrawColor:protected] => 0 G
            [FillColor:protected] => 0 g
            [TextColor:protected] => 0 g
            [ColorFlag:protected] => 
            [WithAlpha:protected] => 
            [ws:protected] => 0
            [images:protected] => Array
                (
                )

            [PageLinks:protected] => 
            [links:protected] => Array
                (
                )

            [AutoPageBreak:protected] => 1
            [PageBreakTrigger:protected] => 276.99758333333
            [InHeader:protected] => 
            [InFooter:protected] => 
            [AliasNbPages:protected] => {nb}
            [ZoomMode:protected] => default
            [LayoutMode:protected] => default
            [metadata:protected] => 
            [PDFVersion:protected] => 1.3
        )

    [logo:Yapo\VoucherDocument:private] => 
    [user_data] => Array
        (
            [0] => Daniberto
            [1] => 100.000-4
            [2] => 31 de Diciembre de 1969
            [3] => Melipilla
        )

    [detail_data] => Array
        (
            [0] => Array
                (
                    [0] => Bump - Dummy ad 2
                    [1] => 1
                    [2] => 5.000
                    [3] => 5.000
                )

        )

    [total] => 10.000
    [doc_num] => 4
)