--TEST--
Test GetVoucher - getFormattedRut - Valid rut
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getFormattedRut('15273155-6');
print_r($data);

?>
--EXPECT--
15.273.155-6
