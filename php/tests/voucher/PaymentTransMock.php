<?php
namespace Yapo\Payment;

class Trans
{
    public static function getPaymentItems($id)
    {
        switch ($id) {
            case 1:
                return array('timestamp' => '123123123', 'doc_type' => 'voucher');
            case 2:
                return array('timestamp' => '123123123', 'doc_type' => 'bill');
            case 3:
                return array('timestamp' => '123123124', 'doc_type' => 'voucher');
            case 4:
                return array(
                    'timestamp' => '123123123', 
                    'doc_type' => 'voucher', 
                    'amount' => '10000',
                    'account_id' => '1',
                    'detail_1' => array(
                        'price' => 5000,
                        'subject' => 'Dummy ad 1',
                        'product_id'=> 1
                    ),
                    'detail_1' => array(
                        'price' => 5000,
                        'subject' => 'Dummy ad 2',
                        'product_id'=> 1
                    )
                );
            default:
                return false;
        }
    }
}
