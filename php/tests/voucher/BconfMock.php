<?php 
namespace Yapo;

class Bconf
{
    public static function bconfGet($BCONF, $key)
    {
        $conf = array(
            "*.common.region.1.commune.1.name" => "Melipilla", 
            "*.common.region.1.commune.2.name" => "Tocopilla", 
            "*.common.region.2.commune.2.name" => "Antofagasta",
            "*.language.DATE.MONTHS.12.long.es"=> "Diciembre",
            "*.payment.product.1.name" => "Bump"
        ); 
        return $conf[$key];
    }
}
