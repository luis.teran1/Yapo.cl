--TEST--
Test GetVoucher - getFormattedRut - without "-"
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getFormattedRut('152731556');
print_r($data);

?>
--EXPECT--
152.731.556
