--TEST--
Test VoucherDocument - generate - Verify don't throw exception when all data is present
--FILE--
<?php

namespace Yapo;
use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new VoucherDocument(false);
$app->doc_num = 10;
$app->user_data = array("Juanito", "10000-4", "24 de Junio de 2016", "Melipilla");
$app->detail_data = array();
$app->detail_data[] = array("INFO","1","1.000","1.000");
$app->total = 1000;
try{
    $error = $app->validate();
    $app->generate();
}catch (Exception $e) {
    print_r($e);
}

?>
--EXPECTHEADERS--
Content-Type: application/pdf
Content-Disposition: inline; filename="voucher_10.pdf"
Cache-Control: private, max-age=0, must-revalidate
Pragma: public
--EXPECTF--

%PDF-1.3
3 0 obj
<</Type /Page
/Parent 1 0 R
/Resources 2 0 R
/Contents 4 0 R>>
endobj
4 0 obj
<</Filter /FlateDecode /Length 861>>
stream
x��V�n�F��+�2Yd2��R��-9& E6��:$�%)���V w����E7$gxyι�!��J���.�����ᦡ��2�����}H��(�N��➒��nm�� ���Қ8ѯ��x�E���RX�X���l��r}��\���=$�5ݗ���!�O�~=Xx.nw`�$L4\O��"��x��u<�k?�$���z�~=X��9g�iN
5���?�h������7�^1��p�'����-�o��U�J����Pfp	�g�D`�Q�ҴUu�O���w���� �ԗJ��0�އ��sM�	8⁃1M8In��!��	�9�� �rw��S2Gv<G� Ͱ}��k��I"ݯek�-b��l�-�l�W���t3H��m��C^�N���1D��`�w<�b<�s�h��x1��g��%���N��D߸����o�����DjP8�P��xp���(�H�����]t"�F,���Og��w��"J�XY�)�ͨN���=��~���+E���η�v }c=�[���! {(���wZ�A�K%�M�٥�,��#Ф���[��E�:"�i4�i�*g{����4�-�4�!�n�@�4|-�4|��e�F(��>Z�t� J�e�:��FyE-���l��q��p���uv�J�v8n0��̏��X���KV�U����'Ȫ:��LU�ٜN�Q/vǬ2�HSE_iF��"T����x?q����}��� 4��IW���}�JְZCr�"������~��x���㋲ޗ���h��#z$������.���^��<JJ�h��F����Iy�s��-���C�����Цa�
endstream
endobj
1 0 obj
<</Type /Pages
/Kids [3 0 R ]
/Count 1
/MediaBox [0 0 595.28 841.89]
>>
endobj
5 0 obj
<</Filter /FlateDecode /Length 364>>
stream
x�]R�n�0��>��L�%�DI�8���~ �%E*r�ﻻvҪHX�gvVk?/���Ῑ��`]�[�x5�3\z��P�}����PO���j�Jݍ^���x6/f�����������|���4}� z�����}���@�,ۖ-��˺E�u�^�,���<��Z_�K� IQ����Yd����C�K�_�%q�8>�!J"V!2&bGģ%r"H��D��\}2EL1n��h�j���e��"a*H����:��d��9c���[�X1~��"�3�g��Ñ�;O��<r_�)-�<%a�I9�󤶕󤜪�8v�s�4z0�97Wcp���x�4�^���M�D*� ��
endstream
endobj
6 0 obj
<</Type /Font
/BaseFont /Courier
/Subtype /Type1
/Encoding /WinAnsiEncoding
/ToUnicode 5 0 R
>>
endobj
7 0 obj
<</Type /Font
/BaseFont /Helvetica-Bold
/Subtype /Type1
/Encoding /WinAnsiEncoding
/ToUnicode 5 0 R
>>
endobj
8 0 obj
<</Type /Font
/BaseFont /Helvetica
/Subtype /Type1
/Encoding /WinAnsiEncoding
/ToUnicode 5 0 R
>>
endobj
9 0 obj
<</Type /Font
/BaseFont /Helvetica-Oblique
/Subtype /Type1
/Encoding /WinAnsiEncoding
/ToUnicode 5 0 R
>>
endobj
2 0 obj
<<
/ProcSet [/PDF /Text /ImageB /ImageC /ImageI]
/Font <<
/F1 6 0 R
/F2 7 0 R
/F3 8 0 R
/F4 9 0 R
>>
/XObject <<
>>
>>
endobj
10 0 obj
<<
/Producer (FPDF 1.81)
/CreationDate (D:%s)
>>
endobj
11 0 obj
<<
/Type /Catalog
/Pages 1 0 R
>>
endobj
xref
0 12
0000000000 65535 f 
0000001018 00000 n 
0000002002 00000 n 
0000000009 00000 n 
0000000087 00000 n 
0000001105 00000 n 
0000001539 00000 n 
0000001650 00000 n 
0000001768 00000 n 
0000001881 00000 n 
0000002136 00000 n 
0000002213 00000 n 
trailer
<<
/Size 12
/Root 11 0 R
/Info 10 0 R
>>
startxref
2263
%%EOF
