--TEST--
Test GetVoucher - getDocument - Verify data validation (document type) 
--FILE--
<?php

namespace Yapo;

require_once 'BconfMock.php';
require_once 'AccountsTransMock.php';
require_once 'PaymentTransMock.php';
require_once 'YapoPaymentAppMock.php';

use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getDocument(2,"88ea39439e74fa27c09a4fc0bc8ebe6d00978392");
print_r($data);

?>
--EXPECT--
Invalid Document
