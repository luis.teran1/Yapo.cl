--TEST--
Test GetVoucher - getUserInfo - Verify user data returned correctly
--FILE--
<?php

namespace Yapo;

require_once 'BconfMock.php';
require_once 'AccountsTransMock.php';

use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getUserInfo(1,1);
print_r($data);

?>
--EXPECT--
Array
(
    [0] => Daniberto
    [1] => 100.000-4
    [2] => 31 de Diciembre de 1969
    [3] => Melipilla
)
