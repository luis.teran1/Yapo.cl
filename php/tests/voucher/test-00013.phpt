--TEST--
Test GetVoucher - getDocument - Verify data validation (Purchase not found) 
--FILE--
<?php

namespace Yapo;

require_once 'BconfMock.php';
require_once 'AccountsTransMock.php';
require_once 'PaymentTransMock.php';

use \Exception;
require_once 'autoload_lib.php';
require_once 'fpdf.php';

const SERVER_ROOT='../regress_final';
$app = new GetVoucher(false);
$data = $app->getDocument(10,1);
print_r($data);

?>
--EXPECT--
Purchase not found
