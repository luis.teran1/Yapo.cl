--TEST--
Test YapesosReport - buildFinancialReport - checks that the report returns empty when using a bad formated date.
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';

$app = new YapesosReport();
$data = $app->buildFinancialReport('2050/01/01','2017/01/05');
print_r($data);

?>
--EXPECT--
Array
(
)
