<?php

class CreditsService
{
    public function getReport($startDate, $endDate, $userId = "")
    {
        global $ReturnCreditsData;
        if (!$ReturnCreditsData) {
            return array();
        }
        $json_response = '
        [
            {
                "External_id": 166,
                "Created_at": "2017-01-04T17:39:12Z",
                "Credits": 3000,
                "Available": 3000,
                "Expiration_date": "2018-01-04T23:27:58Z",
                "Type": "add",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 98,
                "Parent_id": 166
            },
            {
                "External_id": 167,
                "Created_at": "2017-01-04T17:39:23Z",
                "Credits": 3000,
                "Available": 3000,
                "Expiration_date": "2018-01-04T23:28:09Z",
                "Type": "add",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 99,
                "Parent_id": 167
            },
            {
                "External_id": 168,
                "Created_at": "2017-01-04T17:39:40Z",
                "Credits": 3000,
                "Available": 0,
                "Expiration_date": "2018-01-04T23:27:58Z",
                "Type": "consume",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 100,
                "Parent_id": 166
            },
            {
                "External_id": 168,
                "Created_at": "2017-01-04T17:39:40Z",
                "Credits": 3000,
                "Available": 0,
                "Expiration_date": "2018-01-04T23:28:09Z",
                "Type": "consume",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 100,
                "Parent_id": 167
            },
            {
                "External_id": 169,
                "Created_at": "2017-01-04T17:40:02Z",
                "Credits": 19100,
                "Available": 19100,
                "Expiration_date": "2018-01-04T23:28:48Z",
                "Type": "add",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 101,
                "Parent_id": 169
            },
            {
                "External_id": 173,
                "Created_at": "2017-01-04T17:40:37Z",
                "Credits": 19100,
                "Available": 0,
                "Expiration_date": "2018-01-04T23:28:48Z",
                "Type": "consume",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 102,
                "Parent_id": 169
            },
            {
                "External_id": 176,
                "Created_at": "2017-01-02T00:06:23Z",
                "Credits": 12345,
                "Available": 12345,
                "Expiration_date": "2017-01-03T16:55:09Z",
                "Type": "add",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 103,
                "Parent_id": 176
            },
            {
                "External_id": 176,
                "Created_at": "2017-01-02T00:06:23Z",
                "Credits": 12345,
                "Available": 12345,
                "Expiration_date": "2017-01-03T16:55:09Z",
                "Type": "expired",
                "CreditType": "normal",
                "User_id": 6,
                "Transaction_id": 103,
                "Parent_id": 176
            }
        ]';
        return json_decode($json_response, true);
    }
}
