<?php

class bTransaction
{
    public function add_data($key, $value) {
    }

    public function has_error() {
            return false;
    }

    public function send_command($cmd, $var1 = '', $var2 = '') {
        global $ReturnYapoData;
        if (!$ReturnYapoData) {
            return array(
                "status" => "TRANS_ERROR"
            );
        }
        $reports = array(
            0 => array(
                'payment_group_id' => '166',
                'email' => 'many@ads.cl',
                'doc_type' => 'bill',
                'doc_num' => '1',
                'receipt' => '2017-01-04 17:39:10.548613',
                'total_price' => '3000', 'status' => 'paid',
                'purchase_id' => '2'
            ),
            1 => array(
                'payment_group_id' => '167',
                'email' => 'many@ads.cl', 'doc_type' => 'bill',
                'doc_num' => '2',
                'receipt' => '2017-01-04 17:39:22.029572',
                'total_price' => '3000', 'status' => 'paid',
                'purchase_id' => '3'
            ),
            2 => array(
                'payment_group_id' => '168',
                'email' => 'many@ads.cl',
                'doc_type' => 'voucher',
                'doc_num' => '',
                'receipt' => '2017-01-04 17:39:40.075678',
                'total_price' => '6000',
                'status' => 'paid', 'purchase_id' => '4'
            ),
            3 => array(
                'payment_group_id' => '169',
                'email' => 'many@ads.cl',
                'doc_type' => 'bill',
                'doc_num' => '3',
                'receipt' => '2017-01-04 17:39:58.091687',
                'total_price' => '19100', 'status' => 'paid',
                'purchase_id' => '5'
            ),
            4 => array(
                'payment_group_id' => '173',
                'email' => 'many@ads.cl',
                'doc_type' => 'voucher',
                'doc_num' => '',
                'receipt' => '2017-01-04 17:40:37.635978',
                'total_price' => '19100',
                'status' => 'paid',
                'purchase_id' => '7'
            ),
            5 => array(
                'payment_group_id' => '176',
                'email' => 'many@ads.cl',
                'doc_type' => 'bill',
                'doc_num' => '4',
                'receipt' => '2017-01-04 17:40:54.109337',
                'total_price' => '12345',
                'status' => 'paid',
                'purchase_id' => '8'
            )
        );
        return array(
            "status" => "TRANS_OK",
            "get_purchases" => $reports
        );
    }
}

