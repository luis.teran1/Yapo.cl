--TEST--
Test YapesosReport - buildFinancialReport - checks that the report is correctly done.
--FILE--
<?php
include_once 'CreditsServiceExt.php';
include_once 'bTransactionExt.php';
include_once 'autoload_lib.php';
include_once 'init.php';

global $ReturnCreditsData;
$ReturnCreditsData = true;

global $ReturnYapoData;
$ReturnYapoData = true;

$app = new YapesosReport();
$data = $app->buildFinancialReport('2017-01-01','2017-01-05');
print_r($data);

?>
--EXPECT--
Array
(
    [0] => Array
        (
            [payment_group_id] => 166
            [email] => many@ads.cl
            [doc_type] => bill
            [doc_num] => 1
            [receipt] => 2017-01-04 17:39:10.548613
            [total_price] => 3000
            [status] => paid
            [purchase_id] => 2
            [External_id] => 
            [Created_at] => 
            [Credits] => 
            [Available] => 3000
            [Expiration_date] => 2018-01-04T23:27:58Z
            [Type] => add
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 98
            [Parent_id] => 166
            [Parent_doc_num] => 
            [sent_status] => 
            [neto] => 2521
            [iva] => 479
        )

    [1] => Array
        (
            [payment_group_id] => 167
            [email] => many@ads.cl
            [doc_type] => bill
            [doc_num] => 2
            [receipt] => 2017-01-04 17:39:22.029572
            [total_price] => 3000
            [status] => paid
            [purchase_id] => 3
            [External_id] => 
            [Created_at] => 
            [Credits] => 
            [Available] => 3000
            [Expiration_date] => 2018-01-04T23:28:09Z
            [Type] => add
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 99
            [Parent_id] => 167
            [Parent_doc_num] => 
            [sent_status] => 
            [neto] => 2521
            [iva] => 479
        )

    [2] => Array
        (
            [payment_group_id] => 168
            [email] => many@ads.cl
            [doc_type] => voucher
            [doc_num] => 168
            [receipt] => 2017-01-04 17:39:40.075678
            [total_price] => 6000
            [status] => paid
            [purchase_id] => 4
            [External_id] => 168
            [Created_at] => 2017-01-04T17:39:40Z
            [Credits] => 3000
            [Available] => 0
            [Expiration_date] => 2018-01-04T23:27:58Z
            [Type] => consume
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 100
            [Parent_id] => 166
            [Parent_doc_num] => 1
            [sent_status] => confirmed
            [neto] => 5042
            [iva] => 958
        )

    [3] => Array
        (
            [payment_group_id] => 168
            [email] => many@ads.cl
            [doc_type] => voucher
            [doc_num] => 168
            [receipt] => 2017-01-04 17:39:40.075678
            [total_price] => 6000
            [status] => paid
            [purchase_id] => 4
            [External_id] => 168
            [Created_at] => 2017-01-04T17:39:40Z
            [Credits] => 3000
            [Available] => 0
            [Expiration_date] => 2018-01-04T23:28:09Z
            [Type] => consume
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 100
            [Parent_id] => 167
            [Parent_doc_num] => 2
            [sent_status] => confirmed
            [neto] => 5042
            [iva] => 958
        )

    [4] => Array
        (
            [payment_group_id] => 169
            [email] => many@ads.cl
            [doc_type] => bill
            [doc_num] => 3
            [receipt] => 2017-01-04 17:39:58.091687
            [total_price] => 19100
            [status] => paid
            [purchase_id] => 5
            [External_id] => 
            [Created_at] => 
            [Credits] => 
            [Available] => 19100
            [Expiration_date] => 2018-01-04T23:28:48Z
            [Type] => add
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 101
            [Parent_id] => 169
            [Parent_doc_num] => 
            [sent_status] => 
            [neto] => 16050
            [iva] => 3050
        )

    [5] => Array
        (
            [payment_group_id] => 173
            [email] => many@ads.cl
            [doc_type] => voucher
            [doc_num] => 173
            [receipt] => 2017-01-04 17:40:37.635978
            [total_price] => 19100
            [status] => paid
            [purchase_id] => 7
            [External_id] => 173
            [Created_at] => 2017-01-04T17:40:37Z
            [Credits] => 19100
            [Available] => 0
            [Expiration_date] => 2018-01-04T23:28:48Z
            [Type] => consume
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 102
            [Parent_id] => 169
            [Parent_doc_num] => 3
            [sent_status] => confirmed
            [neto] => 16050
            [iva] => 3050
        )

    [6] => Array
        (
            [payment_group_id] => 176
            [email] => many@ads.cl
            [doc_type] => bill
            [doc_num] => 4
            [receipt] => 2017-01-04 17:40:54.109337
            [total_price] => 12345
            [status] => paid
            [purchase_id] => 8
            [External_id] => 
            [Created_at] => 
            [Credits] => 
            [Available] => 12345
            [Expiration_date] => 2017-01-03T16:55:09Z
            [Type] => add
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 103
            [Parent_id] => 176
            [Parent_doc_num] => 
            [sent_status] => 
            [neto] => 10374
            [iva] => 1971
        )

    [7] => Array
        (
            [payment_group_id] => 176
            [email] => many@ads.cl
            [doc_type] => bill
            [doc_num] => 4
            [receipt] => 2017-01-04 17:40:54.109337
            [total_price] => 12345
            [status] => paid
            [purchase_id] => 8
            [External_id] => CADUCAR
            [Created_at] => 2017-01-02T00:06:23Z
            [Credits] => 12345
            [Available] => 
            [Expiration_date] => 2017-01-03T16:55:09Z
            [Type] => expired
            [CreditType] => normal
            [User_id] => 6
            [Transaction_id] => 103
            [Parent_id] => 176
            [Parent_doc_num] => 
            [sent_status] => confirmed
            [neto] => 10374
            [iva] => 1971
        )

)