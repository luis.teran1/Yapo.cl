--TEST--
Test YapesosReport - buildFinancialReport - checks that the report returns empty when using a future date and yapo does not have data.
--FILE--
<?php
include_once 'CreditsServiceExt.php';
include_once 'bTransactionExt.php';
include_once 'autoload_lib.php';
include_once 'init.php';

global $ReturnCreditsData;
$ReturnCreditsData = false;

$app = new YapesosReport();
$data = $app->buildFinancialReport('2050-01-01','2017-01-05');
print_r($data);

?>
--EXPECT--
Array
(
)
