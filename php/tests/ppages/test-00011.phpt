--TEST--
Test promotional pages banners - Gets promo banners list from trans
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';
require_once 'init.php';
require_once 'bTransactionMock.php';

use Yapo\PpageBanner;

global $transDummyResponse;
global $transDummyHasError;

$transDummyHasError = false;
$transDummyResponse = array(
  'status' => 'TRANS_OK',
  'promo_banners' =>
    array(
      'draft_desktop_home_1' =>
        array(
          'pp_id' => '99',
          'url' => 'arma-tu-primer-hogar',
          'image' => '/ppageimages/90/9008523319.jpg',
          'start_date' => '2017-01-24',
          'end_date' => '-1',
          'categories' => '-1',
          'regions' => '-1',
          'platform' => 'desktop',
          'position' => 'home',
          'banner_id' => '1',
          'key' => 'draft:promo_banner:desktop:home:1',
          'is_draft' => '1')
    )
);

var_dump(PpageBanner::fromTrans());

?>
--EXPECT--
object(Yapo\PpageBanner)#3 (13) {
  ["pp_id"]=>
  array(1) {
    [0]=>
    string(2) "99"
  }
  ["url"]=>
  array(1) {
    [0]=>
    string(20) "arma-tu-primer-hogar"
  }
  ["start_date"]=>
  array(1) {
    [0]=>
    string(10) "2017-01-24"
  }
  ["end_date"]=>
  array(1) {
    [0]=>
    string(2) "-1"
  }
  ["categories"]=>
  array(1) {
    [0]=>
    string(2) "-1"
  }
  ["regions"]=>
  array(1) {
    [0]=>
    string(2) "-1"
  }
  ["image"]=>
  array(1) {
    [0]=>
    string(30) "/ppageimages/90/9008523319.jpg"
  }
  ["key"]=>
  array(1) {
    [0]=>
    string(33) "draft:promo_banner:desktop:home:1"
  }
  ["banner_id"]=>
  array(1) {
    [0]=>
    string(1) "1"
  }
  ["platform"]=>
  array(1) {
    [0]=>
    string(7) "desktop"
  }
  ["position"]=>
  array(1) {
    [0]=>
    string(4) "home"
  }
  ["is_draft"]=>
  array(1) {
    [0]=>
    string(1) "1"
  }
  ["status"]=>
  NULL
}
