--TEST--
Test promotional pages banners - Deletes a promo banner
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';
require_once 'init.php';
require_once 'bTransactionMock.php';

use Yapo\PpageBanner;

global $transDummyResponse;
global $transDummyHasError;

$transDummyHasError = false;
$transDummyResponse = array(
  'status' => 'TRANS_OK',
  'banner_id' => 'promo_banner:desktop:home:1'
);

var_dump(PpageBanner::delete('promo_banner:desktop:home:1'));

?>
--EXPECT--
bool(true)
