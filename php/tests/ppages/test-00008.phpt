--TEST--
Test promotional pages banners - Creates banner from form with TRANS_OK response
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';

use Yapo\PpageBanner;

$form = array(
  'm' => 'PPages',
  'a' => 'ppageBanners',
  'pp_id' => '99',
  'start_date' => '2017-01-09',
  'end_date' => '2017-01-13',
  'platform' => 'desktop',
  'position' => 'frankie',
  'image' => '/ppageimages/90/9042616595.jpg',
  'create' => 'send',
  'pp_start_date' => '2016-01-11',
  'pp_end_date' => '-1',
  'url' => 'arma-tu-primer-hogar'
);

$ppbanner = PpageBanner::fromForm($form);
var_dump($ppbanner);

?>
--EXPECT--
object(Yapo\PpageBanner)#2 (13) {
  ["pp_id"]=>
  string(2) "99"
  ["url"]=>
  string(20) "arma-tu-primer-hogar"
  ["start_date"]=>
  string(10) "2017-01-09"
  ["end_date"]=>
  string(10) "2017-01-13"
  ["categories"]=>
  NULL
  ["regions"]=>
  NULL
  ["image"]=>
  string(30) "/ppageimages/90/9042616595.jpg"
  ["key"]=>
  NULL
  ["banner_id"]=>
  NULL
  ["platform"]=>
  string(7) "desktop"
  ["position"]=>
  string(7) "frankie"
  ["is_draft"]=>
  NULL
  ["status"]=>
  NULL
}
