--TEST--
Test promotional pages - Upload image without image
--FILE--
<?php
use Yapo\PpImage;
require_once('common/includes.php');


$ppimage = new PpImage('image', 'main');
$ppimage->save();
?>
--EXPECT--
{"status":"La imagen no se encuentra"}
