--TEST--
Test promotional pages banners - Deletes a promo banner with an invalid key
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';
require_once 'init.php';
require_once 'bTransactionMock.php';

use Yapo\PpageBanner;

global $transDummyResponse;
global $transDummyHasError;

$transDummyHasError = false;
$transDummyResponse = array(
  'status' => 'TRANS_ERROR',
  'banner_id' => 'ERROR_BANNER_ID_INVALID'
);

var_dump(PpageBanner::delete('not a valid key'));

?>
--EXPECT--
bool(false)
