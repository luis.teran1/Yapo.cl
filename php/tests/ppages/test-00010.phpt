--TEST--
Test promotional pages banners - Creates banner from form with no params
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';

use Yapo\PpageBanner;

$form = array();
$ppbanner = PpageBanner::fromForm($form);
var_dump($ppbanner);

?>
--EXPECT--
string(20) "ERROR_NO_PP_SELECTED"
