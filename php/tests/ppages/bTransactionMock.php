<?php

class bTransaction
{
	public $transDummyResponse;
	public $transDummyHasError;
	public $transDummyData;

	public function __construct() {
		global $transDummyResponse;
		global $transDummyHasError;
		$this->transDummyData = array();
		$this->transDummyResponse = $transDummyResponse;
		$this->transDummyHasError = $transDummyHasError;
	}

	public function add_data($key, $value) {
		$this->transDummyData[$key] = $value;
	}

	public function has_error() {
		return $this->transDummyHasError;
	}

	public function send_command($cmd, $var1 = '', $var2 = '') {
		return $this->transDummyResponse;
	}

	public function add_client_info() {
		$this->transDummyData['remote_addr'] = $_SERVER['REMOTE_ADDR'];
		$this->transDummyData['remote_browser'] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : "No browser set";
	}

	public function get_errors() {
		return 'TRANS_DUMMY_ERROR';
	}

	public function add_file($k, $v) {
		return true;
	}
}
