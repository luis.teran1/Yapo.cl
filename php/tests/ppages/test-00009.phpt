--TEST--
Test promotional pages banners - Creates banner from form with missing params
--FILE--
<?php

require_once 'autoload_lib.php';
require_once 'util.php';

use Yapo\PpageBanner;

$form = array(
  'm' => 'PPages',
  'a' => 'ppageBanners',
  'pp_id' => '99',
  'platform' => 'desktop',
  'position' => 'frankie',
  'image' => '/ppageimages/90/9042616595.jpg',
  'create' => 'send',
  'pp_start_date' => '2016-01-11',
  'pp_end_date' => '-1',
  'url' => 'arma-tu-primer-hogar'
);

$ppbanner = PpageBanner::fromForm($form);
var_dump($ppbanner);

?>
--EXPECT--
string(19) "ERROR_NO_START_DATE"
