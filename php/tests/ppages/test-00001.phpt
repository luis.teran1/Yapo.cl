--TEST--
Test promotional pages - Upload empty image
--INI--
enable_post_data_reading=0
--POST_RAW--
Content-Type: multipart/form-data; boundary=---------------------------13065743741575213243430574885
-----------------------------13065743741575213243430574885
Content-Disposition: form-data; name="image"; filename="bg_pp.jpg"
Content-Type: image/jpeg


-----------------------------13065743741575213243430574885--
--FILE--
<?php
use Yapo\PpImage;

$_SERVER['DOCUMENT_ROOT'] = "";
require_once('../common/includes.php');

$ppimage = new PpImage('image', 'main');
$ppimage->save();
?>
--EXPECT--
{"status":"El peso de la imagen es inv\u00e1lido"}
