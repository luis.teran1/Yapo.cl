--TEST--
Test promotional pages - Upload fake image
--FILE--
<?php
use Yapo\PpImage;

$_SERVER['DOCUMENT_ROOT'] = "";
require_once('common/includes.php');

$_FILES['image'] = array(
	'name'=>'archivo_falso.jpg',
	'type' => 'image/jpg',
	'tmp_name' => '/etc/passwd',
	'size' => 15000
);

$ppimage = new PpImage('image', 'main');
$ppimage->save();
?>
--EXPECT--
{"status":"No se pudo subir la imagen"}
