--TEST--
CsvExporter - Empty - Check asigned name without headers and data
--FILE--
<?php
require_once 'autoload_lib.php';
require_once 'CsvExporter.php';
require_once 'init.php';

$app = new CsvExporter("mi_puto_archivo.csv");
$app->run(true);

?>
--EXPECTHEADERS--
Content-Disposition: inline; filename="mi_puto_archivo.csv"
--EXPECT--
Sin resultados
