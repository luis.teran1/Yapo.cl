--TEST--
CsvExporter - success - Check asigned name and output inline
--FILE--
<?php
require_once 'autoload_lib.php';
require_once 'CsvExporter.php';
require_once 'init.php';

$app = new CsvExporter("mi_puto_archivo.csv");
$app->setHeaders(array("test", "fome"));
$app->setData(array(
    array("cosa", "fea"),
    array("wea", "as")
));
$app->run(true);

?>
--EXPECTHEADERS--
Content-Disposition: inline; filename="mi_puto_archivo.csv"
--EXPECT--
test;fome
cosa; fea
wea; as
