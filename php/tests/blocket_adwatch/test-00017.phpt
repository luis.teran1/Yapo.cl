--TEST--
Test Adwatch - set_selected_tab() - with tab_id 'list_ads'
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = $adwatch->set_selected_tab('list_ads');
var_dump($adwatch->selected_tab);

?>
--EXPECT--
string(8) "list_ads"
