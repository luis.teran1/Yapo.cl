--TEST--
Test Adwatch - generate_title() - with a valid query string
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$qs = array(
	'ca=15_s&l=0&q=gundam&w=1',
	'ca=a&q=gundam',
	'foo=1234&var=abc'
);

foreach ($qs as $q) {
	$r = $adwatch->generate_title($q);
	var_dump(count($r));
	var_dump($r[0]);
	var_dump($r[1]);
}

?>
--EXPECT--
int(2)
string(35) "gundam, Vendo, Regi�n Metropolitana"
string(42) "gundam, Vendo, Regi&oacute;n Metropolitana"
int(2)
string(20) "gundam, Vendo, Chile"
string(20) "gundam, Vendo, Chile"
int(2)
string(12) "Vendo, Chile"
string(12) "Vendo, Chile"
