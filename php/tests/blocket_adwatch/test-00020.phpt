--TEST--
Test Adwatch - load_watch_queries() - empty uniq id
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$adwatch->load_watch_queries();

var_dump(count($adwatch->watch_queries));
var_dump($adwatch->watch_queries[0]['watch_query_id']);
var_dump($adwatch->watch_queries[0]['query_string']);
var_dump($adwatch->watch_queries_cachetimeout);

?>
--EXPECTF--
int(0)
NULL
NULL
int(%d)
