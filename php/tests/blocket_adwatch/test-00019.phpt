--TEST--
Test Adwatch - load_watch_queries() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');
include('adwatch_bTransaction.php');

global $dummyResponse;
global $dummyHasError;

$dummyResponse = array(
	'get_watch_queries' => array(
		array(
			'watch_query_id' => '1',
			'query_string' => 'ca=15_s&q=pesas&w=3'
		)
	)
);

$adwatch = new blocket_adwatch();
$adwatch->watch_unique_id = '1915e8f0-be8b-4a71-b58e-89977fd3a44d';
$adwatch->load_watch_queries();
var_dump(count($adwatch->watch_queries));
var_dump($adwatch->watch_queries[0]['watch_query_id']);
var_dump($adwatch->watch_queries[0]['query_string']);
var_dump($adwatch->watch_queries_cachetimeout);

?>
--EXPECTF--
int(1)
string(1) "1"
string(19) "ca=15_s&q=pesas&w=3"
int(%d)
