--TEST--
Test Adwatch - aw_start() - with valid cookie get list
--COOKIE--
adw=1915e8f0-be8b-4a71-b58e-89977fd3a44d
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');
include('adwatch_bTransaction.php');

global $dummyResponse;
$dummyResponse = array('get_watch_queries' => 'foo');

$adwatch = new blocket_adwatch();
$r = $adwatch->aw_start();
var_dump($r);

?>
--EXPECT--
string(12) "presentation"
