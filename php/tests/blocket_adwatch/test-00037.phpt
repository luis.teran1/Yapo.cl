--TEST--
Test Adwatch - watch_ad() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_COOKIE['adw'] = '12345678-abcd-1a2b-3c4d-998877665544';

include('adwatch_includes.php');
include('adwatch_bTransaction.php');

global $dummyResponse;
global $dummyHasError;

$dummyResponse = array(
	'watch_ad' => array(
		array(
			'watch_unique_id' => '12345678-abcd-1a2b-3c4d-998877665544',
		)
	)
);
$dummyHasError = false;

$adwatch = new blocket_adwatch();
$r1 = $adwatch->watch_ad(123456);
var_dump($r1);

?>
--EXPECT--
bool(true)

