--TEST--
Test Adwatch - get_state() - check all valid states and get proper method and function
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['DOCUMENT_ROOT'] = __DIR__;

include('adwatch_includes.php');

$states = array(
	'start',
	'no_cookies',
	'presentation',
	'list_ads',
	'list_all',
	'list',
	'ajax_gencode',
	'save',
	'watch_ad',
	'delete_ad',
	'delete',
	'info',
	'sms_stop',
	'no_valid_state'
);

$adwatch = new blocket_adwatch();
foreach ($states as $s) {
	$r = $adwatch->get_state('start');
	var_dump($r);
}

?>
--EXPECT--
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
array(2) {
  ["function"]=>
  string(8) "aw_start"
  ["method"]=>
  string(3) "get"
}
