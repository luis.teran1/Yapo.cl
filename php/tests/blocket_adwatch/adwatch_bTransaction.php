<?php

class bTransaction
{
	public $dummyResponse;

	public function __construct() {
		global $dummyResponse;
		global $dummyHasError;
		$this->dummyResponse = $dummyResponse;
		$this->dummyHasError = $dummyHasError;
	}

	public function add_data($key, $value) {
		
	}

	public function has_error() {
		return $this->dummyHasError;
	}

	public function send_command($cmd, $var1 = '', $var2 = '') {
		return $this->dummyResponse;
	}
}
