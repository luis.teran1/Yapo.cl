--TEST--
Test Adwatch - get_listing_mode() - with $_GET['th']
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$_GET['th'] = false;
$r1 = $adwatch->get_listing_mode();

$_GET['th'] = true;
$r2 = $adwatch->get_listing_mode();

var_dump($r1);
var_dump($r2);

?>
--EXPECT--
string(2) "li"
string(2) "th"
