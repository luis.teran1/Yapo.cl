--TEST--
Test Adwatch - aw_start() - empty
--FILE--
<?php
$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = $adwatch->aw_start();
var_dump($r);

?>
--EXPECT--
string(12) "presentation"
