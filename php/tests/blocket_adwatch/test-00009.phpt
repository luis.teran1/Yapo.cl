--TEST--
Test Adwatch - update_last_view() - "watch_queries" has 1 element
--COOKIE--
adw=500796db-3d5e-4470-93a9-9af98d9a09d5
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$adwatch->watch_queries = array();
$adwatch->watch_queries[] = unserialize('a:1:{i:0;a:14:{s:14:"watch_query_id";s:1:"1";s:12:"query_string";s:19:"ca=15_s&q=pesas&w=3";s:8:"color_id";s:1:"5";s:9:"last_view";s:26:"2016-08-01 15:42:37.832959";s:18:"adwatch_sms_active";s:1:"f";s:15:"sms_user_status";s:0:"";s:22:"watch_query_sms_status";s:0:"";s:13:"num_sms_today";s:1:"0";s:10:"title_full";s:19:"pesas, Vendo, Chile";s:5:"title";s:19:"pesas, Vendo, Chile";s:17:"query_lookup_data";a:6:{s:7:"sq_desc";s:19:"pesas, Vendo, Chile";s:8:"sq_limit";s:2:"50";s:9:"sq_offset";s:1:"0";s:9:"sq_params";s:7:" type:-";s:8:"sq_query";s:5:"pesas";s:13:"sq_unfiltered";s:22:" count_all(unfiltered)";}s:11:"result_size";s:1:"1";s:11:"has_new_ads";i:0;s:15:"failover_search";i:0;}}');
$adwatch->update_last_view(1);

echo $adwatch->last_view[1];

?>
--EXPECTREGEX--
[0-9]{10}
