--TEST--
Test Adwatch - filter_query_string() - simple query string with valid fields
--COOKIE--
adw=1915e8f0-be8b-4a71-b58e-89977fd3a44d
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = $adwatch->filter_query_string('ca=15_s&l=0&q=gundam&w=1');

var_dump($r);

?>
--EXPECT--
string(20) "ca=15_s&q=gundam&w=1"
