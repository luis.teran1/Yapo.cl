--TEST--
Test Adwatch - get_listing_mode() - width $_COOKIE['md']
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$_COOKIE['md'] = 'li';
$r1 = $adwatch->get_listing_mode();

$_COOKIE['md'] = 'th';
$r2 = $adwatch->get_listing_mode();

$_COOKIE['md'] = 'not_valid';
$r3 = $adwatch->get_listing_mode();

var_dump($r1);
var_dump($r2);
var_dump($r3);

?>
--EXPECT--
string(2) "li"
string(2) "th"
string(2) "th"
