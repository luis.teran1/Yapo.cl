--TEST--
Test Adwatch - filter_query_string() - simple query string with invalid fields
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r1 = $adwatch->filter_query_string('ca=15_s&foo=0x00&var=%00');
$r2 = $adwatch->filter_query_string('foo=0x00&var=%00&ca=15_s');
$r3 = @$adwatch->filter_query_string('foo=0x00&var=%00');

var_dump($r1);
var_dump($r2);
var_dump($r3);

?>
--EXPECT--
string(7) "ca=15_s"
string(7) "ca=15_s"
NULL
