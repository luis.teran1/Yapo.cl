--TEST--
Test Adwatch - delete_old_watch_queries() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = $adwatch->delete_old_watch_queries();

var_dump($r);

?>
--EXPECT--
NULL
