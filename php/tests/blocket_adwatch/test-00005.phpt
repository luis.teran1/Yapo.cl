--TEST--
Test Adwatch - aw_save() - empty query string
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = @$adwatch->aw_save(1);
var_dump($r);

?>
--EXPECT--
array(1) {
  [0]=>
  string(5) "start"
}
