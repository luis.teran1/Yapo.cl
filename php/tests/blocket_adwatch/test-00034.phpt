--TEST--
Test Adwatch - aw_delete() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';
$_COOKIE['adw'] = '12345678-abcd-1a2b-3c4d-998877665544';

include('adwatch_includes.php');
include('adwatch_bTransaction.php');

global $dummyResponse;
global $dummyHasError;

$dummyResponse = array(
	'delete_watch_query' => array(
		array(
			'watch_user_id' => 1
		)
	)
);

$adwatch = new blocket_adwatch();
$r1 = $adwatch->aw_delete(1);

$dummyResponse = array(
	'delete_watch_query' => array(
		array(
			'watch_user_id' => 0
		)
	)
);

$r2 = $adwatch->aw_delete(1);
var_dump($r1[0]);
var_dump($r2[0]);

?>
--EXPECT--
string(4) "list"
string(4) "list"
