--TEST--
Test Adwatch - get_selected_tab() - default values
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = $adwatch->get_selected_tab();

var_dump($r);

?>
--EXPECT--
int(0)
