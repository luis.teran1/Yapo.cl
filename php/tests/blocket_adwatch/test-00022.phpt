--TEST--
Test Adwatch - invalidate_watch_ads_cache() - test default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$adwatch->watch_ads = array('not empty');
$adwatch->watch_ads_cachetimeout = time();

$adwatch->invalidate_watch_ads_cache();

var_dump($adwatch->watch_ads);
var_dump($adwatch->watch_ads_cachetimeout);

?>
--EXPECT--
NULL
NULL
