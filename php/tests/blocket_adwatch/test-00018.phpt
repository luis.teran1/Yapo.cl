--TEST--
Test Adwatch - load_watch_ads() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');
include('adwatch_bTransaction.php');

global $dummyResponse;
$dummyResponse = array('get_watch_ads' => 'dummy watch ads');

$adwatch = new blocket_adwatch();
$adwatch->watch_unique_id = '1915e8f0-be8b-4a71-b58e-89977fd3a44d';
$adwatch->load_watch_ads();

var_dump($adwatch->watch_ads);
var_dump($adwatch->watch_ads_cachetimeout);

?>
--EXPECTF--
string(15) "dummy watch ads"
int(%d)
