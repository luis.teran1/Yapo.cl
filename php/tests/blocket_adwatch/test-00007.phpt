--TEST--
Test Adwatch - get_active_query() - "watch_queries" is empty
--DESCRIPTION--
Test the get_active_query() method with "watch_queries" attribute empty
--COOKIE--
adw=500796db-3d5e-4470-93a9-9af98d9a09d5
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$r = $adwatch->get_active_query(0);
var_dump(count($r));

$r = $adwatch->get_active_query(1);
var_dump(count($r));

?>
--EXPECT--
int(0)
int(0)
