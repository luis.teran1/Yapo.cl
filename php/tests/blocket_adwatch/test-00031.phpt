--TEST--
Test Adwatch - check_cache_expired() - default behaviour with 5 minutes as "cache_expire_min"
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$currentTime = time();
$times = array(
	$currentTime,
	$currentTime - (60 * 5),
	$currentTime - (60 * 10),
	$currentTime - (60 * 100),
);

global $BCONF;
$common = $BCONF['*']['common'];
unset($BCONF);
$BCONF = array(
	'*' => array(
		'common' => $common
	),
	'adwatch' => array(
		'cache_expire_min' => 5
	)
);

foreach ($times as $t) {
	$r = $adwatch->check_cache_expired($t);
	var_dump($r);
}
?>
--EXPECT--
bool(false)
bool(false)
bool(true)
bool(true)
