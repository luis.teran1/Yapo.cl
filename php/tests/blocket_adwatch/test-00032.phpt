--TEST--
Test Adwatch - set_page_title_name() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$adwatch->set_page_title_name();
var_dump($adwatch->page_title);
var_dump($adwatch->page_name);

$adwatch->watch_queries = array(
	'1' => array (
		'watch_query_id' => 1,
		'title_full' => 'Full title'
	)
);
$adwatch->selected_tab = 1;
$adwatch->set_page_title_name();
var_dump($adwatch->page_title);
var_dump($adwatch->page_name);

?>
--EXPECT--
string(19) "Favoritos | yapo.cl"
string(9) "Favoritos"
string(10) "Full title"
string(10) "Full title"
