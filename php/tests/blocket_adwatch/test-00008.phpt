--TEST--
Test Adwatch - get_active_query() - "watch_queries" has 1 element
--COOKIE--
adw=500796db-3d5e-4470-93a9-9af98d9a09d5
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$adwatch->watch_queries = array();
$adwatch->watch_queries[] = unserialize('a:1:{i:0;a:14:{s:14:"watch_query_id";s:1:"1";s:12:"query_string";s:19:"ca=15_s&q=pesas&w=3";s:8:"color_id";s:1:"5";s:9:"last_view";s:26:"2016-08-01 15:42:37.832959";s:18:"adwatch_sms_active";s:1:"f";s:15:"sms_user_status";s:0:"";s:22:"watch_query_sms_status";s:0:"";s:13:"num_sms_today";s:1:"0";s:10:"title_full";s:19:"pesas, Vendo, Chile";s:5:"title";s:19:"pesas, Vendo, Chile";s:17:"query_lookup_data";a:6:{s:7:"sq_desc";s:19:"pesas, Vendo, Chile";s:8:"sq_limit";s:2:"50";s:9:"sq_offset";s:1:"0";s:9:"sq_params";s:7:" type:-";s:8:"sq_query";s:5:"pesas";s:13:"sq_unfiltered";s:22:" count_all(unfiltered)";}s:11:"result_size";s:1:"1";s:11:"has_new_ads";i:0;s:15:"failover_search";i:0;}}');
$r1 = $adwatch->get_active_query(0);
$r2 = $adwatch->get_active_query(1);
$r3 = $adwatch->get_active_query(100);

var_dump(count($r1));
var_dump(count($r2));
var_dump(count($r3));
var_dump($r3);

?>
--EXPECT--
int(1)
int(1)
int(1)
array(1) {
  [0]=>
  array(14) {
    ["watch_query_id"]=>
    string(1) "1"
    ["query_string"]=>
    string(19) "ca=15_s&q=pesas&w=3"
    ["color_id"]=>
    string(1) "5"
    ["last_view"]=>
    string(26) "2016-08-01 15:42:37.832959"
    ["adwatch_sms_active"]=>
    string(1) "f"
    ["sms_user_status"]=>
    string(0) ""
    ["watch_query_sms_status"]=>
    string(0) ""
    ["num_sms_today"]=>
    string(1) "0"
    ["title_full"]=>
    string(19) "pesas, Vendo, Chile"
    ["title"]=>
    string(19) "pesas, Vendo, Chile"
    ["query_lookup_data"]=>
    array(6) {
      ["sq_desc"]=>
      string(19) "pesas, Vendo, Chile"
      ["sq_limit"]=>
      string(2) "50"
      ["sq_offset"]=>
      string(1) "0"
      ["sq_params"]=>
      string(7) " type:-"
      ["sq_query"]=>
      string(5) "pesas"
      ["sq_unfiltered"]=>
      string(22) " count_all(unfiltered)"
    }
    ["result_size"]=>
    string(1) "1"
    ["has_new_ads"]=>
    int(0)
    ["failover_search"]=>
    int(0)
  }
}
