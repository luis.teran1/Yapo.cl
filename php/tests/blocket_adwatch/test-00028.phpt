--TEST--
Test Adwatch - filter_query_string() - test custom regex
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

global $BCONF;
$common = $BCONF['*']['common'];
unset($BCONF);
$BCONF = array(
	'*' => array(
		'common' => $common, 
		'qs' => array(
			'vars' => array(
				'ca' => array(
					'regex' => '[0-9]{1,2}_([0-9]{1,2}_)?[a-z]$'
				),
				'foo' => array(
					'regex' => 'impossible'
				)
			)
		)
	)
);

$r1 = $adwatch->filter_query_string('ca=15_s&foo=15_s');
$r2 = $adwatch->filter_query_string('ca=15_s&foo=impossible');
$r3 = $adwatch->filter_query_string('ca=impossible&foo=impossible');

var_dump($r1);
var_dump($r2);
var_dump($r3);

?>
--EXPECT--
string(7) "ca=15_s"
string(22) "ca=15_s&foo=impossible"
string(14) "foo=impossible"
