--TEST--
Test Adwatch - f_md() - test valid and invalid params
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r = array();
$r[] = $adwatch->f_md('th'); 
$r[] = $adwatch->f_md('li'); 
$r[] = $adwatch->f_md('mp'); 
$r[] = $adwatch->f_md('not valid'); 
$r[] = $adwatch->f_md('');  /* empty */

var_dump($r);

?>
--EXPECT--
array(5) {
  [0]=>
  string(2) "th"
  [1]=>
  string(2) "li"
  [2]=>
  string(2) "mp"
  [3]=>
  NULL
  [4]=>
  NULL
}
