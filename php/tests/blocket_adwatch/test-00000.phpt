--TEST--
Test Adwatch - Adwatch test class
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
if ($adwatch instanceof blocket_adwatch) {
	echo 'true';
}
else {
	echo 'false';
}

?>
--EXPECT--
true
