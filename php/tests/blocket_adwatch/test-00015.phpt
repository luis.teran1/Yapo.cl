--TEST--
Test Adwatch - get_selected_tab() - with $_COOKIE['adw_tab']
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$_COOKIE['adw_tab'] = 1;
$r1 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = 10;
$r2 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = 0;
$r3 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = -10;
$r4 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = '1';
$r5 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = '10';
$r6 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = '2a';
$r7 = $adwatch->get_selected_tab();

$_COOKIE['adw_tab'] = 'a';
$r8 = $adwatch->get_selected_tab();

unset($_COOKIE['adw_tab']);
$rUnset = $adwatch->get_selected_tab();

var_dump($r1);
var_dump($r2);
var_dump($r3);
var_dump($r4);
var_dump($r5);
var_dump($r6);
var_dump($r7);
var_dump($r8);
var_dump($rUnset);

?>
--EXPECT--
int(1)
int(10)
int(0)
int(-10)
int(1)
int(10)
int(2)
int(0)
int(0)
