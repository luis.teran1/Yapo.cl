--TEST--
Test Adwatch - get_watch_cookie() - default behaviour
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();
$r1 = $adwatch->get_watch_cookie();
$_COOKIE['adw'] = 'uniq-cookie-id';
$r2 = $adwatch->get_watch_cookie();

var_dump($r1);
var_dump($r2);

?>
--EXPECT--
bool(false)
string(14) "uniq-cookie-id"
