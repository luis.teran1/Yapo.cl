--TEST--
Test Adwatch - lookup_query() - with valid query strings
--FILE--
<?php

$_SERVER['QUERY_STRING'] = '';
$_SERVER['HTTP_USER_AGENT'] = 'test';
$_SERVER['REMOTE_ADDR'] = '127.0.0.1';

include('adwatch_includes.php');

$adwatch = new blocket_adwatch();

$qs = array(
    'ca=15_s&l=0&q=gundam&w=1',
    'ca=a&q=gundam',
    'foo=1234&var=abc'
);
foreach ($qs as $q) {
    $r = $adwatch->lookup_query($q);
    var_dump(count($r));
    var_dump($r['sq_desc']);
	if (isset($r['sq_query'])) {
	    var_dump($r['sq_query']);
	}
	else {
		echo "EMPTY\n";
	}
    var_dump($r['sq_offset']);
}
?>
--EXPECT--
int(6)
string(35) "gundam, Vendo, Regi�n Metropolitana"
string(6) "gundam"
string(1) "0"
int(6)
string(20) "gundam, Vendo, Chile"
string(6) "gundam"
string(1) "0"
int(5)
string(12) "Vendo, Chile"
EMPTY
string(1) "0"
