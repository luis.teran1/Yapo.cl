<?php

include_once('MetaMock.php');

/**
 * PshClientMock: The name says it all
 * Intended usage:
 *      $acc_session = new PshClientMock();
 *      $acc_session->_add_response(array(
 *          True
 *      ));
 *      $victim = new TestedClass($acc_session);
 *      $response = $victim->testedMethod(123, 'ok');
 *      $invocations = $acc_session->_get_invocations();
 *      assert $response == $expected;
 *      assert $invocations == $expectedInvocations;
 *
 * This allows to configure individual replies for each expected account_session call
 * and check which arguments were supplied to each invocation.
 */
class PshClientMock extends Mock {

    /**
     * statusSubscription
     * Return true if change status and false if does not change status. 
     */
    public function statusSubscription() {
        $invocation = $this->_next_response(__METHOD__);

        $this->_add_invocation(array('statusSubscription', $invocation));
        return $invocation;
    }

    /**
     * paySubscription:
     * Return true if Pay subscription
     */
    public function paySubscription() {
        $invocation = $this->_next_response(__METHOD__);

        $this->_add_invocation(array('paySubscription', $invocation));
        return $invocation;
    }
}
