<?php

include_once('MetaMock.php');

/**
 * bTransactionMock: One bTransaction mock to rule them all
 * Intended usage:
 *      $trans = new bTransactionMock();
 *      $trans->_add_response(array(
 *          'user_id' => 567,
 *          'status' => 'TRANS_OK',
 *      ));
 *      $victim = new TestedClass($trans);
 *      $response = $victim->testedMethod(123, 'ok');
 *      $invocations = $trans->_get_invocations();
 *      assert $response == $expected;
 *      assert $invocations == $expectedInvocations;
 *
 * This allows to configure individual replies for each expected transaction call
 * and check which arguments were supplied to each invocation.
 */
class bTransactionMock extends Mock {

    private $current_tr = array();

    /**
     * reset:
     * Deletes current transaction data and readies for a new one
     */
    public function reset() {
        $this->current_tr = array();
        return $this;
    }

    /**
     * add_data:
     * Adds given $key $value pair to the current transaction
     */
    public function add_data($key, $value) {
        $this->current_tr[$key] = $value;
    }

    /**
     * has_error:
     * Checks whether the last transaction result was TRANS_OK or not
     */
    public function has_error($x = true) {
        if ($this->i < 1) {
            return false;
        }
        foreach ($this->responses[$this->i - 1] as $k => $v) {
            if ($k == 'status' && $v != 'TRANS_OK') {
                return true;
            }
        }
        return false;
    }

    public function get_errors() {
        if ($this->has_error()) {
            $errors = array();
            foreach ($this->responses[$this->i - 1] as $k => $v) {
                if ($k != 'status' && $v != 'TRANS_OK') {
                    $errors[$k] = $v;
                }
            }
            return $errors;
        }
        return array();

    }
    /**
     * send_command:
     * Logs the sent command and returns the next scheduled response
     */
    public function send_command($cmd, $commit = true) {
        $this->add_data('cmd', $cmd);
        if ($commit) {
            $this->add_data('commit', true);
        }
        $this->_add_invocation($this->current_tr);
        $this->current_tr = array();
        return $this->_next_response('send_command');
    }

    /**
     * send_admin_command:
     * Logs the send admin command and returns the next scheduled response
     */
    function send_admin_command($cmd, $commit = true) {
        return $this->send_command($cmd, $commit);
    }

    function validate_command($cmd) {
        return $this->send_command($cmd, false);
    }

}
