<?php

include_once('Conf.php');

/**
 * TestGenerator:
 * This abstract class is intended to be the base class for the test hierarchy.
 * When used correctly, it would allow us to write test cases as code, instead
 * of separated files, facilitating code reuse and review of the test cases.
 *
 * Usage:
 *  class TestSomething {
 *    public $includes = array('TestedClass.php');
 *    public function testCaseA() { $this->_scenario(1, 2); }
 *    public function testCaseB() { $this->_scenario(4, 5); }
 *    private function _scenario($input, $expected) {
 *      $a = TestedClass::add($input, 1);
 *      $this->assertEqual($a, $expected);
 *    }
 *  }
 *
 *  $test = new TestSomething();
 *  $test->generate('/tmp');
 */
abstract class TestGenerator {

    /**
     * generate:
     * Runs the generation algorithm. This method is intended to be called from
     * subclasses instances. It iterates the class methods and generates a test
     * for each method whose name starts with "test".
     */
    public function generate($targetDir) {
        $className = get_class($this);
        echo "Generating tests for: {$className}\n";
        foreach (get_class_methods($this) as $fn) {
            if (substr($fn, 0, 4) == 'test') {
                $this->generateTest($targetDir, $className, $fn);
            }
        }
    }

    /**
     * generateTest:
     * Writes a test case file for the given function based on the target directory.
     * The function is expected to receive zero parameters and produce zero output on
     * success. An exception should be the right way to signal errors.
     */
    private function generateTest($targetDir, $className, $functionName) {
        $path = "{$targetDir}/{$className}/{$functionName}.phpt";
        echo "- {$functionName} -> {$path}\n";
        $desc = $this->getTestDescription($className, $functionName);
        $desc = "{$functionName} - {$desc}";
        $includes = $this->getIncludes();
        $topdir = $this->getTopDir();
        $expect = '';

        $code = "<?php
            set_include_path(  '{$topdir}/php/common'
            . PATH_SEPARATOR . '{$topdir}/php/tests/common'
            . PATH_SEPARATOR . '{$topdir}/php/dummy'
            . PATH_SEPARATOR . '{$topdir}/php/tests/generated'
            . PATH_SEPARATOR . '{$topdir}/regress_final/include'
            . PATH_SEPARATOR . '{$topdir}/regress_final/apps'
            );
            {$includes}
            \$t = new {$className}();
            \$t->{$functionName}();
        ?>";

        $test = sprintf("--TEST--\n{$desc}\n--FILE--\n{$code}\n--EXPECT--\n{$expect}\n");
        $dir = dirname($path);
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        file_put_contents($path, $test);
    }

    /**
     * assertTrue:
     * The preferred way to assert that some expression evaluates to true
     */
    public function assertTrue($a) {
        return $this->assertEqual($a, true);
    }

    /**
     * assertFalse:
     * The preferred way to assert that some expression evaluates to false
     */
    public function assertFalse($a) {
        return $this->assertEqual($a, false);
    }

    /**
     * assertEqual:
     * Asserts that two expressions evaluate the same. In case of error, and exception
     * is thrown, explaining the difference found. Use extensively.
     */
    public function assertEqual($a, $b) {
        if ($a !== $b) {
            $dumpa = var_export($a, true);
            $dumpb = var_export($b, true);
            throw new Exception("Assertion broken: [{$dumpa}] !== [{$dumpb}]");
        }
        return true;
    }

    /**
     * getTestDescription:
     * Retrieves the description of a test case from the docstring comment on top
     * of it. It does a best effort approach to make it a one liner.
     */
    private function getTestDescription($className, $functionName) {
        $reflection = new ReflectionClass($className);
        $doc = $reflection->getMethod($functionName)->getDocComment();
        $comment = explode("\n", $doc);
        $desc = array();
        foreach ($comment as $line) {
            $desc[] = trim(preg_replace('/^\s*\/?\*{0,2}\/?/', '', $line));
        }
        return trim(implode(' ', $desc));
    }

    /**
     * getTopDir:
     * Retrieves the TOPDIR regress variable.
     */
    private function getTopDir() {
        return Conf::getVar('TOPDIR');
    }

    /**
     * getIncludes:
     * Iterates $this->includes and generates the corresponding 'include_once' directives
     */
    private function getIncludes() {
        return implode("\n", array_map(array('self', 'toInclude'), $this->includes));
    }

    /**
     * toInclude:
     * Returns a include_once directive for the given file
     */
    private static function toInclude($file) {
        return "include_once('{$file}');";
    }

}
