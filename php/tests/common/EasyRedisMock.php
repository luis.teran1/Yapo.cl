<?php

include_once('Mock.php');

use Yapo\RedisInstance;

class EasyRedisMock extends Mock implements RedisInstance {

    public function __construct($redis){
    }

    public function __call($name, $arguments)
    {
        $command = $name;
        foreach ($arguments as $arg) {
            if (is_array($arg)) {
                $command .= ' [';
                $first = true;
                foreach ($arg as $v) {
                    if (!$first) {
                        $command .= ' ';
                    }
                    $command .= $v;
                    $first = false;
                }
                $command .= ']';
            } else {
                $command .= " {$arg}";
            }
        }
        $this->_add_invocation(trim($command));
        return $this->_next_response($name);
    }
}
