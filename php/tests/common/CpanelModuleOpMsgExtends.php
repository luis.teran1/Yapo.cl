<?php

namespace Yapo;

class CpanelModuleOpMsgExtends extends CpanelModuleOpMsg
{
    public function getRedisKey($source, $location)
    {
        return parent::getRedisKey($source, $location);
    }
    
    public function badSource($sourceDesktop, $sourceMsite)
    {
        return parent::badSource($sourceDesktop, $sourceMsite);
    }
    
    public function badDate()
    {
        return parent::badDate();
    }
  
    public function badMsg()
    {
        return parent::badMsg();
    }

    public function hasErrors()
    {
        return parent::hasErrors();
    }

    public function adminDel()
    {
        return parent::adminDel();
    }

    public function adminAdd()
    {
        return parent::adminAdd();
    }
}
