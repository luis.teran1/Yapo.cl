<?php

class Response
{
    public $code;
    public $body;

    public function __construct($code, $body)
    {
        $this->code = $code;
        $this->body = $body;
    }
    public function __toString()
    {
        return json_encode($this->body);
    }
}
