<?php

class bImage
{
	public $bImageDummyResponse;
	public $bImageDummyHasError;
	public $bImageDummyData;
	public $_image_buffer;

	public function __construct($file) {
		global $bImageDummyResponse;
		global $bImageDummyHasError;
		$this->bImageDummyResponse = $bImageDummyResponse;
		$this->bImageDummyHasError = $bImageDummyHasError;
		$this->bImageDummyData = array(
			'file'=>$file
		);
	}

	public function has_error() {
		return $this->bImageDummyHasError;
	}

	public function get_error() {
		return 'BIMAGE_DUMMY_ERROR';
	}

	public function create_image() {
		return true;
	}

	public function destruct() {
		return true;
	}

	public function set_image_quality($quality) {
	}
}
