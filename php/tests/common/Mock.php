<?php

class Mock
{

    protected $invocations = array();
    protected $responses = array();
    protected $i = 0;

    /**
     * _add_response:
     * Schedule $r as a response to be given to future transacion invocation.
     * Responses would be returned on FIFO order.
     */
    public function _add_response($r) {
        $this->responses[] = $r;
        return $this;
    }

    /**
     * _add_responses:
     * Schedule all responses in $r for future transaction invocations
     */
    public function _add_responses(array $rs) {
        foreach ($rs as $r) {
            $this->_add_response($r);
        }
        return $this;
    }

    /**
     * _get_invocations:
     * Returns an array of all the invocations (send_command) performed over
     * this object. Each invocation is an array of key values, the ones that
     * would have been sent to trans
     */
    public function _get_invocations() {
        return $this->invocations;
    }

    /**
     * _add_invocation:
     * Used to log that a call has been made
     */
    protected function _add_invocation($call) {
        $this->invocations[] = $call;
    }

    /**
     * _next_response:
     * Returns the next scheduled response. The algorithm could consider using
     * the called function name and have independent response queues per function
     * but not today
     */
    protected function _next_response($functionName) {
        return $this->responses[$this->i++];
    }
}
