<?php

class RedisMock
{
	public function __construct() {
	}

	public function hMSet($key, $value) {
		if ($key == 'mcp1xapp_id_INVALID_PARTNER') {
			return false;
		}
		return true;
	}
}
