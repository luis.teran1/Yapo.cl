<?php

include_once('MetaMock.php');

/**
 * LoggerMock: The name says it all
 * Intended usage:
 *      $logger = new LoggerMock();
 *      $victim = new TestedClass($logger);
 *      $response = $victim->testedMethod(123, 'ok');
 *      $invocations = $logger->_get_invocations();
 *      assert $invocations == $expectedInvocations;
 */
class LoggerMock extends Mock {

    /**
     * logError:
     * Return wether the user is logged or not
     */
    public function logError($method, $message)
    {
        $this->_add_invocation(array('logError', $method, $message));
    }

    /**
     * logWarning:
     * Return wether the user is logged or not
     */
    public function logWarning($method, $message)
    {
        $this->_add_invocation(array('logWarning', $method, $message));
    }

}
