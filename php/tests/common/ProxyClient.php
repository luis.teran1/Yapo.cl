<?php

class ProxyClient
{
    public function doRequest($method, $host, $port, $path, $body = array(), $headers = array())
    {
        global $codeResponse;
        $code = isset($codeResponse) ? $codeResponse : 200;
        return new Response($code, array($body, $method, $path));
    }
}
