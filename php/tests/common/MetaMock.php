<?php

include_once('Mock.php');

class MetaMock extends Mock
{

    public function __call($name, $arguments)
    {
        $call = array(
            'function' => $name,
            'arguments' => $arguments,
        );
        $this->_add_invocation($call);
        return $this->_next_response($name);
    }
}
