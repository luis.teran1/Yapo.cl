<?php

include_once('MetaMock.php');

/**
 * AccountSessionMock: The name says it all
 * Intended usage:
 *      $acc_session = new AccountSessionMock();
 *      $acc_session->_add_response(array(
 *          True
 *      ));
 *      $victim = new TestedClass($acc_session);
 *      $response = $victim->testedMethod(123, 'ok');
 *      $invocations = $acc_session->_get_invocations();
 *      assert $response == $expected;
 *      assert $invocations == $expectedInvocations;
 *
 * This allows to configure individual replies for each expected account_session call
 * and check which arguments were supplied to each invocation.
 */
class AccountSessionMock extends Mock {

    /**
     * is_logged:
     * Return wether the user is logged or not
     */
    public function is_logged() {
        $invocation = $this->_next_response(__METHOD__);

        $this->_add_invocation(array('is_logged', $invocation));
        return $invocation;
    }

    /**
     * get_param:
     * Return the requested param if it exists
     */
    public function get_param($key) {
        $invocation = $this->_next_response(__METHOD__);

        $this->_add_invocation(array($key, $invocation));
        return $invocation;
    }
}
