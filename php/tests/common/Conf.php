<?php

/**
 * Conf:
 * This class exports the static method getVar, that retrieves a variable from in.conf.
 * The file is parsed just once and stored on a static variable. Calling getVar multiple
 * times won't incurr in additional parsing and/or system calls.
 */
class Conf {

    static private $topdir = '~';

    /**
     * setTopDir:
     * Sets the path for yapo root directory. If not set, the user's home
     * would be searched for the in.conf, instead. It's ideal to call this
     * function, once, before any getVar is made.
     */
    public static function setTopDir($topdir) {
        self::$topdir = $topdir;
    }

    /**
     * getVar:
     * Retrieves, in a case sensitive fashion, a value from in.conf.
     * Returns a string with the corresponding value or false, on failure
     */
    public static function getVar($varName) {
        $file = self::getConfFile();
        $conf = self::parseConfFile($file);
        return array_key_exists($varName, $conf) ? $conf[$varName] : false;
    }

    /**
     * getConfFile:
     * Returns the cached conf file path
     */
    private static function getConfFile() {
        static $file = null;
        if (!$file) {
            $file = self::doGetConfFile();
        }
        return $file;
    }

    /**
     * doGetConfFile:
     * Locates the appropiate in.conf file path
     */
    private static function doGetConfFile() {
        $where = self::$topdir;
        $files = `find {$where} -name in.conf`;
        foreach (explode("\n", $files) as $file) {
            if (strpos($file, 'regress') !== false) {
                return $file;
            }
        }
        return false;
    }

    /**
     * parseConfFile:
     * Returns the cached conf file parsed array
     */
    private static function parseConfFile($file) {
        static $conf = null;
        static $conffile = null;
        if (!$conf || $file != $conffile) {
            $conf = self::doParseConfFile($file);
            $conffile = $file;
        }
        return $conf;
    }

    /**
     * doParseConfFile:
     * Fetches the given path and parses it into a $key => $val array
     */
    private static function doParseConfFile($file) {
        $conf = array();
        $fd = fopen($file, "r");
        if (!$fd) {
            throw new Exception("Could not load conf from [{$file}]");
        }
        while (!feof($fd)) {
            $line = fgets($fd);
            $parts = explode('=', $line, 2);
            if (count($parts) < 2) {
                // skip bad lines
                continue;
            }
            list($key, $value) = $parts;
            if ($key) {
                $conf[$key] = trim($value);
            }
        }
        fclose($fd);
        return $conf;
    }

}
