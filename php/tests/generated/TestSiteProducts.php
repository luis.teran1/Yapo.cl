<?php

class TestSiteProducts extends TestGenerator
{

    public function __construct()
    {
        $this->includes = array(
            'bTransactionMock.php',
            'MetaMock.php',
            'autoload_lib.php',
            'init.php',
            'TestGenerator.php',
            'TestSiteProducts.php',
            'autoload_lib.php',

        );
    }

    public function _scenario(
        $productInfo,
        $extraData,
        $expected,
        $expectedInvocations,
        $trResponses = array(),
        $commit = true
    ) {
        $trans = new bTransactionMock();
        $trans->_add_responses($trResponses);

        $victim = new Yapo\SiteProducts($trans, $commit);
        $response = $victim->genericApply($productInfo, $extraData);

        $this->assertEqual($response, $expected);
        $this->assertEqual($trans->_get_invocations(), $expectedInvocations);
    }

    public function testBumpWError()
    {
        $productInfo = array(
            "product_id" => 1,
            "ad_id" => 65,
            "action_id" => 25,
            "doc_type" => "bill",
            "remote_addr" => "127.0.0.1",
            "remote_browser" => "TuPoto"
        );
        $extraData = array();
        $expected = array(
            0 => false,
            1 => array ('status' => 'TRANS_ERROR', 'ad_id' => 'ERROR_AD_NOT_FOUND')
            );
        $expectedInvocations = array(
                0 =>
                array (
                    'ad_id' => 65,
                    'doc_type' => 'bill',
                    'remote_addr' => '127.0.0.1',
                    'remote_browser' => 'TuPoto',
                    'batch' => '0',
                    'cmd' => 'bump_ad',
                    'commit' => true,
                ),
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_ERROR',
                'ad_id' => 'ERROR_AD_NOT_FOUND',
            ),
        );

        $this->_scenario($productInfo, $extraData, $expected, $expectedInvocations, $trResponses);
    }
    public function testBump()
    {
        $productInfo = array(
            "product_id" => 1,
            "ad_id" => 65,
            "action_id" => 25,
            "doc_type" => "bill",
            "remote_addr" => "127.0.0.1",
            "remote_browser" => "TuPoto"
        );
        $extraData = array();
        $expected = array(
            0 => true,
            1 => array ('status' => 'TRANS_OK')
        );
        $expectedInvocations = array(
                0 =>
                array (
                    'ad_id' => 65,
                    'doc_type' => 'bill',
                    'remote_addr' => '127.0.0.1',
                    'remote_browser' => 'TuPoto',
                    'batch' => '0',
                    'cmd' => 'bump_ad',
                    'commit' => true,
                ),
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
            ),
        );

        $this->_scenario($productInfo, $extraData, $expected, $expectedInvocations, $trResponses);
    }

    public function testBumpNoCommit()
    {
        $productInfo = array(
            "product_id" => 1,
            "ad_id" => 65,
            "action_id" => 25,
            "doc_type" => "bill",
            "remote_addr" => "127.0.0.1",
            "remote_browser" => "TuPoto"
        );
        $extraData = array();
        $expected = array(
            0 => true,
            1 => array ('status' => 'TRANS_OK')
        );
        $expectedInvocations = array(
                0 =>
                array (
                    'ad_id' => 65,
                    'doc_type' => 'bill',
                    'remote_addr' => '127.0.0.1',
                    'remote_browser' => 'TuPoto',
                    'batch' => '0',
                    'cmd' => 'bump_ad',
                    'commit' => true,
                ),
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
            ),
        );

        $expectedInvocations = array(
                0 =>
                array (
                    'ad_id' => 65,
                    'doc_type' => 'bill',
                    'remote_addr' => '127.0.0.1',
                    'remote_browser' => 'TuPoto',
                    'batch' => '0',
                    'cmd' => 'bump_ad',
                ),
        );
        $this->_scenario($productInfo, $extraData, $expected, $expectedInvocations, $trResponses, false);
    }

    public function testDailyBump()
    {
        $productInfo = array(
            "product_id" => 8,
            "ad_id" => 65,
            "action_id" => 25,
            "doc_type" => "bill",
            "remote_addr" => "127.0.0.1",
            "remote_browser" => "TuPoto"
        );
        $extraData = array();
        $expected = array(
            0 => true,
            1 => array ('status' => 'TRANS_OK')
        );
        $expectedInvocations = array(
                0 =>
                array (
                    'ad_id' => 65,
                    'action_id' => 25,
                    'remote_addr' => '127.0.0.1',
                    'counter' => '7',
                    'daily_bump' => '1',
                    'batch' => '1',
                    'cmd' => 'weekly_bump_ad',
                    'commit' => true,
                ),
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
            ),
        );

        $this->_scenario($productInfo, $extraData, $expected, $expectedInvocations, $trResponses);
    }

    public function testPackWExtraData()
    {
        $productInfo = array(
            "product_id" => 21020,
            "account_id" => 9,
            "payment_group_id" => 25,
            "doc_type" => "bill",
            "remote_addr" => "127.0.0.1",
            "remote_browser" => "TuPoto"
        );
        $extraData = array(
                "type" => 2,
                "period" => 1,
                "slots" => 20,
        );
        $expected = array(
            0 => true,
            1 => array ('status' => 'TRANS_OK')
        );
        $expectedInvocations = array(
                0 =>
                array (
                    'account_id' => 9,
                    'payment_group_id' => 25,
                    'product_id' => 21020,
                    'type' => 2,
                    'period' => 1,
                    'slots' => 20,
                    'cmd' => 'insert_pack',
                    'commit' => true,
                ),
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
            ),
        );

        $this->_scenario($productInfo, $extraData, $expected, $expectedInvocations, $trResponses);
    }
}
