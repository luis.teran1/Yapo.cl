<?php

function usage() {
    global $argv;
    echo "Usage: php {$argv[0]} <topdir>\n"
        ."Example: php {$argv[0]} .\n"
        ."         php {$argv[0]} ~/yapo_cl\n"
        ."         php {$argv[0]} /home/miguel/yapo_cl\n";
    exit(1);
}

if (!isset($argv[1])) {
    usage();
}

$topdir = $argv[1];
if (!is_dir($topdir) || !is_readable($topdir) || !is_executable($topdir)) {
    echo "Can't find/read directory: {$topdir}\n";
    usage();
}

set_include_path(  "{$topdir}/php/common"
. PATH_SEPARATOR . "{$topdir}/php/tests/generated"
. PATH_SEPARATOR . "{$topdir}/php/tests/common"
. PATH_SEPARATOR . "{$topdir}/php/common/include"
);

include_once("autoload_lib.php");
include_once("Conf.php");
include_once("TestGenerator.php");

Conf::setTopDir($topdir);

$srcDir  = "{$topdir}/php/tests/generated";
$destDir = "{$srcDir}/cases";

foreach (scandir($srcDir) as $fileName) {
    if ($fileName == '.' || $fileName == '..') {
        continue;
    }
    $info = pathinfo($fileName);
    if (array_key_exists('extension', $info) && $info['extension'] == 'php') {
        include_once("$fileName");
    }
    $className = preg_replace("/\.php$/", "", $fileName);
    if (!class_exists($className)) {
        continue;
    }
    $instance = null;
    try {
        $instance = new $className;
    } catch (Exception $e) {}
    if ($instance instanceof TestGenerator) {
        echo "Found test class: {$className}\n";
        $instance->generate($destDir);
    } else {
        echo "Found NON-test class: {$className}\n";
    }
}
