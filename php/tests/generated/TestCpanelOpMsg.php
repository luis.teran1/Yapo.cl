<?php

class TestCpanelOpMsg extends TestGenerator
{

    private $classTest = 'Yapo\CpanelModuleOpMsg';
    private $redisKey = 'redis_cardata';
    public function __construct()
    {
        $this->includes = array(
            'TestGenerator.php',
            'TestCpanelOpMsg.php',
            'autoload_lib.php',
            'init.php',
            'EasyRedisMock.php'
        );
    }
    //Funci�n para instanciar una clase y utilizar sus metodos
    private function scenarioClass(
        $function,
        $functionData,
        $expected,
        $constructData,
        $redisMock,
        $redisResponse,
        $expVectedInvocations
    ) {
        $class = new ReflectionClass($this->classTest);
        $instance = $class->newInstanceArgs($constructData);
        $method = $class->getMethod($function);
        $method->setAccessible(true);
        
        if (!empty($redisResponse) && isset($redisMock)) {
            $redisMock->_add_responses($redisResponse);
        }
        
        $response = $method->invokeArgs($instance, $functionData);
        $this->assertEqual($response, $expected);
        if (!empty($expVectedInvocations) && isset($redisMock)) {
            $this->assertEqual($redisMock->_get_invocations(), $expVectedInvocations);
        }
    }

    public function testGetRedisKey()
    {
        $argsData = array('desktop', 'dashboard', 'add');
        $expected = 'rcd1xop_msg_desktop_dashboard';
        $this->scenarioClass('getRedisKey', $argsData, $expected, array(), null, null, null);
    }

    public function testBadSourceFalse()
    {
        $argsDataFalse = array('desktop', 'msite');
        $expectedFalse = false;
        $this->scenarioClass('badSource', $argsDataFalse, $expectedFalse, array(), null, null, null);
    }

    public function testBadSourceTrue()
    {
        $argsDataTrue = array(null, null);
        $expectedTrue = true;
        $this->scenarioClass('badSource', $argsDataTrue, $expectedTrue, array(), null, null, null);
    }
    
    public function testBadDateFalse()
    {
        $argsDataFalse = array('23:59', '12/12/2400', '285581', '60');
        $expectedFalse = false;
        $this->scenarioClass('badDate', $argsDataFalse, $expectedFalse, array(), null, null, null);
    }

    public function testBadDateTrue()
    {
        $argsDataTrue = array('23-59', '12-12-2040', '20', '60');
        $expectedTrue = true;
        $this->scenarioClass('badDate', $argsDataTrue, $expectedTrue, array(), null, null, null);
    }
    
    public function testBadMsgFalse()
    {
        $argsDataFalse = array('HOLA MUNDO PRUEBA TEST UNITARIO', 'desktop', 'msite', '170', '41', '3');
        $expectedFalse = false;
        $this->scenarioClass('badMsg', $argsDataFalse, $expectedFalse, array(), null, null, null);
    }

    public function testBadMsgTrue()
    {
        $argsDataTrue = array('', null, null, '170', '41', '3');
        $expectedTrue = true;
        $this->scenarioClass('badMsg', $argsDataTrue, $expectedTrue, array(), null, null, null);
    }

    public function testHasErrorFalse()
    {
        $expectedFalse = false;
        $optionFalse = array(
            '12/12/2040',
            '23:59',
            'desktop',
            'desktop',
            'msite',
            'dashboard',
            null,
            'MENSAJE DE TEST UNITARIO',
            null,
            'global',
            '285581',
            null,
        );
        $this->scenarioClass("hasErrors", array(), $expectedFalse, $optionFalse, null, null, null);
    }


    public function testHasErrorTrue()
    {
        $expectedTrue = true;
        $optionTrue = array(
            null,
            null,
            'desktop',
            'desktop',
            'msite',
            'dashboard',
            null,
            null,
            'add',
            'direct',
            '285581',
            null,
        );
        $this->scenarioClass("hasErrors", array(), $expectedTrue, $optionTrue, null, null, null);
    }

    public function testAdminDelDesktop()
    {
        $expected = 1;
        $redisMock = new EasyRedisMock($this->redisKey);
        $responses = array(1);
        $optionDesktop = array(
            null,
            null,
            'desktop',
            'desktop',
            'msite',
            'dashboard',
            null,
            null,
            'del',
            'global',
            null,
            $redisMock,
        );
        $expVectedInvocationsDesktop = array(
            'del rcd1xop_msg_'. $optionDesktop[2] . '_' . $optionDesktop[5]
        );
        $this->scenarioClass(
            "adminDel",
            array(),
            $expected,
            $optionDesktop,
            $redisMock,
            $responses,
            $expVectedInvocationsDesktop
        );
    }

    public function testAdminDelMsite()
    {
        $expected = 1;
        $redisMock = new EasyRedisMock($this->redisKey);
        $responses = array(1);
        $optionMsite = array(
            null,
            null,
            'msite',
            'desktop',
            'msite',
            'dashboard',
            null,
            null,
            'del',
            'global',
            null,
            $redisMock,
        );
        $expVectedInvocationsMsite = array(
            'del rcd1xop_msg_'. $optionMsite[2] . '_' . $optionMsite[5]
        );
        $this->scenarioClass(
            "adminDel",
            array(),
            $expected,
            $optionMsite,
            $redisMock,
            $responses,
            $expVectedInvocationsMsite
        );
    }

    public function testAdminAddDesktop()
    {
        $expected = 1;
        $redisMock = new EasyRedisMock($this->redisKey);
        $responses = array(0,0,0,0,0,1);
        $optionDesktop = array(
            '12/12/2040',
            '23:59',
            'desktop',
            'desktop',
            'msite',
            'dashboard',
            null,
            'MENSAJE DE TEST UNITARIO',
            null,
            'global',
            '285581',
            $redisMock,
        );
        $expireDesktop = $optionDesktop[0] . ' ' . $optionDesktop[1];
        $expVectedInvocationsDesktop = array(
            'hset rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' source ' . $optionDesktop[2],
            'hset rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' location ' . $optionDesktop[5],
            'hset rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' msg ' . $optionDesktop[7],
            'hset rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' time '. time(),
            'hset rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' expire ' . $expireDesktop,
            'expire rcd1xop_msg_'. $optionDesktop[2] .'_'. $optionDesktop[5] .' ' . $optionDesktop[10]
        );
        $this->scenarioClass(
            "adminAdd",
            array(),
            $expected,
            $optionDesktop,
            $redisMock,
            $responses,
            $expVectedInvocationsDesktop
        );
    }

    public function testAdminAddMsite()
    {
        $expected = 1;
        $redisMock = new EasyRedisMock($this->redisKey);
        $responses = array(0,0,0,0,0,1);
        $optionMsite = array(
            '12/12/2040',
            '23:59',
            'msite',
            'desktop',
            'msite',
            'dashboard',
            null,
            'MENSAJE DE TEST UNITARIO',
            null,
            'global',
            '285581',
            $redisMock,
        );
        $expireMsite = $optionMsite[0] . ' ' . $optionMsite[1];
        $expVectedInvocationsMsite = array(
            'hset rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' source ' . $optionMsite[2],
            'hset rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' location ' . $optionMsite[5],
            'hset rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' msg ' . $optionMsite[7],
            'hset rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' time '. time(),
            'hset rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' expire ' . $expireMsite,
            'expire rcd1xop_msg_'. $optionMsite[2] .'_'. $optionMsite[5] .' ' . $optionMsite[10],
        );
        $this->scenarioClass(
            "adminAdd",
            array(),
            $expected,
            $optionMsite,
            $redisMock,
            $responses,
            $expVectedInvocationsMsite
        );
    }
}
