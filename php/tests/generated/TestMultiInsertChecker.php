<?php

class TestMultiInsertChecker extends TestGenerator {

    const CONF_ROOT = "multi_insert_checker";

    public function __construct() {
        $this->includes = array(
            'TestGenerator.php',
            'TestMultiInsertChecker.php',
            'lib/Ads/MultiInsertChecker.php',
            'autoload_lib.php',
            'EasyRedisMock.php',
        );
        $this->bconf = array(
            "*" => array(
                self::CONF_ROOT => array(
                    "category" => array (
                        "1220" => array("enabled" => 1),
                        "1240" => array("enabled" => 1),
                        "2020" => array("enabled" => 1),
                        "2040" => array("enabled" => 1, "attempts" => 1),
                        "7020" => array("enabled" => 1, "ttl" => 2400),
                    ),
                    "enabled" => "1",
                    "ttl" => 600,
                    "attempts" => 3
                )
            )
        );
    }

    /**
     * Check config off response
     */
    public function testConfigOff() {
        $expected = true;
        $expectedInvocations = array ();
        $testConf = $this->bconf;
        $testConf["*"][self::CONF_ROOT]["enabled"] = "0";
        $category = 2020;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $this->_scenario($testConf, $category, $params, $is_pro, $expected, $expectedInvocations);
    }

    /**
     * Check return true if user is pro
     */
    public function testPro() {
        $expected = true;
        $expectedInvocations = array ();
        $category = 2020;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = true;
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations);
    }

    /**
     * Check return true if category was not sent
     */
    public function testEmptyCategory() {
        $expected = true;
        $expectedInvocations = array ();
        $category = '';
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations);
    }

    /**
     * Check return true if params were not sent
     */
    public function testEmptyParams() {
        $expected = true;
        $expectedInvocations = array ();
        $category = 2020;
        $params = array();
        $is_pro = false;
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations);
    }

    /**
     * Check return true if category was not configured
     */
    public function testCategoryNotConfigured() {
        $expected = true;
        $expectedInvocations = array ();
        $category = 2222;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations);
    }

    /**
     * Check return false if the user reached the limit
     */
    public function testOneParamLimited() {
        $expected = false;
        $expectedInvocations = array ("incr multi_insert_checker:2020:perico@palotes.cl");
        $category = 2020;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $responses = array(4);
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations, $responses);
    }

    /**
     * Check return false if the user reached the limit for the second parameter
     */
    public function testMultiParamLimited() {
        $expected = false;
        $expectedInvocations = array (
            'incr multi_insert_checker:2020:perico@palotes.cl',
            'incr multi_insert_checker:2020:65962077',
        );
        $category = 2020;
        $params = array('email' => 'perico@palotes.cl', 'phone' => '65962077');
        $is_pro = false;
        $responses = array(2, 4);
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations, $responses);
    }

    /**
     * Check category specific ttl preferred over default
     */
    public function testCategorySpecificTTL() {
        $expected = true;
        $expectedInvocations = array (
            'incr multi_insert_checker:7020:perico@palotes.cl',
            'setTimeout multi_insert_checker:7020:perico@palotes.cl 2400',
        );
        $category = 7020;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $responses = array(1, 1);
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations, $responses);
    }

    /**
     * Check category specific attempts preferred over default
     */
    public function testCategorySpecificAttempts() {
        $expected = false;
        $expectedInvocations = array (
            'incr multi_insert_checker:2040:perico@palotes.cl',
        );
        $category = 2040;
        $params = array('email' => 'perico@palotes.cl');
        $is_pro = false;
        $responses = array(2, 1);
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations, $responses);
    }

    /**
     * Check return true if the user was not reached the limit
     */
    public function testMultiParamSucces() {
        $expected = true;
        $expectedInvocations = array (
            'incr multi_insert_checker:2020:perico@palotes.cl',
            'setTimeout multi_insert_checker:2020:perico@palotes.cl 600',
            'incr multi_insert_checker:2020:65962077',
        );
        $category = 2020;
        $params = array('email' => 'perico@palotes.cl', 'phone' => '65962077');
        $is_pro = false;
        $responses = array(1, 1, 2);
        $this->_scenario($this->bconf, $category, $params, $is_pro, $expected, $expectedInvocations, $responses);
    }

    public function _scenario($conf, $category, $params, $is_pro,  $expected, $expectedInvocations, $responses = array()) {

        $redisMock = new EasyRedisMock("redis_session");
        if (!empty($responses)) {
            $redisMock->_add_responses($responses);
        }

        $victim = new Yapo\Ads\MultiInsertChecker($conf, $redisMock);
        $response = $victim->allowInsertAttempt($category, $params, $is_pro);

        $this->assertEqual($response, $expected);
        $this->assertEqual($redisMock->_get_invocations(), $expectedInvocations);
    }

}
