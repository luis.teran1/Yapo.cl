<?php

class TestTimer extends TestGenerator
{

    public function __construct()
    {
        $this->includes = array(
            'init.php',
            'TestGenerator.php',
            'LoggerMock.php',
            'TestTimer.php',
            'autoload_lib.php',

        );
    }

    public function scenario($timeToWait, $elapxedMax, $info, $expectedLog)
    {
        $logger = new LoggerMock();
        $timer = new Yapo\Timer($elapxedMax, $logger);
        $timesStart = microtime(true);
        if ($timeToWait > 0) {
            sleep($timeToWait);
        }
        $timer->shutdownInfo($timesStart, $info);
        $this->assertEqual($logger->_get_invocations(), $expectedLog);
    }

    public function testShortTime()
    {
        $expectedLog =  array(array (
            'logWarning',
            'DummyInfo',
            'Execution time: 1,000s',
        ));
        $this->scenario(1, 0, "DummyInfo", $expectedLog);
    }

    public function testLongTime()
    {
        $expectedLog =  array(array (
            'logWarning',
            'DummyInfo',
            'Execution time: 3,000s',
        ));
        $this->scenario(3, 1, "DummyInfo", $expectedLog);
    }

    public function testGetLastError()
    {
        //This line generate an error
        @$dummyVar = count();
        $expectedLog = array(
            array (
                'logError',
                'DummyInfo',
                "Last error on:TestTimer.php line:53 msj: count() expects at least 1 parameter, 0 given",
            ),
            array (
                'logWarning',
                'DummyInfo',
                'Execution time: 1,001s',
            ),
        );
        $this->scenario(1, 0, "DummyInfo", $expectedLog);
    }

    public function testNoLogGenerated()
    {
        $expectedLog =  array();
        $this->scenario(1, 10, "DummyInfo", $expectedLog);
    }
}
