<?php

class TestSingleRedisMutex extends TestGenerator {

    const CONF_ROOT = "multi_insert_checker";

    public function __construct() {
        $this->includes = array(
            'TestGenerator.php',
            'TestSingleRedisMutex.php',
            'autoload_lib.php',
            'RedisInstance.php',
            'EasyRedisMock.php',
        );

        $this->bconf = array(
            "controlpanel" => array(
                "Mutex" => array(
                    "SingleRedisMutex" => array(
                        "Redis" => "EasyRedisMock",
                        "redisConf" => "test_conf",
                        "retryCount" => 2,
                        "retryDelay" => 0,
                        "ttl"        => 10,
                        "NonceGenerator" => "puto",
                    )
                )
            )
        );

        $this->key = "some_key";
        $this->nonce = "puto";
        $this->ttl = $this->bconf['controlpanel']['Mutex']['SingleRedisMutex']['ttl'];
        $this->unlockCommand =
            'eval if redis.call("GET", KEYS[1]) == ARGV[1] then
                return redis.call("DEL", KEYS[1])
            else
                return 0
            '."end [{$this->key} {$this->nonce}] 1";
    }

    /**
     * Locking the mutex sets the lock key and returns a lock object
     */
    public function testLock() {
        $redisResponses = array (
            "1"
        );
        $expectedRedisInvocations = array (
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
        );

        $mutex = $this->getMutex($redisResponses);
        $lockRes = $mutex->lock($this->key);
        $this->assertTrue($lockRes instanceof Yapo\Mutex\SingleRedisMutexLock);
        $this->assertTrue($lockRes->getAcquiredTime() > 0);
        $this->assertEqual($lockRes->getToken(), $this->nonce);
        $this->assertEqual($lockRes->getResource(), $this->key);
        $this->assertEqual($this->redisMock->_get_invocations(), $expectedRedisInvocations);
    }

    /**
     * Re-locking a locked mutex fails
     */
    public function testReLock() {
        $redisResponses = array (
            "1",
            "0",
            "0",
        );
        $expectedRedisInvocations = array (
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
        );

        $mutex = $this->getMutex($redisResponses);
        $mutex->lock($this->key);
        $lockRes = $mutex->lock($this->key);
        $this->assertEqual($lockRes, false);
        $this->assertEqual($this->redisMock->_get_invocations(), $expectedRedisInvocations);
    }

    /**
     * Unlocking a locked mutex releases the lock
     */
    public function testUnlock() {
        $redisResponses = array (
            "1",
            "1",
        );
        $expectedRedisInvocations = array (
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
            $this->unlockCommand
        );

        $mutex = $this->getMutex($redisResponses);
        $lock = $mutex->lock($this->key);
        $unlockRes = $mutex->unlock($lock);
        $this->assertEqual($unlockRes, "1");
        $invocations = $this->redisMock->_get_invocations();
        $this->assertEqual($this->redisMock->_get_invocations(), $expectedRedisInvocations);
    }

    /**
     * Unlocking an unlocked mutex does nothing
     */
    public function testUnlockUnlocked() {
        $redisResponses = array (
            "0",
        );
        $expectedRedisInvocations = array (
            $this->unlockCommand
        );

        $mutex = $this->getMutex($redisResponses);
        $lock = new Yapo\Mutex\SingleRedisMutexLock($this->key, $this->nonce, 0, microtime(true) * 1000);
        $unlockRes = $mutex->unlock($lock);
        $this->assertEqual($unlockRes, "0");
        $invocations = $this->redisMock->_get_invocations();
        $this->assertEqual($this->redisMock->_get_invocations(), $expectedRedisInvocations);
    }

    /**
     * Retry on locked instance
     */
    public function testRetries() {
        $redisResponses = array (
            "0",
            "1",
        );
        $expectedRedisInvocations = array (
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
            "set {$this->key} {$this->nonce} [NX {$this->ttl}]",
        );

        $mutex = $this->getMutex($redisResponses);
        $lockRes = $mutex->lock($this->key);
        $this->assertTrue($lockRes instanceof Yapo\Mutex\SingleRedisMutexLock);
        $this->assertTrue($lockRes->getAcquiredTime() > 0);
        $this->assertEqual($lockRes->getToken(), $this->nonce);
        $this->assertEqual($lockRes->getResource(), $this->key);
        $this->assertEqual($this->redisMock->_get_invocations(), $expectedRedisInvocations);
    }

    private function getMutex($redisResponses) {
        $this->redisMock = new EasyRedisMock("redis_account");
        $this->redisMock->_add_responses($redisResponses);

        $nonceGen = new Yapo\Mutex\StaticNonceGenerator($this->nonce);
        return new Yapo\Mutex\SingleRedisMutex($this->bconf, $this->redisMock, $nonceGen);
    }

}

