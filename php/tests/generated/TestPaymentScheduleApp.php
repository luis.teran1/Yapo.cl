<?php
class TestPaymentScheduleApp extends TestGenerator
{
    private $classTest = 'Yapo\PaymentScheduleApp';
    private $redisKey = 'redis_cardata';
    public function __construct()
    {
        $this->includes = array(
            'TestGenerator.php',
            'TestPaymentScheduleApp.php',
            'autoload_lib.php',
            'EasyRedisMock.php'
        );
    }
    //Funci�n para instanciar una clase y utilizar sus metodos
    private function scenarioClass(
        $function,
        $functionData,
        $expected,
        $constructData,
        $redisMock,
        $redisResponse,
        $expVectedInvocations
    ) {
        $class = new ReflectionClass($this->classTest);
        $instance = $class->newInstanceArgs($constructData);
        $method = $class->getMethod($function);
        $method->setAccessible(true);
        
        $response = $method->invokeArgs($instance, $functionData);
        $this->assertEqual($response, $expected);
        if (!empty($expVectedInvocations) && isset($redisMock)) {
            $this->assertEqual($redisMock->_get_invocations(), $expVectedInvocations);
        }
    }

    public function testGetRedisKey()
    {
        $argsData = array('desktop', 'dashboard', 'cuentaproconpack@yapo.cl');
        $expected = 'rcd1xdirect_msg_cuentaproconpack@yapo.cl_desktop_dashboard';
        $redisMock = new EasyRedisMock('redis_cardata');
        $this->scenarioClass('getRedisKey', $argsData, $expected, array($redisMock), null, array(), array());
    }

    public function testDeleteMsgOpPrettyCase()
    {
        $obj = new \stdClass;
        $obj->email = 'cuentaproconpack@yapo.cl';
        $argsData = array( null, $obj);
        $redisMock = new EasyRedisMock($this->redisKey);
        $expectedInvocation = array(
            'rcd1xdirect_msg_cuentaproconpack@yapo.cl_desktop_dashboard',
            'rcd1xdirect_msg_cuentaproconpack@yapo.cl_msite_dashboard'
        );
        $redisMock->_add_responses(array(1, 1));
        $expected = array("status" => "success");
        $this->scenarioClass(
            'deleteMsgOp',
            $argsData,
            $expected,
            array($redisMock),
            null,
            null,
            $expectedInvocation
        );
    }

    public function testDeleteMsgOpUglyCase()
    {
        $argsData = array( null, null);
        $expected = array('{"error_code":"params","error_message":"Parameters missing"}');
        $this->scenarioClass(
            'deleteMsgOp',
            $argsData,
            $expected[0],
            array(1),
            null,
            null,
            array()
        );
    }
}
