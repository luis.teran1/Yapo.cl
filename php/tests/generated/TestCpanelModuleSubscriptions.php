<?php

class TestCpanelModuleSubscriptions extends TestGenerator
{

    private $classTest = 'Yapo\CpanelModuleSubscriptions';
    private $redisKey = 'redis_cardata';
    public function __construct()
    {
        $this->includes = array(
            'TestGenerator.php',
            'TestCpanelModuleSubscriptions.php',
            'PshClientMock.php',
            'autoload_lib.php',
            'init.php',
            'EasyRedisMock.php'
        );
    }

    //Funci�n para instanciar una clase y utilizar sus metodos
    private function scenarioClass(
        $function,
        $functionData,
        $expected,
        $constructData,
        $redisMock,
        $redisResponse,
        $expVectedInvocations
    ) {
        $class = new ReflectionClass($this->classTest);
        $instance = $class->newInstanceArgs($constructData);
        $method = $class->getMethod($function);
        $method->setAccessible(true);

        if (!empty($redisResponse) && isset($redisMock)) {
            $redisMock->_add_responses($redisResponse);
        }

        $response = $method->invokeArgs($instance, $functionData);
        $this->assertEqual($response, $expected);
        if (!empty($expVectedInvocations) && isset($redisMock)) {
            $this->assertEqual($redisMock->_get_invocations(), $expVectedInvocations);
        }
    }

    public function testGetRedisKey()
    {
        $argsData = array('desktop', 'dashboard', 'yapo@prueba.cl');
        $expected = 'rcd1xdirect_msg_yapo@prueba.cl_desktop_dashboard';
        $this->scenarioClass('getRedisKey', $argsData, $expected, array(), null, null, null);
    }

    public function testPshClientActionUpOk()
    {
        $pshClient = new PshClientMock();
        $pshClient->_add_response(array (true, ''));
        $construct = array(null, null, null, $pshClient, null);
        $argsData = array('up');
        $expected = array(true, '');
        $this->scenarioClass('pshClientAction', $argsData, $expected, $construct, null, null, null);
    }

    public function testPshClientActionDownOk()
    {
        $pshClient = new PshClientMock();
        $pshClient->_add_response(array (true, ''));
        $construct = array(null, null, null, $pshClient, null);
        $argsData = array('down');
        $expected = array(true, '');
        $this->scenarioClass('pshClientAction', $argsData, $expected, $construct, null, null, null);
    }

    public function testPshClientActionRemoveOk()
    {
        $pshClient = new PshClientMock();
        $pshClient->_add_response(array (true, ''));
        $construct = array(null, null, null, $pshClient, null);
        $argsData = array('remove');
        $expected = array(true, '');
        $this->scenarioClass('pshClientAction', $argsData, $expected, $construct, null, null, null);
    }

    public function testPshClientActionPayOk()
    {
        $pshClient = new PshClientMock();
        $pshClient->_add_response(array (true, ''));
        $construct = array(null, null, null, $pshClient, null);
        $argsData = array('pay');
        $expected = array(true, '');
        $this->scenarioClass('pshClientAction', $argsData, $expected, $construct, null, null, null);
    }

    public function testPshClientActionError()
    {
        $pshClient = new PshClientMock();
        $pshClient->_add_response(array (false, 'Problemas con el Cliente PSH'));
        $construct = array(null, null, null, $pshClient, null);
        $argsData = array('up');
        $expected = array(false, 'Problemas con el Cliente PSH');
        $this->scenarioClass('pshClientAction', $argsData, $expected, $construct, null, null, null);
    }
}
