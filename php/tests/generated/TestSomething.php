<?php

class TestSomething extends TestGenerator {

    public function __construct() {
        $this->includes = array(
            'TestGenerator.php',
            'TestSomething.php',
            'TestedClass.php',
            'bTransactionMock.php',
        );
    }

    /**
     * Check Miguel is puto
     * muy muy puto * po
     */
    public function testPuto() {
        $expected = array (1,2,3);
        $expectedInvocations = array ();
        $trResponse = array(
            'user_id' => 567,
            'status' => 'TRANS_OK',
        );
        $this->_scenario($trResponse, $expected, $expectedInvocations);
    }

    public function _scenario($trResponse, $expected, $expectedInvocations) {
        $trans = new bTransactionMock();
        $trans->_add_response($trResponse);

        $victim = new TestedClass($trans);
        $response = $victim->testedMethod(123, 'ok');

        $invocations = $trans->_get_invocations();
        $this->assertEqual($response, $expected);
        $this->assertEqual($invocations, $expectedInvocations);
    }

}
