<?php

class TestAssignedYapesosReport extends TestGenerator {

    public $includes = array(
        'TestGenerator.php',
        'TestAssignedYapesosReport.php',
        'AssignedYapesosReport.php',
        'bTransactionMock.php',
        'MetaMock.php',
        'autoload_lib.php',
        'init.php',
    );
    
    /**
     * Single invocation of AssignedYapesosReport::getUserEmail
     */
    public function testSingleGetUserEmail()
    {
        $account_ids = array('5');
        $expectedReplies = array('wasabi@yapo.cl');
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
                'email'  => 'wasabi@yapo.cl',
            ),     
        );
        $expectedInvocations = array(
            array(
                'account_id' => '5',
                'cmd' => 'get_account',
                'commit' => true,
            ),
        );
        $this->getUserMailScenario($account_ids, $expectedReplies, $trResponses, $expectedInvocations);
    }

    /**
     * Handle TRANS_ERROR on AssignedYapesosReport::getUserEmail
     */
    public function testGetUserEmailError()
    {
        $account_ids = array('5');
        $expectedReplies = array('');
        $trResponses = array(
            array(
                'status' => 'TRANS_ERROR',
                'email'  => 'wasabi@yapo.cl',
            ),     
        );
        $expectedInvocations = array(
            array(
                'account_id' => '5',
                'cmd' => 'get_account',
                'commit' => true,
            ),
        );
        $this->getUserMailScenario($account_ids, $expectedReplies, $trResponses, $expectedInvocations);
    }

    /**
     * AssignedYapesosReport::getUserEmail caches results for the same account_id
     */
    public function testGetUserEmailCache()
    {
        $account_ids = array('5', '8', '5');
        $expectedReplies = array('wasabi@yapo.cl', 'jengibre@yapo.cl', 'wasabi@yapo.cl');
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
                'email'  => 'wasabi@yapo.cl',
            ),
            array(
                'status' => 'TRANS_OK',
                'email'  => 'jengibre@yapo.cl',
            ),
        );
        $expectedInvocations = array(
            array(
                'account_id' => '5',
                'cmd' => 'get_account',
                'commit' => true,
            ),
            array(
                'account_id' => '8',
                'cmd' => 'get_account',
                'commit' => true,
            ),
        );
        $this->getUserMailScenario($account_ids, $expectedReplies, $trResponses, $expectedInvocations);
    }

    private function getUserMailScenario($account_ids, $expectedReplies, $trResponses, $expectedInvocations)
    {
        $trans = new bTransactionMock();
        $trans->_add_responses($trResponses);

        $object = new AssignedYapesosReport(null, $trans);
        $method = new ReflectionMethod('AssignedYapesosReport', 'getUserEmail');
        $method->setAccessible(true);

        foreach (array_map(null, $account_ids, $expectedReplies) as $input) {
            list($account_id, $expected) = $input;
            $reply = $method->invoke($object, $account_id);
            $this->assertEqual($reply, $expected);
        }

        $invocations = $trans->_get_invocations();
        $this->assertEqual($invocations, $expectedInvocations);
    }

    /**
     * The service returned no report
     */
    public function testGetReportEmpty()
    {
        $calls = array(
            array(
                'function'  => 'getAssignedReport',
                'arguments' => array('nunca', 'forever'),
            )
        );
        $expectedReplies = array(array());
        $trResponses = array();
        $crResponse = array();
        $this->getReportScenario($calls, $expectedReplies, $trResponses, $crResponse, $calls);
    }

    /**
     * The service returned no report
     */
    public function testGetReportNoTrans()
    {
        $calls = array(
            array(
                'function'  => 'getAssignedReport',
                'arguments' => array('2016-10-01', '2017-10-01'),
            )
        );
        $trResponses = array(array('status' => 'TRANS_ERROR'));
        $crResponse = array(
            array(
                'User_id' => '5',
                'Credits' => '314159',
                'ExtraData' => 'Untouched',
            )
        );
        $expectedReply = array();
        foreach ($crResponse as $reply) {
            $expected = $reply;
            $expected['Net_value'] = $reply['Credits'] * 81;
            $expected['Net_value_float'] = $reply['Credits'] * 0.81;
            $expectedReply[] = $expected;
        }
        $expectedReplies = array($expectedReply);
        $this->getReportScenario($calls, $expectedReplies, $trResponses, $crResponse, $calls);
    }

    /**
     * Regular usage of getReport
     */
    public function testGetReportOk()
    {
        $calls = array(
            array(
                'function'  => 'getAssignedReport',
                'arguments' => array('2016-10-01', '2017-10-01'),
            )
        );
        $trResponses = array(
            array(
                'status' => 'TRANS_OK',
                'email'  => 'wasabi@yapo.cl'
            )
        );
        $crResponse = array(
            array(
                'User_id' => '5',
                'Credits' => '314159',
                'ExtraData' => 'Untouched',
            ),
            array(
                'User_id' => '5',
                'Credits' => '123567',
                'ExtraData' => 'Untouched',
            ),
        );
        $expectedReply = array();
        foreach ($crResponse as $reply) {
            $expected = $reply;
            $expected['User_email'] = $trResponses[0]['email'];
            $expected['Net_value'] = $reply['Credits'] * 81;
            $expected['Net_value_float'] = $reply['Credits'] * 0.81;
            $expectedReply[] = $expected;
        }
        $expectedReplies = array($expectedReply);
        $this->getReportScenario($calls, $expectedReplies, $trResponses, $crResponse, $calls);
    }

    private function getReportScenario($calls, $expectedReplies, $trResponses, $crResponse, $expectedInvocations)
    {
        $trans = new bTransactionMock();
        $credits = new MetaMock();
        $object = new AssignedYapesosReport($credits, $trans);

        $trans->_add_responses($trResponses);
        $credits->_add_response($crResponse);

        foreach (array_map(null, $calls, $expectedReplies) as $input) {
            list($call, $expected) = $input;
            $reply = call_user_func_array(array($object, 'getReport'), $call['arguments']);
            $this->assertEqual($reply, $expected);
        }

        $invocations = $credits->_get_invocations();
        $this->assertEqual($invocations, $expectedInvocations);
    }
}
