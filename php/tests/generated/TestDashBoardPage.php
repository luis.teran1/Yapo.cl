<?php

class TestDashBoardPage extends TestGenerator
{
    private $expectedTransInvocations = array(
        0 =>
        array (
            'account_id' => array(
                66
            ),
            'page_number' => 0,
            'all_ad_params' => 1,
            'cmd' => 'get_account_ads2',
            'commit' => true,
        ),
    );

    private $expectedTransInvocationsEmpty = array();

    private $expectedAccountSessionOKInvocations = array (
        0 =>
        array (
            0 => 'is_logged',
            1 =>
            array (
                0 => true,
            ),
        ),
        1 =>
        array (
            0 => 'account_id',
            1 =>
            array (
                0 => 66,
            ),
        ),
    );

    private $expectedAccountSessionErrorInvocations = array (
        0 =>
        array (
            0 => 'is_logged',
            1 => false,
        ),
    );

    private $transErrorResponses = array(
        array(
            'status' => 'TRANS_ERROR',
        ),
    );

    private $transOKResponses = array(
        array(
            'status' => 'TRANS_OK',
            'page_ads' => '0',
            'n_ads' => '2',
            'n_pack_ads' => '2',
        ),
    );

    private $accountSessionOKResponses = array(
        array(
            true,
        ),
        array(
            66,
        ),
    );

    private $accountSessionErrorResponses = array(false);

    public function __construct()
    {
        $this->includes = array(

            'util.php',
            'autoload_lib.php',
            'TestGenerator.php',
            'TestDashBoardPage.php',
            'bTransactionMock.php',
            'AccountSessionMock.php',
            'init.php'
        );
    }

    public function _scenario(
        $expected,
        $expectedTransInvocations,
        $transResponses = array(),
        $expectedAccountSessionInvocations,
        $accountSessionResponses = array()
    ) {
        $trans = new bTransactionMock();
        $trans->_add_responses($transResponses);

        $acc_session = new AccountSessionMock();
        $acc_session->_add_responses($accountSessionResponses);

        $response = new bResponse();

        global $_SERVER;
        $_SERVER['REQUEST_URI'] = '';
        $_SERVER['HTTP_USER_AGENT'] = '';
        $_SERVER['REMOTE_ADDR'] = '';
        // Activa el almacenamiento en b�fer de la salida
        ob_start();
        $victim = new Yapo\DashboardPage($trans, $response, $acc_session);
        $response = $victim->showPage();
        ob_end_clean();

        $this->assertEqual($response, $expected);
        $this->assertEqual($trans->_get_invocations(), $expectedTransInvocations);
        $this->assertEqual($acc_session->_get_invocations(), $expectedAccountSessionInvocations);
    }

    public function testDashboardEveryThingOK()
    {
        $expected = array (
            'status' => 'TRANS_OK',
            'page_ads' => '0',
            'n_ads' => '2',
            'n_pack_ads' => '2',
        );

        $this->_scenario(
            $expected,
            $this->expectedTransInvocations,
            $this->transOKResponses,
            $this->expectedAccountSessionOKInvocations,
            $this->accountSessionOKResponses
        );
    }

    public function testDashboardUnlogged()
    {
        $expected = false;
        $this->_scenario(
            $expected,
            $this->expectedTransInvocationsEmpty,
            $this->transOKResponses,
            $this->expectedAccountSessionErrorInvocations,
            $this->accountSessionErrorResponses
        );
    }

    public function testDashboardTransError()
    {
        $expected = array ();

        $this->_scenario(
            $expected,
            $this->expectedTransInvocations,
            $this->transErrorResponses,
            $this->expectedAccountSessionOKInvocations,
            $this->accountSessionOKResponses
        );
    }
}
