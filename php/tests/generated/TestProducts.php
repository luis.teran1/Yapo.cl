<?php

class TestProducts extends TestGenerator
{

    public function __construct()
    {
        $this->includes = array(
            'init.php',
            'TestGenerator.php',
            'TestProducts.php',
            'autoload_lib.php',

        );
    }

    public function _scenario($function, $data, $expected) {
        $response = Yapo\Products::$function($data);
        $this->assertEqual($response, $expected);
    }

    public function testIsStoreTrue()
    {
        $this->_scenario('isStore', 6, true);
    }
    public function testIsStoreFalse()
    {
        $this->_scenario('isStore', 1, false);
    }

    public function testIsComboTrue()
    {
        $this->_scenario('isCombo', 1031, true);
    }

    public function testIsComboFalse()
    {
        $this->_scenario('isCombo', 1, false);
    }

    public function testIsPackTrue()
    {
        $this->_scenario('isPack', 21020, true);
    }

    public function testIsPackFalse()
    {
        $this->_scenario('isPack', 1, false);
    }

    public function testGetExtraDataBump()
    {
        $this->_scenario('getExtraData', array('product_id' => 1), array());
    }

    public function testGetExtraDataAutoBump()
    {
        $data = array('product_id' => 10, 'detail_params' => 'data1:value1;data2:value2');
        $expected = array('data1' => 'value1', 'data2' => 'value2');
        $this->_scenario('getExtraData', $data, $expected);
    }

    public function testGetExtraDataPack1()
    {
        $data = array('product_id' => 11010);
        $expected = array('type' => 'car', 'period' => '1', 'slots' => '010');
        $this->_scenario('getExtraData', $data, $expected);
    }

    public function testGetExtraDataPack2()
    {
        $data = array('product_id' => 10000);
        $expected = array('type' => 'car', 'period' => '0', 'slots' => '000');
        $this->_scenario('getExtraData', $data, $expected);
    }

    public function testGetExtraDataPack3()
    {
        $data = array('product_id' => 24125);
        $expected = array('type' => 'real_estate', 'period' => '4', 'slots' => '125');
        $this->_scenario('getExtraData', $data, $expected);
    }

    public function testGetExtraDataStore1()
    {
        $this->_scenario('getExtraData', array('product_id' => 5), array('expire_time' => '10 minutes'));
    }
    public function testGetExtraDataStore2()
    {
        $this->_scenario('getExtraData', array('product_id' => 7), array('expire_time' => '20 minutes'));
    }

    public function testSortProducts()
    {
        $products = array(
            array("product_id" => 21125, "order" => 4),
            array("product_id" => 1, "order" => 2),
            array("product_id" => 22050, "order" => 5),
            array("product_id" => 10, "order" => 1),
            array("product_id" => 5, "order" => 3),
        );

        $expected = array(
            array("product_id" => 10, "order" => 1),
            array("product_id" => 1, "order" => 2),
            array("product_id" => 5, "order" => 3),
            array("product_id" => 21125, "order" => 4),
            array("product_id" => 22050, "order" => 5),
        );

        $this->_scenario('sortProducts', $products, $expected);
    }

    public function testSortProductCombo()
    {
        $products = array(
            array("product_id" => 1011, "order" => 1),
            array("product_id" => 1, "order" => 12),
        );
        $expected = array(
            array("product_id" => 1011, "order" => 1),
        );
        $this->_scenario('sortProducts', $products, $expected);
    }

}
