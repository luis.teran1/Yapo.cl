<?php

class TestYapoRequest extends TestGenerator
{

    private $classTest = 'Yapo\YapoRequest';
    public function __construct()
    {
        $this->includes = array(
            'TestGenerator.php',
            'TestYapoRequest.php',
            'autoload_lib.php',
            'AccountSessionMock.php',
            'JSON.php',
            'init.php'
        );
    }
    //Funcion para instanciar una clase y utilizar sus metodos
    private function scenarioClass(
        $function,
        $functionData,
        $expected,
        $constructData,
        $expVectedInvocations
    ) {
        $class = new ReflectionClass($this->classTest);
        $instance = $class->newInstanceArgs($constructData);
        $method = $class->getMethod($function);
        $method->setAccessible(true);

        $response = $method->invokeArgs($instance, $functionData);
        $this->assertEqual($response, $expected);
    }

    public function executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body)
    {
        $accSession = new AccountSession();
        $endpoint_name = "testing";
        $endpoint_conf = array("method" => $method, "params" => $params, "path" => $uri);
        $service_conf = array(
            "host" => "www.yapo.cl",
            "port" => "9999",
            $endpoint_name => $endpoint_conf,
            "timeout" => "60");
        $action = $endpoint_name;
        $data = $data;
        $expected = array(
                           "method" => $method,
                           "host" => "www.yapo.cl:9999",
                           "uri" => $expected_uri,
                           "headers" => array("Accept" => "application/json"),
                           "body" => $expected_body,
                           "timeout" => "60"
                         );
        $argsData = array($service_conf, $action, $data);
        $this->scenarioClass($function, $argsData, $expected, array($accSession), null);
    }
    public function testParseConfigurationQueryParams()
    {
        $function = "parseConfiguration";
        $method = "get";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array("param1" => array("type" => "query"), "param2" => array("type" => "query"));
        $data = array("param1" => "1", "param2"=>"testParam");
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name."?param1=1&param2=testParam";
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationQueryParamsPathParams()
    {
        $function = "parseConfiguration";
        $method = "get";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/path1/path2/".$endpoint_name;
        $params = array("param1" => array("type" => "query"), "param2" => array("type" => "query"));
        $data = array(
            "param1" => "1",
            "pathParam1" => "pathValue1",
            "param2"=>"testParam",
            "pathparam2" => "pathValue2");
        $expected_uri = "/ms/api/v1/control-panel/pathValue1/pathValue2/".
            $endpoint_name.
            "?param1=1&param2=testParam";
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationOptionalQueryParams()
    {
        $function = "parseConfiguration";
        $method = "get";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array("param1" => array("type" => "query"), "param2" => array("type" => "query"));
        $data = array("param1" => "1");
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name."?param1=1";
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationNoQueryParams()
    {
        $function = "parseConfiguration";
        $method = "get";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array("param1" => array("type" => "query"), "param2" => array("type" => "query"));
        $data = array();
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationNoQueryParamsAllowed()
    {
        $function = "parseConfiguration";
        $method = "get";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array();
        $data = array("param1" => "1");
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationPostBody()
    {
        $function = "parseConfiguration";
        $method = "post";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array();
        $data = array("param1" => "value1");
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $expected_body = json_encode(array("param1"=>"value1"));
        
        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationPostNoBody()
    {
        $function = "parseConfiguration";
        $method = "post";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array();
        $data = array();
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
    public function testParseConfigurationParamsNoBody()
    {
        $function = "parseConfiguration";
        $method = "post";
        $endpoint_name = "testing";
        $uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $params = array("param1" => array("type" => "query"), "param2" => array("type" => "query"));
        $data = array();
        $expected_uri = "/ms/api/v1/control-panel/".$endpoint_name;
        $expected_body = null;

        $this->executeCase($function, $method, $endpoint_name, $uri, $params, $data, $expected_uri, $expected_body);
    }
}
