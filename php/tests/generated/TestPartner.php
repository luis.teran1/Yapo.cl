<?php

class Service1
{

};
class Service2
{

};

class TestPartner extends TestGenerator
{

    private $partner;

    public function __construct()
    {
        $this->includes = array(
            'TestGenerator.php',
            'TestPartner.php',
            'MetaMock.php',
            'autoload_lib.php'
        );

        $this->partner = array(
            'Name' => "PartnerTest",
            'Protocol' => "ftp",
            'ImgProtocol' => 'ftp',
            'ConnData' => "{user: telconf, domain: 0.0.0.0, password: 123123}",
            'ImgConnData' => '{}',
            'PathFile' => "./",
            'FileName' => "yapo_partner.xls",
            'PathImg' => "./",
            'Delimiter' => "|",
            'Rules' => "{}",
            'IRules' => "{}",
            'ApiPoint' => "http://m.partner.cl",
            'InsDelay' => "10",
            'MailTo' => "partner@yapo.cl",
            'KeyName' => "partner",
            'Market' => "1000",
            'CsvProtocol' => "ftp",
            'CsvConnData' => "{}",
            'TdProtocol' => "ftp",
            'TdConnData' => "{}",
            'RunTime' => "15:00",
            'ConfId' => "123",
            'Active' => null
        );
    }

    /**
     * Partner constructor saves arguments and adds ApiKey with no errors
     */
    public function testPartnerConstructor()
    {
        $originalData = $this->partner;
        $partner = new Yapo\Partner($this->partner);
        $this->assertFalse(isset($partner->errorMsg));

        $resultReflector = new ReflectionObject($partner);
        $data = $resultReflector->getProperty('data');
        $data->setAccessible(true);

        $dataPartner = $data->getValue($partner);

        $this->assertTrue(isset($dataPartner['ApiKey']));
        unset($dataPartner['ApiKey']);
        $this->assertEqual($dataPartner, $originalData);
    }

    /**
     * Partner::addServices queues services in given order
     */
    public function testAddServicesToPartner()
    {
        $partner = new Yapo\Partner($this->partner);
        $partner
            ->addService(new Service1())
            ->addService(new Service2());

        $expected = array("Service1", "Service2");

        $resultReflector = new ReflectionObject($partner);
        $services = $resultReflector->getProperty('services');
        $services->setAccessible(true);

        $servicesPartner = $services->getValue($partner);
        $serviceClasses = array_map('get_class', $servicesPartner);

        $this->assertEqual($serviceClasses, $expected);
    }

    /**
     * Partner::addService adds given service to the list, unchanged
     */
    public function testAddServiceToPartner()
    {
        $partner = new Yapo\Partner($this->partner);
        $service = array('name' => 'This is a service');

        $partner->addService($service);

        $resultReflector = new ReflectionObject($partner);
        $services = $resultReflector->getProperty('services');
        $services->setAccessible(true);

        $servicesPartner = $services->getValue($partner);

        foreach ($servicesPartner as $key => $value) {
            $this->assertEqual($value, $service);
        }
    }

    /**
     * Partner::create should call createPartner for each added service
     */
    public function testCreatePartnerFromServices()
    {
        $m1 = new MetaMock();
        $m2 = new MetaMock();

        $m1->_add_response(array('status' => 'ok'));
        $m2->_add_response(array('status' => 'ok'));

        $partner = new Yapo\Partner($this->partner);
        $partner
            ->addService($m1)
            ->addService($m2);

        $partner->create();

        $resultReflector = new ReflectionObject($partner);
        $services= $resultReflector->getProperty('services');
        $services->setAccessible(true);

        $servicesPartner = $services->getValue($partner);
        foreach ($servicesPartner as $key => $value) {
            $invocations = $value->_get_invocations();
            $this->assertEqual(count($invocations), 1);
            $this->assertEqual($invocations[0]['function'], 'createPartner');
        }
    }
}
