<?php

class TestBlocketApiService extends TestGenerator {

	private $blocket_api;

	public function __construct() {
		$this->includes = array(
			'TestGenerator.php',
			'TestBlocketApiService.php',
			'RedisMock.php',
			'bRedisClassMock.php',
			'autoload_lib.php',
			'init.php'
		);
		$this->blocket_api = array(
			'api_key'=>'',
			'expire_time'=>'300',
			'region_limit'=>'1,2,3,4,5,6,7,8,9,10,11,12,13,14,15',
			'ad_type_limit'=>'h,k,s,u',
			'request_limit'=>'100000',
			'appl_limit'=>'import,importdeletead,newad',
			'cat_limit'=>'1000,1220,1240,1260,2000,2020,2060,2040,2100,2080,2120,5000,5020,5040,5060,5160,4000,4020,4040,4060,9000,9020,9040,9060,6000,6140,6020,6060,6180,6080,6100,6120,6160,3000,3040,3060,3020,3080,7000,7020,7040,7060,7080,8000,8020',	'logging'=>'1',
			'logging'=>'1',
		);
	}

	public function testBlocketApiServiceConstructor() {
		$blocketApiService = new Yapo\BlocketApiService();
		$dataService = $blocketApiService->redisKeys;

		foreach($this->blocket_api as $key => $value) {
			$this->assertEqual($dataService[$key], $this->blocket_api[$key]);
		}
	}

	/**
	* Check return true if partner was created
	*/
	public function testAddPartner() {
		$data = array(
			'apiKey'=>'partner-name',
			'apiHash'=>'thisisthepartnershash',
			'KeyName'=>'nameTest'
		);
		$blocketApiService = new Yapo\BlocketApiService();
		$newPartner = $blocketApiService->createPartner($data);
		$this->assertTrue($newPartner);
	}

	/**
	* Check return error message if partner was not created
	*/
	public function testAddPartnerRedisFail() {
		$data = array(
			'ApiKey'=>'INVALID_PARTNER',
			'KeyName'=>'INVALID_PARTNER'
		);
		$blocketApiService = new Yapo\BlocketApiService();
		$errorException = 'BlocketRedis Error: when try to create partner';
		try {
			$result = $blocketApiService->createPartner($data);
		} catch (\Exception $e) {
			$error = $e->getMessage();
			$this->assertEqual($error,$errorException);
		}
	}
}
