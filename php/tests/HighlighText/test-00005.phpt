--TEST--
Test HighlighText - if the rule match enable warning_type message
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '1',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
    'warning_type' => 'WARNING_PHONE_IN_DESCRIPTION',
  ),
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'contactame por whatsapp en el +56 9 93724959 o por telegram.';

$ht = new HighlighText($bconf);
$htmlText = $ht->getHtml('body', $text, $word_marks);

var_dump($ht->warning_type);

?>
--EXPECT--
string(28) "WARNING_PHONE_IN_DESCRIPTION"
