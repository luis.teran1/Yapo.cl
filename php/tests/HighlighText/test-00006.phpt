--TEST--
Test HighlighText - match a string and substring
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
  ),
  array (
    'color' => '5',
    'regex' => 'what',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '0',
  )
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '5' => '#FF0000',
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'contactame por whatsapp en el +56 9 93724959 o por telegram.';

$ht = new HighlighText($bconf);
$htmlText = $ht->getHtml('body', $text, $word_marks);
echo $htmlText;

?>
--EXPECT--
contactame por <span class="WordBorderSolid" style="border-color: #00FF00"><span class="WordBorderSolid" style="border-color: #FF0000">what</span>sapp</span> en el +56 9 93724959 o por telegram.

