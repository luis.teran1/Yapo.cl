--TEST--
Test HighlighText - Happy path. Match 2 expresions
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
  ),
  array (
    'color' => '5',
    'regex' => '([\\+()]*\\d[^0-9\\n]*){8,11}',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '1',
    'warning_type' => 'WARNING_PHONE_IN_DESCRIPTION',
    'word_boundary' => '0',
  )
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '5' => '#FF0000',
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'contactame por whatsapp en el +56 9 93724959 o por telegram.';

$ht = new HighlighText($bconf);
$htmlText = $ht->getHtml('body', $text, $word_marks);
echo $htmlText;

?>
--EXPECT--
contactame por <span class="WordBorderSolid" style="border-color: #00FF00">whatsapp</span> en el <span class="WordBorderSolid" style="border-color: #FF0000">+56 9 93724959 o por telegram.</span>
