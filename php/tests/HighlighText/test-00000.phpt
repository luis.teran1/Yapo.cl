--TEST--
Test HighlighText - test the constructor
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '5' => '#FF0000',
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$ht = new HighlighText($bconf);
var_dump($ht);

?>
--EXPECT--
object(Yapo\HighlighText)#2 (5) {
  ["frame_ad"]=>
  bool(false)
  ["warning_type"]=>
  string(0) ""
  ["bconf":"Yapo\HighlighText":private]=>
  array(1) {
    ["controlpanel"]=>
    array(1) {
      ["modules"]=>
      array(1) {
        ["adqueue"]=>
        array(1) {
          ["highlight"]=>
          array(1) {
            ["color"]=>
            array(2) {
              [5]=>
              string(7) "#FF0000"
              [2]=>
              string(7) "#00FF00"
            }
          }
        }
      }
    }
  }
  ["text":"Yapo\HighlighText":private]=>
  NULL
  ["all_matches":"Yapo\HighlighText":private]=>
  array(0) {
  }
}
