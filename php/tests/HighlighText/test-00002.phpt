--TEST--
Test HighlighText - Happy Path. No match expresions
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
  ),
  array (
    'color' => '5',
    'regex' => '([\\+()]*\\d[^0-9\\n]*){8,11}',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '1',
    'warning_type' => 'WARNING_PHONE_IN_DESCRIPTION',
    'word_boundary' => '0',
  )
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '5' => '#FF0000',
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'un texto diferente que
no tiene ninguna cohincidencia
y esta escrito en varias lineas';

$ht = new HighlighText($bconf);
$htmlText = $ht->getHtml('body', $text, $word_marks);

echo $htmlText;

?>
--EXPECT--
un texto diferente que
no tiene ninguna cohincidencia
y esta escrito en varias lineas
