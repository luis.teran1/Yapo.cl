--TEST--
Test HighlighText - if the rule match enable the frame_ad
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '1',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
  ),
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'contactame por whatsapp en el +56 9 93724959 o por telegram.';

$ht = new HighlighText($bconf);
$htmlText = $ht->getHtml('body', $text, $word_marks);

var_dump($ht->frame_ad);

?>
--EXPECT--
bool(true)
