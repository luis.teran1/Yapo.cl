--TEST--
Test HighlighText - search a match in the title and other match in the body
--FILE--
<?php

include('autoload_lib.php');

use \Yapo\HighlighText as HighlighText;

$word_marks = array (
  array (
    'color' => '2',
    'regex' => 'whatsapp',
    'types' => '*',
    'where' => 'title',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '0',
    'word_boundary' => '1',
  ),
  array (
    'color' => '5',
    'regex' => '([\\+()]*\\d[^0-9\\n]*){8,11}',
    'types' => '*',
    'where' => 'body',
    'frame_ad' => '0',
    'categories' => '*',
    'pure_regex' => '1',
    'warning_type' => 'WARNING_PHONE_IN_DESCRIPTION',
    'word_boundary' => '0',
  )
);

$bconf = array('controlpanel' => array(
    'modules' => array(
        'adqueue' => array(
            'highlight' => array(
                'color' => array(
                    '5' => '#FF0000',
                    '2' => '#00FF00'
                    )
                )
            )
        )
    )
);

$text = 'contactame por whatsapp en el +56 9 93724959 o por telegram.';

$ht = new HighlighText($bconf);
$htmlText1 = $ht->getHtml('title', $text, $word_marks);
$htmlText2 = $ht->getHtml('body', $text, $word_marks);

echo $htmlText1."\n";
echo $htmlText2."\n";

?>
--EXPECT--
contactame por <span class="WordBorderSolid" style="border-color: #00FF00">whatsapp</span> en el +56 9 93724959 o por telegram.
contactame por whatsapp en el <span class="WordBorderSolid" style="border-color: #FF0000">+56 9 93724959 o por telegram.</span>
