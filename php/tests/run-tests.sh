#!/bin/sh
#
#   Title: run-tests.sh
#   Description: This script will run a php testsuite. It accepts a directory name as a parameter
#   Environment: It uses a variable called $JENKINS to know if it's necesary to write XML reports
#   Usage: ./run-tests.sh <dir-name> | for development
#       or JENKINS=1 ./run-tests.sh <dir-name> | for jenkins jobs

# This function receives a directory name and reads a .log file
#   to write passed and failed tests to.
# It cannot report skipped tests, and puts no time neither.
print_xml(){
    log_file=${1}.log
    report_file=${1}.xml

    # This is the way to escape &, <, > in XML docs
    replacement="s/&/\&amp;/g; s/>/\&gt;/g; s/</\&lt;/g"

    # If there is no log file, we are doomed
    if [ -f "$log_file" ]
    then
        echo "  *  log_file: $log_file"
    else
        echo "$log_file not found" 1>&2
        exit 1
    fi

    # If log file has quotation marks, delete them
    # I do this, because some tests has quitation marks in their names,
    #  and make XML parsers to explode
    sed -i 's/"/ /g' $log_file

    # Get number of passed, failed and skipped tests
    passed_tests=`grep PASSED $log_file | cut -d " " -f 1`
    failed_tests=`grep FAILED $log_file | cut -d " " -f 1`
    skipped_tests=`grep SKIPPED $log_file | cut -d " " -f 1`

    # If empty, set to zero
    if [ -z "$passed_tests" ]
    then
        passed_tests="0"
    fi
    if [ -z "$failed_tests" ]
    then
        failed_tests="0"
    fi
    if [ -z "$skipped_tests" ]
    then
        skipped_tests="0"
    fi

    # Write report header
    echo "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" > $report_file
    echo "<testsuites>" >> $report_file
    echo "  <testsuite name=\"$1\" tests=\"$passed_tests\" failures=\"$failed_tests\">" >> $report_file
    
    # Write passed tests
    grep "PASS " $log_file | sed "s/^PASS ....... //" | sed "$replacement" | while read line
    do
        echo "      <testcase name=\"$line\"/>" >> $report_file
    done

    # Write failed tests
    grep "FAIL " $log_file | sed "s/^FAIL ....... //" | sed "$replacement" | while read line
    do
        echo "      <testcase name=\"$line\">" >> $report_file

        # Write details. Why is this test failed?
        failed_test_name=`echo $line | cut -d "[" -f 2| cut -d "." -f 1`
        diff_file=${failed_test_name}.diff
        exp_file=${failed_test_name}.exp
        echo "          <failure>" >> $report_file
        echo "              Expected:" >> $report_file
        cat $exp_file | sed "$replacement" >> $report_file
        printf "\n" >> $report_file
        echo "              Diff:" >> $report_file
        cat $diff_file | sed "$replacement" >> $report_file
        printf "\n" >> $report_file

        # Close tags
        echo "          </failure>" >> $report_file
        echo "      </testcase>" >> $report_file
    done
    # Close report tags
    echo "  </testsuite>" >> $report_file
    echo "</testsuites>" >> $report_file
}

# I cannot receive zero or more than 1 directory
if [ "$#" != "1" ]
then
    echo "ERROR: $0 only accepts one directory" 1>&2
    exit 1
fi

# Fail if no directory with given name
if ! [ -d "$1" ]
then
    echo "ERROR: $1 directory not found" 1>&2
    exit 2
fi

# Get given tests directory
testsuite=$1

# Run testsuit and print results to a .log file
PHP_BIN=`pear config-get php_bin`
PHP_INI=`pear config-get php_ini`
$PHP_BIN -C -d output_buffering=1 /usr/share/pear/pearcmd.php run-tests -c "/usr/bin/php-cgi -c $PHP_INI" $testsuite > $testsuite.log

# If we are on jenkins, read this .log file and write .xml report
if [ "$JENKINS" == "1"  ]
then
    print_xml $testsuite
# If we are not on jenkins, developer wants to know tests results
else
    cat $testsuite.log
fi
