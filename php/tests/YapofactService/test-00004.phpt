--TEST--
YapofactService - adminReport - get admin report params filtered by all params
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';
include_once 'JSON.php';

$app = new Yapo\YapofactService();
$data = $app->adminReport(
    "2020-03-02",
    "2017-01-01",
    array(
        "Search_email" => "juanito@mena.com",
        "Search_name" =>  "juanito mena",
        "Search_id" => 800073,
        "Search_category" => 2020,
    )
);

print_r($data);
?>
--EXPECT--
Array
(
    [0] => Array
        (
        )

    [1] => get
    [2] => /api/v1/admin/report/2020-03-02/2017-01-01?Search_email=juanito@mena.com&Search_name=juanito mena&Search_id=800073&Search_category=2020
)
