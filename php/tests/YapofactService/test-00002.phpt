--TEST--
YapofactService - requestReport - Check Autofact report creation with error code
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';

global $codeResponse;
$codeResponse = 410;

$app = new Yapo\YapofactService();
$data = $app->requestReport(
    array(
        "User_id" => 1,
        "User_email" => "juanito@mena.com",
        "User_name" =>  "juanito mena",
        "List_id" => 800073,
        "Ad_category" => 2020,
        "Ad_subject" => "Aviso super bacanoso",
        "Pay_type" => "tupoto",
        "Plates" => "ABCD10"
    )
);

echo "Data is null? ".($data == null);
?>
--EXPECT--
Data is null? 1
