--TEST--
YapofactService - requestReport - Check Autofact report creation, so the data sent is Ok
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';

$app = new Yapo\YapofactService();
$data = $app->requestReport(
    array(
        "User_id" => 1,
        "User_email" => "juanito@mena.com",
        "User_name" =>  "juanito mena",
        "List_id" => 800073,
        "Ad_category" => 2020,
        "Ad_subject" => "Aviso super bacanoso",
        "Pay_type" => "tupoto",
        "Plates" => "ABCD10"
    )
);

print_r($data);
?>
--EXPECT--
Array
(
    [0] => stdClass Object
        (
            [User_id] => 1
            [User_email] => juanito@mena.com
            [User_name] => juanito mena
            [List_id] => 800073
            [Ad_category] => 2020
            [Ad_subject] => Aviso super bacanoso
            [Pay_type] => tupoto
            [Plates] => ABCD10
        )

    [1] => post
    [2] => /api/v1/request
)
