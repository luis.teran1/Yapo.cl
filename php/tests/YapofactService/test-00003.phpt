--TEST--
YapofactService - adminReport - Fail on Start date/End date
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';

$app = new Yapo\YapofactService();
$data = $app->adminReport(
    "2020-33-02",
    "2017-01-01",
    array(
        "Search_email" => "juanito@mena.com",
        "Search_name" =>  "juanito mena",
        "Search_id" => 800073,
        "Search_category" => 2020,
    )
);

echo "Data is null? ".($data == null);

$data = $app->adminReport(
    "2020-03-02",
    "2017-21-41",
    array(
        "Search_email" => "juanito@mena.com",
        "Search_name" =>  "juanito mena",
        "Search_id" => 800073,
        "Search_category" => 2020,
    )
);

echo "\nData is null? ".($data == null);
?>
--EXPECT--
Data is null? 1
Data is null? 1
