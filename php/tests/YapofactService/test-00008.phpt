--TEST--
YapofactService - healthcheck - Very call to healthcheck
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';

$app = new Yapo\YapofactService();
$data = $app->healthCheck();

print_r($data);
?>
--EXPECT--
Array
(
    [0] => Array
        (
        )

    [1] => get
    [2] => /api/v1/healthcheck
)
