--TEST--
YapofactService - adminResend - Success send the params correctly
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';
include_once 'JSON.php';

$app = new Yapo\YapofactService();
$data = $app->adminResend(123123,"perico@palotes.com");

print_r($data);
?>
--EXPECT--
Array
(
    [0] => stdClass Object
        (
            [User_email] => perico@palotes.com
        )

    [1] => post
    [2] => /api/v1/admin/resend/123123
)
