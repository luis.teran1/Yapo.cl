--TEST--
YapofactService - adminReport - Error response code
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';
include_once 'JSON.php';

global $codeResponse;
$codeResponse = 410;

$app = new Yapo\YapofactService();
$data = $app->adminReport(
    "2020-03-02",
    "2017-01-01",
    array(
        "Search_email" => "juanito@mena.com",
        "Search_name" =>  "juanito mena",
        "Search_id" => 800073,
        "Search_category" => 2020,
    )
);

echo "Data is null? ".($data == null);
?>
--EXPECT--
Data is null? 1
