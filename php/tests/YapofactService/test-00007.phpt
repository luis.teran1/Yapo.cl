--TEST--
YapofactService - adminResend - Error on response code
--FILE--
<?php
include_once 'autoload_lib.php';
include_once 'init.php';
include_once 'common/Response.php';
include_once 'common/ProxyClient.php';
include_once 'JSON.php';

global $codeResponse;
$codeResponse = 410;

$app = new Yapo\YapofactService();
$data = $app->adminResend(123123,"perico@palotes.com");

echo "Data is null? ".($data == null);
?>
--EXPECT--
Data is null? 1
