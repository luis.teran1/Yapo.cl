--TEST--
Test Cart - getProductAdParams - check params for Label
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

$data = (object)array(
    'category' => '2020',
    'product_id' => 9,
    'label_type' => 4
);

$params = $app->getProductAdParams($data);
print_r($params);

?>
--EXPECT--
Array
(
    [p] => 2200
    [lt] => 4
)
