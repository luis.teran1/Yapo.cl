<?php

class RedisMultiproductManagerExt extends RedisMultiproductManager
{

    public function get($accountId, $productId)
    {
        global $redis_response;
        return $redis_response;
    }

    public function select($accountId, $productId, $listId, $values, $with_ads = false)
    {
        global $redis_response;
        return $redis_response;
    }

    public function unselect($accountId, $productId, $listId, $group = false)
    {
        global $redis_response;
        return $redis_response;
    }

    public function list_all($accountId)
    {
        global $redis_response;
        return $redis_response;
    }
}
