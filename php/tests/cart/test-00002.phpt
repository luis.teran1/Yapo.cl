--TEST--
Test Cart - getError - check service error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('service');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => service
    [error_message] => SERVICE_NO_FOUND
)
