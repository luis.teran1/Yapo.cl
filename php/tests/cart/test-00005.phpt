--TEST--
Test Cart - getError - check product error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('product');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => product
    [error_message] => PRODUCT_MISSING
)
