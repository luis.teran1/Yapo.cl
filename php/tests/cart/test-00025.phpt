--TEST--
Test Cart - getProduct - get a product from redis good case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once('CartAppExt.php');
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

global $redis_response;
$redis_response = array(
        "status"  => "OK"
    );

$url_data = array(
        "product_id" => "9"
    );

$result = $app->getProduct($url_data, null);
print_r($result);

?>
--EXPECT--
Array
(
    [status] => OK
    [data] => Array
        (
            [status] => OK
        )

)
