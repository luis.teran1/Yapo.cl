--TEST--
Test Bconf - getProductConf - check array from bconf bad case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

global $BCONF;
$bconfCredits = Bconf::getProductConf($BCONF, 10000000000000);
var_dump($bconfCredits);

?>
--EXPECT--
bool(false)
