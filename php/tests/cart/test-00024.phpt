--TEST--
Test Cart - listProduct - get list of products from redis
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

global $redis_response;
$redis_response = array(
        "ads" => array(
            "10" => "1000"
        )
    );

$result = $app->listProduct(null, null);
print_r($result);

?>
--EXPECT--
Array
(
    [10] => 1000
    [status] => OK
)
