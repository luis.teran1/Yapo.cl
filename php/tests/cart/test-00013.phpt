--TEST--
Test Cart - validateParams - check params validations label_type missing case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

$data = (object)array(
    'category' => '2020',
    'product_id' => 9,
    'list_id' => 4888
);

$params = $app->validateParams($data);
var_dump($params);

?>
--EXPECT--
array(2) {
  [0]=>
  bool(false)
  [1]=>
  string(2) "lt"
}
