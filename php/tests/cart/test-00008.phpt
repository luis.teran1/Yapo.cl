--TEST--
Test Bconf - getProductConf - check array from bconf good case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

global $BCONF;
$bconfCredits = Bconf::getProductConf($BCONF, 50);
print_r($bconfCredits);

?>
--EXPECT--
stdClass Object
(
    [cart_needs] => credits
    [codename] => credits
    [credits] => Array
        (
            [pack_price] => Array
                (
                    [1] => Array
                        (
                            [name] => 3000
                        )

                    [2] => Array
                        (
                            [name] => 10000
                            [recommended] => 1
                        )

                    [3] => Array
                        (
                            [name] => 25000
                        )

                    [4] => Array
                        (
                            [name] => 150000
                        )

                )

        )

    [description] => Personalizados
    [exchange_rate] => 1
    [expire_time] => 1 year
    [group] => CREDITS
    [max_quantity] => 1000000
    [min_quantity] => 3000
    [name] => Compra de Yapesos
    [skip_apply_process] => 0
    [type] => Yapesos
)
