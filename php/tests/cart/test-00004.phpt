--TEST--
Test Cart - getError - check range error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('range');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => range
    [error_message] => CREDITS_OUT_OF_RANGE
)
