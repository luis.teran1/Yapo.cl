--TEST--
Test Cart - putProduct - check product stored on redis list_id missing case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

global $redis_response;
$redis_response = array(
            "prod" => '9, 10',
            "count" => '2',
            "total" => '2000'
        );

$url_data = array(
    'product_id' => '9',
    'tac' => '3',
    'lt' => '3'
);

$result = $app->putProduct($url_data, null);
print_r($result);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => params
    [error_message] => PARAMETERS_MISSING
    [missing_param] => list_id
)
