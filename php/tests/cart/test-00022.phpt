--TEST--
Test Cart - dropProduct - check product removed from redis good case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

global $redis_response;
$redis_response = array(
            "prod" => '9',
            "count" => '1',
            "total" => '1000'
        );

$url_data = array(
    'product_id' => '9',
);

$result = $app->dropProduct($url_data, null);
print_r($result);

?>
--EXPECT--
Array
(
    [prod] => 9
    [count] => 1
    [total] => 1000
)
