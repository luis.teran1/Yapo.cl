--TEST--
Test Cart - validateParams - check params validations null case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

$data = null;

$params = $app->validateParams($data);
var_dump($params);

?>
--EXPECT--
array(2) {
  [0]=>
  bool(false)
  [1]=>
  NULL
}
