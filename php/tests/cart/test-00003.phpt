--TEST--
Test Cart - getError - check params error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('params');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => params
    [error_message] => PARAMETERS_MISSING
)
