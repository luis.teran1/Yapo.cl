--TEST--
Test Cart - getError - check price error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('price');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => price
    [error_message] => PRICE_NOT_FOUND
)
