--TEST--
Test Cart - getError - check undefined error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CommonCart();
$error = $app->getError('lalalalala');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => lalalalala
    [error_message] => NOT_DEFINED
)
