--TEST--
Test Cart - getError - check account error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CartApp();
$error = $app->getError('account');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => account
    [error_message] => ACCOUNT_NOT_FOUND
)
