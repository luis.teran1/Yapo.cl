--TEST--
Test Cart - getError - check login error message
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';

$app = new CommonCart();
$error = $app->getError('login');
print_r($error);

?>
--EXPECT--
Array
(
    [status] => ERROR
    [error_code] => login
    [error_message] => NOT_LOGGED_IN
)
