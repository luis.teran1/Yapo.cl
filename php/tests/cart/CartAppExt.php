<?php

namespace Yapo;

use \RedisMultiproductManagerExt;

class CartAppExt extends CartApp
{
    public function __construct()
    {
        $this->active = true;
        $this->account_id = 5;
        $this->rmm = new RedisMultiproductManagerExt();
    }
    protected function getAutoBumpPrice($list_id, $freq, $num_days, $use_night)
    {
        global $trans_response;
        return $trans_response;
    }
}
