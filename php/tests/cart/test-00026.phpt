--TEST--
Test Cart - getProduct - get a product from redis product_id missing case
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

$app = new CartAppExt();

global $redis_response;
$redis_response = array(
        "status"  => "OK"
    );

$result = $app->getProduct(array(), null);
print_r($result);

?>
--EXPECT--
Array
(
    [status] => OK
)
