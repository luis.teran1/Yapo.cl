--TEST--
Test Cart - getProductAdParams - check params for Autobump
--FILE--
<?php

namespace Yapo;

require_once 'autoload_lib.php';
include_once 'CartAppExt.php';
include_once 'RedisMultiproductManagerExt.php';

global $trans_response;
$trans_response = 2000;

$app = new CartAppExt();

$data = (object)array(
    'category' => '2020',
    'product_id' => 10,
    'list_id' => '123123123',
    'ab_freq' => '1',
    'ab_num_days' => '1',
    'ab_use_night' => '1'
);

$params = $app->getProductAdParams($data);
print_r($params);

?>
--EXPECT--
Array
(
    [p] => 2000
    [abd] => 1
    [abf] => 1
    [abn] => 1
)
