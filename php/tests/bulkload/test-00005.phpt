--TEST--
Test bulk load - BulkLoadRule->addRule() - Overwrite index
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
$data1 = array(
	'fids' => 1,
	'targets' => 'foo',
	'fun' => NULL,
	'fun_arg' => NULL,
	'regex' => NULL,
	'comment' => 'first simple validator'
);
$validator1 = new BL\Simple($data1);
$blr->addRule('param1', $validator1);
var_dump($blr->rules);

$data2 = array(
	'fids' => 2,
	'targets' => 'foo',
	'fun' => NULL,
	'fun_arg' => NULL,
	'regex' => NULL,
	'comment' => 'second simple validator'
);
$validator2 = new BL\Simple($data2);
$blr->addRule('param1', $validator2);
var_dump($blr->rules);
?>
--EXPECT--
array(1) {
  [0]=>
  object(Yapo\BulkLoad\Simple)#3 (7) {
    ["attrs":protected]=>
    array(6) {
      [0]=>
      string(4) "fids"
      [1]=>
      string(7) "targets"
      [2]=>
      string(3) "fun"
      [3]=>
      string(7) "fun_arg"
      [4]=>
      string(5) "regex"
      [5]=>
      string(7) "comment"
    }
    ["fids"]=>
    int(1)
    ["targets"]=>
    string(3) "foo"
    ["fun"]=>
    NULL
    ["fun_arg"]=>
    NULL
    ["regex"]=>
    NULL
    ["comment"]=>
    string(22) "first simple validator"
  }
}
array(2) {
  [0]=>
  object(Yapo\BulkLoad\Simple)#3 (7) {
    ["attrs":protected]=>
    array(6) {
      [0]=>
      string(4) "fids"
      [1]=>
      string(7) "targets"
      [2]=>
      string(3) "fun"
      [3]=>
      string(7) "fun_arg"
      [4]=>
      string(5) "regex"
      [5]=>
      string(7) "comment"
    }
    ["fids"]=>
    int(1)
    ["targets"]=>
    string(3) "foo"
    ["fun"]=>
    NULL
    ["fun_arg"]=>
    NULL
    ["regex"]=>
    NULL
    ["comment"]=>
    string(22) "first simple validator"
  }
  [1]=>
  object(Yapo\BulkLoad\Simple)#4 (7) {
    ["attrs":protected]=>
    array(6) {
      [0]=>
      string(4) "fids"
      [1]=>
      string(7) "targets"
      [2]=>
      string(3) "fun"
      [3]=>
      string(7) "fun_arg"
      [4]=>
      string(5) "regex"
      [5]=>
      string(7) "comment"
    }
    ["fids"]=>
    int(2)
    ["targets"]=>
    string(3) "foo"
    ["fun"]=>
    NULL
    ["fun_arg"]=>
    NULL
    ["regex"]=>
    NULL
    ["comment"]=>
    string(23) "second simple validator"
  }
}
