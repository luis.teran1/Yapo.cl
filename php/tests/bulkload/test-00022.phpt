--TEST--
Test bulk load - BlocketApiService->createPartner - test exception when redis fails
--FILE--
<?php

use \Yapo;

require_once('common/RedisMock.php');
require_once('common/bRedisClassMock.php');
require_once('common/includes.php');

$bas = new \Yapo\BlocketApiService();
$data = array(
    'apiKey' => 'INVALID_PARTNER',
    'apiHash' => 'thisisthepartnershash'
);
try {
	$bas->createPartner($data);
} catch (\Exception $e) {
	echo $e->getMessage();
}

?>
--EXPECT--
BlocketRedis Error: when try to create partner
