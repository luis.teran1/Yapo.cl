--TEST--
Test bulk load - OneToMany->applyFunction() - test method with non valid argument
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => 1,
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('not_a_valid_function_name');
var_dump($v->fun_arg);
?>
--EXPECT--
int(1)
