--TEST--
Test bulk load - OneToMany - constructor test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => null,
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
var_dump($v);
?>
--EXPECT--
object(Yapo\BulkLoad\OneToMany)#2 (7) {
  ["attrs":protected]=>
  array(6) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(5) "regex"
    [5]=>
    string(7) "comment"
  }
  ["fids"]=>
  int(4)
  ["targets"]=>
  array(2) {
    [0]=>
    string(8) "communes"
    [1]=>
    string(6) "region"
  }
  ["fun"]=>
  string(17) "translate_to_many"
  ["fun_arg"]=>
  NULL
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(21) "one to many validator"
}
