--TEST--
Test bulk load - BulkLoadRule->getJson() - with results
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
$validator = array();
$validator[] = new BL\Simple(
    array(
	'fids' => 1,
	'targets' => 'foo',
	'fun' => NULL,
	'fun_arg' => NULL,
	'regex' => NULL,
	'comment' => 'first simple validator'
    )
);
$validator[] = new BL\Multi(
    array(
	'fids' => array(2,3),
	'targets' => 'foo2',
	'fun' => 'add_until',
	'fun_arg' => NULL,
        'map_fun' => 'forward',
        'fun_init' => 4,
	'regex' => '^.*$',
	'comment' => 'multi validator'
    )
);
$validator[] = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array(
            array(
                'regex' => '^cisnes$',
                'commune' => 123,
                'region' => 11
            ),
            array(
                'regex' => '^temuco$',
                'commune' => 233,
                'region' => 10
            )
        ),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
foreach ($validator as $val) {
    $blr->addRule('param', $val);
}
var_dump($blr->getJson());
?>
--EXPECT--
string(557) "{"rules":[{"class":"Simple","args":{"fids":1,"targets":"foo","fun":null,"fun_arg":null,"regex":null,"comment":"first simple validator"}},{"class":"Multi","args":{"fids":[2,3],"targets":"foo2","fun":"add_until","fun_arg":null,"fun_init":4,"map_fun":"forward","regex":"^.*$","comment":"multi validator"}},{"class":"OneToMany","args":{"fids":4,"targets":["communes","region"],"fun":"translate_to_many","fun_arg":[{"regex":"^cisnes$","commune":123,"region":11},{"regex":"^temuco$","commune":233,"region":10}],"regex":"^.*$","comment":"one to many validator"}}]}"
