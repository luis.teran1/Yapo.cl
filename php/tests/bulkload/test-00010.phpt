--TEST--
Test bulk load - Validator classes getName method
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blv = new BL\BulkLoadValidator(array());
var_dump($blv->getName());
?>
--EXPECT--
string(17) "BulkLoadValidator"
