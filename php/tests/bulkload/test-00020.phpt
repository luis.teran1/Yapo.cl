--TEST--
Test bulk load - BlocketApiService - constructor test
--FILE--
<?php

use \Yapo;

require_once('common/RedisMock.php');
require_once('common/bRedisClassMock.php');
require_once('common/includes.php');

$bas = new \Yapo\BlocketApiService();
var_dump($bas);

?>
--EXPECT--
object(Yapo\BlocketApiService)#2 (5) {
  ["redisKey":"Yapo\BlocketApiService":private]=>
  NULL
  ["redisPrefix":"Yapo\BlocketApiService":private]=>
  string(12) "mcp1xapp_id_"
  ["maps":"Yapo\BlocketApiService":private]=>
  array(2) {
    ["apiHash"]=>
    string(7) "api_key"
    ["market"]=>
    string(9) "cat_limit"
  }
  ["redis"]=>
  object(RedisMock)#3 (0) {
  }
  ["redisKeys"]=>
  array(8) {
    ["api_key"]=>
    string(0) ""
    ["expire_time"]=>
    string(3) "300"
    ["region_limit"]=>
    string(35) "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15"
    ["ad_type_limit"]=>
    string(7) "h,k,s,u"
    ["request_limit"]=>
    string(6) "100000"
    ["appl_limit"]=>
    string(27) "import,importdeletead,newad"
    ["cat_limit"]=>
    string(224) "1000,1220,1240,1260,2000,2020,2060,2040,2100,2080,2120,5000,5020,5040,5060,5160,4000,4020,4040,4060,9000,9020,9040,9060,6000,6140,6020,6060,6180,6080,6100,6120,6160,3000,3040,3060,3020,3080,7000,7020,7040,7060,7080,8000,8020"
    ["logging"]=>
    string(1) "1"
  }
}
