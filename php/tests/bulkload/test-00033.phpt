--TEST--
Test bulk load - OneToMany->createTypeDict() - test method with dictionary with invalid values
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array('vendo' => 0, 'arriendo' => 'notacategory'),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createTypeDict');
var_dump($v->fun_arg);
?>
--EXPECT--
array(2) {
  ["vendo"]=>
  int(0)
  ["arriendo"]=>
  string(12) "notacategory"
}
