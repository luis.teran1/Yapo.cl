--TEST--
Test bulk load - BulkLoadRule() - Constructor test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
var_dump($blr);
?>
--EXPECT--
object(Yapo\BulkLoad\BulkLoadRule)#2 (1) {
  ["rules"]=>
  array(0) {
  }
}
