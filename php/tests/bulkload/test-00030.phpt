--TEST--
Test bulk load - OneToMany->createTypeDict() - test method with a valid argument
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array('vendo' => 1220, 'arriendo' => 1240),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createTypeDict');
var_dump($v->fun_arg);
?>
--EXPECT--
array(2) {
  [0]=>
  array(3) {
    ["partnerCode"]=>
    string(5) "vendo"
    ["types"]=>
    string(1) "s"
    ["category"]=>
    int(1220)
  }
  [1]=>
  array(3) {
    ["partnerCode"]=>
    string(8) "arriendo"
    ["types"]=>
    string(1) "u"
    ["category"]=>
    int(1240)
  }
}
