--TEST--
Test bulk load - BulkLoadRule->addRule() - Happy path
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
$validator = new BL\BulkLoadValidator(array());
$blr->addRule('param1', $validator);
var_dump($blr->rules);
$blr->addRule('param2', $validator);
var_dump($blr->rules);
?>
--EXPECT--
array(1) {
  [0]=>
  object(Yapo\BulkLoad\BulkLoadValidator)#3 (1) {
    ["attrs":protected]=>
    NULL
  }
}
array(2) {
  [0]=>
  object(Yapo\BulkLoad\BulkLoadValidator)#3 (1) {
    ["attrs":protected]=>
    NULL
  }
  [1]=>
  object(Yapo\BulkLoad\BulkLoadValidator)#3 (1) {
    ["attrs":protected]=>
    NULL
  }
}
