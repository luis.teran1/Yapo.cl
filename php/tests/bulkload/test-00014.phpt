--TEST--
Test bulk load - Remove - constructor and getName test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\Remove(
    array(
        'to_check' => 'to_check',
        'fun' => 'fun',
        'fun_arg' => 'fun_arg',
        'to_remove' => 'to_remove'
    )
);
var_dump($v);
var_dump($v->getName());
?>
--EXPECT--
object(Yapo\BulkLoad\Remove)#2 (5) {
  ["attrs":protected]=>
  array(4) {
    [0]=>
    string(8) "to_check"
    [1]=>
    string(3) "fun"
    [2]=>
    string(7) "fun_arg"
    [3]=>
    string(9) "to_remove"
  }
  ["to_check"]=>
  string(8) "to_check"
  ["fun"]=>
  string(3) "fun"
  ["fun_arg"]=>
  string(7) "fun_arg"
  ["to_remove"]=>
  string(9) "to_remove"
}
string(6) "Remove"
