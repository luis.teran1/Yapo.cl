--TEST--
Test bulk load - ValidatorFactory->getValidator - verifies validator creation with wrong parameters
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$validatorFactory = new BL\ValidatorFactory();
$setting = array(
	'class' => 'ImNotAClass'
);
$validator = $validatorFactory->getValidator($setting);
var_dump($validator);

?>
--EXPECT--
NULL
