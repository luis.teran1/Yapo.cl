--TEST--
Test bulk load - Partner->addService() - test partner with multiple services
--FILE--
<?php
use \Yapo;
require_once('common/includes.php');

class MockService
{
	public function createPartner($data)
	{
		echo "I'm creating a partner...\n";
		return true;
	}
}

$data = array(
    'name' => 'The partner',
    'market' => 1000,
    'rules' => 'im a rule',
    'host' => 'hola.mundo.cl',
    'port' => '1313',
    'protocol' => 'ftp',
    'username' => 'tha_partner',
    'password' => '123123123',
    'path' => '/'
);
$partner = new \Yapo\Partner($data);

$partner
    ->addService(new MockService())
    ->addService(new MockService());
var_dump($partner);
?>
--EXPECTF--
object(Yapo\Partner)#2 (3) {
  ["errorMsg"]=>
  NULL
  ["services":"Yapo\Partner":private]=>
  array(2) {
    [0]=>
    object(MockService)#3 (0) {
    }
    [1]=>
    object(MockService)#4 (0) {
    }
  }
  ["data":"Yapo\Partner":private]=>
  array(11) {
    ["apiKey"]=>
    string(11) "the-partner"
    ["apiHash"]=>
    string(64) "%s"
    ["name"]=>
    string(11) "The partner"
    ["market"]=>
    int(1000)
    ["rules"]=>
    string(9) "im a rule"
    ["host"]=>
    string(13) "hola.mundo.cl"
    ["port"]=>
    string(4) "1313"
    ["protocol"]=>
    string(3) "ftp"
    ["username"]=>
    string(11) "tha_partner"
    ["password"]=>
    string(9) "123123123"
    ["path"]=>
    string(1) "/"
  }
}
