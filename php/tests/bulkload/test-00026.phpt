--TEST--
Test bulk load - OneToMany->createCommuneDict() - test method with a valid argument
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array('temuco' => 225, 'valdivia' => 243, 'vi�a' => 81),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createCommuneDict');
var_dump($v->fun_arg);
?>
--EXPECT--
array(3) {
  [0]=>
  array(3) {
    ["partnerCode"]=>
    string(6) "temuco"
    ["communes"]=>
    int(225)
    ["region"]=>
    int(10)
  }
  [1]=>
  array(3) {
    ["partnerCode"]=>
    string(8) "valdivia"
    ["communes"]=>
    int(243)
    ["region"]=>
    int(11)
  }
  [2]=>
  array(3) {
    ["partnerCode"]=>
    string(5) "viña"
    ["communes"]=>
    int(81)
    ["region"]=>
    int(6)
  }
}
