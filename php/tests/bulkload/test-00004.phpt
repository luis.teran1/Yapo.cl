--TEST--
Test bulk load - BulkLoadRule->addRule() - Unhappy path
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
$validator = 0;
$blr->addRule('param1', $validator);
?>
--EXPECTF--
Catchable fatal error: Argument 2 passed to Yapo\BulkLoad\BulkLoadRule::addRule() must be an instance of Yapo\BulkLoad\BulkLoadValidator, integer given, called in %s/php/tests/bulkload/%s on line %d and defined in %s/include/BulkLoad/BulkLoadRule.php on line %d
