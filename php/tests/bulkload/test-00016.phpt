--TEST--
Test bulk load - Partner - test constructor
--FILE--
<?php
use \Yapo;
require_once('common/includes.php');

$data = array(
    'name' => 'The partner',
    'market' => 1000,
    'rules' => 'im a rule',
    'host' => 'hola.mundo.cl',
    'port' => '1313',
    'protocol' => 'ftp',
    'username' => 'tha_partner',
    'password' => '123123123',
    'path' => '/'
);
$partner = new \Yapo\Partner($data);
var_dump($partner);
?>
--EXPECTF--
object(Yapo\Partner)#2 (3) {
  ["errorMsg"]=>
  NULL
  ["services":"Yapo\Partner":private]=>
  array(0) {
  }
  ["data":"Yapo\Partner":private]=>
  array(11) {
    ["apiKey"]=>
    string(11) "the-partner"
    ["apiHash"]=>
    string(64) "%s"
    ["name"]=>
    string(11) "The partner"
    ["market"]=>
    int(1000)
    ["rules"]=>
    string(9) "im a rule"
    ["host"]=>
    string(13) "hola.mundo.cl"
    ["port"]=>
    string(4) "1313"
    ["protocol"]=>
    string(3) "ftp"
    ["username"]=>
    string(11) "tha_partner"
    ["password"]=>
    string(9) "123123123"
    ["path"]=>
    string(1) "/"
  }
}
