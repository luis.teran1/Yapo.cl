--TEST--
Test bulk load - Conditional - constructor and getName test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$conditional = new BL\Conditional(
    array(
	'fids' => 1,
	'targets' => 'target_param',
	'fun' => NULL,
	'fun_arg' => NULL,
	'other_field' => NULL,
	'map_fun' => NULL,
	'regex' => '^.*$',
	'comment' => 'conditional validator'
    )
);
var_dump($conditional);
var_dump($conditional->getName());
?>
--EXPECT--
object(Yapo\BulkLoad\Conditional)#2 (9) {
  ["attrs":protected]=>
  array(8) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(11) "other_field"
    [5]=>
    string(7) "map_fun"
    [6]=>
    string(5) "regex"
    [7]=>
    string(7) "comment"
  }
  ["fids"]=>
  int(1)
  ["targets"]=>
  string(12) "target_param"
  ["fun"]=>
  NULL
  ["fun_arg"]=>
  NULL
  ["other_field"]=>
  NULL
  ["map_fun"]=>
  NULL
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(21) "conditional validator"
}
string(11) "Conditional"
