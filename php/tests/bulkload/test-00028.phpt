--TEST--
Test bulk load - OneToMany->createCommuneDict() - test method with an empty dictionary
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array(),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createCommuneDict');
var_dump($v->fun_arg);
?>
--EXPECT--
array(0) {
}
