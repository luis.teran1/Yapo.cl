--TEST--
Test bulk load - OneToMany->createCommuneDict() - test method with dictionary with invalid values
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => array('temuco' => 1225, 'valdivia' => 1243, 'vi�a' => 1181),
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createCommuneDict');
var_dump($v->fun_arg);
?>
--EXPECT--
array(3) {
  ["temuco"]=>
  int(1225)
  ["valdivia"]=>
  int(1243)
  ["vi�a"]=>
  int(1181)
}
