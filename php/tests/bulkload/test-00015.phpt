--TEST--
Test bulk load - Simple - constructor and getName test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\Simple(
    array(
        'fids' => array(1),
        'targets' => 'targets',
        'fun' => 'fun',
        'fun_arg' => 'fun_arg',
        'regex' => 'regex',
        'comment' => 'comment'
    )
);
var_dump($v);
var_dump($v->getName());
?>
--EXPECT--
object(Yapo\BulkLoad\Simple)#2 (7) {
  ["attrs":protected]=>
  array(6) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(5) "regex"
    [5]=>
    string(7) "comment"
  }
  ["fids"]=>
  array(1) {
    [0]=>
    int(1)
  }
  ["targets"]=>
  string(7) "targets"
  ["fun"]=>
  string(3) "fun"
  ["fun_arg"]=>
  string(7) "fun_arg"
  ["regex"]=>
  string(5) "regex"
  ["comment"]=>
  string(7) "comment"
}
string(6) "Simple"
