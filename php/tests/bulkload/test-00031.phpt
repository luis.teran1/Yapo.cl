--TEST--
Test bulk load - OneToMany->createTypeDict() - test method with invalid dictionary
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\OneToMany(
    array(
        'fids' => 4,
        'fun' => 'translate_to_many',
        'fun_arg' => 1,
        'regex' => '^.*$',
        'targets' => array('communes', 'region'),
        'comment' => 'one to many validator',
    )
);
$v->applyFunction('createTypeDict');
var_dump($v->fun_arg);
?>
--EXPECTF--
Fatal error: Uncaught exception 'Exception' with message 'ERROR_TYPE_DICT_MISSING' in %s/OneToMany.php:%d
Stack trace:
#0 %s/OneToMany.php(27): Yapo\BulkLoad\OneToMany->createTypeDict()
#1 %s/test-00031.php(15): Yapo\BulkLoad\OneToMany->applyFunction('createTypeDict')
#2 {main}
  thrown in %s/OneToMany.php on line %d
