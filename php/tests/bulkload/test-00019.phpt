--TEST--
Test bulk load - Partner->createPartner() - test partner creation
--FILE--
<?php
use \Yapo;
require_once('common/includes.php');

class MockService
{
	public function createPartner($data)
	{
		echo "I'm creating a partner...\n";
		return true;
	}
}

$data = array(
    'name' => 'The partner',
    'market' => 1000,
    'rules' => 'im a rule',
    'host' => 'hola.mundo.cl',
    'port' => '1313',
    'protocol' => 'ftp',
    'username' => 'tha_partner',
    'password' => '123123123',
    'path' => '/'
);

$partner = new \Yapo\Partner($data);
$partner->addService(new MockService());
$partner->addService(new MockService());
var_dump($partner->create());
?>
--EXPECT--
I'm creating a partner...
I'm creating a partner...
bool(true)
