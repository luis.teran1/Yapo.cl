--TEST--
Test bulk load - Multi - constructor and getName test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$v = new BL\Multi(
    array(
        'fids' => array(1,2),
        'targets' => 'target',
        'fun' => 'fun',
        'fun_arg' => 'fun_arg',
        'fun_init' => 'fun_init',
        'map_fun' => 'map_fun',
        'regex' => 'regex',
        'comment' => 'comment'
    )
);
var_dump($v);
var_dump($v->getName());
?>
--EXPECT--
object(Yapo\BulkLoad\Multi)#2 (9) {
  ["attrs":protected]=>
  array(8) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(8) "fun_init"
    [5]=>
    string(7) "map_fun"
    [6]=>
    string(5) "regex"
    [7]=>
    string(7) "comment"
  }
  ["fids"]=>
  array(2) {
    [0]=>
    int(1)
    [1]=>
    int(2)
  }
  ["targets"]=>
  string(6) "target"
  ["fun"]=>
  string(3) "fun"
  ["fun_arg"]=>
  string(7) "fun_arg"
  ["fun_init"]=>
  string(8) "fun_init"
  ["map_fun"]=>
  string(7) "map_fun"
  ["regex"]=>
  string(5) "regex"
  ["comment"]=>
  string(7) "comment"
}
string(5) "Multi"
