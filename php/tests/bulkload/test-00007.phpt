--TEST--
Test bulk load - BulkLoadRule->getJson() - with results
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
$validator1 = new BL\Simple(
    array(
	'fids' => 1,
	'targets' => 'foo',
	'fun' => NULL,
	'fun_arg' => NULL,
	'regex' => NULL,
	'comment' => 'first simple validator'
    )
);
$validator2 = new BL\Simple(
    array(
	'fids' => 2,
	'targets' => 'foo',
	'fun' => NULL,
	'fun_arg' => NULL,
	'regex' => NULL,
	'comment' => 'second simple validator'
    )
);
$blr->addRule('param1', $validator1);
$blr->addRule('param2', $validator2);
var_dump($blr->getJson());
?>
--EXPECT--
string(266) "{"rules":[{"class":"Simple","args":{"fids":1,"targets":"foo","fun":null,"fun_arg":null,"regex":null,"comment":"first simple validator"}},{"class":"Simple","args":{"fids":2,"targets":"foo","fun":null,"fun_arg":null,"regex":null,"comment":"second simple validator"}}]}"
