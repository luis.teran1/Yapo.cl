--TEST--
Test bulk load - ValidatorFactory->getValidator - verifies validator creation according to bconf
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

function getSetting($param = '')
{
    $bulk_settings = array('param' => $param, 'partner' => 'default');
    get_settings(
        bconf_get($BCONF, "*.bulkload_validator"),
        "param",
        create_function('$s,$k,$d', 'return $d[$k];'),
        create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
        $bulk_settings
    );
    return $bulk_settings;
}

$params = array(
    'ext_code',
    'estate_type',
    'subject',
    'bathrooms',
    'currency'
);

$validatorFactory = new BL\ValidatorFactory();

foreach ($params as $key => $param) {
    $setting = getSetting($param);
    $setting['ind'] = array($key);
    $validator = $validatorFactory->getValidator($setting);
    var_dump($validator);
}

?>
--EXPECT--
object(Yapo\BulkLoad\Simple)#3 (7) {
  ["attrs":protected]=>
  array(6) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(5) "regex"
    [5]=>
    string(7) "comment"
  }
  ["fids"]=>
  int(0)
  ["targets"]=>
  string(8) "ext_code"
  ["fun"]=>
  string(7) "forward"
  ["fun_arg"]=>
  string(0) ""
  ["regex"]=>
  string(13) "^[A-Za-z0-9]{"
  ["comment"]=>
  string(0) ""
}
object(Yapo\BulkLoad\Simple)#4 (7) {
  ["attrs":protected]=>
  array(6) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(5) "regex"
    [5]=>
    string(7) "comment"
  }
  ["fids"]=>
  int(1)
  ["targets"]=>
  string(11) "estate_type"
  ["fun"]=>
  string(9) "from_dict"
  ["fun_arg"]=>
  string(0) ""
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(0) ""
}
object(Yapo\BulkLoad\Multi)#3 (9) {
  ["attrs":protected]=>
  array(8) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(8) "fun_init"
    [5]=>
    string(7) "map_fun"
    [6]=>
    string(5) "regex"
    [7]=>
    string(7) "comment"
  }
  ["fids"]=>
  array(1) {
    [0]=>
    int(2)
  }
  ["targets"]=>
  string(7) "subject"
  ["fun"]=>
  string(6) "concat"
  ["fun_arg"]=>
  string(0) ""
  ["fun_init"]=>
  string(0) ""
  ["map_fun"]=>
  string(7) "forward"
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(0) ""
}
object(Yapo\BulkLoad\Multi)#4 (9) {
  ["attrs":protected]=>
  array(8) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(8) "fun_init"
    [5]=>
    string(7) "map_fun"
    [6]=>
    string(5) "regex"
    [7]=>
    string(7) "comment"
  }
  ["fids"]=>
  array(1) {
    [0]=>
    int(3)
  }
  ["targets"]=>
  string(9) "bathrooms"
  ["fun"]=>
  string(9) "add_until"
  ["fun_arg"]=>
  string(0) ""
  ["fun_init"]=>
  string(1) "4"
  ["map_fun"]=>
  string(7) "forward"
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(0) ""
}
object(Yapo\BulkLoad\Simple)#3 (7) {
  ["attrs":protected]=>
  array(6) {
    [0]=>
    string(4) "fids"
    [1]=>
    string(7) "targets"
    [2]=>
    string(3) "fun"
    [3]=>
    string(7) "fun_arg"
    [4]=>
    string(5) "regex"
    [5]=>
    string(7) "comment"
  }
  ["fids"]=>
  int(4)
  ["targets"]=>
  string(8) "currency"
  ["fun"]=>
  string(9) "from_dict"
  ["fun_arg"]=>
  string(0) ""
  ["regex"]=>
  string(4) "^.*$"
  ["comment"]=>
  string(0) ""
}
