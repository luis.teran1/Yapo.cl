--TEST--
Test bulk load - Constrain - constructor and getName test
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$constrain = new BL\Constrain(
    array(
	'to_check' => 1,
	'fun' => 'fun',
	'fun_arg' =>  'fun_arg',
	'qto_check' => NULL,
	'qfun' => NULL,
	'qfun_arg' => NULL,
	'message' => 'constrain validator'
    )
);
var_dump($constrain);
var_dump($constrain->getName());
?>
--EXPECT--
object(Yapo\BulkLoad\Constrain)#2 (8) {
  ["attrs":protected]=>
  array(7) {
    [0]=>
    string(8) "to_check"
    [1]=>
    string(3) "fun"
    [2]=>
    string(7) "fun_arg"
    [3]=>
    string(9) "qto_check"
    [4]=>
    string(4) "qfun"
    [5]=>
    string(8) "qfun_arg"
    [6]=>
    string(7) "message"
  }
  ["to_check"]=>
  int(1)
  ["fun"]=>
  string(3) "fun"
  ["fun_arg"]=>
  string(7) "fun_arg"
  ["qto_check"]=>
  NULL
  ["qfun"]=>
  NULL
  ["qfun_arg"]=>
  NULL
  ["message"]=>
  string(19) "constrain validator"
}
string(9) "Constrain"
