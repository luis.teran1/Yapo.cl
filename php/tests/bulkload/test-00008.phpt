--TEST--
Test bulk load - BulkLoadValidator - class constructor
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blv = new BL\BulkLoadValidator(array());
var_dump($blv);
?>
--EXPECT--
object(Yapo\BulkLoad\BulkLoadValidator)#2 (1) {
  ["attrs":protected]=>
  NULL
}
