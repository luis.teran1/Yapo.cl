--TEST--
Test bulk load - BulkLoadRule()->getJson() - Empty result
--FILE--
<?php
use Yapo\BulkLoad as BL;
require_once('common/includes.php');

$blr = new BL\BulkLoadRule();
var_dump($blr->getJson());
?>
--EXPECT--
string(12) "{"rules":[]}"
