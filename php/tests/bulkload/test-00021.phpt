--TEST--
Test bulk load - BlocketApiService->createPartner() - happy path
--FILE--
<?php

use \Yapo;

require_once('common/RedisMock.php');
require_once('common/bRedisClassMock.php');
require_once('common/includes.php');

$bas = new \Yapo\BlocketApiService();
$data = array(
    'apiKey' => 'partner-name',
    'apiHash' => 'thisisthepartnershash'
);
var_dump($bas->createPartner($data));

?>
--EXPECT--
bool(true)
