--TEST--
Proxy test - userId value 0
--FILE--
<?php
shell_exec("make mock-stage-" . basename($_SERVER["SCRIPT_FILENAME"], '.php'));

require_once 'autoload_lib.php';
require_once 'httpful.php';

$proxy = new ProxyClient();
$body = array(
	'UserId' => 0,
	'Credits' => 2000,
	'ExternalId' => 1,
	'Ttl' => 123123123
);
$host = bconf_get($BCONF, "*.credits.host" );
$port = bconf_get($BCONF, "*.credits.port" );
$path = bconf_get($BCONF, "*.credits.path" );
$response = $proxy->doRequest('post', $host, $port, $path, $body);

echo "$response->code\n";
echo $response;
?>
--EXPECT--
415
{"ErrorMessage":"INVALID_PARAM_VALUE {map[UserID:[less than min]]}"}
