--TEST--
Proxy test - Request with Params with nonvalid name
--FILE--
<?php
shell_exec("make mock-stage-" . basename($_SERVER["SCRIPT_FILENAME"], '.php'));

require_once 'autoload_lib.php';
require_once 'httpful.php';

$proxy = new ProxyClient();
$body = array(
	'UserId' => 100,
	'Credits' => 5000,
	'ExternalId' => -1,
	'Params' => array(
		array(
			'key' => 'adminId',
			'value' => 1
		),
		array(
			'key' => 'subject',
			'value' => '1'
		),
		array(
			'key' => 'is_free',
			'value' => true
		),
	)
);
$host = bconf_get($BCONF, "*.credits.host" );
$port = bconf_get($BCONF, "*.credits.port" );
$path = bconf_get($BCONF, "*.credits.path" );
$response = $proxy->doRequest('post', $host, $port, $path, $body);

echo "$response->code\n";
echo $response;
?>
--EXPECT--
414
{"ErrorMessage":"MISSING_PARAMETER {adminId}"}
