--TEST--
Proxy test - Test get balance success
--FILE--
<?php
shell_exec("make mock-stage-" . basename($_SERVER["SCRIPT_FILENAME"], '.php'));

require_once 'autoload_lib.php';
require_once 'httpful.php';

$proxy = new ProxyClient();
$host = bconf_get($BCONF, "*.credits.host" );
$port = bconf_get($BCONF, "*.credits.port" );
$path = bconf_get($BCONF, "*.credits.path" ) . "/100";
$response = $proxy->doRequest('get', $host, $port, $path);

echo "$response->code\n";
echo $response;
?>
--EXPECT--
200
{"Status":"OK","Balance":0,"UserId":0,"CreditToExpire":{"Credits":0,"ExpirationDate":"0001-01-01T00:00:00Z"}}
