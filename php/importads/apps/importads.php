<?php

/*
 * Importads application
 */
require_once('blocket_application.php');
require_once('dyn_config.php');
require_once('bImage.php');
require_once('bSession.php');
require_once('bResponse.php');
require_once('bAd.php');

$script_list[] = "/js/common.js";

/*
 *  Syslog
 */
openlog("importads", LOG_ODELAY, LOG_LOCAL0);

class blocket_importads extends blocket_application {
	var $xml;
	var $xml_current_tag;
	var $update = array();
	var $update_buffer = array();
	var $error = false;
	var $updateid;
	var $partner;
	var $access_partner;

        function blocket_importads() {
                global $BCONF;
                global $session;

		if (bconf_get($BCONF, "*.common.importads_activated") != "1"){
			exit;
		}

		syslog(LOG_INFO, log_string()."Initializing connection from " . $_SERVER['REMOTE_ADDR'] .
		       " (" . (isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : "/")   . ")");

		if (!bconf_get($BCONF, "*.common.importads.allowed_ip_override")) {
			$allowed_ips = bconf_get($BCONF, "*.common.importads.allowed_ip");
			$authorized = 0;
			foreach($allowed_ips as $partner => $ips) {
				if (in_array($_SERVER['REMOTE_ADDR'], $ips)) {
					$authorized = 1;
					$this->access_partner[] = $partner;
				}
			}
			
			if (!$authorized) {
				syslog(LOG_CRIT, log_string()."Unauthorized access attempt from " . $_SERVER['REMOTE_ADDR']);
				exit;
			}
		}

		
                $this->application_name = "importads";
		$this->init('alert',0);
        }

	function get_state($statename) {
                switch($statename) {
                case 'alert':
                        return array('function' => 'importads_alert',
				     'params' => array('xml' => 'f_clean'),
                                     'method' => 'post');
                }
        }


	function importads_alert($xml) {
		global $BCONF;

		if(empty($xml)){
			syslog(LOG_INFO, log_string()."Got empty xml from " . $_SERVER['REMOTE_ADDR']);
			return 'FINISH';
		}

		/* XXX Put all parse xml somewere else - e.g. separate class (code is now duplicated)?? */
		$xml = preg_replace("/>"."[[:space:]]+"."</i","><", $xml);
	        $this->parse_xml($xml);

		$this->validate_buffer();

		/* Check that partner comes from correct IP */
		if (!bconf_get($BCONF, "*.common.importads.allowed_ip_override")) {
			if(!in_array($this->update['PARTNER'], $this->access_partner)) {
				syslog(LOG_CRIT, log_string()."Partner IP mismatch {$this->update['PARTNER']} comes from " .$_SERVER['REMOTE_ADDR'] );
				header("Content-type: text/xml");
				echo "<response><statuscode>400</statuscode><statustext>Nack (access denied)</statustext><updateid>{$this->update['UPDATEID']}</updateid></response>";
				exit(1);
			}
		}

		/* Decide what partner to use (see $this->update['PARTNER']) */
		$module_id = $this->update['PARTNER'];
		if (($m_id = bconf_get($BCONF, "importads.$module_id.module")))
			$module_id = $m_id;
		$module_file = "importads_module_${module_id}.php";
		if (file_exists('../include/'.$module_file) && require_once($module_file)) {
			$class = "importads_module_${module_id}";
			$partner_app = new $class($this->update, $this);
			$partner_app->decide_state();
		} else {
			syslog(LOG_CRIT, log_string()."No partner module file {$module_file} found");
			exit(1);
		}

		return 'FINISH';
	}

        function f_clean($val) {
		return $val;
	}

	function element_content($parser, $data) {
		if ($this->xml_current_tag == "IMAGE") {
			if (!isset($this->update['IMAGE']))
				$this->update['IMAGE'] = array();
			array_push($this->update['IMAGE'], $data);
		} else {
			if (isset($this->update[$this->xml_current_tag]))
				$this->update[$this->xml_current_tag] .= $data;
			else
				$this->update[$this->xml_current_tag] = $data;
		}

	}

	function start_element($parser, $name, $attrs) {
		$this->xml_current_tag = $name;
	}

	function end_element($parser, $name) {
	}

	function validate_buffer() {
		/* If an xml-tag is empty or doesn't exist it wont be present (i.e. empty) */
		/* Therefore we only test required fields and isset */
		if ( !isset($this->update['PARTNER']) ||
		     !isset($this->update['ADID']) ||
		     !isset($this->update['SIGNAL']) ) {
				syslog(LOG_ERR, log_string()."(ERR) UPDATE_REQUIRED_TAG_MISSING partner = {$this->update['PARTNER']}, adid = {$this->update['ADID']}, signal = {$this->update['SIGNAL']}");
				$this->importads_err("UPDATE_REQUIRED_TAG_MISSING");
			}
	}

	function importads_err($err = "", $statuscode = 400) {
		header("Content-type: text/xml");
		if(empty($err)) {
			echo "<response><statuscode>$statuscode</statuscode><statustext>Nack</statustext><updateid>{$this->updateid}</updateid></response>";
			syslog(LOG_ERR, log_string()."(ERR) Nack $statuscode (no msg)");
		} else {
			if (is_array($err))
				$err = implode(',', $err);
			echo "<response><statuscode>$statuscode</statuscode><statustext>Nack ($err)</statustext><updateid>{$this->updateid}</updateid></response>";
			syslog(LOG_ERR, log_string()."(ERR) Nack $statuscode ($err)");
		}

		exit(1);
	}

	function parse_xml($xml) {
		$xmlparser = xml_parser_create();

		if (!$xml || !$xmlparser)
			return false;

		xml_set_object($xmlparser, $this); 
		xml_set_element_handler($xmlparser, "start_element", "end_element");
		xml_set_character_data_handler($xmlparser, "element_content");
		xml_parser_set_option($xmlparser, XML_OPTION_SKIP_WHITE, 1);

		if (!xml_parse($xmlparser, $xml, true))
			return false;

		if ($this->error)
			return false;

		return true;
	}
}

if (!isset($included)) {
	$importads = new blocket_importads();
	$importads->run_fsm();
}

?>
