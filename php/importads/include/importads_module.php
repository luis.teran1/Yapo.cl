<?php
require_once("util.php");

/*
 * Importads module - baseclass
 */

class importads_module  {
	var $xml;
	var $xml_current_tag;
	var $xml_tag_stack = array();
	var $use_xml_stack = false;
	var $update = array();
	var $updateid;
	var $partner;
	var $error = false;
	var $array_tags;
	var $ignoredigest = false;
	var $importads_conf = array();

	// This is a reference to the importads object which included us. Gives us access to blocket_application members from modules.
	var $importads_handle;

        function importads_module($update, &$importads_handle) {
                global $BCONF;

		$this->importads_conf = bconf_get($BCONF,"*.common.importads.conf"); 
		if (empty($this->importads_conf['curl_timeout'])) {
			$this->importads_conf['curl_timeout'] = 20;
		}
		$this->update = $update;
		$this->updateid = $this->update['UPDATEID'];
		$this->partner = $this->update['PARTNER'];

		if (isset($this->update['PARAMS'])) {
			if (strpos($this->update['PARAMS'], 'ignoredigest') !== false) {
				$this->ignoredigest = true;
			}
		}
		$this->importads_handle = $importads_handle;
        }

	function delete_ad($partner, $external_ad_id, $send_mail = false, $noresp = false) {
		global $BCONF; 

		$transaction = new bTransaction(); 

		$transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);
		$transaction->add_data('remote_browser', $_SERVER['HTTP_USER_AGENT']);
		$transaction->add_data('link_type', $partner);
		$transaction->add_data('external_ad_id', $external_ad_id);

		/* XXX Better name?? Now used by bostart only */
		if (!$send_mail) {
			$transaction->add_data('do_not_send_mail', 1);
		}
		$reply = $transaction->send_command('deletead');
		if ($noresp)
			return;

		if ($transaction->has_error()) {
			syslog(LOG_ERR, log_string()."Delete transaction failed. Partner: $partner External ad_id: $external_ad_id" );
			if ($transaction->get_error("error") == "ERROR_AD_ALREADY_DELETED")
				$this->importads_resp_ok();
			else
			 	$this->importads_err();
		} else {
			syslog(LOG_INFO, log_string() . "Delete transaction OK. Partner: $partner External ad_id: $external_ad_id" );
			$this->importads_resp_ok();
		}
	}

	function importads_err($err = "", $statuscode = 400) {
		header("Content-type: text/xml");
		if(empty($err)) {
			echo "<response><statuscode>$statuscode</statuscode><statustext>Nack</statustext><updateid>{$this->updateid}</updateid></response>";
			syslog(LOG_ERR, log_string()."(ERR) Nack $statuscode (no msg)");
		} else {
			if (is_array($err)) {
				syslog(LOG_ERR, log_string(). "Got err ". print_r($err, TRUE));
				
				if (isset($err['image']) && is_array($err['image']))
					$err = implode(',', $err['image']);
				else
					$err = implode(',', $err);
			}
			echo "<response><statuscode>$statuscode</statuscode><statustext>Nack ($err)</statustext><updateid>{$this->updateid}</updateid></response>";
			syslog(LOG_ERR, log_string()."(ERR) Nack $statuscode ($err)");
		}

		exit(1);
	}

	function importads_resp_ok() {
		header("Content-type: text/xml");
		echo "<response><statuscode>200</statuscode><statustext>Ack</statustext><updateid>{$this->updateid}</updateid></response>";
		exit(0);
	}

	function element_content($parser, $data) {
		$this->xml_element_data .= $data;
	}

	function start_element($parser, $name, $attrs) {
		if ($this->xml_current_tag !== null)
			array_push($this->xml_tag_stack, $this->xml_current_tag);
		if ($this->use_xml_stack && in_array($name, $this->array_tags)) {
			$key = &$this->update;
			foreach ($this->xml_tag_stack as $path)
				$key = &$key[$path];
			if (!empty($key[$name])) {
				$this->xml_current_tag = count($key[$name]);
				array_push($this->xml_tag_stack, $name);
			} else {
				array_push($this->xml_tag_stack, $name);
				$this->xml_current_tag = 0;
			}
		} else
			$this->xml_current_tag = $name;
		$this->xml_element_data = '';
	}

	function end_element($parser, $name) {
		if ($this->xml_element_data !== '') {
			$key = &$this->update;
			$data = $this->xml_element_data;
			if (valid_utf8($data))
				$data = utf8_decode($data);

			if (@$this->use_xml_stack) {
				foreach ($this->xml_tag_stack as $path)
					$key = &$key[$path];
			}

			if ($this->array_tags && in_array($this->xml_current_tag, $this->array_tags)) {
				if (!isset($key[$this->xml_current_tag]))
					$key[$this->xml_current_tag] = array();
				array_push($key[$this->xml_current_tag], $data);
			} else {
				if (isset($key[$this->xml_current_tag]))
					$key[$this->xml_current_tag] .= $data;
				else
					$key[$this->xml_current_tag] = $data;
			}
			$this->xml_element_data = '';
		}

		if ($this->xml_tag_stack) {
			if (is_numeric($this->xml_current_tag)) /* Numeric index indicates an array element */
				array_pop($this->xml_tag_stack);
			$this->xml_current_tag = array_pop($this->xml_tag_stack);
		}
	}

	function fetch_image_urls($urlarr) {
		if (!is_array($urlarr))
			return array();

		$images = array();
		foreach ($urlarr as $img_url) {
			$image = $this->fetch_image($img_url);
			if (is_array($image)) {
				$transaction = new bTransaction();
				$transaction->add_file('image', $image[0]);
				$transaction->add_file('thumbnail', $image[1]);
				$reply = $transaction->send_command('imgput');
				if ($reply['status'] == 'TRANS_OK' && isset($reply['file']))
					$images[] = $reply['file'];
			} else {
				syslog(LOG_ERR, log_string()."failed to download image from $img_url");
			}
		}
		return $images;
	}

	function fetch_image($img_url) {
		global $BCONF;
		$res = null;

		/* Creating temporary file to store image into */
                $tmpdir = ini_get('upload_tmp_dir');
                if (!$tmpdir)
                        $tmpdir = '/tmp';
                $file = tempnam($tmpdir, "image_importads_file");
                if (!$file) {
			syslog(LOG_CRIT, log_string()."tempnam failed");
			$res = 'ERROR_TEMPNAM';
                        return $res;
		}
		$handle = fopen($file, "wb");
		if (!$handle) {
			syslog(LOG_CRIT, log_string()."fopen tempnam failed");
			$res = 'ERROR_FOPEN_TEMPNAM';
			return $res;
		}

		/* Download image from url */
		if ( !preg_match("/^https?:\/\/.*$/", $img_url)) {
			syslog(LOG_ERR, log_string()."Not a valid image URL ($img_url)");
			$res = 'ERROR_CURL_IMG_FETCH:' . $img_url;
			return $this->importads_err($res);
		}

		$ch = curl_init($img_url);
		curl_setopt($ch, CURLOPT_USERAGENT, "Blocket importads");
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->importads_conf['curl_timeout']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_FILE, $handle);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_exec($ch);
		$c_err = curl_error($ch);
		if (!empty($c_err)) {
			syslog(LOG_ERR, log_string()."Curl remote image fetch failed ($img_url) - $c_err");
			$res = 'ERROR_CURL_IMG_FETCH:' . $c_err;
			unlink($file);
			return $res;
		}

		fclose($handle);
		$curl_info =curl_getinfo($ch);
		curl_close($ch);
		if(!preg_match("/^image.*/", $curl_info['content_type'])) {
			syslog(LOG_ERR, log_string()."Curl remote image fetch failed ($img_url) - " . $curl_info['content_type']);
			$res = 'ERROR_CURL_NOIMG_FETCH';
			unlink($file);
			return $res;

		}

		ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
		ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));

		/* Convert the uploaded image */
		$image = new bImage($file);
		$image->resize();
		/* XXX. Do not watermark. Probably need to do this dependent on where we are importing from but disabled now. */
		/* 		$image->watermark(); */

		$image_buffer = $image->create_image();
		$thumbnail_buffer = $image->create_thumbnail();

		/* Check if image upload generated error messages */
		if ($image->has_error()) {
			$res = $image->get_error();
		} else {
// 			syslog(LOG_INFO, log_string()."Image ({$file}) conversion, status=OK");
			$res = array($image_buffer, $thumbnail_buffer);
		}

		/* Release image memory */
		$image->destruct();
		/* Reset the memory limit to its original state */
		ini_restore("memory_limit");
		ini_restore("max_execution_time");
		if (!unlink($file)) {
			syslog(LOG_CRIT, log_string()."Temporary image file deletion failed");
			$res = 'ERROR_TMP_FILE_DELETE';
		}

		return $res;
	}

	function fetch_xml($url) {
		$ch = curl_init($url);
		syslog(LOG_INFO, log_string()."Fetching " . $url);

		curl_setopt($ch, CURLOPT_USERAGENT, "Blocket importads");
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->importads_conf['curl_timeout']);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		/* XXX Add some validation that we connect to a valid site...(SSL etc?) */
		/* Check valid URL */
		if (  !preg_match("/^https?:\/\/.*$/", $this->update['DATA'])) {
			syslog(LOG_ERR, log_string()."Not a valid URL ($url)");
			$res = 'ERROR_CURL_XML_FETCH';
			return $this->importads_err($res);
		}

		$xmldata = curl_exec($ch);
		if (valid_utf8($xmldata) && version_compare(PHP_VERSION, '5.0.0', '<')) {
			$xmldata = utf8_decode($xmldata);
		} 
		$c_err = curl_error($ch);
		if (!empty($c_err)) {
			syslog(LOG_ERR, log_string()."Curl remote xml fetch failed ($c_err)");
			$res = 'ERROR_CURL_XML_FETCH';
			return $this->importads_err($res);
		}
		curl_close($ch);

		$this->update = array();
		$xmldata = preg_replace("/>"."[[:space:]]+"."</i","><", $xmldata);
		$this->parse_xml($xmldata);
	}

	function parse_xml($xml) {
		$xmlparser = xml_parser_create();

		$xml = ltrim($xml);

		if (!$xml || !$xmlparser) {
			return false;
		}

		/* Add xml header if missing */
                if (strncmp($xml, "<?xml", 5) != 0) {
                        $xml = '<?xml version="1.0" encoding="ISO-8859-1"?>' . $xml;
                }

		xml_set_object($xmlparser, $this);
		xml_set_element_handler($xmlparser, "start_element", "end_element");
		xml_set_character_data_handler($xmlparser, "element_content");
		xml_parser_set_option($xmlparser, XML_OPTION_SKIP_WHITE, 1);
		xml_parser_set_option($xmlparser, XML_OPTION_TARGET_ENCODING, "ISO-8859-1");

		if (!xml_parse($xmlparser, $xml, true)) {
			$res = "ERROR_XML_PARSE: line " . xml_get_current_line_number($xmlparser) . 
				" col " . xml_get_current_column_number($xmlparser) . " : " . xml_error_string(xml_get_error_code($xmlparser));
			syslog(LOG_ERR, log_string()."Error parsing xml: $res");
			$this->importads_err($res);
			return false;
		}

		if ($this->error)
			return false;

		return true;
	}

	function find_store($import_id = null) {
		$transaction = new bTransaction();
		$transaction->add_data('link_type', $this->partner);
		if ($import_id)
			$transaction->add_data('import_id', $import_id);
		$reply = $transaction->send_command('find_import_store');
		if ($reply['status'] == "TRANS_OK")
			return intval($reply['store_id']);
		if (isset($reply['error']))
			return $reply['error'];
		return 'ERROR_STORE_NOT_FOUND';
	}

	function get_store_data($store_id) {
		$transaction = new bTransaction();

		$transaction->add_data('id', $store_id);
		$transaction->add_data('offset', 0);
		$transaction->add_data('lim', 1);

		$reply = $transaction->send_command('search_comp');
		return $this->handle_search_comp_reply($reply);
	}

	function handle_search_comp_reply($reply) {
		$headers = explode("\t", $reply['hdrs']);

		if (is_array($reply['row'])) {
			$i = 0;
			foreach($reply['row'] as $row) {
				$stores[$i++] = explode("\t", $row);
			}
		} else
			$stores[0] = explode("\t", $reply['row']);

		$res = array ();
		if ($reply['rows'] != 0) {
			foreach ($stores as $store_data) {
				for ($i = 0; $i < sizeof($headers);$i++) {
					$res[substr($headers[$i], 2)] = @$store_data[$i];
				}
			}
		}

		return $res;
	}

	function init_data($category = null) {
		global $BCONF;

		$data = array();
		/* Partner specific data */
		if ($category) {
			$params = bconf_get($BCONF, "importads.{$this->partner}.category.{$category}.params");
			if (!empty($params))
				foreach ($params as $param => $val)
					$data[$param] = $val;
		}
		$bcfgdata = bconf_get($BCONF, "importads.{$this->partner}.input");
		if (is_array($bcfgdata)) {
			foreach ($bcfgdata as $key => $val) {
				$tag = $val['name'];
				$tag_type = $val['type'];
                        	if (isset($this->update[$tag])) {
					if ($tag_type == "integer")
						$data[$key] = intval($this->update[$tag]);
					else
                               	         $data[$key] = $this->update[$tag];
				}
				if (isset($this->ad_data[$tag])) {
					if ($tag_type == "integer") {
                                                $data[$key] = intval($this->ad_data[$tag]);
					} else if ($tag_type == "map") {
						$data[$key] = @$val[$this->ad_data[$tag]];
					} else {
						$data[$key] = $this->ad_data[$tag];
					}
				}
			}
		}

		/* Convert data keys to lowercase so that it can be parsed by $ad->populate() */
		foreach ($this->update as $key => $val) {
			if (!$this->array_tags || !in_array($key, $this->array_tags))
				$data[strtolower($key)] = $val;
		}
		

		return $data;
	}

	function clean_data($data) {
		/* Clean name */
		if (isset($data['name']))
			$data['name'] = str_replace('/', ' ', $data['name']);

		/* Clean phone */
		if (isset($data['phone']))
			$data['phone'] = preg_replace('/(\(.+\))/', '', $data['phone']);

		/* Clean adress */
		if (isset($data['address'])) {
			$data['address'] = str_replace(array('(',')','/'), ' ', $data['address']);
			$i = strpos($data['address'], ',');
			if ( $i !== false )
				$data['address'] = substr($data['address'], 0, $i);
			$data['address'] = preg_replace('/([^0-9]+[0-9-]*)(.*)/', '$1', $data['address']);
		}
		return $data;
	}

	function get_max_extra_images($ad) {
		global $BCONF;

		$io = get_object_vars($ad);
		$io['parent'] = bconf_get($BCONF, "*.cat.{$ad->category}.parent");
		$io['link_type'] = $this->partner;
		get_settings(bconf_get($BCONF,"*.category_settings"), "extra_images",
				create_function('$s,$k,$d', 'return $d[$k];'),
				create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
				$io);
		return $io['max'];
	}

	function fix_name($name) {
		$name = preg_replace ('/[%^\\()=#+]/', ' ', $name);
		return str_replace('/', ' ', $name);
	}

	/* This function needs more comments, like why each rule is there. */
	function fix_phone($phone) {
		/* Converted from import.pm */
		syslog(LOG_INFO, log_string()."Fixing phone '".$phone."'");

		$phone = preg_replace ('/ \((\d{3,})/', ", $1", $phone); /* "0221-18430 (070-6018010, 070-5" -> "0221-18430, 070-6018010, 070-5" */

		/* And this is uncommented code... BAD */
		$phone = preg_replace ('/[()]/', "", $phone);
		$phone = str_replace ('/', "-", $phone);
		$phone = preg_replace ('/ - /', '-', $phone);
		$phone = preg_replace ('/ +/', '', $phone);
		$phone = preg_replace ('/,+/', ',', $phone);
		$phone = preg_replace ('/^46-/', '+46-', $phone);
		$phone = preg_replace ('/,46-/', ',+46-', $phone);
		$phone = preg_replace ('/^(46)?7/', '+46-7', $phone);
		$phone = preg_replace ('/,(46)?7/', '+46-7', $phone);
		$phone = preg_replace ('/\+460/', '+46', $phone);

		$phones = array ();
		if (preg_match_all('/([0-9+][0-9+ -]{7,})/', $phone, $matches, PREG_PATTERN_ORDER)) {
			foreach ($matches[0] as $p) {
				$p = preg_replace('/^(([-+ ]*[0-9]){1,12}).*/', '$1', $p);
				if (substr ($p, 0, 3) == "46 ")
					$p = "+$p";

				// This is the same test our validator will do, pre-filter here for better handling of invalid phone numbers.
				if(preg_match('/^([-+ ]*[0-9][-+ ]*){8,}/', $p)) {
					$phones[] = $p;
				} else {
					syslog(LOG_INFO, log_string()."Phone '".$p."' rejected, PHONE_TOO_SHORT");
				}
				if (count($phones) >= 3)
					break;
			}
		}
		return implode (',', $phones);
	}

	function commit_ad($ext_ad_id, $ad, $images, $flags = 3) {
		global $BCONF;

		/* Commit to trans */
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->populate($ad->get_data_array($flags), bconf_get($BCONF, "*.common.list_features.feat"), bconf_get($BCONF, "*.common.commalist_features.feat"));
		if (!empty($images))
			$transaction->add_data('image', $images);
		$transaction->add_data('external_ad_id', $ext_ad_id);
		$transaction->add_data('link_type', $this->partner);

		$reply = $transaction->send_command('newad');
		if ($transaction->has_error()) {
			syslog(LOG_INFO, log_string()."Failed newad transaction for external_ad_id: " . $ext_ad_id);
			return $this->importads_err($transaction->get_errors());
		}

		return $reply['ad_id'];
	}


}

?>
