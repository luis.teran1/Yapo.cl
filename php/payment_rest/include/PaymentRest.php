<?php

namespace Yapo;

class PaymentRest extends Restful
{
    public function __construct()
    {
        $this->restAPISettingsKey = "*.payment_rest_settings";
    }
}
