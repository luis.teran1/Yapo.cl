<?php

namespace Yapo;

use PaymentScheduleApiClient;

class PaymentScheduleApp extends PaymentApp
{
    const REDIS = 'redis_cardata';
    public function __construct($redisInstance = null)
    {
        $this->redisInstance = empty($redisInstance) ? new EasyRedis(self::REDIS) : $redisInstance;
    }

    public function getPendingSubscription($url_data, $request_data)
    {
        if (empty($url_data['accSession'])) {
            return $this->getError('params');
        }

        $client = new PaymentScheduleApiClient();
        $client->searchType = "pending";
        $client->accSession = $url_data["accSession"];
        list($ok, $subs) = $client->searchSubscriptions();
        if ($ok) {
            $firstSub = $subs;
            if (is_array($subs)) {
                $firstSub = $subs[0];
            }
            $prodId = $firstSub->SubscriptionDetails[0]->ProductID;
            $name = Bconf::get($BCONF, "*.payment.product.{$prodId}.name");
            $price = Bconf::get($BCONF, "*.payment.product.{$prodId}.price");
            if (!empty($price)) {
                $price = intval($price);
            }
            $data = array(
                "id" => $firstSub->SubscriptionID,
                "product" => $prodId,
                "price" => $price,
                "name" => $name
            );
            return array("subscription" => $data);
        }
        return array("subscription" => "", "error" => "NOT_FOUND");
    }


    public function confirmSubscription($url_data, $request_data)
    {
        if (empty($request_data->id)) {
            return $this->getError('params');
        }

        $client = new PaymentScheduleApiClient();
        $client->subscriptionId = $request_data->id;
        list($ok, $sub) = $client->statusSubscription();
        if ($ok) {
            return array("subscription" => $sub);
        }
        return array("subscription" => "");
    }

    private function getRedisKey($source, $location, $email = null)
    {
        global $BCONF;
        if (isset($source) && isset($location) && isset($email)) {
            $main_key = "xdirect_msg_{$email}";
            $ending = "{$main_key}_{$source}_{$location}";
            return Bconf::get($BCONF, "*.common.".self::REDIS.".master").$ending;
        }
    }

    public function deleteMsgOp($url_data, $request_data)
    {
        if (empty($request_data->email)) {
             return $this->getError('params');
        }
        $sources = array("desktop", "msite");
        foreach ($sources as $source) {
            $key = $this->getRedisKey($source, 'dashboard', $request_data->email);
            $this->redisInstance->del($key);
        }
        return array("status" => "success");
    }
}
