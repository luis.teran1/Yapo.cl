<?php

namespace Yapo;

use OneClickApiClient;

class OneClickPaymentApp extends PaymentApp
{

    /**
     * Check if user is registered on Onclick service
     *
     * In $url_data
     *   email          User email
     * This function is not filtered by IP
     *
     * @url http://REGRESS_HOST:AI_PORT/payment_rest/oc_register?email=user@mail.com
     *
     * @return json {"isRegistered":1} or {"isRegistered":0}
     */
    public function isRegistered($url_data, $request_data)
    {
        if (empty($url_data['email'])) {
            return $this->getError('params');
        }

        $ocp_client = new OneClickApiClient();
        $user_oneclick = $ocp_client->isRegistered($url_data['email']);
        if ($user_oneclick) {
            return array("isRegistered" => 1);
        }
        return array("isRegistered" => 0);
    }


    /**
     * Init registration Using Onclick MS
     *
     * In $request_data
     *   email          User email
     *   return_url     URL to return when registration ends
     * This function is not filtered by IP
     *
     * @url http://REGRESS_HOST:AI_PORT/payment_rest/oc_register
     * @method POST
     * Examples:
     * @input this endpoint need a raw input in json format
     *
     * {"email": "perico@palotes.cl", "return_url": "https://www.yapo.cl"
     *
     * @return json
     *
     * {
     *     "url": "http://lisa.schibsted.cl:30201/inscripSim.php",
     *     "method": "POST",
     *     "params": {
     *         "TBK_TOKEN": "ed63a87457c5f7992f020b0eb4d3a064"
     *      }
     *  }
     */
    public function register($url_data, $request_data)
    {
        if (empty($request_data->email) || empty($request_data->return_url)) {
            return $this->getError('params');
        }

        $email = $request_data->email;
        $return_url = $request_data->return_url;

        $ocp_client = new OneClickApiClient();
        $user_oneclick= $ocp_client->isRegistered($email);
        if ($user_oneclick) {
            return array("error" => "already registered");
        }

        $result = $ocp_client->initInscription($email, $return_url);

        if (isset($result) && $result->result == 'OK') {
            $redirectData = array();
            $redirectData['url'] = $result->url;
            $redirectData['method'] = 'POST';
            $redirectData['params']['TBK_TOKEN'] = $result->token;
            return $redirectData;
        }

        return array("error" => "init inscription error");
    }
}
