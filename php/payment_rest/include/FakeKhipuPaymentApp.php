<?php

namespace Yapo;

use \DateTime;
use \DateInterval;

class FakeKhipuPaymentApp extends PaymentApp
{

    private $khipu;
    private $redis;

    /**
     * Constructor
     * Init the configuration variables
     */
    public function __construct()
    {
        global $BCONF;
        $this->khipu = Bconf::bconfGet($BCONF, "*.{$this->bconf}");
        $this->redis = new QuickRedis("redis_banners");
    }

    /**
     * Function to send payment to khipu
     * All params commes on $request_data
     * From outside this class send the parameters as json format
     * Ex: {"id":"lalala-10","amount":"1000"}
     *
     * @param Array  $url_data     This should be empty
     * @param Object $request_data Jsonf format
     *
     * @method POST
     * @return JSON
     */
    public function sendKhipuPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }

        Logger::logDebug(
            __METHOD__,
            "Generating new payment id: {$request_data->id}"
        );

        $id = $request_data->id;
        $amount = $request_data->amount;
        $subject = $request_data->subject;
        $body = $request_data->body;
        $token = $this->generateRandomString(64);
        $data = $this->generateDemoData($id, $amount, $subject, $body, $token);

        try {
            $encoded = json_encode($data);
            $this->redis->set($token, $encoded) ;
        } catch (Exception $e) {
            Logger::logError(__METHOD__, "Exception: ".$e->getMessage());
            return $this->getError('exc');
        }
        return $data;
    }

    /**
     * Function to obtain payment from khipu
     * All params commes on $url_data because whe use method get
     *
     * @param Array  $url_data     The data needed as an Array
     * @param Object $request_data This should be empty
     *
     * @method GET
     * @return JSON
     */
    public function getKhipuPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }
        $api_version = $url_data['api_version'];
        $notification_token = $url_data['notification_token'];

        if (empty($api_version) || empty($notification_token)) {
            return $this->getError('params');
        }

        Logger::logDebug(__METHOD__, "Using {$notification_token}");

        try {
            if ($api_version == $this->khipu['notify_api_version']) {
                $response = json_decode($this->redis->get($notification_token), true);
                Logger::logDebug(__METHOD__, "Response received:{$response}");
                if ($response['receiver_id'] == $this->khipu['receiver_id']) {
                    return $response;
                } else {
                    return $this->getError('receiver');
                }
            } else {
                return $this->getError('version');
            }
        } catch (Exception $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }

    /**
     * Change the status to paid (done) for payment dummy data
     *
     * @param Array  $url_data     This should be empty
     * @param Object $request_data Jsonf format
     *
     * @method PUT
     * @return JSON
     */
    public function payKhipuPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }
        $api_version = $request_data->api_version;
        $notification_token = $request_data->notification_token;

        if (empty($api_version) || empty($notification_token)) {
            return $this->getError('params');
        }

        Logger::logDebug(__METHOD__, "Using {$notification_token}");

        try {
            if ($api_version == $this->khipu['notify_api_version']) {
                $response = json_decode($this->redis->get($notification_token), true);
                Logger::logDebug(__METHOD__, "Response received:{$response}");
                if ($response['receiver_id'] == $this->khipu['receiver_id']) {
                    $response['status'] = 'done';
                    $response['status_detail'] = 'done';
                    $this->redis->set($notification_token, json_encode($response));

                    return $response;
                } else {
                    return $this->getError('receiver');
                }
            } else {
                return $this->getError('version');
            }
        } catch (Exception $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }


    /**
     *  Generate dummy demo data
     *
     *  @param String $id      New payment group id
     *  @param String $amount  Amount of the payment
     *  @param String $subject Subject for Khipu info
     *  @param String $body    Body with payment detail
     */
    private function generateDemoData($id, $amount, $subject, $body, $token)
    {
        $payment_id = $this->generateRandomString();

        $exp = new DateTime();
        $exp->add(new DateInterval('P'.$this->khipu['expire'].'D'));
        $pay_url = $this->khipu['url_khipu'].
            '?notification_token='.$token.
            '&notify_api_version='.$this->khipu['notify_api_version'];

        $data = array(
            "payment_id" => $payment_id,
            "payment_url" => $pay_url,
            "simplified_transfer_url" => "https://khipu.com/payment/simplified/{$payment_id}",
            "transfer_url" => "https://khipu.com/payment/manual/{$payment_id}",
            "app_url" => "khipu:///pos/{$payment_id}",
            "ready_for_terminal" => false,
            "notification_token" => $token,
            "receiver_id" => $this->khipu["receiver_id"],
            "conciliation_date" => null,
            "subject" => $subject,
            "amount" => $amount,
            "currency" => $this->khipu['currency'],
            "status" => "pending",
            "status_detail" => "pending",
            "body" => $body,
            "picture_url" => "",
            "receipt_url" => "",
            "notify_url" => $this->khipu['notify_url'],
            "notify_api_version" => $this->khipu['notify_api_version'],
            "return_url" => "{$this->khipu['url_success']}?kid={$id}",
            "cancel_url" => "{$this->khipu['url_failure']}?kid={$id}",
            "expires_date" => (array)$exp,
            "attachment_urls" => array(),
            "bank" => "",
            "bank_id" => "",
            "payer_name" => "",
            "payer_email" => "",
            "personal_identifier" => "",
            "bank_account_number" => "",
            "out_of_date_conciliation" => false,
            "transaction_id" => $id,
            "custom" => "",
            "responsible_user_email" => "demos@khipu.com",
            "send_reminders" => false,
            "send_email" => false,
            "payment_method" => "not_available"
        );
        return $data;
    }
}
