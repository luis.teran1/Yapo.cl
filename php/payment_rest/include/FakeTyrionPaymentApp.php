<?php

namespace Yapo;

class FakeTyrionPaymentApp extends PaymentApp
{

    private $redis;
    private $paySimUrl;

    /**
     * Constructor
     * Init the configuration variables
     */
    public function __construct()
    {
        global $BCONF;
        $this->redis = new EasyRedis("redis_payment");
        $this->paySimUrl = Bconf::get($BCONF, "*.common.base_url.secure")."/paysim";
    }

    /**
     * Healthcheck
     * @method POST
     * @return JSON
     */
    public function healthCheck($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }
        return array("result" => "OK");
    }

    /**
     * Verify the payment and add new info on redis
     * @param Array  $url_data     The data needed as an Array
     * @param Object $request_data This should be empty
     *
     * @method GET
     * @return JSON
     */
    public function verifyPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }
        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            return array("error" => "method ".$_SERVER['REQUEST_METHOD']." not allowed for: ".__METHOD__);
        }

        try {
            $redis_key = $this->redis->prepend.$request_data->token_ws."_paysim";
            $redis_data = $this->redis->get($redis_key);
            $decoded_data = json_decode($redis_data);

            $data = array("result" => "OK");

            $data["amount"] = $decoded_data->tbk_amount;
            $data["buyOrder"] = $decoded_data->tbk_order;
            $data["sessionId"] = $decoded_data->tbk_session;
            $data["url"] = "{$this->paySimUrl}?voucher=webpay_plus";

            $elements_to_send = array(
                 "authorizationCode",
                 "cardNumber",
                 "paymentTypeCode",
                 "responseCode",
                 "sharesNumber",
                 "commerceCode",
                 "transactionDate",
                 "vci"
             );
            foreach ($elements_to_send as $key) {
                $data[$key] = $decoded_data->$key;
            }
            return $data;
        } catch (Exception $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }

    /**
     * Create new payment in redis and return random token
     * @param Array  $url_data     This should be empty
     * @param Object $request_data Jsonf format
     *
     * @method PUT
     * @return JSON
     */
    public function startPayment($url_data, $request_data)
    {
        global $BCONF;
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }

        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            return array("error" => "method ".$_SERVER['REQUEST_METHOD']." not allowed for: ".__METHOD__);
        }

        try {
            $token = $this->generateRandomString(64);
            $dataPaySim = json_encode($request_data);
            $redis_key = $this->redis->prepend.$token."_paysim";
            $this->redis->setex($redis_key, 600, $dataPaySim);

            $data = array(
                "result" => "OK",
                 "url" => $this->paySimUrl,
                 "token_ws" => $token
            );

            return $data;
        } catch (Exception $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }

    /**
     * Receipt the payment with card info and allow payment process only for configured cards
     * Also check missing params
     * @param Array  $url_data     This should be empty
     * @param Object $request_data Jsonf format
     *
     * @method PUT
     * @return JSON
     */
    public function completePayment($url_data, $request_data)
    {
        global $BCONF;
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }

        if ($_SERVER['REQUEST_METHOD'] != "POST") {
            return array("error" => "method ".$_SERVER['REQUEST_METHOD']." not allowed for: ".__METHOD__);
        }

        try {
            $token = $this->generateRandomString(64);
            $params = array(
                "amount",
                "sessionId",
                "buyOrder",
                "cardExpirationDate",
                "cvv",
                "cardNumber",
                "shareNumber",
                "gracePeriod",
                "deferredPeriodIndex"
            );

            $header_error = array(
                "result" => "ERROR",
                "message"=> "Webpay no disponible"
            );

            foreach ($params as $key) {
                if (!isset($request_data->$key)) {
                    $header_error["error"] = array($key => "Invalid $key ");
                    return $header_error;
                }
            }

            $shareAmount = round(intval($request_data->amount) / intval($request_data->shareNumber));
            $responseCode = 3;
            $authorizationCode = '0000';
            $resultMsj = "ERROR";
            $payTypeCode = "NC";
            switch ($request_data->shareNumber) {
                case 1:
                    $payTypeCode = "VN";
                    break;
                case 2:
                    $payTypeCode = "S2";
                    break;
                case 3:
                    $payTypeCode = "SI";
                    break;
                case 4:
                case 5:
                case 6:
                    $payTypeCode = "VC";
                    break;
            }

            if ($request_data->cardNumber == "4051885600446623") {
                $responseCode = 0;
                $authorizationCode = rand(1000, 9999);
                $resultMsj = "OK";
                $fullResponse = array(
                    "result"=> $resultMsj,
                    "responses"=> array(
                        "init"=> array(
                            "token"=> $token
                        ),
                        "query"=> array(
                            "buyOrder"=> $request_data->buyOrder,
                            "deferredPeriods"=> null,
                            "queryId"=> 2985100,
                            "shareAmount"=> $shareAmount,
                            "token"=> $token,
                        ),
                        "authorize"=> array(
                            "accountingDate"=> "1121",
                            "buyOrder"=> $request_data->buyOrder,
                            "detailsOutput"=> array(
                                "sharesNumber"=> $request_data->shareNumber,
                                "amount"=> $request_data->amount,
                                "commerceCode"=> "597020000545",
                                "buyOrder"=> $request_data->buyOrder,
                                "authorizationCode"=> $authorizationCode,
                                "paymentTypeCode"=> $payTypeCode,
                                "responseCode"=> $responseCode
                            ),
                            "sessionId"=> "5a142ff1154b4",
                            "transactionDate"=> "2017-11-21T16:04:19"
                        )
                    )
                );
            } else {
                $fullResponse = array(
                    "result" => "ERROR",
                    "message" => "Webpay no disponible",
                    "error" =>  array(
                        "authorize" =>  array(
                            "accountingDate" => "1207",
                            "buyOrder" => $request_data->buyOrder,
                            "detailsOutput" =>  array(
                                "sharesNumber" => $request_data->shareNumber,
                                "amount" => $request_data->amount,
                                "commerceCode" => "597020000545",
                                "buyOrder" => $request_data->buyOrder,
                                "authorizationCode" => "000000",
                                "paymentTypeCode" => "VC",
                                "responseCode" => -3,
                            ),
                            "sessionId" => $request_data->sessionId,
                            "transactionDate" => "2017-12-07T12 => 13 => 29.279-03 => 00"
                        )
                    )
                );
            }
            return $fullResponse;
        } catch (Exception $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }
}
