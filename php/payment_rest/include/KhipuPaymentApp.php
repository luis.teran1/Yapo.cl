<?php

namespace Yapo;

use Khipu;
use \DateTime;
use \DateInterval;

class KhipuPaymentApp extends PaymentApp
{

    private $khipu;
    private $klient;

    /**
     * Constructor
     * Init the configuration variables
     */
    public function __construct()
    {
        global $BCONF;
        $this->khipu = Bconf::bconfGet($BCONF, "*.{$this->bconf}");

        $kconf = new Khipu\Configuration();
        $kconf->setSecret($this->khipu['secret']);
        $kconf->setReceiverId($this->khipu['receiver_id']);
        $kconf->setDebug("true"==$this->khipu['debug']);

        $this->klient = new Khipu\ApiClient($kconf);
        Logger::logDebug(
            __METHOD__,
            "Config {$this->bconf} data:".var_export($this->khipu, true)
        );
    }

    /**
     * Function to send payment to khipu
     * All params commes on $request_data
     * From outside this class send the parameters as json format
     * Ex: {"id":"lalala-10","amount":"1000"}
     *
     * @param Array  $url_data     This should be empty
     * @param Object $request_data Jsonf format
     *
     * @method PUT
     * @return JSON
     */
    public function sendKhipuPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }

        Logger::logDebug(
            __METHOD__,
            "Generating new payment id: {$request_data->id}"
        );

        $kPaymentApi = new Khipu\Client\PaymentsApi($this->klient);

        $id = $request_data->id;
        $amount = $request_data->amount;
        $subject = $request_data->subject;
        $body = $request_data->body;

        try {
            $exp = new DateTime();
            $exp->add(new DateInterval('P'.$this->khipu['expire'].'D'));
            $opts = array(
             "expires_date" => $exp,
             "body" => $body,
             "transaction_id" => $id,
             "notify_url" => $this->khipu['notify_url'],
             "notify_api_version" => $this->khipu['notify_api_version'],
             "return_url" => "{$this->khipu['url_success']}?kid={$id}",
             "cancel_url" => "{$this->khipu['url_failure']}?kid={$id}"
            );
            $resp = $kPaymentApi->paymentsPost(
                $subject,
                $this->khipu['currency'],
                $amount,
                $opts
            );
            Logger::logDebug(__METHOD__, "Response received: {$resp}");
            $payment_generated = $kPaymentApi->paymentsIdGet($resp->getPaymentId());
            return json_decode((string)$payment_generated);
        } catch (Exception $e) {
            Logger::logError(__METHOD__, "Exception: ".$e->getMessage());
            return $this->getError('exc');
        }
    }

    /**
     * Function to obtain payment from khipu
     * All params commes on $url_data because whe use method get
     *
     * @param Array  $url_data     The data needed as an Array
     * @param Object $request_data This should be empty
     *
     * @method GET
     * @return JSON
     */
    public function getKhipuPayment($url_data, $request_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }
        $api_version = $url_data['api_version'];
        $notification_token = $url_data['notification_token'];

        if (empty($api_version) || empty($notification_token)) {
            return $this->getError('params');
        }

        Logger::logDebug(__METHOD__, "Using {$notification_token}");

        try {
            if ($api_version == $this->khipu['notify_api_version']) {
                $payments = new Khipu\Client\PaymentsApi($this->klient);
                $response = $payments->paymentsGet($notification_token);
                Logger::logDebug(__METHOD__, "Response received:{$response}");
                if ($response->getReceiverId() == $this->khipu['receiver_id']) {
                    return json_decode((string)$response);
                } else {
                    return $this->getError('receiver');
                }
            } else {
                return $this->getError('version');
            }
        } catch (\Khipu\ApiException $exception) {
            Logger::logError(__METHOD__, "Exception: ".$exception->getMessage());
            return $this->getError('exc');
        }
    }
}
