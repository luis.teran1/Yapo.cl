<?php

namespace Yapo;

class YapoPaymentApp extends PaymentApp
{

    /**
     * Return the price for autobump
     *
     * In $url_data
     *   id          list_id of ad
     *   frequency   hours between bumps
     *   num_days    quantity of days to execute bumps
     *   use_night   optional (Use 24hrs for bumps by default avoid from 0 to 7 hours)
     * This function is not filtered by IP
     *
     * @url http://REGRESS_HOST:AI_PORT/payment_rest/autobump?id=8000056&frequency=1&num_days=1
     *
     * @return json
     */
    public function getAutoBumpPrice($url_data, $request_data)
    {
        if (empty($url_data['id'])
            || empty($url_data['frequency'])
            || empty($url_data['num_days'])
        ) {
            return $this->getError('params');
        }

        if (empty($url_data['use_night'])) {
            $url_data['use_night'] = 0;
        }

        $result = Payment\Trans::getAutoBumpPrice(
            $url_data['id'],
            $url_data['frequency'],
            $url_data['num_days'],
            $url_data['use_night']
        );
        if ($result === false) {
            return $this->getError('autobump_error_price');
        }

        return $result;
    }

    /**
     * Return payment from trans
     *
     * @param Array  $url_data     This should be empty
     *
     * @method GET
     * @return JSON
     */
    public function getYapoPayment($url_data)
    {
        if (!$this->allowedIp()) {
            return $this->getError('ip');
        }

        $result = Payment\Trans::getPaymentItems($url_data['transaction_id']);
        if ($result === false) {
            return $this->getError('not_found');
        }

        $data = $this->sortPaymentData($result);
        Logger::logDebug(__METHOD__, "Payment Yapo data:".var_export($data, true));
        return $data;
    }

    /**
     * Sort data to return payment in the right format
     *
     * @param array $result Trans returned
     *
     * @return array sorted
     */
    public static function sortPaymentData($result)
    {
        $common_data = array(
            'account_id',
            'doc_type',
            'email',
            'has_pack',
            'purchase_id',
            'purchase_status',
            'remote_addr',
            'store_id',
            'timestamp',
            'amount'
        );
        $master_data = array('amount' => 0);
        foreach ($result as $key => $val) {
            foreach ($val as $itemKey => $itemVal) {
                if (in_array($itemKey, $common_data)) {
                    $master_data[$itemKey] = $itemVal;
                } else {
                    if ($itemKey == "price") {
                        $master_data['amount'] += $itemVal;
                    }
                    if (!isset($master_data["detail_$key"])) {
                        $master_data["detail_$key"] = array();
                    }
                    $master_data["detail_$key"][$itemKey] = $itemVal;
                }
            }
        }
        return $master_data;
    }

    public function resourceNotFound()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        $path = $_SERVER['REQUEST_URI'];

        return array(
            "ERROR" => "Resource not found",
            "METHOD" => $method,
            "PATH" => $path
        );
    }
}
