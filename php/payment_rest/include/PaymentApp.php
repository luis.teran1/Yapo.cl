<?php

namespace Yapo;

abstract class PaymentApp
{
    public function __construct()
    {
        // This constructor is needed by injection process
    }

    protected $bconf = "PaymentApp";


    /**
     * Function to return json formated error
     *
     * @param String $code error code key
     *
     * @return JSON
     */
    protected function getError($code)
    {
        $error_messages = array(
            'exc' => 'Exception generated',
            'version' => 'Payment fail version missmatch',
            'receiver' => 'Payment fail receiver_id missmatch',
            'params' => 'Parameters missing',
            'ip' => 'Access not allowed for Remote address',
            'not_found' => 'Payment not found',
            'autobump_error_price' => 'Error getting price for autobump'
        );
        $error = array(     "error_code" => $code);
        if (isset($error_messages[$code])) {
            $error["error_message"] = $error_messages[$code];
        } else {
            $error["error_message"] = 'Error is not defined';
        }
        return json_encode($error);
    }

    /**
     * Verify if the Remote IP allow access to this API
     *
     * @return Boolean
     */
    protected function allowedIp()
    {
        global $BCONF;

        $remote_address = explode(".", $_SERVER['REMOTE_ADDR']);
        $ip_settings = array(
            'oct1' => $remote_address[0],
            'oct2' => $remote_address[1],
            'oct3' => $remote_address[2],
            'oct4' => $remote_address[3]
        );

        get_settings(
            Bconf::bconfGet($BCONF, "*.{$this->bconf}_settings"),
            "allowed",
            create_function('$s,$k,$d', 'return $d[$k];'),
            create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
            $ip_settings
        );
        return $ip_settings['ip'] == "1";
    }

    /**
     * Generate random string
     *
     * @param int $length Length of output string
     * @param boolean $use_mayus Indicate if we whant MAYUS on output string
     *
     * @return String
     */
    protected function generateRandomString($length = 12, $use_mayus = false)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
        if ($use_mayus) {
            $characters .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
    }
}
