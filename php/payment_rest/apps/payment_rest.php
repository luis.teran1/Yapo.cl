<?php

namespace Yapo;

require_once 'khipu/autoload.php';
require_once 'autoload_lib.php';

//Syslog
openlog('payment_rest', LOG_ODELAY, LOG_LOCAL0);


$api = new PaymentRest();
$api->run();
