<?php
/*
 * Store application
 */
openlog("store", LOG_ODELAY, LOG_LOCAL0);

require_once('blocket_application.php');
require_once('dyn_config.php');
require_once('bTransaction.php');
require_once('bImage.php');

use Yapo\Cpanel;

function handle_line(&$data, $key, $value) {
	if ($key == 'date') {
		$timestamp = format_date($value);
		$nicedate = split(';', $timestamp);
		push_value($data, 'nicedate', $nicedate[0]);
		push_value($data, "nicetime", $nicedate[1]);
	} else {
		push_value($data, $key, $value);
	}
}

class blocket_store extends blocket_token_application {
	var $pay_for;  // Tell us what we're paying for ('voucher', 'bump', ...)
	var $pay_amount; // The amount to pay
	var $bump_pay_type; // what we're paying with (duh)
	var $order_id;
	var $store_id;
 	var $ads_filtered;
 	var $store_saved;
 	var $import_partner;
	var $login_id;
	var $stats_period;
	var $ar_name;
	var $ar_phone;
	var $ar_email;
	var $ar_adreply_body;
	var $ar_cc;
	var $list_id;
	var $checked_ads;
	var $can_has_gallery;
	var $ads_to_bump;
	var $so; /* sort order */
	var $cmd;

	/* Track where user came from ('source') */
	var $ht;

	function blocket_store($init = true) {
		if (!$init)
			return;
		global $BCONF;
		global $session;

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
		$this->headscripts[] = "common.js";
		$this->headscripts[] = "store.js";
		$this->headscripts[] = "/tjs/arrays_v2.js";
		$this->headstyles[] = "store.css";
		$this->store_data = array();
		$this->application_name = 'store';

		$this->init('main', 0, true);
		$this->add_static_vars(array('login_id', 'can_has_gallery', 'checked_ads', 'store_id', 'ads_filtered'));
	}

	function get_state($statename) {
		switch ($statename) {
		case 'main':
			return array('function' => 'store_main',
				     'params' => array('id', 'sp' => 'f_integer', 'email', 'ms' => 'f_integer', 'list_id' => 'f_integer', 'cmd' => 'f_cmd',
						       'ht' => 'f_integer'),
				     'method' => 'get');
		case 'formsendpass':
			return array('function' => 'store_form_sendpass',
				     'params' => array('id'),
				     'method' => 'get');
		case 'sendpass':
			return array('function' => 'store_sendpass',
				     'params' => array('email'),
				     'method' => 'post');
		case 'pass_sent':
			return array('function' => 'store_pass_sent',
				     'method' => 'get');
		case 'formmailstore':
			return array('function' => 'store_form_mailstore',
				     'method' => 'get');
		case 'mailstore':
			return array('function' => 'store_mailstore',
				     'params' => array('name', 'email', 'phone', 'cc'=> 'f_integer', 'adreply_body'),
				     'method' => 'post');
		case 'mail_sent':
			return array('function' => 'store_mail_sent',
				     'method' => 'get');
		case 'update':
			return array('function' => 'store_update',
				     'params' => array('info_header', 'info_text', 'bgcolor' => 'f_integer', 'textcolor' => 'f_integer', 'name', 'address', 'postal_city', 'zipcode', 'phone', 'infopage_home' => 'f_url', 'dellogo' => 'f_integer', 'delextra' => 'f_integer'),
				     'auth_required' => 'login',
				     'method' => 'post');
		case 'update_passwd':
			return array('function' => 'store_update_passwd',
				     'params' => array('passwd', 'verify_passwd' => 'f_passwd', 'np' => 'f_bool'),
				     'auth_required' => 'login',
				     'method' => 'post');
		case 'list':
			return array('function' => 'store_list',
				     'params' => array('o', 'stats_period', 'fm' => 'f_integer', 'so' => 'f_integer_or_null'),
				     'auth_required' => 'login',
				     'method' => 'get');
		case 'presentation':
			return array('function' => 'store_presentation',
				     'auth_required' => 'login',
				     'method' => 'get');
		case 'list_ads':
			return array('function' => 'store_list_ads',
				     'params' => array('o', 'cs', 'so' => 'f_integer_or_null'),
				     'auth_required' => 'login',
				     'method' => 'get');
		case 'login':
			return array('function' => 'store_login',
				     'method' => 'get');
		case 'logout':
			return array('function' => 'store_logout',
				     'params' => array('url'),
				     'method' => 'get');
		case 'verify_login':
			return array('function' => 'store_verify_login',
				     'params' => array('username', 'passwd'),
				     'method' => 'post');
		case 'voucher':
			return array('function' => 'store_voucher',
				     'params' => array('o', 'amount' => 'f_integer'),
				     'auth_required' => 'login',
				     'method' => 'get');
		case 'fillvoucher':
			return array('function' => 'store_fillvoucher',
					'params' => array('amount' => 'f_integer', 'iagree' => 'f_bool'),
					'auth_required' => 'login',
					'method' => 'post');
		case 'pay_redir':
			return array('function' => 'store_pay_redir',
				'params' => array('pay_type' => 'f_pay_type', 'pay_for' => 'f_pay_for'),
				'method' => 'post');
		case 'cancel_payment':
			return array('function' => 'store_cancel_payment',
				'method' => 'get');
		case 'payex_form':
			return array('function' => 'store_payex_form',
					'params' => array('orderid' => 'f_numeric', 'price' => 'f_numeric', 'error_state' => 'f_error_state'),
					'method' => 'get',
					'allowed_states' => array('payex_form'));
		case 'payex_result_s':
			return array('function' => 'store_payex_result_s',
					'params' => array('merchantid' => 'f_integer', 'orderid' => 'f_order_id', 'status' => 'f_integer'),
					'method' => 'get',
					'allowed_states' => array('payex_cancel'));
		case 'payex_result_f':
			return array('function' => 'store_payex_result_f',
					'params' => array('merchantid' => 'f_integer', 'orderid' => 'f_order_id', 'status' => 'f_integer'),
					'method' => 'get',
					'allowed_states' => array('payex_cancel'));
		case 'payex_cancel':
			return array('function' => 'store_payex_cancel',
					'params' => array('merchantid' => 'f_integer', 'orderid' => 'f_order_id'),
					'method' => 'get',
					'allowed_states' => array('payex_cancel'));
		case 'voucher_filled':
			return array('function' => 'store_voucher_filled',
					'method' => 'get',
					'allowed_states' => array('voucher_filled'));
		case 'ad_clicked':
			return array('function' => 'ad_clicked',
					'params' => array('id' => 'f_integer', 'checked' => 'f_integer', 'cs', 'imported' => 'f_bool'),
					'auth_required' => 'login',
					'method' => 'get');
		case 'bump_ads':
			return array('function' => 'store_bump_ads',
				     'params' => array('cs'),
				     'method' =>  'get');
		case 'bump_specification':
			return array('function' => 'store_bump_specification',
				     'method' => 'get');
		case 'bump_done':
			return array('function' => 'store_bump_done',
				     'method' => 'get', 
				     'allowed_states' => array('bump_done'));
		case 'delete_ads':
			return array('function' => 'delete_ads',
				     'method' =>  'get');
		}
	}

	function create_ad_actions($ads, $action_type, $parent_payment_group_id, $pay_type) {

		foreach($ads as $ad_id => $value) {
			$transaction = new bTransaction();

			$transaction->add_data('action_type', $action_type);
			$transaction->add_data('parent_payment_group_id', $parent_payment_group_id);
			$transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);
			$transaction->add_data('pay_type', $this->get_save_name($pay_type));
			$transaction->add_data('ad_id', $ad_id);
			$transaction->add_data('price_name', array('ad_action'));
			$transaction->add_data('price_amount', array($value['category_price']));
			if(isset($value['extra_image_price'])) {
                        	$transaction->add_data('price_name', array('extra_images'));
                        	$transaction->add_data('price_amount', array($value['extra_image_price']));
			}

			$transaction->auth_type = 'store';
			$reply = $transaction->send_admin_command('create_unpaid_ad_action', true, false);
			if(!$reply || $reply['status'] != 'TRANS_OK') {
				die_with_template(__FILE__, log_string()."Transaction handler exited with errorcode '".$reply['status']."' after create_unpaid_ad_action with ad_id=".$ad_id);
			}
		}
	}


	function get_save_name($pay_type) {
		switch ($pay_type) {
		case 'card':
			return 'paycard_save';
		case 'voucher':
			return 'voucher_save';
		default:
			return '';
		}
	}


	function store_pay_redir($pay_type, $pay_for) {

		$this->pay_for = $pay_for;

		switch($pay_for) {
			case 'bump':
				$transaction = new bTransaction();
				$transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);
				$transaction->add_data('action_type','bump');
				$transaction->add_data('pay_type',$this->get_save_name($pay_type));
				$transaction->add_data('store_id', $this->store_id);
				$transaction->add_data('amount', $this->pay_amount);
				$transaction->auth_type = 'store';
				$reply = $transaction->send_admin_command('create_unpaid_store_action', true, false);
				if(!$reply || $reply['status'] != 'TRANS_OK') {
					die_with_template(__FILE__, log_string()."Transaction handler exited with errorcode: ".$reply['status']);
				}
				$payment_group_id = $reply['create_unpaid_store_action'][0]['payment_group_id'];
				$this->order_id = '99'.$payment_group_id; // '99' is magic bump prefix
				$this->create_ad_actions($this->ads_to_bump, $pay_for, $payment_group_id, $pay_type);
				break;

			default:
				die_with_template(__FILE__, log_string()."store_pay_redir(), value of pay_for unknown: '".$pay_for."'");
		}

		$this->bump_pay_type = $pay_type;
		switch($pay_type) {
			case 'card':
				return array('payex_form', 'orderid' => $this->order_id, 'price' => $this->pay_amount, 'error_state' => 'verify_bump');
				break;
			case 'voucher':
				$transaction = new bTransaction();
				$transaction->add_data('order_id', $this->order_id . 'a' . time());
				$transaction->add_data('paid_with_voucher','1');
				$transaction->add_data('amount', $this->pay_amount);
				$transaction->add_data('status','OK');
				$transaction->send_command('clear');
				$transaction->handle_reply();
				$this->store_cleanup('full');
				return 'bump_done';
				break;
			default:
				die_with_template(__FILE__, log_string()."store_pay_redir(), value of pay_type unknown: '".$pay_type."'");
		}

	}

	function store_bump_ads($cs) {
		global $BCONF;

		if($cs == "deleted" && count($this->checked_ads['deleted']) == 1) {
			header("Location: ".bconf_get($BCONF, "common.base_url.ai")."/ai?ca=".$this->caller->to_string()."&id=D".reset($this->checked_ads['deleted']));
			return;
		}

		/* No ads are checked and user shouldn't be here */
		if (empty($this->checked_ads["published"])) {
			syslog(LOG_WARNING, log_string()."No checked ads for user.");
			return 'main';
		}

		if (count($this->checked_ads['published']) == 1 && $this->checked_ads['imported'] == 0) {
			header("Location: " . bconf_get($BCONF, "common.base_url.ai") . "/ai?ca=".$this->caller->to_string()."&id=D".reset($this->checked_ads['published']));
			return;
		}

		$ads = array(); 

		foreach ($this->checked_ads["published"] as $ad_id => $checked) {
			$transaction = new bTransaction();
			$transaction->add_data('ad_id', $ad_id);
			$reply = $transaction->send_command('get_bump_price_params');

			if ($reply["status"] == "TRANS_OK") {
				$ads[$ad_id] = $reply["get_bump_price_params"][0]; 
			} else {
				syslog(LOG_WARNING, log_string()."Could not bump ad: $ad_id");
				return 'presentation';
			}
		}
		$data = array();

		foreach ($ads as $ad_id => $value) {
			$value['parent'] = bconf_get($BCONF, "*.cat.{$value["category"]}.parent");
			get_settings(bconf_get($BCONF,"*.category_settings"), "price", 
				     create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'), 
				     create_function('$s,$k,$v,$d', '$d["price"] = $v; $d["category_price"] = $v;'), $value);

			if($value["images"] > 1) {
				get_settings(bconf_get($BCONF,"*.category_settings"), "extra_images",
					create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
					create_function('$s,$k,$v,$d', 'if ($k == "price") $d["price"] += $v; $d["extra_image_price"] = $v;'),
					$value);
			}
			$ads[$ad_id] = $value;
			$data['tot_price'] += intval($value['price']);
		}
		$this->ads_to_bump = $ads;

		if ($this->get_voucher_balance() >= $data['tot_price'])
			$data['voucher_amount_enough'] = 1;
		$data['renew_ads'] = count($this->checked_ads["published"]);
		$this->pay_amount = $data['tot_price'];
		$this->display_layout('store/verify_bump.html', $data);
	}
	
	function store_bump_specification() {
		$tot_price = 0;
		foreach($this->ads_to_bump as $ad_id => $value) {
			$tot_price += $value['price'];
		}

		$this->display_layout('store/bump_specification.html', array('tot_price' => $tot_price), array('bump_ads' => $this->ads_to_bump), null, 'popup');
	}

	function get_store_id_string() {
		$line = '';
		if (!isset($_SESSION['global']))
			return $line;

		if (isset($_SESSION['global']['store_username']))
			$line .= "email ".$_SESSION['global']['store_username'];
		else
			$line .= "(unknown store username)";

		if (isset($_SESSION['global']['store_name']))
			$line .= " representing ".$_SESSION['global']['store_name'];
		else
			$line .= "(unknown store_name)";

		if (isset($_SESSION['global']['store_id']))
			$line .= " (store_id ".$_SESSION['global']['store_id'].")";
		else
			$line .= "(unknown store_id)";
		return $line;
	}

	function delete_ads() {
		global $BCONF;

		$sid = $this->get_store_id_string();
		if ($sid)
			syslog(LOG_INFO, log_string().$sid." is deleting one or more published ads.");

		/* No ads are checked and user shouldn't be here */
		if (!$this->checked_ads || !isset($this->checked_ads["published"])) {
			syslog(LOG_WARNING, log_string()."No checked published ads for user.");
			return 'presentation';
		} else {
			syslog(LOG_INFO, log_string().count($this->checked_ads['published'])." published ad(s) checked for deletion: ".join(',',$this->checked_ads['published']));
		}
	
		$ads = array(); 
		$num_ok = 0;
		$num_fail = 0;
		foreach ($this->checked_ads["published"] as $ad_id => $checked) {
			$transaction = new bTransaction();
			$transaction->auth_type = 'store';
			$transaction->add_data('ad_id', $ad_id);
			$transaction->add_data('reason', 'user_deleted');
			$reply = $transaction->send_admin_command('deletead');

			if (!preg_match('/^TRANS_OK/', $reply["status"])) {
				syslog(LOG_WARNING, log_string()."Could not delete ad_id $ad_id".(isset($reply['error']) ? ", ".$reply['error'] : ""));
				++$num_fail;
			} else {
				++$num_ok;
			}
		}
		syslog(LOG_INFO, log_string().$num_ok." published ads deleted ok, ".$num_fail." failed.");
		unset($this->checked_ads['published']);
		return array('list', 'fm' => 2);
	}

	function ad_clicked($id, $checked, $cs, $imported) {
		if(!$cs) { // We invent a name for the default tab.
			$cs = 'published';
		}

		if(!isset($this->checked_ads['imported']))
			$this->checked_ads['imported'] = 0;

		if($imported)
			$this->checked_ads['imported'] += $checked ? 1 : -1;

		if(!$checked) {
			unset($this->checked_ads[$cs][$id]);
		} else {
			$this->checked_ads[$cs][$id] = $checked;
		}

		$result = "{checked_count:".count($this->checked_ads[$cs]).", imported:" . $this->checked_ads['imported'];
		
		$i = 0;
		/* Check if exactly one gallery ad candidate is checked, in which case we should enable the gallery button */
		foreach ($this->can_has_gallery as $value) {
			if (isset($this->checked_ads[$cs][$value])) {
				if (++$i > 1) {
					$i = 0;
					break;
				}	
			}
		}

		if ($i == 1) {
			$result .= ", can_gallery:1";
		}

		$result.= "}";

		print $result;
	}

	function check_logged_in() {
		if (empty($_SESSION['global']['store_id']))
			return false;

		$client = memcached_client();
		$res = strlen($client->get(session_id() . '_token_store', true)) > 0;
		return $res;
	}

	function handle_search_comp_reply($reply) {
		$headers = explode("\t", $reply['hdrs']);

		if (is_array($reply['row'])) {
			$i = 0;
			foreach($reply['row'] as $row) {
				$stores[$i++] = explode("\t", $row);
			}	
		} else
			$stores[0] = explode("\t", $reply['row']);

		$res = array ();
		if ($reply['rows'] != 0) {
			foreach ($stores as $store_data) {
				for ($i = 0; $i < sizeof($headers);$i++) {
					$res[substr($headers[$i], 2)] = @$store_data[$i];
				}	
			}
		}

		return $res;
	}

	function upload_image($file, $maxwidth, $maxheight, $thumbnail = false) {
		global $BCONF;
		
		$res = null;
		if (@$_FILES[$file]['error'] == UPLOAD_ERR_OK && strlen(@$_FILES[$file]['name'])) {
			syslog(LOG_INFO, log_string()."Image ({$_FILES[$file]['name']}) uploaded with type ({$_FILES[$file]['type']}) and size ({$_FILES[$file]['size']})");

			/* Set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc) */
			ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
			ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));

			/* Convert the uploaded image */
			$image = new bImage($_FILES[$file]['tmp_name']);
			$image->resize($maxwidth, $maxheight);

			$image_buffer = $image->create_image();
			if ($thumbnail)
				$thumbnail_buffer = $image->create_thumbnail();

			/* Check if image upload generated error messages */
			if ($image->has_error()) {
				$res = $image->get_error();
			} else {
				syslog(LOG_INFO, log_string()."Image ({$_FILES[$file]['name']}) conversion, status=OK");
				if ($thumbnail)
					$res = array($image_buffer, $thumbnail_buffer);
				else
					$res = array($image_buffer);
			}

			/* Release image memory */
			$image->destruct();

			/* Reset the memory limit to its original state */
			ini_restore("memory_limit");
			ini_restore("max_execution_time");
		} else if (strlen(@$_FILES[$file]['name']) > 0) {
			syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
			$res = 'ERROR_IMAGE_LOAD';
		}
		return $res;
	}

	function store_update_passwd($passwd, $verify_passwd, $np) {
		global $BCONF;

		if ($passwd != $verify_passwd) {
			$this->errors['passwd'] = 'ERROR_PASSWORD_MISMATCH';
			return 'presentation';
		}

		if (strlen ($passwd) < 5) {
			$this->errors['passwd'] = 'ERROR_PASSWORD_TOO_SHORT';
			return 'presentation';
		}
		
		$transaction = new bTransaction();
		$transaction->auth_type = 'store';

		$salt = '';
		for ($i = 0; $i < 16; $i++)
			$salt .= chr(mt_rand(32, 126));
		$transaction->add_data('passwd', $salt . sha1($salt . $passwd));
		$reply = $transaction->send_admin_command('store_passwd');
		
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'presentation';
		} else {
			$this->passwd_saved = lang("PASSWD_SAVED");
		}
		if ($np)
			return 'main';
		else
			return 'presentation';
	}

	function store_update($info_header, $info_text, $bgcolor, $textcolor, $name, $address, $postal_city, $zipcode, $phone, $infopage_home,  $dellogo, $delextra) {
		global $BCONF;
		
		$transaction = new bTransaction();
		$transaction->auth_type = 'store';

		if (isset($_FILES['image_logo'])) {
			$image = $this->upload_image('image_logo',
					bconf_get($BCONF, "*.common.store.logo.max_width"),
					bconf_get($BCONF, "*.common.store.logo.max_height"),
					true);
			if (is_array($image)) {
				$transaction->add_file('image_logo', $image[0]);
				$transaction->add_file('image_thumb_logo', $image[1]);
			} elseif (is_string($image))
				$error['image_logo'] = $image;
		}
		
		if (isset($_FILES['image_extra'])) {
			$image = $this->upload_image('image_extra',
					bconf_get($BCONF, "*.common.store.image_extra.max_width"),
					bconf_get($BCONF, "*.common.store.image_extra.max_height"));
			if (is_array($image)) {
				$transaction->add_file('image_extra', $image[0]);
			} elseif (is_string($image))
				$error['image_extra'] = $image;
		}

		$info_text = preg_replace("/\r\n?|\n/", "<br />", $info_text);

		if (!empty($info_header))
			$transaction->add_data('info_header', $info_header);
		if (!empty($info_text))
			$transaction->add_data('info_text', $info_text);
		$transaction->add_data('bgcolor', $bgcolor);
		$transaction->add_data('textcolor', $textcolor);
		$transaction->add_data('address', $address);
		$transaction->add_data('postal_city', $postal_city);
		$transaction->add_data('zipcode', $zipcode);
		$transaction->add_data('phone', $phone);
		if (!empty($infopage_home))
			$transaction->add_data('infopage_home', $infopage_home);
		if (!empty($dellogo))
			$transaction->add_data('dellogo', $dellogo);
		if (!empty($delextra))
			$transaction->add_data('delextra', $delextra);


		$reply = $transaction->send_admin_command('stores');
		
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
		} else {
			$this->store_saved = lang("STORE_SAVED");
		}	
		return 'presentation';
	}

	function store_list_ads($nav_offset, $cs, $so) {
		global $BCONF;
		global $session;

		if (!is_null($so))
			$this->so = $so;
		
		$limit = bconf_get($BCONF, "*.common.store.search.max_limit");
		$max_page_count = bconf_get($BCONF, "*.common.store.search.max_page_count");

		if ($nav_offset < 1)
			$nav_offset = 1;
		$offset = ($nav_offset - 1) * $limit;

		/*run transaction */
		/* Prepare transaction */
		$transaction = new bTransaction();

		if ($cs == 'gallery') {
			$transaction->add_data('offset', 0);
			$transaction->add_data('state', 'deleted');
		} else {
			$transaction->add_data('offset', $offset);
			$transaction->add_data('state', $cs);
		}
		$transaction->add_data('sortby', "list_time DESC"); /* sort by anything*/
		$transaction->add_data('store_id', $this->store_id);
		$transaction->add_data('limit', $limit);
		$transaction->add_data('sortby', "list_time ".($this->so ? "ASC" : "DESC"));
		$transaction->auth_type = 'store';
		$reply = $transaction->send_admin_command('store_deletedads');

		bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "$offset lim:$limit has_gallery:1 store:{$this->store_id}", "handle_line", $gallery_data);
		$reply['gallery'] = intval(@$gallery_data['filtered'][0]);

		if ($reply[$cs] == 0)
			return 'main';

		$this->result_navigation($reply[$cs], $limit, $max_page_count, $nav_offset, $reply);
		
		if ($cs == 'gallery')
			$reply = array_merge($reply, $gallery_data);
		if (!empty($reply['list_time'])) {
			require_once('util.php');
			if (!is_array($reply['list_time']))
				$reply['list_time'] = array($reply['list_time']);

			foreach ($reply['list_time'] as $key => $value) {
				$unixtime = pgsql_mktime($value);
				if ($cs == "deleted") {
					$age = (time() - $unixtime) / (24 * 60 * 60);
					$reply['gracetime'][] = "" . round(bconf_get($BCONF, "*.common.store.grace_time") - $age);
				}
				$timestamp = substr($value, 0, 19);
				$timestamp = format_date($timestamp);
				$nicedate = split(';', $timestamp);
				$reply['nicedate'][] = $nicedate[0];
				$reply['nicetime'][] = $nicedate[1];
			}
		}

		$reply['offset'] = $offset == 0 ? min($reply[$cs], 1) : $offset;
		$reply['limit'] = min($offset + $limit, $reply[$cs]);
		$reply['qs'] = '&cs=' . $cs;
		$reply['cs'] = $cs;

		$reply['deleted_filtered'] = $reply['deleted'];
		$reply['unpaid_filtered'] = $reply['unpaid'];
		$reply['unverified_filtered'] = $reply['unverified'];
		$reply['pending_review_filtered'] = $reply['pending_review'];
		$reply['gallery_filtered'] = $reply['gallery'];
		$this->get_top_five_ads($reply, $this->stats_period, $this->store_id);
		$this->get_store_stats($reply, $this->stats_period, $this->store_id);

		/* get stats for each ad on deleted tab*/
		if ($cs == "deleted"){
			$this->get_ads_stats($reply, $this->stats_period, $this->store_id);
			$reply['is_admin'] = Cpanel::checkLogin() ? 1 : 0;
		}

		$this->page_title = lang('PAGE_STORE_TAB_' . strtoupper($cs));
		$this->page_name = lang('PAGE_STORE_TAB');

		$reply['current_page'] = 'store/list.html';
		$reply['voucher_balance'] = $this->get_voucher_balance();
		$reply['num_checked_ads'] = isset($cs) && $cs != "" ? count(@$this->checked_ads[$cs]) : count(@$this->checked_ads['published']);
		$reply['free_ads_count'] = $this->get_free_ads_count();

		$this->display_layout('store/main.html', $reply, array('checked' => $this->checked_ads));
		unset($this->passwd_saved);

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
	}

	function gallery_key_lookup($setting, $key, $data = null) {
		global $BCONF;

		$i = $data['current_line'];

		if ($key == 'parent')
			return bconf_get($BCONF, "*.cat.".$data['category'][$i].".parent");

		if (isset($data[$key][$i]))
			return $data[$key][$i];
	}


	function store_list($nav_offset, $stats_period, $fm, $so) {
		global $BCONF;
		global $session;

		if (!is_null($so))
			$this->so = $so;
		
		$store_id = $this->store_id;
		$deleted = array();

		if (empty($stats_period))
			$stats_period = 'day';
		
		$this->get_top_five_ads($data, $stats_period, $store_id);
		$this->get_store_stats($data, $stats_period, $store_id);

		$limit = bconf_get($BCONF, "*.common.store.search.max_limit");
		$max_page_count = bconf_get($BCONF, "*.common.store.search.max_page_count");

		if ($nav_offset < 1)
			$nav_offset = 1;
		$offset = ($nav_offset - 1) * $limit;

		/* Get nr of deleted ads */
		$transaction = new bTransaction();
		$transaction->add_data('store_id', $this->store_id);
		$transaction->add_data('offset', 0);
		$transaction->add_data('limit', $limit);
		$transaction->add_data('state', 'deleted');
		$transaction->add_data('sortby', "list_time DESC"); /* sort by anything*/
		if ($fm == 2) {
			/* Somewhat hacky, use master for up to date data. */
			$transaction->add_data('master', '1');
			$fm = 1;
		}
		$transaction->auth_type = 'store';
		$reply = $transaction->send_admin_command('store_deletedads');
		$data['voucher_balance'] = $this->get_voucher_balance();
		$data['free_ads_count'] = $this->get_free_ads_count();

		if (isset($reply['deleted'])) {
			$data['deleted_filtered'] = $reply['deleted'];
			$data['unpaid_filtered'] = $reply['unpaid'];
			$data['unverified_filtered'] = $reply['unverified'];
			$data['pending_review_filtered'] = $reply['pending_review'];
			if (isset($reply['ad_id'])) { 
				if (is_array($reply['ad_id']))
					$deleted = array_flip($reply['ad_id']);
				else
					$deleted = array($reply['ad_id'] => 1);
			}
		}
		
		/* Get ads for store */
		bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "$offset lim:$limit" . ($this->so ? " sort:+" : "") . " store:$store_id", "handle_line", $search_data);

		/* Setup array of ads that are gallery candidates */
		/*
			1. Doesn't have gallery.
			2. At least one image or video
			3. Gallery settings enabled (category, parent, type)
		*/
		$this->can_has_gallery = NULL;
		for($i = 0 ; $i < $search_data['lines'][0] ; ++$i) {

			if ($search_data['has_gallery'][$i] != "1" &&
			   (($search_data['image'][$i] != "") || ($search_data['video_name'][$i] != ""))) {

				$search_data['current_line'] = $i;
				get_settings(bconf_get($BCONF,"*.gallery_settings"), "gallery", "gallery_key_lookup",
					create_function('$s,$k,$v,$d', '$d[$k] = $v;'), $search_data);
		
				if (isset($search_data['enabled']) && $search_data['enabled']) {
					unset($search_data['enabled']);
					$this->can_has_gallery[] = $search_data['ad_id'][$i];
				}
			}
		}
		unset($search_data['current_line']);
		
		$data = array_merge($data, $search_data);

		/* get stats for each ad */
		$this->get_ads_stats($data, $stats_period, $store_id);

		$filtered = isset($data['filtered'][0]) ? $data['filtered'][0] : 0;
		$this->ads_filtered = $filtered;
		
		bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:0 has_gallery:1 store:$store_id", "handle_line", $gallery_data);
		$data['gallery_filtered'] = intval(@$gallery_data['filtered'][0]);

		if ($fm && !$filtered) {
			$cs = null;
			if (@$data['deleted_filtered'])
				$cs = 'deleted';
			elseif (@$data['unpaid_filtered'])
				$cs = 'unpaid';
			elseif (@$data['unverified_filtered'])
				$cs = 'unverified';
			elseif (@$data['pending_review_filtered'])
				$cs = 'pending_review';
			elseif (@$data['gallery_filtered'])
				$cs = 'pending_review';
			if ($cs)
				return array('list_ads', 'cs' => $cs);
		} else if (!$filtered)
			return 'main';

		$this->result_navigation($filtered, $limit, $max_page_count, $nav_offset, $data);

		$data['offset'] = $offset == 0 ? min($this->ads_filtered, 1) : $offset;
		$data['limit'] = min($offset + $limit, $filtered);
		$data['current_page'] = 'store/list.html';
		$data['is_admin'] = Cpanel::checkLogin() ? 1 : 0;
		$data['from_main'] = $fm;
		$data['num_checked_ads'] = isset($cs) && $cs != "" ? count($this->checked_ads[$cs]) : count(@$this->checked_ads['published']);
		$data['num_imported'] = $this->checked_ads['imported'];

		$this->page_title = lang('PAGE_STORE_TAB_PUBLISHED');
		$this->page_name = lang('PAGE_STORE_TAB');

		$this->display_layout('store/main.html', $data, array('checked' => $this->checked_ads, 
								      'deleted' => $deleted));
		unset($this->passwd_saved);

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
	}

	function store_voucher($offset, $amount) {
		global $BCONF;
		global $session;

		$this->page_title = lang('PAGE_STORE_VOUCHER');
		$this->page_name = lang('PAGE_STORE_VOUCHER');

		$limit = bconf_get($BCONF, "*.common.store.search.max_limit");
		$max_page_count = bconf_get($BCONF, "*.common.store.search.max_page_count");

		$transaction = new bTransaction();

		$transaction->auth_type = 'store';
		$transaction->add_data('store_id', $this->store_id);
                if(!empty($limit))
			$transaction->add_data('limit', $limit);
		if(!empty($offset))
			$transaction->add_data('offset', $offset);

                $data = $transaction->send_admin_command('search_voucher');
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
		} else {
			$total = intval($data['total']);

			$this->add_navigation($offset, $total, $limit, $max_page_count);

			if (isset($data['result']))
				ksort($data['result'], SORT_NUMERIC);
		}

		$this->display_layout('store/voucher.html', array('offset' => $offset, 'amount' => $amount), $data);

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
	}

  	function store_presentation($errors = NULL) {
		$transaction = new bTransaction();

		$transaction->add_data('id', $this->store_id);
		$transaction->add_data('offset', 0);
		$transaction->add_data('lim', 1);

		$data = $transaction->send_command('search_comp');
		$data = $this->handle_search_comp_reply($data);

		if (isset($data['info_text'])) {
			$data['info_text'] = preg_replace("/[\r\n]|</p>/i", "", $data['info_text']);
			$data['info_text'] = preg_replace("/<p>/i", "\n\n", $data['info_text']);
			$data['info_text'] = preg_replace("/<br ?\/?>/i", "\n", $data['info_text']);
		}

		$this->page_title = lang('PAGE_STORE_SETTINGS');
		$this->page_name = lang('PAGE_STORE_SETTINGS');

		if (!@$data['passwd'])
			$data['current_page'] = 'store/newpasswd.html';
		else
			$data['current_page'] = 'store/presentation.html';
		$this->display_layout('store/main.html', $data);

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');

		unset($this->store_saved);
		unset($this->passwd_saved);
	}

	function store_main($id, $sp = 0, $email = NULL, $ms = 0, $list_id = 0, $cmd = NULL, $ht = NULL) {
		global $BCONF;

		if (isset($ht) && $ht)
			$this->ht = $ht;

		if ($this->check_logged_in() && !$this->store_id) {
			$this->store_id = $_SESSION['global']['store_id'];
			if (!$this->store_id)
				return $this->store_logout(bconf_get($BCONF, '*.common.base_url.li') . '/li?ca=' . $this->caller->to_string() . '&fk=1');
		}
		if ($list_id > 0)
			$this->list_id = $list_id;
		if ($cmd) 
			$this->cmd = $cmd;
		if ($sp == 1) {
			$this->store_id = $id;
			return 'formsendpass';
		} elseif ($sp == 2 && $this->check_logged_in()) {
			return 'presentation';
		} else if ($sp == 3 && $this->check_logged_in()) {
			return 'voucher';
		} else if ($ms == 1) {
			$this->store_id = $id;
			return 'formmailstore';	
		} else if ($this->check_logged_in() && (!$id || $id == $this->store_id)) {
			$this->lookup_store($this->store_id);
			$this->page_title = lang('PAGE_STORE_TITLE') . ": $this->c_name";
			$this->page_name = lang('PAGE_STORE_NAME') . ": $this->c_name";

			$this->import_partner = $this->c_import_partner;

			/* Redirect user to a specific ad */
			if ($this->list_id) {
				if ($this->ht)
					$url_append = '&ht='.$this->ht;
				else
					$url_append = '';
				if ($this->cmd != "") {
					$url = bconf_get($BCONF, '*.common.base_url.ai)') . '/ai?ca=' . $this->caller->to_string() . '&id=' . $this->list_id . '&cmd='. $this->cmd . $url_append;
				} else {
					$url = bconf_get($BCONF, '*.common.base_url.ai') . '/ai?ca='.$this->caller->to_string() . '&id=' . $this->list_id . $url_append;
				}
				header("Location: $url");
				unset($this->cmd);
				unset($this->list_id);
				return 'STORE_INSTANCE';
			}
			return array('list', 'fm' => 1);
		} elseif ($id && Cpanel::checkLogin() && Cpanel::hasPriv('stores')) {
			/* Buy a store token for this store. */
			$transaction = new bTransaction();
			$transaction->add_data('store_id', $id);
			$reply = $transaction->send_admin_command('buy_store_token');

			if (!$transaction->has_error()) {
				$this->store_id = $id;
				$this->lookup_store($this->store_id);
				$this->page_title = lang('PAGE_STORE_TITLE') . ": $this->c_name";
				$this->page_name = lang('PAGE_STORE_NAME') . ": $this->c_name";

				$this->import_partner = $this->c_import_partner;

				$transaction->auth_type = 'store';
				$transaction->store_token('store_token', $reply['store_token']);

				$_SESSION['global']['store_username'] = $_SESSION['controlpanel']['admin']['username'];
				$_SESSION['global']['store_id'] = $id;
				$_SESSION['global']['store_name'] = $this->c_name;
				setcookie('sli', '1', 0, '/', bconf_get($BCONF, '*.common.session.auth.cookiedomain'));

				return array('list', 'fm' => 1);
			}
		} else {
			if (isset($id))
				$this->login_id = $id;
			if ($this->login_id)
				return 'login';
		}
		return $this->store_logout(bconf_get($BCONF, '*.common.base_url.li') . '/li?ca=' . $this->caller->to_string() . '&fk=1');
	}

	function result_navigation($total, $limit, $max_page_count, $nav_offset, &$data) {
		$page_count = (int)ceil($total / $limit);

		if ($page_count <= 1)
			return;

		if ($nav_offset > 1)
			$data['nav_prev_offset'] = $nav_offset - 1;
		if ($nav_offset < $page_count) {
			$data['nav_next_offset'] = $nav_offset + 1;
			$data['nav_last_offset'] = $page_count;
		}
		if ($nav_offset == $page_count)
			$data['nav_first_offset'] = 1;

		if ($nav_offset >= $max_page_count)
			$i = $nav_offset - 1;
		else
			$i = 1;
		for ($j = 0; $j < $max_page_count; $j++) {
			if ($i > $page_count)
				break;
			$data['nav_offset'][] = $i;
			$data['nav_selected'][] = $i == $nav_offset ? 1 : 0;
			$i++;
		}
	}

	function store_login() {
		global $BCONF;

		if(!$this->login_id) {
			header("Location: ".bconf_get($BCONF, '*.common.base_url.blocket')."/");
			return;
		}
		$store_name = $this->lookup_name($this->login_id);
		$this->page_title = lang('PAGE_STORE_LOGIN');
		$this->page_name = lang('PAGE_STORE_LOGIN');

		/* If user is here we can safely assume that user is not logged in. Remove logged in cookie. */
		setcookie('sli', NULL, 0, '/', bconf_get($BCONF, '*.common.session.cookiedomain'));

		$this->display_layout('store/login.html');

		$this->page_title = lang('PAGE_STORE_TITLE') . ": $store_name";
		$this->page_name = lang('PAGE_STORE_NAME') . ": $store_name";

		return;
	}

	function store_logout($url) {
		global $BCONF;

		$this->deauthenticate();

		$this->store_cleanup('logout');

		/* XXX stricter url check? */
		if (empty($url) || strstr($url, '/store/'))
			$url = bconf_get($BCONF, '*.common.base_url.li') . '/li?ca=' . $this->caller->to_string() . '&id=' . $this->store_id;
		header("Location: $url");
		return 'FINISH';
	}

  	function deauthenticate() {
		global $BCONF;

		setcookie('sli', NULL, 0, '/', bconf_get($BCONF, '*.common.session.cookiedomain'));

		unset($_SESSION['global']['store_username']);
		unset($_SESSION['global']['store_id']);
		unset($_SESSION['global']['store_name']);
		unset($_SESSION['instance_data']['blocket_newad']); 

		parent::deauthenticate();
	}

	function store_verify_login($username, $passwd) {
		global $BCONF;

		$username = trim($username);
		$passwd = trim($passwd);

		$transaction = new bTransaction();
		$transaction->add_data('store_id', $this->login_id);
		$reply = $transaction->send_command('store_login_info');

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'login';
		}

		$salt = $reply['salt'];

		/* transaction */
		$transaction->reset();
		$transaction->add_client_info();
		$transaction->auth_type = 'store';
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('username', $username);
		$transaction->add_data('passwd', $salt . sha1($salt . $passwd));
		$transaction->add_data('auth_type', 'store');
		$transaction->add_data('auth_resource', $this->login_id);

		$reply = $transaction->send_command('authenticate');
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'login';
		}
		/* User has logged in */

		$_SESSION['global']['store_username'] = $username;
		$_SESSION['global']['store_id'] = $this->login_id;
		$_SESSION['global']['store_name'] = $this->lookup_name($this->login_id);


		/* Set logged in cookie */
		setcookie('sli', '1', 0, '/', bconf_get($BCONF, '*.common.session.cookiedomain'));

		$this->store_id = $this->login_id;

		return 'main';
	}

	function store_form_mailstore() {
		$this->page_title = lang('PAGE_STORE_REPLY');
		$this->display_layout('store/storereply.html');
	}

	function store_mailstore($name, $email, $phone, $cc, $body) {
		$this->ar_name = $name;
		$this->ar_email = $email;
		$this->ar_phone = $phone;
		$this->ar_adreply_body = $body;
		$this->ar_cc = $cc;

		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('id', $this->store_id);
		$transaction->add_data('storereply', 1);
		$transaction->add_data('name', $name);
		$transaction->add_data('email', $email);
		if (!empty($phone))
			$transaction->add_data('phone', $phone);
		if (isset($cc) && $cc == "1")
			$transaction->add_data('cc', $cc);
		$transaction->add_data('adreply_body', clean_and_trim($body));

		$reply = $transaction->send_command("adreply");

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'formmailstore';
		}

		return 'mail_sent';
	}

	function store_mail_sent() {
		$this->ar_name = null;
		$this->ar_email = null;
		$this->ar_phone = null;
		$this->ar_adreply_body = null;
		$this->ar_cc = null;

		$this->display_layout('store/sentstoremail.html');
	}

	function store_form_sendpass($store_id) {
		if ($store_id)
			$this->store_id = $store_id;
		if (!$this->store_id)
			return $this->store_logout(bconf_get($BCONF, '*.common.base_url.li') . '/li?ca=' . $this->caller->to_string() . '&fk=1');

		$this->page_title = lang('PAGE_STORE_SENDPASS');
		$this->page_name = lang('PAGE_STORE_SENDPASS');

		$this->display_layout('store/form_sendpass.html');

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
	}

	function store_sendpass($email) {
		$transaction = new bTransaction();
		$transaction->add_data('store_id', $this->store_id);
		$transaction->add_data('email', $email);

		$reply = $transaction->send_command('store_sendpass', true, true);
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			$this->ar_email = $email;
			return 'formsendpass';
		}

		return 'pass_sent';
	}

	function store_pass_sent() {
		$this->page_title = lang('PAGE_STORE_PASS_SENT_TITLE');
		$this->page_name = lang('PAGE_STORE_FORGOTTEN_PASS');

		$this->display_layout('store/sendpass.html');

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');
	}

	function store_fillvoucher($amount, $iagree) {
		$errors = array();

		if (!$iagree)
			$errors['iagree'] = 'ERROR_IAGREE_MISSING';

		$transaction = new bTransaction();
		$transaction->auth_type = 'store';
		$transaction->add_data('store_id', $this->store_id);
		if ($amount)
			$transaction->add_data('amount', $amount);

		$reply = $transaction->send_admin_command('fillvoucher', empty($errors));
		if ($transaction->has_error() || $errors) {
			$this->set_errors($transaction->get_errors() + $errors);
			return array('voucher', 'amount' => $amount);
		}
		$this->order_id = $reply['order_id'];
		$this->pay_amount = $amount;
		$this->pay_for = 'voucher';
		return array('payex_form', 'orderid' => $reply['order_id'], 'price' => $amount, 'error_state' => 'voucher');
	}

	/*
		payex cancel url points here, we simply pass the user to the final 'cancel' page destination for
		whatever is being paid for.
	*/
	function store_cancel_payment() {
		switch($this->pay_for) {
			case 'voucher':
				return array('voucher');
			case 'bump':
				return array('list');
		}	
	}

	function store_payex_form($orderid, $price, $error_state) {
		global $BCONF;

		if(!$price)
			return array($error_state);

		$transaction = new bTransaction();
		$transaction->add_data('order_id', $orderid);
		$reply = $transaction->send_command('get_unique_order_id', true, true);
		if ($reply['status'] != 'TRANS_OK')
			return array($error_state);
		$uniq_order_id  = $reply['unique_order_id'];

		$response_url = bconf_get($BCONF, "*.common.payex.rurl");
		$success_url = bconf_get($BCONF, "*.common.base_url.store") . '/store/payex_result_s/' . $this->instance_id;
		$failure_url = bconf_get($BCONF, "*.common.base_url.store") . '/store/payex_result_f/' . $this->instance_id;
		$cancel_url = bconf_get($BCONF, "*.common.base_url.store") . '/store/cancel_payment/' . $this->instance_id;

		$merchant_id = bconf_get($BCONF, "*.common.payex.merchantid");
		$email = bconf_get($BCONF, "*.common.payex.email");
		$currency = bconf_get($BCONF, "*.common.payex.currency");
		$merchant_key = bconf_get($BCONF, "*.common.payex.merchantkey");

		$paid = $price * 100;

		$data = array();
		$data['payex_signature'] = sha1("$merchant_id&$uniq_order_id&$email&$response_url&$success_url&$failure_url&$cancel_url&$paid&$currency&$merchant_key");

		/* Total amount paid */
		$data['paid'] = $paid;

		/* Items */
		$data['isku'][] = $this->pay_for; // 'voucher', 'bump' ...
		$data['iprice'][] = (int)($paid * 0.8);
		$data['ivat'][] = (int)($paid * 0.2);

		$data['uniq_order_id'] = $uniq_order_id;
		$data['success_url'] = $success_url;
		$data['failure_url'] = $failure_url;
		$data['cancel_url'] = $cancel_url;

		$this->state_progress = 'payex_cancel';
		/* Display payex form */
		$this->display_layout('common/payex_form.html', $data);
	}

	// $style should become an enum.
	function store_cleanup($style) {
		switch($style) {
			case 'logout':
			case 'full':
				unset($this->checked_ads);
				unset($this->ads_to_bump);
				unset($this->pay_amount);
				unset($this->pay_for);
				unset($this->order_id);
				break;

			case 'paycancel':
			case 'payfail':
				unset($this->ads_to_bump);
				unset($this->pay_amount);
				unset($this->pay_for);
				unset($this->order_id);
				break;

			default:
				die_with_template(__FILE__, log_string()."Unknown store_cleanup '".$style."'");
		}
	}

	function store_payex_result_s($merchantid, $orderid, $status) {
		$err = payex_log_result($merchantid, $orderid, $this->order_id, $status, 's');
		$pay_for = $this->pay_for;
		if ($err) {
			$this->set_errors($err);
			switch($this->pay_for) {
				case 'bump':
					return array('list');
					break;
			}
			return array('voucher');
		}

		$this->store_cleanup('full');

		switch($pay_for) {
			case 'bump':
				return array('bump_done');
				break;
		}

		return array('voucher_filled');
	}

	function store_payex_result_f($merchantid, $orderid, $status) {
		$this->store_cleanup('payfail');

		$err = payex_log_result($merchantid, $orderid, $this->order_id, $status, 'f');
		$this->set_errors($err);

		switch($this->pay_for) {
			case 'bump':
				return array('list');
				break;
		}
		return array('voucher');
	}

	function store_payex_cancel($merchantid, $orderid) {
		// Cancel cleanup.
		$this->store_cleanup('paycancel');

		switch($this->pay_for) {
			case 'bump':
				return array('list');
		}
		return array('voucher');
	}

	function store_voucher_filled() {
		$data['voucher_balance'] = $this->get_voucher_balance();
		$this->page_title = lang('PAGE_STORE_VOUCHER_PAGE_TITLE');
		$this->page_name = lang('PAGE_STORE_VOUCHER_PAGE_NAME');


		$this->display_layout('store/voucher_filled.html', $data);

		$this->page_title = lang('PAGE_STORE_TITLE');
		$this->page_name = lang('PAGE_STORE_NAME');

	}

	function store_bump_done() {
		$data['voucher_balance'] = $this->get_voucher_balance();

		$this->display_layout('store/bump_done.html', $data);

	}

	function get_store_stats(&$data, $stats_period, $store_id) {
		global $BCONF;
		$this->stats_period = $stats_period;
		$stats = array();
		bsearch_search_vtree(bconf_get($BCONF, "*.common.statpoints"), "event:VIEW id:store_$store_id", "handle_line", $stats);
		@$data['stats_store_day'] = $stats['day'];
		@$data['stats_store_week'] = $stats['week'];
		@$data['stats_store_4weeks'] = $stats['4weeks'];
	}

	function get_ads_stats(&$data, $stats_period, $store_id) {
		global $BCONF;
		$stats = array();
		bsearch_search_vtree(bconf_get($BCONF, "*.common.statpoints"), "event:VIEW parent:store_$store_id", "handle_line", $stats);

		/* Populate $data with stats */
		$data['stats_total'] = array();
		$data['stats_day'] = array();

		if (!isset($data['list_id']))
			return;
		if (!is_array($data['list_id']))
		    $data['list_id'] = array($data['list_id']);

		for($i = 0; $i < sizeof($data['list_id']); $i++) {
			$data['stats_total'][$i] = "-";
			$data['stats_day'][$i] = "-";
			$data['stats_week'][$i] = "-";
			$data['stats_4weeks'][$i] = "-";
			for ($j = 0; $j < sizeof(@$stats['id']); $j++) {
				if ($data['list_id'][$i] == $stats['id'][$j]) {
					$data['stats_total'][$i] = $stats['total'][$j];
					$data['stats_day'][$i] = $stats['day'][$j];
					$data['stats_week'][$i] = $stats['week'][$j];
					$data['stats_4weeks'][$i] = $stats['4weeks'][$j];
					@$data['stats_ads_day'] += $stats['day'][$j];
					@$data['stats_ads_week'] += $stats['week'][$j];
					@$data['stats_ads_4weeks'] += $stats['4weeks'][$j];
				}
			}
		}
	}
	function get_top_five_ads(&$data, $stats_period, $store_id) {
		global $BCONF;
		bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 hdrs:1 store:$store_id", "handle_line", $data_local);
		$this->get_ads_stats($data_local, $stats_period, $store_id);

		$this->multisort_result($data_local, "stats_$stats_period", "SORT_DESC");

		foreach ($data_local as $key => $value)
			$data["s_" . $key] = is_array($value) ? array_slice($value, 0, 5) : $value;
	}

	function multisort_result(&$data, $sortby_cmd, $sortby_order) {
		if (!isset($data[$sortby_cmd]))
			return;

		/* since we can not specify sort order to search, reorder all arrays in result. */
 		$arrays = array(&$data[$sortby_cmd], $sortby_order);
		foreach ($data as $key=>$value)
			if (is_array($value) && sizeof(@$data['subject']) == sizeof($value))
				$arrays[] = &$data[$key];
		if (!empty($sortby_cmd))
			call_user_func_array('array_multisort', $arrays);
	}

	function auth_type() {
		return 'store';
	}

	function lookup_name($store_id) {
		global $BCONF;
		bsearch_search_vtree(bconf_get($BCONF, "*.common.csearch"), "0 id:$store_id", "get_csearch_name", $data);
		return $data;
	}

	function lookup_store($store_id) {
		global $BCONF;
		bsearch_search_vtree(bconf_get($BCONF, "*.common.csearch"), "0 id:$store_id", "get_csearch_store", $this);
	}

	function get_voucher_balance() {
		/* Get voucher balance */
		$transaction = new bTransaction();
		if (Cpanel::checkLogin() && Cpanel::hasPriv("stores")) {
			$transaction->auth_type = 'admin';
			$transaction->add_data('store_id', $this->store_id);
			$reply = $transaction->send_admin_command('store_voucher_balance');
			$transaction->handle_reply();
		} else {
			$transaction->auth_type = 'store';
			$reply = $transaction->send_admin_command('store_voucher_balance');
		}
		return isset($reply['voucher_balance']) ? $reply['voucher_balance'] : 0;
	}

	function get_free_ads_count() {
		$transaction = new bTransaction();

		$transaction->add_data('id', $this->store_id);
		$transaction->add_data('offset', 0);
		$transaction->add_data('lim', 1);

		$data = $transaction->send_command('search_comp');
		$data = $this->handle_search_comp_reply($data);

		return isset($data['free_ads']) ? $data['free_ads'] : 0;
	}

	function f_username($val) {
		return (string)$val;
	}
	function f_passwd($val) {
		return (string)$val;
	}
	function f_id($val) {
		if (isset($val))
			return intval($val);
	}
	function f_url($val) {
		$val = html_entity_decode($val);

		if (!empty($val) && !preg_match('/http:\/\//', $val))
			$val = 'http://' . $val;

		return $val;
	}
	function f_o($val) {
		return intval($val);
	}
	function f_name($val) {
		$val = preg_replace('/[^[a-zA-Z0-9]������&\.,:; -]/i', '', $val);
		return (string)$val;
	}
	function f_info_header($val) {
		$val = preg_replace('/[^[a-zA-Z0-9]������&!,\.;:\?\(\)\'\/\\ -]/i', '', $val);
		return (string)$val;
	}
	function f_email($val) {
		$val = preg_replace('/[^[a-zA-Z0-9]@._-]/i', '', $val);
		return (string)$val;
	}
	function f_info_text($val) {
		return strip_tags($val);
	}
	function f_adreply_body($val) {
		return (string)$val;
	}
	function f_address($val) {
		$val = preg_replace('/[^[a-zA-Z0-9]������&\.,:; -]/i', '', $val);
		return (string)$val;
	}
	function f_postal_city($val) {
		$val = preg_replace('/[^[a-zA-Z0-9]������ ]/i', '', $val);
		return (string)$val;
	}
	function f_zipcode($val) {
		$val = preg_replace('/[^[a-zA-Z0-9] ]/', '', $val);
		return (string)$val;
	}
	function f_phone($val) {
		$val = preg_replace('/[^[a-zA-Z0-9] +-]/', '', $val);
		return (string)$val;
	}
	function f_stats_period($val) {
		$val = preg_match('/^(day|week|4weeks)$/', $val) ? $val : '';
		return (string)$val;
	}
	function f_cs($val) {
		$val = preg_match('/^(unpaid|unverified|pending_review|deleted|gallery)$/', $val) ? $val : '';
		return (string)$val;
	}
	function f_order_id($val) {
		if (preg_match('^/[0-9a]+$/', $val))
			return substr($val, 0, strpos($val, 'a'));
		return null;
	}
	function f_numeric($val) {
		if (preg_match('/^[0-9]+$/', $val))
			return (string)$val;
		return null;
	}

	function f_pay_type($val) {
		if (in_array($val, array('card', 'voucher')))
			return $val;
		return null;
	}

	function f_pay_for($val) {
		if (in_array($val, array('voucher', 'bump')))
			return $val;
		return null;
	}

	function f_error_state($val) {
		if (in_array($val, array('voucher', 'list')))
			return $val;
		return null;
	}

	/* shamelessly stolen from blocket_newad */
	function f_cmd($val) {
		if (preg_match('/^(delete|edit|renew|gallery)$/', $val))
			return $val;
		return null;
	}


}

function get_csearch_name(&$data, $key, $value) {
	if ($key == "c_name") {
		$data = $value;
	}
}

function get_csearch_store(&$data, $key, $value) {
	$data->$key = $value;
}
?>
