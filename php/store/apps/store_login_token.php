<?php

openlog("store_login_token", LOG_ODELAY, LOG_LOCAL0);
require_once("blocket_application.php");
require_once("bAd.php");
require_once("blocket_store.php");

class store_login_token extends blocket_application {
	function store_login_token() {
		$token = substr($_SERVER['PATH_INFO'], 1);
		if ($token)
			$this->init_data['t'] = $token;

		$this->init('load', 1);
	}

	function get_state($name) {
		switch ($name) {
		case 'load':
			return array('function' => 'load',
					'params' => array('t' => 'f_token'),
					'method' => 'get');
		case 'error':
			return array('function' => 'error',
					'method' => 'get');

		case 'passwd':
			return array('function' => 'passwd',
					'method' => 'get');

		case 'choice':
			return array('function' => 'choice',
					'method' => 'get');
		}
	}

	function load($t) {
		global $BCONF;
		global $session;

		if (!$t) {
			return array('error');
		}

		$storeobj = new blocket_store(false);
		if ($session->load_active_instance($storeobj, get_class($storeobj), "0", false)) {
			/* Logout any store currently logged in. */
			$session->destroy_instance($storeobj);
		}

		$transaction = new bTransaction();
		$transaction->auth_type = 'store';
		$transaction->add_data('login_token', $t);
		$reply = $transaction->send_command('store_login_token', true, true);
		if ($transaction->has_error()) {
			$this->token = $t;
			return array('error');
		}

                $_SESSION['global']['store_username'] = $reply['email'];
                $_SESSION['global']['store_id'] = $reply['store_id'];
                unset($_SESSION['global']['store_noads']);

		$store = bsearch_search_vtree(bconf_get($BCONF, "*.common.csearch"), "0 id:{$reply['store_id']}");
		if ($store)
			$_SESSION['global']['store_name'] = $store['c_name'];

                /* Set logged in cookie */
                setcookie('sli', '1', 0, '/', bconf_get($BCONF, '*.common.session.cookiedomain'));

		session_write_close();

		$newads = @$_SESSION['instance_data']['blocket_newad'];
		if ($newads) {
			foreach ($newads as $iid => $newad) {
				if (is_object(@$newad['ad']) && $newad['ad']->store) {
					header("Location: " . bconf_get($BCONF, "*.common.base_url.ai") . '/ai/preview/' . $iid . '?cl=1');
					return 'FINISH';
				}
			}
		}

		if (!$reply['has_passwd'])
			header("Location: " . bconf_get($BCONF, "*.common.base_url.store") . '/store/main/0?sp=2');
		else
			header("Location: " . bconf_get($BCONF, "*.common.base_url.store") . '/store/main/0');
		return 'FINISH';
	}

	function error() {
		$this->page_title = lang('PAGE_STORE_LOGIN_TOKEN_ERROR_TITLE');
		$this->page_name = lang('PAGE_STORE_LOGIN_TOKEN_ERROR');

		$this->display_layout('store/login_token_error.html');
	}

	function passwd() {
		$this->display_layout('store/login_token_passwd.html');
	}

	function f_token($val) {
		if (preg_match('/^[0-9a-z]{0,24}/', $val))
			return $val;
		return null;
	}
}

$app = new store_login_token();
$app->run_fsm();
?>
