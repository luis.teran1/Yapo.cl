<?php
/**
 * Applicationused to edit a store
 */
require_once('autoload_lib.php');
require_once('bImage.php');
require_once('init.php');
require_once('payment_common.php');
require_once('util.php');

openlog("store_edit", LOG_ODELAY, LOG_LOCAL0);

use Yapo\Cpanel;

class store_edit extends common_payment_application {

	var $account_session;
	var $account_id;
	var $data;
	var $store_editable = 1;
	var $store_prefix;
	var $cpanel_email;
	var $is_admin = 0;
	var $administrator ="";
	var $store_type;
	var $date_start;
	var $date_end;

	private $cart_mngr;
	private $trans;


	function store_edit($init = true) {
		if (!$init) {
			return;
		}
		global $BCONF;
		$this->page_title = lang("PAGE_EDIT_STORE_TITLE");
		$this->page_name= lang("PAGE_STORE_NAME");
		$this->headstyles = bconf_get($BCONF,"*.common.store_edit.headstyles");
		$this->store_prefix = bconf_get($BCONF,"*.common.base_url.blocket");
		$this->store_prefix .= "/".strtolower(lang("STORES"))."/";
		$this->cart_mngr = new RedisMultiproductManager();

		if(isset($_POST['TBK_ID_SESION']) && isset($_POST['TBK_ORDEN_COMPRA'])){
			$this->tbk['TBK_ID_SESION']  = $_POST['TBK_ID_SESION'];
			$this->tbk['TBK_ORDEN_COMPRA'] = $_POST['TBK_ORDEN_COMPRA'];
		}

		$this->application_name = "store_edit";

		$this->account_session = new AccountSession();
		$this->special_conditions_email();

		$this->init('load',1);
		$this->add_volatile_vars(array('account_session',));

		if(substr_count($_SERVER['REQUEST_URI'],'build_url') == 0){

			if (Cpanel::checkLogin() &&  Cpanel::hasPriv('Stores.editStore') ){
				$this->is_admin = 1;
				$this->administrator = $_SESSION['controlpanel']['admin']['username'];
				if(isset($_REQUEST['cpanel_email'])){
					if($this->account_session->is_logged() && $this->account_session->get_param('email') != $_REQUEST['cpanel_email']){
						$this->account_session->expire();
					}

					if(!$this->account_session->loginSpecial($_REQUEST['cpanel_email'])){
						echo $_REQUEST['cpanel_email'].'No es posible cargar la cuenta';
						exit(0);
					}else {
						$this->cpanel_email = $_REQUEST['cpanel_email'];
					}
				}else if(!$this->account_session->is_logged() || ($this->account_session->is_logged() && $this->account_session->get_param('email') != $this->cpanel_email)){
						$this->account_session->expire();
						echo 'Acci�n no permitida.';
						exit(0);
				}
			}

			if(!$this->account_session->is_logged()){
				$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
				header('Location: ' .$login );
				exit(0);
			}
		}

		$this->trans = new bTransaction();
		$this->headscripts = array("/tjs/arrays_v2.js", "store_edition.js");
	}



	function special_conditions_email(){

		if(isset($_REQUEST['e']) && isset($_REQUEST['h'])){
			$trans = new bTransaction();
			$trans->add_data('email',$_REQUEST['e']);
			$reply = $trans->send_command('load_store');

			if($trans->has_error(true)) {
				$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
				header('Location: ' .$login );
				exit(0);
			}
			$hashval= sha1($reply["store_id"].$reply["date_start"]);

			if($hashval == $_REQUEST['h']) {
				if(!$this->account_session->loginSpecial($_REQUEST['e'])){
					$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
					header('Location: ' .$login );
					exit(0);
				}
			}else{
				exit(0);
				$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
				header('Location: ' .$login );
				exit(0);
			}
		}
	}

	function get_state($statename){
		switch ($statename) {
		case 'load':
			return array(
				'function' => 'edit_store_load',
				'params' => array(),
				'method' => 'get'
			);
		case 'form':
			return array(
				'function' => 'edit_store_form',
				'params' => array(),
				'method' => 'get'
			);
		case 'submit':
			return array(
				'function' => 'edit_store_submit',
				'params' => array(
					"store_name" => "f_name",
					"store_info_text" => "f_string",
					"store_region" => "f_integer",
					"store_commune" => "f_string",
					"store_address" => "f_string",
					"store_address_number" => "f_string",
					"store_phone" => "f_phone",
					"store_website_url" => "f_string",
					"store_hide_phone" => "f_bool",
					"main_image" => "f_string",
					"main_image_offset" => "f_integer",
					"logo_image" => "f_string",
					"default_bg" => "f_integer"
				),
				'method' => "post"
			);
		case 'change_status':
			return array(
				'function' => "edit_store_change_status",
				'params' => array(
					"status" => "f_string"),
				'method' => "get"
			);
		case 'upload_image':
			return array(
				'function' => "edit_store_upload_image",
				'params' => array('image_type' => "f_string"),
				'method' => "both"
			);
		case 'build_url':
			return array(
				'function' => 'edit_store_url',
				'params' => array("name" => "f_name", "id" => "f_integer"),
				'method' => 'get'
			);
		case 'error':
			return array(
				'function' => "edit_store_error",
				'params' => array(),
				'method' => "get"
			);

		/* PAYMENT STUFF */
		case 'validate':
			return array(
				'function' => 'edit_store_payment_validate',
				'params' => array('document', 'name', 'lob', 'rut', 'address', 'region', 'communes', 'contact', 'email','prod'),
				'method' => 'post',
				'allowed_states' => array('validate', 'form'));
		/*CONTROL PANEL EDITION*/
		case 'cp_success':
			return array(
				'function' => 'edit_cp_success',
				'params' => array(),
				'method' => 'get'
			);
		}
	}

	function edit_cp_success() {
		$this->account_session->expire();
		unset($this->data['error']);
		$this->display_simple('controlpanel/stores/edit_store_success.html');
	}

	function user_allowed() {
		$this->account_session = new AccountSession();
		$this->account_session->reload_account_info();
		$is_logged = $this->account_session->is_logged();
		$store_id = $this->account_session->get_param("store_id");
		$has_store = !empty($store_id);
		return ($is_logged && $has_store);
	}

	function edit_store_load() {
		global $BCONF;
		if (!$this->user_allowed() ) {
			$this->data = array(
				"has_store" => 0
			);
			return 'error';
		}

		$trans = new bTransaction();
		$trans->add_data('store_id', $this->account_session->get_param('store_id'));
		$reply = $trans->send_command('load_store');

		if($trans->has_error(true)) {
			return 'error';
		}

		// pre-populate store data with account data
		if($reply["store_status"] == 'inactive') {
			$reply["region"] = $this->account_session->get_param('region');
			$reply["commune"] = $this->account_session->get_param('commune');
			$reply["address"] = $this->account_session->get_param('address');
			$reply["phone"] = $this->account_session->get_param('phone');
			$reply["url"] = Cpanel::slugify($reply["name"]);
		}

		$default_image_prefix = bconf_get($BCONF, "*.common.stores.default_image_prefix");
		bLogger::logDebug(__METHOD__, "default image prefix" . $default_image_prefix . " " . $reply['main_image']);
		$default_bg = NULL;

		// extract stock image id when the store is using one of them
		if (strpos($reply['main_image'], $default_image_prefix) === 0) {
			bLogger::logDebug(__METHOD__, "default image found " . $reply['main_image']);
			$default_bg = substr($reply['main_image'], strlen($default_image_prefix));
		}

		$days_remaining = $this->days_until($reply['date_end']);

		$this->data['store_name'] = $reply['name'];
		$this->data['store_info_text'] = $reply['info_text'];
		$this->data['store_region'] = $reply['region'];
		$this->data['store_commune'] = $reply['commune'];
		$this->data['store_address'] = $reply['address'];
		$this->data['store_address_number'] = $reply['address_number'];
		$this->data['store_phone'] = $reply['phone'];
		//$this->data['date_start'] = $reply['date_start'];
		$this->date_start = $reply['date_start'];
		//$this->data['date_end'] = $reply['date_end'];
		$this->date_end = $reply['date_end'];
		//$this->data['store_type'] = $reply['store_type'];
		$this->store_type = $reply['store_type'];
		$this->data['store_website_url'] = $reply['website_url'];
		$this->data['store_hide_phone'] = $reply['hide_phone'] == 't' ? 1 : 0;
		$this->data['logo_image'] = $reply['logo_image'];
		$this->data['main_image'] = $reply['main_image'];
		$this->data['store_url'] = $this->store_prefix.$reply['url'];
		$this->data['main_image_offset'] = $reply['main_image_offset'];
		$this->data['store_status'] = $reply['store_status'];
		$this->data['days_remaining'] = $days_remaining;
		$this->data['is_admin'] = $this->is_admin;
		$this->data['administrator'] = $this->administrator;

		if ($this->account_session->is_logged() && !$this->has_error()) {
			$this->data['account_logged_in'] = true;
			$this->data['name'] = $this->account_session->get_param('name');
			if($this->account_session->get_param('is_company') == 't')
				$this->data['document'] = 2;
			$this->data['phone'] = $this->account_session->get_param('phone');
			$this->data['rut'] = $this->account_session->get_param('rut');
			$this->data['contact'] = $this->account_session->get_param('contact');
			$this->data['lob'] = $this->account_session->get_param('lob');
			$this->data['address'] = $this->account_session->get_param('address');
			$this->data['region'] = $this->account_session->get_param('region');
			$this->data['communes'] = $this->account_session->get_param('commune');
		}

		$prefix = bconf_get($BCONF, "*.common.stores.default_image_prefix");

		/* by default the store comes with the "other" stock main image selected */
		if (strpos($reply["main_image"], $prefix) === 0) {
			$stock_img_id = substr($reply['main_image'], strlen($prefix));
			$this->data['default_bg'] = $stock_img_id;
		}

		if(!in_array($reply["store_status"], array("active", "inactive", "user_deactivated"))) {
			bLogger::logDebug(__METHOD__, "store not editable");
			$this->data["store_editable"] = 0;
			$this->store_editable = 0;
		}

		$this->has_loaded = true;
		return 'form';
	}

	function edit_store_form() {
		if (!empty($this->tbk['TBK_ORDEN_COMPRA'])) {
			$this->data['show_error_message'] = str_replace("#order_id#",$this->tbk['TBK_ORDEN_COMPRA'], $this->get_transaction_fail_html_code()); 
			$this->data['error'] = 'TBK_ORDEN_COMPRA_NOT_EMPTY';
			$this->data['show_error_title'] =  "ERROR_SESSION_EXPIRED_OOPS";
			$this->data['error_order_id'] =  $this->tbk['TBK_ORDEN_COMPRA'];
			$this->data['err_in_payment'] =  "1";
			unset($this->tbk);
		}
		if($this->has_loaded != true)
			return 'load';
		if(!$this->account_session->is_logged()){
			$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
			header('Location: ' .$login );
			exit(0);
		}
		$s_cart = $this->cart_mngr->list_all($this->account_session->get_param('email'));
		$this->data['s_cart_number'] = $s_cart['count'];
		$this->display_layout('stores/edit.html', $this->data, null, null, 'mobile_select');
	}


	function edit_store_submit($store_name, $store_info_text, $store_region, $store_commune, $store_address, $store_address_number, $store_phone, $store_website_url, $store_hide_phone, $main_image, $main_image_offset, $logo_image, $default_bg) {
		global $BCONF;
		if (!$this->user_allowed()) {
			$this->error = "ACCOUNT_HAS_NO_STORE";
			return 'error';
		}

		if($this->store_editable == 0) {
			$this->error = "STORE_NOT_EDITABLE";
			return 'error';
		}

		if (!Cpanel::checkLogin() && $this->is_admin == 1){
			$this->account_session->expire();
			$login = $GLOBALS['BCONF']['*']['common']['base_url']['secure']."/login";
			header('Location: ' .$login );
			exit(0);
		}

		$store_id = $this->account_session->get_param("store_id");

		$newdata = array(
			"store_name" => $store_name,
			"store_info_text" => $store_info_text,
			"store_region" => $store_region,
			"store_commune" => $store_commune,
			"store_address" => $store_address,
			"store_address_number" => $store_address_number,
			"store_phone" => $store_phone,
			"store_website_url" => $store_website_url,
			"store_hide_phone" => ($store_hide_phone ? "1" : "0"),
			//"date_start" => $this->data['date_start'],
			//"date_end" => $this->data['date_end'],
			//"store_type" => $this->data['store_type'],
			"store_url" => $this->store_prefix.Cpanel::slugify($store_name),
			"main_image" => $main_image,
			"main_image_offset" => $main_image_offset,
			"logo_image" => $logo_image,
			"default_bg" => $default_bg,
			"is_admin" => $this->is_admin,
			"administrator" => $this->administrator
		);

		if (empty($this->data)) {
			$this->data = $newdata;
		} else {
			$this->data = array_merge($this->data, $newdata);
		}


		if (!empty($default_bg)) {
			$main_image = bconf_get($BCONF, "*.common.stores.default_image_prefix") . $default_bg;
		}

		$store_url = Cpanel::slugify($store_name);
		$trans = $this->trans;
		$trans->reset();
		$trans->add_data("store_id",$store_id);
		$trans->add_data("name", html_entity_decode($store_name));
		if (!empty($store_info_text)) {
			$trans->add_data("info_text", sanitizeVar($store_info_text, 'string'));
		}
		$trans->add_data("url", $store_url);
		$trans->add_data("phone", $store_phone);
		$trans->add_data("hide_phone", $store_hide_phone ? 1 : 0);
		$trans->add_data("address", $store_address);
		$trans->add_data("address_number", $store_address_number);
		if (!empty($main_image)) {
			$trans->add_data("main_image", $main_image);
		}
		if (!empty($main_image_offset)) {
			$trans->add_data("main_image_offset", $main_image_offset);
		}
		if (!empty($logo_image)) {
			$trans->add_data("logo_image", $logo_image);
		}
		if(!empty($store_region)) {
			$trans->add_data("region", $store_region);
		}
		if(isset($store_commune)) {
			$trans->add_data("commune", $store_commune);
		}
		if(!empty($store_website_url)) {
			$trans->add_data("website_url", $store_website_url);
		}

		if (Cpanel::checkLogin() &&  Cpanel::hasPriv('Stores.editStore')){
			$reply = $trans->send_admin_command("edit_store");
		}
		else{
			$reply = $trans->send_command("edit_store");
		}

		if ($trans->has_error()) {
			$this->set_errors($trans->get_errors("store_"));
			if(array_key_exists('store_url', $this->errors) && !array_key_exists('store_name', $this->errors)){
				$this->errors['store_name'] = $this->errors['store_url'];
			}
			$this->set_warnings($trans->get_warnings("store_"));
			$this->set_messages($trans->get_messages("store_"));
			$this->data['error'] = "1";
			return 'form';
		}

		if (Cpanel::checkLogin() &&  Cpanel::hasPriv('Stores.editStore')){
			return 'cp_success';
		}else{
			$this->account_session->set_param('store_status',$reply['store_status']);
			$this->data["store_status"] = $reply['store_status'];
			$this->data["store_edited_successful"] = 1;
			return 'form';
		}
	}

	function upload_image($file, $type) {
		global $BCONF;

		$minwidth = (int)bconf_get($BCONF, "*.stores.images.$type.width");
		$minheight = (int)bconf_get($BCONF, "*.stores.images.$type.height");
		$maxheight = (int)bconf_get($BCONF, "*.stores.images.$type.maxheight");
		$maxratio = NULL;
		if ($minheight > 0) {
			$maxratio = (float)$minwidth / $minheight;
		}

		bLogger::logDebug(__METHOD__, "Validation data minwidth: $minwidth, minheight: $minheight, maxratio: $maxratio");

		$res = null;
		if (@$_FILES[$file]['error'] == UPLOAD_ERR_OK && strlen(@$_FILES[$file]['name'])) {
			syslog(LOG_INFO, log_string()."Image ({$_FILES[$file]['name']}) uploaded with type ({$_FILES[$file]['type']}) and size ({$_FILES[$file]['size']})");

			/* Set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc) */
			ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
			ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));

			/* Convert the uploaded image */
			$image = new bImage($_FILES[$file]['tmp_name']);
			$image_size = $image->get_image_size();
			$width = $image_size[1];
			$height = $image_size[2];
			$ratio = (float)$width / $height;

			bLogger::logDebug(__METHOD__, "Image dimension width: $width, height: $height, ratio: $ratio, type $type");
			if (!empty($type)) {
				if ($image_size[1] < $minwidth || $image_size[2] < $minheight) {
					bLogger::logInfo(__METHOD__, "Size of image uploaded for $type is less than $minwidth x $minheight");
					$res = "ERROR_IMAGE_TOO_SMALL";
					return $res;
				} elseif ($type != "logo" && !empty($ratio) && $ratio > $maxratio) {
					bLogger::logInfo(__METHOD__, "Aspect ratio for image of type $type is invalid ($ratio) $minwidth x $minheight");
					$res = "ERROR_IMAGE_ASPECT_RATIO";
					return $res;
				}
			}

			// only the logo image gets resized to fit the box :]
			if ($type == "logo") {
				$image->resize($minwidth, $minheight);
			} else {
				$image->resize($minwidth, $maxheight);
			}

			/* Check if image upload generated error messages */
			if ($image->has_error()) {
				$res = $image->get_error();
			} else {
				syslog(LOG_INFO, log_string()."Image ({$_FILES[$file]['name']}) conversion, status=OK");
				$res = array($image->_image_buffer);
			}

			/* Release image memory */
			$image->destruct();

			/* Reset the memory limit to its original state */
			ini_restore("memory_limit");
			ini_restore("max_execution_time");
		} else if (strlen(@$_FILES[$file]['name']) > 0) {
			syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
			$res = 'ERROR_IMAGE_LOAD';
		}
		return $res;
	}

	function edit_store_upload_image($image_type) {
		if (empty($image_type)) {
			$image_type = "main";
		}
		$width = bconf_get($BCONF, "*.stores.images.$image_type.width");
		$image = $this->upload_image('image', $image_type);
		$result = array();

		if (is_string($image)) {
			$result['status'] = $image;
		} elseif (!empty($image)) {
			$trans = new bTransaction();
			$trans->add_file('image', $image[0]);
			$trans->add_data('image_type', $image_type . "_image");

			$reply = $trans->send_command('imgput_store');
			if (!$trans->has_error() && !empty($reply['file'])) {
				bLogger::loginfo(__METHOD__, "Adding image {$reply['file']} to form via store edit ajax");
				$result['status'] = 'ok';
				$result['file'] = $reply['file'];
			} else {
				$result['trans'] = $trans->get_errors();
			}
		} else {
			$result['status'] = 'error';
		}
		die(yapo_json_encode($result));
	}

	function edit_store_error() {
		$this->display_layout('stores/edit.html', $this->data, null, null, 'mobile_select');
	}

	function edit_store_change_status($status) {
		$target_status = "";
		if ($status == "activate") {
			$target_status = "active";
		} elseif ($status == "deactivate") {
			$target_status = "user_deactivated";
		} else {
			$this->error = "INVALID_STATUS";
			return 'error';
		}

		$this->edit_store_load();
		$current_state = $this->data['store_status'];

		if (($status == "activate" && $current_state != "user_deactivated")
			|| ($status == "deactivate" && $current_state != "active")) {
			$this->error = "INVALID_TRANSITION";
			return 'error';
		}


		$store_id = $this->account_session->get_param('store_id');
		$trans = new bTransaction();
		$trans->add_data('store_id', $store_id);
		$trans->add_data('status', $target_status);
		$reply = $trans->send_command('update_store_status');
		if ($trans->has_error()) {
			$this->set_errors($trans->get_errors());
			return 'error';
		}
		$this->redirect('load', '');
	}

	function edit_store_url($name,$store_id){
		$name = trim($name);
		$url = Cpanel::slugify($name);
		$url_to_search = str_replace("-","_",$url);
		$data["url"] = $this->store_prefix.$url;
		if(strlen($name) < 2 || strlen($url) < 2){
			$data["status"] = lang("ERROR_STORE_NAME_TOO_SHORT");
		}
		else{
			$result = bsearch_search_vtree(bconf_get($BCONF, "*.common.csearch"), "0 lim:1 url:$url_to_search");
			if(!empty($result)){
				if(intval($result["store_id"]) != $store_id){
					$data["status"] = lang("ERROR_STORE_URL_ALREADY_EXIST");
				}
			}
		}
		if(empty($data["status"])){
			$data["status"] = "OK";
		}
		echo yapo_json_encode($data);
		die();

	}


	function edit_store_payment_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email,$prod = 0) {
		$this->form_type = self::STORE_TYPE;
		$this->account_session = new AccountSession();
		$this->email_buyer = $this->account_session->get_param('email');
		$this->company_buyer = ($this->account_session->get_param('is_company') == 't')?'COMPANY':'PRIVATE';
		$this->user_id_buyer = $this->account_session->get_param('user_id');
		$this->buyer_name = $this->account_session->get_param('name');
		return $this->common_validate($document, $name, $lob, $rut, $address, $region, $communes, $contact, $email,$prod) ;
	}

	function f_string($value) {
		return $value;
	}
	function f_name($value){
		return Cpanel::filterStoreChars($value);
	}
	function f_url($value){
		return substr($value, strlen($this->store_prefix));
	}

	function f_phone($value)
	{
		if (!empty($value))
			return (int)$value;
	}

	function days_until($expiration_date) {
		$end = new DateTime($expiration_date);
		$diff = $end->diff(new DateTime());
		return $diff->days;
	}
}

$app = new store_edit();
$app->run_fsm();
?>
