<?php
/*
  Can be run from console for testing with:

  $ env TEMPLATES_PATH=../../../templates/dyn php -c ../../../regress_final/conf voucher_monthly_report.php <testinp
*/

require_once('init.php');
if (!defined("SERVER_ROOT"))
	define("SERVER_ROOT", bconf_get($BCONF, "*.common.basedir"));

require_once('pdf.php');
require_once('bResponse.php');

class VoucherPDF extends PDF
{
	function Footer()
	{
		$fy = 280;
		$fw = 35;
		$ln = 3;
		$this->SetY(-15);
		$this->SetFont('Arial', '', 8);
		$this->Line(10, $fy, 190, $fy);
		$this->Cell($fw, $ln, 'B Locket AB');
		$this->Cell($fw, $ln, 'Momsregistrerings nr: 556610342901', 0, 1);
		$this->Cell($fw, $ln, 'Box 4007');
		$this->Cell($fw, $ln, 'F-skatt/org.nr: 556610-3429', 0, 1);
		$this->Cell($fw, $ln, '18104 Liding�');
		$this->Cell($fw, $ln, 'Tel: 0774-45 00 00');
	}

}

$stdin = fopen('php://stdin', 'r');
$stdout = fopen('php://stdout', 'w');

ini_set("max_execution_time", "-1");
ini_set("max_input_time", "-1");
ini_set("memory_limit", "128M");

while (($line = fgets($stdin))) {
	$line = trim($line);
	if (!$line)
		continue;
	list($store_id, $filename, $tot_balance, $orgnr, $comp_name, $start_date, $end_date) = explode("\t", $line);

	$resp = new bResponse();
	while (($line = fgets($stdin))) {
		$line = trim($line);
		if (!$line)
			continue;
		if ($line == ".")
			break;

		list ($timestamp, $subject, $action_type, $adtype, $payments, $amount, $balance) = explode("\t", $line);

		$resp->fill_array('timestamp', $timestamp);
		$resp->fill_array('ad_subject', $subject);
		$resp->fill_array('action_type', $action_type);
		$resp->fill_array('amount', $amount);
		$resp->fill_array('resulting_balance', $balance);

		$paydescs = array();
		if (preg_match("/^\{(.*)\}$/", $payments, $regs))
			$payments = $regs[1];
		foreach (explode(',', $payments) as $p) {
			if ($p == "ad_action")
				$pd = bconf_get($BCONF, "*.common.action_type.$adtype.label");
			else
				$pd = bconf_get($BCONF, "*.common.action_type.$p.label");
			if (count($paydescs))
				$paydescs[] = strtolower($pd);
			else
				$paydescs[] = $pd;
		}

		$resp->fill_array('payments', implode(', ', $paydescs));
	}
	if ($line === false)
		break;

	// Pass information from trans on to template.
	$resp->add_data('orgnr', $orgnr);
	$resp->add_data('tot_balance', $tot_balance);
	$resp->add_data('comp_name', $comp_name);
	if ($start_date)
		$resp->add_data('start_date', $start_date);
	if ($end_date)
		$resp->add_data('end_date', $end_date);

	$pdf = new VoucherPDF();
	if (bconf_get($BCONF, "voucher_monthly_balance.no_compress"))
		$pdf->SetCompression(false);
	$pdf->SetFont('Times', '', 12);
	$pdf->AliasNbPages();

	$pdf->AddPage();
	$output = $resp->call_template('store/voucher_monthly_report.html', true);
	$pdf->WriteHTML($output);
	$pdf->Output($filename);
	fwrite($stdout, "OK\n");
	fflush($stdout);
}
?>
