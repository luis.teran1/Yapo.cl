TOPDIR?=../../..

include ${TOPDIR}/mk/commonsrc.mk
VPATH:=${COMMONSRCDIR}:${VPATH}

INSTALLDIR=/www-ssl/js
INSTALL_TARGETS=store.js

include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
