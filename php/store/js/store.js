
function list_menu_buttons(ad_id, button_states) {

	var buttons = [ 'mb_renew', 'mb_edit', 'mb_extraimages', 'mb_gallery', 'mb_copy', 'mb_delete' ];

	for(i=0 ; i<buttons.length ; ++i) {

		var e_on = document.getElementById(buttons[i]+"_enabled");
		var e_off = document.getElementById(buttons[i]+"_disabled");

		if(!e_on || !e_off) {
			continue;
		}

		if(button_states[i]) {
			if (true) {
				if (buttons[i] == 'mb_edit' ||
				    buttons[i] == 'mb_extraimages' ||
				    buttons[i] == 'mb_gallery' ||
				    buttons[i] == 'mb_copy') {
					var link = e_on.getElementsByTagName('a')[0];
					var list_id = document.getElementById(ad_id).getAttribute('list_id');
					link.href = link.href.replace(/id=.*/, "id=" + list_id);
				}
			}
			e_on.style.display = "inline";
			e_off.style.display = "none";
		} else {
			e_on.style.display = "none";
			e_off.style.display = "inline";
		}
	}

}

function update_list_menu(result, xmlhttp, data) {
	var message = false;

	var tabname = data.tabname;
	var ad_id = data.ad_id;

	if (result) {
		switch(tabname) {
			case 'deleted':
				if (result['checked_count'] == 1) 
					list_menu_buttons(ad_id, [1,0,0,0,0,0] );
				else
					list_menu_buttons(ad_id, [0,0,0,0,0,0] );
				break;
			case 'unpaid':
				if (result['checked_count'] == 1) 
					list_menu_buttons(ad_id, [0,0,0,0,1,0] );
				else
					list_menu_buttons(ad_id, [0,0,0,0,0,0] );
				break;
			case 'unverified':
			case '':
				if(result['imported'] == 0) {
					switch(result['checked_count']) {
						case 0:
							list_menu_buttons(ad_id, [0,0,0,0,0,0] );
							break;
						case 1:
							list_menu_buttons(ad_id, [1,1,1,(result['can_gallery'] ? 1 : 0),1,1] );
							break;
						default:
							list_menu_buttons(ad_id, [1,0,0,0,0,1] );
					}
				} else {
					list_menu_buttons(ad_id, [1,0,0,0,0,0] );
				}
				break;
			case 'pending_review':
				list_menu_buttons(ad_id, [0,0,0,0,0,0] );
					break;

		}
	} else if (xmlhttp.responseText.indexOf('<!DOCTYPE') >= 0) {
		var newbody = xmlhttp.responseText;
		
		/*
		 * #include<stddisclaimer.js>
		 */

		newbody = newbody.replace(/(.|\n)*\<body\>/, "");
		newbody = newbody.replace(/\<\/body\>[^<]*\<\/html\>[^<]*$/m, "");
		document.body.innerHTML = newbody;
		return;
	}

	document.getElementById(ad_id).disabled = false;	
}


function ad_clicked(checkbox,tabname,instance_id,list_id,imported) {
	var post = 'id=' + checkbox.id + '&checked=' + (checkbox.checked ? list_id : 0) + (tabname ? "&cs=" + tabname : '') + (imported ? '&imported=' + imported  : '');
	checkbox.disabled = true;
	ajax_request("/store/ad_clicked/"+instance_id + '?' + post, null, update_list_menu, {'tabname' : tabname, 'ad_id' : checkbox.id}, true);

	return false;
}


