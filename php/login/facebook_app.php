<?php
/*
* How everything is connected.
* ----------------------------
*
*                         ------------
* Mobile ---- php facebook     SSL     api nextgen --- facebook
* Desktop --/             ------------             \-- trans
* \
*  \ facebook
*
* This php app handles the request from mobile and desktop, to login,
* register and associate a user, that is using facebook to login.
*
* The routing look like this:
*
* - POST '/login':
* - POST '/register':
* - POST '/association':
*
*/
require_once('facebook_login.php');

openlog("facebook", LOG_ODELAY, LOG_LOCAL0);
header('Content-type: application/json');

$path_info = (isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : null);

if ($path_info) {
	$source = (isset($_POST['source'])) ? strip_tags($_POST['source']) : null;
	$token = (isset($_POST['token'])) ? strip_tags($_POST['token']) : null;
	$facebook = new Facebook($token);

	try {
		switch($path_info) {
			case '/login':
				echo $facebook->login($source);
				break;
			case '/register':
				$email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : null;
				echo $facebook->register($email, $source);
				break;
			case '/association':
				$email = (isset($_POST['email'])) ? strip_tags($_POST['email']) : null;
				$password = (isset($_POST['password'])) ? strip_tags($_POST['password']) : null;
				echo $facebook->association($email, $password, $source);
				break;
		}
	} catch(Exception $e) {
		syslog(LOG_CRIT, log_string() . $e->getMessage());
		echo(json_encode(array(
			'ok'=>false,
			'code'=>'CONECTION_ERROR'
		)));
	}
}
