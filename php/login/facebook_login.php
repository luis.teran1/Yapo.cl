<?php
/*
* Facebook calls to the nextgen API here.
*
* It uses a "probably not well installed" library httpful.phar,
* to request the nextgen api, using the certificates that are available in the server.
*
* The majority of the logic is implemented in the nextgen api,
* we just created this file to handle the certificate issue.
*
* I personally think an approach, as mexicans did, of removing
* the certificates from the api should be a good movement.
*
*/
require_once('autoload_lib.php');
require_once('AccountSession.php');
require_once('./httpful.phar');
require_once('init.php');

use Yapo\Bconf;

class Facebook {

	private $token;
	private $nextgen_uri;

	function Facebook($token) {
		$this->token = $token;
		$path = '/api/v1.1/private/accounts';
		$this->nextgen_uri = bconf_get($BCONF, '*.common.base_url.nextgen_api') . $path;
		$this->url_referer = @$_SERVER['HTTP_REFERER'];
		$this->url_ai = bconf_get($BCONF, "*.common.base_url.ai");
	}

	function login($source) {
		$this->get_post_redirect();
		$response = \Httpful\Request::post($this->nextgen_uri)
		->addHeader('Accept-Encoding', 'gzip')
		->addHeader('Connection', 'Keep-Alive')
		->addHeader('Content-Type', 'application/json')
		->addHeader('Accept-Language', 'es')
		->addHeader('Accept', 'application/json')
		->addHeader('Authorization', 'tag:scmcoord.com,2013:facebook code="' . $this->token . '"')
		->addHeader('X-Nga-source', $source)
		->send();

		return $this->get_json_response($response);
	}

	function register($email, $source) {
		$response = \Httpful\Request::post($this->nextgen_uri)
		->addHeader('Accept-Language', 'es')
		->addHeader('Accept', 'application/json')
		->addHeader('Authorization', 'tag:scmcoord.com,2013:facebook code="' . $this->token . '"')
		->addHeader('X-Nga-source', $source)
		->body('{"account":{"email": "' . $email .'"}}')
		->send();

		return $this->get_json_response($response);
	}

	function association($email, $password, $source) {

		$hash = base64_encode($email . ':' . $password);
		$response = \Httpful\Request::post($this->nextgen_uri)
		->addHeader('Accept-Language', 'es')
		->addHeader('Accept', 'application/json')
		->addHeader('Authorization', 'Basic ' . $hash)
		->addHeader('X-Nga-source', $source)
		->body('{"account":{"facebook":{"token":"' . $this->token .'"}}}')
		->send();

		return $this->get_json_response($response);
	}

	function get_json_response($api_response) {
		// for some reason, we modify this response
		$json_response = json_encode(array(
			'ok'=>false,
			'code'=>'RESPONSE_NO_EXPECTED'
		));

		if (isset($api_response->body->error)) {
			if (!isset($api_response->body->error->code)) {
				$json_response = json_encode(array(
					'ok'=>false,
					'code'=>'ERROR_WITHOUT_CODE'
				));
			}

			$json_response = json_encode(array(
				'ok' => false,
				'code' => $api_response->body->error->code
			));
		}

		if (isset($api_response->body->account)) {
			$email = $api_response->body->account->email;
			$account_session = new AccountSession();
			$json_response = json_encode(array(
				'ok'=>false,
				'code'=>'LOGIN_FAILED'
			));
			if ($account_session->loginSpecial($email, true)) {
				$json_response = json_encode(array(
					'ok' => true,
					'code' => 'LOGIN_OK',
					'email' => $email
				));
			}
		}
		return $json_response;
	}

	function get_post_redirect() {
		if (strpos($this->url_referer, 'login') !== false)
			$this->unset_redirect_url();
		elseif (strpos($this->url_referer, 'cuenta') !== false)
			$this->unset_redirect_url();
		elseif (strpos($this->url_referer, 'reset_password') !== false)
			$this->unset_redirect_url();
		elseif (strpos($this->url_referer, 'mis_avisos') !== false)
			$this->unset_redirect_url();
		elseif (strpos($this->url_referer, 'pvf') !== false)
			$this->unset_redirect_url();
		elseif (strpos($this->url_referer, '/ai/load/') !== false && strpos($this->url_referer, 'cmd=edit') !== false) {
			$params_pos = strpos($this->url_referer, '?');
			$this->url_referer = $this->url_ai . '/ai' . substr($this->url_referer, $params_pos);
			$this->set_redirect_url();
		} else {
			$this->set_redirect_url();
		}
	}

	function set_redirect_url() {
		setcookie(bconf_get($BCONF, "*.accounts.login.cookie_referer"),
			$this->url_referer,
			time() + bconf_get($BCONF, '*.accounts.session_timeout'), '/',
			bconf_get($BCONF, '*.common.session.cookiedomain'),
			false,
			false
		);
	}

	function unset_redirect_url() {
		unset($_COOKIE[bconf_get($BCONF, "*.accounts.login.cookie_referer")]);
		setcookie(bconf_get($BCONF, "*.accounts.login.cookie_referer"),
			"",
			time() - 3600, '/',
			bconf_get($BCONF, '*.common.session.cookiedomain'),
			false,
			false
		);
	}
}
