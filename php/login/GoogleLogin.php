<?php
/**
 * This file only contains GoogleLogin class
 * The contents were checked with PSR2 standard
 * Please also validate future changes.
 */
namespace Yapo;

use AccountSession;
use bTransaction;

/**
 * GoogleLogin calls to the Google login MS here.
 * and start session when Verification is Ok
 */
class GoogleLogin
{

    const PASSWORDLEN = 9;

    private $token;
    private $googleMsUri;
    private $accSession;
    private $source;
    private $trans;

    public function __construct($token, $source, $accSession = null, $trans = null)
    {
        $this->token = $token;
        $this->url_referer = @$_SERVER["HTTP_REFERER"];
        $this->url_ai = Bconf::get($BCONF, "*.common.base_url.ai");
        $this->accSession = empty($accSession)? new AccountSession(): $accSession;
        $this->trans = empty($trans)? new bTransaction(): $trans;
        $defaultSource = Bconf::get($BCONF, "*.accounts.source.5");
        $this->source = empty($source)? $defaultSource: $source;
        // Init uri
        $path = "";
        $host = Bconf::get($BCONF, "*.google.login.host");
        $port = Bconf::get($BCONF, "*.google.login.port");
        $this->googleMsUri = "http://{$host}:{$port}{$path}";
    }

    public function redirections($referer)
    {
        $url = $referer;
        $blocketUrl = Bconf::get($BCONF, "*.common.base_url.blocket")."/";
        Logger::logDebug(__METHOD__, "redirections checking");
        if ($referer == $blocketUrl) {
            $redirUrl = Bconf::get($BCONF, "*.common.base_url.secure");
            $url = $redirUrl."/dashboard";
        }
        return $url;
    }

    public function login()
    {
        // If the user is already logged
        if ($this->accSession->is_logged()) {
            return json_encode(
                array(
                "ok" => true,
                "code" => "ALREADY_LOGGED"
                )
            );
        }
        // Do the external call
        $apiResponse = \Httpful\Request::post($this->googleMsUri."/tokenVerify")
        ->addHeader("Accept", "application/json")
        ->addHeader("Authorization", $this->token)
        ->send();

        return $this->getJsonResponse($apiResponse);
    }

    /**
     * Handle API Response and make it work with Yapo accounts
     * @apiResponse contain the token validation from external MS
     */
    private function getJsonResponse($apiResponse)
    {
        // Default response
        $resp = array("ok"=>false, "code"=>"RESPONSE_NO_EXPECTED");

        // If external MS response with error
        if (isset($apiResponse->body->error)) {
            $resp["code"] = "ERROR_WITHOUT_CODE";
            if (isset($apiResponse->body->error->code)) {
                $resp["code"] = $apiResponse->body->error->code;
            }
        }

        // If external ms response with account
        if (isset($apiResponse->body->account)) {
            // The email was validated by ms and google
            // Save it to temporary var
            $email = $apiResponse->body->account->email;
            // Set default response
            $resp["code"] = "LOGIN_FAILED";
            // Try to make a loginSpecial (With out password)
            // With the true the login work for pending_confirmation accounts
            if ($this->accSession->loginSpecial($email, true)) {
                $resp = $this->buildSuccessResponse($email);
            } else {
                // In this case the Account does not existi and we need to create it!
                // And try to login again
                if ($this->createAccount($apiResponse->body->account)) {
                    if ($this->accSession->loginSpecial($email, true)) {
                        $resp = $this->buildSuccessResponse($email);
                    }
                }
            }
            $accInfo = $this->accSession->getAccountInfo();
            if ($accInfo["account_status"] == "pending_confirmation") {
                $this->activateAccount($email);
            }
            Logger::logDebug(__METHOD__, "AccInfo".json_encode($accInfo));
            $this->createSocialAccount($apiResponse->body->account, $accInfo);
        }

        // Translate resp to Json
        $json_response = json_encode($resp);
        Logger::logDebug(__METHOD__, "response:".$json_response);
        return $json_response;
    }

    /**
     * Create social account and social account params using google info
     */
    private function createSocialAccount($googleInfo, $accInfo)
    {
        Logger::logDebug(__METHOD__, "for:".json_encode($googleInfo));
        // Send account associate transaction
        $trans = $this->trans->reset();
        $trans->add_data("account_id", $accInfo["account_id"]);
        $trans->add_data("google_id", $googleInfo->user_id);
        $reply = $trans->send_command("account_associate");

        if ($trans->has_error(true) || $reply["status"] != "TRANS_OK") {
            Logger::logError(__METHOD__, var_export($trans->get_errors(), true));
            return false;
        }

        // Send social account params to trans
        $trans = $this->trans->reset();
        $trans->add_data("social_account_id", $reply["o_social_account_id"]);
        $trans->add_data("image", $googleInfo->picture);
        $trans->add_data("first_name", $googleInfo->given_name);
        $trans->add_data("last_name", $googleInfo->family_name);
        $trans->add_data("source", $this->source);
        $reply = $trans->send_command("create_social_accounts_params");

        if ($trans->has_error(true) || $reply["status"] != "TRANS_OK") {
            Logger::logError(__METHOD__, var_export($trans->get_errors(), true));
            return false;
        }

        return true;
    }

    /**
     *  Build the output for our Happy User :dance:
     */
    private function buildSuccessResponse($email)
    {
        // Build redir url
        $redirUrl = Bconf::get($BCONF, "*.common.base_url.secure");
        $redirUrl .= "/google/load?session=";
        $redirUrl .= $this->accSession->get_session_id();
        // Build array to return
        return array(
            "ok" => true,
            "code" => "LOGIN_OK",
            "email" => $email,
            "redirect" => $redirUrl
        );
    }

    /**
     * Create new account with Google info
     * This is the most basic version of account
     * the user will need complete the info on his
     * profile.
     */
    private function createAccount($data)
    {
        Logger::logDebug(__METHOD__, "for:".json_encode($data));
        $trans = $this->trans->reset();
        $trans->add_data("name", $data->name);
        $trans->add_data("email", $data->email);
        $trans->add_data("is_company", "0");
        $trans->add_data("source", $this->source);
        $randPass = $this->randomString(self::PASSWORDLEN);
        $trans->add_data("password", hash_password($randPass));
        $trans->add_data("mobile", "1");
        $trans->add_data("accept_conditions", "1");
        $trans->add_data("autoactivate", "1");

        $reply = $trans->send_command("create_account");

        if ($trans->has_error(true) || $reply["status"] != "TRANS_OK") {
            Logger::logError(__METHOD__, "failed for {$data->email} user");
            Logger::logError(__METHOD__, var_export($trans->get_errors(), true));
            return false;
        }
        return true;
    }

    /**
     * Activate account by email
     */
    private function activateAccount($email)
    {
        Logger::logDebug(__METHOD__, "Activating account {$email}");
        $trans = $this->trans->reset();
        $trans->add_data("status", "active");
        $trans->add_data("email", $email);
        $reply = $trans->send_command("manage_account");

        if ($trans->has_error(true) || $reply["status"] != "TRANS_OK") {
            Logger::logError(
                __METHOD__,
                "account email could not be activated for {$email} user"
            );
            return false;
        }
        return true;
    }
    /**
     * Build a random string with the indicated Size
     * By default, put 2 random letters at the beginning
     * and fill with random numbers
     */
    private function randomString($size, $letters = 2)
    {
        $alpha_key = "";
        $keys = range('a', 'z');

        for ($i = 0; $i < $letters; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - $letters;

        $key = "";
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $alpha_key . $key;
    }
}
