<?php
require_once('autoload_lib.php');
require_once('AccountSession.php');
require_once('./httpful.phar');
require_once('init.php');
require_once('util.php');

use Yapo\Bconf;
use Yapo\GoogleLogin;

openlog("google", LOG_ODELAY, LOG_LOCAL0);

$path_info = (isset($_SERVER['PATH_INFO']) ? $_SERVER['PATH_INFO'] : null);

if ($path_info) {
	$source = (isset($_POST['source'])) ? strip_tags($_POST['source']) : null;
	$token = (isset($_POST['token'])) ? strip_tags($_POST['token']) : null;
	$google = new GoogleLogin($token, $source);

	try {
		switch($path_info) {
			case '/login':
				header('Content-type: application/json');
				echo $google->login();
				break;
			case '/load':
				$referer = @$_SERVER['HTTP_REFERER'];
				$session = (isset($_GET['session'])) ? strip_tags($_GET['session']) : null;
				if (empty($referer)){
					echo "MISSING";
				} elseif (empty($session)) {
					echo "session MISSING";
				} else {
					$cookie_name =  Bconf::get($BCONF, "*.accounts.session_cookie_name");
					$cookie_domain = Bconf::get($BCONF, "*.common.session.cookiedomain");
					$cookie_ttl = time() + Bconf::get($BCONF, '*.accounts.session_long_timeout');
					// Keep this cookie here...
					// when use the sendCookie method from AccountSession doesnt work
					setcookie($cookie_name,	$session, $cookie_ttl, '/',	$cookie_domain,	false, true);
					$redirectTo = $google->redirections($referer);
					header('Location: '.$redirectTo);
				}
				break;
		}
	} catch(Exception $e) {
		syslog(LOG_CRIT, log_string() . $e->getMessage());
		echo(json_encode(array(
			'ok'=>false,
			'code'=>'CONECTION_ERROR'
		)));
	}
}
