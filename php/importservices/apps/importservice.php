<?php
require_once("blocket_soap_application.php");
require_once("bAd.php");
require_once("bTransaction.php");
require_once("bImage.php");

openlog("importservice", LOG_ODELAY, LOG_LOCAL0);

class blocket_import extends blocket_soap_application {
	var $import_partner_id;
	var $license_key;
	var $version;

	function blocket_import() {
		$this->init();
		$this->application_name = 'importservice';
	}

	function get_state($name) {
		switch ($name) {
		case 'UPLOADIMAGE':
			return array('function' => 'uploadimage',
				     'auth_required' => 'access_denied',
					'params' => array('file' => 'f_base64Binary'),
					'result' => array('image_id' => 'string'));
		case 'IMPORTAD':
			return array('function' => 'importad',
				     'auth_required' => 'access_denied',
					'params' => array('data' => 'importad_data'),
					'result' => array('list_id' => 'integer'),
					'descr_tmpl' => 'importservice/importad.html');
		case 'DELETEAD':
			return array('function' => 'deletead',
				     'auth_required' => 'access_denied',
				     'descr_tmpl' => 'importservice/deletead.html',
				     'params' => array('ad_id' => 'f_integer',
						       'external_ad_id' => 'f_string'),
				     'result' => array('output' => 'integer'));
		case 'GETADSSTATUS':
			return array('function' => 'getadsstatus',
				     'auth_required' => 'access_denied',
				     	'params' => array('store_id' => 'f_integer',
							  'list_id' => 'f_integer',
							  'external_ad_id' => 'f_string'),
				     'descr_tmpl' => 'importservice/getadsstatus.html',
				     'result_descr_tmpl' => 'importservice/getadsstatus_result_descr.html',
				     'result_tmpl' => 'importservice/getadsstatus_result.xml');
		case 'GETSTORES':
			return array('function' => 'getstores',
				     'auth_required' => 'access_denied',
				        'descr_tmpl' => 'importservice/getstores.html',
					'result_descr_tmpl' => 'importservice/getstores_result_descr.html',
					'result_tmpl' => 'importservice/getstores_result.xml'
					);
		}
		
	}

	function get_states() {
		return array('uploadimage', 'importad', 'deletead', 'getadsstatus', 'getstores');
	}

	function get_headers($state) {
		return array('import_partner_id' => array('filter' => 'f_import_partner_id'),
			     'license_key' => array('filter' => 'f_license_key'),
			     'version' => array('filter' => 'f_version', 'minOccurs' => 0) );
	}

	function handle_header($header, $value = false) {
		if ($value === false) {
			$tag = $this->get_tag($header);
			$contents = $this->get_contents($header);
		} else {
			$tag = $header;
			$contents = $value;
		}

		switch ($tag) {
		case 'IMPORT_PARTNER_ID':
			$this->import_partner_id = $contents;
			return true;
		case 'LICENSE_KEY':
			$this->license_key = $contents;
			return true;
		case 'VERSION':
			$this->version = $contents;
			return true;
		default:
			return false;
		}
	}

	function validate_state() {
		global $BCONF;
		if (empty($this->import_partner_id) || empty($this->license_key)) {
			syslog(LOG_ERR, log_string()."Missing import_partner_id/licensekey: partner:{$this->import_partner_id} license_key:{$this->license_key}");
			return false;
		}

		$partner_key = bconf_get($BCONF, 'services.ad.user.import_partner.' . $this->import_partner_id . '.key');
		if (empty($partner_key)) {
			syslog(LOG_ERR, log_string().'No licensekey in conf for import partner ' . $this->import_partner_id);
			return false;
		}
		if ($partner_key != $this->license_key) {
			syslog(LOG_ERR, log_string().'Wrong licensekey ($partner_key)for import partner ' . $this->import_partner_id );
			return false;
		}

		if (!$this->allowed_ip()) {
			syslog(LOG_ERR, log_string().'Ip ' . $_SERVER['REMOTE_ADDR'] . ' not allowed for ' . $this->import_partner_id);
			return false;
		}

		return true;
	}

	function allowed_ip() {
		global $BCONF;

		foreach (bconf_get($BCONF, 'services.ad.user.import_partner.' . 
				   $this->import_partner_id . '.allowed_ip') as $k => $ip) {
			if ($ip == $_SERVER['REMOTE_ADDR'])
				return true;
		}
		
		foreach (bconf_get($BCONF, '*.common.ip_allowed_list.blocket') as $k => $ip) {
			if ($ip == $_SERVER['REMOTE_ADDR'])
				return true;
		}

		foreach (bconf_get($BCONF, '*.common.ip_allowed_list.blocket') as $k => $ip) {
			if ($ip == $_SERVER['REMOTE_ADDR'])
				return true;
		}

		return false;
	}

	function find_store($import_id) {
		$transaction = new bTransaction();
		$transaction->add_data('link_type', $this->import_partner_id);
		$transaction->add_data('import_id', $import_id);
		$reply = $transaction->send_command('find_import_store');
		if ($reply['status'] == "TRANS_OK")
			return intval($reply['store_id']);
		syslog(LOG_INFO, log_string()."Failed store lookup for import id " . $import_id . ": " . $reply['status'] );
		$this->fault(array('Sender', $reply['error']), 'Store lookup error');
	}

	function uploadimage($file) {
		if (empty($file))
			$this->fault(array('Sender', 'Empty file data'), 'Missing parameter');
		
		if (!empty($file))
		    $image = $this->get_image(base64_decode($file));
		else
		    $image = $this->get_image($file_binary);

		if (is_array($image)) {
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			$transaction->add_file('image', $image[0]);
			$transaction->add_file('thumbnail', $image[1]);
			$reply = $transaction->send_command('imgput');
			if ($reply['status'] == 'TRANS_OK' && isset($reply['file'])) {
				return array('image_id' => $reply['file']);
			} else
				$this->fault("Sender", $reply['status']);
		} else if (is_string($image)) {
			$this->fault("Sender", $image);
		}
	}

	function importad($data) {
		global $BCONF;

		$store_id = $this->find_store($data->import_id);

		if (!empty($data->lkf)) {
			if ($lkfdata = bconf_get($BCONF, "importads.lkf.{$data->lkf}")) {
				/* Find region/city/area from lkf number */
				$data->region = $lkfdata['r'];
				if (isset($lkfdata['c']))
					$data->city = $lkfdata['c'];
				$data->area = $lkfdata['a'];
			} else {
				$this->fault(array('Sender','ERROR_LKF_INVALID'), 'LKF '.$data->lkf.' not in database');
			}
		} else {
			$this->fault(array('Sender','ERROR_LKF_MISSING'), 'Could not lookup region information due to missing LKF');
		}

		$ad = new bAd(NULL);
		$ad->populate(get_object_vars($data), false, array("noclean" => array("subject_tail" => 1, "periods" => 1)) );
		$ad->company_ad = '1';
		$ad->store = $store_id;

		/* Commit to trans */
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->populate($ad->get_data_array(2), bconf_get($BCONF, "*.common.list_features.feat"), bconf_get($BCONF, "*.common.commalist_features.feat"));
		$transaction->add_data('image', $data->image);
		if (!empty($data->image_text)) {
			foreach ($data->image_text as $idx => $text)
				$transaction->add_data('image_text' . ($idx + 1), $text);
		}
		$transaction->add_data('external_ad_id', $data->external_ad_id);
		$transaction->add_data('link_type', $this->import_partner_id);

		$reply = $transaction->send_command('newad');
		if ($transaction->has_error()) {
			syslog(LOG_INFO, log_string()."Failed newad transaction for external_ad_id: " . $data->external_ad_id);
			$this->fault(array('Sender', 'TRANS_ERROR'), 
				     'Failed to insert ad', 
				     $transaction->get_errors(),
				     $transaction->get_messages());
		}

		return array('list_id' => $reply['list_id']);
	}

	function deletead($ad_id, $external_ad_id) {
		global $BCONF;

		if (empty($external_ad_id)) {
			if (empty($ad_id)) {
				$this->fault('Sender', 'Missing ad_id or external_ad_id');
			}
			$data = $this->getadsstatus(NULL, $ad_id, NULL);
			$external_ad_id = $data[0]['external_ad_id'];
		}
		
		$transaction = new bTransaction(); 
		
		$transaction->add_data('remote_addr', $_SERVER['REMOTE_ADDR']);
		$transaction->add_data('link_type', $this->import_partner_id);
		$transaction->add_data('external_ad_id', $external_ad_id);
		$transaction->add_data('do_not_send_mail', 1);
		
		$reply = $transaction->send_command('deletead');

		if ($transaction->has_error())
			$this->fault("Receiver", $transaction->get_error("error"));
		
		return array('status' => 1);
	}
	

	function getstores() {
		global $BCONF;

		$transaction = new bTransaction();
		$transaction->add_data('import_partner_id', $this->import_partner_id);
		$reply = $transaction->send_command('partner_stores');
		if ($transaction->has_error()) {
			syslog(LOG_INFO, log_string()."Failed partner_stores transaction for import_partner: ".$this->import_partner_id);
			$this->fault(array('Sender', 'TRANS_ERROR'), 'Query failed', $transaction->get_errors(),
				     $transaction->get_messages());
		}
		foreach ($reply['partner_stores'] as $idx => &$storedata) {
			$cats = array();
			foreach (explode(',', $storedata['import_type']) as $type) {
				foreach (explode(',', bconf_get($BCONF, "*.common.stores.import_type.$type.categories")) as $cat)
					$cats[$cat] = 1;
			}
			$storedata['categories'] = array_keys ($cats);
			sort ($storedata['categories']);
			unset ($storedata['import_type']);
		}
		return array('stores' => $reply['partner_stores']);
	}

	function getadsstatus($store_id, $list_id, $external_ad_id) {
		$transaction = new bTransaction();
		$transaction->add_data('import_partner', $this->import_partner_id);
		if(!empty($store_id))
			$transaction->add_data('store_id', $store_id);
		if(!empty($list_id))
			$transaction->add_data('list_id', $list_id);
		if(!empty($external_ad_id))
			$transaction->add_data('external_ad_id', $external_ad_id);

		$reply = $transaction->send_command('partner_get_ads_status');
		if ($transaction->has_error()) {
			syslog(LOG_INFO, log_string()."Failed get_ads_status transaction for import_partner: ".
			       $this->import_partner_id);
			$this->fault(array('Sender', 'TRANS_ERROR'), 'Query failed', $transaction->get_errors(),
				     $transaction->get_messages());
		}
		
		foreach ($reply['partner_get_ads_status'] as $idx => &$ad_data) {
			if (isset($ad_data['image_ids'])) {
				if (strlen($ad_data['image_ids']))
					$ad_data['images'] = explode(",", $ad_data['image_ids']);
				unset($ad_data['image_ids']);
			}
		}

		return array('ads' => $reply['partner_get_ads_status']);
	}

	function get_image($file) {
		global $BCONF;

		/* 
		 * Set the PHP memory limit to image max resolution * 4 (rgba) * 2 
		 * (to have enough memory to resize and etc) 
		 */
		ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
		ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));


		$tmpdir = ini_get('upload_tmp_dir');
		if (!$tmpdir)
			$tmpdir = '/tmp';
		$filename = tempnam($tmpdir, "image_load_server");
		if (!$filename)
			return false;
		if (empty($file)) {
			$this->fault(array('Sender', 'Empty image data'));
		}
		file_put_contents($filename, $file);

		/* Convert the uploaded image */
		$image = new bImage($filename);
		unlink($filename);

		$thumbnail_buffer = $image->create_thumbnail();
		$image->resize();
		$image_buffer = $image->create_image();

		/* Check if image upload generated error messages */
		if ($image->has_error()) {
			$res = $image->get_error();
		} else {
			$res = array($image_buffer, $thumbnail_buffer);
		}

		/* Release image memory */
		$image->destruct();

		/* Reset the memory limit to its original state */
		ini_restore("memory_limit");
		ini_restore("max_execution_time");

		return $res;
	}

	function f_string($v) {
		return strip_tags($v);
	}

	function f_integer($v) {
		return intval($v, 10);
	}

	function f_base64Binary($input) {
		$input = trim($input);
		if (preg_match("/[^a-z0-9\/\+=]/i", $input)) {
			return "";
		}
		return $input;
	}
}

class importad_data extends blocket_filter {
	var $name = 'f_string';
	var $import_id = 'f_string';
	var $category = 'f_category';
	var $email = 'f_string';
	var $subject = 'f_string';
	var $body = 'f_body';
	var $phone = 'f_string';
	var $phone_hidden = 'f_integer';
	var $type = 'f_type';
	var $external_ad_id = 'f_string';
	var $lkf = 'f_integer';
	var $price = 'f_integer';
	var $old_price = 'f_integer';
	var $infopage = 'f_http_url';
	var $infopage_title = 'f_string';
	var $image = 'f_array';
	var $image_text = 'f_array';
	var $zipcode = 'f_string';

	function get_filter_for_type($type) {
		switch($type) {
			case 'checkbox':
			case 'range':
			case 'int':
				return 'f_integer';
			case 'string':
				return 'f_string';

			case 'multiple':
			case 'checklist':
			default:
				return null;
		}

	}

	function importad_data() {
		global $BCONF;

		$params = array();
		foreach (bconf_get ($BCONF, "*.cat") as $cat => $catdata) {
			if (empty($catdata['leaf']))
				continue;
			$io = array();
			$io['category'] = $cat;
			$io['parent'] = $catdata['parent'];
			$io['company_ad'] = 1;

			get_settings(bconf_get($BCONF,"*.category_settings"), "types",
					create_function('$s,$k,$d', 'return @$d[$k];'),
					create_function('$s,$k,$v,$d', '$d["types"][] = $k;'),
					$io);

			foreach ($io['types'] as $type) {
				$io['type'] = $type;
				$io['params'] = array();
				get_settings(bconf_get($BCONF,"*.category_settings"), "params",
						create_function('$s,$k,$d', 'return @$d[$k];'),
						create_function('$s,$k,$v,$d', '$d["params"][] = $k;'),
						$io);

				foreach ($io['params'] as $p)
					$params[$p] = 1;
			}
		}
		// Create instance variables for adparams and associate with an appropriate filter.
		$ad_params = array();
		foreach (bconf_get($BCONF, "*.common.ad_params") as $param) {
			if (empty($params[$param['name']]))
				continue;
			if (bconf_get($BCONF, "import.skip_param." . $param['name']))
				continue;
			if (isset($param['name'])) {
				if( ($filter = $this->get_filter_for_type($param['type'])) != null) {
					$instance_var = $param['name'];
					$this->$instance_var = $filter;
				}
			}
		}
		

	}

	function f_category($v) {
		return intval($v, 10);
	}

	function f_body($v) {
		return strip_tags($v);
	}

	function f_http_url($v) {
		return strip_tags($v);
	}

	function f_string($v) {
		return strip_tags($v);
	}

	function f_array($a) {
		if (is_array($a)) {
			foreach ($a as $k => $v) {
				$a[$k] = strip_tags($v);
			}
		}
		return $a;
	}

	function f_type($v) {
		return (string)$v;
	}
}

$app = new blocket_import();
$app->run_fsm();

?>
