<?php

include_once('autoload_lib.php');
include_once('init.php');
require_once('util.php');

/* This is a generic mail landing page, its supose to be used for any mail to
 * execute actions directly from it
 */
class blocket_landing extends blocket_application {

	var $ad_data;
	var $account_session;
	var $cmd;
	var $error_msg;
	var $force_login; // used in login state to force login (false = force)

	function blocket_landing() {
		global $BCONF;
		$this->application_name = 'landing';
		$this->page_title = lang('PAGE_LANDING_TITLE');
		$this->page_name = lang('PAGE_LANDING_NAME');
		$this->headstyles = bconf_get($BCONF, "landing.headstyles");
		$this->headscripts = bconf_get($BCONF, "landing.headscripts");
		$this->init('load', 10);
		$this->account_session = new AccountSession();
		$this->force_login = false;
	}

	function get_state($statename) {
		switch($statename) {
			case 'load': // load ad -> [login|error]
				return array('function' => 'landing_load',
						'params' => array('id', 'a', 'h', 'cmd'),
						'method' => 'get');
			case 'login': // logs in the user -> [excute|error]
				return array('function' => 'landing_login',
						'method' => 'get');
			case 'execute': // excute the required action -> [error|<action_state>]
				return array('function' => 'landing_execute',
						'method' => 'get');
			case 'activate_success': // if the activate ad is successful
				return array('function' => 'landing_activate_success',
						'method' => 'get');
			case 'activate_failure': // if the activate ad is a failure
				return array('function' => 'landing_activate_failure',
						'method' => 'get');
			case 'error': // generic error
				return array('function' => 'landing_error',
						'method' => 'get');
		}
	}

	function landing_load($id, $a, $h, $cmd){
		$this->ad_data['loaded'] = 'yes';	
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('id', $id);
		if ($cmd == 'activate') {
			$transaction->add_data('action', "deactivated");
			$transaction->add_data('action_id', $a);
		}
		$reply = $transaction->send_command('loadad');
		if ($reply['status'] == 'TRANS_OK'){

			// save ad data
			$this->ad_data['ad_id'] = $reply['ad']['ad_id'];
			$this->ad_data['list_id'] = $reply['ad']['list_id'];
			$this->ad_data['subject'] = $reply['ad']['subject'];
			$this->ad_data['price'] = $reply['ad']['price'];
			$this->ad_data['list_time'] = $reply['ad']['list_time'];
			$this->ad_data['image'] = @$reply['images'][0]['name'];
			$this->ad_data['salted_passwd'] = $reply['ad']['salted_passwd'];
			$this->ad_data['email'] = $reply['users']['email'];
			$this->ad_data['last_status'] = $reply['ad']['status'];
			$this->cmd = $cmd;

			// if ad was deleted...
			if ($this->ad_data['last_status'] == 'deleted'){
				header('Location: '.bconf_get($BCONF, "*.common.base_url.vi").'/vi/'.$this->ad_data["list_id"].'.htm');
				return 'FINISH';
			}

			// commands that need loging
			if ($cmd == 'bump' || $cmd == 'activate') {
				$this->force_login = false;
				if ( $h == sha1($this->ad_data['salted_passwd']) ) {
					return 'login';
				} else {
					// AUTH FAIL
					$this->error_msg = 'LANDING_ERROR_TITLE';
					return 'error';
				}
			}
		}
		// BAD AD
		$this->error_msg = 'LANDING_ERROR_TITLE';
		return 'error';
	}

	function landing_login() {
		if (empty($this->ad_data['loaded'])) {
			return 'load';
		}
		$this->account_session = new AccountSession();
		if(!$this->account_session->loginSpecial($this->ad_data['email'])){
			echo('User doesnt have an account');
			if ($this->force_login) {
				// FORCED LOGIN NOT MEET
				$this->error_msg = 'LANDING_ERROR_TITLE';
				return 'error';
			}
		} else {				
			echo('User logged');
		}
		return 'execute';
	}

	function activate_ad() {
		$transaction = new bTransaction();
        $transaction->add_data('log_string', log_string());
        $transaction->add_data('list_id', $this->ad_data['list_id']);
        $transaction->add_data('new_status', 'active');
        $transaction->add_data('salted_passwd', $this->ad_data['salted_passwd']);
        $transaction->add_data('remote_addr', '1.1.1.1');
        $transaction->add_data('remote_browser', 'firefox');
        $transaction->add_data('source', 'web');
		$reply = $transaction->send_command('change_ad_status');
		return $reply;
	}

	function landing_execute() {
		if (empty($this->ad_data['loaded']))
			return 'load';
		switch($this->cmd) {
			case 'activate':
				$result = $this->activate_ad();
				if ($result['status'] == 'TRANS_OK'){
					return 'activate_success';
				} else {
					return 'activate_failure';
				}
			case 'bump':
				header('Location: '.bconf_get($BCONF, "*.common.base_url.secure").'/pagos?id='.$this->ad_data["list_id"].'&prod=1&ftype=2');
				return 'FINISH';
			default:
				// UNKNOWN CMD 
				$this->error_msg = 'LANDING_ERROR_TITLE';
				return 'error';
		}
		return 'FINISH';
	}

	function landing_activate_success() {
		if (empty($this->ad_data['loaded']))
			return 'load';
		$this->display_layout('landing/activate_success.html', $this->ad_data, null, null, 'responsive_base');
		return 'FINISH';
	}

	function landing_activate_failure() {
		if (empty($this->ad_data['loaded']))
			return 'load';
        $result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:1 id:".$this->ad_data['list_id']);
		if (empty($result['list_id'])) {
			$this->display_layout('landing/error.html', array(
				'error_translate' => 'LANDING_ERROR_TRANSITION',
				'error_title' => 'LANDING_ERROR_TRANSITION_TITLE'),
				null, null, 'responsive_base');
		} else {
			header('Location: '.bconf_get($BCONF, "*.common.base_url.vi").'/vi/'.$this->ad_data["list_id"].'.htm');
		}
		return 'FINISH';
	}

	function landing_error() {
		if (empty($this->ad_data['loaded']))
			return 'load';
		$this->display_layout('landing/error.html', array('error_translate' => $this->error_msg), null, null, 'responsive_base');
		return 'FINISH';
	}

	function f_id($id) {
        return $id;
	}

	function f_a($a) {
        return $a;
	}

	function f_h($h) {
        return $h;
	}

	function f_cmd($cmd) {
        return $cmd;
	}

}
$landing = new blocket_landing();
$landing->run_fsm();
?>
