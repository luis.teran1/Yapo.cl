<?php

namespace Yapo;

class CommonCart
{
    protected $trans;
    protected $acc_session;
    protected $rmm;
    protected $credits;
    protected $yapofact;

    protected $active;
    protected $account_id;
    protected $account_number;

    private $encodedParams = array('tac');

    const PRICE = 'p';
    const LABEL_TYPE = 'lt';
    const AB_USE_NIGHT = 'abn';
    const AB_FREQ = 'abf';
    const AB_NUM_DAYS = 'abd';
    const CATEGORY = 'tac';
    const CREDITS = 'crdts';

    public static $elements = array(
        'product_id' => 'product_id',
        'list_id' => 'list_id',
        'group' => 'group',
        'category' => self::CATEGORY,
        'label_type' => self::LABEL_TYPE,
        'credits' => self::CREDITS,
        'ab_freq' => self::AB_FREQ,
        'ab_num_days' => self::AB_NUM_DAYS,
        'ab_use_night' => self::AB_USE_NIGHT
    );

    public function __construct($redis = null, $acc_session = null, $trans = null, $credits = null, $yapofact = null)
    {
        $this->rmm = $redis;
        $this->trans = $trans;
        $this->acc_session = $acc_session;
        $this->credits = $credits;
        $this->yapofact = $yapofact;
        if ($this->acc_session) {
            $this->active = $this->acc_session->is_logged();
            $this->account_id = $this->acc_session->get_param('email');
            $this->account_number = $this->acc_session->get_param('account_id');
        }
        Logger::logDebug(__METHOD__, "active = " .
            var_export($this->active, true) . " - accId = " .
            var_export($this->account_id, true));
    }

    protected function getDataFromRequest($url_data)
    {
        $result = array(
            'account_id' => $this->account_id,
            'account_number' => $this->account_number
        );

        foreach (self::$elements as $key => $val) {
            if (isset($url_data[$val]) && in_array($val, $this->encodedParams)) {
                $url_data[$val] = base64_decode($url_data[$val]);
            }
            $result[$key] = isset($url_data[$val])?$url_data[$val]:null;
        }
        return (object) $result;
    }

    public function validateParams($data)
    {
        if (!isset($data)) {
            return array(false, null);
        }

        $product = Bconf::getProductConf($BCONF, $data->product_id);
        if (isset($product->referenced_product)) {
            $product = Bconf::getProductConf($BCONF, $product->referenced_product);
        }

        if (empty($product->cart_needs)) {
            return array(true, null);
        }

        $params_needed = explode(',', $product->cart_needs);
        foreach ($params_needed as $need) {
            if (!isset($data->$need)) {
                return array(false, self::$elements[$need]);
            }
        }

        return array(true, null);
    }

    public function getProductAdParams($data)
    {
        $product = Bconf::getProductConf($BCONF, $data->product_id);
        $category = $data->category;
        $codename = $product->codename;

        if (isset($product->referenced_product)) {
            $product = $product->referenced_product;
        } else {
            $product = $data->product_id;
        }

        $payment_settings = array('product' => $product, 'category' => $category);
        get_settings(
            Bconf::bconfGet($BCONF, "*.payment_settings"),
            "product",
            create_function('$s,$k,$d', 'return $d[$k];'),
            create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
            $payment_settings
        );

        $result[self::PRICE] = $payment_settings['price'];

        if (isset($data->label_type)) {
            $result[self::LABEL_TYPE] = $data->label_type;
        }
        if ($codename == 'autobump'
            && isset($data->ab_use_night)
            && isset($data->ab_num_days)
            && isset($data->ab_freq)) {
            $result[self::AB_NUM_DAYS] = $data->ab_num_days;
            $result[self::AB_FREQ] = $data->ab_freq;
            $result[self::AB_USE_NIGHT] = $data->ab_use_night;
            $result[self::PRICE] = $this->getAutoBumpPrice(
                $data->list_id,
                $data->ab_freq,
                $data->ab_num_days,
                $data->ab_use_night
            );
        }
        return $result;
    }

    protected function getAutoBumpPrice($list_id, $freq, $num_days, $use_night)
    {
        $trans_load = $this->trans->reset();
        $trans_load->add_data('list_id', $list_id);
        $trans_load->add_data('frequency', $freq);
        $trans_load->add_data('num_days', $num_days);
        $trans_load->add_data('use_night', $use_night);
        $result_tr = $trans_load->send_command('price_autobump');
        if (isset($result_tr['total_price'])) {
            return $result_tr['total_price'];
        }
        return 0;
    }

    public function getError($code, $missing_param = null)
    {
        $error_messages = array(
            'login' => 'NOT_LOGGED_IN',
            'service' => 'SERVICE_NO_FOUND',
            'params' => 'PARAMETERS_MISSING',
            'range' => 'CREDITS_OUT_OF_RANGE',
            'product' => 'PRODUCT_MISSING',
            'price' => 'PRICE_NOT_FOUND',
            'account' => 'ACCOUNT_NOT_FOUND',
        );
        $error = array(
            'status' => 'ERROR',
            'error_code' => $code
        );
        if (isset($error_messages[$code])) {
            $error['error_message'] = $error_messages[$code];
        } else {
            $error['error_message'] = 'NOT_DEFINED';
        }

        if ($missing_param) {
            $error['missing_param'] = $missing_param;
        }

        return $error;
    }

    public function getProductsFromCart($email, $cart_status = null)
    {
        $list_ids = array();
        $ad_ids = array();
        $list_id_prod_params = array();
        $ad_id_prod_params = array();
        $other_products_ids = array();

        if (!isset($cart_status)) {
            $cart_status = $this->rmm->list_all($email);
        }

        $shopping_cart = $cart_status['ads'];
        if (!empty($shopping_cart)) {
            foreach ($shopping_cart as $key => $val) {
                foreach ($val as $prod_item) {
                    if (is_array($prod_item)) {
                        $item = $prod_item[0];
                        $params = $prod_item[1];
                        $use_ad_id = Bconf::get($BCONF, "*.payment.product.{$item}.get_ad_by") == "ad_id";
                        $group = Bconf::get($BCONF, "*.payment.product.{$item}.group");

                        if (Bconf::get($BCONF, "*.payment.product.{$item}.is_for_ads") == 1) {
                            if ($use_ad_id) {
                                $ad_ids[$key][] = $item;
                                $ad_id_prod_params[$key][$item] = $params;
                            } elseif (in_array($group, array('BUMP', 'GALLERY', 'LABEL'))) {
                                $list_ids[$key][] = $item;
                                $list_id_prod_params[$key][$item] = $params;
                            }
                        } else {
                            $other_products_ids[] = $prod_item;
                        }
                    }
                }
            }
        }
        return array($list_ids, $ad_ids, $list_id_prod_params, $ad_id_prod_params, $other_products_ids);
    }

    protected function getAdIds($list_ids)
    {
        $new_ads_ids = array();
        if (!empty($list_ids)) {
            $trans_load = $this->trans->reset();
            $trans_load->add_data('list_id', implode('|', array_keys($list_ids)));
            $result_tr = $trans_load->send_command('get_ads_by_listid');
            if (!empty($result_tr)) {
                if (is_array($result_tr['list_id'])) {
                    for ($i = 0; $i < count($result_tr['list_id']); $i++) {
                        $ad_id = $result_tr['ad_id'][$i];
                        $list_id = $result_tr['list_id'][$i];
                        $new_ads_ids[$list_id] = $ad_id;
                    }
                } else {
                    $ad_id = $result_tr['ad_id'];
                    $list_id = $result_tr['list_id'];
                    $new_ads_ids[$list_id] = $ad_id;
                }
            }
        }
        return $new_ads_ids;
    }

    /**
     * Add basic params to validate using trans
     */
    private function populateParams($cmdConf, $prod_id, $ad_id, $params)
    {
        $new_params = array();
        if (!empty($ad_id)) {
            $new_params['ad_id'] = $ad_id;
        }

        $extraParams = Products::getExtraData(array("product_id" => $prod_id, "detail_params" => $params));
        foreach ($extraParams as $key => $value) {
            $new_params[$key] = $value;
        }

        if (Products::isLabel($prod_id)) {
            Logger::logDebug(__METHOD__, "Params for label = " . var_export($params, true));
            $new_params['label_type'] = Products::getLabelTypeName($params);
        }

        if (!Products::isPack2if($prod_id) && !Products::isInsertingFee($prod_id)) {
            $new_params['batch'] = "1";
        }
        $new_params['remote_addr'] = get_remote_addr();
        return $new_params;
    }

    /**
     * Validate products for ads
     * For Autofact products call to Yapofact healthCheck
     */
    protected function validateAdProduct($prod_id, $ad_id, $params)
    {
        if (Products::isAutofact($prod_id)) {
            // Autofact calls to MS Yapofact
            $healthcheck = $this->yapofact->healthCheck();
            return $healthcheck != null && $healthcheck->YapofactAPI === "OK";
        } elseif (!Products::isInsertingFeePremium($prod_id)) {
            $trans = $this->trans->reset();
            $cmdConf = Bconf::bconfGet($BCONF, "*.upselling_settings.command");
            Logger::logDebug(__METHOD__, "Product id to verify {$prod_id}");
            $productInfo = $this->populateParams($cmdConf, $prod_id, $ad_id, $params);

            // Populate purchase info for product
            foreach ($productInfo as $key => $val) {
                if (isset($val)) {
                    $trans->add_data($key, $val);
                }
            }

            // Populate default params for product
            $default_params = Bconf::getParams($cmdConf, $prod_id, "default_params");
            foreach ($default_params as $node) {
                $default = explode(':', $node['value']);
                if (in_array($default[0], array("commit"))) {
                    continue;
                }
                $trans->add_data($default[0], $default[1]);
            }

            $command = Bconf::getParams($cmdConf, $prod_id, "cmd");
            Logger::logDebug(__METHOD__, "Trans to call {$command}");

            $reply = $trans->validate_command($command);

            if ($trans->has_error(true)) {
                $errors = $trans->get_errors();
                Logger::logError(
                    __METHOD__,
                    "validation error: {$command} "
                    .var_export($errors, true)
                    ."params:"
                    .var_export($params, true)
                );
                return false;
            }
        }
        return true;
    }

    /**
     * Validate Packs & Stores using trans
     * Validate Credits using Ms & try to get the user balance
     */
    protected function validateOtherProduct($prod_id, $account_id)
    {
        if (Products::isPack($prod_id)) {
            $command = "insert_pack";
            $trans = $this->trans->reset();
            $trans->add_data("account_id", $account_id);
            $trans->add_data("product_id", $prod_id);
            $extraData = Products::getExtraData(array("product_id" => $prod_id));
            // Populate extraData (Optional) for product
            foreach ($extraData as $key => $value) {
                $trans->add_data($key, $value);
            }
            $reply = $trans->validate_command($command);

            if ($trans->has_error(true)) {
                $errors = $trans->get_errors();
                Logger::logError(__METHOD__, "validation error {$command} ".var_export($errors, true));
                return false;
            }
        } elseif (Products::isStore($prod_id)) {
            $store_id = $this->acc_session->get_param('store_id');
            $command = "create_store";
            $trans = $this->trans->reset();
            if (!empty($store_id)) {
                $command = "apply_product_to_store";
                $trans->add_data("store_id", $store_id);
                $ttl = Bconf::get($BCONF, "*.payment.product.{$prod_id}.expire_time");
                $trans->add_data("expire_time", $ttl);
            } else {
                $trans->add_data("account_id", $account_id);
            }
            $reply = $trans->validate_command($command);
            if ($trans->has_error(true)) {
                $errors = $trans->get_errors();
                Logger::logError(__METHOD__, "validation error {$command} ".var_export($errors, true));
                return false;
            }
        } elseif (Products::isCredits($prod_id)) {
            $balance = $this->credits->getBalance($this->account_number);
            if (!isset($balance) || $balance->Status !== "OK") {
                Logger::logError(__METHOD__, "Communication error with credits service");
                return false;
            }
        }

        return true;
    }
}
