<?php

use Yapo\Bconf;

require_once('JSON.php');

/**
 * Class to be used in order to store and get product(s) data of a purchase from an user
 */
class RedisMultiproductManager extends bRedisClass {
	const DEFAULT_KEY = "0";
	const PRICE = 'p';
	const LABEL_TYPE = 'lt';
	const AB_FREQ = 'abf';
	const AB_DATE_END = 'abd';
	const AB_USE_NIGHT = 'abn';

	private $products = Array();
	private $old_format;
	private $allow_multi;

	public function __construct(){
		parent::__construct();
		$definition_products = Bconf::bconfGet($BCONF, '*.payment.product');
		$this->old_format = Bconf::bconfGet($BCONF, '*.cart_handler.old_format');
		$this->allow_multi = Bconf::bconfGet($BCONF, '*.cart_handler.allow_multi_products');
		foreach($definition_products as $key => $value) {
			if(isset($value['group'])){
				$this->products [$key] = $value['group'];
			}
		}
	}

	/**
	 * @desc Used by the parent class to identify the redis server
	 */
	protected function getRedisRootNode() {
		return "*.common.redis_multiproduct.host.ra1";
	}

	private function get_group_members($product_id){
		$family_products = Bconf::bconfGet($BCONF, "*.cart_handler.family_products.$product_id");
		if(isset($family_products)){
			return explode(",", $family_products);
		}
		$elements = array();
		$group = Bconf::bconfGet($BCONF, "*.payment.product.$product_id.group");
		foreach($this->products as $key => $value) {
			if($value == $group) {
				$elements[] = $key;
			}
		}
		bLogger::logDebug(__METHOD__,  "group: $group ---- group_members:".print_r($elements, true));
		return $elements;
	}

	private function number_products_old_format(){
		$elements = 0;
		foreach($this->products as $key => $group) {
			if(in_array($group, $this->old_format)){
				$elements++;
			}
		}
		return $elements;
	}

	/**
	* @desc Delete stored data related to a Payment
	* @param $accountId the payment that the extra data belongs to
	* @return a true on success, false if not
	*/
	public function cleanData($accountId){
		$multi = $this->redis->multi();
		foreach($this->products as $product_id => $group){
			if(in_array($group, $this->old_format)){
				$keyName = $this->getRedisKeyName($accountId,$product_id);
				$multi->delete($keyName);
			}else{
				$keyName = $this->getRedisKeyName($accountId,self::DEFAULT_KEY);
				$multi->hdel($keyName,$product_id);
			}
		}
		return $multi->exec();
	}

	/**
	* @desc Generate a string to be used a redis key using a accountId and optionally a data key
	* @param $accountId the payment that the extra data belongs to
	* @param $key a data key name
	* @return a generated key name
	*/
	private function getRedisKeyName($accountId, $key = null){
		$prefix = Bconf::bconfGet($BCONF,'*.common.account_cart_info');
		$keyName = $prefix .":". $accountId;
		if ($key != null)
			$keyName = $keyName . ":" . $key;
		return $keyName;
	}

	/**
	 * @desc Get default time to live for all keys
	 */
	private function getttl(){
		if (! isset($this->ttl))
			$this->ttl = 2592000;
			# $this->ttl = Bconf::bconfGet($BCONF, '*.redis_multiproduct.redis_ttl');
		return $this->ttl;
	}

	/**
	 * @desc Get all ads selected in a product for an user
	 * @param accountId (email)
	 * @param productId (numeric)
	 * @return Unidimencional Array
	 */
	public function get($accountId, $productId){
		if(Bconf::bconfGet($BCONF, '*.payment.enabled') == 0){
			return Array();
		}
		$key = $this->getRedisKeyName($accountId,$productId);
		return $this->redis->hgetall($key);
	}

	/**
	 *NOTE: if you are using pack don't use this method
	 *
	 */
	private function clear_product_group($multi, $accountId, $productId, $listId) {
		global $BCONF;

		if (!isset($this->products[$productId])) {
			return;
		}

		$group = $this->products[$productId];
		$ad_id = "";

		if (in_array($group,$this->allow_multi)) {
			return;
		}

		if (in_array(Bconf::bconfGet($BCONF, "*.payment.product.$productId.group") , array("BUMP", "GALLERY", "LABEL"))) {
			$result = bsearch_search_vtree(Bconf::bconfGet($BCONF, "*.common.asearch"), "id:$listId");
			if (!empty($result)) {
				$ad_id = $result['ad_id'];
			}
		}
		elseif (Bconf::bconfGet($BCONF, "*.payment.product.$productId.group") == "UPSELLING") {
			$ad_id = $listId;
			$result = bsearch_search_vtree(Bconf::bconfGet($BCONF, "*.common.asearch"), "ad_id:$ad_id");
			if (!empty($result)) {
				$listId = $result['list_id'];
			}
		}

		if (in_array($group, $this->old_format)) {
			foreach($this->get_group_members($productId) as $product_id) {
				if(!empty($ad_id) && Bconf::bconfGet($BCONF, "*.payment.product.$product_id.group") == "UPSELLING"){
					$multi->hdel($this->getRedisKeyName($accountId, $product_id), $ad_id);
				}
				else {
					$multi->hdel($this->getRedisKeyName($accountId, $product_id), $listId);
				}
			}
		}
		else {
			foreach($this->get_group_members($productId)  as $product_id) {
				$multi->hdel($this->getRedisKeyName($accountId, self::DEFAULT_KEY), $product_id);
			}
		}
	}

	private function add_product($multi, $accountId, $productId, $listId, $values) {
		$group = $this->products[$productId];
		if (in_array($group, $this->old_format)) {
			$key = $this->getRedisKeyName($accountId, $productId);
			$multi->hset($key, $listId, $values);
		}else{
			$key = $this->getRedisKeyName($accountId, self::DEFAULT_KEY);
			$multi->hset($key, $productId, $values);
		}
	}

	private function del_product($multi, $accountId, $productId, $listId) {
		$group = $this->products[$productId];
		if (in_array($group, $this->old_format)) {
			$key= $this->getRedisKeyName($accountId,$productId);
			return $multi->hdel($key, $listId);
		}else{
			$key = $this->getRedisKeyName($accountId, self::DEFAULT_KEY);
			return $multi->hdel($key, $productId);
		}
	}

	// this function enqueues commands, that later on format_products will exec
	private function query_products($multi, $accountId) {
		$multi->hgetall($this->getRedisKeyName($accountId, self::DEFAULT_KEY));
		foreach($this->products as $product_id => $group) {
			if (in_array($group, $this->old_format)) {
				$multi->hgetall($this->getRedisKeyName($accountId, $product_id));
			}
		}
	}

	private function quantity_of_elements($prod_id, &$products){
		if($this->products[$prod_id] == "PACK"){
			$key_product = $prod_id / 10000;
			$key_product = round($key_product, 0, PHP_ROUND_HALF_DOWN) * 10000;
			if(isset($products[$key_product])){
				$products[$key_product] += 1;
			}else{
				$products[$key_product] = 1;
			}
		}
		else{
			$products[$prod_id] = 1;
		}
	}

	private function format_products($multi, $with_ads = false) {
		$raw = $multi->exec();
		$total = 0;
		$prods = 0;
		$ads = array();
		$products = array();
		$n_products = $this->number_products_old_format() + 1;
		$redis_products = array_slice($raw, -$n_products, $n_products);

		foreach ($redis_products[0] as $prod_in_redis => $values) {
			$decoded_values = (array)yapo_json_decode($values);
			$ads[self::DEFAULT_KEY][] = array($prod_in_redis, $decoded_values[self::PRICE]);
			$this->quantity_of_elements($prod_in_redis,$products);
			$total += $decoded_values[self::PRICE];
			$prods ++;
		}

		$counter = 1;
		foreach ($this->products as $product_id => $group) {
			if (in_array($group, $this->old_format)) {
				$c = count($redis_products[$counter]);
				if ($c > 0) {
					$prods += $c;
					$products[$product_id] = $c;
					foreach ($redis_products[$counter] as $ad => $values) {
						if (!isset($ads[$ad])) {
							$ads[$ad] = array();
						}
						$decoded_values = (array)yapo_json_decode($values);
						$total += $decoded_values[self::PRICE];
						$params = 0;
						if(isset($decoded_values[self::LABEL_TYPE])){
							$params = intval($decoded_values[self::LABEL_TYPE]);
						}
						if(isset($decoded_values[self::AB_FREQ]) && isset($decoded_values[self::AB_DATE_END])){
							$params = 'frequency:'.$decoded_values[self::AB_FREQ]
								.';num_days:'.$decoded_values[self::AB_DATE_END]
								.';use_night:'.$decoded_values[self::AB_USE_NIGHT];
						}
						$ads[$ad][] = array($product_id,$params);
					}
				}
				$counter++;
			}
		}
		$output = array(
			"prod" => $products,
			"count" => $prods,
			"total" => $total
		);
		if ($with_ads) {
			$output['ads'] = $ads;
		}
		return $output;
	}

	public function select($accountId, $productId, $listId, $values, $with_ads = false) {
		if(Bconf::bconfGet($BCONF, '*.payment.enabled') == 0){
			return Array();
		}
		$multi = $this->redis->multi();
		$this->clear_product_group($multi, $accountId, $productId, $listId);
		$this->add_product($multi, $accountId, $productId, $listId, $values);
		$this->query_products($multi, $accountId, $productId);
		$result = $this->format_products($multi, $with_ads);
		return $result;
	}

	public function unselect($accountId, $productId, $listId, $group = false) {
		if(Bconf::bconfGet($BCONF, '*.payment.enabled') == 0){
			return Array();
		}
		$multi = $this->redis->multi();
		if($group){
			$this->clear_product_group($multi, $accountId, $productId, $listId);
		}else{
			$this->del_product($multi, $accountId, $productId, $listId);
		}
		$this->query_products($multi, $accountId, $productId);
		$result = $this->format_products($multi);
		return $result;
	}

	public function unselect_multiple($accountId, $productListArray, $group = false) {
		foreach ($productListArray as $productId => $listIdList) {
			foreach ($listIdList as $listId) {
				$this->unselect($accountId, $productId, $listId, $group);
			}
		}
	}

	public function list_all($accountId) {
		if(Bconf::bconfGet($BCONF, '*.payment.enabled') == 0){
			return Array();
		}
		$multi = $this->redis->multi();
		$this->query_products($multi, $accountId);
		$result = $this->format_products($multi, true);
		return $result;
	}

	public function removeAllAdProducts($accountId, $listId) {
		$multi = $this->redis->multi();
		foreach($this->products as $k => $v){
			if (Bconf::get($BCONF, "*.payment.product." . $k . ".is_for_ads") == "1") {
				$key = $this->getRedisKeyName($accountId, $k);
				$multi->hdel($key, $listId);
			}
		}
		$multi->exec();
	}

	public function removeInsertingFeeProducts($accountId, $listId) {
		$multi = $this->redis->multi();
		foreach ($this->products as $k => $v) {
			if (Bconf::get($BCONF, "*.payment.product." . $k . ".group") == "INSERTING_FEE") {
				$key = $this->getRedisKeyName($accountId, $k);
				$multi->hdel($key, $listId);
			}
		}
		$multi->exec();
	}

	public function isBuyingStore($accountId) {
		$new_products = $this->redis->hgetall($this->getRedisKeyName($accountId, self::DEFAULT_KEY ));
		if(empty($new_products))
			return false;

		foreach($this->get_group_members('4') as $product_id) {
			if(array_key_exists($product_id, $new_products)){
				return true;
			}
		}
		return false;
	}

	/**
	 * Utilitary functions to manage the time inter rebuild-indexes
	 */
	public function addPendingProducts($email,$prod,$listId){
		$listName = $this->getPendingKeyName($prod,$email);
		$this->redis->sadd($listName, $listId);
		$this->redis->setTimeout($listName,Bconf::bconfGet($BCONF, '*.payment.product_specific.buylock.ttl'));
	}

	public function addAllPendingProducts($email, $prodListIds){
		foreach ($prodListIds as $prod => $ads) {
			foreach($ads as $listId){
				$this->addPendingProducts($email,$prod,$listId);
			}
		}
	}
	public function getAdsWithPendingProducts($email) {
		$adsList = array();
		foreach($this->products as $key => $value){
			if(in_array($value, $this->old_format)){
				$listName = $this->getPendingKeyName($key,$email);
				if($this->redis->exists($listName)) {
					$adsList[$key] = $this->redis->smembers($listName);
				}
			}
		}
		return $adsList;
	}

	public function removeAdsWithPendingProducts($email,$prod,$listId) {
		$listName = $this->getPendingKeyName($prod,$email);
		return $this->redis->sRem($listName , $listId);
	}

	public function checkProductsPending($email,$listAds){
		$pendings = $this->getAdsWithPendingProducts($email);
		$output = array();
		foreach ($listAds as $value) {
			foreach($pendings as $product =>$list){
				if(in_array($value,$list)){
					@$output[$product][] = $value;
				}
			}
		}
		return $output;
	}

	private function getPendingKeyName($product,$email){
		$key = Bconf::bconfGet($BCONF,'*.payment.product_specific.buylock.key');
		$replace_keys = array('PRODUCT','EMAIL');
		$replace_values = array($product,$email);
		return str_replace($replace_keys,$replace_values,$key);
	}
}
