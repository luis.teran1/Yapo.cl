<?php

namespace Yapo;

class CartApi extends Restful
{

    public function __construct()
    {
        $this->restAPISettingsKey = "*.cart_handler_rest_settings";
    }
}
