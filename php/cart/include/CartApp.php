<?php

namespace Yapo;

class CartApp extends CommonCart
{
    public function putProduct($url_data, $request_data)
    {
        if (!$this->active) {
            return $this->getError("login");
        }
        Logger::logDebug(__METHOD__, "url_data = " . var_export($url_data, true));
        $data = $this->getDataFromRequest($url_data);

        if (!empty($data->account_id) && !empty($data->product_id)) {
            $product = Bconf::getProductConf($BCONF, $data->product_id);
            if ($product === false) {
                 return $this->getError("service");
            }
            list($validated, $missing_param) = $this->validateParams($data);
            if (!$validated) {
                return $this->getError("params", $missing_param);
            }

            if (in_array($product->group, Bconf::bconfGet($BCONF, "*.cart_handler.old_format"))) {
                $params = $this->getProductAdParams($data);
            } else {
                if ($product->group === "CREDITS") {
                    $min = $product->min_quantity;
                    $max = $product->max_quantity;
                    if (!is_numeric($data->credits) || $data->credits < $min || $max < $data->credits) {
                        return $this->getError("range");
                    }
                    $exchange_rate = $product->exchange_rate;
                    $params[self::PRICE] = $exchange_rate * $data->credits;
                } else {
                    $params[self::PRICE] = $product->price;
                }
            }
        } else {
            return $this->getError("product");
        }
        if (empty($params[self::PRICE])) {
            return $this->getError("price");
        }

        $response = $this->rmm->select($data->account_id, $data->product_id, $data->list_id, json_encode($params));
        return $response;
    }

    public function dropProduct($url_data, $request_data)
    {
        if (!$this->active) {
            return $this->getError("login");
        }
        Logger::logDebug(__METHOD__, "url_data = " . var_export($url_data, true));
        $data = $this->getDataFromRequest($url_data);
        if (!isset($data->account_id) || !isset($data->product_id)) {
            return $this->getError("product");
        }
        $response = $this->rmm->unselect($data->account_id, $data->product_id, $data->list_id, $data->group);
        return $response;
    }

    public function listProduct($url_data, $request_data)
    {
        if (!$this->active) {
            return $this->getError("login");
        }
        Logger::logDebug(__METHOD__, "url_data = " . var_export($url_data, true));
        $data = $this->getDataFromRequest($url_data);
        if (!isset($data->account_id)) {
            return $this->getError("account");
        }
        $ret = $this->rmm->list_all($data->account_id);
        $ret = $ret['ads'];
        $ret['status'] = 'OK';
        return $ret;
    }

    public function getProduct($url_data, $request_data)
    {
        if (!$this->active) {
            return $this->getError("login");
        }
        Logger::logDebug(__METHOD__, "url_data = " . var_export($url_data, true));
        $data = $this->getDataFromRequest($url_data);
        if (!isset($data->account_id)) {
            return $this->getError("account");
        }
        if (isset($data->product_id)) {
            $ret = $this->rmm->get($data->account_id, $data->product_id);
            return $response = array('status' => 'OK', 'data' => $ret);
        } else {
            $ret = $this->rmm->list_all($data->account_id);
            return $ret;
        }
    }

    public function validateProducts($url_data, $request_data)
    {
        if (!$this->active) {
            return $this->getError("login");
        }
        Logger::logDebug(__METHOD__, "url_data = " . var_export($url_data, true));
        $data = $this->getDataFromRequest($url_data);
        if (!isset($data->account_id)) {
            return $this->getError("account");
        }
        list(
            $list_ids,
            $ad_ids,
            $list_id_prod_params,
            $ad_id_prod_params,
            $other_products_ids) = $this->getProductsFromCart($data->account_id);

        $ads_from_list = $this->getAdIds($list_ids);

        $new_ads_ids = array();
        $listIdFromAdId = array();

        foreach ($ads_from_list as $lid => $aid) {
            $new_ads_ids[] = array('aid' => intval($aid), 'prods' => $list_id_prod_params[$lid]);
            $listIdFromAdId[$aid] = $lid;
        }

        foreach (array_keys($ad_ids) as $ad_id) {
            $new_ads_ids[] = array('aid' => intval($ad_id), 'prods' => $ad_id_prod_params[$ad_id]);
        }

        $errors = array();
        foreach ($new_ads_ids as $ad) {
            foreach ($ad['prods'] as $product => $param) {
                $is_ok = $this->validateAdProduct($product, $ad['aid'], $param);
                if ($is_ok !== true) {
                    $lid = $listIdFromAdId[$ad["aid"]];
                    $product_conf = Bconf::getProductConf($BCONF, $product);
                    if (isset($lid) && $product_conf->get_ad_by == "list_id") {
                        $errors["{$lid}-{$product}"] = $is_ok;
                        $this->rmm->unselect($data->account_id, $product, $lid);
                    } else {
                        $errors["{$ad['aid']}-{$product}"] = $is_ok;
                        $this->rmm->unselect($data->account_id, $product, $ad['aid']);
                    }
                }
            }
        }

        foreach ($other_products_ids as $prod) {
            $prod_id = $prod[0];
            $is_ok = $this->validateOtherProduct($prod_id, $data->account_number);
            if ($is_ok !== true) {
                $errors["0-{$prod_id}"] = $is_ok;
                $this->rmm->unselect($data->account_id, $prod_id, 0);
            }
        }

        $output = array();
        if (!empty($errors)) {
            $output['errors'] = $errors;
            $output['status'] = 'ERROR';
        } else {
            $output['status'] = 'OK';
        }
        return $output;
    }
}
