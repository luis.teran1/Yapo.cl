<?php

namespace Yapo;

require_once 'autoload_lib.php';

//Syslog
openlog('cart', LOG_ODELAY, LOG_LOCAL0);

$api = new CartApi();
$api->run();
