<?php
require_once('autoload_lib.php');
include_once('./httpful.phar');
include_once('JSON.php');

openlog("msg_widget_proxy", LOG_ODELAY, LOG_LOCAL0);

$msg_widget = new MsgWidgetProxy();
$msg_widget->run_fsm();

