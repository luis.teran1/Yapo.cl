<?php

require_once 'autoload_lib.php';
require_once('init.php');
require_once('Servipag.php');
openlog("paysim", LOG_ODELAY, LOG_LOCAL0);

use Yapo\EasyRedis;

global $BCONF;
/*webpay plus voucher */
if (isset($_GET["voucher"])) {
    $token_ws = $_POST["token_ws"];
    $redis = new EasyRedis("redis_payment");
    $redis_key = $redis->prepend.$token_ws."_paysim";

    $jsonInfo = $redis->get($redis_key);
    $dataPaySim = json_decode($jsonInfo, true);
    $data = array(
        "token_ws" => $token_ws,
    );
    foreach ($dataPaySim as $key => $val) {
        $data[$key] = $val;
    }
    call_template(
        $data,
        array(),
        $GLOBALS['BCONF']['*'],
        "paysim/paysim_transbank_voucher.html",
        default_template_options()
    );
} elseif (isset($_POST["token_ws"])) {
    /* Transbank Simulator */
    $token_ws = $_POST["token_ws"];
    $redis = new EasyRedis("redis_payment");
    $redis_key = $redis->prepend.$token_ws."_paysim";

    $jsonInfo = $redis->get($redis_key);
    $dataPaySim = json_decode($jsonInfo);
    $data = array(
        "tbk_type" => "NORMAL",
        "tbk_amount" => $dataPaySim->amount,
        "tbk_order" => $dataPaySim->buyOrder,
        "tbk_session" => $dataPaySim->sessionId,
        "tbk_surl" => $dataPaySim->finishURL,
        "tbk_furl" => $dataPaySim->returnURL,
        "token_ws" => $token_ws
    );

    if (isset($_POST["tbk_ok"]) || isset($_POST["tbk_cancel"]) || isset($_POST["tbk_voided"])) {
        $redirect_url = $dataPaySim->finishURL;
        if (isset($_POST["tbk_ok"])) {
            $data["vci"] = "TSY";
            $data["responseCode"] = 0;
        }

        if (isset($_POST["tbk_cancel"])) {
            $data["vci"] = "ERR";
            $data["responseCode"] = 1;
        }

        if (isset($_POST["tbk_ok"]) || isset($_POST["tbk_cancel"])) {
            $post_data = array("token_ws" => $token_ws);
            $codigo_comercio = 30362381;
            $data["commerceCode"] = $codigo_comercio;
            $data["transactionDate"] = date("Y-m-d H:i:s");
            $data["authorizationCode"] = strval(rand(100000, 999999));
            $data["paymentTypeCode"] = $_POST["TBK_TIPO_PAGO"];
            $data["sharesNumber"] = $_POST["TBK_NUMERO_CUOTAS"];
            $data["cardNumber"] = strval(rand(1000, 9999));
            $redirect_url = $dataPaySim->returnURL;
        } else {
            $post_data = array("TBK_TOKEN" => $token_ws, "TBK_ORDEN_COMPRA" => $dataPaySim->buyOrder);
        }

        $dataPaySimJson = json_encode($data);
        $jsonInfo = $redis->setex($redis_key, 600, $dataPaySimJson);
        redirect_post($redirect_url, $post_data);
    }
    call_template($data, array(), $GLOBALS['BCONF']['*'], "paysim/paysim_transbank.html", default_template_options());
} elseif (isset($_POST["TBK_ID_SESION"])) {
    $data = array(
        "tbk_type" => $_POST["TBK_TIPO_TRANSACCION"],
        "tbk_amount" => $_POST["TBK_MONTO"],
        "tbk_order" => $_POST["TBK_ORDEN_COMPRA"],
        "tbk_session" => $_POST["TBK_ID_SESION"],
        "tbk_surl" => $_POST["TBK_URL_EXITO"],
        "tbk_furl" => $_POST["TBK_URL_FRACASO"]
    );

    if (isset($_POST["tbk_ok"]) || isset($_POST["tbk_cancel"]) || isset($_POST["tbk_voided"])) {
        $post_string = "TBK_ID_SESION=".$data["tbk_session"]
        ."&TBK_MONTO=".$data["tbk_amount"]
        ."&TBK_ORDEN_COMPRA=".$data["tbk_order"];
        $r = "";

        if (isset($_POST["tbk_ok"])) {
            $post_string .="&TBK_ACCION=ACK&TBK_VCI=ACK&TBK_RESPUESTA=0";
        }

        if (isset($_POST["tbk_cancel"])) {
            $post_string .="&TBK_ACCION=ERR&TBK_VCI=ERR&TBK_RESPUESTA=1";
        }

        if (isset($_POST["tbk_ok"]) || isset($_POST["tbk_cancel"])) {
            $codigo_comercio = 30362381;

            $post_string .="&TBK_ID_TRANSACCION=".generateRandomNumber(20);
            $post_string .="&TBK_TIPO_TRANSACCION=TR_NORMAL";
            $post_string .="&TBK_CODIGO_COMERCIO=".$codigo_comercio;
            $post_string .="&TBK_CODIGO_COMERCIO_ENC=".sha1($codigo_comercio);

            $post_string .="&TBK_FECHA_CONTABLE=".date('md');
            $post_string .="&TBK_FECHA_EXPIRACION=".date('ym');
            $post_string .="&TBK_CODIGO_AUTORIZACION=".rand(100000, 999999);
            $post_string .="&TBK_FECHA_TRANSACCION=".date('md');
            $post_string .="&TBK_HORA_TRANSACCION=".date('His');
            $post_string .="&TBK_TIPO_PAGO=".$_POST["TBK_TIPO_PAGO"];
            $post_string .="&TBK_NUMERO_CUOTAS=".$_POST["TBK_NUMERO_CUOTAS"];
            $post_string .="&TBK_FINAL_NUMERO_TARJETA=".rand(1000, 9999);
            $post_string .="&TBK_MAC=".generateRandomString(4096);

            $xt_url = bconf_get($BCONF, "*.common.base_url.ai");
            //Verify if the payment come from telesales module
            if (substr_count($data["tbk_furl"], "ts_pagos") > 0 || substr_count($data["tbk_surl"], "ts_pagos") > 0) {
                $xt_url .= "/third_party_ts/authorize";
            } else {
                $xt_url .= "/third_party_tbk/authorize";
            }
            list($r,$http_code) = curl_response($xt_url, $post_string);
            bLogger::logDebug(__METHOD__, "Paysim curl: " . print_r($r, true));
        }

        $post_data =array("TBK_ID_SESION"=> $data["tbk_session"],"TBK_ORDEN_COMPRA" => $data["tbk_order"]);
        if (strstr($r, "ACEPTADO") && isset($_POST["tbk_ok"])) {
            redirect_post($_POST['TBK_URL_EXITO'], $post_data);
        } elseif (strstr($r, "RECHAZADO") || isset($_POST["tbk_cancel"]) || isset($_POST["tbk_voided"])) {
            redirect_post($_POST['TBK_URL_FRACASO'], $post_data);
        } else {
            echo "<h3>The response is not recognized. Check the parameters</h3><br/>";
            if (isset($_POST)) {
                echo "<h3>Parameters:</h3><br/>".var_export($_POST);
            }
            if (isset($r)) {
                echo "<br/><h3>Curl response:</h3><br/>".var_export($r);
            }
        }
    }

    call_template($data, array(), $GLOBALS['BCONF']['*'], "paysim/paysim_transbank.html", default_template_options());
} elseif (isset($_POST["xml"])) {
    /* Servipag Simulator XML1 and XML2 */
    $data = array(
        "xml" => $_POST["xml"],
    );
    bLogger::logDebug(__METHOD__, "Xml1 from yapo: " . print_r($_POST["xml"], true));
    $xml1 = new ServipagXML1Simulator($_POST["xml"]);

    if ($xml1->verify_signature()) {
        if (isset($_POST["tbk_ok"])) {
            $xml2 = new ServipagXML2Simulator(
                $xml1->IdTxPago,
                generateRandomNumber(8),
                $xml1->Identificador,
                $xml1->FechaPago,
                $xml1->Boleta,
                $xml1->Monto
            );
            $post_string = "xml=".urlencode($xml2->build_xml());
            $xt_url = bconf_get($BCONF, "*.common.base_url.ai");
            $xt_url .= "/third_party_servipag/authorize";
            $r = "";
            if (isset($_POST['send_xml2'])) {
                for ($i = 0; $i < (int)bconf_get($BCONF, "*.payment.servipag.retry"); $i++) {
                    list($r, $http_code) = curl_response($xt_url, $post_string);
                    bLogger::logDebug(__METHOD__, "retry $i xml3 from yapo: " . print_r($r, true));
                }
                $xml3 = new ServipagXML3Simulator($r);

                if ($xml3->CodigoRetorno == "0") {
                    $xml4 = new ServipagXML4Simulator($xml2->IdTrxServipag, $xml2->IdTxCliente, 0, "OK");
                    $post_data =array("xml"=> $xml4->build_xml());
                    redirect_post(bconf_get($BCONF, "*.payment.servipag.url_success"), $post_data);
                } else {
                    bLogger::logDebug(__METHOD__, "Yapo response xml3, Error code: " . print_r($xml3, true));
                    echo "<h3>The response is not recognized. Check the parameters</h3><br/>";
                    if (isset($_POST)) {
                        echo "<h3>Parameters:</h3><br/>".var_export($_POST);
                    }
                    if (isset($r)) {
                        echo "<br/><h3>Curl response:</h3><br/>".var_export($r);
                    }
                }
            } else {
                $xml4 = new ServipagXML4Simulator($xml2->IdTrxServipag, $xml2->IdTxCliente, 0, "OK");
                $post_data =array("xml"=> $xml4->build_xml());
                redirect_post(bconf_get($BCONF, "*.payment.servipag.url_success"), $post_data);
            }
        }
    } else {
        bLogger::logDebug(__METHOD__, "Paysim invalid signature xml1 " . print_r($data, true));
    }

    call_template($data, array(), $GLOBALS['BCONF']['*'], "paysim/paysim_servipag.html", default_template_options());
} elseif (isset($_REQUEST['notification_token'])) {
    /**
     * Khipu simulation
     */
    $token = $_REQUEST['notification_token'];
    $version = $_REQUEST['notify_api_version'];

    $data = array(
        'notification_token' => $token,
        'notify_api_version' => $version
    );

    // Get the info from khipu dummy
    $confUrl = "*.common.base_url.payment_rest_api";
    $confMethods = "*.payment_rest_api_client.methods.";

    $apiClient = new Yapo\ApiClient($confUrl, $confMethods, true);

    $params = "api_version=$version&notification_token=$token";
    $response = $apiClient->call("getKhipu", $params);
    bLogger::logDebug(__METHOD__, "Payment rest response".$response);

    $data['response_api'] = (string)$response;
    $response_decoded = json_decode($response, true);
    //  And put into data
    $data['amount'] = $response_decoded['amount'];
    $data['order'] = $response_decoded['transaction_id'];
    $data['success_url'] = $response_decoded['return_url'];
    $data['failure_url'] = $response_decoded['cancel_url'];
    // get notification url from payment dummy data
    $notification_url = $response_decoded['notify_url'];


    // IF the user send tbk_ok or cancel
    if (isset($_POST['tbk_ok']) || isset($_POST['tbk_cancel'])) {
        $post_data = array();
        if (isset($_POST['tbk_ok'])) {
            // Complete payment on khipu dummy data
            $complete_params= array(
                'notification_token' => $token,
                'api_version' => $version
            );
            $response = $apiClient->call("payKhipu", $complete_params);
            $data['response_api'] = (string)$response;
            // Send notification
            $post_string = "api_version=$version&notification_token=$token";
            list ($r, $http_code) = curl_response($notification_url, $post_string);

            bLogger::logDebug(__METHOD__, "Paysim curl: " . print_r($r, true));
            if ($http_code != 200) {
                redirect_post($_POST['failure_url'], $post_data);
            } else {
                redirect_post($_POST['success_url'], $post_data);
            }
        }
        if (isset($_POST['tbk_cancel'])) {
            redirect_post($_POST['failure_url'], $post_data);
        }
    }
    call_template($data, array(), $GLOBALS['BCONF']['*'], 'paysim/paysim_khipu.html', default_template_options());
} else {
    bLogger::logDebug(__METHOD__, "POST vars: " . print_r($_POST, true));
    if (isset($_GET["paysim"])) {
        $paylog = fopen($BCONF['*']['common']['basedir'] . "/logs/paysim.log", "a");
        $phone = explode('-', $_GET['a_number']);
        if (count($phone) == 1) {
            $phone = array ('', $phone[0]);
        }
        fwrite(
            $paylog,
            sprintf(
                "%-15s%-15s%-15s%-15s%-15s%-15s\n",
                $_GET["code"],
                $_GET["amount"],
                $phone[0],
                $phone[1],
                date("ymd"),
                date("H:i:s")
            )
        );
        fclose($paylog);
    }
    call_template(array(), array(), $GLOBALS['BCONF']['*'], "paysim/paysim.html", default_template_options());
}

function curl_response($xt_url, $post_string)
{

    $da_port = $GLOBALS['BCONF']['*']['common']['http_prefork_port'];
    $c = curl_init($xt_url);
    curl_setopt($c, CURLOPT_POST, 1);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_PORT, $da_port);
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($c, CURLOPT_HEADER, 0);
    curl_setopt($c, CURLOPT_POSTFIELDS, $post_string);
    curl_setopt($c, CURLOPT_VERBOSE, 1);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($c, CURLOPT_TIMEOUT, 25);

    $r = curl_exec($c);

    // Check if any error occurred
    if (curl_errno($c)) {
        echo 'Curl error: ' . curl_error($c);
        bLogger::logError(__METHOD__, "Paysim curl: ".curl_error($c));
        die();
    }


    $http_code = curl_getinfo($c, CURLINFO_HTTP_CODE);
    curl_close($c);

    return array($r, $http_code);
}

function redirect_post($url, array $data)
{
    ?>
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <link rel="stylesheet" type="text/css" href="css/paysim.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script type="text/javascript">
    function closethisasap (){
        document.forms["redirectpost"].submit();
    }
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    </head>
    <body>
        <div class="paysim-head">
            <h1>You are being redirected</h1>
        </div>
        <div class="main-wrap">
            <p>URL: <?php echo $url;?></p>
            <p class="be-happy">PLEASE BE HAPPY</p>
            <form name="redirectpost" method="post" action="<?php echo $url; ?>" >
            <?php
            if (!is_null($data)) {
                foreach ($data as $k => $v) {
                    echo '<input type="hidden" name="'.$k.'" value="'.htmlentities($v).'"> ';
                }
            }
            ?>
                <input  id="continue"
                        type="button"
                        class="btn btn-accept"
                        name="tbk_next"
                        value="Goto the site"
                        onclick="closethisasap()"
                />
            </form>
            </div>
        </body>
        </html>
        <?php
        exit();
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
function generateRandomNumber($length = 10)
{
    $characters = '0123456789';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}
?>
