<?php
namespace Yapo;

use Yapo\Packs;

class Upselling
{
    /**
     * showUpselling
     *
     * Determines whether or not Upselling should be displayed to a user
     * on AdInsert/AdEdit.
     *
     * @param array  $BCONF Array of available bconf
     * @param bool   $userLogged Indicates whether the user is logged in
     * @param string $userProCategories Comma separated list of categories where the user is pro at
     * @param bool   $adHasProduct Indicates whether the ad has products currently applied
     * @return bool  Whether or not upselling should be displayed with given parameters
     */
    public static function showUpselling($BCONF, $userLogged, $userProCategories, $adHasProducts, $userHasSlots, $packUpsellingEnabled)
    {
        /* Unlogged users get upselling */
        if (!$userLogged) {
            return true;
        }

        /* Pack categories never get upselling */
        $cats = explode(',', $userProCategories);
        foreach ($cats as $cat) {
            if ((Categories::isPackCategory($BCONF, $cat) && !($userHasSlots > 1 && $packUpsellingEnabled)) || Categories::isInsertingFeeCategory($BCONF, $cat)) {
                return false;
            }
        }

        /* Don't hush users that already have bought products */
        if ($adHasProducts) {
            return false;
        }

        /* Everybody else gets it */
        return true;
    }
}

