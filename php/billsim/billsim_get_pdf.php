<?php
require_once('init.php');
global $BCONF;

function ObtenerPDF($ObtenerPDF) {
	// $ObtenerPDF->token
	// $ObtenerPDF->identificadorArchivo

	// handle the generated tokens, redis maybe, file?
	// handle the generated token ttl
	// ArgumentNullException
	// ArgumentException
	// ApplicationException
	// ExpiredSessionException
	// UnauthorizedException
	// InternalException

	$filename = bconf_get($BCONF, "*.common.basedir") . "/apps/bill.pdf";
	$contents = base64_encode(file_get_contents($filename));

	return (object) array("ObtenerPDFResult" => $contents);
}
