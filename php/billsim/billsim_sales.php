<?php

require_once('nusoap/nusoap.php');
require_once('billsim_common.php');
require_once('billsim_get_pdf.php');
require_once('billsim_load.php');
require_once('init.php');
global $BCONF;

$server = new soap_server();
$ns = bconf_get($BCONF, "*.common.base_url.ai");
$server->configureWSDL('ArchivoDeVentasElectronicas', $ns);
$server->wsdl->schemaTargetNamespace = $ns;

$server->wsdl->addComplexType(
    'ObtenerPDF',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'token' => array('name'=>'token','type'=>'xsd:string'),
	'identificadorArchivo' => array('name'=>'identificadorArchivo','type'=>'xsd:int'),
    )
);

$server->wsdl->addComplexType(
    'ObtenerPDFResponse',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'ObtenerPDFResult' => array('name'=>'ObtenerPDFResult','type'=>'xsd:base64Binary'),
    )
);

$server->wsdl->addComplexType(
    'CargarYEmitir',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
        'token' => array('name'=>'token','type'=>'xsd:string'),
        'archivo' => array('name'=>'archivo','type'=>'xsd:base64Binary'),
        'opcionFolios' => array('name'=>'opcionFolios','type'=>'xsd:int'),
        'opcionRutClienteExiste' => array('name'=>'opcionRutClienteExiste','type'=>'xsd:int'),
        'opcionRutClienteNoExiste' => array('name'=>'opcionRutClienteNoExiste','type'=>'xsd:int'),
    )
);

$server->wsdl->addComplexType(
    'CargarYEmitirResult',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'any' => array('name'=>'any','type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'CargarYEmitirResponse',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'CargarYEmitirResult' => array('name'=>'CargarYEmitirResult','type'=>'tns:CargarYEmitirResult'),
    )
);

$server->wsdl->addComplexType(
    'ConsultaResultadoDocumento',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
        'token' => array('name'=>'token','type'=>'xsd:string'),
        'folioCargado' => array('name'=>'folioCargado','type'=>'xsd:int'),
        'codigoSII' => array('name'=>'codigoSII','type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'ConsultaResultadoDocumentoResult',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'any' => array('name'=>'any','type'=>'xsd:string'),
    )
);

$server->wsdl->addComplexType(
    'ConsultaResultadoDocumentoResponse',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'ConsultaResultadoDocumentoResult' => array('name'=>'ConsultaResultadoDocumentoResult','type'=>'tns:ConsultaResultadoDocumentoResult'),
    )
);

$server->register('ConsultaResultadoDocumento',
	array('ConsultaResultadoDocumento' => 'tns:ConsultaResultadoDocumento'),
	array('return' => 'tns:ConsultaResultadoDocumentoResponse'),
	$ns);

$server->register('CargarYEmitir',
	array('CargarYEmitir' => 'tns:CargarYEmitir'),
	array('return' => 'tns:CargarYEmitirResponse'),
	$ns);

$server->register('ObtenerPDF',
	array('ObtenerPDF' => 'tns:ObtenerPDF'),
	array('return' => 'tns:ObtenerPDFResponse'),
	$ns);

$server->service(get_post_data());
