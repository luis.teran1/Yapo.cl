<?php

function get_post_data() {
	global $HTTP_RAW_POST_DATA;

	if(@$HTTP_RAW_POST_DATA)
		return $HTTP_RAW_POST_DATA;

	return file_get_contents("php://input");
}
