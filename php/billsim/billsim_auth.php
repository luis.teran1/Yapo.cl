<?php

function Autenticar($Autenticar) {
	// $Autenticar->rutCliente
	// $Autenticar->rutUsuario
	// $Autenticar->contrasena
	// $Autenticar->sistema
	// $Autenticar->numeroSerie

	// handle the generated tokens, redis maybe, file?
	// handle the generated token ttl
	// ArgumentNullException
	// ArgumentException
	// UnauthorizedException
	// InternalException
        return (object) array("AutenticarResult" => generateRandomString(64));
}

function generateRandomString($length = 64) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/+';
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $randomString;
}
