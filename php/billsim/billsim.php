<?php

require_once('nusoap/nusoap.php');
require_once('billsim_common.php');
require_once('billsim_auth.php');
require_once('init.php');
global $BCONF;

$server = new soap_server();
$ns = bconf_get($BCONF, "*.common.base_url.ai");
$server->configureWSDL('Autenticador', $ns);
$server->wsdl->schemaTargetNamespace = $ns;

$server->wsdl->addComplexType(
    'Autenticar',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'rutCliente' => array('name'=>'rutCliente','type'=>'xsd:string'),
	'rutUsuario' => array('name'=>'rutUsuario','type'=>'xsd:string'),
	'contrasena' => array('name'=>'contrasena','type'=>'xsd:string'),
	'sistema' => array('name'=>'sistema','type'=>'xsd:unsignedByte'),
	'numeroSerie' => array('name'=>'numeroSerie','type'=>'xsd:int')
    )
);

$server->wsdl->addComplexType(
    'AutenticarResponse',
    'complexType',
    'struct',
    'sequence',
    '',
    array(
	'AutenticarResult' => array('name'=>'AutenticarResult','type'=>'xsd:string'),
    )
);

$server->register('Autenticar',
	array('Autenticar' => 'tns:Autenticar'),
	array('return' => 'tns:AutenticarResponse'),
	$ns);

$server->service(get_post_data());
