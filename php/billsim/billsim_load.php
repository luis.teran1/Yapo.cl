<?php

require_once('init.php');
openlog("PAYMENT_VERIFY", LOG_ODELAY, LOG_LOCAL0);

function ConsultaResultadoDocumento($params) {


	$folio = $params["folioCargado"];
	$purchases_to_timeout = explode(",", bconf_get($BCONF, '*.billsim.fail_by_timeout.ids'));
	if (in_array($folio, $purchases_to_timeout)){
		return;
	}
	$bills_to_fail = explode(",", bconf_get($BCONF, '*.billsim.purchase_with_bills.ids'));
	$invoices_to_fail = explode(",", bconf_get($BCONF, '*.billsim.purchase_with_invoices.ids'));
	$tipo_documento=$params["codigoSII"];

	$bill_doc_type = bconf_get($BCONF, '*.billsim.doc_type.'.$folio);
	if (isset($bill_doc_type) && $tipo_documento == $bill_doc_type) {
		$doc_num = bconf_get($BCONF, '*.billsim.doc_num.'.$folio);
		$identificador = bconf_get($BCONF, '*.billsim.identificador.'.$folio);
	} else {
		$doc_num = $folio;
		$identificador = rand(1,100);
	}

	if ($tipo_documento == '39' && in_array($folio, $invoices_to_fail) || $tipo_documento == '33' && in_array($folio, $bills_to_fail)) {
		$result = "OK";
		$cantidad = 1;
		$description = "Documento OK";
		$archivos= '<Archivos>';
		$archivos.= '	<Archivo>';
		$archivos.= "    	<Identificador>$identificador</Identificador>";
		$archivos.= '    	<Documento Folio="'.$doc_num.'" Tipo="'.$tipo_documento.'" FolioCargado="1" />';
		$archivos.= '	</Archivo>';
		$archivos.= '</Archivos>';
	} else {
		$result = "NOK";
		$description = "Documento no encontrado para identificador provisto";
		$cantidad = 0;
		$archivos = "<Archivos/>";
	}

	$xml = '<RespuestaConsultaResultadoDocumentoXml>';
	$xml.= "<Resultado>$result</Resultado>";
	$xml.= "<Descripcion>$description</Descripcion>";
	$xml.= "<CantidadEncontrada>$cantidad</CantidadEncontrada>";
	$xml.= $archivos;
	$xml.= '</RespuestaConsultaResultadoDocumentoXml>';

	return (object) array("ConsultaResultadoDocumentoResult" => (object) array("any" => $xml));
}

function CargarYEmitir($CargarYEmitir) {

	// validate here the params of $CargarYEmitir
	$csv = $CargarYEmitir["archivo"];

	$data = explode("\n", $csv);
	$number_of_rows = count($data);
	$result = "OK";
	$description = "";
	$final_description = "";

	if ($number_of_rows < 2 || $data[1] == "") {
		$description = "Linea 2. Linea sin datos.";
	}

	if($description == "") {
		for ($i=1;$i<$number_of_rows;$i++) {
			$errors = validate_row($data[$i]);
			if (!empty($errors)) {
				foreach ($errors as $error) {
					$line_number = $i + 1;
					$description .= "Linea $line_number: $error\n";
				}
				$description .= "\n";
			}
		}
	}

	if ($description != "") {
		$result = "C1";
		$final_description = "PROCESO DE CARGA DE VENTAS ELECTRONICAS\n";
		$final_description .= "NOMBRE ARCHIVO: CARGADO_WS_ARCHIVO_DE_VENTAS_ELECTRONICAS\n";
		$final_description .= "Fase 1 - Revisi�n Formato 3:14:15 p.m.\n";
		$final_description .= "Errores encontrados:\n";
		$final_description .= $description;
		$final_description .= "Fin Fase 1.";
	}

	// crappy code to have this working in devs env
	if (function_exists('pg_connect')) {
		$folio = getNextDocNum();
	} else {
		$folio = time();
		sleep(1);
	}
	$Id = 1000 + $folio;

	$xml = '<ResultadoCargaArchivosXml>';
	$xml.= "<Resultado>$result</Resultado>";
	$xml.= "<Descripcion>$final_description</Descripcion>";
	$xml.= "<Identificador>$Id</Identificador>";
	$xml.= '<Documentos>';
	$xml.= '     <Documento Folio="'.$folio.'" Tipo="TipoDocumento" FolioCargado="1" />';
	$xml.= ' </Documentos>';
	$xml.= '</ResultadoCargaArchivosXml>';

        return (object) array("CargarYEmitirResult" => (object) array("any" => $xml));
}

function getNextDocNum() {
	/* XXX: This was done in order to have differents "folios" and documents id */
	$user = exec('whoami');
	$conn = pg_connect("host=/dev/shm/regress-$user/pgsql0/data dbname=blocketdb");
	$result = pg_query($conn, "SELECT MAX(doc_num) + 1 FROM purchase");
	if (!$result) {
			return 1;
	}
	while ($row = pg_fetch_row($result)) {
		if ($row[0] == 0)
			return 1;
		else
			return $row[0];
	}
}


function validate_row($row){
	$messages = array();
	if($row == "") {
		return $messages;
	}
	$cells = explode(";", $row);

	/* 0 -> type in 39 -> boleta or 33 -> factura */
	$types = array("39", "33");
	$type = $cells[0];
	if (!in_array($type, $types)) {
		array_push($messages, "Tipo de documento inv�lido, solo puede tomar valor 33, 34, 39, 41, 46, 52, 56 y 61.");
	}

	/* 1 -> folio is number >0 */
	$folio = $cells[1];
	if (!is_numeric($folio) || !intval($folio) > 0){
		array_push($messages, "Folio de documento debe ser un dato num�rico.");
	}

	/* 2 -> sequence is number >0 */
	$sequence = $cells[2];
	if (!is_numeric($sequence) || !intval($sequence) > 0){
		array_push($messages, "Secuencia de documento debe ser un dato num�rico.");
	}

	/* 3 -> date, format is dd-mm-YYYY */
	$date = $cells[3];
	if (preg_match("/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $date)){
		list($day,$month,$year) = explode("-", $date);
		if(!checkdate($month, $day, $year)) {
			array_push($messages, "Fecha inv�lida. D�a debe ser num�rico.");
		}
	} else {
		array_push($messages, "Fecha inv�lida. D�a debe ser num�rico.");
	}

	/* 4 -> rut is a valid rut no points, just a hyphen */
	$rut = $cells[4];
	if (!validate_rut($rut)) {
		array_push($messages, "RUT cliente inv�lido (d�gito verificador no corresponde al rut ingresado).");
	}

	// XXX the string validation is just stupid xD, please dont blame me
	/* 5 -> name is a string no quotes are allowed max 50 */
	$name = $cells[5];
	$name_len = strlen($name);
	if (!is_string($name) || $name_len > 50 || $name_len == 0) {
		array_push($messages, "Raz�n social de cliente es obligatoria.");
	}

	/* 6 -> lob is a string no quotes are allowed max 40 */
	$lob = $cells[6];
	$lob_len = strlen($lob);
	if (!is_string($lob) || $lob_len > 40 || $lob_len == 0) {
		array_push($messages, "Giro de cliente inv�lido.");
	}

	/* 7 -> commune is a string no quotes are allowed max 100 */
	$commune = $cells[7];
	$commune_len = strlen($commune);
	if (!is_string($commune) || $commune_len > 100 || $commune_len == 0) {
		array_push($messages, "Comuna de cliente es obligatoria.");
	}

	/* 8 -> address is a string no quotes are allowed max 60 */
	$address = $cells[8];
	$address_len = strlen($address);
	if (!is_string($address) || $address_len > 60 || $address_len == 0) {
		array_push($messages, "La direcci�n del cliente es obligatoria.");
	}

	/* 9 -> subject to tax must be SI|NO */
	$subject_to_tax = $cells[9];
	if (!in_array(strtoupper($subject_to_tax),array("SI", "NO"))) {
		array_push($messages, "Campo afecto inv�lido. S�lo puede tomar valores 'SI' o 'NO'.");
	}

	/* 10 -> product name, string no quotes are allowed, max 80 */
	$product_name = $cells[10];
	$product_name_len = strlen($product_name);
	if (!is_string($product_name) || $product_name_len  > 80 || $product_name_len == 0) {
		array_push($messages, "Descripci�n del producto es obligatoria.");
	}

	/* 11 -> description, string, no quotes are allowed, max 900, is_optional */
	$description = $cells[11];
	if (!is_string($description) || strlen($description) > 900) {
		array_push($messages, "Descripci�n del producto es muy larga.");
	}

	/* 12 -> quantity, number, >0 */
	$quantity = $cells[12];
	if (!is_numeric($quantity) || !intval($quantity) > 0){
		array_push($messages, "Cantidad del producto inv�lida. Debe ingresar un valor num�rico.");
	}

	/* 13 -> price, number >0 */
	$price = $cells[13];
	$price = str_replace(',', '.', $price);

	if (!is_numeric($price)) {
		array_push($messages, "Precio del producto inv�lido. Debe ingresar un n�mero.");
	}

	/* 14 -> discount, number should be always 0, is_optional */
	$discount = $cells[14];
	if (!is_numeric($discount) || intval($discount) != 0){
		array_push($messages, "Descuento del producto inv�lido. Debe ser 0.");
	}

	/* 15 -> email, a valid email, max 100 */
	$email = $cells[15];
	if (!empty($email)) {
		if (!preg_match("/([a-zA-Z0-9_-]+)(\@)([a-zA-Z0-9_-]+)(\.)([a-zA-Z0-9]{2,4})(\.[a-zA-Z0-9]{2,4})?/i", $email)){
			array_push($messages, "Correo electr�nico inv�lido.");
		}
	}

	/* 16 -> type of service is a number in 1|2|3, in our case is always 3 */
	$type_of_service = $cells[16];
	if (!is_numeric($type_of_service) || intval($type_of_service) != 3){
		array_push($messages, "Indicador de tipo de servicio inv�lido. Solo puede tomar los valores 1, 2 o 3.");
	}
	return $messages;
}

function validate_rut($rut) {
    // TODO: also validate if there are points in the value
    $r=strtoupper(preg_replace('/[^0-9kK]/','',$rut));
    $sub_rut=substr($r,0,strlen($r)-1);
    $sub_dv=substr($r,-1);
    $x=2;
    $s=0;
    for ($i=strlen($sub_rut)-1;$i>=0;$i--) {
      if ( $x >7 ) {
        $x=2;
      }
      $s += $sub_rut[$i]*$x;
      $x++;
    }
    $dv=11-($s%11);
    if ( $dv==10 ) {
      $dv='K';
    }
    if ( $dv==11 ) {
      $dv='0';
    }
    if ( $dv==$sub_dv ) {
      return $r;
    }
    else {
      return false;
    }
}
