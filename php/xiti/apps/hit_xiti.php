<?php
	require_once('JSON.php');

	$content = var_export($_GET, true);
	if (empty($_GET['stc'])) { # is a click hit tag
		$file_name = (empty($_GET['p'])) ? "../logs/hit_xiti" : "../logs/hit_xiti." . str_replace("::", "-", strtolower($_GET['p']));
		file_put_contents($file_name . '.click.log', $content);
	}
	else { # is a page hit
		$json = json_decode($_GET['stc']);
		$page_name = str_replace("/", "_", strtolower($json->page_name));
		$file_name = "../logs/hit_xiti." . $page_name;
		file_put_contents($file_name . '.page.log', $content);
		file_put_contents($file_name . '.json.log', var_export($json, true));
	}
?>
