<?php
require_once("bTransaction.php");
require_once("init.php");

openlog("ajax", LOG_ODELAY, LOG_LOCAL0);

if (isset($_GET['a']) && $_GET['a'] == 'getstores') {

	$transaction = new bTransaction();

	if (isset($_POST['email']))
		$transaction->add_data('email', $_POST['email']);

	$reply = $transaction->send_command('find_stores');

	if ($transaction->has_error()) {
		exit(1);
	}

	$stores = array();
	foreach (explode("\t", $reply['stores']) as $s) {
		$ss = explode(':', $s, 2);
		if (count($ss) == 2)
			$stores[$ss[0]] = $ss[1];
	}
	echo json_encode($stores);

	exit(0);
}

?>
