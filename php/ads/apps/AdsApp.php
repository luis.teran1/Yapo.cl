<?php

namespace Yapo;

use Yapo\Bconf;

class AdsApp
{

    private $session_prefix;
    private $trans;
    private $redis;
    private $bconf;

    public function __construct($trans = null, $redis = null, $conf = null)
    {
        global $BCONF;
        if (is_null($conf)) {
            $this->bconf = $BCONF;
        } else {
            $this->bconf = $conf;
        }
        $this->trans = $trans;
        $this->redis = $redis;

        $this->prefix = Bconf::get($this->bconf, '*.accounts.session_redis_prefix');
        $this->session_prefix = $this->prefix . Bconf::get($this->bconf, '*.accounts.session_redis_key_prefix');
        $this->cookie_name = Bconf::get($this->bconf, '*.accounts.session_cookie_name');
        $redis_conf = Bconf::get($this->bconf, "*.redis.session.host.rsd1");
        $this->redis->connect($redis_conf['name'], $redis_conf['port'], $redis_conf['timeout']);
    }

    public function activateAd($url_data, $request_data)
    {
        $trans = $this->trans->reset();
        $trans->add_data('id', $request_data->list_id);
        $loadad = $trans->send_command('loadad');
        if ($trans->has_error(true)) {
            $response = array('status' => 'error');
            $response = array_merge($response, $this->translateErrors($trans));
            return $response;
        }

        $category = $loadad['ad']['category'];
        $pack_type = Bconf::get($this->bconf, "*.pack.reverse_type.$category");
        if (array_key_exists($this->cookie_name, $_COOKIE)) {
            $session_id = $_COOKIE[$this->cookie_name];
            $session_email = $this->redis->hGet($this->session_prefix.$session_id, 'email');
            $usr_passwd = $this->redis->hGet($this->prefix.sha1($session_email), 'password');
            $account_id = $this->redis->hGet($this->prefix.sha1($session_email), 'account_id');
            $user_id = $this->redis->hGet($this->prefix.sha1($session_email), 'user_id');
            if (empty($usr_passwd)) {
                $trans_acc = $this->trans->reset();
                $trans_acc->add_data('email', $session_email);
                $trans_acc->send_command('get_account');
                if (!$trans_acc->has_error(true)) {
                    $usr_passwd = $this->redis->hGet($this->prefix.sha1($session_email), 'password');
                    $account_id = $this->redis->hGet($this->prefix.sha1($session_email), 'account_id');
                    $user_id = $this->redis->hGet($this->prefix.sha1($session_email), 'user_id');
                }
            }

            if (!empty($usr_passwd) && $loadad['ad']['user_id'] == $user_id) {
                $trans = null;
                if ($loadad['ad']['status'] == 'disabled') {
                    $trans = $this->callToTransEnable($request_data->list_id, $account_id);
                } else {
                    $trans = $this->callToTransActivate($request_data->list_id, $usr_passwd);
                }

                $response = array('status' => 'ok');
                if ($trans->has_error(true)) {
                    $response = array('status' => 'error', 'pack_type' => $pack_type);
                    $response = array_merge($response, $this->translateErrors($trans));
                }
                return $response;
            }
        }

        $usr_passwd = hash_password($request_data->passwd, $loadad['ad']['salt']);

        $trans = null;
        if ($loadad['ad']['status'] == 'disabled') {
            $trans = $this->trans->reset();
            $trans->add_data('id', $request_data->list_id);
            $trans->add_data('passwd', $usr_passwd);
            $loadad_w_passwd = $trans->send_command('loadad');
            if (!$trans->has_error(true)) {
                $trans = $this->callToTransEnable($request_data->list_id, $loadad_w_passwd['users']['email']);
            }
        } else {
            $trans = $this->callToTransActivate($request_data->list_id, $usr_passwd);
        }

        $response = array('status' => 'ok');
        if ($trans->has_error(true)) {
            $response = array('status' => 'error', 'pack_type' => $pack_type);
            $response = array_merge($response, $this->translateErrors($trans));
        }
        return $response;
    }

    private function callToTransActivate($list_id, $passwd)
    {
        $trans = $this->trans->reset();
        $trans->add_data('log_string', log_string());
        $trans->add_data('list_id', $list_id);
        $trans->add_data('salted_passwd', $passwd);
        $trans->add_data('new_status', 'active');
        $trans->add_client_info();
        $trans->send_command('change_ad_status');
        return $trans;
    }

    private function callToTransEnable($list_id, $account)
    {
        $trans = $this->trans->reset();
        $trans->add_data('log_string', log_string());
        $trans->add_data('list_id', $list_id);
        $trans->add_data('new_status', 'active');
        if (is_numeric($account)) {
            $trans->add_data('account_id', $account);
        } else {
            $trans->add_data('email', $account);
        }
        $trans->add_client_info();
        $trans->send_command('pack_change_ad_status');
        return $trans;
    }

    private function translateErrors($trans)
    {
        $errors = $trans->get_errors();
        $translated = array();
        foreach ($errors as $key => $value) {
            $translated[$key] = $value;
            $translated[$key."_msg"] = Bconf::lang($this->bconf, $value);
        }
        return $translated;
    }
}
