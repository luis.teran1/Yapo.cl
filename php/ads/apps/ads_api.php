<?php

require_once('autoload_lib.php');
require_once('util.php');

//Syslog
openlog("ads_api", LOG_ODELAY, LOG_LOCAL0);

class ads_api extends bRESTful {

	function __construct() {
		$this->restAPISettingsKey = "*.ads_rest_settings";
	}

}

$api = new ads_api();
$api->run();
