<?php

require_once('autoload_lib.php');
require_once('init.php');
require_once('util.php');

class blocket_refund extends blocket_application {

	var $tmpl_data;
	var $loaded = false;

	function blocket_refund() {
		global $BCONF;
		$this->application_name = 'refund';
		$this->page_title = lang('PAGE_REFUND_TITLE');
		$this->page_name = lang('PAGE_REFUND_NAME');
		$this->headstyles = bconf_get($BCONF, "refund.headstyles");
		$this->headscripts = bconf_get($BCONF, "refund.headscripts");
		$this->init('load', 10);
	}

	function get_state($statename) {
		switch($statename) {
			case 'load':
				return array('function' => 'refund_load',
						'params' => array('id', 'a', 'p', 'h'),
						'method' => 'get');
			case 'form':
				return array('function' => 'refund_form',
						'method' => 'get');
			case 'validate':
				return array('function' => 'refund_validate',
						'params' => array('first_name' => 'f_name', 'last_name' => 'f_name', 'rut', 'email', 'bank_account_type' => 'f_id', 'bank' => 'f_id', 'bank_account_number'),
						'method' => 'post');
			case 'success':
				return array('function' => 'refund_success',
						'method' => 'get');
			case 'error':
				return array('function' => 'refund_error',
						'method' => 'get');
		}
	}

	function refund_load($id, $a, $p, $h) {
		// The "id" ad has an upselling purchase with action_id "a"
		$trans = new bTransaction();
		$trans->add_data('ad_id', $id);
		$trans->add_data('action_id', $a);
		$trans->add_data('payment_group_id', $p);
		$reply = $trans->send_command('refund_validate');
		if($reply['status'] != 'TRANS_OK'){
			return 'error';
		}
		// The "purchase date" sha1 is equal to "h" 
		if ($h === sha1($reply['receipt'])){	
			$this->tmpl_data['ad_id'] = $reply['ad_id'];
			$this->tmpl_data['action_id'] = $reply['action_id'];
			$this->tmpl_data['purchase_id'] = $reply['purchase_id'];
			if ($reply['refund_exists'] != -1){
				$this->tmpl_data['refund_id'] = $reply['refund_id'];
				$this->tmpl_data['first_name'] = $reply['first_name'];
				$this->tmpl_data['last_name'] = $reply['last_name'];
				$this->tmpl_data['rut'] = $reply['rut'];
				$this->tmpl_data['email'] = $reply['email'];
				$this->tmpl_data['bank_account_type'] = $reply['bank_account_type'];
				$this->tmpl_data['bank'] = $reply['bank'];
				$this->tmpl_data['bank_account_number'] = $reply['bank_account_number'];
			}
			$this->loaded = true;
			
			return 'form';
		}
		return 'error';
	}
	
	function refund_form() {
		if($this->loaded != true)
			return 'load';
		$this->display_layout('refund/form.html', $this->tmpl_data, null, null, 'responsive_base');
	}
	
	function refund_validate($first_name, $last_name, $rut, $email, $bank_account_type, $bank, $bank_account_number){
		if(!$this->loaded)
			return 'load';
		$trans = new bTransaction();
		$trans->add_data('ad_id', $this->tmpl_data['ad_id']);
		$trans->add_data('action_id', $this->tmpl_data['action_id']);
		$trans->add_data('purchase_id', $this->tmpl_data['purchase_id']);

		$params = array('first_name','last_name','rut','email','bank_account_type','bank','bank_account_number');
		foreach ($params as $key) {
			if($$key != "") {
				$trans->add_data($key, $$key);
			}
		}

		$reply = $trans->send_command('refund_insert');

		if($reply['status'] == 'TRANS_OK'){
			return 'success';
		}
		
		if($trans->has_error()) {
			$this->set_errors($trans->get_errors());
		}

		foreach ($params as $key) {
			$this->tmpl_data[$key] = $$key;
		}
		return 'form';
	}
	
	function refund_success() {
		if(!$this->loaded)
			return 'load';
		$this->display_layout('refund/success.html', array(), null, null, 'responsive_base');
		return 'FINISH';	
	}
	
	function refund_error() {
		$this->display_layout('refund/error.html', array(), null, null, 'responsive_base');
		return 'FINISH';	
	}

	function f_id($id) {
		return strip_tags($id);
	}

	function f_action($action) {
		return strip_tags($action);
	}

	function f_name($val) {
        $val = preg_replace('/[^a-zA-Z0-9����������������&\.,:; -]/i', '', $val);
        return (string)$val;
	}

	function f_email($val) {
        $val = preg_replace('/[^a-zA-Z0-9@._-]/i', '', $val);
        return (string)$val;
	}

	function f_bank_account_number($val) {
		$val = preg_replace('/[^0-9-]/i', '', $val);
		return strip_tags($val);
	}

	function f_rut($val) {
        $val = preg_replace('/[^0-9kK-]/i', '', $val);
        return strtoupper($val);
    }

	function f_a($val) {
		return $val;
	}

	function f_p($val) {
		return $val;
	}

	function f_h($val) {
		return $val;
	}
}
$refund = new blocket_refund();
$refund->run_fsm();
?>
