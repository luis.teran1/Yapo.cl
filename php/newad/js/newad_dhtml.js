$(document).ready(function() {
  $('input.icheck-green').iCheck({
    checkboxClass: 'icheckbox_minimal-green',
    increaseArea: '20%' // optional
  });

});

var is_admin = false;

//If javascript enabled don't do php check
function setJavascriptON() {
  document.getElementById("check_type_diff").value = 0;
}

function is_company_ad() {
  var compinput = document.getElementById('c_ad');

  if (compinput.type == "hidden") {
    return compinput.value == "1";
  } else {
    return compinput.checked;
  }
}

function get_ad_store() {
  var elem;

  if ((elem = document.getElementById('store_row')) && elem.style.display != 'none') {
    if ((elem = document.getElementById('store')))
      return elem.value;
  }

  if ((elem = document.getElementById('default_store')))
    return elem.value;

  return 0;
}

function toggle_phone(e, args) {
  var phone_match = this.getAttribute("match");

  if (phone_match == 0) {
    if (this.checked) {
      document.getElementById("pay_phone").style.display = "none";
      document.getElementById("pay_type_phone").checked = false;
      document.getElementById("pay_type_card").checked = true;
    } else {
      document.getElementById("pay_phone").style.display = "inline";
    }
  }
}


var value_communes_selected = "0";

function populateCommunes(result, xmlhttp, arg) {
  var commune_select = document.formular.communes;
  var communes = eval("[" + result + "]");
  for(var i=0;i<communes.length;i++){
    var obj = communes[i];
    for(var key in obj){
      commune_select.options[i+1] = new Option(obj[key]['yapo_name'], key);
      $(commune_select.options[i+1]).data('map', obj[key]['map_name'])
    }
  }
  $('#communes').attr('data-region', $('#region').val());
}

function resetCommunes() {
  select = document.formular.communes;
  if (select.value != undefined && select.value != "" && select.value != "0" ){
    window.value_communes_selected = select.value;
  }

  if (select.options) {
    select.options.length = 0;
    select.options[0] = new Option(String.fromCharCode(171)+js_info['CHOOSE']+String.fromCharCode(187),"",false,false);
    select.options[0].selected = true;
  }
}

function setCommunes(e,args) {
  resetCommunes();
  var region = document.formular.region.value;
  if (!region || region == 0) {
    region = regionDefault;
  }
  ajax_request("/templates/common/communes.html?region="+region, '', populateCommunes, e, false, 'GET');
}

function setCommunesFromRegion(e, args) {
  if ( typeof document.formular.communes !== 'undefined') {
    setCommunes(e, args);
  }
}

/*
 * check price threshold
 */
function check_price_condition(e, args) {
  if (args == null || args.id == null || document.getElementById(args.id) == null)
    return;
  var warning = split_setting(get_settings('price_warning', form_key_lookup, category_settings));
  var price = document.formular.price.value;

  /*
  * Here we should not manipulate the price field. We should just make sure that
  * it does not contain any non numeric values, in case that the function that does that
  * has not kicked in
  */
  price = price.replace(/[^\d,]/g,'');
  price = price.replace(/\./g, '');
  price = price.replace(/,/, '.');

  if (price == "" || warning == null || warning.min == null) {
    if (document.getElementById(args.id).style.display == 'block')
      document.getElementById(args.id).style.display = 'none';
    return;
  }

  if (parseInt(price) < parseInt(warning.min)) {
    document.getElementById(args.id).style.display = 'block';
  }else{
    document.getElementById(args.id).style.display = 'none';
  }

  pesos2uf();
}

function pesos2uf(e, args){
  var div = document.getElementById("pesos2uf");
  var uf_price = document.getElementById("uf_price");
  var $currencyUF = $('.currency-type.__uf');
   if (div) {
    var price = document.getElementById("price");
    var price_value = price.value.replace(/\./g, "");

     if (parseInt(price_value) > 0 && !isNaN(price_value)) {
      var result = parseInt((price_value / uf_conversion_factor)*100);
      result = (result/100);
      result = Math.round(result*100)/100;
      div.innerHTML = "(UF "+real_format_price(result)+")";
    } else {
        div.innerHTML = "";
    }
  }
}


function display_infopage(e, args) {

   if (is_admin)
     return;

   var company_ad = is_company_ad();
   var brokerinput = document.getElementById('broker');
   var set_label = false;

   if (brokerinput) {
     /* Verify that current choices has broker param. */
     var params = split_setting(get_settings("params", form_key_lookup, category_settings));
     if (params && !params['broker'])
       brokerinput = null;
   }


   if (get_ad_store() > 0) {
     /* This is a store ad, display infopage and title */
     document.getElementById('infopage').disabled = false;
     document.getElementById('infopage_title').disabled = false;
     document.getElementById('infopage_row').className = 'infopage_enabled';
     document.getElementById('infopage_title_row').className = 'infopage_title_enabled';
     document.getElementById('infopage_title_label').className = 'infopage_enabled';

     set_label = true;
   } else if (brokerinput) {
     /* Display infopage if broker is checked. */
     if (brokerinput.checked) {
       /* Display broker infopage without infopage title */

       document.getElementById('infopage').disabled = false;
       document.getElementById('infopage_title').disabled = true;
       document.getElementById('infopage_row').className = 'infopage_enabled';
       document.getElementById('infopage_title_row').className = 'infopage_title_hidden';
       document.getElementById('infopage_title_label').className = 'infopage_enabled';

       set_label = true;
     } else {
       /* Display nothing */
       document.getElementById('infopage').disabled = true;
       document.getElementById('infopage_title').disabled = true;
       document.getElementById('infopage_row').className = 'infopage_hidden';
       document.getElementById('infopage_title_row').className = 'infopage_title_hidden';
       document.getElementById('infopage_title_label').className = 'infopage_enabled';
     }
   } else if (company_ad && company_ad != "0") {
     /* Display infopage and infopage title disabled */
     document.getElementById('infopage').disabled = true;
     document.getElementById('infopage_title').disabled = true;
     document.getElementById('infopage_row').className = 'infopage_disabled';
     document.getElementById('infopage_title_row').className = 'infopage_title_disabled';
     document.getElementById('infopage_title_label').className = 'infopage_disabled';

     set_label = true;
   } else {
     /* Display nothing */
     document.getElementById('infopage').disabled = true;
     document.getElementById('infopage_title').disabled = true;
     document.getElementById('infopage_row').className = 'infopage_hidden';
     document.getElementById('infopage_title_row').className = 'infopage_title_hidden';
     document.getElementById('infopage_title_label').className = 'infopage_hidden';
   }

   if (set_label) {
     var label = get_settings('infopage', form_key_lookup, label_settings, '');

     if (label) {
       label = split_setting(label);
       document.getElementById('infopage_label').innerHTML = label['label'] + ':';
       if (label['text'])
         document.getElementById('infopage_info').innerHTML = label['text'];
       else
         document.getElementById('infopage_info').innerHTML = '';
     }
   }
   return true;
 }

 var shown_category = 0;
 var shown_type = 's';
 var shown_company_ad = 0;
 var cat_data = [];

 function show_category_template_cb(result, xmlhttp) {
   shown_category = form_key_lookup('category', '');
   shown_type = form_key_lookup('type', '');
   shown_company_ad = is_company_ad() ? 1 : 0;

  shown_estate_type = form_key_lookup('estate_type', '');
  if(shown_category > 2000 && typeof(shown_estate_type) != 'undefined' && shown_estate_type != '' ) {
    est_type = document.getElementById("estate_type");
    if(est_type) {
      est_type.value = '';
      cat_data["selectbox estate_type.estate_type"] = '';
    }
  }

   if (!result) {
     return;
   }

   var cat_tables = getElementsByClassName(document.formular, 'fieldset', 'da-information');
   for (var tidx in cat_tables) {
     var table = cat_tables[tidx];

     cat_data = mergeElementValues(cat_data, table.getElementsByTagName('INPUT'));
     cat_data = mergeElementValues(cat_data, table.getElementsByTagName('TEXTAREA'));
     cat_data = mergeElementValues(cat_data, table.getElementsByTagName('SELECT'));
   }
   var cat_data_containers = getElementsByClassName(document.formular, 'DIV', 'cat_data_container');
   for (var cidx in cat_data_containers) {
     var cont = cat_data_containers[cidx];

     if (!cont.id)
       continue;

     var element_group = cont.className.replace(/.*element_group([0-9]+).*/, "$1");
     if (element_group != "")
       element_group += ".";
     cat_data[element_group + cont.id + ".innerhtml"] = cont.innerHTML;
   }

   var ic = get_item_count();
   if (ic > 0)
     cat_data['item_count'] = ic;

   var cat_cont = document.getElementById('category_contents');
   cat_cont.style.display = 'none'; /* Hide the shuffling */
   cat_cont.innerHTML = result;
   if (get_item_count() > 0 && cat_data['item_count'] > 0) {
     ic = cat_data['item_count'];
     while (ic-- > 1)
       add_item(null, "batch");
   }

   var cat_data_containers = getElementsByClassName(document.formular, 'DIV', 'cat_data_container');
   for (var cidx in cat_data_containers) {
     var cont = cat_data_containers[cidx];

     if (!cont.id)
       continue;

     var element_group = cont.className.replace(/.*element_group([0-9]+).*/, "$1");
     if (element_group != "")
       element_group += ".";
     var innerhtml = cat_data[element_group + cont.id + ".innerhtml"];
     if (innerhtml)
       cont.innerHTML = innerhtml;
   }
   var cat_tables = getElementsByClassName(document.formular, 'fieldset', 'da-information');
   for (var tidx in cat_tables) {
     var table = cat_tables[tidx];

     setElementValues(cat_data, table.getElementsByTagName('INPUT'));
     setElementValues(cat_data, table.getElementsByTagName('TEXTAREA'));
     setElementValues(cat_data, table.getElementsByTagName('SELECT'));
   }

   register_events(jsevents.ai, document);
   cat_cont.style.display = 'block';
   $(cat_cont).attr('data-cat', shown_category);

   if (shown_category == "2020") {
     persist_after_ajax = true;
   }

  //Form has changed, we should add the validators again.
  new Validator(val_conf_ai.aiform);

  // Really dirty hack... we desperatelly need to add an option to the validator framework to validate the fields on construction
  $("#err_zipcode").removeClass("error").hide();
  /* XXX cascading call, but no infrastructure to do it elsewise here. */
  //   display_infopage();
}

function queue_show_category_template_cb(result, xmlhttp, e) {
  show_category_template_cb(result, xmlhttp);
  placeholder_enhanced(jQuery);
  var body = document.getElementById('body');
  if (navigator.userAgent.toLowerCase().indexOf('webkit') > -1){
    if ( body.value != '')
      body.style.backgroundImage = '';
  }
}

function show_category_template(e, args) {
  var sel_id = this.getAttribute("data-sel_id");
  var cat = form_key_lookup('category', sel_id);
  var type = form_key_lookup('type', sel_id);
  var estate_type = form_key_lookup('estate_type', sel_id);
  if(typeof(estate_type) == 'undefined') {
    estate_type = 0;
  }
  var footwear_gender = form_key_lookup('footwear_gender', sel_id);
  if(typeof(footwear_gender) == 'undefined') {
    footwear_gender = 0;
  }
  var company_ad = is_company_ad() ? 1 : 0;
  var store = get_ad_store();
  var types = "s,k";

  // Validate the category and type exist
  if (category_settings["types"]["1"][cat] && category_settings["types"]["1"][cat]["value"]) {
    types = category_settings["types"]["1"][cat]["value"];
  }

  /* if the type not exist for the category, put the first value.
  example: when you change from "otros productos" to "Ofertas the empleo"
  */
  if (types.indexOf(type) < 0) {
    type = types.substr(0,1);
  }

  var request = document.formular.action.replace('verify', 'cat_tmpl') + '?category=' + cat + '&shown_category=' + shown_category +
        '&type=' + type + '&shown_type=' + shown_type + '&shown_company_ad='+ shown_company_ad +'&company_ad=' + company_ad +
        '&estate_type=' + estate_type + '&store=' + store + '&footwear_gender=' + footwear_gender + '&currency=peso';
  var categoryName = category_list[cat] ? category_list[cat].name : null;
  clearCurrencySelection();
  clearJobCategorySelection();
  Yapo.toggleJobEditionAdvice(cat, categoryName, category_features, category_settings);
  Yapo.toggleJobInsertionPriceAdvice(cat, category_features, category_settings);
  Yapo.aiCategoryLogin(cat, forcedLoginData);
  ajax_request(request, '', queue_show_category_template_cb, e, false, 'GET');
}

function resetRegion() {
  select = document.formular.region;
  if (select.options) {
    select.options.length = 0;
    select.options[0] = new Option(js_info['CHOOSE'],"",false,false);
    select.options[0].selected = true;
  }
}

function populateRegions(result, xmlhttp, arg) {
  var region_select = document.formular.region;
  var regions = eval("[" + result + "]");

  if (document.formular.r_sel != undefined) {
    var user_selected_region = document.formular.r_sel.value ;
  }

  for(var i=0;i<regions.length;i++){
    var obj = regions[i];
    for(var key in obj){
      region_select.options[i+1] = new Option(obj[key], key);
      if (user_selected_region!=undefined && user_selected_region==key)
        region_select.options[i+1].selected = true;
    }
  }
  if (regions.length==1)
    region_select.options[1].selected = true;
  if(document.getElementById('hidden_region') && document.getElementById('zipcode').value != '') {
    region_select.value = document.getElementById('hidden_region').value;
  }
}

function setRegions(e,args) {

  resetRegion();
  var state = document.formular.state.value;

  if (state) {
    ajax_request("/templates/common/regions.html?state="+state, '', populateRegions, e, false, 'GET');
  }
}

function setRegionFromState(e, args) {
  /* Overwrite state if the selected region comes in the form */
  if (document.formular.r_sel != undefined && document.getElementById('zipcode').value == '') {
    var state = regionArray[document.formular.r_sel.value]['state'];
    document.formular.state.value = state;
  }
  setRegions(e, args);
}

var busy = 0;

function get_item_count() {
  var obj = document.getElementById("item_count");
  if (obj)
    return Number(obj.getAttribute("max_item"));
  return 0;
}

function set_item_count(c) {
  var obj = document.getElementById("item_count");
  if (obj)
    obj.setAttribute("max_item", c);
}

function add_item(e, args) {

  var condition_count = get_item_count();
  if (!display_add_item(condition_count, "new_item"))
    return;

  if (!busy) {
    busy = 1;
    var template_html = document.getElementById('template0');
    var obj = document.createElement('span');
    var dyn = document.getElementById('dynamic');
    var dyn_row = document.getElementById('dynamic_row');

    set_item_count(condition_count + 1);

    obj.setAttribute("item_no", condition_count);
    obj.setAttribute("id", 'template' + condition_count);

    var html = template_html.innerHTML;

    html = html.replace(/\[0\]/g, '['+condition_count+']');
    html = html.replace(/_id0/g, "_id" + condition_count);
    html = html.replace(/Plagg ./g, js_info['CLOTHING_ITEM'] + ' ' + (condition_count + 1));
    obj.innerHTML = html;

    /* Reduce margin on the first element. */
    if (condition_count == 1)
      obj.getElementsByTagName("fieldset")[0].style.marginTop = "-4px";

    reset_item(obj, condition_count);

    dyn.appendChild(obj);
    document.getElementById('item_desc_info_id' + condition_count).style.display = 'none';
    document.getElementById('remove_item_id' + condition_count).style.display = 'block';
    document.getElementById('remove_item_id0').style.display = 'block';

    /* XXX Remove when all clothes ads no longer have body (see item_desc.html.tmpl too) */
    if(condition_count > 0 && document.getElementById('warn_clothes_old_id' + condition_count)) {
      document.getElementById('warn_clothes_old_id' + condition_count).style.display = 'none';
    }

    package_price_update(condition_count+1);
    display_add_item(condition_count+1, "new_item");
    dyn_row.style.display ="";

    var new_element = document.getElementsByName("item_desc[" + condition_count+ "]")[0];

    /* Move new item link */
    var link_element = document.getElementById('new_item_container_id' + (condition_count - 1 ));
    var new_link_element = document.getElementById('new_item_container_id' + condition_count);

    new_link_element.innerHTML = link_element.innerHTML;
    link_element.innerHTML = '';

    document.getElementById('item_howto_id' + condition_count).style.display = 'none';

    if (args != "batch") {
      new_element.focus();
      window.scrollTo(0, findPosY(new_element));

      register_events(jsevents.ai, document);
    }
    busy = 0;
  }
}

function package_price_update(item_count) {

  var o = document.getElementById("package_price_container");
  if (item_count > 1) {
    o.style.display = "block";
  } else {
    o.style.display = "none";
    document.getElementById('package_price').value = '';
  }

}

function display_add_item(count, obj_id) {
  var obj = document.getElementById(obj_id);

  obj.style.display = "none";
  return false;
}

function reset_item(o, no) {
  var selects = o.getElementsByTagName('select');
  var textarea = o.getElementsByTagName('textarea')[0];
  var textbox = o.getElementsByTagName('input')[0];
  var errors = o.getElementsByTagName('div');

  textarea.innerHTML = "";
  textbox.value = "";

  for (var k in selects) {
    if (selects[k].id) {
      selects[k].options[0].selected = true;
    }
  }

  for (var k in errors) {
    if (errors[k].id && (errors[k].id.substr(0,4) == 'err_')) {
      errors[k].style.display ='none';
    }
  }
}

function decrease_item_names(o, ic) {
  var item_no = o.getAttribute("item_no");
  var new_item_no = item_no - 1;
  var selects = o.getElementsByTagName('select');
  var textarea = o.getElementsByTagName('textarea')[0];
  var textbox = o.getElementsByTagName('input')[0];
  var item_vals = new Array();

  var remove_item = document.getElementById("remove_item_id" + item_no);
  if (remove_item) {
    remove_item.setAttribute("id", "remove_item_id" + new_item_no);
  }

  if (ic == 2)
    remove_item.style.display = "none";

  for (var k in selects) {
    if (selects[k].id) {
      var select_name = selects[k].name.replace(/\[\d\d?\]/g, '[' + new_item_no + ']');
      item_vals[select_name] = selects[k].value;
    }
  }

  item_vals[textarea.name.replace(/\[\d\d?\]/g, '[' + new_item_no + ']')] = textarea.value;
  item_vals[textbox.name.replace(/\[\d\d?\]/g, '[' + new_item_no + ']')] = textbox.value;

  o.setAttribute("item_no", new_item_no);
  o.setAttribute("id", 'template' + new_item_no);
  o.getElementsByTagName("legend")[0].innerHTML = js_info['CLOTHING_ITEM'] + ' '  + item_no;
  o.innerHTML = o.innerHTML.replace(/\[\d\d?\]/g, '[' + new_item_no + ']');
  o.innerHTML = o.innerHTML.replace(/_id\d\d?/g, '_id' + new_item_no);

  for (var k in item_vals) {
    if (document.getElementsByName(k)[1])
      document.getElementsByName(k)[1].value = item_vals[k];
    else
      document.getElementsByName(k)[0].value = item_vals[k];
  }
}

function remove_item(e, args) {
  var this_id = Number(this.parentNode.parentNode.getAttribute('item_no'));
  var myself = document.getElementById('template' + this_id);
  var item_count = get_item_count();
  var link_html = document.getElementById('new_item_container_id' + (item_count - 1)).innerHTML;
  var move_new_item = false;

  for (++this_id ; this_id < item_count ; ++this_id) {
    var mysibling = document.getElementById('template' + this_id);
    decrease_item_names(mysibling, item_count);
  }
  set_item_count(--item_count);
  if (item_count == 1)
    document.getElementById("remove_item_id0").style.display = 'none';

  package_price_update(item_count);

  if (item_count > 0)
    myself.parentNode.removeChild(myself);

  if (link_html.length > 0) {
    var new_link_element = document.getElementById('new_item_container_id' + (item_count - 1));
    new_link_element.innerHTML = link_html;
  }

  display_add_item(item_count, "new_item");

  document.getElementById('item_howto_id0').style.display = 'block';
  register_events(jsevents.ai, document);
}

function setAreas(e, args) {
  var munics;
  var i = 1;
  var region = document.formular.region.value;

  if (region > 0)
    munics = regionArray[region]['municipality'];

  if (!munics)
    return;

  var arr = Array();
  for (var k in munics) {
    var areas = munics[k]['subarea'];

    arr[k] = new Array();
    arr[k]['area'] = munics[k]['name'];
    if (areas) {
      for (var a in areas)
        arr[k][a] = ' \xa0  - ' + areas[a]['name'];
    }
  }

  var oldArea = document.formular.area.value;
  document.formular.area.options.length = 1;
  var firstArea;
  var str;
  for (var a in arr) {
    document.formular.area.options[i] = new Option(arr[a]['area'], a);
    if (oldArea == a)
      document.formular.area.options[i].selected = true;

    for (var b in arr[a]) {
      if ( b != 'area' ) {
        if (i++ == 1)
          firstArea = arr[a]['area'];

        str= a + ':' + b;
        document.formular.area.options[i] = new Option(arr[a][b], str);
        if (oldArea == str)
          document.formular.area.options[i].selected = true;
      }

    }

    if (i++ == 1)
      firstArea = arr[a]['area'];
  }
  var areaSpan = document.getElementById('area_fixed');
  var area = document.getElementById('area');
  if (i == 2) {
    area.style.display = "none";
    document.formular.area.options[1].selected = true;
    areaSpan.innerHTML = firstArea;
  } else {
    area.style.display = "block";
    areaSpan.innerHTML = '';
  }
}

function updateMaxImages() {
  var number = document.getElementById('extra_images_num');
  var extra_images = get_settings('extra_images', form_key_lookup, category_settings, '');
  if (extra_images) {
    extra_images = split_setting(extra_images);
    max_allowed_images = parseInt(extra_images['max']);

    /* Check if category has special */
    if (number && typeof(number) != 'undefined') {
      /* USB109 ET5: The number shown is the max number, not the max extra number */
      number.innerHTML = language_numbers[1+max_allowed_images];
    }

    if(ai_images.length < max_allowed_images + 1) {
      $('#wrapper_image_upload_button').removeClass("full");
    }
  }
  if(typeof ai_images != 'undefined') {
    if(ai_images.length == max_allowed_images + 1) {
      state.text(ai_warnings['MAX_IMAGES'].es);
      state.removeClass('error');
      state.removeClass('success');
      state.addClass('success');
      state.show();
      ai_images_button.disable();
      $('#wrapper_image_upload_button').addClass("full");
    }else if(ai_images.length > max_allowed_images + 1) {
      state.text(ai_warnings['MAX_IMAGES_OVERFLOW'].es);
      state.removeClass('error');
      state.removeClass('success');
      state.addClass('error');
      state.show();
      ai_images_button.disable();
      $('#wrapper_image_upload_button').addClass("full");
    }else{
      if (typeof state !== 'undefined'
      && typeof state.hide !== 'undefined') {
        state.hide()
      };
      ai_images_button.enable();
    }
  }else {
    if (typeof state !== 'undefined'
    && typeof state.hide !== 'undefined') {
      state.hide()
    };
    ai_images_button.enable();
  }
}

/*
 * Show extra images price and nr of allowed images for chosen category
 */
 function showExtraImagesNumbers() {
   var cat = form_key_lookup('category', '');
   var number = document.getElementById('extra_images_num');
   var price = document.getElementById('extra_images_price');
   var sub_category = document.getElementById('sub_category');
   var category_group = document.getElementById('category_group');

   var extra_images = get_settings('extra_images', form_key_lookup, category_settings, '');

   if (extra_images) {
     extra_images = split_setting(extra_images);
     max_allowed_images = parseInt(extra_images['max']);

     /* Check if category has special */
     if (number && typeof(number) != 'undefined') {
       /* USB109 ET5: The number shown is the max number, not the max extra number */
       number.innerHTML = language_numbers[1+max_allowed_images];
     }

     if (price && typeof(price) != 'undefined')
       price.innerHTML = extra_images['price'];

    // Show or hide
    if (uploaded_images < max_allowed_images) {
      showField("extra_images_form", "inline");
      showField("image_button", "inline");
      enable_field("image2");
      showField("image2", "inline");
    } else {
      showField("extra_images_form", "none");
      showField("image2", "none");
      disable_field("image2");
      showField("image_button", "none");
    }

    // Show field for text about extra images
    if ((category_group.options[category_group.selectedIndex].value % 1000) != 0 && (category_group.value != 0) && (sub_category.value != 0 || sub_category.style.display == "none"))
      showField("extra_images_text", "block");
    else
      showField("extra_images_text", "none");
  } else {
    // Hide field for text about extra images
    showField("extra_images_text", "none");
  }
}

/*
 * Validate email and check for store
 */
 function checkEmail() {
   var compinput = document.getElementById('c_ad');

   if (!document.getElementById('email'))
     return false;

   if (compinput.type == "hidden" && document.getElementById('default_store')) {
//     display_infopage();
show_by_feature(null, {id : 'hide_address_row', feat : 'hide_address'});
return true;
}

var compradios = document.getElementsByName('company_ad');
if (!compradios || compradios.length < 2 || !compradios[1].checked)
  return false;

var email = document.getElementById('email').value;
var store = document.getElementById('store');
var default_store = document.getElementById('default_store');
var url = "/ajax?a=getstores";

var postdata = "email=" + escape(email);

ajax_request(url, postdata, checkEmailCB, store ? store.value : (default_store ? default_store.value : null));
return true;
}

function checkEmailCB(result, xmlhttp, arg) {
  var storediv = document.getElementById('store_holder');
  var store_list = '';

  for (var store_id in result) {
    if (arg != null && store_id == arg)
      store_list += "<option value='" + store_id + "' selected='selected'>" + result[store_id] + "</option>\n";
    else
      store_list += "<option value='" + store_id + "'>" + result[store_id] + "</option>\n";
  }

  if (storediv) {
    if (store_list) {
      storediv.innerHTML = "<select name='store' id='store'>" + store_list + "</select>";
      document.getElementById("store_row").style.display = ''; /* IE6 chokes on table-row */
    } else {
      storediv.innerHTML = "";
      document.getElementById("store_row").style.display = 'none';
    }
  }

//   display_infopage();
show_by_feature(null, {id : 'hide_address_row', feat : 'hide_address'});
}

function actionSelected() {
  var elem;

  elem = document.getElementById('store_passwd_td1');
  if (elem) elem.style.display = 'none';
  elem = document.getElementById('store_passwd_td2');
  if (elem) elem.style.display = 'none';
  elem = document.getElementById('store_username_td1');
  if (elem) elem.style.display = 'none';
  elem = document.getElementById('store_username_td2');
  if (elem) elem.style.display = 'none';
  elem = document.getElementById('ad_passwd_td1');
  if (elem) elem.style.display = 'block';
  elem = document.getElementById('ad_passwd_td2');
  if (elem) elem.style.display = 'block';

  /* show or hide the deletion reasons */
  var deletion_reasons_fieldset = document.getElementById('deletion_reasons_fieldset');
  elem = document.getElementById('cmd_delete');
  if (elem && elem.checked && deletion_reasons_fieldset) {
    deletion_reasons_fieldset.style.display = 'block';
  }
  else {
    deletion_reasons_fieldset.style.display = 'none';
  }

  /* show or hide the edit education msg */
  elem = document.getElementById('cmd_edit');
  var edit_education_msg = document.getElementById('edit_education_msg');
  if (elem && elem.checked) {
    edit_education_msg.style.display = 'block';
  }
  else {
    edit_education_msg.style.display = 'none';
  }
}

/*
 * Change form state and query
 */
function form_action_state(state, query) {
  var form = document.getElementsByTagName('form')[0];
  form.action = form.action.replace(/verify/, state);
  if (query) {
    var queryArray = query.split("&");
    for (var i in queryArray) {
      var q = queryArray[i];
      var keyvalueArray = q.split("=");
      var input = document.createElement('input');

      input.type = 'hidden';
      input.name = keyvalueArray[0];
      input.value = keyvalueArray[1];
      form.appendChild(input);
    }
  }
  form.setAttribute('target', window.name);
  form.submit();

  return false;
}

function click_extra_images(elem) {
  var form = document.getElementsByTagName('form')[0];
  var input = document.createElement('input');
  var posY = 5;

  input.type = 'hidden';
  input.name = 'extra_images';
  input.value = '1';
  form.appendChild(input);

  showProgressBar(elem, posY);
  form.submit();
}

function get_apartment_type() {
  var apartment_type = document.formular.apartment_type;
  for (i = 0; i < apartment_type.length; i++) {
    if (apartment_type[i].checked == true) {
      return apartment_type[i].value;
    }
  }

  return "tenant_ownership";
}

function key_lookup(keyname) {
  if (keyname == 'type')
    return getCheckedTypeVal();
  else if (keyname == 'apartment_type')
    return get_apartment_type();
  else if (document.getElementById(keyname))
    return document.getElementById(keyname).value;
  else
    return null;
}

function has_feature(feature) {
  return get_settings(feature, key_lookup, category_settings);
}

function show_addr_and_zip_tip(e, args) {
  var address = document.getElementById("address");
  var zipcode = document.getElementById("zipcode").value;
  var tips_long = document.getElementById("tips_long_address_zipcode_");

  if (!tips_long)
    return;
  if (address && (address.value.length < 5 || zipcode.length < 9)) {
    tips_long.style.display = "block";
  } else {
    tips_long.style.display = "none";
  }
}

function check_consecutive_chars(subject){
  var s_nospaces = subject.replace(/ /g, "");
  var regex = /[A-Z][A-Z][A-Z][A-Z][A-Z]/;

  return regex.test(s_nospaces);
}

function check_forbidden_words(subject){
  var forbidden_words_text = $("#forbidden_words").html().toLowerCase();
  var forbidden_words = forbidden_words_text.split(" ");
  var splited_subject= subject.split(" ");
  var result = "";
  var allowed_words = [];
  var current_category = $("#category_group").val();
  $(".allowed_words_cat[data-category]").each(function(){
    if($(this).attr("data-category") == current_category){
      allowed_words.push($(this).text().toLowerCase());
    }
  });

  for(var i = 0; i < forbidden_words.length; i++){
    for(var j = 0; j < splited_subject.length; j++){
      if (forbidden_words[i] === splited_subject[j]){
        var is_allowed_word = false;
        $(allowed_words).each(function(key, value){
          if(value == forbidden_words[i]){
            is_allowed_word = true;
            return;
          }
        });
        if (!is_allowed_word && forbidden_words!=="" && splited_subject[j]!=="" ){
          if(result === ""){
            result = "\""+splited_subject[j]+"\"";
          }else{
            result += ",\""+splited_subject[j]+"\"";
          }
        }
      }
    }
  }
  return result;
}

function check_forbidden_rx(text){
  var t = text.toLowerCase();
  var result = "";

  var uls = document.getElementById("forbidden_words_rx");
  var lis = uls.getElementsByTagName("li");

  for (var i = 0, len = lis.length; i < len; i++) {
    rx = lis[i].innerHTML;
    result = t.match(rx);
    if (result != null)
      return true;
  }

  return false;
}

function check_body(){
  var body = document.getElementById("body").value;

  if (check_forbidden_rx(body)) {
    document.getElementById("forbid_message").innerHTML =
    ai_warnings['EMAIL_FORBIDDEN_DESCRIPTION'];
    show_forbid_message($("#body"));
  } else
  hide_forbid_message();
}

function check_subject(){
  var subject = document.getElementById("subject").value;
  var message_forbidden_words = document.getElementById("message").innerHTML;
  var message_uppercase = document.getElementById("message_consec_chars").innerHTML;
  var consecutive_chars = check_consecutive_chars(subject);
  var forbidden_words = check_forbidden_words(subject.toLowerCase());

  var result = "";

  if(forbidden_words !== "") {
    result += message_forbidden_words + " " + forbidden_words + ".";
    document.getElementById("forbid_message").style.lineHeight = null;
  }

  if (consecutive_chars) {
    if (forbidden_words) {
      result += "<br>";
      document.getElementById("forbid_message").style.lineHeight = "16px";
    }

    result += message_uppercase;
  }

  if (check_forbidden_rx(subject)) {
    if (forbidden_words || consecutive_chars) {
      result += "<br>";
      document.getElementById("forbid_message").style.lineHeight = "16px";
    }

    result += ai_warnings['EMAIL_FORBIDDEN_TITLE'];
  }

  if (result !== "") {
    result = "Ops! " + result;
    document.getElementById("forbid_message").innerHTML = result;
    show_forbid_message($("#subject"));
  } else {
    hide_forbid_message();
  }
}


function hide_forbid_message(){
  document.getElementById("forbid_container").style.display="none";
}

function show_forbid_message(elm){
  // default subject box
  var m_offset = elm.height() + 10;
  var m_top = '10px';

  if (elm.attr("name") == "body") {
    m_offset = elm.height() / 2;
    m_top = '50%';
  }

  $("#forbid_arrow").css({
    top: m_top
  });

  var p = elm.offset();
  $("#forbid_container").css({
    top: (p.top + m_offset),
    left: (p.left+elm.width()+17)
  });

  document.getElementById("forbid_container").style.display="inline";
}

function display_label(e, args) {
  var id = args.id;

  var label = split_setting(get_settings(id, form_key_lookup, label_settings));
  if (label) {
    var lbl = label.label;
    var tips = label.tips;

    if (lbl) {
      var elem = document.getElementById(id + "_label");
      if (elem)
        elem.innerHTML = lbl + " *";
    }

    var tipsElem = document.getElementById(id + "_tips");
    if (tips) {
      if (tipsElem) {
        tipsElem.innerHTML = tips;
        tipsElem.style.display = 'block';
      }
    } else if (tipsElem) {
      tipsElem.style.display = 'none';
    }
  }
}

/* Stolen function [http://stackoverflow.com/questions/512528/set-cursor-position-in-html-textbox] - Zane */
function setCaretPosition(elemId, caretPos) {
  var elem = document.getElementById(elemId);

  if (elem == null) { return; }

  if (elem.createTextRange) {
    var range = elem.createTextRange();
    range.move('character', caretPos);
    range.select();
  }
  else {
    if (elem.selectionStart) {
      elem.setSelectionRange(caretPos, caretPos);
    }
  }

  return;
}

function format_price_add_cents() {
  var field = document.getElementById("price");

  if (field.value.indexOf(',') < 0) {
    field.value += ',00';
    if ($(document.activeElement)[0].name === 'price') {
      setCaretPosition("price", field.value.indexOf(','));
    }
  }

  return;
}

function _format_price_rm_cents(price) {

  if ( price ) {
    var price_len = price.length;
    var pos = price.indexOf(',');

    if ((pos >= 0) && ((price_len - pos) >= 3)) {
      price = price.substring(0, pos);
    }
  }
  return price;
}

function normalise_price(e, args) {
  var price_input = document.getElementById("price");
  var old_position = getCaretPosition(price_input);
  price_input.value = _format_price_rm_cents(price_input.value);
  format_number_input("price");
  setCaretPosition('price', old_position);
  return;
}

function format_price(e, args) {
  var field = document.getElementById("price");
  var price_s = field.value;
  var price_len = price_s.length;

  if (price_len > 0) { // You never know... - Zane
    var ar = new Array();
    ar.push( price_s.charAt(0) );

    for (var i = 1; i < price_len; ++i) {
      if ( !((price_len - i) % 3) ) {  ar.push( '.' + price_s.charAt(i));  }
      else {        ar.push( price_s.charAt(i) );    }
    }

    field.value = ar.join('');
    format_price_add_cents();
  }

  return;
}

function format_stuff(e, args) {
  var name = e[0].name;
  var input = document.getElementById(name);
  var old_position = getCaretPosition(input);
  input.value = _format_price_rm_cents(input.value);
  format_number_input(name);
  if(e[1] === 'keyup' || e[1] === 'input') setCaretPosition(name, old_position);
}

function remove_left_zeros(e, args) {
  var name = e[0].name;
  var input = document.getElementById(name);
  if (input.value != "")
    input.value = parseInt(input.value);
}


function format_zipcode() {
  var zipcode = document.getElementById("zipcode").value;
  zipcode=zipcode.replace(/\D/g,"");
  if(zipcode.length > 8) {
    zipcode = zipcode.substring(0, 8);
  }
  if(zipcode.length >= 5 && zipcode.length < 8) {
    zipcode=zipcode.replace(/^(\d\d\d\d\d)(\d)/g,"$1-$2");
  }
  if(zipcode.length == 8) {
    zipcode=zipcode.replace(/^(\d\d\d\d\d)(\d\d\d)/g,"$1-$2");
  }
  document.getElementById("zipcode").value = zipcode;
}

function zipcode_append_zeros() {
  var zipcode = document.getElementById("zipcode").value;
  zipcode=zipcode.replace(/\D/g,"");
  if ((zipcode.length > 4) && (zipcode.length < 8)) {
    while (zipcode.length != 8){ zipcode += '0'; }

    var patt = /\d{8}/g;
    if(patt.test(zipcode) == true){
      zipcode = zipcode.substring(0, 5) + '-' + zipcode.substring(5);
    }

    document.getElementById("zipcode").value = zipcode ;
  }

  return;
}

function show_munic_nh(result, xmlhttp) {
  document.getElementById('tr_munic').style.display = 'none';
  document.getElementById('tr_nh').style.display = 'none';
  if (result!=false) {

    values = eval('(' + result + ')');

    nh = document.getElementById('nh_ph');
    munic = document.getElementById('munic_ph');

    if(munic != null && values['munic'].length > 0) {
      munic.innerHTML = values['munic'];
      if(nh != null && values['nh'].length > 0) {
        nh.innerHTML = values['nh'];
      }

      if(values['ddd_id'].length > 0 && values['state_id'].length > 0) {

        document.getElementById('state').value = values['state_id'];
        document.getElementById('hidden_region').value = values['ddd_id'];
        setRegionFromState(null, null);

      } else {
        document.getElementById('hidden_region').value = '';

        document.getElementById('tr_state').style.display = 'block';
        document.getElementById('tr_region').style.display = 'block';
      }

      document.getElementById('tr_munic').style.display = '';

      if (nh.innerHTML != ' ' && nh.innerHTML != '')
        document.getElementById('tr_nh').style.display = '';
    }
  } else {
    document.getElementById('tr_state').style.display = '';
    document.getElementById('tr_region').style.display = '';
  }
}

function clear_munic_nh() {
  nh = document.getElementById('nh_ph');
  munic = document.getElementById('munic_ph');
  if(nh != null && munic != null) {
    nh.innerHTML = '';
    munic.innerHTML = '';
  }

  document.getElementById('tr_munic').style.display = 'none';
  document.getElementById('tr_nh').style.display = 'none';
}

function get_munic_nh() {
  if(document.getElementById("zipcode").value.length > 4) {
    ajax_request("/templates/common/qs_munic_nh_from_zip.txt?zipcode=" + document.getElementById("zipcode").value.replace(/^0*/g, ""), '', show_munic_nh, null, null, 'GET');
  }
}

function xt_submit_register_step1(){
  xt_submit_register = xtcustom;
  xt_submit_register['page_name'] = 'Ad_Insert';
  xt_submit_register['page_type'] = 'Registerpage_step1';
  try { return xt_click(this, 'F', ''/*s2*/, 'foo&stc=' + JSON.stringify(xt_submit_register)); } catch(e) {}
}

function submitForm(e, args) {

  $.each(["create", "validate"], function(index, value) {
    var arr = document.getElementsByName(value);
    $.each(arr, function(inner_idx, inner_val) {
      document.getElementById(inner_val.id).disabled = true;
    });
  });

  if (typeof appboy === "object" && appboy.logCustomEvent) {
    appboy.logCustomEvent('Ad Insert Submit');
  }

  var create_flag = 1;
  var xt_click_str = '';
  switch ( e[0].id ) {
    case 'submit_preview':
    xt_click_str = 'AI_visualizar';
    create_flag = 0;
    break;
    case 'submit_create_now':
    if($("#create_account").is(":checked")){
      xt_submit_register_step1();
    }
    try {
      if($("#want_bump").is(":checked"))
        xt_click(this, 'C', '', xiti2ClickTags.tags[0].bump_edit.click_name, 'N');
    } catch(e){}
    xt_click_str = 'AI_publish_now';
    break;
    case 'submit_create_t_prv':  xt_click_str = 'AI_preview_top';  break;
    case 'submit_create_b_prv':  xt_click_str = 'AI_preview_bottom';  break;
    default:      xt_click_str = 'unknown_id';    break;
  }

  try {
    xt_click(this, 'C', xtn2, xt_click_str);
  } catch (e) {}

  //document.getElementById(inner_val.id).disabled = true;
  if ( create_flag ) {
    $('<input>').attr({type: 'hidden', name: 'create'}).appendTo('#aiform');
  }

  format_number_input("condominio");
  format_number_input("price");
  format_number_input("mileage");
  format_number_input("size");
  format_number_input("util_size");
  format_number_input("irrig_hect");
  if($('#category_group').val() == "2020"){
    if($('#version').attr("disabled")){
      $('#version').removeAttr('disabled');
    }
  }
  document.getElementById("aiform").submit();
}

function placeholder_enhanced_event(e, args) {

  var body = document.getElementById('body');
  if(body && body.value == '') {
    placeholder_enhanced(jQuery);
  } else if (body.value != "") {
    body.style.background = '';
  }
}
function chrome_body_onfocus() {
  var body = document.getElementById('body');
  if(body && body.value == '')
    body.style.background = '';
}
function chrome_body_onblur() {
  var body = document.getElementById('body');
  if(body && body.value == '') {
    body.style.backgroundImage = 'url(/img/placeholder_chrome.png)';
    body.style.backgroundRepeat = 'no-repeat';
  }
}

function my_set_focus(my_name) {
  document.getElementById(my_name).focus();
}

function fill_version_select_cb(result, xmlhttp) {
  var resultArray = eval("[" + result + "]");
  var car_version_select = document.getElementById("carversion");
  var car_model_select = document.getElementById("carmodel");
  clear_select(car_version_select);
  clear_select(car_model_select);

  // Done here in order to avoid losing the carversion/Modelo info when the page is reloaded
  // not the best place, maybe should be refactored to avoid using 'global' variables // rsouza/bbatista
  modelLength = 0;

  renderOrderedCarDropDown(car_version_select, resultArray[0]);
}

function renderOrderedCarDropDown(ctrl, list){

  var versions = [ ];

  for( var key in list){
    if(key == "name") continue;

    var obj = {"name": list[key].name, "id": key};
    versions.push(obj);
  }

  versions.sort(car_model_version_sort);
  var count = 0;
  for ( var obj in versions){
    ctrl.options[++count] = new Option(versions[obj].name, versions[obj].id);
  }
  ctrl.disabled = false;
}

function car_model_version_sort(obj1, obj2){
  var name1 = obj1.name != undefined ? obj1.name.toString().toLowerCase() : '';
  var name2 = obj2.name != undefined ? obj2.name.toString().toLowerCase() : '';
  if( name1 < name2)
    return -1;
  else if (name1 > name2)
    return 1;
  else
    return 0;
}

function fill_version_select(e, args) {

  var carbrand = form_key_lookup('carbrand');

  if(carbrand != "" && carbrand != "0"){
    var request = document.formular.action.replace('verify', 'ajax_receiver') + '?carbrand=' + carbrand;
    ajax_request(request, '', fill_version_select_cb, e, false, 'GET');
  }else{
    var car_version_select = document.getElementById("carversion");
    var car_model_select = document.getElementById("carmodel");
    clear_select(car_version_select);
    clear_select(car_model_select);

    // Done here in order to avoid losing the carversion/Modelo info when the page is reloaded
    // not the best place, maybe should be refactored to avoid using 'global' variables // rsouza/bbatista
    modelLength = 0;
  }
}

function clear_select(select) {
  select.disabled = "true";
  select.options.length = 1;
}

function fill_model_select_cb(result, xmlhttp) {
  var resultArray = eval("[" + result + "]");
  var car_model_select = document.getElementById("carmodel");
  clear_select(car_model_select);
  renderOrderedCarDropDown(car_model_select, resultArray[0]);
}

function fill_model_select(e, args) {

  var sel_id = this.getAttribute("data-sel_id");
  var carbrand = form_key_lookup('carbrand', sel_id);
  var carversion = form_key_lookup('carversion', sel_id);

  if(carbrand != "" && carversion != ""){
    var request = document.formular.action.replace('verify', 'ajax_receiver') + '?carbrand=' + carbrand + '&carversion=' + carversion;
    ajax_request(request, '', fill_model_select_cb, e, false, 'GET');
  }else{
    var car_model_select = document.getElementById("carmodel");
    clear_select(car_model_select);
  }
}

function block_carmodel_edit(e,args){
  e = e[2];
  if (shown_category != "2020") return;
  var totalLength = brandLength + modelLength;
  if(brandLength > 0) totalLength++;
  if(modelLength > 0) totalLength++;

  var subject = document.getElementById("subject");
  var selectionStart = getSelectionStart(subject);
  var selectionEnd = getSelectionEnd(subject);

  var beforeLimit = selectionStart < totalLength || selectionEnd < totalLength;
  var onLimit = selectionStart == totalLength || selectionEnd == totalLength;
  var keyCode = e.keyCode;
  var bsKey = keyCode == 8;
  var leftKey = keyCode == 37;
  if(beforeLimit || (onLimit && (bsKey || leftKey)) ) {
    subject.selectionStart = totalLength;
    subject.selectionEnd = totalLength;
    e.preventDefault ? e.preventDefault() : (e.returnValue = false);
  }
}

function getSelectionStart(o) {
  if (o.createTextRange) {
    var r = document.selection.createRange().duplicate()
    r.moveEnd('character', o.value.length)
    if (r.text == '') return o.value.length
      return o.value.lastIndexOf(r.text)
  } else return o.selectionStart
}

function getSelectionEnd(o) {
  if (o.createTextRange) {
    var r = document.selection.createRange().duplicate()
    r.moveStart('character', -o.value.length)
    return r.text.length
  } else return o.selectionEnd
}

Validator.ai_form = {};
Validator.ai_form.on_validator_error = function (err_array){
  Validator.ai_form.show_message.call(this,err_array,"error");
};

Validator.ai_form.show_message = function (msg_array, msg_class, selector_appender){
  selector_appender = selector_appender ? selector_appender : "";
  var parentDiv = $(this).closest(".rightcol");
  var msgField = parentDiv.find(".validation_msg" + selector_appender).first();
  if(msgField.size() > 0) {
    msgField.html("");
    for( var i = 0; i < msg_array.length; i++) {
      var msg = msg_array[i];
      if(i>0) {
        msgField.append("<br>");
      }
      msgField.append(ValidatorError.createErrorMsg(msg));
    }
    msgField.removeClass("success");
    msgField.removeClass("warning");
    msgField.removeClass("error");
    msgField.addClass(msg_class);
    msgField.show();
  }
};

Validator.ai_form.validate_not_forbidden_words = function () {
  var error = [];
  var subject = $(this).val();
  var consecutive_chars = check_consecutive_chars(subject);
  var forbidden_words = check_forbidden_words(subject.toLowerCase());

  var result = "";

  if(forbidden_words !== "") {
    error.push({err_code: "FORBIDDEN_WORDS_ERROR_SUBJECT", data: {forbidden_words: forbidden_words}});
  }

  /*
  REMOVE UPPERCASE VALIDATE FOR SUBJECT AI
  if (consecutive_chars) {
    error.push({err_code: "GENERIC_MSG", data: {msg: document.getElementById("message_consec_chars").innerHTML}});
  }
  */

  if (check_forbidden_rx(subject)) {
    error.push({err_code: "GENERIC_MSG", data: {msg: ai_warnings['EMAIL_FORBIDDEN_TITLE']}});
  }

  return error;
}

Validator.ai_form.validate_not_forbidden_words_body = function () {
  var error = [];
  var body = $(this).val();

  if (check_forbidden_rx(body)) {
    error.push({err_code: "GENERIC_MSG", data: {msg: ai_warnings['EMAIL_FORBIDDEN_DESCRIPTION']}});
  }


  return error;
}

Validator.ai_form.on_validator_success = function (){
  Validator.ai_form.show_message.call(this, [{err_code: "GENERIC_MSG", data: {msg:"Ok!"}}],"success", ".error");
};

Validator.ai_form.validate_phone = function (data){
  var error = [];
  var phone = $(this).val();
  var min_size = data[0];
  var error_phone_empty = data[1];
  var error_phone_too_short = data[2];

  if("" == phone) {
    error.push({err_code: error_phone_empty, data: {}});
  }else if( phone.length < min_size) {
    error.push({err_code: error_phone_too_short, data: {}});
  }

  return error;
}

Validator.ai_form.validate_zipcode = function (data){
  var category_group = $(this).closest("form").find("#category_group").val();
  var code_size = data[0];
  var parent_cat = parseInt(category_group/1000) * 1000;

  //Only required for real estate
  if(parent_cat == 1000) {
    return Validator.validate_string.call(this,["9", code_size, "9", code_size]);
  }
  return [];
}
Validator.ai_form.check_empty_name = function (data){

  var error = [];
  var msg_name_empty_person = data[0];
  var msg_name_empty_company = data[1];
  // Check empty
  if("" == $(this).val()) {
     // message depends on if checked company or person
     error.push({err_code:  $('#aiform input[name=company_ad]:checked').val() == "1" ?  msg_name_empty_company : msg_name_empty_person , data: {}});
    }
    return error;
  }

  Validator.ai_form.validate_price = function (data){
    var category_group = $(this).closest("form").find("#category_group").val();
    var type = form_key_lookup('type');
    var parent_cat = parseInt(category_group/1000) * 1000;

  //Only required for real estate
  if(parent_cat == 1000 && (type == 's' || type == 'u')) {
    return Validator.validate_not_empty.call(this,data);
  }
  return [];
}

Validator.ai_form.validate_body_length = function (data){
  var error = [];
  var largo = data[0];
  var code  = data[1];
  var objeto = $(this).val();
  var tamano = objeto.length;
  if(tamano > largo){
    error.push({err_code: code, data: {}});
  }
  return error;
}

Validator.ai_form.validateRutCheckAndFormater = function (data){
  var error = [];
  var input = $("#rut");
  var form = $('#aiform');
  var checkedValue = form.find('input[name=company_ad]:checked').val();
  var required = checkedValue == '1';
  //var errorDiv = $("#err_rut");

  if(input.val()=="" && required){
    error.push({err_code: data[1], data: {}});
  }else if(input.val()==""){
    //NOthing to do
  }else if(!$.Rut.validar(input.val())){
    error.push({err_code: data[1], data: {}});
  }else{
    input.val($.Rut.formatear(input.val(),true));
  }
  return error;
}


$(function(){new Validator(val_conf_ai.aiform)});

display_tooltip = function(args) {
  $('.tooltip').hide();
  $(args).blur(function(){
    $('.tooltip').hide();
  });
  if(bubble_msgs) {
    var cg = $("#category_group").val();
    var cg_msgs = bubble_msgs[cg];
    if(cg_msgs) {
      var element = args[0];
      var id = element.id;
      var msg = cg_msgs[id];
      if(msg) {
        var tooltip_div = $("#" + id + "_info");
        var text_field = tooltip_div.find("p.text");
        if(text_field.size() == 0) {
          text_field = tooltip_div.find("p.title");
        }
        text_field.html(msg);
        $(tooltip_div).fadeIn('fast');
      }
    }
  }
}

hide_tooltip = function(args) {
  var element = args[0];
  var id = element.id;
  var tooltip_div = $("#" + id + "_info");
  var text_field = tooltip_div.find("p.text");
  if(text_field.size() == 0) {
    text_field = tooltip_div.find("p.title");
  }

  var data = $(element).data("tooltip");
  if(data) {
    data.getTrigger().unbind("focus");
  }
}

function show_passwd_div() {
  $('#passwd').removeAttr("disabled");
  $('#passwd_ver').removeAttr("disabled");
  $('#main_passwd_div').show();
  $('#main_passwd_link').remove();
}


//STUFF for carbrand and carmodel

function array_unique(t) {
  var i, j;
  var a = [];
  var l = t.length;
  for(i=0; i<l; i++) {
    for(j=i+1; j<l; j++) {
      if (t[i] === t[j])
        j = ++i;
    }
    a.push(t[i]);
  }
  return a;
}

function sort_desc(a, b) {
  return parseInt(b, 10) - parseInt(a, 10)
}

function my_set_focus(my_name) {
  document.getElementById(my_name).focus();
}

function fill_version_select_cb(result, xmlhttp) {
  var resultArray = eval("[" + result + "]");
  var car_version_select = document.getElementById("carversion");
  var car_model_select = document.getElementById("carmodel");
  clear_select(car_version_select);
  clear_select(car_model_select);
  var i = 0;
  var versionArray = resultArray[0];
  for(var key in versionArray) {
    var versionObj = versionArray[key];
    var versionName = versionObj.name;
    if(versionName) {
      car_version_select.options[i+1] = new Option(versionName, key);
      i++;
    }
  }
  if(car_version_select.options.length > 1) {
    car_version_select.disabled = false;
  }
}

function fill_version_select(e, args) {

  var sel_id = this.getAttribute("data-sel_id");
  var carbrand = form_key_lookup('carbrand', sel_id);

  if(carbrand != ""){
    var request = document.formular.action.replace('verify', 'ajax_receiver') + '?carbrand=' + carbrand;
    ajax_request(request, '', fill_version_select_cb, e, false, 'GET');
  }else{
    var car_version_select = document.getElementById("carversion");
    var car_model_select = document.getElementById("carmodel");
    clear_select(car_version_select);
    clear_select(car_model_select);
  }
}

function clear_select(select) {
  select.disabled = "true";
  select.options.length = 1;
}

function fill_model_select_cb(result, xmlhttp) {
  var resultArray = eval("[" + result + "]");
  var car_model_select = document.getElementById("carmodel");
  clear_select(car_model_select);
  var i = 0;
  var modelsArray = resultArray[0];
  for(var key in modelsArray) {
    var modelObj = modelsArray[key];
    var modelName = modelObj.name;
    if(modelName) {
      car_model_select.options[i+1] = new Option(modelName, key);
      i++;
    }
  }
  if(car_model_select.options.length > 1) {
    car_model_select.disabled = false;
  }
}

function fill_model_select(e, args) {

  var sel_id = this.getAttribute("data-sel_id");
  var carbrand = form_key_lookup('carbrand', sel_id);
  var carversion = form_key_lookup('carversion', sel_id);

  if(carbrand != "" && carversion != ""){
    var request = document.formular.action.replace('verify', 'ajax_receiver') + '?carbrand=' + carbrand + '&carversion=' + carversion;
    ajax_request(request, '', fill_model_select_cb, e, false, 'GET');
  }else{
    var car_model_select = document.getElementById("carmodel");
    clear_select(car_model_select);
  }
}

/*
Cars functionality
*/

var select_brand;
var select_model;
var select_version;
var select_year;
var select_fuel;
var select_gearbox;
var select_cartype;

var persist_after_ajax = false;
var hasSubmitForm = false;
var selected_regdate;
var selected_gearbox;
var selected_fuel;
var selected_cartype;


function cars_get_selects() {
  select_brand = $('#brand');
  select_model = $('#model');
  select_version = $('#version');
  select_year = $('#regdate');
  select_fuel = $('#fuel');
  select_gearbox = $('#gearbox');
  select_cartype = $('#cartype');
}



function save_params_cars(){
  selected_year = $('#regdate').val();
  selected_fuel = $('#fuel').val();
  selected_gearbox = $('#gearbox').val();
  selected_cartype = $('#cartype').val();
}

function restore_params_cars(){
  if(typeof selected_year != "undefined" ) $('#regdate').val(selected_year);
  if(typeof selected_fuel != "undefined" )  $('#fuel')   .val(selected_fuel);
  if(typeof selected_gearbox != "undefined" ) $('#gearbox').val(selected_gearbox);
  if(typeof selected_cartype != "undefined" ) $('#cartype').val(selected_cartype);

}

function get_attr_options(obj) {
  return obj.prop?obj.prop('options'):obj.attr('options');
}

function add_other_option(obj, label) {
  var options = get_attr_options(obj);
  options[options.length] = new Option(label, "0");
}

function remove_leaving_tail(obj, skip) {
  var options=get_attr_options(obj);

  for(i=(options.length-1); i >= skip; i--) {
    $("#"+obj[0].name+" option[value='"+$(options[i]).val()+"']").remove();
  }

  obj.val("");
}

function disable_attributes(val) {
  select_year.attr('disabled', val);
  select_fuel.attr('disabled', val);
  select_gearbox.attr('disabled', val);
  select_cartype.attr('disabled', val);
}

function reset_attributes(){
  select_year.val("");
  select_fuel.val("");
  select_gearbox.val("");
  select_cartype.val("");
}

function handle_otros(level) {
  var i;
  var options;

  cars_get_selects();

  options = get_attr_options(select_year);

  if(level<1) {
    add_other_option(select_model, cars_others_labels.model);
    select_model.val("0");
    select_model.attr('disabled', true);
    selected_model = "0";
  }

  if(level<2) {
    add_other_option(select_version, cars_others_labels.version);
    select_version.val("0");
    select_version.attr('disabled', true);
    selected_version = "0";
  }
  reset_attributes();
  disable_attributes(true);
}

function set_model_by_brand(e, args) {
  var i;
  var options;

  cars_get_selects();
  if(!hasSubmitForm ){
    options = get_attr_options(select_model);

    remove_leaving_tail(select_model, 1);
    remove_leaving_tail(select_version, 1);

    selected_brand = select_brand.val();

    if(select_brand.val()=="") {
      select_model.attr('disabled', true);
      select_version.attr('disabled', true);
      handle_otros(0);
      select_model.val("");
      select_version.val("");
      selected_model = "";
      selected_version = "";
      disable_attributes(true);
    } else if(select_brand.val()=="0") {
      remove_leaving_tail(select_version, 1);
      handle_otros(0);
      select_model.attr('disabled', true);
      select_version.attr('disabled', true);
      disable_attributes(false);
    } else {
      select_model.attr('disabled', false);
      select_version.attr('disabled',true);
      reset_attributes();
      disable_attributes(true);

      var request = "/templates/common/ajax_cars.html?cg="+$('#category_group').val()+"&brand=" + select_brand.val();
      ajax_request(request, '', set_model_by_brand_cb, e, false, 'GET');
    }

    if(!persist_after_ajax){
      on_change_car_modify_title();
    }
  }else{
    hasSubmitForm = false;
    if(select_brand.val() != ""){
      var request = "/templates/common/ajax_cars.html?cg="+$('#category_group').val()+"&brand=" + select_brand.val();
      persist_after_ajax = true;
      ajax_request(request, '', set_model_by_brand_cb, e, false, 'GET');
    }

  }
}

function set_model_by_brand_cb(result, xmlhttp) {
  var options;
  var i;

  cars_get_selects();
  options = get_attr_options(select_model);

  selected_brand = eval("("+result+")");

  if(persist_after_ajax){
    persist_param_cars();
    restore_params_cars();
    focus_car_params();
    on_change_car_modify_title();
    persist_after_ajax = false;
  }
  else{
    $.each(selected_brand['models'], function(key, val) {
      if(val != undefined){
        options[options.length] = new Option(val["name"], val["key"]);
      }
    });
    add_other_option(select_model, cars_others_labels.model);
    add_other_option(select_version, cars_others_labels.version);
    selected_model = "";
    selected_version = "";
  }
  $('#model').attr('data-brand', $('#brand').val())
}

function set_version_by_model(e, args) {
  var i;
  var options;

  cars_get_selects();
  options = get_attr_options(select_version);

  remove_leaving_tail(select_version, 1);

  selected_model = select_model.val();

  if(select_model.val()=="") {
    handle_otros(1);
    select_version.val("");
    select_version.attr('disabled', true);
    selected_version = "";
    disable_attributes(true);
  } else if(select_model.val()=="0") {
    handle_otros(1);
    select_version.attr('disabled', true);
    disable_attributes(false);
  } else {
    reset_attributes();
    disable_attributes(true);
    select_version.attr('disabled', false);

    for(i=0; i<selected_brand['models'].length; ++i) {
      if(selected_brand['models'][i].key==select_model.val()) {
        selected_model = selected_brand['models'][i];
        break;
      }
    }

    $.each(selected_model['versions'], function(key, val) {
      if(val != undefined){
        options[options.length] = new Option(val["name"], val["key"]);
      }
    });
    add_other_option(select_version, cars_others_labels.version);
    selected_version = "";

  }
  on_change_car_modify_title();
}

function set_attributes_by_version(e, args) {
  var options;
  var i;
  var years=[];
  var fuels=[];
  var gearboxes=[];

  cars_get_selects();

  selected_version = select_version.val();

  if(select_version.val()=="") {
    handle_otros(2);
    disable_attributes(true);
  } else if(select_version.val()=="0") {
    handle_otros(2);
    disable_attributes(false);
  } else {
    disable_attributes(false);

    for(i=0; i<selected_model['versions'].length; ++i) {
      if(selected_model['versions'][i].key==select_version.val()) {
        selected_version = selected_model['versions'][i];
        break;
      }
    }

    options = get_attr_options(select_year);

    for(i=0; i<selected_version["attributes"].length; i++) {
      if(selected_version["attributes"][i] != undefined){
        years[years.length]=selected_version["attributes"][i]["year"];
        fuels[fuels.length]=selected_version["attributes"][i]["fuel"];
        gearboxes[gearboxes.length]=selected_version["attributes"][i]["gearbox"];
      }
    }
    years=array_unique(years.sort(sort_desc));
    fuels=array_unique(fuels);
    gearboxes=array_unique(gearboxes);
    select_year.val((years.length == 1)?years[0]:"");
    select_cartype.val(selected_version["cartype"]);
    select_fuel.val((fuels.length==1)?fuels[0]:"");
    select_gearbox.val((gearboxes.length==1)?gearboxes[0]:"");
  }

  on_change_car_modify_title();
}

function persist_param_cars(){

  cars_get_selects();

  if($("#category_group").val()=="2020"){

    /* delete options of model and version */
    remove_leaving_tail(select_model, 1);
    remove_leaving_tail(select_version, 1);

    if(selected_brand != null){

      disable_model = true;
      disable_version = true;
      disable_attribute = true;

      if(selected_brand != "" && selected_brand != "0"){

        options_model = get_attr_options(select_model);

        $.each(selected_brand['models'], function(key, val) {
          if(val != undefined){
            options_model[options_model.length] = new Option(val["name"], val["key"]);
          }
        });
        add_other_option(select_model, cars_others_labels.model);

        if(selected_brand['models'].length > 0)
          disable_model = false;

        if (typeof selected_model != 'undefined' && selected_model != null) {

          if(selected_model != "" && selected_model != "0"){
            if($.isNumeric(selected_model)){
              for(var i = 0; i < selected_brand['models'].length ; i++){
                if(selected_brand['models'][i]['key'] == selected_model){
                  selected_model = selected_brand['models'][i];
                  break;
                }
              }
            }
            select_model.val(selected_model['key']);

            options_version = get_attr_options(select_version);

            $.each(selected_model['versions'], function(key, val) {
              if(val != undefined){
                options_version[options_version.length] = new Option(val["name"], val["key"]);
              }
            });
            add_other_option(select_version, cars_others_labels.version);

            disable_version = false;

            /* if selected version */
            if(selected_version != null){

              if(selected_version != "" && selected_version != "0"){
                if($.isNumeric(selected_version)){
                  for(var i=0 ; i < selected_model['versions'].length ; i++ ){
                    if(selected_model['versions'][i]['key'] == selected_version){
                      selected_version = selected_model['versions'][i] ;
                      break;
                    }
                  }
                }

                select_version.val(selected_version['key']);
                disable_attribute = false;

              }else{
                select_version.val(selected_version);
                if(selected_version == "0" ){
                  disable_attribute = false;
                }
              }

            }
          }
          else{
            add_other_option(select_version, cars_others_labels.version);
            select_model.val(selected_model);
            select_version.val(selected_model);
            if(selected_brand['models'].length > 0)
              disable_model = false;
            if(selected_model == "0"){
              disable_version = true;
              disable_attribute = false;
            }
          }
        }

        select_model.attr('disabled', disable_model);
        select_version.attr('disabled',disable_version);
        disable_attributes(disable_attribute);

      }
      else{
        /* In case not selected brand or his value is other */
        add_other_option(select_model, cars_others_labels.model);
        add_other_option(select_version, cars_others_labels.version);
        select_model.attr('disabled', true);
        select_version.attr('disabled',true);

        if(selected_brand == "0")
          disable_attributes(false);
        else
          disable_attributes(true);

        select_brand.val(selected_brand);
        select_model.val(selected_brand);
        select_version.val(selected_brand);

      }
    }
    else{
      /* In case brand doesn't exist */
      add_other_option(select_model, cars_others_labels.model);
      add_other_option(select_version, cars_others_labels.version);
      select_model.attr('disabled', true);
      select_version.attr('disabled',true);
      disable_attributes(true);
      select_brand.val("");
      select_model.val("");
      select_version.val("");
    }
  }

  if(!persist_after_ajax){
    focus_car_params();
    on_change_car_modify_title();
  }
}

function check_car_selected(e, args){

  if($("#category_group").val()=="2020"){
    cars_get_selects();

    if(select_brand.val()!="" && select_brand.val()!="0"){

      var request = "/templates/common/ajax_cars.html?cg="+$('#category_group').val()+"&brand=" + select_brand.val();
      ajax_request(request, '', onload_model_by_brand_cb, e, false, 'GET');

    }else{
      if(select_brand.val()=="0" || select_brand.val()==""){
        selected_model = select_brand.val();
        selected_version = select_brand.val();
      }
    }

  }

}

function onload_model_by_brand_cb(result, xmlhttp) {
  var options;

  cars_get_selects();
  options = get_attr_options(select_model);

  eval(result);
  selected_brand['models'];

  for(i=0; i<selected_brand['models'].length; ++i) {
    if(selected_brand['models'][i].key==select_model.val()) {
      selected_model = selected_brand['models'][i];
      break;
    }
  }


  for(i=0; i<selected_model['versions'].length; ++i) {
    if(selected_model['versions'][i].key==select_version.val()) {
      selected_version = selected_model['versions'][i];
      break;
    }
  }

  if(select_version.val()=="0" || select_version.val()==""){
    selected_version = select_version.val();
  }

  if(select_model.val()=="0" || select_model.val()==""){
    selected_model = select_model.val();
    selected_version = select_model.val();
  }

  persist_param_cars();

}

function format_thousands_separator(e ,args){
  var element = e[0];

  if (element == null)
    return;

  var number =  element.value;
  var clean_number = number.replace(/\D/g, '');
  var upper_number = clean_number.length % 3;


  z = clean_number.substr(upper_number)

  z =  z.replace(/([0-9]{3})/mg, ".$1");
  if (upper_number > 0 ){
    z = clean_number.substr(0 , upper_number) + z;
  }else{
    z = z.substr(1);
  }
  element.value =  z;

}

function real_state_low_price(){
  var cg = $("#category_group").val();
  var $currencyUF = $('#currency_uf:checked');

  $("#warning_container").hide();

  if (!$currencyUF.length) {
    cg = cg === undefined ? $('#cat_id').val() : cg;

    if (cg !== undefined && cg.match(/1\d{3}/) !== null){
      var price = parseInt(_format_price_rm_cents(document.getElementById("price").value).replace(/\D/g,""));
      if(price < 20000 ){
        $("#warning_container p").text(ai_warnings['PRICE_PESOS_REAL_STATE'].es);
        $("#warning_container").show();
        return;
      }
    }

    pesos2uf();
  }
}


function real_state_price(){
  var $priceMessage = $('#warning_container_pesos');
  var cg = $("#category_group").val();

  /**
   * Excluded Categories
   * 1220: `Vendo`
   * 1240: `Arriendo`
   **/
  var excludedCategories = [1220, 1240];
  var isExcluded = excludedCategories.indexOf(parseInt(cg, 10)) !== -1;

  $priceMessage.hide();

  if(cg == undefined){
    cg = $("#cat_id").val();
  }

  if (cg != undefined && cg.match(/1\d{3}/) != null && !isExcluded){
    $priceMessage.show();
    return;
  }
}

function load_car_data_after_submit(args){
  cars_get_selects();
  select_brand.val(args[0]);
  select_model.val(args[1]);
  select_version.val(args[2]);
  select_year.val(args[3]);
  select_fuel.val(args[4]);
  select_gearbox.val(args[5]);
  select_cartype.val(args[6]);

  selected_brand = args[0];
  selected_model = args[1];
  selected_version = args[2];
  selected_year = args[3];
  selected_fuel = args[4];
  selected_gearbox = args[5];
  selected_cartype = args[6];

  hasSubmitForm = true;

}

function rutInit(e,args){
  var input = $("#rut");
  if(input.val()!="")
    input.val($.Rut.formatear(input.val(),true));
}

function getErrorMessage(args){
  var errorMsgs = val_conf_ai.aiform.error_msgs;
  for(var i=0; i< errorMsgs.length; i++){
    if(errorMsgs[i].cod == args)
      return errorMsgs[i].format;
  }
  return "";
}

function xiti_ai_utype_handler(self){
  create_acc = $(self).is(':checked');
  pri = $('#p_ad').is(':checked');
  pro =$('#c_ad').is(':checked');
  if(create_acc){
    if(pri){
      return xt_click(this, 'C', '', 'Register page::register page::register page::register private', 'N')
    }
    if(pro){
      return xt_click(this, 'C', '', 'Register page::register page::register page::register pro', 'N')
    }

    return xt_click(this, 'C', '', 'Register page::register page::register page::ad insertion', 'N')
  }
}

/*
 * @Name only_numbers_and_letters
 * @description
 * Prevent write characters other than number or letter
 *
 * @params {Object} element DOM e
 * @params {array} list of errors
 *
 */

function only_numbers_and_letters (e, args) {
  var input = '';
  var old_position = '';

  if (e != undefined) {
    input = document.getElementById(e[0].name);
  }

  if (input) {
    old_position = getCaretPosition(input);
    input.value = input.value.replace(/[^A-Za-z0-9]/g,'');
  }

  if (e[1] === 'keyup') {
    setCaretPosition(name, old_position);
  }
}

function not_start_zero (e, args) {
  if (e == null) {
    return;
  } else {
    el = e[0];
  }

  if (el.value[0] == 0) {
    el.value = el.value.replace(/^0+/, '');
  }
}
