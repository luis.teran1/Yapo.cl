function getCaretPosition (ctrl) {

  var CaretPos = 0;
  // IE Support
  if (document.selection) {

     ctrl.focus ();
    var Sel = document.selection.createRange ();

    Sel.moveStart ('character', -ctrl.value.length);

    CaretPos = Sel.text.length;
  }
  // Firefox support
  else if (ctrl.selectionStart || ctrl.selectionStart == '0')
    CaretPos = ctrl.selectionStart;

  return (CaretPos);

}

/*
 * @Name only_numbers_and_letters
 * @description
 * Prevent write characters other than number or letter
 *
 * @params {Object} element DOM e
 * @params {array} list of errors
 *
 */

function only_numbers_and_letters (e, args) {
  var input = '';
  var old_position = '';

  if (e != undefined) {
    input = document.getElementById(e[0].name);
  }

  if (input) {
    old_position = getCaretPosition(input);
    input.value = input.value.replace(/[^A-Za-z0-9]/g,'');
  }

  if (e[1] === 'keyup') {
    setCaretPosition(name, old_position);
  }
};
