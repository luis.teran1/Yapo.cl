<?php
/*
 * Newad application
 */

require_once('autoload_lib.php');
require_once('blocket_store.php');
require_once("weeks.php");
require_once("dyn_config.php");
require_once("util.php");
require_once('redis_cardata.php');
require_once('JSON.php');
require_once("httpful.php");

use Yapo\Upselling;
use Yapo\Products;
use Yapo\Ads\MultiInsertChecker;
use Yapo\Cpanel;
use Yapo\PaymentDeliveryQueryService;
use Yapo\DuplicatedAdDesktop;

openlog("NEWAD", LOG_ODELAY, LOG_LOCAL0);

/* XXX bconf these */
$ignore_fields['edit'] = array('store');
$ignore_fields['renew'] = array('name', 'email', 'category', 'type', 'region', 'city', 'area', 'store', 'company_ad');
$ignore_fields['deleted'] = array('email');

$hide_fields['refused'] = array();
$hide_fields['deleted'] = array('email');

/* JavaScripts */
$script_list = bconf_get($BCONF,"*.common.newad.headscripts");
$script_list_deletion = bconf_get($BCONF,"*.common.deletion.headscripts");
$styles_list_deletion = bconf_get($BCONF,"*.common.deletion.headstyles");
$styles_list_deleted = bconf_get($BCONF,"*.common.deleted.headstyles");
$script_list_deletion_success = bconf_get($BCONF,"*.common.deletion_success.headscripts");

define("BAD_CLEANUP", 1);
define("BAD_UNSET", 2);

/*
 * Syslog
 */

if (isset($_SERVER['PATH_INFO'])) {
	syslog(LOG_INFO, log_string()." {$_SERVER['PATH_INFO']}");
}


/* session for logged in users */
$account_session;
$account_util;

class blocket_newad extends blocket_application {
	var $ad;
	var $ad_id;
	var $list_id;
	var $raw_list_id; /*this is the real list_id, because list_id was being replaced by mixed with some hashes*/
	var $action;
	var $refused_reason;
	var $refused_refusal_text;
	var $already_paid_extra_images;
	var $company_ad;
	var $name;
	var $business_name;
	var $rut;
	var $ai_create_account;
	var $add_location;
	var $communes;
	var $address;
	var $address_number;
	var $geoposition;
	var $geoposition_is_precise;
	var $email;
	var $phone_type;
	var $area_code;
	var $phone;
	var $phone_hidden;
	var $account;
	var $paid;
	var $images;
	var $stores;
	var $thumbnail_digest;
	var $digest_present;
	var $type_list;
	var $orig_list_time;
	var $refusal_edited;
//	var $no_footer_links = 1;
	var $action_id;
	var $pre_refuse_action_type;
	var $focus_extra_images;
	var $loadaction = true;
	var $has_gallery = 0;
	var $has_label = 0;
	var $has_weekly_bump = 0;
	var $has_daily_bump = 0;
	var $store_token;
	var $store_id;
	var $login_id;
	var $addimages;
	var $order_id;
	var $params;
	var $red_arrow_price;
	var $voucher;
	var $voucher_result;
	var $ad_price;
	var $area_unset;
	var $link_type;
	var $job_category;
	var $service_type;
	var $footwear_type;
	var $services; //for real estate (at the beginig)
	var $duplicated_detected = 0;
	var $publish_similar = false;
	var $ext_code;
	var $equipment;

	var $offseason;
	var $offseason_checked_descr;
	var $peakseason;
	var $peakseason_checked_descr;
	var $available_weeks;
	var $available_weeks_checked_descr;

	var $show_password;
	var $salted_passwd;

	var $show_upselling_in_form = 1;

	var $hide_address;
	/* XXX Remove when old category are old */
	var $allow_category_change = false;

	var $set_uid;

	/* Track where user came from ('source') */
	var $ht;

	/* Delete ad reasons page variables */
	var $del_reason;
	var $user_text;
	var $user_text_testimonial;
	var $del_timer = 0;

	/* id for unfinished ads CHUS 229 */
	var $unf_ad_id;
	var $want_bump;
	var $upselling_product;
	var $upselling_product_label;

	/* keeping selected options from user between states */
	var $ai_accept_conditions;

	var $acc_status;
	var $old_category;

	function blocket_newad() {
		global $BCONF;
		global $session;
		global $account_session;
		global $account_util;

		$account_session = new AccountSession();
		$account_util = new AccountUtil();
		$this->add_static_vars(array('company_ad', 'name', 'email', 'phone', 'phone_hidden', 'area', 'hide_address', 'phone_type', 'area_code', 'plates'));
		$this->add_volatile_vars(array('no_footer_links'));

		$this->init('load', bconf_get($BCONF, "*.common.session.instances.ai"));
		$this->headstyles = bconf_get($BCONF,"*.common.newad.headstyles");

		$this->application_name = 'ai';

		if ($session->new_session) {
			syslog(LOG_INFO, log_string()."New session");
			syslog(LOG_INFO, log_string()."Browser: ".$_SERVER['HTTP_USER_AGENT']);
		}
	}

	function get_state($statename) {
		switch($statename) {
		case 'load':
			return array('function' => 'newad_load',
				     'params' => array('id' => 'f_newad_id', 'cmd', 'p' => 'f_passwd', 'ht' => 'f_integer', 'unf_id' => 'f_integer'),
				     'reset_progress' => true,
				     'method' => 'get',
				     'allowed_states' => array('action', 'load'));
		case 'similar':
			return array('function' => 'newad_similar',
					'params' => array('id' => 'f_integer'),
					'method' => 'get');
		case 'action':
			return array('function' => 'newad_action',
				     'params' => array('cmd', 'passwd', 'store_username', 'store_passwd', 'password_hash' => 'f_passwd', 'deletion_reason' => 'f_deletion_reason', 'user_text' => 'f_string', 'user_text_testimonial' => 'f_string', 'deletion_timer' => 'f_deletion_timer'),
				     'method' => 'post',
				     'allowed_states' => array('load', 'deleted', 'form', 'bumped'));
		case 'bumped':
			return array('function' => 'newad_bumped',
				     'allowed_states' => array('bumped'),
				     'method' => 'get');

		case 'confirm_delete':
			return array('function' => 'newad_confirm_delete',
					'allowed_states' => array ('confirm_delete'),
					'method' => 'get');
		case 'delete':
			return array('function' => 'newad_delete',
				     	'params' => array('passwd', 'deletion_reason' => 'f_deletion_reason', 'user_text' => 'f_string',  'user_text_testimonial' => 'f_string', 'deletion_timer' => 'f_deletion_timer'),
					'method' => 'post',
					'allowed_states' => array ('confirm_delete'),
					);
		case 'deleted':
			return array('function' => 'newad_deleted',
				     'allowed_states' => array('deleted', 'testimonial'),
				     'method' => 'get');
		case 'activated':
			return array('function' => 'newad_activated',
				     'allowed_states' => array('activated'),
				     'method' => 'get');
		case 'testimonial':
			return array('function' => 'newad_testimonial',
				     'params' => array('user_text_testimonial' => 'f_string'),
				     'allowed_states' => array('deleted','testimonial'),
				     'method' => 'get');
		case 'blocked':
			return array('function' => 'newad_blocked',
					'method' => 'get');
		case 'form':
			return array('function' => 'newad_form',
				     'method' => 'get',
				     'params' => array('mv' => 'f_string'),
				     'allowed_states' => array('form', 'preview'));

		case 'check_duplicates':
			return array('function' => 'newad_check_duplicates',
				     'method' => 'post',
				     'params' => array('back' => 'f_bool', 'mv' => 'f_string'),
				     'allowed_states' => array('check_duplicate', 'is_duplicate', 'form', 'preview'));

		case 'is_duplicate':
			return array('function' => 'newad_has_duplicates',
				     'method' => 'get',
				     'params' => array('mv' => 'f_string'),
				     'allowed_states' => array('check_duplicate', 'is_duplicate', 'form', 'preview'));

		case 'upload_image':
			return array('function' => 'newad_upload_image',
				     'method' => 'both');

		case 'verify':
			return array('function' => 'newad_verify',
				     'params' => array('passwd_ver' => 'f_new_passwd', 'email_confirm' => 'f_string', 'check_type_diff' => 'f_integer', 'extra_images' => 'f_bool', 'ai_accept_conditions' => 'f_bool'),
				     'method' => 'post',
				     'allowed_states' => array('form', 'preview'));
		case 'preview':
			return array('function' => 'newad_preview',
				     'method' => 'get',
				     'params' => array('cl' => 'f_bool', 'sp' => 'f_show_password'),
				     'allowed_states' => array('preview'));
		case 'create':
			return array('function' => 'newad_create',
				     'params' => array('back' => 'f_bool', 'pay_type', 'store_passwd' => 'f_new_passwd',
				     'gallery' => 'f_bool', 'ai_accept_conditions' => 'f_bool'),
				     'method' => 'post',
				     'allowed_states' => array('preview', 'is_duplicate', 'check_duplicates'));

		case 'restart':
			return array('function' => 'newad_restart',
				     'method' => 'get'
				     );

		case 'confirm':
			return array('function' => 'newad_confirm',
				     'method' => 'get',
				     'allowed_states' => array('confirm', 'restart'));

		case 'show_hide_done':
			return array('function' => 'newad_show_hide_done',
					'method' => 'get',
					'allowed_states' => array('show_hide_done'));

		case 'error':
			return array('function' => 'newad_error',
				     'method' => 'get');

		/*
		 * Ajax categories
		 */
		case 'cat_tmpl':
			return array('function' => 'cat_tmpl',
					'method' => 'get',
					'params' => array('category' => 'f_integer', 'shown_category' => 'f_integer', 'type' => 'f_type', 'shown_type' => 'f_type',
									'shown_company_ad' => 'f_integer', 'company_ad' => 'f_integer', 'estate_type' => 'f_integer', 'store' => 'f_integer',
									'region' => 'f_integer', 'footwear_gender' => 'f_integer', 'currency' => 'f_string'));

		case 'ajax_receiver':
			return array('function' => 'fipe_searcher',
					'method' => 'get',
					'params' => array('carbrand' => 'f_integer', 'carversion' => 'f_integer'));

		case 'want_bump':
			return array('function' => 'want_bump',
				     'method' => 'get');
		case 'pay_upselling':
			return array('function' => 'pay_upselling',
				     'method' => 'get');
		}
	}

	/* Start a clean instance */
	function newad_restart() {
		header("Location: /ai");
		return 'FINISH';
	}

	function newad_similar($id) {
		bLogger::logInfo(__METHOD__, "SIMILAR: entering newad_similar (list_id:$this->list_id, action_id:$this->action_id)");
		$this->publish_similar = true;
		$this->ad->subject = '';
		$this->images = array();
		$this->loadaction = true;
		$this->action = null;
		return 'form';
	}

	/*
	 * User has entered newad, possibly with an id.
	 * Redirect to form or display action select page.
	 */
	function newad_load($id, $cmd, $passwd, $ht, $unf_id = NULL) {
		global $script_list;
		global $BCONF;
		$data = array();

		// Handle case of persistent variables phone & area_code
		// only if we have name keep the phone and area_code
		if (empty($this->name)) {
			$this->phone = "";
			$this->area_code ="";
		}
		/* Get the source or, by default, web */
		if (isset($ht) && $ht) {
			/* XXX we dont check if $ht is in bconf. In this case $ht will be empty for source. */
			$this->ht = $ht;
		} else {
			$this->ht = bconf_get($BCONF, '*.ai.source.default');
		}

		syslog(LOG_INFO, log_string()."newad_load".($this->ht ? " (source:".$this->ht.":".ht_to_source($this->ht).")" : ""));
		if ($this->ad) {
			return $this->display_action_page(null,null,$passwd);
		} elseif (is_array($id)) {
			syslog(LOG_INFO, log_string()."Requested to load ad information for id : $id (" . serialize($id) . ")");

			/* Load ad */
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			/* When loading an ad from the store application sent authentification data */
			if (@$id['action'] == 'store_deleted') {
				global $session;
				$store = new blocket_store(false);
				if ($session->load_active_instance($store, get_class($store), "0") &&
					$data['is_admin'] != 1) {
					$transaction->auth_type = 'store';
					$this->store_token =true;
				}
				$transaction->populate($id);
			} elseif (@$id['action'] == "fromcmd") {
				$transaction->add_data('id', $id["id"]);
				$transaction->add_data('action', $cmd);
				$id['action'] = $cmd;
			} else
				$transaction->populate($id);

			if ($transaction->auth_type)
				$reply = $transaction->send_admin_command('loadad');
			else
				$reply = $transaction->send_command('loadad');
			$this->complete_phone_params($reply['ad']);
			$this->raw_list_id = $reply['ad']['list_id'];

			/* Warn if ad is too old */
			if (isset($reply['error']) && $reply['error'] == 'ERROR_AD_TOO_OLD') {
				syslog(LOG_INFO, log_string()."Ad data too old to load");
				$this->ad_too_old = 1;
				return 'error';
			} if (isset($reply['error']) && $reply['error'] == 'ERROR_DELETED') {
				syslog(LOG_INFO, log_string()."Ad already deleted");
				$this->deleted = 1;
				return 'error';
			} if (isset($reply['error']) && $reply['error'] == 'ERROR_DEACTIVATED') {
				syslog(LOG_INFO, log_string()."Ad deactivated");
				$this->deactivated = 1;
				return 'error';
			} elseif($transaction->has_error()) {
				$this->set_errors($transaction->get_errors());
				syslog(LOG_INFO, log_string()."No subject found for ad $id");
				return 'error';
			}

			if (isset($reply['images']) && is_array($reply['images'])) {
				for ($i = 0; $i < count($reply['images']); $i++)
					$this->images[] = $reply['images'][$i]['name'];
			}

			/* Check for sendpass links */
			global $account_session;

			if(isset($reply['ad']['acc_status']))
				$this->acc_status=$reply['ad']['acc_status'];
			else
				unset($this->acc_status);

			if (!empty($passwd)) {
				if (!isset($reply['ad']['salted_passwd']))
					$reply['ad']['salted_passwd'] = '';
				if (check_password_hash($passwd, $reply['ad']['salted_passwd'])) {
					$this->salted_passwd = $reply['ad']['salted_passwd'];
					bLogger::logDebug(__METHOD__, "Setting salted_passwd to {$this->salted_passwd}");
					if($this->is_acc_active()){
						$this->check_account_session($data,$reply['users']['email']);
					}

				} else {
					bLogger::logDebug(__METHOD__, "UNsetting salted_passwd : {$this->salted_passwd}");
					unset($this->salted_passwd);
					$this->set_errors(array('passwd' => "ERROR_ACTION_PASSWORD_HASH_ERROR"));
					return $this->display_action_page($cmd, $data);
				}
			}else{
				/*
				FT823: Create account is not working with the edit link from half time email
				We add the password for the ad if the user come from edit link from half time mail
				*/
				if($cmd == 'renew'){
					$this->salted_passwd = $reply['ad']['salted_passwd'];
					bLogger::logDebug(__METHOD__, "Setting salted_passwd to {$this->salted_passwd}");
				}

				if($this->is_acc_active()){
					if (preg_match('/^[0-9]{7,}\.[0-9]+$/', $id['id'])) {
						$trans_extra_check = new bTransaction();
						$trans_extra_check->add_data("list_id",$this->raw_list_id);
						$extra_check_reply=$trans_extra_check->send_command("fetch_extra_check");
						$validate_with_extracheck = $extra_check_reply['list_id'].$extra_check_reply['extra_check'];
						if($validate_with_extracheck == $id['id']){
							$this->check_account_session($data,$reply['users']['email']);
						}
					}else{
						if(@$id['action'] == 'refused'
						   && $id['id'] == $reply['ad']['ad_id']
						   && @$id['action_id'] == $reply['actions']['action_id']){
							$this->check_account_session($data,$reply['users']['email']);
						}
					}
				}
			}

 			if (isset($reply['ad']['orig_list_time'])) {
				$this->orig_list_time = strtotime(substr($reply['ad']['orig_list_time'], 0, 19), 0);
				if ($this->orig_list_time == -1)
					$this->orig_list_time = null;
			}

			if (isset($reply['params']['link_type']))
				$this->link_type = $reply['params']['link_type'];

			/* Used by the preview step to hide the gallery option if the ad has one already */
			$this->has_gallery = $this->has_product($reply,'gallery');
			$this->has_label = $this->has_product($reply,'label');
			$this->has_weekly_bump = $this->has_product($reply,'weekly_bump');
			$this->has_daily_bump =  $this->has_product($reply,'daily_bump');

			/* Check store status */
			if (isset($reply['ad']['store_id']) && @$reply['store']['active'] != "t"
					&& @$reply['store']['active'] != 1) {
				syslog(LOG_INFO, log_string()."Unsetting store " . $reply['ad']['store_id']);
				unset($reply['ad']['store_id']);
			}

			$this->ad = new bAd();
			$this->ad->populate($reply, true);

			$this->handle_reply($reply);
			$this->ad_id = $reply['ad']['ad_id'];
			$this->ad_status = $reply['ad']['status'];
			$this->old_category = $reply['ad']['category'];
			if (isset($reply['ad']['salt']))
				$this->salt = $reply['ad']['salt'];

			/* If editing an active ad, show action select page. */
			if (!isset($id['action'])) {
				$this->list_id = $id['id'];
				$this->loadaction = false;

				/* CHUS 233:
				 * Link to edit from "edita and accept" and "accept" mails,
				 * should go directly to edit page, not to the options "editar/borrar"
				 *
				 * if the cmd == 'edit' and passwd != '' (the pass was validated ina a previews step)
				 * we send to the form directly
				 *
				 */

				if (($this->logged_account_owns_ad() || $passwd != '') && in_array($cmd, array('edit', 'renew'))) {
					if (isset($reply['ad']['status']) && $reply['ad']['status'] == 'deleted') {
						return $this->display_action_page($cmd, $data, $passwd);
					} else {
						return $this->newad_action($cmd, '', '', '', $passwd, '', '', '', '');
					}
				} else {
					return $this->display_action_page($cmd, $data, $passwd);
				}

			} else {
				syslog(LOG_INFO, log_string()."Ad type = {$id['action']}, loading ad information");
				switch($id['action']) {
				case 'refused' :
					if (isset($reply['actions']) && isset($reply['actions']['has_parent']) && $reply['actions']['has_parent'] == 't') {
					 	syslog(LOG_INFO, log_string()."Refusal already edited");
						$this->refusal_edited = 1;
						return 'error';
					}
					if (isset($reply['ad_change_params'])) {
						/* user should see the refused changes. apply ad_change_params to ad */

						/* CATEGORY CHANGED. New category adparams must be created in $this->ad object
						   and old category adparams must be unset.*/
						if (isset($reply['ad_change_params']['category'])) {
							// get old category params
							get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $params_preedition);
							// get new category params
							$params_postedition = array('category' => $reply['ad_change_params']['category'],
							'parent' => bconf_get($BCONF, "*.cat.{$reply['ad_change_params']['category']}.parent"),
							'type' => isset($reply['ad_change_params']['type'])?$reply['ad_change_params']['type']:$reply['ad']['type'],
							'company_ad' => isset($reply['ad_change_params']['company_ad'])?$reply['ad_change_params']['company_ad']:$reply['ad']['company_ad']);
							get_settings(bconf_get($BCONF,"*.category_settings"), "params",
								create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
								create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
								$params_postedition);

							$this->ad->category_group = $reply['ad_change_params']['category'];
							// Create new adparams in bAd object
							foreach (array_keys($params_postedition) as $key => $value) {
								if (!array_key_exists($value, get_object_vars($this->ad)) ) {
									$this->ad->$value = '';
								}
							}

							// Unset from bAd object not needed old category adparams
							foreach (explode(',', $params_preedition) as $key => $value) {
								if (array_key_exists($value, get_object_vars($this->ad))
								&& !in_array($value, array_keys($params_postedition))) {
									unset($this->ad->$value);
								}
							}
						}

						foreach ($reply['ad_change_params'] as $key => $value) {
							if (!strncmp($key, "param_", 6))
								$key = substr($key, 6);

							if (array_key_exists($key, get_object_vars($this->ad))) {
								$this->ad->$key = $value;
							} elseif (preg_match('/\d+$/', $key)) {
								// Apply changes to numbered (array-based) ad_params
								$match_data = array();
								preg_match('/([^\d]*)(\d+)$/', $key, $match_data);
								if (count($match_data) == 3) {
									$arr_idx = intval($match_data[2])-1;
									$arr_key_ref = &$this->ad->$match_data[1];
									$arr_key_ref[$arr_idx] = $value;
								} else {
									syslog(LOG_INFO, log_string()."Unexpected number of elements after split of numbered adparam '".$key."'");
								}
							}
						}
						$this->ad->cleanup();
					}
					if (isset($reply['ad_image_changes'])) {
						ksort($reply['ad_image_changes']);
						$this->images = array();
						$this->thumbnail_digest = array();
						$this->digest_present = array();
						$digest_found = 0;
						foreach ($reply['ad_image_changes'] as $image_id => $image) {
							if (empty($image['name']))
								continue;
							$this->images[] = $image['name'];
							$this->digest_present[] = ($image['is_new'])? 0: 1;
							foreach ( $reply['digest'] as $ind => $values) {
								if(@$values['db_name'] == $image['name']) {
									 if (isset($values['digest'])) {
										$this->thumbnail_digest[] = $values['digest'];
										$digest_found = 1;
									}
								}
							}
							if (!$digest_found)
								$this->thumbnail_digest[] = "";
							else
								$digest_found = 0;
						}
					}
					$this->action_id = $reply['actions']['action_id'];
					$this->refused_reason = @$reply['refused']['reason'];
					$this->refused_refusal_text = @$reply['refused']['refusal_text'];
					if (isset($reply['pre_refuse_action_type']['action_type'])) {
						$this->pre_refuse_action_type = $reply['pre_refuse_action_type']['action_type'];
					}
					$this->action = 'refused';
					if (!empty($reply['action_params']['cleared_by']) || ($reply['payment']['pay_type'] == 'campaign' && !empty($reply['action_params']['campaign_id']))) {
						$this->campaign = $reply['action_params']['campaign_id'];
						$this->campaign_ad_id = $reply['action_params']['cleared_by'];
						$this->pay_type = 'campaign';
					}
					break;

				case 'store_deleted' :
					if ($reply['ad']['status'] == 'active')
						$this->action = 'renew';
					else
						$this->action = 'deleted';
					break;

				case 'delete':
					/* Somewhat hackish, use the long id as list_id for deletead. */
					$this->list_id = $id['id'];
					return 'confirm_delete';

				default :
					$this->action = $id['action'];
					break;
				}
			}

			if (!empty($reply['ad']['salted_passwd'])) {
				$this->salted_passwd = $reply['ad']['salted_passwd'];
			}
		}
		/* load unfinished ad based on unf_id */
		else if ($unf_id > 0 && $passwd != "") {

			$unf_ad_id = (int)$unf_id;

			$unf_transaction = new bTransaction();
			$unf_transaction->add_data('log_string', log_string());

			$unf_transaction->add_data('unf_ad_id', $unf_ad_id);
			$unf_reply = $unf_transaction->send_command('get_unfinished_ad');

			if ($unf_transaction->has_error()) {
				syslog(LOG_INFO, log_string()." Error when try to get unfinished ad (Iluvatar killed three kittens)");
			}
			else {
				$unf_data = unserialize(base64_decode($unf_reply["unf_data"]));

				if ($unf_reply["unf_status"] == "active") {

					$unf_hash = sha1($unf_reply["unf_email"].$unf_reply["unf_session_id"]);

					if ($passwd == $unf_hash) {
						$this->ad = $unf_data->ad;
						$this->images = $unf_data->images;
						$this->unf_ad_id = $unf_ad_id;
						$this->thumbnail_digest = $unf_data->thumbnail_digest;
						$this->digest_present = $unf_data->digest_present;
						$this->show_password = $unf_data->show_password;
						$this->salted_passwd = $unf_data->salted_passwd;
						bLogger::logDebug(__METHOD__, "Setting salted_passwd to {$this->salted_passwd}");
						if (isset($unf_data->rut)){
							$this->rut = $unf_data->rut;
						}
						if (isset($unf_data->ai_create_account)){
							$this->ai_create_account = $unf_data->ai_create_account;
						}

						if (isset($unf_data->show_image_warn)) {
							$this->show_image_warn = 1;
						}
						if (isset($unf_data->show_price_warn)) {
							$this->show_price_warn = 1;
						}
						if (isset($unf_data->show_body_warn)) {
							$this->show_body_warn = 1;
						}

						if (isset($unf_data->service_type)) {
							$this->service_type = $unf_data->service_type;
						}
						if (isset($unf_data->job_category)) {
							$this->job_category = $unf_data->job_category;
						}
						if (isset($unf_data->services)) {
							$this->services = $unf_data->services;
						}
						if (isset($unf_data->equipment)) {
							$this->equipment = $unf_data->equipment;
						}
						if (isset($unf_data->footwear_type)) {
							$this->footwear_type= $unf_data->footwear_type;
						}

						return 'preview';
					}
				}
				else {
					$this->unf_ad_id = NULL;
				}
			}
		}
		else if ($id === false) {
			return 'error';
		}

		/* Redirect to the form page. */
		$this->area_unset = empty($this->ad->area) ? 1 : 0;

		$this->ai_create_account = 'on';
		return 'form';
	}

	/* User has choosen an option on the action select page. */
	function newad_action($cmd, $passwd, $store_username, $store_passwd, $password_hash, $deletion_reason, $user_text, $user_text_testimonial, $deletion_timer) {
		global $session;
		global $BCONF;
		global $account_session;

		if (!empty($password_hash) && check_password_hash($password_hash, $this->salted_passwd)) {
			$passwd = $this->salted_passwd;
		}
		// Check if the password belongs to the account of the publisher's account
		else if (strlen($passwd) && $account_session->login($this->ad->email, $passwd)) {
			syslog(LOG_INFO, log_string()."ad:".$this->list_id." accesed using the account password");
			$passwd = $this->get_ad_password($this->list_id);
		}
		else if ($account_session->is_blocked($this->ad->email)) {
			$this->set_errors(array('passwd' => 'ACC_ERROR_ACCOUNT_BLOCKED'));
			$this->del_reason = $deletion_reason;
			return 'load';
		}
		else if (empty($passwd) && $this->logged_account_owns_ad()) {
			syslog(LOG_INFO, log_string()."ad:".$this->list_id." accesed by account session");
			$passwd = $this->get_ad_password($this->list_id);
		}
		else if (strlen($passwd) && $this->salt) {
			$salt = $this->salt;
			$loops = bconf_get($BCONF, "ai.passwd.stretch_loops");

			if (preg_match('/^\$([0-9]+)\$(.*)/', $salt, $regs)) {
				$loops = $regs[1];
				$salt = $regs[2];
			}

			for ($i = 0; $i < $loops; $i++)
				$passwd = sha1($salt . $passwd);
			$passwd = "\$$loops\$$salt$passwd";
		}

		$store = new blocket_store(false);

		syslog(LOG_INFO, log_string()."newad_action ($cmd)");

		if (!$this->ad || !$this->list_id) {
			return 'load';
		}

		if (!$cmd) {
			$this->set_errors(array('action' => 'ERROR_AD_ACTION_MISSING'));
			return 'load';
		}
		$this->action = $cmd;

		/* FT1161: Separate function for edit command an hidden status should redirect 404. */
		if ($cmd == 'edit' && in_array($this->ad_status, array('hidden', 'deactivated'))){
			return 'error';
		}
		/* Separate function for delete command. */
		if ($cmd == 'delete'){
			if ( (isset($passwd) && isset($deletion_reason)) ||
				(Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) ) {
				return $this->newad_delete($passwd, $deletion_reason, $user_text, $user_text_testimonial, $deletion_timer);
			}else{
				return $this->display_action_page($cmd, null, $passwd);
			}
		}
		/* Separate function for delete command. */
		if ($cmd == 'deactivate'){
			if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
				if( $this->deactivate_ad($from_cp = true, $passwd, $deletion_reason, $user_text, $user_text_testimonial, $deletion_timer))
					return 'deleted';
				else
					return 'load';
			}else{
				return $this->display_action_page($cmd, null, $passwd);
			}
		}
		if ($cmd == 'activate'){
			if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
				if( $this->activate_ad($from_cp = true, $passwd))
					return 'activated';
				else
					return 'load';
			}else{
				return $this->display_action_page($cmd, null, $passwd);
			}
		}
		/* Separate function for store command. */
		if ($cmd == 'store')
			return $this->newad_store($store_username, $store_passwd);

		/* Separate function for bump command. */
		if ($cmd == 'bump')
			return $this->newad_do_apply_product();
		if ($cmd == 'admin_renew') {
			$this->newad_do_apply_product();
			$this->action = 'edit';
			$cmd = 'edit';
		}

		if (in_array($cmd, array('hide', 'show'))) {
			return $this->newad_show_hide($cmd);
		}

		/* Reload the ad with complete data, and check password. */
		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('id', $this->list_id);
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$transaction->auth_type = 'admin';
		else if ($this->store_token) {
			if ($session->load_active_instance($store, get_class($store), "0")) {
				$transaction->auth_type = 'store';
			}
		} elseif ($passwd)
			$transaction->add_data('passwd', $passwd);
		$transaction->add_data('action', $cmd);
		if ($transaction->auth_type)
			$reply = $transaction->send_admin_command('loadad', true, false);
		else
			$reply = $transaction->send_command('loadad');
		$this->complete_phone_params($reply['ad']);

		if ($transaction->has_error()) {
			$errors = $transaction->get_errors();
			$this->set_errors($errors);
			syslog(LOG_INFO, log_string()."User denied to ($cmd) ad");
			return 'load';
		}
		$this->loadaction = true;

		/* Check store status */
		if (@$reply['store']['active'] != "t" && @$reply['store']['active'] != 1) {
			syslog(LOG_INFO, log_string()."Unsetting store " . @$reply['ad']['store_id']);
			unset($reply['ad']['store_id']);
		}

		$this->ad->populate($reply, true);

		$this->handle_reply($reply);
		if (isset($reply['ad']['orig_list_time'])) {
			$this->orig_list_time = strtotime(substr($reply['ad']['orig_list_time'], 0, 19), 0);
			if ($this->orig_list_time == -1)
				$this->orig_list_time = null;
		}

		syslog(LOG_INFO, log_string()."User is allowed to $cmd ad id:" . $this->list_id);

		/* Check for new stores */
		if(!(in_array($cmd, array('renew', 'edit')) &&
			$this->ad->company_ad &&
			!$this->stores &&
			$this->ad->email) && $cmd == 'copy') {
			/* Clear ad id data to make a new ad. */
			$this->action = null;
			$this->ad_id = null;
			$this->list_id = null;
			$this->ad->passwd = null;
			$this->already_paid_extra_images = null;
			$this->ad->city = null;

			/* XXX: Change loadad to not send images without action instead? */
			$this->images = array();
		}

		$this->area_unset = empty($this->ad->area) ? 1 : 0;

		return 'form';
	}

	/* User is trying to bump the ad. */
	function newad_do_apply_product() {
		global $session;
		global $BCONF;
		$transaction = new bTransaction();
		if (Cpanel::checkLogin()) {
			if (Cpanel::hasPriv('adminad.bump')) {
				$transaction->auth_type = 'admin';
			}
			else {
				return 'load';
			}
		}

		$command = 'bump_ad';
		$transaction->add_client_info();
		$transaction->add_data('ad_id', $this->ad_id);
		if ($transaction->auth_type)
			$reply = $transaction->send_admin_command($command);
		else
			$reply = $transaction->send_command($command);

		$this->set_errors($transaction->get_errors());
		return 'bumped';
	}

	/* Ad has been bumped. */
	function newad_bumped() {
		$data['page_name'] = lang('PAGE_ACTION_BUMP_NAME');
		$data['page_title'] = lang('PAGE_ACTION_BUMP_TITLE');

		$this->display_layout('ai/bumped.html', $data, null, null, 'html5_base');
		return 'FINISH';
	}

	function newad_activated() {
		$data['page_name'] = lang('PAGE_ACTION_BUMP_NAME');
		$data['page_title'] = lang('PAGE_ACTION_BUMP_TITLE');

		$this->display_layout('ai/bumped.html', $data, null, null, 'html5_base');
		return 'FINISH';
	}

	function newad_confirm_delete () {
		global $script_list_deletion;
		global $styles_list_deletion;

		$data = array('type' => $this->ad->type, 'category' => $this->ad->category);

		$data['page_name'] = lang('PAGE_CONFIRM_DELETE_NAME');
		$data['page_title'] = lang('PAGE_CONFIRM_DELETE_TITLE');
		$data['raw_list_id'] = $this->raw_list_id;
		if (isset($this->ht) && ht_to_source($this->ht))
			$data['source'] = ht_to_source($this->ht);

		$data['headscripts'] = $script_list_deletion;
		$data['headstyles'] = $styles_list_deletion;
		$this->display_layout('ai/delete_reasons.html', $data, null, null, 'html5_base');
	}


	/* User is trying to delete the ad. */
	function newad_delete($passwd = null, $deletion_reason, $user_text, $user_text_testimonial = null, $deletion_timer = null) {

		global $account_session;
		$is_success = false;
		bLogger::logInfo(__METHOD__, "passwd : {$passwd}, reason : {$deletion_reason}, testimonial : {$user_text_testimonial}, time : {$deletion_timer}");

		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
			bLogger::logInfo(__METHOD__, "Call delete ad from CP");
			$is_success = $this->delete_ad(true, $passwd,$deletion_reason, $user_text, $user_text_testimonial, $deletion_timer);
		} else {
			$allow_delete = false;
			if($account_session->is_logged() && $this->ad->email == $account_session->get_param('email') ){
				bLogger::logInfo(__METHOD__, "Check if the user is PRO with pack status active");
				$data_ipf = $account_session->get_param('is_pro_for');
				$data_pks = $account_session->get_param('pack_status');
				$data_cat = $this->ad->category;

				$allow_delete = allow_delete_ad($data_ipf, $data_pks, $data_cat, $this->ad->inserting_fee);
				bLogger::logInfo(__METHOD__, "INFO: is_pro_for: $data_ipf");
				bLogger::logInfo(__METHOD__, "INFO: pack_status: $data_pks");
				bLogger::logInfo(__METHOD__, "INFO: category: $data_cat");
				bLogger::logInfo(__METHOD__, "INFO: acc_is_pro:". (int)$allow_delete);
			}

			if($allow_delete){
				bLogger::logInfo(__METHOD__, "Calling DELETE ad from PRO user");
				$is_success = $this->delete_ad(false, $passwd,$deletion_reason, $user_text, $user_text_testimonial, $deletion_timer);
			}
			else {
				bLogger::logInfo(__METHOD__, "Calling DEACTIVATE ad from NORMAL & PRO user");
				$is_success = $this->deactivate_ad(false, $passwd, $deletion_reason, $user_text, $user_text_testimonial, $deletion_timer);
			}
		}

		if (!$is_success) {
			$res = $this->f_newad_id($this->list_id);
			if (is_array($res) && !empty($res['action']) && $res['action'] == 'fromcmd') {
				return 'confirm_delete';
			} else {
				return 'load';
			}
		}

		return 'deleted';
	}

	function delete_ad($is_admin = false, $passwd = null, $deletion_reason, $user_text, $user_text_testimonial = null, $deletion_timer = null){

		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('id', $this->list_id);

		if (isset($this->ht) && ht_to_source($this->ht)) {
			$transaction->add_data('source', ht_to_source($this->ht));
		}
		if (!empty($deletion_reason)) {
			$this->del_reason = $deletion_reason;
			$transaction->add_data('deletion_reason', $deletion_reason);
			if (!empty($deletion_timer)) {
				$this->del_timer = $deletion_timer;
				$transaction->add_data('deletion_timer', $deletion_timer);
			}

			if (isset($user_text) && !is_array($user_text)) {
				$this->user_text = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', trim($user_text));
				if (!empty($this->user_text)) {
					$transaction->add_data('user_text', $this->user_text);
				}
			} else if (isset($user_text[$deletion_reason])) {
				$user_text[$deletion_reason] = trim($user_text[$deletion_reason]);
				$this->user_text=$user_text[$deletion_reason];
				if (!empty($user_text[$deletion_reason])) {
					$transaction->add_data('user_text', $user_text[$deletion_reason]);
				}
			}
		}

		$this->user_text_testimonial=$user_text_testimonial;
		if (!empty($user_text_testimonial)) {
			$transaction->add_data('site_area', 'delete_ad_form');
			$transaction->add_data('testimonial', $user_text_testimonial);
		}

		if($is_admin){
			$transaction->auth_type = 'admin';
			$transaction->add_data('reason', 'admin_deleted');
			$reply = $transaction->send_admin_command('deletead', true, false);
		} else {
			$transaction->add_data('reason', 'user_deleted');
			if ($passwd) {
				$transaction->add_data('passwd', $passwd);
			}
			$reply = $transaction->send_command('deletead');
		}

		$this->set_errors($transaction->get_errors());
		return !$transaction->has_error();
	}

	function deactivate_ad($is_admin = false, $passwd = null, $deletion_reason, $user_text, $user_text_testimonial = null, $deletion_timer = null){
		global $account_session;
		bLogger::logInfo(__METHOD__, "passwd : {$passwd}, reason : {$deletion_reason}, testimonial : {$user_text_testimonial}, time : {$deletion_timer}");
		bLogger::logInfo(__METHOD__, "is_pro_for : {".$account_session->get_param('is_pro_for')."}, email : {".$account_session->get_param('email')."}");

		// Fill form data when logged into an account-

		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('list_id', $this->raw_list_id);
		$transaction->add_data('new_status', 'deactivated');

		if (!empty($deletion_reason)) {
			$this->del_reason = $deletion_reason;
		}

		if (isset($this->ht) && ht_to_source($this->ht)) {
			$transaction->add_data('source', ht_to_source($this->ht));
		}

		if (!empty($deletion_reason)) {
			$this->del_reason = $deletion_reason;
			$transaction->add_data('deletion_reason', $deletion_reason);
			if (!empty($deletion_timer)) {
				$this->del_timer = $deletion_timer;
				$transaction->add_data('deletion_timer', $deletion_timer);
			}

			if (isset($user_text) && !is_array($user_text)) {
				$this->user_text = preg_replace('/[\x00-\x1F\x80-\xFF]/', ' ', trim($user_text));
				if (!empty($this->user_text)) {
					$transaction->add_data('user_text', $this->user_text);
				}
			} else if (isset($user_text[$deletion_reason])) {
				$user_text[$deletion_reason] = trim($user_text[$deletion_reason]);
				$this->user_text=$user_text[$deletion_reason];
				if (!empty($user_text[$deletion_reason])) {
					$transaction->add_data('user_text', $user_text[$deletion_reason]);
				}
			}
		}

		$this->user_text_testimonial=$user_text_testimonial;
		if (!empty($user_text_testimonial)) {
			$transaction->add_data('site_area', 'delete_ad_form');
			$transaction->add_data('testimonial', $user_text_testimonial);
		}

		if ($is_admin) {
			$transaction->auth_type = 'admin';
			$transaction->add_data('reason', 'admin_deleted');
			$transaction->send_admin_command('change_ad_status', true, false);
		} else {
			$transaction->add_data('reason', 'user_deleted');
			if (preg_match('/^[0-9]{7,}\.[0-9]+$/', $this->list_id)) {
				$passwd = $this->get_ad_password($this->list_id);
			}
			$transaction->add_data('salted_passwd', $passwd);
			$transaction->send_command('change_ad_status');
		}

		$this->set_errors($transaction->get_errors());
		return !$transaction->has_error();
	}

	function activate_ad($is_admin = false, $passwd = null){
		global $account_session;
		bLogger::logInfo(__METHOD__, "passwd : {$passwd} ");
		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('list_id', $this->list_id);
		$transaction->add_data('new_status', 'active');
		if (isset($this->ht) && ht_to_source($this->ht)) {
			$transaction->add_data('source', ht_to_source($this->ht));
		}

		if ($is_admin) {
			$transaction->send_admin_command('change_ad_status', true, false);
		} else {
			if ($passwd) {
				$transaction->add_data('salted_passwd', $passwd);
			}
			$transaction->send_command('change_ad_status');
		}
		return !$transaction->has_error();
	}

	/* Ad deleted, now the user inserts testimonial. */
	function newad_testimonial($user_text_testimonial = null) {
		global $script_list_deletion_success;
		global $styles_list_deletion;
		global $styles_list_deleted;

		$data = array('type' => $this->ad->type);
		$data['user_text_testimonial'] = $user_text_testimonial;

		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $this->ad_id);
		if (isset($user_text_testimonial)) {
			$transaction->add_data('testimonial', $user_text_testimonial);
		}
		$reply = $transaction->send_command('user_testimonial');
		$this->set_errors($transaction->get_errors());
		if ($transaction->has_error()) {
			$data['status'] = 'ERROR';
		} else {
			$data['status'] = 'OK';
		}
		$data['page_name'] = lang('PAGE_ACTION_NAME_DELETED');
		$data['page_title'] = lang('PAGE_ACTION_TITLE_DELETED');

		$data['headscripts'] = $script_list_deletion_success;
		$data['headstyles'] = $styles_list_deletion;
		$data['inserting_fee'] = $this->ad->inserting_fee;

		$this->headstyles = $styles_list_deletion;
		$this->display_layout('ai/delete_reasons_success.html', $data, null, null, 'html5_base');
	}

	/* Ad has been deleted. */
	function newad_deleted() {
		global $script_list_deletion_success;
		global $styles_list_deletion;
		global $styles_list_deleted;

		$data = array('type' => $this->ad->type, 'cg' => $this->ad->category, 'region' => $this->ad->region);

		$data['page_name'] = lang('PAGE_ACTION_NAME_DELETED');
		$data['page_title'] = lang('PAGE_ACTION_TITLE_DELETED');
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$data['is_admin'] = 1;

		$data['headscripts'] = $script_list_deletion_success;
		$data['headstyles'] = $styles_list_deletion;
		$data['email'] = $this->ad->email;
		$data['inserting_fee'] = $this->ad->inserting_fee;

		$main_cat_id = bconf_get($BCONF, "*.cat.{$this->ad->category}.parent");
		$main_cat_name = utf8_encode(bconf_get($BCONF, "*.cat.{$main_cat_id}.name"));

		$this->headstyles = $styles_list_deletion;
		$this->display_layout('ai/delete_reasons_success.html', $data, null, null, 'html5_base');
	}

	/* User wants to access his store */
	function newad_store($store_username, $store_passwd) {
		/* Do transaction and verify username/passwd */
		syslog(LOG_INFO, log_string()."newad_store");

		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('log_string', log_string());
		$transaction->add_data('store_username', $store_username);
		$transaction->add_data('store_passwd', $store_passwd);

		$reply = $transaction->send_command('store_login');

		$this->set_errors($transaction->get_errors());
		if ($transaction->has_error()) {
			/* Fail. Goto action page */
			syslog(LOG_INFO, log_string()."user failed to login to store $store_username");
			return 'action';
		}

		/* Success. Redirect to store app */
	}

	function newad_blocked() {
		if ($this->loadaction === false) {
			return 'load';
		}

		bLogger::logInfo(__METHOD__, "SIMILAR: entering newad_blocked (list_id:$this->list_id, action_id:$this->action_id)");
		$data = $this->get_ad_data();

		$this->headstyles = bconf_get($BCONF,"*.common.editad.blocked.headstyles");
		$this->display_layout('ai/edition_expired.html', $data, null, null, 'html5_base');
	}

	/* An ad has been loaded, or a new ad is to be entered. Display the form for editing. */
	function newad_form($mv) {
		global $script_list;
		global $ignore_fields;
		global $BCONF;

		if ($this->loadaction === false) {
			return 'load';
		}

		$data = $this->get_action_data();
		$data += $this->get_ad_data();
		$data += $this->get_maps_data();
		$data += $this->get_store_data();
		$data += $this->get_category_data();
		$data['mv'] = $mv;

		if (!Cpanel::checkLogin() || !(Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
			if (array_key_exists('category', $data) && !$this->publish_similar) {
				$edit_block = bconf_get($BCONF,"*.edit_allowed_days.cat.".$data['category']);
				if ($edit_block != null && array_key_exists('can_edit_ad', $data) && $data['can_edit_ad'] === 'f') {
					return 'blocked';
				}
			}
		}

		$data['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
		if (in_array($this->action, array('edit', 'renew')) && $this->ad->price > 0) {

			/* Red arrow price tip */
			if (!isset($this->red_arrow_price)) {
				$red_arrow_price = get_red_arrow_price($this->ad->price);
				if ($red_arrow_price > 0)
					$this->red_arrow_price = "$red_arrow_price";
			}

			/* Check if red arrow is allowed */
			$red_arrow_time = (int)bconf_get($BCONF, "*.common.list.red_arrow_allowed_age");
			if ($red_arrow_time == "")
				$red_arrow_time = 0;
			if ($this->orig_list_time && (time() - $this->orig_list_time) >= $red_arrow_time)
				$data['red_arrow_price_allowed'] = 1;
		}

		$data['headscripts'] = $script_list;

		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
			$data['is_admin'] = 1;
			$data['administrator'] = $_SESSION['controlpanel']['admin']['username'];
		}

		$data += generate_weeks($response);
		$this->offseason = create_avail_weeks(@$this->ad->available_weeks_offseason, null, $response);
		$this->peakseason = create_avail_weeks(@$this->ad->available_weeks_peakseason, null, $response);
		$this->available_weeks = create_avail_weeks(@$this->ad->available_weeks, null, $response);
		if (is_array($this->offseason))
			list($data['offseason_checked_descr'], $data['offseason_checked']) = $this->offseason;
		if (is_array($this->peakseason))
			list($data['peakseason_checked_descr'], $data['peakseason_checked']) = $this->peakseason;
		if (is_array($this->available_weeks))
			list($data['available_weeks_checked_descr'], $data['available_weeks_checked']) = $this->available_weeks;

		/* If infopage is set as feature - keep infopage, US1106 - links to real estate agents */

		if ($this->infopage_allowed()) {
			$data['keep_infopage'] = 1;
		} else if ($this->ad) {
			$data['infopage'] = null;
			$data['infopage_title'] = null;
			$this->ad->infopage = null;
			$this->ad->infopage_title = null;
		}
		global $session;

		$store = new blocket_store(false);

		if ($store->check_logged_in() && @$data['is_admin'] != 1 && !in_array($this->action, array('renew', 'edit', 'refused', 'prolong'))) {
			/* Pre fill data on form */
			if (!$session->load_active_instance($store, get_class($store), "0") || !isset($store->c_name) || empty($store->c_name))
				$store->lookup_store($_SESSION['global']['store_id']);

			if (!isset($this->errors) && !isset($this->warnings)) {
				$this->name = $store->c_name;
				$this->email = $_SESSION['global']['store_username'];
				$this->phone = $store->c_phone;
				if (!$this->ad) {
					$data['region'] = $store->c_region;
					$data['city'] = $store->c_city;
				}
				$data['zipcode'] = $store->c_zip;
				$data['store'] = $store->c_id;
			}
			$data['store_name'] = $store->c_name;
			$this->company_ad = 1;
			$this->store_token = true;

			$data['logged_in_store'] = $_SESSION['global']['store_id'];
			$data['ignore_company_ad'] = 1;
			$data['ignore_email'] = 1;
			$data['ignore_store'] = 1;
			$data['has_store'] = '1';
			$this->ignore_fields = array('company_ad', 'email', 'store');
		} elseif (@$this->pay_type == 'campaign') {
			$this->ignore_fields = array('company_ad', 'email', 'store');
			$data['ignore_company_ad'] = 1;
			$data['ignore_email'] = 1;
			$data['ignore_store'] = 1;
			if ($store->check_logged_in() && @$data['is_admin'] != 1)
				$this->store_token = true;
		} else {
			$this->ignore_fields = NULL;
			if ($this->stores)
				$data['has_store'] = '1';
			if(in_array($this->action, array('renew', 'edit', 'refused', 'prolong'))){
				$data['ignore_email'] = 1;
			}
		}
		/* Check if there's only one area in selected region / city. */
		if (isset($data['region']))
			$munics = bconf_get($BCONF, "*.common.region.{$data['region']}.municipality");
		if (isset($munics) && count($munics) == 1) {
			$munic = array_pop($munics);
			if (!isset($munic['area']))
				$data['area_fixed'] = $munic['name'];
			elseif (count($munic['area']) == 1) {
				$area = array_pop($munic['area']);
				$data['area_fixed'] = $area['name'];
			}
		}

		if(strstr($_SERVER['HTTP_USER_AGENT'],"Chrome")||strstr($_SERVER['HTTP_USER_AGENT'],"Safari")) {
			$data['webkit_browser'] = 1;
		}

		global $account_session;

		/* The (int) cast below is 'because reasons'. Template receives NULL otherwise */
		$adHasProducts = $this->has_gallery + $this->has_label + $this->has_weekly_bump + $this->has_daily_bump;
		$packUpsellingEnabled = (int) bconf_get($BCONF,"*.upselling.desktop.ai.for_pro.enabled");
		$packSlotsAvailables =  (!is_null($account_session->get_param('pack_slots')) ? $account_session->get_param('pack_slots') : 0);

		$this->show_upselling_in_form = (int) Upselling::showUpselling(
			$BCONF,
			$account_session->is_logged(),             /* userLoggedIn */
			$account_session->get_param('is_pro_for'), /* userProCategories */
			$adHasProducts,                            /* adHasProducts */
			$packSlotsAvailables,                      /* userHasSlots */
			$packUpsellingEnabled                      /* packUpsellingEnabled */
		);

		// Fill form data when logged into an account
		if ($account_session->is_logged() && !Cpanel::checkLogin()) {
			$data['account_logged_in'] = true;
			$data['email'] = $account_session->get_param('email');
			$data['name'] = (!empty($this->ad->name)) ? $this->ad->name : $account_session->get_param('name');
			$data['phone'] = (!empty($this->ad->phone)) ? $this->ad->phone : $account_session->get_param('phone');
			$data['company_ad'] = ($account_session->get_param('is_company') == 't'?"1":"0");
			$this->complete_phone_params($data);
			$data['rut'] = $account_session->get_param('rut');
			$data['business_name'] = $account_session->get_param('business_name');
			$is_pro_for = $account_session->get_param('is_pro_for');
			if( !empty($is_pro_for) ){
				$data['is_pro_for'] = $is_pro_for;
			}
			if (empty($this->action) || !in_array($this->action, array('edit', 'refused', 'renew'))){
				if (!empty($this->back) || count($this->errors) == 0) {
					$data['region'] = (array_key_exists('region', $data) && strlen($data['region']) > 0) ? $data['region']: $account_session->get_param('region');
					$data['communes'] = (array_key_exists('communes', $data) && strlen($data['communes']) > 0) ? $data['communes']: $account_session->get_param('commune');
				}
			}
			$this->rut = $data['rut'];
			$this->business_name = $data['business_name'];
			$data['hide_password_field']=1;
			$data['ignore_email_account']=1;

			// Check if the ad was created using an account, and set a flag for the template
			/* TODO there must be a better way than to check the list time, to know if the ad was
			 *      created using an account or not
			 */
			if($this->ad && strtotime($this->ad->list_time) > strtotime($account_session->get_param('creation_date'))) {
				syslog(LOG_DEBUG, log_string()."running command on ad ".$this->list_id." created by the account:".$account_session->get_param('email'));
				$data['ad_created_from_account'] = 1;
			}
		} else {
			// Added so "create account" is by default checked
			if (!empty($this->ai_create_account) && $this->ai_create_account == "on"){
				$data['ai_create_account'] = $this->ai_create_account;
			}
		}
		if ($this->refused_reason) {
			$dyn_bconf = get_dynamic_bconf();
			$refused_reasons = bconf_get($dyn_bconf, "*.*.common.refusal");
			$this->display_layout('ai/form_general.html', $data, array('refusal' => $refused_reasons), null, 'html5_base');
		} else {
			syslog(LOG_WARNING, log_string()."form general");
			$this->display_layout('ai/form_general.html', $data, null, null, 'html5_base');
		}
	} /* function newad_form() */

	function newad_check_duplicates($back, $mv) {
		if ($back){
			$this->back = $back;
			return 'form';
		}
		if ($this->is_duplicated()) {
			return 'is_duplicate';
		} else {
			$goto = $this->newad_create(NULL,NULL,NULL,1);
			if ($this->want_bump == '1') {
				return 'want_bump';
			}
			return $goto;
		}
	}

	function newad_has_duplicates($mv) {
		/* TODO: the following lines repeat in the beginning of at least 2 more states. Refactor? */
		global $script_list;
		$data = $this->get_action_data();
		$data += $this->get_ad_data();
		$data += $this->get_maps_data();
		$data += $this->get_store_data();
		$data['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];

		$data['page_title'] = lang('PAGE_NEWAD_DUPLICATED');

		$data['headscripts'] = $script_list;
		$this->duplicated_detected = 1;
		$this->display_layout('ai/duplicated.html', $data, null, null, 'html5_base');
	}

	/* User has clicked the verify ad button. */
	function newad_verify($passwd_ver, $email_confirm, $check_type_diff, $extra_images, $accept_conditions = true) {
		global $ignore_fields;
		global $BCONF;
		bLogger::logDebug(__METHOD__, "Validate salted_passwd value is {$this->salted_passwd}");
		$postfilter = array(
			'address'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'email'		=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'rut'		=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'business_name'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'add_location'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'communes'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'address_number'=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'geoposition'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'add_location'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW),
			'source'	=>	array('filter' => FILTER_VALIDATE_REGEXP, 'flags' => !FILTER_FLAG_STRIP_LOW, 'options' => array("regexp"=>"/^(web|msite)/")),
			'phone_type'	=>	array('filter' => FILTER_VALIDATE_REGEXP, 'flags' => !FILTER_FLAG_STRIP_LOW, 'options' => array("regexp"=>"/^(f|m)/")),
			'area_code'	=>	array('filter' => FILTER_SANITIZE_SPECIAL_CHARS, 'flags' => !FILTER_FLAG_STRIP_LOW)
		);
		$revised_post_array = filter_var_array($_POST, $postfilter);

		$_POST['email'] = $revised_post_array['email'];

		if (isset($_POST["ai_accept_conditions"]))
			$this->ai_accept_conditions = $_POST["ai_accept_conditions"];
		else if(!$accept_conditions)
			unset($this->ai_accept_conditions);
		if (isset($_POST["ai_create_account"]))
			$this->ai_create_account = $_POST["ai_create_account"];
		else
			unset($this->ai_create_account);

		if(!empty($_POST["rut"]))
			$this->rut = $this->f_rut($revised_post_array['rut']);
		else
			unset($this->rut);

		if (!empty($_POST["business_name"])) {
			$this->business_name = $revised_post_array['business_name'];
		} else {
			unset($this->business_name);
		}

		if(!empty($_POST['add_location']))
			$this->add_location = $revised_post_array['add_location'];
		else unset($this->add_location);

		if(!empty($_POST['communes']))
			$this->communes = $revised_post_array['communes'];
		else unset($this->communes);

		if(!empty($_POST['address']))
			$this->address = $revised_post_array['address'];
		else unset($this->address);

		if(!empty($_POST['address_number']))
			$this->address_number = $revised_post_array['address_number'];
		else unset($this->address_number);

		if(!empty($_POST['geoposition']))
			$this->geoposition = $revised_post_array['geoposition'];
		else unset($this->geoposition);

		if(!empty($_POST['geoposition_is_precise']))
			$this->geoposition_is_precise = $_POST['geoposition_is_precise'];
		else unset($this->geoposition_is_precise);

		if(!empty($_POST['add_location'])){
			$this->add_location = $revised_post_array['add_location'];
		}else{
			unset($this->add_location);
		}
		if (!empty($_POST['source'])) {
			$this->source = $revised_post_array['source'];
		}else{
			unset($this->source);
		}
		if (!empty($_POST['phone_type'])) {
			$this->phone_type = $revised_post_array['phone_type'];
		}else{
			unset($this->phone_type);
		}
		if (!empty($_POST['area_code'])) {
			$this->area_code = $revised_post_array['area_code'];
		}else{
			unset($this->area_code);
		}
		if (!empty($_POST['currency'])) {
			$this->currency = $_POST['currency'];
		} else {
			$this->currency = 'peso';
		}

		$errors = array();
		$warnings = array();
		$messages = array();

		/* ad->body needs to be removed if we have ad->item_desc */
		if (!empty($this->ad->item_desc) && $this->ad->body)
			$this->ad->body = null;

		if (isset($this->ad->price) && isset($this->ad->item_price))
			$this->ad->price = null; /* Remove this so that price doesn't show up on old ads that have been edited. */

		unset($this->focus_extra_images);
		unset($this->voucher);

		$this->show_password = empty($this->action) && !$this->publish_similar;

		if ($this->ad != NULL)
			$cat_parent = bconf_get($BCONF, "*.cat.".$this->ad->category . ".parent");

		/* Do not remove fields for administrators */
		if (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad'))) {
			/* Clean from fields that are ignored for chosen ad type */
			if (isset($ignore_fields[$this->action])) {
				foreach($ignore_fields[$this->action] as $field) {
					if ($field != 'area' || $this->area_unset != 1 ) {
						if(!($field == 'type' && $cat_parent == "1000")) {
							unset($_POST[$field]);
						}
					}
				}
			}
			if (isset($this->ignore_fields)) {
				foreach($this->ignore_fields as $field) {
					unset($_POST[$field]);
				}
			}
		}

		if ($this->ad) {
			$old_category = $this->ad->category;
		}
		else {
			$this->ad = new bAd();
			$old_category = null;
		}

		/* XXX Don't use $_POST. */
		$this->ad->populate($_POST);
		$data = $this->get_action_data();
		$data += $this->get_ad_data();
		$data += $this->get_maps_data();

		global $account_session;
		if(!empty($this->ad->email)
			&& ( ($account_session->is_logged() && $this->ad->email == $account_session->get_param('email') )
				|| $account_session->login($this->ad->email, $passwd_ver)
			)
		) {
			$this->ad->passwd = $account_session->get_param('password');
			if (is_null($this->ad->company_ad))
				$this->ad->company_ad = ($account_session->get_param('is_company') == 't'?"1":"0");

			if ($this->ad->company_ad == "1") {
				$this->rut = $account_session->get_param('rut');
				$this->business_name = $account_session->get_param('business_name');
			}
			$this->add_account_session_data($data);
			syslog(LOG_INFO, log_string()."User logged in. Inserted account password into ad password");
		}
		else if ($this->ad->store || (!$this->ad->passwd && Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))) {
			$this->ad->passwd = null;
			syslog(LOG_DEBUG, log_string()."Password1");
		} elseif ($this->show_password || (isset($this->ad->passwd) && $this->ad->passwd != "")) {
			syslog(LOG_INFO, log_string()."Password2:");

			/* Need to check length here since trans will not know it. */
			syslog(LOG_INFO, log_string()."VERIFYING PASSWD: " . print_r($this, true));
			$check_passwd = password_verify($this->ad->passwd, $passwd_ver);
			if ($check_passwd !== True) {
				$errors['passwd'] = $check_passwd;
				$this->ad->passwd = null;
				syslog(LOG_DEBUG, log_string()."Password3:");
			}
			else if ($account_session->get_error() !='ACCOUNT_NOT_FOUND' && $account_session->get_error() != 'ACCOUNT_NOT_ACTIVE') {
				if ($account_session->get_error() !='ERROR_EMAIL_MISSING') {
					if ($account_session->is_blocked($this->ad->email)) {
						$errors['passwd'] = 'ACC_ERROR_ACCOUNT_BLOCKED';
					} else {
						$errors['passwd'] = 'ERROR_ACCOUNT_LOGIN_FAILED';
					}
					$this->ad->passwd = null;
					syslog(LOG_DEBUG, log_string()."Password5:");
					syslog(LOG_DEBUG, log_string()."login failed!");
					syslog(LOG_DEBUG, log_string()."Error: ".$account_session->get_error());
				}
			}
			else{
				$this->ad->passwd = hash_password($this->ad->passwd);
				syslog(LOG_DEBUG, log_string()."Password6: ". $this->ad->passwd);
			}
		}

		if (!$accept_conditions && !in_array($this->action, array("edit", "renew"))) {
			$errors['accept_conditions']  = 'ERROR_ACCEPT_CONDITIONS';
		}

		if ($this->ad->email != $email_confirm && $this->action != "edit" && $this->action != "renew" && $this->action != "refused") {
			$errors['email_confirm'] = 'ERROR_EMAIL_MISMATCH';
		}

		$this->verify_company_ad_by_cat();

		/*
		CHUS 288: if brand is set to 'other', set model and version to 'other'. If model is 'other', version should be 'other' too.
		*/
		if (isset($_POST['brand']) && $_POST['brand'] === "0" && $this->ad != NULL) {
			$this->ad->model = "0";
		}
		if (isset($_POST['model']) && $_POST['model'] === "0" && $this->ad != NULL) {
			$this->ad->version = "0";
		}

		$store = new blocket_store(false);

		if ($store->check_logged_in() && !in_array($this->action, array('renew', 'edit', 'refused', 'prolong'))) {
			if (@$data['is_admin'] != 1) {
				/* Override some data */
				$_POST['email'] = $_SESSION['global']['store_username'];
				$_POST['store'] = $_SESSION['global']['store_id'];
				$_POST['company_ad'] = 1;
			}
		}
		/* XXX Don't use $_POST. */
		//$this->ad->populate($_POST);
		$this->ad->phone = $this->clear_formated_number($this->ad->phone);

		/* remove the thousand separator dots  */
		$this->ad->price = str_replace(".", "", $this->ad->price);

		/* FT329: remove leading zeros from price, the for loop don't execute any action, but count the zeros */
		for($i = 0; $i < strlen($this->ad->price) && $this->ad->price[$i] == "0"; $i++);
		if($i < strlen($this->ad->price) && $this->ad->price[$i] == "," && ($i > 0)) {
			$i -= 1;
		}
		$this->ad->price = substr($this->ad->price, $i);

		/* remove quotes from name */
		$this->ad->name = str_replace("\"", "", $this->ad->name);

		if (strlen($this->ad->body) < bconf_get($BCONF, "*.newad.preview.show_warn.body.length"))
			$this->show_body_warn = 1;
		else if (isset($this->show_body_warn))
			unset($this->show_body_warn);

		if (!$this->ad->store)
			$this->ad->hide_address = null;

		/* Infopage check */
		$this->get_category_data();

		/* Empty infopage for renewed/deleted if ad is not connected to a store. */
		/* Also empty for new (user going back and editing the ad) */
		/* Also empty for editrefused where previous action was not edit */
		/* Added: if infopage is set as feature - keep infopage. US1106 - links to real estate agents */
		if (isset($this->ad->infopage) && !$this->infopage_allowed()) {
			syslog(LOG_INFO, log_string() . "Removed infopage.");
			unset($this->ad->infopage);
			unset($this->ad->infopage_title);
		}

		/* Logging email adress */
		if (isset($this->ad->email)) {
			syslog(LOG_INFO, log_string()."User email: {$this->ad->email}");
		}

		/* Log javascript support */
		syslog(LOG_INFO, log_string()."Javascript : ".(($check_type_diff == 1)?"OFF":"ON"));

		/* User uploaded image */
		$this->images = array();
		$this->thumbnail_digest = array();
		$this->digest_present = array();
		if (isset($_POST['image']) && is_array($_POST['image'])) {
			$post_images = $_POST['image'];
			for($i=0; $i<sizeof($post_images); $i++) {
				$curr_image = $post_images[$i];

				//check image duplicates
				if(in_array($curr_image, $this->images)) {
					$image_array_str = "(";
					for($j=0; $j<sizeof($post_images); $j++) {
						if($j!=0) $image_array_str.=",";
							$image_array_str.=$post_images[$j];
					}
					$image_array_str .= ")";
					syslog(LOG_WARNING, log_string()."WARNING - Found duplicated image \"$curr_image\" on posted images $image_array_str. Ignoring...");
					continue;
				}

				$this->images[] = $curr_image;
				$this->thumbnail_digest[] = $_POST['thumbnail_digest'][$i];
				$this->digest_present[] = $_POST['digest_present'][$i];
			}
			if (isset($this->show_image_warn))
				unset($this->show_image_warn);
		} else {
			$this->show_image_warn = 1;
		}

		/* FT 445: if changing category to 'jobs', 'looking for a job' or 'services', change type to 'buy' to avoid 'type invalid' error */
		if ((in_array($this->action ,array('edit','refused'))) && isset($this->ad->category) && in_array($this->ad->category, array('7020', '7040', '7060')) && $this->ad->type != 's') {
			$this->ad->type = 's';
		}
		/* End FT 445 */

		// If the category only allow one type of ad and the actual ad doesn't belongs to this ad_type
		// This default type is set
		if(!empty($this->ad->type) && !in_array($this->ad->type,$this->type_list) && count($this->type_list) == 1) {
			$this->ad->type = $this->type_list[0];
		}

		/* Post size too big */
		if (isset($GLOBALS['php_errormsg']) && preg_match('/bytes exceed/', $GLOBALS['php_errormsg']))
			$errors['error'] = 'ERROR_FILE_TOO_BIG';

		/* Send ad for validation, don't commit */
		$transaction = new bTransaction();
		$transaction->add_data('log_string', log_string());

		syslog(LOG_DEBUG, log_string()."Password7: ". $this->ad->passwd);

		$transaction_data = $this->ad->get_data_array(BAD_CLEANUP | BAD_UNSET);

		if (array_key_exists('can_edit_ad', $transaction_data)) {
			unset($transaction_data['can_edit_ad']);
		}

		$this->phone_std_for_trans($transaction_data);

		syslog(LOG_INFO, log_string()."Sending data to transhandler for validation.");

		$transaction->populate($transaction_data, bconf_get($BCONF, "*.common.list_features.feat"), bconf_get($BCONF, "*.common.commalist_features.feat"));
		if ($this->action) {
			$transaction->add_data('ad_type', $this->action);
			if ($this->list_id && strpos($this->list_id, '.') == false)
				$transaction->add_data('list_id', $this->list_id);
			else
				$transaction->add_data('ad_id', $this->ad_id);
		}

		/* Admin stuff, send token for validation */
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$transaction->auth_type = 'admin';

		if ($check_type_diff && $this->check_type_list())
			$warnings['type'] = 'WARN_CATEGORY_TYPE_MISMATCH';

		/* Add job categories*/
		if (!empty($_POST["jc"])){
			$this->job_category = implode("|",array_keys($_POST["jc"]));
			$transaction->add_data('job_category',$this->job_category);
		}

		/* Add service type*/
		if (!empty($_POST["sct"])){
			$this->service_type = implode("|",array_keys($_POST["sct"]));
			$transaction->add_data('service_type',$this->service_type);
		}

		/* Add services real estate*/
		if (($this->ad->category == 1260) && in_array($this->ad->estate_type, array(1,2,3))){
			$sv = array();
			if(isset($_POST["srv"])){
				$sv = sanitizeVar($_POST["srv"], "array_int");
			}
			$this->services = implode("|", array_keys($sv));
			$transaction->add_data('services',$this->services);
		}

		/* Add footwear_type */
		if (!empty($_POST["footwear_type"])){
			$this->footwear_type = implode("|",array_keys($_POST["footwear_type"]));
			$transaction->add_data('footwear_type',$this->footwear_type);
		}

		/* Add ext_code */
		if (!empty($_POST["ext_code"]) &&
			$account_session->is_logged() &&
			$account_session->get_param('pack_type') == "real_estate" &&
			$account_session->get_param('pack_status') == "active" ) {
			$transaction->add_data('ext_code',$_POST["ext_code"]);
		}

		/* Add equipment real estate*/
		if (in_array($this->ad->category, array(1220,1240)) && in_array($this->ad->estate_type, array(1,2,3))){
			$equipmt = array();
			if(isset($_POST["eqp"])){
				$equipmt = sanitizeVar($_POST["eqp"], "array_string");
			}
			$this->equipment = implode("|", array_keys($equipmt));
			$transaction->add_data('equipment', $this->equipment);
		}

		/* communes */
		if (isset($this->communes)) {
			$transaction->add_data('communes', $this->communes);
		}

		if (isset($this->currency)) {
			$transaction->add_data('currency', $this->currency);
		}

		if (@$this->pay_type == 'campaign' && $this->campaign_ad_id) {
			$transaction->add_data('cleared_by', $this->campaign_ad_id);
			$transaction->add_data('campaign_id', $this->campaign);
		}

		if ($transaction->auth_type)
			$reply = $transaction->send_admin_command('newad', false);
		else
			$reply = $transaction->validate_command('newad');

		/* Handle new images, stores, verify */
		$this->handle_reply($reply);
		if ($this->has_uploaded_too_many_images()) {
			syslog(LOG_INFO, log_string()."User has uploaded too many images.");
			$errors['extra_image'] = 'ERROR_TOO_MANY_EXTRA_IMAGES';
		} elseif ($check_type_diff && $this->check_extra_images_count($old_category)) {
			syslog(LOG_INFO, log_string()."User doesn't have javascript, and we should stay in form page because category.max_extra_images changed.");
			$warnings['extra_image'] = '';
		}

		/* FIXME: to newad only with password fix TRANS_ERROR_NO_SUCH_PARAMETER on password */
		$errors_transaction = $transaction->get_errors();
		if ($errors_transaction && isset($errors_transaction['passwd']) && $errors_transaction['passwd'] == "TRANS_ERROR_NO_SUCH_PARAMETER" ){
			unset($errors_transaction['passwd']);
		}
		if( ($this->ad->passwd == null) && isset($this->salted_passwd) && $this->salted_passwd != "" ){
			$this->ad->passwd = $this->salted_passwd;
		}
		bLogger::logDebug(__METHOD__, "create_account validation password: {$this->ad->passwd}");
		if (!empty($this->ai_create_account) && $this->ai_create_account == "on"){
			$account_trans = new bTransaction();
			$account_trans->add_data('name', $this->ad->name);
			$account_trans->add_data('email', $this->ad->email);
			if (isset($this->rut)) {
				$account_trans->add_data('rut', $this->rut);
			}
			if (isset($this->business_name)) {
				$account_trans->add_data('business_name', $this->business_name);
			}
			$account_trans->add_data('is_company', $this->ad->company_ad);
			$account_trans->add_data('region', $this->ad->region);
			$account_trans->add_data('commune', $this->communes);
			$account_trans->add_data('password', $this->ad->passwd);
			$account_trans->add_data('accept_conditions', ($this->ai_accept_conditions) ? 1 : 0);
			$account_trans->add_data('source', $this->source);
			//Phone process
			$phone_array['phone_type'] = $this->ad->phone_type;
			$phone_array['phone'] = $this->ad->phone;
			$phone_array['area_code'] = $this->ad->area_code;
			$this->phone_std_for_trans($phone_array);
			$account_trans->add_data('phone', $phone_array['phone']);
			// Call to validate using create account transaction
			$account_reply = $account_trans->validate_command('create_account');
			if ($account_trans->has_error(true)) {
				syslog(LOG_INFO, log_string()."error validating create_account");
				if ($account_trans->get_reply('error') != 'ACCOUNT_ALREADY_EXISTS') {
					$account_errors= $account_trans->get_errors();
					foreach($account_errors as $k => $v) {
						if(isset($errors_transaction[$k])){
							unset($account_errors[$k]);
						}
					}
					$errors = $errors + $account_errors;
				}
			}
		}
		$this->set_errors($errors + $errors_transaction);

		$this->set_warnings($warnings + $transaction->get_warnings());
		$this->set_messages($messages + $transaction->get_messages());

		/* We have recieved price, category and type. We need to define a variable
		 * to show a warning in preview page. Existing warnings do not serve because
		 * we do not want to stay on the same form */
		if (isset($this->show_price_warn))
			unset($this->show_price_warn);
		if ($this->warn_price() && !isset($this->errors->price))
			$this->show_price_warn = 1;

		if (!empty($_POST["want_bump"])){
			$this->want_bump = $_POST["want_bump"];
		}

		if (!empty($_POST["upselling_product"])){
			$this->upselling_product = sanitizeVar($_POST["upselling_product"]);
		}
		if (!empty($_POST["upselling_product_label"])){
			$this->upselling_product_label = sanitizeVar($_POST["upselling_product_label"]);
		}
		if ($this->has_error() || $this->has_warning()) {
			syslog(LOG_INFO, log_string() . "1:" . print_r($this->warnings, true) . print_r($this->errors, true));
			return 'form';
		}
		if (!$this->action || in_array($this->action, array('new'))) {
			// Only for new ads check if inserting at the same time
			$multiInsertion = new MultiInsertChecker($BCONF);
			$multiInsertionParams = array("email" => $this->ad->email, "phone" => $this->ad->phone);

			// Call to verification
			$multiInsertionAllowed = $multiInsertion->allowInsertAttempt(
				$this->ad->category,
				$multiInsertionParams,
				is_pro_for($this->ad->category, $account_session->get_param('is_pro_for'))
			);
			// Not allowed
			if (!$multiInsertionAllowed) {
				syslog(LOG_INFO, log_string() . "ERROR MULTI INSERT:" . var_export($multiInsertionParams, true));
				$this->err_error = lang("ERROR_MANY_INSERTS");
				return 'form';
			}
		}

		// Here we check if the ad is a duplicated
		if (isset($_POST['create']) && empty($errors)) {
			if ($this->is_duplicated()) {
				return 'is_duplicate';
			}
		}

		$this->get_category_data();
		$this->offseason = create_avail_weeks(@$this->ad->available_weeks_offseason, null, $response);
		$this->peakseason = create_avail_weeks(@$this->ad->available_weeks_peakseason, null, $response);
		$this->available_weeks = create_avail_weeks(@$this->ad->available_weeks, null, $response);
		if (is_array($this->offseason))
			list($this->offseason_checked_descr, ) = $this->offseason;
		if (is_array($this->peakseason))
			list($this->peakseason_checked_descr, ) = $this->peakseason;
		if (is_array($this->available_weeks))
			list($this->available_weeks_checked_descr, ) = $this->available_weeks;

		setcookie('email', md5($this->ad->email), time() + 60*60*24*365, '/');

		if(isset($_POST['create'])) {
			$goto = $this->newad_create(NULL, NULL, NULL, 1);
			if ($this->want_bump == '1') {
				return 'want_bump';
			}
			// an user buy a upselling combo, so it should go to payment
			if (!empty($this->upselling_product)) {
				// conditions to avoid upselling payment should be here
				if ($account_session->is_logged() || $this->pack_result != 1) {
					return 'pay_upselling';
				}
			}
			return $goto;
		}
		return 'preview';
	}

	function newad_upload_image() {
		/* called via ajax on /ai/form/X */
		$image = $this->upload_image();
		if (is_array($image)) {
			$transaction = new bTransaction();
			$transaction->add_data('log_string', log_string());
			$transaction->add_file('image', $image[0]);
			//$transaction->add_file('thumbnail', $image[1]);
			$reply = $transaction->send_command('imgput');
			if ($reply['status'] == 'TRANS_OK' && isset($reply['file'])) {
				syslog(LOG_INFO, log_string()."Adding image {$reply['file']} to form via ajax");
				// seems that json_encode doesn't like associative arrays?!
				$ret['status'] = 'ok';
				$ret['file'] = $reply['file']; // to put in images[]
				$ret['thumbnail_digest'] = $image[2];
				$ret['digest_present'] = $image[3];
			} else
				$ret['trans'] = $transaction->get_errors();
		} else if (is_string($image)) {
			$ret['status'] = $image;
		} else {
			$ret['status'] = 'error';
		}
		die(yapo_json_encode($ret));
	}

	/**
	 * Returns the UF conversion factor
	 */
	function getUFConversionFactor() {
		$dynBConf = get_dynamic_bconf();
		$conf = bconf_get($dynBConf, "*.*.common");
		return $conf['uf_conversion_factor'];
	}

	/* Preview ad, with password */
	function newad_preview($cl, $sp) {
		global $script_list;
		global $BCONF;
		if (!is_null($sp)) {
			if (!empty($this->action))
				$this->show_password = $sp;
			else
				$this->show_password = 1;
		}

		$data = $this->get_action_data();
		$data += $this->get_ad_data();
		$data += $this->get_maps_data();
		$data += $this->get_store_data();
		$data['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];

		if (!empty($this->currency) && !empty($data['price'])) {
			$conversionFactor = $this->getUFConversionFactor();
			if ($this->currency === 'uf') {
				$data['price_uf'] = str_replace(',', '.', $data['price']) * 100;
				$data['price'] = convertUFToPesos($conversionFactor, $data['price']);
			}
		}

		if (!is_null($this->ad->list_time)) {
			$data['date'] = $this->ad->list_time;
		}

		if (isset($this->show_price_warn) && !empty($this->show_price_warn))
			$data['show_price_warn'] = $this->show_price_warn;

		if (!empty($this->service_type)) {
			/* Job categories*/
			$service_types = explode("|",$this->service_type);
			$data["service_types"]= $service_types;
		}

		if (!empty($this->job_category)) {
			/* Job categories*/
			$job_categories = explode("|",$this->job_category);
			$data["job_categories"]= $job_categories;
		}

		if (!empty($this->services)) {
			/* Inmo categories*/
			$serv = explode("|",$this->services);
			$data["services"]= $serv;
		}

		if (!empty($this->footwear_type)) {
			/* footwear_type */
			$footwear_type = explode("|",$this->footwear_type);
			$data["footwear_type"]= $footwear_type;
		}

		if (!empty($this->equipment)) {
			$equip = explode("|", $this->equipment);
			$data["equipment"]= $equip;
		}

		/* Display the body with linebreaks in PREVIEW step */
		$data['body'] = nl2br(wordwrap($data['body'], 50, "\n", true));

		$data['page_title'] = lang('PAGE_NEWAD_PREVIEW');

		if (isset($this->offseason_checked_descr))
			$data['offseason_checked_descr'] = $this->offseason_checked_descr;
		if (isset($this->peakseason_checked_descr))
			$data['peakseason_checked_descr'] = $this->peakseason_checked_descr;
		if (isset($this->available_weeks_checked_descr))
			$data['available_weeks_checked_descr'] = $this->available_weeks_checked_descr;

		$ad_price = $this->get_ad_price(false, true);

		if ($this->account >= $ad_price) {
			$data['can_verify'] = 1;
		}
		if ($this->voucher >= $ad_price) {
			$data['show_voucher'] = 1;
		} else {
			$data['show_voucher'] = 0;
		}

		$data['show_phone'] = 1;

		if (empty($this->pay_type) && !isset($data['can_verify']) && bconf_get($BCONF, "ai.pay_type.default")) {
			foreach (bconf_get($BCONF, "ai.pay_type.default") as $def_pt) {
				if (isset($data["show_$def_pt"]) && $data["show_$def_pt"] == 1) {
					$this->pay_type = $def_pt;
					break;
				}
			}
		}

		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$data['is_admin'] = 1;
		elseif (isset ($this->hide_fields)) {
			foreach ($this->hide_fields as $field)
				$data["hide_$field"] = "1";
		}

		if ($cl) {
			$store = new blocket_store(false);

			if ($store->check_logged_in() && @$data['is_admin'] != 1)
				$this->store_token = true;
		}

		if (!$this->store_id)
			$this->store_id = $this->ad->store;

		if ($this->store_id && !isset($data['can_verify'])
				&& (!$this->action || in_array($this->action, array('new')))) {
			$transaction = new bTransaction();

			$transaction->add_data('id', $this->store_id);
			$transaction->add_data('offset', 0);
			$transaction->add_data('lim', 1);

			$store_data = $transaction->send_command('search_comp');
			foreach (handle_search_comp_reply($store_data) as $key => $val) {
				if (!isset ($this->ad->$key) && !isset($data[$key]))
					$data[$key] = $val;
			}
			if ($data['free_ads'] > 0) {
				$this->pay_type = 'campaign';
				$this->campaign = 'start_package';
			}
		}

		if ($this->gallery_enabled() && !$this->has_gallery) {
			$data['gallery_price'] = $this->get_gallery_price();
			$ad_price_with_gallery = $ad_price;
			// On the first time through, gallery price is not already included so add it.
			if(@!$this->ad->gallery)
				$ad_price_with_gallery += intval($data['gallery_price']);

			// Check if there's a valid pay phone for this ad with gallery price added.
			$payphones = bconf_get($BCONF, "*.common.payphones.ads");
			$phone_match = 0;
			foreach($payphones as $k => $v) {
				if( ($ad_price_with_gallery == $k)) {
					$phone_match = 1;
					break;
				}
			}

			if ($phone_match) {
				$data['gallery_enabled'] = 1;
			}

			$data['gallery_phone_match'] = $phone_match;
		}

		if(isset($data['gallery_enabled']) && $data['gallery_enabled'] == 1)
			$data['gallery'] = @$this->gallery ? 1 : 0;

		// Remember_e-mail_in_a_cookie feature
		if (isset($_COOKIE['email']) && strpos($_COOKIE['email'], md5($this->ad->email)) !== false)
			$data['has_email_in_cookie'] = 1;

		/* Store the unfinished ad (CHUS 229) */
		GLOBAL $session;
		$unf_session_id = $session->_find_session_id();

		if (isset($this->unf_ad_id)) {
			$unf_ad_id = $this->unf_ad_id;
		}
		else {
			$unf_ad_id = NULL;
		}

		/* Save the unfinished ad */
		$unf_transaction = new bTransaction();
		$unf_transaction->add_data('log_string', log_string());

		/*
		FT826: the unfinished ad doesn't work
		We encode the serialized string to avoid the direct use of "null" characters and other no printeable characters
		*/
		$serialized = base64_encode(serialize($this));

		$unf_transaction->add_data('unf_ad_id', $unf_ad_id);
		$unf_transaction->add_data('session_id',$unf_session_id);
		$unf_transaction->add_data('email', $this->email);
		$unf_transaction->add_data('data', $serialized);
		$unf_reply = $unf_transaction->send_command('save_unfinished_ad');


		if ($unf_transaction->has_error()) {
			syslog(LOG_INFO, log_string()." Error when try to store unfinished ad (Iluvatar killed a cat)");
		}
		else {
			$this->unf_ad_id = $unf_reply["save_unfinished_ad"][0]["o_unf_ad_id"];
		}

		setcookie('email', md5($this->ad->email), time() + 60*60*24*365, '/');

		if(isset($_POST['create'])) {
			$goto = $this->newad_create(NULL, NULL, NULL, NULL );
				return $goto;
		}

		syslog(LOG_INFO, log_string()." Go to preview");
		//return 'preview';

		if(isset($this->images) && count($this->images) > 0) {
			$dirs = array();
			foreach($this->images as $k => $v) {
				$dirs[] = substr($v, 0, 2)."/";
			}
			$data['imgdirs'] = $dirs;
		} else {
			unset($this->images);
		}

		$this->headstyles = bconf_get($BCONF,"*.common.ad_preview.headstyles");
		$this->headscripts = bconf_get($BCONF,"*.common.ad_preview.headscripts");
		$this->display_layout('ai/preview.html', $data, null, null, 'html5_base');
	}


	/* User has verified the ad, create it. */
	function newad_create($back, $pay_type, $store_passwd, $gallery, $accept_conditions = true) {
		global $session;
		global $BCONF;
		$err_flag = 0;
		bLogger::logDebug(__METHOD__, "Create salted_passwd value is {$this->salted_passwd}");

		$this->verify_company_ad_by_cat();
		/* We'd like to remember these if preview reloads */
		$this->gallery = $gallery;

		if ($back){
			$this->back = $back;
			return 'form';
		}

		$this->ad_price = $this->get_ad_price();

		$store = new blocket_store(false);

		/* Paytype */

		syslog(LOG_INFO, log_string()."PayType: newad_create before = ". $pay_type);

		if (bconf_get($BCONF, "*.common.pay_type.free.enabled") == 1) {
			if ($this->ad_price > 0 && (!$this->action || $this->action == 'new'))
				$pay_type = "card";
			else
				$pay_type = "free";
		} else if ($this->pay_type == 'campaign')
			$pay_type = 'campaign';
		else if (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad'))) {
			if (bconf_get($BCONF, '*.common.payex.enabled') != 1 || bconf_get($BCONF, '*.common.payex.hidden') != 0) {
				if ($this->account >= $this->ad_price)
					$pay_type = 'verify';
				if ($pay_type == 'card')
					return 'preview';
				$this->pay_type=$pay_type;
			} elseif ($this->account >= $this->ad_price)
				$pay_type = 'verify';
			elseif ($pay_type)
				$this->pay_type = $pay_type;
		} else
			$pay_type = 'verify';

		syslog(LOG_INFO, log_string()."PayType: newad_create after = ". $pay_type);

		if (isset($this->ad->store))
			$store_id = $this->ad->store;

		if ($pay_type != 'verify' && isset($store_id)
		    && isset($store_passwd) && !empty($store_id) && !empty($store_passwd)) {
			$username = $this->ad->email;
			$this->login_id=$store_id;
			$store_login =	$this->store_verify_login($username, $store_passwd);
			if ($store_login)
				$this->store_token = true;
			else
				return 'preview';
		}

		if ($err_flag) {
			return 'preview';
		}

		/* Prepare transaction */
		$errors = array();
		$warnings = array();

		$transaction = new bTransaction();

		$transaction->add_data('log_string', log_string());
		$transaction->add_client_info();
		if (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad')))
			$transaction->add_data('pay_type', $pay_type);

		/* If user never had an image set and now uploaded extra images, pay extra */
		if ($this->pay_for_extra_images()) {
			$transaction->add_data('image_set', 1);
		} else if ($this->already_paid_extra_images) {
			$transaction->add_data('image_set', 0);
		}

		/* Add images to transaction */
		if (!empty($this->images)) {
			$transaction->add_data("image", $this->images);
			$transaction->add_data("thumbnail_digest", $this->thumbnail_digest);
			$transaction->add_data("digest_present", $this->digest_present);
		}

		/* UID */
		if (!empty($_COOKIE['uid'])) {
			// only use the cookie UID when it has a valid value
			$cookie_uid = (int)$_COOKIE['uid'];
			if ($cookie_uid > 0) {
				$transaction->add_data('uid', $cookie_uid);
			} else {
				unset($_COOKIE['uid']);
			}
		} else if (isset($_COOKIE['annonsbev'])) {
			$old_uid = (int)preg_replace("/^(\+-)?\d/", "", $_COOKIE['annonsbev']);
			$transaction->add_data('uid', (string)$old_uid);
		}

		/* Insert REDIR */
		if (isset($_COOKIE['redir'])) {
			$pos = strrpos($_COOKIE['redir'], '-');
			$site = substr($_COOKIE['redir'], 0, $pos);
			$transaction->add_data('redir_code', $site);
		}

		/* Send ad creation transaction */
		syslog(LOG_INFO, log_string()."Send ad creation transaction (creation)");

		/* Admin stuff, send token */
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad'))) {
			$transaction->auth_type = 'admin';
			$transaction->add_data('do_not_send_mail', 1);
		} else if ($this->store_token) {
			$transaction->auth_type = 'store';
		}
		$transaction_data = $this->ad->get_data_array(BAD_CLEANUP | BAD_UNSET);

		if (array_key_exists('can_edit_ad', $transaction_data)) {
			unset($transaction_data['can_edit_ad']);
		}

		$this->phone_std_for_trans($transaction_data);
		$transaction->populate($transaction_data, bconf_get($BCONF, "*.common.list_features.feat"), bconf_get($BCONF, "*.common.commalist_features.feat"));

		if ($this->action) {
			$transaction->add_data('ad_type', $this->action);
			if ($this->list_id && strpos($this->list_id, '.') == false)
				$transaction->add_data('list_id', $this->list_id);
			else
				$transaction->add_data('ad_id', $this->ad_id);
		}

		/* If editing a refused action, send previous action_id. */
		if ($this->action == 'refused') {
			$transaction->add_data('prev_action_id', $this->action_id);
			/* This is the action type before any editrefused. */
			if ($this->pre_refuse_action_type)
				$transaction->add_data('pre_refuse_action_type', $this->pre_refuse_action_type);
		}

		// Tell transaction handler that this isn't a usual "edit"
		if (!empty($this->addimages)) {
			$transaction->add_data('addimages', 1);

			/* FT3318: To be able to add images to an old-category ad - we don't attempt to convert to new categories */
			if ($this->ad->category > 0 && $this->ad->category < 1000)
				$transaction->add_data('old_cat', 1);
		}

		if (isset($this->ht) && ht_to_source($this->ht))
			$transaction->add_data('source', ht_to_source($this->ht));


		if ($gallery && $this->gallery_enabled()) {
			$transaction->add_data('gallery', 'request');
			if ($this->ht && $this->action == 'gallery') {
				$transaction->add_data('addgallery', '1');
			}
		}

		if ($pay_type == 'campaign') {
			if ($this->campaign_ad_id)
				$transaction->add_data('cleared_by', $this->campaign_ad_id);
			$transaction->add_data('campaign_id', $this->campaign);
		}

		if (in_array($pay_type, array('free')) && bconf_get($BCONF, '*.ai.no_verify_email') == 1) {
			$transaction->add_data('do_not_send_mail', 1);
		}

		 /*Job categories*/
		if (!empty($this->job_category)) {
			$transaction->add_data('job_category',$this->job_category);
		}

		/* service_type */
		if (!empty($this->service_type)) {
			$transaction->add_data('service_type',$this->service_type);
		}

		 /* services real estate*/
		if (($this->ad->category == 1260) && in_array($this->ad->estate_type, array(1,2,3))) {
			if (isset($this->services)) {
				$transaction->add_data('services', $this->services);
			}
		}

		/* footwear_type */
		if (!empty($this->footwear_type)) {
			$transaction->add_data('footwear_type',$this->footwear_type);
		}

		/* equipment */
		if (in_array($this->ad->category, array(1220,1240)) && in_array($this->ad->estate_type, array(1,2,3))){
			if (isset($this->equipment)) {
				$transaction->add_data('equipment', $this->equipment);
			}
		}

		if (!empty($this->currency)) {
			$transaction->add_data('currency', $this->currency);
		}

		if ($transaction->auth_type)
			$reply = $transaction->send_admin_command('newad', true, false);
		else
			$reply = $transaction->send_command('newad');

		syslog(LOG_INFO, log_string()."Reply from transhandler recieved (creation)");
		$this->handle_reply($reply);
		$this->set_errors($transaction->get_errors());

		if ($this->has_error()) {
			//CLEAR
			syslog(LOG_INFO, log_string()."has error this->ad_id: ".$this->ad_id);
			syslog(LOG_INFO, log_string()."has error reply->ad_id: ".$reply['ad_id']);
			syslog(LOG_INFO, log_string()."has error info: ".implode("|",$transaction->get_errors()));

			$this->pay_type = $pay_type;
			/* In case of error now redirect to the form, because the preview doesn't show errors */
			return 'form';
		}

		global $account_util;
		$account_session = new AccountSession();
		$data_to_update = array('phone' => $this->ad->phone, 'region' => $this->ad->region, 'commune' => $this->ad->communes);

		if(!$account_util->edit_account_data_if_empty($this->ad->email, $data_to_update, $account_session)){
			syslog(LOG_ERR, log_string()."ERROR: Account data could not be updated for this account");
		}

		$account_successfully_created = false;
		# call to the create account trans
		if (!empty($this->ai_create_account) && $this->ai_create_account == "on"){
			# check if the email has an account
			if ($account_session->authenticateSpecial($this->ad->email))
				bLogger::logInfo(__METHOD__, "Trying to create an account with a mail already active: ".$this->ad->email);
			else {
				bLogger::logInfo(__METHOD__, "Creating account ".$this->ad->email);
				$account_trans = new bTransaction();
				$account_trans->add_data('name', $this->ad->name);
				$account_trans->add_data('email', $this->ad->email);
				if (isset($this->rut)) {
					$account_trans->add_data('rut', $this->rut);
				}
				if (isset($this->business_name)) {
					$account_trans->add_data('business_name', $this->business_name);
				}

				$account_trans->add_data('is_company', $this->ad->company_ad);
				$account_trans->add_data('region', $this->ad->region);
				$account_trans->add_data('commune', $this->communes);
				if (empty($this->ad->passwd)) {
					$this->ad->passwd = $this->salted_passwd;
				}
				$account_trans->add_data('password', $this->ad->passwd);
				$account_trans->add_data('accept_conditions', ($this->ai_accept_conditions) ? 1 : 0);
				$account_trans->add_data('source', $this->source);
				//Phone process
				$phone_array['phone_type'] = $this->ad->phone_type;
				$phone_array['phone'] = $this->ad->phone;
				$phone_array['area_code'] = $this->ad->area_code;
				$this->phone_std_for_trans($phone_array);
				$account_trans->add_data('phone', $phone_array['phone']);
				//Call to create account
				$account_reply = $account_trans->send_command('create_account');
				syslog(LOG_INFO, log_string()."Pre check error");
				if (!$account_trans->has_error(true) || $account_trans->get_reply('error') == 'ACCOUNT_ALREADY_EXISTS') {
					syslog(LOG_INFO, log_string()."Error account already exists!");
					$account_mail = $account_util->account_mail($this->ad->email, $this->ad->name);
					$account_successfully_created = true;
				}
			}
		}

		# If we get here then there were no errors.
		if (!empty($reply['ad_id']))
			   $this->ad_id = $reply['ad_id'];
		if (!empty($reply['action_id']))
			   $this->action_id = $reply['action_id'];

		/* Update UID cookie */
		$this->set_uid = $reply['uid'];

		$this->set_uid_cookie();
		if ($this->store_token && $this->ad->company_ad)
			unset($_SESSION['global']['store_noads']);

		if ($pay_type == "card" && !$this->action) {
			$this->pay_inserting_fee();
		}

		/*
		FT 434: The renew process show a page with message in english
		FT 435: The renew process doesn't close the unfinished add.

		I add (boris@yapo) the renew action to the list bellow
		*/
		if((!$this->action || in_array($this->action, array('edit','refused','editrefuse','renew'))) &&
		 (bconf_get($BCONF, '*.ai.no_verify_email') == 1) &&
		 (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad')))
		) {
			$vf_trans = new bTransaction();
			$vf_response = new bResponse();
			$vf_response->add_data('headscripts', array('common.js'));
			$vf_response->add_data('headstyles', array('ai_success.css'));
			$vf_response->add_data('headscript_templates', array('common/fb_conversion.html'));
			if ($pay_type == 'free') {
				$vf_trans->add_client_info();
				$vf_trans->add_data('verify_code', $reply['paycode']);
				$vf_reply = $vf_trans->send_command('clear', 1);

				$this->handle_reply($vf_reply);
				$this->set_errors( $vf_trans->get_errors() );
			} else {
				$vf_reply = array(
					'ad_subject' => $this->ad->subject
				);
			}

			global $account_session;
			if( $this->has_error() ){
				/* On this point the ad exists on the DB, on error it'll not be verified
				 * automatically - Zane */
				$vf_response->add_data('id', $reply['paycode']);
				$vf_response->add_data('content', 'vf/verify_error.html');
				$vf_response->add_data('page_title', lang('PAGE_VERIFY_ERROR_TITLE'));
			} else {
				/* Display new subject if it has been changed */
				if( isset($vf_reply['ad_new_subject']) ){
					$vf_response->add_data('ad_subject', $vf_reply['ad_new_subject']);
				} else {
					$vf_response->add_data('ad_subject', $vf_reply['ad_subject']);
				}

				if( isset($vf_reply['action_type']) ){
					$vf_response->add_data('action_type', $vf_reply['action_type']);
				}

				if( isset($vf_reply['pack_result']) ){
					$this->pack_result = $vf_reply['pack_result'];
					$vf_response->add_data('pack_url_suffix', pack_url_suffix($this->ad->category));
					$vf_response->add_data('pack_result', $vf_reply['pack_result']);
				}

				/* Get xiti info for templates */
				$vf_trans->reset();
				$vf_trans->add_data('verify_code', $reply['paycode']);
				$xiti_reply = $vf_trans->send_command('xiti_info', 1);
				$this->handle_reply($xiti_reply);

				/* If TRANS is not Ok, follow process, but don't load Xiti */
				if( !$this->has_error() && isset($vf_reply['ad_id']) ){
					$vf_response->add_data('ad_id', $vf_reply['ad_id']);
					$vf_response->add_data('company_ad', $vf_reply['company_ad']);
					$vf_response->add_data('region', $vf_reply['region']);
					$vf_response->add_data('category', $vf_reply['category']);
					$vf_response->add_data('has_images', $vf_reply['has_images']);
					$vf_response->add_data('number_of_images', $vf_reply['number_of_images']);
					$vf_response->add_data('xiti_ok', '1');
				} else {
					$vf_response->add_data('xiti_ok', '0');
				}

				if ($this->unf_ad_id != NULL && $this->unf_ad_id > 0) {
					/* Change the state for unconfirmed ad to deleted */
					$unf_transaction = new bTransaction();
					$unf_transaction->add_data('log_string', log_string());
					$unf_transaction->add_data("unf_ad_id", $this->unf_ad_id);
					$unf_transaction->add_data("unf_status", "deleted");

					$unf_reply = $unf_transaction->send_command('status_unfinished_ad');

					if ($unf_transaction->has_error()) {
						syslog(LOG_INFO, log_string()." Error when try to change the status to deleted on unfinished ad (Iluvatar killed two cute cats)");
					}
				}

				$upselling_category = empty($this->old_category)?$this->ad->category:$this->old_category;
				$inserting_fee = bconf_get($BCONF,"*.cat.${upselling_category}.inserting_fee") == 1;

				$vf_response->add_data('list_id', $this->list_id);
				$vf_response->add_data('ad_id', $this->ad_id);
				$vf_response->add_data('has_images', (count($this->images) == 0 ?0:1));
				$vf_response->add_data('upselling_category', $upselling_category);
				$vf_response->add_data('show_upselling_in_form', $this->show_upselling_in_form);

				if (!isset($this->action)
				 && (!empty($this->upselling_product)
				 && !$inserting_fee
				 && !$this->has_slots($account_session)
				 && is_pro_for($this->ad->category, $account_session->get_param('is_pro_for')))
				) {
					unset($this->upselling_product);
					$vf_response->add_data('show_upselling_error_no_slot', 'true');
					$vf_response->add_data('headstyles', bconf_get($BCONF,"*.common.pack.headstyles"));
					$ad_category = $this->ad->category;
					$pro_categories = $account_session->get_param('is_pro_for');
					$vf_response->add_data('content', 'ai/include/pack_confirm.html');
				} else if ($account_successfully_created) {
					$created_account = true;
					$vf_response->add_data('content', 'vf/verify_confirm_account_creation.html');
					// for now just 1, but it should be dynamically generated
					// here we process newads from non-account users that wants to create an account
					if ($this->validatePaymentDeliveryAd($this->ad)) {
						$vf_response->add_data("pd_newad_info", "1");
					}
					$vf_response->add_data('headstyles', array('ai_success.css'));
				} else {
					if( isset($vf_reply['pack_result']) && $vf_reply['pack_result'] >= 0 && $vf_reply['pack_result'] != 5){
						$vf_response->add_data('headstyles', bconf_get($BCONF,"*.common.pack.headstyles"));
						$ad_category = $this->ad->category;
						$pro_categories = $account_session->get_param('is_pro_for');
						$vf_response->add_data('content', 'ai/include/pack_confirm.html');
					} else if ( isset($vf_reply['pack_result']) && $vf_reply['pack_result'] == -3 ){
						$vf_response->add_data('category', floor($this->ad->category/1000)*1000);
						$vf_response->add_data('content', 'components/dual_pro/dual_pro.html');
					} else {
						$vf_response->add_data('headscripts', array('ai_success.js'));
						$vf_response->add_data('content', 'vf/verify_confirm.html');
					}
				}
				$vf_response->add_data('page_title', lang('PAGE_VERIFY_TITLE'));
			}

			$vf_response->add_data('page_name', lang('PAGE_VERIFY_NAME'));
			$vf_response->add_data('appl', 'vf');
			$vf_response->add_data('REMOTE_ADDR', $_SERVER['REMOTE_ADDR']);
			$vf_response->add_data('post_email', $this->ad->email);
			$vf_response->add_data('post_name', $this->ad->name);
			$vf_response->add_data('post_phone', $this->ad->phone);
			$vf_response->add_data('has_gallery', $this->has_gallery);
			$vf_response->add_data('has_label', $this->has_label);
			$vf_response->add_data('has_weekly_bump', $this->has_weekly_bump);
			$vf_response->add_data('has_daily_bump', $this->has_daily_bump);

			global $account_session;
			if($account_session->is_logged()) {
				$vf_response->add_data('account_name', $account_session->get_param('name'));
				$vf_response->add_data('account_is_pro_for', $account_session->get_param('is_pro_for'));
				$vf_response->add_data('user_value_name', $account_session->get_param('name'));
				// for now just 1, but it should be dynamically generated
				// here we process newads from users with an account
				if ($this->validatePaymentDeliveryAd($this->ad)) {
					$vf_response->add_data("pd_newad_info", "1");
				}
			}

			$vf_response->add_data('duplicated_detected',$this->duplicated_detected);
			$vf_response->show_html_results('html5_base');

			/* vf page happens above now, no third step as of now - Zane */
			#header("Location: /vf/{$reply['paycode']}?ca={$this->ad->region}_{$this->ad->type}");
			return 'FINISH';
		}

		return 'confirm';
	}

	function validatePaymentDeliveryAd($ad) {
		global $BCONF;
		if (!bconf_get($BCONF, "*.pd_newad_info.enabled")) {
			return false;
		}
		$PaymentDeliveryQueryService = new PaymentDeliveryQueryService();
		$body = array_map('strval', (array)$ad);
		$body["account"] = "1";
		return !$PaymentDeliveryQueryService->execute("ad_status", $body) == null;
	}

	function set_uid_cookie() {
		if (isset($this->set_uid) && !empty($this->set_uid)) {
			setcookie('uid', $this->set_uid, time() + 10 * 365 * 24 * 60 * 60, '/', bconf_get($BCONF, '*.common.session.cookiedomain')); // Expire in ten years!
			unset($this->set_uid);
		}
	}

	/* Show confirmation page. */
	function newad_confirm() {
		global $script_list;
		global $BCONF;

		$this->set_uid_cookie();

		$data = $this->get_action_data();
		$data += $this->get_ad_data();
		$data += $this->get_maps_data();
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$data['is_admin'] = 1;
		$data['headscripts'] = $script_list;

		if (!empty($this->addimages)) {
			$data['page_name'] = lang('PAGE_ADDIMAGES_NAME');
			$data['page_title'] = lang('PAGE_ADDIMAGES_TITLE');
		}

		/* Display ad about stores if this is a company ad without a store, and email has more ads */
		get_settings(bconf_get($BCONF,"*.category_settings"), "company_subscription", array($this, "key_lookup"), array($this, "set_values"), $company_subscription);

		if ($this->ad->company_ad && !$this->ad->store && !isset($company_subscription)) {
			$bs = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:1 email:{$this->email} company_ad:1");
			if (isset($bs['list_id']))
				$data['show_store_splash'] = 1;
		}

		if (@$this->pay_type == 'voucher') {
			$data['ad_price']=$this->ad_price;
			$data['voucher_result'] = $this->voucher_result;
			$data['paid_with_voucher'] = 1;
			$data['page_title'] = lang('PAGE_NEWAD_CONFIRM_PAID_TITLE');
			$data['page_name'] = lang('PAGE_NEWAD_CONFIRM_PAID_TITLE');
		} else if (@$this->pay_type == 'card') {
			$data['page_title'] = lang('PAGE_NEWAD_CONFIRM_PAID_TITLE');
			$data['page_name'] = lang('PAGE_NEWAD_CONFIRM_PAID_TITLE');
		}

		if (@$this->pay_type == "phone") {
			if ($this->action == "gallery") {
				$data['page_title'] = lang('PAGE_NEWAD_CONFIRM_PHONE_GALLERY_TITLE');
				$data['page_name'] = lang('PAGE_NEWAD_CONFIRM_PHONE_GALLERY_TITLE');
			} else {
				$data['page_title'] = lang('PAGE_NEWAD_CONFIRM_PHONE_TITLE');
				$data['page_name'] = lang('PAGE_NEWAD_CONFIRM_PHONE_TITLE');
			}
		}

		$this->display_layout('ai/confirm.html', $data, null, null, 'html5_base');

		/*
		 * Change state progress to restart,
		 * which will send user to empty form if back button is used,
		 * but allows her to stay on confirm by reloads.
		 */
		$this->state_progress = 'restart';
	}

	/* Reset object, but keep caller in case user uses the back button. */
	function newad_reset() {
		foreach ($this as $var => $val) {
			if (in_array($var, $this->static_vars))
				continue;
			if (in_array($var, $this->volatile_vars))
				continue;
			if (in_array($var, array ('caller', 'instance_id')))
				continue;
			$this->$var = null;
		}
	}

	function newad_show_hide($cmd) {
		if (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad')))
			return 'load';

		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $this->ad_id);
		if ($cmd == 'show')
			$transaction->add_data('newstatus', 'active');
		else
			$transaction->add_data('newstatus', 'hidden');
		$reply = $transaction->send_admin_command('ad_status_change');
		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'load';
		}
		$this->show_hide_done = $cmd;

		return 'show_hide_done';
	}

	function newad_show_hide_done() {
		$this->display_layout('ai/show_hide_done.html', null, null, null, 'html5_base');
	}

	/* Error state */
	function newad_error() {
		unset($this->headstyles);
		$data = array();
		$data["domain"] = "ad_not_found";
		$this->display_layout('ai/error.html', $data, null, null, 'html5_base');
		return 'FINISH';
	}

	function check_already_paid() {
		if (!$this->ad_id || !$this->action_id)
			return false;

		$transaction = new bTransaction();
		$transaction->add_data('ad_id', $this->ad_id);
		$transaction->add_data('action_id', $this->action_id);
		$reply = $transaction->send_command('action_state');

		if ($reply['status'] != 'TRANS_OK')
			return false;
		if ($reply['action_state'][0]['state'] != 'unpaid')
			return true;
		return false;
	}

	/* Get image data, if one has been uploaded. */
	function upload_image() {
		global $BCONF;

		$uploaded_image = null;
		$res = null;

		if (@$_FILES['image']['error'] == UPLOAD_ERR_OK && strlen(@$_FILES['image']['name']) && !$this->has_uploaded_too_many_images()) {
			syslog(LOG_INFO, log_string()."Image ({$_FILES['image']['name']}) uploaded with type ({$_FILES['image']['type']}) and size ({$_FILES['image']['size']})");

			/* Set the PHP memory limit to image max resolution * 4 (rgba) * 2 (to have enough memory to resize and etc) */
			ini_set("memory_limit", (bconf_get($BCONF, "*.common.image.maxres") * 4 * 2)."M");
			ini_set("max_execution_time", bconf_get($BCONF, "*.common.image.max_execution_time"));

			/* Convert the uploaded image */
			$uploaded_image = new bImage($_FILES['image']['tmp_name']);

			//Explicitly silence errors, it's only a log after all
			$size = @$uploaded_image->get_image_size();
			$size = sprintf("w:%s, h:%s", @$size[1], @$size[2]);
			$exif = @print_r(@$uploaded_image->_exif, true);
			bLogger::logInfo(__METHOD__, "uploaded image from desktop size: $size, exif $exif");

			$previously_uploaded = "0";
			$uploaded_image->resize();
			$thumbnail_buffer = $uploaded_image->create_thumbnail();

			/* Check if image upload generated error messages */
			if ($uploaded_image->has_error()) {
				$res = $uploaded_image->get_error();
				syslog(LOG_INFO, log_string()."Image ({$_FILES['image']['name']}) conversion, status=ERR".$res);
			} else {
				syslog(LOG_INFO, log_string()."Image ({$_FILES['image']['name']}) conversion, status=OK");
				$res = array($uploaded_image->_image_buffer, $thumbnail_buffer, $uploaded_image->_thumb_digest, $previously_uploaded);
			}

			/* Release image memory */
			$uploaded_image->destruct();

			/* Reset the memory limit to its original state */
			ini_restore("memory_limit");
			ini_restore("max_execution_time");
		} else if (@$_FILES['image']['error'] != UPLOAD_ERR_OK || strlen(@$_FILES['image']['name'])) {
			syslog(LOG_WARNING, log_string()."Image upload went wrong, should never happen");
			$res = 'ERROR_IMAGE_LOAD';
		}
		return $res;
	}

        /*
        *  This function embed thumbnail digest in image EXIF header
        */
        function digest_embed($uploaded_image) {
                $digest = $uploaded_image->_thumb_digest;

                // Get jpeg header
                $jpeg_header_data = get_jpeg_header_data($uploaded_image);

                if ($jpeg_header_data == FALSE) {
                        $uploaded_image->_error = 'ERROR_READING_JPEG_HEADER';
                        syslog(LOG_ERR, log_string()."Couldn't read Image jpeg header:this picture hasnt md5 coef embed");
                        return;
                }

                // New EXIF header construction
                // 15 is mandatory, 15 is leboncoin digest-md5 tag index
                $Exif_array_header = array('Byte_Align'=> 'MM',
                                          'Tags Name'=>'TIFF',
                                           array(15 => array('Tag Name'=>'digest',
                                                             'Tag Number' => 15,
                                                             'Data Type'=>'2',
                                                             'Data'=> array($digest),
                                                             'Type' => 'String')
                                                )
                                           );

                // EXIF header Insertion  in old JPEG header. Old exif header will be overwrited
                $new_jpeg_header_data = put_EXIF_JPEG( $Exif_array_header, $jpeg_header_data );
                if ($new_jpeg_header_data == FALSE) {
                        $uploaded_image->_error = 'ERROR_MODIFYING_JPEG_HEADER';
                        syslog(LOG_ERR, log_string()."Couldn't write exif header into jpeg header:this picture hasnt md5 coef embed");
                        return;
                }

                // Get new file same data, header modified with our digest tag
		if (put_jpeg_header_data($uploaded_image, $uploaded_image->_image_buffer, $uploaded_image->_image_buffer, $new_jpeg_header_data) === FALSE) {
                        $uploaded_image->_error = 'ERROR_INSERTING_JPEG_HEADER';
                        syslog(LOG_ERR, log_string()."Couldn't write exif header into jpeg header:this picture hasnt md5 coef embed");
                        return;
                }
	}

	function cat_tmpl($new_cat, $shown_cat, $new_type, $shown_type, $shown_company_ad, $new_company_ad, $estate_type, $store, $region, $footwear_gender, $currency) {
		global $BCONF;
		$io = array('has_store' => ($store ? 1 : 0));

		/* Look up old (shown) category first */
		$io['company_ad'] = $shown_company_ad;
		$io['category'] = $shown_cat;
		$io['parent'] = bconf_get($BCONF, "*.cat.$shown_cat.parent");
		$io['type'] = $shown_type;
		$io['estate_type'] = $estate_type;
		$io['footwear_gender'] = $footwear_gender;

		get_settings(bconf_get($BCONF,"*.category_settings"), "template",
			     create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
			     create_function('$s,$k,$v,$d', '$d["old_template_" . $k] = $v;'),
			     $io);

		/* now look up new category */
		$io['company_ad'] = $new_company_ad;
		$io['parent'] = bconf_get($BCONF, "*.cat.$new_cat.parent");
		$io['category'] = $new_cat;
		$io['type'] = $new_type;
		$io['estate_type'] = $estate_type;
		$io['footwear_gender'] = $footwear_gender;

		get_settings(bconf_get($BCONF,"*.category_settings"), "template",
			     create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
			     create_function('$s,$k,$v,$d', '$d["new_template_" . $k] = $v;'),
			     $io);

		if (!isset($io['old_template_ai']))
			$io['old_template_ai'] = "general";
		if (!isset($io['new_template_ai']))
			$io['new_template_ai'] = "general";

		$volatile = isset($io['old_template_ai_volatile']) || isset($io['new_template_ai_volatile']);

		$shown_tmpl = $io['old_template_ai'];
		$new_tmpl = $io['new_template_ai'];

		if (!$volatile && $new_tmpl == $shown_tmpl && $new_type == $shown_type && $new_cat == $shown_cat && $estate_type == 0 && $footwear_gender == 0)
			return;

		/**
			Some params need to be removed from the session upon changes to the form.
			'.volatile_on' can contain a list of cat_tmpl variables which, if they differ,
			will unset the corresponding param.
		*/

		foreach (bconf_get($BCONF, "*.common.ad_params") as $key => $tmp) {
			if ($volatile_on = bconf_get($BCONF, "*.common.ad_params.$key.volatile_on")) {
				$param = bconf_get($BCONF, "*.common.ad_params.$key.name");
				$vols = explode(",", $volatile_on);
				foreach ($vols as $v) {
					$var_new = "new_".$v;
					$var_shown = "shown_".$v;
					if ($$var_new != $$var_shown) {
			    			unset($this->ad->$param);
						break;
					}
				}
			}
		}

		$io = array('category' => $new_cat,
				'parent' => bconf_get($BCONF, "*.cat.$new_cat.parent"),
				'type' => $new_type,
				'company_ad' => $new_company_ad,
				'has_store' => ($store ? 1 : 0)
			);
		if($estate_type != 0) {
			$io['estate_type'] = $estate_type;
		}
		if($footwear_gender!= 0) {
			$io['footwear_gender'] = $footwear_gender;
		}

		get_settings(bconf_get($BCONF,"*.category_settings"), "params",
				create_function('$s,$k,$d', 'return isset($d[$k]) ? $d[$k] : NULL;'),
				create_function('$s,$k,$v,$d', '$d[$k] = $v;'),
				$io);

		$adp_list = array_keys($io);
		$data = array('category' => $new_cat,
				'parent' => bconf_get($BCONF, "*.cat.$new_cat.parent"),
				'type' => $new_type,
				'company_ad' => $new_company_ad,
				'has_store' => ($store ? 1 : 0),
				'is_cat_tmpl' => 1,
				'shown_cat' => $shown_cat,
				'currency' => $currency,
				);

		global $account_session;
		if ($account_session->is_logged()) {
			$data['account_pack_type'] = $account_session->get_param('pack_type') ?: '';
			$data['account_pack_status'] = $account_session->get_param('pack_status') ?: '';
		}

		if (isset($region) && !empty($region) && $region != 0) {
			$data['region'] = $region;
		}
		if($estate_type != 0) {
			$data['estate_type'] = $estate_type;
		}
		if($footwear_gender != 0) {
			$data['footwear_gender'] = $footwear_gender;
		}

		foreach($adp_list as $key)
			$data[$key.'_visible'] = 1;
		$data += generate_weeks($response);

		$data += array('ca' => $this->caller->to_string(), 'ca_region' => $this->caller->region, 'ca_city' => $this->caller->city);
		if(strstr($_SERVER['HTTP_USER_AGENT'],"Chrome")||strstr($_SERVER['HTTP_USER_AGENT'],"Safari")) {
			$data['webkit_browser'] = 1;
		}

/*
		$type_list = bconf_get($BCONF, "*.category_settings.types.1.$new_cat.value");

		if ($type_list !== NULL) {
			$type_list = explode(',', $type_list);
			$this->type_list = $type_list;
		}
		else {
			$type_list = bconf_get($BCONF, "*.category_settings.types.2.default");
			$type_list = explode(',', $type_list);
			$this->type_list = $type_list;
		}
*/
		$this->display("ai/form_main_fragment.html", $data);
		return;
	}

	/* Check if category has changed and no JS is present. */
	function check_type_list() {
		global $BCONF;

		/* Get the current ad types (s, k ...) for chosen category and compare to last chosen types */
		if ($this->ad->category > 0) {
			get_settings(bconf_get($BCONF,"*.category_settings"), "types", array($this, "key_lookup"), array($this, "set_values"), $type_list);

			if (implode(',', $this->type_list) != $type_list) {
				/* ERROR */
				syslog(LOG_INFO, log_string()."User doesn't have JS support and category types needs a refresh " . implode(',', $this->type_list) . " ----> $type_list");
				$this->type_list = explode(',',$type_list);
				return true;
			}
		}
		return false;
	}

	/* Check if there're extra images present, but no JS, and the maximum has increased. */
	function check_extra_images_count($old_category) {
		return ($old_category &&
		        $this->ad->category &&
			$this->get_number_extra_images() &&
			get_category_max_extra_images($this->ad->category) > get_category_max_extra_images($old_category));
	}

	function has_uploaded_too_many_images() {
		/*
		 * !$this->ad is true if we are calling through ajax newad_upload_image() - in that case, no limit is directly set
		 */
		if (!$this->ad ) {
			$max = 0;
		} else if ( $this->ad->category==NULL ) {
			/* This means "All categories" */
			$max = get_category_max_extra_images("0");
		} else {
			$max = get_category_max_extra_images($this->ad->category);
		}
		return ($this->get_number_extra_images() > $max);
	}

	/* Return true if user should pay for extra images. */
	function pay_for_extra_images() {
		if ($this->get_number_extra_images()) {
			/* Check if extra images was added this edit. */
			if (!$this->already_paid_extra_images)
				return true;

			/* Renew and deleted (prolong) pays full price. */
			if (in_array($this->action, array('renew', 'deleted')))
				return true;

			/* If refused, need to check that previous action was not an edit. */
			if ($this->action == 'refused' && $this->pre_refuse_action_type != 'edit')
				return true;
		}
		return false;
	}

	function get_number_extra_images() {
		if (count($this->images))
			return count($this->images) - 1;
		return 0;
	}

	function gallery_enabled() {
		global $BCONF;

		get_settings(bconf_get($BCONF,"*.gallery_settings"), "gallery", array($this, "key_lookup"),
				create_function('$s,$k,$v,$d', '$d[$k] = $v;'), $data);

		if (isset($data['enabled']) && $data['enabled'])
			return 1;

		return 0;
	}

	function get_category_price() {
		get_settings(bconf_get($BCONF,"*.category_settings"), "price", array($this, "key_lookup"),
				create_function('$s,$k,$v,$d', '$d = $v;'), $price);

		syslog(LOG_INFO, log_string()."get_category_price price: $price");

		return $price;
	}

	function get_gallery_price() {
		global $BCONF;

		get_settings(bconf_get($BCONF,"*.gallery_settings"), "gallery_price", array($this, "key_lookup"),
				create_function('$s,$k,$v,$d', '$d[$k] = $v;'), $price);

		if (isset($price['price']))
			return $price['price'];

		return 0;
	}

	function get_ad_price($specified = false, $nogallery = false) {
		global $BCONF;

		$price = $this->get_category_price();
		$data['ad'] = $price;

		if (isset($this->ad) && !empty($this->gallery) && $this->gallery_enabled() && !$nogallery) {
			$data['gallery_price'] = $this->get_gallery_price();
			$price += $data['gallery_price'];
		}

		// We must do these if $specified is numbers, because callers expect to find
		// these entries in $data. This should be safe since we don't return $price in this case.
		if ($this->pay_for_extra_images() || $this->addimages || $specified === 'numbers') {
			get_settings(bconf_get($BCONF,"*.category_settings"), "extra_images", array($this, "key_lookup"),
					create_function('$s,$k,$v,$d', '$d[$k] = $v;'), $extra_image_price);
			$price += ($data['extra_images'] = $extra_image_price['price']);
			if ($specified === 'numbers')
				$data['extra_images_number'] = $extra_image_price['max'];
		}

		if ($specified) return $data;

		return $price;
	}


	/* Handle special transaction reply values. */
	/* XXX move some of these to bAd? */
	function handle_reply($reply) {
		$this->stores = null;

		foreach ($reply as $key => $str) {
			switch($key) {
			case 'order_id':
				$this->order_id = $str;
				break;

			case 'action_id':
				$this->action_id = $str;
				break;

			case 'verify':
				$paid = $this->get_ad_price();

				syslog(LOG_INFO, log_string()."Prepaid $paid kr and has left $str kr in account");

				$this->account = intval($str);
				$this->paid = $paid;
				break;

			case 'stores':
				$stores = explode("\t", $str);
				foreach($stores as $store) {
					$store_id = (int)$store;
					$store_name = substr($store, strpos($store, ":") + 1);
					if ($store_id > 0) {
						$this->stores[$store_id] = $store_name;
						syslog(LOG_INFO, log_string()."Retrieved store $store_name with id $store_id");
					}
				}
				if (count($this->stores) == 1 && $this->ad) {
					$this->ad->store = (int)$stores[0];
				}
				break;

			case 'images':
				$this->images = array();
                                $this->thumbnail_digest = array();
                                $this->digest_present = array();
				ksort($str);
				$digest_found = 0;
				foreach ($str as $num => $img) {
				        if (!is_error($img['name'])) {
                                                $this->images[] = $img['name'];
                                                @$this->digest_present[] = $img['digest_present'];
                                                if (isset($reply['digest']) && is_array($reply['digest'])) {
                                                        foreach ( $reply['digest'] as $ind => $values) {
                                                                if(@$values['db_name'] == $img['name'])
                                                                        if (isset($values['digest'])) {
                                                                                $this->thumbnail_digest[] = $values['digest'];
										$digest_found = 1;
									}
                                                        }
							if (!$digest_found)
	                                                        $this->thumbnail_digest[] = "";
							else
								$digest_found = 0;
                                                } else {
	                                                $this->thumbnail_digest[] = "";
						}
                                        }
				}
				if (count($this->images) > 1) {
					$this->already_paid_extra_images = 1;
					$this->extra_images_button = true;
				}
				break;

			case 'params':
				break;

			case 'account':
				if (!isset($reply['verify'])) /* Verify contains more up to date account. */
					$this->account = intval($str);
				break;
			case 'voucher':
				$this->voucher = intval($str);
				break;
			case 'voucher_result':
				$this->voucher_result = intval($str);
				break;
			case 'users':
				if (isset($str['account']))
					$this->account = intval($str['account']);
				break;
			}
		}

		/* If the reply didn't have any stores, unset ad store */
		if ($this->ad && !$this->stores && !$this->action && (!isset($reply['store']) || $reply['store']['active'] != "t"))
			$this->ad->store = null;
	}

	/* Get template data based on action type. */
	function get_action_data() {
		global $ignore_fields;
		global $hide_fields;

		$data = array();
		if ($this->action && (!Cpanel::checkLogin() || (!Cpanel::hasPriv('adminedit') && !Cpanel::hasPriv('adminad.edit_ad')))) {
			if (isset($ignore_fields[$this->action])) {
				foreach($ignore_fields[$this->action] as $field) {
					if ($field == "area" && !isset($this->ad->area))
						continue;
					$data["ignore_$field"] = 1;
				}
			}
			if (isset($hide_fields[$this->action])) {
				foreach($hide_fields[$this->action] as $field)
					$data["hide_$field"] = 1;
			}
		}
		switch($this->action) {
		case 'edit':
			$data['page_title'] = lang('PAGE_EDIT_TITLE');
			$data['page_name'] = lang('PAGE_EDIT_NAME');
			break;
		case 'renew':
			$data['page_title'] = lang('PAGE_RENEW_TITLE');
			$data['page_name'] = lang('PAGE_RENEW_NAME');
			break;
		default:
			$data['page_title'] = lang('PAGE_NEWAD_TITLE');
			$data['page_name'] = lang('PAGE_NEWAD_NAME');
			break;
		}
		return $data;
	}

	function clear_formated_number($formated) {
		$cleaned = preg_replace('/[^\d]/', '', $formated);
		return $cleaned;
	}

	/* Load ad data, or defaults if no ad. */
	function get_ad_data() {
		global $BCONF;

		$data = array();
		if ($this->ad) {
			/* Store static vars */
			$this->company_ad = $this->ad->company_ad;
			$this->name = $this->ad->name;
			$this->email = $this->ad->email;
			$this->phone_type = $this->ad->phone_type;
			$this->area_code = $this->ad->area_code;
			$this->phone = $this->clear_formated_number($this->ad->phone);
			$this->phone_hidden = $this->ad->phone_hidden;
			$this->area = $this->ad->area;
			$this->hide_address = $this->ad->hide_address;

			foreach($this->ad->get_data_array() as $var => $val) {
				if (!is_null($val) && !isset($this->$var)) {
					if (is_array($val)) {
						foreach ($val as $k => $v) {
							$data[$var][$k] = htmlentities($v, ENT_COMPAT, $BCONF['*']['common']['charset']);
						}
					} else {
						$data[$var] = htmlentities($val, ENT_COMPAT, $BCONF['*']['common']['charset']);
					}
				}
			}
		} else {
			/* If _GET caller is set then use region and city from argument,
			 * The default behaviour for newad is When no caller is given then
			 * it's up to the user to choose region/city
			 */
			if (!$this->caller->default_set) {
				$data['region'] = $this->caller->region;
			}
			$data['type'] = 's';
		}

		# get the rut from the database
		if ($this->company_ad == 1 && isset($this->email) && !isset($this->rut))
		{
			$get_account_trans = new bTransaction();
			$get_account_trans->add_data('email', $this->email);
			/* Jose told me that is better to not use optional params, blame him */
			$get_account_reply = $get_account_trans->send_command('get_account');
			if (isset($get_account_reply) && $get_account_reply['status'] == 'TRANS_OK') {
				syslog(LOG_INFO, log_string()."Retrieved account data for email {$this->email}");
 				if (isset($get_account_reply['rut'])) {
					$data['rut'] = $get_account_reply['rut'];
				}
 				if (isset($get_account_reply['business_name'])) {
					$data['business_name'] = $get_account_reply['business_name'];
				}
			}
		}
		return $data;
	}

	function get_category_data() {
		global $BCONF;
		$res = array();

		/* Default types */
		get_settings(bconf_get($BCONF,"*.category_settings"), "types", array($this, "key_lookup"), array($this, "set_values"), $type_list);

		if (is_object($this->ad))
			syslog(LOG_INFO, log_string() . "ad: " . isset($this->ad) . " category: " . $this->ad->category);
		/* Show params */
		get_settings(bconf_get($BCONF,"*.category_settings"), "params", array($this, "key_lookup"), array($this, "set_values"), $params);

		syslog(LOG_INFO, log_string() . "Feature now set to: " . $params);
		$this->params = $params;

		foreach(explode(',', $params) as $key) {
			$stripped_key = explode(':', $key);
			$res[$stripped_key[0].'_visible'] = 1;
		}

		$this->type_list = explode(',', $type_list);

		return $res;
	}

	function get_store_data() {
		$res = array();

		if ($this->ad)
			$company_ad = $this->ad->company_ad;
		else
			$company_ad = $this->company_ad;
		if ($company_ad && $this->stores) {
			if (count($this->stores) > 0)
				$res['show_stores'] = 1;

			foreach($this->stores as $id => $name) {
				$res['store_id_list'][] = $id;
				$res['store_name_list'][] = $name;
			}
		}

		return $res;
	}

	function get_maps_data() {
		$res = array();
		$res["address"] = (isset($this->address) ? $this->address : '');
		$res["address_number"] = (isset($this->address_number) ? $this->address_number : '');
		$res["add_location"] = (isset($this->add_location) ? $this->add_location : '');
		$res["communes"] = (isset($this->communes) ? $this->communes : '');
		$res["geoposition"] = (isset($this->geoposition) ? $this->geoposition : '');
		$res["geoposition_is_precise"] = (isset($this->geoposition_is_precise) ? $this->geoposition_is_precise : '');
		return $res;
	}

	function display_action_page($cmd = null, $data = null, $passwd = null) {
		global $BCONF, $script_list_deletion, $styles_list_deletion, $script_list;

		if (!is_array($data))
		    $data = array();
		if (Cpanel::checkLogin() && (Cpanel::hasPriv('adminedit') || Cpanel::hasPriv('adminad.edit_ad')))
			$data['is_admin'] = 1;

		$data['headscripts'] = $script_list;
		if(!isset($data['is_admin']) || !$data['is_admin']){
			if($this->action == "delete" || $cmd == "delete"){
				$data['headscripts'] = $script_list_deletion;
				$data['headstyles'] = $styles_list_deletion;
				$this->headstyles = $styles_list_deletion;
			}
		}

		$data['deletion_reason'] = $this->del_reason;
		$data['user_text'] = $this->user_text;
		$data['user_text_testimonial'] = $this->user_text_testimonial;
		$data['deletion_timer'] = $this->del_timer;

		if (!empty($passwd)) {
			if (check_password_hash($passwd, $this->salted_passwd)){
				$data['password_hash'] = $passwd;
			}
		}

		$price_spec = $this->get_ad_price('numbers');
		if (is_array($price_spec)) {
			foreach ($price_spec as $key => $price) {
				if ($key == 'extra_images_number')
					$data[$key] = $price;
				else
					$data["{$key}_price"] = $price;
			}
		}

		if(isset($this->ad))
			$data['type'] = $this->ad->type;

		if (!$this->action && $cmd)
			$this->action = $cmd;


		/*
		 * FT556: set the correct title for edit an ad or delete an add
		 */

		switch ($this->action) {
			case "edit":
				$data['page_title'] = lang('PAGE_ACTION_TITLE_EDIT');
				$data['page_name'] = lang('PAGE_ACTION_NAME_EDIT');
				break;
			case "delete":
				$data['page_title'] = lang('PAGE_ACTION_TITLE_DELETE');
				$data['page_name'] = lang('PAGE_ACTION_NAME_DELETE');
				break;
			default:
				$data['page_title'] = lang('PAGE_ACTION_TITLE');
				$data['page_name'] = lang('PAGE_ACTION_NAME');
				break;
		}

		if (isset($this->ad) && $this->ad->store && (!isset($data['is_admin']) || !$data['is_admin']) ) {
			return $this->store_session($data, $this->action);
		} else {

			if (isset($this->ad->mileage))
				$data['mileage'] = $this->ad->mileage;
			if (isset($this->ad->regdate))
				$data['regdate'] = $this->ad->regdate;
			if (!empty($this->has_gallery))
				$data['nas_gallery'] = 1;
			if (!empty($this->link_type))
				$data['link_type'] = $this->link_type;
			if (!empty($this->ad->inserting_fee))
                                $data['inserting_fee'] = $this->ad->inserting_fee;


			/* FT486 Enhancing already deleted ad error */
			if (isset($this->ad_status) && in_array($this->ad_status, array('deleted')))
				$this->headstyles = bconf_get($BCONF,"*.common.newad.error_headstyles");
			if (isset($this->ad_status) && in_array($this->ad_status, array('deactivated')))
				$this->headstyles = "ai.css";

			// If the logged account is the same as the ad owner; set this variable to hide the password input
			if ($this->logged_account_owns_ad()) {
				$data['logged_account_owns_ad'] = 1;
			}
			$data['email'] = $this->ad->email;
			$this->display_layout('ai/action.html', $data, null, null, 'html5_base');
		}
	}

	function store_session(&$data, $cmd = null) {
		global $session;

		$store = new blocket_store(false);
		if ($session->load_active_instance($store, get_class($store), "0") && $store->check_logged_in()) {
			if ($this->ad->store == $store->store_id) {
				if (isset($data['params.link_type']))
					return 'error';
				$this->store_token = true;
				$this->store_id = $store->store_id;
				if (!$this->action && $cmd)
					$this->action = $cmd;
				$this->display_layout('ai/action.html', $data, null, null, 'html5_base');
			} else {
				$this->wrong_store = 1;
				return 'error';
			}
		} else {
			$this->display_layout('ai/action_store.html');
		}
	}

	function set_values($setting, $key, $value, $data = null) {
		if ($data)
			$data .= ',' . $key;
		else
			$data = $key;

		if ($value)
			$data .= ":" . $value;
	}

	function key_lookup($setting, $key, $data = null) {
		global $BCONF;
		global $account_session;

		if ($key == 'type' && !is_object($this->ad))
			return 's';

		if (is_object($this->ad)) {
			if (!empty($this->ad->sub_category))
				$cat = $this->ad->sub_category;
			else if (!empty($this->ad->category_group))
				$cat = $this->ad->category_group;
			else
				$cat = $this->ad->category;

			if ($key == 'category')
				return $cat;
			else if ($key == 'parent')
				return bconf_get($BCONF, "*.cat.{$cat}.parent");
		} elseif ($key == 'category')
			return '0';

		if ($key == 'action' && $this->action == 'refused' && $this->pre_refuse_action_type == 'edit') {
			return 'edit';
		} else if (is_object($this->ad) && isset($this->ad->$key)) {
			if (is_array($this->ad->$key)) {
				return NULL;
			}
			return $this->ad->$key;
		}

		if ($key == 'pro_for_category') {
			$is_pro_for = $account_session->get_param('is_pro_for');
			$pro_for_cat = AccountUtil::is_pro_for_cat($is_pro_for, $this->ad->category);
			return $pro_for_cat;
		}

		return @$this->$key;
	}

	/* Filter for the id. Return an array to be sent to loadad. */
	function f_newad_id($val) {
		if (preg_match('/^S([0-9]{13,})$/', $val, $match))
			return array('action' => 'spidered', 'id' => $match[1]);

		if (preg_match('/^RH([a-zA-Z0-9=\+\/]+)$/', $val, $match)) {
			global $BCONF;
			$key = bconf_get($BCONF, '*.common.aes_cbc_keys.ad_refuse_email');
			$data = aes_cbc_decode($key, $match[1]);

			$parts = explode('_', $data);
			$id = (int)$parts[0];
			$action_id = (int)$parts[1];

			return array('action' => 'refused', 'id' => $id, 'action_id' => $action_id);
		}

		if (preg_match('/^D([0-9]{7,})$/', $val, $match))
			return array('action' => 'store_deleted', 'id' => $match[1]);

		if (preg_match('/^[0-9]{7,}\.[0-9]+$/', $val)) {
			return array('action' => 'fromcmd', 'id' => $val);
		}
		if (preg_match('/^[0-9]{13,}$/', $val))
			return array('action' => 'deleted', 'id' => $val);

		if (preg_match('/^[0-9]+$/', $val))
			return array('id' => $val);

		if (!empty($val))
			return false;

		return NULL;
	}

	/* Filter the choosen command, returning null if invalid. */
	function f_cmd($val) {
		if (preg_match('/^(delete|edit|renew|addimages|store|copy|bump|admin_renew|hide|show|activate|deactivate)$/', $val))
			return $val;
		return null;
	}

	/* Filter for password on action select page. */
	function f_passwd($val) {
		return (string)$val;
	}

	/* Check what image number to delete. */
	function f_remove_img($val) {
		if (!strlen($val))
			return -1;
		if ($val < 0)
			return 0;
		return $val;
	}

	/* Filter new passwords. */
	function f_new_passwd($val) {
		return (string)$val;
	}

	/* Filter store usernames. */
	function f_store_username($val) {
		return (string)$val;
	}

	/* Filter store passwords. */
	function f_store_passwd($val) {
		return (string)$val;
	}

	function f_pay_type($val) {
		if (in_array($val, array('card', 'phone', 'voucher', 'free')))
			return $val;
		return null;
	}

	function f_order_id($val) {
		if (preg_match('/^[0-9a]+$/', $val))
			return substr($val, 0, strpos($val, 'a'));
		return null;
	}

	function warn_price() {
		if (!$this->ad->price || $this->ad->price == "") {
			return false;
		}

		get_settings(bconf_get($BCONF,"*.category_settings"), "price_warning", array($this, "key_lookup"), array($this, "set_values"), $min);

		if (isset($min) && !empty($min)) {
			$m = explode(':', $min);
			if ($this->ad->price < $m[1]) {
				return true;
			}
		}
		return false;
	}

	function infopage_allowed() {
		/* case 1: allowed in feature list */
		get_settings(bconf_get($BCONF,"*.category_settings"), "infopage", array($this, "key_lookup"), array($this, "set_values"), $infopage_feat);
		if ($infopage_feat)
			return true;

		/* case 2: ad connected to a store */
		if (isset($this->ad->store))
			return true;

		return false;
	}

        function store_verify_login($username, $passwd) {
                global $BCONF;

                $username = trim($username);
                $passwd = trim($passwd);

		$transaction = new bTransaction();
		$transaction->add_data('store_id', $this->login_id);
		$reply = $transaction->send_command('store_login_info');

		if ($transaction->has_error()) {
			$this->set_errors($transaction->get_errors());
			return 'login';
		}

		$salt = $reply['salt'];

		/* transaction */
		$transaction->reset();
                $transaction->add_client_info();
                $transaction->auth_type = 'store';
                $transaction->add_data('log_string', log_string());
                $transaction->add_data('username', $username);
		$transaction->add_data('passwd', $salt . sha1($salt . $passwd));
		$transaction->add_data('auth_type', 'store');
		$transaction->add_data('auth_resource', $this->login_id);

                $reply = $transaction->send_command('authenticate');
                if ($transaction->has_error()) {
                        $this->set_errors($transaction->get_errors());
                        return 0;
                }
                /* User has logged in */

                $_SESSION['global']['store_username'] = $username;
                $_SESSION['global']['store_id'] = $this->login_id;
                unset($_SESSION['global']['store_noads']);
		$_SESSION['global']['store_name'] = $this->stores[$this->login_id];


                /* Set logged in cookie */
                setcookie('sli', '1', 0, '/', bconf_get($BCONF, '*.common.session.cookiedomain'));

                $this->store_id = $this->login_id;

                return 1;
        }

	function get_voucher_balance() {
		/* Get voucher balance */
		$transaction = new bTransaction();
		if (Cpanel::checkLogin() && Cpanel::hasPriv("stores")) {
			$transaction->auth_type = 'admin';
			$reply = $transaction->send_admin_command('store_voucher_balance');
			$transaction->handle_reply();
		/* Only logged in stores can have a voucher balance, so to save on unnecessary but harmless
		   transactions, return immediately if current session is NOT a logged in store.  */
		} else if(@$_SESSION['global']['store_id']) {
			$transaction->auth_type = 'store';
			$reply = $transaction->send_admin_command('store_voucher_balance');
		} else return;
		return $reply['voucher_balance'];
	}

	function f_deletion_reason($val) {
		global $BCONF;

		if (bconf_get($BCONF, '*.deletion_reason.' . $val))
			return $val;
		return null;
	}

	function f_deletion_timer($val) {
                global $BCONF;

                if (bconf_get($BCONF, '*.deletion_reason_timer.' . $val))
                        return $val;
                return null;
        }

	function f_string($val) {
		return strip_tags($val);
	}

	function f_type($val) {
		global $BCONF;

		if (bconf_get($BCONF, "*.common.type.list.$val"))
			return $val;
		return null;
	}

	function f_show_password($val) {
		if (!isset($val))
			return NULL;

		if (empty($val))
			return 0;

		return 1;
	}

    function fipe_searcher($carbrand, $carversion) {
        $bconf_key = "*.cardata.$carbrand";
        if(isset($carversion) && $carversion > 0 ) {
            $bconf_key .= ".$carversion";
        }
		global $BCONF;

		$result_branch = bconf_get($BCONF, $bconf_key);
        die(yapo_json_encode($result_branch));
	}

	/**
	 * Retrieves the password of the requested ad
	 */
	//TODO make a more generic function for the loadad transaction
	function get_ad_password($list_id) {
		//Get the ad information
		$transaction = new bTransaction();
		$transaction->add_data('id', $list_id);
		$reply = $transaction->send_command('loadad');
		//set the salted password as the retrieved ad's one
		//Basically whe skip the ad password check
		if (isset($reply['ad']['salted_passwd'])) {
			$passwd = $reply['ad']['salted_passwd'];
			return $passwd;
		}

		return '';
	}


	/** Function to know the first category pro for some user*/
	function first_pro($is_pro_for) {
	    $pro_for = explode(',', $is_pro_for);
	    if(floor($pro_for[0]/1000) == 1){
		syslog(LOG_INFO, ">>>>>>>INMO");
		return 'inmo';
	    } else if (floor($pro_for[0]/1000) == 2){
		syslog(LOG_INFO, ">>>>>>>AUTO");
		return 'auto';
	    }
	}

	/**
	 * Returns true if the current logged account is the
	 * owner of the current ad
	 */
	function logged_account_owns_ad() {
		global $account_session;
		return ($account_session->is_logged() && $this->ad->email === $account_session->get_param('email'));
	}

	function f_rut ($val) {
		return strtoupper(str_replace('.','',strip_tags($val)));
	}
	/**
	 * Modify the company ad if the category has an unique company type defined in bconf
	 */
	function verify_company_ad_by_cat(){
		global $BCONF;
		global $account_session;
		$company_ad_setting = split_settings(bconf_get($BCONF, '*.category_settings.rut_control.1.0.'.$this->ad->category.'.*.value'));
		if (isset($company_ad_setting) && isset($company_ad_setting['company_ad'])
			&& in_array($company_ad_setting['company_ad'], array('0','1'))){
			$this->ad->company_ad = $company_ad_setting['company_ad'];
		}
		$is_pro_for = $account_session->get_param("is_pro_for");
		if($account_session->is_logged() && !empty($is_pro_for)){
			$is_pro_for = "";
			get_settings(bconf_get($BCONF,"*.category_settings"), "is_pro_for", array($this, "key_lookup"), array($this, "set_values"), $is_pro_for);
			if($is_pro_for == "0"){
				$this->ad->company_ad = 1;
			}
		}
	}

	/**
	 * Check if the user is logged and try to loggin
	 * NOTE: Call to this function only if all validations are ok
	 */
	function check_account_session(&$data, $ad_email){
		global $account_session;
		/* If the user is logged but the email of the ad is diferent from account email distroy actual session */
		if($account_session->is_logged()){
			if($account_session->get_param('email') != $ad_email)
				$account_session->expire();
		}
		/* If the user is not logged but the passwd is OK do the loggin */
		if(!$account_session->is_logged()){
			if($account_session->loginSpecial($ad_email)){
				$this->add_account_session_data($data);
			}
		}
	}
	/* If the user is logged populate basic account info on $data */
	function add_account_session_data(&$data){
		global $account_session;
		if($account_session->is_logged()){
			$data['account_name']=$account_session->get_param('name');
			$data['account_email']=$account_session->get_param('email');
			$data['account_is_pro_for']=$account_session->get_param('is_pro_for');
			$data['user_value_name']=$data['account_name'];
		}
	}
	/* Simplify code only */
	function is_acc_active(){
		if(isset($this->acc_status) && $this->acc_status == 'active')
			return true;
		return false;
	}

	function want_bump() {
		if(!empty($this->list_id)){
			$list_id = $this->list_id;
		}else{
			$list_id = $this->raw_list_id;
		}
		$prefix =  bconf_get($BCONF, "*.redis_session.redis.prepend")."x";
		$redis_key_subject	= "rksubject";
		$redis_key_price	= "rkprice";
		$redis_key_image0	= "rkimage0";
		$redis_key_len_image	= "rklenimage";
		$expire = bconf_get($BCONF,"*.payment.redis_expiration.standard");
                $roptions = array(
                        'servers' =>  bconf_get($BCONF, "*.redis.session.host"),
                        'debug' => false,
                        'id_separator' => bconf_get($BCONF, "*.redis.session.id_separator"),
                        'timeout' => bconf_get($BCONF, "*.redis.session.connect_timeout")
                );
                $redis = new RedisSessionClient($roptions);
		$redis->set($prefix.$list_id."_".$redis_key_subject   , $this->ad->subject);
		$redis->expire($prefix.$list_id."_".$redis_key_subject,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_price     , $this->ad->price);
		$redis->expire($prefix.$list_id."_".$redis_key_price,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_image0    , @$this->images[0]);
		$redis->expire($prefix.$list_id."_".$redis_key_image0,$expire);
		$redis->set($prefix.$list_id."_".$redis_key_len_image , count($this->images));
		$redis->expire($prefix.$list_id."_".$redis_key_len_image,$expire);
		$redis->disconnect();
		$bumpUrl = bconf_get($BCONF, "*.common.base_url.payment") . '/pagos?id='.$list_id.'&prod=1&ftype=2&from=edit&want_bump='.$this->want_bump;
		header("Location: " . $bumpUrl);
		return;
	}

	function pay_upselling() {
		global $account_session;

		$action = (isset($this->action) && $this->action == "edit" ? "edit" : "newad");
		$paymentUrl = bconf_get($BCONF, "*.common.base_url.payment") . '/pagos?id=' . $this->ad_id . '&prod=' . $this->upselling_product . '&ftype=1&from=' . $action;
		if (isset($this->pack_result) && $this->pack_result >= 0) {
			$paymentUrl .= "&pr=" . $this->pack_result;
		}
		$account_status = $account_session->get_param('account_status');
		if (empty($account_status) && !empty($this->ai_create_account)) {
			$paymentUrl .= "&cc=1";
		}
		if (isset($this->upselling_product_label)) {
			$paymentUrl = $paymentUrl . '&lt=' . $this->upselling_product_label;
		}
		header("Location: " . $paymentUrl);
		return;
	}

	function pay_inserting_fee() {
		$paymentUrl = bconf_get($BCONF, "*.common.base_url.payment").'/pagos?id='.$this->ad_id.'&prod='.Products::INSERTING_FEE.'&from=newad';
		header("Location: " . $paymentUrl);
		return;
	}

	/**
	 * Check if reply has the product indicated
	 */
	function has_product($reply, $product){

		if (isset($reply['params'][$product]) || isset($reply['params']['upselling_'.$product])){
			return 1;
		}

		if(isset($reply['upselling_pending']) && $product != 'gallery'){
			$command = $reply['upselling_pending']['command'];
			if(!is_array($command))
				$command = array($command);

			foreach ($command as $cmd) {
				if($product == 'weekly_bump' && strpos($cmd,'daily_bump:1') === false){
					return 1;
				}
				if($product == 'daily_bump' && strpos($cmd,'daily_bump:1') !== false){
					return 1;
				}
			}

		}
		return 0;
	}

	function has_slots($account_session) {
		$is_pro = ($account_session->get_param('is_pro_for') != "" ? true : false );
		if ($is_pro && $account_session->get_param('pack_slots') == 0) {
			return false;
		}
		return true;
	}

	function is_duplicated() {
        global $account_session;
		$duplicated = new DuplicatedAdDesktop($account_session);
		$check_duplicated = $duplicated->shouldCheck($this->ad->category);
		// unless we force the ad to be "published anyway", we check always
		if ($check_duplicated) {
			$array_data = array();
			$array_data['email'] = $this->ad->email ;
			$array_data['subject'] = $this->ad->subject ;
			$array_data['body'] = $this->ad->body ;
			$array_data['category'] = $this->ad->category ;
			$array_data['price'] = $this->ad->price ;
			$array_data['type'] = $this->ad->type;
			$array_data['region'] = $this->ad->region;
			if (!empty($this->ad_id)) {
				$array_data['ad_id'] = $this->ad_id;
			}
			if (!empty($this->ad->currency)) {
				$array_data['currency'] = $this->ad->currency ;
			}
			$result = $duplicated->check($array_data);
			if (is_array($result)) {
				$this->set_errors($result);
				return true;
			}
		}
		return false;
	}

	/**
	 * Standarize phone to send info to transaction server
	 */
	function phone_std_for_trans(&$data){
		if($data['phone_type'] == "f"){
			$data['phone'] = $data['area_code'].$data['phone'];
		} else {
			$data['phone'] = "9".$data['phone'];
		}
		unset($data['area_code']);
		unset($data['phone_type']);
	}

	/**
	 * Standarize phone to send info to templates
	 */
	function complete_phone_params(&$info){
		if(!empty($info['phone']) && strlen($info['phone']) == 9) {
			$f_char_ad_phone = substr($info['phone'], 0, 1);
			if($f_char_ad_phone == "9"){
				$info['phone_type'] = "m";
				$info['area_code'] = "";
				$info['phone'] = substr($info['phone'],1,8);
			}
			else {
				$info['phone_type'] = "f";
				if($f_char_ad_phone == "2"){
					$info['area_code'] = "2";
					$info['phone'] = substr($info['phone'],1,8);
				}
				else {
					$f_char_ad_phone = substr($info['phone'], 0, 2);
					$info['area_code'] = $f_char_ad_phone;
					$info['phone'] = substr($info['phone'],2,7);
				}
			}
			$this->phone = $info['phone'];
			$this->area_code = $info['area_code'];
			$this->phone_type = $info['phone_type'];
		}
	}

}

$newad = new blocket_newad();
$newad->run_fsm();

