<?php
	if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');
	}

	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])){
			header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
		}
		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])){
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
		}
	}
	require_once('JSON.php');
	$utag_data = $_GET['utag_data'];
	$json_utag_data = json_encode($utag_data);
	$event_name = strtolower($utag_data['event_name']);

	$file_name = "../logs/hit_tealium." . $event_name;
	if(isset($_GET['type']) && $_GET['type'] == "click"){ # is a click hit
		file_put_contents($file_name . '.click.json.log', $json_utag_data);
	}else{ # is a page hit
		file_put_contents($file_name . '.page.json.log', $json_utag_data);
	}
?>
