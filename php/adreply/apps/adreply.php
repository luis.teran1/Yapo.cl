<?php
//require_once('init.php');

/*
 * Insurance application
 */
require_once('blocket_application.php');
require_once('dyn_config.php');
require_once('bTransaction.php');
require_once('bImage.php');

$script_list[] = "/js/common.js";


/*
 * Syslog
 */
openlog("ADREPLY", LOG_ODELAY, LOG_LOCAL0);

class blocket_ar extends blocket_application {
	var $page_title;
	var $page_name;
	var $head_styles;
	var $headscripts;
	var $search_result;
	var $ar_name;
	var $ar_email;
	var $ar_body;
	var $ar_phone;
	var $ar_cc;
	var $page;
	var $errors = array();
	var $list_id;
	var $data;
	var $meta_template = "ar/meta.html";

	function blocket_ar() {
		global $BCONF;
		global $session;
		global $script_list;

		$this->application_name = "ar";

		$this->page_title = lang('PAGE_AR_TITLE');
		$this->page_name = lang('PAGE_AR_NAME');
		$this->headstyles = array('ad_view.css');
		$this->headscripts = $script_list;
		$this->data = $_POST;
		$this->init('send',0);
	}

	/*** Now the entry point for the state machine is state send.  */
	/*   In case on an error, it will then go to state form.       */
	/*   If success, it will go to state sent.                   ***/
	function get_state($statename) {
		switch($statename) {
		case 'form':
			return array('function' => 'adreply_form',
				     'method' => 'get',
				     'allowed_states' => array('form', 'sent'));
		case 'send':
			return array('function' => 'adreply_send',
				     'params' => array('id' => 'f_clean', 'name' => 'f_clean', 'email' => 'f_clean', 'adreply_body' => 'f_clean', 'phone' => 'f_clean', 'cc' => 'f_clean'),
				     'method' => 'post');
		case 'sent':
			return array('function' => 'adreply_sent',
				     'method' => 'get',
				     'allowed_states' => array('sent'));
		}
	}

	function adreply_form() {
		global $BCONF;

		$list_id = $this->list_id;

		$result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:1 id:$list_id");
		if (empty($result['list_id'])) {
			$this->display_layout('list/view_missing.html', $result, null, null, 'html5_base');
			return 'FINISH';
		}

		if (bconf_get($BCONF, "*.cat." . $result['category'] . ".level") == "2")
			$cat_name = bconf_get($BCONF, "*.cat." . bconf_get($BCONF, "*.cat." . $result['category'] . ".parent") . ".name");
		else
			$cat_name = bconf_get($BCONF, "*.cat." . $result['category'] . ".name");

		$this->page_title = lang('PAGE_ADREPLY_FORM_TITLE');
		$this->page_name = bconf_get($BCONF, "*.common.region." . $result['region'] . ".name") . ", " . $cat_name . " " . bconf_get($BCONF, "*.common.type.list.{$result['type']}");

		$this->search_result = $result;
		if (!empty($result['regdate']))
			$result['regdate_short'] = "-" . substr($result['regdate'], 2, 2);
		if (!empty($this->errors))
			$result += $this->errors;

		$result['no_links'] = 1;
		
		if (!empty($result['image']))
			$images[] = $result['image'];

		if (!empty($result['extra_images']))
			$images = array_merge($images, explode(',', $result['extra_images']));

		if (!empty($images)) {
			foreach ($images as $key => $value) {
				$result['images'][$key] = substr($value, 0, 2) . '/' . $value;
			}
		}	

		# FT2740 -- ar/adreply template includes common/adview which expects a working 'id' 
		$result['id'] = $result['list_id'];
		# FT2984 -- ad type missing in template
		#$result['type'] = $result['ad_type'];
		if(!empty($this->page)){
			$result['has_error'] = 'true';
			unset($this->page);
		}

		$this->display_layout('ar/adreply.html', $result, null, null, 'html5_base');
	}

	function adreply_send($id, $name, $email, $adreply_body, $phone, $cc) {
		global $BCONF;

		// The data is submitted using post, but the init method assumes get is used, so the following is required when starting the state machine.
		if (isset($this->data) && empty($id) && empty($name) && empty($email) && empty($adreply_body) && empty($phone) && empty($cc)) {
			$id    = $this->data['id'];
			$name  = $this->data['name'];
			$email = $this->data['email'];
			$adreply_body = $this->data['adreply_body'];
			$phone = $this->data['phone'];
			$cc    = @$this->data['cc'];
		}

		$this->list_id = $id;
		$list_id = $this->list_id;

		$result = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:1 id:$id");
		if (empty($result['list_id'])) {
			return 'form';
		}

		syslog(LOG_INFO, log_string()."User posted message to ad {$list_id}");
		$transaction = new bTransaction();
		$transaction->add_client_info();
		$transaction->add_data('id', $list_id);
		$transaction->add_data('name', $name);
		$transaction->add_data('email', $email);
		if (!empty($phone))
			$transaction->add_data('phone', $phone);
		if (isset($cc) && $cc == "1")
			$transaction->add_data('cc', $cc);
		$transaction->add_data('adreply_body', clean_and_trim($adreply_body));

		/* Insert REDIR */
		if (isset($_COOKIE['redir'])) {
			$pos = strrpos($_COOKIE['redir'], '-');
			$site = substr($_COOKIE['redir'], 0, $pos);
			# since the value should be base64 encoded, we can safely remove anything after and including all dashes
			while ( ( $pos = strrpos($site, '-') ) ) {
				$site = substr($site, 0, $pos);
			}
			$transaction->add_data('redir_code', $site);
		}

		$transaction->add_data('log_string', log_string());
		$reply = $transaction->send_command("adreply");

		// Retrieve status
		@list ($status, $message) = explode(":", $reply['status']);
		syslog(LOG_INFO, log_string()."Retrieved status=$status $message");
		if ($status == 'TRANS_OK') {
			syslog(LOG_INFO, log_string()."SEND: To ad id: {$list_id} %% From: {$name} <{$email}> {$adreply_body} ({$_SERVER['REMOTE_ADDR']}, {$_SERVER['HTTP_USER_AGENT']})");
			$this->page = 'SENT';

			if (isset($this->search_result['store']))
				$parent = 'store_' . $this->search_result['store'];
			else
				$parent = NULL;

		} else if ($status == 'TRANS_ERROR') {
			if (in_array('ERROR_USER_BLOCKED', $reply)) {
				syslog(LOG_INFO, log_string()."BLOCK: To ad id: {$list_id} %% From: {$name} <{$email}> {$adreply_body} ({$_SERVER['REMOTE_ADDR']}, {$_SERVER['HTTP_USER_AGENT']})");
				$this->page = 'SENT';
			} else {
				syslog(LOG_INFO, log_string()."Transaction error for ad id {$this->search_result['list_id']} from {$email} ({$_SERVER['REMOTE_ADDR']})");
			}
		} else die_with_template(__FILE__, log_string()."Transaction handler exited with errorcode: $status");

		// Handle reply
		foreach ($reply as $key => $str) {
			// Split the string into code and message
			@list($error_code, $message) = explode(":", $str);
			$error_message = lang($error_code);

			// Ad not found in database
			if ($error_code == 'ERROR_AD_NOT_FOUND')
				syslog(LOG_INFO, log_string()."ERROR: Ad id {$list_id} doesn't exist in database");

			// Warnings/Errors
			if ($key != 'status' && $status == 'TRANS_ERROR' && (is_error($error_code) || is_warning($error_code)))
				$this->errors[(is_error($error_code)?"err_":"warn_").$key] = $error_message;
		}

		$this->ar_name = strip_tags($name);
		$this->ar_email = strip_tags($email);
		$this->ar_body = strip_tags($adreply_body);
		$this->ar_phone = strip_tags($phone);
		if (isset($cc) && $cc == "1")
			$this->ar_cc = "1";

		if ($this->page == 'SENT')
			return 'sent';
		else{
			$this->page= 'ERROR';
			return 'form';
		}
	}

	function adreply_sent() {
		global $BCONF;
		$data['sq'] = isset($_COOKIE['sq']) ? htmlspecialchars($_COOKIE['sq']):'';
		$this->page_title = lang('PAGE_ADREPLY_SENT_TITLE');
		$this->display_layout('ar/adreply_sent.html', $data, null, null, 'html5_base');
		return 'FINISH';
	}

	function f_clean($val) {
		$clean_val = strip_tags($val);
		return $clean_val;
	}
}

$adreply = new blocket_ar();
$adreply->run_fsm();

?>
