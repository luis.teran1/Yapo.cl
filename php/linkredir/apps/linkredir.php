<?php
/*
 * Link redirect application (e.g. redirect from bostadsannons to Hemnet)
 * Test with an ad having infopage set. To query statpoints do something like:
 * echo "0 lim:100 sort:0 event:CLICK" | nc localhost 23831
 */

require_once('blocket_application.php');

/*
 * Syslog
 */
openlog("LINKREDIR", LOG_ODELAY, LOG_LOCAL0);

class blocket_linkredir extends blocket_application {
	function blocket_linkredir() {
		$this->init('list', 0);
	}

	function get_state($statename) {
		switch($statename) {
		case 'list':
			return array('function' => 'linkredir_redir',
				     'params' => array('id' => 'f_integer', 'event' => 'f_event'),
				     'method' => 'get');
		}
	}

	function linkredir_redir($id, $event) {
		global $BCONF;
		$data = bsearch_search_vtree(bconf_get($BCONF, "*.common.asearch"), "0 lim:1 hdrs:1 id:$id");
		$event_id = bconf_get($BCONF, "linkredir.event.$event.id");
		$syslog = bconf_get($BCONF, "linkredir.event.$event.syslog");
		$column = bconf_get($BCONF, "linkredir.event.$event.column");
		$data['vi_link'] = bconf_get($BCONF, "*.common.base_url.vi") . "/vi/$id.htm&ca=" . $this->caller->to_string();

		if (@$data[$column]) {
			$redir_to = $data[$column]; /* Working id in regressenv teststores 8348563 */
			if (isset($data['store']) && $data['store']) {
				$parent = 'store_' . $data['store'];
			} else {
				$parent = NULL;
			}

			if($syslog) {
				$log_str = "redir: ".strtolower($event_id)." first_redir ".$_SERVER['REMOTE_ADDR']; // .($_SERVER['HTTP_REFERER'] ? ' '.$_SERVER['HTTP_REFERER'] : '');
				syslog(LOG_INFO, $log_str);
			}

			/* Send CLICK event with id + optional store to statpoints */
			stat_log(bconf_get($BCONF, "*.common.statpoints"), $event_id, $id, $parent);
			header("Location: $redir_to");

			return 'FINISH';
		} else {
			$error_page = 'common/error_page.html';
			$this->display_layout($error_page);
			syslog(LOG_WARNING, log_string()."No '$column' data found or invalid input $id, $event");
			return 'FINISH';
		}
	}

	function f_event($val) {
		if (in_array($val, array('info', 'gallery')))
			return $val;
		return 'info';
	}
}

$linkredir = new blocket_linkredir();
$linkredir->run_fsm();
?>
