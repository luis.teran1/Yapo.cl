<?php
require_once("blocket_soap_application.php");

class blocket_soap_test extends blocket_soap_application {
	function blocket_soap_test() {
		$this->init();
	}

	function get_state($name) {
		switch ($name) {
		case 'TEST':
			return array('function' => 'test',
					'params' => array('x' => 'f_integer'),
					'result' => array('value' => 'integer'));
		}
	}

	function get_states() {
		return array('test');
	}

	function test($x) {
		return $x;
	}
}

$app = new blocket_soap_test();
$app->run_fsm();

?>
