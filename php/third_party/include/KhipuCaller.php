<?php

namespace Yapo;


class KhipuCaller
{
    private $apiClient;

    public function __construct()
    {
        $confUrl = "*.common.base_url.payment_rest_api";
        $confMethods = "*.payment_rest_api_client.methods.";

        $this->apiClient = new ApiClient($confUrl, $confMethods, true);
    }
    /**
     * Get the payment from khipu servers using payment_rest api
     *
     * @param String $version Version of khipu to verify
     * @param String $token   Token to identify transaction
     *
     * @return JSON
     */
    private function khipuGetPayment($version, $token)
    {
        $params = "api_version={$version}&notification_token={$token}";
        $response = $this->apiClient->call("getKhipu", $params);
        return $response;
    }
    /**
     * Get the payment info from yapo data
     *
     * @param String $id Payment id (Payment_group_id)
     *
     * @return JSON
     */
    private function yapoGetPayment($id)
    {
        $params = "transaction_id={$id}";
        $response = $this->apiClient->call("getYapo", $params);
        return $response;
    }

    /**
     * Main function to handle notification
     *
     * @param String $version Version of khipu to verify
     * @param String $token   Token to identify transaction
     *
     * @return HTTP status code
     */
    public function khipuNotify($version, $token)
    {
        Logger::logDebug(__METHOD__, "Parameters found: {$version} - {$token}");
        $paymentKhipu = $this->khipuGetPayment($version, $token);
        if (empty($paymentKhipu)) {
            Logger::logError(__METHOD__, "Payment not found in khipu");
            header("HTTP/1.0 404 KhipuPayment Not Found");
            return;
        }

        $decodedKhipu = json_decode($paymentKhipu, true);
        $transaction_id = $decodedKhipu['transaction_id'];
        $kAmount = $decodedKhipu['amount'];
        $kStatus = $decodedKhipu['status'];

        if ($kStatus !== "done") {
            Logger::logError(
                __METHOD__,
                "Payment status is not done in khipu ({$transaction_id}, {$kStatus})"
            );
            header("HTTP/1.0 402 KhipuPayment Not done");
            return;
        }

        $paymentYapo = $this->yapoGetPayment($transaction_id);
        if (empty($paymentYapo)) {
            Logger::logError(__METHOD__, "Payment not found in Yapo {$transaction_id}");
            header("HTTP/1.0 400 YapoPayment Not Found");
            return;
        }

        $decodedYapo = json_decode($paymentYapo, true);
        $yStatus = $decodedYapo['purchase_status'];
        $yAmount = $decodedYapo['amount'];
        if ($yStatus !== "pending") {
            Logger::logError(
                __METHOD__,
                "Payment status is not pending in Yapo ({$transaction_id}, {$yStatus})"
            );
            header("HTTP/1.0 400 Payment already reviewed");
            return;
        }
        if (intval($yAmount) != intval($kAmount)) {
            Logger::logError(
                __METHOD__,
                "Payment price validation missmatch {$transaction_id}, {$yAmount}, {$kAmount}"
            );
            header("HTTP/1.0 400 Payment amount missmatch {$yAmount} vs {$kAmount}");
            return;
        }

        $confirmation = new ConfirmPayment();

        try {
            $confirmation->authorizeExecute(
                $transaction_id,
                0,
                0000,
                $decodedKhipu['payment_id']
            );
            Logger::logDebug(
                __METHOD__,
                "Payment notification succes for transaction id: {$transaction_id}"
            );
            echo "Notification success";
        } catch (Exception $e) {
            Logger::logDebug(
                __METHOD__,
                "Payment notification can not be authorized transaction id: {$transaction_id}"
            );
            Logger::logError(__METHOD__, $e->getMessage());
            echo "Notification can not be Authorized";
        }
    }
}
