<?php

require_once('autoload_lib.php');
require_once('util.php');
require_once('init.php');
require_once('TransbankConfirmation.php');

openlog("PAYMENT", LOG_ODELAY, LOG_LOCAL0);

use Yapo\ConfirmPayment;

class blocket_transbank extends blocket_application {
	var $purchase_order;
	var $price;
	var $state;
	var $ad_id;
	var $product_id;
	var $purchase_id;
	var $action_id;
	var $pay_log_status;

	function blocket_transbank() {
		$this->init('authorize', -1);
	}

	function get_state($statename) {
		switch($statename) {
			case 'authorize':
				return array('function' => 'transbank_authorize',
					'params' => array(
						'TBK_ACCION', 'TBK_ORDEN_COMPRA', 'TBK_CODIGO_COMERCIO', 'TBK_CODIGO_COMERCIO_ENC',
						'TBK_TIPO_TRANSACCION', 'TBK_RESPUESTA', 'TBK_MONTO', 'TBK_CODIGO_AUTORIZACION', 'TBK_FINAL_NUMERO_TARJETA',
						'TBK_FECHA_CONTABLE', 'TBK_FECHA_TRANSACCION', 'TBK_FECHA_EXPIRACION', 'TBK_HORA_TRANSACCION', 'TBK_ID_SESION',
						'TBK_ID_TRANSACCION', 'TBK_TIPO_PAGO', 'TBK_NUMERO_CUOTAS', 'TBK_VCI', 'TBK_MAC'
					),
					'method' => 'post');
		}
	}

	function transbank_authorize(
		$TBK_ACCION, $TBK_ORDEN_COMPRA, $TBK_CODIGO_COMERCIO, $TBK_CODIGO_COMERCIO_ENC, $TBK_TIPO_TRANSACCION,
		$TBK_RESPUESTA, $TBK_MONTO, $TBK_CODIGO_AUTORIZACION, $TBK_FINAL_NUMERO_TARJETA, $TBK_FECHA_CONTABLE,
		$TBK_FECHA_TRANSACCION, $TBK_FECHA_EXPIRACION, $TBK_HORA_TRANSACCION, $TBK_ID_SESION, $TBK_ID_TRANSACCION,
		$TBK_TIPO_PAGO, $TBK_NUMERO_CUOTAS, $TBK_VCI, $TBK_MAC
	) {
		global $BCONF;
		if (!payment_enabled()) {
			exit();
		}
		bLogger::logDebug(__METHOD__, "Starting payment id: ".var_export($TBK_ORDEN_COMPRA, true));

		$elements = array(
			"TBK_ID_SESION",
			"TBK_ACCION",
			"TBK_ORDEN_COMPRA",
			"TBK_CODIGO_COMERCIO",
			"TBK_CODIGO_COMERCIO_ENC",
			"TBK_TIPO_TRANSACCION",
			"TBK_RESPUESTA",
			"TBK_MONTO",
			"TBK_CODIGO_AUTORIZACION",
			"TBK_FECHA_CONTABLE",
			"TBK_FECHA_TRANSACCION",
			"TBK_FECHA_EXPIRACION",
			"TBK_HORA_TRANSACCION",
			"TBK_ID_TRANSACCION",
			"TBK_FINAL_NUMERO_TARJETA",
			"TBK_TIPO_PAGO",
			"TBK_NUMERO_CUOTAS",
			"TBK_VCI",
			"TBK_MAC"
		);

		$tbkData = array();
		foreach ($elements as $key) {
			$tbkData[$key] = $$key;
		}

		bLogger::logDebug(__METHOD__, "TBK_INFO: ".var_export($tbkData, true));

		$TbkConfirmation = new TransbankConfirmation();
		$result = $TbkConfirmation->authorize((object)$tbkData);
		bLogger::logDebug($TBK_ORDEN_COMPRA, "Tbk result".var_export($result,true));
		if($result->status == 'authorized') {
			$confirmation = new ConfirmPayment();
			$clear_status = $confirmation->validatePayment($TBK_ORDEN_COMPRA);
			if($clear_status == 'CLEAR_SUCCESS'){
				$apply_products = $confirmation->applyProducts($TBK_ORDEN_COMPRA);
				if($apply_products['status'] == 'AUTHORIZED') {
					echo ("<html>ACEPTADO</html>");
					flush();
					exit();
				}
			}
		}
		echo ("<html>RECHAZADO</html>");
		flush();
		exit();
	}

	/* Validations of parameters from transbank */
	function f_TBK_ACCION                   ($val) { return strip_tags($val); }
	function f_TBK_ORDEN_COMPRA             ($val) { return strip_tags($val); }
	function f_TBK_CODIGO_COMERCIO          ($val) { return strip_tags($val); }
	function f_TBK_CODIGO_COMERCIO_ENC      ($val) { return strip_tags($val); }
	function f_TBK_TIPO_TRANSACCION         ($val) { return strip_tags($val); }
	function f_TBK_RESPUESTA                ($val) { return strip_tags($val); }
	function f_TBK_MONTO                    ($val) { return strip_tags($val); }
	function f_TBK_CODIGO_AUTORIZACION      ($val) { return strip_tags($val); }
	function f_TBK_FINAL_NUMERO_TARJETA     ($val) { return strip_tags($val); }
	function f_TBK_FECHA_CONTABLE           ($val) { return strip_tags($val); }
	/* This value come from transbank in format mmdd (Month Day, four digits in total)  */
	function f_TBK_FECHA_TRANSACCION        ($val) { return strip_tags($val); }
	function f_TBK_FECHA_EXPIRACION         ($val) { return strip_tags($val); }
	function f_TBK_HORA_TRANSACCION         ($val) { return strip_tags($val); }
	function f_TBK_ID_SESION                ($val) { return strip_tags($val); }
	function f_TBK_ID_TRANSACCION           ($val) { return strip_tags($val); }
	function f_TBK_TIPO_PAGO                ($val) { return strip_tags($val); }
	function f_TBK_NUMERO_CUOTAS            ($val) { return strip_tags($val); }
	function f_TBK_VCI                      ($val) { return strip_tags($val); }
	function f_TBK_MAC                      ($val) { return strip_tags($val); }
}

$transbank = new blocket_transbank();
$transbank->run_fsm();

