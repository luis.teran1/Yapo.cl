<?php

require_once('autoload_lib.php');
require_once('util.php');
require_once('ServipagConfirmation.php');

openlog("PAYMENT", LOG_ODELAY, LOG_LOCAL0);

class blocket_servipag extends blocket_application {
	var $purchase_order;
	var $price;
	var $state;
	var $ad_id;
	var $product_id;
	var $purchase_id;
	var $action_id;
	var $pay_log_status;

	function blocket_servipag() {
		$this->init('authorize', -1);
	}

	function get_state($statename) {
		switch($statename) {
			case 'authorize':
				return array('function' => 'servipag_authorize',
					'params' => array('xml'),
					'method' => 'post');
		}
	}

	function servipag_authorize($xml) {
		global $BCONF;
		if (!payment_enabled()) {
			$this->tbk_output(false);
			exit();
		}

		$svpConfirmation = new ServipagConfirmation();
		$result = $svpConfirmation->authorize($xml);
		bLogger::logNotice(__METHOD__, "response for xml = " . $xml .  " from sendCallback " . var_export($result,true));
		//echo stripcslashes(substr($result, 1, -1));//remove start and end ", remove \n and slashes
		echo stripcslashes($result);//remove start and end ", remove \n and slashes
	    flush();
		exit();
	}


	/* Validations of parameters from transbank
	  TODO Implement logic of validations */
	function f_xml	($val) { return $val; }
}

$servipag = new blocket_servipag();
$servipag->run_fsm();

