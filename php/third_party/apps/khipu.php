<?php

namespace Yapo;

/**
 * File used to receive khipu notifications
 *
 * PHP version 5
 *
 * @category Khipu
 * @package  BlocketKhipuPayment
 * @author   Dany Jaque <dany@schibsted.cl>
 * @license  Private http://www.yapo.cl
 * @link     http://www.yapo.cl
 */
require_once 'autoload_lib.php';

require_once 'KhipuCaller.php';
require_once 'init.php';

openlog("payment_khipu", LOG_ODELAY, LOG_LOCAL0);

Logger::logDebug("third_party_khipu", var_export($_REQUEST, true));
if (isset($_REQUEST['api_version']) && isset($_REQUEST['notification_token'])) {
    $api_version = $_REQUEST['api_version'];
    $notification_token = $_REQUEST['notification_token'];
    $khipuCaller = new KhipuCaller();
    $khipuCaller->khipuNotify($api_version, $notification_token);
} else {
    Logger::logError("Khipu notification", "missing parameters");
}
