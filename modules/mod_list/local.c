#include "mod_list.h"
#include "bsapi.h"
#include "bconf.h"
#include "cookies.h"
#include "ais.h"

#include <ctype.h>
#include <apr_strings.h>

#define WHERE_STATE	6

static char *friendlyurl_converter_local(request_rec *r, struct list_conf *conf);
static int failover_search_local(struct list_call *b, int limit, struct bsconf *search_conf);
static void set_filters_local(struct list_call *b);
static void set_result_navigation_local(struct list_call *b);

int
setup_list_local(apr_pool_t *pool, struct list_conf *conf, struct bconf_node *root) {
	mod_list_friendlyurl_converter = friendlyurl_converter_local;
	mod_list_failover_search = failover_search_local;
	mod_list_set_filters = set_filters_local;
	mod_list_set_result_navigation = set_result_navigation_local;
	return OK;
}




static int
handle_ca(struct list_call *call, const char *ca) {
	char reg[12];
	char typ[2];
	int region = 0;
	char type = 0;

	sscanf(ca, "%d_%c", &region, &type);

	/* Validate region */
	if (region) {
		snprintf(reg, sizeof (reg), "%d", region);
		if (!bconf_vget(call->conf->root, "common.region", reg, NULL))
			region = 0;
	}

	/* Validate type */
	if (type) {
		typ[0] = type;
		typ[1] = '\0';
		if (!bconf_vget(call->conf->root, "common.type.list", typ, NULL))
			type = 's';
	}

	if (region) {
		if (!type)
			type = 's';

		bpapi_set_printf(&call->bparse, "ca", "%d_%c", region, type);
		bpapi_set_int(&call->bparse, "ca_region", region);
		bpapi_set_char(&call->bparse, "ca_type", type);
		return 1;
	}
	return 0;
}

static void
configure_page_counter(struct list_call *call, const char *mode) {
	const char *blocket_counter = get_cookie(call->r, "stat_counter");
	struct tm tm;
	int daystomonday;

	localtime_r(&call->start_time.tv_sec, &tm);
	daystomonday = (8 - tm.tm_wday) % 7 ?: 7;

	if (!blocket_counter)
		set_cookie_with_expiry(call->r, "stat_counter", "1", bconf_get_string(call->conf->root, "common.cookie.domain"), -daystomonday, 0);
}

void
list_start_call_local(struct list_call *call, int res) {
	const char *w;

	/* Backward compat "layout" */
	bpapi_insert(&call->bparse, "l", "0");

	/* Caller */
	if (!bpapi_has_key(&call->bparse, "ca")) {
		const char *default_ca = get_cookie(call->r, "default_ca");
		int set = 0;

		if (default_ca)
			set = handle_ca(call, default_ca);
		if (!set) {
			default_ca = bconf_get_string(call->conf->root, "common.default.caller");
			if (default_ca)
				handle_ca(call, default_ca);
		}
	}

	/* "reg" variable used by searchbox */
	if ((w = bpapi_get_element(&call->bparse, "w", 0)) && atoi(w) > 100) {
		bpapi_set_int(&call->bparse, "reg", atoi(w) - 100);
	} else
		bpapi_insert(&call->bparse, "reg", bpapi_get_element(&call->bparse, "ca_region", 0));

	/* "f" variable used for private / company it has to be ALWAYS set */
	if(!bpapi_has_key(&call->bparse, "f"))
		bpapi_set_char(&call->bparse, "f", 'a');
	
	configure_page_counter(call, call->mode);
}

void
parse_qs_cb_local(struct list_call *call, enum query_string_var qsv, char *key, char *value, int *insert_bparse, int *append_qs) {
	switch (qsv) {
	case QSV_CA:
		handle_ca(call, value);
		break;
	default:
		break;
	}
}

void
set_company_query_local(struct list_call *call) {
	const char *w = bpapi_get_element(&call->bparse, "w", 0);
	const char *reg = bpapi_get_element(&call->bparse, "ca_region", 0);
	const char *city = bpapi_get_element(&call->bparse, "ca_city", 0);
	int wint = w ? atoi(w) : 0;

	if (reg && reg[0]) {
		if (wint == 1 || (wint == 0 && (!city || !city[0])))
			call->query = apr_psprintf(call->r->pool, "%s c_region:%s", call->query, reg);
		else if (wint == 0)
			call->query = apr_psprintf(call->r->pool, "%s c_region:%s c_city:%s", call->query, reg, city);
	}
}


/*
 *  Setup helper variables for the filter (the region/neighborhood/allcountry and all/private/company tab)
 */
static void
set_filters_local(struct list_call *b) {
	int region_count = 0;
	int failover_level;
	const char *w = bpapi_get_element(&b->bparse, "w", 0);
	const char *f = NULL;
	const char *tab = NULL;
	int private_count = 0;
	int company_count = 0;

	failover_level = bpapi_get_int(&b->bparse, "search_failover_where");

	if (failover_level == 3 ) {
		bpapi_insert(&b->bparse, "tab", "country_tab");
	} else {
		/* Set the tab selector variable */
		switch (w ? *w : 0) {
			case '3':
				bpapi_insert(&b->bparse, "tab", "country_tab");
				break;
			default:
				region_count = bpapi_get_int(&b->bparse, "region_count");
				if (region_count > 0) {
					bpapi_insert(&b->bparse, "tab", "region_tab");
				} else {
					bpapi_insert(&b->bparse, "tab", "country_tab");
				}
				break;
		}
	}

	/* "f" variable used for private / company it has to be ALWAYS set */
	f = bpapi_get_element(&b->bparse, "f", 0);
	tab = bpapi_get_element(&b->bparse, "tab", 0);
	private_count = atoi(bpapi_get_element(&b->bparse, tab, 0) ?: "0");
	company_count = atoi(bpapi_get_element(&b->bparse, "c_company", 0) ?: "0");

	switch (f ? *f : 0) {
		case 'p':
			/* No need to substract here */
			bpapi_set_int(&b->bparse, "private_count", private_count); 
			bpapi_insert(&b->bparse, "company_count", "0");
			break;
		case 'c':
			bpapi_insert(&b->bparse, "private_count", "0");
			bpapi_set_int(&b->bparse, "company_count", company_count);
			break;
		default:
			if (bpapi_has_key(&b->bparse, "f"))
				bpapi_remove(&b->bparse, "f");
			bpapi_insert(&b->bparse, "f", "a");
			bpapi_set_int(&b->bparse, "private_count", ( private_count > company_count ) ? private_count - company_count : 0);
			bpapi_set_int(&b->bparse, "company_count", company_count);
			break;
	}
}


/*
 * Puts thousand separator in "num"
 */
static void
format_number(char* res, int num) {
	char *num_str = xmalloc(10);
	int i = 0;
	int ri = 0;
	snprintf(num_str, 10, "%d", num);
	int num_len = strlen(num_str);

	while (num_str[i]) {
		if (i && (i % 3 == (num_len % 3)) )
			res[ri++] = '.';
		res[ri++] = num_str[i++];
	}
	res[ri] = '\0';
	free(num_str);
}


/*
 *  Setup helper variables for the result navigation (first, previous, next, last and the 1, 2, 3 ...)
 */
static void
set_result_navigation_local(struct list_call *b) {
	int region_count = 0, country_count = 0, filtered = 0, unfiltered = 0;
	int pages = 0, max_pages = 0, start_page = 0, end_page = 0, current_page = 0;
	int first_ad = 0, last_ad = 0, i = 0;

	/* Set helper variables that indicates wheather prices and images exists in the result */
	bpapi_set_int(&b->bparse, "has_prices", b->has_prices);

	/* Do not set the has_images variable for the company-ad-list (always thumbnails) */
	if (b->company_id == 0) {
		bpapi_set_int(&b->bparse, "has_images", b->has_images);
	}	

	if (bpapi_get_element(&b->bparse, "region_tab", 0) != NULL) {
		region_count = atoi(bpapi_get_element(&b->bparse, "region_tab", 0));
		if (region_count) bpapi_set_int(&b->bparse, "region_count", region_count);
	}

	if (bpapi_get_element(&b->bparse, "country_tab", 0) != NULL) {
		country_count = atoi(bpapi_get_element(&b->bparse, "country_tab", 0));
		if (country_count) bpapi_set_int(&b->bparse, "country_count", country_count);
	}

	unfiltered = bpapi_get_int(&b->bparse, "unfiltered");

	filtered = bpapi_get_int(&b->bparse, "filtered");

	bpapi_set_int(&b->bparse, "total", filtered);

	/* Log search hits */
	if (0 < unfiltered && unfiltered <= bconf_get_int(b->conf->root, "common.log.searchhits.maxhits") &&
	    bconf_get_int(b->conf->root, "common.log.searchhits.activated")) {
		char *log_string = NULL;
		log_string = apr_psprintf(b->r->pool, "SEARCHHITS: Got search with %d hits (logging up to %d hits)", unfiltered, bconf_get_int(b->conf->root, "common.log.searchhits.maxhits"));
		apr_table_add(b->r->notes, "searchhits_log", log_string);
	}
        
	/* Total is equal to the total number of ads for the current filter (region based)  */
	if (unfiltered < 1)
		return;

	/* Calculate the total number of pages in the result */
	if (filtered == b->ads_per_page)
		pages = (filtered / b->ads_per_page);
	else
		pages = (filtered / b->ads_per_page) + ((filtered % b->ads_per_page) ? 1 : 0);
	
	current_page = b->offset / b->ads_per_page + 1;
	
	/* Calculate the maximum number of page-links */
	if (current_page > 1000 - 5)
		max_pages = 10;
	else if (current_page > 100 - 20)
		max_pages = 12;
	else
		max_pages = 20;

	/* Check which navigation selections that are necessary */
	if (pages > 1) {
		/* Set first and previous */
		if (current_page != 1)
			bpapi_set_int(&b->bparse, "nav_prev_offset", current_page - 1);		

		bpapi_set_int(&b->bparse, "nav_total_pages", pages);

		/* Set last and next*/
		if (current_page == pages) {
			bpapi_set_int(&b->bparse, "nav_first_offset", 1);
		} else {
			bpapi_set_int(&b->bparse, "nav_last_offset", pages);
			bpapi_set_int(&b->bparse, "nav_next_offset", current_page + 1);
		}

		/* Page scrolling does not start until we reach max_pages */
		if (current_page < max_pages) {
			start_page = 1;
			end_page = max_pages > pages ? pages : max_pages;
		} else {
			if (current_page - 1 + max_pages <= pages) {
				start_page = current_page - 1;
				end_page = start_page + max_pages - 1;
			} else {
				end_page = pages;
				start_page = (end_page - max_pages > 1) ? end_page - max_pages + 1 : 1;
			}
		}

		for (i = start_page; i <= end_page; i++) {
			bpapi_set_int(&b->bparse, "nav_offset", i);
			bpapi_set_int(&b->bparse, "nav_selected", (i == current_page) ? 1 : 0);
		}
	}

	/* Calculate the ad span in the current page (for example: 1 - 100) */
	first_ad = b->offset + 1;
	last_ad = current_page * b->ads_per_page;

	if (last_ad > filtered)
		last_ad = filtered;

	/* Just to avoid printing out "501 - 600 out of 23" for errornous offset */
	if (first_ad <= filtered) {
		/* FT 337: number for page range must have thousand separators */
		char* form_firstad = malloc(13);
		char* form_lastad = malloc(13);
		format_number(form_firstad, first_ad);
		format_number(form_lastad, last_ad);
		bpapi_insert(&b->bparse, "list_span", apr_psprintf(b->r->pool, "%s - %s", form_firstad, form_lastad));
		free(form_firstad);
		free(form_lastad);
	}

}


/*
 * Add f=a in the args in case that is not set before calling 
 * the search_request_handle, so that all necesary filters and
 * params will be set
 */
static char *
company_var_local(request_rec *r, char *replace_args) {
	char *var = NULL;
	char *strtok_state = NULL;
	char *qs = apr_pstrdup(r->pool, r->args);

	if (qs) {
		while ((var = strtok_r(qs, "&", &strtok_state))) {
			if (strncmp(var, "f=", 2) == 0)
				return replace_args;
			qs = NULL;
		}
	}

	return apr_psprintf(r->pool, "%s&f=a", replace_args ? replace_args : "");
}

char *
replace_list_vars_local(struct list_call *b, request_rec *r, char *replace_args) {
	char * new_replace_args = replace_args;

	if (replace_args)
		bpapi_insert(&b->bparse, "REPLACE_ARGS", replace_args);

//	bpapi_insert(&b->bparse, "original_qs", replace_args);

	//new_replace_args = type_var_local(b, r, new_replace_args);
	new_replace_args = company_var_local(r, new_replace_args);
	//new_replace_args = swap_range_vars_local(r, new_replace_args);

	return new_replace_args;
}


/* Loop through category, category parent, all categories */
/* Between category and all categories discard type if exists */
/* And check ads in region,entire  country */
static int
failover_search_local(struct list_call *b, int limit, struct bsconf *search_conf) {
	int where = WHERE_UNSET;
	const char *w = bpapi_get_element(&b->bparse, "w", 0);
	char *query_string = NULL;
	char *q = NULL;
	char* strtok_state = NULL;
	char *var = NULL;
	int category;
	int total = 0;
	int catlevel;
	int catparent;
	int original_where;
	int original_n_results;
	int expanded_filters = 0;
	char *f;
	const char *original_f;

	original_f = bpapi_get_element(&b->bparse, "f", 0) ?: "";
	category = b->category;
	if (!bconf_get_string(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.name", category))){
		category = 0;
	}

	catlevel = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.level", category));
	catparent = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.parent", category));

	if (w && *w)
		where = atoi(w);

	original_where = where;

	q = apr_psprintf(b->r->pool, "%s", b->args);
	f = apr_psprintf(b->r->pool, "%s", original_f);

	while ((var = strtok_r(q, "&", &strtok_state))) {
		if (strncmp(var, "w=", 2) != 0 &&
		    strncmp(var, "cg=", 3) != 0 &&
		    strncmp(var, "c=", 2) != 0 && 
			strncmp(var, "f=", 2) != 0) {
			if (query_string) 
				query_string = apr_psprintf(b->r->pool, "%s&%s", query_string , var);
			else		
				query_string = apr_psprintf(b->r->pool, "%s", var);
		}
		q = NULL;
	}

	original_n_results = -1;
	while (1) {
		if (expanded_filters == 0 && category != 0) { //just need to expand filter when have a selected category
			//check for results
			if (catlevel == 2)
				q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&c=%d&f=%s", query_string, where, catparent, category, f);
			else
				q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&f=%s", query_string, where, category, f);

			set_search_query(b, q);
			total = check_search_query(b, search_conf);
			if (original_n_results < 0)
				original_n_results = total;	

			if (total >= limit)
				break;
			
			//expand the filters to try to get more results
			expanded_filters = 1;

			if (query_string) {
				char *new_qs = NULL;
				q = xstrdup(query_string);

				while ((var = strtok_r(q, "&", &strtok_state))) {
					if (strncmp(var, "ca=", 3) == 0 ||
						strncmp(var, "q=", 2) == 0 ||
						strncmp(var, "o=", 2) == 0)
						new_qs = apr_psprintf(b->r->pool, "%s&%s", new_qs ? new_qs : "", var);
					q = NULL;
				}
				if (new_qs) {
					query_string = xstrdup(new_qs);
					new_qs = NULL;
				}
			}
			continue;// We don't want to change the category yet
	
		}
		else if (where != WHERE_ALL) {
			while (1) {
				/* Check for results */
				if (catlevel == 2)
					q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&c=%d&f=%s", query_string, where, catparent, category, f);
				else
					q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&f=%s", query_string, where, category, f);

				set_search_query(b, q);
				total = check_search_query(b, search_conf);
				if(original_n_results < 0)
					original_n_results = total;

				if (total >= limit || where == WHERE_ALL)
					break;

				if (strcmp(f, "a") != 0 && where == original_where) {
					f = apr_psprintf(b->r->pool, "a");
				} else {
					switch (where) {
						case WHERE_UNSET:
						case WHERE_IN_REGION:
							where = WHERE_ALL;
							break;
						default:
							where = WHERE_IN_REGION;
					}
				}
			}
		} else {
			if (catlevel == 2)
				q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&c=%d&f=%s", query_string, where, catparent, category, f);
			else
				q = apr_psprintf(b->r->pool, "%s&w=%d&cg=%d&f=%s", query_string, where, category, f);

			set_search_query(b, q);
			total = check_search_query(b, search_conf);
			
			if(original_n_results < 0)
				original_n_results = total;
		}

		/* 
		 * If no result first check for parent category 
		 * then check if "f" is not all
		 */
		if (category != 0 && total < limit) {
			if (catlevel == 1 && strcmp(f, "a") != 0) { 
				/* All types */
				f = apr_psprintf(b->r->pool, "a");
			} else if (catlevel > 0) {
				/* Get parent */
				category = catparent;
				catparent = bconf_get_int(b->conf->root, apr_psprintf(b->r->pool, "cat.%d.parent", category));
				catlevel--;
		 	} else {
				/* All categories */
				category = 0;
			}
			where = original_where;
		} else if (category == 0 && total < limit && strcmp(f, "a") != 0 ) {
			f = apr_psprintf(b->r->pool, "a");
		} else {
			break;
		}
	}

	/* See also if there has been category and / or type failover */
	if (strcmp(original_f, f) && total > 0) {
		bpapi_set_int(&b->bparse, "search_failover_f", 1);

		/* Here we need to get the set &f=a and also set the counters */
		bpapi_remove(&b->bparse, "f");
		bpapi_insert(&b->bparse, "f", "a");
	}

	if (b->category != category && total > 0) {
		bpapi_set_int(&b->bparse, "search_failover_category", category);
	} 
	
	if (where != (w ? atoi(w) : 0) && total > 0) {
		bpapi_set_int(&b->bparse, "search_failover_where", where);
	}
	
	if(expanded_filters > 0 && total > 0)
		bpapi_set_int(&b->bparse, "search_failover_filters", 1);

	/* We do this check here in order not to change platform code */
	if (limit == bconf_get_int(b->conf->root, "failover.index"))
		bpapi_set_int(&b->bparse, "search_failover_from_index", 1);

	/* MOD_LIST in platform sets this cookie before the failover... */
	const char *cookiedomain = bconf_get_string(b->conf->root, "common.cookie.domain");
	if (cookiedomain && !*cookiedomain)
		cookiedomain = NULL;

	clear_cookie(b->r, "sq", cookiedomain);
	set_cookie(b->r, "sq", q, cookiedomain, 0);

	/*Setting the catparent failover here to remove it from template code.*/
	{
		int has_fo_category	 = bpapi_has_key(&b->bparse, "search_failover_category");
		int has_fo_where	 = bpapi_has_key(&b->bparse, "search_failover_where");
		int has_fo_f		 = bpapi_has_key(&b->bparse, "search_failover_f");
		int has_fo_filters	 = bpapi_has_key(&b->bparse, "search_failover_fiters");

		if(total != 0 && (has_fo_category || has_fo_where || has_fo_f || has_fo_filters)) {
			if(has_fo_category && bpapi_get_int(&b->bparse, "search_failover_category") == 0) {
				bpapi_set_int(&b->bparse, "search_failover_catparent",0);
			} else if(has_fo_category) {
				bpapi_set_int(&b->bparse, "search_failover_catparent",1);
			}
		}

		/* Helper variable so that we will not overwrite sq cookie in Mod_list */
		if (has_fo_category || has_fo_filters || has_fo_f || has_fo_where){
			bpapi_set_int(&b->bparse, "has_failover", 1);
		}
	}

	if (original_n_results < bconf_get_int(b->conf->root, "ais.threshold")) {
		ais(b,original_n_results, search_conf);
	}

	return 0;
}

static const char *
get_uri_region(struct list_conf *conf, char *uri, int *where) {
	const char *region = NULL;
	struct bconf_node *nearby_node = NULL;
	struct bconf_node *node = NULL;

	if (uri && strlen(uri) > 1) {
		uri[0] = tolower(uri[0]);
		if ((node = bconf_vget(conf->root, "common", "seo", "region", "number", uri, NULL)))  {
			region = bconf_value(node);
			*where = 1;

		} else if ((node = bconf_vget(conf->root, "common", "seo", "city", "number", uri, NULL))) {
			region = bconf_value(node);
			*where = 0;

		} else if ( (nearby_node = bconf_vget(conf->root, "common", "seo", "nearby", "text", NULL)) ) {
			int i;
			const char *nearby_bconf = NULL;
			for (i = 0; ((nearby_bconf = bconf_value(bconf_byindex(nearby_node, i)))); i++) {
				const char *nearby = strstr(uri, nearby_bconf);
				if (nearby) {
					char *region_name = xstrndup(uri, nearby - uri - 1);
					if (!(node = bconf_vget(conf->root, "common", "seo", "region", "number", region_name, NULL)))
						node = bconf_vget(conf->root, "common", "seo", "city", "number", region_name, NULL);
					free(region_name);

					if (node) {
						region = bconf_value(node);
						*where = 2;
						break;
					}
				}
			}
		}
		if (!region && (node = bconf_vget(conf->root, "common", "seo", "allcountry", uri, "default", NULL)) ) {
			region = bconf_value(node);
			*where = 3;
		}
	}

	return region;
}

static const char *
get_uri_category(struct list_conf *conf, char *uri, const char *category_group) {
	const char *category;
	struct bconf_node *node = NULL;

	if (uri && strlen(uri) > 1) {
		uri[0] = tolower(uri[0]);
		if (category_group) {
			node = bconf_vget(conf->root, "common", "seo", "subcat", "number", category_group, uri, NULL);
		} else {
			node = bconf_vget(conf->root, "common", "seo", "cat", "number", uri, NULL);
		}

		if (node && (category = bconf_value(node)))
			return category;
	}

	return NULL;
}


/*
 *   Translate uri from new and friendly style to old li style
 */
char *
friendlyurl_converter_local(request_rec *r, struct list_conf *conf) {
	/*Jump over first slash */ 
	char *uri = apr_pstrdup(r->pool, (r->uri) + 1);	
	char *token = NULL;
	char *tok_region = NULL;

	token = strtok_r(uri, "/", &tok_region);
	if (token) {
		const char *region = NULL;
		int where = 0;
		region = get_uri_region(conf, token, &where);

		char *qs = apr_pstrdup(r->pool, r->args);
		if (region) {
			char *category = NULL;
			char *subcategory = NULL;

			token = strtok_r(NULL, "/", &tok_region);
			category = (char *) get_uri_category(conf, token, NULL);
			if ( (token = strtok_r(NULL, "/", &tok_region)) && category)
				subcategory = (char *)get_uri_category(conf, token, category); 

			/* append parameters if don't exist */
			if (!r->args || (r->args && !strstr(r->args, "ca=")))
				qs = apr_psprintf(r->pool, "%s&ca=%s_s", qs? qs : "" , region);

			if (where && (!r->args || (r->args && !strstr(r->args,"w=")))) 
				qs = apr_psprintf(r->pool, "%s&w=%d", qs? qs : "" , where);

			if (category != NULL && (!r->args || (r->args && category && !strstr(r->args,"cg="))))
				qs = apr_psprintf(r->pool, "%s&cg=%s", qs? qs : "" , category);
			else if (category == NULL && ((!r->args || (r->args && !strstr(r->args,"cg=")))))
				qs = apr_psprintf(r->pool, "%s&cg=0", qs? qs : "");

			if (subcategory != NULL && (!r->args || (r->args && subcategory && !strstr(r->args,"c="))))
				qs = apr_psprintf(r->pool, "%s&c=%s", qs? qs : "" , subcategory);

			if (qs[0] == '&')
				return qs + 1;
		}
		return qs;
	}
	return NULL;
}


