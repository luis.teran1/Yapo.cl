#include "mod_list.h"
#include "bsapi.h"
#include "bpapi.h"
#include "bconf.h"
#include "ais.h"
#include <apr_strings.h>
#include <ctype.h>

#include "http_log.h"
#include <search.h>

/* Defines for transformation functions */
#define TRANSFORMATION_ERROR -1
#define NO_MORE_RESULTS	0
#define MORE_RESULTS	1

/* Data type for the tree */
struct result_t {
	struct list_call *b;
	char *query_string;
	char *query;
	char *description;
	int number_of_results;
	int used_transformations;
};

/* Lookup table containing correspondence between a transformation name and its implementation*/
struct lookup_table {
	const char *function_name;
	int (*transformation_function)(struct result_t *result, int current_iteration, char transf_index);
};

int synonyms(struct result_t *result, int current_iteration, char transf_index);
int misspellings(struct result_t *result, int current_iteration, char transf_index);
int geography(struct result_t *result, int current_iteration, char transf_index);
int category_words(struct result_t *result, int current_iteration, char transf_index);
int plural(struct result_t *result, int current_iteration, char transf_index);
int remove_stopwords(struct result_t *result, int current_iteration, char transf_index);
struct lookup_table transformations[] = {
	{"synonyms", synonyms},
	{"misspellings", misspellings},
	{"geography", geography},
	{"category_words", category_words},
	{"plural", plural},
	{"remove_stopwords", remove_stopwords},
	{NULL}
};

struct result_t *copy_result(struct result_t *original) ;

int perform_transformation(struct bconf_node *filter, int transformation, struct result_t *result, int current_iteration) ;
int compare(const void *pa, const void *pb);

void swap_qs_params(struct result_t *result, const char *key,const char *new_value);
void perform_recursive(int max_depth, int current_depth, struct bconf_node *filter, void **tree, struct result_t *result, struct bsconf *search_conf);
void dummy_free(void *p);
void insert_element(struct result_t *result, void **tree, struct bsconf *search_conf);
void add_template_vars(const void *nodep, const VISIT which, const int depth);

char *get_qs_param(struct result_t *result, const char *qs_key) ;
char *find_qs_param(const char *haystack, const char *needle);

void update_transformations(struct result_t *result, char transf_index);
char *get_transformations_names(struct result_t *result);

int original_res;

/* Main function */
void
ais(struct list_call *b, int original_results, struct bsconf *search_conf) {
	int i,j;
	struct bconf_node *ais_root;
	struct bconf_node *filter;
	struct result_t *result;
	void *results_queue = NULL; /* Actually it is a binary tree, but behaves as a priority queue */
	int max_depth;
	/* Save original query because it will be destroyed in the process, and it will be needed
	 * later on for the actual search results query */
	char * original_query = b->query;

    original_res = original_results;

	if (b->args && strstr(b->args, "q=")) {
		ap_log_rerror(APLOG_MARK, APLOG_INFO, OK, b->r, "Running IAS!!");
		ais_root = bconf_get(b->conf->root, "ais.filter");

		/* FT 363:  Add 'st' parameter by default if it does not exists */
		if(!strstr(b->args,"st=")) {
			apr_psprintf(b->r->pool, "%s&st=a", b->args ? b->args : "");
		}


		for (i = 0 ; (filter = bconf_byindex(ais_root, i)) ; i++) {

			ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, b->r, "Processing Filter[%d]", i+1);

			/* Create a tree node */
			result = apr_pcalloc(b->r->pool, sizeof(struct result_t));
			result->b = b;
			result->query_string = apr_pstrdup(b->r->pool, b->args);
			result->number_of_results = 0;
			result->description = NULL;
			result->used_transformations = 0;

			/* Apply preproccessing functions: lower */
			for (j=0 ; j<(int)strlen(result->query_string) ; j++) {
				result->query_string[j] = tolower(result->query_string[j]);
			}

			/* Loop through all transformations defined for this filter.  */
			max_depth = bconf_count(bconf_get(filter, "transformation")) - 1; /* substract one, because its zero based */
			perform_recursive(max_depth, 0, filter, &results_queue, result, search_conf);
		}

		/* Restore original search query */
		b->query = original_query;

		/* Now decide results, etc., and then insert template */
		twalk(results_queue, add_template_vars);
		//if(results_queue != NULL) {
		//	add_template_vars(results_queue, leaf, 0);
		//}

		/* Finally, if needed, free up space */
		tdestroy(results_queue, dummy_free);
	}
}

void
add_template_vars(const void *nodep, const VISIT which, const int depth) {
	struct result_t *result = *(struct result_t **)nodep;

	if (which == postorder || which == leaf) {
		int curr_number_of_results = 0;
		ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r, "Inserting %d results node: %s", 
				result->number_of_results, result->query_string);
		curr_number_of_results = bpapi_get_int(&result->b->bparse, "ais_number_of_results");
		if(curr_number_of_results < result->number_of_results) {
			bpapi_remove(&result->b->bparse, "ais_number_of_results");
			bpapi_remove(&result->b->bparse, "ais_query_string");
			bpapi_remove(&result->b->bparse, "ais_query");
			bpapi_remove(&result->b->bparse, "ais_description");
			bpapi_remove(&result->b->bparse, "ais_transformation_names");
			bpapi_remove(&result->b->bparse, "ais_transformation_indexes");
			
			bpapi_set_int(&result->b->bparse, "ais_number_of_results", result->number_of_results);
			bpapi_insert( &result->b->bparse, "ais_query_string", result->query_string?:"");
			bpapi_insert( &result->b->bparse, "ais_query", result->query?:"");
			bpapi_insert( &result->b->bparse, "ais_description", result->description?:"");
			bpapi_insert( &result->b->bparse, "ais_transformation_names", get_transformations_names(result)?:"");
			bpapi_set_int( &result->b->bparse, "ais_transformation_indexes", result->used_transformations);
		}
	}
}

/* Comparison function to keep the queue sorted */
int
compare(const void *pa, const void *pb) {

	/* If we find that the EXACT same query is being done, don't insert new node */
	if (strcmp(((struct result_t *) pa)->query_string, ((struct result_t *) pb)->query_string)==0)		
		return 0;

	if (((struct result_t *) pa)->number_of_results < ((struct result_t *) pb)->number_of_results) 
		return 1;

	/* Tricky: return -1 instead of 0 to never consider two nodes equal, and so new nodes will always be created */
	/* if (((struct result_t *)pa)->number_of_results > ((struct result_t *)pb)->number_of_results) return -1; */
	/* return 0; */
	return -1;
}

/* Returns the value of the query string parameter defined by the key 'qs_key' */
char *
get_qs_param(struct result_t *result, const char *qs_key) {
	const char *qs_key_start = strstr(result->query_string, qs_key);
	if (qs_key_start) {
		int qs_key_len =(int)((long int)(strstr(qs_key_start,"&") - qs_key_start) - (int)strlen(qs_key));
		return  apr_pstrndup(result->b->r->pool, qs_key_start+(int)strlen(qs_key), qs_key_len);
	} else {
		return NULL;
	}
}

/* Modifies the value of the parameter defined by 'key' */
void
swap_qs_params( struct result_t *result, const char *key, const char *new_value ){
	/* key is a param value: 'cg', 'w' */
	char *whole_key_string = apr_psprintf(result->b->r->pool, "%s=", key);
	char *start = strstr(result->query_string, whole_key_string);
	char *params;

	/* if the parameter is not in the query_string, we put on the end, concatenated with an ampersand */
	if (start == NULL) {
		params = NULL;
		whole_key_string = apr_psprintf(result->b->r->pool, "&%s=", key);
		/*
		FT 440 In some case Ais could crash when try to access to query string
		In this case start must have a value !!!
		Patch provided by Rafael Zanella from BomNegocio
		*/
		start = result->query_string + strlen(result->query_string);
	} else {
		params = strstr(start, "&");
	}

	result->query_string = apr_psprintf(result->b->r->pool, "%.*s%s%s%s",
			(int)(start-result->query_string), 
			result->query_string, 
			whole_key_string,
			(new_value==NULL) ? "" : new_value,
			(params==NULL)    ? "" : params
			);
}

void
dummy_free(void *p) {
}

/* Create a copy of a tree node */
struct result_t *
copy_result(struct result_t *original) {
	struct result_t *copy = apr_pcalloc(original->b->r->pool, sizeof(struct result_t));
	copy->b = original->b;
	copy->query_string = apr_pstrdup(copy->b->r->pool, original->query_string);
	copy->description = apr_pstrdup(copy->b->r->pool, original->description);
	copy->used_transformations = original->used_transformations;
	copy->number_of_results = original->number_of_results;
	return copy;
}	

/* Used to insert a result into the tree */
void 
insert_element(struct result_t *result, void **tree, struct bsconf *search_conf) {

	/* Perform search and get number of results */
	set_search_query(result->b, result->query_string);
	result->number_of_results = check_search_query(result->b, search_conf);

	/* Add to (priority) queue */
	if ((result->number_of_results >= bconf_get_int(result->b->conf->root, "ais.min_hits_per_result")) && (result->number_of_results > original_res)) {
		result->query = apr_pstrdup(result->b->r->pool, strstr(result->b->query, "*:* ") + 4);
		tsearch((void *)result, tree, compare);
	}
}

/* 
 * strstr() overwrite that actually takes into account that for the needle to be matched, 
 * it needs to have the exact same size in the query string, so we do not match singulars with
 * plurals in the QS*/
char *find_qs_param(const char *haystack, const char *needle) {
	char *start = strstr(haystack, needle);
	if (start==NULL)
		return NULL;

	/* For the needle to be in the haystack, they must be the exact same size */
	int len = strlen(needle);
	if ( *(start+len)=='\0' || *(start+len)=='&' || *(start+len)=='+' )
		return start;
	return NULL;
}


/* Main recursive function */
void
perform_recursive(int max_depth, int current_depth, struct bconf_node *filter, void **tree, struct result_t *result, struct bsconf *search_conf) {

	int current_iteration=0;
	int transformation_result;
	struct result_t *copy = copy_result(result);

	do {
		/* Using the copy here, so it gets carried away through the filter and is reset 
		 * when the transformation is called again */
		struct result_t *temporary_result = copy_result(copy);

		transformation_result = perform_transformation(filter, current_depth, temporary_result, current_iteration);
		if (transformation_result==TRANSFORMATION_ERROR) {
			break;
		}

		/* This should be true ONLY when the last transformation has been made */
		if (current_depth==max_depth) {
			if (strcasecmp(result->b->args, temporary_result->query_string) != 0) {
				insert_element(temporary_result, tree, search_conf);
			}
		} else {
			perform_recursive(max_depth, current_depth+1, filter, tree, temporary_result, search_conf);
		}	

		current_iteration++;

	} while (transformation_result==MORE_RESULTS) ;
}

int
perform_transformation(struct bconf_node *filter, int transformation, struct result_t *result, int current_iteration) {
	int i;
	char *transformation_name;

	/* Get current transformation name from bconf */
	transformation_name = apr_pstrdup(result->b->r->pool, bconf_value(bconf_byindex(bconf_get(filter, "transformation"), transformation)));
	if (transformation_name==NULL) {
		ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, result->b->r, "Transformation index invalid: %d", transformation); 
		return TRANSFORMATION_ERROR;
	}

	/* Search for that transformation in the function lookup table */
	for (i = 0; transformations[i].function_name; i++) {
		if(!strcmp(transformation_name, transformations[i].function_name)) {

			ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r, 
					"We call %s with qs [%s]", transformation_name, result->query_string);
			return transformations[i].transformation_function(result, current_iteration,i);
		}
	}

	ap_log_rerror(APLOG_MARK, APLOG_ERR, OK, result->b->r, "Unknown transformation name: %s",transformation_name); 
	return TRANSFORMATION_ERROR;
}

static void
update_description(struct result_t *result, const char *new_description) {
	if(new_description) {
		if (result->description) {
			/* Make sure we do not insert same description twice */
			if (strstr(result->description, new_description)==NULL) {
				result->description = 
					apr_psprintf(result->b->r->pool, "%s, %s", result->description, new_description);
			}
		} else	{
			result->description = apr_psprintf(result->b->r->pool, "%s", new_description);
		}
	}
}

void
update_transformations(struct result_t *result, char transf_index) {
	/* Each transformation has an index in the transformation table, so we turn on the bit corresponding to the position of the 
		transformation on the table.*/
	result->used_transformations = result->used_transformations | (1 << transf_index);
}

char *
get_transformations_names(struct result_t *result) {
	int used_transformation = result->used_transformations;
	int i = 0;
	char * transformation_names = NULL;
	while (used_transformation) {
		/* if this bit is on we write the transformation name */
		if(used_transformation % 2) {
			const char * transformation_name = transformations[i].function_name;
			if (transformation_names) {
				transformation_names = 
					apr_psprintf(result->b->r->pool, "%s, %s", transformation_names, transformation_name);
			} else	{
				transformation_names = apr_psprintf(result->b->r->pool, "%s", transformation_name);
			}
		}
		i++;
		used_transformation /= 2;
	}
	return transformation_names;
}

static const char *
get_description_format(struct result_t *result, const char *transformation_name) {
	const char *lang;

	if (bconf_get_int(result->b->conf->root, "*.*.multilang.enabled") == 1) {
		lang = bpapi_get_element(&result->b->bparse, "b__lang", 0);
	} else {
		lang = bconf_value(bconf_get(result->b->conf->root, "common.default.lang"));
	}
	return bconf_value(bconf_vget(result->b->conf->root, "language", "ais", transformation_name, lang, NULL));

}

/* Transformation functions below */
int synonyms(struct result_t *result, int current_iteration, char transf_index) {	
	int i = 0;
	int hits_number=0;

	struct bconf_node *current_word;
	struct bconf_node *current_synonym;
	struct bconf_node *synonyms_conf = bconf_get(result->b->conf->root, "ais.synonyms.word");

	while ((current_word = bconf_byindex(synonyms_conf, i)) != NULL) {
		const char *word = bconf_value(bconf_vget(current_word, "word", NULL));
		const char *start;
		int pos;
		if ((start = find_qs_param(result->query_string, word)) != NULL) { /* XXX we should find complete words, that is, surrounded by spaces */
			pos = start - result->query_string;
			int j=0;
			while ((current_synonym = bconf_byindex(bconf_get(current_word, "synonyms"), j)) != NULL) {
				/* Hit number, to support different synonyms in a query */
				if (current_iteration!=hits_number) {
					hits_number++;
				} else {

					const char *synonym = bconf_value(current_synonym);
					result->query_string = apr_psprintf(result->b->r->pool, "%.*s%s%s", pos, result->query_string, synonym, result->query_string + pos + strlen(word));
					const char *format_string = get_description_format(result, "synonyms");

					if (format_string) {
						char *new_description = apr_psprintf(result->b->r->pool, format_string, word, synonym);
						update_description(result, new_description);
						update_transformations(result, transf_index);
						ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r,"%s",new_description);
					}

					/* There might be more results later */
					return MORE_RESULTS;
				}
				j++;
			}
		}
		i++;
	}
	return NO_MORE_RESULTS;
}

int misspellings(struct result_t *result, int current_iteration, char transf_index) {
	int i,j;
	struct bconf_node *current_word;
	struct bconf_node *current_misspelled;
	struct bconf_node *misspelled_conf = bconf_get(result->b->conf->root, "ais.misspellings.word");

	i=0;
	while ((current_word = bconf_byindex(misspelled_conf, i)) != NULL) {

		/* For each word with misspellings defined, we iterate over them... */
		j=0;
		while ((current_misspelled = bconf_byindex(bconf_get(current_word, "misspelled"), j)) != NULL) {
			const char *misspell = bconf_value(current_misspelled);
			const char *start;
			int pos;
			const char *word = bconf_value(current_misspelled);

			if ((start = find_qs_param(result->query_string, misspell))!=NULL) {

				/* we found a match, we stop searching!! */
				const char *intended = bconf_get_string(current_word, "intended");
				pos = start - result->query_string;
				result->query_string = apr_psprintf(result->b->r->pool, "%.*s%s%s", 
						pos, result->query_string, intended, result->query_string + pos + strlen(word));

				const char *format_string = get_description_format(result, "misspellings");
				if (format_string) {
					char *new_description = apr_psprintf(result->b->r->pool, format_string, misspell, intended);
					update_description(result, new_description);
					update_transformations(result, transf_index);
					ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r,"%s",new_description);
				}

				/* Break here so it checks more misspellings in one go */
				break;
			}
			j++;
		}
		i++;
	}
	return NO_MORE_RESULTS;
}

int geography(struct result_t *result, int current_iteration, char transf_index) {	
	struct bconf_node *geography_conf = bconf_get(result->b->conf->root, "ais.geography");
	struct bconf_node *regroot = bconf_get(result->b->conf->root, "common.region");
	int nreg = bconf_count(regroot);
	int i,pos;
	char *aux = NULL,*token=NULL,*start=NULL; 
	int flag_hits=0;
	int hits=0;

	for (i = 0 ; i < nreg ; i++) {
		const char *current_region = bconf_key(bconf_byindex(regroot, i));
		char *words = apr_pstrdup(result->b->r->pool, bconf_value(bconf_vget(geography_conf, current_region, NULL)));
		if (!words) 
			continue;

		/* Search all region words in query_string*/
		token = strtok_r(words,",",&aux);	
		do {
			if ( token  && (start = find_qs_param(result->query_string, token))!=NULL) {

				/* To allow different regions with similar tokens */
				if (current_iteration!=hits){
					hits++;
				} else {			

					/* Remove region word from query string*/
					pos = start - result->query_string;			
					if ( *(result->query_string + pos + strlen(token)) == '+') {
						result->query_string = 
							apr_psprintf(result->b->r->pool, 
									"%.*s%s", 
									pos, 
									result->query_string, 
									result->query_string + pos + strlen(token)+ 1);
					} else {
						result->query_string = 
							apr_psprintf(result->b->r->pool, 
									"%.*s%s", 
									pos, 
									result->query_string, 
									result->query_string + pos + strlen(token));
					} 

					/* Modify caller ca param with new region */
					const char *ca_value = apr_psprintf(result->b->r->pool, 
							"%s_s", current_region);
					swap_qs_params( result, "ca", ca_value);

					/* If we have a region we put this tab with w*/
					char *w = strstr(result->query_string, "&w=");
					if (w == NULL){
						result->query_string = 
							apr_psprintf(result->b->r->pool, 
									"%s&w=1", 
									result->query_string); 
					} else {
						swap_qs_params( result, "w", "1");
					}

					const char *format_string = get_description_format(result, "geography");
					if (format_string) {
						char *new_description = apr_psprintf(result->b->r->pool, format_string,
								bconf_value(bconf_vget(bconf_byindex(regroot, i),"name",NULL))
								);
						update_description(result, new_description);
						update_transformations(result, transf_index);
						ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r,"%s",new_description);
					}
					flag_hits=1;
				}
			}
		} while( (token = strtok_r(NULL,",",&aux)) );

		/* return after this, so more regions can be found */
		if (flag_hits==1){
			return MORE_RESULTS;
		}
	}
	return NO_MORE_RESULTS;
}

int category_words(struct result_t *result, int current_iteration, char transf_index) {
	struct bconf_node *category_words_conf = bconf_get(result->b->conf->root, "ais.category_words");
	struct bconf_node *catroot = bconf_get(result->b->conf->root, "cat");
	int ncat = bconf_count(catroot);
	int i,pos;
	char *aux = NULL,*token=NULL,*start=NULL; 

	for (i = 0 ; i < ncat ; i++) {
		const char *current_category = bconf_key(bconf_byindex(catroot, i));
		char *words = apr_pstrdup(result->b->r->pool, bconf_value(bconf_vget(category_words_conf, current_category, NULL)));
		if (!words) 
			continue;

		/* Search all category words in query_string*/
		token = strtok_r(words,",",&aux);	
		do {
			if ( token  && (start = find_qs_param(result->query_string, token))!=NULL) {
				pos = start - result->query_string;

				/* Remove category word from query string*/
				if ( *(result->query_string + pos + strlen(token)) == '+') {
					result->query_string = 
						apr_psprintf(result->b->r->pool, 
								"%.*s%s", 
								pos, 
								result->query_string, 
								result->query_string + pos + strlen(token)+ 1);
				} else {
					result->query_string = 
						apr_psprintf(result->b->r->pool, 
								"%.*s%s", 
								pos, 
								result->query_string, 
								result->query_string + pos + strlen(token));
				} 

				/* Modify category cg param with new category 'current_category*/		
				swap_qs_params( result, "cg", current_category);

				const char *format_string = get_description_format(result, "category_words");
				if (format_string) {
					char *new_description = apr_psprintf(result->b->r->pool, format_string,
							bconf_value(bconf_vget(bconf_byindex(catroot, i),"name",NULL))
							);
					update_description(result, new_description);
					update_transformations(result, transf_index);
					ap_log_rerror(APLOG_MARK, APLOG_DEBUG, OK, result->b->r,"%s",new_description);
				}

				return NO_MORE_RESULTS;
			}
		} while( (token = strtok_r(NULL,",",&aux)) );
	}
	return NO_MORE_RESULTS;
}

int plural(struct result_t *result, int current_iteration, char transf_index) {
	struct bconf_node *plurals_conf = bconf_get(result->b->conf->root, "ais.plurals");
	int i = 0;
	char *aux = NULL, *token = NULL, *qs_buffer = NULL;
	char qs_changed = 0;

	/* We need to check out every qs word independently */
	char *qs_strings = get_qs_param(result, "q=");

	/*
	FT 440: in some case AiS could crash whe try to access to query string
	This happens when qs_strings varialble is NULL
	Patch provided by Rafael Zanella
	*/
	if ( !qs_strings ) { return NO_MORE_RESULTS; }

	while ((token = strtok_r(qs_strings, "+", &aux))!=NULL) {
		const char *current_exception;
		char ignore_flag = 0;
		qs_strings = NULL; 

		/* If it is NOT a plural, move forward... */ 
		if (strlen(token)<2 || token[strlen(token)-1]!='s') {
			if (qs_buffer==NULL)
				qs_buffer = apr_psprintf(result->b->r->pool, "%s", token);
			else
				qs_buffer = apr_psprintf(result->b->r->pool, "%s+%s", qs_buffer, token);
			continue;
		}

		/* First check if this word is an exception */
		i=ignore_flag=0;
		while ((current_exception = bconf_value(bconf_byindex(bconf_get(plurals_conf, "exceptions"), i))) != NULL) {
			if (strncmp(current_exception, token, strlen(token))==0) {
				ignore_flag=1;
				break;
			}
			i++;
		}
		if (ignore_flag) {
			/* add this one to the new qs and continue */
			if (qs_buffer==NULL)
				qs_buffer = apr_psprintf(result->b->r->pool, "%s", token);
			else
				qs_buffer = apr_psprintf(result->b->r->pool, "%s+%s", qs_buffer, token);	
			continue;
		}

		/* Try to find a rule that applies for this word */
		const char *ends_with, *singular ;
		struct bconf_node *ending_node;
		i=0;
		while ((ending_node = bconf_byindex(bconf_get(plurals_conf, "word"), i)) != NULL) {
			ends_with = bconf_get_string(ending_node, "ends_with");

			/* Compare token's ending with each configured ending */
			int token_ending = strlen(token)- strlen(ends_with);
			if (token_ending && strcmp(token + token_ending, ends_with)==0) {

				/* add this one to the new qs and continue */
				singular = bconf_get_string(ending_node, "singular");
				if (qs_buffer==NULL)
					qs_buffer = apr_psprintf(result->b->r->pool, "%.*s%s", token_ending, token, singular);
				else
					qs_buffer = apr_psprintf(result->b->r->pool, "%s+%.*s%s", 
							qs_buffer, token_ending, token, singular);
				/* Mark that this word has been modified to continue searching */
				ignore_flag=1;
				qs_changed = 1;
				break;
			}
			i++;
		}
		if (ignore_flag)
			continue;

		/* if nothing else works, remove final 's' */
		if (qs_buffer==NULL)
			qs_buffer = apr_psprintf(result->b->r->pool, "%.*s", (int)strlen(token)-1, token);
		else
			qs_buffer = apr_psprintf(result->b->r->pool, "%s+%.*s", 
					qs_buffer, (int)strlen(token)-1, token);
		qs_changed = 1;
	}

	if(qs_changed) {
		update_transformations(result, transf_index);
	}
	/* Update original qs */
	swap_qs_params( result, "q", qs_buffer);
	return NO_MORE_RESULTS;
}

int remove_stopwords(struct result_t *result, int current_iteration, char transf_index) {
	char qs_changed = 0;
	char *aux = NULL, *token = NULL, *qs_buffer = NULL;

	/* We need to check out every qs word independently */
	char *qs_strings = get_qs_param(result, "q="); 
	/*
	FT 440: In some case AiS could crash whe try to access to query string
	This happens when qs_strings is NULL
	Patch provided by Rafael Zanella from BomNegocio
	*/
	if ( !qs_strings ) { return NO_MORE_RESULTS; }

	while ((token = strtok_r(qs_strings, "+", &aux))!=NULL) {
		const char *current_stopword;
		char stopword_found = 0;
		qs_strings = NULL;

		/* First check if this word is an exception */
		int i=0;
		while ((current_stopword = bconf_value(bconf_byindex(bconf_get(result->b->conf->root, "ais.stopword"), i))) != NULL) {
			if (strcmp(current_stopword, token)==0) {
				stopword_found=1;
				break;
			}
			i++;
		}
		if (!stopword_found) {
			/* add this one to the new qs and continue */
			if (qs_buffer==NULL)
				qs_buffer = apr_psprintf(result->b->r->pool, "%s", token);
			else
				qs_buffer = apr_psprintf(result->b->r->pool, "%s+%s", qs_buffer, token);	
		} else {
			qs_changed = 1;
		}
	}
	if(qs_changed) {
		update_transformations(result, transf_index);
	}

	/* Update original qs */
	swap_qs_params( result, "q", qs_buffer);
	return NO_MORE_RESULTS;
}
