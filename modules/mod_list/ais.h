#ifndef AIS_H
#define AIS_H

#include "mod_list.h"
#include "bconf.h"

void ais(struct list_call *b, int original_results, struct bsconf *search_conf);

#endif

