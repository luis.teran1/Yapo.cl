window.onload = function() {
  disableUnusedSearchForm(false);
};

$(document).on('change', '[name=fwgend]', updateFootwearSize);
$(document).ready(function(ev){
  updateFootwearSize();
  document.querySelector('#searchextras').style.display = 'block';
  SetClothingConditionEvents();
});

function SetClothingConditionEvents(){
	const clothingConditionInput = document.querySelectorAll('.search-listing__filters input[name="clothing_condition"]');
	clothingConditionInput.forEach((element) => {
		element.addEventListener('click', (event) => {
			const value = event.target.value;
			const element = document.querySelector(`#condition_cond option[value="${value}"]`);
			element.selected = 'selected';
		});
	});
}

function updateFootwearSize(ev){
  $('[name=fwsize]').prop( "disabled", false );
  sizes = $('[name=fwsize] option');
  sizes_len = sizes.length;
  var i;
  if ($('[name=fwgend]').val()) {
    for (i=0; i<sizes_len; i++){
      if (ev) {
        sizes[i].selected=false;
      }
	  if (sizes[i].getAttribute('value') != "") {
        if ($('[name=fwgend] option:selected')[0].getAttribute('data-sizeseq').split(",").indexOf(sizes[i].getAttribute('value')) != -1) {
          sizes[i].setAttribute('style', 'display:block');
        } else {
          sizes[i].setAttribute('style', 'display:none');
        }
      }
    }
  } else {
    for (i=0; i<sizes_len; i++){
	  sizes[i].selected=false;
	}
    $('[name=fwsize]').prop( "disabled", true);
  }
}

function reset_value(name) {
	var elems = document.getElementsByName(name);

	if (elems) {
		for (var e in elems)
			e.disabled = true;
	}
}

function search_key_lookup(key, data) {
	if (key == "parent") {
		var category = (data['cg'])? data['cg']: data['category'];
		if (category_list[category])
			return category_list[category]["parent"];
		return null;
	}
	return data[key];
}


/*
 * Show/hide searchextras
 */
function SearchCrit(_checkCat, adType, additional_key, additional_input, checkStByCat) {
	/* Get selected values only if same category as in query string or called _checkCat */
	var get_selected = (_checkCat || queryString("cg") == document.getElementById("catgroup").value) ? true : false;
	var reset_values = 0;
	var appl;
	var fk = document.getElementById("fk") != null ? 1 : 0;
	var agt=navigator.userAgent.toLowerCase();
	var is_major = parseInt(navigator.appVersion);
	var is_ie     = ((agt.indexOf("msie") != -1) && (agt.indexOf("opera") == -1));
	var is_ie5 = (is_ie && (is_major == 4) && (agt.indexOf("msie 5.0")!=-1) );
	var caller = queryString("ca");
	var additional_key_val = additional_key && (document.getElementById(additional_key) != null) ? document.getElementById(additional_key).value : null;
	if(additional_input && additional_key) {
		additional_key_val = (document.getElementById(additional_input) != null) ? document.getElementById(additional_input).value : null;
	}
	var lookup_data = Array();

	if (document.getElementById("cmd") != null) 
		appl = 'aw';
	else if (document.getElementById("wid") != null)
		appl = 'adwatch';
	else
		appl = 'li';

	if (!_checkCat) {
		var Category = document.getElementById("cat") ? document.getElementById("cat").value : queryString("c");
		var Categorygroup = document.getElementById("cat") ? document.getElementById("catgroup").value : queryString("cg");
		// Using timeout instead of using setcookie, "rendering flash" fix in ie
		setTimeout("document.cookie='c="+Category+"'", 0);
		setTimeout("document.cookie='cg="+Categorygroup+"'", 0);
		setTimeout("document.cookie='features=0'", 0);
		reset_values = 1;
	} else {
		var Category = (_checkCat > 0 || queryString("c") < 0) ? _checkCat : queryString("c");
		document.getElementById("cat").value = Category;
	}

	if (checkStByCat) {
		// check if st is defined in the query params
		// if defined its value takes priority
		var regex = new RegExp('[\\?&]st=([^&#]*)');
		var results = regex.exec(location.search);
		if (results) {
			var type = 'type_' + decodeURIComponent(results[1].replace(/\+/g, ' '));
		} else {
			var Categorygroup = document.getElementById("cat") ? document.getElementById("catgroup").value : queryString("cg");
			var type = category_settings['default_type'][Categorygroup];
			if (!type) {
				type = category_settings['default_type']['default'];
			}
		}
		document.getElementById(type).checked = true;
	}

	/* Update category drops */
	var catgroup_value = document.getElementById("catgroup").value;
	var cat_id = "cat";

	lookup_data['cg'] = catgroup_value;
	lookup_data['appl'] = appl;
	lookup_data['region'] = (document.getElementById('searcharea_expanded').value > 100)?document.getElementById('searcharea_expanded').value -100: getRegionFromSq();
	lookup_data['w'] = (document.getElementById('searcharea_expanded').value > 100)?"1": document.getElementById('searcharea_expanded').value;

	if (additional_key && additional_key_val) {
		var offs = additional_key.search(/[0-9]/);
		var key = additional_key;
		if (offs > -1)
			key = key.substr(0, offs - 1);
		if (key.match(/^cat/))
			key = "subcategory";
		else
			key = additional_key;

		lookup_data[key] = additional_key_val;
	}

	if (!adType) {
		/* Check with category only if subtypes are available */
		var extras = split_setting(get_settings('searchextras', search_key_lookup, list_settings, lookup_data));

		if (extras['types']) {
			if (extras['types'] == 'sellrent' && getAdTypeFromCaller() == 'k')
				adType = 'k';
			else if (document.getElementById('types').style.display == 'none')
				adType = 'a';
			else
				adType = get_radio_value(document.forms.f.st);
		}
		if (!adType) {
			adType = getAdTypeFromCaller();
		}
	}

	lookup_data['st'] = adType;

	var extras = split_setting(get_settings('searchextras', search_key_lookup, list_settings, lookup_data));

	var labels = getElementsByClassName(document.getElementById('types'), 'label', 'st_label');
	if (extras['types']) {
		showField('types', 'flex');
		var types;
		var one_checked = false;
		var l_all;
		var inp_all;
		var checked;

		if (extras['types'] == "sellrent")
			types = {"s" : 1, "u" : 1};
		else
			types = split_setting(get_settings('types', search_key_lookup, category_settings, {'category':catgroup_value,'type':adType, 'appl' : appl}));
		for (var i = 0; i < labels.length; i++) {
			var l = labels[i];
			var t = l.id.replace("label_st_", "");
			var inp = document.getElementById("type_" + t);
			if (t == "a") {
				l_all = l;
				inp_all = inp;
			} else if (types[t]) {
				inp.disabled = false;
				l.style.display = getLabelDisplayType(l);
				if (inp.checked) {
					one_checked = true;
				}
			} else {
				l.style.display = 'none';
				inp.disabled = true;
				inp.checked = false;
			}
		}

		if (extras['types'] != 'simple') {
			if (!one_checked || inp_all.disabled)
				inp_all.checked = true;
			inp_all.disabled = false;
			l_all.style.display=getLabelDisplayType(l_all);
		} else {
			inp_all.disabled = false;
			inp_all.checked = false;
			l_all.style.display=getLabelDisplayType(l_all);
			if (!one_checked)
				document.getElementById('type_a').checked = true;
		}
		/* Once updated the 'type' radio button, get the checked one, and charge extras for it
		   They may be different from the 'extras' we already have!! */
		adType = get_radio_value(document.forms.f.st);
		lookup_data['st'] = adType;
		extras = split_setting(get_settings('searchextras', search_key_lookup, list_settings, lookup_data)); 
	} else {
		for (var i = 0; i < labels.length; i++) {
			var l = labels[i];
			var t = l.id.replace("label_st_", "");
			var inp = document.getElementById("type_" + t);

			inp.disabled = true;
			inp.checked = false;
			l.style.display= 'none';
		}
	}

	var fields = getElementsByClassName(document.getElementById('search'), 'div', 'featurebox');

	var subcat = document.getElementById(cat_id);
	subcat.options.length = 1;
	subcat.options[0] = new Option(extras["sub" + cat_id], "0");

	var i = 1;
	for (var k in category_list) {
		if (category_list[k]["level"] == 2 && category_list[k]["parent"] == catgroup_value) {
			subcat.options[i] = new Option(category_list[k]["name"], k);
			if (Category == k)
				subcat.selectedIndex = i;
			i++;
		}
	}

	var hasExtras = false;
	for (var e in extras) {
		if (e != 'none')
			hasExtras = true;
		break;
	}

	if (!hasExtras) {
    document.getElementById('searchextras').classList.remove('--show');
	} 

	if (additional_key != 'estate_type' && extras['estate_type']) {
		setEstateType(lookup_data['cg']);
	}

	for (var i = 0; i < fields.length; i++) {
		var select = fields[i].getElementsByTagName('select');
		var inputs = fields[i].getElementsByTagName('input');
		var eid = Array();
		var offs = fields[i].id.search(/[0-9]/);

		if (offs > -1) {
			eid[0] = fields[i].id.substr(0, offs - 1);
			eid[1] = fields[i].id.substr(offs);
		} else {
			eid[0] = fields[i].id;
		}

		if ((eid.length == 2 && extras[eid[0]] == eid[1]) || (eid.length == 1 && extras[eid[0]]) || extras[fields[i].id]) {
			if (extras["display_none"] != eid[0]){
				if(fields[i].id === 'types'){
					showField(fields[i].id, 'flex');
				} else {
					showField(fields[i].id, 'block');
				}
			}
			if (select) {
				for (var j = 0; j < select.length; j++) {
					enable_field(select[j]);
					/*
					if (!select[j].id.startsWith("footwear_size_gend")) {
						enable_field(select[j]);
					} else {
						select[j].style.display = 'none';
					}
					*/
				}
			}

			if (inputs) {
				for (var j = 0; j < inputs.length; j++) {
					if (inputs[j].type == "radio") {
						if ((additional_key == inputs[j].id) || inputs[j].id.substr(inputs[j].id.length-2, 2) == "_0")
							inputs[j].checked = "checked";
					}	
					enable_field(inputs[j]);
				}	
			}	
		} else {
			showField(fields[i].id, 'none');
			if (select) {
				for (var j = 0; j < select.length; j++) {
					setValue(select[j], "");
					disable_field(select[j]);
				}
			} 

			if (inputs) {
				for (var j = 0; j < inputs.length; j++) {
					disable_field(inputs[j]);
				}	
			}
		}
	}

	if (extras['subcat']) {
		subcat.options[0].innerHTML = extras['subcat'];
	}
	if (hasExtras)
    document.getElementById('searchextras').classList.add('--show');

	var search_where = split_setting(get_settings('search_where', search_key_lookup, list_settings, {'cg':catgroup_value,'st':adType, 'appl' : appl, 'fk' : fk}));
	if (search_where['country']) {
		/* Replace normal search region list with a list of countries */
		showField('country', 'inline');
		showField('searcharea_expanded', 'none');
		showField('searcharea', 'none');
		enable_field("country");
		enable_field("co");
		disable_field("searcharea_expanded");	
		disable_field("searcharea");
	} else if (search_where['expanded']) {
		/* Replace normal search region list with an expanded region list (including all regions) */
		showField('searcharea_expanded', 'inline');
		showField('searcharea', 'none');
		showField('country', 'none');
		enable_field("searcharea_expanded");	
		disable_field("searcharea");
		disable_field("country");
		disable_field("co");
	} else if (search_where['area']) {
		var searcharea = document.getElementById('searcharea');
		var region = caller.split('_');
		var munic = regionArray[region[0]]['municipality'];
		var cities = regionArray[region[0]]['cities'];
		//var i = searcharea.options.length;
		var i = cities ? 4 : 3;

		var selected = searcharea.options[searcharea.selectedIndex].value;
		searcharea.options.length = i;

		if (cities) {
			var opt = searcharea.options[i++] = new Option("-- St�der --", 0);
			opt.style.backgroundColor = '#dcdcc3';
		} else {
			var opt = searcharea.options[i++] = new Option("-- Kommuner --", 0);
			opt.style.backgroundColor = '#dcdcc3';
		}

		if (cities) {
			for (var key in cities) {
				var v = '1' + region[0] + ":" + key;
				searcharea.options[i++] = new Option(cities[key], v, v == selected);
			}	
		} else {
			for (var key in munic) {
				var v = '1' + region[0] + ":1" + key;
				searcharea.options[i++] = new Option(munic[key]['name'], v, v == selected);
				if (munic[key]['subarea']) {
					for (var key2 in munic[key]['subarea']) {
						var v = '1' + region[0] + ":1" + key + ":" + key2;
						searcharea.options[i++] = new Option("  - " + munic[key]['subarea'][key2]['name'], v, v == selected );
					}	
				}	
			}	
		}

		/* Expanded search region list with municipality  */
		showField('searcharea', 'inline');
		showField('searcharea_expanded', 'none');
		showField('country', 'none');
		enable_field("searcharea");	
		disable_field("searcharea_expanded");	
		disable_field("country");
		disable_field("co");
	} else {
		var searcharea = document.getElementById('searcharea');

		if (searcharea.options.length >= 5 && searcharea.options[4].innerHTML.match(/^-- /))
			searcharea.options.length = 4;
		else if (searcharea.options.length >= 4 && searcharea.options[3].innerHTML.match(/^-- /))
			searcharea.options.length = 3;

		/* Normal search region list (current, neighboring and everywhere) */
		showField('searcharea', 'inline');
		showField('searcharea_expanded', 'none');
		showField('country', 'none');
		enable_field("searcharea");	
		disable_field("searcharea_expanded");	
		disable_field("country");
		disable_field("co");
	}

	/* Display/hide searchbox tip */
	var searchbox_tips = split_setting(get_settings('searchbox_tip', search_key_lookup, list_settings, {'cg':catgroup_value, 'appl' : appl}));
	var searchbox_tip = searchbox_tips['tip'];
	var searchbox_href = searchbox_tips['href'];

	if (searchbox_tip) {
		var sbtip = document.getElementById('searchbox_tip');
		if (sbtip) {
			/* build href string and replace it in tip */
			var ca = queryString('ca');
			if (ca)
				searchbox_href += '&ca=' + ca;

			searchbox_tip = searchbox_tip.replace(/#href#/g, searchbox_href);

			sbtip.innerHTML = searchbox_tip;
			showField('searchbox_tip', 'block');
		}
	} else {
		showField('searchbox_tip', 'none');
	}

	/* Any action hides the job banner */
	var mb = document.getElementById("job_blocket");
	if (mb)
		mb.style.display = 'none';
	change_searchbox_placeholder();
	listener_brand();
	updateFootwearSize();
	EnableClothingCondition();
}

function EnableClothingCondition(){
	const clothingConditionInput = document.querySelectorAll('.search-listing__filters input[name="clothing_condition"]');
	clothingConditionInput.forEach((element, i) => {
		if(i === 0){
			element.checked = true;
		} else {
			element.checked = false;
		}
		element.disabled = false;
	});
}

function getLabelDisplayType(label){
	var display = 'inline';
	if(label.classList.contains('search-listing__filters-radio')){
		display = 'grid';
	}

	return display
}

function change_searchbox_placeholder() {

	var category = document.getElementById("catgroup").value;
	if(!category || category == 0) category = "default";//if the category is not set, set to the default

	var old_placeholder = $("#searchtext").attr("placeholder");//the current placeholder
	var new_placeholder = search_tips[category]['text'];//the new placeholder

	var ctrl = $("#searchtext");
	var placeholder_class = "placeholder";

	//check if the browser support the placeholder feature
	var enabled = false;
	if (placeholder_class  in document.createElement("input")) {
		enabled = true
	}
	//set the placeholder to the new value
	$("#searchtext").attr("placeholder", new_placeholder);

	ctrl.unbind("focus.placeholder");
	ctrl.unbind("blur.placeholder");
	ctrl.bind("focus.placeholder", function(){
			if(!enabled){
			if(ctrl.val() === new_placeholder){
			ctrl.val("");
			ctrl.removeClass(placeholder_class);
			}
			}else{
			ctrl.removeClass(placeholder_class);
			}
			}).bind("blur.placeholder", function(){
				if(!enabled){
				if(ctrl.val() === "" || ctrl.val() === new_placeholder || ctrl.val() === old_placeholder) {
				ctrl.val(new_placeholder);
				ctrl.addClass(placeholder_class);
				}
				}else{
				if(!ctrl.val()) ctrl.addClass(placeholder_class);
				}
				}).trigger("blur.placeholder");
}

/*
 * Set category cookie, used by searchbox and reset the searchbox feature cookie
 */ 
function clearSearch(_category) {
	setCookie('c', _category);
	setCookie('features', '0');
}

	function get_radio_value(radio_container) {
		if(!radio_container)
			return;

		for(var i=0; i < radio_container.length; i++) {
			if(radio_container[i].checked) {
				return radio_container[i].value;	
			}
		}	

		return null;
	}

function set_ca_param(){
	var w = document.getElementById("searcharea_expanded").value;
	var ca = document.forms['search_form'].elements['ca'];
	var new_caller;
	if(w >100){
		new_caller = w - 100;
		new_caller = new_caller + "_";
		new_caller = new_caller + ca.value.split('_')[1];	
		ca.value = new_caller;
	} 	
	return ca.value;
}

function friendly_name_change(ca){
	var ca_region = ca.split('_')[0];
	var w = document.getElementById("searcharea_expanded").value;
	var category = document.getElementById("catgroup").value;
	var form = document.getElementById("search_form");
	var action = form.action.split('?');
	var uri = action[0].split('/');
	var state = regionArray[ca_region]['state'];
	var index=3;

	switch (w) {
		case '2': 
			uri[index++] = friendlyregion[ca_region][b__lang] + '-' + nearbyregions;
			break;
		case '3': 
			uri[index++] = friendlyregion['allcountry'][b__lang];
			break;
		default:
			uri[index++] = friendlyregion[ca_region][b__lang];
			break;
	}
	uri[index++] = friendlycategory[category][b__lang];
	action[0] = uri.join('/');
	form.action = action.join('?');
}

function checkSeller(priv, comp) {
	// former values of the checkbox elements
	var g_priv = 1;
	var g_comp = 1;

	var el_p = document.getElementById("p").checked;
	var el_c = document.getElementById("c").checked;
	if (!el_p && !el_c) {
		g_priv = priv;
		g_comp = comp;
	} else if (el_p && el_c) {
		g_priv = comp;
		g_comp = priv;
	}
	
	var seller = split_setting(get_settings('private', search_key_lookup, 
				list_settings, {'g_priv':g_priv, 'g_comp':g_comp, 'priv':priv, 'comp':comp}));

	document.getElementById("p").checked = ( seller.p == 'true' ) ? true : false;
	document.getElementById("c").checked = ( seller.c == 'true' ) ? true : false;
	g_priv = seller.g_priv;
	g_comp = seller.g_comp;
	if (g_priv == g_comp) {
		document.getElementById("p").name = 'f';
		document.getElementById("p").value = 'a';
		document.getElementById("c").name = 'f';
		document.getElementById("c").value = 'a';
	} else {
		document.getElementById("p").name = 'f';
		document.getElementById("p").value = 'p';
		document.getElementById("c").name = 'f';
		document.getElementById("c").value = 'c';
	}
}

function searchSubmit() {
  setMinMax();
  var caller = queryString("ca");
  caller = set_ca_param();
  var w = document.getElementById("searcharea_expanded"); 

  /* Re-checking checkbox states, for history.back issues */
  if (document.getElementById("p").checked === document.getElementById("c").checked) {
    document.getElementById("p").name = 'f';
    document.getElementById("p").value = 'a';
    document.getElementById("c").name = 'f';
    document.getElementById("c").value = 'a';
  } else {
    document.getElementById("p").name = 'f';
    document.getElementById("p").value = 'p';
    document.getElementById("c").name = 'f';
    document.getElementById("c").value = 'c';
  }

  /* Disable the parameters with a default value. */
  if(document.getElementById("p").value === 'a') {
    document.getElementById("p").disabled = true;
    document.getElementById("c").disabled = true;
  }
  if(document.getElementById("searchtext").value == false)	
    document.getElementById("searchtext").disabled = true;

		const clothingConditionInput = document.querySelectorAll('.search-listing__filters input[name="clothing_condition"]');
		clothingConditionInput.forEach((element) => {
			element.disabled = true;
		});

		if(w.value >100){
    new_w = "1";
    for(index = 0; index < w.length; index++) {
      if(w[index].value == new_w)
        w.selectedIndex = index;
      }
  }
  friendly_name_change(caller);

  // Clean unnecesary parameters for the submit
  disableUnusedSearchForm(true);
}


/* Communes populate here */
function populateCommunes(result, xmlhttp, arg) {
	var commune_select = document.getElementById('communes_cmn');
	var communes = eval("[" + result + "]");
	if (commune_select && commune_select['data-sel_id']) {
		var user_selected_commune = commune_select['data-sel_id'];
		commune_select['data-sel_id'] = "";
	}
	for(var i = 0; i < communes.length; i++){
		var obj = communes[i];
		for(var comm_id in obj){
			commune_select.options[i+1] = new Option(obj[comm_id]['yapo_name'], comm_id);
			if (user_selected_commune != undefined && user_selected_commune == comm_id)
				commune_select.options[i+1].selected = true;
		}
	}
	if (communes.length == 1){
		commune_select.options[1].selected = true;
	}
	drawMulticomWidget();
}

function resetCommunes() {
	var lookup_data = Array();
	var label = split_setting(get_settings('communes', search_key_lookup, label_settings, lookup_data));
	$('#communes_cmn option').remove();
	$('#communes_cmn').append($('<option value="" selected="selected">' + label['label'] +'</option>'));
} 

function setCommunes(region) {
	resetCommunes();
	if (!region || region == 0) {
		region = regionDefault;
	}
	ajax_request("/templates/common/communes.html?region="+region, '', populateCommunes, null, false, 'GET');
}

/* Populate the estate types on searchbox */
function populateEstateTypes(result, xmlhttp, arg) {
	var ret = eval("[" + result + "]");
	// Objects def
	var obj = $('#estate_type_ret');
	var selected = obj.val();
	var options_ret = obj.prop?obj.prop('options'):obj.attr('options');
	// Clean the old options keep the first
	for(i=(options_ret.length-1); i >= 1; i--) {
		$("#estate_type_ret option[value='"+$(options_ret[i]).val()+"']").remove();
	}
	// Add the new options
	for(var i = 0; i < ret.length; i++){
		var obj = ret[i];
		for(var key in obj){
			options_ret[options_ret.length] = new Option(obj[key]['yapo_name'], key);
		}
	}
}
/* Ask for the estate types depends on the category */
function setEstateType(cg) {
  ajax_request("/templates/common/estate_types.html?cg="+cg, '', populateEstateTypes, null, false, 'GET');
}

/*
   SELECT ELEMENTS FROM THE FORM
   The select controls used for range search are named "string_xs" and "string_xe", where "string_x"
   corresponds to the name of the range and "s" for start and "e" for end values.
   We consider rootName as "string_x" and leafName as "s" or "e". The only range that not apply this
   convention is the price, that uses "ps_x" for start values and "pe_x" for end values.
   In this case we consider "p_x" as the rootName and "s" or "e" as leafName; The sizelist have a
   different pattern too, it uses "ss_x" for start values and "se_x"for end values. In this case
   we consider "s_x" as the rootName and "s" or "e" as leafName;
   We loop through all the elements in the form and we get the rootName and baseName, and we store
   it in an array in a form of [rootName][leafName].
   After check if both "s" and "e" values exists and the "s"  does not have the minimum index
   selected and "e" does not have the maximum index selected and the value of "s" is greater
   than the value of "e", we apply the function changeSelectValues.
 */
function setMinMax() {
  var form =  document.getElementById('search_form');
  var ctrlArray = [];

  for (var i = 0; i < form.elements.length; i++) {
    if (form.elements[i].type === 'select-one' &&
         form.elements[i].parentNode.style.display !== 'none' &&
         form.elements[i].selectedIndex > 0) {
      var elementNameLength = form.elements[i].id.length;
      var rootName;
      var leafName;

      if (form.elements[i].id.match(/[p|s]\w_\d/)) {
        rootName = form.elements[i].id.replace(/(p|s)._/, '$1_');
        leafName = form.elements[i].id.substring(1, 2);
      } else {
        rootName = form.elements[i].id.substring(0, elementNameLength - 1);
        leafName = form.elements[i].id.substring(elementNameLength - 1);
      }

      if (!ctrlArray[rootName]) {
        ctrlArray[rootName] = [];
      }

      ctrlArray[rootName][leafName] = form.elements[i];
      var minElement = ctrlArray[rootName].s;
      var maxElement = ctrlArray[rootName].e;
      if (minElement && maxElement) {
        var minSelectedIndex = minElement.selectedIndex;
        var maxSelectedIndex = maxElement.selectedIndex;
        var maxElementLength = maxElement.length;
        var minValue = Number(minElement.value);
        var maxValue = Number(maxElement.value);
        if ((minSelectedIndex > 0) && (maxSelectedIndex < maxElementLength)
            && (minValue > maxValue)) {
          changeSelectValues(minElement, maxElement);
        }
      }
    }
  }
}

/*
 * To change the select values we do the following:
 * For the max value, we apply the same index as selected in the minimum.
 */
function changeSelectValues (selectmin, selectmax) {
  var maxValue = selectmax.value;
  var minValue = selectmin.value;
  selectmax.value = minValue;
  selectmin.value = maxValue;
}

function disableUnusedSearchForm(dis) {
	var formToEnable = document.getElementById("search_form");
	if (formToEnable) {
		for(var i = 0; i < formToEnable.elements.length; i++){
			var el = formToEnable.elements[i];
			if ((el.type == 'select-one' && 
						el.parentNode.style.display != 'none' && 
						el.selectedIndex == 0) ||
					(el.type == 'text' && 
					 el.parentNode.style.display != 'none' && 
					 el.value == "")) {
				el.disabled=dis;
			}
		}
		document.getElementById("catgroup").disabled=dis;
		if (!dis) {
			document.getElementById("p").disabled = false;
			document.getElementById("c").disabled = false;
			onReadyForCars();
		}
	}
}
function onReadyForCars(){
	var category = $('#catgroup').val();
	if( category == '2020'){
		if( $("#brand_br").val() == "" ){
			$('#model_mo').attr('disabled','disabled');
		}else{
			$('#model_mo').attr('disabled',false);
		}
	}
}

function listener_brand(){
	var category = $('#catgroup').val();
	if( category == '2020'){
		if( $("#brand_br").val() == "" ){
			$('#model_mo').attr('disabled','disabled');
			$('#model_mo').val('');
		}else{
			var request = "/templates/common/ajax_cars.html?cg="+$('#catgroup').val()+"&brand=" + $('#brand_br').val();
			ajax_request(request, '', listener_brand_cb, null, false, 'GET');

		}
	}
}

function listener_brand_cb(result, xmlhttp) {
	// Save old selected model
	var models = $("#model_mo");
	var current_val = models.val();
	var current_label = $("#model_mo option[value='"+current_val+"']").text();


	var selected_brand = jQuery.parseJSON(result);
	var obj = $('#model_mo');
	var options_model = obj.prop?obj.prop('options'):obj.attr('options');

	for(i=(options_model.length-1); i >= 1; i--) {
		$("#model_mo option[value='"+$(options_model[i]).val()+"']").remove();
	}

	$.each(selected_brand['models'], function(key, val) {
		if(val != undefined){
			options_model[options_model.length] = new Option(val["name"], val["key"]);
		}
	});

	obj.attr('disabled',false);

	var new_option = models.find("option[value='"+current_val+"']");
	if(new_option.text() == current_label) {
		new_option.attr('selected', 'selected');
	}
}
