#include <httpd.h>
#include <http_log.h>
#include <http_config.h>
#include <http_protocol.h>
#include <apr_strings.h>
#include <ctype.h>
#include <string.h>

#include "mod_bconf_extern.h"
#include "util.h"
#include "query_string.h"
#include "cookies.h"
#include "url.h"

/*
 *  The request handler
 */

static void *create_redir_config(apr_pool_t *pool, server_rec *s);
static void register_redir_hooks(apr_pool_t *p);

struct redir_conf {
	struct bconf_node *root;
};

struct redir_call {
	apr_table_t *vars;
};

module AP_MODULE_DECLARE_DATA redir_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL, /* per dir create config */
	NULL, /* per dir merge config */
	create_redir_config, /* per server create config */
	NULL, /* per server merge config */
	NULL, /* config file commands */
	register_redir_hooks /* register hooks */
};

static void *
create_redir_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct redir_conf));
}

static int
redir_post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
	struct redir_conf *conf;
	struct bconf_node *br;

	for ( ; s != NULL; s = s->next) {
		if ((br = mod_bconf_get_root(s)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_redir - Failed to get bconf root.");
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		conf = ap_get_module_config(s->module_config, &redir_module);
		conf->root = br;
	}

	return OK;
}

static void
redir_qs_cb(struct parse_cb_data *pqcb_data, char *key, int klen, char *value, int vlen) {
	struct redir_call *call = pqcb_data->cb_data;

	apr_table_set(call->vars, key, value);
}

static int
redir_handler(request_rec *r) {
	struct redir_call call = {0};
	char *previous_site  = NULL;
	char *redir_site = NULL;
	char *redir_referer  = NULL;
	char *redir_type  = NULL;
	const char *base_url = NULL;
	const char *site = NULL;
	const char *referer  = NULL;
	char *cp = NULL;
	char datestring[64];
	time_t t;
	struct tm tm;
	char *log_string = NULL;
	char *cookie = NULL;
	char *next_url = NULL;
	char *seo_url = NULL;
	char *lp_param = NULL;
	char *lp_str = NULL;
	const char *ad_group_url = NULL;
	const char *ad_group_page_key = NULL;
	char *args = NULL;
	struct redir_conf *conf;
	const char *cookie_name;
	const char *redir_cookie_name;

	if (strcmp(r->handler, "redir-handler") != 0)
		return DECLINED;

	conf = ap_get_module_config(r->server->module_config, &redir_module);
	call.vars = apr_table_make(r->pool, 4);
	if (r->args)
		parse_query_string(apr_pstrdup(r->pool, r->args), redir_qs_cb, &call, NULL, 0, 0, NULL);

	base_url = bconf_get_string(conf->root, "common.base_url.blocket");
	if (!base_url || !base_url[0]) {
		ap_log_error(APLOG_MARK, APLOG_ERR, OK, NULL, "mod_redir - Failed to lookup base_url in bconf.");
		return HTTP_INTERNAL_SERVER_ERROR;
	}
	ap_set_content_type(r, "text/html");

	/* Set P3P header */
	apr_table_add(r->err_headers_out, "P3P", "CP='NOI DSP COR PSAo PSDo'");

	/* Read previous cookie */
	cookie_name = bconf_vget_string(conf->root, "mod_redir.cookie", r->uri + 1, NULL);
	if (!cookie_name)
		cookie_name = "redir";
	cookie = get_cookie(r, cookie_name);
	if (cookie && ((cp = strrchr(cookie, '-')) != NULL)) {
		*cp = '\0';

		previous_site = apr_pstrdup(r->pool, cookie);
		/* if previous date is needed, just cp++ and it will be under the
		   char pointer for copying */
	}

	/* Redir site */
	site = apr_table_get(call.vars, "s");
	if (site) {
		/* This is needed so campaign codes from different codesets de not conflictap_pbase64encode*/
		redir_site = ap_pbase64encode(r->pool, (char *)site);

		/* Be careful here, different sites have differnet politics here (such as Tori) */
		/* redir_site = apr_pstrdup(r->pool, (char *)site); */
	}

	/* Referer */
	referer = apr_table_get(r->headers_in, "Referer");
	if (referer) {
		redir_referer = apr_pstrdup(r->pool, referer);
	}

	/* Redirect to next url */
	if (apr_table_get(call.vars, "url")) {
		char *pos;

		args = apr_pstrdup(r->pool, r->args);
		next_url = strstr(args, "url=");
		/* Point to after url= */
		if (next_url)
			next_url += 4;

		/* Fix for weekend - replace first %3f with ? in redir */
		if (next_url && (pos = strstr(next_url, "%3f")) != NULL) {
			memmove(pos+1, pos+3, strlen(pos+3)+1); /* +1 to copy \0 */
			*pos = '?';
		}
	}

	/* Redirect to SEO campaigns */
	if( next_url && (lp_str = strstr(next_url, "lp=")) ) {
		char	key_buf[1024];

		lp_str += 3; /* Point to after lp= */
		lp_param = apr_pstrndup(r->pool, lp_str, strcspn(lp_str, "&"));

		snprintf(key_buf, sizeof(key_buf), "%s.page", lp_param);
		ad_group_page_key = bconf_get_string(bconf_get(conf->root, "common.ad_group.code"), key_buf);

		if ( !ad_group_page_key ) {
			struct	bconf_node *wild_char_node = NULL;

			if ((wild_char_node = bconf_get(conf->root, "common.ad_group.code")) != NULL) {
				int			wcnl_i = 0;
				int			wild_char_node_len = bconf_count(wild_char_node);
				struct bconf_node	*wcn_st = NULL;
				const char		*s_holder = NULL;
				char			*_tok = NULL;

				for (; (wcnl_i < wild_char_node_len); ++wcnl_i) {
					if ( !(wcn_st = bconf_byindex(wild_char_node, wcnl_i)) )
						continue; 

					s_holder = bconf_key(wcn_st);
					if ( !(_tok = strchr(s_holder, '%')) )
						continue;

					if ( !strncmp(s_holder, lp_param, (_tok - s_holder)) ) {
						snprintf(key_buf, sizeof(key_buf), "%s.page", s_holder);
						ad_group_page_key = bconf_get_string(wild_char_node, key_buf);
						lp_param = apr_pstrndup(r->pool, s_holder, strlen(s_holder));
						break;
					}
				}
			}
		}
	}

	if (ad_group_page_key != NULL) {
		ad_group_url = bconf_get_string(bconf_get(conf->root, "common.seo.landing"), ad_group_page_key);
	}

	if (ad_group_url != NULL) {
		char	*nu_params = strstr(next_url, "?");

		if (lp_str && nu_params) {
			char	nu_buf[4096], *s = NULL;
			int	nu_len;

			memset(nu_buf, '\0', sizeof(nu_buf));
			// Removes lp=... - Zane
			memcpy(nu_buf, nu_params, ((lp_str - 4) - nu_params));
			if( (s = strchr(lp_str, '&')) ) {
				nu_len =  strlen(nu_buf);
				snprintf((nu_buf + nu_len), sizeof(nu_buf) - nu_len, "%s", s);
			}

			seo_url = apr_psprintf(r->pool, "%s%s&lp=%s", ad_group_url, nu_buf, lp_param);
		}  else if(nu_params){
			seo_url = apr_psprintf(r->pool, "%s%s", ad_group_url, nu_params);
		}else {
			seo_url = apr_psprintf(r->pool, "%s", ad_group_url);
                }


		next_url = seo_url;
	}

	/* Log redir in sql format */
	t = time(NULL);
	gmtime_r(&t, &tm);
	
	strftime(datestring, sizeof(datestring), "%Y/%m/%d+%H_%M_%S", &tm);
	if (redir_site && previous_site
	    && ((strncmp(redir_site,previous_site,strlen(redir_site))==0))) {
		redir_type=apr_pstrdup(r->pool,"repeated_redir");
	} else {
		redir_type=apr_pstrdup(r->pool,"first_redir");
	}

	/* site (code), type, remote_addr, referer */
	log_string = apr_psprintf(r->pool, "%s %s %s %s",redir_site?:ap_pbase64encode(r->pool, (char *)"unknown"), redir_type, r->connection->remote_ip, redir_referer? redir_referer : "");

	if (strlen(log_string) < 900) {
		apr_table_add(r->notes, "redir_log", log_string);
		ap_log_error(APLOG_MARK, APLOG_INFO, OK, NULL, "REDIR: %s", log_string);
	}

	/* Get new "from_click" cookie name from bconf */
	redir_cookie_name = bconf_get_string(conf->root, "common.from_click_cookie_name");
	if (!redir_cookie_name)
		redir_cookie_name = "from_click";
		
	if (!apr_table_get(call.vars, "nc")) {
		/* Create cookie */
		cookie = apr_psprintf(r->pool, "%s-%s", redir_site?:ap_pbase64encode(r->pool, (char *)"unknown"), datestring);

		/* Set cookie */
		set_cookie_with_expiry(r, cookie_name, cookie, bconf_get_string(conf->root, "common.cookie.domain"), 3 * 30, 0);
		
		/* Set new "from_click" cookie */
		set_cookie_with_expiry_minutes(r, redir_cookie_name, "y", bconf_get_string(conf->root, "common.cookie.domain"), 1, 0);
	}

	const char *pass;
	int i;
	for (i = 0 ; (pass = bconf_value(bconf_byindex(bconf_get(conf->root, "mod_redir.pass"), i))) ; i++) {
		const char *val;

		if ((val = apr_table_get(call.vars, pass))) {
			struct buf_string kv = {0};

			url_encode(&kv, pass, strlen(pass));
			bufwrite(&kv.buf, &kv.len, &kv.pos, "=", 1);
			url_encode(&kv, val, strlen(val));

			if (!next_url) {
				next_url = apr_psprintf(r->pool, "?%s", kv.buf);
			} else if (!strstr(next_url, kv.buf)) {
				if (strchr(next_url, '?'))
					next_url = apr_psprintf(r->pool, "%s&%s", next_url, kv.buf);
				else
					next_url = apr_psprintf(r->pool, "%s?%s", next_url, kv.buf);
			}
			free(kv.buf);
		}
	}

	/* Redirect to next url */
	if (!next_url || strpbrk(next_url, "\r\n")) {
		/* Missing or bad next_url, goto first page */
		next_url = apr_pstrdup(r->pool, base_url);
	} else if (strncmp(next_url, "http://", 7) != 0 && next_url[0] != '/') {
		/* Prepend base_url to next_url */
		next_url = apr_psprintf(r->pool, "%s/%s", base_url, next_url);
	} else if (strncmp(next_url, "http://", 7) == 0) {
		/*TO ALLOW REDIR TO OTHER DOMAINS IS NECESSARY TO ADD A KEY IN BCONF*/
      	const char *allowed_url;
       	int i;
		int is_allowed = 0;
       	for (i = 0 ; (allowed_url = bconf_value(bconf_byindex(bconf_get(conf->root, "common.redir.allowed_url"), i))) ; i++) {
			if(strncmp(next_url, allowed_url, strlen(allowed_url)) == 0) {
				is_allowed = 1;
			}
		}
		if (!is_allowed) {
			next_url = apr_psprintf(r->pool, "%s%s", base_url, bconf_value(bconf_byindex(bconf_get(conf->root, "common.template.152.alias"), 0)));
		}
	}
	apr_table_add(r->headers_out, "Location", next_url);
	return HTTP_MOVED_TEMPORARILY;
}

static void
register_redir_hooks(apr_pool_t *p) {
	ap_hook_handler(redir_handler, NULL, NULL, APR_HOOK_MIDDLE);
	ap_hook_post_config(redir_post_config, NULL, NULL, APR_HOOK_MIDDLE);
}

