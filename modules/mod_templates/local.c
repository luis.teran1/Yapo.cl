#include "mod_templates.h"
#include "bsapi.h"
#include "bconf.h"

#include <query_string.h>
#include <http_log.h>
#include <apr_strings.h>
#include "memcacheapi.h"
#include "cookies.h"

void
templates_start_call_local(struct template_call *call, struct bconf_node *tmpl_root, const char **lang) {
	/* Backward compat "layout" */
	bpapi_insert(&call->ba, "l", "0");
}

