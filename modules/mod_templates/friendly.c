
#include <httpd.h>
#include <http_config.h>
#include <http_log.h>
#include <ctype.h>
#include <http_request.h>
#include <apr_strings.h>

#include "bconf.h"
#include "util.h"
#include "mod_templates.h"

static const char * 
get_uri_region(struct templates_conf *conf, char *uri, int *where) {
	const char *region = NULL;
	struct bconf_node *nearby_node = NULL;
	struct bconf_node *node = NULL;

	if (uri && strlen(uri) > 1) {
		uri[0] = tolower(uri[0]);
		if ((node = bconf_vget(conf->root, "common", "seo", "region", "number", uri, NULL)))  {
			region = bconf_value(node);
			
			if (region && bconf_get_int(conf->root, "common.seo.semantic.enabled") == 1) {
				*where = atoi(region) + 100;
			} else {
				*where = 1;
			}

		} else if ((node = bconf_vget(conf->root, "common", "seo", "city", "number", uri, NULL))) {
			region = bconf_value(node);
			*where = 0;

		} else if ( (nearby_node = bconf_vget(conf->root, "common", "seo", "nearby", "text", NULL)) ) {
			int i;
			const char *nearby_bconf = NULL;
			for (i = 0; ((nearby_bconf = bconf_value(bconf_byindex(nearby_node, i)))); i++) {
				const char *nearby = strstr(uri, nearby_bconf);
				if (nearby) {
					char *region_name = xstrndup(uri, nearby - uri - 1);
					if (!(node = bconf_vget(conf->root, "common", "seo", "region", "number", region_name, NULL)))
						node = bconf_vget(conf->root, "common", "seo", "city", "number", region_name, NULL);
					free(region_name);

					if (node) {
						region = bconf_value(node);
						*where = 2;
						break;
					}
				}
			}
		}

		if (!region && (node = bconf_vget(conf->root, "common", "seo", "allcountry", uri, "default", NULL)) ) {
			region = bconf_value(node);
			*where = 3;
		}
	}

	return region;
}

static const char * 
get_uri_category(struct templates_conf *conf, char *uri, const char *category_group) {
	const char *category;
	struct bconf_node *node = NULL;

	if (uri && strlen(uri) > 1) {	
		uri[0] = tolower(uri[0]);
		if (category_group) {
			node = bconf_vget(conf->root, "common", "seo", "subcat", "number", category_group, uri, NULL);
		} else {
			node = bconf_vget(conf->root, "common", "seo", "cat", "number", uri, NULL);
		}

		if (node && (category = bconf_value(node)))
			return category;
	}

	return NULL;
}


/*
 *   Translate uri from new and friendly style to old li style
 */
static char *
default_friendlyurl_converter(request_rec *r, struct templates_conf *conf) {
	/*Jump over first slash */ 
	char *uri = apr_pstrdup(r->pool, (r->uri) + 1);	
	char *token = NULL;
	char *tok_state = NULL;

	token = strtok_r(uri, "/", &tok_state);

	if (token) {
		const char *region = NULL;
		int where = 0;

		region = get_uri_region(conf, token, &where);
	
		if (region) {
			char *category = NULL;
			char *subcategory = NULL;
			char *qs = NULL;

			if (r->args != NULL) {
				qs = apr_pstrdup(r->pool, r->args);
			} else {
				qs = apr_pstrdup(r->pool, "");
			}

			/* apr_pstrdup must have failed */
			if (qs == NULL)
				return NULL;

			token = strtok_r(NULL, "/", &tok_state);
			category = (char *) get_uri_category(conf, token, NULL);
			if ( (token = strtok_r(NULL, "/", &tok_state)) && category)
				subcategory = (char *)get_uri_category(conf, token, category);
			
			if (region != NULL && (!r->args || (r->args && !strstr(r->args, "ca="))))
				qs = apr_psprintf(r->pool, "%s&ca=%s_s", qs, region);
		
			if (where && (!r->args || (r->args && !strstr(r->args,"w=")))) 
				qs = apr_psprintf(r->pool, "%s&w=%d", qs, where);

			if (category != NULL && (!r->args || (r->args && category && !strstr(r->args,"cg="))))
				qs = apr_psprintf(r->pool, "%s&cg=%s", qs, category);

			if (subcategory != NULL && (!r->args || (r->args && subcategory && !strstr(r->args,"c="))))
				qs = apr_psprintf(r->pool, "%s&c=%s", qs, subcategory);

			if (qs[0] == '&')
				return qs + 1;
			return qs;
		}
	}

	return NULL;
}

//char *(*mod_list_friendlyurl_converter)(request_rec *r, struct list_conf *conf) = default_friendlyurl_converter;


