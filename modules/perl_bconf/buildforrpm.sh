#!/bin/bash
echo $1

if [ -z "$RPM_BUILD_ROOT" -a -n $1 ] ; then
    RPM_BUILD_ROOT=$1
elif [ -z "$RPM_BUILD_ROOT" ] ; then
    echo "must provide RPM_BUILD_ROOT as envvar or arg 1"
    exit 1
fi

cd modules/perl_bconf/
if [ $? != 0 ] ; then
   $0 must be called from TOPDIR
   exit 1
fi

# File juggling to be able to do proper OS install
mv local.mk local.mk.cvs
mv Makefile Makefile.blocket
perl Makefile.PL PREFIX=$RPM_BUILD_ROOT/usr INSTALLDIRS=vendor
mv Makefile local.mk
mv Makefile.blocket Makefile
touch Makefile
make
#rm -rf $RPM_BUILD_ROOT
make install
mv local.mk.cvs local.mk
[ -x /usr/lib/rpm/brp-compress ] && /usr/lib/rpm/brp-compress
find $RPM_BUILD_ROOT \( -name perllocal.pod -o -name .packlist -o -name '*3pm.gz' \) -exec rm -vf {} \;


