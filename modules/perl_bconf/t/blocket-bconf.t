# Before `make install' is performed this script should be runnable with
# `make test'. After `make install' it should work as `perl blocket-bconf.t'

#########################

# change 'tests => 1' to 'tests => last_test_to_print';

use Test::More tests => 1;
BEGIN { use_ok('blocket::bconf') };

#########################

# Insert your test code below, the Test::More module is use()ed here so read
# its man page ( perldoc Test::More ) for help writing this test script.

$bc = blocket::bconf::init_conf('/opt/blocket');
print $bc->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'name'} . 
	":" . $bc->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'port'} ."\n";

