#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"

#include "bconfig.h"
#include "bconf.h"
#include "transapi.h"
#include "vtree.h"

XS(boot_blocket__bconf) EXPORTED;

MODULE = blocket::bconf		PACKAGE = blocket::bconf		

PROTOTYPES: ENABLE

SV *
init_conf(basedir)
	char *basedir;
	INIT:
	char *trans_conf_filename;
	char *blocket_id;
	struct bconf_node *conf;
	struct stat conf_stat;
	transaction_t *trans;
	char *key;
	char *value;
	HV * rethv;
	rethv = newHV();
	SV * leafval;
	struct bpapi_vtree_chain vtree;
	CODE:
	if (stat(basedir, &conf_stat) == 0 && S_ISREG(conf_stat.st_mode))
		asprintf(&trans_conf_filename, "%s", basedir);
	else	
		asprintf(&trans_conf_filename, "%s/conf/bconf.conf", basedir);

        if ((conf = config_init(trans_conf_filename)) == NULL) {
                croak("Failed to read bconf configuration file %s", trans_conf_filename);
		
        }

        if ((blocket_id = (char*) bconf_get_string(conf, "blocket_id")) == NULL) {
                croak("Failed to get blocket_id from configuration file %s", trans_conf_filename);
        }

        trans = trans_init_vtree(10000, bconf_vtree(&vtree, conf));

        trans_add_pair(trans, "cmd", "bconf", strlen("bconf"));
        trans_add_pair(trans, "host", blocket_id, strlen(blocket_id));
        trans_add_pair(trans, "commit", "1", strlen("1"));

        if (trans_commit(trans) != 0) {
		vtree_free(&vtree);
		XSRETURN_UNDEF;
        }
	vtree_free(&vtree);
        while (trans_iter_next(trans, &key, &value)) {
                char *keysep;
                char *k;
                int cnt;
		HV * hvp;

                if (strcmp(key, "conf") != 0) {
                        continue;       
                }

                key = value;
                value = strchr(value , '=');
                if (!value) {
                        continue;
                }
                *value = '\0';
                value++;

                /* bconf_add_data(&bconf_root, key, value); */

                keysep = key;
		hvp = rethv;

                for (cnt = 0 ; (k = strsep(&keysep, ".")); cnt++) {
		        HV * newhv;
			SV **fetched;
                        if (!keysep) {
                                break;
			}
                        if (hv_exists(hvp, k, strlen(k))) {
			/*	printf ("hash for %s already there\n",k); */
				if ((fetched = hv_fetch(hvp,k,strlen(k),0)) == NULL)
					croak("BConf array is borked\n");
				if (!SvROK(*fetched))
					croak("BConf array contains non-RV\n");
				hvp = (HV *) SvRV(*fetched);
			} else {
				newhv = newHV();
				(void)hv_store(hvp,k,strlen(k), newRV((SV *)newhv),0);
		            /*  printf("stored %x in %x with key %s\n",(unsigned int)newhv,(unsigned int)hvp,k); */
				hvp = newhv;
                        } 
                }

		leafval = sv_2mortal((SV *) newSVpv(value,strlen(value)));
		if (looks_like_number(leafval)) {
			(void)hv_store(hvp,k,strlen(k), newSViv(atoi(value)),0);
		} else {
			(void)hv_store(hvp, k, strlen(k), newSVpv(value,strlen(value)),0);
		}
        }
	trans_free(trans);
	RETVAL = newRV((SV *) rethv);
	OUTPUT:
	RETVAL


