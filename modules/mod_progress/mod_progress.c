#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <ctype.h>

#ifdef WIN32
#include <direct.h>
#else
#include <sys/types.h>
#include <unistd.h>
#endif

#if !defined(OS2) && !defined(WIN32) && !defined(BEOS) && !defined(NETWARE)
#include "unixd.h"
#define __SET_MUTEX_PERMS
#endif

#include "ap_config.h"
#include "httpd.h"
#include "http_config.h"
#include "http_request.h"
#include "http_core.h"
#include "http_log.h"
#include "http_protocol.h"
#include "util_script.h"
#include "ap_mpm.h"

#include "apr.h"
#include "apr_strings.h"
#include "apr_hash.h"
#include "apr_user.h"
#include "apr_lib.h"
#include "apr_signal.h"
#include "apr_global_mutex.h"

#include "bconf.h"
#include "util.h"
#include "cacheapi.h"
#include "cookies.h"
#include "mod_bconf_extern.h"

#include <time.h>

#include "bsapi.h"

struct progress_conf {
	struct bconf_node *root;
	struct shadow_vtree vbconf;
	struct cache_pools session;
};

struct progress_filter_context {
	char key[64];
	char value[64];
	off_t total_bytes;
	time_t sec;
	const char *content_length;
	const char *session_id;
	struct progress_conf *conf;
};

extern module AP_MODULE_DECLARE_DATA progress_module;

static void *
create_progress_config(apr_pool_t *pool, server_rec *s) {
	return apr_pcalloc(pool, sizeof(struct progress_conf));
}

static int
progress_post_config(apr_pool_t *pool, apr_pool_t *plog, apr_pool_t *ptemp, server_rec *s) {
	struct progress_conf *conf;
	struct bconf_node *br;

	for ( ; s != NULL; s = s->next) {
		if ((br = mod_bconf_get_root(s)) == NULL) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, OK, NULL, "mod_progress - Failed to get bconf root.");
			return HTTP_INTERNAL_SERVER_ERROR;
		}
		conf = ap_get_module_config(s->module_config, &progress_module);
		conf->root = br;
		bconf_vtree(&conf->vbconf.vtree, conf->root);
		cache_init_pools(&conf->session, &conf->vbconf.vtree);
	}

	return OK;
}

static apr_status_t
progress_filter(ap_filter_t *f, apr_bucket_brigade *bb, ap_input_mode_t mode, apr_read_type_e block, apr_off_t nbytes) {
	struct progress_filter_context *ctx = f->ctx;
	apr_status_t status = ap_get_brigade(f->next, bb, mode, block, nbytes);
	apr_off_t len;
	time_t sec;

	apr_brigade_length(bb, 1, &len);

	sec = time(NULL);

	ctx->total_bytes += len;
	if (sec != ctx->sec) {
		ctx->sec = sec;

		/* Save data in cache and expire in 1 minute */
		snprintf(ctx->value, sizeof(ctx->value), "%lld/%s", (long long)ctx->total_bytes, ctx->content_length);
	}

	return status;
}

static char *
get_arg(request_rec *r, char const *opt) {
	const char *inptr;
        char *outptr = NULL, *output = NULL;

	if (r->args && *r->args) {
		if ((inptr = strstrptrs(r->args, opt, NULL, "=&")) != NULL) {
			output = apr_palloc(r->pool, strlen(r->args) + 1);
			outptr = output;

			inptr += strlen(opt) + 1;
			while ( *inptr != '&' && *inptr != '\0' )
				*outptr++ = *inptr++;
			*outptr++ = '\0';
		}
	}

	return output;
}

static int
progress_filter_init(ap_filter_t *f) {
	struct progress_filter_context *ctx;
	char *upload_instance_id;
	unsigned int i = 0;
	struct progress_conf *conf;

	if (f->r == NULL) {
		/* ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "progress init NO request!"); */
		return -1;
	}

	if (f->r->path_info == NULL) {
		return -1;
	}


	if (!f->r->uri || !f->r->server) {
		return -1;
	}

	conf = ap_get_module_config(f->r->server->module_config, &progress_module);
	if (!conf)
		return -1;

	/* Create filter context space */
	ctx = apr_palloc(f->r->pool, sizeof(*ctx));
	ctx->total_bytes = 0;
	ctx->sec = time(NULL);
	ctx->content_length = apr_table_get(f->r->headers_in, "Content-Length");
	ctx->conf = conf;
	f->ctx = ctx;

	/* Session id */
	if (f->r->args && strstrptrs(f->r->args, "s", NULL, "&="))
		ctx->session_id = get_arg(f->r, "s");
	else
		ctx->session_id = get_cookie_with_count(f->r, "s", NULL);

	if (ctx->session_id == NULL)
		return -1;

	/* Upload id */
	upload_instance_id = apr_pstrdup(f->r->pool, f->r->uri);
	if (upload_instance_id == NULL)
		return -1;

	/* Convert invalid chars to underscore [^a-z0-9] */
	for (i = 0; i < strlen(upload_instance_id); i++) {
		if (!isdigit(upload_instance_id[i]) && (upload_instance_id[i] < 'a' || upload_instance_id[i] > 'z'))
			upload_instance_id[i] = '_';
	}
	snprintf(ctx->key, sizeof(ctx->key), "progress%s", upload_instance_id);

	/* Set upload progress */
	snprintf(ctx->value, sizeof(ctx->value), "%lld/%s", (long long)ctx->total_bytes, ctx->content_length);
	cache_set(&ctx->conf->session, ctx->session_id, ctx->key, ctx->value, 60);

	/* Debug */
	/*
	ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "progress query_string:%s, s:%s", f->r->args, get_arg(f->r, "s"));
	ap_log_error(APLOG_MARK, APLOG_CRIT, OK, NULL, "progress init %s session_id:%s key:%s", f->r->the_request, ctx->session_id, ctx->key);
	 */

	return 0;
}

static void
register_hooks(apr_pool_t *p) {
	ap_register_input_filter("PROGRESS_IN", progress_filter, progress_filter_init, AP_FTYPE_RESOURCE);
	ap_hook_post_config(progress_post_config, NULL, NULL, APR_HOOK_MIDDLE);
}

module AP_MODULE_DECLARE_DATA progress_module EXPORTED = {
	STANDARD20_MODULE_STUFF,
	NULL,
	NULL,
	create_progress_config,
	NULL,
	NULL,
	register_hooks
};
