%define blocket /opt/blocket
%define cwd %(pwd)
%define branchid 1_85
%define bpvschema bpv
%define topdir %(cd ../..;pwd)
%define _rpmdir %topdir/rpm
Summary: Blocket
Name: blocket
Version: 1.85
Release: %(expr `%{topdir}/platform/scripts/build/buildversion.sh` - 20820)
License: Proprietary
Group: Blocket/Applications
URL: http://www.yapo.cl

BuildRoot: %{topdir}/dest-i686/%{name}-root

%description
These are the noarch applications that make up www.yapo.cl

%package plsqlmodules
Summary: postgres modules only to be installed on db servers
Group: Blocket/Applications
#BuildArch: x86_64 

%description plsqlmodules
Only install on db hosts!

%preun plsqlmodules
cat << 'EOEOEO2' | su - postgres -c "psql blocketdb"
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat %{topdir}/modules/psql_uuid/uuid_remove.rpm-sql)
COMMIT;
EOEOEO2

%post plsqlmodules
cat << 'EOEOEO2' | su - postgres -c "psql blocketdb"
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat %{topdir}/modules/psql_uuid/uuid_install.rpm-sql)
COMMIT;
EOEOEO2

%install
# for pachage blocket-plsqlprocs
mkdir -p $RPM_BUILD_ROOT%{blocket}/modules
cd %cwd
%ifarch x86_64
cc -c -O2 -g -fno-omit-frame-pointer -pipe -Wall -Werror -Wwrite-strings -Wpointer-arith -Wcast-align -Wsign-compare -I/usr/include/pgsql/server -I%topdir/lib/common -fpic -shared -DPIC -Wold-style-definition  -o pguuid.soo pguuid.c
cc  -fpic -shared -o psql_uuid.so pguuid.soo -luuid -Wl,-T -Wl,%topdir/scripts/linker/default.x86_64
%else
cc -c -O2 -g -fno-omit-frame-pointer -pipe -Wall -Werror -Wwrite-strings -Wpointer-arith -Wcast-align -Wsign-compare -I/usr/include/pgsql/server -I%topdir/lib/common -shared -Wold-style-definition -m32  -o pguuid.soo pguuid.c
cc -shared -o psql_uuid.so pguuid.soo -luuid -m32
%endif
/usr/bin/install psql_uuid.so $RPM_BUILD_ROOT%{blocket}/modules

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files plsqlmodules
%defattr(-,root,root)
%{blocket}/modules/psql_uuid.so
