/*
(C) Claudiu RAVEICA

*/

#include <uuid/uuid.h>
#include <postgres.h>
#include <fmgr.h>

Datum uuid(void);

#pragma GCC visibility push(default)

PG_MODULE_MAGIC;

PG_FUNCTION_INFO_V1(uuid);

Datum
uuid(void)
{
	text *p;
	uuid_t uu;
	char buffer[37];
	int len;

	uuid_generate(uu);
	uuid_unparse(uu, buffer);
	len = strlen (buffer);
	p = palloc(len + VARHDRSZ);
#ifdef SET_VARSIZE
	SET_VARSIZE(p, len + VARHDRSZ);
#else
	p->vl_len = len + VARHDRSZ;
#endif
	memcpy(VARDATA(p), buffer, len);

	PG_RETURN_TEXT_P(p);
}

