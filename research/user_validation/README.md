User validation
===============

This document is registry for the proposed queries to implement the first step of the User Validation project.

Percent of approved ads on the user's lifetime
----------------------------------------------

The proposed query is:
```sql
with t1 as (
  select state, count(*) as n
  from ad_actions join ads using(ad_id)
  where action_id = 1 and user_id = 1444700
  group by state
)
select perc as accepted_perc from (
  select state, 100.0 * n / sum(n) over () as perc from t1
) t2
where state = 'accepted';

    accepted_perc
---------------------
90.2485659655831740
```

The data before condensing is:
```sql
select state, count
from ad_actions join ads using(ad_id)
where action_id = 1 and user_id = 1444700
group by state;

  state  | count
---------+------
accepted | 944
refused  | 102
```

This approach is compatible with using the archive schemas `blocket_*`.

Has the user bought premium products?
-------------------------------------

The proposed query is:
```sql
select count(*) > 0 as has_bought
from purchase
where account_id = 3426438 and status = 'confirmed';

 has_bought
------------
 t
```

The data before condensing is:
```sql
select purchase_id, status
from purchase
where account_id = 3426438;

 purchase_id |  status
-------------+-----------
     1333420 | pending
     1333422 | pending
     1333433 | pending
     1333437 | pending
     1333439 | pending
     1333441 | pending
     1333445 | pending
     1766269 | confirmed
```

Percent of ads sold via yapo
----------------------------

The proposed query is:
```sql
with t1 as (
  select aps.value as reason, count(*) as n
  from blocket_2015.action_params aps join blocket_2015.ads using (ad_id)
  where user_id = 215122 and aps.name = 'deletion_reason'
  group by aps.value
)
select perc as yapo_sold from (
  select reason, 100.0 * n / sum(n) over () as perc from t1
) t2
where reason = '1';

      yapo_sold
---------------------
 50.0000000000000000
 ```
 
 The data before condensing is:
 ```sql
 select aps.value, count(*) as reason
 from blocket_2015.action_params aps join blocket_2015.ads using (ad_id)
 where user_id = 215122 and aps.name = 'deletion_reason'
 group by aps.value;
 
 value | reason
-------+--------
 6     |      2
 1     |      2
 ```

Months with active ads:
-----------------------

This query is well beyond my knowledge.
I studied some solutions to similar problems, and I'm pasting links here for reference:

https://stewashton.wordpress.com/2014/03/16/merging-contiguous-date-ranges/
http://dba.stackexchange.com/questions/96974/check-if-contiguous-date-interval-exists
http://dba.stackexchange.com/questions/17045/efficiently-select-beginning-and-end-of-multiple-contiguous-ranges-in-postgresql

The complexity is high, specially if it's going to run for every user on the database.
I rather think about this more deeply and truly understand the costs of studied solutions before committing to an implementation.
