ifdef USE_GEOIP
LDLIBS+=-L${TOPDIR}/build/lib/libgeoip -lgeoip
CPPFLAGS+=-I${TOPDIR}/lib/libgeoip/GeoIP-1.4.8/libGeoIP
EXTRADEPEND+=${TOPDIR}/build/lib/libgeoip/libgeoip.a

${TOPDIR}/build/lib/libgeoip/libgeoip.a:
	exec ${MAKE} -C ${TOPDIR}/lib/libgeoip
endif

ifdef USE_SITE_FUNCTIONS
VPATH:=${TOPDIR}/lib/template/template_functions:${VPATH}
SRCS+= tflocal.c
endif

ifdef USE_SITE_FILTERS
VPATH:=${TOPDIR}/lib/template/template_filters:${VPATH}
SRCS+= bn_tpfilters.c
endif

#ifdef USE_MOBILE_TEMPLATE_API || USE_MSITE_TEMPLATES
include ${TOPDIR}/mk/modules_paths.mk 
#endif
