LESSFILES=*.less

less:
	echo "Compiling all LESS files to CSS"; \
	for file in ${LESSFILES}; do \
		echo $$file; \
		lessc $$file > ../$${file/.*/.css.in}; \
	done \
