include ${TOPDIR}/platform/mk/regressinit.mk
include ${TOPDIR}/mk/defvars.mk
include ${TOPDIR}/mk/colors.mk

LOCK_PROG=${TOPDIR}/util/lock
UNLOCK_PROG=${TOPDIR}/util/unlock
REGRESS_CONF_DIR=${TOPDIR}/regress_final/conf
REGRESS_DIR=${TOPDIR}/regress_final
REGRESS_LOG_DIR=${TOPDIR}/regress_final/logs
REGRESS_SCRATCH_DIR=${TOPDIR}/regress/scratch
REGRESS_MODULES_DIR=${TOPDIR}/regress_final/modules/
REGRESS_BIN_DIR=${TOPDIR}/regress_final/bin
REGRESS_INCLUDE_DIR=${TOPDIR}/regress_final/include
REGRESS_TMP_DIR=${TOPDIR}/regress_final/tmp
REGRESS_LOG=${REGRESS_LOG_DIR}/${USER}-regress.log
REGRESS_LIBEXEC_DIR=${TOPDIR}/regress_final/libexec
REGRESS_WWW=${TOPDIR}/regress_final/www
REGRESS_WWW_SSL=${TOPDIR}/regress_final/www-ssl

NINJA_DEST=${TOPDIR}/ninja_build/regress
NINJA_BIN_DIR=${NINJA_DEST}/bin
NINJA_DEST_REGRESS=${NINJA_DEST}/regress
DEST=${TOPDIR}/ninja_build/regress
DEST_BIN=${TOPDIR}/ninja_build/regress/bin
NINJA_PGSQL=${NINJA_DEST}/pgsql

SYSLOGHOOKSO=${DEST}/modules/sysloghook.so
SYSLOGHOOK=env SYSLOGROOT=${REGRESS_LOG_DIR} LD_PRELOAD=${SYSLOGHOOKSO}

WM:=$(shell which openbox 2> /dev/null)

ifndef WM
	WM:=$(shell which blackbox)
endif

HTTPD_OPS:=-D REGRESS -D VER_2_${APACHE_MINOR}
ifeq ("${REGRESS_CSS_REDIRECT}", "true")
       HTTPD_OPS:=-D REGRESS -D VER_2_${APACHE_MINOR} -D CSS_REDIRECT
endif
ifeq ("${REGRESS_JS_PP_REDIRECT}", "true")
       HTTPD_OPS:=-D REGRESS -D VER_2_${APACHE_MINOR} -D JS_PP_REDIRECT
endif

pgsql-build-start:
	@echo -e "\\033[1;35mStarting postgres\\033[39;0m"
	@if (cd ${TOPDIR}; util/lock pgsql_build${GENPORTOFF}); then \
		rm -rf ${REGRESS_PGSQL_HOST}; \
		mkdir -p ${REGRESS_PGSQL_HOST}; \
		cp -rp ${TOPDIR}/pgsql ${REGRESS_PGSQL_HOST}; \
		(cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} REGRESS_DIR=${REGRESS_DIR} REGRESS_PG_TCP_PORT=${REGRESS_PG_TCP_PORT} TOPDIR=${TOPDIR} ./start-build-pgsql.sh ${REGRESS_PGSQL_HOST} ${PGVERSION}); \
	else \
		echo -e "${RED}Postgres is already running or there's a leftover lock ${NO_COLOR}"; true; \
	fi

pgsql-build-stop: 
	@echo -e "\\033[1;35mStopping postgres\\033[39;0m"
	@if (cd ${TOPDIR}; util/unlock pgsql_build${GENPORTOFF}); then \
		(cd ${TOPDIR}/scripts/db && exec env TOPDIR=${TOPDIR} ./stop-build-pgsql.sh ${REGRESS_PGSQL_HOST}); \
		rm -rf ${REGRESS_PGSQL_HOST}; \
	else \
		true; \
	fi

pgsql-update-procs:
	@echo -e "\\033[1;35mUpdating or adding plsqlprocs \\033[39;0m"
	(cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} TOPDIR=${TOPDIR} ./update-plsqlprocs.sh ${REGRESS_PGSQL_HOST} ${PGVERSION});

db-snap-%:
	printf "cmd:db_snap\naction:snap\nname:$(subst db-snap-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-store-%:
	printf "cmd:db_snap\naction:store\nname:$(subst db-store-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-restore-%:
	make -C ${TOPDIR} redissession-flushall
	printf "cmd:db_snap\naction:restore\nname:$(subst db-restore-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-load-%:
	cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ./load_data.sh ${REGRESS_PGSQL_HOST} $(subst db-load-,,$@)

dbfile-store-%:
	cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} ./backup_data.sh ${REGRESS_PGSQL_HOST} $(subst dbfile-store,db-snap,$@) 1

dbfile-restore-%:
	cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} ./restore_data.sh ${REGRESS_PGSQL_HOST} $(subst dbfile-restore,db-snap,$@)

redis-start-%:
	@SERVER="`echo $@ | sed s,redis-start-,,`"; \
	if [ -f ${REGRESS_FINALDIR}/conf/redis_$$SERVER.conf ]; then \
		if (cd ${TOPDIR}; util/lock redis_$$SERVER\_build${GENPORTOFF}); then \
			echo -e "\\033[1;35mStarting redis $$SERVER \\033[39;0m"; \
			mkdir -p ${REGRESS_FINALDIR}/redis_$$SERVER >/dev/null 2>&1; \
	   		if [ -x "${REGRESS_REDIS_SERVER}" ]; then \
				(exec ${REGRESS_REDIS_SERVER} ${REGRESS_FINALDIR}/conf/redis_$$SERVER.conf); \
			else \
				(exec ${REGRESS_FINALDIR}/bin/redis-server ${TOPDIR}/regress_final/conf/redis_$$SERVER.conf); \
			fi; \
			try=0; while ! redis-cli -p $(shell sed -nre 's/^port //p' ${TOPDIR}/regress_final/conf/redis_$*.conf) < /dev/null && ((try++ < 100)); do sleep 0.1 ; done; \
		else \
			echo -e "lock file: redis_$$SERVER\_build${GENPORTOFF}" && \
       		echo -e "\\033[1;35mLock file present for server $$SERVER\\033[39;0m" && \
			true; \
		fi \
	else \
		echo -e "\\033[1;35mNo conf available for $$SERVER \\033[39;0m"; \
		false; \
	fi

redis-stop-%:
	@SERVER="`echo $@ | sed s,redis-stop-,,`"; \
	if (cd ${TOPDIR}; util/unlock redis_$$SERVER\_build${GENPORTOFF}); then \
		echo -e "\\033[1;35mStopping redis_$$SERVER\\033[39;0m" && \
		rdpid=$$(< ${TOPDIR}/.redis_$$SERVER.pid); \
		kill $$rdpid && rm  -f ${TOPDIR}/.redis_$$SERVER.pid; \
		while kill -0 $$rdpid 2> /dev/null ; do sleep 0.1; echo -n "."; done; \
	else \
		echo -e "\\033[1;35mService $$SERVER not running\\033[39;0m"; \
	fi 

redis-load-cardata:
	@echo -e "\\033[1;35mLoading redis_cardata\\033[39;0m"; \
	(mkdir -p ${REGRESS_FINALDIR}/redis_cardata >/dev/null 2>&1 && cp ${TOPDIR}/util/sii_car_data/redis_cardatadump.rdb ${TOPDIR}/regress_final/redis_cardata/); \
	make redis-stop-cardata; \
	make redis-start-cardata


redis-load-wordsranking:
	@echo -e "\\033[1;35mLoading redis_wordsranking\\033[39;0m"; \
	make redis-stop-wordsranking; \
	(mkdir -p ${REGRESS_FINALDIR}/redis_wordsranking >/dev/null 2>&1 && cp ${TOPDIR}/util/ad_not_found/redis_wordsranking.dump.rdb ${TOPDIR}/regress_final/redis_wordsranking/); \
	make redis-start-wordsranking
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} SCRIPT LOAD "`cat ${REGRESS_FINALDIR}/scripts/rank.lua`"


redis-load-email:
	@echo -e "\\033[1;35mLoading redis email (this process may take some time)\\033[39;0m"; \
	(mkdir -p ${TOPDIR}/scripts/fill_redis_email/tmp >/dev/null 2>&1); \
	(cd ${TOPDIR}/scripts/fill_redis_email && exec ./fill_redis_email.sh -h ${REGRESS_PGSQL_HOST} -p 5432 -d blocketdb -u ${REGRESS_BLOCKET_ID} -R localhost -P ${REGRESS_REDISEMAIL_PORT} -w tmp -l 1000000 -n 16) &> /dev/null


memcache-build-start:
	@echo -e "${PURPLE}Starting memcache${NO_COLOR}";
	@if (cd ${TOPDIR}; ${LOCK_PROG} memcache_${GENPORTOFF}); then \
		(cd ${REGRESS_DIR} && exec memcached -p${REGRESS_MCPORT} -d -P ${TOPDIR}/.mcpid); \
		try=0; while ! nc localhost ${REGRESS_MCPORT} < /dev/null && (( try++ < 100 )) ; do sleep 0.1 ; done; \
	else \
		echo -e "${RED}Memcached is already running or there's a leftover lock ${NO_COLOR}"; true; \
	fi

memcache-build-stop:
	@echo -e "${PURPLE}Stopping memcache${NO_COLOR}";
	@if (cd ${TOPDIR}; ${UNLOCK_PROG} memcache_${GENPORTOFF}); then \
		mcpid=$$(< ${TOPDIR}/.mcpid); \
		kill $$mcpid && rm ${TOPDIR}/.mcpid; \
		while kill -0 $$mcpid ; do sleep 0.1 ; done; \
	else \
		true; \
	fi

regress-fuzzystrmatch rfuzzy:
	@echo "Trying to install fuzzy data"; \
	psql -h ${REGRESS_PGSQL_HOST} blocketdb -f ${TOPDIR}/build/modules/psql_fuzzy/fuzzystrmatch.sql -q

apache-regress-start:
	@echo -e "\\033[1;35mStarting apache\\033[39;0m"
	@if (cd ${TOPDIR}; util/lock apache_regress${GENPORTOFF}); then \
		(cd ${REGRESS_FINALDIR}/modules; for a in `find ${HTTPD_MOD_DIR} -name '*.so'` ; do ln -fs $$a ; done) ; \
		(cd ${REGRESS_FINALDIR}/modules; for a in `find ${PHP_EXT_DIR}  -type f` ; do ln -fs $$a ; done) ; \
		mkdir ${REGRESS_FINALDIR}/logs > /dev/null 2>&1 || true ; \
		mkdir -p ${TOPDIR}/regress/scratch/certcache > /dev/null 2>&1 || true ; \
		chmod 777 ${REGRESS_FINALDIR}/logs > /dev/null 2>&1 ; \
		mkdir -p ${REGRESS_FINALDIR}/www-ssl > /dev/null 2>&1 || true ; \
		touch ${REGRESS_FINALDIR}/www-ssl/index.htm ; \
		mkdir -p ${REGRESS_FINALDIR}/msite > /dev/null 2>&1 || true ; \
		touch ${REGRESS_FINALDIR}/msite/index.htm ; \
		${TOPDIR}/scripts/apache-testcert.sh ${REGRESS_FINALDIR} ${TOPDIR}/regress/scratch/certcache ; \
		env TEMPLATES_PATH=${TEMPLATES_PATH} LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ${HTTPD} ${HTTPD_OPS} -c "PidFile httpd.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \
		env TEMPLATES_PATH=${TEMPLATES_PATH} LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ${HTTPD_WORKER} ${HTTPD_OPS} -c "PidFile httpd.worker.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \
		try=0 ; echo -n "Waiting for apache startup"; while [ ! -f ${REGRESS_FINALDIR}/httpd.pid -o ! -f ${TOPDIR}/regress_final/httpd.worker.pid ] && (( try++ < 100 )); do echo -n "."; sleep 0.2; done ; echo; \
		if ((try == 101)); then \
			exit 1; \
		fi; \
		true; \
	fi

apache-regress-stop:
	@echo -e "\\033[1;35mStopping apache\\033[39;0m"
	@if (cd ${TOPDIR}; util/unlock apache_regress${GENPORTOFF}; test "$$?" != "2"); then \
		httppid=`cat ${REGRESS_FINALDIR}/httpd.pid`; \
		httpworkerpid=`cat ${REGRESS_FINALDIR}/httpd.worker.pid`; \
		echo -e "\\033[1;35mStopping apache ($$httppid)\\033[39;0m"; \
		echo -e "\\033[1;35mStopping apache ($$httpworkerpid)\\033[39;0m"; \
		echo -n "Waiting for apache to shutdown"; while [[ -n "$$httppid" && -n "$$httpworkerpid" && (-a /proc/$$httppid || -a /proc/$$httpworkerpid) ]]; do echo -n "."; kill $$httppid; kill $$httpworkerpid; sleep 0.2; done; echo ; \
	fi

apache-regress-restart:
	@echo -e "\\033[1;35mRestarting apache\\033[39;0m"
	${CMD_PREFIX} ${HTTPD} -k restart ${HTTPD_OPS} -c "PidFile httpd.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \
	${CMD_PREFIX} ${HTTPD_WORKER} -k restart ${HTTPD_OPS} -c "PidFile httpd.worker.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \

apache-regress-graceful:
	@echo -e "\\033[1;35mGracefulling apache\\033[39;0m"
	${CMD_PREFIX} ${HTTPD} -k graceful ${HTTPD_OPS} -c "PidFile httpd.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \
	${CMD_PREFIX} ${HTTPD_WORKER} -k graceful ${HTTPD_OPS} -c "PidFile httpd.worker.pid" -f `readlink -f ${REGRESS_FINALDIR}/conf`/httpd.conf ; \

trans-regress-start:
	@echo -e "\\033[1;35mStarting trans\\033[39;0m"
	(cd ${TOPDIR}; util/lock trans_regress${GENPORTOFF}) && (cd ${REGRESS_FINALDIR}/bin && { setsid env TEMPLATES_PATH=${TEMPLATES_PATH} PGOPTIONS="-c search_path=${BPVSCHEMA},public" LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ./trans --debug -f --config ../conf/trans.conf < /dev/null > /dev/null 2>> ${REGRESS_LOG_DIR}/trans.log & } && echo "$$!" > ${SOURCE_PATH}/.transpid) 
	echo -n "Waiting for trans to start"; while ! nc localhost ${REGRESS_TRANS_CPORT} < /dev/null > /dev/null && ((try++ < 50)); do echo -n "."; sleep 0.5; done ; echo; \
	if ((try == 51)); then \
		echo -e "\\033[1;33mWARNING: Trans non-responsive, running '---check-only=bconf,dbprocs' on trans\\033[39;0m"; \
		${REGRESS_FINALDIR}/bin/trans --check-only=bconf,dbprocs --debug -f --config ${TOPDIR}/regress_final/conf/trans.conf; \
		if [ $$? -ne 0 ]; then echo -e "\\033[1;31mTRANS ERROR -- CHECK YOUR TRANSACTION LOG!\\033[39;0m" ; echo "-----" ; tail -n 4 ${REGRESS_LOG_DIR}/trans.log ; echo "-----" ; exit 1; fi; \
	fi
	mkdir -p ${REGRESS_FINALDIR}/tmp/blocket

trans-regress-stop:
	@echo -e "\\033[1;35mStopping trans\\033[39;0m"
	@if (cd ${TOPDIR}; util/unlock trans_regress${GENPORTOFF}); then \
		kill `cat ${TOPDIR}/.transpid` ; rm ${TOPDIR}/.transpid; \
	else \
		true; \
	fi

trans-regress-reload:
	kill -HUP `cat ${TOPDIR}/.transpid`

rabbitmq-regress-status:
	@echo -e "\\033[1;35mChecking rabbitmq\\033[39;0m"
	@if (${DEST_BIN}/regress-rabbitmq rabbitmqctl status &> /dev/null); then \
		echo "RabbitMQ service seems to be running"; \
		true; \
	else \
		echo "RabbitMQ service seems to be stopped"; \
		false; \
	fi

rabbitmq-regress-restart: rabbitmq-regress-stop rabbitmq-regress-start

filterd-regress-start:
	@echo -e "\\033[1;35mStarting filterd\\033[39;0m"
	(${LOCK} filterd) && (${SYSLOGHOOK} ${CMD_PREFIX} ${DEST_BIN}/filterd --quick-start --config ${REGRESS_CONF_DIR}/filterd.conf --pidfile ${TOPDIR}/.filterd.pid &)
	while ! test "$$(curl -s --write-out "%{http_code}" http://localhost:${REGRESS_FILTERD_CONTROLLER_PORT}/stats -o /dev/null)" = 200; do sleep 0.1 ; done ; true

filterd-regress-stop:
	@echo -e "\\033[1;35mStopping filterd\\033[39;0m"
	if (${UNLOCK} filterd); then \
		kill `cat ${TOPDIR}/.filterd.pid` ; \
		rm ${TOPDIR}/.filterd.pid ; \
		while lsof -nP -u $$(id -u) | fgrep -q 'TCP *:${REGRESS_FILTERD_CONTROLLER_PORT}' ; do sleep 0.1 ; done ; \
	fi ; true

filterd-regress-reload:
	@echo -e "\\033[1;35mReloading filterd\\033[39;0m"
	curl http://localhost:${REGRESS_FILTERD_CONTROLLER_PORT}/reload

asearch-regress-full-index:
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl reindex asearch)

asearch-regress-merge-index-full:
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ./index_merge $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/asearch.conf).{index,new})

asearch-regress-clean-index:
	rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).index[0-9]
	rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).old

asearch-regress-delete-index:
	rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf){,.new,.index}

asearch-regress-start-engine:
	(cd ${TOPDIR}; util/lock asearch_regress${GENPORTOFF})
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ./search --config ../conf/asearch.conf --logtag asearch --pidfile ${TOPDIR}/.asearchpid --timeout 2000 --quick-start)
	try=0; while ! nc localhost $(shell sed -nre 's/.*port.search ?=//p' ${REGRESS_FINALDIR}/conf/asearch.conf) < /dev/null && ((try++ < 100)); do sleep 0.1 ; done

update-aindex:
	env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${REGRESS_FINALDIR}/bin/aindex_update.sh BDIR=${TOPDIR}/regress_final regress
	while [ -d $(shell sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).new ] ; do sleep 0.1 ; done
	make asearch-regress-clean-index

update-aindex-copy-sms:
	env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${REGRESS_FINALDIR}/bin/aindex_update.sh BDIR=${TOPDIR}/regress_final regress sms_copy
	while [ -d $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).new ] ; do sleep 0.1 ; done
	make asearch-regress-clean-index

update-aindex-send-sms:
	env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${REGRESS_FINALDIR}/bin/aindex_update.sh BDIR=${TOPDIR}/regress_final regress sms_send
	while [ -d $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).new ] ; do sleep 0.1 ; done
	make asearch-regress-clean-index

update-aindex-send-sms-synchronous:
	env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${REGRESS_FINALDIR}/bin/aindex_update.sh BDIR=${TOPDIR}/regress_final regress sms_send synchronous
	while [ -d $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/asearch.conf).new ] ; do sleep 0.1 ; done
	make asearch-regress-clean-index

asearch-regress-start:
	@echo -e "\\033[1;35mStarting asearch\\033[39;0m"
	make asearch-regress-full-index
	make asearch-regress-merge-index-full
	make asearch-regress-clean-index
	make asearch-regress-start-engine

asearch-regress-stop-engine:
	kill $$(< ${TOPDIR}/.asearchpid)
	rm ${TOPDIR}/.asearchpid

asearch-regress-stop:
	@echo -e "\\033[1;35mStopping asearch\\033[39;0m"
	if (cd ${TOPDIR}; ${TOPDIR}/util/unlock asearch_regress${GENPORTOFF}; test "$$?" != "2"); then \
		make asearch-regress-stop-engine; \
		make asearch-regress-delete-index; \
	fi

asearch-regress-restart:
	make asearch-regress-stop
	make asearch-regress-start

csearch-regress-start:
	@echo -e "\\033[1;35mStarting csearch\\033[39;0m"
	rm -rf $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/csearch.conf)*
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl reindex csearch)
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ./index_merge $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/csearch.conf).{index,new})
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl distribute_full csearch all)
	(cd ${TOPDIR}; util/lock csearch_regress${GENPORTOFF}) && \
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ./search --config ../conf/csearch.conf --logtag csearch --pidfile ${TOPDIR}/.csearchpid --timeout 2000 --quick-start)
	try=0; while ! nc localhost $(shell sed -nre 's/.*port.search ?=//p' ${REGRESS_FINALDIR}/conf/csearch.conf) < /dev/null && ((try++ < 100)); do sleep 0.1 ; done

csearch-regress-stop:
	@echo -e "\\033[1;35mStopping csearch\\033[39;0m"
	-(cd ${TOPDIR}; util/unlock csearch_regress${GENPORTOFF}) && (kill `cat ${TOPDIR}/.csearchpid` && rm ${TOPDIR}/.csearchpid ;rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/csearch.conf))

josesearch-regress-start:
	@echo -e "\\033[1;35mStarting josesearch\\033[39;0m"
	rm -rf $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/josesearch.conf)*
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl reindex josesearch)
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ./index_merge $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/josesearch.conf).{index,new})
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl distribute_full josesearch all)
	(cd ${TOPDIR}; util/lock josesearch_regress${GENPORTOFF}) && \
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ./search --config ../conf/josesearch.conf --logtag josesearch --pidfile ${TOPDIR}/.josesearchpid --timeout 2000 --quick-start)
	try=0; while ! nc localhost $(shell sed -nre 's/.*port.search ?=//p' ${REGRESS_FINALDIR}/conf/josesearch.conf) < /dev/null && ((try++ < 100)); do sleep 0.1 ; done

josesearch-regress-stop:
	@echo -e "\\033[1;35mStopping josesearch\\033[39;0m"
	-(cd ${TOPDIR}; util/unlock josesearch_regress${GENPORTOFF}) && (kill `cat ${TOPDIR}/.josesearchpid` && rm ${TOPDIR}/.josesearchpid ;rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/josesearch.conf))

msearch-regress-rebuild-index:
	@echo -e "\\033[1;35mRebuilding msearch\\033[39;0m"
	rm -rf $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/msearch.conf)*
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl reindex msearch)
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ./index_merge $$(sed -nre 's/^db_name=//p' ${TOPDIR}/regress_final/conf/msearch.conf).{index,new})
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ${AINDEX_CMD_PREFIX} ./index_ctl distribute_full msearch all)

msearch-regress-start:
	@echo -e "\\033[1;35mStarting msearch\\033[39;0m"
	(cd ${TOPDIR}; util/lock msearch_regress${GENPORTOFF}) && \
	(cd ${REGRESS_FINALDIR}/bin && env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ${STRESS_CMD_PREFIX} ./search --config ../conf/msearch.conf --logtag msearch --pidfile ${TOPDIR}/.msearchpid --timeout 2000 --quick-start)
	try=0; while ! nc localhost $(shell sed -nre 's/.*port.search ?=//p' ${REGRESS_FINALDIR}/conf/msearch.conf) < /dev/null && ((try++ < 100)); do sleep 0.1 ; done

msearch-copy-index:
	mkdir -p /dev/shm/regress-$(shell whoami)/street_index/
	cp ${TOPDIR}/scripts/maps/db.blob /dev/shm/regress-$(shell whoami)/street_index/

msearch-start-w-index:
	make msearch-regress-rebuild-index msearch-regress-start

msearch-copy-and-start:
	make msearch-copy-index msearch-regress-start

msearch-regress-stop:
	@echo -e "\\033[1;35mStopping msearch\\033[39;0m"
	-(cd ${TOPDIR}; util/unlock msearch_regress${GENPORTOFF}) && (kill `cat ${TOPDIR}/.msearchpid` && rm ${TOPDIR}/.msearchpid ;rm -rf $$(sed -nre 's/.*db_name=//p' ${REGRESS_FINALDIR}/conf/msearch.conf))

upload-clean:
	@find ${REGRESSDIR}/upload -name '*jpg' -exec rm {} \;
	@find ${REGRESSDIR}/upload -name '*flv' -exec rm {} \;
	@cd ${REGRESSDIR}/../images_test ; find . -name '*jpg' -exec cp --parents {} ${REGRESSDIR}/upload \; ; cd - 
upload-init:
	for a in upload/images upload/thumbs upload/bids/thumbs/images upload/bids/images upload/storeimages upload/ppageimages; do \
		mkdir -p ${REGRESSDIR}/$$a/{00..99}; \
	done;
	for a in var/run www var/lock; do mkdir -p ${REGRESSDIR}/$$a ; done
	@cd ${REGRESSDIR}/../images_test; find . -name '*jpg' -exec cp --parents {} ${REGRESSDIR}/upload \; ; cd -

dav-replay-recreate:
	rm -fr ${REGRESSDIR}/dav_replay_folder; \
	if [ ! -d ${REGRESSDIR}/dav_replay_folder ]; then \
		mkdir ${REGRESSDIR}/dav_replay_folder; \
	fi
	if [ ! -d ${REGRESSDIR}/dav_replay_folder/tmp ]; then \
		mkdir ${REGRESSDIR}/dav_replay_folder/tmp; \
	fi

sendmail-replace:
	if ! test -f ${REGRESS_FINALDIR}/libexec/sendmail.bak ; then  \
        if ! test -f ${REGRESS_FINALDIR}/libexec/sendmail; then \
            cp /usr/lib/sendmail ${REGRESS_FINALDIR}/libexec/sendmail.bak;\
        else \
            cp ${REGRESS_FINALDIR}/libexec/sendmail ${REGRESS_FINALDIR}/libexec/sendmail.bak; \
        fi \
	fi
	cp ${REGRESS_FINALDIR}/libexec/sendmail.fake ${REGRESS_FINALDIR}/libexec/sendmail; \
	rm -f .mail.txt .mail.orig.txt ${REGRESS_FINALDIR}/logs/mail.txt

sendmail-restore:
	if [ -f ${REGRESS_FINALDIR}/libexec/sendmail.bak ] ; then \
		mv ${REGRESS_FINALDIR}/libexec/sendmail.bak ${REGRESS_FINALDIR}/libexec/sendmail; \
	fi

wmstart:
	${XVNC_PATH} -ac -SecurityTypes None :${shell id -u} >> ${REGRESS_LOG} 2> /dev/null & \
	DISPLAY=:${shell id -u} ${WM}> /dev/null 2>&1 &

wmstop:
	pkill -U ${USER} Xvnc

selenium-start:
	@echo -e "\\033[1;35mStarting selenium environment\\033[39;0m"
ifndef	SELENIUM_DISPLAY
	if (cd ${TOPDIR}; util/lock selenium${GENPORTOFF}); then \
		${XVNC_PATH} -ac -SecurityTypes None :${shell id -u} >> ${REGRESS_LOG} 2> /dev/null & \
		DISPLAY=:${shell id -u} ${WM}> /dev/null 2>&1 & \
		rm -f ${REGRESS_FINALDIR}/selenium-core/core/log.txt; \
		make -C ${TOPDIR} firefox-setup; \
	fi
else
	if (cd ${TOPDIR}; util/lock selenium${GENPORTOFF}); then \
		rm -f ${REGRESS_FINALDIR}/selenium-core/core/log.txt;\
		make -C ${TOPDIR} firefox-setup;\
	fi
endif
	${MAKE} bconf_backup bconf_update-common.store.display_stats:1 bconf_update-common.stat_counter.insight.display:0 bconf_update-common.stat_counter.xiti.display:0 bconf_update-common.link.mapimgservice: bconf_update-common.link.largemap: bconf_update-controlpanel.modules.search.target_links:0  bconf_update_base_url trans-regress-reload apache-regress-restart
	[ -z "$$stresstest_rerun_valgrind" ] || sleep 8

selenium-stop:
	@echo -e "\\033[1;35mStopping selenium environment\\033[39;0m"
ifndef	SELENIUM_DISPLAY
	if (cd ${TOPDIR}; util/unlock selenium${GENPORTOFF}); then \
		pkill -U ${USER} Xvnc || true;\
		pkill -U ${USER} gconfd-2 || true;\
		pkill -U ${USER} gam_server || true;\
	fi
else
	if (cd ${TOPDIR}; util/unlock selenium${GENPORTOFF}); then \
		pkill -U ${USER} gconfd-2 || true;\
		pkill -U ${USER} gam_server || true;\
	fi
endif

firefox-setup:
	echo -e "${BGREEN}Init firefox profile 'default'${COLOR_OFF}"
	@rm -rf ${HOME}/.mozilla; \
	firefox --display :${shell id -u} -CreateProfile default ; \
	dir=`ls -d ${HOME}/.mozilla/firefox/*.default` ; \
	cat ${REGRESS_FINALDIR}/selenium-core/prefs.js >> $$dir/prefs.js ;  \
	certutil -A -n "Test-Only Certificate" -t "CT,CT,CT" -d $$dir -i ${REGRESS_FINALDIR}/conf/server.crt

#update-templates:
#	@make -C ${TOPDIR}/templates/; \
#	make -C ${TOPDIR}/modules all; \
#	make -C ${TOPDIR} apache-regress-stop; \
#	sleep 8; \
#	cp ${TOPDIR}/modules/php_templates/php_templates.so ${REGRESS_FINALDIR}/modules/; \
#	cp ${TOPDIR}/modules/mod_blocket/mod_blocket.so ${REGRESS_FINALDIR}/modules/; \
#	cp ${TOPDIR}/modules/mod_progress/mod_progress.so ${REGRESS_FINALDIR}/modules/; \
#	cp ${TOPDIR}/modules/mod_fastcgi-2.4.6/mod_fastcgi.so ${REGRESS_FINALDIR}/modules/; \
#	cp ${TOPDIR}/www/css/*.css ${REGRESS_FINALDIR}/www/css/; \
#	cp ${TOPDIR}/www/css/*.css ${REGRESS_FINALDIR}/www-ssl/css/; \
#	cp ${TOPDIR}/php/controlpanel/css/*.css ${REGRESS_FINALDIR}/www-ssl/css/; \
#	cp ${TOPDIR}/www/js/*.js ${REGRESS_FINALDIR}/www/js/; \
#	cp ${TOPDIR}/www/js/*.js ${REGRESS_FINALDIR}/www-ssl/js/; \
#	cp ${TOPDIR}/php/controlpanel/js/*.js ${REGRESS_FINALDIR}/www-ssl/js/; \
#	make -C ${TOPDIR} apache-regress-start;

regress-adcodes:
	(env PERL5LIB="${REGRESS_FINALDIR}/${PERL_INSTALL}" ${TOPDIR}/regress_final/bin/trans_command.pl -config ${TOPDIR}/regress_final/conf/bconf.conf "cmd:create_adcodes\nverifylimit:200\npaylimit:200\nadwatchlimit:100\ncommit:1\nend\n") 

regress-admins ra:
	@echo "Creating admins and privs"; \
	sed -e 's,%USER%,${USER},g' \
            -e 's,%USER_PASSWORD%,${shell php -r "echo sha1(\"${USER}\");"},g' \
            -e 's,%HOSTNAME%,${shell hostname},g' \
            < ${TOPDIR}/scripts/db/insert_blocketdb_adminsdata.pgsql.in \
            > ${TOPDIR}/scripts/db/insert_blocketdb_adminsdata.pgsql
	psql -h ${REGRESS_PGSQL_HOST} blocketdb -f ${TOPDIR}/scripts/db/insert_blocketdb_adminsdata.pgsql -q

regress-mailqueue-%:
	@echo "Mail queue data for $(subst regress-mailqueue-,,$@) year schema"; \
	sed -e 's,%SCHEMA%,blocket_$(subst regress-mailqueue-,,$@),g' \
            < ${TOPDIR}/scripts/db/insert_blocketdb_mailqueuedata.pgsql.in \
            > ${TOPDIR}/scripts/db/insert_blocketdb_mailqueuedata.pgsql

show-session:
	sess="`echo get \`grep -o  'mc1x[[:xdigit:]]*' ${REGRESS_LOG} | tail -1\` | nc localhost ${REGRESS_MCPORT} | head -2 | tail -1`"; \
	printf "<? session_start(); session_decode('%s'); print_r(\$$_SESSION); session_destroy();?>" "$$sess" | php; \
	echo -n "cpanel token: "; printf "get $$sess""_token_admin\n" | nc localhost ${REGRESS_MCPORT}; \
	echo -n "store token: "; printf "get $$sess""_token_admin\n" | nc localhost ${REGRESS_MCPORT}

statpoints-regress-start:
	@echo -e "\\033[1;35mStarting statpoints daemon\\033[39;0m"
	@if (cd ${TOPDIR}; util/lock statpoints_${GENPORTOFF}); then \
		(cd ${REGRESS_FINALDIR}/bin/ && exec setsid ${SYSLOGHOOK} ./statpoints --config=../conf/bconf.conf --foreground < /dev/null > /dev/null 2>> ${REGRESS_LOG} & echo "$$!" > ${SOURCE_PATH}/.statpointspid); \
	else \
		true; \
	fi

statpoints-regress-stop:
	@echo -e "\\033[1;35mStopping statpoints daemon\\033[39;0m"
	@if (cd ${TOPDIR}; util/unlock statpoints_${GENPORTOFF}; test "$$?" != "2"); then \
		kill $$(< ${TOPDIR}/.statpointspid); \
		rm ${TOPDIR}/.statpointspid; \
		rm -rf ${REGRESS_FINALDIR}/statpoints; \
	else \
		true; \
	fi

#pixel-regress-start:
#	@echo -e "\\033[1;35mStarting pixel daemon\\033[39;0m"
#	@if (cd ${TOPDIR}; util/lock pixel_${GENPORTOFF}); then \
#		(cd ${REGRESS_FINALDIR}/bin/ && { env LD_PRELOAD=${SYSLOGHOOKSO} ${CMD_PREFIX} ./pixel --config=../conf/pixel.conf --foreground --debug --level 3 < /dev/null > /dev/null 2>> ${PIXEL_LOG} & } && echo "$$!" > ${SOURCE_PATH}/.pixelpid); \
#	else \
#		true; \
#	fi
#
#pixel-regress-stop:
#	@echo -e "\\033[1;35mStopping pixel daemon\\033[39;0m"
#	@if (cd ${TOPDIR}; util/unlock pixel_${GENPORTOFF}; test "$$?" != "2"); then \
#		kill $$(< ${TOPDIR}/.pixelpid); \
#		rm ${TOPDIR}/.pixelpid; \
#		rm -rf ${REGRESS_FINALDIR}/pixel; \
#	else \
#		true; \
#	fi

redir-clean:
	@rm -f ${REGRESS_FINALDIR}/logs/redir_log

regress-images rim:
	${shell cd ${TOPDIR}; pwd}/scripts/regressimages.pl --pghost=${REGRESS_PGSQL_HOST} --regressdir=${TOPDIR}/regress --quiet

ifdef TEMPLATES_PATH
clean-dyn-templates:
	rm -rf ${TEMPLATES_PATH}
endif

send-mail-accepted-ads:
	printf "cmd:flushqueue\nqueue:accepted_mail\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

send-mail-bumped-ads:
	printf "cmd:flushqueue\nqueue:bump_mail\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

flush-weekly-bump-ads:
	printf "cmd:flushqueue\nqueue:weekly_bump\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

flush-upselling-products:
	printf "cmd:flushqueue\nqueue:upselling\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

expire-galleries:
	printf "cmd:gallery_expired\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

expire-packs:
	printf "cmd:expire_packs\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

flush-preprocessing-queue:
	(cd ${TOPDIR}/scripts/preprocessing && exec ./flush_preprocessing_queue.sh -h localhost -p ${REGRESS_TRANS_PORT}) &> /dev/null

remove-gallery-%:
	printf "cmd:remove_gallery\nad_id:$(subst remove-gallery-,,$@)\nremote_addr:127.0.0.1\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}

load-redisstat-scripts:
	@echo -n "waiting for redis ok"
	@while(true); \
	do \
		if [[ `${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSTAT_PORT} PING 2> /dev/null` != "PONG" ]]; \
		then \
			echo -n "."; \
		else \
			echo ""; \
			echo "Success"; \
			break; \
		fi \
	done
	@cd ${TOPDIR};echo $(shell ${TOPDIR}/util/load_redis_script.sh ${REGRESS_REDISSTAT_PORT} "${TOPDIR}/scripts/redis/stats/get_ad_stats.lua" 2> /dev/null)
	@cd ${TOPDIR};echo $(shell ${TOPDIR}/util/load_redis_script.sh ${REGRESS_REDISSTAT_PORT} "${TOPDIR}/scripts/redis/stats/get_pack_visits_stats.lua" 2> /dev/null)

# Use incremental asearch indexing by default, so it resembles production behaviour
rebuild-index:  expire-galleries flush-weekly-bump-ads flush-upselling-products update-aindex send-mail-accepted-ads send-mail-bumped-ads flush-preprocessing-queue

rebuild-index-full: expire-galleries flush-weekly-bump-ads flush-upselling-products rebuild-asearch send-mail-accepted-ads send-mail-bumped-ads flush-preprocessing-queue

rebuild-asearch: asearch-regress-stop asearch-regress-start

rebuild-csearch: csearch-regress-stop csearch-regress-start

rebuild-josesearch: josesearch-regress-stop josesearch-regress-start

rebuild-msearch: msearch-regress-stop msearch-start-w-index

flush-queues: expire-galleries flush-weekly-bump-ads flush-upselling-products flush-preprocessing-queue

setup-demo: rc ri rd db-snap-asd db-load-ruleslistfilters db-load-1000ads db-load-add100galleries rim rebuild-index regress-admins sendmail-restore
	@echo -e "\\033[1;35mDemo environment set up\\033[39;0m"

auto-reindex:
	while [ 1==1 ]; do make rebuild-index; sleep 60; done

migrate_ipv6:
	TOPDIR=${TOPDIR} PGHOST=${REGRESS_PGSQL_HOST} sh ${TOPDIR}/scripts/db/ipv6_migrate_slony.sh

migrate_ipv6-notfinal:
	TOPDIR=${TOPDIR} PGHOST=${REGRESS_PGSQL_HOST} notfinal=1 sh ${TOPDIR}/scripts/db/ipv6_migrate_slony.sh

# For api 
#.PHONY: add-api-key
#add-api-key:
#	printf "HMSET redmxapp_id_blocket_mobile api_key 746e1a72b4172e50d9c6b4f981312a1803283a74 region_limit 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15 ad_type_limit b,h,k,s,u request_limit 100000 appl_limit import,importdeletead,linkshelf,list,view,areas,categories,category_settings,category_params,sendmail,settings,related_ads,newad,sendtip,deletead,findstores,account_login,account_create,account_modify,account_change_password,account_lost_password,account_get_unpublished_ads,account_get_published_ads,account_adwatches,account_get_adwatch_ads,account_info,account_adwatch_create,account_adwatch_delete,account_save_ad,account_delete_save_ad,account_get_saved_ads,account_adwatch_email_start,account_adwatch_email_stop,list_latest,payment_status,payment_choice,account_create_jobb,account_jobb_list cat_limit 1000,1020,1040,1060,1080,1100,1120,2000,2020,2040,2060,2080,2100,2120,3000,3020,3040,3060,3080,5000,5020,5040,5060,5100,5120,5140,6000,6020,6060,6080,6100,6120,6140,6160,6180,6200,7000,7020,7040,7060,7080,8000,8020" | ${REGRESS_REDISSTAT_CLIENT} -p ${REGRESS_REDISSTAT_PORT}

#redis-cardata-load-data:
#	@echo -e "\\033[1;35mLoading cardata (this process may take some time)\\033[39;0m"
#	@/usr/bin/php -c ${REGRESS_FINALDIR}/conf/php.ini ${TOPDIR}/util/sii_car_data/cardata_sql2redis.php ${REGRESS_PGSQL_HOST} | ${TOPDIR}/regress_final/bin/redis-cli -p ${REGRESS_REDIS_CARDATA_PORT} 2>&1 > /dev/null


bconf_update-%:
	${TOPDIR}/regress/final/bconf_update.pl -update $@ -bconf ${REGRESS_FINALDIR}/conf/bconf.txt.site
	${MAKE} original-bconf

bconf_backup:
	${TOPDIR}/regress/final/bconf_update.pl -backup    -bconf ${REGRESS_FINALDIR}/conf/bconf.txt.site
	${MAKE} original-bconf

bconf_restore:
	${TOPDIR}/regress/final/bconf_update.pl -restore   -bconf ${REGRESS_FINALDIR}/conf/bconf.txt.site
	${MAKE} original-bconf

original-bconf:
	printf 'cmd:bconf\ncommit:1\nend\n' | nc localhost ${REGRESS_TRANS_PORT} > .start_bconf

bconf_restart:
	${TOPDIR}/regress/final/check_bconf.sh ${REGRESS_TRANS_PORT}

bconf_update_base_url:

	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.ai&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.li&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.vi&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.blocket&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.msite&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.support&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.common.base_url.templates&value=https%3A%2F%2F${REGRESS_HOSTNAME}%3A${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200

	${MAKE} apache-regress-graceful
