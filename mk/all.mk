ifndef SITE_ALL_MK_I
SITE_ALL_MK_I:=1

BUILDPATH?=build
include ${TOPDIR}/platform/mk/all.mk
include ${TOPDIR}/mk/defvars.mk
include ${TOPDIR}/mk/modules_paths.mk

endif #SITE_ALL_MK_I
