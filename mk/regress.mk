include ${TOPDIR}/platform/mk/regress.mk

TESTDIR=tests

t-%:
	@if [ "${CUSTOM_DO_TEST}" ==  "" ]; then  \
		CUSTOM_DO_TEST="${DO_TEST}"; \
		CUSTOM_TESTDIR="${TESTDIR}"; \
	else \
		CUSTOM_DO_TEST="${CUSTOM_DO_TEST}"; \
		CUSTOM_TESTDIR="${CUSTOM_TESTDIR}"; \
	fi; \
	rm -f .cmd.in; \
	if echo $@ | egrep -q '\.(set)?token'; \
	then \
		cat .token >> .cmd.in; \
	fi; \
        if echo $@ | egrep -q '\.uid'; \
        then \
	        cat .uid >> .cmd.in; \
        fi; \
	TAR="`echo $@ | sed s,[.].*,,`"; \
	cat $$CUSTOM_TESTDIR/$$TAR.in >> .cmd.in; \
	echo -ne "${BGREEN}Running:${COLOR_OFF}$$TAR"; \
	touch .ad.code .prevtime .watch_unique_id .ad.id ;\
	sed -i -e "s/%WATCH_UNIQUE_ID%/$$(< .watch_unique_id)/" -e "s/%AD_CODE%/$$(< .ad.code)/" -e "s/%AD_ID%/$$(< .ad.id)/" -e "s/%PREV_TIME%/$$(< .prevtime)/" -e "s/%CURR_TIME%/`date +'%Y-%m-%d %H:%M:%S' | tee .prevtime`/" -e 's/%CURR_DATE%/${shell date +"%Y-%m-%d"}/' -e 's/%REGRESS_HTTPD_PORT%/${REGRESS_HTTPD_PORT}/' -e 's/%REGRESS_HTTPS_PORT%/${REGRESS_HTTPS_PORT}/' -e 's/%REGRESS_HOSTNAME%/${REGRESS_HOSTNAME}/' -e 's,%TESTDIR%,$(abspath ${CURDIR})/$$CUSTOM_TESTDIR,' .cmd.in; \
	if [ -e .paycode ]; then sed -i -e "s/%PAYCODE%/$$(< .paycode)/" .cmd.in; fi; \
	rm -f .cmd.out; \
	if [ "$(subst .transwait,,$@)" != "$@" ]; then \
		make --quiet trans-pending-wait; \
	fi; \
	if [ "$(subst .mail,,$@)" != "$@" -o "$(subst .nomail,,$@)" != "$@" -o "$(subst .qpmail,,$@)" != "$@" ]; then \
		rm -f .mail.*; \
		rm -f .mq.*; \
		rm -f .attachment; \
	fi; \
	if [ "$(subst .date,,$@)" != "$@" ]; then \
		echo -n " -- DATE:"; \
		sed -i -e "s/%YEAR%/$$(date +%Y)/g" .cmd.in; \
		sed -i -e "s/%MONTH%/$$(date "+%-m")/g" .cmd.in; \
	fi; \
	cat .cmd.in | $$CUSTOM_DO_TEST > .cmd.out; \
	test $$? != 0 && exit 1; \
	if echo $@ | egrep -q '\.(get)?token'; \
	then \
		grep "token:" .cmd.out > .token; \
	fi; \
        if echo $@ | egrep -q '\.getuid'; \
        then \
                grep "uid:" .cmd.out > .uid; \
        fi; \
	if echo $@ | egrep -q '\.storetoken'; \
	then \
		sed -ne 's/store_token:/token:/p' .cmd.out > .token; \
	fi; \
	if echo $@ | egrep -q '\.transwait'; then \
		make --quiet trans-pending-wait; \
	fi; \
	if echo $@ | egrep -q '\.adid'; then \
		grep "ad_id:" .cmd.out | cut -f2 -d':' > .ad.id ; \
	fi; \
	${MATCH} .cmd.out $$CUSTOM_TESTDIR/$$TAR.out --ignore-time --ignore-token; \
	if [ $$? != 0 ]; \
	then \
		echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.in"; \
		exit 1; \
	else \
		echo -n " -- OK"; \
	fi; \
	if [ "$(subst .pgsqldiff,,$@)" != "$@" ]; then \
		echo -n " -- PG:"; \
		cp -f $$CUSTOM_TESTDIR/$$TAR.pgsqlin .sql.in; \
		sed -i -e "s/%AD_CODE%/$$(< .ad.code)/" -e "s/%YEAR%/$$(date +%Y)/g" .sql.in; \
		psql --no-psqlrc -h ${REGRESS_PGSQL_HOST} --no-align --field-separator=, --pset footer blocketdb < .sql.in > .sql.out; \
		${MATCH} .sql.out $$CUSTOM_TESTDIR/$$TAR.pgsqlout ;\
		if [ $$? != 0 ]; \
		then \
			echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.pgsqlin"; \
			exit 1; \
		else \
			echo -n "OK"; \
		fi; \
	fi; \
	if [ "$(subst .mqdiff,,$@)" != "$@" ]; then \
		echo -n " -- PG:"; \
		cp -f $$CUSTOM_TESTDIR/$$TAR.mqin .mq.in; \
		cat .mq.in | xargs -I % echo amqp-get -u amqp://${REGRESS_HOSTNAME}:${REGRESS_RABBITMQ_PORT}/%; \
		cat .mq.in | xargs -I % echo amqp-get -u amqp://${REGRESS_HOSTNAME}:${REGRESS_RABBITMQ_PORT}/% | bash - > .mq.out; \
		${MATCH} .mq.out $$CUSTOM_TESTDIR/$$TAR.mqout ;\
		if [ $$? != 0 ]; \
		then \
			echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.mqin"; \
			exit 1; \
		else \
			echo -n "OK"; \
		fi; \
	fi; \
	if [ "$(subst .mail,,$@)" != "$@" ]; then \
		echo -n " -- MAIL:"; \
		cnt=180; \
		while [ $$cnt -gt 0 -a \( ! -f .mail.txt -o `/sbin/fuser -s .mail.txt 2>/dev/null; echo $$?` -eq 0 \) ] ; \
			do sleep 0.1 ; cnt=`expr $$cnt - 1` ; \
		done ; \
		if [ "$(subst .mailskip,,$@)" = "$@" ]; then \
			mv .mail.txt .mail.orig.txt ;\
			${MAILDECODER} < .mail.orig.txt > .mail.test.txt ;\
			${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php .mail.test.txt > .mail.txt; \
			${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php $$CUSTOM_TESTDIR/$$TAR.mail > .mail.test.txt; \
			${MATCH} .mail.txt .mail.test.txt ;\
			if [ $$? != 0 ]; \
			then \
				echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.mail" .mail.orig.txt; \
				exit 1; \
			else \
				echo -n "OK"; \
			fi; \
			if [ -f .attachment ]; then \
				${MATCH} .attachment $$CUSTOM_TESTDIR/$$TAR.attach ;\
				if [ $$? != 0 ]; \
				then \
					echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.attach"; \
					exit 1; \
				fi; \
			fi; \
		fi; \
	fi; \
	if	[ "$(subst .oldmail,,$@)" != "$@" ]; then \
		mv .mail.txt.old .mail.orig.txt.old ;\
		${MAILDECODER} < .mail.orig.txt.old > .mail.test.txt.old ;\
		${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php .mail.test.txt.old > .mail.txt.old; \
		${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php $$CUSTOM_TESTDIR/$$TAR.oldmail > .mail.test.txt.old; \
		${MATCH} .mail.txt.old .mail.test.txt.old ;\
		if [ $$? != 0 ]; \
		then \
			echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.oldmail" .mail.txt.old; \
			exit 1; \
		fi; \
	fi; \
	if [ "$(subst .qpmail,,$@)" != "$@" ]; then \
		cnt=180; \
		while [ $$cnt -gt 0 -a \( ! -f .mail.txt -o `/sbin/fuser -s .mail.txt 2>/dev/null; echo $$?` -eq 0 \) ] ; \
			do sleep 0.1 ; cnt=`expr $$cnt - 1` ; \
		done ; \
		mv .mail.txt .mail.orig.txt ;\
		${MAILDECODER} < .mail.orig.txt > .mail.test.txt ;\
		${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php .mail.test.txt > .mail.txt; \
		${TOPDIR}/util/quoteprinteable2text/quoteprinteable2text.php $$CUSTOM_TESTDIR/$$TAR.mail > .mail.test.txt; \
		${MATCH} .mail.txt .mail.test.txt ;\
		if [ $$? != 0 ]; \
		then \
			echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.mail"; \
			exit 1; \
		fi; \
		if [ -f .attachment ]; then \
			${MATCH} .attachment $$CUSTOM_TESTDIR/$$TAR.attach ;\
			if [ $$? != 0 ]; \
			then \
				echo ": edit INPUT  ; $$EDITOR $$CUSTOM_TESTDIR/$$TAR.attach"; \
				exit 1; \
			fi; \
		fi; \
	fi; \
	if [ "$(subst .nomail,,$@)" != "$@" ]; then \
		if [ -e .mail.txt ]; then \
			echo ": You sent an email"; \
			exit 1;\
		fi; \
	fi; \
	if [ "$(subst .paycode,,$@)" !=  "$@" ]; then \
		grep "paycode:" .cmd.out | cut -f2 -d':' > .paycode ; \
	fi; \
	echo "."; exit 0

