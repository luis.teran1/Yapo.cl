#####################################
### BEGIN LAZY RESTORE DEFINITIONS ###
####################################
include $(TOPDIR)/mk/db_snap_definitions.mk

db-kill-snap-%:
	cat ${TOPDIR}/platform/bin/trans/templates/sql/db_snap_drop_snap.sql.tmpl | sed -e s.\<\%\%name\%\>.$*. | psql -h ${REGRESS_PGSQL_HOST} blocketdb;

db-lrestore-cleandb:
	@if printf "cmd:db_exists\ndb_name:db-snap-cleandb\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK ; then \
		$(MAKE) --quiet db-restore-cleandb; \
	else \
		$(MAKE) --quiet db-snap-lixo db-snap-cleandb db-kill-snap-lixo; \
	fi

db-lrestore-%:
	@echo "Cleaning redis session"; $(MAKE)  -C ${TOPDIR} --quiet redissession-flushall
	@if printf "cmd:db_exists\ndb_name:db-snap-$*\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT}| fgrep TRANS_OK ; then \
		echo "Restoring db"; $(MAKE) --quiet db-restore-$*; \
	else \
		$(MAKE) --quiet db-lrestore-cleandb &&\
		$(MAKE) --quiet $(DATA_SCRIPTS) && \
		$(MAKE) --quiet db-store-$*; \
	fi;

db-snap-%:
	@echo -ne " (*) db_snap :: ${GREEN} $(subst db-snap-,,$@) ${NO_COLOR} -- "; \
	printf "cmd:db_snap\naction:snap\nname:$(subst db-snap-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-store-%:
	@echo -ne " (*) db_snap :: store :: ${GREEN} $(subst db-store-,,$@) ${NO_COLOR} -- "; \
	printf "cmd:db_snap\naction:store\nname:$(subst db-store-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-restore-%:
	@echo -ne " (*) db_snap :: restore :: ${GREEN} $(subst db-restore-,,$@) ${NO_COLOR} -- "; \
	printf "cmd:db_snap\naction:restore\nname:$(subst db-restore-,,$@)\ncommit:1\nend\n" | nc localhost ${REGRESS_TRANS_PORT} | fgrep TRANS_OK

db-load-%:
	@echo -e " * Loading into the DB :: ${GREEN} $(subst db-load-,,$@) ${NO_COLOR}"; \
	(cd ${TOPDIR}/scripts/db && exec env BPVSCHEMA=${BPVSCHEMA} PGOPTIONS="-c search_path=${BPVSCHEMA},public" ./load_data.sh ${REGRESS_PGSQL_HOST} $(subst db-load-,,$@))
##END LAZY RESTORE DEFINITIONS
