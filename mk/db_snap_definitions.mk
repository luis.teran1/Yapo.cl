# default, erro
db-lrestore-%: DATA_SCRIPTS = $(error ERROR_NO_DATA_SCRIPTS_DEFINED_FOR_THIS_SNAP $*)

db-lrestore-accounts: DATA_SCRIPTS = \
	db-load-accounts

db-lrestore-gallery: DATA_SCRIPTS = \
	db-load-gallery

db-lrestore-example: DATA_SCRIPTS = \
	db-load-example \
	db-load-newcat \
	migrate_ipv6-notfinal

db-lrestore-fads-and-stores: DATA_SCRIPTS = \
	db-load-example \
	db-load-1000ads \
	migrate_ipv6

db-lrestore-reg-cat: DATA_SCRIPTS = \
	db-load-test \
	db-load-reg_cat \
	migrate_ipv6

db-lrestore-fads-and-stores-1030: DATA_SCRIPTS = \
	db-load-1030ads \
	db-load-newcat \
	db-load-example \
	migrate_ipv6

db-lrestore-bumpedads: DATA_SCRIPTS = \
    db-load-bumpedads

db-lrestore-whitelist: DATA_SCRIPTS = \
    db-load-whitelist

db-lrestore-whitelist2: DATA_SCRIPTS = \
    db-load-whitelist2

db-lrestore-api_test: DATA_SCRIPTS = \
    db-load-api_test

db-lrestore-android: DATA_SCRIPTS = \
    db-load-android

db-lrestore-acc2pro: DATA_SCRIPTS = \
	db-load-acc2pro

db-lrestore-adqueues: DATA_SCRIPTS = \
	db-load-adqueues \
	migrate_ipv6
#EOF
