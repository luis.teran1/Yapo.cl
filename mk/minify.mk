ifndef MINIFY_TYPE
MINIFY_TYPE=js
endif
${INSTALL_TARGETS}:

	rm -f $@.full;
	for file in  ${$(shell echo ${@:.${MINIFY_TYPE}=_${MINIFY_TYPE}} | tr '[a-z]' '[A-Z]')} ; do \
		cat $$file.in >> $@.full; \
	done 
ifndef WWW_NO_MIN
	java -jar ${TOPDIR}/util/yuicompressor/yuicompressor-2.4.6.jar --type ${MINIFY_TYPE} $@.full -o $@ ;
	rm -f $@.full ;
else
	mv $@.full $@
endif

