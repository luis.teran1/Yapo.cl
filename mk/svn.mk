ifndef SITE_SVN
SITE_SVN:=1

# updates the ignore list on repo
.PHONY: svnignoreup
svnignoreup: .svnignore
	svn propset svn:ignore -F $< .

endif # SITE_SVN
