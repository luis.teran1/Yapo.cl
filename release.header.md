# DONT remove these lines, please!

 Use the following comments like a "remember" of a correct release.md
 Please, if you have to add database changes, REMEMBER:

 - Create the tables and insert the data in the correct order
 - Set the correct permission in the new tables and sequences
 - If you going to add a new column NOT NULL, and the table has data,
   create the column NULL, add the correct UPDATES to set the data and then
   alter the column again to set NOT NULL


# Microservices

Please udpate the following microservices: *None*

