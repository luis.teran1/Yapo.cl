#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif
#include <bpapi.h>

/*
 * Check if value exists in list
 * Usage:
 * &in_list("needle", "list_name");
 * returns the first index of hit, NULL if not found
 */

static const struct tpvar *
in_list(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
    int i;
    int list_len;
    char *ret;
    const char *needle;
    const char *list;

    if (argc != 2)
        return TPV_SET(dst, TPV_NULL);

    TPVAR_STRTMP(needle, argv[0]);
    TPVAR_STRTMP(list,  argv[1]);

    *cc = BPCACHE_CANT;

    list_len = bpapi_length(api, list);

    for (i = 0; i < list_len; i++) {
        const char *list_val = bpapi_get_element(api, list, i);
        if ( list_val && strcmp(needle, list_val) == 0 ) {
            xasprintf(&ret, "%d", i);
            return TPV_SET(dst, TPV_DYNSTR(ret));
        }
    }
    return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(in_list);

