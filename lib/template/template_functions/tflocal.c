#include <bpapi.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include <time.h>
#include <ctemplates.h>
#include "log_event.h"
#include "logging.h"
#include <locale.h>
#include <math.h>
#ifdef TEMPLATE_TIMERS
#include "timer.h"
#endif
#include "util.h"
#include "query_string.h"
#include <sys/types.h>
#include <stdint.h>
#include <unistd.h>
#include <sha1.h>
#include <pcre.h>
#include "settings.h"

/* XXX Should use a proper filter instead */
static const struct tpvar *
format_phone_number(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	const char* s;
	char buf[64] = { 0 };
	char buf_modified[64] = { 0 };
	char aux[16] = { 0 };
	int pos = 0;
	char c;

	TPVAR_STRTMP(s, argv[0]);

	if (argc != 1 || !s || !*s)
		return TPV_SET(dst, TPV_NULL);

	while ((pos < (int)sizeof(buf)-1) && (c = *s++)) {
		// remove all non digits
		if ( c > 57 || c < 48 ) {
			continue;
		}
		buf[pos++] = c;
	}
	return TPV_SET(dst, pos ? TPV_DYNSTR(xstrdup(buf)) : TPV_NULL);

}
ADD_TEMPLATE_FUNCTION(format_phone_number);

static const struct tpvar *
format_phone_number_standarized(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	const char* s;
	char buf[64] = { 0 };
	char buf_modified[64] = { 0 };
	char aux[16] = { 0 };
	int pos = 0;
	char c;

	TPVAR_STRTMP(s, argv[0]);

	if (argc != 1 || !s || !*s)
		return TPV_SET(dst, TPV_NULL);

	while ((pos < (int)sizeof(buf)-1) && (c = *s++)) {
		// remove all non digits
		if ( c > 57 || c < 48 ) {
			continue;
		}
		buf[pos++] = c;
	}
	if (strlen(buf) == 9) {
		if(strcmp(&buf[0], "9") == 0) { //es celu
			strcpy(buf_modified, "+(569) ");
			memcpy(aux, &buf[1], 8);
			strcat(buf_modified, aux);
		}
		else if(strcmp(&buf[0], "2") == 0) { //es stgo
			strcpy(buf_modified, "+(562) ");
			memcpy(aux, &buf[1], 8);
			strcat(buf_modified, aux);
		}
		else { //es region
			strcpy(buf_modified, "+(56) ");
			memcpy(aux, &buf[0], 9);
			strcat(buf_modified, aux);
		}
		return TPV_SET(dst, pos ? TPV_DYNSTR(xstrdup(buf_modified)) : TPV_NULL);
	}
	return TPV_SET(dst, pos ? TPV_DYNSTR(xstrdup(buf)) : TPV_NULL);

}
ADD_TEMPLATE_FUNCTION(format_phone_number_standarized);

/* This function create a fake phone number using the real phone number */
static const struct tpvar *
format_fake_phone_number(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

	const char* s;
	char buf[64] = { 0 };
	int pos = 0;
	char c;
	const char* fake_hash;

	TPVAR_STRTMP(s, argv[0]);
	TPVAR_STRTMP(fake_hash, argv[1]);


	if (argc != 2 || !s || !*s || !fake_hash || !*fake_hash)
		return TPV_SET(dst, TPV_NULL);


	/* the hash must be a strlen() > 10 */
	if (strlen(fake_hash) < 10) {
		return TPV_SET(dst, TPV_NULL);
	}

	while ((pos < (int)sizeof(buf)-1) && (c = *s++)) {
		// remove all non digits
		if ( c > 57 || c < 48 ) {
			continue;
		}
		if (pos > 2)
			buf[pos++] = fake_hash[(c-48)];
		else
			buf[pos++] = c;
	}
	return TPV_SET(dst, pos ? TPV_DYNSTR(xstrdup(buf)) : TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(format_fake_phone_number);


/* Aux function for handling exchange types in currency. Gets an *char and gives a long long */
/* If float point number, we only get the (int decimals) decimals */

long long static ascii_to_longlong (const char *aux, int decimals) {

	long long a=0;
	int cont = 0;

	log_printf(LOG_DEBUG, "Value reaching ascii_to_longlong: %s", aux);

	while (*aux && *aux!=',') {
		a=a*10 + (*aux++ - '0');
	}
	while (*aux++ && (cont<decimals)) {
		a=a*10 + (*aux - '0');
		++cont;
	}
	return(a);
}

/* Returns the uf conversion factor with two decimals */
static const char *
_get_uf_conversion_factor(struct bpapi *api) {
	const char *conv_factor = vtree_get(&api->vchain, "common", "uf_conversion_factor", NULL);
	/* Try to fetch a fresher one from bconfd (later) */
	return conv_factor;
}

static const struct tpvar *
get_uf_conversion_factor(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	/*
	 * 	Call in templates as &get_uf_conversion_factor()
	 */
	const char *factor = _get_uf_conversion_factor(api);
	return TPV_SET(dst, TPV_STRING(factor));
}
ADD_TEMPLATE_FUNCTION(get_uf_conversion_factor);

static long long
convert_uf_to_pesos(const char *conv_factor, const char *uf) {
	long long factor = ascii_to_longlong(conv_factor, 2);
	long long uf_amount = ascii_to_longlong(uf, 0);
	long long pesos = uf_amount * factor / 10000;
	return pesos;
}

/* uf_to_pesos(): Function to make the conversion from uf to pesos*/
/* Param 1 = conversion factor (cf) Param 2 = UF*/
/* To handle float point operations, we remove the , and operate. Then recover the decimals */
/* Also, the 2 lsb of UF are the decimal part, so we /100 before converting to pesos */

static const struct tpvar *
uf_to_pesos(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char * con_factor;
	const char * uf;
	char * res = NULL;
	long long pesos = 0;
	long long uf_amount = 0;
	long long cr = 0;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(con_factor, argv[0]);
	TPVAR_STRTMP(uf, argv[1]);

	if (!con_factor || !uf || !*con_factor || !*uf)
		return TPV_SET(dst, TPV_NULL);

	cr = ascii_to_longlong(con_factor, 2);
	uf_amount = ascii_to_longlong(uf, 2);
	pesos = uf_amount * cr;
	pesos /=10000;
	xasprintf(&res,"%llu",pesos);
	return TPV_SET(dst, TPV_DYNSTR(res));

}

ADD_TEMPLATE_FUNCTION(uf_to_pesos);



/* Convert pesos tu UFs. Thanks to Pablo Paso to write the nice preview function !!! */
static const struct tpvar *
pesos_to_uf(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char * con_factor;
	const char * pesos;
	char * res = NULL;

	long long uf = 0;
	long long pesos_amount = 0;
	long long cr = 0;

	if (argc < 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(con_factor, argv[0]);
	TPVAR_STRTMP(pesos, argv[1]);

	if (!con_factor || !pesos || !*con_factor || !*pesos)
		return TPV_SET(dst, TPV_NULL);

	cr = ascii_to_longlong(con_factor, 2);
	pesos_amount = ascii_to_longlong(pesos, 2);

	pesos_amount = pesos_amount * 10000;

	uf = pesos_amount / cr;
	xasprintf(&res,"%llu",uf);
	return TPV_SET(dst, TPV_DYNSTR(res));

}

ADD_TEMPLATE_FUNCTION(pesos_to_uf);


/* XXX return the value, do not print it. Convert to filter? */
static const struct tpvar *
format_price_ex(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	int i = 0; 
	int l;
	int decs = 0;
	const char *price;
	int addplus = 0;
	int end = 0;

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);
	if (argc > 1 && tpvar_int(argv[1]))
		addplus = 1;
	if (argc > 2)
		decs = tpvar_int(argv[2]);
	if (decs < 0)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(price, argv[0]);

	/* XXX Swedish */
	if (strlen(price) > 1 && price[0] == '-') {
		++price;
		bpapi_outstring_fmt(api, "%c", '-');
	}

	/* Printing symbol */
	if (addplus && price[0] != '-')
		bpapi_outstring_raw(api, "+", 1);

	end = strlen(price) - decs;

	/* Checking if the number of decimals can be shown for the number entered */
	if (end <= 0) {
		bpapi_outstring_raw(api, "0,", 2);
		while (end != 0) {
			bpapi_outstring_raw(api, "0", 1);
			end++;	
		}
		while (price[i]) {
			bpapi_outstring_fmt(api, "%c", price[i++]);
		}  
	} else {
		l = (end) % 3;
	
		while (i != end) {
			if (i && (i % 3 == l))
				bpapi_outstring_raw(api, ".", 1);
	
			bpapi_outstring_fmt(api, "%c", price[i++]);
		}
		if (decs != 0) {
			bpapi_outstring_raw(api, ",", 1);
			while (price[i]) {
				bpapi_outstring_fmt(api, "%c", price[i++]);
			}  
		}
	}
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(format_price_ex);


static long long
get_red_arrow_price(long long price) {
	double lowprice;

	/* XXX this looks like SEK numbers. */
	if (price < 100) {
		lowprice = -1;
	} else if (price < 20000) {
		lowprice = price * .9;
	} else if (price < 40000) {
		lowprice = price - 2000;
	} else if (price < 60000) {
		lowprice = price - 3000;
	} else if (price < 80000) {
		lowprice = price - 4000;
	} else if (price < 100000) {
		lowprice = price - 5000;
	} else if (price < 150000) {
		lowprice = price - 6000;
	} else if (price < 200000) {
		lowprice = price - 8000;
	} else if (price < 250000) {
		lowprice = price - 10000;
	} else if (price < 500000) {
		lowprice = price - 12500;
	} else if (price < 600000) {
		lowprice = price - 16500;
	} else {
		lowprice = price * (1 - .03);
	}

	if (lowprice > 0) {
		return lowprice + .5;
	}

	return 0;
}

#define CURRENCY_PESO "peso"
#define CURRENCY_UF "uf"

static const struct tpvar *
get_price_lowered(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	/*
	 * 	Call in templates as &get_price_lowered(price, old_price, orig_list_time, currency?, old_currency?)
	 */
	if (argc < 3)
		return TPV_SET(dst, TPV_NULL);

	*cc = BPCACHE_CANT;

	const char *ts;
	struct tm tm;
	TPVAR_STRTMP(ts, argv[2]);
	if (strptime(ts, "%F %T", &tm) == NULL)
		return TPV_SET(dst, TPV_NULL);

	time_t timet = mktime(&tm);
	if (timet == -1)
		return TPV_SET(dst, TPV_NULL);

	int res = 0;
	char *price = tpvar_strdup(argv[0]);
	char *old_price = tpvar_strdup(argv[1]);
	if (!strcmp(price, "") || !strcmp(old_price, ""))
		goto freeing;

	char *src, *d;
	for (src = d = price; *src; src++) {
		if (!isspace(*src))
			*d++ = *src;
	}
	*d = '\0';
	for (src = d = old_price; *src; src++) {
		if (!isspace(*src))
			*d++ = *src;
	}
	*d = '\0';

	long long converted_price = atoll(price);
	long long converted_old_price = atoll(old_price);
	const char *conv_factor = _get_uf_conversion_factor(api);
	const char *arg_currency, *arg_old_currency;

	if (argc > 3) {
		TPVAR_STRTMP(arg_currency, argv[3]);
		if (!strcmp(arg_currency, CURRENCY_UF))
			converted_price = convert_uf_to_pesos(conv_factor, price);
	}

	if (argc > 4) {
		TPVAR_STRTMP(arg_old_currency, argv[4]);
		if (!strcmp(arg_old_currency, CURRENCY_UF))
			converted_old_price = convert_uf_to_pesos(conv_factor, old_price);
	}

	const char *redarrow_time = vtree_get(&api->vchain, "common", "list", "red_arrow_allowed_age", NULL);
	int ra_time = redarrow_time ? atoi(redarrow_time) : 604800;

	res = timet < time(NULL) - ra_time && converted_price <= get_red_arrow_price(converted_old_price);

freeing:
	free(price);
	free(old_price);
	return TPV_SET(dst, res ? TPV_INT(1) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(get_price_lowered);

/*
 * Create hash for ip address using seed for randomise
 * 
 */
static const struct tpvar *
hash_ip (struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {

        unsigned int octet, count;

	/* the ip address */
	const char *str2;

	/* the ip hashed */
	unsigned int ip;

	/* the seed */
	const char *sseed;
	int seed;
	
	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(str2, argv[0]);
	TPVAR_STRTMP(sseed, argv[1]);

	//  get the integer representation for seed 
	seed = atoi(sseed);

        octet=0;
        ip=0;
        count=0;

        while(1) {
                if(*str2=='.') {
                        ip=(ip<<8)+octet;
                        octet=0;
                        count++;
                } else if(*str2>='0' && *str2<='9') {
                        octet=octet*10 + (*str2-'0');
                        if(octet>255) return TPV_SET(dst, TPV_NULL);
                } else if(*str2==0 && count==3) {
                        ip=(ip<<8)+octet;
                        break;
                } else {
                        return TPV_SET(dst, TPV_NULL);
                }
                str2++;
        }
        seed = seed<<24|seed<<16|seed<<8|seed;

        ip = ip^seed;

	return TPV_SET(dst, ip ? TPV_INT(ip) : TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(hash_ip);


/* XXX deprecated since platform version 2.2.0, "resurrected" since in Yapo they are used a lot	*/
/* Convert all of these into template-based function ASAP, please				*/
static const struct tpvar *
format_price(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	const char *price;
	const char *tsep = " ";
	int addplus = 0;
	int mod_off = 0;
	int i = 0;

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);
	if (argc >= 2 && tpvar_int(argv[1]))
		addplus = 1;
	if (argc >= 3)
		TPVAR_STRTMP(tsep, argv[2]);

	TPVAR_STRTMP(price, argv[0]);
	int price_len = strlen(price);
	char *res = xmalloc(2+price_len*2);
	int ri = 0;

	if (*price == '-') {
		res[ri++] = *price++;
		mod_off = 1;
	} else if (addplus) {
		res[ri++] = '+';
	}

	while (price[i]) {
		if (i && ((i+mod_off) % 3 == (price_len % 3)) && *tsep)
			res[ri++] = *tsep;
		res[ri++] = price[i++];
	}
	res[ri] = '\0';
	return TPV_SET(dst, TPV_DYNSTR(res));
}
ADD_TEMPLATE_FUNCTION(format_price);


struct has_feature_data {
	const char *category;
	const char *type;
	int has_feature;
};

static const char *
has_feature_key_lookup(const char *settings, const char *key, void *cbdata) {
	struct has_feature_data *hfd = cbdata;

	if (strcmp(key, "category") == 0) {
		return hfd->category;
	} else if (strcmp(key, "type") == 0) {
		return hfd->type;
	}
	return NULL;
}

static void
has_feature_cb(const char *setting, const char *key, const char *value, void *cbdata) {
	struct has_feature_data *hfd = cbdata;

	if (key) {
		hfd->has_feature = 1;
	}
}

/* XXX Only supports category and type */
static int
has_feature(struct bpapi_vtree_chain *vchain, const char *feature, const char *category, const char *type) {
	struct has_feature_data hfd;
	hfd.category = category;
	hfd.type = type;
	hfd.has_feature = 0;
	struct bpapi_vtree_chain vtree = {0};
	
	get_settings(vtree_getnode(vchain, &vtree, "category_settings", NULL), feature,
		     has_feature_key_lookup,
		     has_feature_cb, &hfd);
	if (vtree.fun)
		vtree_free(&vtree);
	return hfd.has_feature;
}


struct cat_type_name_data {
	const char *cat;
	const char *type;
	char *value;

	struct bpapi_simplevars_chain *schain;
};

static const char *
cat_type_name_key_lookup(const char *settings, const char *key, void *cbdata) {
	struct cat_type_name_data *data = cbdata;

	if (strcmp(key, "category") == 0)
		return data->cat;
	if (strcmp(key, "type") == 0)
		return data->type;
	return bps_get_element(data->schain, key, 0);
}

static void
cat_type_name_set_value(const char *setting, const char *key, const char *value, void *cbdata) {
	struct cat_type_name_data *data = cbdata;

	if (strcmp(key, "value") == 0 && value && !data->value)
		data->value = xstrdup(value);
}


static const struct tpvar *
cat_type_name(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	struct bpapi_vtree_chain cat_node = {0};
	char *fcat, *ftype = NULL;
	const char *cat = tpvar_str(argv[0], &fcat);
	const char *type = NULL;
	const char *cat_name = NULL;
	const char *cat_parent = NULL;
	int start_level = 1;
	int end_level = -1;
	char *res = NULL;
	struct cat_type_name_data data;
	struct bpapi_vtree_chain lang_settings = {0};
	int has_lang_settings;

	if (argc >= 2)
		type = tpvar_str(argv[1], &ftype);

	if (argc >= 3)
		start_level = tpvar_int(argv[2]);

	if (argc >= 4)
		end_level = tpvar_int(argv[3]);

	data.cat = cat;
	data.type = type;
	data.value = NULL;
	data.schain = &api->schain;
	has_lang_settings = (vtree_getnode(&api->vchain, &lang_settings, "lang_settings", NULL) != NULL);

	if (vtree_getnode(&api->vchain, &cat_node, "old_cat", cat, NULL)) {
		cat_name = vtree_get(&cat_node, "name", bpapi_get_element(api, "b__lang", 0), NULL);
		if (!cat_name || !*cat_name)
			cat_name = vtree_get(&cat_node, "name", NULL);
		if (cat_name) {
			if (!type || vtree_getint(&cat_node, "no_ad_type", NULL))
				TPV_SET(dst, TPV_STRING(cat_name));
			else if (has_lang_settings) {
				get_settings(&lang_settings, "type_name", cat_type_name_key_lookup, cat_type_name_set_value, &data);
				if (data.value) {
					xasprintf(&res, "%s, %s", cat_name, data.value);
					free(data.value);
					TPV_SET(dst, TPV_DYNSTR(res));
				} else {
					TPV_SET(dst, TPV_STRING(cat_name));
				}
			}
		}
		vtree_free(&cat_node);
	} else if (vtree_getnode(&api->vchain, &cat_node, "cat", cat, NULL)) {
		char *buf = NULL;
		char *nbuf = NULL;
		int level;

		if ((cat_name = vtree_get(&cat_node, "name", bpapi_get_element(api, "b__lang", 0), NULL)) == NULL || *cat_name == '\0')
			cat_name = vtree_get(&cat_node, "name", NULL);
		cat_parent = vtree_get(&cat_node, "parent", NULL);

		if (type && *type && has_lang_settings
		    && !has_feature(&api->vchain, "no_ad_type", cat, type) 
		    && !has_feature(&api->vchain, "no_ad_type", cat_parent, type)) {
			get_settings(&lang_settings, "type_name", cat_type_name_key_lookup, cat_type_name_set_value, &data);

			buf = data.value;
		}

		while ( (level = vtree_getint(&cat_node, "level", NULL)) >= start_level) {
			if (cat_name && cat_name[0] && (end_level == -1 || level <= end_level)) {
				if (buf) {
					xasprintf(&nbuf, "%s, %s", cat_name, buf);
					free(buf);
					buf = nbuf;
				} else 
					buf = xstrdup(cat_name);
			}

			if (!cat_parent || !cat_parent[0])
				break;
			if (!vtree_getnode(&api->vchain, &cat_node, "cat", cat_parent, NULL))
				break;
			if ((cat_name = vtree_get(&cat_node, "name", bpapi_get_element(api, "b__lang", 0), NULL)) == NULL || *cat_name == '\0')
				cat_name = vtree_get(&cat_node, "name", NULL);
			cat_parent = vtree_get(&cat_node, "parent", NULL);
		}
		vtree_free(&cat_node);

		TPV_SET(dst, buf ? TPV_DYNSTR(buf) : TPV_NULL);
	} else if (has_lang_settings) {
		/* No valid category given, try just outputting the type */
		get_settings(&lang_settings, "type_name", cat_type_name_key_lookup, cat_type_name_set_value, &data);

		if (data.value)
			TPV_SET(dst, TPV_DYNSTR(data.value));
		else
			TPV_SET(dst, TPV_NULL);
	} else {
		TPV_SET(dst, TPV_NULL);
	}
	free(fcat);
	free(ftype);
	return dst;
}
ADD_TEMPLATE_FUNCTION(cat_type_name);

static const struct tpvar *
area_descr(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *munic;
	char *area;
	const char *val;
	char *res = NULL;
	struct bpapi_vtree_chain root = {0};
	const char *lang;
	const char *region, *city;
	char *fr, *fc;

	if (argc < 3) {
		return TPV_SET(dst, TPV_NULL);
	}

	region = tpvar_str(argv[0], &fr);
	city = tpvar_str(argv[1], &fc);

	if (atoi(city)) {
		if (!vtree_getnode(&api->vchain, &root, "common", "region", region, "city", city, NULL)) {
			free(fr);
			free(fc);
			return TPV_SET(dst, TPV_NULL);
		}
	} else {
		if (!vtree_getnode(&api->vchain, &root, "common", "region", region, NULL)) {
			free(fr);
			free(fc);
			return TPV_SET(dst, TPV_NULL);
		}
	}
	free(fr);
	free(fc);

	munic = tpvar_strdup(argv[2]);
	area = strchr(munic, ':');
	lang = bpapi_get_element(api, "b__lang", 0);
	if (lang && !*lang)
		lang = NULL;
	if (!area) {
		val = vtree_get(&root, "municipality", munic, "name", lang, NULL);
		if (!val || !*val)
			val = vtree_get(&root, "municipality", munic, "name", NULL);
		vtree_free(&root);
		free(munic);
		if (val)
			return TPV_SET(dst, TPV_STRING(val));
		return TPV_SET(dst, TPV_NULL);
	}
	*area++ = '\0';
	val = vtree_get(&root, "municipality", munic, "subarea", area, "name", lang, NULL);
	if (!val || !*val)
		val = vtree_get(&root, "municipality", munic, "subarea", area, "name", NULL);
	if (val && val[0]) {
		/* Do not print municipality if argv[3] is set to 1*/
		if (argc > 3 && tpvar_int(argv[3])) {
			TPV_SET(dst, TPV_STRING(val));
		} else {
			const char *m = vtree_get(&root, "municipality", munic, "name", lang, NULL);

			if (!m || !*m)
				m = vtree_get(&root, "municipality", munic, "name", NULL);

			xasprintf(&res, "%s - %s", m, val);
			TPV_SET(dst, TPV_DYNSTR(res));
		}
	} else if ((val = vtree_get(&root, "municipality", munic, "name", lang, NULL)) && *val) {
		TPV_SET(dst, TPV_STRING(val));
	} else if ((val = vtree_get(&root, "municipality", munic, "name", NULL))) {
		TPV_SET(dst, TPV_STRING(val));
	} else {
		TPV_SET(dst, TPV_NULL);
	}
	free(munic);
	vtree_free(&root);
	return dst;
}

ADD_TEMPLATE_FUNCTION_WEAK(area_descr);

/* XXX return the data or convert to filter */
static const struct tpvar *
format_weeks(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *weeks_tok;
	char *weeks;
	char *str;
	char *delim = NULL;
	int year;
	int week;
	int start_year = 0;
	int start_week = 0;
	int last_week = 0;
	int len = 0;
	const char *sep = "";

	if (argc < 1)
		return TPV_SET(dst, TPV_NULL);

	weeks = tpvar_strdup(argv[0]);
	weeks_tok = weeks;

	if (argc >= 2)
		TPVAR_STRTMP(sep, argv[1]);

	while ((str = strtok_r(weeks_tok, ",", &delim))) {
		sscanf(str, "%d:%d", &year, &week);
		weeks_tok = NULL;

		if (year != start_year) {
			if (start_week != last_week)
				len += bpapi_outstring_fmt(api, "-v%d", last_week);

			start_year = year;
			start_week = week;
			last_week = week;

			/* XXX Swedish */
			if (argc == 1) {
				len += bpapi_outstring_fmt(api, "%s%d: v%d", (len ? ".\n" : ""), year, week);
			} else {
				len += bpapi_outstring_fmt(api, "%s%s%d: v%d", (len ? "." : ""), (len ? sep : ""), year, week);
			}
			continue;
		}

		if (week == last_week + 1) {
			last_week = week;
		} else {
			if (start_week != last_week)
				len += bpapi_outstring_fmt(api, "-v%d, v%d", last_week, week);
			else
				len += bpapi_outstring_fmt(api, ", v%d", week);

			start_week = week;
			last_week = week;
		}
	}

	free(weeks);

	if (start_week != last_week)
		bpapi_outstring_fmt(api, "-v%d.", last_week);
	else if (len)
		bpapi_outstring_raw(api, ".", 1);
	return TPV_SET(dst, TPV_NULL);
}

ADD_TEMPLATE_FUNCTION(format_weeks);

/* XXX return the value, do not print it. Convert to filter? */
static const struct tpvar *
format_zipcode(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	if (argc != 1)
		return TPV_SET(dst, TPV_NULL);

	const char* s;
	int run_count = 0;
	char c;

	TPVAR_STRTMP(s, argv[0]);

	/* XXX Swedish? */
	while ((c = *s++)) {
		if (c == ' ') {
			continue;
		}
		bpapi_outstring_raw(api, &c, 1);
		if (++run_count == 3) {
			bpapi_outstring_raw(api, " ", 1);
			run_count = 0;
		}
	}
	return TPV_SET(dst, TPV_NULL);
}
ADD_TEMPLATE_FUNCTION(format_zipcode);


static const struct tpvar *
format_date_minus_days(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar **argv) {
	char *date;
	time_t t,current;
	struct tm tm = {0};
	const char *fmt;
	int ndays;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	date = xmalloc(64);

	TPVAR_STRTMP(fmt, argv[0]);
	ndays = tpvar_int(argv[1]);

	current = time(NULL);
	t = current - (ndays * 3600 * 24);
	strftime(date, 64, fmt, localtime_r(&t, &tm));
	*cc = BPCACHE_CANT;

	return TPV_SET(dst, TPV_DYNSTR(date));
}
ADD_TEMPLATE_FUNCTION(format_date_minus_days);
