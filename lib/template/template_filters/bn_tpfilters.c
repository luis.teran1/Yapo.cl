#include <string.h>
#include <ctype.h>
#include <bpapi.h>
#include <time.h>
#include <stdio.h>
#include <ctemplates.h>
#include <aes.h>
#include <strings.h>
#include <qsort.h>
#include <logging.h>
#include <math.h>
#include "sha1.h"
#include "queue.h"
#include <limits.h>
#include "atomic.h"
#include "hash.h"
#ifdef _REENTRANT
#include <pthread.h>
#endif
#include "sbo.h"

struct bnjoin_data
{
	struct buf_string buf;
	const char *separator;
	char *fs;
	int seplen;
	int nflushed;
};

static int 
bnjoin_outstring_raw(struct bpapi_output_chain *ochain, const char *str, size_t len) {
	struct bnjoin_data *jd = ochain->data;

	bufwrite(&jd->buf.buf, &jd->buf.len, &jd->buf.pos, str, len);

	return 0;
}

static int
init_bnjoin(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct bnjoin_data *jd = BPAPI_OUTPUT_DATA(api);

	jd->separator = tpvar_str(argv[0], &jd->fs);
	jd->seplen = strlen(jd->separator);

	return 0;
}

static void
flush_bnjoin(struct bpapi_output_chain *ochain) {
	struct bnjoin_data *jd = ochain->data;

	if (jd->buf.buf) {
		if (jd->nflushed++) {
			bpo_outstring_raw(ochain->next, jd->separator, jd->seplen);
		}
		bpo_outstring_raw(ochain->next, jd->buf.buf, jd->buf.pos);
		free(jd->buf.buf);
		jd->buf.buf = NULL;
		jd->buf.pos = jd->buf.len = 0;
	}
}

static void
fini_bnjoin(struct bpapi_output_chain *ochain) {
	struct bnjoin_data *jd = ochain->data;

	if (jd->buf.buf) {
		bpo_outstring_raw(ochain->next, jd->buf.buf, jd->buf.pos);
		free(jd->buf.buf);
	}
	free(jd->fs);
}

const struct bpapi_output bnjoin_output = {
	self_outstring_fmt,
	bnjoin_outstring_raw,
	fini_bnjoin,
	flush_bnjoin
};

ADD_OUTPUT_FILTER(bnjoin, sizeof(struct bnjoin_data));


