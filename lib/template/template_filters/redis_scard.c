#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include <platform/lib/template/redis/redis_filters.h>


static const struct tpvar *
redis_scard(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *engine;
	const char *set;
	struct fd_pool *pool;
	struct fd_pool_conn *conn;

	if (argc != 2)
		return TPV_SET(dst, TPV_NULL);

	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(set, argv[1]);

	pool = get_redis_pool(api, engine);
	conn = redis_sock_conn(pool, "slave");
	int res = redis_sock_scard(conn, set);

	fd_pool_free_conn(conn);
	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION(redis_scard);
