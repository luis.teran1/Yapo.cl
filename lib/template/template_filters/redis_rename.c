
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <logging.h>

#include <platform/lib/template/redis/redis_filters.h>


static int
init_redis_rename(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct redis_filter_data *data = BPAPI_OUTPUT_DATA(api);
	const char *engine;

	TPVAR_STRTMP(engine, argv[0]);
	data->key = tpvar_str(argv[1], &data->fk);
	data->field = tpvar_str(argv[2], &data->ff);

	data->fsconf = get_redis_fsconf(api, engine);
	if (!data->fsconf)
		return 1;

	return 0;
}

static void
fini_redis_rename(struct bpapi_output_chain *ochain) {
	struct redis_filter_data *data = ochain->data;
	int err;

	if (!data->fsconf)
		goto out;

	struct fd_pool_conn *conn = redis_sock_conn(data->fsconf->pool, "master");

	err = redis_sock_rename(conn, data->key, data->field);
	if (err < 0)
		log_printf(LOG_CRIT, "redis_sock_rename call failed for (%s, %s) with error: %d", data->key, data->field, err);

	fd_pool_free_conn(conn);

out:
	free(data->buf.buf);
	free(data->fk);
	free(data->ff);
}

const struct bpapi_output redis_rename_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_redis_rename
};

ADD_OUTPUT_FILTER(redis_rename, sizeof(struct redis_filter_data));
