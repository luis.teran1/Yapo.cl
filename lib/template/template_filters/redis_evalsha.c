
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <patchvars.h>
#include <bconf.h>

#include <platform/lib/template/redis/redis_filters.h>


struct redis_keys_data {
	struct bconf_node *node;
};

static void
fini_redis_evalsha(struct patch_data *patch) {
	struct redis_keys_data *data = patch->user_data;
	bconf_free(&data->node);
	free(data);
}


static void
redis_evalsha_add(const void *value, size_t vlen, void *cbarg) {
	struct patch_data *patch = cbarg;
	struct redis_keys_data *data = patch->user_data;

	if (patch->prefix_len) {
		bps_insert(&patch->vars, patch->prefix, value);
		bconf_add_data(&data->node, patch->prefix, value);
	} else {
		bps_insert(&patch->vars, "redis_evalsha", value);
		bconf_add_data(&data->node, "redis_evalsha", value);
	}
}

static int
init_redis_evalsha(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct patch_data *patch = BPAPI_SIMPLEVARS_DATA(api);
	const char *engine, *sha, *prefix = NULL, *nkeys = "0";
	char *fp = NULL;
	struct fd_pool *pool;
	struct shadow_vtree *subtree = BPAPI_VTREE_DATA(api);

	struct redis_keys_data *data;
	data = zmalloc(sizeof(struct redis_keys_data));


	TPVAR_STRTMP(engine, argv[0]);
	TPVAR_STRTMP(sha, argv[1]);
	if (argc > 2) {
		prefix = tpvar_str(argv[2], &fp);
	} else {
		prefix = "redis_evalsha";
	}
	if (argc > 3) {
		TPVAR_STRTMP(nkeys, argv[3]);
	}

	/* load args into extra_args */
	int i, keys = atoi(nkeys);
	int nargs = argc > 4 ? argc-4 : 0;
	
	// We can't have more keys than total arguments
	if (keys > nargs)
		keys = nargs;

	const char *extra_args[nargs];
	for (i = 0; i < nargs; ++i)
		TPVAR_STRTMP(extra_args[i], argv[4+i]);

	patchvars_init(patch, "redis_evalsha", prefix, strlen(prefix), fini_redis_evalsha, data);

	pool = get_redis_pool(api, engine);
	struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");
	redis_sock_evalsha(conn, sha, keys, nargs, extra_args, redis_evalsha_add, patch);

	bconf_vtree(&subtree->vtree, data->node);

	fd_pool_free_conn(conn);
	free(fp);

	return 0;
}

ADD_FILTER(redis_evalsha, NULL, 0, &patch_simplevars, sizeof(struct patch_data), &shadow_vtree, sizeof (struct shadow_vtree));
