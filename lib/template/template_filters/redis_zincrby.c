
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <logging.h>

#include <platform/lib/template/redis/redis_filters.h>


static int
init_redis_zincrby(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	int incr;
	char *fe, *fk, *ff;
	const char *engine, *key, *field;
	fe = fk = ff = NULL;

	engine = tpvar_str(argv[0], &fe);
	key = tpvar_str(argv[1], &fk);
	field = tpvar_str(argv[2], &ff);
	incr = tpvar_int(argv[3]);

	struct fd_pool *pool = get_redis_pool(api, engine);

	if (pool) {
		struct fd_pool_conn *conn = redis_sock_conn(pool, "master");

		int err = redis_sock_zincrby(conn, key, incr, field);
		if (err < 0)
			log_printf(LOG_CRIT, "redis_sock_zincrby call failed: %d", err);

		fd_pool_free_conn(conn);
	}

	free(fe);
	free(fk);
	free(ff);

	return 0;
}

const struct bpapi_output redis_zincrby_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	NULL
};

ADD_OUTPUT_FILTER(redis_zincrby, 0);
