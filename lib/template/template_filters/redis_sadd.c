
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>
#include <logging.h>

#include <platform/lib/template/redis/redis_filters.h>


static int
init_redis_sadd(const struct bpfilter *filter, struct bpapi *api, int argc, const struct tpvar **argv) {
	struct redis_filter_data *data = BPAPI_OUTPUT_DATA(api);

	const char *engine;

	TPVAR_STRTMP(engine, argv[0]);
	data->fsconf = get_redis_fsconf(api, engine);
	data->key = tpvar_str(argv[1], &data->fk);

	return 0;
}

static void
fini_redis_sadd(struct bpapi_output_chain *ochain) {
	struct redis_filter_data *data = ochain->data;
	struct fd_pool_conn *conn = redis_sock_conn(data->fsconf->pool, "master");

	int err = redis_sock_sadd(conn, data->key, data->buf.buf ?: "");
	if (err < 0)
		log_printf(LOG_CRIT, "redis_sock_sadd call failed: %d", err);

	fd_pool_free_conn(conn);

	free(data->buf.buf);
	free(data->fk);
}

const struct bpapi_output redis_sadd_output = {
	bufstring_outstring_fmt,
	bufstring_outstring_raw,
	fini_redis_sadd
};

ADD_OUTPUT_FILTER(redis_sadd, sizeof(struct redis_filter_data));
