
#include <bpapi.h>
#include <sredisapi.h>
#include <fd_pool.h>

#include <platform/lib/template/redis/redis_filters.h>


static const struct tpvar *
redis_sismember(struct bpapi *api, struct tpvar *dst, enum bpcacheable *cc, int argc, const struct tpvar *argv[]) {
	const char *engine;
	const char *key;
	const char *field;
	struct fd_pool *pool;

	switch (argc) {
		case 3:
			TPVAR_STRTMP(engine, argv[0]);
			TPVAR_STRTMP(key, argv[1]);
			TPVAR_STRTMP(field, argv[2]);
			break;
		default:
			return TPV_SET(dst, TPV_NULL);
	}

	pool = get_redis_pool(api, engine);

	struct fd_pool_conn *conn = redis_sock_conn(pool, "slave");

	int res = redis_sock_sismember(conn, key, field);

	fd_pool_free_conn(conn);

	*cc = BPCACHE_CANT;
	return TPV_SET(dst, TPV_INT(res));
}

ADD_TEMPLATE_FUNCTION(redis_sismember);
