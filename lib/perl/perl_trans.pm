use strict;
use Socket;
use IO::Handle;
use IO::Poll qw(POLLIN);
use IPC::Open2;
use POSIX ":sys_wait_h";

# Use do_trans if you want to connect to a specific transhost
# Otherwise use bconf_trans
sub do_trans {
	# Perl does not support IPv6, thus we fork nc to do the connection. Yeah, it's a bit silly.
	my ($hostname, $port, $param_ref, $array_ref) = @_;

	my ($addr, $paddr, $key, $line);
	my %resp;
	
	$addr = inet_aton($hostname);
	$paddr = sockaddr_in($port, $addr);

	open2 (*TRANSread, *TRANSwrite, "nc $hostname $port") || return %resp;

	my $poll = new IO::Poll;
	my $handle = new IO::Handle;

	$handle->fdopen(fileno(TRANSread), "r+") || return %resp;

	$poll->mask($handle => POLLIN);
	if ($poll->poll(5000)) {
		my $line = <TRANSread>;
		if ($line !~ /220 Welcome\./) {
			return %resp;
		}
	}

	TRANSwrite->autoflush(1);
  
	print TRANSwrite "newline:\n";

	my %hash = %{$param_ref};
	foreach $key (keys %hash) {
		my $val = $hash{$key};
		next if !defined($val);

		$val = [$val] if ref ($val) ne "ARRAY";

		foreach my $v (@$val) {
			if ($v =~ /[\0\n\r]/) {
				print TRANSwrite "blob:" . length($v) . ":$key\n" . $v;
			} else {
				print TRANSwrite "$key:" . $v . "\n";
			}
		}
	}

	if (defined($array_ref)) {
		print TRANSwrite @{$array_ref};
	}

	print TRANSwrite "end\n";
	shutdown(TRANSwrite, 1);

	while (defined($line = <TRANSread>)) {
		chomp($line);
		if ($line =~ /^blob:([^:]*):([^:]*)$/) {
			my $blob = '';
			my $read = 0;
			my $key = $2;
			my $len = $1;

			while ($read < $len) {
				my $data;
       				$read += read(TRANSread, $data, $len - $read);
				$blob .= $data;
			}
			push @{$resp{$key}}, $blob;
		} elsif ($line =~ /^([^:]*):(.*)$/) {
			push @{$resp{$1}}, $2;
		}
	}
	for (keys %resp) {
	    $resp{$_} = ${$resp{$_}}[0] if (!$#{$resp{$_}});
	}
	$resp{'status'} = 'TRANS_FATAL:Unexpected EOF' if !$resp{'status'};
	
	close(TRANSwrite);
	close(TRANSread);

	my $pid;
	do {
		$pid = waitpid(-1, WNOHANG);
	} while $pid > 0;

	return %resp;
}

sub bconf_trans {
	my ($bconf, %params) = @_;

	my %resp;

	foreach my $num (sort {$a <=> $b} keys %$bconf) {
		my $host = $$bconf{$num};

		%resp = do_trans($host->{'name'}, $host->{'port'}, \%params);
		return %resp if %resp;
	}

	$resp{'status'} = 'TRANS_FATAL:bconf_trans() failed to contact any transhost';

	return %resp;
}

1;
