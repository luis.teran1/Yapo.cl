/* ANSI-C code produced by gperf version 3.0.1 */
/* Command-line: gperf -L ANSI-C --output-file=/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.h /home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf  */
/* Computed positions: -k'1,3-11,$' */

#if !((' ' == 32) && ('!' == 33) && ('"' == 34) && ('#' == 35) \
      && ('%' == 37) && ('&' == 38) && ('\'' == 39) && ('(' == 40) \
      && (')' == 41) && ('*' == 42) && ('+' == 43) && (',' == 44) \
      && ('-' == 45) && ('.' == 46) && ('/' == 47) && ('0' == 48) \
      && ('1' == 49) && ('2' == 50) && ('3' == 51) && ('4' == 52) \
      && ('5' == 53) && ('6' == 54) && ('7' == 55) && ('8' == 56) \
      && ('9' == 57) && (':' == 58) && (';' == 59) && ('<' == 60) \
      && ('=' == 61) && ('>' == 62) && ('?' == 63) && ('A' == 65) \
      && ('B' == 66) && ('C' == 67) && ('D' == 68) && ('E' == 69) \
      && ('F' == 70) && ('G' == 71) && ('H' == 72) && ('I' == 73) \
      && ('J' == 74) && ('K' == 75) && ('L' == 76) && ('M' == 77) \
      && ('N' == 78) && ('O' == 79) && ('P' == 80) && ('Q' == 81) \
      && ('R' == 82) && ('S' == 83) && ('T' == 84) && ('U' == 85) \
      && ('V' == 86) && ('W' == 87) && ('X' == 88) && ('Y' == 89) \
      && ('Z' == 90) && ('[' == 91) && ('\\' == 92) && (']' == 93) \
      && ('^' == 94) && ('_' == 95) && ('a' == 97) && ('b' == 98) \
      && ('c' == 99) && ('d' == 100) && ('e' == 101) && ('f' == 102) \
      && ('g' == 103) && ('h' == 104) && ('i' == 105) && ('j' == 106) \
      && ('k' == 107) && ('l' == 108) && ('m' == 109) && ('n' == 110) \
      && ('o' == 111) && ('p' == 112) && ('q' == 113) && ('r' == 114) \
      && ('s' == 115) && ('t' == 116) && ('u' == 117) && ('v' == 118) \
      && ('w' == 119) && ('x' == 120) && ('y' == 121) && ('z' == 122) \
      && ('{' == 123) && ('|' == 124) && ('}' == 125) && ('~' == 126))
/* The character set is not based on ISO-646.  */
#error "gperf generated tables don't work with this execution character set. Please report a bug to <bug-gnu-gperf@gnu.org>."
#endif

#line 1 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
struct alias { int name; unsigned int encoding_index; };

#define TOTAL_KEYWORDS 329
#define MIN_WORD_LENGTH 2
#define MAX_WORD_LENGTH 45
#define MIN_HASH_VALUE 2
#define MAX_HASH_VALUE 1394
/* maximum key range = 1393, duplicates = 0 */

#ifdef __GNUC__
__inline
#else
#ifdef __cplusplus
inline
#endif
#endif
static unsigned int
aliases_hash (register const char *str, register unsigned int len)
{
  static const unsigned short asso_values[] =
    {
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395,    5,    5, 1395,   95,    0,
        25,   15,  175,    5,   10,  190,   30,   40,  320, 1395,
      1395, 1395, 1395, 1395, 1395,  100,  405,  130,   50,  170,
       385,   15,  125,    0,  290,   75,    0,   25,    0,    0,
        50, 1395,  210,    5,    5,  155,   80,   75,  445,    0,
         0, 1395, 1395, 1395, 1395,  245, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395,
      1395, 1395, 1395, 1395, 1395, 1395, 1395, 1395
    };
  register int hval = len;

  switch (hval)
    {
      default:
        hval += asso_values[(unsigned char)str[10]];
      /*FALLTHROUGH*/
      case 10:
        hval += asso_values[(unsigned char)str[9]];
      /*FALLTHROUGH*/
      case 9:
        hval += asso_values[(unsigned char)str[8]];
      /*FALLTHROUGH*/
      case 8:
        hval += asso_values[(unsigned char)str[7]];
      /*FALLTHROUGH*/
      case 7:
        hval += asso_values[(unsigned char)str[6]];
      /*FALLTHROUGH*/
      case 6:
        hval += asso_values[(unsigned char)str[5]];
      /*FALLTHROUGH*/
      case 5:
        hval += asso_values[(unsigned char)str[4]];
      /*FALLTHROUGH*/
      case 4:
        hval += asso_values[(unsigned char)str[3]];
      /*FALLTHROUGH*/
      case 3:
        hval += asso_values[(unsigned char)str[2]];
      /*FALLTHROUGH*/
      case 2:
      case 1:
        hval += asso_values[(unsigned char)str[0]];
        break;
    }
  return hval + asso_values[(unsigned char)str[len - 1]];
}

struct stringpool_t
  {
    char stringpool_str2[sizeof("L1")];
    char stringpool_str7[sizeof("L5")];
    char stringpool_str11[sizeof("LATIN1")];
    char stringpool_str12[sizeof("L6")];
    char stringpool_str17[sizeof("L3")];
    char stringpool_str19[sizeof("SJIS")];
    char stringpool_str21[sizeof("LATIN5")];
    char stringpool_str27[sizeof("L2")];
    char stringpool_str31[sizeof("LATIN6")];
    char stringpool_str32[sizeof("L8")];
    char stringpool_str41[sizeof("LATIN3")];
    char stringpool_str53[sizeof("866")];
    char stringpool_str61[sizeof("LATIN2")];
    char stringpool_str71[sizeof("LATIN8")];
    char stringpool_str83[sizeof("862")];
    char stringpool_str89[sizeof("TCVN")];
    char stringpool_str91[sizeof("IBM866")];
    char stringpool_str97[sizeof("LATIN-9")];
    char stringpool_str105[sizeof("MS936")];
    char stringpool_str111[sizeof("GB2312")];
    char stringpool_str119[sizeof("ISO8859-1")];
    char stringpool_str121[sizeof("IBM862")];
    char stringpool_str125[sizeof("ISO-8859-1")];
    char stringpool_str126[sizeof("KOI8-T")];
    char stringpool_str127[sizeof("HZ")];
    char stringpool_str129[sizeof("ISO8859-5")];
    char stringpool_str130[sizeof("ISO8859-15")];
    char stringpool_str132[sizeof("CN")];
    char stringpool_str135[sizeof("ISO-8859-5")];
    char stringpool_str136[sizeof("ISO-8859-15")];
    char stringpool_str139[sizeof("ISO8859-6")];
    char stringpool_str140[sizeof("ISO8859-16")];
    char stringpool_str141[sizeof("IBM819")];
    char stringpool_str142[sizeof("MS-ANSI")];
    char stringpool_str145[sizeof("ISO-8859-6")];
    char stringpool_str146[sizeof("ISO-8859-16")];
    char stringpool_str149[sizeof("ISO8859-3")];
    char stringpool_str150[sizeof("ISO8859-13")];
    char stringpool_str155[sizeof("ISO-8859-3")];
    char stringpool_str156[sizeof("ISO-8859-13")];
    char stringpool_str161[sizeof("CP1361")];
    char stringpool_str162[sizeof("US")];
    char stringpool_str166[sizeof("CP1251")];
    char stringpool_str168[sizeof("GBK")];
    char stringpool_str169[sizeof("ISO8859-2")];
    char stringpool_str175[sizeof("ISO-8859-2")];
    char stringpool_str176[sizeof("CP1255")];
    char stringpool_str177[sizeof("L4")];
    char stringpool_str179[sizeof("ISO8859-8")];
    char stringpool_str181[sizeof("CP1133")];
    char stringpool_str185[sizeof("ISO-8859-8")];
    char stringpool_str186[sizeof("CP1256")];
    char stringpool_str192[sizeof("L7")];
    char stringpool_str193[sizeof("L10")];
    char stringpool_str195[sizeof("CP866")];
    char stringpool_str196[sizeof("CP1253")];
    char stringpool_str199[sizeof("ISO8859-9")];
    char stringpool_str202[sizeof("LATIN10")];
    char stringpool_str205[sizeof("ISO-8859-9")];
    char stringpool_str210[sizeof("CP936")];
    char stringpool_str213[sizeof("C99")];
    char stringpool_str216[sizeof("CP1252")];
    char stringpool_str218[sizeof("TIS620.2533-1")];
    char stringpool_str220[sizeof("UCS-2")];
    char stringpool_str221[sizeof("VISCII")];
    char stringpool_str223[sizeof("850")];
    char stringpool_str225[sizeof("CP862")];
    char stringpool_str226[sizeof("CP1258")];
    char stringpool_str235[sizeof("ASCII")];
    char stringpool_str236[sizeof("VISCII1.1-1")];
    char stringpool_str240[sizeof("CP932")];
    char stringpool_str241[sizeof("TIS620")];
    char stringpool_str242[sizeof("R8")];
    char stringpool_str245[sizeof("CP819")];
    char stringpool_str247[sizeof("TIS-620")];
    char stringpool_str248[sizeof("ISO-IR-6")];
    char stringpool_str250[sizeof("ISO-IR-165")];
    char stringpool_str251[sizeof("CSISOLATIN1")];
    char stringpool_str252[sizeof("WINDOWS-1251")];
    char stringpool_str253[sizeof("TIS620.2529-1")];
    char stringpool_str256[sizeof("IBM850")];
    char stringpool_str257[sizeof("WINDOWS-1255")];
    char stringpool_str260[sizeof("ISO-IR-166")];
    char stringpool_str261[sizeof("CSISOLATIN5")];
    char stringpool_str262[sizeof("WINDOWS-1256")];
    char stringpool_str265[sizeof("IBM-CP1133")];
    char stringpool_str267[sizeof("WINDOWS-1253")];
    char stringpool_str271[sizeof("CSISOLATIN6")];
    char stringpool_str275[sizeof("ISO-IR-126")];
    char stringpool_str277[sizeof("WINDOWS-1252")];
    char stringpool_str281[sizeof("CSISOLATIN3")];
    char stringpool_str282[sizeof("WINDOWS-1258")];
    char stringpool_str288[sizeof("MAC")];
    char stringpool_str294[sizeof("ISO-IR-58")];
    char stringpool_str296[sizeof("WINDOWS-936")];
    char stringpool_str300[sizeof("ISO-IR-226")];
    char stringpool_str301[sizeof("CSISOLATIN2")];
    char stringpool_str305[sizeof("ISO-IR-138")];
    char stringpool_str309[sizeof("MULELAO-1")];
    char stringpool_str310[sizeof("ISO8859-10")];
    char stringpool_str313[sizeof("TIS620.2533-0")];
    char stringpool_str315[sizeof("ISO-IR-159")];
    char stringpool_str316[sizeof("ISO-8859-10")];
    char stringpool_str320[sizeof("TCVN5712-1")];
    char stringpool_str321[sizeof("ISO-2022-CN")];
    char stringpool_str325[sizeof("ISO-IR-101")];
    char stringpool_str330[sizeof("ISO-2022-CN-EXT")];
    char stringpool_str334[sizeof("ARMSCII-8")];
    char stringpool_str339[sizeof("ISO646-CN")];
    char stringpool_str342[sizeof("JP")];
    char stringpool_str343[sizeof("TIS620-0")];
    char stringpool_str344[sizeof("TCVN-5712")];
    char stringpool_str345[sizeof("CSISOLATINGREEK")];
    char stringpool_str347[sizeof("WINDOWS-1250")];
    char stringpool_str350[sizeof("ISO-IR-199")];
    char stringpool_str352[sizeof("GB18030")];
    char stringpool_str353[sizeof("CSVISCII")];
    char stringpool_str356[sizeof("CP1250")];
    char stringpool_str360[sizeof("CP850")];
    char stringpool_str361[sizeof("LATIN4")];
    char stringpool_str365[sizeof("ISO_8859-1")];
    char stringpool_str368[sizeof("ECMA-118")];
    char stringpool_str370[sizeof("CP950")];
    char stringpool_str372[sizeof("CSASCII")];
    char stringpool_str374[sizeof("ISO646-US")];
    char stringpool_str375[sizeof("ISO_8859-5")];
    char stringpool_str376[sizeof("ISO_8859-15")];
    char stringpool_str377[sizeof("MS-CYRL")];
    char stringpool_str380[sizeof("ISO-IR-203")];
    char stringpool_str381[sizeof("ISO_8859-16:2001")];
    char stringpool_str385[sizeof("ISO_8859-6")];
    char stringpool_str386[sizeof("ISO_8859-16")];
    char stringpool_str391[sizeof("LATIN7")];
    char stringpool_str392[sizeof("MACTHAI")];
    char stringpool_str395[sizeof("ISO_8859-3")];
    char stringpool_str396[sizeof("ISO_8859-13")];
    char stringpool_str401[sizeof("ROMAN8")];
    char stringpool_str403[sizeof("US-ASCII")];
    char stringpool_str405[sizeof("ISO-IR-109")];
    char stringpool_str406[sizeof("ISO_8859-15:1998")];
    char stringpool_str415[sizeof("ISO_8859-2")];
    char stringpool_str416[sizeof("GEORGIAN-PS")];
    char stringpool_str418[sizeof("UHC")];
    char stringpool_str420[sizeof("ISO-IR-110")];
    char stringpool_str424[sizeof("MACINTOSH")];
    char stringpool_str425[sizeof("ISO_8859-8")];
    char stringpool_str426[sizeof("KOI8-U")];
    char stringpool_str427[sizeof("WINDOWS-1254")];
    char stringpool_str430[sizeof("CP949")];
    char stringpool_str434[sizeof("BIG5")];
    char stringpool_str435[sizeof("EUCCN")];
    char stringpool_str436[sizeof("IBM367")];
    char stringpool_str440[sizeof("BIG-5")];
    char stringpool_str441[sizeof("EUC-CN")];
    char stringpool_str442[sizeof("WINDOWS-1257")];
    char stringpool_str445[sizeof("ISO_8859-9")];
    char stringpool_str446[sizeof("CSISO2022CN")];
    char stringpool_str456[sizeof("CSISOLATINHEBREW")];
    char stringpool_str460[sizeof("EUCTW")];
    char stringpool_str465[sizeof("ISO-IR-148")];
    char stringpool_str466[sizeof("EUC-TW")];
    char stringpool_str469[sizeof("ISO8859-4")];
    char stringpool_str470[sizeof("ISO8859-14")];
    char stringpool_str475[sizeof("ISO-8859-4")];
    char stringpool_str476[sizeof("ISO-8859-14")];
    char stringpool_str483[sizeof("ASMO-708")];
    char stringpool_str485[sizeof("ISO-IR-149")];
    char stringpool_str486[sizeof("CSISOLATINARABIC")];
    char stringpool_str491[sizeof("ISO_8859-10:1992")];
    char stringpool_str495[sizeof("ISO-10646-UCS-2")];
    char stringpool_str496[sizeof("GREEK8")];
    char stringpool_str498[sizeof("MACROMAN")];
    char stringpool_str499[sizeof("ISO8859-7")];
    char stringpool_str500[sizeof("ISO-IR-179")];
    char stringpool_str505[sizeof("ISO-8859-7")];
    char stringpool_str510[sizeof("GREEK")];
    char stringpool_str515[sizeof("ISO-IR-100")];
    char stringpool_str516[sizeof("CP1254")];
    char stringpool_str518[sizeof("CSISOLATINCYRILLIC")];
    char stringpool_str520[sizeof("UCS-4")];
    char stringpool_str525[sizeof("X0212")];
    char stringpool_str526[sizeof("UNICODE-1-1")];
    char stringpool_str533[sizeof("ISO-2022-JP-1")];
    char stringpool_str534[sizeof("HP-ROMAN8")];
    char stringpool_str536[sizeof("KOI8-R")];
    char stringpool_str537[sizeof("UCS-2LE")];
    char stringpool_str538[sizeof("UCS-2-SWAPPED")];
    char stringpool_str540[sizeof("CP367")];
    char stringpool_str545[sizeof("MS-EE")];
    char stringpool_str546[sizeof("CP1257")];
    char stringpool_str553[sizeof("ELOT_928")];
    char stringpool_str556[sizeof("ISO_8859-10")];
    char stringpool_str557[sizeof("MS-TURK")];
    char stringpool_str558[sizeof("ISO-2022-JP-2")];
    char stringpool_str560[sizeof("GB_2312-80")];
    char stringpool_str561[sizeof("KOREAN")];
    char stringpool_str566[sizeof("CSBIG5")];
    char stringpool_str568[sizeof("KSC_5601")];
    char stringpool_str570[sizeof("X0201")];
    char stringpool_str571[sizeof("UTF-16")];
    char stringpool_str572[sizeof("CN-BIG5")];
    char stringpool_str573[sizeof("CSPC862LATINHEBREW")];
    char stringpool_str574[sizeof("JAVA")];
    char stringpool_str576[sizeof("ISO_8859-14:1998")];
    char stringpool_str577[sizeof("JIS0208")];
    char stringpool_str579[sizeof("ISO-IR-14")];
    char stringpool_str580[sizeof("ISO-CELTIC")];
    char stringpool_str581[sizeof("ISO-2022-JP")];
    char stringpool_str590[sizeof("CSISO159JISX02121990")];
    char stringpool_str591[sizeof("GEORGIAN-ACADEMY")];
    char stringpool_str594[sizeof("UCS-2-INTERNAL")];
    char stringpool_str595[sizeof("GB_1988-80")];
    char stringpool_str599[sizeof("ISO646-JP")];
    char stringpool_str601[sizeof("CSISOLATIN4")];
    char stringpool_str608[sizeof("CYRILLIC")];
    char stringpool_str610[sizeof("UTF-8")];
    char stringpool_str614[sizeof("ISO-IR-57")];
    char stringpool_str615[sizeof("ISO-IR-157")];
    char stringpool_str616[sizeof("UTF-32")];
    char stringpool_str628[sizeof("CSIBM866")];
    char stringpool_str630[sizeof("X0208")];
    char stringpool_str635[sizeof("ISO-IR-127")];
    char stringpool_str637[sizeof("KOI8-RU")];
    char stringpool_str639[sizeof("ISO-IR-87")];
    char stringpool_str644[sizeof("CSPC850MULTILINGUAL")];
    char stringpool_str645[sizeof("ISO-10646-UCS-4")];
    char stringpool_str646[sizeof("CSUNICODE11")];
    char stringpool_str648[sizeof("CSGB2312")];
    char stringpool_str652[sizeof("CHINESE")];
    char stringpool_str654[sizeof("CHAR")];
    char stringpool_str655[sizeof("HZ-GB-2312")];
    char stringpool_str656[sizeof("CSMACINTOSH")];
    char stringpool_str658[sizeof("ECMA-114")];
    char stringpool_str660[sizeof("TCVN5712-1:1993")];
    char stringpool_str662[sizeof("CSKOI8R")];
    char stringpool_str665[sizeof("MACICELAND")];
    char stringpool_str682[sizeof("CSISO2022JP2")];
    char stringpool_str686[sizeof("ISO-2022-KR")];
    char stringpool_str687[sizeof("UCS-4LE")];
    char stringpool_str688[sizeof("UCS-4-SWAPPED")];
    char stringpool_str693[sizeof("CSKSC56011987")];
    char stringpool_str695[sizeof("EUCJP")];
    char stringpool_str698[sizeof("UNICODELITTLE")];
    char stringpool_str700[sizeof("MACROMANIA")];
    char stringpool_str701[sizeof("EUC-JP")];
    char stringpool_str705[sizeof("CP874")];
    char stringpool_str706[sizeof("CSISO2022JP")];
    char stringpool_str709[sizeof("SHIFT-JIS")];
    char stringpool_str710[sizeof("CSHPROMAN8")];
    char stringpool_str711[sizeof("MACCROATIAN")];
    char stringpool_str715[sizeof("ISO_8859-4")];
    char stringpool_str716[sizeof("ISO_8859-14")];
    char stringpool_str722[sizeof("UNICODE-1-1-UTF-7")];
    char stringpool_str725[sizeof("ISO_8859-5:1988")];
    char stringpool_str735[sizeof("ISO_8859-3:1988")];
    char stringpool_str738[sizeof("NEXTSTEP")];
    char stringpool_str740[sizeof("CSISO58GB231280")];
    char stringpool_str743[sizeof("MS_KANJI")];
    char stringpool_str744[sizeof("UCS-4-INTERNAL")];
    char stringpool_str745[sizeof("ISO_8859-7")];
    char stringpool_str747[sizeof("CSEUCTW")];
    char stringpool_str750[sizeof("ISO_8859-8:1988")];
    char stringpool_str751[sizeof("ISO_646.IRV:1991")];
    char stringpool_str752[sizeof("CSISO14JISC6220RO")];
    char stringpool_str753[sizeof("MS-GREEK")];
    char stringpool_str755[sizeof("ISO-IR-144")];
    char stringpool_str766[sizeof("MACCYRILLIC")];
    char stringpool_str770[sizeof("ISO_8859-9:1989")];
    char stringpool_str772[sizeof("WCHAR_T")];
    char stringpool_str774[sizeof("JIS_C6226-1983")];
    char stringpool_str776[sizeof("CSUCS4")];
    char stringpool_str779[sizeof("BIG5HKSCS")];
    char stringpool_str785[sizeof("BIG5-HKSCS")];
    char stringpool_str791[sizeof("WINDOWS-874")];
    char stringpool_str794[sizeof("CN-GB-ISOIR165")];
    char stringpool_str800[sizeof("EUCKR")];
    char stringpool_str806[sizeof("EUC-KR")];
    char stringpool_str811[sizeof("CSISO2022KR")];
    char stringpool_str814[sizeof("CSUNICODE")];
    char stringpool_str833[sizeof("CSISO57GB1988")];
    char stringpool_str840[sizeof("CSUNICODE11UTF7")];
    char stringpool_str847[sizeof("JIS_C6220-1969-RO")];
    char stringpool_str855[sizeof("WINBALTRIM")];
    char stringpool_str864[sizeof("KS_C_5601-1989")];
    char stringpool_str865[sizeof("MACTURKISH")];
    char stringpool_str871[sizeof("ARABIC")];
    char stringpool_str878[sizeof("MACGREEK")];
    char stringpool_str880[sizeof("ISO_8859-1:1987")];
    char stringpool_str890[sizeof("ISO_8859-6:1987")];
    char stringpool_str895[sizeof("ISO_8859-4:1988")];
    char stringpool_str903[sizeof("UTF-16LE")];
    char stringpool_str905[sizeof("ISO_8859-2:1987")];
    char stringpool_str930[sizeof("UTF-7")];
    char stringpool_str933[sizeof("UTF-32LE")];
    char stringpool_str942[sizeof("UCS-2BE")];
    char stringpool_str949[sizeof("SHIFT_JIS")];
    char stringpool_str950[sizeof("UNICODEBIG")];
    char stringpool_str960[sizeof("CSSHIFTJIS")];
    char stringpool_str965[sizeof("CN-GB")];
    char stringpool_str1014[sizeof("KS_C_5601-1987")];
    char stringpool_str1019[sizeof("ANSI_X3.4-1986")];
    char stringpool_str1023[sizeof("JISX0201-1976")];
    char stringpool_str1039[sizeof("ANSI_X3.4-1968")];
    char stringpool_str1045[sizeof("MACUKRAINE")];
    char stringpool_str1066[sizeof("HEBREW")];
    char stringpool_str1070[sizeof("ISO_8859-7:1987")];
    char stringpool_str1087[sizeof("CSEUCKR")];
    char stringpool_str1092[sizeof("UCS-4BE")];
    char stringpool_str1114[sizeof("CSHALFWIDTHKATAKANA")];
    char stringpool_str1126[sizeof("MACCENTRALEUROPE")];
    char stringpool_str1140[sizeof("CSISO87JISX0208")];
    char stringpool_str1157[sizeof("MS-HEBR")];
    char stringpool_str1164[sizeof("JIS_X0212")];
    char stringpool_str1209[sizeof("JIS_X0201")];
    char stringpool_str1230[sizeof("EXTENDED_UNIX_CODE_PACKED_FORMAT_FOR_JAPANESE")];
    char stringpool_str1232[sizeof("BIGFIVE")];
    char stringpool_str1238[sizeof("BIG-FIVE")];
    char stringpool_str1239[sizeof("MACARABIC")];
    char stringpool_str1244[sizeof("JIS_X0212-1990")];
    char stringpool_str1246[sizeof("JIS_X0212.1990-0")];
    char stringpool_str1257[sizeof("MS-ARAB")];
    char stringpool_str1264[sizeof("JIS_X0208-1983")];
    char stringpool_str1269[sizeof("JIS_X0208")];
    char stringpool_str1308[sizeof("UTF-16BE")];
    char stringpool_str1330[sizeof("JOHAB")];
    char stringpool_str1338[sizeof("UTF-32BE")];
    char stringpool_str1344[sizeof("JIS_X0208-1990")];
    char stringpool_str1364[sizeof("CSEUCPKDFMTJAPANESE")];
    char stringpool_str1394[sizeof("MACHEBREW")];
  };
static const struct stringpool_t stringpool_contents =
  {
    "L1",
    "L5",
    "LATIN1",
    "L6",
    "L3",
    "SJIS",
    "LATIN5",
    "L2",
    "LATIN6",
    "L8",
    "LATIN3",
    "866",
    "LATIN2",
    "LATIN8",
    "862",
    "TCVN",
    "IBM866",
    "LATIN-9",
    "MS936",
    "GB2312",
    "ISO8859-1",
    "IBM862",
    "ISO-8859-1",
    "KOI8-T",
    "HZ",
    "ISO8859-5",
    "ISO8859-15",
    "CN",
    "ISO-8859-5",
    "ISO-8859-15",
    "ISO8859-6",
    "ISO8859-16",
    "IBM819",
    "MS-ANSI",
    "ISO-8859-6",
    "ISO-8859-16",
    "ISO8859-3",
    "ISO8859-13",
    "ISO-8859-3",
    "ISO-8859-13",
    "CP1361",
    "US",
    "CP1251",
    "GBK",
    "ISO8859-2",
    "ISO-8859-2",
    "CP1255",
    "L4",
    "ISO8859-8",
    "CP1133",
    "ISO-8859-8",
    "CP1256",
    "L7",
    "L10",
    "CP866",
    "CP1253",
    "ISO8859-9",
    "LATIN10",
    "ISO-8859-9",
    "CP936",
    "C99",
    "CP1252",
    "TIS620.2533-1",
    "UCS-2",
    "VISCII",
    "850",
    "CP862",
    "CP1258",
    "ASCII",
    "VISCII1.1-1",
    "CP932",
    "TIS620",
    "R8",
    "CP819",
    "TIS-620",
    "ISO-IR-6",
    "ISO-IR-165",
    "CSISOLATIN1",
    "WINDOWS-1251",
    "TIS620.2529-1",
    "IBM850",
    "WINDOWS-1255",
    "ISO-IR-166",
    "CSISOLATIN5",
    "WINDOWS-1256",
    "IBM-CP1133",
    "WINDOWS-1253",
    "CSISOLATIN6",
    "ISO-IR-126",
    "WINDOWS-1252",
    "CSISOLATIN3",
    "WINDOWS-1258",
    "MAC",
    "ISO-IR-58",
    "WINDOWS-936",
    "ISO-IR-226",
    "CSISOLATIN2",
    "ISO-IR-138",
    "MULELAO-1",
    "ISO8859-10",
    "TIS620.2533-0",
    "ISO-IR-159",
    "ISO-8859-10",
    "TCVN5712-1",
    "ISO-2022-CN",
    "ISO-IR-101",
    "ISO-2022-CN-EXT",
    "ARMSCII-8",
    "ISO646-CN",
    "JP",
    "TIS620-0",
    "TCVN-5712",
    "CSISOLATINGREEK",
    "WINDOWS-1250",
    "ISO-IR-199",
    "GB18030",
    "CSVISCII",
    "CP1250",
    "CP850",
    "LATIN4",
    "ISO_8859-1",
    "ECMA-118",
    "CP950",
    "CSASCII",
    "ISO646-US",
    "ISO_8859-5",
    "ISO_8859-15",
    "MS-CYRL",
    "ISO-IR-203",
    "ISO_8859-16:2001",
    "ISO_8859-6",
    "ISO_8859-16",
    "LATIN7",
    "MACTHAI",
    "ISO_8859-3",
    "ISO_8859-13",
    "ROMAN8",
    "US-ASCII",
    "ISO-IR-109",
    "ISO_8859-15:1998",
    "ISO_8859-2",
    "GEORGIAN-PS",
    "UHC",
    "ISO-IR-110",
    "MACINTOSH",
    "ISO_8859-8",
    "KOI8-U",
    "WINDOWS-1254",
    "CP949",
    "BIG5",
    "EUCCN",
    "IBM367",
    "BIG-5",
    "EUC-CN",
    "WINDOWS-1257",
    "ISO_8859-9",
    "CSISO2022CN",
    "CSISOLATINHEBREW",
    "EUCTW",
    "ISO-IR-148",
    "EUC-TW",
    "ISO8859-4",
    "ISO8859-14",
    "ISO-8859-4",
    "ISO-8859-14",
    "ASMO-708",
    "ISO-IR-149",
    "CSISOLATINARABIC",
    "ISO_8859-10:1992",
    "ISO-10646-UCS-2",
    "GREEK8",
    "MACROMAN",
    "ISO8859-7",
    "ISO-IR-179",
    "ISO-8859-7",
    "GREEK",
    "ISO-IR-100",
    "CP1254",
    "CSISOLATINCYRILLIC",
    "UCS-4",
    "X0212",
    "UNICODE-1-1",
    "ISO-2022-JP-1",
    "HP-ROMAN8",
    "KOI8-R",
    "UCS-2LE",
    "UCS-2-SWAPPED",
    "CP367",
    "MS-EE",
    "CP1257",
    "ELOT_928",
    "ISO_8859-10",
    "MS-TURK",
    "ISO-2022-JP-2",
    "GB_2312-80",
    "KOREAN",
    "CSBIG5",
    "KSC_5601",
    "X0201",
    "UTF-16",
    "CN-BIG5",
    "CSPC862LATINHEBREW",
    "JAVA",
    "ISO_8859-14:1998",
    "JIS0208",
    "ISO-IR-14",
    "ISO-CELTIC",
    "ISO-2022-JP",
    "CSISO159JISX02121990",
    "GEORGIAN-ACADEMY",
    "UCS-2-INTERNAL",
    "GB_1988-80",
    "ISO646-JP",
    "CSISOLATIN4",
    "CYRILLIC",
    "UTF-8",
    "ISO-IR-57",
    "ISO-IR-157",
    "UTF-32",
    "CSIBM866",
    "X0208",
    "ISO-IR-127",
    "KOI8-RU",
    "ISO-IR-87",
    "CSPC850MULTILINGUAL",
    "ISO-10646-UCS-4",
    "CSUNICODE11",
    "CSGB2312",
    "CHINESE",
    "CHAR",
    "HZ-GB-2312",
    "CSMACINTOSH",
    "ECMA-114",
    "TCVN5712-1:1993",
    "CSKOI8R",
    "MACICELAND",
    "CSISO2022JP2",
    "ISO-2022-KR",
    "UCS-4LE",
    "UCS-4-SWAPPED",
    "CSKSC56011987",
    "EUCJP",
    "UNICODELITTLE",
    "MACROMANIA",
    "EUC-JP",
    "CP874",
    "CSISO2022JP",
    "SHIFT-JIS",
    "CSHPROMAN8",
    "MACCROATIAN",
    "ISO_8859-4",
    "ISO_8859-14",
    "UNICODE-1-1-UTF-7",
    "ISO_8859-5:1988",
    "ISO_8859-3:1988",
    "NEXTSTEP",
    "CSISO58GB231280",
    "MS_KANJI",
    "UCS-4-INTERNAL",
    "ISO_8859-7",
    "CSEUCTW",
    "ISO_8859-8:1988",
    "ISO_646.IRV:1991",
    "CSISO14JISC6220RO",
    "MS-GREEK",
    "ISO-IR-144",
    "MACCYRILLIC",
    "ISO_8859-9:1989",
    "WCHAR_T",
    "JIS_C6226-1983",
    "CSUCS4",
    "BIG5HKSCS",
    "BIG5-HKSCS",
    "WINDOWS-874",
    "CN-GB-ISOIR165",
    "EUCKR",
    "EUC-KR",
    "CSISO2022KR",
    "CSUNICODE",
    "CSISO57GB1988",
    "CSUNICODE11UTF7",
    "JIS_C6220-1969-RO",
    "WINBALTRIM",
    "KS_C_5601-1989",
    "MACTURKISH",
    "ARABIC",
    "MACGREEK",
    "ISO_8859-1:1987",
    "ISO_8859-6:1987",
    "ISO_8859-4:1988",
    "UTF-16LE",
    "ISO_8859-2:1987",
    "UTF-7",
    "UTF-32LE",
    "UCS-2BE",
    "SHIFT_JIS",
    "UNICODEBIG",
    "CSSHIFTJIS",
    "CN-GB",
    "KS_C_5601-1987",
    "ANSI_X3.4-1986",
    "JISX0201-1976",
    "ANSI_X3.4-1968",
    "MACUKRAINE",
    "HEBREW",
    "ISO_8859-7:1987",
    "CSEUCKR",
    "UCS-4BE",
    "CSHALFWIDTHKATAKANA",
    "MACCENTRALEUROPE",
    "CSISO87JISX0208",
    "MS-HEBR",
    "JIS_X0212",
    "JIS_X0201",
    "EXTENDED_UNIX_CODE_PACKED_FORMAT_FOR_JAPANESE",
    "BIGFIVE",
    "BIG-FIVE",
    "MACARABIC",
    "JIS_X0212-1990",
    "JIS_X0212.1990-0",
    "MS-ARAB",
    "JIS_X0208-1983",
    "JIS_X0208",
    "UTF-16BE",
    "JOHAB",
    "UTF-32BE",
    "JIS_X0208-1990",
    "CSEUCPKDFMTJAPANESE",
    "MACHEBREW"
  };
#define stringpool ((const char *) &stringpool_contents)

static const struct alias aliases[] =
  {
    {-1}, {-1},
#line 60 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str2, ei_iso8859_1},
    {-1}, {-1}, {-1}, {-1},
#line 125 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str7, ei_iso8859_9},
    {-1}, {-1}, {-1},
#line 59 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str11, ei_iso8859_1},
#line 133 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str12, ei_iso8859_10},
    {-1}, {-1}, {-1}, {-1},
#line 76 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str17, ei_iso8859_3},
    {-1},
#line 294 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str19, ei_sjis},
    {-1},
#line 124 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str21, ei_iso8859_9},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 68 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str27, ei_iso8859_2},
    {-1}, {-1}, {-1},
#line 132 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str31, ei_iso8859_10},
#line 147 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str32, ei_iso8859_14},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 75 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str41, ei_iso8859_3},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 203 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str53, ei_cp866},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 67 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str61, ei_iso8859_2},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 146 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str71, ei_iso8859_14},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 199 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str83, ei_cp862},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 244 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str89, ei_tcvn},
    {-1},
#line 202 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str91, ei_cp866},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 154 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str97, ei_iso8859_15},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 310 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str105, ei_ces_gbk},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 305 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str111, ei_euc_cn},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 62 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str119, ei_iso8859_1},
    {-1},
#line 198 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str121, ei_cp862},
    {-1}, {-1}, {-1},
#line 53 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str125, ei_iso8859_1},
#line 228 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str126, ei_koi8_t},
#line 316 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str127, ei_hz},
    {-1},
#line 93 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str129, ei_iso8859_5},
#line 155 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str130, ei_iso8859_15},
    {-1},
#line 274 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str132, ei_iso646_cn},
    {-1}, {-1},
#line 87 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str135, ei_iso8859_5},
#line 150 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str136, ei_iso8859_15},
    {-1}, {-1},
#line 102 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str139, ei_iso8859_6},
#line 162 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str140, ei_iso8859_16},
#line 58 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str141, ei_iso8859_1},
#line 175 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str142, ei_cp1252},
    {-1}, {-1},
#line 94 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str145, ei_iso8859_6},
#line 156 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str146, ei_iso8859_16},
    {-1}, {-1},
#line 78 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str149, ei_iso8859_3},
#line 141 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str150, ei_iso8859_13},
    {-1}, {-1}, {-1}, {-1},
#line 71 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str155, ei_iso8859_3},
#line 136 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str156, ei_iso8859_13},
    {-1}, {-1}, {-1}, {-1},
#line 336 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str161, ei_johab},
#line 21 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str162, ei_ascii},
    {-1}, {-1}, {-1},
#line 170 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str166, ei_cp1251},
    {-1},
#line 308 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str168, ei_ces_gbk},
#line 70 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str169, ei_iso8859_2},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 63 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str175, ei_iso8859_2},
#line 182 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str176, ei_cp1255},
#line 84 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str177, ei_iso8859_4},
    {-1},
#line 119 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str179, ei_iso8859_8},
    {-1},
#line 230 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str181, ei_cp1133},
    {-1}, {-1}, {-1},
#line 113 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str185, ei_iso8859_8},
#line 185 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str186, ei_cp1256},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 140 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str192, ei_iso8859_13},
#line 161 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str193, ei_iso8859_16},
    {-1},
#line 201 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str195, ei_cp866},
#line 176 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str196, ei_cp1253},
    {-1}, {-1},
#line 127 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str199, ei_iso8859_9},
    {-1}, {-1},
#line 160 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str202, ei_iso8859_16},
    {-1}, {-1},
#line 120 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str205, ei_iso8859_9},
    {-1}, {-1}, {-1}, {-1},
#line 309 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str210, ei_ces_gbk},
    {-1}, {-1},
#line 51 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str213, ei_c99},
    {-1}, {-1},
#line 173 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str216, ei_cp1252},
    {-1},
#line 237 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str218, ei_tis620},
    {-1},
#line 24 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str220, ei_ucs2},
#line 241 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str221, ei_viscii},
    {-1},
#line 195 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str223, ei_cp850},
    {-1},
#line 197 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str225, ei_cp862},
#line 191 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str226, ei_cp1258},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 13 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str235, ei_ascii},
#line 242 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str236, ei_viscii},
    {-1}, {-1}, {-1},
#line 297 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str240, ei_cp932},
#line 233 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str241, ei_tis620},
#line 222 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str242, ei_hp_roman8},
    {-1}, {-1},
#line 57 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str245, ei_iso8859_1},
    {-1},
#line 232 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str247, ei_tis620},
#line 16 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str248, ei_ascii},
    {-1},
#line 280 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str250, ei_isoir165},
#line 61 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str251, ei_iso8859_1},
#line 171 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str252, ei_cp1251},
#line 235 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str253, ei_tis620},
    {-1}, {-1},
#line 194 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str256, ei_cp850},
#line 183 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str257, ei_cp1255},
    {-1}, {-1},
#line 238 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str260, ei_tis620},
#line 126 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str261, ei_iso8859_9},
#line 186 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str262, ei_cp1256},
    {-1}, {-1},
#line 231 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str265, ei_cp1133},
    {-1},
#line 177 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str267, ei_cp1253},
    {-1}, {-1}, {-1},
#line 134 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str271, ei_iso8859_10},
    {-1}, {-1}, {-1},
#line 106 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str275, ei_iso8859_7},
    {-1},
#line 174 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str277, ei_cp1252},
    {-1}, {-1}, {-1},
#line 77 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str281, ei_iso8859_3},
#line 192 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str282, ei_cp1258},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 207 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str288, ei_mac_roman},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 277 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str294, ei_gb2312},
    {-1},
#line 311 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str296, ei_ces_gbk},
    {-1}, {-1}, {-1},
#line 159 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str300, ei_iso8859_16},
#line 69 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str301, ei_iso8859_2},
    {-1}, {-1}, {-1},
#line 116 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str305, ei_iso8859_8},
    {-1}, {-1}, {-1},
#line 229 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str309, ei_mulelao},
#line 135 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str310, ei_iso8859_10},
    {-1}, {-1},
#line 236 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str313, ei_tis620},
    {-1},
#line 269 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str315, ei_jisx0212},
#line 128 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str316, ei_iso8859_10},
    {-1}, {-1}, {-1},
#line 246 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str320, ei_tcvn},
#line 313 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str321, ei_iso2022_cn},
    {-1}, {-1}, {-1},
#line 66 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str325, ei_iso8859_2},
    {-1}, {-1}, {-1}, {-1},
#line 315 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str330, ei_iso2022_cn_ext},
    {-1}, {-1}, {-1},
#line 225 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str334, ei_armscii_8},
    {-1}, {-1}, {-1}, {-1},
#line 272 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str339, ei_iso646_cn},
    {-1}, {-1},
#line 251 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str342, ei_iso646_jp},
#line 234 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str343, ei_tis620},
#line 245 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str344, ei_tcvn},
#line 111 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str345, ei_iso8859_7},
    {-1},
#line 168 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str347, ei_cp1250},
    {-1}, {-1},
#line 145 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str350, ei_iso8859_14},
    {-1},
#line 312 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str352, ei_gb18030},
#line 243 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str353, ei_viscii},
    {-1}, {-1},
#line 167 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str356, ei_cp1250},
    {-1}, {-1}, {-1},
#line 193 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str360, ei_cp850},
#line 83 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str361, ei_iso8859_4},
    {-1}, {-1}, {-1},
#line 54 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str365, ei_iso8859_1},
    {-1}, {-1},
#line 107 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str368, ei_iso8859_7},
    {-1},
#line 327 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str370, ei_cp950},
    {-1},
#line 22 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str372, ei_ascii},
    {-1},
#line 14 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str374, ei_ascii},
#line 88 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str375, ei_iso8859_5},
#line 151 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str376, ei_iso8859_15},
#line 172 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str377, ei_cp1251},
    {-1}, {-1},
#line 153 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str380, ei_iso8859_15},
#line 158 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str381, ei_iso8859_16},
    {-1}, {-1}, {-1},
#line 95 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str385, ei_iso8859_6},
#line 157 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str386, ei_iso8859_16},
    {-1}, {-1}, {-1}, {-1},
#line 139 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str391, ei_iso8859_13},
#line 219 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str392, ei_mac_thai},
    {-1}, {-1},
#line 72 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str395, ei_iso8859_3},
#line 137 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str396, ei_iso8859_13},
    {-1}, {-1}, {-1}, {-1},
#line 221 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str401, ei_hp_roman8},
    {-1},
#line 12 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str403, ei_ascii},
    {-1},
#line 74 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str405, ei_iso8859_3},
#line 152 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str406, ei_iso8859_15},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 64 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str415, ei_iso8859_2},
#line 227 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str416, ei_georgian_ps},
    {-1},
#line 334 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str418, ei_cp949},
    {-1},
#line 82 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str420, ei_iso8859_4},
    {-1}, {-1}, {-1},
#line 206 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str424, ei_mac_roman},
#line 114 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str425, ei_iso8859_8},
#line 165 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str426, ei_koi8_u},
#line 180 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str427, ei_cp1254},
    {-1}, {-1},
#line 333 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str430, ei_cp949},
    {-1}, {-1}, {-1},
#line 321 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str434, ei_ces_big5},
#line 304 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str435, ei_euc_cn},
#line 20 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str436, ei_ascii},
    {-1}, {-1}, {-1},
#line 322 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str440, ei_ces_big5},
#line 303 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str441, ei_euc_cn},
#line 189 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str442, ei_cp1257},
    {-1}, {-1},
#line 121 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str445, ei_iso8859_9},
#line 314 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str446, ei_iso2022_cn},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 118 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str456, ei_iso8859_8},
    {-1}, {-1}, {-1},
#line 319 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str460, ei_euc_tw},
    {-1}, {-1}, {-1}, {-1},
#line 123 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str465, ei_iso8859_9},
#line 318 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str466, ei_euc_tw},
    {-1}, {-1},
#line 86 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str469, ei_iso8859_4},
#line 149 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str470, ei_iso8859_14},
    {-1}, {-1}, {-1}, {-1},
#line 79 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str475, ei_iso8859_4},
#line 142 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str476, ei_iso8859_14},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 99 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str483, ei_iso8859_6},
    {-1},
#line 285 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str485, ei_ksc5601},
#line 101 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str486, ei_iso8859_6},
    {-1}, {-1}, {-1}, {-1},
#line 130 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str491, ei_iso8859_10},
    {-1}, {-1}, {-1},
#line 25 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str495, ei_ucs2},
#line 109 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str496, ei_iso8859_7},
    {-1},
#line 205 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str498, ei_mac_roman},
#line 112 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str499, ei_iso8859_7},
#line 138 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str500, ei_iso8859_13},
    {-1}, {-1}, {-1}, {-1},
#line 103 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str505, ei_iso8859_7},
    {-1}, {-1}, {-1}, {-1},
#line 110 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str510, ei_iso8859_7},
    {-1}, {-1}, {-1}, {-1},
#line 56 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str515, ei_iso8859_1},
#line 179 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str516, ei_cp1254},
    {-1},
#line 92 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str518, ei_iso8859_5},
    {-1},
#line 33 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str520, ei_ucs4},
    {-1}, {-1}, {-1}, {-1},
#line 268 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str525, ei_jisx0212},
#line 29 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str526, ei_ucs2be},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 300 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str533, ei_iso2022_jp1},
#line 220 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str534, ei_hp_roman8},
    {-1},
#line 163 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str536, ei_koi8_r},
#line 31 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str537, ei_ucs2le},
#line 48 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str538, ei_ucs2swapped},
    {-1},
#line 19 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str540, ei_ascii},
    {-1}, {-1}, {-1}, {-1},
#line 169 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str545, ei_cp1250},
#line 188 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str546, ei_cp1257},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 108 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str553, ei_iso8859_7},
    {-1}, {-1},
#line 129 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str556, ei_iso8859_10},
#line 181 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str557, ei_cp1254},
#line 301 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str558, ei_iso2022_jp2},
    {-1},
#line 276 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str560, ei_gb2312},
#line 287 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str561, ei_ksc5601},
    {-1}, {-1}, {-1}, {-1},
#line 326 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str566, ei_ces_big5},
    {-1},
#line 282 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str568, ei_ksc5601},
    {-1},
#line 255 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str570, ei_jisx0201},
#line 38 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str571, ei_utf16},
#line 325 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str572, ei_ces_big5},
#line 200 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str573, ei_cp862},
#line 52 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str574, ei_java},
    {-1},
#line 144 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str576, ei_iso8859_14},
#line 260 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str577, ei_jisx0208},
    {-1},
#line 250 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str579, ei_iso646_jp},
#line 148 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str580, ei_iso8859_14},
#line 298 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str581, ei_iso2022_jp},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 270 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str590, ei_jisx0212},
#line 226 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str591, ei_georgian_academy},
    {-1}, {-1},
#line 47 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str594, ei_ucs2internal},
#line 271 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str595, ei_iso646_cn},
    {-1}, {-1}, {-1},
#line 249 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str599, ei_iso646_jp},
    {-1},
#line 85 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str601, ei_iso8859_4},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 91 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str608, ei_iso8859_5},
    {-1},
#line 23 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str610, ei_utf8},
    {-1}, {-1}, {-1},
#line 273 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str614, ei_iso646_cn},
#line 131 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str615, ei_iso8859_10},
#line 41 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str616, ei_utf32},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 204 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str628, ei_cp866},
    {-1},
#line 261 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str630, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1},
#line 97 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str635, ei_iso8859_6},
    {-1},
#line 166 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str637, ei_koi8_ru},
    {-1},
#line 262 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str639, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1},
#line 196 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str644, ei_cp850},
#line 34 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str645, ei_ucs4},
#line 30 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str646, ei_ucs2be},
    {-1},
#line 307 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str648, ei_euc_cn},
    {-1}, {-1}, {-1},
#line 279 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str652, ei_gb2312},
    {-1},
#line 339 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str654, ei_local_char},
#line 317 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str655, ei_hz},
#line 208 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str656, ei_mac_roman},
    {-1},
#line 98 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str658, ei_iso8859_6},
    {-1},
#line 247 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str660, ei_tcvn},
    {-1},
#line 164 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str662, ei_koi8_r},
    {-1}, {-1},
#line 210 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str665, ei_mac_iceland},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 302 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str682, ei_iso2022_jp2},
    {-1}, {-1}, {-1},
#line 337 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str686, ei_iso2022_kr},
#line 37 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str687, ei_ucs4le},
#line 50 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str688, ei_ucs4swapped},
    {-1}, {-1}, {-1}, {-1},
#line 286 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str693, ei_ksc5601},
    {-1},
#line 289 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str695, ei_euc_jp},
    {-1}, {-1},
#line 32 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str698, ei_ucs2le},
    {-1},
#line 212 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str700, ei_mac_romania},
#line 288 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str701, ei_euc_jp},
    {-1}, {-1}, {-1},
#line 239 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str705, ei_cp874},
#line 299 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str706, ei_iso2022_jp},
    {-1}, {-1},
#line 293 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str709, ei_sjis},
#line 223 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str710, ei_hp_roman8},
#line 211 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str711, ei_mac_croatian},
    {-1}, {-1}, {-1},
#line 80 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str715, ei_iso8859_4},
#line 143 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str716, ei_iso8859_14},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 45 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str722, ei_utf7},
    {-1}, {-1},
#line 89 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str725, ei_iso8859_5},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 73 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str735, ei_iso8859_3},
    {-1}, {-1},
#line 224 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str738, ei_nextstep},
    {-1},
#line 278 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str740, ei_gb2312},
    {-1}, {-1},
#line 295 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str743, ei_sjis},
#line 49 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str744, ei_ucs4internal},
#line 104 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str745, ei_iso8859_7},
    {-1},
#line 320 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str747, ei_euc_tw},
    {-1}, {-1},
#line 115 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str750, ei_iso8859_8},
#line 15 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str751, ei_ascii},
#line 252 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str752, ei_iso646_jp},
#line 178 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str753, ei_cp1253},
    {-1},
#line 90 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str755, ei_iso8859_5},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1},
#line 213 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str766, ei_mac_cyrillic},
    {-1}, {-1}, {-1},
#line 122 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str770, ei_iso8859_9},
    {-1},
#line 340 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str772, ei_local_wchar_t},
    {-1},
#line 263 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str774, ei_jisx0208},
    {-1},
#line 35 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str776, ei_ucs4},
    {-1}, {-1},
#line 329 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str779, ei_big5hkscs},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 328 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str785, ei_big5hkscs},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 240 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str791, ei_cp874},
    {-1}, {-1},
#line 281 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str794, ei_isoir165},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 331 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str800, ei_euc_kr},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 330 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str806, ei_euc_kr},
    {-1}, {-1}, {-1}, {-1},
#line 338 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str811, ei_iso2022_kr},
    {-1}, {-1},
#line 26 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str814, ei_ucs2},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 275 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str833, ei_iso646_cn},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 46 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str840, ei_utf7},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 248 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str847, ei_iso646_jp},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 190 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str855, ei_cp1257},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 284 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str864, ei_ksc5601},
#line 216 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str865, ei_mac_turkish},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 100 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str871, ei_iso8859_6},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 215 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str878, ei_mac_greek},
    {-1},
#line 55 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str880, ei_iso8859_1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 96 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str890, ei_iso8859_6},
    {-1}, {-1}, {-1}, {-1},
#line 81 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str895, ei_iso8859_4},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 40 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str903, ei_utf16le},
    {-1},
#line 65 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str905, ei_iso8859_2},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 44 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str930, ei_utf7},
    {-1}, {-1},
#line 43 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str933, ei_utf32le},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 27 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str942, ei_ucs2be},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 292 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str949, ei_sjis},
#line 28 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str950, ei_ucs2be},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 296 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str960, ei_sjis},
    {-1}, {-1}, {-1}, {-1},
#line 306 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str965, ei_euc_cn},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1},
#line 283 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1014, ei_ksc5601},
    {-1}, {-1}, {-1}, {-1},
#line 18 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1019, ei_ascii},
    {-1}, {-1}, {-1},
#line 254 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1023, ei_jisx0201},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 17 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1039, ei_ascii},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 214 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1045, ei_mac_ukraine},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 117 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1066, ei_iso8859_8},
    {-1}, {-1}, {-1},
#line 105 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1070, ei_iso8859_7},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 332 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1087, ei_euc_kr},
    {-1}, {-1}, {-1}, {-1},
#line 36 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1092, ei_ucs4be},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1},
#line 256 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1114, ei_jisx0201},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 209 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1126, ei_mac_centraleurope},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1},
#line 264 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1140, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 184 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1157, ei_cp1255},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 265 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1164, ei_jisx0212},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 253 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1209, ei_jisx0201},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 290 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1230, ei_euc_jp},
    {-1},
#line 324 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1232, ei_ces_big5},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 323 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1238, ei_ces_big5},
#line 218 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1239, ei_mac_arabic},
    {-1}, {-1}, {-1}, {-1},
#line 267 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1244, ei_jisx0212},
    {-1},
#line 266 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1246, ei_jisx0212},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1},
#line 187 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1257, ei_cp1256},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 258 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1264, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1},
#line 257 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1269, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 39 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1308, ei_utf16be},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1},
#line 335 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1330, ei_johab},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
#line 42 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1338, ei_utf32be},
    {-1}, {-1}, {-1}, {-1}, {-1},
#line 259 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1344, ei_jisx0208},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1},
#line 291 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1364, ei_euc_jp},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1}, {-1},
    {-1}, {-1},
#line 217 "/home/sergio/src/countries/yapo/platform_v2/lib/libiconv/libiconv-1.9.2/lib/aliases.gperf"
    {(int)(long)&((struct stringpool_t *)0)->stringpool_str1394, ei_mac_hebrew}
  };

#ifdef __GNUC__
__inline
#endif
const struct alias *
aliases_lookup (register const char *str, register unsigned int len)
{
  if (len <= MAX_WORD_LENGTH && len >= MIN_WORD_LENGTH)
    {
      register int key = aliases_hash (str, len);

      if (key <= MAX_HASH_VALUE && key >= 0)
        {
          register int o = aliases[key].name;
          if (o >= 0)
            {
              register const char *s = o + stringpool;

              if (*str == *s && !strcmp (str + 1, s + 1))
                return &aliases[key];
            }
        }
    }
  return 0;
}
