
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <sys/types.h>
#include <sys/time.h>

#include "event.h"
#include "eventextra.h"

/*
 * Reads a line terminated by a given newline.
 * Calls evbuffer_readline if newline is NULL.
 * The returned buffer needs to be freed by the called.
 */

char *
evbuffer_read_newline(struct evbuffer *buffer, const char *newline, size_t nllen)
{
	u_char *data;
	size_t len;
	char *line;
	int i;
	int nli = 0;

	if (!newline || !nllen)
		return evbuffer_readline(buffer);

	data = EVBUFFER_DATA(buffer);
	len = EVBUFFER_LENGTH(buffer);
	
	for (i = 0; i < len; i++) {
		if (data[i] == newline[nli]) {
			if (++nli == nllen)
				break;
		} else if (nli) {
			/* Jump back to recheck characters passed. */
			i -= nli;
			nli = 0;
		}
	}
	
	if (i == len)
		return (NULL);
	i -= nli - 1;

	if ((line = malloc(i + 1)) == NULL) {
		fprintf(stderr, "%s: out of memory\n", __func__);
		evbuffer_drain(buffer, i);
		return (NULL);
	}

	memcpy(line, data, i);
	line[i] = '\0';

	evbuffer_drain(buffer, i + nli);

	return (line);
}
