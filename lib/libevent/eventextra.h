#ifndef _EVENTEXTRA_H_
#define _EVENTEXTRA_H_

#include "event.h"

char *evbuffer_read_newline(struct evbuffer *buffer, const char *newline, size_t nllen);

#endif /*_EVENTEXTRA_H_*/
