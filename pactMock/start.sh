#!/bin/bash
PACT=$1
PORT=$2
if [ ! -f "./pacts/$PACT.json" ]; then
	echo "There are no pacts for $PACT";
	exit 1;
fi
echo 'Remember to "make updatepacts" to get latests pacts'
if [ ! -d "./pactbins" ]; then
	echo 'please run "make pact-download" to get pact binaries';
	exit 1;
fi
if [ ! -d "./pacts" ]; then
	echo 'please run "make updatepacts" to get pacts';
	exit 1;
fi
RUNNING=`ps x -u ${USER} |grep "pactbins/pact" |grep -v grep |grep $PACT |wc -l`
if [ "${RUNNING}" != "1" ]; then
	nohup pactbins/pact/bin/pact-stub-service pacts/$PACT.json -p $PORT -h 0.0.0.0 > pMock.out 2> pMock.err &
else
	echo "Mock for $PACT is running";
fi
