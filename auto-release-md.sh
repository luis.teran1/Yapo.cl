#!/bin/bash
#
# This script is meant to automate the release.md generation
# Just run ./auto-release-md.sh on the release branch and enjoy
#

branch_name() {
	git branch | grep '^\*' | cut -d ' ' -f 2
}

show() {
	echo -e "\n# BEFORE installing Yapo.cl RPMS\n" > pre.banner
	echo -e "\n# AFTER installing Yapo.cl RPMS\n" > post.banner
	cat release.header.md
	for file in pre.banner release.pre.*.md post.banner release.post.*.md
	do
		if [[ $file == release.*.md ]]
		then
			echo -e "## ${file%.md}\n"
			cat $file
			echo
		else
			cat $file
		fi
	done
	cat release.footer.md
	rm pre.banner post.banner
}

generate() {
	force=$1
	branch=$(branch_name)
	if [[ $branch == "release-"* ]] || [[ "$force" == "-f" ]]
	then
		show > release.md
		git add release.md
		git rm release.pre.*
		git rm release.post.*
		git commit -m "${branch} - Generated release.md"

		echo
		echo "release.md has been updated and committed."
		echo "if you are happy with the result, feel free to push."
		echo
		echo "if you are unhappy, you can rollback with:"
		echo "git reset --hard HEAD~1"
	else
		echo "Warning: you are not on a release branch."\
		"Are you sure you want to save the release.md?"\
		"(Use -f to override)"
		return 1
	fi
}

usage() {
	echo "auto-release-md.sh"
	echo
	echo This script is meant to automate the release.md generation
	echo Just run it on the release branch and enjoy
	echo
	echo "usage: ./auto-release-md.sh generate [-f]"
	exit 1
}

case $1 in
	generate) generate $2;;
	*) usage;;
esac
