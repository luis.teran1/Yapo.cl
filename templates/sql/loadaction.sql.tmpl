--  Select ad-data from ads
SELECT
	'ad' as prefix,
	ads.ad_id,
	ads.list_id,
	ads.list_time,
	coalesce(can_edit_ad(<%%ad_id%>, '{<%<%(@@# > 0),%>{$(edit_allowed_days.cat.*), $(edit_allowed_days.cat.@)}%>}'), true) as can_edit_ad,
	ads.status,
	ads.type,
	ads.name,
	ads.phone,
	ads.region,
	ads.city,
	ads.category,
	ads.user_id,
	ads.passwd,
	ads.salted_passwd,
	regexp_replace(ads.salted_passwd, E'^(\\$[0-9]+\\$.{16}).*', E'\\1') AS salt,
	CASE ads.phone_hidden WHEN true THEN 1 ELSE 0 END AS phone_hidden,
	CASE ads.company_ad WHEN true THEN 1 ELSE 0 END AS company_ad,
	ads.subject,
	ads.body,
	price,
	infopage,
	infopage_title,
	COALESCE(orig_list_time, list_time) AS orig_list_time,
	COALESCE(old_price, price) AS orig_price,
	store_id,
	accounts.status AS acc_status,
	CASE WHEN EXISTS (
			SELECT * FROM packs
			WHERE account_id = accounts.account_id AND status = 'active'
		) THEN 1
		ELSE 0
	END AS has_active_pack

FROM 
	<%%schema%>ads LEFT JOIN accounts USING(user_id) 
WHERE 
	ad_id = <%%ad_id%>;

--  Select ad-parameters
SELECT
	'params' as prefix,
	name,
	value
FROM	
	<%%schema%>ad_params
WHERE
	ad_id = <%%ad_id%>
ORDER BY
	name;

--  Select ad-images
SELECT
	'images.' as prefix,
	images.name,
        CASE ad_images_digests.previously_inserted WHEN 't' THEN 1 ELSE 0 END AS digest_present
FROM 
	 (SELECT lpad(ad_media_id::text, 10, '0') || '.jpg' AS name, seq_no FROM ad_media WHERE ad_id = <%%ad_id%> ORDER BY seq_no) AS images 
         LEFT JOIN ad_images_digests ON ad_images_digests.ad_id= <%%ad_id%> AND ad_images_digests.name = images.name
ORDER BY seq_no
;

--  Select state information for the current state
SELECT
	'actions' as prefix,
	ad_actions.action_id,
	action_type,
	current_state,
	ad_actions.state,
	transition,
	timestamp,
	action_states.remote_addr,
	fullname,
	queue,
	CASE WHEN ad_actions.state = 'locked' AND locked_until > CURRENT_TIMESTAMP THEN 1 ELSE 0 END AS locked<%(?action_id),
	EXISTS (SELECT * FROM action_params WHERE ad_id = %ad_id AND name = 'prev_action_id' AND value = '%action_id') AS has_parent%>
FROM	
	<%%schema%>ad_actions
	JOIN <%%schema%>action_states USING (ad_id, action_id)
	LEFT JOIN <%%schema%>tokens ON
		action_states.token_id = tokens.token_id
	LEFT JOIN admins ON 
		admins.admin_id = tokens.admin_id
WHERE	
	ad_actions.ad_id = <%%ad_id%> AND	
	ad_actions.action_id = <%(?action_id && %action_id != "")%action_id::1%> AND
	state_id = current_state;

-- Select upselling info
<%(?action_id)
	SELECT
		'upselling' as prefix,
		a.action_id,
		a.action_type,
		a.current_state,
		p.receipt,
		p.status,
		d.product_id,
		d.payment_group_id
	 FROM
		ad_actions a
		join purchase_detail d using (payment_group_id)
		join purchase p using(purchase_id)
	WHERE
		a.ad_id = %ad_id
		AND a.action_id > '%action_id'
		AND a.state != 'unpaid'
		AND p.status in ('paid', 'sent', 'confirmed', 'failed')
		AND a.action_type in ('upselling_daily_bump','upselling_weekly_bump','upselling_gallery','upselling_label','upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others');

	SELECT
		'inserting_fee' as prefix,
		a.action_id,
		a.action_type,
		a.current_state,
		p.receipt,
		p.status,
		d.product_id,
		d.payment_group_id,
        a.state
	 FROM
		ad_actions a
		join purchase_detail d using (payment_group_id)
		join purchase p using(purchase_id)
	WHERE
		a.ad_id = %ad_id
		AND a.state in ('accepted', 'refused')
		AND p.status in ('paid', 'sent', 'confirmed', 'failed')
		AND a.action_type in ('inserting_fee', 'if_pack_car', 'if_pack_inmo');

	SELECT
		'pack2if' as prefix,
		a.action_id,
		a.action_type,
		a.current_state,
		p.receipt,
		p.status,
		d.product_id,
		d.payment_group_id
	 FROM
		ad_actions a
		join purchase_detail d using (payment_group_id)
		join purchase p using(purchase_id)
	WHERE
		a.ad_id = %ad_id
		AND a.action_id <= '%action_id'
		AND a.state != 'unpaid'
		AND p.status in ('paid', 'sent', 'confirmed', 'failed')
		AND a.action_type in('if_pack_car', 'if_pack_inmo');

	SELECT
		'upselling_in_app' as prefix,
		a.action_id,
		a.action_type,
		a.current_state,
		p.receipt_date as receipt,
		p.status,
		p.product_id,
		p.purchase_in_app_id as payment_group_id
	 FROM
		ad_actions a
		join purchase_in_app p using (ad_id,action_id)
	WHERE
		a.ad_id = %ad_id
		AND a.action_id > '%action_id'
		AND a.state != 'unpaid'
		AND p.status in ('pending', 'confirmed')
		AND a.action_type in ('upselling_bump', 'upselling_daily_bump', 'upselling_weekly_bump', 'upselling_gallery', 'upselling_label');
%>
--  Select state information for the reg state
SELECT
	'reg_action' as prefix,
	timestamp,
	action_states.remote_addr
FROM	
	<%%schema%>ad_actions
	JOIN <%%schema%>action_states USING (ad_id, action_id)
WHERE	
	ad_actions.ad_id = <%%ad_id%> AND	
	ad_actions.action_id = (CASE WHEN NOT EXISTS (SELECT action_id FROM ad_actions WHERE ad_id = <%%ad_id%> AND action_type = 'post_refusal' <%(?action_id && %action_id != "")AND action_id = %action_id %>) THEN <%(?action_id && %action_id != "")%action_id::1%> ELSE 1 END) AND
	action_states.state = 'reg';

<%(?action_times)
SELECT
	'actions' as prefix,
	reg.state_id as reg_state_id,
	reg.timestamp as regtime,
	pay.timestamp as paytime,
	pub.timestamp as publishtime
FROM
	<%%schema%>action_states AS reg
	LEFT JOIN <%%schema%>action_states AS pay ON
		pay.ad_id = reg.ad_id AND
		pay.action_id = reg.action_id AND
		pay.transition IN ('pay', 'verify', 'adminclear', 'import')
	LEFT JOIN <%%schema%>action_states AS pub ON
		pub.ad_id = reg.ad_id AND
		pub.action_id = reg.action_id AND
		pub.transition = 'accept'
WHERE
	reg.state = 'reg' AND
	reg.ad_id = %ad_id
ORDER BY
	pay.timestamp
LIMIT 1;
%>

--  Select action params.
SELECT
	'action_params' as prefix,
	name,
	value
FROM
	action_params
WHERE
	ad_id = <%%ad_id%>
	AND action_id = <%(?action_id && %action_id != "")%action_id::1%>
ORDER BY
	name;

--  Select state information for the previous state
SELECT
	'previous_actions' as prefix,
	ad_actions.action_id,
	action_type,
	current_state,
	action_states.state,
	transition,
	timestamp,
	action_states.remote_addr,
	fullname
FROM	
	<%%schema%>ad_actions
	JOIN <%%schema%>action_states USING (ad_id, action_id)
	LEFT JOIN <%%schema%>tokens ON
		action_states.token_id = tokens.token_id
	LEFT JOIN admins ON 
		admins.admin_id = tokens.admin_id
WHERE	
	ad_actions.ad_id = <%%ad_id%> AND	
	ad_actions.action_id = <%(?action_id && %action_id != "")%action_id::1%> AND
	fullname IS NOT NULL
ORDER BY
	state_id DESC
OFFSET 1
LIMIT 1;


--  Select ad-changes
SELECT
	'ad_change_params' as prefix,
	CASE is_param WHEN true THEN 'param_' ELSE '' END || column_name,
	CASE WHEN column_name = 'company_ad' AND new_value = 'true' THEN '1'
		WHEN column_name = 'company_ad' AND new_value = 'false' THEN '0'
		ELSE new_value END as new_value
FROM
	<%%schema%>ad_changes
WHERE
	ad_id = <%%ad_id%> AND
	action_id = <%(?action_id && %action_id != "")%action_id::1%> AND
	(state_id, column_name) IN (
		SELECT
			MAX(state_id),
			column_name
		FROM
			<%%schema%>ad_changes
		WHERE
			ad_id = <%%ad_id%> AND
			action_id = <%(?action_id && %action_id != "")%action_id::1%>
		GROUP BY
			column_name)
ORDER BY
	2;

--  Select image-changes
SELECT
	'ad_image_changes' || '.image' || seq_no::text as prefix,
	name,
	is_new
FROM
	<%%schema%>ad_image_changes
WHERE
	ad_id = <%%ad_id%> AND
	action_id = <%(?action_id && %action_id != "")%action_id::0%> AND
	state_id = (
		SELECT
			state_id
		FROM
			<%%schema%>action_states
		WHERE
			ad_id = <%%ad_id%> AND
			action_id = <%(?action_id && %action_id != "")%action_id::0%> AND
			state = 'reg');

--  Select user-information

SELECT
	'users' as prefix,
	ads.name,
	uid,
	users.email,
	account,
	uuid
FROM
	users
	JOIN <%%schema%>ads ON(users.user_id = ads.user_id)
	LEFT JOIN email_uuid ON(users.email = email_uuid.email)
WHERE
	ad_id = <%%ad_id%>;
	
--  Select user-parameters
SELECT
	'user_params' as prefix,
	user_params.name,
	user_params.value
FROM
	user_params,
	<%%schema%>ads
WHERE
	ad_id = <%%ad_id%> AND
	ads.user_id = user_params.user_id;

--  Select additional payment information (total price)
SELECT
	'payment' as prefix,
	sum(pay_amount - discount) as total_price,
	sum(CASE payment_type WHEN 'ad_action' THEN pay_amount - discount ELSE 0 END) as cat_price,
	sum(CASE payment_type WHEN 'extra_images' THEN pay_amount - discount ELSE 0 END) as extra_images_price,
	sum(CASE payment_type WHEN 'video' THEN pay_amount - discount ELSE 0 END) as video_price,
	sum(CASE payment_type WHEN 'gallery' THEN pay_amount - discount ELSE 0 END) as gallery_price,
	round(sum(pay_amount - discount) * 0.8, 2) as vat_price,
	round(sum(pay_amount - discount) * 0.2, 2) as vat,
	payment_groups.code,
	pay_log.paid_at,
	pay_log.pay_type,
	pay_log.code AS pay_log_code,
	pay_log.token_id
FROM
	<%%schema%>ad_actions
	JOIN <%%schema%>payment_groups USING (payment_group_id)
	JOIN <%%schema%>payments USING (payment_group_id)
	LEFT JOIN <%%schema%>pay_log ON
		pay_log.payment_group_id = payment_groups.payment_group_id AND
		pay_log.status IN ('OK', 'MORE')
WHERE
	ad_id = <%%ad_id%> AND
	action_id = <%(?action_id && %action_id != "")%action_id::1%>
	AND action_type not in ('upselling_weekly_bump', 'upselling_daily_bump', 'upselling_gallery', 'upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others','inserting_fee')
GROUP BY
	payment_groups.code,
	pay_log.paid_at,
	pay_log.pay_type,
	pay_log.code,
	pay_log.token_id;

SELECT
	'payment.reference' AS prefix,
	ref_type,
	<%%schema%>pay_log_references.reference,
	'param' AS "type"
FROM
	<%%schema%>ad_actions
	JOIN <%%schema%>payment_groups USING (payment_group_id)
	JOIN <%%schema%>pay_log ON
		pay_log.payment_group_id = ad_actions.payment_group_id AND
		pay_log.status IN ('OK', 'MORE')
	JOIN <%%schema%>pay_log_references USING (pay_log_id)
WHERE
	ad_id = <%%ad_id%> AND
	action_id = <%(?action_id && %action_id != "")%action_id::1%>
	AND action_type not in ('upselling_weekly_bump', 'upselling_daily_bump', 'upselling_gallery', 'upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others')
ORDER BY
	ref_type,
	ref_num;

--  Select detailed payments
SELECT
	'payment.' as prefix,
        payment_type,
        pay_amount AS price,
	discount
FROM	
	<%%schema%>ad_actions
	JOIN <%%schema%>payments USING (payment_group_id)
WHERE	
	ad_id = <%%ad_id%> AND
	action_id = <%(?action_id && %action_id != "")%action_id::1%>
	AND action_type not in ('upselling_weekly_bump', 'upselling_daily_bump', 'upselling_gallery', 'upselling_standard_cars','upselling_advanced_cars','upselling_premium_cars','upselling_standard_inmo','upselling_advanced_inmo','upselling_premium_inmo','upselling_standard_others','upselling_advanced_others','upselling_premium_others');

<%(?get_associated_ads)
--  Find associated ads
SELECT 
		'associated.' as prefix,
		o_ad_id as ad_id,
		o_action_id as action_id,
		o_subject as subject,
		o_category as category,
		o_body as body,
		o_type as type,
		o_list_id as list_id,
		o_list_time as list_time,
		o_status as status,
		o_region as region,
		o_city as city,
		o_current_state as current_state,
		o_price as price,
		o_image as image,
		o_score as score,
		o_ad_params as params
FROM
		find_associated_ads(%ad_id,<%$(controlpanel.common.max_duplicated_ads)%>);

SELECT
	'deactivated.' as prefix,
	ads.ad_id,
	MAX(ad_actions.action_id) as action_id,
	ads.subject,
	ads.category,
	ads.body,
	ads.type,
	ads.list_id,
	ads.list_time,
	ads.status,
	ads.region,
	ads.city,
	ads.price,
	ads.image,
	(
		SELECT value
		FROM ad_params
		WHERE name = 'currency'
		AND ad_id = ads.ad_id
	) AS currency
FROM
	ads 
JOIN 
	ad_actions USING (ad_id) 
where ads.user_id = (select user_id from ads where ad_id = <%%ad_id%>)
and ad_id != <%%ad_id%> and ads.status = 'deactivated'
GROUP BY ads.ad_id
order by ads.list_time DESC
limit <%$(controlpanel.common.deactivated_ads_limit)%>;
%>

--  Ad history - 
<%(?get_ad_history)
SELECT
	'action_list.' || ad_actions.action_id AS prefix,
	action_type,
	reg.timestamp AS regtime,
	regadm.fullname AS regadmin,
	curr.state AS currstate,
	curr.timestamp AS currtime,
	curradm.fullname AS curradmin
FROM
	<%%schema%>ad_actions
	JOIN <%%schema%>action_states AS reg ON
		ad_actions.ad_id = reg.ad_id AND
		ad_actions.action_id = reg.action_id AND
		reg.state = 'reg'
	LEFT JOIN <%%schema%>tokens AS regtok ON
		reg.token_id = regtok.token_id
	LEFT JOIN admins AS regadm ON
		regtok.admin_id = regadm.admin_id
	JOIN <%%schema%>action_states AS curr ON
		ad_actions.current_state = curr.state_id
	LEFT JOIN <%%schema%>tokens as currtok ON
		curr.token_id = currtok.token_id
	LEFT JOIN admins AS curradm ON
		currtok.admin_id = curradm.admin_id
WHERE
	ad_actions.ad_id = '%ad_id'
ORDER BY
	ad_actions.action_id;
%>

--  Notices

<%(?schema)
SELECT 
	'notices.' AS prefix,
	notice_id, 
	CASE notices.abuse WHEN 't' THEN 1 ELSE 0 END AS abuse,
	notices.body,
	notices.uid,
	notices.user_id,
	notices.ad_id,
	notices.created_at,
	username
FROM
	<%%schema%>notices
	JOIN <%%schema%>tokens USING (token_id)
	JOIN admins USING (admin_id)
WHERE
	notices.ad_id = '<%%ad_id%>'
UNION ALL
%>
SELECT 
	'notices.' AS prefix,
	notice_id, 
	CASE notices.abuse WHEN 't' THEN 1 ELSE 0 END AS abuse,
	notices.body,
	notices.uid,
	notices.user_id,
	notices.ad_id,
	notices.created_at,
	username
FROM
	<%%schema%>ads
	JOIN users USING (user_id)
	JOIN notices ON
		<%(!?schema)notices.ad_id = ads.ad_id OR%>
		notices.user_id = users.user_id OR
		notices.uid = users.uid
	JOIN tokens USING (token_id)
	JOIN admins USING (admin_id)
WHERE
	ads.ad_id = '<%%ad_id%>'
ORDER BY
	notice_id;

--  Select number of emails connected to same uid
SELECT
        'uid' as prefix,
        COUNT(*) as count
FROM
        users
WHERE
        uid = (
        SELECT
                users.uid
        FROM
                <%%schema%>ads
                NATURAL JOIN users
        WHERE
                ads.ad_id = <%%ad_id%>
        );


-- Refuse reason for this action
<%(?action_id && %action_id != "")
SELECT
	'refused' as prefix,
	reason.value as reason,
	refusal_text.value as refusal_text
FROM
	<%%schema%>state_params AS reason LEFT JOIN
	<%%schema%>state_params AS refusal_text ON 
	reason.ad_id = refusal_text.ad_id AND 
	reason.action_id = refusal_text.action_id AND 
	refusal_text.name = 'refusal_text'
WHERE
	reason.ad_id = <%%ad_id%> AND
	reason.action_id = <%%action_id%> AND
	reason.name = 'reason';

-- Previous refuse reason
SELECT 
	'prev_refused' as prefix,
	state_params.value as reason
FROM
	<%%schema%>state_params
WHERE
	ad_id = <%%ad_id%> AND
	state_params.name = 'reason' AND
	action_id = (
		SELECT 
			value\:\:integer
		FROM 
			<%%schema%>action_params 
		WHERE 
			ad_id = <%%ad_id%> AND 
			action_id = <%%action_id%> AND 
			name = 'prev_action_id'
	);

-- Action type that was refused
SELECT
	'pre_refuse_action_type' as prefix,
	at.o_action_type as action_type
FROM
	action_type_before_refuse(%ad_id, %action_id, <%(?schema)'&replace(".", "", %schema)'::NULL%>) AS at;
%>

-- Reason text
<%((?reason) && %reason != "")
SELECT
	'reason_text' as prefix,
	value
FROM
	conf
WHERE
	key = '*.*.common.refusal.' || '<%%reason%>' || '.text';

SELECT
	'accepted_reason_text' as prefix,
	value
FROM
	conf
WHERE
	key = '*.*.common.accepted.' || '<%%reason%>' || '.text';
%>
--  Select store-information
SELECT
	'store' as prefix,
	now() between date_start and date_end as active
FROM	
	ads JOIN
	stores USING (store_id)
WHERE
	ad_id = <%%ad_id%> AND
	stores.status = 'active';

-- Select extra thumbnail digests
<%(?action_id && (%action_id == "" || %action_id == 1) )
SELECT
	'digest.' AS prefix,
	name AS db_name,
	digest,
        CASE previously_inserted WHEN 't' THEN 1 ELSE 0 END AS digest_present
FROM
	ad_images_digests
WHERE
	ad_id = <%%ad_id%> AND
	name IN (
		SELECT
			lpad(ad_media_id\:\:text, 10, '0') || '.jpg' AS name
		FROM
			<%%schema%>ad_media	
		WHERE
			ad_id = <%%ad_id%>
	);
::
SELECT
        'digest.' AS prefix,
        ad_images_digests.name AS db_name,
        ad_images_digests.digest,
        CASE ad_images_digests.previously_inserted WHEN 't' THEN 1 ELSE 0 END AS digest_present
FROM
        ad_images_digests 
-- JOIN ad_image_changes USING (ad_id, name)  
WHERE
        ad_images_digests.ad_id = <%%ad_id%> AND
        ad_images_digests.name IN (
                SELECT
                        name
                FROM
                        <%%schema%>ad_image_changes     
                WHERE
                        ad_id = <%%ad_id%> AND  
                        -- seq_no != 0 AND 
                        action_id = <%(?action_id && %action_id != "")%action_id::0%> AND
                        state_id = (
                                SELECT
                                        state_id
                                FROM
                                        <%%schema%>action_states
                                WHERE
                                        ad_id = <%%ad_id%> AND
                                        action_id = <%(?action_id && %action_id != "")%action_id::0%> AND
                                        state = 'reg') 
	)
-- ORDER BY ad_image_changes.seq_no
;
%>

--------Used for spidered - We have the same query in fetch_extra_check.sql.tmpl. Be carefull when changing it.
<%(?spidered && %spidered == "accept_spidered")
SELECT 
	'fetch_extra_check' as prefix,
        to_char(reg.timestamp, 'HH24MISS') || '.' || reg.state_id as extra_check, 
	list_id as list_id
FROM 
        ads
        join ad_actions aa using (ad_id)
        join action_states reg on (aa.ad_id = reg.ad_id and aa.action_id = reg.action_id and reg.state = 'reg')
        left join action_states pay on (reg.ad_id = pay.ad_id and reg.action_id = pay.action_id and pay.transition in ('pay', 'verify', 'adminclear', 'import'))
        
WHERE 
        aa.state = 'accepted' AND
   <%(?ad_id) ads.ad_id = %ad_id :: list_id = %list_id%>

ORDER BY
        aa.action_id desc,
        reg.state_id desc
LIMIT 1;%>

-- get the uf for mail templates
SELECT
	'uf' AS prefix,
	value
FROM
	conf
WHERE
	key = '*.*.common.uf_conversion_factor';

-- see if the user e-mail is in whitelist.
SELECT
	'is_in_list' AS prefix,
	is_in_list(users.email, 'whitelist') as whitelist
FROM
	<%%schema%>ads
	JOIN users USING (user_id)
WHERE
	ad_id = <%%ad_id%>
;
