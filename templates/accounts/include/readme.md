*Si entras a modificar un archivo, docum�ntalo. Basta con escribir como se llama y que hace/tiene.*

# Includes
Esta carpeta contiene templates parciales que son importados por los archivos del directorio padre.

## Contenido
- **main_form.html.tmpl**: Es un wrapper del formulario de creaci�n de usuario, dependiendo de la opcion escogida es si 
abrir� la version con redes sociales o la cl�sica de usuario y contrase�a.
- **main_form_inner.html.tmpl**: Contiene el formulario cl�sico de creaci�n de usuario.
- **main_form_fb_inner.html.tmpl**: Contiene el formulario de creaci�n de usuario con *Facebook*.
