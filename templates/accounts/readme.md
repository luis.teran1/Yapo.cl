Si entras a modificar un archivo, docum�ntalo. Basta con escribir como se llama y que hace/tiene.
# Accounts
Esta carpeta contiene los templates relacionados con la cuenta de usuario.


## Contenido
- **login.html.tmpl**: Este archivo es utilizado para las funciones de autenticaci�n en el sitio. OJO, es solo un 
wrapper. El contenido real del mismo se encuentra en `../common/login.html.tmpl`
- **create_account.html.tmpl**: Wrapper para la creaci�n de una cuenta. V�ase: `main_form.html.tmpl`, `main_form_inner.html.tmpl` y `main_form_inner.html.tmpl`
- **reset_password.html.tmpl**: Template con el formulario para recuperar contrase�a.
- **change_password.html.tmpl**: Formulario para cambiar la contrase�a.