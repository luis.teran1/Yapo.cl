*Si entras a modificar un archivo, docum�ntalo. Basta con escribir como se llama y que hace/tiene.*

# Template Common
Templates de uso com�n en la plataforma.

## Contenidos

- **login.html.tmpl**: Contiene el formulario de login de la plataforma. Este es el que se abre en el modal.
- **logo_bars.html.tmpl**: Wrapper del header, logo y barra de b�squeda.
- **banners_listing_top.html.tmpl**: Contiene los banners que se mostraran en la parte superior.

## Modificaciones importantes
