#!/bin/bash
#
# frontend.sh: Compiles the frontend stuff. Fast.
#
# This effort is a refactor to the gulp process used to generate css and js
# files for dev and prod environments. The goal was to reduce the overall
# compilation time by going back to the roots and cutting off most of the fancy
# stuff.
#
# The code is far from perfect, but times went down from 20m a deploy to
# 10m a deploy.
#
# If you think the code is ugly, feel free to help us improve it.
#
# Author: marcelo@schibsted.cl
#

REGRESS_PATH="regress_final"
DEPLOY_PATH="dest/blocket-root/opt/blocket"
SCRIPT_NAME=$(basename $0)
SUBCOMMAND=$1

# frontend.sh#help
# Shows the available options for the script

command_help() {
    echo "Usage: $SCRIPT_NAME <subcommand> [--help]"
    echo "Subcommands:"
    echo "  js - Compiles JS files"
    echo "  sass - Compiles Sass files"
    echo "  webpack - Compiles React components"
    echo "  simulate - Compiles js, sass and react as if it were production"
    echo "For help with each subcommand run:"
    echo "$SCRIPT_NAME <subcommand> -h"
    echo ""
}

# frontend.sh#sass_usage
# Shows the available options for the sass command

command_sass_usage() {
    echo "NAME: $SCRIPT_NAME#sass"
    echo "DESCRIPTION: Compiles Sass files"

    echo "OPTIONS:"
    echo "  -p : Compiles sass files for an specific platform."
    echo "       Available values 'desktop' or 'mobile'"

    echo "  -e : Indicates the environment to compiles."
    echo "       Available values 'production' or 'regress'"

    echo "  -s : Compiles react components as if it were production"

    echo "  -h : Show this list"
    echo ""
    exit 1
}

# frontend.sh#js_usage
# Shows the available options for the js command

command_js_usage() {
    echo "NAME: $SCRIPT_NAME#js"
    echo "DESCRIPTION: Compiles JS files"

    echo "OPTIONS:"
    echo "  -p : Compiles JS files for an specific platform."
    echo "       Available values 'desktop' or 'mobile'"

    echo "  -e : Indicates the environment to compiles."
    echo "       Available values 'production' or 'regress'"

    echo "  -s : Compiles react components as if it were production"

    echo "  -h : Show this list"
    echo ""
    exit 1
}

# frontend.sh#webpack_usage
# Shows the available options for the webpack command

command_webpack_usage() {
    echo "NAME: $SCRIPT_NAME#webpack"
    echo "DESCRIPTION: Compiles react components"

    echo "OPTIONS:"
    echo "  -e : Indicates the environment to compiles."
    echo "       Available values 'production' or 'regress'"

    echo "  -s : Compiles react components as if it were production"

    echo "  -h : Show this list"
    echo ""
    exit 1
}

# frontend.sh#simulate_usage
# Shows the available options for the simulate command

command_simulate_usage() {
    echo "NAME: $SCRIPT_NAME#simulate"
    echo "DESCRIPTION: Simulates the production environment on regress"

    echo "OPTIONS:"
    echo "  -h : Show this list"
    echo ""
    exit 1
}


# frontend.sh#sass
# Compiles Sass files to each platform and environment.
# Usage: bash frontend.sh sass
# For more information run `bash frontend.sh sass -h`

command_sass() {
    local environment="regress"
    local platform=""
    local simulate=false
    local base_path=$REGRESS_PATH
    local option OPTIND

    while getopts "e:p:hs" option
    do
        case $option in
            p) platform=$OPTARG;;
            e) environment=$OPTARG;;
            s) simulate=true;;
            h) command_sass_usage;;
            *) echo "Invalid option"; command_sass_usage;;
        esac
    done

    if ! [[ $platform =~ ^(mobile|desktop)$ || $environment =~ ^(production|regress)$ ]]
    then
        echo "Invalid option."; command_sass_usage
    fi

    if [ "$environment" != "regress" ]
    then
       base_path=$DEPLOY_PATH
    fi

    local css_secure_path="$base_path/www-ssl/css"
    local frontend_legacy_path="node_modules/@yapo-legacy-fe/sass/dist/www-ssl/css"
    local frontend_pp_path="node_modules/@yapo-legacy-fe/pp/dist/www-ssl/css"

    time {
        echo "Starting: Sass ..."
        # Creating output directories
        mkdir -p $css_secure_path

        for css_file in $(find $frontend_legacy_path -name "*.css")
        do
            filename=$(basename $css_file)
            rm -f $css_secure_path/$filename
            cp $css_file $css_secure_path/$filename
        done

        for css_file in $(find $frontend_pp_path -name "*.css")
        do
            filename=$(basename $css_file)
            rm -f $css_secure_path/$filename
            cp $css_file $css_secure_path/$filename
        done

        # cp $frontend_legacy_path/Builddesc $css_secure_path
        # cp $frontend_legacy_path/sassvars.ninja $css_secure_path

        wait $(jobs -p)
        TIMEFORMAT='Finished: Sass after %Rs'
    }
}

# frontend.sh#js_paths
# `Returns` the `gulp-js-dependencies.js` file in accordance with the specified platform.

get_js_paths() {
    local desktop='www/gulp-js-dependencies.js'
    local mobile='msite/www/gulp-js-dependencies.js'

    if [ "$1" = "mobile" ]
    then
        echo $mobile
    elif [ "$1" = "desktop" ]
    then
        echo ${desktop}
    elif [ -z $1 ]
    then
        echo "${desktop} ${mobile}"
    fi
}


# frontend.sh#js
# Compiles JS and React files.
# Usage: bash frontend.sh dev
# For more information run `bash frontend.sh js -h`

command_js() {
    local environment="regress"
    local platform=""
    local simulate=false
    local base_path=$REGRESS_PATH
    local option OPTIND

    while getopts "e:p:hs" option
    do
        case $option in
            p) platform=$OPTARG;;
            e) environment=$OPTARG;;
            s) simulate=true;;
            h) command_js_usage;;
            *) echo "Invalid option."; command_js_usage;;
        esac
    done

    if ! [[ $platform =~ ^(mobile|desktop)$ || $environment =~ ^(production|regress)$ ]]
    then
        echo "Invalid option."; command_js_usage
    fi

    if [ "$environment" != "regress" ]
    then
       base_path=$DEPLOY_PATH
    fi

    local js_secure_path="$base_path/www-ssl/js"
    local paths=$(get_js_paths $platform)
    local file_prefix='ugli'
    local final_file='uglify_files'

    if [ "$environment" = "regress" ]
    then
        paths+=" www/gulp-js-dependencies-regress.js"
    fi

    time {
        echo "Starting: JS ..."
        mkdir -p $js_secure_path

        for path in $paths
        do
            # Converts `gulp-js-dependencies.js` file in a valid JSON file.
            echo "[" > uglify_files
            local remove_lines=$(($(sed '/^var/ q' $path | wc -l) - 1))
            sed -e "1,${remove_lines}d;" -e '/^\w/d; /\s*\/\//d; s/;//; s/^\(\s\+\)\(\w\+\):/\1"\2":/; s/'\''/"/g;' $path >> $final_file

            python3 ugliformer.py

            for js_file in $(find . -name "${file_prefix}_*")
            do
                filename=$(basename $js_file | cut -c6-)
                rm -f $js_secure_path/$filename

                for c in $(cat $js_file)
                do
                    if [ -f $c ]
                    then
                        cat $c >> $js_secure_path/$filename
                    else
                        >&2 echo "File not found: $c for $js_secure_path/$filename"
                    fi
                done

                if [[ "$environment" = "regress" && "$simulate" = false ]]
                then
                    continue
                else
                    # Only for Production environment
                    uglifyjs $js_secure_path/$filename -c drop_console=true -o $js_secure_path/${filename} --comments &
                fi
            done

            rm ${file_prefix}_* $final_file
        done

        wait $(jobs -p)
        TIMEFORMAT="Finished: JS after %Rs"
    }
}

# frontend.sh#dev
# Compiles JS, Sass, and React files for the development environment. All files aren't minified.
# Usage: bash frontend.sh dev

command_dev() {
    time {
        echo "Starting: Dev ..."
        command_sass -e regress
        command_js -e regress
        command_webpack -e regress
        wait $(jobs -p)
        TIMEFORMAT="Finished: Dev after %Rs"
    }
}


# frontend.sh#deploy
# Compiles JS, Sass, and React files for the production environment. All files are minified.
# Usage: bash frontend.sh deploy

command_deploy() {
    time {
        echo "Starting: Deploy ..."
        command_sass -e production
        command_js -e production
        command_webpack -e production
        wait $(jobs -p)
        TIMEFORMAT="Finished: Deploy after %Rs"
    }
}

# frontend.sh#webpack
# Compiles React files using webpack as transpiler. The main settings are setted in
# `webpack.config.js`.
# Usage: bash frontend.sh webpack
# For more information run `bash frontend.sh webpack -h`

command_webpack() {
    local environment="regress"
    local base_path=$REGRESS_PATH
    local simulate=false
    local option OPTIND

    while getopts "e:hs" option
    do
        case $option in
            e) environment=$OPTARG;;
            h) command_webpack_usage;;
            s) simulate=true;;
            *) echo "Invalid option."; command_webpack_usage;;
        esac
    done

    if ! [[ $environment =~ ^(production|regress)$ ]]
    then
        echo "Invalid option."; command_js_usage
    fi

    if [ "$environment" != "regress" ]
    then
       base_path=$DEPLOY_PATH
    fi

    local webpack_secure_path="./$base_path/www-ssl/js"
    local frontend_pp_path="node_modules/@yapo-legacy-fe/pp/dist/www-ssl/js"

    time {
        echo "Starting: WEBPACK ..."
        mkdir -p $webpack_secure_path

        for path in $(find $frontend_pp_path -name "*.js")
        do
            filename=$(basename $path)
            rm -f $webpack_secure_path/$filename

            cp $path $webpack_secure_path/$filename
        done
        wait $(jobs -p)
        TIMEFORMAT="Finished: WEBPACK after %Rs"
    }
}

# frontend.sh#simulate
# Compiles JS, Sass, and React files to simulate the production compilation on regress environment.
# Usage: bash frontend.sh simulate

command_simulate() {
    local option OPTIND

    while getopts "h" option
    do
        case $option in
            h) command_simulate_usage;;
            *) echo "Invalid option."; command_webpack_usage;;
        esac
    done

    time {
        echo "Starting: Simulate ..."
        command_sass -s
        command_js -s
        command_webpack -s
        wait $(jobs -p)
        TIMEFORMAT="Finished: Simulate after %Rs"
    }
}

# The following lines are the entry point for the script. Each `subcommand` has their own command
# prefixed by `command_<subcommand>`.
# If the subcommand is empty, `-h` or `help` the script will show the help menu.

case $SUBCOMMAND in
    "" | "help" | "-h") command_help;;
    *)
        shift
        command_${SUBCOMMAND} $@
        if [ $? = 127 ]; then
            echo "Error: '$SUBCOMMAND' is not a known subcommand." >&2
            echo ""
            command_help
            exit 1
        fi
        ;;
esac

