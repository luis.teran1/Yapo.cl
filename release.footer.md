
# HOTFIX

_This section should be used only to fix bugs and if the fix is easy to apply, here you can add extra instructions needed only for **PRODUCTION**_

Example: 
* We need to turnOff servipag for desktop version

  In bconf machine, edit the file: /opt/blocket/conf/bconf.txt.payment_common
  find and change the line: 
  `*.*.payment.desktop.servipag.enabled=1` 

  and make it look like:    
  `*.*.payment.desktop.servipag.enabled=0`

  Restart bconfd and www2/bapache services   
  CH40: `/etc/init.d/bconfd restart`       
  www2: `/etc/init.d/bapache restart`

**This changes should be validated with system team**

## Hotfix title

* Hotfix detail
