UPDATE admin_privs SET priv_name = 'Stores' WHERE priv_name like 'stores'; 
UPDATE admin_privs SET priv_name = 'Stores.createStore' WHERE priv_name like 'stores.create_store';
UPDATE admin_privs SET priv_name = 'Stores.editStore' WHERE priv_name like 'stores.edit_store';
UPDATE admin_privs SET priv_name = 'Stores.extendStore' WHERE priv_name like 'stores.extend_store';
