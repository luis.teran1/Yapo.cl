UPDATE admin_privs SET priv_name = 'packs'                      WHERE priv_name like 'Packs';
UPDATE admin_privs SET priv_name = 'packs.searchandassign_inmo' WHERE priv_name like 'Packs.searchAndAssignInmo';
UPDATE admin_privs SET priv_name = 'packs.searchandassign_auto' WHERE priv_name like 'Packs.searchAndAssignAuto';
UPDATE admin_privs SET priv_name = 'packs.update_pro_param'     WHERE priv_name like 'Packs.updateProParam';
UPDATE admin_privs SET priv_name = 'packs.partner_ad_report'    WHERE priv_name like 'Packs.partnerAdReport';
