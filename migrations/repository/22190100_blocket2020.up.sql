--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

--
-- Name: blocket_2020; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA blocket_2020;


ALTER SCHEMA blocket_2020 OWNER TO postgres;

SET search_path = blocket_2020, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: accounts; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE accounts (
    account_id integer NOT NULL,
    name character varying(200) NOT NULL,
    rut character varying(12),
    email character varying(80) NOT NULL,
    phone character varying(100) NOT NULL,
    is_company boolean,
    region smallint NOT NULL,
    salted_passwd character varying(100) NOT NULL,
    creation_date timestamp without time zone DEFAULT now() NOT NULL,
    status public.enum_account_status NOT NULL,
    address character varying(500),
    contact character varying(200),
    lob character varying(200),
    commune smallint,
    user_id integer NOT NULL
);


ALTER TABLE blocket_2020.accounts OWNER TO postgres;

--
-- Name: accounts_account_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE accounts_account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.accounts_account_id_seq OWNER TO postgres;

--
-- Name: accounts_account_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE accounts_account_id_seq OWNED BY accounts.account_id;


--
-- Name: action_params; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE action_params (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    name public.enum_action_params_name NOT NULL,
    value text NOT NULL
);


ALTER TABLE blocket_2020.action_params OWNER TO postgres;

--
-- Name: action_states; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE action_states (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    state_id integer NOT NULL,
    state public.enum_action_states_state DEFAULT 'reg'::public.enum_action_states_state NOT NULL,
    transition public.enum_action_states_transition DEFAULT 'initial'::public.enum_action_states_transition NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    remote_addr character varying(20) DEFAULT ''::character varying,
    token_id integer
);


ALTER TABLE blocket_2020.action_states OWNER TO postgres;

--
-- Name: ad_actions; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_actions (
    ad_id integer NOT NULL,
    action_id integer DEFAULT 0 NOT NULL,
    action_type public.enum_ad_actions_action_type NOT NULL,
    current_state integer,
    state character varying(16) DEFAULT 'reg'::character varying NOT NULL,
    queue public.enum_ad_actions_queue DEFAULT 'normal'::public.enum_ad_actions_queue NOT NULL,
    locked_by integer,
    locked_until timestamp without time zone,
    payment_group_id integer
);


ALTER TABLE blocket_2020.ad_actions OWNER TO postgres;

--
-- Name: ad_changes; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_changes (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    state_id integer NOT NULL,
    is_param boolean NOT NULL,
    column_name character varying(50) NOT NULL,
    old_value text,
    new_value text
);


ALTER TABLE blocket_2020.ad_changes OWNER TO postgres;

--
-- Name: ad_image_changes; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_image_changes (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    state_id integer NOT NULL,
    seq_no integer NOT NULL,
    name character varying(20),
    is_new boolean DEFAULT false NOT NULL
);


ALTER TABLE blocket_2020.ad_image_changes OWNER TO postgres;

--
-- Name: ad_images; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_images (
    ad_id integer NOT NULL,
    seq_no smallint DEFAULT 0 NOT NULL,
    name character varying(20) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE blocket_2020.ad_images OWNER TO postgres;

--
-- Name: ad_media; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_media (
    ad_media_id bigint NOT NULL,
    ad_id integer,
    seq_no smallint DEFAULT 0 NOT NULL,
    upload_time timestamp without time zone NOT NULL,
    media_type public.enum_ad_media_media_type DEFAULT 'image'::public.enum_ad_media_media_type NOT NULL,
    width integer,
    height integer,
    length integer
);


ALTER TABLE blocket_2020.ad_media OWNER TO postgres;

--
-- Name: ad_media_changes; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_media_changes (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    state_id integer NOT NULL,
    seq_no integer NOT NULL,
    ad_media_id bigint,
    is_new boolean DEFAULT false NOT NULL,
    media_type public.enum_ad_media_media_type DEFAULT 'image'::public.enum_ad_media_media_type NOT NULL,
    CONSTRAINT ad_media_changes_check CHECK (((seq_no = 0) OR (ad_media_id IS NOT NULL)))
);


ALTER TABLE blocket_2020.ad_media_changes OWNER TO postgres;

--
-- Name: ad_params; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ad_params (
    ad_id integer NOT NULL,
    name public.enum_ad_params_name NOT NULL,
    value text NOT NULL
);


ALTER TABLE blocket_2020.ad_params OWNER TO postgres;

--
-- Name: ads; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE ads (
    ad_id integer NOT NULL,
    list_id integer,
    list_time timestamp without time zone,
    status public.enum_ads_status NOT NULL,
    type public.enum_ads_type NOT NULL,
    name character varying(100) DEFAULT ''::character varying NOT NULL,
    phone character varying(100) DEFAULT ''::character varying NOT NULL,
    region smallint NOT NULL,
    city smallint DEFAULT 0 NOT NULL,
    category integer DEFAULT 0 NOT NULL,
    user_id integer DEFAULT 0 NOT NULL,
    passwd character varying(30) DEFAULT NULL::character varying,
    phone_hidden boolean DEFAULT false NOT NULL,
    no_salesmen boolean DEFAULT false NOT NULL,
    company_ad boolean DEFAULT false NOT NULL,
    subject character varying(50) DEFAULT ''::character varying NOT NULL,
    body text,
    price bigint,
    image character varying(20) DEFAULT NULL::character varying,
    infopage text,
    infopage_title character varying(50) DEFAULT NULL::character varying,
    orig_list_time timestamp without time zone,
    old_price bigint,
    store_id integer,
    salted_passwd character varying(100) DEFAULT NULL::character varying,
    modified_at timestamp without time zone,
    lang character varying DEFAULT 'es'::character varying NOT NULL
);


ALTER TABLE blocket_2020.ads OWNER TO postgres;

--
-- Name: mail_202001; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202001 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202001 OWNER TO postgres;

--
-- Name: mail_202001_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202001_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202001_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202001_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202001_mail_id_seq OWNED BY mail_202001.mail_id;


--
-- Name: mail_202002; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202002 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202002 OWNER TO postgres;

--
-- Name: mail_202002_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202002_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202002_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202002_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202002_mail_id_seq OWNED BY mail_202002.mail_id;


--
-- Name: mail_202003; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202003 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202003 OWNER TO postgres;

--
-- Name: mail_202003_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202003_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202003_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202003_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202003_mail_id_seq OWNED BY mail_202003.mail_id;


--
-- Name: mail_202004; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202004 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202004 OWNER TO postgres;

--
-- Name: mail_202004_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202004_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202004_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202004_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202004_mail_id_seq OWNED BY mail_202004.mail_id;


--
-- Name: mail_202005; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202005 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202005 OWNER TO postgres;

--
-- Name: mail_202005_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202005_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202005_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202005_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202005_mail_id_seq OWNED BY mail_202005.mail_id;


--
-- Name: mail_202006; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202006 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202006 OWNER TO postgres;

--
-- Name: mail_202006_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202006_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202006_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202006_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202006_mail_id_seq OWNED BY mail_202006.mail_id;


--
-- Name: mail_202007; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202007 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202007 OWNER TO postgres;

--
-- Name: mail_202007_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202007_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202007_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202007_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202007_mail_id_seq OWNED BY mail_202007.mail_id;


--
-- Name: mail_202008; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202008 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202008 OWNER TO postgres;

--
-- Name: mail_202008_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202008_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202008_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202008_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202008_mail_id_seq OWNED BY mail_202008.mail_id;


--
-- Name: mail_202009; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202009 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202009 OWNER TO postgres;

--
-- Name: mail_202009_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202009_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202009_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202009_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202009_mail_id_seq OWNED BY mail_202009.mail_id;


--
-- Name: mail_202010; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202010 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202010 OWNER TO postgres;

--
-- Name: mail_202010_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202010_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202010_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202010_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202010_mail_id_seq OWNED BY mail_202010.mail_id;


--
-- Name: mail_202011; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202011 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202011 OWNER TO postgres;

--
-- Name: mail_202011_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202011_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202011_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202011_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202011_mail_id_seq OWNED BY mail_202011.mail_id;


--
-- Name: mail_202012; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_202012 (
    mail_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.mail_202012 OWNER TO postgres;

--
-- Name: mail_202012_mail_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE mail_202012_mail_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.mail_202012_mail_id_seq OWNER TO postgres;

--
-- Name: mail_202012_mail_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE mail_202012_mail_id_seq OWNED BY mail_202012.mail_id;


--
-- Name: mail_queue; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE mail_queue (
    mail_queue_id integer DEFAULT nextval('public.mail_queue_mail_queue_id_seq'::regclass) NOT NULL,
    state public.enum_mail_queue_state NOT NULL,
    template_name character varying(60) NOT NULL,
    added_at timestamp without time zone DEFAULT now() NOT NULL,
    remote_addr character varying(100) NOT NULL,
    sender_email character varying(200) NOT NULL,
    sender_name character varying(200) NOT NULL,
    receipient_email character varying(200) NOT NULL,
    receipient_name character varying(100) NOT NULL,
    subject character varying(100) NOT NULL,
    body text NOT NULL,
    list_id integer,
    rule_id integer,
    reason character varying(255) DEFAULT NULL::character varying,
    sender_phone character varying(100) DEFAULT NULL::character varying
);


ALTER TABLE blocket_2020.mail_queue OWNER TO postgres;

--
-- Name: notices; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE notices (
    notice_id integer NOT NULL,
    body text NOT NULL,
    uid integer,
    user_id integer,
    ad_id integer,
    abuse boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    token_id integer NOT NULL
);


ALTER TABLE blocket_2020.notices OWNER TO postgres;

--
-- Name: pay_log; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE pay_log (
    pay_log_id integer NOT NULL,
    pay_type public.enum_pay_log_pay_type NOT NULL,
    amount integer DEFAULT 0 NOT NULL,
    code character varying(30),
    token_id integer,
    paid_at timestamp(0) without time zone DEFAULT now() NOT NULL,
    payment_group_id integer,
    status public.enum_pay_log_status NOT NULL
);


ALTER TABLE blocket_2020.pay_log OWNER TO postgres;

--
-- Name: pay_log_references; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE pay_log_references (
    pay_log_id integer NOT NULL,
    ref_num integer DEFAULT 0 NOT NULL,
    ref_type public.enum_pay_log_references_ref_type NOT NULL,
    reference character varying(60) NOT NULL
);


ALTER TABLE blocket_2020.pay_log_references OWNER TO postgres;

--
-- Name: payment_groups; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE payment_groups (
    payment_group_id integer NOT NULL,
    code character varying(20),
    status public.enum_payment_groups_status NOT NULL,
    added_at timestamp without time zone NOT NULL,
    parent_payment_group_id integer
);


ALTER TABLE blocket_2020.payment_groups OWNER TO postgres;

--
-- Name: payments; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE payments (
    payment_group_id integer NOT NULL,
    payment_type public.enum_payments_payment_type NOT NULL,
    pay_amount integer NOT NULL,
    discount integer DEFAULT 0 NOT NULL
);


ALTER TABLE blocket_2020.payments OWNER TO postgres;

--
-- Name: purchase; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE purchase (
    purchase_id integer NOT NULL,
    doc_type public.enum_purchase_doc_type NOT NULL,
    doc_num bigint,
    external_doc_id bigint,
    status public.enum_purchase_status NOT NULL,
    receipt timestamp without time zone DEFAULT now() NOT NULL,
    delivery timestamp without time zone,
    rut character varying(12),
    name character varying(200),
    lob character varying(200),
    address character varying(500),
    region smallint,
    communes smallint,
    contact character varying(200),
    email character varying(80) NOT NULL,
    tax integer NOT NULL,
    discount integer DEFAULT 0 NOT NULL,
    seller_id integer,
    total_price integer NOT NULL,
    payment_group_id integer,
    payment_method public.enum_purchase_payment_method DEFAULT 'webpay'::public.enum_purchase_payment_method NOT NULL,
    payment_platform public.enum_purchase_payment_platform DEFAULT 'unknown'::public.enum_purchase_payment_platform NOT NULL,
    account_id integer
);


ALTER TABLE blocket_2020.purchase OWNER TO postgres;

--
-- Name: purchase_detail; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE purchase_detail (
    purchase_detail_id integer NOT NULL,
    purchase_id integer NOT NULL,
    product_id integer NOT NULL,
    price integer NOT NULL,
    action_id integer,
    ad_id integer NOT NULL,
    payment_group_id integer DEFAULT 0 NOT NULL
);


ALTER TABLE blocket_2020.purchase_detail OWNER TO postgres;

--
-- Name: purchase_states; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE purchase_states (
    purchase_id integer NOT NULL,
    purchase_state_id integer NOT NULL,
    status public.enum_purchase_status NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    remote_addr character varying(20) DEFAULT ''::character varying
);


ALTER TABLE blocket_2020.purchase_states OWNER TO postgres;

--
-- Name: state_params; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE state_params (
    ad_id integer NOT NULL,
    action_id integer NOT NULL,
    state_id integer NOT NULL,
    name public.enum_state_params_name NOT NULL,
    value text NOT NULL
);


ALTER TABLE blocket_2020.state_params OWNER TO postgres;

--
-- Name: stats; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE stats (
    stat_id integer NOT NULL,
    stat_type public.enum_stats_type NOT NULL,
    ext_id integer NOT NULL,
    total integer DEFAULT 0 NOT NULL
);


ALTER TABLE blocket_2020.stats OWNER TO postgres;

--
-- Name: stats_detail; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE stats_detail (
    stat_id integer NOT NULL,
    reg_date date NOT NULL,
    reg_hour time without time zone NOT NULL,
    value integer NOT NULL
);


ALTER TABLE blocket_2020.stats_detail OWNER TO postgres;

--
-- Name: stats_stat_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE stats_stat_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.stats_stat_id_seq OWNER TO postgres;

--
-- Name: stats_stat_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE stats_stat_id_seq OWNED BY stats.stat_id;


--
-- Name: tokens; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE tokens (
    token_id integer NOT NULL,
    token character varying(50) NOT NULL,
    admin_id integer,
    created_at timestamp without time zone,
    valid_to timestamp without time zone,
    remote_addr character varying(20) NOT NULL,
    info character varying(255),
    store_id integer,
    email character varying(60)
);


ALTER TABLE blocket_2020.tokens OWNER TO postgres;

--
-- Name: view_202001; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202001 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202001 OWNER TO postgres;

--
-- Name: view_202001_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202001_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202001_view_id_seq OWNER TO postgres;

--
-- Name: view_202001_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202001_view_id_seq OWNED BY view_202001.view_id;


--
-- Name: view_202002; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202002 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202002 OWNER TO postgres;

--
-- Name: view_202002_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202002_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202002_view_id_seq OWNER TO postgres;

--
-- Name: view_202002_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202002_view_id_seq OWNED BY view_202002.view_id;


--
-- Name: view_202003; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202003 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202003 OWNER TO postgres;

--
-- Name: view_202003_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202003_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202003_view_id_seq OWNER TO postgres;

--
-- Name: view_202003_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202003_view_id_seq OWNED BY view_202003.view_id;


--
-- Name: view_202004; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202004 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202004 OWNER TO postgres;

--
-- Name: view_202004_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202004_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202004_view_id_seq OWNER TO postgres;

--
-- Name: view_202004_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202004_view_id_seq OWNED BY view_202004.view_id;


--
-- Name: view_202005; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202005 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202005 OWNER TO postgres;

--
-- Name: view_202005_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202005_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202005_view_id_seq OWNER TO postgres;

--
-- Name: view_202005_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202005_view_id_seq OWNED BY view_202005.view_id;


--
-- Name: view_202006; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202006 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202006 OWNER TO postgres;

--
-- Name: view_202006_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202006_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202006_view_id_seq OWNER TO postgres;

--
-- Name: view_202006_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202006_view_id_seq OWNED BY view_202006.view_id;


--
-- Name: view_202007; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202007 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202007 OWNER TO postgres;

--
-- Name: view_202007_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202007_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202007_view_id_seq OWNER TO postgres;

--
-- Name: view_202007_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202007_view_id_seq OWNED BY view_202007.view_id;


--
-- Name: view_202008; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202008 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202008 OWNER TO postgres;

--
-- Name: view_202008_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202008_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202008_view_id_seq OWNER TO postgres;

--
-- Name: view_202008_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202008_view_id_seq OWNED BY view_202008.view_id;


--
-- Name: view_202009; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202009 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202009 OWNER TO postgres;

--
-- Name: view_202009_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202009_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202009_view_id_seq OWNER TO postgres;

--
-- Name: view_202009_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202009_view_id_seq OWNED BY view_202009.view_id;


--
-- Name: view_202010; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202010 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202010 OWNER TO postgres;

--
-- Name: view_202010_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202010_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202010_view_id_seq OWNER TO postgres;

--
-- Name: view_202010_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202010_view_id_seq OWNED BY view_202010.view_id;


--
-- Name: view_202011; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202011 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202011 OWNER TO postgres;

--
-- Name: view_202011_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202011_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202011_view_id_seq OWNER TO postgres;

--
-- Name: view_202011_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202011_view_id_seq OWNED BY view_202011.view_id;


--
-- Name: view_202012; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE view_202012 (
    view_id integer NOT NULL,
    list_id integer,
    date date,
    visits integer,
    total integer
);


ALTER TABLE blocket_2020.view_202012 OWNER TO postgres;

--
-- Name: view_202012_view_id_seq; Type: SEQUENCE; Schema: blocket_2020; Owner: postgres
--

CREATE SEQUENCE view_202012_view_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE blocket_2020.view_202012_view_id_seq OWNER TO postgres;

--
-- Name: view_202012_view_id_seq; Type: SEQUENCE OWNED BY; Schema: blocket_2020; Owner: postgres
--

ALTER SEQUENCE view_202012_view_id_seq OWNED BY view_202012.view_id;


--
-- Name: voucher_actions; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE voucher_actions (
    voucher_id integer NOT NULL,
    voucher_action_id integer NOT NULL,
    action_type public.enum_voucher_actions_action_type NOT NULL,
    current_state integer,
    state public.enum_voucher_states_state,
    amount integer NOT NULL,
    payment_group_id integer
);


ALTER TABLE blocket_2020.voucher_actions OWNER TO postgres;

--
-- Name: voucher_states; Type: TABLE; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE TABLE voucher_states (
    voucher_id integer NOT NULL,
    voucher_action_id integer NOT NULL,
    voucher_state_id integer NOT NULL,
    state public.enum_voucher_states_state NOT NULL,
    transition public.enum_voucher_states_transition NOT NULL,
    "timestamp" timestamp without time zone DEFAULT now() NOT NULL,
    remote_addr character varying(80),
    token_id integer,
    resulting_balance integer
);


ALTER TABLE blocket_2020.voucher_states OWNER TO postgres;

--
-- Name: account_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY accounts ALTER COLUMN account_id SET DEFAULT nextval('accounts_account_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202001 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202001_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202002 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202002_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202003 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202003_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202004 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202004_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202005 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202005_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202006 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202006_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202007 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202007_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202008 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202008_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202009 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202009_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202010 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202010_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202011 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202011_mail_id_seq'::regclass);


--
-- Name: mail_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY mail_202012 ALTER COLUMN mail_id SET DEFAULT nextval('mail_202012_mail_id_seq'::regclass);


--
-- Name: stat_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY stats ALTER COLUMN stat_id SET DEFAULT nextval('stats_stat_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202001 ALTER COLUMN view_id SET DEFAULT nextval('view_202001_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202002 ALTER COLUMN view_id SET DEFAULT nextval('view_202002_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202003 ALTER COLUMN view_id SET DEFAULT nextval('view_202003_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202004 ALTER COLUMN view_id SET DEFAULT nextval('view_202004_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202005 ALTER COLUMN view_id SET DEFAULT nextval('view_202005_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202006 ALTER COLUMN view_id SET DEFAULT nextval('view_202006_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202007 ALTER COLUMN view_id SET DEFAULT nextval('view_202007_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202008 ALTER COLUMN view_id SET DEFAULT nextval('view_202008_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202009 ALTER COLUMN view_id SET DEFAULT nextval('view_202009_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202010 ALTER COLUMN view_id SET DEFAULT nextval('view_202010_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202011 ALTER COLUMN view_id SET DEFAULT nextval('view_202011_view_id_seq'::regclass);


--
-- Name: view_id; Type: DEFAULT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY view_202012 ALTER COLUMN view_id SET DEFAULT nextval('view_202012_view_id_seq'::regclass);


--
-- Name: accounts_email_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_email_key UNIQUE (email);


--
-- Name: accounts_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (account_id);


--
-- Name: action_params_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY action_params
    ADD CONSTRAINT action_params_pkey PRIMARY KEY (ad_id, action_id, name);


--
-- Name: action_states_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY action_states
    ADD CONSTRAINT action_states_pkey PRIMARY KEY (ad_id, action_id, state_id);


--
-- Name: action_states_state_id_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY action_states
    ADD CONSTRAINT action_states_state_id_key UNIQUE (state_id);


--
-- Name: ad_actions_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_actions
    ADD CONSTRAINT ad_actions_pkey PRIMARY KEY (ad_id, action_id);


--
-- Name: ad_changes_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_changes
    ADD CONSTRAINT ad_changes_pkey PRIMARY KEY (ad_id, action_id, state_id, is_param, column_name);


--
-- Name: ad_image_changes_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_image_changes
    ADD CONSTRAINT ad_image_changes_pkey PRIMARY KEY (ad_id, action_id, state_id, seq_no);


--
-- Name: ad_images_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_images
    ADD CONSTRAINT ad_images_pkey PRIMARY KEY (ad_id, seq_no);


--
-- Name: ad_media_changes_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_media_changes
    ADD CONSTRAINT ad_media_changes_pkey PRIMARY KEY (ad_id, action_id, state_id, seq_no);


--
-- Name: ad_media_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_media
    ADD CONSTRAINT ad_media_pkey PRIMARY KEY (ad_media_id);


--
-- Name: ad_params_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ad_params
    ADD CONSTRAINT ad_params_pkey PRIMARY KEY (ad_id, name);


--
-- Name: ads_list_id_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ads
    ADD CONSTRAINT ads_list_id_key UNIQUE (list_id);


--
-- Name: ads_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ads
    ADD CONSTRAINT ads_pkey PRIMARY KEY (ad_id);


--
-- Name: mail_202001_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202001
    ADD CONSTRAINT mail_202001_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202002_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202002
    ADD CONSTRAINT mail_202002_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202003_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202003
    ADD CONSTRAINT mail_202003_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202004_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202004
    ADD CONSTRAINT mail_202004_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202005_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202005
    ADD CONSTRAINT mail_202005_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202006_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202006
    ADD CONSTRAINT mail_202006_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202007_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202007
    ADD CONSTRAINT mail_202007_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202008_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202008
    ADD CONSTRAINT mail_202008_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202009_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202009
    ADD CONSTRAINT mail_202009_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202010_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202010
    ADD CONSTRAINT mail_202010_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202011_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202011
    ADD CONSTRAINT mail_202011_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_202012_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_202012
    ADD CONSTRAINT mail_202012_pkey PRIMARY KEY (mail_id);


--
-- Name: mail_queue_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY mail_queue
    ADD CONSTRAINT mail_queue_pkey PRIMARY KEY (mail_queue_id);


--
-- Name: notices_ad_id_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_ad_id_key UNIQUE (ad_id);


--
-- Name: notices_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_pkey PRIMARY KEY (notice_id);


--
-- Name: notices_uid_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_uid_key UNIQUE (uid);


--
-- Name: notices_user_id_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_user_id_key UNIQUE (user_id);


--
-- Name: pay_log_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pay_log
    ADD CONSTRAINT pay_log_pkey PRIMARY KEY (pay_log_id);


--
-- Name: pay_log_references_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pay_log_references
    ADD CONSTRAINT pay_log_references_pkey PRIMARY KEY (pay_log_id, ref_num);


--
-- Name: payment_groups_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY payment_groups
    ADD CONSTRAINT payment_groups_pkey PRIMARY KEY (payment_group_id);


--
-- Name: payments_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_pkey PRIMARY KEY (payment_group_id, payment_type);


--
-- Name: purchase_detail_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY purchase_detail
    ADD CONSTRAINT purchase_detail_pkey PRIMARY KEY (purchase_id, purchase_detail_id);


--
-- Name: purchase_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY purchase
    ADD CONSTRAINT purchase_pkey PRIMARY KEY (purchase_id);


--
-- Name: purchase_states_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY purchase_states
    ADD CONSTRAINT purchase_states_pkey PRIMARY KEY (purchase_id, purchase_state_id);


--
-- Name: state_params_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY state_params
    ADD CONSTRAINT state_params_pkey PRIMARY KEY (ad_id, action_id, state_id, name);


--
-- Name: stats_detail_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stats_detail
    ADD CONSTRAINT stats_detail_pkey PRIMARY KEY (stat_id, reg_date, reg_hour);


--
-- Name: stats_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY stats
    ADD CONSTRAINT stats_pkey PRIMARY KEY (stat_id);


--
-- Name: tokens_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_pkey PRIMARY KEY (token_id);


--
-- Name: tokens_token_key; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tokens
    ADD CONSTRAINT tokens_token_key UNIQUE (token);


--
-- Name: view_202001_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202001
    ADD CONSTRAINT view_202001_pkey PRIMARY KEY (view_id);


--
-- Name: view_202002_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202002
    ADD CONSTRAINT view_202002_pkey PRIMARY KEY (view_id);


--
-- Name: view_202003_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202003
    ADD CONSTRAINT view_202003_pkey PRIMARY KEY (view_id);


--
-- Name: view_202004_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202004
    ADD CONSTRAINT view_202004_pkey PRIMARY KEY (view_id);


--
-- Name: view_202005_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202005
    ADD CONSTRAINT view_202005_pkey PRIMARY KEY (view_id);


--
-- Name: view_202006_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202006
    ADD CONSTRAINT view_202006_pkey PRIMARY KEY (view_id);


--
-- Name: view_202007_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202007
    ADD CONSTRAINT view_202007_pkey PRIMARY KEY (view_id);


--
-- Name: view_202008_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202008
    ADD CONSTRAINT view_202008_pkey PRIMARY KEY (view_id);


--
-- Name: view_202009_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202009
    ADD CONSTRAINT view_202009_pkey PRIMARY KEY (view_id);


--
-- Name: view_202010_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202010
    ADD CONSTRAINT view_202010_pkey PRIMARY KEY (view_id);


--
-- Name: view_202011_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202011
    ADD CONSTRAINT view_202011_pkey PRIMARY KEY (view_id);


--
-- Name: view_202012_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY view_202012
    ADD CONSTRAINT view_202012_pkey PRIMARY KEY (view_id);


--
-- Name: voucher_actions_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY voucher_actions
    ADD CONSTRAINT voucher_actions_pkey PRIMARY KEY (voucher_id, voucher_action_id);


--
-- Name: voucher_states_pkey; Type: CONSTRAINT; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY voucher_states
    ADD CONSTRAINT voucher_states_pkey PRIMARY KEY (voucher_id, voucher_action_id, voucher_state_id);


--
-- Name: index_action_params_ad_id_action_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_params_ad_id_action_id ON action_params USING btree (ad_id, action_id);


--
-- Name: index_action_params_name_value; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_params_name_value ON action_params USING btree (name, value);


--
-- Name: index_action_states_ad_id_action_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_ad_id_action_id ON action_states USING btree (ad_id, action_id);


--
-- Name: index_action_states_ad_id_action_id_state; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_ad_id_action_id_state ON action_states USING btree (ad_id, action_id, state);


--
-- Name: index_action_states_state; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_state ON action_states USING btree (state);


--
-- Name: index_action_states_state_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_state_ad_id ON action_states USING btree (state, ad_id);


--
-- Name: index_action_states_state_reg_timestamp; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_state_reg_timestamp ON action_states USING btree ("timestamp", ((state = 'reg'::public.enum_action_states_state)));


--
-- Name: index_action_states_state_timestamp; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_state_timestamp ON action_states USING btree (state, "timestamp");


--
-- Name: index_action_states_token_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_token_id ON action_states USING btree (token_id);


--
-- Name: index_action_states_transition_timestamp; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_action_states_transition_timestamp ON action_states USING btree (transition, "timestamp");


--
-- Name: index_ad_actions_action_type; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_action_type ON ad_actions USING btree (action_type);


--
-- Name: index_ad_actions_action_type_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_action_type_ad_id ON ad_actions USING btree (action_type, ad_id);


--
-- Name: index_ad_actions_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_ad_id ON ad_actions USING btree (ad_id);


--
-- Name: index_ad_actions_locked_by; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_locked_by ON ad_actions USING btree (locked_by);


--
-- Name: index_ad_actions_queue; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_queue ON ad_actions USING btree (queue);


--
-- Name: index_ad_actions_state; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_state ON ad_actions USING btree (state);


--
-- Name: index_ad_actions_state_queue; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_actions_state_queue ON ad_actions USING btree (state, queue);


--
-- Name: index_ad_changes_ad_id_action_id_state_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_changes_ad_id_action_id_state_id ON ad_changes USING btree (ad_id, action_id, state_id);


--
-- Name: index_ad_images_changes_ad_id_action_id_state_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_images_changes_ad_id_action_id_state_id ON ad_image_changes USING btree (ad_id, action_id, state_id);


--
-- Name: index_ad_media_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_media_ad_id ON ad_media USING btree (ad_id);


--
-- Name: index_ad_media_changes_ad_id_action_id_state_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_media_changes_ad_id_action_id_state_id ON ad_media_changes USING btree (ad_id, action_id, state_id);


--
-- Name: index_ad_params_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_params_ad_id ON ad_params USING btree (ad_id);


--
-- Name: index_ad_params_name_value; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ad_params_name_value ON ad_params USING btree (name, value);


--
-- Name: index_ads_category; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_category ON ads USING btree (category);


--
-- Name: index_ads_category_region; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_category_region ON ads USING btree (category, region);


--
-- Name: index_ads_company_ad; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_company_ad ON ads USING btree (company_ad);


--
-- Name: index_ads_list_time; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_list_time ON ads USING btree (list_time);


--
-- Name: index_ads_name; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_name ON ads USING btree (lower((name)::text));


--
-- Name: index_ads_passwd; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_passwd ON ads USING btree (lower((passwd)::text));


--
-- Name: index_ads_phone; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_phone ON ads USING btree (phone varchar_pattern_ops);


--
-- Name: index_ads_phone_repl; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_phone_repl ON ads USING btree (replace(replace((phone)::text, '-'::text, ''::text), ' '::text, ''::text) varchar_pattern_ops);


--
-- Name: index_ads_price; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_price ON ads USING btree (price);


--
-- Name: index_ads_status; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_status ON ads USING btree (status);


--
-- Name: index_ads_status_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_status_ad_id ON ads USING btree (status, ad_id);


--
-- Name: index_ads_status_list_time; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_status_list_time ON ads USING btree (status, list_time);


--
-- Name: index_ads_subject; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_subject ON ads USING btree (lower((subject)::text) varchar_pattern_ops);


--
-- Name: index_ads_user_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_ads_user_id ON ads USING btree (user_id);


--
-- Name: index_mail_202001_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202001_list_id_date ON mail_202001 USING btree (list_id, date);


--
-- Name: index_mail_202002_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202002_list_id_date ON mail_202002 USING btree (list_id, date);


--
-- Name: index_mail_202003_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202003_list_id_date ON mail_202003 USING btree (list_id, date);


--
-- Name: index_mail_202004_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202004_list_id_date ON mail_202004 USING btree (list_id, date);


--
-- Name: index_mail_202005_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202005_list_id_date ON mail_202005 USING btree (list_id, date);


--
-- Name: index_mail_202006_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202006_list_id_date ON mail_202006 USING btree (list_id, date);


--
-- Name: index_mail_202007_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202007_list_id_date ON mail_202007 USING btree (list_id, date);


--
-- Name: index_mail_202008_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202008_list_id_date ON mail_202008 USING btree (list_id, date);


--
-- Name: index_mail_202009_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202009_list_id_date ON mail_202009 USING btree (list_id, date);


--
-- Name: index_mail_202010_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202010_list_id_date ON mail_202010 USING btree (list_id, date);


--
-- Name: index_mail_202011_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202011_list_id_date ON mail_202011 USING btree (list_id, date);


--
-- Name: index_mail_202012_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_202012_list_id_date ON mail_202012 USING btree (list_id, date);


--
-- Name: index_mail_queue_added_at; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_added_at ON mail_queue USING btree (added_at);


--
-- Name: index_mail_queue_receipient_email; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_receipient_email ON mail_queue USING btree (lower((receipient_email)::text) varchar_pattern_ops);


--
-- Name: index_mail_queue_receipient_name; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_receipient_name ON mail_queue USING btree (lower((receipient_name)::text) varchar_pattern_ops);


--
-- Name: index_mail_queue_sender_email; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_sender_email ON mail_queue USING btree (lower((sender_email)::text) varchar_pattern_ops);


--
-- Name: index_mail_queue_sender_name; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_sender_name ON mail_queue USING btree (lower((sender_name)::text) varchar_pattern_ops);


--
-- Name: index_mail_queue_state_spam; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_mail_queue_state_spam ON mail_queue USING btree (state) WHERE (state = ANY (ARRAY['manual'::public.enum_mail_queue_state, 'abuse'::public.enum_mail_queue_state]));


--
-- Name: index_notices_ad_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_notices_ad_id ON notices USING btree (ad_id);


--
-- Name: index_notices_token_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_notices_token_id ON notices USING btree (token_id);


--
-- Name: index_notices_user_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_notices_user_id ON notices USING btree (user_id);


--
-- Name: index_pay_log_amount; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_amount ON pay_log USING btree (amount);


--
-- Name: index_pay_log_code; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_code ON pay_log USING btree (code varchar_pattern_ops);


--
-- Name: index_pay_log_code_1; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_code_1 ON pay_log USING btree (code);


--
-- Name: index_pay_log_paid_at; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_paid_at ON pay_log USING btree (paid_at);


--
-- Name: index_pay_log_pay_type; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_pay_type ON pay_log USING btree (pay_type);


--
-- Name: index_pay_log_payment_group_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_payment_group_id ON pay_log USING btree (payment_group_id);


--
-- Name: index_pay_log_references_pay_log_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_references_pay_log_id ON pay_log_references USING btree (pay_log_id);


--
-- Name: index_pay_log_references_ref_type_reference; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_references_ref_type_reference ON pay_log_references USING btree (ref_type, reference varchar_pattern_ops);


--
-- Name: index_pay_log_references_ref_type_reference_1; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_references_ref_type_reference_1 ON pay_log_references USING btree (ref_type, reference);


--
-- Name: index_pay_log_status; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_status ON pay_log USING btree (status);


--
-- Name: index_pay_log_status_paid_at; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_status_paid_at ON pay_log USING btree (status, paid_at);


--
-- Name: index_pay_log_token_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_pay_log_token_id ON pay_log USING btree (token_id);


--
-- Name: index_payment_group_code; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_group_code ON payment_groups USING btree (code);


--
-- Name: index_payment_group_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_group_id ON ad_actions USING btree (payment_group_id);


--
-- Name: index_payment_groups_added_at; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_groups_added_at ON payment_groups USING btree (added_at);


--
-- Name: index_payment_groups_parent_payment_group_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_groups_parent_payment_group_id ON payment_groups USING btree (parent_payment_group_id);


--
-- Name: index_payment_groups_status; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_groups_status ON payment_groups USING btree (status);


--
-- Name: index_payment_payment_type; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payment_payment_type ON payments USING btree (payment_type);


--
-- Name: index_payments_pay_amount; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payments_pay_amount ON payments USING btree (pay_amount);


--
-- Name: index_payments_payment_group; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_payments_payment_group ON payments USING btree (payment_group_id);


--
-- Name: index_puchase_detail_ad_product; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_puchase_detail_ad_product ON purchase_detail USING btree (ad_id, product_id);


--
-- Name: index_purchase_detail_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_detail_id ON purchase_detail USING btree (purchase_id, purchase_detail_id);


--
-- Name: index_purchase_doc; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_doc ON purchase USING btree (doc_type, doc_num) WHERE (doc_num IS NOT NULL);


--
-- Name: index_purchase_email; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_email ON purchase USING btree (email);


--
-- Name: index_purchase_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_id ON purchase USING btree (purchase_id);


--
-- Name: index_purchase_rut; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_rut ON purchase USING btree (rut) WHERE ((rut IS NOT NULL) AND (doc_num IS NOT NULL));


--
-- Name: index_purchase_states; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_purchase_states ON purchase_states USING btree (purchase_id, purchase_state_id);


--
-- Name: index_remote_addr; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_remote_addr ON action_states USING btree (remote_addr);


--
-- Name: index_stat_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_stat_id ON stats USING btree (stat_id);


--
-- Name: index_stat_search; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_stat_search ON stats USING btree (stat_type, ext_id);


--
-- Name: index_state_params_ad_id_action_id_state_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_state_params_ad_id_action_id_state_id ON state_params USING btree (ad_id, action_id, state_id);


--
-- Name: index_state_params_name_value; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_state_params_name_value ON state_params USING btree (name, value);


--
-- Name: index_store_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_store_id ON ads USING btree (store_id);


--
-- Name: index_tokens_admin_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_tokens_admin_id ON tokens USING btree (admin_id);


--
-- Name: index_tokens_email; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_tokens_email ON tokens USING btree (email);


--
-- Name: index_tokens_store_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_tokens_store_id ON tokens USING btree (store_id);


--
-- Name: index_tokens_valid_to; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_tokens_valid_to ON tokens USING btree (valid_to);


--
-- Name: index_view_202001_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202001_list_id_date ON view_202001 USING btree (list_id, date);


--
-- Name: index_view_202002_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202002_list_id_date ON view_202002 USING btree (list_id, date);


--
-- Name: index_view_202003_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202003_list_id_date ON view_202003 USING btree (list_id, date);


--
-- Name: index_view_202008_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202008_list_id_date ON view_202008 USING btree (list_id, date);


--
-- Name: index_view_202009_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202009_list_id_date ON view_202009 USING btree (list_id, date);


--
-- Name: index_view_202010_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202010_list_id_date ON view_202010 USING btree (list_id, date);


--
-- Name: index_view_202011_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202011_list_id_date ON view_202011 USING btree (list_id, date);


--
-- Name: index_view_202012_list_id_date; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_view_202012_list_id_date ON view_202012 USING btree (list_id, date);


--
-- Name: index_voucher_actions_payment_group_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_voucher_actions_payment_group_id ON voucher_actions USING btree (payment_group_id);


--
-- Name: index_voucher_actions_voucher_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_voucher_actions_voucher_id ON voucher_actions USING btree (voucher_id);


--
-- Name: index_voucher_states_token_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_voucher_states_token_id ON voucher_states USING btree (token_id);


--
-- Name: index_voucher_states_voucher_id_voucher_action_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX index_voucher_states_voucher_id_voucher_action_id ON voucher_states USING btree (voucher_id, voucher_action_id);


--
-- Name: mail_queue_list_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX mail_queue_list_id ON mail_queue USING btree (list_id);


--
-- Name: mail_queue_remote_addr_state; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX mail_queue_remote_addr_state ON mail_queue USING btree (remote_addr, state);


--
-- Name: stats_detail_id; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE INDEX stats_detail_id ON stats_detail USING btree (stat_id, reg_date, reg_hour);


--
-- Name: unique_pay_log_pay_type_code_paid_at; Type: INDEX; Schema: blocket_2020; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX unique_pay_log_pay_type_code_paid_at ON pay_log USING btree (pay_type, code, paid_at, (COALESCE(payment_group_id, 0)));


--
-- Name: action_params_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY action_params
    ADD CONSTRAINT action_params_ad_id_fkey FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions(ad_id, action_id);


--
-- Name: action_states_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY action_states
    ADD CONSTRAINT action_states_ad_id_fkey FOREIGN KEY (ad_id, action_id) REFERENCES ad_actions(ad_id, action_id);


--
-- Name: action_states_token_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY action_states
    ADD CONSTRAINT action_states_token_id_fkey FOREIGN KEY (token_id) REFERENCES tokens(token_id);


--
-- Name: ad_actions_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_actions
    ADD CONSTRAINT ad_actions_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES ads(ad_id);


--
-- Name: ad_actions_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_actions
    ADD CONSTRAINT ad_actions_payment_group_id_fkey FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: ad_changes_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_changes
    ADD CONSTRAINT ad_changes_ad_id_fkey FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states(ad_id, action_id, state_id);


--
-- Name: ad_image_changes_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_image_changes
    ADD CONSTRAINT ad_image_changes_ad_id_fkey FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states(ad_id, action_id, state_id);


--
-- Name: ad_images_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_images
    ADD CONSTRAINT ad_images_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES ads(ad_id);


--
-- Name: ad_media_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_media
    ADD CONSTRAINT ad_media_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES ads(ad_id);


--
-- Name: ad_media_changes_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_media_changes
    ADD CONSTRAINT ad_media_changes_ad_id_fkey FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states(ad_id, action_id, state_id);


--
-- Name: ad_params_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY ad_params
    ADD CONSTRAINT ad_params_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES ads(ad_id);


--
-- Name: notices_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_ad_id_fkey FOREIGN KEY (ad_id) REFERENCES ads(ad_id);


--
-- Name: notices_token_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY notices
    ADD CONSTRAINT notices_token_id_fkey FOREIGN KEY (token_id) REFERENCES tokens(token_id);


--
-- Name: pay_log_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY pay_log
    ADD CONSTRAINT pay_log_payment_group_id_fkey FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: pay_log_references_pay_log_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY pay_log_references
    ADD CONSTRAINT pay_log_references_pay_log_id_fkey FOREIGN KEY (pay_log_id) REFERENCES pay_log(pay_log_id);


--
-- Name: pay_log_token_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY pay_log
    ADD CONSTRAINT pay_log_token_id_fkey FOREIGN KEY (token_id) REFERENCES tokens(token_id);


--
-- Name: payment_groups_parent_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY payment_groups
    ADD CONSTRAINT payment_groups_parent_payment_group_id_fkey FOREIGN KEY (parent_payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: payments_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY payments
    ADD CONSTRAINT payments_payment_group_id_fkey FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: purchase_detail_purchase_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY purchase_detail
    ADD CONSTRAINT purchase_detail_purchase_id_fkey FOREIGN KEY (purchase_id) REFERENCES purchase(purchase_id) ON DELETE CASCADE;


--
-- Name: purchase_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY purchase
    ADD CONSTRAINT purchase_payment_group_id_fkey FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: purchase_states_purchase_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY purchase_states
    ADD CONSTRAINT purchase_states_purchase_id_fkey FOREIGN KEY (purchase_id) REFERENCES purchase(purchase_id) ON DELETE CASCADE;


--
-- Name: state_params_ad_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY state_params
    ADD CONSTRAINT state_params_ad_id_fkey FOREIGN KEY (ad_id, action_id, state_id) REFERENCES action_states(ad_id, action_id, state_id);


--
-- Name: stats_detail_stat_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY stats_detail
    ADD CONSTRAINT stats_detail_stat_id_fkey FOREIGN KEY (stat_id) REFERENCES stats(stat_id) ON DELETE CASCADE;


--
-- Name: voucher_actions_payment_group_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY voucher_actions
    ADD CONSTRAINT voucher_actions_payment_group_id_fkey FOREIGN KEY (payment_group_id) REFERENCES payment_groups(payment_group_id);


--
-- Name: voucher_states_token_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY voucher_states
    ADD CONSTRAINT voucher_states_token_id_fkey FOREIGN KEY (token_id) REFERENCES tokens(token_id);


--
-- Name: voucher_states_voucher_id_fkey; Type: FK CONSTRAINT; Schema: blocket_2020; Owner: postgres
--

ALTER TABLE ONLY voucher_states
    ADD CONSTRAINT voucher_states_voucher_id_fkey FOREIGN KEY (voucher_id, voucher_action_id) REFERENCES voucher_actions(voucher_id, voucher_action_id);


--
-- Name: blocket_2020; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA blocket_2020 FROM PUBLIC;
REVOKE ALL ON SCHEMA blocket_2020 FROM postgres;
GRANT ALL ON SCHEMA blocket_2020 TO postgres;
GRANT USAGE ON SCHEMA blocket_2020 TO breaders;
GRANT USAGE ON SCHEMA blocket_2020 TO bwriters;
GRANT USAGE ON SCHEMA blocket_2020 TO trans;
GRANT USAGE ON SCHEMA blocket_2020 TO bnbiuser;
GRANT USAGE ON SCHEMA blocket_2020 TO devs;
GRANT USAGE ON SCHEMA blocket_2020 TO querybill;


--
-- Name: accounts; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE accounts FROM PUBLIC;
REVOKE ALL ON TABLE accounts FROM postgres;
GRANT ALL ON TABLE accounts TO postgres;
GRANT SELECT ON TABLE accounts TO bnbiuser;
GRANT SELECT ON TABLE accounts TO devs;
GRANT SELECT ON TABLE accounts TO querybill;


--
-- Name: action_params; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE action_params FROM PUBLIC;
REVOKE ALL ON TABLE action_params FROM postgres;
GRANT ALL ON TABLE action_params TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE action_params TO bwriters;
GRANT SELECT ON TABLE action_params TO breaders;
GRANT SELECT ON TABLE action_params TO querybill;


--
-- Name: action_states; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE action_states FROM PUBLIC;
REVOKE ALL ON TABLE action_states FROM postgres;
GRANT ALL ON TABLE action_states TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE action_states TO bwriters;
GRANT SELECT ON TABLE action_states TO breaders;
GRANT SELECT ON TABLE action_states TO querybill;


--
-- Name: ad_actions; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_actions FROM PUBLIC;
REVOKE ALL ON TABLE ad_actions FROM postgres;
GRANT ALL ON TABLE ad_actions TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_actions TO bwriters;
GRANT SELECT ON TABLE ad_actions TO breaders;
GRANT SELECT ON TABLE ad_actions TO querybill;


--
-- Name: ad_changes; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_changes FROM PUBLIC;
REVOKE ALL ON TABLE ad_changes FROM postgres;
GRANT ALL ON TABLE ad_changes TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_changes TO bwriters;
GRANT SELECT ON TABLE ad_changes TO breaders;
GRANT SELECT ON TABLE ad_changes TO querybill;


--
-- Name: ad_image_changes; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_image_changes FROM PUBLIC;
REVOKE ALL ON TABLE ad_image_changes FROM postgres;
GRANT ALL ON TABLE ad_image_changes TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_image_changes TO bwriters;
GRANT SELECT ON TABLE ad_image_changes TO breaders;
GRANT SELECT ON TABLE ad_image_changes TO querybill;


--
-- Name: ad_images; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_images FROM PUBLIC;
REVOKE ALL ON TABLE ad_images FROM postgres;
GRANT ALL ON TABLE ad_images TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_images TO bwriters;
GRANT SELECT ON TABLE ad_images TO breaders;
GRANT SELECT ON TABLE ad_images TO querybill;


--
-- Name: ad_media; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_media FROM PUBLIC;
REVOKE ALL ON TABLE ad_media FROM postgres;
GRANT ALL ON TABLE ad_media TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_media TO bwriters;
GRANT SELECT ON TABLE ad_media TO breaders;
GRANT SELECT ON TABLE ad_media TO querybill;


--
-- Name: ad_media_changes; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_media_changes FROM PUBLIC;
REVOKE ALL ON TABLE ad_media_changes FROM postgres;
GRANT ALL ON TABLE ad_media_changes TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_media_changes TO bwriters;
GRANT SELECT ON TABLE ad_media_changes TO breaders;
GRANT SELECT ON TABLE ad_media_changes TO querybill;


--
-- Name: ad_params; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ad_params FROM PUBLIC;
REVOKE ALL ON TABLE ad_params FROM postgres;
GRANT ALL ON TABLE ad_params TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ad_params TO bwriters;
GRANT SELECT ON TABLE ad_params TO breaders;
GRANT SELECT ON TABLE ad_params TO querybill;


--
-- Name: ads; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE ads FROM PUBLIC;
REVOKE ALL ON TABLE ads FROM postgres;
GRANT ALL ON TABLE ads TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE ads TO bwriters;
GRANT SELECT ON TABLE ads TO breaders;
GRANT SELECT ON TABLE ads TO querybill;


--
-- Name: mail_202001; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202001 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202001 FROM postgres;
GRANT ALL ON TABLE mail_202001 TO postgres;
GRANT SELECT ON TABLE mail_202001 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202001 TO bwriters;
GRANT SELECT ON TABLE mail_202001 TO bnbiuser;
GRANT SELECT ON TABLE mail_202001 TO devs;
GRANT SELECT ON TABLE mail_202001 TO querybill;


--
-- Name: mail_202001_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202001_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202001_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202001_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202001_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202001_mail_id_seq TO bwriters;


--
-- Name: mail_202002; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202002 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202002 FROM postgres;
GRANT ALL ON TABLE mail_202002 TO postgres;
GRANT SELECT ON TABLE mail_202002 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202002 TO bwriters;
GRANT SELECT ON TABLE mail_202002 TO bnbiuser;
GRANT SELECT ON TABLE mail_202002 TO devs;
GRANT SELECT ON TABLE mail_202002 TO querybill;


--
-- Name: mail_202002_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202002_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202002_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202002_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202002_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202002_mail_id_seq TO bwriters;


--
-- Name: mail_202003; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202003 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202003 FROM postgres;
GRANT ALL ON TABLE mail_202003 TO postgres;
GRANT SELECT ON TABLE mail_202003 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202003 TO bwriters;
GRANT SELECT ON TABLE mail_202003 TO bnbiuser;
GRANT SELECT ON TABLE mail_202003 TO devs;
GRANT SELECT ON TABLE mail_202003 TO querybill;


--
-- Name: mail_202003_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202003_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202003_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202003_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202003_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202003_mail_id_seq TO bwriters;


--
-- Name: mail_202004; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202004 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202004 FROM postgres;
GRANT ALL ON TABLE mail_202004 TO postgres;
GRANT SELECT ON TABLE mail_202004 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202004 TO bwriters;
GRANT SELECT ON TABLE mail_202004 TO bnbiuser;
GRANT SELECT ON TABLE mail_202004 TO devs;
GRANT SELECT ON TABLE mail_202004 TO querybill;


--
-- Name: mail_202004_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202004_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202004_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202004_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202004_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202004_mail_id_seq TO bwriters;


--
-- Name: mail_202005; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202005 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202005 FROM postgres;
GRANT ALL ON TABLE mail_202005 TO postgres;
GRANT SELECT ON TABLE mail_202005 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202005 TO bwriters;
GRANT SELECT ON TABLE mail_202005 TO bnbiuser;
GRANT SELECT ON TABLE mail_202005 TO devs;
GRANT SELECT ON TABLE mail_202005 TO querybill;


--
-- Name: mail_202005_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202005_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202005_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202005_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202005_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202005_mail_id_seq TO bwriters;


--
-- Name: mail_202006; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202006 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202006 FROM postgres;
GRANT ALL ON TABLE mail_202006 TO postgres;
GRANT SELECT ON TABLE mail_202006 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202006 TO bwriters;
GRANT SELECT ON TABLE mail_202006 TO bnbiuser;
GRANT SELECT ON TABLE mail_202006 TO devs;
GRANT SELECT ON TABLE mail_202006 TO querybill;


--
-- Name: mail_202006_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202006_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202006_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202006_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202006_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202006_mail_id_seq TO bwriters;


--
-- Name: mail_202007; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202007 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202007 FROM postgres;
GRANT ALL ON TABLE mail_202007 TO postgres;
GRANT SELECT ON TABLE mail_202007 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202007 TO bwriters;
GRANT SELECT ON TABLE mail_202007 TO bnbiuser;
GRANT SELECT ON TABLE mail_202007 TO devs;
GRANT SELECT ON TABLE mail_202007 TO querybill;


--
-- Name: mail_202007_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202007_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202007_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202007_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202007_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202007_mail_id_seq TO bwriters;


--
-- Name: mail_202008; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202008 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202008 FROM postgres;
GRANT ALL ON TABLE mail_202008 TO postgres;
GRANT SELECT ON TABLE mail_202008 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202008 TO bwriters;
GRANT SELECT ON TABLE mail_202008 TO bnbiuser;
GRANT SELECT ON TABLE mail_202008 TO devs;
GRANT SELECT ON TABLE mail_202008 TO querybill;


--
-- Name: mail_202008_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202008_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202008_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202008_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202008_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202008_mail_id_seq TO bwriters;


--
-- Name: mail_202009; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202009 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202009 FROM postgres;
GRANT ALL ON TABLE mail_202009 TO postgres;
GRANT SELECT ON TABLE mail_202009 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202009 TO bwriters;
GRANT SELECT ON TABLE mail_202009 TO bnbiuser;
GRANT SELECT ON TABLE mail_202009 TO devs;
GRANT SELECT ON TABLE mail_202009 TO querybill;


--
-- Name: mail_202009_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202009_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202009_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202009_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202009_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202009_mail_id_seq TO bwriters;


--
-- Name: mail_202010; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202010 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202010 FROM postgres;
GRANT ALL ON TABLE mail_202010 TO postgres;
GRANT SELECT ON TABLE mail_202010 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202010 TO bwriters;
GRANT SELECT ON TABLE mail_202010 TO bnbiuser;
GRANT SELECT ON TABLE mail_202010 TO devs;
GRANT SELECT ON TABLE mail_202010 TO querybill;


--
-- Name: mail_202010_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202010_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202010_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202010_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202010_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202010_mail_id_seq TO bwriters;


--
-- Name: mail_202011; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202011 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202011 FROM postgres;
GRANT ALL ON TABLE mail_202011 TO postgres;
GRANT SELECT ON TABLE mail_202011 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202011 TO bwriters;
GRANT SELECT ON TABLE mail_202011 TO bnbiuser;
GRANT SELECT ON TABLE mail_202011 TO devs;
GRANT SELECT ON TABLE mail_202011 TO querybill;


--
-- Name: mail_202011_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202011_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202011_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202011_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202011_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202011_mail_id_seq TO bwriters;


--
-- Name: mail_202012; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_202012 FROM PUBLIC;
REVOKE ALL ON TABLE mail_202012 FROM postgres;
GRANT ALL ON TABLE mail_202012 TO postgres;
GRANT SELECT ON TABLE mail_202012 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_202012 TO bwriters;
GRANT SELECT ON TABLE mail_202012 TO bnbiuser;
GRANT SELECT ON TABLE mail_202012 TO devs;
GRANT SELECT ON TABLE mail_202012 TO querybill;


--
-- Name: mail_202012_mail_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE mail_202012_mail_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE mail_202012_mail_id_seq FROM postgres;
GRANT ALL ON SEQUENCE mail_202012_mail_id_seq TO postgres;
GRANT USAGE ON SEQUENCE mail_202012_mail_id_seq TO breaders;
GRANT USAGE ON SEQUENCE mail_202012_mail_id_seq TO bwriters;


--
-- Name: mail_queue; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE mail_queue FROM PUBLIC;
REVOKE ALL ON TABLE mail_queue FROM postgres;
GRANT ALL ON TABLE mail_queue TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE mail_queue TO bwriters;
GRANT SELECT ON TABLE mail_queue TO breaders;
GRANT SELECT ON TABLE mail_queue TO querybill;


--
-- Name: notices; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE notices FROM PUBLIC;
REVOKE ALL ON TABLE notices FROM postgres;
GRANT ALL ON TABLE notices TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE notices TO bwriters;
GRANT SELECT ON TABLE notices TO breaders;
GRANT SELECT ON TABLE notices TO querybill;


--
-- Name: pay_log; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE pay_log FROM PUBLIC;
REVOKE ALL ON TABLE pay_log FROM postgres;
GRANT ALL ON TABLE pay_log TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pay_log TO bwriters;
GRANT SELECT ON TABLE pay_log TO breaders;
GRANT SELECT ON TABLE pay_log TO querybill;


--
-- Name: pay_log_references; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE pay_log_references FROM PUBLIC;
REVOKE ALL ON TABLE pay_log_references FROM postgres;
GRANT ALL ON TABLE pay_log_references TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE pay_log_references TO bwriters;
GRANT SELECT ON TABLE pay_log_references TO breaders;
GRANT SELECT ON TABLE pay_log_references TO querybill;


--
-- Name: payment_groups; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE payment_groups FROM PUBLIC;
REVOKE ALL ON TABLE payment_groups FROM postgres;
GRANT ALL ON TABLE payment_groups TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE payment_groups TO bwriters;
GRANT SELECT ON TABLE payment_groups TO breaders;
GRANT SELECT ON TABLE payment_groups TO querybill;


--
-- Name: payments; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE payments FROM PUBLIC;
REVOKE ALL ON TABLE payments FROM postgres;
GRANT ALL ON TABLE payments TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE payments TO bwriters;
GRANT SELECT ON TABLE payments TO breaders;
GRANT SELECT ON TABLE payments TO querybill;


--
-- Name: purchase; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE purchase FROM PUBLIC;
REVOKE ALL ON TABLE purchase FROM postgres;
GRANT ALL ON TABLE purchase TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE purchase TO bwriters;
GRANT SELECT ON TABLE purchase TO breaders;
GRANT SELECT ON TABLE purchase TO querybill;


--
-- Name: purchase_detail; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE purchase_detail FROM PUBLIC;
REVOKE ALL ON TABLE purchase_detail FROM postgres;
GRANT ALL ON TABLE purchase_detail TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE purchase_detail TO bwriters;
GRANT SELECT ON TABLE purchase_detail TO breaders;
GRANT SELECT ON TABLE purchase_detail TO querybill;


--
-- Name: purchase_states; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE purchase_states FROM PUBLIC;
REVOKE ALL ON TABLE purchase_states FROM postgres;
GRANT ALL ON TABLE purchase_states TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE purchase_states TO bwriters;
GRANT SELECT ON TABLE purchase_states TO breaders;
GRANT SELECT ON TABLE purchase_states TO querybill;


--
-- Name: state_params; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE state_params FROM PUBLIC;
REVOKE ALL ON TABLE state_params FROM postgres;
GRANT ALL ON TABLE state_params TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE state_params TO bwriters;
GRANT SELECT ON TABLE state_params TO breaders;
GRANT SELECT ON TABLE state_params TO querybill;


--
-- Name: stats; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE stats FROM PUBLIC;
REVOKE ALL ON TABLE stats FROM postgres;
GRANT ALL ON TABLE stats TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE stats TO bwriters;
GRANT SELECT ON TABLE stats TO breaders;
GRANT SELECT ON TABLE stats TO querybill;


--
-- Name: stats_detail; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE stats_detail FROM PUBLIC;
REVOKE ALL ON TABLE stats_detail FROM postgres;
GRANT ALL ON TABLE stats_detail TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE stats_detail TO bwriters;
GRANT SELECT ON TABLE stats_detail TO breaders;
GRANT SELECT ON TABLE stats_detail TO querybill;


--
-- Name: tokens; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE tokens FROM PUBLIC;
REVOKE ALL ON TABLE tokens FROM postgres;
GRANT ALL ON TABLE tokens TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE tokens TO bwriters;
GRANT SELECT ON TABLE tokens TO breaders;
GRANT SELECT ON TABLE tokens TO querybill;


--
-- Name: view_202001; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202001 FROM PUBLIC;
REVOKE ALL ON TABLE view_202001 FROM postgres;
GRANT ALL ON TABLE view_202001 TO postgres;
GRANT SELECT ON TABLE view_202001 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202001 TO bwriters;
GRANT SELECT ON TABLE view_202001 TO bnbiuser;
GRANT SELECT ON TABLE view_202001 TO devs;
GRANT SELECT ON TABLE view_202001 TO querybill;


--
-- Name: view_202001_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202001_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202001_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202001_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202001_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202001_view_id_seq TO bwriters;


--
-- Name: view_202002; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202002 FROM PUBLIC;
REVOKE ALL ON TABLE view_202002 FROM postgres;
GRANT ALL ON TABLE view_202002 TO postgres;
GRANT SELECT ON TABLE view_202002 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202002 TO bwriters;
GRANT SELECT ON TABLE view_202002 TO bnbiuser;
GRANT SELECT ON TABLE view_202002 TO devs;
GRANT SELECT ON TABLE view_202002 TO querybill;


--
-- Name: view_202002_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202002_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202002_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202002_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202002_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202002_view_id_seq TO bwriters;


--
-- Name: view_202003; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202003 FROM PUBLIC;
REVOKE ALL ON TABLE view_202003 FROM postgres;
GRANT ALL ON TABLE view_202003 TO postgres;
GRANT SELECT ON TABLE view_202003 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202003 TO bwriters;
GRANT SELECT ON TABLE view_202003 TO bnbiuser;
GRANT SELECT ON TABLE view_202003 TO devs;
GRANT SELECT ON TABLE view_202003 TO querybill;


--
-- Name: view_202003_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202003_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202003_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202003_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202003_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202003_view_id_seq TO bwriters;


--
-- Name: view_202004; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202004 FROM PUBLIC;
REVOKE ALL ON TABLE view_202004 FROM postgres;
GRANT ALL ON TABLE view_202004 TO postgres;
GRANT SELECT ON TABLE view_202004 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202004 TO bwriters;
GRANT SELECT ON TABLE view_202004 TO bnbiuser;
GRANT SELECT ON TABLE view_202004 TO devs;
GRANT SELECT ON TABLE view_202004 TO querybill;


--
-- Name: view_202004_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202004_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202004_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202004_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202004_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202004_view_id_seq TO bwriters;


--
-- Name: view_202005; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202005 FROM PUBLIC;
REVOKE ALL ON TABLE view_202005 FROM postgres;
GRANT ALL ON TABLE view_202005 TO postgres;
GRANT SELECT ON TABLE view_202005 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202005 TO bwriters;
GRANT SELECT ON TABLE view_202005 TO bnbiuser;
GRANT SELECT ON TABLE view_202005 TO devs;
GRANT SELECT ON TABLE view_202005 TO querybill;


--
-- Name: view_202005_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202005_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202005_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202005_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202005_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202005_view_id_seq TO bwriters;


--
-- Name: view_202006; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202006 FROM PUBLIC;
REVOKE ALL ON TABLE view_202006 FROM postgres;
GRANT ALL ON TABLE view_202006 TO postgres;
GRANT SELECT ON TABLE view_202006 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202006 TO bwriters;
GRANT SELECT ON TABLE view_202006 TO bnbiuser;
GRANT SELECT ON TABLE view_202006 TO devs;
GRANT SELECT ON TABLE view_202006 TO querybill;


--
-- Name: view_202006_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202006_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202006_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202006_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202006_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202006_view_id_seq TO bwriters;


--
-- Name: view_202007; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202007 FROM PUBLIC;
REVOKE ALL ON TABLE view_202007 FROM postgres;
GRANT ALL ON TABLE view_202007 TO postgres;
GRANT SELECT ON TABLE view_202007 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202007 TO bwriters;
GRANT SELECT ON TABLE view_202007 TO bnbiuser;
GRANT SELECT ON TABLE view_202007 TO devs;
GRANT SELECT ON TABLE view_202007 TO querybill;


--
-- Name: view_202007_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202007_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202007_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202007_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202007_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202007_view_id_seq TO bwriters;


--
-- Name: view_202008; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202008 FROM PUBLIC;
REVOKE ALL ON TABLE view_202008 FROM postgres;
GRANT ALL ON TABLE view_202008 TO postgres;
GRANT SELECT ON TABLE view_202008 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202008 TO bwriters;
GRANT SELECT ON TABLE view_202008 TO bnbiuser;
GRANT SELECT ON TABLE view_202008 TO devs;
GRANT SELECT ON TABLE view_202008 TO querybill;


--
-- Name: view_202008_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202008_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202008_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202008_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202008_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202008_view_id_seq TO bwriters;


--
-- Name: view_202009; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202009 FROM PUBLIC;
REVOKE ALL ON TABLE view_202009 FROM postgres;
GRANT ALL ON TABLE view_202009 TO postgres;
GRANT SELECT ON TABLE view_202009 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202009 TO bwriters;
GRANT SELECT ON TABLE view_202009 TO bnbiuser;
GRANT SELECT ON TABLE view_202009 TO devs;
GRANT SELECT ON TABLE view_202009 TO querybill;


--
-- Name: view_202009_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202009_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202009_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202009_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202009_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202009_view_id_seq TO bwriters;


--
-- Name: view_202010; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202010 FROM PUBLIC;
REVOKE ALL ON TABLE view_202010 FROM postgres;
GRANT ALL ON TABLE view_202010 TO postgres;
GRANT SELECT ON TABLE view_202010 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202010 TO bwriters;
GRANT SELECT ON TABLE view_202010 TO bnbiuser;
GRANT SELECT ON TABLE view_202010 TO devs;
GRANT SELECT ON TABLE view_202010 TO querybill;


--
-- Name: view_202010_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202010_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202010_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202010_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202010_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202010_view_id_seq TO bwriters;


--
-- Name: view_202011; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202011 FROM PUBLIC;
REVOKE ALL ON TABLE view_202011 FROM postgres;
GRANT ALL ON TABLE view_202011 TO postgres;
GRANT SELECT ON TABLE view_202011 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202011 TO bwriters;
GRANT SELECT ON TABLE view_202011 TO bnbiuser;
GRANT SELECT ON TABLE view_202011 TO devs;
GRANT SELECT ON TABLE view_202011 TO querybill;


--
-- Name: view_202011_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202011_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202011_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202011_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202011_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202011_view_id_seq TO bwriters;


--
-- Name: view_202012; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE view_202012 FROM PUBLIC;
REVOKE ALL ON TABLE view_202012 FROM postgres;
GRANT ALL ON TABLE view_202012 TO postgres;
GRANT SELECT ON TABLE view_202012 TO breaders;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE view_202012 TO bwriters;
GRANT SELECT ON TABLE view_202012 TO bnbiuser;
GRANT SELECT ON TABLE view_202012 TO devs;
GRANT SELECT ON TABLE view_202012 TO querybill;


--
-- Name: view_202012_view_id_seq; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON SEQUENCE view_202012_view_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE view_202012_view_id_seq FROM postgres;
GRANT ALL ON SEQUENCE view_202012_view_id_seq TO postgres;
GRANT USAGE ON SEQUENCE view_202012_view_id_seq TO breaders;
GRANT USAGE ON SEQUENCE view_202012_view_id_seq TO bwriters;


--
-- Name: voucher_actions; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE voucher_actions FROM PUBLIC;
REVOKE ALL ON TABLE voucher_actions FROM postgres;
GRANT ALL ON TABLE voucher_actions TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE voucher_actions TO bwriters;
GRANT SELECT ON TABLE voucher_actions TO breaders;
GRANT SELECT ON TABLE voucher_actions TO querybill;


--
-- Name: voucher_states; Type: ACL; Schema: blocket_2020; Owner: postgres
--

REVOKE ALL ON TABLE voucher_states FROM PUBLIC;
REVOKE ALL ON TABLE voucher_states FROM postgres;
GRANT ALL ON TABLE voucher_states TO postgres;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE voucher_states TO bwriters;
GRANT SELECT ON TABLE voucher_states TO breaders;
GRANT SELECT ON TABLE voucher_states TO querybill;


--
-- PostgreSQL database dump complete
--

SET search_path = public, pg_catalog;
