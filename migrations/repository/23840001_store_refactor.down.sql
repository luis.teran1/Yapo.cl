UPDATE admin_privs SET priv_name = 'stores' WHERE priv_name like 'Stores';
UPDATE admin_privs SET priv_name = 'stores.create_store' WHERE priv_name like 'Stores.createStore';
UPDATE admin_privs SET priv_name = 'stores.edit_store' WHERE priv_name like 'Stores.editStore';
UPDATE admin_privs SET priv_name = 'stores.extend_store' WHERE priv_name like 'Stores.extendStore';
