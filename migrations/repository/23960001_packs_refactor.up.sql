UPDATE admin_privs SET priv_name = 'Packs'                     WHERE priv_name like 'packs';
UPDATE admin_privs SET priv_name = 'Packs.searchAndAssignInmo' WHERE priv_name like 'packs.searchandassign_inmo';
UPDATE admin_privs SET priv_name = 'Packs.searchAndAssignAuto' WHERE priv_name like 'packs.searchandassign_auto';
UPDATE admin_privs SET priv_name = 'Packs.updateProParam'      WHERE priv_name like 'packs.update_pro_param';
UPDATE admin_privs SET priv_name = 'Packs.partnerAdReport'     WHERE priv_name like 'packs.partner_ad_report';
