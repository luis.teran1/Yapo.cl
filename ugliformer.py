"""
    ugliformer.py
    Author: marcelo@schibsted.cl

    This script reads a JSON object from a cleaned up version of
    `gulp-js-dependencies.js`. The recovered structure is traversed to generate
    files named after each one of the `final` directives, containing the names
    of the js files that should be appended together to produce the `final` file.

    The output files of the `ugliformer` start with `ugli_`, followed by the name
    of the expected output file.

    Example:
    Input:

    [
        {
            "final": "ai.js",
            "sources": [
                "www/js/ai_src.js",
                "www/js/dummy_file.js"
            ]
        },
        {
            "final": "ugliformer.js",
            "sources": [
                "www/js/ugli.js",
                "www/js/former.js"
            ]
        }
    ]

    Output:

    File: ./ugli_ai.js
    Content:
        www/js/ai_src.js
        www/js/dummy_file.js

    File: ./ugli_ugliformer.js
    Content:
        www/js/ugli.js
        www/js/former.js

"""


import json


def make_unique(sources):
    """ Remove duplicates in a stable way """
    r = []
    already = set()
    for s in sources:
        if s not in already:
            r.append(s)
            already.add(s)
    return r


with open('uglify_files', 'r') as f:
    content = json.loads(f.read())

for l in content:
    unique_sources = make_unique(l.get('sources'))
    final = l.get('final')

    with open('ugli_{}'.format(final), 'w') as f:
        f.write('\n'.join(unique_sources))

