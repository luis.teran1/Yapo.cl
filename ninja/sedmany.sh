#!/bin/bash

BUILD_VERSION=`cat ninja_build/short_version`;
IFS=' ';
arr=( $1 );
rm -f $2;
for TMPFILE in "${arr[@]}"
do
	sed -e s.\#BUILD_VERSION\#.$BUILD_VERSION. < $TMPFILE >> $2;
done;
