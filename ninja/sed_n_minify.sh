#!/bin/bash

type=$3
BUILD_VERSION=`cat ninja_build/short_version`;
IFS=' ';
sed -e s.\#BUILD_VERSION\#.$BUILD_VERSION. < $1 > $2.tmp;
java -jar util/yuicompressor/yuicompressor-2.4.6.jar --charset iso-8859-1 --type $type -o $2 $2.tmp;
