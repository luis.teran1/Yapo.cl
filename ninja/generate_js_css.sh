in=$1
out=$2
tmp_file=$2.full
buildflavor=$3
type=$4

./ninja/concatmany.sh "$in" "$tmp_file"

if [ "$buildflavor" = "regress" ]; then
	./ninja/sedmany.sh "$tmp_file" $out
else
	./ninja/sed_n_minify.sh "$tmp_file" $out $type
fi

