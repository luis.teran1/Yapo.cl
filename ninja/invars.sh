#!/bin/sh

PLATFORMDIR=`/bin/pwd`/platform
source $PLATFORMDIR/scripts/build/invars.sh

SCHEMA="blocket_$(date +"%Y")"
NOW=`date +'%F %R'`
THIS_YEAR=`echo ${NOW} | cut -d '-' -f 1`
FUTURE_YEAR=`expr ${THIS_YEAR} + 5`
LATER=`echo ${NOW} | sed 's/${THIS_YEAR}/${FUTURE_YEAR}/g'`
USER=`id -un`
REGRESS_FAST_DIR=/dev/shm/regress-`id -un`


if [ "$buildflavor" != "regress" ]; then
	BDIR=/opt/blocket
	DESTDIR=/opt/blocket
	PGSQL_HOST=10.49.0.30
	USER=batch
else
	BDIR=`readlink -f ${TOPDIR}/ninja_build/regress`
	DESTDIR=`readlink -f ${TOPDIR}/regress_final`
	PGSQL_HOST=/dev/shm/regress-${USER}/pgsql0/data
	REGRESS_PHP_HOOKABLE_LOGGING=extension=php_hookable_logging.so
	REGRESS_DIR_T=${TOPDIR}/regress_final/logs
fi

echo REGRESS_DIR_T=${REGRESS_DIR_T}
echo DESTDIR=${DESTDIR}
echo BDIR=${BDIR}
echo NOW=${NOW}
echo LATER=${LATER}
echo USER=${USER}
echo USER_PASSWORD=`php -r "echo sha1(\"${USER}\");"`
REGRESS_HOSTNAME=`hostname | head -1`
REGRESS_HOST=${REGRESS_HOSTNAME}
REGRESS_HOST_ESCAPED=`printf "${REGRESS_HOSTNAME}" | sed 's/\./_/g'`
echo REGRESS_HOST_ESCAPED=${REGRESS_HOST_ESCAPED}
echo REGRESS_HOSTNAME=${REGRESS_HOSTNAME}
echo REGRESS_PGSQL_HOST=${PGSQL_HOST}
echo PGSQL_HOST=${PGSQL_HOST}
echo SCHEMA=${SCHEMA}

if [ "$buildflavor" = "regress" ]; then
PHP_ERROR_REPORTING=E_ALL
PHP_DISPLAY_ERRORS=On
PHP_TMP_DIR=/tmp
PHP_ALWAYS_POPULATE_RAW_POST_DATA=On
HTTPD_BID_PORT=${REGRESS_BIDPORT}
else
PHP_ERROR_REPORTING="E_ALL \& ~E_NOTICE"
PHP_DISPLAY_ERRORS=Off
PHP_TMP_DIR=/var/opt/blocket/tmp/php
PHP_ALWAYS_POPULATE_RAW_POST_DATA=Off
HTTPD_BID_PORT=${HTTPD_PORT}
fi

echo PHP_ERROR_REPORTING=${PHP_ERROR_REPORTING}
echo PHP_DISPLAY_ERRORS=${PHP_DISPLAY_ERRORS}
echo PHP_TMP_DIR=${PHP_TMP_DIR}
echo PHP_ALWAYS_POPULATE_RAW_POST_DATA=${PHP_ALWAYS_POPULATE_RAW_POST_DATA}
echo HTTPD_BID_PORT=${HTTPD_BID_PORT}

#genport "REGRESS_ZSEARCH_SEARCH_PORT" 53
#genport "REGRESS_ZSEARCH_CMD_PORT" 54
#genport "REGRESS_ZSEARCH_KEEPALIVE_PORT" 55

genport "REGRESS_FILTERD_JSON_PORT" 53
genport "REGRESS_FILTERD_CONTROLLER_PORT" 54

genport "REGRESS_JOSESEARCH_SEARCH_PORT" 56
genport "REGRESS_JOSESEARCH_CMD_PORT" 57
genport "REGRESS_JOSESEARCH_KEEPALIVE_PORT" 58

genport "REGRESS_DSEARCH_SEARCH_PORT" 62
genport "REGRESS_DSEARCH_CMD_PORT" 63
genport "REGRESS_DSEARCH_KEEPALIVE_PORT" 64

genport "REGRESS_MOD_IMAGE_PORT" 98
genport "REGRESS_PIXEL_PORT" 25
genport "REGRESS_MCPORT" 3

genport "REGRESS_HTTPS_ACCOUNT_PORT" 6
genport "REGRESS_HTTPS_PAYMENT_MOD_PORT" 97

genport "REGRESS_MAMA_MCPORT" 45

genport "REGRESS_REDISSTAT_PORT" 46
genport "REGRESS_REDISACC_PORT" 47
genport "REGRESS_REDISGRAVITY_PORT" 50
genport "REGRESS_REDISACCOUNTS_PORT" 55
genport "REGRESS_REDISSTATSLAVE_PORT" 61

genport "REGRESS_MAMA_PORT" 41
genport "REGRESS_MAMA_FPORT" 42
genport "REGRESS_PG_TCP_PORT" 73

genport "REGRESS_MOBILE_PORT" 60
genport "REGRESS_REDISEMAIL_PORT" 59
genport "REGRESS_REDISSESSION_PORT" 49
genport "REGRESS_REDISDASHBOARD_PORT" 19
genport "REGRESS_REDIS_CARDATA_PORT" 48
genport "REGRESS_REDISWORDSRANKING_PORT" 84
genport "REGRESS_REDISNEXTGEN_API_PORT" 86
genport "REGRESS_REDISPROMETHEUS_PORT" 79

#Haunter viewer port
genport "HAUNTER_VIEWER_PORT" 69

### XXX - https://scmcoord.com/gitlab/platform/merge_requests/473 ###
genport "REGRESS_HTTPS_CP_PORT" 13
genport "REGRESS_HTTPS_PORT" 11
genport "REGRESS_NEXTGEN_PORT" 17

genport "REGRESS_CREDITS_API_PORT" 24
genport "REGRESS_YAPOFACT_API_PORT" 10402
genport "REGRESS_BIFROST_API_PORT" 10202
genport "REGRESS_BIFROST_API_SPORT" 10204
genport "REGRESS_SMS_API_PORT" 10103
genport "REGRESS_WEBPAY_API_PORT" 10002
genport "REGRESS_SUBSCRIPTION_API_PORT" 28
genport "REGRESS_ADS_EVALUATOR_API_PORT" 10502

genport "REGRESS_BFF_PORT" 30001

#####################################################################

# # # # # # # # # # # # #
# Mock ports		#

genport MOCK_CONTROL_PORT 15
genport MOCK_MC_PORT 22
genport MOCK_NG_PORT 23
genport MOCK_CR_PORT 24
genport MOCK_FAV_CMD_PORT 25
genport MOCK_FAV_QUERY_PORT 26

# # # # # # # # # # # # #

echo REGRESS_COOKIE_DOMAIN=`hostname -d | head -1`

if [ "$buildflavor" == "regress" ]
 then
	echo WWW_HOST=${REGRESS_HOSTNAME}:${REGRESS_HTTPD_WORKER_PORT}
	echo WWW2_HOST=${REGRESS_HOSTNAME}:${REGRESS_HTTPS_PORT}
	echo WWW2_HTTP=${REGRESS_HOSTNAME}:${REGRESS_HTTPD_PORT}
elif [ "$buildflavor" == "prod" ]
then
	echo WWW_HOST=www.yapo.cl
	echo WWW2_HOST=www2.yapo.cl
	echo WWW2_HTTP=www2.yapo.cl
fi

echo REGRESS_SLD_HOSTNAME=`hostname | cut -d\. -f2-`#cache
echo REGRESSDIR=${TOPDIR}/regress/scratch
echo REGRESS_SCRATCH_DIR=${TOPDIR}/regress/scratch

###
echo KILLME_REGRESS_FINAL=${TOPDIR}/regress_final
###

echo SYSLOGHOOKSO=${BDIR}/modules/sysloghook.so
echo SYSLOGHOOK=env SYSLOGROOT=${KILLME_REGRESS_FINAL/logs} LD_PRELOAD="${SYSLOGHOOKSO}"
echo REGRESS_PHP_HOOKABLE_LOGGING=${REGRESS_PHP_HOOKABLE_LOGGING}

# Version to be used on .in files
echo STATIC_FILES_BUILD_VERSION=${STATIC_FILES_BUILD_VERSION}

echo PGVERSION=9.3
echo PGVERSION_MAJOR=9
echo PGVERSION_MINOR=3

if [ "$(type -t genport_services)" != "function" ]; then
	genport_services() {
	    local _p=$( expr \( $REGRESS_USER_ID - \( $REGRESS_USER_ID / 100 \) \* 100 \) \* 200 + 30000 + $2 )
	    setval "$1" "$_p"
	}
fi

genport_services "REGRESS_MS_ONECLICK_PORT" 2
