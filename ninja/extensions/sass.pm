package sass;

sub register {
    return ('sass', \&compile_sass);
}

sub compile_sass {
	my ($srcdir, $src, $ops, $basesrc) = @_;

	my $csssrc=$basesrc . ".css";
	my $destdir = "www/sass";

	main::add_target($ops, $csssrc, "sass_compile", [ $src ], "obj", $srcdir, undef, { ruledep => 1 });
	push @{$ops->{'conf'}}, $csssrc
}

1;
