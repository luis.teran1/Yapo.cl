#!/bin/bash

IFS=' ';
arr=( $1 );
rm -f $2;
for TMPFILE in "${arr[@]}"
do
	./scripts/build/custom_in_transformations.py --in_file $TMPFILE --out_file $2 ${@:3};
done;

