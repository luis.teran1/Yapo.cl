# DONT remove these lines, please!

 Use the following comments like a "remember" of a correct release.md
 Please, if you have to add database changes, REMEMBER:

 - Create the tables and insert the data in the correct order
 - Set the correct permission in the new tables and sequences
 - If you going to add a new column NOT NULL, and the table has data,
   create the column NULL, add the correct UPDATES to set the data and then
   alter the column again to set NOT NULL


# Microservices

Please udpate the following microservices: *None*


# BEFORE installing Yapo.cl RPMS

## release.pre.*



# AFTER installing Yapo.cl RPMS

## release.post.*



# HOTFIX

_This section should be used only to fix bugs and if the fix is easy to apply, here you can add extra instructions needed only for **PRODUCTION**_

Example: 
* We need to turnOff servipag for desktop version

  In bconf machine, edit the file: /opt/blocket/conf/bconf.txt.payment_common
  find and change the line: 
  `*.*.payment.desktop.servipag.enabled=1` 

  and make it look like:    
  `*.*.payment.desktop.servipag.enabled=0`

  Restart bconfd and www2/bapache services   
  CH40: `/etc/init.d/bconfd restart`       
  www2: `/etc/init.d/bapache restart`

**This changes should be validated with system team**

## Hotfix title

* Hotfix detail
