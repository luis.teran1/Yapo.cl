TOPDIR:=$(abspath ${CURDIR})
include ${TOPDIR}/mk/colors.mk
MOBILEDIR:=ext-modules/mobile-api
MSITEDIR:=msite


RPM_REPOSITORY_HOST=ch9
RPM_REPOSITORY_PATH=/opt/repo/yapo

PROD_INSTALLROOT=${TOPDIR}/dest/blocket-root/opt/blocket
REGRESS_INSTALLROOT=${TOPDIR}/regress_final

SUBDIR=regress

EXTRACLEAN+=cache*.mk regress_final dest dest-noarch rpm/i686 rpm/x86_64 rpm/noarch rpm/ia32e build .replay_test.out ninja_build

LOG_FILE=/tmp/${USER}-errors.log

BUILDPATH=ninja_build
include ${TOPDIR}/platform/mk/ninja.mk

build_for_tests:
	./compile.sh;
	${MAKE} build-only;
	${MAKE} gambi_regress_setup rc kill; sleep 1;

run_trans_tests:
	@${MAKE} -C daemons/trans rc rd rdo REGRESS_LOGERR_FILE=${LOG_FILE};

run_mobile_api_tests:
	@${MAKE} -C ext-modules/mobile-api/tests rc rd regress REGRESS_LOGERR_FILE=${LOG_FILE}

run_selenium_tests:
	make regress-final REGRESS_LOGERR_FILE=${LOG_FILE}

run_all_tests:
	@${MAKE} -C daemons/trans rc rd rdo REGRESS_LOGERR_FILE=${LOG_FILE};
	@${MAKE} -C daemons/trans rc; sleep 1
	@${MAKE} rc; sleep 1
	@${MAKE} -C ext-modules/mobile-api/tests rc rd regress REGRESS_LOGERR_FILE=${LOG_FILE}
	@${MAKE} rc; sleep 1

# Build util first since it builds tools needed to build the rest.
.PHONY: build templates
build:
	./compile.sh;
	${MAKE} build-only;
	${MAKE} gambi_regress_setup rc kill; sleep 1;
	${MAKE} run_all_tests;
	rm -f ${LOG_FILE}
	echo -n > ${LOG_FILE}
	make regress-final REGRESS_LOGERR_FILE=${LOG_FILE}

ifeq (${JUNITXML},1)
	if [ -s /tmp/${USER}-errors.log ]; then \
		echo "Failed tests:"; \
		cat /tmp/${USER}-errors.log; \
	fi

else
	if [ -s /tmp/${USER}-errors.log ]; then \
		echo "Failed tests:"; \
		cat /tmp/${USER}-errors.log; \
		exit 1; \
	fi; true
endif

build-only:
	make build-cache
	make depend in-file BUILD_STAGE=REGRESS
	make build-compile

build-cache:
	rm -f build/cache*.mk
	@make build/cache${GENPORTOFF}.mk
	@make build/cache50.mk GENPORTOFF=50

build-compile:
	make -j 2 all BUILD_STAGE=REGRESS

regress-final rf: gambi_regress_install regress-final-do
	cat ${TOPDIR}/regress/final/.selenium_development_main_result
	cat ${TOPDIR}/regress/final/.selenium_development_controlpanel_result

regress-install ri:
	@if [ ! -z "`ls 2>/dev/null ${TOPDIR}/.*.lock`" ] ; then echo "Lock file present. Please clean before install."; exit 1; fi
	@rm -rf ${REGRESS_FINALDIR} www/.stamp php/.stamp
	@mkdir -p ${REGRESS_FINALDIR}/logs
	make install BUILD_STAGE=REGRESS INSTALLROOT=${REGRESS_FINALDIR} DESTDIR=${TOPDIR}/regress_final

regress-final-do:
	echo "start regress-final ${CURDIR} $@ `date +%s`" >> ${TOPDIR}/build/build.timing
	env PERL5LIB="${TOPDIR}/modules/perl_bconf/blib/lib/:${TOPDIR}/modules/perl_bconf/blib/arch/auto/blocket/bconf" LD_PRELOAD=${SYSLOGHOOKSO} make -C regress/final rd rdo
	echo "end regress-final ${CURDIR} $@ `date +%s`" >> ${TOPDIR}/build/build.timing

ports-params:
	@echo -e "${REGRESS_HTTPD_WORKER_PORT}"
	@echo -e "${REGRESS_MOBILE_PORT}"
	@echo -e "${REGRESS_HTTPS_PORT}"
	@echo -e "${REGRESS_HTTPD_PORT}"
	@echo -e "${REGRESS_BCONFD_JSON_PORT}"
	@echo -e "${REGRESS_TRANS_PORT}"
	@echo -e "${REGRESS_TRANS_CPORT}"
	@echo -e "${REGRESS_MOD_IMAGE_PORT}"

regress-params:
	@echo -e "${BPURPLE}REGRESS_HOST${COLOR_OFF} = ${REGRESS_HOSTNAME} (used for the base_url's)"
	@echo -e REGRESS_BLOCKET_ID = "${REGRESS_BLOCKET_ID}" "(used as blocket_id for the regress-final step)"
	@echo -e ""
	@echo -e ${REGRESS_HTTPD_PORT} "- portnumber for the prefork server (php apps)"
	@echo -e ${REGRESS_HTTPS_PORT} "- portnumber for the ssl server (php admin apps)"
	@echo -e ${REGRESS_HTTPD_WORKER_PORT} "- portnumber for the threaded server (mod_blocket)"
	@echo -e ${REGRESS_DAVPORT} "- portnumber for the dav-host"
	@echo -e ${REGRESS_ASEARCH_SEARCH_PORT} "- portnumber for asearch"
	@echo -e ${REGRESS_ASEARCH_KEEPALIVE_PORT} "- keepalive port for asearch"
	@echo -e ${REGRESS_CSEARCH_SEARCH_PORT} "- portnumber for csearch"
	@echo -e ${REGRESS_MSEARCH_SEARCH_PORT} "- portnumber for msearch"
	@echo -e ${REGRESS_JOSESEARCH_SEARCH_PORT} "- portnumber for josesearch"
	@echo -e ${REGRESS_TRANS_PORT} "- portnumber for the transaction-server"
	@echo -e ${REGRESS_TRANS_CPORT} "- control portnumber for the transaction-server"
	@echo -e ${REGRESS_BCONFD_TRANS_PORT} "- portnumber for the bconf-server"
	@echo -e ${REGRESS_STATPOINTS_PORT} "- portnumber for the statpoints-server"
#	@echo -e ${REGRESS_PIXEL_PORT} "- portnumber for the pixel-server"
	@echo -e ${REGRESS_MCPORT} "- portnumber for memcached"
	@echo -e ""
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_WORKER_PORT}/index.htm" "- first-page"
# 	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_PORT}/paysim" "- paylog generator"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_MOBILE_PORT}/index.htm" "- mobile site"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPS_PORT}/controlpanel" "- controlpanel"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPS_PORT}/login" "- Account access"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_PORT}/doc" "- narcos tests doc"
#	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_PIXEL_PORT}/server-status" "- Hermes internal status info"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_WORKER_PORT}/selenium-core/core/TestRunner.html" "- Selenium test env"
	@echo -e "https://${REGRESS_HOSTNAME}:${HAUNTER_VIEWER_PORT}" "- Haunter viewer"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_PORT}/selenium-core/getMail.php?htmlmail=1" "- getMail"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/bconf" "- json formatted bconf from bconfd"
	@echo -e "https://${REGRESS_HOSTNAME}:${REGRESS_HTTPD_WORKER_PORT}/mailtester?mail=TEMPLATE_MAIL" "- Mail tmpl viewer"
	@echo -e ""
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_PORT}      - redis server (api)"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSTAT_PORT}      - redisstat server"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDIS_CARDATA_PORT}      - redis server (cardata)"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISEMAIL_PORT}      - redisemail"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSESSION_PORT}      - redissession"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISDASHBOARD_PORT}      - redisdashboard"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISACCOUNTS_PORT}      - redisaccounts"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISACCOUNTS_PORT} -n 2 - shopping cart"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT}      - redis_wordsranking"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} -n 1 - redis_banners"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} -n 2 - redis_ppages"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISNEXTGEN_API_PORT}      - redis nextgen api (poh perroh)"
	@echo -e "${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISPROMETHEUS_PORT}      - redis prometheus"
	@echo -e ""
	@echo -e "psql -h ${REGRESS_PGSQL_HOST} blocketdb"

regress-info rinfo:
	@make regress-params BUILD_STAGE=REGRESS

regress-depend rd:
	env LD_PRELOAD=${SYSLOGHOOKSO} make --quiet -C regress/final regress-depend

regress-clean rc:
	LD_PRELOAD=${SYSLOGHOOKSO} make --quiet -C regress/final regress-clean; \
	${MAKE} --quiet mock-stop

trans-install ti trans:
	${MAKE} --quiet gambi_misc INSTALLROOT=${TOPDIR}/regress_final BUILD_DIR=regress BUILD_FLAVOR=regress;
	make --quiet trans-regress-stop pgsql-update-procs trans-regress-start;

sendaemon-install sdi sendaemon:
	@${MAKE} --quiet ninja-compile FLAVOR=regress;
	@${MAKE} --quiet gambi_misc INSTALLROOT=${REGRESS_DIR} BUILD_DIR=regress BUILD_FLAVOR=regress;

search-install si:
	@${MAKE} --quiet ninja-compile FLAVOR=regress;
	@${MAKE} --quiet gambi_bin INSTALLROOT=${REGRESS_DIR} BUILD_DIR=regress;
	@${MAKE} --quiet asearch-regress-restart;

trans-reload:
	pkill -HUP -U ${USER} trans
	@echo Give trans some time to load the new bconf
	@sleep 2

#install-bconf ib:
#	#make -C conf install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
#	# Added by boris to add conf file from API to regress
#	make -C ${MOBILEDIR}/conf/ install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
#	# end !!
#	# Adding conf file from mobile web to regress
#	make -C ${MSITEDIR}/conf/ install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS

reload-bconf rb bconf bu:
	@${MAKE} --quiet ninja-compile FLAVOR=regress;
	@make --quiet gambi_conf INSTALLROOT=${REGRESS_INSTALLROOT} BUILD_DIR=regress BUILD_FLAVOR=regress ;
	@make --quiet bconfd-trans-regress-reload trans-regress-reload apache-regress-restart ;

enable-css-redirect:
	@env REGRESS_CSS_REDIRECT=true make --quiet apache-regress-stop apache-regress-start rb templates;

disable-css-redirect: apache-regress-stop apache-regress-start rb templates

enable-pp-redirect:
	@env REGRESS_JS_PP_REDIRECT=true make --quiet apache-regress-stop apache-regress-start rb templates;

disable-pp-redirect: apache-regress-stop apache-regress-start rb templates

ninja-compile:
	@rm -f ${TOPDIR}/ninja_build/build.ninja;
	./compile.sh ;

gambi_bin:
	mkdir -p ${INSTALLROOT}/bin;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/bin/* ${INSTALLROOT}/bin/;

gambi_conf:
	mkdir -p ${INSTALLROOT}/conf ;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/conf/* ${INSTALLROOT}/conf/ ;
	if [ -d ${TOPDIR}/ninja_build/${BUILD_DIR}/${BUILD_FLAVOR}/conf ]; then \
		rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/${BUILD_FLAVOR}/conf/* ${INSTALLROOT}/conf/ ; \
	fi;
	echo "DONE" ;

gambi_rw:
	@${MAKE} --quiet ninja-compile FLAVOR=${BUILD_FLAVOR};
	mkdir -p ${INSTALLROOT}/www-ssl ;
	shopt -s dotglob && rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/www-ssl/* ${INSTALLROOT}/www-ssl/ && shopt -u dotglob ;
	if [ -d ${TOPDIR}/ninja_build/${BUILD_DIR}/cgi-bin ]; then \
		mkdir -p ${INSTALLROOT}/cgi-bin ; \
		rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/cgi-bin/* ${INSTALLROOT}/cgi-bin/; \
	fi ;
	if [ -d ${TOPDIR}/ninja_build/${BUILD_DIR}/ts-cgi-bin ]; then \
		mkdir -p ${INSTALLROOT}/ts-cgi-bin ; \
		rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/ts-cgi-bin/* ${INSTALLROOT}/ts-cgi-bin/; \
	fi ;
	echo "DONE" ;

gambi_selenium:
	${MAKE} -C regress/final cst
	${MAKE} -C util/selenium-core install

gambi_modules:
	mkdir -p ${INSTALLROOT}/modules;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/modules/* ${INSTALLROOT}/modules/;

gambi_php:
	mkdir -p ${INSTALLROOT}/apps ;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/apps/* ${INSTALLROOT}/apps/ ;
	mkdir -p ${INSTALLROOT}/include;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/php_include/* ${INSTALLROOT}/include/;

gambi_misc:
	@${MAKE} --quiet ninja-compile FLAVOR=${BUILD_FLAVOR};
	make --quiet gambi_rw INSTALLROOT=${INSTALLROOT} BUILD_DIR=${BUILD_DIR} FLAVOR=${BUILD_FLAVOR};
	mkdir -p ${INSTALLROOT}/libexec;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/libexec/* ${INSTALLROOT}/libexec/;
	if [ -d ${TOPDIR}/ninja_build/obj/${BUILD_DIR}/tools/libexec ]; then \
		rsync -r ${TOPDIR}/ninja_build/obj/${BUILD_DIR}/tools/libexec/* ${INSTALLROOT}/libexec/; \
	fi;
	mkdir -p ${INSTALLROOT}/share;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/share/* ${INSTALLROOT}/share/;
	${MAKE} --quiet gambi_bin INSTALLROOT=${INSTALLROOT} BUILD_DIR=${BUILD_DIR};
	${MAKE} --quiet gambi_conf INSTALLROOT=${INSTALLROOT} BUILD_DIR=${BUILD_DIR} BUILD_FLAVOR=${BUILD_FLAVOR};
	mkdir -p ${INSTALLROOT}/lib/perl5/5.8.8;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/include_lib/* ${INSTALLROOT}/lib/;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/lib/perl5/search_index.{pm,so} ${INSTALLROOT}/lib/perl5/5.8.8
	rm ${TOPDIR}/ninja_build/${BUILD_DIR}/lib/perl5/search_index.pm
	rsync -r --exclude="*_pic.o" --exclude="*.a" --exclude="*.so" ${TOPDIR}/ninja_build/${BUILD_DIR}/lib/* ${INSTALLROOT}/lib/;
	mkdir -p ${INSTALLROOT}/include;
	rsync -r --exclude="*.h" ${TOPDIR}/ninja_build/${BUILD_DIR}/include/* ${INSTALLROOT}/include/;
	${MAKE} --quiet gambi_modules INSTALLROOT=${INSTALLROOT} BUILD_DIR=${BUILD_DIR};
	if [ -d ${TOPDIR}/ninja_build/${BUILD_DIR}/etc ]; then \
		mkdir -p ${INSTALLROOT}/etc; \
		rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/etc/* ${INSTALLROOT}/etc/; \
	fi;
	if [ -d ${TOPDIR}/ninja_build/${BUILD_DIR}/sysadmin ]; then \
		mkdir -p ${INSTALLROOT}/sysadmin; \
		rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/sysadmin/* ${INSTALLROOT}/sysadmin/; \
	fi;
	${MAKE} --quiet gambi_php INSTALLROOT=${INSTALLROOT} BUILD_DIR=${BUILD_DIR};
	# *search init scripts get an special treatment, since they are generated *somewhere* else
	mkdir -p ${INSTALLROOT}/etc/init.d;
	cp ${TOPDIR}/platform/scripts/init/search ${INSTALLROOT}/etc/init.d/asearch;
	cp ${TOPDIR}/platform/scripts/init/search ${INSTALLROOT}/etc/init.d/csearch;
	cp ${TOPDIR}/platform/scripts/init/search ${INSTALLROOT}/etc/init.d/msearch;
	cp ${TOPDIR}/platform/scripts/init/search ${INSTALLROOT}/etc/init.d/josesearch;
	mkdir -p ${INSTALLROOT}/tmp;
	mkdir -p ${INSTALLROOT}/scripts;
	rsync -r ${TOPDIR}/ninja_build/${BUILD_DIR}/scripts/* ${INSTALLROOT}/scripts/;
	echo "DONE";

reinstall-php rp:
	@${MAKE} --quiet ninja-compile FLAVOR=regress;
	@${MAKE} --quiet gambi_php INSTALLROOT=${REGRESS_INSTALLROOT} BUILD_DIR=regress;

rp-%:
	make -C php/$(subst rp-,,$@) install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS

reinstall-util ru:
	make -C util install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS

reinstall-www-no-min rw-no-min rw:
	make --quiet gambi_rw INSTALLROOT=${REGRESS_INSTALLROOT} BUILD_DIR=regress BUILD_FLAVOR=regress
	${MAKE} gulp-regress

#Build css
bc bs:
	bash frontend.sh sass -e regress -p desktop

bs-min:
	bash frontend.sh sass -s -p desktop

#Build javascript
bj:
	bash frontend.sh js -e regress -p desktop
	bash frontend.sh webpack -e regress

bj-min:
	bash frontend.sh js -s -p desktop

#Build mobile css
mbc mbs:
	bash frontend.sh sass -e regress -p mobile

mbs-min:
	bash frontend.sh sass -s -p mobile

#Build mobile javascript
mbj:
	bash frontend.sh js -e regress -p mobile

mbj-min:
	bash frontend.sh js -s -p mobile

gulp-%:
	@make --quiet PROD_VARIATION=$(subst gulp-,,$@) inner_gulp;

inner_gulp:
	@${MAKE} --quiet ninja-compile FLAVOR=${PROD_VARIATION};
	@# Copy configuration from ninja_build
	@if [ ! -f www/gulp-config.js ] ; \
	then \
		cp ninja_build/${PROD_VARIATION}/conf/gulp-config.js www/gulp-config.js; \
	fi;
	. artifactory-npm-env.sh && yarn install --no-progress --ignore-engines
	@if [ "${PROD_VARIATION}" = "regress" ]; \
	then \
		bash frontend.sh dev; . artifactory-npm-env.sh && yarn gulp;\
	else \
		bash frontend.sh deploy && . artifactory-npm-env.sh && yarn gulp;\
	fi;

gulp-watch:
	. artifactory-npm-env.sh && yarn gulp watch
	${MAKE} -C msite gulp-watch

reinstall-modules remod:
	for m in php_templates mod_list mod_templates; do \
		make -C modules/$$m install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS; \
	done
	${MAKE} apache-regress-restart

rept reml remt templates:
	@FLAVOR=regress ./compile.sh; \
	${MAKE} --quiet gambi_modules INSTALLROOT=${REGRESS_INSTALLROOT} BUILD_DIR=regress; \
	${MAKE} --quiet apache-regress-restart;

mod-stats:
	make -C platform/modules/mod_stats install DESTDIR=${REGRESS_FINALDIR} INSTALLROOT=${TOPDIR}/regress_final BUILD_STAGE=REGRESS
	${MAKE} apache-regress-stop apache-regress-start

so:
	@echo "sorry, no make so, use make rept (php-templates), reml (mod_list) or remt (mod_templates)"

ss:
	make -C regress/final show-session

sov soverbose:
	make -C templates so-files SOVERBOSE=1

senv:
	make -C regress/final selenium-environment

load-many-stores:
	make db-load-accounts
	@./util/many_stores.sh 100 ${REGRESS_TRANS_PORT}
	make rebuild-asearch rebuild-csearch

nubox-process:
	@echo -e "${BPURPLE}Procesing bills:${COLOR_OFF}"
	@/usr/bin/php -c ${REGRESS_FINALDIR}/conf/php.ini ${REGRESS_FINALDIR}/bin/bill_processing.php echo
	@echo -e  "${BPURPLE}Sending Emails:${COLOR_OFF}"
	@/usr/bin/php -c ${REGRESS_FINALDIR}/conf/php.ini ${REGRESS_FINALDIR}/bin/bill_send_email.php echo

pgdump-to-%:
	pg_dump -a --insert --column-inserts --no-owner -h /dev/shm/regress-$(USER)/pgsql0/data blocketdb > pg_dump_$(subst pgdump-to-,,$@).pgsql

# getpaid:
# 	cd ${REGRESS_FINALDIR}/bin; env PERL5LIB="${TOPDIR}/regress_final/${PERL_INSTALL}" ./getpaid.pl --configfile=../conf/bconf.conf < ${TOPDIR}/regress_final/logs/paysim.log

docs:
	@mkdir -p regress_final/www/docs
	doxygen

ifndef DESTDIR
CLEAN_DEST=rm -rf
else
CLEAN_DEST=echo
endif

DESTDIR?=$(shell echo `/bin/pwd`/dest)

dist: build dist.tar.gz

rpm::
	@if [ ! -z "`ls 2>/dev/null ${TOPDIR}/.*.lock`" ] ; then echo "Lock file present. Please clean before rpm."; exit 1; fi
	make DESTDIR=/opt/blocket build;
	@${MAKE} --quiet rc kill; sleep 1
	@${MAKE} rpm-build
	@if [ ! -z scripts/build/timing.pl ] ; then scripts/build/timing.pl < build/build.timing ; fi
	-cat ${TOPDIR}/regress/final/.selenium_development_main_result
	-cat ${TOPDIR}/regress/final/.selenium_development_controlpanel_result

#rpm-build: rpmclean
#	@if [ ! -z "`ls 2>/dev/null ${TOPDIR}/.*.lock`" ] ; then echo "Lock file present. Please clean before install."; exit 1; fi
#	rpmbuild --quiet --nobuild --rcfile rpm/rpmrc rpm/blocket.spec 2>&1 | grep error; if [ $$? == 0 ] ; then exit 1; fi
#	rpmbuild --quiet --nobuild --rcfile rpm/rpmrc rpm/noarch.spec 2>&1 | grep error; if [ $$? == 0 ] ; then exit 1; fi
#	rpmbuild -bb --rcfile rpm/rpmrc rpm/blocket.spec
#	rpmbuild -bb --rcfile rpm/rpmrc rpm/noarch.spec
#	@echo RPMs built.

rpm-build: rpm-build-prod

rpm-build-%: rpmclean
	@make --quiet PROD_VARIATION=$(subst rpm-build-,,$@) inner_rpm-build ;

inner_rpm-build:
	@if [ ! -z "`ls 2>/dev/null ${TOPDIR}/.*.lock`" ] ; then echo "Lock file present. Please clean before install."; exit 1; fi
	#rm -fr ${TOPDIR}/ninja_build;
	rm -fr ${TOPDIR}/dest/blocket-root;
	rm -fr ${TOPDIR}/rpm/x86_64;
	rm -fr ${TOPDIR}/rpm/noarch;
	@${MAKE} gulp-${PROD_VARIATION}
	rpmbuild --quiet --nobuild --rcfile rpm/rpmrc rpm/blocket.spec 2>&1 | grep error; if [ $$? == 0 ] ; then exit 1; fi
	rpmbuild --quiet --nobuild --rcfile rpm/rpmrc rpm/noarch.spec 2>&1 | grep error; if [ $$? == 0 ] ; then exit 1; fi
	rpmbuild -bb --buildroot=${TOPDIR}/dest/blocket-root --macros=/usr/lib/rpm/macros:${TOPDIR}/rpm/rpmmacros --rcfile ${TOPDIR}/rpm/rpmrc  --define 'prod_variation ${PROD_VARIATION}' rpm/blocket.spec
	rpmbuild -bb --buildroot=${TOPDIR}/dest/blocket-root --macros=/usr/lib/rpm/macros:${TOPDIR}/rpm/rpmmacros --rcfile ${TOPDIR}/rpm/rpmrc rpm/noarch.spec
	@echo RPMs built.

ninja-%:
	@make --quiet gambi_misc INSTALLROOT=${INSTALLROOT} BUILD_DIR=$(subst ninja-,,$@) BUILD_FLAVOR=$(subst ninja-,,$@);

rpm-deploy: rpm-build
	@echo -e "${BPURPLE}+++ Sending RPM's to the production RPM repository${COLOR_OFF}"
	@scp -r rpm/ia32e rpm/noarch $(RPM_REPOSITORY_HOST):$(RPM_REPOSITORY_PATH)
	@echo -e "${BPURPLE}+++ Regenerating the repository packages list${COLOR_OFF}"
	@ssh root@$(RPM_REPOSITORY_HOST) "createrepo $(RPM_REPOSITORY_PATH)"
	@echo -e "${BPURPLE}+++ RPM deploy in repository succesfuly!${COLOR_OFF}"

selenium:
	make rc
	make ri
	make rd
	make -C regress/final rs-load
	make -C regress/final rdo REGRESS_SKIPTO=selenium-start

gambi_regress_setup: gambi_regress_install
	${MAKE} --quiet rd;
	${MAKE} --quiet redis_banner_nga;

gambi_regress_install:
	${MAKE} --quiet rc kill ri;
	${MAKE} --quiet gambi_selenium INSTALLROOT=${TOPDIR}/regress_final BUILD_DIR=regress;
	${MAKE} --quiet gambi_misc INSTALLROOT=${TOPDIR}/regress_final BUILD_DIR=regress BUILD_FLAVOR=regress;

rall:
	@echo "Compiling, killing and starting...";
	forever stopall;
	${MAKE} --quiet gambi_regress_setup;
	${MAKE} --quiet regress-admins;
	${MAKE} --quiet mock-start;
	${MAKE} gulp-regress;
	. artifactory-npm-env.sh && yarn gulp sprites;

start-pact-mock:
	${MAKE} --quiet -C pactMock/ pact-download updatepacts start;

stop-pact-mock:
	${MAKE} --quiet -C pactMock/ stop;

redis-start-all:
	@for server in stat email session dashboard accounts prometheus; do \
		make redis-start-$$server; \
	done
	make redis-load-cardata
	make redis-load-wordsranking
	make load-redisstat-scripts

redis-stop-all:
	@for server in stat email session dashboard cardata accounts prometheus wordsranking; do \
		make redis-stop-$$server; \
	done

redissession-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSESSION_PORT} $(subst redissession-,,$@)

redisaccounts-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISSESSION_PORT} $(subst redisaccounts-,,$@)

redis_banners-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} -n 1 $(subst redis_wordsranking-,,$@)

redis_ppages-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} -n 2 $(subst redis_wordsranking-,,$@)

redis_wordsranking-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} $(subst redis_wordsranking-,,$@)

redisnextgen_api-%:
	# Executes a redis command. See redis.io/commands for all available commands
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISNEXTGEN_API_PORT} $(subst redisnextgen_api-,,$@)

redis_banner_nga:
	@${REGRESS_REDIS_CLIENT} -p ${REGRESS_REDISWORDSRANKING_PORT} -n 1 "hset" "banner_nga_global" "enabled" "1"


clean_ipc:
	@echo -n " * Cleaning semaphores..."; \
	ipcs -s | awk '/[0-9]/ { print $$2}' | \
	while read a; do ipcrm -s $$a; done; \
	echo " ok";

regresstest rt:
	make rc
	make ri
	make rd
	make -C regress/final rdo

rpmclean:
	rm -f rpm/*/*.rpm

kill-%:
	@for (( n=1; n <= $(subst kill-,,$@); n++ )); do \
		make kill; \
	done

VALGRIND_PROCS=$(shell ps wux | grep valgrind | grep -v grep | sed -e "s/ \+/ /g" | cut -d " " -f 2)
REDIS_PROCS=$(shell ps hux | grep "redis" | grep -v "grep" | awk '{print $$2}')
kill:
	@killall -q -u $(USER) postmaster; exit 0;
	@killall -q -u $(USER) trans; exit 0;
	@killall -q -u $(USER) sendaemon; exit 0;
	@killall -q -u $(USER) epmd; exit 0;
	@killall -q -u $(USER) beam.smp; exit 0;
	@killall -q -u $(USER) memcached; exit 0;
	@killall -q -u $(USER) httpd; exit 0;
	@killall -q -u $(USER) httpd.worker; exit 0;
	@killall -q -u $(USER) search; exit 0;
	@killall -q -u $(USER) statpoints; exit 0;
	@killall -q -u $(USER) Xvnc; exit 0;
	@killall -q -u $(USER) redis-server; exit 0;
	@killall -q -u $(USER) bconfd; exit 0;
	@killall -q -u $(USER) openssl; exit 0;
	@if [ ! -z "${VALGRIND_PROCS}" ]; then \
		kill ${VALGRIND_PROCS}; \
	fi;
	@if [ ! -z "${REDIS_PROCS}" ]; then \
		kill -2 ${REDIS_PROCS}; \
	fi;
	@rm -f $(TOPDIR)/.*pid; exit 0
	@rm -f $(TOPDIR)/.*.lock; exit 0
	@make --quiet clean_ipc

help:
	@echo -e "${ON_IRED}${BYELLOW}Yapo.cl make targets${COLOR_OFF}"
	@echo -e "make ${BPURPLE}ninja-compile${COLOR_OFF} (builds the site)"
	@echo -e "make ${BPURPLE}rall${COLOR_OFF} (builds & brings up regress environment)"
	@echo -e "make ${BPURPLE}ga${COLOR_OFF} (set up 1000 ads and rebuild index)"
	@echo -e "make ${BPURPLE}rp${COLOR_OFF} (reinstalls php & gracefuls apache)"
	@echo -e "make ${BPURPLE}rb${COLOR_OFF} (reinstalls & reloads bconf)"
	@echo -e "make ${BPURPLE}rw${COLOR_OFF} (reinstalls www & gracefuls apache)"
	@echo -e "make ${BPURPLE}rw-no-min${COLOR_OFF} (reinstalls www using unminified js/css & gracefuls apache)"
	@echo -e "make ${BPURPLE}remt${COLOR_OFF} (rebuilds, reinstalls templates compiled against mod_template & gracefuls apache)"
	@echo -e "make ${BPURPLE}rept${COLOR_OFF} (rebuilds, reinstalls templates compiled against php_template & gracefuls apache)"
	@echo -e "make ${BPURPLE}reml${COLOR_OFF} (rebuilds, reinstalls templates compiled against mod_list & gracefuls apache)"
	@echo -e "make ${BPURPLE}rc${COLOR_OFF} (bring down regress environment)"
	@echo -e "make ${BPURPLE}apache-regress-stop${COLOR_OFF} (brings down apache)"
	@echo -e "make ${BPURPLE}apache-regress-start${COLOR_OFF} (bring up apache)"
	@echo -e "make ${BPURPLE}trans-regress-stop${COLOR_OFF} (bring down trans)"
	@echo -e "make ${BPURPLE}trans-regress-start${COLOR_OFF} (bring up trans)"
	@echo -e "make ${BPURPLE}rabbitmq-regress-stop${COLOR_OFF} (bring down rabbitmq)"
	@echo -e "make ${BPURPLE}rabbitmq-regress-start${COLOR_OFF} (bring up rabbitmq)"
	@echo -e "make ${BPURPLE}asearch-regress-stop${COLOR_OFF} (bring down search engine)"
	@echo -e "make ${BPURPLE}asearch-regress-start${COLOR_OFF} (bring up search engine)"
	@echo -e "make ${BCYAN}rebuild-index${COLOR_OFF} (rebuild indexes. Incremental index for asearch)"
	@echo -e "make ${BCYAN}rebuild-index-full${COLOR_OFF} (rebuild indexes. Full index for asearch)"
	@echo -e "make ${BCYAN}rebuild-msearch${COLOR_OFF} (rebuild the msearch for street suggestions)"
	@echo -e "make ${BRED}rpm-build${COLOR_OFF} (does no tests, creates prod rpms)"
	@echo -e "make ${BRED}rpm-build-(prod)${COLOR_OFF} (does no tests, creates rpms for the given environment)"
	@echo -e "make ${BRED}build${COLOR_OFF}  (does all tests)"
	@echo -e "make ${BRED}rpm${COLOR_OFF}  (does all tests, creates rpms)"
	@echo -e "make ${BBLUE}rinfo${COLOR_OFF} (print your regress environment URLs and paths)"
	@echo -e "make ${BBLUE}clean${COLOR_OFF} (removes all .o & .so files)"
	@echo -e "make ${BBLUE}cleandir${COLOR_OFF} (removes everything in regress_final directory)"
	@echo -e "make ${BBLUE}kill${COLOR_OFF} (attempts to kill al regress processes)"
	@echo -e "make ${BBLUE}kill-n${COLOR_OFF} (attempts to kill all regress processes 'n' times)"
	@echo -e "make ${BBLUE}mail-stats${COLOR_OFF} (run daily_stats script and sends daily stats mail with yesterday stats)"
	@echo -e "make ${BBLUE}id${COLOR_OFF} (insert 1030 ads, admins and adcodes in the db and reindex)"
	@echo -e "make ${BGREEN}redis-start-service-name${COLOR_OFF} (brings up redis service of service-name or all of them with *all*)"
	@echo -e "make ${BGREEN}redis-stop-service-name${COLOR_OFF} (brings down redis service of service-name or all of them with *all*)"
	@echo -e "make ${BGREEN}redis-load-cardata${COLOR_OFF} (loads cardata and brings service up)"
	@echo -e "make ${BGREEN}redis-load-wordsranking${COLOR_OFF} (loads redis wordsranking and brings the service up)"
	@echo -e "make ${BGREEN}redissession-{command}${COLOR_OFF} (Executes a redis command. See redis.io/commands for all available commands)"
	@echo -e "make ${BCYAN}sendmail-restore${COLOR_OFF} (Allow regress to send real mails)"
	@echo -e "make ${BCYAN}sendmail-replace${COLOR_OFF} (Deny regress to send real mails  sending content to .mail.txt)"
	@echo -e "make ${BPURPLE}nubox-process${COLOR_OFF} (Send the payments to nubox simulator)"
	@echo -e "make ${BPURPLE}pgdump-to-${BGREEN}filename${COLOR_OFF} (Create a pg_dump file from your actual blocketdb into filename)"
	@echo -e "make ${BRED}load-many-stores${COLOR_OFF}  (Create 100 stores - without ads)"
	@echo -e "make ${BRED}create-pack-stats${COLOR_OFF}  (Create random data about visits per ad and Calculate the average per ads storing the data in redis (make calculate-pack-stats))"
	@echo -e "make ${BRED}calculate-pack-stats${COLOR_OFF}  (Calculate the average visits per ads and store it in redis to display the average visists graph in pack user's dashboard)"
	@echo -e "make ${BRED}create-mail_queue_data${COLOR_OFF}  (Create data for mail_queue table, adding random ad_reply)"
	@echo -e "${ON_IRED}${BYELLOW}make leninade (https://en.wikipedia.org/wiki/Leninade)${COLOR_OFF}"
##	PIXEL STUFF
# 	@echo -e "make ${BBLUE}pi${COLOR_OFF} (brings up pixel server)"
# 	@echo -e "make ${BBLUE}pixel-dumpclosed${COLOR_OFF} (dumping closed visits to db)"
# 	@echo -e "make ${BBLUE}pixel-dumpall${COLOR_OFF} (dumping all visits to db)"
# 	@echo -e "make ${BBLUE}pixel-replay_log${COLOR_OFF} (run an accuracy test on Hermes)"
##	MAMA STUFF
#	@echo -e "make ${BBLUE}benders-start${COLOR_OFF} (start nonhumans reviewers)"
#	@echo -e "make ${BBLUE}benders-stop${COLOR_OFF} (stop nonhumans reviewers)"
#	@echo -e "make ${BBLUE}bender${COLOR_OFF} (launch nonhuman reviewer)"
leninade:
	@echo -e "${ON_IRED}${BYELLOW}`cat .leninade`${COLOR_OFF}"

initpackcars:
	make -C ${TOPDIR} db-load-accounts db-load-diff_pack_cars1 db-snap-packauto db-restore-packauto rebuild-asearch redis-flushall
initpackinmo:
	make -C ${TOPDIR} db-load-accounts db-load-diff_pack_inmo1 db-snap-packinmo db-restore-packinmo rebuild-asearch redis-flushall
restorepackcars:
	make -C ${TOPDIR} db-restore-packauto rebuild-asearch redis-flushall
restorepackinmo:
	make -C ${TOPDIR} db-restore-packinmo rebuild-asearch redis-flushall

cleanlogs:
	@for d in ${TOPDIR}/regress_final/logs; do \
		for f in $$d/*; do \
			if [ -f $$f ]; then \
				> $$f; \
			fi; \
		done; \
	done;

# YESTERDAY=$(shell date +"%Y-%m-%d" -d "yesterday")
# mail-stats:
# 	( \
# 		env PERL5LIB="${REGRESS_FINALDIR}/lib/perl5" ${TOPDIR}/regress_final/bin/daily_stats.pl -configfile ${TOPDIR}/regress_final/conf/bconf.conf -rundate ${YESTERDAY} ${TOPDIR}/regress_final/logs/pixel_debug.log \
# 		&& printf 'cmd:mail_stats\ncommit:1\nday:${YESTERDAY}\nend\n' | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT} \
# 	)

# PIXEL_DUMP_PID=$(shell ps wux | grep "pixel.*--config" | grep -v grep | sed -e "s/ \+/ /g" | cut -d" " -f2 | tail -1)
# pixel-dump%:
# 	@DUMP="$(subst pixel-dump,,$@)"; \
# 	if [ "$$DUMP" == "closed" ]; \
# 		then \
# 		echo "$$DUMP received going to dump closed visits"; \
#         kill -14  ${PIXEL_DUMP_PID}; \
# 	elif [ "$$DUMP" == "all" ]; \
# 		then \
# 		echo "$$DUMP received going to dump closed visits"; \
#         kill -10  ${PIXEL_DUMP_PID}; \
# 	fi;

insert-data id:
	@make db-snap-foo db-load-1030ads regress-admins regress-adcodes rebuild-index-full rim

ns-%:
	printf 'cmd:newad\nregion:15\npasswd:$$1024$$j4jmSAY6cMUmA0ig8d830c8e2785dc4e3588b85d5a7b5b31ff714b82\nname:Automated Test User\nemail:autotest@yapo.cl\nphone:17471666\nphone_hidden:1\ncompany_ad:0\nsubject:Test ad.\nbody:Created using a custom infile generator.\nprice:4711\nlang:es\ncommit:1\ncategory:8020\ntype:s\nexternal_ad_id:$(subst ns-,,$@)\nlink_type:whatever\nend\n' | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT}

insert-random-%:
	@echo -e "Inserting ${BPURPLE}$(subst insert-random-,,$@)${COLOR_OFF} ads per region and category, please wait..."
	@php -c ${TOPDIR}/build/conf/php.ini ${TOPDIR}/util/autogen/create_ads_per_reg_cat.php -l $(subst insert-random-,,$@)
	@echo -e "${BPURPLE}Finished${COLOR_OFF}, redistributing images and rebuilding index"
	@make rim rebuild-index
	@echo -e "${BPURPLE}Insertion successful.${COLOR_OFF}"

insert-randomreq-%:
	@echo -e "Inserting ${BPURPLE}$(subst insert-randomreq-,,$@)${COLOR_OFF} ads per region and category, please wait..."
	@php -c ${TOPDIR}/build/conf/php.ini ${TOPDIR}/util/autogen/create_ads_per_reg_cat.php -l $(subst insert-randomreq-,,$@) -r
	@echo -e "${BPURPLE}Finished${COLOR_OFF}, redistributing images and rebuilding index"
	@make rim rebuild-index
	@echo -e "${BPURPLE}Insertion successful.${COLOR_OFF}"

insert-queued-%:
	@echo -e "Inserting ${BPURPLE}$(subst insert-random-,,$@)${COLOR_OFF} random ads TO QUEUE, please wait..."
	@php -c ${TOPDIR}/build/conf/php.ini ${TOPDIR}/util/autogen/queue_random_ad.php -l $(subst insert-queued-,,$@) -n -v
	@echo -e "${BPURPLE}Insertion successful.${COLOR_OFF}"

enable-payment pay payon:
	${MAKE} -s bconf-overwrite-*.*.payment.enabled:1
	@make apache-regress-restart
disable-payment notpay payoff:
	${MAKE} -s bconf-overwrite-*.*.payment.enabled:0
	@make apache-regress-restart

webpay-plus-enable:
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.payment.desktop.webpay_plus.enabled&value=1' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.payment.msite.webpay_plus.enabled&value=1' -o /dev/null)" = 200
	${MAKE} apache-regress-graceful

webpay-plus-disable:
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.payment.desktop.webpay_plus.enabled&value=0' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.payment.msite.webpay_plus.enabled&value=0' -o /dev/null)" = 200
	${MAKE} apache-regress-graceful

webpay-plus-use-api:
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.host&value=${REGRESS_HOSTNAME}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.port&value=${REGRESS_WEBPAY_API_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.startPayment.path&value=/startPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.verifyPayment.path&value=/verifyPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.nullifyPayment.path&value=/nullifyPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.completePayment.path&value=/completePayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.healthCheck.path&value=/healthCheck' -o /dev/null)" = 200
	${MAKE} apache-regress-graceful webpay-plus-bconf-info

webpay-plus-use-regress:
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.host&value=https://${REGRESS_HOSTNAME}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.port&value=${REGRESS_HTTPD_PORT}' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.startPayment.path&value=/payment_rest/TyrionStartPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.verifyPayment.path&value=/payment_rest/TyrionVerifyPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.nullifyPayment.path&value=/payment_rest/TyrionNullifyPayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.completePayment.path&value=/payment_rest/TyrionCompletePayment' -o /dev/null)" = 200
	test "$$(curl -s --write-out "%{http_code}" http://${REGRESS_HOSTNAME}:${REGRESS_BCONFD_JSON_PORT}/'bconf_overwrite?key=*.*.webpay_service.healthCheck.path&value=/payment_rest/TyrionHealthCheck' -o /dev/null)" = 200
	${MAKE} apache-regress-graceful webpay-plus-bconf-info

webpay-plus-bconf-info:
	printf "cmd:bconf\ncommit:1\nend\n"|nc ${REGRESS_HOSTNAME} ${REGRESS_BCONFD_TRANS_PORT}|grep webpay_service;

send-bill:
	/usr/bin/php -c regress_final/conf/php.ini regress_final/bin/bill_processing.php echo

send-bill-email:
	/usr/bin/php -c regress_final/conf/php.ini regress_final/bin/bill_send_email.php echo

flushmail fm: flush-queue-accepted_mail
flush-delete-queue: flush-queue-deletead

flush-queue-%:
	printf "cmd:flushqueue\nqueue:$*\ncommit:1\nend\n" | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT};

flush-autobump:
	printf "cmd:flush_autobump\ncommit:1\nend\n" | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT};

redis-flushall:
	for port in ${REGRESS_REDISSTAT_PORT} ${REGRESS_REDISSESSION_PORT} ${REGRESS_REDISACCOUNTS_PORT} ${REGRESS_REDISWORDSRANKING_PORT} ${REGRESS_REDISNEXTGEN_API_PORT}; do \
		${REGRESS_REDIS_CLIENT} -p $$port FLUSHALL; \
	done;

create-pack-stats:
	@echo "SELECT bpv.create_regress_stats_structure();" | psql -h ${REGRESS_PGSQL_HOST} blocketdb
	@echo "SELECT bpv.create_regress_stats_data();" | psql -h ${REGRESS_PGSQL_HOST} blocketdb
	@${MAKE} calculate-pack-stats

calculate-pack-stats:
	@for i in $$(seq 1 29); do \
		DATE=$$(date +%Y-%m-%d -d "$$i day ago"); \
		echo "calculating stats for $$DATE"; \
		printf "cmd:get_pack_daily_stats\npack_type:cars\ndate:$$DATE\ncommit:1\nend\n" | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT} > /dev/null; \
		printf "cmd:get_pack_daily_stats\npack_type:inmorent\ndate:$$DATE\ncommit:1\nend\n" | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT} > /dev/null; \
		printf "cmd:get_pack_daily_stats\npack_type:inmosell\ndate:$$DATE\ncommit:1\nend\n" | nc ${REGRESS_HOSTNAME} ${REGRESS_TRANS_PORT} > /dev/null; \
	done;

create-mail_queue_data:
	@echo "SELECT bpv.create_regress_mail_queue_data();" | psql -h ${REGRESS_PGSQL_HOST} blocketdb

mock-%:
	${MAKE} -C mock $(subst mock-,,$@)

merken-regress-on:
	@sed  -i 's/merken\.status=0/merken\.status=1/' conf/regress/bconf/bconf.txt.site.in
	@make rb

merken-regress-off:
	@sed  -i 's/merken\.status=1/merken\.status=0/' conf/regress/bconf/bconf.txt.site.in
	@make rb

# Random shit
coffee:
	@cat .coffee

show-rabbit-queues:
	ninja_build/regress/bin/regress-rabbitmq rabbitmqctl list_queues

include $(TOPDIR)/mk/subdir.mk
include $(TOPDIR)/mk/regressinit.mk
include $(TOPDIR)/mk/hudson.mk
include ${TOPDIR}/mk/svn.mk
