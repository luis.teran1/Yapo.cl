#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>

#include "ctemplates.h"
#include "transapi.h"
#include "config.h"
#include "bsapi.h"
#include "bconf.h"
#include "strl.h"

/*
 *  Startup functions
 */


static int
setup_bconf(struct bconf_node **conf) {
	transaction_t *trans;
	char *key, *value, *ptr;
	int res;

	/* Now talk with the transaction server to get the bconf-info */
	trans = trans_init(10000);
	trans_add_pair(trans, "cmd", "bconf", strlen("bconf"));
	trans_add_pair(trans, "host", "www1", strlen("www1"));
	trans_add_pair(trans, "commit", "1", strlen("commit"));
	res = trans_commit(trans, "localhost", 56565, NULL);

	if (res == 0) {
		while(trans_iter_next(trans, &key, &value)) {
			ptr = strchr(value, '=');
			
			if (ptr) {
				*ptr++ = '\0';
				
				if (value + 2 < ptr)
					bconf_add_data(conf, value + 2, ptr);
			}
		}
	}

	trans_free(trans);
	return 0;
}

int
main(int argc, char **argv) {
	int cnt;
	int templates = 0;
	char *ptr;
	struct bconf_node *bconf = NULL;
	struct bpapi bparse;

	setup_bconf(&bconf);
	
	BPAPI_INIT(&bparse, bconf, hash);

	/* Add key-value pairs from command line */
	for (cnt = 1 ; cnt < argc ; cnt++) {
		if ( (ptr = strchr(argv[cnt], '=')) ) {
			*ptr = '\0';
			bparse.insert(&bparse, argv[cnt], ptr + 1);
			*ptr = '=';
		} else {
			templates++;
		}
	}

	if (templates == 0) {
		printf("Usage: %s [key=value key1=value1 ...] template [template1 template2 ...] \n", argv[0]);
		exit(0);
	}
	
	bparse.opaque = "ZAKAY";
	
	for (cnt = 1 ; cnt < argc ; cnt++) {
		if ( (ptr = strchr(argv[cnt], '=')) == NULL)
			call_template(&bparse, argv[cnt]);
	}
	

	return 0;
}
