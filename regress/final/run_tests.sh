#!/bin/bash

# script to run tests, as a first draw only will contain the logic ussed in jenkins

if [ "$1" = "--jenkins" ]; then
	JUNITXML=1 REGRESS_LOGERR_FILE=err.log make rd rdo

	ERRORS=$(wc -l err.log | awk '{ print $1 }')
	if [ $ERRORS -gt 0 ]; then
		make rc rd rs-load selenium-start
		for test in $(cat err.log); do
			make rs-$test
		done
	fi
fi
