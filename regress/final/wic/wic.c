#include <err.h>
#include <fcntl.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <alloca.h>

#include <sys/types.h>

#include <curl/curl.h>
#include <curl/types.h>
#include <curl/easy.h>

#include <pcre.h>

#include "util.h"
#include "strl.h"

#include "test_case_attr.h"

/* Quick and dirty C code to parse and run inject tests */
/* Tried to use libxml but webinject xml is broken... */

const char *
parse_identifier(const char *str, char *out_id, size_t idlen) {
	const char *end = str;

	while (*end && !isspace(*end) && !strchr("/=\"'", *end))
		end++;

	if (end == str)
		errx(1, "Syntax error, identifier expected");

	if ((size_t)(end - str) >= idlen)
		errx(1, "Buffer overflow in identifier");

	memcpy(out_id, str, end - str);
	out_id[end - str] = '\0';

	return end;
}

const char *
skip_space(const char *str) {
	while (isspace (*str))
		str++;
	return str;
}

const char *
parse_tag_open(const char *str, char *tagname, size_t tagnamelen, int *isclose) {
	while (*str) {
		str = skip_space(str);
		if (!*str)
			return NULL;

		if (*str++ != '<')
			errx(1, "Syntax error, '<' expected.");
		
		if (!*str)
			errx(1, "Syntax error, unterminated tag.");
		
		if (*str == '?') {
			char *ns = strstr(str + 1, "?>");
			if (!ns)
				errx(1, "Syntax error, unterminated <? tag");
			str = ns + 2;
			continue;
		}

		if (*str == '/') {
			*isclose = 1;
			str++;
		} else
			*isclose = 0;
		str = parse_identifier(str, tagname, tagnamelen);

		return str;
	}
	return NULL;
}

const char *
parse_attribute(const char *str, char *attname, size_t attnamelen, char *attval, size_t attvallen) {
	const char *end;

	str = skip_space(str);

	if (!*str || strchr("/>", *str))
		return NULL;

	str = parse_identifier(str, attname, attnamelen);
	if (*str++ != '=')
		errx(1, "Syntax error, unterminated attribute name '%s'", attname);

	if (*str == '"')
		end = strchr(++str, '"');
	else if (*str == '\'')
		end = strchr(++str, '\'');
	else
		errx(1, "Syntax error, attribute value needs to be quoted");

	if ((size_t)(end - str) >= attvallen)
		errx(1, "Buffer overflow in attribute value");

	memcpy(attval, str, end - str);
	attval[end - str] = '\0';
	str = end + 1;
	return str;
}

struct testcase
{
	int id;
	long verifyrespcode;
	char method[256];
	char desc1[1024];
	char desc2[1024];
	char url[1024];
	char verifypositive[7][1024];
	char verifynegative[7][1024];
	char postbody[1024];
	char posttype[1024];
	char cookie[1024];
	char cookiedomain[1024];
	char addheader[1024];
	struct testcase *next;
};

struct testcase *testcases;
struct testcase **lastcase;

const char *
parse_testcases(const char *str) {
	while (*str) {
		char tagname[256];
		int isclose;
		const char *ns;
		char attname[256];
		char attval[1024];
		struct testcase *tc;

		ns = parse_tag_open(str, tagname, sizeof (tagname), &isclose);
		if (!ns)
			errx(1, "Syntax error, EOF within testcases");

		if (isclose)
			break;

		str = ns;

		if (strcmp(tagname, "case") != 0)
			errx(1, "Syntax error, case tag expected");

		tc = zmalloc(sizeof (*tc));

		while ((ns = parse_attribute(str, attname, sizeof (attname), attval, sizeof (attval)))) {
			enum test_case_attr tca;
			str = ns;

			switch ((tca = lookup_test_case_attr(attname, strlen(attname)))) {
			case TCA_NONE:
				errx(1, "Unknown test case attribute '%s'", attname);
			case TCA_ID:
				tc->id = atoi(attval);
				break;
			case TCA_VERIFYRESPONSECODE:
				tc->verifyrespcode = atoi(attval);
				break;
			case TCA_METHOD:
				strlcpy(tc->method, attval, sizeof(tc->method));
				break;
			case TCA_DESCRIPTION1:
				strlcpy(tc->desc1, attval, sizeof(tc->desc1));
				break;
			case TCA_DESCRIPTION2:
				strlcpy(tc->desc2, attval, sizeof(tc->desc2));
				break;
			case TCA_URL:
				strlcpy(tc->url, attval, sizeof(tc->url));
				break;
			case TCA_VERIFYPOSITIVE:
			case TCA_VERIFYPOSITIVE1:
			case TCA_VERIFYPOSITIVE2:
			case TCA_VERIFYPOSITIVE3:
			case TCA_VERIFYPOSITIVE4:
			case TCA_VERIFYPOSITIVE5:
			case TCA_VERIFYPOSITIVE6:
			case TCA_VERIFYNEGATIVE:
			case TCA_VERIFYNEGATIVE1:
			case TCA_VERIFYNEGATIVE2:
			case TCA_VERIFYNEGATIVE3:
			case TCA_VERIFYNEGATIVE4:
			case TCA_VERIFYNEGATIVE5:
			case TCA_VERIFYNEGATIVE6:
				{
					char *targ = tca < TCA_VERIFYNEGATIVE ? tc->verifypositive[tca - TCA_VERIFYPOSITIVE] :
						tc->verifynegative[tca - TCA_VERIFYNEGATIVE];

					strlcpy(targ, attval, sizeof(tc->verifypositive[0]));
				}
				break;
			case TCA_POSTBODY:
				strlcpy(tc->postbody, attval, sizeof(tc->postbody));
				break;
			case TCA_POSTTYPE:
				strlcpy(tc->posttype, attval, sizeof(tc->posttype));
				break;
			case TCA_COOKIE:
				strlcpy(tc->cookie, attval, sizeof(tc->cookie));
				break;
			case TCA_COOKIEDOMAIN:
				strlcpy(tc->cookiedomain, attval, sizeof(tc->cookiedomain));
				break;
			case TCA_ADDHEADER:
				strlcpy(tc->addheader, attval, sizeof(tc->cookiedomain));
				break;
			}
		}

		str = skip_space(str);
		if (*str++  != '/')
			errx(1, "Syntax error, / expected");
		if (*str++ != '>')
			errx(1, "Syntax error, > expected");

		if (lastcase)
			*lastcase = tc;
		else
			testcases = tc;
		lastcase = &tc->next;
	}
	return str;
}

void
parse_root(const char *str) {
	int have_root = 0;
	const char *ns;

	while (*str) {
		char tagname[256];
		int isclose;

		str = parse_tag_open(str, tagname, sizeof (tagname), &isclose);
		if (!str || isclose) {
			if (!have_root)
				errx(1, "Syntax error, no root tag.");
			break;
		}

		if (have_root)
			errx(1, "Syntax error, multiple root tags.");
		have_root = 1;

		if (strcmp(tagname, "testcases") != 0)
			errx(1, "Syntax error, root tag is '%s', not 'testcases'", tagname);

		/* XXX attributes */
		ns = strchr(str, '>');
		if (!ns)
			errx(1, "Syntax error, unterminated root tag");
		str = ns + 1;

		str = parse_testcases(str);
	}
}

CURL *curl;
char bigbuf[0x80000];
int bigbuflen;
char curlerr[CURL_ERROR_SIZE];
int failed;
int dumpfd;

size_t tc_write(void *ptr, size_t size, size_t nmemb, void *stream) {
	if (bigbuflen + size * nmemb >= sizeof(bigbuf))
		return 0;

	memcpy(bigbuf + bigbuflen, ptr, size * nmemb);
	bigbuflen += size * nmemb;
	return size * nmemb;
}

void
fail_test(struct testcase *tc, const char *fmt, ...) __attribute__((format (printf, 2, 3)));

#define FAILSEP "\n\n======================================\n\n"

void
fail_test(struct testcase *tc, const char *fmt, ...)
{
	va_list ap;

	if (dumpfd < 0) {
		dumpfd = open("webinject/http.log", O_WRONLY|O_TRUNC|O_CREAT, 0666);
		if (dumpfd < 0)
			err(1, "open(\"webinject/http.log\")");
	} else {
		write (dumpfd, FAILSEP, sizeof(FAILSEP) - 1);
	}
	if (write (dumpfd, bigbuf, bigbuflen) < bigbuflen)
		err(1, "write(\"webinject/http.log\")");

	warnx("Id %d URL: %s", tc->id, tc->url);
	va_start(ap, fmt);
	vwarnx(fmt, ap);
	va_end(ap);
	failed++;
}

struct curl_httppost *
parse_httppost(int id, const char *str) {
	struct curl_httppost *first = NULL, *last = NULL;

	if (*str++ != '(')
		errx(1, "Id %d postbody: Syntax error: ( expected", id);

	str = skip_space(str);

	while (*str != ')') {
		char key[1024];
		const char *end;

		str = parse_identifier(str, key, sizeof(key));

		str = skip_space(str);
		if (strncmp(str, "=>", 2) != 0)
			errx(1, "Id %d postbody: Syntax error: => expected", id);
		str = skip_space(str + 2);

		switch (*str) {
		case '"':
			end = strchr(++str, '"');
			if (!end)
				errx(1, "Id %d postbody: EOF in value", id);
			curl_formadd(&first, &last, CURLFORM_COPYNAME, key, CURLFORM_CONTENTSLENGTH, (long)(end - str), CURLFORM_PTRCONTENTS, str, CURLFORM_END);
			str = end + 1;
			break;
		case '\'':
			end = strchr(++str, '\'');
			if (!end)
				errx(1, "Id %d postbody: EOF in value", id);
			curl_formadd(&first, &last, CURLFORM_COPYNAME, key, CURLFORM_CONTENTSLENGTH, (long)(end - str), CURLFORM_PTRCONTENTS, str, CURLFORM_END);
			str = end + 1;
			break;
		case '[':
			{
				char *fn;

				str = skip_space(str + 1);
				switch (*str) {
				case '"':
					end = strchr(++str, '"');
					break;
				case '\'':
					end = strchr(++str, '\'');
					break;
				default:
					errx(1, "Id %d postbody: ' or \" expected", id);
				}
				if (!end)
					errx(1, "Id %d postbody: EOF in file name", id);

				fn = strndupa(str, end - str);
				curl_formadd(&first, &last, CURLFORM_COPYNAME, key, CURLFORM_FILE, fn, CURLFORM_END);

				str = skip_space(end + 1);
				if (*str++ != ']')
					errx(1, "Id %d postbody: ] expected", id);
			}
			break;
		default:
			errx(1, "Id %d postbody: ', \" or [ expected", id);
		}
		str = skip_space(str);
		if (*str == ')')
			break;
		if (!*str || *str++ != ',')
			errx(1, "Id %d postbody: , or ) expected near %s", id, str);
		str = skip_space(str);
	}
	if (!first)
		warnx("Empty postbody");
	return first;
}

void
run_testcase(struct testcase *tc) {
	CURLcode res;
	long respcode;
	int i;
	struct curl_slist *header = NULL;
	struct curl_httppost *httppost = NULL;

	if (tc->url[0] == '\0')
		errx(1, "No url in testcase");

	if (!curl) {
		curl = curl_easy_init();
		if (!curl)
			err(1, "Failed to init curl");
		curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, tc_write);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
		curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, tc_write);
		curl_easy_setopt(curl, CURLOPT_WRITEHEADER, NULL);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "wic/1.0");
		curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curlerr);
		/*curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);*/
	}

	if (strcasecmp(tc->method, "post") == 0) {
		if (strcmp(tc->posttype, "multipart/form-data") == 0) {
			httppost = parse_httppost(tc->id, tc->postbody);
			curl_easy_setopt(curl, CURLOPT_HTTPPOST, httppost);
		} else {
			curl_easy_setopt(curl, CURLOPT_POST, 1L);
			curl_easy_setopt(curl, CURLOPT_POSTFIELDS, tc->postbody);
		}
	} else {
		curl_easy_setopt(curl, CURLOPT_HTTPGET, 1L);
	}
	curl_easy_setopt(curl, CURLOPT_URL, tc->url);
	curl_easy_setopt(curl, CURLOPT_COOKIELIST, tc->cookie);
	if (tc->cookie[0])
		curl_easy_setopt(curl, CURLOPT_COOKIE, tc->cookie);
	if (tc->addheader[0]) {
		header = curl_slist_append(header, tc->addheader);
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, header);
	}
	bigbuflen = 0;
	res = curl_easy_perform(curl);
	if (res)
		errx(1, "curl_easy_perform(%s): %s (%d)", tc->url, curlerr, res);

	if (header) {
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
		curl_slist_free_all(header);
	}
	if (httppost) {
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
		curl_formfree(httppost);
	}

	curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &respcode);
	if (tc->verifyrespcode && respcode != tc->verifyrespcode)
		fail_test(tc, "Response code %ld did not match expected %ld", respcode, tc->verifyrespcode);

	for (i = 0 ; i < 7 ; i++) {
		if (tc->verifypositive[i][0] != '\0') {
			const char *err;
			int erri;
			pcre *pregex = pcre_compile(tc->verifypositive[i], PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE | PCRE_CASELESS,
					&err, &erri, NULL);
			int match;

			if (!pregex)
				errx(1, "pcre_compile(%s): %s", tc->verifypositive[i], err);
			match = pcre_exec(pregex, NULL, bigbuf, bigbuflen, 0, 0, NULL, 0);
			if (match < -1)
				errx(1, "pcre_exec(%d): %d", i, match);
			if (match == -1)
				fail_test(tc, "Id %d positive verification %d failed: '%s' not matched",
						tc->id, i, tc->verifypositive[i]);
			pcre_free(pregex);
		}
		if (tc->verifynegative[i][0] != '\0') {
			const char *err;
			int erri;
			pcre *pregex = pcre_compile(tc->verifynegative[i], PCRE_DOLLAR_ENDONLY | PCRE_DOTALL | PCRE_NO_AUTO_CAPTURE,
					&err, &erri, NULL);
			int match;

			if (!pregex)
				errx(1, "pcre_compile(%s): %s", tc->verifynegative[i], err);
			match = pcre_exec(pregex, NULL, bigbuf, bigbuflen, 0, 0, NULL, 0);
			if (match < -1)
				errx(1, "pcre_exec(%d): %d", i, match);
			if (match != -1)
				fail_test(tc, "Id %d negative verification %d failed: '%s' matched",
						tc->id, i, tc->verifynegative[i]);
			pcre_free(pregex);
		}
	}

	if (failed)
		errx(1, "Failed %d verifications", failed);
}

int
main (int argc, char *argv[]) {
	int fd;
	int n;
	struct testcase *tc;

	if (argc != 2)
		errx(1, "Syntax: %s <injectfile>", argv[0]);

	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		err(1, "open(%s)", argv[1]);

	n = read(fd, bigbuf, sizeof(bigbuf));
	if (n < 0)
		err(1, "read");

	if (n >= (int)sizeof(bigbuf))
		errx(1, "File is too big");

	bigbuf[n] = '\0';
	bigbuflen = n;
	parse_root(bigbuf);

	if (!testcases)
		errx(1, "No test cases");

	dumpfd = -1;
	for (tc = testcases ; tc ; tc = tc->next)
		run_testcase(tc);

	return 0;
}
