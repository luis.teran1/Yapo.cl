#!/bin/bash

curr_md5=$(md5sum <(printf "cmd:bconf\ncommit:1\nend\n" | \
                    nc localhost $1) | \
                    cut -d' ' -f1)
start_md5=$(md5sum .start_bconf | cut -d' ' -f1)
if [[ $curr_md5 != $start_md5 ]]; then
	make --quiet trans-regress-reload apache-regress-restart;
fi
