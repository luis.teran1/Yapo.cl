#!/usr/bin/env python

import os
import time
import sys

g_TOPDIR = None;
g_REGRESS_DAVPORT = None;

g_progName = None;
g_scratch = None;

g_replay_files = {};
def modifyReplayFiles():
	l = os.listdir(g_scratch + '/dav_replay_folder/');
	for f in l:
		if( os.path.isdir(g_scratch + '/dav_replay_folder/' + f) ):	continue;

		if f.find('_images') != -1:
			_key = 'images';
		elif f.find('thumbs') != -1:
			_key = 'thumbs';
		else:	continue;

		g_replay_files[_key] = {'fileName' : f, 'path' : g_scratch + '/dav_replay_folder/'};
		
		_fullPath = g_replay_files[_key]['path'] + g_replay_files[_key]['fileName'];
		try:
			_replayData = open(_fullPath, 'r+');
		except Exception, e:
			print 'Unable to open replay file [%s]: %s' % (_fullPath, str(e));
			sys.exit(1);

		_replayData.seek(0);
		g_replay_files[_key]['origData'] = _replayData.readline().strip();
		_replayData.truncate(0);
		_replayData.seek(0);
		_replayData.write( g_replay_files[_key]['origData'].split()[0] );
		_replayData.write(' http://127.0.0.1:' + str(g_REGRESS_DAVPORT) + ' PUT\n');
		_replayData.close();
	#END_for

	return;
#END_modifyReplayFiles()

def moveDavImgs():
	os.chdir( g_scratch + '/upload/' );

	for _k in g_replay_files.iterkeys():
		_d = g_replay_files[_k];
		_d['img_path'] = _d['fileName'].replace('_', '/');

		try:
			os.system('cp --parents ./' + _d['img_path'] + ' ' + g_scratch +'/dav_replay_folder/');
		except Exception, e:
			print 'Error trying to COPY the file %s' % (_d['img_path'], str(e));
			sys.exit(1);

		try:
			os.system('rm -f ./' + _d['img_path']);
		except Exception, e:
			print 'Error trying to REMOVE the file %s' % (_d['img_path'], str(e));
			sys.exit(1);
	#END_for

	_davPutConfPath = g_TOPDIR + '/regress_final/conf/dav_put.conf';
	try:
		_davPutConf_fd = open(_davPutConfPath, 'a');
	except Exception, e:
		print 'Unable to open conf file [%s]: %s' % (_davPutConfPath , str(e));
                sys.exit(1);

	_davPutConf_fd.write('root = ' + g_scratch + '/dav_replay_folder\n');
	_davPutConf_fd.close();

	return;
#END_moveDavImgs();

def runDavReplayFor_n_Sec(sec):
    try:
        os.system(g_TOPDIR + '/regress_final/etc/init.d/dav_replay start');
    except Exception, e:
        print 'Unable to RUN dav_replay: %s' % str(e);
        sys.exit(1);

    time.sleep(sec);

    try:
        os.system(g_TOPDIR + '/regress_final/etc/init.d/dav_replay stop');
    except Exception, e:
        print 'Unable to STOP dav_replay: %s' % str(e);
        sys.exit(1);

    return;
#END_runDavReplayFor_n_Sec

def checkDavReplayActions():
	_uploadPath = g_scratch + '/upload';

	_filesInReplayFolder = os.listdir(g_scratch + '/dav_replay_folder/');

	for _k in g_replay_files.iterkeys():
		_d = g_replay_files[_k];

		_fullPathImg = _uploadPath + _d['img_path'];

		try:
			os.stat(_fullPathImg);
		except Exception, e:
			print 'File {0} missing on directory {1}' . format(_d['img_path'], _uploadPath);
			sys.exit(1);
		
		#l = s.listdir(_uploadPath + _d['img_path'][ : _d['img_path'].rfind('/')]);
		if _d['fileName'] in _filesInReplayFolder:
			print 'File {0} is still on directory {1}/dav_replay_folder/' . format(_d['fileName'], g_scratch);
			sys.exit(1);
	#END_for

	return;
#END_checkDavReplayActions

def parseArgs():
	global g_TOPDIR, g_REGRESS_DAVPORT, g_progName, g_scratch;

	g_progName, g_TOPDIR, g_REGRESS_DAVPORT = sys.argv;

	g_scratch = g_TOPDIR + '/regress/scratch';	
#END_parseArgs

def main():
	parseArgs(); 
	modifyReplayFiles();
	moveDavImgs();
	runDavReplayFor_n_Sec(5);
	checkDavReplayActions();

	sys.exit(0);
#END_main()

if __name__ == '__main__':
	main();

#EOF

