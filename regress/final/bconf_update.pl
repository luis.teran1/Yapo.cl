#!/usr/bin/perl -w
###################################################################################
#
# This script was written for webinject tests where a bconf value needed to
# be altered.
################

use strict;

sub help
{
    print qq(Usage: bconf_update.pl
 -bconf <filename> : bconf file to change
 -update <string>  : update syntax string "bconf_update-<key:value>"
 -substr           : The key in the update string should use the substring path for matching.
 -backup           : make a backup of the current bconf to "bconf.bpk"
 -restore          : restore the previously created backup
);
}

my $bconf;
my $cmd;
my $key;
my $is_substr = 0;

# Parse arguments
while ($#ARGV>=0) {
    my $arg=shift @ARGV;
    if ($arg eq "-bconf") {
        $bconf=shift @ARGV;
    } elsif ($arg eq "-substr") {
	$is_substr = 1;
    } elsif ($arg eq "-update") {
        $key=shift @ARGV;
        $cmd=$arg;
    } elsif ($arg eq "-backup" || $arg eq "-restore") {
        $cmd=$arg;
    } else {
        err("Unknown arg '$arg'.\n");
    }
}

if ($cmd eq "-backup") {
    system("/bin/cp '$bconf' '$bconf.bkp'");
    exit($?); # Exit with same code
}
if ($cmd eq "-restore") {
    system("/bin/mv '$bconf.bkp' '$bconf'");
    exit($?); # Exit with same code
}

if ($cmd eq "-update" && $key =~ m/bconf_update-(.*?):(.*)/) {
    my $key=$1;
    my $val=$2;
	
    if (open(BCONF, "<$bconf")) {
	my @arr=<BCONF>;
	close(BCONF);

	if (open(BCONF, ">$bconf")) {
	    my $found=0;
	    foreach my $line (@arr) {
		if ($is_substr) {
			if ($line =~ m/^(.*\Q$key\E[^=]*)=.*/) {
				$line=$1."=".$val."\n";
				print STDERR $line;
			}
		} elsif ($line =~ s/$key=.*/$key=$val/) {
		    $found=1;
		    print STDERR $line;
		}
		print BCONF $line;
	    }
	    if (!$found && !$is_substr) {
		print BCONF "*.*.$key=$val\n";
	    }
	    close(BCONF);
	} else {
	    err("Couldn't open $bconf for writing.\n");
	}
    } else {
	err("Couldn't open $bconf for reading.\n");
    }
} else {
    err("Wrong type of argument ($cmd).\n");
}

#####################
# Print an error, the help and exit.
sub err
{
    my ($err)=@_;
    print "ERROR: bconf_update.pl: ",  $err, "\n";
    help();
    exit(1);
}
