#!/usr/bin/php -q
<?
echo "Testing memcached server\n";
echo " - Starting memcached\n";
$uniq = tempnam("/tmp", "memcached");
exec("memcached -d -p 11111 -P $uniq", $res, $err);
if ($err) {
	echo " - ERROR: memcached could not be started\n";
	exit(1);
}
usleep(500);

echo " - Starting socket session to memcached\n";
$socket = fsockopen("127.0.0.1", "11111", $errono, $errostr, 5);
if (!$socket) {
	echo " - ERROR: Unable to connect to memcached\n";
	exit(1);
}
usleep(500);

// testing set variabel
fwrite($socket, "set test 0 0 7\r\nTESTING\r\n");
$res = fgets($socket);
if (ereg("STORE", $res)) {
	echo " - OK: SET test = TESTING\n";
} else {
	echo(" - ERROR: Could not SET test variable\n");
	exit(1);
}

// testing get variabel
fwrite($socket, "get test\n");
$res = fgets($socket); // value of test
if (ereg ("VALUE test 0 7", $res)) {
	$res = fgets($socket); // The value
	echo " - OK: GET test = $res";
} else {
	echo(" - ERROR: Could not GET value of test\n");
	exit(1);
}

fclose($socket);

// TEST SUCCESSFULL
echo " - TEST SUCCESSFULL\n";
// Kill memcached server
exec("kill \$(cat $uniq)");
unlink($uniq);

exit(0);
?>
