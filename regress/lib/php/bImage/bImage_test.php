<?php
// To test a case that should return false
// Create the variabel $negate in the case #:

// Require argument
if (!isset($argv[1])) {
	echo "Usage: ./bImage_test.php [number]\n";
	echo "  Alt: php bImage_test.php [number]\n";
	exit(1);
}

// Include path
set_include_path(get_include_path().PATH_SEPARATOR."../../../../php/common/include:../../../../php/common/PHP_JPEG_Metadata_Toolkit:../../../../php/newad/apps:../../../../php/controlpanel/include:../../../../php/store/include" );

/*
 * FAKE BCONF
 */
if (!isset($BCONF)) {
	$GLOBALS['BCONF']['*']['common']['image']['quality'] = 80;
	$GLOBALS['BCONF']['*']['common']['thumbnail']['quality'] = 70;
	$GLOBALS['BCONF']['*']['common']['image']['maxsize'] = 70 * 1024;
	$GLOBALS['BCONF']['*']['common']['image']['maxres'] = 16 * 1024 * 1024;

	// Public function watermark can override watermarks
	$GLOBALS['BCONF']['*']['common']['watermark']['standard'] = "watermark.png";
	$GLOBALS['BCONF']['*']['common']['watermark']['small'] = "watermark_small.png";
	$GLOBALS['BCONF']['*']['common']['image']['width'] = 640;
	$GLOBALS['BCONF']['*']['common']['image']['height'] = 480;
	$GLOBALS['BCONF']['*']['common']['image']['min_width'] = 35;
	$GLOBALS['BCONF']['*']['common']['image']['min_height'] = 35;

	// Doesn't have a set function
	$GLOBALS['BCONF']['*']['common']['thumbnail']['width'] = 80;
	$GLOBALS['BCONF']['*']['common']['thumbnail']['height'] = 60;

	$GLOBALS['BCONF']['*']['common']['locale'] = 'en_US';
}

require_once("bImage.php");
ini_set("memory_limit", (16 * 4 * 2)."M");
ini_set("max_execution_time", "120");

/*
 * TEST
 */
// Unlink the test.jpg if exists
@unlink("test.jpg");

// Create image object
$image = new bImage();

// Do test
switch ($argv[1]) {
	case 1:
		echo "Testing bImage class (load/resize/watermark) \n";
		echo " - JPEG BIG [";
		$image->load_image("test_image_big.jpg");
	break;
	
	case 2:
		echo " - JPEG NORMAL [";
		$image->load_image("test_image_normal.jpg");
	break;
	
	case 3:
		echo " - JPEG SMALL [";
		$image->load_image("test_image_small.jpg");
	break;
	
	case 4:
		echo " - JPEG VERYSMALL [";
		$image->load_image("test_image_verysmall.jpg");
	break;
	
	case 5:
		echo " - BASELINE STANDARD JPEG [";
		$image->load_image("test_jpeg_baseline_standard.jpg");
	break;
	
	case 6:
		echo " - BASELINE OPTIMIZED JPEG [";
		$image->load_image("test_jpeg_baseline_optimized.jpg");
	break;
	
	case 7:
		echo " - PROGRESSIVE JPEG [";
		$image->load_image("test_jpeg_progressive.jpg");
	break;
	
	case 8:
		echo " - PNG [";
		$image->load_image("test_image_png.png");
	break;
	
	case 9:
		echo " - INTERLACED PNG [";
		$image->load_image("test_png_interlaced.png");
	break;
	
	case 10:
		echo " - GIF [";
		$image->load_image("test_image_gif.gif");
	break;
	
	case 11:
		echo " - BMP [";
		$image->load_image("test_image_bmp.bmp");
	break;
	
	case 12:
		echo " - THUMBNAIL [";
		$image->load_image("test_image_normal.jpg");
		$image->create_thumbnail("test.jpg");
		$image->destruct();
	break;
	
	case 13:
		echo " - CORRUPTED JPEG [";
		$image->load_image("test_corrupted.jpg");
		$negate = true;
	break;
	
	case 14:
		echo " - TXT [";
		$image->load_image("test.txt");
		$negate = true;
	break;
	
	case 15:
		echo " - EMPTY STRING [";
		$image->load_image("");
		$negate = true;
	break;
	
	case 16:
		$string = crypt(rand());
		echo " - RANDOM STRING $string [";
		
		$image->load_image($string);
		$negate = true;
	break;
	
	case 17:
		echo " - SPACED STRING [";
		$image->load_image("                              ");
		$negate = true;
	break;

	case 18:
		echo " - 1x1 image [";
		$image->load_image("test_image_1x1.gif");
		$image->resize();
		$image->destruct();
		$negate = true;
	break;
	
	case 19:
		echo " - CROPPED IMAGE [";
		$image->load_image("test_image_normal.jpg");
		$image->resize_crop(30, 30);
	break;

	case 20:
		echo " - SAME DIGEST IN SAME IMAGES [";
		$image->load_image("test_image_digest_1.jpg");
		$image->create_thumbnail();
		$digest1 = $image->get_thumbnail_digest();
		$image->destruct();
		unset($image);
	
		$image = new bImage();
		$image->load_image("test_image_digest_1.jpg");
		$image->create_thumbnail();
		$digest2 = $image->get_thumbnail_digest();

		if ($digest1 != $digest2)
		{
			echo "ERROR]\n";
			exit(1);
		}
		else
		{
			echo "OK]\n";
			exit(0);
		}
	break;

	case 21:
		echo " - DIFFERENT DIGEST IN DIFFERETN IMAGES [";
		$image->load_image("test_image_digest_1.jpg");
		$image->create_thumbnail();
		$digest1 = $image->get_thumbnail_digest();
		$image->destruct();
		unset($image);
	
		$image = new bImage();
		$image->load_image("test_image_digest_2.jpg");
		$image->create_thumbnail();
		$digest2 = $image->get_thumbnail_digest();

		if ($digest1 == $digest2)
		{
			echo "ERROR]\n";
			exit(1);
		}
		else
		{
			echo "OK]\n";
			exit(0);
		}
	
	break;
	case 22:
		unset($argv[1]);
		@require_once("blocket_newad.php");
		echo " - WRITE DIGEST IN EXIF HEADER [";
		$newad = @new blocket_newad();

		$uploaded_image = new bImage("test_image_to_embed_digest.bmp");
		$embed_digest = empty($uploaded_image->_thumb_digest);  
		$uploaded_image->resize();	// Also sets up _image_buffer
		$thumbnail_buffer = $uploaded_image->create_thumbnail();
			
		if ($embed_digest) {
			$uploaded_image->watermark();
			$uploaded_image->_image_to_buffer(); // Recreate _image_buffer after watermarking needed for digest embed process
		}

		$newad->digest_embed($uploaded_image);


		$fd =fopen("test_image_digest_embedded.jpg", "w");
		fwrite($fd, $uploaded_image->_image_buffer);
		fclose($fd);

		if ( strstr($uploaded_image->_image_buffer, $uploaded_image->_thumb_digest))
		{
			echo "OK]\n";
			exit(0);
		}
		else
		{
			echo "ERROR]\n";
			exit(1);
		}
	
	break;

	case 23:
		echo " - READ EMBEDDED DIGEST IN EXIF HEADER [";
		$image->load_image("test_image_digest_embedded.jpg");
		$embedded_digest = $image->get_thumbnail_digest();
		@unlink("test_image_digest_embedded.jpg");

		if ( !isset($embedded_digest) )
		{
			echo "ERROR]\n";
			exit(1);
		}
		else
		{
			echo "OK]\n";
			exit(0);
		}
	
	break;


	default:
		exit(1);
	break;
}

if ($image->is_ready()) {
	$image->resize();
	$image->watermark();
	$image->create_image("test.jpg");
}

// Return code
$error = strpos(`file test.jpg`, "JPEG")?0:1;
// If this test is negated
if (isset($negate)) $error = abs($error - 1);
echo $error?"ERROR":"OK";
echo "]\n";


// Remove the test image
@unlink("test.jpg");

// Exit with error code
exit($error);
?>
