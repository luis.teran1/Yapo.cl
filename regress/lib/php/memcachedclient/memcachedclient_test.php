#!/usr/bin/php -q
<?php
set_include_path(get_include_path().PATH_SEPARATOR."../../../../php/common/include");
echo "Testing Weird memcached php client\n";
// FAKE call_template function
if (!function_exists("call_template")) {
	function call_template() {}
}
// FAKE BCONF
if (!isset($BCONF))
	$BCONF = 0 ;
require_once('memcachedclient.php');
$uniq = tempnam("/tmp", "memcached");

// Memcached server
echo " - Starting memcached\n";
exec("memcached -d -p 11111 -P $uniq", $res, $err);
if ($err) {
        echo " - ERROR: memcached could not be started\n";
        exit(1);
}
usleep(500);

// Client settings
$hosts = array('localhost' => '127.0.0.1:11111');
$options = array(
	'servers' => $hosts,
	'debug' => false,
	'compress' => false,
	'id_separator' => "000"
);

// Starting client
$client = new WeirdMemCachedClient($options);

// Setting and getting values
echo " - Setting and getting a test variabel test='TESTING'\n";
$client->set("localhost000test", "TESTING");
if (@$client->get("localhost000test") == "TESTING") {
	echo " - Test successfull\n";
} else {
	echo(" - Test failed\n");
	exec("kill \$(cat $uniq)");
	unlink($uniq);
	exit(1);
}

$client->disconnect_all();

exec("kill \$(cat $uniq)");
unlink($uniq);
exit(0);
?>
