#! /bin/bash

num_stores=$1
port=$2
if [[ -z $port || -z $num_stores ]]
then
	echo "Usage: ./many_stores.sh <number of stores> <trans port>"
	exit 1;
fi

function create_account {  # pass name and region
	echo "cmd:create_account
commit:1
name:$1
email:$1@schibsted.cl
phone:987654323
password:\$1024\$Ud[B.!5B9V'7nob~1fc33f62d41d3556d433244df925578e738445a1
is_company:0
region:$2
contact:
lob:
commune:
accept_conditions:on
end
" | nc localhost $port;
	return 0;
}

function activate_account { # give me the email
	echo "cmd:manage_account
commit:1
status:active
email:$1@schibsted.cl
end
" | nc localhost $port;
	return 0;
}


function create_store { # give me the account id
	echo "cmd:create_store
commit:1
account_id:$1
end
" | nc localhost $port;
	return 0;
}

function apply_products { # give me the store id
	echo "cmd:apply_product_to_store
commit:1
store_id:$1
expire_time:20 minutes
end
" | nc localhost $port;
	return 0;	
}

function edit_store { # give me the store_id
					  #             store_name
					  #             region
					  #             commune
	echo "cmd:edit_store
commit:1
store_id:$1
name:$2
info_text:descripcion de negocio
url:$2
phone:987654323
region:$3
commune:$4
end
" | nc localhost $port;
	return 0;
}



account_id_start=8;
account_name_pattern="account";
store_id_start=1;
communes=(1 5 12 21 30 45 83 116 146 200 232 244 274 284 295);




for i in `seq 0 $num_stores` 
do
	region="$(($i % 15 + 1))"
	commune_index="$(($region - 1))";
	account_id="$(($account_id_start + $i))";
	account_name="$account_name_pattern$account_id";
	store_id="$(($store_id_start + $i))";
	create_account "$account_name" "$region";
	activate_account "$account_name";
	create_store "$account_id";
	apply_products "$store_id";
	edit_store "$store_id" "$account_name" "$region" "${communes[$commune_index]}";
done
