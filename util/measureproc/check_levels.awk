#!/bin/awk -f
function abs(x) {
	if (x < 0)
		return x * -1;
	else
		return x;
}

BEGIN {
	err = 0;
	if (ENVIRON["STRESS_FACTOR"] == 0)
		{
			print "STRESS_FACTOR not found";
			err = 1;
			exit 1;
		}
}

{
	limit = $6;
	if ( $1 ~ /^cpu/)
		diff = abs($4 - $2)/(1.0*ENVIRON["STRESS_FACTOR"]);
	else
		diff = abs($4 - $2);

	print $1, diff;
# 67108864 is a magic number appearing in vsz diffs sometimes. I haven't been able to track it down, ignore it for now.
	if ( diff == 67108864 )
	{
		print $1 " diff is magic number";
	}
	else if ( diff > limit)
		{
			print $1 " (" diff ") difference exceeds limit (" limit ")";
			err = 1;
		}
}

END { 
	if (err != 0) 
		exit 1;
	exit 0;
}
