#!/bin/sh
# XXX assumes command name does not contain spaces.
# output:
# num: <num of matching procs>
# rss: <sum of residental pages>
# vsz: <sum of virtual size in bytes>
# cpu: <sum of jiffies spent by procs and their reaped children>
pids=$(pgrep -u `id -u` -f "^$*")
for pid in $pids; do
	if [ $pid != $$ -a -f /proc/$pid/stat ]; then
		cat /proc/$pid/stat
	fi
done | awk '{ num++; rss += $24 + 3; vsz += $23; cpu += $14 + $15 + $16 + $17; } END { printf "num: %d\nrss: %d\nvsz: %d\ncpu: %d\n", num, rss, vsz, cpu }'
