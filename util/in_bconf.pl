#!/usr/bin/perl

sub load_bconf {
	open BCONF, $ARGV[0] || die "Failed to open bconf";

	while (<BCONF>) {
		chomp;
		my ($k, $value) = split("=");
		my ($host, $app, $key) = split(/[.]/, $k, 3);
		$bconf{$key} = $value;
	}
}

sub get_bconf_var {
	my $var = shift @_;
	if (!defined %bconf) {
		load_bconf;
	}
	return $bconf{$var};
}

while(<STDIN>) {
	s/%\$\(([a-z0-9_.*]+)\)%/&get_bconf_var($1)/e;
	print;
}
