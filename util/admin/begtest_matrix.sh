#!/bin/bash

# XXX cut fields might change, check that you extract price, mileage and regdate, in that order.
# Result is in *.xls, various rm test* etc are done, so use an empty directory.

rm -f *.xls

sq="0 lim:2147483647 subsort:+ subrange:3000- mileage:1-"
alias GETDATA="nc localhost 9080 | cut -f 24,29,30 | grep -vE 'info|price' | awk '{ printf \"\\t%s\\t%s\\t%s\\n\", \$3, \$2, \$1; }'"

file=audi-100.xls
rm -f $file
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1991-1994 *:* $search" | GETDATA >> $file
done << EOT
audi 100 not bytes
audi 100 not avant not bytes not combi
audi 100 avant not bytes
audi 100 quattro
audi 100 not quattro not bytes
EOT
file=audi-100.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1995-1997 *:* $search" | GETDATA >> $file
done << EOT
Audi A6 not bytes
audi A6 not avant not bytes not combi not kombi
audi A6 avant not bytes
audi A6 quattro not bytes
audi A6 not quattro not bytes
Audi A6 1,8 not bytes
Audi A6 V6 not bytes
Audi A6 TDI not bytes
EOT
file=audi-a4.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1995-2000 *:* $search" | GETDATA >> $file
done << EOT
Audi A4 not bytes
audi A4 not avant not bytes not combi not kombi
audi A4 avant not bytes
audi A4 quattro not bytes
audi A4 not quattro not bytes
Audi A4 1,8 not bytes not 1,8t not 1,8ts
Audi A4 1,8T not bytes
Audi A4 TDI not bytes
EOT
file=audi-a6.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2004 *:* $search" | GETDATA >> $file
done << EOT
Audi A6 not bytes
audi A6 not avant not bytes not combi not kombi
audi A6 avant not bytes
audi A6 not quattro not bytes
audi A6 quattro not bytes
Audi A6 1,8 not bytes
Audi A6 2,4 not bytes
Audi A6 2,7 not bytes
Audi A6 2,8 not bytes
Audi A6 3,0 not bytes
Audi A6 1,9 not bytes
Audi A6 2,5 not bytes
EOT
file=ford-mondeo.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2001-2006 *:* $search" | GETDATA >> $file
done << EOT
Ford Mondeo not byte
Ford Mondeo not hgv not kombi not combi not byte
Ford Mondeo kombi not tdci not byte
Ford Mondeo 2,0 not tdci not hgv not kombi not combi not byte
Ford Mondeo 2,0 kombi not tdci not byte
Ford Mondeo 2,5 not hgv not kombi not combi not byte
Ford Mondeo 2,5 kombi not byte
Ford Mondeo st220 not byte
EOT
file=peugot-206.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2005 *:* $search" | GETDATA >> $file
done << EOT
Peugeot 206 not byte
Peugeot 206 1,4 not byte
Peugeot 206 1,6 not byte not cc
Peugeot 206 gti not byte
Peugeot 206 rc not byte
Peugeot 206 cc not byte
EOT
file=saab-9-3.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1994-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 900 not byte
EOT
file=saab-9-3.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2002 *:* $search" | GETDATA >> $file
done << EOT
Saab 9-3 not byte
Saab 9-3 2,0 not 185 not turbo not aero not byte
Saab 9-3 2,0 turbo not aero not byte
Saab 9-3 aero not cab not byte
Saab 9-3 cab not byte
EOT
file=saab-9-3.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2003 *:* $search" | GETDATA >> $file
done << EOT
Saab 9-3 polar not byte
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1985-1991 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 not cd not cde not bytes
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 cd not cs not cse not cc not byte
Saab 9000 cde not cs not cse not cc not byte 
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1992-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 cs not cd not cde not cc not byte
Saab 9000 cse not cd not cde not cc not byte
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1995-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 2,0 not turbo not aero not byte
saab 9000 2,0 turbo not 150 not byte
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1990-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 2,3 not 2,3turbo not turbo not aero not byte
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1994-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 2,3 turbo 170 not byte
EOT
file=saab-9000.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1991-1998 *:* $search" | GETDATA >> $file
done << EOT
Saab 9000 Aero not 2,0 not byte
EOT
file=saab-9-5.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998- *:* $search" | GETDATA >> $file
done << EOT
Saab 9-5 not byte
Saab 9-5 not kombi not combi not sportkombi not sportcombi not byte 
Saab 9-5 kombi or sportkombi or combi or sportcombi not byte
Saab 9-5 2,0 not byte not biopower
Saab 9-5 2,0 biopower not byte
saab 9-5 2,3t not biopower not bytes
Saab 9-5 aero not byte
Saab 9-5 2,2 not byte
EOT
file=volvo-740.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1984-1992 *:* $search" | GETDATA >> $file
done << EOT
Volvo 740 not kombi not 760 not byte not a-traktor not 240 not 940
volvo 740 GL not kombi not 760 not byte not a-traktor not 240 not 940
volvo 740 GLE not kombi not 760 not byte not a-traktor not 240 not 940
volvo 740 GLT not kombi not 760 not byte not a-traktor not 240 not 940
volvo 740 turbo not kombi not 760 not byte not a-traktor not 240 not 940
volvo 745 not 765 not byte not a-traktor not 245 not 945
745 GL not 765 not byte not a-traktor not 245 not 945
745 GLE not 765 not byte not a-traktor not 245 not 945
745 GLT not 765 not byte not a-traktor not 245 not 945
745 turbo not 765 not byte not a-traktor not 245 not 945
volvo 760 not byte not a-traktor not 740
EOT
file=volvo-850.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1992-1997 *:* $search" | GETDATA >> $file
done << EOT
Volvo 850 not kombi not combi not 855 not 940 not 960 not bytes not byte
Volvo 855 not 940 not 960 not bytes not byte
Volvo 850 kombi not 855 not 940 not 960 not bytes not byte
Volvo 850 or 855 2,0 not turbo not t5 not r not t5r not glt not byte
Volvo 850 or 855 2,5 not turbo not t5 not r not t5r not glt not byte
Volvo 850 or 855 glt not byte
Volvo 850 or 855 turbo not glt not byte not t5 not t-5 not r not 850r not t5-r not t5r not se not gle not 940 not 960 not awd
Volvo 850 or 855 t5r not byte
Volvo 850 or 855 tdi not glt not gle not t5r not t5
EOT
file=volvo-s60.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2000- *:* $search" | GETDATA >> $file
done << EOT
Volvo S60 not byte
Volvo S60 2,4 140
Volvo S60 2,4 170
Volvo S60 2,4t
Volvo S60 2,5t
Volvo S60 t5
Volvo S60 d5
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1996-2003 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40, not honda, not s60, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2004 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40 not n, not s40n, not 2,4, not t5, not t-5, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1996-2004 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40 1,8 not 1,8i, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1996-2003 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40 2,0 not 2,0t, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2004 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40 2,0t not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2003 *:* $search" | GETDATA >> $file
done << EOT
Volvo S40 t4 not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1996-2004 *:* $search" | GETDATA >> $file
done << EOT
Volvo V40 not byte, not bytes, not byter, not inbyte
Volvo V40 1,8 not 1,8i, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1996-2003 *:* $search" | GETDATA >> $file
done << EOT
Volvo V40 2,0 not 2,0t, not turbo, not t4, not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2004 *:* $search" | GETDATA >> $file
done << EOT
Volvo V40 2,0t not byte, not bytes, not byter, not inbyte
EOT
file=volvo-sv40.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2003 *:* $search" | GETDATA >> $file
done << EOT
Volvo V40 t4 not byte, not bytes, not byter, not inbyte
EOT
file=volvo-v70.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1997- *:* $search" | GETDATA >> $file
done << EOT
Volvo V70
EOT
file=volvo-v70.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2000- *:* $search" | GETDATA >> $file
done << EOT
Volvo V70 2,4 140
Volvo V70 2,4 170
Volvo V70 2,5T
Volvo V70 T5
Volvo V70 AWD not XC
Volvo V70 XC
EOT
file=golf.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998-2003 *:* $search" | GETDATA >> $file
done << EOT
golf not byte
golf not byte not variant
golf variant
golf 1,6
golf TDI
golf gti
golf r32
EOT
file=passat.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1997-2004 *:* $search" | GETDATA >> $file
done << EOT
Passat not bytes
Passat not variant not kombi not combi not bytes
Passat variant or kombi or combi not bytes
Passat 1,8T not bytes
Passat 1,9 TDI not byte
Passat V5 not byte
Passat V6 not tdi not byte
EOT
file=ford-mondeo-ii.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:2001-2006 *:* $search" | GETDATA >> $file
done << EOT
Ford Mondeo not byte
Ford Mondeo not hgv not kombi not combi not byte
Ford Mondeo kombi not tdci not byte
Ford Mondeo 2,0 not tdci not hgv not kombi not combi not byte
Ford Mondeo 2,0 kombi not tdci not byte
Ford Mondeo 2,5 not hgv not kombi not combi not byte
Ford Mondeo 2,5 kombi not byte
Ford Mondeo st220 not byte
EOT
file=saab-9-5.xls
while read search; do
	printf "\n\n%s\n\t" "$search" >> $file
	printf "$sq regdate:1998- *:* $search" | GETDATA >> $file
done << EOT
Saab 9-5 not byte
Saab 9-5 not kombi not combi not sportkombi not sportcombi not byte
Saab 9-5 kombi or sportkombi or combi or sportcombi not byte
Saab 9-5 2,0 not byte not biopower
Saab 9-5 2,0 biopower not byte
saab 9-5 2,3t not biopower not bytes
Saab 9-5 aero not byte
Saab 9-5 2,2 not byte
EOT


for f in *.xls; do
	awk 'BEGIN { mileage[1] = "0"; mileage[2] = "500"; mileage[3] = "1000"; mileage[4] = "1500"; mileage[5] = "2000"; mileage[6] = "2500"; mileage[7] = "3000"; mileage[8] = "3500"; mileage[9] = "4000"; mileage[10] = "4500"; mileage[11] = "5000"; mileage[12] = "5500"; mileage[13] = "6000"; mileage[14] = "6500"; mileage[15] = "7000"; mileage[16] = "7500"; mileage[17] = "8000"; mileage[18] = "8500"; mileage[19] = "9000"; mileage[20] = "9500"; mileage[21] = "10000"; mileage[22] = "11000"; mileage[23] = "12000"; mileage[24] = "13000"; mileage[25] = "14000"; mileage[26] = "15000"; mileage[27] = "16000"; mileage[28] = "17000"; mileage[29] = "18000"; mileage[30] = "19000"; mileage[31] = "20000"; mileage[32] = "25000"; mileage[33] = "30000"; mileage[34] = "35000"; mileage[35] = "40000"; mileage[36] = "45000"; mileage[37] = "50000"; } /^\t/ { printf "\t%s\t%s\t%s\n", $1, mileage[$2], $3; } /^[^\t]/ { print $0; }' $f > new-$f
	mv -f new-$f $f
done
