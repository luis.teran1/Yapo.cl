#!/bin/sh

set -e

db_script_path='/opt/blocket/bin/create_blocketdb_archive.pgsql';
host='';

set -- $(getopt h:d: "$@")
while [ $# -gt 0 ]
do
    case "$1" in
    (-d) db_script_path="$2"; shift;;
    (-h) host=" -h $2"; shift;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*)  break;;
    esac
    shift
done

if [ -z "$1" ] ; then
   echo "Usage: $0 \"where-clause\""
   echo "Options:";
   echo " -h host";
   echo " -d db script path ";
   exit 1
fi

if [ ! -f $db_script_path ] ; then
   echo "Error: need $db_script_path"
   exit 1
fi

DATE=`/bin/date +%Y%m%d`

where=$*

schema=dump_ads

# Create schema
(
 	cat << EOT
DROP SCHEMA IF EXISTS $schema CASCADE;
CREATE SCHEMA $schema;
SET search_path=$schema;
\i $db_script_path
EOT
) | psql $host -e blocketdb

# Copy
(
	cat << EOT
SET search_path=bpv,public,$schema;
BEGIN;
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
SELECT * FROM archive_ad(ARRAY(SELECT ad_id FROM ads WHERE $where), 'dump_ads', false);
COMMIT;
EOT

) | psql $host -e blocketdb

# Clean data
(
	cat <<EOT
SET search_path=$schema;
ALTER TABLE dump_ads.payment_groups ALTER added_at DROP NOT NULL;
SET search_path=bpv,public,$schema;
EOT

	cat <<EOT
SET search_path=$schema;
CREATE TABLE dump_ads.users AS SELECT users.* FROM public.users JOIN ads USING (user_id) WHERE $where;
UPDATE dump_ads.users SET email = replace(email, '@', '_') || '@yapo.cl';
UPDATE dump_ads.tokens SET admin_id = 1 WHERE admin_id IS NOT NULL;
UPDATE dump_ads.ad_actions SET locked_by = 1 WHERE locked_by IS NOT NULL;
EOT
	cat <<EOT
INSERT INTO dump_ads.ad_media (SELECT public.ad_media.* FROM public.ad_media JOIN dump_ads.ads on (dump_ads.ads.ad_id = public.ad_media.ad_id));
EOT

) | psql $host -e blocketdb

# Dump data
pg_dump $host --column-inserts -aO -F c -n dump_ads blocketdb > ads_dump.$DATE.dump
#pg_dump $host --column-inserts -aO -F p -n dump_ads blocketdb > ads_dump.$DATE.dump

# Order data
order="users tokens ads payment_groups ad_actions action_params action_states ad_changes ad_image_changes ad_media ad_params notices payments pay_log state_params"

pg_restore -l ads_dump.$DATE.dump > ads_dump.$DATE.order
egrep '^;' ads_dump.$DATE.order > ads_dump.$DATE.order.new
for t in $order; do
	grep " $t " ads_dump.$DATE.order >> ads_dump.$DATE.order.new
done
rm ads_dump.$DATE.order
mv ads_dump.$DATE.order.new ads_dump.$DATE.order

ls -l ads_dump.*
echo "Restore command:"
echo " pg_restore -F c -d blocketdb -L ads_dump.$DATE.order ads_dump.$DATE.dump"
echo "Command to cleanup:"
echo " psql blocketdb -c 'DROP SCHEMA $schema CASCADE'"
