#!/bin/sh


# XXX Must use new archive_ad functions!
# XXX create_blocketdb_archive.pgsql is needed 

set -e

where=$1

schema=dump_ads
archives="blocket_2004 blocket_2005 blocket_2006 blocket_2007 blocket_2008"

(
	cat <<EOT
CREATE SCHEMA $schema;
SET search_path=$schema;
\i create_blocketdb_archive.pgsql
ALTER TABLE payment_groups ALTER added_at DROP NOT NULL;
SET search_path=bpv,public;
SELECT copy_ad(ad_id) FROM users JOIN ads USING (user_id) WHERE $where;
EOT

	for s in $archives; do
		cat <<EOT
SET search_path=$s;
\i copy_token.sql
\i copy_archive_ad.sql
SELECT copy_archive_ad(ad_id) FROM public.users JOIN ads USING (user_id) WHERE $where;
DROP FUNCTION copy_archive_ad(integer, varchar);
DROP FUNCTION copy_token(integer, varchar);
EOT
	done

	cat <<EOT
SET search_path=$schema;
CREATE TABLE users AS SELECT DISTINCT(users.*) FROM public.users JOIN ads USING (user_id) WHERE $where;
UPDATE users SET email = replace(email, '@', '_') || '@yapo.cl';
UPDATE tokens SET admin_id = 1 WHERE admin_id IS NOT NULL;
UPDATE ad_actions SET locked_by = 1 WHERE locked_by IS NOT NULL;

EOT

) | psql blocketdb

pg_dump -aDO -F c -n dump_ads blocketdb > ads_dump.dump

order="users tokens ads payment_groups ad_actions action_params action_states ad_changes ad_image_changes ad_media ad_params notices payments pay_log state_params"

pg_restore -l ads_dump.dump > ads_dump.order
egrep '^;' ads_dump.order > ads_dump.order.new
for t in $order; do
	grep " $t " ads_dump.order >> ads_dump.order.new
done
rm ads_dump.order
mv ads_dump.order.new ads_dump.order

ls -l ads_dump.*
echo "Restore command (ignore duplicate key errors):"
echo " pg_restore -F c -d blocketdb -L ads_dump.order ads_dump.dump | less"
echo "Command to cleanup:"
echo " psql blocketdb -c 'DROP SCHEMA $schema CASCADE'"

