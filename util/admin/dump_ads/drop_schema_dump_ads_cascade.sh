#!/bin/sh
# $Id: drop_schema_dump_ads_cascade.sh 21626 2008-12-15 14:30:44Z mikael $

if [ "`whoami`" == "postgres" ]
then

echo -e "This will DROP SCHEMA dump_ads CASCADE.\nPress any key to continue or ^C to abort."

psql blocketdb << EOSQL
BEGIN;
DROP SCHEMA dump_ads CASCADE;
COMMIT;
EOSQL

DUMP_ADS_NOT_EMPTY="$HOME/dump_ads/dump_ads_not_empty"
rm -v $DUMP_ADS_NOT_EMPTY

else
	echo "You are not postgres."
	exit 1
fi
