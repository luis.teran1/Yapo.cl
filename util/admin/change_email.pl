#!/usr/bin/perl -w
# Move all ads belonging to one email to another email.

use strict;
use Getopt::Long;
use blocket::bconf;
use Sys::Syslog;
use Socket;
use Sys::Hostname;
use perl_trans;
use Digest::SHA1;
use Data::Dumper;

my $configfile;
my $debug;

# init, argv, open syslog
GetOptions("configfile=s" => \$configfile,
	   "debug" => \$debug);

my $bconf = blocket::bconf::init_conf($configfile);
if (!$bconf->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'name'}) {
	die "Failed to read bconf from trans.";
}

sub my_log {
	my ($loglevel, $message) = @_;

	print "($loglevel) $message\n";
}

sub trans {
	my %param = @_;

	$param{'commit'} = 1;
	my %resp = do_trans($bconf->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'name'}, 
			 $bconf->{'*'}->{'common'}->{'transaction'}->{'host'}->{'1'}->{'port'},
			 \%param);
	if (!%resp) {
		my_log('crit', "(CRIT) Failed to communicate with trans.");
		exit 1;
	}
	
	if (!($resp{status} eq "TRANS_OK" || $resp{status} =~ /^TRANS_ERROR/)) {
		my_log('crit', "(CRIT) trans returned status: $resp{status}.");
		exit 1;
	}

	return %resp;
}

sub parse_ad {
	my $idx = shift;
	my %ads = @_;
	my %res = ();
	
	my $prefix = "ad.$idx";

	$res{'region'} = $ads{"$prefix.ad.region"};
	return () if !$res{'region'};

	$res{'city'} = $ads{"$prefix.ad.city"} if $ads{"$prefix.ad.city"};
	my $type = $ads{"$prefix.ad.type"};
	$res{'type'} = 's' if $type eq 'sell';
	$res{'type'} = 'k' if $type eq 'buy';
	$res{'type'} = 'b' if $type eq 'swap';
	$res{'type'} = 'u' if $type eq 'let';
	$res{'type'} = 'h' if $type eq 'rent';
	$res{'category'} = $ads{"$prefix.ad.category"};
	$res{'passwd'} = $ads{"$prefix.ad.passwd"};
	$res{'name'} = $ads{"$prefix.ad.name"};
	$res{'email'} = $ads{"$prefix.users.email"};
	undef($res{'multi_email'}); # XXX
	$res{'phone'} = $ads{"$prefix.ad.phone"};
	$res{'phone_hidden'} = $ads{"$prefix.ad.phone_hidden"};
	$res{'no_salesmen'} = $ads{"$prefix.ad.no_salesmen"};
	$res{'company_ad'} = $ads{"$prefix.ad.company_ad"};
	$res{'subject'} = $ads{"$prefix.ad.subject"};
	$res{'body'} = $ads{"$prefix.ad.body"};
	$res{'infopage'} = $ads{"$prefix.ad.infopage"} if $ads{"$prefix.ad.infopage"};
	$res{'infopage_title'} = $ads{"$prefix.ad.infopage_title"} if $ads{"$prefix.ad.infopage_title"};
	$res{'price'} = $ads{"$prefix.ad.price"} if $ads{"$prefix.ad.price"};
	$res{'image'} = $ads{"$prefix.ad.image"} if $ads{"$prefix.ad.image"};
	for (my $i = 0; $i <= 5; $i++) {
		last if !$ads{"$prefix.images.$i.name"};
		$res{"image_set"} = '1';
		my $j = $i + 1;
		$res{"image$j"} = $ads{"$prefix.images.$i.name"};
	}
	$res{'uid'} = $ads{"$prefix.users.uid"};
	$res{'list_id'} = $ads{"$prefix.ad.list_id"};
	$res{'prev_action_id'} = $ads{"$prefix.actions.action_id"};

	# Ad params.
	my $paramexp = "^$prefix\\.params\\.(.*)";
	foreach my $param (grep /$paramexp/, keys(%ads)) {
		$param =~ /$paramexp/;
		$res{$1} = $ads{$param};
	}

	return %res;
}

# Get hostname and IP-address for logging
my $host = hostname();
my $addr = inet_ntoa(scalar(gethostbyname($host)) || 'localhost');
my $crit = 0;

my $stdout = select(STDOUT);
$stdout->autoflush(1);
print "Username: ";
chomp(my $username = <STDIN>);
exit if !$username;
system("stty -echo");
print "Password: ";
chomp(my $passwd = <STDIN>);
system("stty echo");
print "\n";
exit if !$passwd;

my $sha1 = Digest::SHA1->new();
$sha1->add($passwd);
$passwd = $sha1->hexdigest;

my %login = trans('cmd' => 'authenticate', 
		  'remote_addr' => $addr,
		  'username' => $username,
		  'passwd' => $passwd);

die "Failed to login" if $login{'status'} ne 'TRANS_OK';

my $token = $login{'token'};

print "Email to move from: ";
chomp(my $from = <STDIN>);
exit if !$from;

my %ads = trans('cmd' => 'search_ads',
		'search_type' => 'email',
		'filter_name' => 'published',
		'email' => $from,
		'token' => $token,
		'remote_addr' => $addr);

die "Failed to find ads" if $ads{'status'} ne 'TRANS_OK';
$token = $ads{'token'};

print "$ads{'total'} active ads found.\nEmail to move to: ";
chomp(my $to = <STDIN>);
exit if !$to;

my $setcomp;
while (1) {
	print "Set company_ad? (set|clear|no) ";
	chomp($setcomp = <STDIN>);
	exit if !$setcomp;
	last if $setcomp =~ /^(set|clear|no)$/;
}

my $store;
my $i;
for ($i = 0; (my %ad = parse_ad($i, %ads)); $i++) {
	if ($setcomp eq 'set') {
		$ad{'company_ad'} = '1';
		if ($store) {
			$ad{'store'} = $store;
		} else {
			undef($ad{'store'});
		}
	} elsif ($setcomp eq 'clear') {
		$ad{'company_ad'} = '0';
		undef($ad{'store'});
	}
	$ad{'email'} = $to;
	$ad{'cmd'} = 'newad';
	$ad{'ad_type'} = 'edit';
	$ad{'token'} = $token;
	$ad{'remote_addr'} = $addr;
	$ad{'do_not_send_mail'} = '1';

	my %res = trans(%ad);
	die "Failed to move ad" if ($res{'status'} ne 'TRANS_OK');
	$token = $res{'token'};
	# XXX stores
}

print "$i ads moved ok.\n";
