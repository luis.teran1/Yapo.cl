<?
require_once('init.php');
require_once('bTransaction.php');
require_once('icalendar.php');

$username = '';
$password = '';
$token = '';
$admin_id = '';

$fp = fopen("php://stdin","r");
if ($fp) {
	// Get username
	while (!eregi("^[a-z]+$", $username)) {
		print "Username: ";
		ob_flush();flush();
		$username = trim(rtrim(fgetss($fp, 1024)));
	}

	// Get Password
	while (!eregi("^[a-h0-9]+$", $password)) {
		print "Password: ";
		ob_flush();flush();
		$password = sha1(trim(rtrim(fgetss($fp, 1024))));
	}

	fclose($fp);

	// Login
	$transaction = new bTransaction();
	$transaction->add_data('username', $username);
	$transaction->add_data('passwd', $password);
	$transaction->add_data('remote_addr', '127.0.0.1');
	$reply = $transaction->send_command('authenticate');
	if ($reply['status'] == 'TRANS_OK') {
		$token = $reply['token'];
		$admin_id = $reply['admin_id'];
	} else {
		die("ACCESS DENIED!");
	}

	// create an ical object
	$ical = new icalendar();

	// Get all times
	$transaction->reset();
	$transaction->add_data('token', $token);
	$transaction->add_data('remote_addr', '127.0.0.1');
	$transaction->add_data('query', 23);
	$reply = $transaction->send_command('websql');
	if ($reply['status'] == 'TRANS_OK') {
		$token = $reply['token'];
	} else {
		die("ACCESS DENIED!");
	}

	ksort($reply['result']);
	foreach ($reply['result'] as $key => $data) {
		$ical_event = new icalendar_event();
		$ical_event->register_attendee($data['name'], $data['email']);
		$ical_event->register('start', $data['start']);
		$ical_event->register('end', $data['end']);
		$ical->add_event($ical_event);
	}

	// Get all actions
	$transaction->reset();
	$transaction->add_data('token', $token);
	$transaction->add_data('remote_addr', '127.0.0.1');
	$transaction->add_data('query', 24);
	$reply = $transaction->send_command('websql');
	if ($reply['status'] == 'TRANS_OK') {
		$token = $reply['token'];
	} else {
		die("ACCESS DENIED!");
	}

	ksort($reply['result']);
	foreach ($reply['result'] as $key => $data) {
		$data = array_reverse($data);
		$ical_event = new icalendar_event();
		$ical_event->register_attendee($data['name'], $data['email']);
		$ical_event->register('start', $data['start']);
		$ical_event->register('end', date("Y-m-d h:i:s", strtotime($data['start']) + $data['end'] * 60 * 60));
		$ical_event->register('summary', ereg_replace("[\n\r]", " ", $data['summary']));
		$ical->add_event($ical_event);
	}

	// Logout
	$transaction->reset();
	$transaction->add_data('admin_id', $admin_id);
	$transaction->send_command('deauthenticate');

	// Now we are done, write the ics file
	print $ical->to_string();
}
?>
