#!/usr/bin/perl -w
use DBI;
use Getopt::Long;
use POSIX;
use locale;
#
# Pull out counts of existance of a specified adparam for all active ads of a given category or categories (comma-separated string)
#
# Example:
#   adparam_pop_stats.pl --category=55,57 --adparam=area --title="some areas"


my $pghost = "/home/eddy/src/1.58/pgsql0/"; # "10.0.101.36";
#my $pghost = "localhost"; # "10.0.101.36";
my $pgname = "blocketdb";
my $pguser = $ENV{'USER'};
my $pgpwd = "";
my $pgoptions = "";
my $debug = 1;
my $adparam = "area";
my $category = 0;
my $title = "";

setlocale(LC_ALL, "se_SV");

sub clean_exit {
	my $message = shift;

	$dbh->disconnect();
	die $message if (defined($message));
	exit;
}

GetOptions ("pghost=s" => \$pghost,
	    "pgoptions=s" => \$pgoptions,
	    "category=s" => \$category,
	    "adparam=s" => \$adparam,
	    "title=s" => \$title,
	    "debug" => \$debug);


$pgdsn = "DBI:Pg:dbname=$pgname;host=$pghost";
if ($pgoptions) {
	$pgdsn .= ";options='$pgoptions'";
}
$dbh = DBI->connect($pgdsn, $pguser, $pgpwd) or die("Could not connect to database.");

my $sql_category = ((index $category,",") == -1) ? "=".$category : " IN(".$category.")";
my $q1 = "SELECT COUNT(ad_id) AS cnt FROM ads WHERE category".$sql_category." AND status='active'";
my $q2 = "SELECT COUNT(ad_id) AS cnt FROM ads JOIN ad_params USING(ad_id) WHERE status='active' AND category".$sql_category." and ad_params.name='".$adparam."'";
my $sel_ads = $dbh->prepare($q1);
my $sel_adparams = $dbh->prepare($q2);

print $q1."..." if $debug;
$sel_ads->execute();
print "Done\n".$q2."..." if $debug;
$sel_adparams->execute();
print "Done\n" if $debug;

$cnt1 = $sel_ads->fetchrow_arrayref->[0];
$cnt2 = $sel_adparams->fetchrow_arrayref->[0];

print ($title ne "" ? $title : $category); print ": ".$cnt2." / ".$cnt1."\n";

$sel_ads->finish;
$sel_adparams->finish;

clean_exit;

