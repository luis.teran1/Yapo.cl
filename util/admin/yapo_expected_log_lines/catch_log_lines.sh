#!/bin/bash
### Usage: [PAST_DATE=(all|year|<YY>|month|<YYMM>|yesterday|now|<YYMMDD>)] [L_TYPE=unexpected] [LOG_NAMES="LOG1 LOG2"] ~/yapo_expected_log_lines/catch_log_lines.sh
### Examples:
### PAST_DATE=all ~/yapo_expected_log_lines/catch_log_lines.sh
### L_TYPE=unexpected LOG_NAMES="API_NEWAD NEWAD" ~/yapo_expected_log_lines/catch_log_lines.sh

# Path to all phrases files: *_log_{expected,hard_errors,soft_errors}.txt
EXPECTED_LOG_LINES_DIR=~/yapo_expected_log_lines
# Path to logs
LOGS_DIR=/opt/logs

# By default, catch hard errors
if [ -z "${L_TYPE}" ] ; then
    L_TYPE="hard_errors"
    GREP_OPT=""
    PHRASES_SUFF="hard_errors"
elif [ ${L_TYPE} == "unexpected" ] ; then
    GREP_OPT="-v"
    PHRASES_SUFF="*"
fi

# By default, catch lines on all logs but ip_log, last, mod_security and postgres
if [ -z "${LOG_NAMES}" ] ; then
    LOG_NAMES=$(echo $(cd ${EXPECTED_LOG_LINES_DIR}; ls *_log_${PHRASES_SUFF}.txt 2>&1 |grep -v "ip_log\|last\|mod_security\|postgres"|sed 's/_log_[^(log)].*//g;'|sort -u))
fi

# Today's logs are the only non-zipped logs
if [ -z "${PAST_DATE}" ] ; then
    CAT_CMD=cat
    LOG_FILES=$(date '+%Y%m%d').log
elif [ "${PAST_DATE}" == "now" ] ; then
    CAT_CMD=tac
    LOG_FILES=$(date '+%Y%m%d').log
else
    CAT_CMD=cat
    if [ "${PAST_DATE}" == "yesterday" ] ; then
	LOG_FILES=$(date '+%Y%m%d' --date='1 day ago').log
    elif [ "${PAST_DATE}" == "all" ] ; then
	LOG_FILES=*.log
    elif [ "${PAST_DATE}" == "year" ] ; then
	LOG_FILES=$(date '+%Y')*.log
    elif [ "${PAST_DATE}" == "month" ] ; then
	LOG_FILES=$(date '+%Y%m')*.log
    else
	LOG_FILES=${PAST_DATE}*.log
    fi
fi

# Create grep scripts to be used on each log type, ie. grep_trans_log_lines_{hard_errors,unexpected}.sh
for LOG_NAME in $(echo ${LOG_NAMES}) ; do

    NB_LINES=$(cat ${EXPECTED_LOG_LINES_DIR}/${LOG_NAME}_log_${PHRASES_SUFF}.txt 2> /dev/null |grep -c .)

    GREP_SH=${EXPECTED_LOG_LINES_DIR}/grep_${L_TYPE}_log_lines_${LOG_NAME}.sh
    rm -f ${GREP_SH}
    if [ ${NB_LINES} -gt 0 ] ; then

	echo "grep ${GREP_OPT} \"\\(\\" > ${GREP_SH}
	cat ${EXPECTED_LOG_LINES_DIR}/${LOG_NAME}_log_${PHRASES_SUFF}.txt 2> /dev/null |grep .|(
	    PIPE_SUFF="\\|" 
	    for X in $(seq 1 $NB_LINES) ; do
		read -r LINE
		if [ "$X" == "$NB_LINES" ] ; then 
		    PIPE_SUFF="" ;
		fi
		echo "${LINE}${PIPE_SUFF}\\" 
	    done
	) >> ${GREP_SH}
    echo "\)\"" >> ${GREP_SH}
    chmod a+x ${GREP_SH}
    fi

done

(echo -ne "### Usage: [PAST_DATE=(all|year|<YY>|month|<YYMM>|yesterday|now|<YYMMDD>)] [L_TYPE=unexpected] [LOG_NAMES=\"LOG1 LOG2\"] ~/yapo_expected_log_lines/catch_log_lines.sh\n\n"
echo -ne "## Finished generating grep files to check for ${L_TYPE} log lines in following logs:\n##  ${LOG_NAMES}\n\n" ;
echo -ne "## Will grep on following files: ${LOGS_DIR}/*/${LOG_FILES} using patterns that were found in following files: ${EXPECTED_LOG_LINES_DIR}/*_log_*.txt \n\n" ;
for LOG_NAME in ${LOG_NAMES} ; do
    if [ -e ${EXPECTED_LOG_LINES_DIR}/grep_${L_TYPE}_log_lines_${LOG_NAME}.sh ]; then
	for LOG_FILE in $(ls ${LOGS_DIR}/${LOG_NAME}/${LOG_FILES} 2> /dev/null) ; do
	    echo -ne "\n## ${CAT_CMD} $(basename ${LOG_FILE}) | grep_${L_TYPE}_log_lines_${LOG_NAME}.sh\n\n"
	    ${CAT_CMD} ${LOG_FILE} 2> /dev/null | ${EXPECTED_LOG_LINES_DIR}/grep_${L_TYPE}_log_lines_${LOG_NAME}.sh 2> /dev/null
        done
    fi
done
)|less

