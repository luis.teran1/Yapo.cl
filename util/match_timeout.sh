#!/bin/bash

# Runs command repeatedly until it succeeds or 30s have ellapsed

if [ "$3" == "" ] ; then
	echo "Usage: $0 command parameter1 parameter2"
	exit 2
fi

I=300
$1 $2 $3
RETVAL=$?
until [ "$RETVAL" == "0" -o "$I" == "0" ] ; do
	echo -n '.'
	sleep .1
	I=`expr $I - 1`
	$1 $2 $3
	RETVAL=$?
done
exit $RETVAL

