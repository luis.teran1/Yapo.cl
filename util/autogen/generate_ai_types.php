<?php
require_once("config.php");

$begin = '';
$begin .= '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
$begin .= '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' . PHP_EOL;
$begin .= '<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . PHP_EOL;
$begin .= '<head profile="http://selenium-ide.openqa.org/profiles/test-case">' . PHP_EOL;
$begin .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . PHP_EOL;
$begin .= '<title>Massive check of ai types by category</title>' . PHP_EOL;
$begin .= '</head>' . PHP_EOL;
$begin .= '<body>' . PHP_EOL;
$begin .= '<table cellpadding="1" cellspacing="1" border="1">' . PHP_EOL;
$begin .= '<thead>' . PHP_EOL;
$begin .= '<tr><td rowspan="1" colspan="3">Massive check of ai types by category</td></tr>' . PHP_EOL;
$begin .= '</thead><tbody>' . PHP_EOL;

$begin .= "<tr>" . PHP_EOL;
$begin .= "   <td>open</td>" . PHP_EOL;
$begin .= "   <td>/chile/</td>" . PHP_EOL;
$begin .= "   <td></td>" . PHP_EOL;
$begin .= "</tr>" . PHP_EOL;
$begin .= "<tr>". PHP_EOL
."        <td>clickAndWait</td>". PHP_EOL
."        <td>link=Publicar aviso</td>". PHP_EOL
."        <td></td>". PHP_EOL
."</tr>". PHP_EOL;

$types_cat_default = shell_exec("grep \"*.*.category_settings.types.2.default=\" $BCONF_CATEGORY | cut -d= -f2");

foreach ($category as $cat => $cat_friendly) {

	$cat_name = trim(shell_exec("grep \"*.*.cat.$cat.name=\" $BCONF_CATEGORY | cut -d= -f2"));

	$types_cat = shell_exec("grep \"*.*.category_settings.types.1.$cat.value=\" $BCONF_CATEGORY | cut -d= -f2");
	if (empty($types_cat)) 
		$types_cat = $types_cat_default;
	//if ($cat == '7020' || $cat == '7040' || $cat == '7060') // neither jobs nor services show type
	//	$types_cat = '';
	$types_cat = explode(',', trim($types_cat));

	$content = '';


	$content .= '<tr>'. PHP_EOL;
	$content .= '        <td>waitForCondition</td>'. PHP_EOL;
	$content .= '        <td>var el=selenium.browserbot.getCurrentWindow().document.getElementById(\'field_ad_type\');el.parentNode.removeChild(el);1==1;</td>'. PHP_EOL;
	$content .= "        <td>3000</td>". PHP_EOL;
	$content .= '</tr>'. PHP_EOL;

	$content .= '<tr>'. PHP_EOL;
	$content .= '        <td>select</td>'. PHP_EOL;
	$content .= '        <td>name=category_group</td>'. PHP_EOL;
	$content .= "        <td>label=$cat_name</td>". PHP_EOL;
	$content .= '</tr>'. PHP_EOL;

	$content .= '<tr>'. PHP_EOL;
	$content .= '        <td>waitForElementPresent</td>'. PHP_EOL;
	$content .= '        <td>//tr[@id=\'field_ad_type\']</td>'. PHP_EOL;
	$content .= "        <td></td>". PHP_EOL;
	$content .= '</tr>'. PHP_EOL;

	foreach ($list_types as $typ => $typ_name) {

		$content .= "<tr>". PHP_EOL;
		if (in_array($typ, $types_cat))
			$content .= "        <td>waitForElementPresent</td>". PHP_EOL;
		else
			$content .= "        <td>waitForElementNotPresent</td>". PHP_EOL;
		$content .= "        <td>//td[@id='type_container']/label[@for='r$typ']</td>". PHP_EOL;
		$content .= "        <td></td>". PHP_EOL;
		$content .= "</tr>". PHP_EOL;


		$content .= "<tr>". PHP_EOL;
		if (in_array($typ, $types_cat))
			$content .= "        <td>assertElementPresent</td>". PHP_EOL;
		else
			$content .= "        <td>assertElementNotPresent</td>". PHP_EOL;
		$content .= "        <td>//td[@id='type_container']/label[@for='r$typ']</td>". PHP_EOL;
		$content .= "        <td></td>". PHP_EOL;
		$content .= "</tr>". PHP_EOL;
	
		if (in_array($cat, $array_sell_not_visible) && $typ == 's') {
			$content .= "<tr>". PHP_EOL;
			$content .= "        <td>verifyNotVisible</td>". PHP_EOL;
			$content .= "        <td>//td[@id='type_container']/label[@for='r$typ']</td>". PHP_EOL;
			$content .= "        <td></td>". PHP_EOL;
			$content .= "</tr>". PHP_EOL;
		} 
	}

	$begin .=  $content;
}

$begin .= "</tbody></table>";
$begin .= "</body>";
$begin .= "</html>";

$file_name = "ai_type_categories_check.html" ;
file_put_contents($file_name, $begin);

