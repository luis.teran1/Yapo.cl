<?php
$region = array(
'1' => 'arica_parinacota',
'2' => 'tarapaca',
'3' => 'antofagasta',
'4' => 'atacama',
'5' => 'coquimbo',
'6' => 'valparaiso',
'7' => 'ohiggins',
'8' => 'maule',
'9' => 'biobio',
'10' => 'araucania',
'11' => 'los_rios',
'12' => 'los_lagos',
'13' => 'aisen',
'14' => 'magallanes_antartica',
'15' => 'region_metropolitana'
);

$category = array(
'1020' => 'departamentos_piezas',
'1040' => 'casas',
'1060' => 'oficinas',
'1080' => 'comercial_industrial',
'1100' => 'terrenos',
'1120' => 'estacionamientos_bodegas_otros',
'2020' => 'autos',
'2040' => 'camiones_furgones',
'2060' => 'motos',
'2080' => 'barcos_lanchas_aviones',
'2100' => 'accesorios_vehiculos',
'2120' => 'otros_vehiculos',
'5020' => 'muebles_hogar',
'5040' => 'electrodomesticos',
'5060' => 'jardin_herramientas',
#'5080' => 'cama_mesa_bano',
'5100' => 'moda_vestuario',
'5120' => 'embarazada_bebes_ninos',
'5140' => 'bolsos_bisuteria_accesorios',
#'5160' => 'bisuteria_relojes_joyas',
'6020' => 'deportes_gimnasia',
#'6040' => 'camping_caza_pesca',
'6060' => 'bicicletas_ciclismo',
'6080' => 'instrumentos_musicales',
'6100' => 'musica_peliculas',
'6120' => 'libros_revistas',
'6140' => 'animales_accesorios',
'6160' => 'arte_antiguedades_colecciones',
'6180' => 'hobbies_outdoor',
'6200' => 'salud_belleza',
'3020' => 'consolas_videojuegos',
'3040' => 'computadores',
'3060' => 'celulares',
'3080' => 'television_camaras',
'7020' => 'ofertas_de_empleo',
'7040' => 'busco_empleo',
'7060' => 'servicios',
'7080' => 'negocios_maquinaria_construccion',
'8020' => 'otros_productos'
);

$params = array(
'1020' => array('price','rooms','size','garage_spaces','condominio','communes','currency'),
'1040' => array('price','rooms','size','garage_spaces','condominio','communes','currency'),
'1060' => array('price','garage_spaces','communes','currency'),
'1080' => array('price','garage_spaces','communes','currency'),
'1100' => array('price','size','condominio','communes','currency'),
'1120' => array('price','size','garage_spaces','condominio','communes','currency'),
'2020' => array('price','mileage','regdate','cartype','gearbox','fuel'),
'2040' => array('price','mileage','regdate','gearbox','fuel'),
'2060' => array('price','mileage','regdate','cubiccms'),
#'5080' => array('condition'),
'5100' => array('condition','gender'),
'5120' => array('condition'),
'5140' => array('condition'),
'5160' => array('condition'),
'7020' => array('job_category'),
'7040' => array('job_category'),
'7060' => array('service_type','price'),
);

$params_required = array(
'1020' => array('rooms'),
'1040' => array('rooms'),
'1060' => array('currency'),
'1080' => array('currency'),
'1100' => array('currency'),
'1120' => array('currency'),
'2020' => array('mileage','regdate','cartype','gearbox','fuel'),
'2040' => array('mileage','regdate','gearbox','fuel'),
'2060' => array('mileage','regdate','cubiccms'),
#'5080' => array('condition'),
'5100' => array('condition'),
'5120' => array('condition'),
'5140' => array('condition'),
'5160' => array('condition'),
'7020' => array('job_category'),
'7040' => array('job_category'),
'7060' => array('service_type'),
);

$params_not_required = array(
'1020' => array('price','size','garage_spaces','condominio','communes','currency'),
'1040' => array('price','size','garage_spaces','condominio','communes','currency'),
'1060' => array('price','garage_spaces','communes'),
'1080' => array('price','garage_spaces','communes'),
'1100' => array('price','size','condominio','communes'),
'1120' => array('price','size','garage_spaces','condominio','communes'),
'2020' => array('price'),
'2040' => array('price'),
'2060' => array('price'),
//'5080' => array(),
'5100' => array('gender'),
//'5120' => array(),
//'5140' => array(),
//'5160' => array(),
//'7020' => array(),
//'7040' => array(),
'7060' => array('price'),
);

$price = array(10000, 500000000);
$rooms = array(1, 4);
$size = array(1, 999);
$garage_spaces = array(1, 10);
$condominio = array(1, 500);
//$communes = 2;
$mileage = array(0, 999999);
$regdate = array(1990, 2012);
$cartype = array(1, 5);
$gearbox = array(1, 2);
$fuel = array(1, 5);
$cubiccms = array(1, 7);
$condition = array(1, 2);
$gender = array(1, 3);
$job_category = array(1, 15);
$service_type = array(1, 11);
$currency = 'peso';

/* Just some random ip4 addresses for test purposes */
$ip_pool = array('226.132.168.36', '34.65.164.14', '116.33.108.156', '198.128.49.201', '125.233.21.191', '227.148.126.229', '185.131.200.140', '102.226.2.74', '104.170.109.137', '234.19.151.95', '51.5.250.248', '132.45.194.2', '23.215.192.249', '108.63.223.39', '194.169.178.42', '141.179.115.244', '94.223.127.73', '242.23.168.38', '27.163.32.158', '207.225.160.230');

$list_types = array('s' => 'Vendo', 'b' => 'Cambia', 'h' => 'Busco arriendo', 'k' => 'Busco', 'u' => 'Arriendo');
$BCONF_CATEGORY = '../../conf/bconf/bconf.txt.categories';
$array_sell_not_visible = array('7020', '7040', '7060');
?>
