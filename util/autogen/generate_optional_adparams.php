<?php

require_once("config.php");
require_once("bTransaction.php");
ini_set('memory_limit','1024M');

GLOBAL $BCONF;

$head_part = '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
$head_part .= ' <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">' . PHP_EOL;
$head_part .= ' <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">' . PHP_EOL;
$head_part .= ' <head profile="http://selenium-ide.openqa.org/profiles/test-case">' . PHP_EOL;
$head_part .= ' <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />' . PHP_EOL;
$head_part .= ''; 

$body_start = '<body>' . PHP_EOL;
$head_part .= ' <table cellpadding="1" cellspacing="1" border="1">' . PHP_EOL;
$head_part .= ' <thead>' . PHP_EOL;
$head_part .= ' <tr><td rowspan="1" colspan="3">' . PHP_EOL;

$content = '';
foreach ($region as $reg => $reg_name) { 
	foreach ($category as $cat => $cat_name) {
		$content .= $head_part . PHP_EOL;
		$content .= "<title>Massive adparam requirement testing $reg_name $cat_name</title>" . PHP_EOL;
		$content .= '</head>' . PHP_EOL;
		$content .= $body_start . PHP_EOL;
		$content .= "Massive adparam req. $reg_name $cat_name</td></tr>" . PHP_EOL;
		$content .= "</thead><tbody>\n\n" . PHP_EOL;
		$content .= "<tr>" . PHP_EOL;
		$content .= "	<td>open</td>" . PHP_EOL;
		$content .= "	<td>/$reg_name/$cat_name</td>" . PHP_EOL;
		$content .= "	<td></td>" . PHP_EOL;
		$content .= "</tr>" . PHP_EOL;
		$content .= "<tr>" . PHP_EOL;
		$content .= "	<td>clickAndWait</td>" . PHP_EOL;
 		$content .= "	<td>//table[@id='hl']/tbody/tr[1]/td[3]/a</td>" . PHP_EOL;
		$content .= "	<td></td>" . PHP_EOL;
		$content .= "</tr>" . PHP_EOL;

		$i=0;
		while (isset($params_required[$cat][$i])) {
			$param = $params_required[$cat][$i++];
			$label = bconf_get($BCONF, "*.label_settings." . $param . ".1.default");
			if (substr($label,0,6) == 'label:') {
				$comma = strpos($label, ',');
				if (!empty($comma))
					$label = substr($label,6,$comma-6);	
				else
					$label = substr($label,6);
				$content .= "<tr>" . PHP_EOL;
				$content .= "	<td>verifyTextPresent</td>" . PHP_EOL;
				$content .= "	<td>$label:</td>" . PHP_EOL;
				$content .= "	<td></td>" . PHP_EOL;
				$content .= "</tr>" . PHP_EOL;
			}
		}
		$i=0;
		while (isset($params_not_required[$cat][$i])) {
			$param = $params_not_required[$cat][$i++];
			$label = bconf_get($BCONF, "*.label_settings." . $param . ".1.default");
			if (substr($label,0,6) == 'label:') {
				$comma = strpos($label, ',');
				if (!empty($comma))
					$label = substr($label,6,$comma-6);	
				else
					$label = substr($label,6);
				$content .= "<tr>" . PHP_EOL;
				$content .= "	<td>verifyTextNotPresent</td>" . PHP_EOL;
				$content .= "	<td>$label:</td>" . PHP_EOL;
				$content .= "	<td></td>" . PHP_EOL;
				$content .= "</tr>" . PHP_EOL;
			}
		}

		$content .= "</tbody></table>\n</body>\n</html>\n" . PHP_EOL;

		$file_name = "adparam_req_reg_$reg" . "_$cat.html" ;
		file_put_contents($file_name, $content);
		$add_line = "<tr><td><a href=$file_name>check friendly $reg_name $cat_name</a></td></tr>" . PHP_EOL;
		echo $add_line;
		$content = '';
	}
}

?>
