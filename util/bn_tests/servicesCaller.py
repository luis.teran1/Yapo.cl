#!/usr/bin/env python
# coding: iso-8859-1

import sys
from optparse import OptionParser
import commands
###############################################################################

Dav_path = '/opt/blocket/lib/python/2';

def main():

	global host;
	global port;
	parser = OptionParser();
	parser.add_option("-a", "--address", dest="host", help="Service host ");
	parser.add_option("-p", "--port", dest="port", help="Service port");
	parser.add_option("-s", "--service", dest="service", help="[trans | asearch | zsearch | dav]");
	(options, args) = parser.parse_args();

	if not options.host:
		print 'Service host required';
		sys.exit(1);
	
	if not options.port:
		print 'Service port required';
		sys.exit(1);
	
	if not options.service:
		print 'Service name required. [trans | asearch | zsearch | dav]';
		sys.exit(1);

	host = options.host;
	port = options.port;

	if options.service == 'trans':	 execute_trans(); sys.exit(0);
	if options.service == 'asearch': execute_asearch(); sys.exit(0);
	if options.service == 'zsearch': execute_zsearch(); sys.exit(0);
	if options.service == 'dav':   	 execute_dav(); sys.exit(0);
	
	print 'Invalid service name. Valid names: [trans | asearch | zsearch | dav]';

#END_main

def execute_trans():

	(_status, _output) = commands.getstatusoutput('printf \"cmd:bconf\\ncommit:1\\nend\\n\" | nc %s %s | grep ^status:' % (host, port));
	if _status != 0 or _output != 'status:TRANS_OK':
		print '[ERROR]: Fail executing Trans test. [%s]' %  _output;
		return;
	
	print '[OK]';
	
#END_execute_trans

def execute_asearch():
	(_status, _output) = commands.getstatusoutput('printf \"0 lim:1 region:11 *:*\" | nc %s %s| grep ^info:lines:' % (host, port));
	if _status != 0 or _output != 'info:lines:1':
		print '[ERROR]: Fail executing Asearch test. [%s]' %  _output;
		return;
	
	print '[OK]';
#END_execute_asearch

def execute_zsearch():
	(_status, _output) = commands.getstatusoutput('printf \"0 lim:1 cep_min:-22261001\" | nc %s %s| grep ^info:lines:' % (host, port));
	if _status != 0 or _output != 'info:lines:1':
		print '[ERROR]: Fail executing Zsearch test. [%s]' %  _output;
		return;
	
	print '[OK]';

#END_execute_zsearch

def execute_dav():

	sys.path.append(Dav_path);
#	print 'Using dav module in %s' % Dav_path;

	import PyDAV_0_21.client;	
	client = PyDAV_0_21.client;	

	res = client.Resource('http://%s:%s/images/45/4549315353.jpg' % (host, port));

	response = res.get();
	response_code = int(response.get_status().split()[0]);
	if (response_code < 200) or (response_code > 300):
		print '[ERROR]: Fail executing Dav test. [%s]' % response.get_status();
		return;

	print '[OK]';

#END_execute_dav


if __name__ == '__main__':
    main();

