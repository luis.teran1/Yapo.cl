#!/usr/bin/env python
# coding: iso-8859-1

import sys
import os.path
from optparse import OptionParser
import re

### TODO: 
# implement debug mode if needed
# better path management
# Add headers to custom files!!

### Defines
ENCODING="iso-8859-1"

###  Add input parameters: Debug mode, output directory, ...
parser = OptionParser()
parser.add_option("-d", "--debug", dest="debug", action="store_true",
                  help="Enable debug mode")
parser.add_option("-t", "--target",  dest="target_dir", default="./",
                  help="Target directory for test files, usually xxxx/util/selenium-core/blocket-test")
(options, args) = parser.parse_args()

LINKSELVES_BY_LINE_BRAZIL=4
LINKSELVES_BY_LINE_DEFAULT=3

states = {
		'sao_paulo':{
			'id':1,
			'regions':['Vale do Para�ba e Litoral Norte', 'Baixada Santista e Litoral Sul', 'Bauru e Mar�lia', 'Sorocaba', 'Ribeir�o Preto', 'S�o Jos� do Rio Preto', 'Presidente Prudente', 'Grande Campinas'],
			'short_name':'SP',
			'single_region':0,
			'linkshelves_per_line':2
		},
		'minas_gerais':{
			'id':2,
			'regions':['Grande Belo Horizonte', 'Juiz de Fora', 'Gov. Valadares, T. Otoni e regi�o', 'Uberl�ndia e Uberaba', 'Po�os de Caldas e Varginha', 'Divin�polis', 'Mtes Claros, Diamantina e regi�o'], 
			'short_name':'MG', 
                        'single_region':0,
                        'linkshelves_per_line':2
		},
		'rio_de_janeiro':{
			'id':3,
			'regions':['Grande Rio de Janeiro', 'Norte do Estado do Rio', 'Serra, Volta Redonda e Angra dos Reis'], 
			'short_name':'RJ',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'bahia':{
			'id':4,
			'regions':['Grande Salvador', 'Sul da Bahia', 'Juazeiro e Jacobina', 'Feira de Santana e Alagoinhas', 'V da Conquista, Barreiras'], 
			'short_name':'BA',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'rio_grande_do_sul':{
			'id':5,
			'regions':['Porto Alegre, Torres e Santa Cruz do Sul', 'Pelotas, Rio Grande e Bag�', 'Caxias do Sul e Passo Fundo', 'Santa Maria, Uruguaiana e Cruz Alta'], 
			'short_name':'RS',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'parana':{
			'id':6,
			'regions':['Curitiba e Paranagu�', 'Ponta Grossa e Guarapuava', 'Londrina', 'Maring�', 'Foz do Igua�u e Cascavel', 'Francisco Beltr�o e Pato Branco'], 
			'short_name':'PR',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'pernambuco':{
			'id':7,
			'regions':['Petrolina e Garanhuns','Grande Recife'],
			'short_name':'PE',
                        'single_region':0
		},
		'ceara':{
			'id':8,
			'regions':['Grande Fortaleza', 'Juazeiro do Norte e Sobral'], 
			'short_name':'CE',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'para':{
			'id':9,
			'regions':['Bel�m', 'Santar�m', 'Marab�'], 
			'short_name':'PA',
                        'single_region':0
		},
		'maranhao':{
			'id':10,
			'regions':['S�o Lu�s', 'Imperatriz e Caxias'], 
			'short_name':'MT', # Workaround because a bad sort in page, MA <-> MT
                        'single_region':0
		},
		'santa_catarina':{
			'id':11,
			'regions':['Norte de Santa Catarina', 'Sul de Santa Catarina', 'Oeste de Santa Catarina'], 
			'short_name':'SC',
                        'single_region':0
		},
		'goias':{
			'id':12,
			'regions':['Grande Goi�nia e An�polis', 'Rio Verde e Caldas Novas'], 
			'short_name':'GO',
                        'single_region':0,
			'linkshelves_per_line':2
		},
		'paraiba':{
			'id':13,
			'regions':['Para�ba'], 
			'short_name':'glob:* Para�ba',
                        'single_region':1
		},
		'espirito_santo':{
			'id':14,
			'regions':['Norte do Esp�rito Santo', 'Sul do Esp�rito Santo'], 
			'short_name':'ES',
                        'single_region':0
		},
		'amazonas':{
			'id':15,
			'regions':['Manaus', 'Leste do Amazonas'],
			'short_name':'glob:* Amap�', # Workaround because these two states are incorrectly sorted
                        'single_region':1
		},
		'alagoas':{
			'id':16,
			'regions':['Alagoas'], 
			'short_name':'glob:* Alagoas', 
                        'single_region':1
		},
		'piaui':{
			'id':17,
			'regions':['Teresina e Parna�ba', 'Picos e Floriano'], 
			'short_name':'PI',
                        'single_region':0
		},
		'rio_grande_do_norte':{
			'id':18,
			'regions':['Rio Grande do Norte'], 
			'short_name':'glob:* Rio Grande do Norte',
                        'single_region':1
		},
		'mato_grosso':{
			'id':19,
			'regions':['Cuiab�', 'Rondon�polis e Sinop'], 
			'short_name':'glob:* Mato Grosso do Sul', # Workaround because a bad sort in page, MA <-> MT
                        'single_region':1
		},
		'distrito_federal':{
			'id':20,
			'regions':['Grande Bras�lia'],
			'short_name':'glob:* Bras�lia e regi�o',
                        'single_region':1
		},
		'mato_grosso_do_sul':{
			'id':21,
			'regions':['Mato Grosso do Sul'],
			'short_name':'MA', # Workaround because a bad sort in page, MA <-> MT
                        'single_region':0 
		},
		'sergipe':{
			'id':22,
			'regions':['Sergipe'], 
			'short_name':'glob:* Sergipe',
                        'single_region':1
		},
		'rondonia':{
			'id':23,
			'regions':['Rond�nia'], 
			'short_name':'glob:* Rond�nia',
                        'single_region':1
		},
		'tocantins':{
			'id':24,
			'regions':['Tocantins'], 
			'short_name':'glob:* Tocantins',
                        'single_region':1
		},
		'acre':{
			'id':25,
			'regions':['Acre'], 
			'short_name':'glob:* Acre',
                        'single_region':1
		},
		'amapa':{
			'id':26,
			'regions':['Amap�'], 
			'short_name':'AM', # Workaround because this state are incorrectly sorted with Amazonas
                        'single_region':0
		},
		'roraima':{
			'id':27,
			'regions':['Roraima'],
			'short_name':'glob:* Roraima',
                        'single_region':1
		}
	}
empty_regions = ['Gov. Valadares, T. Otoni e regi�o', 'Mtes Claros, Diamantina e regi�o', 'Sul da Bahia',
		 'V da Conquista, Barreiras', 'Marab�', 'Leste do Amazonas'] 

regions_nearby = {
	'Grande S�o Paulo' : { 
		'nearby_regions' :[ 'Grande S�o Paulo','Vale do Para�ba e Litoral Norte','Baixada Santista e Litoral Sul','Sorocaba','Grande Campinas','Po�os de Caldas e Varginha'],
		'friendly' : 'grande_sao_paulo',
		'friendly_state' : 'sao_paulo',
		'linkshelves_per_line':2
	}, 
	'Vale do Para�ba e Litoral Norte' : { 
		'nearby_regions' :[ 'Vale do Para�ba e Litoral Norte','Grande S�o Paulo','Baixada Santista e Litoral Sul','Serra, Volta Redonda e Angra dos Reis','Po�os de Caldas e Varginha'],
		'friendly' : 'vale_do_paraiba_e_litoral_norte',
		'friendly_state' : 'sao_paulo',
		'linkshelves_per_line':2
	}, 
	'Baixada Santista e Litoral Sul' : { 
		'nearby_regions' :[ 'Baixada Santista e Litoral Sul','Grande S�o Paulo','Vale do Para�ba e Litoral Norte','Sorocaba','Curitiba e Paranagu�'],
		'friendly' : 'baixada_santista_e_litoral_sul',
		'friendly_state' : 'sao_paulo',
		'linkshelves_per_line':2
	}, 
	'Bauru e Mar�lia' : { 
		'nearby_regions' :[ 'Bauru e Mar�lia','Sorocaba','Ribeir�o Preto','S�o Jos� do Rio Preto','Presidente Prudente','Grande Campinas','Londrina'],
		'friendly' : 'regiao_de_bauru_e_marilia',
		'friendly_state' : 'sao_paulo',
	}, 
	'Sorocaba' : { 
		'nearby_regions' :[ 'Sorocaba','Grande S�o Paulo','Baixada Santista e Litoral Sul','Bauru e Mar�lia','Grande Campinas','Curitiba e Paranagu�','Londrina'],
		'friendly' : 'regiao_de_sorocaba',
		'friendly_state' : 'sao_paulo',
		'linkshelves_per_line':2
	}, 
	'Ribeir�o Preto' : { 
		'nearby_regions' :[ 'Ribeir�o Preto','Bauru e Mar�lia','S�o Jos� do Rio Preto','Grande Campinas','Uberl�ndia e Uberaba','Po�os de Caldas e Varginha'],
		'friendly' : 'regiao_de_ribeirao_preto',
		'friendly_state' : 'sao_paulo',
	}, 
	'S�o Jos� do Rio Preto' : { 
		'nearby_regions' :[ 'S�o Jos� do Rio Preto','Bauru e Mar�lia','Ribeir�o Preto','Presidente Prudente','Uberl�ndia e Uberaba','Mato Grosso do Sul'],
		'friendly' : 'regiao_de_sao_jose_do_rio_preto',
		'friendly_state' : 'sao_paulo',
	}, 
	'Presidente Prudente' : { 
		'nearby_regions' :[ 'Presidente Prudente','Bauru e Mar�lia','S�o Jos� do Rio Preto','Londrina','Maring�','Mato Grosso do Sul'],
		'friendly' : 'regiao_de_presidente_prudente',
		'friendly_state' : 'sao_paulo',
	}, 
	'Grande Campinas' : { 
		'nearby_regions' :[ 'Grande Campinas','Grande S�o Paulo','Bauru e Mar�lia','Sorocaba','Ribeir�o Preto','Po�os de Caldas e Varginha'],
		'friendly' : 'grande_campinas',
		'friendly_state' : 'sao_paulo',
	}, 
	'Grande Rio de Janeiro' : { 
		'nearby_regions' :[ 'Grande Rio de Janeiro','Norte do Estado do Rio','Serra, Volta Redonda e Angra dos Reis'],
		'friendly' : 'grande_rio_de_janeiro',
		'friendly_state' : 'rio_de_janeiro',
		'linkshelves_per_line':2
	}, 
	'Norte do Estado do Rio' : { 
		'nearby_regions' :[ 'Norte do Estado do Rio','Grande Rio de Janeiro','Serra, Volta Redonda e Angra dos Reis','Sul do Esp�rito Santo','Juiz de Fora'],
		'friendly' : 'norte_do_estado_do_rio',
		'friendly_state' : 'rio_de_janeiro',
		'linkshelves_per_line':2
	}, 
	'Serra, Volta Redonda e Angra dos Reis' : { 
		'nearby_regions' :[ 'Serra, Volta Redonda e Angra dos Reis','Vale do Para�ba e Litoral Norte','Grande Rio de Janeiro','Norte do Estado do Rio','Juiz de Fora','Po�os de Caldas e Varginha'],
		'friendly' : 'regioes_da_serra__volta_redonda_e_angra_dos_reis',
		'friendly_state' : 'rio_de_janeiro',
		'linkshelves_per_line':2
	}, 
	'Norte do Esp�rito Santo' : { 
		'nearby_regions' :[ 'Norte do Esp�rito Santo','Sul do Esp�rito Santo','Gov. Valadares, T. Otoni e regi�o','Sul da Bahia'],
		'friendly' : 'norte_do_espirito_santo',
		'friendly_state' : 'espirito_santo',
		'linkshelves_per_line':2
	}, 
	'Sul do Esp�rito Santo' : { 
		'nearby_regions' :[ 'Sul do Esp�rito Santo','Norte do Estado do Rio','Norte do Esp�rito Santo','Juiz de Fora','Gov. Valadares, T. Otoni e regi�o'],
		'friendly' : 'sul_do_espirito_santo',
		'friendly_state' : 'espirito_santo',
		'linkshelves_per_line':2
	}, 
	'Grande Belo Horizonte' : { 
		'nearby_regions' :[ 'Grande Belo Horizonte','Juiz de Fora','Gov. Valadares, T. Otoni e regi�o','Po�os de Caldas e Varginha','Divin�polis','Mtes Claros, Diamantina e regi�o'],
		'friendly' : 'grande_belo_horizonte',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Juiz de Fora' : { 
		'nearby_regions' :[ 'Juiz de Fora','Norte do Estado do Rio','Serra, Volta Redonda e Angra dos Reis','Sul do Esp�rito Santo','Grande Belo Horizonte','Gov. Valadares, T. Otoni e regi�o','Po�os de Caldas e Varginha','Divin�polis'],
		'friendly' : 'regiao_de_juiz_de_fora',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Governador Valadares e Te�filo Otoni' : { 
		'nearby_regions' :[ 'Gov. Valadares, T. Otoni e regi�o','Norte do Esp�rito Santo','Sul do Esp�rito Santo','Grande Belo Horizonte','Juiz de Fora','Mtes Claros, Diamantina e regi�o','Sul da Bahia','V da Conquista, Barreiras'],
		'friendly' : 'regiao_de_governador_valadares_e_teofilo_otoni',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Uberl�ndia e Uberaba' : { 
		'nearby_regions' :[ 'Uberl�ndia e Uberaba','Ribeir�o Preto','S�o Jos� do Rio Preto','Po�os de Caldas e Varginha','Divin�polis','Mtes Claros, Diamantina e regi�o','Rio Verde e Caldas Novas','Mato Grosso do Sul'],
		'friendly' : 'regiao_de_uberlandia_e_uberaba',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Po�os de Caldas e Varginha' : { 
		'nearby_regions' :[ 'Po�os de Caldas e Varginha','Grande S�o Paulo','Vale do Para�ba e Litoral Norte','Ribeir�o Preto','Grande Campinas','Serra, Volta Redonda e Angra dos Reis','Juiz de Fora','Uberl�ndia e Uberaba','Divin�polis'],
		'friendly' : 'regiao_de_pocos_de_caldas_e_varginha',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Divin�polis' : { 
		'nearby_regions' :[ 'Divin�polis','Grande Belo Horizonte','Juiz de Fora','Uberl�ndia e Uberaba','Po�os de Caldas e Varginha','Mtes Claros, Diamantina e regi�o'],
		'friendly' : 'regiao_de_divinopolis',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Montes Claros e Diamantina' : { 
		'nearby_regions' :[ 'Mtes Claros, Diamantina e regi�o','Grande Belo Horizonte','Gov. Valadares, T. Otoni e regi�o','Uberl�ndia e Uberaba','Divin�polis','Grande Bras�lia','Grande Goi�nia e An�polis','Rio Verde e Caldas Novas','V da Conquista, Barreiras'],
		'friendly' : 'regiao_de_montes_claros_e_diamantina',
		'friendly_state' : 'minas_gerais',
		'linkshelves_per_line':2
	}, 
	'Curitiba e Paranagu�' : { 
		'nearby_regions' :[ 'Curitiba e Paranagu�','Baixada Santista e Litoral Sul','Sorocaba','Ponta Grossa e Guarapuava','Londrina','Norte de Santa Catarina'],
		'friendly' : 'regiao_de_curitiba_e_paranagua',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Ponta Grossa e Guarapuava' : { 
		'nearby_regions' :[ 'Ponta Grossa e Guarapuava','Curitiba e Paranagu�','Londrina','Maring�','Foz do Igua�u e Cascavel','Francisco Beltr�o e Pato Branco','Norte de Santa Catarina','Oeste de Santa Catarina'],
		'friendly' : 'regiao_de_ponta_grossa_e_guarapuava',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Londrina' : { 
		'nearby_regions' :[ 'Londrina','Bauru e Mar�lia','Sorocaba','Presidente Prudente','Curitiba e Paranagu�','Ponta Grossa e Guarapuava','Maring�'],
		'friendly' : 'regiao_de_londrina',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Maring�' : { 
		'nearby_regions' :[ 'Maring�','Presidente Prudente','Ponta Grossa e Guarapuava','Londrina','Foz do Igua�u e Cascavel','Mato Grosso do Sul'],
		'friendly' : 'regiao_de_maringa',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Foz do Igua�u e Cascavel' : { 
		'nearby_regions' :[ 'Foz do Igua�u e Cascavel','Ponta Grossa e Guarapuava','Maring�','Francisco Beltr�o e Pato Branco'],
		'friendly' : 'regiao_de_foz_do_iguacu_e_cascavel',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Francisco Beltr�o e Pato Branco' : { 
		'nearby_regions' :[ 'Francisco Beltr�o e Pato Branco','Ponta Grossa e Guarapuava','Foz do Igua�u e Cascavel','Oeste de Santa Catarina'],
		'friendly' : 'regiao_de_francisco_beltrao_e_pato_branco',
		'friendly_state' : 'parana',
		'linkshelves_per_line':2
	}, 
	'Norte de Santa Catarina' : { 
		'nearby_regions' :[ 'Norte de Santa Catarina','Curitiba e Paranagu�','Ponta Grossa e Guarapuava','Sul de Santa Catarina','Oeste de Santa Catarina'],
		'friendly' : 'norte_de_santa_catarina',
		'friendly_state' : 'santa_catarina',
		'linkshelves_per_line':2
	}, 
	'Sul de Santa Catarina' : { 
		'nearby_regions' :[ 'Sul de Santa Catarina','Norte de Santa Catarina','Oeste de Santa Catarina','Porto Alegre, Torres e Santa Cruz do Sul','Caxias do Sul e Passo Fundo'],
		'friendly' : 'sul_de_santa_catarina',
		'friendly_state' : 'santa_catarina',
		'linkshelves_per_line':2
	}, 
	'Oeste de Santa Catarina' : { 
		'nearby_regions' :[ 'Oeste de Santa Catarina','Ponta Grossa e Guarapuava','Francisco Beltr�o e Pato Branco','Norte de Santa Catarina','Sul de Santa Catarina','Caxias do Sul e Passo Fundo','Santa Maria, Uruguaiana e Cruz Alta'],
		'friendly' : 'oeste_de_santa_catarina',
		'friendly_state' : 'santa_catarina',
		'linkshelves_per_line':2
	}, 
	'Porto Alegre, Torres e Santa Cruz do Sul' : { 
		'nearby_regions' :[ 'Porto Alegre, Torres e Santa Cruz do Sul','Sul de Santa Catarina','Pelotas, Rio Grande e Bag�','Caxias do Sul e Passo Fundo','Santa Maria, Uruguaiana e Cruz Alta'],
		'friendly' : 'regioes_de_porto_alegre__torres_e_santa_cruz_do_sul',
		'friendly_state' : 'rio_grande_do_sul',
		'linkshelves_per_line':2
	}, 
	'Pelotas, Rio Grande e Bag�' : { 
		'nearby_regions' :[ 'Pelotas, Rio Grande e Bag�','Porto Alegre, Torres e Santa Cruz do Sul','Santa Maria, Uruguaiana e Cruz Alta'],
		'friendly' : 'regioes_de_pelotas__rio_grande_e_bage',
		'friendly_state' : 'rio_grande_do_sul',
		'linkshelves_per_line':2
	}, 
	'Caxias do Sul e Passo Fundo' : { 
		'nearby_regions' :[ 'Caxias do Sul e Passo Fundo','Sul de Santa Catarina','Oeste de Santa Catarina','Porto Alegre, Torres e Santa Cruz do Sul','Santa Maria, Uruguaiana e Cruz Alta'],
		'friendly' : 'regioes_de_caxias_do_sul_e_passo_fundo',
		'friendly_state' : 'rio_grande_do_sul',
		'linkshelves_per_line':2
	}, 
	'Santa Maria, Uruguaiana e Cruz Alta' : { 
		'nearby_regions' :[ 'Santa Maria, Uruguaiana e Cruz Alta','Oeste de Santa Catarina','Porto Alegre, Torres e Santa Cruz do Sul','Pelotas, Rio Grande e Bag�','Caxias do Sul e Passo Fundo'],
		'friendly' : 'regioes_de_santa_maria__uruguaiana_e_cruz_alta',
		'friendly_state' : 'rio_grande_do_sul',
		'linkshelves_per_line':2
	}, 
	'Grande Bras�lia' : { 
		'nearby_regions' :[ 'Grande Bras�lia','Mtes Claros, Diamantina e regi�o','Grande Goi�nia e An�polis','Rio Verde e Caldas Novas'],
		'friendly' : 'grande_brasilia',
		'friendly_state' : 'distrito_federal',
		'linkshelves_per_line':2
	}, 
	'Grande Goi�nia e An�polis' : { 
		'nearby_regions' :[ 'Grande Goi�nia e An�polis','Mtes Claros, Diamantina e regi�o','Grande Bras�lia','Tocantins','Rio Verde e Caldas Novas','Rondon�polis e Sinop','V da Conquista, Barreiras'],
		'friendly' : 'grande_goiania_e_anapolis',
		'friendly_state' : 'goias',
		'linkshelves_per_line':2
	}, 
	'Tocantins' : { 
		'nearby_regions' :[ 'Tocantins','Grande Goi�nia e An�polis','Rondon�polis e Sinop','V da Conquista, Barreiras','Picos e Floriano','Marab�','Imperatriz e Caxias'],
		'friendly' : 'tocantins',
		'friendly_state' : 'tocantins',
	}, 
	'Rio Verde e Caldas Novas' : { 
		'nearby_regions' :[ 'Rio Verde e Caldas Novas','Uberl�ndia e Uberaba','Mtes Claros, Diamantina e regi�o','Grande Bras�lia','Grande Goi�nia e An�polis','Rondon�polis e Sinop','Mato Grosso do Sul'],
		'friendly' : 'regiao_de_rio_verde_e_caldas_novas',
		'friendly_state' : 'goias',
		'linkshelves_per_line':2
	}, 
	'Cuiab�' : { 
		'nearby_regions' :[ 'Cuiab�','Rondon�polis e Sinop','Mato Grosso do Sul','Rond�nia'],
		'friendly' : 'regiao_de_cuiaba',
		'friendly_state' : 'mato_grosso',
	}, 
	'Rondon�polis e Sinop' : { 
		'nearby_regions' :[ 'Rondon�polis e Sinop','Grande Goi�nia e An�polis','Tocantins','Rio Verde e Caldas Novas','Cuiab�','Mato Grosso do Sul','Rond�nia','Manaus','Santar�m','Marab�','Leste do Amazonas'],
		'friendly' : 'regiao_de_rondonopolis_e_sinop',
		'friendly_state' : 'mato_grosso',
		'linkshelves_per_line':2
	}, 
	'Mato Grosso do Sul' : { 
		'nearby_regions' :[ 'Mato Grosso do Sul','S�o Jos� do Rio Preto','Presidente Prudente','Uberl�ndia e Uberaba','Maring�','Rio Verde e Caldas Novas','Cuiab�','Rondon�polis e Sinop'],
		'friendly' : 'mato_grosso_do_sul',
		'friendly_state' : 'mato_grosso_do_sul',
		'linkshelves_per_line':2
	}, 
	'Acre' : { 
		'nearby_regions' :[ 'Acre','Rond�nia','Leste do Amazonas'],
		'friendly' : 'acre',
		'friendly_state' : 'acre',
	}, 
	'Rond�nia' : { 
		'nearby_regions' :[ 'Rond�nia','Cuiab�','Rondon�polis e Sinop','Acre','Leste do Amazonas'],
		'friendly' : 'rondonia',
		'friendly_state' : 'rondonia',
	}, 
	'Grande Salvador' : { 
		'nearby_regions' :[ 'Grande Salvador','Feira de Santana e Alagoinhas'],
		'friendly' : 'grande_salvador',
		'friendly_state' : 'bahia',
		'linkshelves_per_line':2
	}, 
	'Sul da Bahia' : { 
		'nearby_regions' :[ 'Sul da Bahia','Norte do Esp�rito Santo','Gov. Valadares, T. Otoni e regi�o','Feira de Santana e Alagoinhas','V da Conquista, Barreiras'],
		'friendly' : 'sul_da_bahia',
		'friendly_state' : 'bahia',
		'linkshelves_per_line':2
	}, 
	'Juazeiro e Jacobina' : { 
		'nearby_regions' :[ 'Juazeiro e Jacobina','Feira de Santana e Alagoinhas','V da Conquista, Barreiras','Petrolina e Garanhuns','Picos e Floriano'],
		'friendly' : 'regiao_de_juazeiro_e_jacobina',
		'friendly_state' : 'bahia',
		'linkshelves_per_line':2
	}, 
	'Feira de Santana e Alagoinhas' : { 
		'nearby_regions' :[ 'Feira de Santana e Alagoinhas','Grande Salvador','Sul da Bahia','Juazeiro e Jacobina','V da Conquista, Barreiras','Sergipe','Alagoas','Petrolina e Garanhuns'],
		'friendly' : 'regiao_de_feira_de_santana_e_alagoinhas',
		'friendly_state' : 'bahia',
		'linkshelves_per_line':2
	}, 
	'Vit�ria da Conquista e Barreiras' : { 
		'nearby_regions' :[ 'V da Conquista, Barreiras','Gov. Valadares, T. Otoni e regi�o','Mtes Claros, Diamantina e regi�o','Grande Goi�nia e An�polis','Tocantins','Sul da Bahia','Juazeiro e Jacobina','Feira de Santana e Alagoinhas','Picos e Floriano'],
		'friendly' : 'regiao_de_vitoria_da_conquista_e_barreiras',
		'friendly_state' : 'bahia',
		'linkshelves_per_line':2
	}, 
	'Sergipe' : { 
		'nearby_regions' :[ 'Sergipe','Feira de Santana e Alagoinhas','Alagoas'],
		'friendly' : 'sergipe',
		'friendly_state' : 'sergipe',
		'linkshelves_per_line':2
	}, 
	'Grande Recife' : { 
		'nearby_regions' :[ 'Grande Recife','Alagoas','Para�ba','Petrolina e Garanhuns'],
		'friendly' : 'grande_recife',
		'friendly_state' : 'pernambuco',
	}, 
	'Alagoas' : { 
		'nearby_regions' :[ 'Alagoas','Feira de Santana e Alagoinhas','Sergipe','Grande Recife','Petrolina e Garanhuns'],
		'friendly' : 'alagoas',
		'friendly_state' : 'alagoas',
		'linkshelves_per_line':2
	}, 
	'Para�ba' : { 
		'nearby_regions' :[ 'Para�ba','Grande Recife','Rio Grande do Norte','Petrolina e Garanhuns','Juazeiro do Norte e Sobral'],
		'friendly' : 'paraiba',
		'friendly_state' : 'paraiba',
		'linkshelves_per_line':2
	}, 
	'Rio Grande do Norte' : { 
		'nearby_regions' :[ 'Rio Grande do Norte','Para�ba','Juazeiro do Norte e Sobral'],
		'friendly' : 'rio_grande_do_norte',
		'friendly_state' : 'rio_grande_do_norte',
		'linkshelves_per_line':2
	}, 
	'Grande Fortaleza' : { 
		'nearby_regions' :[ 'Grande Fortaleza','Juazeiro do Norte e Sobral'],
		'friendly' : 'grande_fortaleza',
		'friendly_state' : 'ceara',
		'linkshelves_per_line':2
	}, 
	'Teresina e Parna�ba' : { 
		'nearby_regions' :[ 'Teresina e Parna�ba','Juazeiro do Norte e Sobral','Picos e Floriano','S�o Lu�s','Imperatriz e Caxias'],
		'friendly' : 'regiao_de_teresina_e_parnaiba',
		'friendly_state' : 'piaui',
		'linkshelves_per_line':2
	}, 
	'Petrolina e Garanhuns' : { 
		'nearby_regions' :[ 'Petrolina e Garanhuns','Juazeiro e Jacobina','Feira de Santana e Alagoinhas','Grande Recife','Alagoas','Para�ba','Juazeiro do Norte e Sobral','Picos e Floriano'],
		'friendly' : 'regiao_de_petrolina_e_garanhuns',
		'friendly_state' : 'pernambuco',
		'linkshelves_per_line':2
	}, 
	'Juazeiro do Norte e Sobral' : { 
		'nearby_regions' :[ 'Juazeiro do Norte e Sobral','Para�ba','Rio Grande do Norte','Grande Fortaleza','Teresina e Parna�ba','Petrolina e Garanhuns','Picos e Floriano'],
		'friendly' : 'regiao_de_juazeiro_do_norte_e_sobral',
		'friendly_state' : 'ceara',
		'linkshelves_per_line':2
	}, 
	'Picos e Floriano' : { 
		'nearby_regions' :[ 'Picos e Floriano','Tocantins','Juazeiro e Jacobina','V da Conquista, Barreiras','Teresina e Parna�ba','Petrolina e Garanhuns','Juazeiro do Norte e Sobral','Imperatriz e Caxias'],
		'friendly' : 'regiao_de_picos_e_floriano',
		'friendly_state' : 'piaui',
		'linkshelves_per_line':2
	}, 
	'Bel�m' : { 
		'nearby_regions' :[ 'Bel�m','Santar�m','Marab�','Amap�','S�o Lu�s'],
		'friendly' : 'regiao_de_belem',
		'friendly_state' : 'para',
	}, 
	'Manaus' : { 
		'nearby_regions' :[ 'Manaus','Rondon�polis e Sinop','Santar�m','Roraima','Leste do Amazonas'],
		'friendly' : 'regiao_de_manaus',
		'friendly_state' : 'amazonas',
	}, 
	'Santar�m' : { 
		'nearby_regions' :[ 'Santar�m','Rondon�polis e Sinop','Bel�m','Manaus','Marab�','Roraima','Amap�'],
		'friendly' : 'regiao_de_santarem',
		'friendly_state' : 'para',
	}, 
	'Marab�' : { 
		'nearby_regions' :[ 'Marab�','Tocantins','Rondon�polis e Sinop','Bel�m','Santar�m','S�o Lu�s','Imperatriz e Caxias'],
		'friendly' : 'regiao_de_maraba',
		'friendly_state' : 'para',
	}, 
	'Roraima' : { 
		'nearby_regions' :[ 'Roraima','Manaus','Santar�m','Leste do Amazonas'],
		'friendly' : 'roraima',
		'friendly_state' : 'roraima',
	}, 
	'Amap�' : { 
		'nearby_regions' :[ 'Amap�','Bel�m','Santar�m'],
		'friendly' : 'amapa',
		'friendly_state' : 'amapa',
	}, 
	'Leste do Amazonas' : { 
		'nearby_regions' :[ 'Leste do Amazonas','Rondon�polis e Sinop','Acre','Rond�nia','Manaus','Roraima'],
		'friendly' : 'leste_do_amazonas',
		'friendly_state' : 'amazonas',
	}, 
	'S�o Lu�s' : { 
		'nearby_regions' :[ 'S�o Lu�s','Teresina e Parna�ba','Bel�m','Marab�','Imperatriz e Caxias'],
		'friendly' : 'regiao_de_sao_luis',
		'friendly_state' : 'maranhao',
	}, 
	'Imperatriz e Caxias' : { 
		'nearby_regions' :[ 'Imperatriz e Caxias','Tocantins','Teresina e Parna�ba','Picos e Floriano','Marab�','S�o Lu�s'],
		'friendly' : 'regiao_de_imperatriz_e_caxias',
		'friendly_state' : 'maranhao',
	} 
}
### Used to add a Selenium command
def createSeleniumCommand( command, target, value="" ):
	return "<tr>\n\t<td>%s</td>\n\t<td>%s</td>\n\t<td>%s</td>\n</tr>\n" % (command, target, value )
			

# On with the show....
if os.path.exists(options.target_dir)==False:
	print "[ERROR] Target directory  does not exist or is not accesible..."
	sys.exit(-1)

# Open template files ...
openfile_list = []

try :
	header_file = open("selenium_test_templates/selenium_template_linkshelf_common.html", "r")
	openfile_list.append(header_file)
	header_data = header_file.read()

	region_file  = open("selenium_test_templates/selenium_template_linkshelf_by_region.html", "r")
	openfile_list.append(region_file)
	region_data = region_file.read()

	all_country_file  = open("selenium_test_templates/selenium_template_linkshelf_all_country.html", "r")
	openfile_list.append(all_country_file)
	all_country_data = all_country_file.read()
except:
	print "Failure opening template files: %s " % (sys.exc_info()[0] )
	for filename in openfile_list:
		filename.close()
	sys.exit(-1)

# "Done opening templates!

testfile_list = {}

for state_friendly_name,state_dict in states.iteritems():

	testfile_name = "check_%s_linkshelves.html" % state_friendly_name
	test_title = "Checking linkshelves in '%s' tab " % state_friendly_name

	# Create new test file
	if options.target_dir and os.path.exists(options.target_dir):
		testfile_name = options.target_dir + testfile_name
	testfile = open(testfile_name, "w" )

	# Add common headers
	testfile.write( header_data % (test_title, test_title, state_friendly_name) )

	# Get the number of columns per line
	try:
		linkselves_by_line = state_dict['linkshelves_per_line']
	except:
		linkselves_by_line = LINKSELVES_BY_LINE_DEFAULT

	number = len(state_dict['regions'])
	if number > 1:
		for i in range(0, number):
			if state_dict['regions'][i] in empty_regions:
				testfile.write(createSeleniumCommand("verifyTextPresent", "glob:%s*, 0" % state_dict['regions'][i]))
			else:
				testfile.write( region_data % (i/linkselves_by_line +1 , i%linkselves_by_line + 1,i/linkselves_by_line +1 , i%linkselves_by_line + 1, 'tab_region', 'tab_region'))

	# Close the HTML text
	testfile.write( "</tbody></table></body></html>")

	# Add this suite to list for later output
	testfile_list[testfile_name] = test_title
	print "Done with %s" % test_title
	
	testfile.close()

# Iterate for all the nearby regions
for region_name, region_dict in regions_nearby.iteritems():
	
	# Create new test file for nearby tab
	region_nearby_friendly_name = region_dict['friendly'] + "-regioes-proximas"
	testfile_nearby_name = "check_%s-nearby_linkshelves.html" % region_dict['friendly']
	test_nearby_title = "Checking linkshelves in nearby regions tab for '%s'" % region_name

	if options.target_dir and os.path.exists(options.target_dir):
		testfile_nearby_name = options.target_dir + testfile_nearby_name
	testfile = open(testfile_nearby_name, "w" )

	# Get the number of columns per line
	try:
		linkselves_by_line = region_dict['linkshelves_per_line']
	except:
		linkselves_by_line = LINKSELVES_BY_LINE_DEFAULT

	# Add common headers
	testfile.write( header_data % (test_nearby_title, test_nearby_title, region_dict['friendly_state'] + "/" + region_nearby_friendly_name) )

	number = len(region_dict['nearby_regions'])
	if number > 1:
		for i in range(0, number):
			if region_dict['nearby_regions'][i] in empty_regions:
				testfile.write(createSeleniumCommand("verifyTextPresent", "glob:%s*, 0" % region_dict['nearby_regions'][i]))
			else:
				testfile.write( region_data % (i/linkselves_by_line +1 , i%linkselves_by_line + 1,i/linkselves_by_line +1 , i%linkselves_by_line + 1, 'tab_region', 'tab_region'))

	# Close the HTML text
	testfile.write( "</tbody></table></body></html>")

	# Add this suite to list for later output
	testfile_list[testfile_nearby_name] = test_nearby_title
	print "Done with %s" % test_nearby_title

	testfile.close()

# Create file for testing linkshelves of all country tab
# Final file should have different behaviour for states with just one region
testfile_name = "check_brazil_all_country_linkshelves.html"
test_title = "Checking linkshelves in all country tab"

# Create new test file
if options.target_dir and os.path.exists(options.target_dir):
	testfile_name = options.target_dir + testfile_name
testfile = open(testfile_name, "w" )

# Add common headers
testfile.write( header_data % (test_title, test_title, "brasil") )

# Sort states by name
keys = states.keys()
keys.sort()
sorted_states=[states[key] for key in keys]
number = len(states)
if number > 1:
	for i in range(0, number):
		if sorted_states[i]['single_region']:
			testfile.write( all_country_data % (i/LINKSELVES_BY_LINE_BRAZIL +1 , i%LINKSELVES_BY_LINE_BRAZIL + 1,i/LINKSELVES_BY_LINE_BRAZIL +1 , i%LINKSELVES_BY_LINE_BRAZIL + 1 , 'tab_region\']/h2', sorted_states[i]['short_name'], 'tab_region\']/h2'))
		else:
			testfile.write( all_country_data % (i/LINKSELVES_BY_LINE_BRAZIL +1 , i%LINKSELVES_BY_LINE_BRAZIL + 1,i/LINKSELVES_BY_LINE_BRAZIL +1 , i%LINKSELVES_BY_LINE_BRAZIL + 1 , 'tab_state\']/h1', sorted_states[i]['short_name'], 'tab_state\']/h1'))

# Close the HTML text
testfile.write( "</tbody></table></body></html>")
testfile.close()


# Job done
print "Job done, add these lines to your Selenium suite (make sure the html files are in the right places!!!)\n"
for file in testfile_list:
	name = os.path.basename(file)
	print '<tr><td><a href="%s">%s</a></td></tr>' % ( name, testfile_list[file] )
	
# Close all used files
for file in openfile_list:
	file.close()

sys.exit(0)

