#!/bin/bash 

echo "220 Welcome."
echo "token:{{.*}}"

month=`date "+%m"`
year=`date "+%Y"`
res=0
dias=0

while [ $dias -lt 63 ]
do
	val=`date -d "${month}/1 + 1 month - 1 day" "+%d"`
	day=`date "+%d"`
	echo "result."${res}".month:"${year}"-"${month}"-01 00:00:00"
	((month=10#$month-1))
	if [ $month -lt 10 ]
	then
		month="0"$month
	fi
	if [ $month -eq 0 ]
	then
		((month = 12))
		((year=10#$year-1))
	fi 

	if [ $dias -eq 0 ]
	then
		echo "result."${res}".count:"$((10#$day*12))
		((dias = dias + 10#$day))
	else
		if [ $((dias + 10#$val)) -lt 63 ]
		then
			echo "result."${res}".count:"$((10#$val*12))
		else
			echo "result."${res}".count:"$(((63 - dias)*12))
		fi
		((dias = dias + 10#$val))
	fi
	((res=res+1))
done

echo "status:TRANS_OK"
echo "end"
