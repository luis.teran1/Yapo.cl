#!/bin/bash 

echo "220 Welcome."
echo "token:{{.*}}"

week=`date "+%U"`
((week=10#$week+1))
res=0
dias=0

while [ $dias -lt 63 ]
do
	val=7
	day=`date "+%u"`
	echo "result."${res}".week:{{.*}}"
	if [ $dias -eq 0 ]
	then 	
		echo "result."${res}".count:"$((10#$day*12))
		((dias=dias+10#$day))
	elif [ $((dias + val)) -lt 63 ]
	then
		echo "result."${res}".count:"$((val*12))
		((dias=dias+7))
	else 
		echo "result."${res}".count:"$(((63-dias)*12))
		((dias=dias+7))
	fi
	
	echo "result."${res}".date_trunc:{{....-..-.. ..:..:..}}"
	((week=10#$week-1))
	((res=res+1))
done

echo "status:TRANS_OK"
echo "end"
