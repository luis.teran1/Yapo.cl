#!/usr/bin/php -q
<?php

/*
 * quoteprinteable2text.php
 * This script convert a quoted-printable file to an 8 bit string
 * 
 * v0.1: the first version
 */

if (empty($argv[1])) {
	echo "Please spefify a filename with 'quoted-printable' enconding\n";
	exit();
}

if (!is_file($argv[1])) {
	echo "File ".$argv[1]." not found\n";
	exit();
}

$content = "";
$content = file_get_contents($argv[1]);
echo quoted_printable_decode($content)."\n";

