#include <stdio.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdarg.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>
#include <math.h>
#include <sys/time.h>
#include <err.h>


void stat_log_host(int s, const char *event, const char *id, const char *parent);

void usage(char *file) {
	errx(1, "usage: %s host port frequence", file);
}

int
main(int argc, char *argv[]) {
	unsigned long long sleep;
	struct timespec sleep_ts;
	struct timespec tsrem;
	float x1;
	float y1;
	float x2;
	float y2;
	const char *host = NULL;
	const char *port = NULL;
	char id[7];
	int freq = 1;
	struct addrinfo hints = {AI_CANONNAME, 0, SOCK_DGRAM};
	struct addrinfo *res;
	int e;
	int s;

	if (argc < 4)
		usage(argv[0]);

	host = argv[1];
	port = argv[2];
	freq = atoi(argv[3]);

	if (host == NULL || port == NULL || freq <= 0) {
		usage(argv[0]);
		exit(1);
	}
	
	
	sleep = 1000000000LL / freq;
	sleep_ts.tv_sec = 0;
	sleep_ts.tv_nsec = sleep;

	if ((e = getaddrinfo(host, port, &hints, &res)) || !res) {
		fprintf(stderr, "Host lookup failed: %s\n", gai_strerror(e));
	}

	s = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (s < 0)
		err(1, "socket");

	if (connect(s, res->ai_addr, res->ai_addrlen))
		err(1, "connect");

	printf("Sending UDP event packages to %s:%s with speed %d/sec. Ctrl-c to stop\n", res->ai_canonname, port, freq);
	
	while (1) {
		struct timeval tv,tv_now;
		unsigned long long elapsed;

		gettimeofday(&tv,NULL);
		x1 = rand() / (float) RAND_MAX;
		x2 = rand() / (float) RAND_MAX;
		y1 = sqrt(-2 * logf(x1)) * cos(2 * M_PI * x2);
		y2 = sqrt(-2 * logf(x1)) * sin(2 * M_PI * x2);
		y1 = abs(y1*500000);
		y2 = abs(y2*500000);
		snprintf(id, 7, "%ld", (long) floor(y1));
		stat_log_host(s, "VIEW", id, NULL);
		gettimeofday(&tv_now,NULL);
		nanosleep(&sleep_ts, &tsrem);	
		snprintf(id, 7, "%ld",(long) floor(y2));
		stat_log_host(s, "VIEW", id, NULL);
		nanosleep(&sleep_ts, &tsrem);	
		gettimeofday(&tv_now,NULL);
		elapsed = 500000000 * (tv_now.tv_sec - tv.tv_sec) + 500 * (tv_now.tv_usec - tv.tv_usec);
		if (elapsed < 1000000000LL) {
			sleep_ts.tv_nsec = sleep - (elapsed - sleep_ts.tv_nsec);
		} else {
			/* we have been interrupted by something - dont wait in next loop */
			sleep_ts.tv_nsec = 0;
		}
	}
	
}

void
stat_log_host(int s, const char *event, const char *id, const char *parent) {
        char buf[256];
	int len;

        if (parent)
                len = snprintf(buf, sizeof(buf), "event:%s id:%s parent:%s\n", event, id, parent);
        else
                len = snprintf(buf, sizeof(buf), "event:%s id:%s\n", event, id);

	if (len < 0 || len >= sizeof(buf))
		errx(1, "buffer to small");

        if (write(s, buf, len) < 0)
		err(1, "write");
}

