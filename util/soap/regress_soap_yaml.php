<?

require('soap_request.php');
require('spyc.php5');


$progname = array_shift($argv);
$dump = 0;

if ($argc < 2) {
	die("Usage: php $progname [--dump] http://location/ testfile.yml");
}

if ($argv[0] == "--dump") {
	$dump = 1;
	array_shift($argv);
}

$test = Spyc::YAMLLoad($argv[1]);

if (!isset($test['app']))
	die("Missing method!");
if (!isset($test['method']))
	die("Missing method!");
if (!isset($test['headers']))
	die("Missing headers!");
// if (!isset($test['params']))
//	die("Missing params!");

if (@is_array($test['params'])) {
	foreach ($test['params'] as $key => $value) {
		if (is_array ($value) && isset($value['type'])) {
			switch ($value['type']) {
			case "file":
				$file_content = base64_encode(file_get_contents($value['value']));
				$test['params'][$key] = $file_content;
				break;
			default:
				print "Got unknown type {$value['type']}, write more code\n";
			}
		}
	}
 } else {
	$test['params'] = array();
}
	
	
$soap = new soap_request($argv[0] . $test['app'], $test['method'], $test['params'], null, $test['headers']);
if ($dump)
	echo $soap->get_request();
echo $soap->send_request();
?>