<?
/*
	A simple SOAP client which creates requests from the command line.

	example invocation:
	php -c ../../regress_final/conf/php.ini regress_soap_request.php http://REGRESS/importads/ alert var:value var2:value2 image:array:img1,img2,img3,img4 arne:array:n\;val,n2\;val2 encoded:file:regress_soap_request.php
*/
require('soap_request.php');

if ($argc < 3) {
	die("Usage: php {$argv[0]} http://location/ method [--dump] param:value param:array:[name;]value,[name;]value param:file:file_path headerkey:header:headerval.... \n");
}

$params_map =  array_slice($argv, 3);
$params = array(); 
$headers = array();
$dump = false;

foreach ($params_map as $p) {
	if ($p == '--dump') {
		$dump = true;
	} elseif(substr_count($p, ":") > 1) {
		list ($key, $type, $value) = explode(":", $p, 3);
		switch ($type) {
			case 'array':
				$values = explode(",", $value);
				$result_array = array();
				foreach($values as $v) { // handle name;value pairs
					if(strpos($v,';') !== FALSE) {
						list($a_key, $a_val) = explode(";", $v);
						$result_array[$a_key] = $a_val;
					} else {
						$result_array[] = $v;
					}
				}
				$params[$key] = $result_array;
				break;
			case 'file':
				$file_content = base64_encode(file_get_contents($value));
				$params[$key] = $file_content;
				break;
			case 'header':
				$headers[$key] = $value;
				break;
			default:
				print "ERROR: Unknown type/handler.\n";
		} // ... end switch to handle special types
	} else {
			list ($key, $value) = explode(":", $p, 2);
			$params[$key] = $value;		
		}
	}
	$location = $argv[1];
	$method = $argv[2];

	// $params = array('test'=> array(1, 2, 3), 'kaka' => 'bulle', 'oscar' => array('image1' => '2312432.jpg', 'jdfjd' => '334.jpg')));
	$soap = new soap_request($location, $method, $params, null, $headers);
	if ($dump) {
		echo $soap->get_request();
		echo "=======\n";
	}
	echo $soap->send_request();
?>
