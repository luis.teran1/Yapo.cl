# -*- coding: latin-1 -*-
import unicodedata, re, sys, configparser, os.path
from collections import Counter
from redis import Redis
from scm.ext_services.search import Search
from scm.platform import bsapi

""" Gets results from asearch of the published ads
    iterates over them, and save the number of repetitions per word in redis """

SPECIAL_CHARS_RE = re.compile('["\(\),:\.]')
MULTI_SPACES_RE = re.compile(' +')
REDIS_HOST = ''
REDIS_PORT = ''
ASEARCH_HOST = ''
ASEARCH_PORT = ''
BLACK_LIST_PATH = ''
BLACK_LIST = []

def setup(config_file_path):
    """ load the config and prepare initial data """
    # load the configuration from config file
    config = configparser.ConfigParser()
    config.read(config_file_path)
    global REDIS_PORT, REDIS_HOST, ASEARCH_HOST, ASEARCH_PORT, BLACK_LIST_PATH
    REDIS_HOST = config['DEFAULT']['REDIS_HOST']
    REDIS_PORT = config['DEFAULT']['REDIS_PORT']
    ASEARCH_HOST = config['DEFAULT']['ASEARCH_HOST']
    ASEARCH_PORT = config['DEFAULT']['ASEARCH_PORT']
    BLACK_LIST_PATH = config['DEFAULT']['BLACK_LIST_PATH']
    # load the black list words
    with open(BLACK_LIST_PATH, 'r') as f:
        for line in f.readlines():
            BLACK_LIST.append(line.strip())


def clean_subject(s):
    """ cleans the subject by removing tittles and tildes, and lowercases it """
    s = s.strip()
    s = unicodedata.normalize('NFKD', s).encode('ascii', 'ignore')
    # all the puntuation simbols should be removed
    s = re.sub(SPECIAL_CHARS_RE, ' ', s.decode("ascii"))
    # remove duplicated spaces
    s = s.strip().lower()
    s = re.sub(MULTI_SPACES_RE, ' ', s)
    list = s.split(' ')
    return [i for i in list if i not in BLACK_LIST and len(i) > 1]


def count_then_save_multi(subjects):
    """ Same as above, just sends all the commands with a fucking multi """
    counter = Counter()
    c = Redis(host=REDIS_HOST, port=int(REDIS_PORT))
    p = c.pipeline()
    words = []
    for subject in subjects:
        words = clean_subject(subject)
        counter.update(words)

    for word, count in counter.items():
        p.zincrby("words_new", word, count)
    p.execute()

def create_bsconf(host, port):
    conf = {"host": {"1":{
            "name": host,
            "port": port
    	   }}}
    bsconf = bsapi.bsconf_struct()
    if bsapi.bsconf_init_vtree(bsconf, conf, True) != 0:
        raise Exception("failed to initialize bsconf")
    return bsconf

def get_data():
    query_string = "lim:100 type:- *:*"
    bsconf = create_bsconf(host='localhost', port=ASEARCH_PORT)
    search = Search(conn_pool=bsconf, text_encoding='latin-1')
    result_query = search.search(query_string)
    return [ad['subject'] for ad in result_query['row']]


def get_data_by_page(search, query, page):
    q = "{} {}".format(page, query)
    page_q = None
    subjects = []
    res = search.search(q)
    info = res['info_full']

    for ad in res['row']:
        subjects.append(ad['subject'])

    if 'paging_last' in info:
        page_q = "P:{}".format(info['paging_last'][0][0])

    return subjects, page_q


def usage(script_name):
    print('usage:')
    print('\tpython3.3 {} config.ini --load'.format(script_name))
    print('\tpython3.3 {} config.ini --rank "text to rank"'.format(script_name))

if __name__ == "__main__":
    if len(sys.argv) == 4 and sys.argv[2] == '--rank' and os.path.isfile(sys.argv[1]):
        setup(sys.argv[1])
        send_smart_search(sys.argv[3])
    elif len(sys.argv) == 3 and sys.argv[2] == '--load' and os.path.isfile(sys.argv[1]):
        setup(sys.argv[1])
        bsconf = create_bsconf(host=ASEARCH_HOST, port=ASEARCH_PORT)
        search = Search(conn_pool=bsconf, text_encoding='latin-1')
        page = "P:0,0,0"
        page_size = 20
        query = "lim:" + str(page_size) + " type:- *:*"
        ads_number = 0
        print ('loading ads')
        while page is not None:
            print(".", end="")
            ads_number += page_size
            subjects, page = get_data_by_page(search, query, page)
            count_then_save_multi(subjects)
        c = Redis(host=REDIS_HOST, port=int(REDIS_PORT))
        p = c.pipeline()
        p.delete('words')
        p.rename('words_new', 'words')
        p.execute()
        print("")
    else:
        usage(sys.argv[0])
