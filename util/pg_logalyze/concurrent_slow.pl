#!/usr/bin/perl -w

use POSIX;

my $prog="concurrent_slow.pl";

open(PGLOG,$ARGV[0]) or die "usage: $prog log";

my $l;
my %Q;
my $firststart;
my $lastend;
while ($l=<PGLOG>) {
	while ($l =~ /CES*TLOG:.*duration/) {
		$l = query_complete($l);
	}

}
print_queries($firststart,$lastend);
exit(0);

sub query_complete {
	my $l = shift;
	$l =~ /([0-9]+-[0-9]+-[0-9]+\ [0-9]+:[0-9]+:[0-9]+)\ .*duration: ([0-9.]+)\ ms\ +[a-z:]+\ (.*)[\ \t]*$/ or die "strange line $l";
	my $endtime = to_seconds($1);
	my $duration = POSIX::floor($2/1000);
	my $query = $3;
	chomp($query);
	while ($l = <PGLOG>) {
		$l =~ /^20[0-9]+-[0-9]+-[0-9]+/ and last;
		chomp($l);
		$query .= "$l";
	}
	$query =~ y/[\ \t]/\ /s;
	store_query($endtime-$duration,$endtime,substr($query,0,90));
	return $l;
}

sub to_seconds {
	my $strtime = shift;
	$strtime =~ /([0-9]+)-([0-9]+)-([0-9]+)\ ([0-9]+):([0-9]+):([0-9]+)/ or die "strange time $strtime";
	return POSIX::mktime($6,$5,$4,$3,$2,$1-1900) + 3600;
}

sub store_query {
	my %q;
	($q{start},$q{end},$q{query}) = @_;
	$q{depth} = 0;
	defined($firststart) or $firststart = $q{start};
	$firststart <= $q{start} or $firststart = $q{start}; 
	$lastend = $q{end};
	my $qs;
	if (defined($Q{$q{start}})) {
		$qs = $Q{$q{start}};
	} else {
		my @a = ();
		$qs = \@a;
		$Q{$q{start}} = $qs;
	}
	push @{$qs},\%q;
}

sub print_queries {
	my ($from,$to)=@_;
	my $s;
	my @running_q = ();
	for ($s=$from;$s<=$to;$s++) {
		if (defined($Q{$s})) {
			my $q;
			foreach $q (@{$Q{$s}}) {
				push (@running_q, $q)
			} 
		}
		if (scalar @running_q > 0) {
			my $r_q;
			my @running_q_new = ();
			print POSIX::strftime('%H:%M:%S',$s,0,0,0,0,0) . " ";
			my $i=1;
			foreach $r_q (@running_q) {
				$i+=print_query($r_q,$i);
				if ($r_q->{end} > $s) {
					push (@running_q_new,$r_q);
				}
			}
			@running_q = @running_q_new;
			print "\n";
		}
	}
}

sub print_query {
	my $q = shift;
	my $depth = shift;
	my $padding = '   ';
	if ($q->{depth} == 0) {
		$padding = ' + ';
		$q->{depth} = $depth;
	} else {
		my $i;
		for ($i=$depth; $i < $q->{depth};$i++) {
			$padding .= ' ';	
		}
	}
	 
	my $str = $padding. ($q->{end} - $q->{start}) . " " . substr($q->{query},0,25) . " ";
	print $str;
	if (length($q->{query}) > 25) {
		$q->{query} = substr($q->{query},25)
	} else {
		$q->{query} = ' | ';
	}
	return length($str);
}
