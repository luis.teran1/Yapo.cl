#!/usr/bin/perl
# Usage: compconf.pl <root-variable-name> <bconf-file> ...

use strict;
use Data::Dumper;
use IPC::Open2;
use Digest::MD5 qw(md5);

my %data = ();

$SIG{CHLD} = 'IGNORE';

my $name = shift;

# Create bconf hash.
while (<>) {
	chomp;
	s/^[[:space:]]+//;
	/^$/ && next;

	# Skip comments
	/^#/ && next;

	my ($key, $val) = split (/=/, $_, 2);
	die "Invalid line" if not defined $val;

	my @keyparts = split (/\./, $key);

	my $dp = \%data;
	for my $kpi (0 .. $#keyparts - 1) {
		my $kp = $keyparts[$kpi];

		$dp = \%{$$dp{$kp}};
	}
	$$dp{$keyparts[$#keyparts]} = $val;
}

# Bconf comparsion, see lib/common/bconf.c
sub bconf_sort {
	if ($a =~ /^[0-9]/ && $b =~ /^[0-9]/) {
		my $lendiff = length($a) - length($b);
		return $lendiff if $lendiff;
	}

	return $a cmp $b;
}

print <<EOT
#include <string.h>
#include "compconf.h"

EOT
;

my $funidx = 0;
my %functions = ();

sub compile_node($$) {
	my ($prefix, $node) = @_;

	if (ref($node) ne 'HASH') {
		return "NULL, \"$node\"";
	} else {
		while (my ($key, $val) = each (%$node)) {
			my $val = $$node{$key};
			my $newpref = $prefix . "_$key";
			$newpref =~ y/*/x/;
			$$node{$key} = compile_node($newpref, $val);
		}
		my @keys = sort bconf_sort keys %$node;
		my $funkey = join ("\n", @keys);
		if (!$functions{$funkey}) {
			$functions{$funkey} = {'func' => "function_" . $funidx++};
			my $prefix = $functions{$funkey}{'func'};

			open2 (*GPERFread, *GPERFwrite, 'gperf');
			print GPERFwrite <<EOT
struct compconf_lookup;
\%struct-type
\%omit-struct-type
\%define hash-function-name ${prefix}_hash
\%define lookup-function-name ${prefix}_lookup
\%readonly-tables
\%global-table
\%define string-pool-name ${prefix}_strings
\%define word-array-name ${prefix}_words
\%define slot-name key
\%\%
EOT
;
# Switch makes the code much smaller, but does a binary search on big data sets.
# It also takes much longer to compile the generated code...
#\%switch=1
			my $idx = 0;
			for my $key (@keys) {
				print GPERFwrite "$key, $idx\n";
				$idx++;
			}
			print GPERFwrite "\%\%\n";
			close GPERFwrite;
			my $inwords = 0;
			my $arridx = 0;
			my %arrvals = ();
			while (<GPERFread>) {
				print;
			}
			close GPERFread;
			print <<EOT
#undef TOTAL_KEYWORDS
#undef MIN_WORD_LENGTH
#undef MAX_WORD_LENGTH
#undef MIN_HASH_VALUE
#undef MAX_HASH_VALUE
EOT
;
		}
		my $reccode = '';
		my $idx = 0;
		my $staridx = -1;
		foreach my $key (@keys) {
			my $rec = "{ \"$key\", " . $$node{$key} . "}";
			$reccode .= "\t$rec,\n";
			$staridx = $idx if $key eq '*';
			$idx++;
		}

		my $hash = md5($reccode);
		return $functions{$funkey}{'hash'}{$hash} if defined ($functions{$funkey}{'hash'}{$hash});

		print "static const struct compconf_node ${prefix}_words_sorted[] = {\n$reccode};\n";
		my $fprefix = $functions{$funkey}{'func'};
		my $nodecode = "${fprefix}_lookup, NULL, ${prefix}_words_sorted, " . scalar(values(%$node)) . ($staridx != -1 ? ", &${prefix}_words_sorted[${staridx}]" : "");
		$functions{$funkey}{'hash'}{$hash} = $nodecode;
		return $nodecode;
	}
}

my $root = compile_node($name, \%data);
print "const struct compconf_node ${name}_root_node = { \"\", ${root} };\n";
print "const struct compconf_node * const ${name} = &${name}_root_node;\n";

