
sed -r -e 's/^([0-9]{5}),[^,]*,0?([1-9]?[0-9]),.*/\1 \2/' PnrPortLanKommun06.csv | while read z l; do
	r=none;
	case $l in
		1) r=11;;
		3) r=10;;
		4) r=12;;
		5) r=14;;
		6) r=17;;
		7) r=21;;
		8) r=18;;
		9) r=19;;
		10) r=22;;
		12) r=23;;
		13) r=20;;
		#14) r=XXX;;
		17) r=7;;
		18) r=8;;
		19) r=9;;
		20) r=6;;
		21) r=5;;
		22) r=4;;
		23) r=3;;
		24) r=2;;
		25) r=1;;
	esac;
	if [ "$r" != "none" ]; then
		echo "*.*.zipcodes.$z.r=$r"; done >> conf/bconf/bconf.txt.zipcodes 
	fi
done

grep 'V�stra G�taland' PnrPortLanKommun06.csv  | cut -d , -f 1,5 | tr ',' ' ' | while read z k; do
	grep "\.$k.*r=" src/head/conf/bconf/bconf.txt.lkf | sed -e "s/.*r=/*.*.zipcodes.$z.r=/";
done >> conf/bconf/bconf.txt.zipcodes 
