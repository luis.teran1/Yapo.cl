#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>

#define MAX_ARG 100

char *hostname;
int port;
void help();

#define TRANSACTION_BANNER_OK "220 Welcome.\n"

/****************************************************************************
 * trans_open
 * Open a socket to trans.
 * Returns socket
 ******/
static int 
trans_open(void) {
	struct sockaddr_in pin;
	struct hostent *hp;
	char buf[64];
        int sd;

	/* go find out about the desired host machine */
	if ((hp = gethostbyname(hostname)) == 0) {
		fprintf(stderr, "ERROR: gethostbyname error\n");
                exit(3);
	}

	/* fill in the socket structure with host information */
	memset(&pin, 0, sizeof(pin));
	pin.sin_family = AF_INET;
	pin.sin_addr.s_addr = ((struct in_addr *)(hp->h_addr))->s_addr;
	pin.sin_port = htons(port);

	/* grab an Internet domain socket */
	if ((sd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		fprintf(stderr, "ERROR: socket error\n");
                exit(1);
	}

	/* connect to PORT on HOST */
	if (connect(sd,(struct sockaddr *)  &pin, sizeof(pin)) == -1) {
		fprintf(stderr, "ERROR: connect error\n");
                exit(2);
	}

	if (read(sd, buf, sizeof(TRANSACTION_BANNER_OK) - 1) != sizeof(TRANSACTION_BANNER_OK) - 1) {
		fprintf(stderr, "ERROR: banner error\n");
		exit(3);
	}

        return sd;
}

/****************************************************************************
 * trans_printf
 * Write formatted string to socket.
 * Returns ok=1 / error=0.
 ******/
static int 
trans_printf(int sd, const char *fmt, ...)
{
        char buf[1024];
        int len;
	va_list ap;

	va_start(ap, fmt);
	len = vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);

	/* send a message to the server PORT on machine HOST */
	if (send(sd, buf, len, 0) == -1) {
		fprintf(stderr, "ERROR: send error\n");
                return 0;
	}
        return 1;
}

/****************************************************************************
 * trans_read
 * Read from socket.
 * Returns length of read data.
 ******/
static int 
trans_read(int sd, char *buf, int maxlen) {
        int len;
        /* wait for a message to come back from the server */
        len = recv(sd, buf, maxlen, 0);
        if (len == -1) {
        }
        return len;
}

/****************************************************************************
 * trans_close
 ******/
static int
trans_close(int sd) {
	close(sd);
        return(0);
}

/****************************************************************************
 * hold_mail
 * Handle the hold_mail transaction.
 ******/
static void
hold_mail(int argc, char **argv) {
        int sd = trans_open();
        char buf[512];
        int len;

        trans_printf(sd, "cmd:hold_mail\n");
        if (argc > 0) {
                if (strcmp(argv[0], "hold") == 0) {
                        if (argc < 3) {
                                fprintf(stderr, "ERROR:You must specify 'name' and 'value' too\n");
                                trans_close(sd);
                                return;
                        } else {
                                /* Do transaction */
                                trans_printf(sd, "name:%s\nvalue:%s\n",
                                             argv[1],
                                             argv[2]);
                        }
                        
                } else if (strcmp(argv[0], "delete") == 0) {
			if (argc >= 2 && !strcmp(argv[1], "-f")) {
				trans_printf(sd, "flush:1\n");
				argv++;
				argc--;
			}
                        if (argc < 2) {
                                fprintf(stderr, "ERROR:You must specify an 'id' or 'all'\n");
                                trans_close(sd);
                                return;
                        } else {
                                /* Do transaction */
                                trans_printf(sd, "delete:%s\n",
                                             argv[1]);
                        }
                } else {
                        fprintf(stderr, "ERROR: Unknown command '%s'\n",
                                argv[0]);
                        trans_close(sd);
                        return;
                }
        }
        trans_printf(sd, "commit:1\n");
        trans_printf(sd, "end\n");

        /* Get result */
        while ((len = trans_read(sd, buf, sizeof(buf))) > 0) {
                buf[len] = '\0';
                printf("%s", buf);
        }
        trans_close(sd);
}


/***************************************
 * Voucher monthly balance
 * Does a voucher monthly balance.
 */
static void
voucher_monthly_balance(int argc, char *argv[]) {
        int sd = trans_open();
        char buf[512];
        int len;

        trans_printf(sd, "cmd:voucher_monthly_balance\n");
        trans_printf(sd, "commit:1\n");
        trans_printf(sd, "end\n");

        /* Get result */
        while ((len = trans_read(sd, buf, sizeof(buf))) > 0) {
                buf[len] = '\0';
                printf("%s", buf);
        }
        trans_close(sd);
}

/***************************************
 * Flushqueue
 * Does a flushqueue.
 */
static void
flushqueue(int argc, char *argv[]) {
        int sd = trans_open();
        char buf[512];
        int len;

	if (argc != 2) {
		help();
		return;
	}

        trans_printf(sd, "cmd:flushqueue\n");
	trans_printf(sd, "queue:%s\n", argv[0]);
	trans_printf(sd, "maxtime:%s\n", argv[1]);
        trans_printf(sd, "commit:1\n");
        trans_printf(sd, "end\n");

        /* Get result */
        while ((len = trans_read(sd, buf, sizeof(buf))) > 0) {
                buf[len] = '\0';
                printf("%s", buf);
        }
        trans_close(sd);
}

void
help(void) {
        printf("Available commands:\n");
        printf("hold_mail                          - list current hold mail parameters.\n");
        printf("hold_mail hold <key> <regex>       - hold mails matching the regex.\n");
        printf("hold_mail delete [-f] <all/db-id>  - delete hold parameter(s).\n");
	printf("                                     -f to flush held mails.\n");
        printf("voucher-monthly-balance            - do a voucher-monthly-balance.\n");
        printf("flushqueue <queue> maxtime         - flush specified queue.\n");
}

/****************************************************************************
 * do_cmd
 * Parse command line and do the action
 ******/
static int
do_cmd(char *line)
{
        int ac=0;
		char *state = NULL;
        char *acv[MAX_ARG];
        char *cmd = strtok_r(line, " ", &state);
        acv[ac] = cmd;
        while (cmd) {
                cmd=strtok_r(NULL, " ", &state);
                acv[++ac] = cmd;
        }
        if (ac > 0) {
                if (strcmp(acv[0], "hold_mail") == 0) {
                        hold_mail(ac - 1, acv + 1);
                        return 1;
                } else if (strcmp(acv[0], "voucher-monthly-balance") == 0) {
                        voucher_monthly_balance(ac - 1, acv + 1);
			return 1;
                } else if (strcmp(acv[0], "flushqueue") == 0) {
                        flushqueue(ac - 1, acv + 1);
			return 1;
                } else if (strcmp(acv[0], "help") == 0) {
                        help();
                } else {
                        fprintf(stderr, "Unknown command.\n");
                }
        }
        return 0;
}


/****************************************************************************
 * main
 ******/
int
main(int argc, char **argv) {
        if (argc < 2) {
                printf("Usage: bcli <hostname> <port>\n");
                exit(0);
        }
        hostname = argv[1];
        port = atoi(argv[2]);
        if (port == 0) {
                printf("Wrong port number.\n");
                printf("Usage: bcli <hostname> <port>\n");
                exit(0);
        }

        /* Check if we have command on command line */
        if (argc > 3) {
                char *tmp = strdup(argv[3]);
                do_cmd(tmp);
                return(0);
        }

        /* Do the interactive command loop */
        while (1) {
                char *line = readline("cmd> ");
                if (!line) {
                        printf("\n");
                        return(0);
                }
                do_cmd(line);

                free(line);
        }


        return(0);
}
