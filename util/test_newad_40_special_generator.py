#!/usr/bin/env python
#
# this was written in order to test some special price-ranges in categories
# we have ads for fitting in following price-ranges, for the listed
# categories{}:
#
#  . [0 - 200]
#  . [ - 50]
#  . [100 - ]
#
# bbatista@ and oliveira@

import sys

categories = { 
    '5040': 'eletro',
    '5060': 'jardinagem'     ,
    '5080': 'cama'           ,
    '5100': 'roupas'         ,
    '5120': 'gestante'       ,
    '5140': 'malas'          ,
    '5160': 'bijuterias'     ,
    '6040': 'caca'           ,
    '6060': 'ciclismo'       ,
    '6080': 'instrumentos'   ,
    '6100': 'musica'         ,
    '6120': 'livros'         ,
    '6140': 'animais'        ,
    '6160': 'antiguidades'   ,
    '6180': 'hobbies'        ,
    '6200': 'bebidas'        ,
    '3020': 'videogames'     ,
    '3040': 'computadores'   ,
    '3060': 'telefonia'      ,
    '3080': 'Audio'          
}

def main():
    initial_ad_id = 14866335
    initial_list_id = 15614734
    user_id = 13
    #payment_group = 
    #initial_current_state = 112823441
    count = 0
    for cat_id in categories.iterkeys():
        for search in range(2):

            ad_id_proda = initial_ad_id + (count)
            list_id_proda = initial_list_id + (count)
#            current_state = initial_current_state + (count)
            count = count + 1

            if search == 0: 
                price = '125'
                search_case = "[0-200, 100-]"
            elif search == 1: 
                price = '50' 
                search_case = "[-50, 0-200]"

            subject = ''.join(["Produto ", categories[cat_id], "(", cat_id, ") price:", price, " ", search_case])

            insert_ads_sql = "insert into ads values (%s, %s, CURRENT_TIMESTAMP," % (ad_id_proda, list_id_proda)
            insert_ads_sql += "'active', 'sell', 'Andre/Bruno', '1167247615', '99',"
            insert_ads_sql += "'0', %s, %s, NULL, false, false, true, '%s', 'body', %s," % (cat_id, user_id, subject, price)
            insert_ads_sql += "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pt');"
            print  insert_ads_sql

            current_state = ad_id_proda
            insert_adactions_sql = "insert into ad_actions values (%s, " % ad_id_proda 
            insert_adactions_sql += "1, 'new', %s, 'accepted', 'normal', NULL, NULL, NULL);" % current_state
            print insert_adactions_sql

            insert_action_states = "insert into action_states values (%s, " % ad_id_proda
            insert_action_states += "1, %s, 'accepted', 'accept'," % current_state
            insert_action_states += "CURRENT_TIMESTAMP, '192.168.4.75', NULL);"
            print insert_action_states

            print


if __name__ == "__main__": main()

# vim: set ts=4 sw=4 et:
