#!/usr/bin/perl

my @targs = ();

sub flush {
	($tmp = $current) =~ s/\.in$//;
	$tmp =~ s/^.*\///;
	print "$tmp: $current\n\t\@sed \\\n";

	push @targs, $tmp;

	if ($sects) {
		print "\t-n '/\${BUILD_STAGE}_START/,/\${BUILD_STAGE}_END/p' < \$< |\\\n";
		print "\tsed -re '/^%\${BUILD_STAGE}_(START|END)%\$\$/d' \\\n"
	}
	
	
	foreach (keys %vars) {
		print "\t-e 's,%$_%,\${$_},g' \\\n";
	}

	if (!$sects) {
		print "\t< \$< | \${TOPDIR}/util/in_bconf.pl \${TOPDIR}/conf/bconf.txt > \$@\n\n";
	} else {
		print "\t | \${TOPDIR}/util/in_bconf.pl \${TOPDIR}/conf/bconf.txt > \$@\n\n";
	}

}

$current = $ARGV[0];
while (<>) {
	if ($current ne $ARGV) {
		&flush;
		$current = $ARGV;
		undef %vars;
		$sects = 0;
	}

	while (/%([a-zA-Z0-9_]+)%/) {
		$var = $1;

		if ($var =~ /^(.*)_(START|END)$/) {
			$sects = 1;
		} else {
			$vars{$var} = 1;
		}
		s/%([a-zA-Z0-9_]+)%//;
	}

}

&flush;
print "IN_TARGETS=" . join(' ', @targs) . "\n";
