#!/bin/bash

MAXLINE=76

line=

IFS=

function pqparse {
	local softline=0

	while read line; do
		if [[ ${#line} -gt $MAXLINE ]]; then
			echo -e "\nTHIS LINE IS TO LONG"
			return 2;
		fi

		if [[ $softline -ne 1 ]]; then
			if [[ $line == "--${1}" ]]; then
				echo "$line"
				return 0
			fi
			if [[ $line == "--${1}--" ]]; then
				echo "$line"
				return 1
			fi
		fi

		if [[ $line == *= ]]; then
			echo -n "${line:0:${#line}-1}"
			softline=1
		else
			echo "$line"
			softline=0
		fi
	done

	return 1
}

function parsemime {
	# Read headers
	local qp=0
	local boundary=

	if [[ -n $line && $line == "--${1}--" ]]; then
		return 1
	fi

	while read line; do
		if [[ ${#line} -eq 0 ]]; then
			echo 
			break
		fi	

		if [[ $line == "Content-Transfer-Encoding:"* ]]; then
			qp=1 # Only allow QP or mime multipart...
		fi

		if [[ $line == *"boundary=\""* ]]; then
			boundary=${line##*boundary=\"};
			boundary=${boundary%%\"*}
		fi

		echo "$line"
	done

	if [[ $qp -eq 1 ]]; then
		pqparse $1
		return $?
	fi

	if [[ -n $boundary ]]; then
		# Print everyting prior to marker
		while [[ 1 ]]; do
			parsemime $boundary || break
		done
		return $?
	fi

	while read line; do
		echo $line
	done;

	return 1
}

parsemime

