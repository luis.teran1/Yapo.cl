#!/bin/bash
#==============================================================================
# title			: read_avg_stats.sh
# description	: read average, minimum and maximum ms time
# date			: 2015-04-22
# version		: 0.1
# usage			: ./read_avg_stats.sh <hostname>
# notes			:
#=============================================================================

hostname=$1
data_type="AVG_TIME"

if [[ -z $hostname || -z $data_type ]]
then
	echo "Usage: ./read_stats.sh <hostname>"
	exit 1;
fi

CWD=/usr/share/cacti/scripts
filename=$CWD/uwsgi_stats_brief.log

while read server type date time avg min max
do
	if [ "$hostname" == "$server" ] && [ "$data_type" == "$type" ]
	then
		echo -n "avg:$avg min:$min max:$max"
	fi
done < $filename
