#!/bin/bash
#==============================================================================
# title			: read_stats.sh
# description	: read a statistic file and parse data for each server
#				  and data type.
# date			: 2015-04-14
# version		: 0.2
# usage			: ./read_stats.sh <hostname> <data_type>
#				  valid values for data_type are RPS or LQ
# notes			:
#=============================================================================

hostname=$1
data_type=$2

if [[ -z $hostname || -z $data_type ]]
then
	echo "Usage: ./read_stats.sh <hostname> <RPS|LQ>"
	exit 1;
fi

CWD=/usr/share/cacti/scripts
filename=$CWD/uwsgi_stats_brief.log

while read server type date time avg min max
do
	if [ "$hostname" == "$server" ] && [ "$data_type" == "$type" ]
	then
		echo -n "avg:$avg min:$min max:$max"
	fi
done < $filename
