-- BLOQUE 3
BEGIN;
set search_path=bpv,public;
ALTER TABLE ads ALTER COLUMN price TYPE integer;
ALTER TABLE ads ALTER COLUMN old_price TYPE integer;
ALTER TABLE dashboard_ads ALTER COLUMN price TYPE integer;
COMMIT;
