-- BLOQUE 2
-- CREATE ALL FUNCTIONS AND VIEWS
BEGIN;
set search_path=bpv,public;
CREATE OR REPLACE VIEW v_ads AS
SELECT
    ad_id,
    list_id,
    list_time,
    status::varchar,
    type::varchar,
    name,
    phone,
    region,
    city,
    category,
    user_id,
    salted_passwd,
    phone_hidden,
    no_salesmen,
    company_ad,
    subject,
    body,
    price,
    infopage,
    infopage_title,
    store_id
FROM
    public.ads
UNION ALL
SELECT
    ad_id,
    list_id,
    list_time,
    status::varchar,
    type::varchar,
    name,
    phone,
    region,
    city,
    category,
    user_id,
    salted_passwd,
    phone_hidden,
    no_salesmen,
    company_ad,
    subject,
    body,
    price,
    infopage,
    infopage_title,
    store_id
FROM
    blocket_2010.ads;

GRANT select,insert,delete,update ON v_ads to bwriters;
GRANT select on v_ads TO breaders;

CREATE OR REPLACE VIEW v_ads_current AS
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	public.ads
UNION ALL
SELECT 
	ad_id,
	list_id,
	list_time,
	status::varchar,
	type::varchar,
	name,
	phone,
	region,
	city,
	category,
	user_id,
	salted_passwd,
	phone_hidden,
	no_salesmen,
	company_ad,
	subject,
	body,
	price,
	infopage,
	infopage_title,
	store_id
FROM 
	blocket_2012.ads;

GRANT select,insert,delete,update ON v_ads_current to bwriters;
GRANT select on v_ads_current TO breaders;

CREATE OR REPLACE VIEW v_adflow AS
SELECT
	ads.*,
	action_id,
	payment_groups.code,
	payment_groups.status AS payments_status,
	users.email
FROM
	ads JOIN 
	ad_actions USING (ad_id)
	JOIN users USING (user_id)
	LEFT JOIN payment_groups USING (payment_group_id)
ORDER BY
	ads.ad_id;

GRANT select,insert,delete,update ON v_adflow TO bwriters;
GRANT select ON v_adflow TO breaders;

CREATE OR REPLACE FUNCTION view_ad(i_schema text,
				   i_ad_id action_states.ad_id%TYPE,
				   i_action_id action_states.action_id%TYPE,
				   i_state_id action_states.state_id%TYPE) RETURNS SETOF v_ads AS $$
DECLARE
	l_ad v_ads;
	l_rec RECORD;
	l_param_name text[];
	l_param_value text[];
	l_num_params integer;
	l_i integer;
	l_j integer;
	l_query varchar;
BEGIN
	-- The output of this query should have the same parameters of the view v_ads
	l_query := 'SELECT
			ad_id,
			list_id,
			list_time,
			status::varchar,
			type::varchar,
			name,
			phone,
			region,
			city,
			category,
			user_id,
			salted_passwd,
			phone_hidden,
			no_salesmen,
			company_ad,
			subject,
			body,
			price,
			infopage,
			infopage_title,
			store_id
		FROM ' || i_schema || '.ads
		WHERE ad_id = ' || i_ad_id;

	FOR
		l_ad
	IN
		EXECUTE l_query
	LOOP
		-- Fetch ad params
		l_num_params := 0;
		FOR
			l_rec
		IN
			EXECUTE
				'SELECT
					name::varchar,
					value
				FROM
					' || i_schema || '.ad_params
				WHERE
					ad_id = ' || i_ad_id
		LOOP
			l_param_name[l_num_params] := l_rec.name;
			l_param_value[l_num_params] := l_rec.value;
			l_num_params := l_num_params + 1;
		END LOOP;

		-- Apply ad_changes
		FOR
			l_rec
		IN
			EXECUTE 'SELECT
					*
				FROM
					' || i_schema || '.ad_changes
				WHERE
					ad_id = ' || i_ad_id || '
				ORDER BY
					state_id DESC'
		LOOP
			EXIT WHEN i_state_id > 0 AND l_rec.state_id <= i_state_id;
			EXIT WHEN l_rec.action_id <= i_action_id;

			IF l_rec.column_name = 'name' THEN
				l_ad.name := l_rec.old_value;
			ELSIF l_rec.column_name = 'phone' THEN
				l_ad.phone := l_rec.old_value;
			ELSIF l_rec.column_name = 'region' THEN
				l_ad.region := l_rec.old_value;
			ELSIF l_rec.column_name = 'city' THEN
				l_ad.city := l_rec.old_value;
			ELSIF l_rec.column_name = 'type' THEN
				l_ad.type := l_rec.old_value;
			ELSIF l_rec.column_name = 'category' THEN
				l_ad.category := l_rec.old_value;
			ELSIF l_rec.column_name = 'subject' THEN
				l_ad.subject := l_rec.old_value;
			ELSIF l_rec.column_name = 'body' THEN
				l_ad.body := l_rec.old_value;
			ELSIF l_rec.column_name = 'price' THEN
				l_ad.price := l_rec.old_value;
			ELSIF l_rec.column_name = 'infopage' THEN
				l_ad.infopage := l_rec.old_value;
			ELSIF l_rec.column_name = 'infopage_title' THEN
				l_ad.infopage_title := l_rec.old_value;
			ELSIF l_rec.column_name = 'phone_hidden' THEN
				l_ad.phone_hidden := l_rec.old_value;
			ELSIF l_rec.column_name = 'no_salesmen' THEN
				l_ad.no_salesmen := l_rec.old_value;
			ELSIF l_rec.column_name = 'company_ad' THEN
				l_ad.company_ad := l_rec.old_value;
			ELSIF l_rec.column_name = 'user_id' THEN
				l_ad.user_id := l_rec.old_value;
			ELSIF l_rec.column_name = 'store_id' THEN
				l_ad.store_id := l_rec.old_value;
			END IF;
		END LOOP;

		RETURN NEXT l_ad;
	END LOOP;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION view_all_ad_versions(i_schema text,
                        i_ad_id action_states.ad_id%TYPE) RETURNS SETOF v_ads AS $$
DECLARE
    l_action_id ad_actions.action_id%TYPE;
    l_ad v_ads;
BEGIN
    FOR
        l_action_id
    IN
        EXECUTE 'SELECT
                ad_actions.action_id
            FROM
                ' || i_schema || E'.ad_actions
            WHERE
                ad_actions.ad_id = ' || i_ad_id || '
            ORDER BY
                ad_actions.action_id'
    LOOP
        SELECT
            ad_id,
            list_id,
            list_time,
            status::varchar,
            type::varchar,
            name,
            phone,
            region,
            city,
            category,
            user_id,
            salted_passwd,
            phone_hidden,
            no_salesmen,
                company_ad,
                subject,
                body,
                price,
                infopage,
                infopage_title,
                store_id
        INTO
            l_ad
        FROM
            view_ad(i_schema, i_ad_id, l_action_id, -1);

        RETURN NEXT l_ad;
    END LOOP;
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION accept_action(
						i_ad_id ads.ad_id%TYPE,
						i_action_id ad_actions.action_id%TYPE,
						i_transition action_states.transition%TYPE,
						i_remote_addr action_states.remote_addr%TYPE,
						i_token tokens.token%TYPE,
						i_gallery_expire_minutes integer,
						i_action_type ad_actions.action_type%TYPE,
						i_accepted_state_id action_states.state_id%TYPE,
						OUT o_state_id action_states.state_id%TYPE) AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
	l_old_price ads.price%TYPE;
	l_new_price ads.price%TYPE;
BEGIN
	IF i_accepted_state_id IS NULL THEN
		-- Find the latest state that changes the images, if any.
		SELECT
			MAX(state_id)
		INTO
			l_state_id
		FROM
			action_states
			JOIN ad_image_changes USING (ad_id, action_id, state_id)
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id;

		o_state_id := insert_state(i_ad_id, 
					i_action_id, 
					'accepted', 
					i_transition, 
					i_remote_addr, 
					get_token_id(i_token));
	ELSE
		l_state_id := i_accepted_state_id;
		o_state_id := i_accepted_state_id;
	END IF;

	IF i_gallery_expire_minutes IS NOT NULL THEN
		IF get_changed_value (
				i_ad_id,
				i_action_id,
				true,
				'gallery'
				) = 'request' THEN
			PERFORM insert_ad_change (
					i_ad_id,
					i_action_id,
					o_state_id,
					true,
					'gallery',
					(CURRENT_TIMESTAMP + interval '1 minute' * i_gallery_expire_minutes)::text
					);
		END IF;
	END IF;

	PERFORM apply_ad_changes(i_ad_id, i_action_id, l_state_id);

	-- Set old_price if the price changed with this action.
	SELECT
		old_value,
		new_value
	INTO
		l_old_price,
		l_new_price
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id AND action_id = i_action_id AND column_name = 'price'
	ORDER BY
		state_id DESC
	LIMIT 1;

	IF FOUND THEN
		IF l_old_price != l_new_price THEN
			UPDATE
				ads
			SET
				old_price = l_old_price
			WHERE
				ad_id = i_ad_id;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION apply_ad_changes(i_ad_id ad_actions.ad_id%TYPE,
					    i_action_id ad_actions.action_id%TYPE,
					    i_image_state_id action_states.state_id%TYPE) RETURNS VOID AS $$

DECLARE
	l_name ads.name%TYPE;
	l_phone ads.phone%TYPE;
	l_region ads.region%TYPE;
	l_city ads.city%TYPE;
	l_type ads.type%TYPE;
	l_category ads.category%TYPE;
	l_passwd ads.passwd%TYPE;
	l_subject ads.subject%TYPE;
	l_body ads.body%TYPE;
	l_price ads.price%TYPE;
	l_infopage ads.infopage%TYPE;
	l_infopage_title ads.infopage_title%TYPE; 
	l_phone_hidden ads.phone_hidden%TYPE;
	l_company_ad ads.company_ad%TYPE;
	l_user_id ads.user_id%TYPE;
	l_store_id ads.store_id%TYPE;
	l_salted_passwd ads.salted_passwd%TYPE;

	l_ad_changes_cursor CURSOR (i_ad_id ads.ad_id%TYPE, 
				    i_action_id ad_actions.action_id%TYPE) IS 
		SELECT
			is_param,
			column_name,
			new_value
		FROM
			ad_changes
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id AND
			(state_id, column_name) IN (
				SELECT
					MAX(state_id),
					column_name
				FROM
					ad_changes
				WHERE
					ad_id = i_ad_id AND
					action_id = i_action_id
				GROUP BY
					column_name)
		ORDER BY
			state_id;

	l_is_param ad_changes.is_param%TYPE;
	l_column_name ad_changes.column_name%TYPE;
	l_new_value ad_changes.new_value%TYPE;
BEGIN
-- XXX What to do if old_value is wrong?
	SELECT 
		name,
		phone,
		region,
		city,
		type,
		category,
		passwd,
		subject,
		body,
		price,
		infopage,
		infopage_title, 
		phone_hidden,
		company_ad,
		user_id,
		store_id,
		salted_passwd
	INTO
		l_name,
		l_phone,
		l_region,
		l_city,
		l_type,
		l_category,
		l_passwd,
		l_subject,
		l_body,
		l_price,
		l_infopage,
		l_infopage_title, 
		l_phone_hidden,
		l_company_ad,
		l_user_id,
		l_store_id,
		l_salted_passwd
	FROM
		ads
	WHERE
		ad_id = i_ad_id;
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_FOUND';
	END IF;
	
	OPEN l_ad_changes_cursor(i_ad_id, i_action_id);
	LOOP
		FETCH l_ad_changes_cursor INTO l_is_param, l_column_name, l_new_value;
		EXIT WHEN NOT FOUND;
		IF NOT l_is_param THEN
			-- XXX column names, use EXECUTE?
			IF l_column_name = 'name' THEN
				l_name := l_new_value;
			ELSIF l_column_name = 'phone' THEN
				l_phone := l_new_value;
			ELSIF l_column_name = 'region' THEN
				l_region := l_new_value;
			ELSIF l_column_name = 'city' THEN
				l_city := l_new_value;
			ELSIF l_column_name = 'type' THEN
				l_type := l_new_value;
			ELSIF l_column_name = 'category' THEN
				l_category := l_new_value;
			ELSIF l_column_name = 'passwd' THEN
				l_passwd := l_new_value;
			ELSIF l_column_name = 'subject' THEN
				l_subject := l_new_value;
			ELSIF l_column_name = 'body' THEN
				l_body := l_new_value;
			ELSIF l_column_name = 'price' THEN
				l_price := l_new_value;
			ELSIF l_column_name = 'infopage' THEN
				l_infopage := l_new_value;
			ELSIF l_column_name = 'infopage_title' THEN
				l_infopage_title := l_new_value;
			ELSIF l_column_name = 'phone_hidden' THEN
				l_phone_hidden := l_new_value;
			ELSIF l_column_name = 'company_ad' THEN
				l_company_ad := l_new_value;
			ELSIF l_column_name = 'user_id' THEN
				l_user_id := l_new_value;
			ELSIF l_column_name = 'store_id' THEN
				l_store_id := l_new_value;
			ELSIF l_column_name = 'salted_passwd' THEN
				l_salted_passwd := l_new_value;
			END IF;
		ELSE -- is_param
			IF l_new_value IS NULL OR l_new_value = '' THEN -- param removed
				DELETE FROM
					ad_params
				WHERE
					ad_id = i_ad_id AND
					name = l_column_name::enum_ad_params_name;
			ELSE 
				UPDATE
					ad_params
				SET
					value = l_new_value
				WHERE
					ad_id = i_ad_id AND
					name = l_column_name::enum_ad_params_name;
				IF NOT FOUND THEN
					INSERT INTO
						ad_params
						       (ad_id,
							name,
							value)
					VALUES
					       (i_ad_id,
						l_column_name::enum_ad_params_name,
						l_new_value);
				END IF;
			END IF;
		END IF;
	END LOOP;
	CLOSE l_ad_changes_cursor;
	
	UPDATE
		ads
	SET
		name = l_name,
		phone = l_phone,
		region = l_region,
		city = l_city,
		type = l_type,
		category = l_category,
		passwd = l_passwd,
		subject = l_subject,
		body = l_body,
		price = l_price,
		infopage = l_infopage,
		infopage_title = l_infopage_title,
		phone_hidden = l_phone_hidden,
		company_ad = l_company_ad,
		user_id = l_user_id,
		store_id = l_store_id,
		salted_passwd = l_salted_passwd
	WHERE
		ad_id = i_ad_id;
	
	IF i_image_state_id IS NOT NULL THEN
		-- Unlink images
		UPDATE
			ad_media	
		SET
			ad_id = NULL
		WHERE	
			COALESCE(media_type, 'image') = 'image' AND
			ad_id = i_ad_id;

		-- Compat. Move ad_image_changes to ad_media
		INSERT INTO 
			ad_media
		SELECT
			TRIM(trailing '.jpg' FROM name)::bigint	AS ad_media_id,
			ad_id,
			seq_no,
			CURRENT_TIMESTAMP AS upload_time,
			'image' AS media_type
		FROM
			ad_image_changes
		WHERE
			ad_id = i_ad_id AND
			action_id = i_action_id AND
			state_id = i_image_state_id AND
			name IS NOT NULL AND
			NOT EXISTS (
				SELECT
					ad_media_id
				FROM
					ad_media
				WHERE
					COALESCE(media_type, 'image') = 'image' AND
					ad_media_id = TRIM(trailing '.jpg' FROM ad_image_changes.name)::bigint
			);

		-- Link new images
		UPDATE
			ad_media
		SET
			ad_id = i_ad_id,
			seq_no = ad_image_changes.seq_no
		FROM
			ad_image_changes
		WHERE
			ad_image_changes.ad_id = i_ad_id AND
			ad_image_changes.action_id = i_action_id AND
			ad_image_changes.state_id = i_image_state_id AND
			ad_image_changes.name IS NOT NULL AND
			ad_media.ad_media_id = TRIM(trailing '.jpg' from ad_image_changes.name)::bigint;
	
		UPDATE
			ad_media
		SET
			ad_id = NULL
		WHERE
			ad_id = i_ad_id AND media_type = 'video';
		
		UPDATE
			ad_media
		SET
			ad_id = i_ad_id
		FROM
			ad_media_changes
		WHERE
			ad_media_changes.ad_id = i_ad_id AND
			ad_media_changes.action_id = i_action_id AND
			ad_media_changes.state_id = i_image_state_id AND
			ad_media.ad_media_id = ad_media_changes.ad_media_id AND
			ad_media.media_type = 'video'
			
		;
	END IF;			
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION autoaccept(i_ad_id ad_actions.ad_id%TYPE,
				      i_action_id ad_actions.action_id%TYPE,
				      i_no_auto_categories integer[],
				      i_category_price_limits integer[][],
				      i_global_price_limit integer,
				      i_blocked_items_lists integer[]) RETURNS BOOL AS $$
DECLARE
	l_action_type	ad_actions.action_type%TYPE;
	l_category	ads.category%TYPE;
	l_price_old	ads.price%TYPE;
	l_price_new	ads.price%TYPE;
	l_original_price ads.price%TYPE;
	l_i		integer;
	l_percentage_diff_price	float8;
	l_subject_old ad_changes.old_value%TYPE;
	l_subject_new ad_changes.new_value%TYPE;
BEGIN
	SELECT
		action_type
	INTO
		l_action_type
	FROM
		ad_actions
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	IF NOT FOUND THEN
		RETURN false;
	END IF;

	-- Autoaccept edit/renew actions with no changes or only price-changes (apart from passwd-changes)
	IF l_action_type NOT IN ('edit', 'renew', 'prolong') THEN
		RETURN false;
	END IF;

	IF EXISTS (SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND column_name NOT IN ('price', 'passwd', 'area', 'salted_passwd', 'phone', 'currency', 'prev_currency', 'lang', 'name', 'country', 'subject', 'geoposition_is_precise', 'geoposition', 'communes', 'address', 'address_number', 'street_id') ) OR
	   EXISTS (SELECT * from ad_image_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND is_new) OR
	   EXISTS (SELECT * FROM ad_changes WHERE ad_id = i_ad_id AND action_id = i_action_id AND column_name IN ('name') AND
			EXISTS (SELECT * FROM blocked_items WHERE list_id = ANY(i_blocked_items_lists) AND ad_changes.new_value LIKE '%' || value || '%' ) ) THEN
		RETURN false; 
	END IF;

	-- Check category.
	l_category := get_changed_value(i_ad_id,
					i_action_id,
					false,
					'category');
	IF l_category = ANY (i_no_auto_categories) THEN
		RETURN false;
	END IF;

	-- Check subject
	-- It's normalized so the comparison ignores case changes
	SELECT
		old_value, new_value
	INTO
		l_subject_old, l_subject_new
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id
		AND NOT is_param
		AND column_name = 'subject';

	IF LOWER(l_subject_old) <> LOWER(l_subject_new) 
	THEN
		RETURN FALSE;
	END IF;


	-- Now it only depends on price
	SELECT
		old_value, new_value
	INTO
		l_price_old, l_price_new
	FROM
		ad_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id
		AND NOT is_param
		AND column_name = 'price';

	SELECT	old_price
	INTO	l_original_price
	FROM	ads
	WHERE	ad_id = i_ad_id;

	-- If there's no "first price", we look for it in the first ad_change (action) of the ad where the price was inserted in an edit...
	IF l_original_price IS NULL THEN
		SELECT	ac.new_value
		INTO	l_original_price
		FROM	ad_changes ac
		INNER JOIN ad_actions aa USING (ad_id, action_id)
		WHERE	ac.ad_id = i_ad_id
		AND	ac.action_id < i_action_id
		AND	ac.old_value IS NULL
		AND	ac.column_name = 'price'
		AND	aa.action_type = 'edit'
		AND	aa.state = 'accepted'
		ORDER BY action_id ASC
		LIMIT	1;
	END IF;

	-- ...otherwise, we get the value from the current 'ad_change' (action), since it is the price the ad currently has.

	/*
	"old_price" is a column in the "ads" table which contains the very first price the ad ever had, or null if it hasn't been edited or was inserted without one.
	Some ads (specially old ads) have been inserted directly in the ads table, without recoding any history about that first value.
	On those cases, we get the current price value from column old_value in table ad_changes.
	 */	
	IF l_original_price IS NULL THEN
		l_original_price = l_price_old;
	END IF;
	
	IF l_price_old IS NULL AND l_price_new IS NULL THEN
		RETURN true;
	ELSIF l_price_old IS NULL AND l_price_new IS NOT NULL THEN
		RETURN false;
	ELSIF l_price_old IS NOT NULL AND l_price_new IS NULL THEN
		RETURN false;
	ELSIF l_price_new <= i_global_price_limit THEN
		RETURN false;
	ELSIF l_original_price IS NULL OR l_original_price = 0 THEN
		RETURN false;
	ELSIF l_price_new >= l_price_old OR l_price_new >= l_original_price THEN
		RETURN true;
	END IF;

	l_percentage_diff_price := cast((l_original_price - l_price_new) as float8) * 100 / l_original_price;

	-- Loop over category percentage limits
	-- TODO: get only needed category percentage in the beginning, instead of passing over the whole 2-dimensional array
	--	 (this is done in call_clear_ad_action.sql.tmpl)
	l_i := 1;
	LOOP
		EXIT WHEN i_category_price_limits[l_i][1] IS NULL;
		IF l_category = i_category_price_limits[l_i][1] THEN
			IF l_percentage_diff_price <= i_category_price_limits[l_i][2] THEN
				RETURN true;
			ELSE
				RETURN false;
			END IF;
		END IF;
		l_i := l_i + 1;
	END LOOP;

	RETURN false;
END;
$$ LANGUAGE plpgsql STABLE;
--
-- This function returns all ad_id+subject with the same uid as the input ad_id.
--
CREATE OR REPLACE FUNCTION find_associated_ads(i_ad_id ads.ad_id%TYPE,
				i_limit integer,
				OUT o_ad_id ads.ad_id%TYPE,
				OUT o_action_id ad_actions.action_id%TYPE,
				OUT o_subject ads.subject%TYPE,
				OUT o_body ads.body%TYPE,
				OUT o_category ads.category%TYPE,
				OUT o_list_id ads.list_id%TYPE,
				OUT o_list_time ads.list_time%TYPE,
				OUT o_status ads.status%TYPE,
				OUT o_type ads.type%TYPE,
				OUT o_region ads.region%TYPE,
				OUT o_city ads.city%TYPE,
				OUT o_current_state ad_actions.current_state%TYPE,
				OUT o_price ads.price%TYPE,
				OUT o_ad_params text,
				OUT o_image char(10),
				OUT o_score FLOAT) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_uid users.uid%TYPE;
BEGIN
	--
	-- Get uid from users
	--
	SELECT users.uid INTO l_uid
	FROM
		ads join users using(user_id)
	WHERE
		ads.ad_id = i_ad_id;

	--
	-- Loop through all ads with same uid and return matches.
	--
	FOR
		l_rec
	IN
		SELECT
			ad_id,
			subject,
			body,
			category,
			type,
			status,
			COALESCE(list_id, 0) AS list_id,
			list_time,
			city,
			region,
			price,
			score_duplicate_ad(i_ad_id, ads.ad_id) as score,
			(SELECT COALESCE((SELECT lpad(ad_media_id::text, 10, '0') || '.jpg' FROM ad_media WHERE ad_id = ads.ad_id AND seq_no=0),'0')) as image
		FROM
			ads join users using(user_id)
		WHERE
			users.uid = l_uid AND
			ads.ad_id != i_ad_id AND
			ads.status != 'deleted'
		ORDER BY score DESC
		LIMIT i_limit
	LOOP
		o_ad_id := l_rec.ad_id;
		o_subject := l_rec.subject;
		o_body := l_rec.body;
		o_type := l_rec.type;
		o_region := l_rec.region;
		o_city := l_rec.city;
		o_status := l_rec.status;
		o_list_id := l_rec.list_id;
		o_score := l_rec.score;
		o_image := l_rec.image;
		o_category := l_rec.category;
		o_price :=l_rec.price;
		--
		-- Select the last paid/verified but not refused action_id and its current state
		--
		SELECT
			MAX(action_id)
		INTO
			o_action_id
		FROM
			ad_actions
		WHERE
			ad_id = o_ad_id AND
			state IN ('pending_review', 'locked', 'accepted', 'deactivated');

		IF o_action_id IS NULL THEN
			CONTINUE;
		END IF;

		SELECT current_state INTO o_current_state
		FROM
			ad_actions
		WHERE
			ad_actions.ad_id = o_ad_id AND
			ad_actions.action_id = o_action_id;

		IF o_status IN ('active') THEN
			 o_list_time := l_rec.list_time;
		ELSE
			SELECT
				MAX(action_states.timestamp)
			INTO
				o_list_time
			FROM
				action_states
			WHERE
				action_states.state_id = o_current_state AND
				action_states.state IN ('pending_review', 'locked', 'deactivated');
			
			IF o_list_time IS NULL THEN
				CONTINUE;
			END IF;
		END IF;

		-- Concat all ad param values
		SELECT
			GROUP_CONCAT_ORDERED(name || ':' || value)
		INTO
			o_ad_params
		FROM
			ad_params
		WHERE
			ad_id = o_ad_id;

		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION get_account_ads2(
					i_user_id 				users.user_id%TYPE,
					i_account_id 			accounts.account_id%TYPE,
					i_query 		        varchar,
					i_pack_sort				integer,
					i_all_params			integer,
					OUT o_ad_id          	ads.ad_id%TYPE,
					OUT o_list_id        	ads.list_id%TYPE,
					OUT o_list_time      	ads.list_time%TYPE,
					OUT o_subject        	ads.subject%TYPE,
					OUT o_body				ads.body%TYPE,
					OUT o_company_ad		integer,
					OUT o_ad_type			ads.type%TYPE,
					OUT o_category          ads.category%TYPE,
					OUT o_price          	ads.price%TYPE,
					OUT o_ad_status        	varchar,
					OUT o_ad_media_id    	BIGINT,
					OUT o_image_count		BIGINT,
					OUT o_daily_bump		varchar,
					OUT o_weekly_bump		varchar,
					OUT o_label				varchar,
					OUT o_gallery_date		varchar,
					OUT o_ad_pack_status	varchar,
					OUT o_ad_params_list	varchar,
					OUT o_auto_bump         varchar
) RETURNS SETOF record AS $$
DECLARE
	l_account_status 	accounts.status%TYPE;
	l_rec 			record;
	l_action_type 		ad_actions.action_type%TYPE;
	l_action_state 		ad_actions.state%TYPE;
	l_subject_changed	TEXT;
	l_category_changed  TEXT;
	l_price_changed	        TEXT;
	l_image_0_changed 	TEXT;
	l_image_count_changed	integer;
	l_time_min timestamp;
BEGIN
	IF i_user_id IS NOT NULL THEN
		SELECT status INTO l_account_status FROM accounts WHERE user_id = i_user_id;
	ELSE
		SELECT user_id,status INTO i_user_id,l_account_status FROM accounts WHERE account_id = i_account_id;
	END IF;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	l_time_min = CURRENT_TIMESTAMP - (INTERVAL '60 minutes');

	FOR
		l_rec
	IN
		SELECT
			ad_id,
			list_id,
			list_time,
			subject,
			REPLACE(body, E'\n', '<br>') as body,
			category,
			price,
			ad_status,
			type,
			company_ad,
			ad_media_id,
			image_count,
			daily_bump,
			weekly_bump,
			label,
			gallery_date,
			ad_pack_status,
			ad_params_list,
			last_change,
			auto_bump
		FROM
			dashboard_ads
		WHERE
			user_id = i_user_id
		and ad_status not in ('hidden', 'refused')
		and (
			CASE
				WHEN ad_status = 'deleted' AND last_change < l_time_min  THEN false ELSE true
			END
		)
		and (
			CASE
				WHEN i_query is not null THEN lower(subject) like '%'||i_query||'%'
				ELSE true
			END
		)
	ORDER BY
		(CASE
			WHEN i_pack_sort IS NULL THEN 1
			WHEN i_pack_sort = 1 AND ad_pack_status = '1' THEN 1
			WHEN i_pack_sort = 1 AND ad_pack_status = '0' THEN 2
			WHEN i_pack_sort = 0 AND ad_pack_status = '1' THEN 2
			WHEN i_pack_sort = 0 AND ad_pack_status = '0' THEN 1
			ELSE 3
		END),
		(CASE
			WHEN ad_status = 'inactive' THEN 1
			WHEN ad_status = 'refused' THEN 2
			ELSE 3
		END),
		list_time DESC,
		ad_id DESC

	LOOP
		o_ad_id        := l_rec.ad_id;
		o_list_id      := l_rec.list_id;
		o_list_time    := l_rec.list_time;
		o_subject      := l_rec.subject;
		o_body         := l_rec.body;
		o_category     := l_rec.category;
		o_price        := l_rec.price;
		o_ad_status    := l_rec.ad_status;
		o_ad_type      := l_rec.type;
		o_company_ad   := l_rec.company_ad::INTEGER;

		o_ad_media_id  := l_rec.ad_media_id;
		o_image_count  := l_rec.image_count;

		o_daily_bump   := l_rec.daily_bump;
		o_weekly_bump  := l_rec.weekly_bump;
		o_gallery_date := l_rec.gallery_date;
		o_label        := l_rec.label;

		o_ad_pack_status := l_rec.ad_pack_status;
		o_auto_bump      := l_rec.auto_bump;

		IF i_all_params IS NOT NULL AND i_all_params = 1 THEN
			o_ad_params_list := l_rec.ad_params_list;
		ELSE
			o_ad_params_list := '';
		END IF;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION get_account_ads(
					i_user_id 				users.user_id%TYPE,
					i_account_id 			accounts.account_id%TYPE,
					i_query 		        varchar,
					i_pack_sort				integer,
					i_all_params			integer,
					OUT o_ad_id          	ads.ad_id%TYPE,
					OUT o_list_id        	ads.list_id%TYPE,
					OUT o_list_time      	ads.list_time%TYPE,
					OUT o_subject        	ads.subject%TYPE,
					OUT o_body				ads.body%TYPE,
					OUT o_company_ad		integer,
					OUT o_ad_type			ads.type%TYPE,
					OUT o_category          ads.category%TYPE,
					OUT o_price          	ads.price%TYPE,
					OUT o_ad_status        	varchar,
					OUT o_ad_media_id    	BIGINT,
					OUT o_image_count		BIGINT,
					OUT o_daily_bump		varchar,
					OUT o_weekly_bump		varchar,
					OUT o_label				varchar,
					OUT o_gallery_date		varchar,
					OUT o_ad_pack_status	varchar,
					OUT o_ad_params_list	varchar
) RETURNS SETOF record AS $$
DECLARE
	l_account_status 	accounts.status%TYPE;
	l_rec 			record;
	l_action_type 		ad_actions.action_type%TYPE;
	l_action_state 		ad_actions.state%TYPE;
	l_subject_changed	TEXT;
	l_category_changed  TEXT;
	l_price_changed	        TEXT;
	l_image_0_changed 	TEXT;
	l_image_count_changed	integer;
	l_time_min timestamp;
BEGIN
	IF i_user_id IS NOT NULL THEN
		SELECT
			status
		INTO
			l_account_status
		FROM
			accounts
		WHERE
			user_id = i_user_id;
	ELSE
		SELECT
			user_id,
			status
		INTO
			i_user_id,
			l_account_status
		FROM
			accounts
		WHERE
			 account_id = i_account_id;
	END IF;

	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_ACCOUNT_NOT_FOUND';
	END IF;

	l_time_min = CURRENT_TIMESTAMP - (INTERVAL '60 minutes');

	FOR
		l_rec
	IN
		SELECT
			ads.ad_id,
			ads.list_id,
			ads.list_time,
			ads.subject,
			REPLACE(body, E'\n', '<br>') as body,
			ads.category,
			ads.price,
			ads.status,
			ads.type,
			ads.company_ad,
			aac.action_id,
			aac.action_type,
			aac.state,
			aac.current_state,
			ad_media_id,
			CASE WHEN ap1.name = 'daily_bump' THEN ap1.value
			 	 WHEN ap1.name = 'upselling_daily_bump' THEN (ap1.value::INTEGER - 1)::VARCHAR
			END as daily_bump,
			CASE WHEN ap2.name = 'weekly_bump' THEN ap2.value
			 	 WHEN ap2.name = 'upselling_weekly_bump' THEN (ap2.value::INTEGER - 1)::VARCHAR
			END as weekly_bump,
			ap3.value as gallery_date,
			ap4.value as pack_status,
			ap5.value as label
		FROM
			ads
			INNER JOIN ad_actions 		aac USING(ad_id)
			INNER JOIN action_states 	act ON (aac.ad_id = act.ad_id AND aac.action_id = act.action_id AND aac.current_state = act.state_id)
			LEFT JOIN ad_media ON (ad_media.ad_id = ads.ad_id AND ad_media.seq_no = 0)
			LEFT JOIN ad_params ap1 ON (ads.ad_id = ap1.ad_id AND (ap1.name = 'daily_bump' OR ap1.name = 'upselling_daily_bump'))
			LEFT JOIN ad_params ap2 ON (ads.ad_id = ap2.ad_id AND (ap2.name = 'weekly_bump' OR ap2.name = 'upselling_weekly_bump'))
			LEFT JOIN ad_params ap3 ON (ads.ad_id = ap3.ad_id AND ap3.name = 'gallery')
			LEFT JOIN ad_params ap4 ON (ads.ad_id = ap4.ad_id AND ap4.name = 'pack_status')
			LEFT JOIN ad_params ap5 ON (ads.ad_id = ap5.ad_id AND ap5.name = 'label')

		WHERE
			ads.user_id = i_user_id
		and aac.action_id = (select max(action_id) from ad_actions where ad_id = ads.ad_id )
		and ads.status != 'hidden'
		and (
			CASE
				WHEN ads.status = 'refused' THEN aac.action_type = 'editrefused' AND act.state != 'refused'
				WHEN ads.status = 'deleted' AND act.timestamp < l_time_min  THEN false
				ELSE true
			END
		)
		and (
			CASE
				WHEN i_query is not null THEN lower(ads.subject) like '%'||i_query||'%'
				ELSE true
			END
		)
				
	ORDER BY
		(CASE
			WHEN i_pack_sort IS NULL THEN 1
			WHEN i_pack_sort = 1 AND ap4.value = 'active' THEN 1
			WHEN i_pack_sort = 1 AND ap4.value = 'disabled' THEN 2
			WHEN i_pack_sort = 0 AND ap4.value = 'active' THEN 2
			WHEN i_pack_sort = 0 AND ap4.value = 'disabled' THEN 1
			ELSE 3
		END),
		(CASE
			WHEN ads.status = 'inactive' THEN 1
			WHEN ads.status = 'refused' THEN 2
			ELSE 3
		END),
		ads.list_time DESC,
		ads.ad_id DESC

	LOOP
		o_ad_id        := l_rec.ad_id;
		o_list_id      := l_rec.list_id;
		o_list_time    := l_rec.list_time;
		o_subject      := l_rec.subject;
		o_body         := l_rec.body;
		o_category     := l_rec.category;
		o_price        := l_rec.price;
		o_ad_status    := l_rec.status;
		o_ad_type      := l_rec.type;
		o_company_ad   := l_rec.company_ad::INTEGER;
		o_ad_media_id  := l_rec.ad_media_id;
		l_action_type  := l_rec.action_type;
		l_action_state := l_rec.state;
		o_daily_bump   := l_rec.daily_bump;
		o_weekly_bump  := l_rec.weekly_bump;
		o_gallery_date := l_rec.gallery_date;
		o_label        := l_rec.label;

		IF i_all_params IS NOT NULL THEN
			SELECT STRING_AGG(name || ',' || REPLACE(value, E'\n', '<br>'), '|&|') INTO o_ad_params_list FROM ad_params WHERE ad_id = o_ad_id;
		END IF;

		SELECT
			COUNT(1)
		INTO
			o_image_count
		FROM
			ad_media
		WHERE
			ad_media.ad_id = l_rec.ad_id;

		IF l_action_type = 'editrefused' AND NOT (o_ad_status = 'disabled' and l_action_state = 'accepted') THEN
			o_ad_status := l_action_type;

			IF l_rec.state = 'refused' AND l_rec.action_type = 'new' THEN
				o_ad_status := 'refused';
			END IF;

			select new_value
			into l_subject_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'subject' ;
			IF FOUND THEN
				o_subject := l_subject_changed;
			END IF;

			select new_value
			into l_category_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'category' ;
			IF FOUND THEN
				o_category := l_category_changed;
			END IF;

			select new_value
			into l_price_changed
			from ad_changes
			where ad_id = l_rec.ad_id
			      and action_id = l_rec.action_id
				 and column_name = 'price' ;
			IF FOUND THEN
				o_price := cast(nullif(l_price_changed,'') as integer);
			END IF;


			select substring(name from 0 for position('.jpg' in name )) as id
			INTO l_image_0_changed
			from ad_image_changes
			where ad_id = l_rec.ad_id
				and action_id = l_rec.action_id
				and seq_no = 0;
			IF FOUND THEN
				o_ad_media_id := cast(nullif(l_image_0_changed,'') as BIGINT);
			END IF;


			select count(1)
			INTO l_image_count_changed
			FROM ad_image_changes
			WHERE ad_id = l_rec.ad_id
				AND name IS NOT NULL
				AND name != ''
				and action_id = l_rec.action_id;
			IF FOUND THEN
				o_image_count := l_image_count_changed;
			END IF;

		END IF;

		-- This is used to recognize if the ad is a pack ad being shown
		SELECT
			CASE
				WHEN new_value = 'deactivated' THEN '0'
				WHEN new_value = 'active' THEN '1'
				WHEN new_value = 'disabled' THEN '0'
			END
		INTO o_ad_pack_status
		FROM ad_changes
		WHERE ad_id = l_rec.ad_id AND column_name = 'pack_status'
		ORDER BY action_id DESC
		LIMIT 1;

		IF NOT FOUND THEN
			o_ad_pack_status = '';
		END IF;

		-- The special cases
		IF o_ad_status = 'active' THEN
			-- This is used to recognize if the ad is a pack ad being shown
			IF o_ad_pack_status = '1' THEN
				o_ad_status = 'pack_show';
			END IF;

			IF EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = o_ad_id LIMIT 1) THEN
				o_ad_status = 'review-edit';
			END IF;

			IF l_action_type = 'status_change' AND l_action_state = 'accepted' AND o_ad_status != 'pack_show' THEN
				o_ad_status = 'admin_show';
			END IF;

			IF l_action_type  = 'undo_delete' AND l_action_state = 'undo_deleted' THEN
				o_ad_status = 'undo_deleted';
			END IF;

			IF l_action_type  = 'editrefused' AND l_action_state = 'undo_deleted' THEN
				o_ad_status = 'undo_deleted';
			END IF;
		END IF;

		IF o_ad_status = 'disabled' THEN
			IF EXISTS (SELECT 'edited' FROM ad_actions WHERE state = 'pending_review' AND ad_id = o_ad_id LIMIT 1) THEN
				o_ad_status = 'review-edit-disabled';
			END IF;
		END IF;

		RETURN NEXT;
	END LOOP;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION get_ads_by_uid(
	i_uid users.uid%TYPE,
	i_email users.email%TYPE,
	i_ad_status enum_ads_status,
	i_ad_id integer,
	OUT o_ad_id ads.ad_id%TYPE,
	OUT o_subject ads.subject%TYPE,
	OUT o_body ads.body%TYPE,
	OUT o_category ads.category%TYPE,
	OUT o_status ads.status%TYPE,
	OUT o_list_id ads.list_id%TYPE,
	OUT o_list_time ads.list_time%TYPE,
	OUT o_price ads.price%TYPE,
	OUT o_currency varchar,
	OUT o_image char(10),
	OUT o_email varchar,
	OUT o_account_id accounts.account_id%TYPE,
	OUT o_require_password boolean,
	OUT o_pack_status varchar
) RETURNS SETOF record AS $$
DECLARE
	l_rec record;
	l_uid users.uid%TYPE;
	l_require_password boolean;
BEGIN
	SELECT uid INTO l_uid FROM users where email = i_email;

	IF NOT EXISTS( SELECT *
	FROM users JOIN accounts USING (user_id)
	WHERE users.uid = l_uid AND accounts.status = 'active'
	) THEN
		l_require_password := true;
	END IF;

	FOR
		l_rec
	IN
		SELECT
			ads.ad_id,
			subject,
			body,
			category,
			ads.status,
			COALESCE(list_id, 0) AS list_id,
			list_time,
			price,
			(SELECT COALESCE((SELECT lpad(ad_media_id::text, 10, '0') || '.jpg' FROM ad_media WHERE ad_id = ads.ad_id AND seq_no=0), '0')) as image,
			users.email,
			COALESCE(account_id, 0) AS account_id,
			users.email <> i_email AS require_password,
			ap1.value AS pack_status,
			COALESCE(ap2.value, 'peso') AS currency
		FROM
			users
			JOIN ads USING (user_id)
			LEFT JOIN accounts ON(ads.user_id = accounts.user_id AND accounts.status = 'active')
			LEFT JOIN ad_params ap1 ON(ads.ad_id = ap1.ad_id AND ap1.name = 'pack_status')
			LEFT JOIN ad_params ap2 ON(ads.ad_id = ap2.ad_id AND ap2.name = 'currency')
		WHERE
			(uid = l_uid or uid = i_uid) AND (i_ad_id != ads.ad_id) AND ads.status IN (i_ad_status)
	LOOP
		o_ad_id := l_rec.ad_id;
		o_subject := l_rec.subject;
		o_body := l_rec.body;
		o_category := l_rec.category;
		o_status := l_rec.status;
		o_list_id := l_rec.list_id;
		o_list_time := l_rec.list_time;
		o_price := l_rec.price;
		o_currency := l_rec.currency;
		o_image := l_rec.image;
		o_email := l_rec.email;
		o_account_id := l_rec.account_id;
		o_pack_status := l_rec.pack_status;
		IF l_require_password THEN
			o_require_password := l_require_password;
		ELSE
			IF o_account_id = 0 THEN
				o_require_password := true;
			ELSE
				o_require_password := l_rec.require_password;
			END IF;
		END IF;
		RETURN NEXT;
	END LOOP;
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION insert_all_ad_changes(i_ad_id action_states.ad_id%TYPE,
						 i_action_id action_states.action_id%TYPE,
						 i_state_id action_states.state_id%TYPE,
						 i_ads_name ads.name%TYPE,
						 i_ads_phone ads.phone%TYPE,
						 i_ads_region ads.region%TYPE,
						 i_ads_city ads.city%TYPE,
						 i_ads_type ads.type%TYPE,
						 i_ads_category ads.category%TYPE,
						 i_ads_passwd ads.salted_passwd%TYPE,
						 i_user_id ads.user_id%TYPE,
						 i_store_id ads.store_id%TYPE,
						 i_phone_hidden ads.phone_hidden%TYPE,
						 i_company_ad ads.company_ad%TYPE,
						 i_ads_subject ads.subject%TYPE,
						 i_ads_body ads.body%TYPE,
						 i_ads_price ads.price%TYPE,
						 i_ads_infopage ads.infopage%TYPE,
						 i_ads_infopage_title ads.infopage_title%TYPE, 
						 i_ad_images varchar[],
						 i_ad_thumb_digest varchar[],
						 i_ad_image_had_digest bool[],
						 i_ad_params varchar[][],
						 i_edit_type varchar(20),
						 i_static_params text[],
 						 i_lang varchar) RETURNS VOID AS $$
DECLARE
	l_name ads.name%TYPE;
	l_phone ads.phone%TYPE;
	l_region ads.region%TYPE;
	l_city ads.city%TYPE;
	l_type ads.type%TYPE;
	l_category ads.category%TYPE;
	l_passwd ads.passwd%TYPE;
	l_subject ads.subject%TYPE;
	l_body ads.body%TYPE;
	l_price ads.price%TYPE;
	l_infopage ads.infopage%TYPE;
	l_infopage_title ads.infopage_title%TYPE; 
	l_old_param_value ad_params.value%TYPE;
	l_phone_hidden ads.phone_hidden%TYPE;
	l_company_ad ads.company_ad%TYPE;
	l_user_id ads.user_id%TYPE;
	l_i integer;
	l_ad_param_name VARCHAR; --ad_params.name%TYPE;
	l_ad_param_value ad_params.value%TYPE;
	l_is_new_image bool;
	l_image ads.image%TYPE;
	l_digest ad_images_digests.digest%TYPE;
	l_uid users.uid%TYPE;
	l_old_cat ads.category%TYPE;
	l_ad_image_name ad_image_changes.name%TYPE;
	l_ad_image_id bigint;
	l_image_ad_id action_states.ad_id%TYPE;
	l_temp_ad_status ads.status%TYPE;
	l_skip_image_changes boolean;
BEGIN
	-- Get uid
        SELECT
                uid
        INTO
                l_uid
        FROM
                users
        WHERE
                user_id = i_user_id;
	
	-- Ad images 
	IF i_ad_images[1] IS NULL THEN

		INSERT INTO
			ad_image_changes (
				ad_id,
				action_id,
				state_id,
				seq_no,
				name,
				is_new)
		VALUES (
			i_ad_id,
			i_action_id,
			i_state_id,
			0,
			NULL,
			'f');
	ELSE
		l_i := 1;
		LOOP
			l_ad_image_name := i_ad_images[l_i];
			l_skip_image_changes := FALSE;
			EXIT WHEN l_ad_image_name IS NULL;

			l_ad_image_id := TRIM(trailing '.jpg' FROM l_ad_image_name)::bigint;
			SELECT
				ad_media.ad_id,
				ads.status
			FROM
				ad_media
			LEFT JOIN
				ads USING (ad_id)
			WHERE
				ad_media_id = l_ad_image_id
			INTO
				l_image_ad_id,
				l_temp_ad_status;

			-- This is to prevent img theft
			IF (l_image_ad_id IS NOT NULL AND l_image_ad_id != i_ad_id) THEN
				RAISE NOTICE 'ERROR_INVALID_UPDATE_TO_ADMEDIA';
				IF l_temp_ad_status = 'active' THEN
					l_skip_image_changes := TRUE;
				END IF;
			END IF;

			-- If the image is new or not
			l_is_new_image := l_image_ad_id IS NULL;

			IF l_skip_image_changes = FALSE THEN 
				INSERT INTO
					ad_image_changes (
						ad_id,
						action_id,
						state_id,
						seq_no,
						name,
						is_new)
				VALUES (
					i_ad_id,
					i_action_id,
					i_state_id,
					l_i - 1,
					l_ad_image_name,
					COALESCE(l_is_new_image, 'f'));
			END IF;

			IF COALESCE(length(i_ad_thumb_digest[l_i]), 0) = 32 THEN
				-- Add extra images thumb digests
				SELECT 
					digest 
				INTO
					l_digest
				FROM
					ad_images_digests
				WHERE
					ad_id = i_ad_id AND
					name = l_ad_image_name;

				IF l_digest IS NULL THEN
					INSERT
					INTO
						ad_images_digests
							(name,
							digest,
							ad_id,
							previously_inserted,
							uid)
					VALUES
						(l_ad_image_name,
						i_ad_thumb_digest[l_i],
						i_ad_id,
						i_ad_image_had_digest[l_i],
						l_uid);
				ELSE
					UPDATE
						ad_images_digests
					SET
						digest = i_ad_thumb_digest[l_i],
						previously_inserted = i_ad_image_had_digest[l_i]
					WHERE	
						ad_id = i_ad_id AND
						name = l_ad_image_name;
				END IF;
			END IF;

			l_i := l_i + 1;
		END LOOP;
	END IF;
	
	IF i_edit_type != 'review' THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'phone', i_ads_phone);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'city', i_ads_city::text);
		IF i_ads_passwd IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'salted_passwd', i_ads_passwd);
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'passwd', NULL);
		END IF;
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'subject', i_ads_subject);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'body', i_ads_body);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'phone_hidden', i_phone_hidden::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage', i_ads_infopage);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage_title', i_ads_infopage_title);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'store_id', i_store_id::text);
	ELSIF i_ads_infopage IS NULL THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage', NULL);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'infopage_title', NULL);
	END IF;
	PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'price', i_ads_price::text);

	IF i_company_ad IS NOT NULL THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'company_ad', i_company_ad::text);
	END IF;
	IF i_edit_type IN ('adminedit', 'new', 'prolong', 'editrefused', 'import', 'edit') THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'name', i_ads_name);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'region', i_ads_region::text);
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'type', i_ads_type::text);
	ELSIF i_edit_type = 'review' THEN
		IF i_ads_subject IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'subject', i_ads_subject);
		END IF;
		IF i_ads_body IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'body', i_ads_body);
		END IF;
		IF i_ads_category IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		END IF;
		IF i_ads_type IS NOT NULL THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'type', i_ads_type::text);
		END IF;
	END IF;
	-- Allow category change from old category (<1000) to new category 
	-- XXX: Prolong is matched above but should it be?? 
	IF i_edit_type IN ('edit', 'renew') THEN 
		SELECT
			category
		INTO
			l_old_cat
		FROM
			ads
		WHERE
			ad_id = i_ad_id;

		IF l_old_cat < 1000 AND  i_ads_category >= 1000 THEN
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'category', i_ads_category::text);
		END IF;
	END IF;
	IF i_edit_type IN ('adminedit', 'import') THEN
		PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, false, 'user_id', i_user_id::text);
	END IF;

	-- Add new/changed params
	IF i_ad_params IS NOT NULL THEN
		l_i := 1;
	
		LOOP
			l_ad_param_name := i_ad_params[l_i][1];
			l_ad_param_value := i_ad_params[l_i][2];
			EXIT WHEN l_ad_param_name IS NULL;
			
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, true, l_ad_param_name,
						 l_ad_param_value);
			l_i := l_i + 1;
		END LOOP;
	END IF;
	
	-- Add removed params
	DECLARE 
		rec_ad_params RECORD;
	BEGIN
		FOR
			rec_ad_params
		IN
			SELECT
				CAST( name AS VARCHAR )
			FROM
				ad_params
			WHERE
				ad_id = i_ad_id AND
				(i_ad_params IS NULL OR
				CAST( name AS VARCHAR ) <> ALL (i_ad_params[1:array_upper(i_ad_params,1)][1])) AND
				CAST( name AS VARCHAR ) <> ALL (i_static_params)
		LOOP
			PERFORM insert_ad_change(i_ad_id, i_action_id, i_state_id, true, rec_ad_params.name, NULL);
		END LOOP;
	END;
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION review_accept(i_ad_id ad_actions.ad_id%TYPE,
					 i_action_id ad_actions.action_id%TYPE,
					 i_token tokens.token%TYPE,
					 i_remote_addr action_states.remote_addr%TYPE,
					 i_filter_name filters.name%TYPE,
					 i_ads_type ads.type%TYPE,
					 i_ads_category ads.category%TYPE,
					 i_company_ad ads.company_ad%TYPE,
					 i_ads_price ads.price%TYPE,
					 i_ads_subject ads.subject%TYPE,
					 i_ads_body ads.body%TYPE,
					 i_remove_infopage bool,
					 i_remove_images bool[],
					 i_ad_params varchar[][],
					 i_valid_params varchar[],
					 i_gallery_expire_minutes integer,
					 i_set_first smallint,
					 i_lang varchar,
					 i_minimal_review_description state_params.value%TYPE,
					 i_detailed_review_description state_params.value%TYPE,
					 OUT o_list_id ads.list_id%TYPE,
					 OUT o_first_listing bool,
					 OUT o_redir_code redir_stats.code%TYPE,
					 OUT o_remote_addr action_states.remote_addr%TYPE,
					 OUT o_action_type ad_actions.action_type%TYPE,
					 OUT o_company_ad ads.company_ad%TYPE) AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
	l_list_time ads.list_time%TYPE;
	l_orig_list_time ads.list_time%TYPE;
	l_action_type ad_actions.action_type%TYPE;
	l_action_type_before_refuse ad_actions.action_type%TYPE;
	l_ads_category ads.category%TYPE;
	l_ad_status ads.status%TYPE;
	l_img_change_state action_states.state_id%TYPE;
	l_ad_images varchar[];
	l_i integer;
	l_rec record;
	l_ads_price ads.price%TYPE;
	l_price ads.price%TYPE;
	l_currency varchar;
	l_old_price ads.price%TYPE;
	l_prev_currency varchar;
	l_status integer;
	l_user_id ads.user_id%TYPE;
	l_first_approved_ad user_params.value%TYPE;
	l_external_ad_id ads.list_id%TYPE;
	l_admin_id integer;
	l_pack_status varchar;
	l_pack_act_id ad_actions.action_id%TYPE;
BEGIN
	-- Get price
	SELECT
		price,
		old_price,
		user_id
	INTO
		l_price,
		l_old_price,
		l_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	-- Get currency
	SELECT
		value
	INTO
		l_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'currency';

	SELECT
		value
	INTO
		l_prev_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'prev_currency';

	-- Insert the accepted state
	l_state_id := insert_state(i_ad_id,
				   i_action_id, 
				   'accepted',
				   'accept', 
				   i_remote_addr, 
				   get_token_id(i_token));
	INSERT INTO
		state_params 
	VALUES (
		i_ad_id,
		i_action_id,
		l_state_id,
		'filter_name',
		i_filter_name);

	IF i_minimal_review_description IS NOT NULL THEN
		INSERT INTO
			state_params
		VALUES (
			i_ad_id,
			i_action_id,
			l_state_id,
			'minimal_review_description',
			i_minimal_review_description
		);
	END IF;

	IF i_detailed_review_description IS NOT NULL THEN
		INSERT INTO
			state_params
		VALUES (
			i_ad_id,
			i_action_id,
			l_state_id,
			'detailed_review_description',
			i_detailed_review_description
		);
	END IF;

	-- Build up a new array of images from the old one and i_remove_images
	SELECT
		MAX(state_id)
	INTO
		l_img_change_state
	FROM
		ad_image_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id;

	IF i_set_first IS NOT NULL THEN
		l_i := 1;
	ELSE
		l_i := 0;
	END IF;

	IF l_img_change_state IS NOT NULL THEN
		FOR
			l_rec
		IN
			SELECT
				name,
				seq_no
			FROM
				ad_image_changes
			WHERE
				ad_id = i_ad_id
				AND action_id = i_action_id
				AND state_id = l_img_change_state
			ORDER BY
				seq_no
		LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images
				l_ad_images[1] := l_rec.name;
			ELSE
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;
	ELSE
		FOR
			l_rec
		IN
			SELECT
				ad_media_id||'.jpg' AS name,
				seq_no
			FROM
				ad_media
			WHERE
				COALESCE(media_type, 'image') = 'image' AND
				ad_id = i_ad_id
			ORDER BY
				seq_no

		LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images
				l_ad_images[1] := l_rec.name;
			ELSE
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;
	END IF;

	IF i_ads_price IS NULL AND i_token IS NULL THEN
		-- Autoaccept doesn't send the price. Find it.
		l_ads_price := get_changed_value(i_ad_id,
						 i_action_id,
						 false,
						 'price');
	ELSE
		l_ads_price := i_ads_price;
	END IF;
	PERFORM insert_all_ad_changes(i_ad_id,
				      i_action_id,
				      l_state_id,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_ads_type,
				      i_ads_category,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_company_ad,
				      i_ads_subject,
				      i_ads_body,
				      l_ads_price,
				      CASE WHEN i_remove_infopage THEN NULL ELSE '' END,
				      NULL,
				      l_ad_images,
				      NULL,
				      NULL,
				      i_ad_params,
				      'review',
				      i_valid_params,  -- Mark all valid params as static, so they're not deleted
				      i_lang);

	-- Get current status information
	SELECT
		list_id,
		list_time,
		orig_list_time,
		action_type,
		status,
		company_ad
	INTO
		o_list_id,
		l_list_time,
		l_orig_list_time,
		l_action_type,
		l_ad_status,
		o_company_ad
	FROM
		ad_actions
		JOIN ads USING (ad_id)
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Get link_type
	SELECT
		value::integer
	INTO
		l_external_ad_id
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND
		name = 'external_ad_id' AND
		ad_id = (
			SELECT 
				ad_id
			FROM
				ad_params
			WHERE
				name = 'link_type' AND
				value = 'spidered_balcao' AND
				ad_id = i_ad_id
		);

	-- Check if the ad never been listed
	IF o_list_id IS NULL THEN
		IF l_external_ad_id IS NULL THEN
			o_list_id := NEXTVAL('list_id_seq');
		ELSE
			o_list_id := l_external_ad_id;
		END IF;
		o_first_listing := true;
	ELSE
		o_first_listing := false;
	END IF;

	PERFORM accept_action (i_ad_id, i_action_id, NULL, i_remote_addr, i_token, i_gallery_expire_minutes,
			l_action_type, l_state_id);

	-- Store the original list_time
	IF l_orig_list_time IS NULL AND l_list_time IS NOT NULL THEN
		l_orig_list_time := l_list_time;
	END IF;

	-- Update list_time if needed
	IF l_list_time IS NULL OR l_action_type IN ('renew', 'prolong') OR (l_action_type IN ('editrefused') AND l_ad_status IN ('refused')) THEN
		l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
	ELSE
		SELECT
			atbr.o_action_type
		INTO
			l_action_type_before_refuse
		FROM
			action_type_before_refuse(i_ad_id, i_action_id, NULL) AS atbr;

		IF l_action_type_before_refuse IN ('renew', 'prolong') THEN
			l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
		END IF;
	END IF;

	-- Reset the orig_list_time if ad is prolonged or if list_time never changed
	IF l_orig_list_time = l_list_time OR l_action_type IN ('prolong') OR (l_action_type IN ('editrefused') AND l_action_type_before_refuse IN ('prolong')) THEN
		l_orig_list_time := NULL;
	END IF;

	-- Set old_price to the original price
	-- accept_action sets old_price only if it changes with this action, we want to set it always.
	-- XXX set old_price only when price changes is maybe better?
	IF l_old_price IS NULL THEN
		l_old_price := l_price;

		IF l_currency IS NULL THEN
			l_prev_currency := 'peso';
		ELSE
			l_prev_currency := l_currency;
		END IF;

		IF l_ads_price = l_old_price THEN
			l_old_price := NULL;
			l_prev_currency := NULL;
		END IF;
	ELSIF l_old_price IS NOT NULL AND (l_action_type IN ('prolong') OR l_action_type_before_refuse IN ('prolong')) THEN
		l_old_price := NULL;
		l_prev_currency := NULL;
	END IF;

	-- Chech if the ad have the ad_param pack_status
	SELECT
		value
	INTO
		l_pack_status
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'pack_status';

	IF NOT FOUND THEN
		l_pack_status := 'active';
	END IF;

	-- Update ad status
	UPDATE
		ads
	SET
		status = l_pack_status::enum_ads_status,
		list_id = o_list_id,
		list_time = l_list_time,
		orig_list_time = l_orig_list_time,
		old_price = l_old_price,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;


       -- update subject
       IF i_ads_subject IS NOT NULL THEN
               UPDATE
			ads
               SET
			subject = i_ads_subject
               WHERE
			ad_id = i_ad_id;
       END IF;

       -- update body
       IF i_ads_body IS NOT NULL THEN
               UPDATE
			ads
               SET
			body = i_ads_body
               WHERE
			ad_id = i_ad_id;
       END IF;

	-- Inserts/updates prev_currency in ad_params
	IF l_prev_currency IS NOT NULL THEN
		IF NOT EXISTS (SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'prev_currency') THEN
			INSERT INTO
				ad_params
			VALUES
				(i_ad_id, 'prev_currency', l_prev_currency);
		ELSE
			UPDATE
				ad_params
			SET
				value = l_prev_currency
			WHERE
				ad_id = i_ad_id AND name = 'prev_currency';
		END IF;
	END IF;

	--Add new action to activate trigger
	IF l_action_type in ('new') AND EXISTS ( SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'pack_status' AND value = 'active') THEN
		l_pack_act_id := insert_ad_action(i_ad_id, 'status_change', NULL);
		PERFORM insert_state(i_ad_id, l_pack_act_id, 'accepted', 'status_change', i_remote_addr, get_token_id(i_token));
	END IF;

	-- Unlock the action in ad_queues
	DELETE
	FROM
		ad_queues
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Check for redir
	SELECT
		value
	INTO
		o_redir_code
	FROM
		action_params
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		name = 'redir';

	-- Get remote addr on the reg state of current action
	SELECT
		remote_addr
	INTO
		o_remote_addr
	FROM
		action_states
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		state = 'reg';

	o_action_type := COALESCE(l_action_type, l_action_type_before_refuse);

	-- Insert first_approved_ad into user_params if it doesn't already exist
	SELECT
		value
	INTO
		l_first_approved_ad
	FROM
		user_params
	WHERE
		user_id = l_user_id AND
		name = 'first_approved_ad';

	IF l_first_approved_ad IS NULL THEN
		INSERT INTO
			user_params (user_id, name, value)
		VALUES
			(l_user_id, 'first_approved_ad', NOW());
	END IF;

    --Get the admin id to put in the review log
	--Only if it is a human action (not autoaccept)
	IF i_token IS NOT NULL THEN
		SELECT
		   COALESCE(admin_id, 0)
		INTO
			l_admin_id
		FROM
			tokens
		WHERE
			token = i_token;

		--Insert a row in the review log
		INSERT INTO
			review_log(
				ad_id,
				action_id,
				admin_id,
				queue,
				action_type,
				action,
				category)
		VALUES 
				(i_ad_id,
				i_action_id,
				COALESCE(l_admin_id, 0),
				i_filter_name,
				o_action_type,
				'accepted',
				i_ads_category);
	END IF;

END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION review_accept_with_changes(i_ad_id ad_actions.ad_id%TYPE,
					 i_action_id ad_actions.action_id%TYPE,
					 i_token tokens.token%TYPE,
					 i_remote_addr action_states.remote_addr%TYPE,
					 i_filter_name filters.name%TYPE,
					 i_ads_type ads.type%TYPE,
					 i_ads_category ads.category%TYPE,
					 i_company_ad ads.company_ad%TYPE,
					 i_ads_price ads.price%TYPE,
					 i_ads_subject ads.subject%TYPE,
					 i_ads_body ads.body%TYPE,
					 i_remove_infopage bool,
					 i_remove_images bool[],
					 i_ad_params varchar[][],
					 i_valid_params varchar[],
					 i_gallery_expire_minutes integer,
					 i_set_first smallint,
					 i_lang varchar,
					 i_reason state_params.value%TYPE,
					 i_acceptance_text state_params.value%TYPE,
					 OUT o_list_id ads.list_id%TYPE,
					 OUT o_first_listing bool,
					 OUT o_redir_code redir_stats.code%TYPE,
					 OUT o_remote_addr action_states.remote_addr%TYPE,
					 OUT o_action_type ad_actions.action_type%TYPE,
					 OUT o_company_ad ads.company_ad%TYPE) AS $$
DECLARE
	l_state_id action_states.state_id%TYPE;
	l_list_time ads.list_time%TYPE;
	l_orig_list_time ads.list_time%TYPE;
	l_action_type ad_actions.action_type%TYPE;
	l_action_type_before_refuse ad_actions.action_type%TYPE;
	l_ads_category ads.category%TYPE;
	l_ad_status ads.status%TYPE;
	l_img_change_state action_states.state_id%TYPE;
	l_ad_images varchar[];
	l_i integer;
	l_rec record;
	l_ads_price ads.price%TYPE;
	l_price ads.price%TYPE;
	l_currency varchar;
	l_old_price ads.price%TYPE;
	l_prev_currency varchar;
	l_status integer;
	l_user_id ads.user_id%TYPE;
	l_first_approved_ad user_params.value%TYPE;
	l_external_ad_id ads.list_id%TYPE;
    l_admin_id integer;
	l_pack_status varchar;
	l_pack_act_id ad_actions.action_id%TYPE;
BEGIN
	-- Get price  
	SELECT
		price,
		old_price,
		user_id
	INTO
		l_price,
		l_old_price,
		l_user_id
	FROM
		ads
	WHERE
		ad_id = i_ad_id;

	-- Get currency
	SELECT
		value
	INTO
		l_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'currency';

	SELECT
		value
	INTO
		l_prev_currency
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'prev_currency';

	-- Insert the accepted with changes state
	l_state_id := insert_state(i_ad_id,
				   i_action_id, 
				   'accepted',
				   'accept_w_chngs', 
				   i_remote_addr, 
				   get_token_id(i_token));

	INSERT INTO
		state_params 
	VALUES (
		i_ad_id,
		i_action_id,
		l_state_id,
		'filter_name',
		i_filter_name);


	-- Build up a new array of images from the old one and i_remove_images
	SELECT
		MAX(state_id)
	INTO
		l_img_change_state
	FROM
		ad_image_changes
	WHERE
		ad_id = i_ad_id
		AND action_id = i_action_id;


	IF i_set_first IS NOT NULL THEN
		l_i := 1;
	ELSE
		l_i := 0;
	END IF;

	
	IF l_img_change_state IS NOT NULL THEN
		FOR
			l_rec
		IN
			SELECT
				name,
				seq_no
			FROM
				ad_image_changes
			WHERE
				ad_id = i_ad_id
				AND action_id = i_action_id
				AND state_id = l_img_change_state
			ORDER BY
				seq_no
		LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images
				l_ad_images[1] := l_rec.name;
			ELSE 
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;
	ELSE
		FOR
			l_rec
		IN
			SELECT
				ad_media_id||'.jpg' AS name,
				seq_no
			FROM
				ad_media
			WHERE
				COALESCE(media_type, 'image') = 'image' AND
				ad_id = i_ad_id
			ORDER BY
				seq_no
LOOP
			IF i_set_first IS NOT NULL AND l_rec.seq_no = i_set_first THEN --CHUS 261: Change order of images 
				l_ad_images[1] := l_rec.name;
			ELSE 
				IF i_remove_images[l_rec.seq_no + 1] IS NULL OR NOT i_remove_images[l_rec.seq_no + 1] THEN
					l_ad_images[l_i + 1] := l_rec.name;
					l_i := l_i + 1;
				END IF;
			END IF;
		END LOOP;

	END IF;


	IF i_ads_price IS NULL AND i_token IS NULL THEN
		-- Autoaccept doesn't send the price. Find it.
		l_ads_price := get_changed_value(i_ad_id,
						 i_action_id,
						 false,
						 'price');
	ELSE
		l_ads_price := i_ads_price;
	END IF;
	PERFORM insert_all_ad_changes(i_ad_id,
				      i_action_id,
				      l_state_id,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_ads_type,
				      i_ads_category,
				      NULL,
				      NULL,
				      NULL,
				      NULL,
				      i_company_ad,
				      i_ads_subject,
				      i_ads_body,
				      l_ads_price,
				      CASE WHEN i_remove_infopage THEN NULL ELSE '' END,
				      NULL,
				      l_ad_images,
				      NULL,
				      NULL,
				      i_ad_params,
				      'review',
				      i_valid_params,  -- Mark all valid params as static, so they're not deleted
				      i_lang);

	-- Get current status information
	SELECT
		list_id,
		list_time,
		orig_list_time,
		action_type,
		status,
		company_ad
	INTO
		o_list_id,
		l_list_time,
		l_orig_list_time,
		l_action_type,
		l_ad_status,
		o_company_ad
	FROM
		ad_actions
		JOIN ads USING (ad_id)
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;
	
	-- Get link_type
	SELECT 
		value::integer
	INTO
		l_external_ad_id
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND
		name = 'external_ad_id' AND
		ad_id = (
			SELECT 
				ad_id
			FROM
				ad_params
			WHERE
				name = 'link_type' AND
				value = 'spidered_balcao' AND
				ad_id = i_ad_id
		);

	-- Check if the ad never been listed
	IF o_list_id IS NULL THEN
		IF l_external_ad_id IS NULL THEN 
			o_list_id := NEXTVAL('list_id_seq');
		ELSE
			o_list_id := l_external_ad_id;
		END IF;
		o_first_listing := true;
	ELSE
		o_first_listing := false;
	END IF;

	PERFORM accept_action (i_ad_id, i_action_id, NULL, i_remote_addr, i_token, i_gallery_expire_minutes,
			l_action_type, l_state_id);
	
	-- Store the original list_time
	IF l_orig_list_time IS NULL AND l_list_time IS NOT NULL THEN
		l_orig_list_time := l_list_time;
	END IF;

	-- Update list_time if needed
	IF l_list_time IS NULL OR l_action_type IN ('renew', 'prolong') OR (l_action_type IN ('editrefused') AND l_ad_status IN ('refused')) THEN
		l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
	ELSE
		SELECT
			atbr.o_action_type
		INTO
			l_action_type_before_refuse
		FROM
			action_type_before_refuse(i_ad_id, i_action_id, NULL) AS atbr;

		IF l_action_type_before_refuse IN ('renew', 'prolong') THEN
			l_list_time := date_trunc('second', CURRENT_TIMESTAMP);
		END IF;
	END IF;

	-- Reset the orig_list_time if ad is prolonged or if list_time never changed
	IF l_orig_list_time = l_list_time OR l_action_type IN ('prolong') OR (l_action_type IN ('editrefused') AND l_action_type_before_refuse IN ('prolong')) THEN
		l_orig_list_time := NULL;
	END IF;

	-- Set old_price to the original price
	-- accept_action sets old_price only if it changes with this action, we want to set it always.
	-- XXX set old_price only when price changes is maybe better?
	IF l_old_price IS NULL THEN
		l_old_price := l_price;

		IF l_currency IS NULL THEN
				l_prev_currency := 'peso';
		ELSE
				l_prev_currency := l_currency;
		END IF;

		IF l_ads_price = l_old_price THEN
			l_old_price := NULL;
			l_prev_currency := NULL;
		END IF;
	ELSIF l_old_price IS NOT NULL AND (l_action_type IN ('prolong') OR l_action_type_before_refuse IN ('prolong')) THEN
		l_old_price := NULL;
		l_prev_currency := NULL;
	END IF;


	-- Chech if the ad have the ad_param pack_status
	SELECT
		value
	INTO
		l_pack_status
	FROM
		ad_params
	WHERE
		ad_id = i_ad_id AND name = 'pack_status';

	IF NOT FOUND THEN
		l_pack_status := 'active';
	END IF;
	-- Update ad status
	UPDATE
		ads
	SET
		status = l_pack_status::enum_ads_status,
		list_id = o_list_id,
		list_time = l_list_time,
		orig_list_time = l_orig_list_time,
		old_price = l_old_price,
		modified_at = CURRENT_TIMESTAMP
	WHERE
		ad_id = i_ad_id;


       -- update subject
       IF i_ads_subject IS NOT NULL THEN
               UPDATE 
			ads
               SET 
			subject = i_ads_subject
               WHERE 
			ad_id = i_ad_id;
       END IF;

       -- update body
       IF i_ads_body IS NOT NULL THEN
               UPDATE 
			ads
               SET 
			body = i_ads_body
               WHERE 
			ad_id = i_ad_id;
       END IF;

	-- Inserts/updates prev_currency in ad_params
	IF l_prev_currency IS NOT NULL THEN
		IF NOT EXISTS (SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'prev_currency') THEN
			INSERT INTO
				ad_params
			VALUES
				(i_ad_id, 'prev_currency', l_prev_currency);
		ELSE
			UPDATE
				ad_params
			SET
				value = l_prev_currency
			WHERE
				ad_id = i_ad_id AND name = 'prev_currency';
		END IF;
	END IF;

	--Add new action to activate trigger
	IF l_action_type in ('new') AND EXISTS ( SELECT * FROM ad_params WHERE ad_id = i_ad_id AND name = 'pack_status' AND value = 'active') THEN
		l_pack_act_id := insert_ad_action(i_ad_id, 'status_change', NULL);
		PERFORM insert_state(i_ad_id, l_pack_act_id, 'accepted', 'status_change', i_remote_addr, get_token_id(i_token));
	END IF;

	-- Unlock the action in ad_queues
	DELETE
	FROM
		ad_queues
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id;

	-- Check for redir
	SELECT
		value
	INTO
		o_redir_code
	FROM
		action_params
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		name = 'redir';

	-- Get remote addr on the reg state of current action
	SELECT
		remote_addr
	INTO
		o_remote_addr
	FROM
		action_states
	WHERE
		ad_id = i_ad_id AND
		action_id = i_action_id AND
		state = 'reg';

	o_action_type := COALESCE(l_action_type, l_action_type_before_refuse);

	-- Insert first_approved_ad into user_params if it doesn't already exist
	SELECT
		value
	INTO
		l_first_approved_ad
	FROM
		user_params
	WHERE
		user_id = l_user_id AND
		name = 'first_approved_ad';

	IF l_first_approved_ad IS NULL THEN
		INSERT INTO
			user_params (user_id, name, value)
		VALUES
			(l_user_id, 'first_approved_ad', NOW());
	END IF;
	
    --Get the admin id to put in the review log
	--Only if it is a human action (not autoaccept)
	IF i_token IS NOT NULL THEN
		SELECT 
		   COALESCE(admin_id, 0)
		INTO 
			l_admin_id 
		FROM 
			tokens 
		WHERE 
			token = i_token;
			
		--Insert a row in the review log
		INSERT INTO review_log(
			ad_id,
			action_id, 
			admin_id, 
			queue, 
			action_type, 
			action,
			category,
			refusal_reason_text)
		VALUES 
			(i_ad_id, 
			i_action_id, 
			COALESCE(l_admin_id, 0),
			i_filter_name, 
			COALESCE(o_action_type, (SELECT action_type FROM ad_actions WHERE ad_id = i_ad_id AND action_id = i_action_id)),
			'accept_ch',
			COALESCE(i_ads_category,(SELECT category FROM ads WHERE ad_id = i_ad_id)),
			COALESCE(i_acceptance_text,(SELECT value FROM conf WHERE key = '*.*.common.accepted.' || i_reason || '.name'), i_reason)
			);
	END IF;
END
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION scarface_edit(i_ad_id ads.ad_id%TYPE,
		i_category ads.category%TYPE,
		i_company_ad ads.company_ad%TYPE,
		i_type ads.type%TYPE,
		i_price ads.price%TYPE,
		i_remote_addr action_states.remote_addr%TYPE,
		i_token tokens.token%TYPE) RETURNS VOID AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_action_id ad_actions.action_id%TYPE;
BEGIN
	SELECT
		ads.ad_id
	INTO
		l_ad_id
	FROM
		ads
	WHERE
		ads.ad_id = i_ad_id
		AND status IN ('active', 'hidden');
	
	IF NOT FOUND THEN
		RAISE EXCEPTION 'ERROR_AD_NOT_ACTIVE';
	END IF;

	-- Create new action
	l_action_id := insert_ad_action(i_ad_id, 'scarface_edit', NULL);
	l_state_id := insert_state(i_ad_id,
			l_action_id, 
			'reg', 
			'initial', 
			i_remote_addr, 
			NULL);

	-- EDIT
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'category', i_category::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'company_ad', i_company_ad::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'type', i_type::text);
	PERFORM insert_ad_change(i_ad_id, l_action_id, l_state_id, false, 'price', i_price::text);

	-- apply ad_changes
	PERFORM apply_ad_changes(i_ad_id, l_action_id, NULL);

	PERFORM insert_state(i_ad_id,
			l_action_id, 
			'accepted',
			'scarface_edit', 
			i_remote_addr, 
			get_token_id(i_token));
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION import_ad(i_email users.email%TYPE,
				     i_remote_addr action_states.remote_addr%TYPE,
				     i_ads_name ads.name%TYPE,
				     i_ads_phone ads.phone%TYPE,
				     i_ads_region ads.region%TYPE,
				     i_ads_city ads.city%TYPE,
				     i_ads_type ads.type%TYPE,
				     i_ads_category ads.category%TYPE,
				     i_ads_passwd ads.passwd%TYPE,
				     i_store_id ads.store_id%TYPE,
				     i_phone_hidden ads.phone_hidden%TYPE,
				     i_company_ad ads.company_ad%TYPE,
				     i_ads_subject ads.subject%TYPE,
				     i_ads_body ads.body%TYPE,
				     i_ads_price ads.price%TYPE,
				     i_ads_infopage ads.infopage%TYPE,
				     i_ads_infopage_title ads.infopage_title%TYPE, 
				     i_ad_images varchar[],
				     i_ad_params varchar[][],
				     i_link_type VARCHAR(50),
				     i_ext_ad_id VARCHAR(50),
				     i_digest VARCHAR(50),
				     i_list_time ads.list_time%TYPE,
				     i_old_price ads.old_price%TYPE,
				     i_link_type_group_check_whitelist varchar[],
				     i_pending_review_queue ad_queues.queue%TYPE,
				     i_static_params text[],
				     i_lang varchar,
				     i_category_edition_limits integer[][],
				     OUT o_ad_id ads.ad_id%TYPE,
				     OUT o_action_id ad_actions.action_id%TYPE,
				     OUT o_list_id ads.list_id%TYPE,
				     OUT o_is_new_ad bool,
				     OUT o_list_time ads.list_time%TYPE) AS $$
DECLARE
	l_ad_id ads.ad_id%TYPE;
	l_old_list_time ads.list_time%TYPE;
	l_status ads.status%TYPE;
	l_new_status ads.status%TYPE;
	l_deleted_at action_states.timestamp%TYPE;
	l_old_price ads.old_price%TYPE;
	l_orig_list_time ads.orig_list_time%TYPE;
BEGIN
	SELECT
		ext_id.ad_id,
		list_time,
		status,
		orig_list_time
	INTO
		l_ad_id,
		l_old_list_time,
		l_status,
		l_orig_list_time
	FROM
		ads
		JOIN ad_params AS ext_id ON
			ads.ad_id = ext_id.ad_id AND
			ext_id.name = 'external_ad_id'
		JOIN ad_params AS link_type ON
			ext_id.ad_id = link_type.ad_id AND
			link_type.name = 'link_type'
	WHERE
		ext_id.value = i_ext_ad_id AND
		link_type.value = i_link_type
	ORDER BY CASE status WHEN 'active' THEN 1 ELSE 2 END, ad_id -- In case of duplicates (ads with same external_ad_id)
	LIMIT 1;

	IF FOUND THEN
		o_is_new_ad := FALSE;
	ELSE
		o_is_new_ad := TRUE;
	END IF;
	
	IF i_list_time IS NULL THEN
		o_list_time := COALESCE(l_old_list_time, CURRENT_TIMESTAMP);
	ELSE
		o_list_time := i_list_time;
	END IF;

	SELECT
		insert_ad.o_ad_id,
		insert_ad.o_action_id
	INTO
		o_ad_id,
		o_action_id
	FROM
		insert_ad(i_email,
			  NULL, -- pay_type
			  NULL, -- cat_price
			  NULL, -- extra_images_price
			  NULL, -- gallery_price
			  NULL, -- redir_code
			  NULL, -- previous_action_id
			  i_remote_addr,
			  i_ads_name,
			  i_ads_phone,
			  i_ads_region,
			  i_ads_city,
			  i_ads_type,
			  i_ads_category,
			  i_ads_passwd,
			  NULL,
			  i_store_id,
			  i_phone_hidden,
			  i_company_ad,
			  i_ads_subject,
			  i_ads_body,
			  i_ads_price,
			  i_ads_infopage,
			  i_ads_infopage_title,
			  i_ad_images,
			  NULL,
			  NULL,
			  i_ad_params || ARRAY[ARRAY['link_type',i_link_type],ARRAY['digest',i_digest]],
			  NULL,
			  'import',
			  NULL,
			  NULL,
			  l_ad_id,
			  i_static_params,
			  i_lang,
			  i_category_edition_limits);
	
	IF i_pending_review_queue IS NOT NULL THEN
		PERFORM insert_state(o_ad_id, 
				     o_action_id, 
				     'pending_review', 
				     'import', 
				     i_remote_addr, 
				     NULL);
		-- Set the queue
		UPDATE
			ad_actions
		SET
			queue = i_pending_review_queue
		WHERE
			ad_id = o_ad_id AND
			action_id = o_action_id;
		-- Register the action in the ad_queues table
		INSERT
		INTO
			ad_queues 
			(ad_id,
			 action_id,
			 queue)
		VALUES
			(o_ad_id,
			 o_action_id, 
			 i_pending_review_queue);
	ELSE
		PERFORM accept_action (
				o_ad_id,
				o_action_id,
				'import',
				i_remote_addr,
				NULL,
				NULL,
				'import',
				NULL);

		SELECT
			list_id
		INTO
			o_list_id
		FROM
			ads
		WHERE
			ad_id = o_ad_id;
	
		IF o_list_id IS NULL THEN
			o_list_id := NEXTVAL('list_id_seq');
		END IF;

		IF NOT o_is_new_ad AND i_store_id IS NOT NULL THEN
			o_list_time := NULL;
			INSERT INTO trans_queue (
				added_at,
				added_by,
				command,
				queue,
				sub_queue,
				info
			) VALUES (
				CURRENT_TIMESTAMP,
				'import_ad',
				E'cmd:bump_ad\nbatch:1\nbatch_bump:1\ncommit:1\nad_id:' || o_ad_id || E'\n',
				'bump',
				'import',
				i_store_id::varchar
			);
		ELSIF l_old_list_time IS NULL OR o_list_time != l_old_list_time OR o_list_time <= CURRENT_TIMESTAMP - INTERVAL '50 day' THEN
			o_list_time := adjust_imported_list_time(i_link_type, i_store_id, o_list_time, i_link_type_group_check_whitelist);
		END IF;

		IF l_status = 'hidden' THEN
			l_new_status := 'hidden';
		ELSIF o_list_time IS NULL THEN
			l_new_status := 'inactive';
		ELSE
			l_new_status := 'active';
		END IF;

		IF l_orig_list_time IS NULL THEN
			l_orig_list_time := o_list_time;
		END IF;

		UPDATE
			ads
		SET
			list_id = o_list_id,
			list_time = o_list_time,
			status = l_new_status,
			modified_at = CURRENT_TIMESTAMP,
			orig_list_time = l_orig_list_time
		WHERE
			ad_id = o_ad_id;

		IF i_old_price IS NOT NULL THEN
			UPDATE
				ads
			SET
				old_price = i_old_price
			WHERE
				ad_id = o_ad_id;
		ELSE
			IF l_old_price IS NOT NULL THEN
				UPDATE 
					ads 
				SET
					old_price = NULL
				WHERE 
					ad_id = o_ad_id;
			END IF;
		END IF;
	END IF;
END;
$$ LANGUAGE plpgsql;
CREATE OR REPLACE FUNCTION insert_ad(
	i_email users.email%TYPE,
	i_pay_type varchar,
	i_cat_price payments.pay_amount%TYPE,
	i_extra_images_price payments.pay_amount%TYPE,
	i_gallery_price payments.pay_amount%TYPE,
	i_redir_code redir_stats.code%TYPE,
	i_previous_action_id ad_actions.action_id%TYPE,
	i_remote_addr action_states.remote_addr%TYPE,
	i_ads_name ads.name%TYPE,
	i_ads_phone ads.phone%TYPE,
	i_ads_region ads.region%TYPE,
	i_ads_city ads.city%TYPE,
	i_ads_type ads.type%TYPE,
	i_ads_category ads.category%TYPE,
	i_ads_passwd ads.salted_passwd%TYPE,
	i_uid users.uid%TYPE,
	i_store_id ads.store_id%TYPE,
	i_phone_hidden ads.phone_hidden%TYPE,
	i_company_ad ads.company_ad%TYPE,
	i_ads_subject ads.subject%TYPE,
	i_ads_body ads.body%TYPE,
	i_ads_price ads.price%TYPE,
	i_ads_infopage ads.infopage%TYPE,
	i_ads_infopage_title ads.infopage_title%TYPE,
	i_ad_images varchar[],
	i_ad_thumb_digest varchar[],
	i_ad_image_had_digest bool[],
	i_ad_params varchar[][],
	i_token tokens.token%TYPE,
	i_action_type ad_actions.action_type%TYPE,
	i_action_params varchar[][],
	i_list_id ads.list_id%TYPE,
	i_ad_id ads.ad_id%TYPE,
	i_static_params text[],
	i_lang varchar,
	i_category_edition_limits integer[][],
	OUT o_ad_id ads.ad_id%TYPE,
	OUT o_action_id ad_actions.action_id%TYPE,
	OUT o_paycode payment_groups.code%TYPE,
	OUT o_uid users.uid%TYPE,
	OUT o_account users.account%TYPE,
	OUT o_prepaid integer,
	OUT o_user_id users.user_id%TYPE,
	OUT o_is_new_user integer,
	OUT o_payment_group_id payment_groups.payment_group_id%TYPE,
	OUT o_action_type ad_actions.action_type%TYPE) AS $$
DECLARE
	l_uid users.uid%TYPE;
	l_i integer;
	l_ad_image_name varchar;
	l_ad_param_name ad_params.name%TYPE;
	l_ad_param_value ad_params.value%TYPE;
	l_action_param_name action_params.name%TYPE;
	l_action_param_value action_params.value%TYPE;
	l_action_id ad_actions.action_id%TYPE;
	l_state action_states.state%TYPE;
	l_transition action_states.transition%TYPE;
	l_code_type ad_codes.code_type%TYPE;
	l_paycode ad_codes.code%TYPE;
	l_prepaid integer;
	l_account users.account%TYPE;
	l_ad_id ads.ad_id%TYPE;
	l_state_id action_states.state_id%TYPE;
	l_ref_types enum_pay_log_references_ref_type[];
	l_references varchar[];
	l_voucher vouchers.balance%TYPE;
	l_transfer_ad_id ads.ad_id%TYPE;
	l_cat_price_discount payments.discount%TYPE;
	l_extra_images_price_discount payments.discount%TYPE;
	l_gallery_price_discount payments.discount%TYPE;
	l_ad_image_id bigint;
	l_temp_ad_id ads.ad_id%TYPE;
	l_temp_ad_status ads.status%TYPE;
	l_skip_media_update boolean;
	l_user_id users.user_id%TYPE;
	l_value user_params.value%TYPE;
BEGIN

	IF i_action_type = 'edit' AND i_token IS NOT NULL AND get_auth_type(i_token) = 'admin' THEN
		o_action_type = 'adminedit';
	ELSE
		o_action_type = i_action_type;
	END IF;

	IF i_store_id IS NOT NULL AND i_pay_type != 'verify' AND i_action_type != 'adminedit' THEN
		IF i_token IS NULL OR get_auth_type(i_token) != 'store' THEN
			RAISE EXCEPTION 'ERROR_STORE_NOT_LOGGED_IN';
		END IF;
	END IF;

	-- check user id
	SELECT
		*
	INTO 
		l_account, o_user_id, l_uid 
	FROM 
		get_user_by_email(i_email);

	l_prepaid := 0;
	o_is_new_user := 0;
	-- existing user (email)
	IF i_pay_type = 'voucher' AND o_action_type NOT IN ('adminedit', 'import') THEN
		IF i_token IS NULL OR get_auth_type(i_token) != 'store' THEN
			RAISE NOTICE 'NOT_GOOD_TOKEN';
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;

		SELECT
			balance
		INTO
			l_voucher
		FROM
			vouchers
		WHERE
			store_id = i_store_id;

		IF NOT FOUND OR l_voucher < i_cat_price + i_extra_images_price + i_gallery_price THEN
			RAISE NOTICE 'NOT_ENOUGH_VOUCHER';
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;
	END IF;

	IF i_pay_type = 'campaign' THEN
		IF o_action_type NOT IN ('new', 'editrefused') THEN
			RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
		END IF;
	END IF;

	-- Getting the real uid for partners inserting by API
	IF i_action_type = 'import' AND i_ad_params IS NOT NULL THEN
		l_i := 1;
		LOOP
			IF i_ad_params[l_i][1] = 'link_type' THEN
				l_uid := get_uid_by_link_type(i_ad_params[l_i][2]);
				EXIT;
			END IF;
			l_i := l_i + 1;
		END LOOP;
	END IF;

	IF o_user_id IS NOT NULL THEN
		IF o_action_type NOT IN ('adminedit', 'import') THEN
			IF i_pay_type = 'verify' THEN
				IF l_account < i_cat_price + i_extra_images_price + i_gallery_price THEN
					RAISE NOTICE 'NOT_ENOUGH';
					RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';
				END IF;
				l_account := l_account - (i_cat_price + i_extra_images_price + i_gallery_price);
				l_prepaid := 1;
				
				UPDATE
					users
				SET
					account = l_account
				WHERE
					user_id = o_user_id;
			END iF;
		END IF;

		-- here I should set the pre_first_inserted_ad or first_inserted_ad flag if the user does not have the flag with other user_id with the same uid
		IF i_action_type = 'new' THEN
			SELECT
				uid
			INTO
				l_uid
			FROM
				users
			WHERE
				user_id = o_user_id;

			SELECT
				value
			INTO
				l_value
			FROM
				users u
			INNER JOIN
				user_params up USING (user_id)
			WHERE
				u.uid = l_uid AND up.name = 'first_inserted_ad';

			IF NOT FOUND THEN
				IF EXISTS( SELECT
						*
					FROM
						user_params
					WHERE
						user_id = o_user_id AND name = 'pre_first_inserted_ad')
				THEN
					UPDATE
						user_params
					SET
						value = NOW()
					WHERE
						user_id = o_user_id AND name = 'pre_first_inserted_ad';
				ELSE
					INSERT INTO
						user_params (user_id, name, value)
					VALUES
						(o_user_id, 'pre_first_inserted_ad', NOW());
				END IF;
			END IF;
		END IF;

	ELSIF i_pay_type IN ('verify') THEN
		RAISE NOTICE 'NOT_EXISTING_USER';
		RAISE EXCEPTION 'ERROR_PAY_TYPE_INVALID';

	-- not existing user, but has uid (from cookie)
	ELSIF i_uid IS NOT NULL THEN
		SELECT
			uid
		INTO
			l_uid
		FROM
			users
		WHERE
			uid = i_uid;

		-- Don't use fake UIDs
		IF NOT FOUND THEN
			l_uid := NEXTVAL('uid_seq');
		END IF;

		INSERT INTO
			users
			(uid,
			 email)
		VALUES
			(l_uid,
			 i_email);

		l_user_id := CURRVAL('users_user_id_seq');

		-- here I should set the pre_first_inserted_ad or first_inserted_ad flag if the user does not have the flag with other user_id with the same uid
		IF i_action_type = 'new' THEN
			SELECT
				value
			INTO
				l_value
			FROM
				users u
			INNER JOIN
				user_params up USING (user_id)
			WHERE
				u.uid = l_uid AND up.name = 'first_inserted_ad';

			IF NOT FOUND THEN
				INSERT INTO
					user_params (user_id, name, value)
				VALUES
					(l_user_id, 'pre_first_inserted_ad', NOW());
			ELSE
				INSERT INTO
					user_params (user_id, name, value)
				VALUES
					(l_user_id, 'first_inserted_ad', l_value);
			END IF;
		END IF;

	-- new user
	ELSE 
		IF l_uid IS NULL THEN
			l_uid := NEXTVAL('uid_seq');
		END IF;

		INSERT INTO
			users
			(uid,
			 email)
		VALUES	 
			(l_uid,
			i_email);

		l_user_id := CURRVAL('users_user_id_seq');

		-- set first_ad flag
		IF i_action_type = 'new' THEN
			INSERT INTO
				user_params (user_id, name, value)
			VALUES
				(l_user_id, 'pre_first_inserted_ad', NOW());
		END IF;


	END IF;

	IF o_user_id IS NULL THEN
		o_user_id := CURRVAL('users_user_id_seq');
		o_is_new_user := 1;
	END IF;

	-- check for existing ad - either by list_id or ad_id
	
	IF i_list_id IS NOT NULL OR i_ad_id IS NOT NULL THEN
		
		SELECT
			ad_id
		INTO
			l_ad_id
		FROM
			ads
		WHERE
			CASE WHEN i_ad_id IS NOT NULL THEN
				ad_id = i_ad_id
			ELSE
				list_id = i_list_id
			END;

		IF NOT FOUND THEN
			RAISE EXCEPTION 'ERROR_ORIG_AD_NOT_FOUND';
		END IF;

		IF NOT can_edit_ad(l_ad_id, i_category_edition_limits) THEN
			RAISE EXCEPTION 'ERROR_TOO_OLD_TO_EDIT';
		END IF;
			
		SELECT
			iaas.o_action_id,
			iaas.o_state_id
		INTO
			l_action_id,
			l_state_id
		FROM
			insert_ad_action_state(
				     CAST(l_ad_id AS integer),
				     o_action_type,
				     'reg', 
				     'initial', 
				     i_remote_addr, 
				     get_token_id(i_token)
			) AS iaas;

		PERFORM insert_all_ad_changes(l_ad_id,
					      l_action_id,
					      l_state_id,
					      i_ads_name,
					      i_ads_phone,
					      i_ads_region,
					      i_ads_city,
					      i_ads_type,
					      i_ads_category,
					      i_ads_passwd,
					      o_user_id,
					      i_store_id,
					      i_phone_hidden,
					      i_company_ad,
					      i_ads_subject,
					      i_ads_body,
					      i_ads_price,
					      i_ads_infopage,
					      i_ads_infopage_title,
					      i_ad_images,
					      i_ad_thumb_digest,
					      i_ad_image_had_digest,
					      i_ad_params,
					      o_action_type::text,
					      i_static_params,
					      i_lang);
						
	ELSE -- new ad
		-- Insert the ad
	
		INSERT 
		INTO 
			ads
			(name,
			 phone,
			 region,
			 city,
			 type,
			 category,
			 salted_passwd,
			 user_id,
			 phone_hidden,
			 company_ad,
			 subject,
			 body,
			 price,
			 infopage,
			 infopage_title, 
			 store_id,
			 lang) 
		VALUES
			(i_ads_name,
			 i_ads_phone,
			 i_ads_region,
			 i_ads_city,
			 i_ads_type,
			 i_ads_category,
			 i_ads_passwd,
			 o_user_id,
			 i_phone_hidden,
			 i_company_ad,
			 i_ads_subject,
			 i_ads_body,
			 i_ads_price,
			 i_ads_infopage,
			 i_ads_infopage_title, 
			 i_store_id,
			 i_lang);
		
		l_ad_id := CURRVAL('ads_ad_id_seq');

		-- Add images
		IF i_ad_images IS NOT NULL THEN
			l_i := 0;
	
			LOOP
				l_ad_image_name := i_ad_images[l_i + 1];
				l_skip_media_update := FALSE;
				EXIT WHEN l_ad_image_name IS NULL;

				l_ad_image_id := TRIM(trailing '.jpg' FROM l_ad_image_name)::bigint;

				SELECT
					ad_media.ad_id,
					ads.status
				FROM
					ad_media
				LEFT JOIN
					ads USING (ad_id)
				WHERE
					ad_media_id = l_ad_image_id
				INTO
					l_temp_ad_id,
					l_temp_ad_status;

				-- we only let the user update empty entries in ad_media or the current ad entries
				IF (l_temp_ad_id IS NOT NULL AND l_temp_ad_id != l_ad_id) THEN
					RAISE NOTICE 'ERROR_INVALID_UPDATE_TO_ADMEDIA';
					IF l_temp_ad_status = 'active' THEN
						l_skip_media_update := TRUE;
					END IF;
				END IF;

				IF l_skip_media_update = FALSE THEN
					UPDATE
						ad_media
					SET
						ad_id = l_ad_id,
						seq_no = l_i
					WHERE
						ad_media_id = l_ad_image_id;
				END IF;

	
				IF COALESCE(length(i_ad_thumb_digest[l_i + 1]), 0) = 32 THEN

					INSERT 
					INTO
						ad_images_digests
						(name,
						digest,
						ad_id,
						previously_inserted,
						uid)
					VALUES
						(i_ad_images[l_i + 1],
						i_ad_thumb_digest[l_i + 1],
						l_ad_id,
						i_ad_image_had_digest[l_i + 1],
						l_uid);
				END IF;
	
				l_i := l_i + 1;
	
			END LOOP;
		END IF;

		-- Add ad_params
		IF i_ad_params IS NOT NULL THEN
			l_i := 1;
		
			LOOP
				l_ad_param_name := i_ad_params[l_i][1];
				l_ad_param_value := i_ad_params[l_i][2];
				EXIT WHEN l_ad_param_name IS NULL;

				INSERT INTO
					ad_params
					(ad_id,
					 name,
					 value)
				VALUES
					(l_ad_id,
					l_ad_param_name,
					replace(l_ad_param_value, E'\'\'', E'\''));
		
				l_i := l_i + 1;
			END LOOP;
		END IF;

		-- Create ad_action
		l_action_id := 1;
	
		INSERT INTO
			ad_actions
			(ad_id,
			 action_id,
			 action_type)
		VALUES
			(l_ad_id,
			 l_action_id,
			 o_action_type);
		PERFORM insert_state(CAST(l_ad_id AS integer),
				     l_action_id, 
				     'reg', 
				     'initial', 
				     i_remote_addr, 
				     get_token_id(i_token));
	END IF; -- new ad

	-- Add action_params
	IF i_action_params IS NOT NULL THEN
		l_i := 1;
	
		LOOP
			l_action_param_name := i_action_params[l_i][1];
			l_action_param_value := i_action_params[l_i][2];
			EXIT WHEN l_action_param_name IS NULL;

			INSERT INTO
				action_params
				(ad_id,
				 action_id,
				 name,
				 value)
			VALUES
				(l_ad_id,
				 l_action_id,
 				 l_action_param_name,
	 			 l_action_param_value);
		
			l_i := l_i + 1;
		END LOOP;
	END IF;

	-- Redir action param
	IF i_redir_code IS NOT NULL THEN
		-- insert into action_params
		INSERT INTO
			action_params
			(ad_id,
			 action_id,
			 name,
			 value)
		VALUES
			(l_ad_id,
			 l_action_id,
			 'redir',
			 i_redir_code);
	END IF;


	IF o_action_type NOT IN ('adminedit', 'import') THEN
		-- Create 'unpaid' or 'unverified' ad_action
		IF i_pay_type = 'phone' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := 'pay';
		ELSIF i_pay_type = 'card' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := 'pay';
		ELSIF i_pay_type = 'voucher' THEN
			l_state := 'unpaid';
			l_transition := 'newad';
			l_code_type := NULL;
		ELSIF i_pay_type = 'campaign' THEN
			IF o_action_type = 'editrefused' THEN
				l_state = 'unverified';
			ELSE
				l_state := 'unpaid';
			END IF;
			l_transition := 'newad';
			l_code_type := NULL;
			l_cat_price_discount := i_cat_price;
			l_extra_images_price_discount := i_extra_images_price;
			l_gallery_price_discount := i_gallery_price;
		ELSE
			l_state := 'unverified';
			l_transition := 'verifymail';
			l_code_type := 'verify';
		END IF;
	
		PERFORM insert_state(CAST(l_ad_id AS integer), 
				     l_action_id, 
				     l_state, 
				     l_transition, 
				     i_remote_addr, 
				     get_token_id(i_token));
	
		IF l_code_type IS NULL THEN
			l_paycode := NULL;
		ELSE
			l_paycode := get_ad_code(l_code_type);
		END IF;

		-- Create new payment
		INSERT INTO payment_groups (
			code,
			status
		) VALUES (
			l_paycode,
			(l_state::text)::enum_payment_groups_status
		);
		-- XXX This is where we want to insert a value discount for certain ads (US1575)
		INSERT INTO payments (
			payment_group_id,
			payment_type,
			pay_amount,
			discount
		) VALUES (
			CURRVAL('payment_groups_payment_group_id_seq'),
			'ad_action',
			i_cat_price,
			COALESCE(l_cat_price_discount, 0)
		);
		o_payment_group_id := CURRVAL('payment_groups_payment_group_id_seq');
		UPDATE
			ad_actions
		SET
			payment_group_id = o_payment_group_id
		WHERE
			ad_id = l_ad_id AND
			action_id = l_action_id;
			
		IF i_extra_images_price > 0 THEN
			INSERT INTO
				payments
				(payment_group_id,
			 	payment_type,
			 	pay_amount,
				discount)
			VALUES
				(o_payment_group_id,
			 	'extra_images',
			 	i_extra_images_price,
				COALESCE(l_extra_images_price_discount, 0)
				);
		END IF;

		IF i_gallery_price > 0 THEN
			INSERT INTO
				payments
				(payment_group_id,
			 	payment_type,
			 	pay_amount,
				discount)
			VALUES
				(o_payment_group_id,
			 	'gallery',
			 	i_gallery_price,
				COALESCE(l_gallery_price_discount, 0)
				);
		END IF;

		l_references := string_to_array(i_ads_phone, ',');
		IF l_references IS NOT NULL THEN 
			FOR l_i IN 1 .. array_upper(l_references, 1) LOOP
				l_ref_types[l_i] := 'adphone';
			END LOOP;
		END IF;
		PERFORM insert_pay_log('save', l_paycode::varchar, 0, NULL, l_ref_types, l_references,
				       i_remote_addr, NULL, o_payment_group_id, 'SAVE');
	END IF;
	
	o_ad_id := l_ad_id;
	o_action_id := l_action_id;
	o_uid := l_uid;
	o_prepaid := l_prepaid;
	o_account := l_account;
	o_paycode := l_paycode;
END;
$$ LANGUAGE plpgsql;
COMMIT;
