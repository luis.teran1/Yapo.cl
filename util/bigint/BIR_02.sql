-- BLOQUE 2
BEGIN;
set search_path=bpv,public;
ALTER TABLE ads ALTER COLUMN price TYPE bigint;
ALTER TABLE ads ALTER COLUMN old_price TYPE bigint;
ALTER TABLE dashboard_ads ALTER COLUMN price TYPE bigint;
COMMIT;
