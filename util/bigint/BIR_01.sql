-- BLOQUE 1
BEGIN;
set search_path=bpv,public;

DROP FUNCTION IF EXISTS view_ad(text, integer, integer, integer);
DROP FUNCTION IF EXISTS view_all_ad_versions(text, integer);
DROP FUNCTION IF EXISTS accept_action(integer, integer, enum_action_states_transition, character varying, character varying, integer, enum_ad_actions_action_type, integer);
DROP FUNCTION IF EXISTS apply_ad_changes(integer, integer, integer);
DROP FUNCTION IF EXISTS autoaccept(integer, integer, integer[], integer[], integer, integer[]);
DROP FUNCTION IF EXISTS find_associated_ads(integer, integer);
DROP FUNCTION IF EXISTS get_account_ads(integer, integer, character varying, integer, integer);
DROP FUNCTION IF EXISTS get_account_ads2(integer, integer, character varying, integer, integer);
DROP FUNCTION IF EXISTS get_ads_by_uid(integer, character varying, enum_ads_status, integer);
DROP FUNCTION IF EXISTS import_ad(character varying, character varying, character varying, character varying, smallint, smallint, enum_ads_type, integer, character varying, integer, boolean, boolean, character varying, text, integer, text, character varying, character varying[], character varying[], character varying, character varying, character varying, timestamp without time zone, integer, character varying[], enum_ad_actions_queue, text[], character varying);
DROP FUNCTION IF EXISTS import_ad(character varying, character varying, character varying, character varying, smallint, smallint, enum_ads_type, integer, character varying, integer, boolean, boolean, character varying, text, integer, text, character varying, character varying[], character varying[], character varying, character varying, character varying, timestamp without time zone, integer, character varying[], enum_ad_actions_queue, text[], character varying, integer[]);
DROP FUNCTION IF EXISTS insert_ad(character varying, character varying, integer, integer, integer, character varying, integer, character varying, character varying, character varying, smallint, smallint, enum_ads_type, integer, character varying, integer, integer, boolean, boolean, character varying, text, integer, text, character varying, character varying[], character varying[], boolean[], character varying[], character varying, enum_ad_actions_action_type, character varying[], integer, integer, text[], character varying);
DROP FUNCTION IF EXISTS insert_ad(character varying, character varying, integer, integer, integer, character varying, integer, character varying, character varying, character varying, smallint, smallint, enum_ads_type, integer, character varying, integer, integer, boolean, boolean, character varying, text, integer, text, character varying, character varying[], character varying[], boolean[], character varying[], character varying, enum_ad_actions_action_type, character varying[], integer, integer, text[], character varying, integer[]);
DROP FUNCTION IF EXISTS insert_all_ad_changes(integer, integer, integer, character varying, character varying, smallint, smallint, enum_ads_type, integer, character varying, integer, integer, boolean, boolean, character varying, text, integer, text, character varying, character varying[], character varying[], boolean[], character varying[], character varying, text[], character varying);
DROP FUNCTION IF EXISTS review_accept(integer, integer, character varying, character varying, character varying, enum_ads_type, integer, boolean, integer, character varying, text, boolean, boolean[], character varying[], character varying[], integer, smallint, character varying, text, text);
DROP FUNCTION IF EXISTS review_accept_with_changes(integer, integer, character varying, character varying, character varying, enum_ads_type, integer, boolean, integer, character varying, text, boolean, boolean[], character varying[], character varying[], integer, smallint, character varying, text, text);
DROP FUNCTION IF EXISTS scarface_edit(integer, integer, boolean, enum_ads_type, integer, character varying, character varying);

DROP VIEW IF EXISTS v_ads_current;
DROP VIEW IF EXISTS v_adflow;
DROP VIEW IF EXISTS v_ads;
COMMIT;
