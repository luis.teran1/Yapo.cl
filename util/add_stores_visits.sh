#! /bin/bash

for i in `seq 0 23`
do
	# insert some stats on redisstat
	redis-cli -p $1 -n 1 ZINCRBY STORE_VIEW:2014-01-01 1 1:$i > /dev/null
	redis-cli -p $1 -n 1 ZINCRBY STORE_VIEW:2014-12-31 1 1:$i > /dev/null
done
