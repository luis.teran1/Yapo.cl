#!/usr/bin/python
#
# tmplcrawl.py:
#   crawls templates starting from each provided file and returns
#   a formatted output with the include tree found. Some includes may not
#   be followed, in particular when there are variables involved. Good luck
#   with those. Enjoy the rest.
#
# Miguel Diaz <miguel@schibsted.cl>
#

import re
import os.path
import sys

def log(tag, message, depth=0):
    print "{spaces}{tag} {message}".format(
        spaces=" " * depth,
        tag=tag,
        message=message
    )

def crawl_file(tmpl, depth=0):
    log('- [x]', tmpl, depth)
    with open(tmpl) as f:
        for i, line in enumerate(f):
            find_includes(line, depth)

def find_file(partial):
    tmpl_dirs = [ 'templates/', 'ext-modules/mobile-api/templates/', 'msite/templates/' ]
    for d in tmpl_dirs:
        path = '{base}{partial}.tmpl'.format(base=d, partial=partial)
        if os.path.isfile(path):
            return path

def find_includes(line, depth):
    # TODO: What happens if there are more than one include per line?
    m = re.search('include\((.*)\)', line)
    if m:
        data = m.group(1)
        if data.startswith('"') and data.endswith('"'):
            clean = data[1:-1]
            path = find_file(clean)
            if path:
                crawl_file(path, depth+2)
            else:
                log('- [?]', clean, depth)
        else:
            log('- [ ]', data, depth)

def usage():
    print '''
    Usage: {bin} tmpl [tmpls...]

        {bin} crawls templates starting from each provided file and returns
        a formatted output with the include tree found. Some includes may not
        be followed, in particular when there are variables involved. Good luck
        with those. Enjoy the rest.

        tmpl: One or many template files to start crawling from
    '''.format(bin=sys.argv[0])
    sys.exit(1)

if len(sys.argv) < 2:
    usage()

for path in sys.argv[1:]:
    if os.path.isfile(path):
        crawl_file(path)
    else:
        print 'noooooooo: ' + path
