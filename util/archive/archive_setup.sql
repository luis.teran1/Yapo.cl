DELETE FROM archive_cache;

INSERT INTO archive_cache
SELECT DISTINCT
        ad_id
FROM
    action_states
WHERE
    state_id IN (
        SELECT
            max(state_id)
        FROM
            action_states
        WHERE
            transition != 'reminder'
        GROUP BY ad_id)
    AND CASE
        WHEN transition = 'refuse' THEN
            (SELECT status FROM ads WHERE ad_id = action_states.ad_id) != 'active'
            AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * 20)
        WHEN transition IN ('paymail', 'verifymail', 'paymailsent') THEN
            (SELECT status FROM ads WHERE ad_id = action_states.ad_id) IN ('inactive', 'deleted', 'refused')
            AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * 3)
        WHEN state = 'deleted' OR transition = 'store_change' THEN
            (SELECT status FROM ads WHERE ad_id = action_states.ad_id) = 'deleted'
            AND timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * 60)
        WHEN state = 'postpone_archive' THEN
            timestamp <= CURRENT_TIMESTAMP - (interval '1 day' * 7)
        ELSE
            false
    END
LIMIT 500
;
