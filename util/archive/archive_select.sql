DELETE FROM
	archive_cache
WHERE
	ad_id IN (
		SELECT
			ad_id
		FROM
			archive_cache
		WHERE 
			ad_id NOT IN (
				SELECT
					 ad_id
				FROM
					archive_cache
				JOIN
					ad_actions USING (ad_id)
				JOIN 
					payment_groups USING (payment_group_id)
				WHERE
					parent_payment_group_id IS NOT NULL)
		LIMIT 1
	)
RETURNING ad_id
;
