#!/usr/bin/perl

use strict;

my $name = shift;

my $prefix = uc ($name);
$prefix =~ s/([^_])[^_]*(_|$)/\1/g;
$prefix .= '_';

print "struct ${name}_rec {\n\tconst char *name;\n\tenum $name {\n\t\t${prefix}NONE";

my @fields;
my $num = 0;
while (<>) {
	chomp;

	push @fields, $_;
	print ", $prefix" . uc($_);
	$num++;
}

print "\n\t} val;\n};\n\n";
print "%{\n#define MAX_${prefix}WORD $num\n%}\n";

print "\%struct-type\n";
print "\%define hash-function-name ${name}_hash\n";
print "\%define lookup-function-name ${name}_lookup\n";
print "\%readonly-tables\n";
print "\%enum\n";
print "\%compare-strncmp\n";
print "\%define string-pool-name ${name}_strings\n";
print "\%define word-array-name ${name}_words\n\n";

print "\%\%\n";

for (@fields) {
	print "$_, $prefix" . uc($_) . "\n";
}

print <<EOT
\%\%

static
enum $name lookup_$name (const char *str, int len) {
	if (len == -1)
		len = strlen (str);
	const struct ${name}_rec *val = ${name}_lookup (str, len);

	if (val)
		return val->val;
	return 0;
}
EOT
;
