#include "dav_put.h"
#include "dav_ftp.h"

static int	ftp_error = 0;

void
clean_squid(const char *host, const char *port, char *file) {
        struct addrinfo *squid;
        int sock;
        FILE *s;

        getaddrinfo(host, port, NULL, &squid);
        if (!squid) {
                return;
        }

        if ((sock = socket(PF_INET, SOCK_STREAM, 0)) == -1)
                return;

        if (connect(sock, squid->ai_addr, squid->ai_addrlen) < 0) {
                return;
        }

        s = fdopen(sock, "w");

        fprintf(s, "GET /%s HTTP/1.0\n", file);
        fprintf(s, "Pragma: no-cache\n");
        fprintf(s, "Cache-Control: no-cache\n\n");
        fclose(s);

        freeaddrinfo(squid);
}

/* Refresh squids */
void
squid_delete(struct file *dav_file) {
struct bconf_node       *squids = bconf_get(g_bconf, "squid");
int                     hosts = 0, cnt = 0;
const char              *host = NULL, *port = NULL;

        if (!squids) { return; }

        hosts = bconf_count(bconf_get(squids, "host"));
        for (cnt = 0 ; cnt < hosts ; cnt++) {
                host = bconf_get_string(bconf_byindex(squids, "host", cnt), "name");
                port = bconf_get_string(bconf_byindex(squids, "host", cnt), "port");
                clean_squid(host, port, dav_file->file);
        }

        return;
}

int
ftp_cb(struct ftpdb *ftp) {
        free(ftp->command);
        if (ftp->status / 100 != 2) {
                /* Error. */
                if (ftp->data)
                        free(ftp->data);
                ftp_error = 1;
                return 0;
        }
        if (ftp->data) {
                /* Run second command. */
                ftp->command = ftp->data;
                ftp->data = NULL;
                return 1;
        }
        if (ftp->databuf)
                evbuffer_free(ftp->databuf);
        return 0;
}

void
ftp_log_cb(struct ftpdb *ftp, int level, const char *fmt, ...) {
        char *str;
        va_list ap;

        /* XXX Implement log_vprintf */
        va_start(ap, fmt);
        xvasprintf(&str, fmt, ap);
        va_end(ap);

        log_printf(level, "%s", str);
        free(str);
}

void
ftp_cmd(char *ftpcmd, char *ftpcmd2, struct evbuffer *ftpbuf, struct file *dav_file) {
struct ftpdb    *ftp = NULL;

        if (!ftpcmd) { return; }

        ftp = zmalloc( sizeof(*ftp) );
        log_printf(LOG_DEBUG, "FTP command: %s", ftpcmd);
        if (ftpcmd2){ log_printf(LOG_DEBUG, "FTP command 2: %s", ftpcmd2); }
        ftp->bconf = bconf_get(g_bconf, "ftp");
        ftp->command = ftpcmd;
        ftp->databuf = ftpbuf;
        ftp->data = ftpcmd2;
        ftp->cb = ftp_cb;
        ftp->log_cb = ftp_log_cb;
        ftp_error = 0;
        ftp_do(ftp);
        event_dispatch();
        if (ftp_error) {
                /* XXX Replay-log */
                log_printf(LOG_INFO, "%s;%s;%s\n", dav_file->method, dav_file->file, dav_file->destination);
        }

        return;
}

void
ftp_delete(struct file *dav_file) {
char    *ftpcmd = NULL;

        if (!regexec(g_ftpregex, dav_file->file, 0, NULL, 0)) {
                xasprintf(&ftpcmd, "DELE %s", dav_file->file + 1);
        }

        return ftp_cmd(ftpcmd, NULL, NULL, dav_file);
}

void
ftp_put(char *file, struct file *dav_file) {
int             srcfd;
struct stat     st;
char            *ftpcmd = NULL, *ftpcmd2 = NULL;
struct evbuffer *ftpbuf = NULL;

        if (regexec(g_ftpregex, dav_file->file, 0, NULL, 0)) { return; }

        if ((srcfd = open(file, O_RDONLY)) >= 0) {
                fstat(srcfd, &st);
                ftpbuf = evbuffer_new();
                evbuffer_read(ftpbuf, srcfd, st.st_size);
                xasprintf(&ftpcmd, "STOR %s", dav_file->file + 1);
                close(srcfd);
        } else {
                log_printf(LOG_ERR, "Failed to open file %s: %m", file);
                log_printf(LOG_INFO, "%s;%s\n", dav_file->method, dav_file->file);
        }

        return ftp_cmd(ftpcmd, ftpcmd2, ftpbuf, dav_file);
}

void
ftp_move(struct file *dav_file) {
char    *ftpcmd = NULL, *ftpcmd2 = NULL;

        if (!regexec(g_ftpregex, dav_file->file, 0, NULL, 0)) {
                xasprintf(&ftpcmd, "RNFR %s", dav_file->file + 1);
                xasprintf(&ftpcmd2, "RNTO %s", dav_file->destination + 1);
        }

        return ftp_cmd(ftpcmd, ftpcmd2, NULL, dav_file);
}

void
ftp_copy(struct file *dav_file) {
char            *file = NULL;
int             srcfd = 0;
struct stat     st;
char            *ftpcmd = NULL, *ftpcmd2 = NULL;
struct evbuffer *ftpbuf = NULL;

        if (regexec(g_ftpregex, dav_file->file, 0, NULL, 0)) { return; }

        xasprintf(&file, "%s%s", g_root, dav_file->file);

        if ((srcfd = open(file, O_RDONLY)) >= 0) {
                fstat(srcfd, &st); ftpbuf = evbuffer_new();
                evbuffer_read(ftpbuf, srcfd, st.st_size);

                xasprintf(&ftpcmd, "STOR %s", dav_file->destination + 1);
                close(srcfd);
        } else {
                log_printf(LOG_ERR, "Failed to open file %s: %m", file);
                log_printf(LOG_INFO, "%s;%s\n", dav_file->method, dav_file->file);
        }

        /*
                xasprintf(&ftpcmd, "RETR %s", dav_file.file + 1);
                xasprintf(&ftpcmd2, "STOR %s", dav_file.destination + 1);
                ftpbuf = evbuffer_new();
        */

        return ftp_cmd(ftpcmd, ftpcmd2, ftpbuf, dav_file);
}

//EOF

