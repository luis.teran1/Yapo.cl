#ifndef DAV_FTP_H
#define DAV_FTP_H

extern struct bconf_node       *g_bconf;
extern regex_t                 *g_ftpregex;
extern char                    *g_root;

void clean_squid(const char *host, const char *port, char *file);
void squid_delete(struct file *dav_file);

int ftp_cb(struct ftpdb *ftp);
void ftp_log_cb(struct ftpdb *ftp, int level, const char *fmt, ...);
void ftp_cmd(char *ftpcmd, char *ftpcmd2, struct evbuffer *ftpbuf, struct file *dav_file);
void ftp_delete(struct file *dav_file);
void ftp_put(char *file, struct file *dav_file);
void ftp_move(struct file *dav_file);
void ftp_copy(struct file *dav_file);

#endif

//EOF

