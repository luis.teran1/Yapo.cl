#ifndef DAV_NEON_WRAPPER_H
#define DAV_NEON_WRAPPER_H

int neput_file(char *filename, ne_uri uri, char *remote_path);
int nemv_file(struct file *f, ne_uri uri);
int necp_file(struct file *f, ne_uri uri);
int nedel_file(struct file *f, ne_uri uri);

#endif

