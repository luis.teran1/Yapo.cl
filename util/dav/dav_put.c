#include "dav_put.h"

void * replicateData(void *t_id);
void usage(char *app);

UrlList			*g_urlList = NULL, *g_extUrlList = NULL;

static int		g_urlListLen = 0, g_replay_folder_len = 0, g_extUrlListLen = 0,
				g_root_len = 0, debug = 0, g_ulc_len = 0, g_exit_flag = 0;

static const char	*g_replay_folder = NULL;

static char		*g_root = NULL;

static struct UrlListContainer	g_ulc[2];

pthread_mutex_t		g_newMsg_mutex, g_msgList_mutex;
pthread_cond_t		g_newMsg_cond;


SLIST_HEAD(ListHead, ListNode_struct)	g_msgList;

/************************************/
LogReplayFile
logReplayFile_init(void){
	LogReplayFile	lrf;

	lrf = xmalloc( sizeof(*lrf) );
	memset(lrf, 0, sizeof(*lrf));

	return lrf;	
}

int
logReplay_commit(LogReplayFile lrf, ListNode nh, long t_id) {
char	*errMsg = NULL;
int		rc = 0;

	if( !(lrf->log_replay_active) ) return rc;

	sprintf(lrf->full_path, "%s/%s", g_replay_folder, lrf->file_name);

	if (rename(lrf->full_path_tmp, lrf->full_path) < 0) {
		errMsg = strerror(errno);
		log_printf(LOG_ERR, "Unable to rename the file [%s] to [%s]: [%s]",
			lrf->full_path_tmp, lrf->full_path, errMsg);

		//Push the file back on the threads queue and hope for the best - Zane
		pthread_mutex_lock(&g_msgList_mutex);
			SLIST_INSERT_HEAD(&g_msgList, nh, nextNode);
		pthread_mutex_unlock(&g_msgList_mutex);

		rc = 1;
	}

	lrf->full_path_tmp[0] = lrf->full_path[0] = '\0';
	lrf->log_replay_active = 0;

	return rc;
}

void
logReplay_append(LogReplayFile lrf, struct file *f, UrlList remote_dav) {
FILE		*pFile = NULL;
unsigned int	i = 0;
int		file_name_len = 0;

	if(!f || !f->file){
		log_printf(LOG_ERR, "Invalid file struct.");
		goto free_and_exit;
	}

	if (((file_name_len = strlen(f->file)) + 1) > lrf->file_name_sz) {
		if(lrf->file_name){ free(lrf->file_name); }

		lrf->file_name_sz = file_name_len * 4;
		lrf->file_name = xmalloc( sizeof(char) * lrf->file_name_sz); //Optimistic :-) - Zane
	}

	/* XXX - This should be done only when a new file_name is received - Zane */
	sprintf(lrf->file_name, "%s", f->file);

	for (i = 0; lrf->file_name[i] != '\0'; ++i) {
		/* replace the '/' for '_', to save the whole path of the image as the file name */
		if (lrf->file_name[i] == '/') lrf->file_name[i] = '_';
	}

	/* XXX - If we run out of memory this will fail here too, and the log will be lost - Zane */
	if((g_replay_folder_len + 5 + lrf->file_name_sz + 1) > lrf->full_path_sz){
		if(lrf->full_path_tmp){ free(lrf->full_path_tmp); }
		if(lrf->full_path){ free(lrf->full_path); }

		lrf->full_path_sz = (g_replay_folder_len + 5 + lrf->file_name_sz + 1);
		lrf->full_path_tmp = xmalloc( sizeof(char) * lrf->full_path_sz); //Optimistic :-) - Zane
		lrf->full_path = xmalloc( sizeof(char) * lrf->full_path_sz);
	}

	sprintf(lrf->full_path_tmp, "%s/%s/%s", g_replay_folder, "tmp", lrf->file_name);

	if ( !(pFile = fopen(lrf->full_path_tmp, "a")) ) {
		log_printf(LOG_ERR, "Unable to create/open the file: [%s]", lrf->full_path_tmp);
		goto free_and_exit;
	}

	/*We put the HTTP verb on the file in order to know what the replay script should do*/
	fprintf(pFile, "%s %s %s\n", f->file, remote_dav->url, f->method);

	fclose(pFile);
	++(lrf->log_replay_active);

free_and_exit:

	return;
}

/*+++++++++++++++++++++++++++++++++++*/

void *
replicateData(void *t_id){
int					ulcIter = 0, urlIter = 0, res = 0, rc = 0, _file_sz = 0, file_len = 0;
long				_t_id = (long)t_id;
UrlList				dav_srv_url = NULL;
ListNode			_nh = NULL;
struct file			dav_file;
LogReplayFile		lrf = NULL;
struct timespec		wait = {0, 0};
char				*_file = NULL;

	lrf = logReplayFile_init();

	while (1) {
		if (_nh != NULL) {
			free(_nh);
			_nh = NULL;
		}

		if ( (rc = pthread_mutex_lock(&g_msgList_mutex)) ) {
			log_printf(LOG_ERR, "pthread_mutex_lock FAILED: [%d]\nThread [%ld] exiting...", rc, _t_id);
			break;
		}
			_nh = g_msgList.slh_first;
			if (_nh) { SLIST_REMOVE_HEAD(&g_msgList, nextNode); }
		pthread_mutex_unlock(&g_msgList_mutex);

	 	if (!_nh) { //queue empty - Zane
			if (g_exit_flag) { break; }

			pthread_mutex_lock(&g_newMsg_mutex);
				wait.tv_sec = time(NULL) + 30; // Magical Number - Zane
				rc = pthread_cond_timedwait(&g_newMsg_cond, &g_newMsg_mutex, &wait);
				//if (rc) { printf("[THREAD-%ld] timeout happened", _t_id); }
			pthread_mutex_unlock(&g_newMsg_mutex);

			continue; //Will check the queue. If nothing, we wait here again - Zane
		}

		if (_nh->row[_nh->rowLen - 1] == '\n') {
			_nh->row[_nh->rowLen - 1] = '\0';
		}

		if (parse_row(_nh->row, &dav_file) != 0) { continue; }

		file_len = strlen(dav_file.file);

		for (ulcIter = 0; ulcIter < g_ulc_len; ++ulcIter) {
			if (g_ulc[ulcIter].urlList == NULL) { continue; }

			for (urlIter = 0; urlIter < (*g_ulc[ulcIter].listLen); ++urlIter) {
				if( !(dav_srv_url = g_ulc[ulcIter].urlList[urlIter]) ){
					xerr(1, "[Thread-%ld] The impossible has happened...", _t_id);
				}

				if (strcmp(dav_file.method, "PUT") == 0) {
					if ((g_root_len + file_len + 1) > _file_sz) {
						if(_file) free(_file);

						_file_sz = (file_len * 4) + g_root_len + 1;
						_file = xmalloc( sizeof(char) * _file_sz); //Optimistic :-) - Zane
					}

					sprintf(_file, "%s%s", g_root, dav_file.file);

					if ((res = neput_file(_file, dav_srv_url->uri, dav_file.file)) != 0) {
						logReplay_append(lrf, &dav_file, dav_srv_url);
						// XXX - Nagios
					}
				} else if (strcmp(dav_file.method, "MOVE") == 0) {
					if((res = nemv_file(&dav_file, dav_srv_url->uri)) != 0){
						/* XXX Replay-log */
						log_printf(LOG_INFO, "%s;%s;%s\n",
							dav_file.method, dav_file.file, dav_file.destination);
					}
				} else if (strcmp(dav_file.method, "COPY") == 0) {
					if((res = necp_file(&dav_file, dav_srv_url->uri)) != 0){
						/* XXX Replay-log */
						log_printf(LOG_INFO, "%s;%s;%s\n",
							dav_file.method, dav_file.file, dav_file.destination);
					}
				} else if (strcmp(dav_file.method, "DELETE") == 0) {
					if((res = nedel_file(&dav_file, dav_srv_url->uri)) != 0){
						logReplay_append(lrf, &dav_file, dav_srv_url);
					}
				}
			}//END_for urlIter
		}//END_for urlListContainerIter

		if ( (rc = logReplay_commit(lrf, _nh, _t_id)) ) {
			_nh = NULL; // We have put it back on the queue, no free() then - Zane
		}
	} //END_while(1)

	pthread_exit((void *) t_id);
}

/****************************************/

int
parse_row(char *row, struct file *f) {
	char	*tmp = NULL;

	if (!row){ return 1; }

	if( !(tmp = strchr(row, ';')) ){ return 1; }

	f->method = row;
	f->file = tmp + 1;
	*tmp = '\0';

	if (!(tmp = strchr(tmp + 1, ';'))) {
		f->destination = 0;
		return 0;
	}

	f->destination = tmp + 1;
	*tmp = '\0';

	if (strncmp(f->destination, "http://", 7) == 0) {
		f->destination = strchr(f->destination + 7, '/');
		/* Apache adds a slash under some circumstances, remove it */
		if (f->destination[1] == '/') {
			f->destination++;
		}
	}

	return 0;
}

void
usage(char *app) {
	printf("Usage: %s [--config <CONF_FILE>] [--root <DAV_ROOT_PATH>] [--help] [--debug]\n", app);
}

int
main(int argc, char **argv) {
	char			row[MAX_ROWLEN];
	char			*config = NULL;
	ne_uri			uri = {0};
	struct bconf_node	*bconf = NULL;
	//0-> local and remote replication| 1-> only local replication 
	int			singleDataCenterMode = 0, rc = 0, len = 0, i = 0;
	void		*threadStatus;

	struct ifaddrs	*ifaddr = NULL, *ifa = NULL;
	pthread_t		threads[NUM_THREADS];
	pthread_attr_t	attr;
	long			t = 0;
	ListNode		np;

	int option_index = 0, opt = 0;
	static struct option long_options[] = {
		{"root", 1, 0, 0},
		{"config", 1, 0, 0},
		{"help", 0, 0, 0},
		{"debug", 0, 0, 0},
		{"singledc", 0, 0, 0},
		{0, 0, 0, 0}
	};

	while (1) {
		opt = getopt_long(argc, argv, "", long_options, &option_index);
		if (opt == -1){ break; }

		if (opt == 0) {
			switch (option_index) {
			case 0:		g_root = xstrdup(optarg);	break;
			case 1:		config = xstrdup(optarg);	break;
			case 2:		usage(argv[0]);			exit(1);
			case 3:		debug = 1;			break;
			case 4: 	singleDataCenterMode = 1;	break;
			}
		}
	}

	if (!config) { config = xstrdup("dav_put.conf"); }

	if (!(bconf = config_init(config))) { xerr(0, "Failed to read config file %s\n", config); }

	if ( !g_urlListLen ){
		struct bconf_node	*_bconfHolder = NULL, *urlNode = NULL;
		const char		*_valHolder = NULL;
		int			_i = 0, urlListIx = 0, family, nameInfoRes, sameHost_flag;
		char			host[NI_MAXHOST];
		
		if( getifaddrs(&ifaddr) == -1){ xerr(1, "%s: Failed to obtain local address.\n", argv[0]); }

		if((urlNode = bconf_get(bconf, "url")) != NULL){
			if( !(g_urlListLen = bconf_count(urlNode)) ){
				xerr(0, "Error [%d] while reading url from bconf\n", __LINE__);
			}

			g_urlList = xmalloc(sizeof(UrlList) * g_urlListLen);

			for(_i = 0; _i < g_urlListLen; ++_i){
				if( !(_bconfHolder = bconf_byindex(bconf_get(bconf, "url"), _i)) ){
					xerr(0, "Error [%d] while reading url from bconf\n", __LINE__);
				}

				if( !(_valHolder = bconf_value(_bconfHolder)) ){
					xerr(0, "Error [%d] while reading url from bconf\n", __LINE__);
				}

				if(ne_uri_parse(_valHolder, &uri) || !uri.host || !uri.path){
					xerr(0, "Could not parse URL [%s].\n", _valHolder);
				}

				for(sameHost_flag = 0, ifa = ifaddr; ifa != NULL; ifa = ifa->ifa_next){
					if (ifa->ifa_addr == NULL){ continue; }

					family = ifa->ifa_addr->sa_family;		
					if( !(family == AF_INET) ){ continue; }

					nameInfoRes = getnameinfo(ifa->ifa_addr, sizeof( struct sockaddr_in ),
						host, NI_MAXHOST, NULL, 0, NI_NUMERICHOST);
					if(nameInfoRes != 0){
						xerr(0, "getnameinfo() failed: %s.\n", gai_strerror(nameInfoRes));
					}

					/* Special case, for testing purpose - Zane*/
					if( !strcmp("127.0.0.1", host) ){ continue; }

					if ( strcmp(uri.host, host) ){ continue; }

					sameHost_flag = 1;
					break;
				}
				
				if(sameHost_flag){ continue; }
				
				g_urlList[urlListIx] = xmalloc( sizeof(struct UrlList_struct) );
				g_urlList[urlListIx]->url = xstrdup(_valHolder);
				g_urlList[urlListIx]->uri = uri; //struct copy - Zane
				++urlListIx;
			}

			g_urlListLen = urlListIx;

			/* It's either buggy or I'm using it wrongly, as it is it segfaults - Zane
			 * bconf_free(&_bconfHolder); bconf_free(&urlNode);
			 */
		} else {
			xerr(0, "No URL supplied\n");
		}
	}

	if (!g_extUrlListLen && !singleDataCenterMode) {
		struct bconf_node	*_bconfHolder = NULL, *urlNode = NULL;
		const char		*_valHolder = NULL;
		int			_i = 0;

		if ((urlNode = bconf_get(bconf, "ext_url")) == NULL) {
			xerr(0,"No Ext_Url entry found on bconf %s.\n", config);
		}

		if( !(g_extUrlListLen = bconf_count(urlNode)) ){
			xerr(0, "Error [%d] while reading ext_url from bconf\n", __LINE__);
		}

		g_extUrlList = xmalloc(sizeof(UrlList) * g_extUrlListLen);

		for(_i = 0; _i < g_extUrlListLen; ++_i){
			if( !(_bconfHolder = bconf_byindex(bconf_get(bconf, "ext_url"), _i)) ){
				xerr(0, "Error [%d] while reading ext_url from bconf\n", __LINE__);
			}

			if( !(_valHolder = bconf_value(_bconfHolder)) ){
				xerr(0, "Error [%d] while reading ext_url from bconf\n", __LINE__);
			}

			g_extUrlList[_i] = xmalloc( sizeof(struct UrlList_struct) );
			g_extUrlList[_i]->url = xstrdup(_valHolder);

			if(ne_uri_parse(g_extUrlList[_i]->url, &(g_extUrlList[_i]->uri)) ||
			!(g_extUrlList[_i]->uri.host) || !(g_extUrlList[_i]->uri.path)){
				xerr(0, "Could not parse URL [%s].\n", g_extUrlList[_i]->url);
			}
		}
	} 

	/* Set defaults. */
	if (uri.scheme == NULL) { uri.scheme = (char *) "http"; /* Neon doesn't use const. */ }
	if (uri.port == 0) { uri.port = ne_uri_defaultport(uri.scheme); }

	if( !g_root ){
		const char	*broot = bconf_get_string(bconf, "root");

		if (broot){ g_root = xstrdup(broot); }
		else{ g_root = xstrdup(""); }
	}

	g_root_len = strlen(g_root);

	if( !g_replay_folder ){
		if( !(g_replay_folder = bconf_get_string(bconf, "replay_folder")) ){
			xerr(0, "Error [%d] while reading url from bconf\n", __LINE__);
		}

		g_replay_folder_len = strlen(g_replay_folder);
	}

	/* Initialize socket libraries */
	if( ne_sock_init() ){ xerr(1, "%s: Failed to initialize socket libraries.\n", argv[0]); }

	log_setup("davput", debug ? "debug" : NULL);
	event_init();
	
	g_ulc[0].urlList = g_urlList;
	g_ulc[0].listLen = &g_urlListLen;
	if (g_extUrlListLen) {
		g_ulc[1].urlList = g_extUrlList;
		g_ulc[1].listLen = &g_extUrlListLen;
		g_ulc_len = 2;
	} else {
		g_ulc[1].urlList = NULL;
		g_ulc[1].listLen = NULL;
		g_ulc_len = 1;
	}

	if ( (rc = pthread_mutex_init(&g_newMsg_mutex, NULL)) ) { xerr(1, "pthread_mutex_init returned: %d", rc); }
	if ( (rc = pthread_mutex_init(&g_msgList_mutex, NULL)) ){ xerr(1, "pthread_mutex_init returned: %d", rc); }
	if ( (rc = pthread_cond_init (&g_newMsg_cond, NULL)) )  { xerr(1, "pthread_mutex_init returned: %d", rc); }

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

	for(t = 0; t < NUM_THREADS; ++t){
		rc = pthread_create(&threads[t], &attr, replicateData, (void *)t);
		if (rc) { xerr(1, "[ERROR] return code from pthread_create() is %d\n", rc); }
	}

	/*
	 *	No need to wait for at least one thread listening, since we recheck the predicate (queue not
	 * empty) inside a loop even if we lose cond_signals() the threads will catch up - Zane
	 */

	SLIST_INIT(&g_msgList);

	while (!feof(stdin) && fgets(row, MAX_ROWLEN, stdin)) {
		if( !(len = strlen(row)) ) continue;

		log_printf(LOG_DEBUG, "read line: %s", row);
		if (row[len - 1] != '\n') {
			fprintf(stderr, "Long row, ignoring\n");
			while (!feof(stdin) && fgets(row, MAX_ROWLEN, stdin) &&
				row[strlen(row) - 1] != '\n'){ continue; }
			continue;
		}

		np = xmalloc( sizeof(struct ListNode_struct) );
		memset(np, 0, sizeof(*np));
		sprintf(np->row, "%s", row);
		np->rowLen = len;

		pthread_mutex_lock(&g_msgList_mutex);
			SLIST_INSERT_HEAD(&g_msgList, np, nextNode);
		pthread_mutex_unlock(&g_msgList_mutex);

		pthread_mutex_lock(&g_newMsg_mutex);
			pthread_cond_signal(&g_newMsg_cond);
		pthread_mutex_unlock(&g_newMsg_mutex);
	}

	for (i = 0; i < NUM_THREADS; ++i) {
		g_exit_flag = 1;
		rc = pthread_join(threads[i], &threadStatus);
		if (rc) {
			log_printf(LOG_ERR, "[ERROR] return code from pthread_join() is %d\n", rc);
		}

		log_printf(LOG_DEBUG, "thread %ld exited with status of %ld\n", t, (long)threadStatus);
	}

	freeifaddrs(ifaddr);
	free(g_root);

	pthread_attr_destroy(&attr);
	pthread_mutex_destroy(&g_newMsg_mutex);
	pthread_mutex_destroy(&g_msgList_mutex);
	pthread_cond_destroy(&g_newMsg_cond);
	pthread_exit(NULL);
}

//EOF

