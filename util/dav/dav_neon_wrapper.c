#include "dav_put.h"
#include "dav_neon_wrapper.h"

int
neput_file(char *filename, ne_uri uri, char *remote_path) {
        ne_session *sess;
        int srcfd, ret;

        if ((srcfd = open(filename,O_RDONLY,0)) == -1) { return 1024; }

        sess = ne_session_create(uri.scheme, uri.host, uri.port);
        ret = ne_put(sess, remote_path, srcfd);

        close (srcfd);

        ne_session_destroy(sess);

        return ret;
}

int
nemv_file(struct file *f, ne_uri uri) {
        ne_session *sess;
        int ret;

        sess = ne_session_create(uri.scheme, uri.host, uri.port);
        ret = ne_move(sess, 0, f->file, f->destination);
        ne_session_destroy(sess);

        return ret;
}


int
necp_file(struct file *f, ne_uri uri) {
        ne_session *sess;
        int ret;

        sess = ne_session_create(uri.scheme, uri.host, uri.port);
        ret = ne_copy(sess, 0, 0, f->file, f->destination);
        ne_session_destroy(sess);

        return ret;
}

int
nedel_file(struct file *f, ne_uri uri) {
        ne_session *sess;
        int ret;

        sess = ne_session_create(uri.scheme, uri.host, uri.port);
        ret = ne_delete(sess, f->file);
        ne_session_destroy(sess);

        return ret;
}

//EOF

