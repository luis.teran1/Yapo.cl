#ifndef DAV_PUT_H
#define DAV_PUT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>
#include <err.h>
#include <regex.h>
#include <errno.h>
#include <arpa/inet.h>
#include <ifaddrs.h>

#include <getopt.h>

#include <ne_socket.h>
#include <ne_session.h>
#include <ne_uri.h>
#include <ne_basic.h>
#include <ne_utils.h>

#include <pthread.h>
#include <queue.h>

#include "bconfig.h"
#include "bconf.h"
#include "logging.h"
#include "event.h"
#include "util.h"

#define MAX_ROWLEN	4096
#define NUM_THREADS	5

struct file {
	char	*method;
	char	*file;
	char	*destination;
};

struct ListHead ; //*headp;

typedef struct ListNode_struct {
        char	row[MAX_ROWLEN];
        int		rowLen;

        SLIST_ENTRY(ListNode_struct)	nextNode;
} *ListNode;


/**********************************/
typedef struct UrlList_struct{
        char    *url;
        ne_uri	uri;
} *UrlList;

struct UrlListContainer {
	UrlList	*urlList;
	int	*listLen;
};
/**********************************/


/**********************************/
typedef struct LogReplayFile_struct {
        char    *file_name, *full_path, *full_path_tmp;
        int     file_name_sz, full_path_sz, log_replay_active;
} *LogReplayFile;

LogReplayFile	logReplayFile_init(void);
int		logReplay_commit(LogReplayFile lrf, ListNode nh, long t_id);
void		logReplay_append(LogReplayFile lrf, struct file *f, UrlList remote_dav);
/**********************************/


/********************************/
void clean_squid(const char *host, const char *port, char *file);

int parse_row(char *row, struct file *f);

int neput_file(char *filename, ne_uri uri, char *remote_path);
int nemv_file(struct file *f, ne_uri uri);
int necp_file(struct file *f, ne_uri uri);
int nedel_file(struct file *f, ne_uri uri);

#endif

