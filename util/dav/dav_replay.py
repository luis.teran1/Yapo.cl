#!/usr/bin/env python

import re
import os
import sys
from optparse import OptionParser
import signal
import time
import urlparse

conf_dict = {};
search_interval = 30; #The amount of seconds the function is gonna wait to search again
client = None;
g_dbg = 0;

def stop_running(signum, frame):
	
	print 'Signal %s received. Exiting...' % str(signum);
	sys.exit(0);
#END_stop_running

def start_variables_from_conf(options):
	global client;
	_dav_path = None;

	try:
		_conf = open(options.config, 'r');
	except Exception, e:
		print 'Unable to open conf file [%s]: %s' % (options.config, str(e));
		sys.exit(1);

	_replay_folder_pattern = re.compile('replay_folder\s?=\s?(.*)');
	_url_pattern = re.compile('url\s?=\s?(.*)');
	_image_path_pattern = re.compile('root\s?=\s?(.*)');
	_pydav_path = re.compile('pydav_path\s?=\s?(.*)');

	for line in _conf:
		_m = _replay_folder_pattern.match(line);
		if _m:
			conf_dict['replay_folder'] = _m.group(1).strip();
			continue;

		_m = _url_pattern.match(line);
		if  _m:
			conf_dict['url']  = _m.group(1).strip();
			continue;
		
		_m = _image_path_pattern.match(line);
		if _m:
			conf_dict['image_path'] = _m.group(1).strip();
			continue;

		_m = _pydav_path.match(line);
		if _m:
			_dav_path = _m.group(1).strip();
			continue;
	#End_for	
	_conf.close();

	if not _dav_path: 
		_dav_path = '/opt/blocket/lib/python/2';

	sys.path.append(_dav_path);
	print 'Using dav module in %s' % _dav_path;

	import PyDAV_0_21.client;	
	client = PyDAV_0_21.client;	

	try:
		_fd = open(options.pid, 'w');
		_fd.write( str( os.getpid() ) );
		_fd.close();
	except Exception, e:
		print 'Unable to open [%s]: %s' % (options.pid, str(e));
		sys.exit(1);

	return;
#End_start_variables_from_conf

g_ignore_path = {};
def search_files():
	while 1:
		files = os.listdir(conf_dict['replay_folder']);
		for file in files:
			file = file.strip();

			if g_ignore_path.has_key( file ):
				continue;

			_file_name = "%s/%s" % (conf_dict['replay_folder'], file);
			if( os.path.isdir( _file_name ) ):
				g_ignore_path[file] = None;
				continue;

			_file_name = file.split('/')[-1];

			replay_send_file(_file_name);
		time.sleep(search_interval);

#End_search_files

def sanitize_url(url):
	parsed = list(urlparse.urlparse(url))
	parsed[2] = re.sub("/{2,}", "/", parsed[2]) # replace two or more / with one
	return urlparse.urlunparse(parsed)

def replay_send_file(filename):
	_local_path = conf_dict['image_path'] + '/' + filename.replace('_', '/');
	_replay_file = conf_dict['replay_folder'] + '/' + filename;
	_all_images_succeeded = 1;
	_host_exceeded_attempts = '';

	try:
		_rf = open(_replay_file, "r");
	except Exception, e:
		print "Error reading the replay_file [%s]: %s" % (_replay_file, str(e));
		return;

	for _line in _rf:
		#Each line of the file must have the url of the dav and the HTTP verb used in order to try to resend the command
		#The pattern of the line must be <image_path> <dav_host> <VERB>
		_line_fields = _line.strip().split(' ');
		_remote_path = _line_fields[1] + _line_fields[0];
		_http_verb = _line_fields[2];

		if not len(_remote_path): 
			continue;
		_remote_path = sanitize_url(_remote_path)
		res = client.Resource(_remote_path);

		try:
			print 'Sending file %s to dav [%s] %s' % (_local_path, _http_verb, _remote_path)
			if (_http_verb == 'PUT'):
				response = res.put(file=_local_path);
			elif (_http_verb == 'DELETE'):
				response = res.delete();
			else:
				print 'File with invalid HTTP VERB [%s]' % _http_verb;
				continue;
			response_code = int(response.get_status().split()[0]);
			#If we are trying to delete the file and get the 404 error, we should consider the operation was successfully executed
			if (((response_code < 200) or (response_code > 300)) and not (_http_verb == 'DELETE' and response_code == 404)):
				print 'Error sending image to dav %s [%s]' % (_remote_path,response.get_status());
				_all_images_succeeded = 0;

		except Exception, e:
			print 'Error trying to connect to the dav server [%s]: %s' % (_remote_path, str(e));
			_all_images_succeeded = 0;
	#End_for
	_rf.close();

	#Delete the file only if all the dav servers have successfully received the image
	if _all_images_succeeded == 1:
		try:
			os.system('rm ' + _replay_file);
		except Exception, e:
			print 'Error trying to remove the file %s' % (_replay_file, str(e));

	#TODO: Verify when to notify nagios

#End_replay_send_file

def parse_args():
	_err_flag = 0;

	usage = "\nusage: %s [options] <--pid|-p PID_FILE> <--cfg|-c CFG_FILE>" % os.path.basename(sys.argv[0]);

	parser = OptionParser();
	parser.add_option("-c", "--cfg", action="store", dest="config", help="Configuration file",metavar="FILE");
	parser.add_option("-p", "--pid", action="store", dest="pid", help="File to write PID", metavar="FILE");

	(options, args) = parser.parse_args();

	if not options.config:
		print 'You must specify a conf file';
		_err_flag = 1;

	if not options.pid:
		print 'Missing PID file.';
		_err_flag = 1;

	if _err_flag:
		parser.print_help();
		sys.exit(1);

	return options;
#END_parse_args

def main():	
	signal.signal(signal.SIGTERM, stop_running);

	start_variables_from_conf( parse_args() );

	print '%s running...' % os.path.basename(sys.argv[0]);

	search_files();

	return;
#END_main

if __name__ == '__main__':
	# do the UNIX double-fork magic, see Stevens' "Advanced 
	# Programming in the UNIX Environment" for details (ISBN 0201563177)
	try: 
		pid = os.fork() 
		if pid > 0:
			# exit first parent
			sys.exit(0) 
	except OSError, e: 
		print >>sys.stderr, "fork #1 failed: %d (%s)" % (e.errno, e.strerror) 
		sys.exit(1)

	# decouple from parent environment
	os.setsid() 
	os.umask(0) 
	
	# do second fork
	try: 
		pid = os.fork() 
		if pid > 0:
			# exit from second parent, print eventual PID before
			print "Daemon PID %d" % pid 
			sys.exit(0) 
	except OSError, e: 
		print >>sys.stderr, "fork #2 failed: %d (%s)" % (e.errno, e.strerror) 
		sys.exit(1) 

	main();

#EOF

