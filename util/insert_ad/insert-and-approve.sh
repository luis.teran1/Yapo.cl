#!/bin/bash

TRANS="nc ch4stg 5656"

function insert_ad {
	AD_ID=$($TRANS < newad.in | grep ad_id)
	echo $AD_ID
	sed "s/AD_ID/$AD_ID/g" clear.in | $TRANS
	TOKEN=$($TRANS < authenticate.in | grep token)
	sed "s/AD_ID/$AD_ID/g; s/TOKEN/$TOKEN/g" review.in | $TRANS
}

insert_ad
