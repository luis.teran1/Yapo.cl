import re

def ip_str2int(iptext):
	m = re.search(r"(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})", iptext)
	m = m.groups()
	return int(m[0])<<24|int(m[1])<<16|int(m[2])<<8|int(m[3])

def ip_int2str(ipdec):
	return "%d.%d.%d.%d" % ((ipdec>>24)&0xFF, (ipdec>>16)&0xFF, (ipdec>>8)&0xFF, ipdec&0xFF)



def ip_whois_cache(ip):
	return


ip_whois_cache = {ip_str2int("10.0.1.28"): "victor", ip_str2int("10.0.1.24"): "boris del 8"} 

def ip_whois(ip):
	if ip in ip_whois_cache:
		return ip_whois_cache[ip]
	return "unknown"
	

