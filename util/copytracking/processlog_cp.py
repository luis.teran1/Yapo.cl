import curses
import sys
import re
import time
import sets
import signal
import operator
import ipstuff




def signal_handler(signal, frame):
        print "te pusificaste!!"
	deinit_screen()	
        sys.exit(0)


screen = None

wips = None
wipsm = None


def window_init(x, y, width, height, title, footer):
	return {'win': None, 'x': x, 'y': y, 'width': width, 'height': height, 'title': title, 'footer': footer}

def window_update_footer(win):
	screen.addstr(win['y']+win['height']+1, win['x'], win['footer'] + " "*(win['width']-len(win['footer'])), curses.A_BOLD | curses.A_REVERSE)
	screen.refresh()

def window_draw(win):
	global screen	
	screen.addstr(win['y'], win['x'], win['title'] + " "*(win['width']-len(win['title'])), curses.A_BOLD | curses.A_REVERSE)
	win['win']=curses.newwin(win['height'], win['width'], win['y']+1, win['x'])
	window_update_footer(win)

def deinit_screen():
	curses.echo()
	curses.endwin()

def init_screen():
	global screen, win_ips, win_ips_mismatch, wips, wipsm

	width = 40
	height = 42
	
	screen = curses.initscr()
	curses.noecho()
	curses.curs_set(0)

	wips  = window_init(0, 0, width, height, "Copy Paste TOP 40 IPs", "blah")
	wipsm = window_init(width+2, 0, width, height, "Copy Paste TOP 40 mismatch IPs", "blah2")
	window_draw(wips)
	window_draw(wipsm)
	
	screen.refresh()


def follow(fp):
	fp.seek(0,0)
	while True:
		line = fp.readline()
		if not line:
			break
		yield line

	fp.seek(0,2)
	while True:
		line = fp.readline()
		if not line:
			time.sleep(0.1)
			continue
		yield line


def small_window(w, data, fmt):
	w.clear()
	w.border()
	i = 1
	for row in data:
		w.addstr(i, 1, fmt % row)
		i = i + 1
	
	w.refresh()

def update_top(ipsi, footer):
	wips['footer'] = footer
	window_update_footer(wips)
	r = sorted(ips.iteritems(), key=operator.itemgetter(1), reverse=True)
	small_window(wips['win'], ( (ipstuff.ip_int2str(r[i][0]), r[i][1], ipstuff.ip_whois(r[i][0]))   for i in range(len(r))  ), "%12s (%4d) [%s]")

def update_top_mismatch(ips, footer):
	wipsm['footer'] = footer
	window_update_footer(wipsm)
	r = sorted(ips.iteritems(), key=operator.itemgetter(1), reverse=True)
	small_window(wipsm['win'], ( (ipstuff.ip_int2str(r[i][0]), r[i][1], ipstuff.ip_whois(r[i][0]))   for i in range(len(r))  ), "%12s (%4d) [%s]")

def main():
	init_screen()

	signal.signal(signal.SIGINT, signal_handler)
	fp=open(sys.argv[1])

	count, mismatch_count = 0, 0
	ts = []
	#for line in fp.xreadlines():
	for line in follow(fp):
		m = line_format.match(line)
		mm = m.groupdict() 

		r =  request_format.match(mm["request"])
		if not r:	
			continue

		#t = time.strptime(mm["time"].split()[0], "%d/%b/%Y:%H:%M:%S")
		#ts.append(t)
	
		rr = r.groupdict()

		x = int(rr["rand"])
		x = x << 24 | x << 16 | x << 8 | x
	
		ip = ipstuff.ip_str2int(mm["host"])
		
		if ip == (int(rr["ip"]) ^ x):
			if ip in ips_mismatch:
				ips_mismatch[ip] = ips_mismatch[ip] + 1
			else:
				ips_mismatch[ip] = 1

			mismatch_count = mismatch_count + 1
			if mismatch_count % 10 == 0:
				update_top_mismatch(ips_mismatch, "Total: %d C+P form mismatch IPs" % mismatch_count)
				
				continue
		else:
			if ip in ips:
				ips[ip] = ips[ip] + 1
			else:
				ips[ip] = 1

			#sys.stdout.write("\r%d: %d" % (count, ip))
			count = count + 1

			if count % 10 == 0:
				update_top(ips, "Total: %d C+P actions" % count)


ips = {}
ips_mismatch = {}

# log format
# 186.173.2.213 - - [16/Aug/2012:00:00:00 -0400] "GET /img/prissankt.gif HTTP/1.1" 200 64

line_format = re.compile( 
    r"(?P<host>[\d\.]+)\s" 
    r"(?P<identity>\S*)\s" 
    r"(?P<user>\S*)\s"
    r"\[(?P<time>.*?)\]\s"
    r'"(?P<request>.*?)"\s'
    r"(?P<status>\d+)\s"
    r"(?P<bytes>\S*)\s"
)

request_format = re.compile(
	r"GET /img/line_t\.gif\?(?P<ip>\-?\d+)\.(?P<rand>\d+)\.(?P<count>\d+)"
)


main()
