import ipstuff
import sys
import sets
# ftp://ftp.lacnic.net/pub/stats/lacnic/delegated-lacnic-latest
# ftp://ftp.arin.net/pub/stats/arin/delegated-arin-latest
## arin|US|ipv4|23.32.0.0|2359296|20110516|allocated

geo_bm = dict((1<<i & 0xFFFFFFFF, i) for i in range(32))
ip_tree = [{} for i in range(32)] 


def geo_init(filename):
	fp = open(filename, "r")

	for line in fp:
		cont = line.split('|')
		if cont[1] != "*" and cont[2] == "ipv4":
			ip_tree[geo_bm[int(cont[4])]][ipstuff.ip_str2int(cont[3])]=cont[1]
	fp.close()

def geo_get_cc(ip):
	ipd = ipstuff.ip_str2int(ip)
	for i in range(32):
		p = (0xFFFFFFFF<<i) & 0xFFFFFFFF & ipd
		if p in ip_tree[i]:
			return ip_tree[i][p]
	return None



geo_init("delegated-lacnic-latest")
geo_init("delegated-arin-latest")


print geo_get_cc("190.217.187.181")
print geo_get_cc("64.76.70.98")
print geo_get_cc("200.58.125.232")




#print ip_tree


