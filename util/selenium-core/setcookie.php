<?
require_once('init.php');

if (isset($_REQUEST['name']) && isset($_REQUEST['value'])) {
	global $BCONF;
	header('Set-Cookie: '. $_REQUEST['name'] . '=' . $_REQUEST['value']. '; expires=' . 
	       (string)(time() + bconf_get($BCONF, 'adwatch.cookie_expire_days') * 86400)
	       . '; path=' . '/'
	       . '; domain=' . bconf_get($BCONF, 'adwatch.cookie_domain')
	       , false
	       );

	print "<pre>Set cookie ".$_REQUEST['name'] . " to ". $_REQUEST['value'] ." with domain".
		bconf_get($BCONF, 'adwatch.cookie_domain')
		. "</pre>";
} elseif (isset($_REQUEST['name']) && isset($_REQUEST['delete'])) {
	global $BCONF;
	header('Set-Cookie: '. $_REQUEST['name'] . '=; expires=0'
	       . '; path=' . '/'
	       . '; domain=' . (isset($_REQUEST['domain'])?$_REQUEST['domain']:bconf_get($BCONF, 'adwatch.cookie_domain'))
	       , false
	       );

	print "<pre>Delete cookie ".$_REQUEST['name'] . " with domain ".
		bconf_get($BCONF, 'adwatch.cookie_domain')
		. "</pre>";
}
