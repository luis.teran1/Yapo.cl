INSTALLDIR=/selenium-core

INSTALL_TARGETS=index.html getMail.php getVar.php bCmd.php prefs.js hostperm.1 setcookie.php

IN_FILES=prefs.js.in hostperm.1.in

include ${TOPDIR}/mk/defvars.mk
include ${TOPDIR}/mk/in.mk
include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
include $(TOPDIR)/mk/clean.mk
