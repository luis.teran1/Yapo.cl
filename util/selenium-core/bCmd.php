<?php
/*
bCmd.php

A little php script that allows to execute commands via an URL (like for
Selenium scripts).
*/
require_once('init.php');

echo "<html><body>";

/* Send the complete transaction command string $s to the transaction handler */
function trans_send($s) {
	global $BCONF;
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
	fwrite($fh, $s);
	fpassthru($fh);
	fclose($fh);
}

function bconf_overwrite($key, $value) {
	$ret = 0;
	$dir = "../../regress/final";
	$val = urlencode($value);
	$cmd = "make -C ${dir} bconf-overwrite-${key}:${val}";
	system($cmd, $ret);
	return $ret;
}

/* rebuild_campsearch */
if (isset($_REQUEST['rebuild-campsearch'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-campsearch ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* rebuild_bidsearch */
if (isset($_REQUEST['rebuild-bidsearch'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-bidsearch ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* rebuild_csearch */
if (isset($_REQUEST['rebuild-csearch'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-csearch ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* rebuild_asearch */
if (isset($_REQUEST['rebuild-asearch'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-asearch ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* rebuild_index */
if (isset($_REQUEST['rebuild-index'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-index ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* rebuild_index_full */
if (isset($_REQUEST['rebuild-index-full'])) {
        print "<pre>";
	system("(cd ../../ && make rebuild-index-full ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}
/* Update index */
if (isset($_REQUEST['update-aindex'])) {
	$suff = '';
	if ($_REQUEST['update-aindex'] != 1)
		$suff = '-' . $_REQUEST['update-aindex'];
        print "<pre>";
	system("cd ../../ && make update-aindex$suff");
	print "Done</pre>";
	exit;
}
/* Flush queue */
if (isset($_REQUEST['flushqueue'])) {
        print "<pre>";
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
        fwrite($fh, "cmd:flushqueue\nqueue:{$_REQUEST['flushqueue']}\ncommit:1\n" . (isset($_REQUEST['per_info']) ? "per_info:1\n" : "") . "end\n");
	fpassthru($fh);
	fclose($fh);
        print "</pre>";
}
if (isset($_REQUEST['statpoints'])) {
	while (!$haveone) {
		$haveone = false;
		sleep(1);
		$data = bsearch_search_vtree("*.common.statpoints", "0 event:{$_REQUEST['statpoints']} id:{$_REQUEST['id']}");
		echo "<pre>\n";
		foreach ($data as $key => $val) {
			if (!empty($val))
				$haveone = true;
			if (is_array($val))
				$val = implode("\t", $val);
			echo "$key $val\n";
		}
		echo "</pre>\n";
	}
}

/* Give store id 14661 (Arvika Bilsmide) credit 20 */
if (isset($_REQUEST['arvika-credit20'])) {
        echo "<pre>\n";
        system("(cd ../../ && echo \"update users set account=20 where email='patrik@blocket.se'\" | `make rinfo|fgrep 'psql -h'` )");
	echo "Credit set to 20\n";
        echo "</pre>\n";
}
/* Give store id 14661 (Arvika Bilsmide) credit 0 */
if (isset($_REQUEST['arvika-credit0'])) {
        echo "<pre>\n";
        system("(cd ../../ && echo \"update users set account=0 where email='patrik@blocket.se'\" | `make rinfo|fgrep 'psql -h'` )");
	echo "Credit reset to 0\n";
        echo "</pre>\n";
}

/* Update an existing ad_param */
if (isset($_REQUEST['update_adparam'])) {
	if(!isset($_REQUEST['name'])) { echo "Missing argument name\n"; exit(1); }
	if(!isset($_REQUEST['value'])) { echo "Missing argument value\n"; exit(1); }
	if(!isset($_REQUEST['ad_id'])) { echo "Missing argument ad_id\n"; exit(1); }
        $cmdstr = "(cd ../../ && echo \"update ad_params set value='".$_REQUEST['value']."' where name='".$_REQUEST['name']."' and ad_id=".$_REQUEST['ad_id']."\" | `make rinfo|fgrep 'psql -h'` )";
        echo "<pre>\n";
        $last_line = system($cmdstr);
	if ($last_line == "UPDATE 1") {
		echo $_REQUEST['name']." set to ".$_REQUEST['value']." for ad_id ".$_REQUEST['ad_id']."\n";
	} else {
		echo "UPDATE FAILED";
	}
        echo "</pre>\n";
}

if (isset($_REQUEST['flushmail'])) {
        print "<pre>";
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
        fwrite($fh, "cmd:flushmail\nmail_id:{$_REQUEST['id']}\ncommit:1\napplication:{$_REQUEST['flushmail']}\nend\n");
	fpassthru($fh);
	fclose($fh);
        print "</pre>";
}

if (isset($_REQUEST['bump_ad_trans'])) {
        print "<pre>";
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
        fwrite($fh, "cmd:bump_ad\nad_id:{$_REQUEST['ad_id']}\nbatch:1\ncommit:1\nend\n");
	fpassthru($fh);
	fclose($fh);
	if ($_REQUEST['null_orig'] == 1)
        	system("(cd ../../ && echo \"update ads set orig_list_time = NULL WHERE ad_id=".$_REQUEST['ad_id']."\" | `make rinfo|fgrep 'psql -h'` )");
        print "</pre>";
}

if(isset($_REQUEST['transaction'])) {
	$cmd = $_REQUEST["cmd"];

	if(!isset($cmd) || $cmd == "") {
		echo "cmd missing";
		exit();
	}


	print "<pre>"; 
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
        fwrite($fh, "$cmd");
	fpassthru($fh);
	fclose($fh);

        print "</pre>";
}


if (isset($_REQUEST['bconf-disable-list'])) {
	$key = $_REQUEST['bconf-disable-list'];
	$php_key = $key;
	if(substr($php_key,0,2) == '*.')
		$php_key = substr($php_key, 2);
	$value = $_REQUEST['value'];

	print "<pre>";
	for($i = 0 ; $i < 200 ; ++$i) {
		if(bconf_get($BCONF, $php_key.".".$i)) {
			bconf_overwrite("${key}.${i}", $value);
		}
	}

	// Graceful apache, unless option=nograceful given.
	if(!(isset($_REQUEST['option']) && strstr($_REQUEST['option'], 'nograceful'))) {
		system("make -C ../../ apache-regress-graceful");
	}
	print "</pre>";
	print "<pre>Done</pre>";
}

if (isset($_REQUEST['set-ab-code'])) {
	$key = $_REQUEST['key'];
	$valid_for = $_REQUEST['valid_for'];
        print "<pre>";
	
	$value = 'valid_for:' . $valid_for . ',valid_from:"' . date("Y-m-d") . ' 00:00:00",valid_to:"' . date("Y-m-d", mktime(0, 0, 0, date("m")  , date("d") + 1, date("Y"))) . ' 04:00:00"';	
		
	bconf_overwrite($key, urlencode($value));

	if(!(isset($_REQUEST['option']) && strstr($_REQUEST['option'], 'nograceful'))) {
		system("make -C ../../ apache-regress-graceful");
	}
        print "</pre>";
	print "<pre>Done</pre>";
}

if (isset($_REQUEST['bconf-overwrite'])) {
	$key = $_REQUEST['bconf-overwrite'];
	$value = $_REQUEST['value'];
	print "<pre>";
	bconf_overwrite($key, $value);
	if(!(isset($_REQUEST['option']) && strstr($_REQUEST['option'], 'nograceful'))) {
		system("make -C ../../ apache-regress-graceful");
	}
	print "</pre>";
	print "<pre>Done</pre>";
}

if (isset($_REQUEST['trans-reload'])) {
	system("make -C ../../ trans-reload apache-regress-graceful"); 
	print "<pre>Done</pre>";
}

if (isset($_REQUEST['dumpHtml'])) {
	$data = $HTTP_RAW_POST_DATA;
	$count = $_REQUEST['dumpHtml'];
	$fh = fopen("../../regress_final/logs/dumped_html$count.html", "wb");
	fwrite($fh, $data);
	fclose($fh);
}

if (isset($_REQUEST['review'])) {
        print "<pre>";
	$trans_port = bconf_get($BCONF, "*.common.transaction.host.1.port");
	$fh = fsockopen("localhost", $trans_port);
        fwrite($fh, "cmd:review\nad_id:{$_REQUEST['review']}\naction_id:{$_REQUEST['action_id']}\naction:{$_REQUEST['action']}\nfilter_name:autoaccept\nremote_addr:{$_SERVER['REMOTE_ADDR']}\ncommit:1\nend\n");
	fpassthru($fh);
	fclose($fh);
        print "</pre>";
}
/* Generic make function */
if (isset($_REQUEST['make'])) {
        print "<pre>";
	print "make {$_REQUEST['make']}\n";
	system("(cd ../../ && make {$_REQUEST['make']} ) >/dev/null");
	/* We needed to pipe result to /dev/null otherwise it hung */
	print "Done</pre>";
	exit;
}

/* Add IP to bconf block list - CHUS 282 */
if (isset($_REQUEST["blockedip"])) {
	$ip = $_SERVER['REMOTE_ADDR'];
	print "<pre>";
	$cmd = "make -C ../../regress/final/ bconf-overwrite-\\*.\\*.common.blockedip.$ip:$_REQUEST[blockedip]";
	echo "$cmd\n";
	system($cmd);
	$cmd = "cd ../../ && make apache-regress-graceful";
	echo "$cmd\n";
	system($cmd);
	print "Done</pre>";
	exit;
}

if (isset($_REQUEST["redis_get"])) {
	$ip = $_SERVER['REMOTE_ADDR'];
	$redis_session_port = bconf_get($BCONF, "*.common.redisstat.host.rs1.port");
	print "<pre>";
	$cmd = "cd ../../regress/final && make  redis-session-get-rsd1x_validate_".$_REQUEST['redis_get'];
	$exec = shell_exec($cmd);

	$lines = explode("\n",$exec);
	print "Done</pre>";
        print "<pre id='result'>";
        foreach ($lines as $val) {
                if(strpos($val,"make") === false)
                        print $val;
        }                                                                                                                                                                        
        print "</pre>";
	exit;
}
?>
