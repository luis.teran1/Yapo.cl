# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage
from selenium.webdriver.common.by import By
import sys

reload(sys)
sys.setdefaultencoding("latin-1")

class AdViewPage(AccountPage):
    """
    page object used to interact with the ad view page
    """

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(20)
        self.wait = ui.WebDriverWait(self.driver, 10)
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def send_ad_reply(self, list_id, name = None, email = None, phone = None, message = None, send_copy = None):
        self.send_ad_reply_form(list_id, name, email, phone, message, send_copy)
        self.wait.until(lambda driver: u'�Mensaje enviado con �xito!' in driver.find_element_by_css_selector('#ad-reply-success').text)

    def send_ad_reply_form(self, list_id, name = None, email = None, phone = None, message = None, send_copy = None):
        browser = self.driver
        self.go_to_view(list_id = list_id)
        self.wait.until(lambda driver: driver.find_element_by_css_selector('.publisher-info .name') != '')
        if name:
                browser.find_element_by_name('name').clear()
                browser.find_element_by_name('name').send_keys(name)
        if email:
                browser.find_element_by_name('email').clear()
                browser.find_element_by_name('email').send_keys(email)
        if phone:
                browser.find_element_by_name('phone').clear()
                browser.find_element_by_name('phone').send_keys(phone)
        if message:
                browser.find_element_by_name('adreply_body').clear()
                browser.find_element_by_name('adreply_body').send_keys(message)
        if send_copy == True:
                browser.find_element_by_name('cc').click()
        browser.find_element_by_id('send').click()

    def get_reply_form_fields(self):
        """
        Retuns a dict containing field name as key and it's value
        """
        fields = self.driver.find_elements_by_tag_name("input")
        fields += self.driver.find_elements_by_tag_name("textarea")
        return fields

    def go_to_view(self, list_id = None, subject = None):
        browser = self.driver
        if list_id:
            browser.get(self.base_url+"/vi/"+list_id+".htm")
            self.wait.until(lambda driver: driver.find_element_by_id('da_subject'))
        elif subject:
            # search by the subject
            self.go_to_region("chile")
            browser.find_element_by_id('searchtext').clear()
            browser.find_element_by_id('searchtext').send_keys(subject)
            browser.find_element_by_id('searchbutton').click()
            # loop over the ads found
            ads = browser.find_elements_by_class_name('ad')
            item = None
            for ad in ads:
                # get the subject
                item = ad.find_element_by_xpath('td[3]/a')
                if item.text == subject:
                    item .click()
                    break
            return item

    def go_to_view_and_bump(self, list_id = None,  subject = None):
        self.go_to_view(list_id, subject)
        self.driver.find_element_by_link_text("Subir Ahora").click()

    def go_to_view_and_edit(self, list_id = None, passwd = None, subject = None):
        self.go_to_view(list_id, subject)
        self.driver.find_element_by_id("ad_admin_link_editar").click()
        if passwd:
            self.driver.find_element_by_id("passwd").clear()
            self.driver.find_element_by_id("passwd").send_keys(passwd)
            self.driver.find_element_by_name("continue").click()

    def go_to_view_and_delete(self, list_id = None, subject = None, delete_reason = None, passwd = None, buy_bump = None):
        self.go_to_view(list_id, subject)
        self.driver.find_element_by_id("ad_admin_link_eliminar").click()
        self.wait.until(lambda driver: driver.find_element_by_class_name('reasons-tooltip'))

        tooltip = self.driver.find_element_by_class_name('reasons-tooltip')
        if delete_reason:
            self.driver.find_element_by_id("label"+delete_reason).click()

        if delete_reason == '1':
            self.driver.find_element_by_css_selector('ins[data-radio=r3]').click()

        if delete_reason and passwd:
            if self.is_element_present(By.ID,"passwd"):
                tooltip.find_element_by_id("passwd").clear()
                tooltip.find_element_by_id("passwd").send_keys(passwd)

            tooltip.find_element_by_id("delete"+delete_reason).click()
        else:
            if delete_reason and buy_bump:
                tooltip.find_element_by_link_text("Comprar").click()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def getPhoneNumberAd(self):
        return self.driver.find_element_by_class_name('AdPhonenum')
