# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage

class EditPage(AccountPage):
    """
    Page object for the edit account page
    """
    field_names = ['name', 'rut', 'phone', 'region', 'commune', 'address']

    def __init__(self, driver):
        AccountPage.__init__(self,driver)

    def go_to_edit(self, email, password):
        self.login(email, password)
        self.go_to_dashboard()
        self.driver.find_element_by_partial_link_text(u'Perfil').click()

    def set_text_field_value(self, field_name, value):
        field = self.driver.find_element_by_name(field_name)
        field.clear()
        field.send_keys(value)

    def field_has_ok_mark(self, field_name):
        field = self.driver.find_element_by_name(field_name)
        field.parent.find_element_by_class_name('success').is_displayed()

    def get_field_error_message(self, field_name):
        field = self.driver.find_element_by_name(field_name)
        message = field.parent.find_element_by_id('err_'+field_name)
        if message.is_displayed():
            return message.text
        return ''

    def get_ok_mark_count(self):
        ok_marks = self.driver.find_elements_by_class_name('icon-form-ok')
        return len(ok_marks)

    def submit_edit_form(self):
        self.driver.find_element_by_name('submit').click()

    def set_name(self, value):
        self.set_text_field_value('name', value)

