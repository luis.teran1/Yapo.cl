# -*- coding: latin-1 -*
import conf
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import ui

class EditMobile():
    """
    Page Object for mobile edit ad
    """

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_MOBILE_PORT

    def warp_to_refused_edition(self, refused_id):
        self.driver.get(self.base_url + "/editar-aviso/?id=R{id}".format(id=refused_id))

    def get_value(self, field):
            element = self.driver.find_element_by_id(field)
            return element.get_attribute("value").strip()

    def is_text_present_on_refuse_box(self, text):
        try:
            body = self.driver.find_element_by_css_selector(".warning_msg.attention.ad_refused") # find body tag element
        except NoSuchElementException, e:
            return False
        return text in body.text # check if the text is in body's text

    def go_to_edit_page(self, list_id = None, password = "11111", caller = "15"):
        """
           Goes to edit the requested ad
        """
        browser = self.driver
        if(list_id == None):
            self.go_to_region("region metropolitana")
            browser.find_element_by_class_name('title').click()
        else:
            url = "https://%s:%s/vi/%s.htm?ca=%s_s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT, list_id, caller)
            browser.get(url)

        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: browser.find_element_by_id('ad_admin_link_editar'))
        browser.find_element_by_id('ad_admin_link_editar').click()
        browser.find_element_by_id('password_input').clear()
        browser.find_element_by_id('password_input').send_keys(password)
        browser.find_element_by_class_name('btn').click()

    def submit_form(self):
        self.driver.find_element_by_id("newad_submit").click()

    def change_input_values(self, **kwargs):
            for field, new_value in kwargs.iteritems():
                try:
                    element = self.driver.find_element_by_id(field)
                    element.clear()
                    element.send_keys(new_value)
                except NoSuchElementException, e:
                    continue
