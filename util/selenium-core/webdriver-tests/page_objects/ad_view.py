from selenium.webdriver.common.by import By
from page_objects.ad_edit import AdEditPasswordVerificationPage
import conf
import sunbro

class AdViewPage(sunbro.Page):
    """
    Page object used to interact with the ad view page
    """
    edit_link = sunbro.FindByID('ad_admin_link_editar')
    publish_date = sunbro.FindElement(By.CSS_SELECTOR, '.date time')

    def __init__(self, driver, list_id = None):
        super(AdViewPage, self).__init__(driver)

        # if we want to go directly to the ad view page, we go there
        # but the excepted behavior is to already be there
        super(AdViewPage, self).__init__(driver)
        if list_id:
            self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
            self._driver.get(self.base_url+"/vi/"+list_id+".htm")
        super(AdViewPage, self).__init__(driver)

    def go_to_edit(self):
        """ This method clicks on the edit link, and returns an AdEditPasswordVerificationPage object
            It is supposed to be used by a non logged user
        """
        self.edit_link.click()
        return AdEditPasswordVerificationPage(self._driver)

    def get_date_ad(self):
        return self.publish_date.text
