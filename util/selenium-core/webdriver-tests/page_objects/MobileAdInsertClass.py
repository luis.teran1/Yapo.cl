# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.support.select import Select
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage

class MobileAdInsert(AccountPage):
    def insert(self, region, commune, category, **kwargs):
        select_region = Select(self.driver.find_element_by_id('region'))
        select_region.select_by_value(region)
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: browser.find_element_by_css_selector('#communes option[value="' + commune + '"]'))
        select_communes = Select(self.driver.find_element_by_id('communes'))
        select_communes.select_by_value(commune)
        select_category = self.driver.find_element_by_id('category')
        for option in select_category.find_elements_by_tag_name('option'):
            if option.get_attribute("value") == category:
                option.click()
        for field,value in kwargs.items():
            input = self.driver.find_element_by_id(field)
            input.clear()
            input.send_keys(value)
        self.driver.find_element_by_css_selector('#terms+ins').click()
        self.driver.find_element_by_id('newad_submit').click()

    def go_to_adinsert(self):
        self.driver.find_element_by_id('acc_page_da_insert').click()
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: self.driver.find_element_by_id('newad_form'))

    def warp_to_mai(self):
        self.driver.get("https://{host}:{port}/mai".format(host=conf.REGRESS_HOST, port=conf.REGRESS_MOBILE_PORT))
