# -*- coding: latin-1 -*-
import sunbro
from sunbro import By
from selenium.webdriver.support.wait import WebDriverWait
import utils
import time
from selenium.common.exceptions import NoSuchElementException

class DeletePage(sunbro.Page):
    """Page object from mobile delete"""

    reasons = sunbro.FindElements(By.CSS_SELECTOR, "#accordion h3")
    active_reason_div = sunbro.FindElement(By.CSS_SELECTOR, '#accordion div[style="display: block;"]')
    password = sunbro.FindElement(By.CSS_SELECTOR, '#accordion div[style="display: block;"] input[name="passwd"]')
    sell_time_options = sunbro.FindElements(By.CSS_SELECTOR,  '#accordion div[style="display: block;"] .slider-level')
    submit = sunbro.FindElement(By.CSS_SELECTOR,  '#accordion div[style="display: block;"] a[name="continue"]')
    modal_message =  sunbro.FindElement(By.CSS_SELECTOR,  '.pm-content div')
    validations = sunbro.FindAllByClass('validation_msg', within='active_reason_div')
    user_text = sunbro.FindByName('user_text', within="active_reason_div")
    help_form = sunbro.FindElement(By.ID, 'help-payment')
    p1 = sunbro.FindElement(By.ID, 'pass1')
    p2 = sunbro.FindElement(By.ID, 'pass2')
    p3 = sunbro.FindElement(By.ID, 'pass3')
    p4 = sunbro.FindElement(By.ID, 'pass4')
    p5 = sunbro.FindElement(By.ID, 'pass5')
    p6 = sunbro.FindElement(By.ID, 'pass6')
    d1 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="1"] [name="continue"]')
    d2 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="2"] [name="continue"]')
    d3 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="3"] [name="continue"]')
    d4 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="4"] [name="continue"]')
    d5 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="5"] [name="continue"]')
    d6 = sunbro.FindElement(By.CSS_SELECTOR,'[data-reason="6"] [name="continue"]')
    user_text2 = sunbro.FindElement(By.ID,'dlv2')
    user_text6 = sunbro.FindElement(By.ID,'dlv6')
    logout = sunbro.FindElement(By.ID,'acc_page_close_session')

    def go(self, list_id):
        self._driver.get(utils.MOBILE_URL + "/delete_reasons?id=" + str(list_id))

    def delete(self, reason_id, password = None,user_text=None):
        reason = getattr(self, 'reasons')[reason_id - 1]
        reason.click()

        if user_text:
           user_text_box = getattr(self,'user_text%s'%(reason_id))
           user_text_box.clear()
           user_text_box.send_keys(user_text)

        if password:
            password_box = getattr(self, 'p'+str(reason_id))
            password_box.clear()
            password_box.send_keys(password)

        submit = getattr(self, 'd'+str(reason_id))
        submit.click()
        return ModalWindowDelete(self._driver)

class ModalWindowDelete(sunbro.Page):
      modal_window     = sunbro.FindElement(By.CLASS_NAME,'modal-window')
      btn_go_to_search = sunbro.FindByPartialLinkText(u'Ir a b�squeda')

      def go_to_search(self):
          print(self.btn_go_to_search)
          self.btn_go_to_search.click()
          return ListPage(self._driver)

class HomePage(sunbro.Page):
    """Page object from Home Mobile"""

    region_metro = sunbro.FindElements(By.LINK_TEXT, "Regi�n Metropolitana")
    footer_rules = sunbro.FindElements(By.LINK_TEXT, "Reglas")
    footer_tips = sunbro.FindElements(By.LINK_TEXT, "Consejos de Seguridad")
    footer_terms = sunbro.FindElements(By.LINK_TEXT, "T�rminos y Condiciones")
    footer_full_web = sunbro.FindElements(By.LINK_TEXT, "Versi�n web completa")
    header_account_login = sunbro.FindElement(By.ID, "acc_bar_login")
    header_ad_insert = sunbro.FindElement(By.ID, "acc_page_da_insert")
    header_account_username = sunbro.FindElement(By.ID, "acc_bar_user_name")
    header_account_logout = sunbro.FindElement(By.ID, "acc_bar_close_session")

    def go(self):
        self._driver.get(utils.MOBILE_URL)

class ListPage(sunbro.Page):
    """Page object from list Mobile"""
    home = sunbro.FindElement(By.CSS_SELECTOR, ".logo")
    btn_insertad = sunbro.FindElement(By.CSS_SELECTOR, ".bt_inserir-anuncio")
    btn_region = sunbro.FindElement(By.CSS_SELECTOR, ".bt_localizacao_state")
    btn_myads = sunbro.FindElement(By.CSS_SELECTOR, ".bt_meus-anuncios")
    field_search = sunbro.FindElement(By.ID, "keyword")
    btn_search = sunbro.FindElement(By.ID,"search")
    btn_filters = sunbro.FindElement(By.ID,"filters")
    select_categories = sunbro.FindElement(By.ID,"selectbox_categories")
    ads_by_subject = sunbro.FindElements(By.CSS_SELECTOR,".list_ads.ad.info.title")

    def go(self, region = None):
        if region is None:
            self._driver.get(utils.MOBILE_URL + "/li")
        else:
            self._driver.get(utils.MOBILE_URL + "/" +region)

    def search_for_ad(self, search_text):
        self.field_search.clear()
        self.field_search.send_keys(search_text)
        self.btn_search.click()

    def is_ad_on_page(self, subject):
        try: self._driver.find_element_by_link_text(subject)
        except NoSuchElementException, e: return False
        return True

class ViewPage(sunbro.Page):
    btns_back = sunbro.FindElements(By.CSS_SELECTOR, ".bt_back-listing")
    btns_adreply = sunbro.FindElements(By.CSS_SELECTOR, "bt_reply")
    btns_callphone = sunbro.FindElements(By.CSS_SELECTOR, "bt_callphone")
    btn_act_bump = sunbro.FindElement(By.CSS_SELECTOR, "bt_bump")
    btn_act_edit = sunbro.FindElement(By.CSS_SELECTOR, "bt_edit")
    btn_act_delete = sunbro.FindElement(By.CSS_SELECTOR, "bt_delete")

    def go(self, list_id):
        self._driver.get(utils.MOBILE_URL + "/vi/" + str(list_id) + ".htm")


class PaymentInvoiceForm(sunbro.Page):
    name = sunbro.FindByCSS('#id_name')
    lob = sunbro.FindByCSS('#id_lob')
    rut = sunbro.FindByCSS('#id_rut')
    address = sunbro.FindByCSS('#id_address')
    region = sunbro.FindByCSS('#id_region')
    commune = sunbro.FindByCSS('#id_communes')
    contact = sunbro.FindByCSS('#id_contact')
    submit = sunbro.FindByCSS('#submit-invoice')
    errors = sunbro.FindAllByCSS('.error')
    email = sunbro.FindByCSS('input[name="email"]')

class PaymentPage(sunbro.Page):
    btns_back = sunbro.FindElements(By.CSS_SELECTOR, ".bt_back-listing")
    rds_bill = sunbro.FindElement(By.ID,"radio-bill")
    rds_invoice = sunbro.FindElement(By.ID,"radio-invoice")
    btn_submit = sunbro.FindElement(By.ID,"submit-bill")
    form_invoice = sunbro.FindByCSS('#payment_2')
    lnk_error_more_info = sunbro.FindElement(By.ID,"error-more-info")
    txts_error_more_info = sunbro.FindElements(By.CSS_SELECTOR,"error-text")
    form_bill = sunbro.FindByCSS("form#payment_1")
    form_invoice = sunbro.FindByCSS("form#payment_2")
    region_select = sunbro.FindByCSS('select#id_region')
    region_options = sunbro.FindAllByCSS("#id_region option")
    commune_select = sunbro.FindByCSS('select#id_communes')
    commune_options = sunbro.FindAllByCSS("#id_communes option")
    modify_email_link = sunbro.FindByCSS('a.edit-email')
    email_input = sunbro.FindByCSS('form[name="payment"] input[name="email"]')
    link_desktop = sunbro.FindByCSS('.link_fullversion')

    def go(self, list_id, prod = "1" , ftype = "2"):
        self._driver.get(utils.SECURE_URL + "/pagos?id=" + str(list_id) + "&prod="+str(prod)+"&ftype="+str(ftype))
        self._driver.get('{0}/pagos?id={1}&prod={2}&ftype={3}'.format(utils.SECURE_URL, list_id, prod, ftype))

    def get_invoice_form(self):
        return PaymentInvoiceForm(self.form_invoice)

    def select_invoice(self):
        self.rds_invoice.find_element_by_xpath('../ins').click()

class PaysimPage(sunbro.Page):
    btn_accept = sunbro.FindElement(By.NAME,"tbk_ok")
    btn_refuse = sunbro.FindElement(By.NAME,"tbk_cancel")
    btn_cancel = sunbro.FindElement(By.NAME,"tbk_voided")
    btn_next = sunbro.FindElement(By.NAME,"tbk_next")

class LoginPage(sunbro.Page):
    email = sunbro.FindElement(By.ID, "email_input")
    password = sunbro.FindElement(By.ID, "password_input")
    forgot_passwd = sunbro.FindElement(By.ID, "forgot_passwd_link")
    submit = sunbro.FindElement(By.ID, "submit_button")
    dont_close = sunbro.FindElement(By.ID, "logindontclose")
    create_account = sunbro.FindElement(By.ID, "login_create_account")
    myads_email = sunbro.FindElement(By.ID, "myads_email")
    myads_submit = sunbro.FindElement(By.ID, "myads_submit")
    error = sunbro.FindElement(By.ID, "err_form")
    myads_error = sunbro.FindElement(By.ID, "err_myads_email")
    myads_ok_message = sunbro.FindElement(By.CSS_SELECTOR, ".legend.confirmation")

    def login(self, email, password):
        """
        Login the user using the top login form in almost every page
        """
        self.email.send_keys(email)
        self.password.send_keys(password)
        self.submit.click()
        wait = WebDriverWait(self, 10)
        wait.until(lambda self: 'dashboard' in self._driver.current_url)
        return self

    def go(self):
        self._driver.get(utils.SECURE_URL + "/login")


class RegisterPage(sunbro.Page):
    is_company_and_terms_n_conditions = sunbro.FindElements(By.CSS_SELECTOR, ".iCheck-helper")
    is_company_p = sunbro.FindElement(By.CSS_SELECTOR, "#is_company_p")
    is_company_c = sunbro.FindElement(By.CSS_SELECTOR, "#is_company_c")
    is_company_p_ins = sunbro.FindElement(By.CSS_SELECTOR, "#is_company_p + ins")
    is_company_c_ins = sunbro.FindElement(By.CSS_SELECTOR, "#is_company_c + ins")
    name = sunbro.FindElement(By.ID, "account_name")
    name_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_name + i")
    rut = sunbro.FindElement(By.ID, "account_rut")
    rut_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_rut + i")
    phone = sunbro.FindElement(By.ID, "account_phone")
    phone_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_phone + i")
    region = sunbro.FindElement(By.ID, "account_region")
    email = sunbro.FindElement(By.ID, "account_email")
    email_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_email + i")
    password = sunbro.FindElement(By.ID, "account_password")
    password_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_password + i")
    password_verify = sunbro.FindElement(By.ID, "account_password_verify")
    password_verify_ok = sunbro.FindElement(By.CSS_SELECTOR, "#account_password_verify + i")
    accept_conditions = sunbro.FindElement(By.ID, "accept_conditions")
    accept_conditions_ins = sunbro.FindElement(By.CSS_SELECTOR, "#accept_conditions + ins")
    confirm_btn = sunbro.FindElement(By.ID, "confirm_btn")
    err_is_company = sunbro.FindElement(By.ID, "err_is_company")
    err_name = sunbro.FindElement(By.ID, "err_name")
    err_rut = sunbro.FindElement(By.ID, "err_rut")
    err_phone = sunbro.FindElement(By.ID, "err_phone")
    err_region = sunbro.FindElement(By.ID, "err_region")
    err_email = sunbro.FindElement(By.ID, "err_email")
    err_password = sunbro.FindElement(By.ID, "err_password")
    err_password_verify = sunbro.FindElement(By.ID, "err_password_verify")
    err_accept_conditions = sunbro.FindElement(By.ID, "err_accept_conditions")
    ok_message = sunbro.FindElement(By.CSS_SELECTOR, ".create-account-confirm")

    def go(self):
        self._driver.get(utils.SECURE_URL + "/cuenta")
    
    def getOKIcons(self):
        return [self.name_ok, self.rut_ok, self.phone_ok, self.email_ok, self.password_ok, self.password_verify_ok]

    def getErrorMessages(self):
        return [self.err_is_company, self.err_name, self.err_rut, self.err_phone, self.err_region, self.err_email, self.err_password, self.err_password_verify, self.err_accept_conditions]

    def assertErrorMessages(self, error_messages):
        ''' Assert the visible error messages '''
        messages = self.getErrorMessages()
        for message in messages:
            if message in error_messages:
                if (not message.is_displayed()):
                    print 'the error message {0} should be visible'.format(message.get_attribute('id'))
                    return False
            else:
                if (message.is_displayed()):
                    print 'the error message {0} should NOT be visible'.format(message.get_attribute('id'))
                    return False
        return True

    def assertOKIcons(self, visible_icons):
        ''' Assert the visible validatioin icons '''
        icons = self.getOKIcons()
        for icon in icons:
            if icon in visible_icons:
                if (not icon.is_displayed()):
                    print 'the icon for {0} should be visible'.format(icon.find_element_by_xpath('../input').get_attribute('id'))
                    return False
            else:
                if (icon.is_displayed()):
                    print 'the icon for {0} should NOT be visible'.format(icon.find_element_by_xpath('../input').get_attribute('id'))
                    return False
        return True


class MyProfilePage(RegisterPage):
    profile_title = sunbro.FindElement(By.CSS_SELECTOR, ".edit-account-title h2")
    address = sunbro.FindElement(By.ID, "account_address")
    update_message = sunbro.FindElement(By.CSS_SELECTOR, ".update-success")

    def go(self):
        self._driver.get(utils.SECURE_URL + "/cuenta/edit")

    def getOKIcons(self):
        return [self.name_ok, self.rut_ok, self.phone_ok, self.password_ok, self.password_verify_ok]

    def getErrorMessages(self):
        return [self.err_is_company, self.err_name, self.err_rut, self.err_phone, self.err_region, self.err_password, self.err_password_verify]



class DashboardPage(sunbro.Page):
    inactive_ads = sunbro.FindElements(By.CSS_SELECTOR, ".single-da.inactive")
    active_ads = sunbro.FindElements(By.CSS_SELECTOR, ".single-da:not(.inactive)")
    subjects = sunbro.FindElements(By.CSS_SELECTOR, ".da-subject")
    ad_insert = sunbro.FindElement(By.ID, "acc_page_da_insert")
    edit_profile = sunbro.FindElement(By.ID, "acc_page_user_name")
    logout = sunbro.FindElement(By.ID, "acc_page_close_session")
    ads_view = sunbro.FindElement(By.CSS_SELECTOR, ".dashboard-options li:nth-child(1)")
    my_profile = sunbro.FindElement(By.CSS_SELECTOR, ".dashboard-options li:nth-child(2)")
    edit_buttons = sunbro.FindElements(By.CSS_SELECTOR, ".da-options > #da-edit")
    delete_buttons = sunbro.FindElements(By.CSS_SELECTOR, ".da-options > .da-delete")
    search = sunbro.FindElement(By.ID, "search-ads")
    clear_search = sunbro.FindElement(By.ID, "clear-search")
    background = sunbro.FindElement(By.CSS_SELECTOR, "body")
    def go(self):
        self._driver.get(utils.SECURE_URL + "/dashboard")


class AdEditedSuccessfully(sunbro.Page):
    """Little page object for page mai/success-edit"""

    # Text
    text_success_message = sunbro.FindByCSS("div.success-info p.text-legend")
    text_create_account_success = sunbro.FindByCSS(".success-desc")

    def success_message(self, message):
        assert 'Tu aviso "{0}" ser� revisado'.format(message) in self.text_success_message.text


class AdInsert(sunbro.Page):
    """Page object for ad insert page"""

    # Selects
    select_region = sunbro.FindByID("region")
    select_category= sunbro.FindByID("category")

    # Buttons
    button_publish_now = sunbro.FindByID("newad_submit")
    button_modal_ok = sunbro.FindElement(By.CSS_SELECTOR, ".modal button")

    # Inputs
    input_subject = sunbro.FindByID("subject")
    input_body = sunbro.FindByID("body")
    input_price = sunbro.FindByID("price")
    input_name = sunbro.FindByID("name")
    input_email = sunbro.FindByID("email")
    input_phone = sunbro.FindByID("phone")
    input_password = sunbro.FindByID("passwd")
    input_password_confirm = sunbro.FindByID("passwd_ver")

    # Radio Buttons
    radio_vendo = sunbro.FindByID("label_st_s")
    radio_busco = sunbro.FindByID("label_st_k")
    radio_persona = sunbro.FindByID("p_ad")
    radio_profesional = sunbro.FindByID("c_ad")

    # Check boxes
    check_box_hide_phone = sunbro.FindByID("phone_hidden")
    check_box_create_account = sunbro.FindByID("icheck_create_account")
    check_box_accept_terms = sunbro.FindByID("terms")

    # Components
    box_ad_information = sunbro.FindByID("ad_information")
    box_upload_images = sunbro.FindByCSS(".single_upload")

    # Error messages
    err_region = sunbro.FindByID("param_region")
    err_category = sunbro.FindByID("param_category")
    err_subject = sunbro.FindByID("param_subject")
    err_body = sunbro.FindByID("param_body")
    err_price = sunbro.FindByID("param_price")
    err_name = sunbro.FindByID("param_name")
    err_rut = sunbro.FindByID("param_rut")
    err_email = sunbro.FindByID("param_email")
    err_phone = sunbro.FindByID("param_phone")
    err_phone_hidden = sunbro.FindByID("param_phone_hidden")
    err_passwd = sunbro.FindByID("param_passwd")
    msg_modal_ok = sunbro.FindElement(By.CSS_SELECTOR, ".modal p")

    def go(self):
        self._driver.get(utils.MOBILE_URL + "/mai")

    def is_check_image_box_present(self):
        print self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[1]/input[1]").get_attribute('outerHTML')
        print self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[2]/input[1]").get_attribute('outerHTML')
        print self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[3]/input[1]").get_attribute('outerHTML')
        displayed = self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[1]/input[1]").is_displayed()
        displayed = displayed and self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[2]/input[1]").is_displayed()
        displayed = displayed and self._driver.find_element_by_xpath("//*[@id=\"mpu_pics\"]/div[3]/input[1]").is_displayed()
        print displayed
        return displayed

    # TODO Add locators and complement the method for categories with ad_params
    #      Also consider the case for 'Profesional'. Rut not considered
    def insert_ad_unlogged(self, wd, **kwargs):
        params = {'region': "15",
                  'category': "5020",
                  'subject': "Foo",
                  'body': "Foo Bar Bazz Qux",
                  'price': "123123",
                  'role': "Persona",
                  'name': "Foobar",
                  'email': "foo@bar.com",
                  'phone': "123123",
                  'hide_phone': False,
                  'password': "123123123",
                  'password_confirm': "123123123",
                  'create_account': False,
                  'password_confirm': "123123123",
                  'accept_terms': True, }
        params.update(kwargs)

        self.go_ai(wd)
        common = HeaderElements(wd)
        self.fill_form(select_region=params['region'],
                       select_category=params['category'],
                       input_subject=params['subject'],
                       input_body=params['body'],
                       input_price=params['price'],
                       radio_persona=params['role'] == 'Persona',
                       radio_profesional=params['role'] == 'Profesional',
                       input_name=params['name'],
                       input_email=params['email'],
                       input_phone=params['phone'],
                       check_box_hide_phone=params['hide_phone'],
                       input_password=params['password'],
                       input_password_confirm=params['password_confirm'],
                       check_box_create_account=params['create_account'],
                       check_box_accept_terms=params['accept_terms'], )
        self.button_publish_now.click()
        success_page = AdInsertedSuccessfuly(wd)
        success_page.success_message(params['subject'])

    def insert_ad_logged(self, wd, **kwargs):
        header = HeaderElements(wd)
        if not header.is_logged(wd):
            raise BaseException("Need to be logged in")

        params = {'region': "15",
                  'category': "5020", # Default: Muebles
                  'subject': "Foo",
                  'body': "Foo Bar Bazz Qux",
                  'price': "123123",
                  'role': "Persona",
                  'name': "Foobar",
                  'email': "foo@bar.com",
                  'phone': "123123",
                  'hide_phone': False,
                  'password': "123123123",
                  'accept_terms': True, }
        params.update(kwargs)

        self.go_ai(wd)
        common = HeaderElements(wd)
        self.fill_form(select_region=params['region'],
                       select_category=params['category'],
                       input_subject=params['subject'],
                       input_body=params['body'],
                       input_price=params['price'],
                       radio_persona=params['role'] == 'Persona',
                       radio_profesional=params['role'] == 'Profesional',
                       input_name=params['name'],
                       input_phone=params['phone'],
                       check_box_hide_phone=params['hide_phone'],
                       check_box_accept_terms=params['accept_terms'], )
        self.button_publish_now.click()
        success_page = AdInsertedSuccessfuly(wd)
        success_page.success_message(params['subject'])

    def getErrorMessages(self):
        return [self.err_region, self.err_category, self.err_subject, self.err_body, self.err_price, self.err_name, self.err_rut, self.err_email, self.err_phone, self.err_phone_hidden, self.err_passwd]

    def assertErrorMessages(self, error_messages):
        ''' Assert the visible error messages '''
        messages = self.getErrorMessages()
        for message in messages:
            if message in error_messages:
                if (not message.is_displayed()):
                    print 'the error message {0} should be visible'.format(message.get_attribute('id'))
                    return False
            else:
                if (message.is_displayed()):
                    print 'the error message {0} should NOT be visible'.format(message.get_attribute('id'))
                    return False
        return True
