# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage

class ResetPasswordPage(AccountPage):
    """
    Page object for the forgot password page
    """

    def __init__(self, driver):
        self.driver = driver
        AccountPage.__init__(self, driver)

    def create_2accounts(self):
        self.create(name = 'Perico uno',
                    phone = '123123',
                    email = 'perico1@yapo.cl',
                    password = '123123123',
                    password_verify = '123123123'
                   ) 
        self.create(name = 'Perico dos',
                    phone = '123123',
                    email = 'perico2@yapo.cl',
                    password = '123123123',
                    password_verify = '123123123'
                   )


