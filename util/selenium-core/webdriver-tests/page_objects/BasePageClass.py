# -*- coding: latin-1 -*-
import unittest, subprocess
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri

class BasePage(object):
    """
    Base page for the page objects
    """
    base_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT)
    secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_region (self, key):
        """
        Go to ad list page from a specified region
        """
        self.driver.get(self.base_url + "/" + key.replace(' ', '_') + "/")


    def go_to_region_los_lagos (self):
        """
        Go to region los lagos ad list page
        """
        self.go_to_region("los lagos")


    def go_to_region_metropolitana (self):
        """
        Go to region metropolitana ad list page
        """
        self.go_to_region("region metropolitana")

    def get_mail(self):
        mail_path = os.path.join(conf.REGRESS_FINALDIR, 'logs', 'mail.txt')
        f = open(mail_path)
        t = f.read()
        decoded_mail = quopri.decodestring(t)
        return decoded_mail

    def decode_mail_log_to_text(self):
        os.system('make -C ' + conf.TOPDIR + '/regress/final decode-mail' )

    def get_link_from_mail_log(self, text_to_find):
        self.decode_mail_log_to_text()
        mydriver = webdriver.Firefox()
        mydriver.get (self.base_url + "/selenium-core/getMail.php")
        os.system('cp ' + conf.REGRESS_FINALDIR + '/logs/mail.txt.old ' + conf.REGRESS_FINALDIR + '/logs/mail.txt' )
        link = mydriver.find_element_by_partial_link_text(text_to_find).text
        mydriver.quit()
        return link

    def bconf_get_by_key(self, key):
        """return a bconf value from a key """
        if not key.startswith('*.*.'):
            key = '*.*.' + key
        key = 'conf:' + key
        value = self.bconf_get_by_filter(key)
        return value[len(key) + 1:len(value) - 1]

    def bconf_get_by_filter(self, filter):
        """return filtered bconf values"""
        return os.popen('printf "cmd:bconf\ncommit:1\nend\n" | nc localhost ' + conf.REGRESS_TRANS_PORT + ' | grep "' + filter + '"').read()


    def bconf_overwrite(self, key, value):
        """Overwrite a Bconf key"""
        os.system('make --quiet -C ' + conf.TOPDIR + '/regress/final bconf-overwrite-' + key + ':' + value)

    def bconf_get_value(self, key):
        """Overwrite a Bconf key"""
        return os.system('printf "cmd:bconf\ncommit:1\nend\n" | nc localhost ' +conf.REGRESS_TRANS_PORT+ '| grep '+ key +' | cut -d= -f2')


    def bconf_overwrite_restart_apache(self):
        """Graceful Apache Restart"""
        os.system('make --quiet -C ' + conf.TOPDIR + ' apache-regress-graceful > /dev/null 2>&1')

    def reload_bconf(self):
        """
        make rebuild asearch
        """
        subprocess.call(["make", "--quiet", "-C", conf.TOPDIR, "rb"], stdout=open(os.devnull, 'wb'))

    def rebuild_index_full(self):
        """
        make rebuild index full
        """
        os.system("make rebuild-index-full")

    def rebuild_index(self):
        """
        make rebuild index
        """
        os.system("make rebuild-index")

    def rebuild_asearch(self):
        """
        make rebuild asearch
        """
        subprocess.call(["make", "--quiet", "-C", conf.TOPDIR, "rebuild-asearch"], stdout=open(os.devnull, 'wb'))

    def redis_send_command(self, command, port, db=0):
        """
        send a command to redis server
        """
        return os.popen('echo "%s" | %s -p %s -n %s' % (command, conf.REGRESS_REDIS_DIR, port, db)).read()

    def restore_db_snap(self,snap_name):
        """
        make db-restore-snap
        """
        snap_name = "db-lrestore-"+snap_name
        subprocess.call(["make", "--quiet", "-C", conf.TOPDIR + "/regress/final", snap_name], stdout=open(os.devnull, 'wb'))

    def send_mail(self,mail_type,ad_id):
        """
        call the trans admail
        """
        os.system('printf "cmd:admail\nmail_type:'+mail_type+'\nad_id:'+ad_id+'\ncommit:1\nend\n" | nc localhost '+conf.REGRESS_TRANS_PORT)

    def run_database_query(self, query):
        """
        run a query in the database
        """
        dbpath = os.popen("make -C " + conf.TOPDIR + " rinfo|fgrep 'psql -h' | awk '{ print $3 }'").read().rstrip()
        data = os.popen("echo '" + query + "' | psql -t -h " + dbpath + " blocketdb").read()
        return data.splitlines()







