from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import conf
import sunbro
import time

class AdListPage(sunbro.Page):
    """
    Page object used to interact with the ad view page
    """

    search_box    = sunbro.FindElement(By.ID, 'searchtext')
    search_button = sunbro.FindElement(By.ID, 'searchbutton')
    category_selector = sunbro.FindElement(By.ID, 'catgroup')
    filter_car_brand_selector = sunbro.FindElement(By.ID, 'brand_br')
    filter_car_model_selector = sunbro.FindElement(By.ID, 'model_mo')
    filter_cars_min_price_selector = sunbro.FindElement(By.ID, 'ps_1')
    filter_cars_max_price_selector = sunbro.FindElement(By.ID, 'pe_1')
    filter_monthly_rent_selector = sunbro.FindElement(By.ID, 'monthly_rent_mre')
    filter_cars_reg_min_year_selector = sunbro.FindElement(By.ID, 'regdate_rs')
    filter_cars_reg_max_year_selector = sunbro.FindElement(By.ID, 'regdate_re')
    filter_cars_mileage_min_selector = sunbro.FindElement(By.ID, 'mileage_ms')
    filter_cars_mileage_max_selector = sunbro.FindElement(By.ID, 'mileage_me')
    filter_fuel_selector = sunbro.FindElement(By.ID, 'fuel_fu')
    filter_max_monthly_rent_selector = sunbro.FindElement(By.ID, 'max_monthly_rent_mre')
    filter_min_rooms_selector = sunbro.FindElement(By.ID, 'rooms_ros')
    filter_max_rooms_selector = sunbro.FindElement(By.ID, 'rooms_roe')
    filter_min_beds_selector = sunbro.FindElement(By.ID, 'beds_bs')
    filter_max_beds_selector = sunbro.FindElement(By.ID, 'beds_be')
    filter_car_type_selector = sunbro.FindElement(By.ID, 'cartype_ctp')
    filter_car_gearbox_selector = sunbro.FindElement(By.ID, 'gearbox_gb')
    base_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT)

    def search_for_ad(self, search_text):
        self.search_box.clear()
        self.search_box.send_keys(search_text)
        self.search_button.click()

    def is_ad_on_page(self, subject):
        try: self._driver.find_element_by_link_text(subject)
        except NoSuchElementException, e: return False
        return True

    def go_to_ad(self, list_id=None, subject=None):
        if list_id:
            self._driver.find_element_by_id(unicode(list_id)).find_element_by_tag_name("a").click()
        elif subject:
            self._driver.find_element_by_partial_link_text(unicode(subject)).click()

    def select_category_by_value(self, value):
        options = self._driver.find_elements_by_css_selector('#catgroup option')
        option = None
        for index in range(len(options)):
            if (options[index].get_attribute('value') == value):
                option = options[index]
                break
        if option is not None: 
            option.click()
        else:
            raise Exception('option not found!')

    def search_ads_by_text(self, text):
        """ Search for ads in the listing page """
        self._driver.find_element_by_id('searchtext').send_keys(text)
        self._driver.find_element_by_id('searchbutton').click()

    def check_few_ads_found_message(self):
        """ check if message 'Fueron encontrador menos de 30 avisos' is present """
        return (self._driver.find_element_by_css_selector('.redwarning_failover').text == u'Fueron encontrados menos de 30 avisos.')

    def check_ad_not_found_error_message_is_displayed(self):
        """ check if message 'ad not found' is present """
        message = self._driver.find_element_by_css_selector('.redwarning_failover')
        return (message.text == u'Resultado no encontrado.') and message.is_displayed()

    def check_not_ads_found_message(self):
        """ check if message 'ads not found' is present """
        return (self._driver.find_element_by_css_selector('.TabContainer .TabContainer tbody tr:nth-child(2) td h1').text == u'No se encontraron entradas')

    def go_to_region (self, key):
        """ Go to ad list page from a specified region """
        self._driver.get(self.base_url + "/" + key.replace(' ', '_') + "/")

    def go_to_region_los_lagos (self):
        """ Go to region los lagos ad list page """
        self.go_to_region("los lagos")

    def go_to_region_metropolitana (self):
        """ Go to region metropolitana ad list page """
        self.go_to_region("region metropolitana")
