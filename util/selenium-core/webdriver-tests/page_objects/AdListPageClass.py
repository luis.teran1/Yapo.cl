# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage

class AdListPage(AccountPage):
    """
    Page object used to interact with the Ad List page
    """

    def go_to_ad(self, list_id=None, subject=None):
        if list_id:
            self.driver.find_element_by_id(unicode(list_id)).find_element_by_tag_name("a").click()
        elif subject:
            self.driver.find_element_by_partial_link_text(unicode(subject)).click()


