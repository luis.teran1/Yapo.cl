import sunbro
from selenium.webdriver.common.by import By
from page_objects import ad_delete

class AccountDashboard(sunbro.Page):
    """ this class describes the account bar """
    next_page = sunbro.FindByID('next_page')
    ads_list = sunbro.FindAllByCSS('.cont-dashboard-das .single-loop-das')

    def goto_next_page(self):
        self.next_page.click()
        return self

    def click_delete_ad(self, list_id):
        self.delete_link = self._driver.find_element_by_css_selector('a[href*="'+list_id+'&cmd=delete"]')
        self.delete_link.click()
        return ad_delete.AdDeletePage(self._driver)

    def is_ad_on_page(self, subject):
        try: self._driver.find_element_by_link_text(subject)
        except NoSuchElementException, e: return False
        return True

    def ad_in_position(self, position=0):
        return DashboardAd(self.ads_list[position])

class DashboardAd(sunbro.Page):
    """ An ad in the dashboard """
    next_page = sunbro.FindByID('next_page')
    message = sunbro.FindByClass('text-status-das')
    edit_button = sunbro.FindByCSS('.icons-panel a:nth-child(1)')
    delete_button = sunbro.FindByCSS('.icons-panel a:nth-child(2)')
