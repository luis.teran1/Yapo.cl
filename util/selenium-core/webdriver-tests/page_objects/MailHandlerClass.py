# -*- coding: latin-1 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from page_objects import BasePage
import conf, os, shutil, quopri

class MailHandler(BasePage):
    """ a mail test helper """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT)
        self.mobile_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)

    def get_mail(self, old = None, htmlformat = None):
        get_mail_url = self.base_url + "/selenium-core/getMail.php?"
        params=[]
        if old is not None:
            file_path = conf.REGRESS_FINALDIR + '/logs/mail.txt.old'
        else:
            file_path = conf.REGRESS_FINALDIR + '/logs/mail.txt'
        params.append(['filepath', file_path])
        if htmlformat is not None: params.append(['htmlmail', '1'])
        for param in params:
            get_mail_url = get_mail_url + '&' + param[0] + '=' + param[1]
        self.driver.get(get_mail_url)

    def sendmail_restore(self):
        """ run the sendmail-restore make command """
        os.system("make sendmail-restore")

    def backup_mail_file(self):
        file_path = conf.REGRESS_FINALDIR + '/logs/mail.txt'
        file_back = conf.REGRESS_FINALDIR + '/logs/mail.txt.fullbackup'  
        os.system("cat " + file_path + " >> " + file_back )


