# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage

class FaqPage(BasePage):
    """
    Page object for the faq page
    """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_faq(self):
        self.driver.get(self.base_url+"/ayuda/preguntas_frecuentes.html")

    def go_to_support_form(self,id):
        self.driver.get(self.base_url+"/support/form/0?id="+str(id))

    def check_item(self, item, expected_question, expected_answer):
        h3 = item.find_element_by_tag_name("h3")
        h3.click()
        real_question = item.find_element_by_class_name("question_title")
        if expected_question != real_question.text:
            return False

        answer = item.find_element_by_class_name("answer")
        if answer.value_of_css_property("display") != u"block":
            return False

        real_answer = answer.find_element_by_tag_name("p")
        if expected_answer != real_answer.text:
            return False

        return True
