# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage

class ResetPasswordForm(AccountPage):
    """
    Page object for reset password
    """
    def __init__(self, hash, op, email):
        super(ResetPasswordForm, self).__init__()
        self.driver.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, hash, op, email))

    def type_passwords(self, password):
        """ Type both password and verification """
        self.driver.find_element_by_xpath('//*[@id="password"]').send_keys(password)
        self.driver.find_element_by_xpath('//*[@id="password_verification"]').send_keys(password)


