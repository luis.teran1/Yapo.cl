# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AdViewPage
from selenium.webdriver.support.wait import WebDriverWait

class PaymentPage(BasePage):
    """
    Page object for the payment's features
    """

    doc_types = { 1: "Boleta", 2: "Factura"}
    fields_invoice = ['name', 'lob', 'rut', 'address', 'contact']
    fields_paysim = {'transaction_type': 'TBK_TIPO_TRANSACCION', 'amount': 'TBK_MONTO', 'purchase_order': 'TBK_ORDEN_COMPRA',
	'session_id': 'TBK_ID_SESION', 'success_url': 'TBK_URL_EXITO', 'failure_url': 'TBK_URL_FRACASO'}

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_pay_bump(self, list_id):
        browser = self.driver
        av = AdViewPage(browser)
        av.go_to_view(list_id)
        browser.find_element_by_id('product_link_1').click()

    def press_pay_button(self):
        self.driver.find_element_by_id('submit-bill').click()

    def paysim_accept_payment(self):
        browser = self.driver
        browser.find_element_by_name('tbk_ok').click()
        browser.find_element_by_name('tbk_next').click()

    def paysim_refuse_with_success_url(self):
        browser = self.driver
        success_url = browser.find_element_by_name('TBK_URL_EXITO').get_attribute("value")
        browser.find_element_by_name('TBK_URL_FRACASO').clear()
        browser.find_element_by_name('TBK_URL_FRACASO').send_keys(success_url)
        browser.find_element_by_name('tbk_cancel').click()
        browser.find_element_by_name('tbk_next').click()

    def paysim_refuse_payment(self):
        browser = self.driver
        browser.find_element_by_name('tbk_cancel').click()
        browser.find_element_by_name('tbk_next').click()

    def paysim_cancel_payment(self):
        browser = self.driver
        browser.find_element_by_name('tbk_voided').click()
        browser.find_element_by_name('tbk_next').click()

    def buy_bump_and_pay(self, list_id):
        browser = self.driver
        self.go_to_pay_bump(list_id)
        self.press_pay_button()
        self.paysim_accept_payment()

    def change_doc_type(self, doc_type, ftype=2):
        browser = self.driver
        if(ftype == 2):
            browser.find_elements_by_class_name('document-label')[doc_type-1].click()
        else:
            browser.find_element_by_partial_link_text(doc_types[doc_type]).click()

    def populate_invoice(self, name, lob, rut, address, region, commune, contact):
        browser = self.driver
        args = locals()
        for field in self.fields_invoice:
            if not args[field] is None:
                input = self.driver.find_element_by_id('id_' + field)
                input.clear()
                input.send_keys(args[field])
        if region is not None:
            select_region = self.driver.find_element_by_id('id_region')
            for option in select_region.find_elements_by_tag_name('option'):
                if option.get_attribute("value") == region:
                    option.click()
        if commune is not None:
            select_commune = self.driver.find_element_by_id('id_communes')
            for option in select_commune.find_elements_by_tag_name('option'):
                if option.get_attribute("value") == commune:
                    option.click()


    def paysim_populate(self, transaction_type, amount, purchase_order, session_id, success_url, failure_url):
        browser = self.driver
        args = locals()
        for k, v in self.fields_paysim.items():
            if not args[k] is None:
                input = self.driver.find_element_by_name(v)
                input.clear()
                input.send_keys(args[k])


    def pay_from_summary_page(self):
        self.driver.find_element_by_id("checkout").click()
        self.driver.find_element_by_id("checkout").click()
        self.paysim_accept_payment()


    def is_ad_correct_in_confirmation_page(self, position, subject, product, price):
        cells = self.driver.find_element_by_css_selector(".table.table-striped.purchase-list").find_elements_by_xpath("//tbody/tr")[position].find_elements_by_xpath("td")
        if cells[0].text != subject:
            print "{0} != {1}".format(cells[0].text, subject)
            return False
        if cells[1].text != product:
            print "{0} != {1}".format(cells[1].text, product)
            return False
        if cells[2].text != price:
            print "{0} != {1}".format(cells[2].text, price)
            return False
        return True


    def is_ad_correct_in_summary_page(self, position, subject, product, price):
        cells = self.driver.find_element_by_css_selector(".order-details").find_elements_by_xpath("//table/tbody/tr")[position].find_elements_by_xpath("td")
        if cells[0].find_element_by_class_name('solid-link').text != subject:
            print "{0} != {1}".format(cells[0].find_element_by_class_name('solid-link').text, subject)
            return False
        if cells[1].text != product:
            print "{0} != {1}".format(cells[1].text, product)
            return False
        if cells[2].text != price:
            print "{0} != {1}".format(cells[2].text, price)
            return False
        return True




