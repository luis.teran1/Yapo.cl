# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
from page_objects import BasePage, 


class AccountPage(BasePage):
    """A page object for the shopping cart known"""

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(5)
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    

