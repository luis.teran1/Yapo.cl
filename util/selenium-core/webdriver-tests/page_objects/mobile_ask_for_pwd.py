""" Collection of page objects for mobile site """
from selenium.webdriver.common.by import By
from page_objects.mobile_ai import MobileAdForm
import sunbro
import conf
import time

class AdView(sunbro.Page):
    """ Page object of ad view for mobile """

    edit_button = sunbro.FindElement(By.CLASS_NAME, 'edit')
    delete_button = sunbro.FindElement(By.CLASS_NAME, 'bt_delete')
    ad_subject = sunbro.FindElement(By.CSS_SELECTOR, 'div.title > h1')
    ad_body = sunbro.FindElement(By.CLASS_NAME, 'texto')
    ad_price = sunbro.FindElement(By.CLASS_NAME, 'price')    

    def go_to_edit(self):
        self.edit_button.click()
        return AskForPwd(self._driver)

class AskForPwd(sunbro.Page):
    """ Page object of ad ask for pwd for mobile """

    password = sunbro.FindElement(By.ID, 'password_input')
    submit = sunbro.FindElement(By.ID, 'submit_button')
    validation_msg = sunbro.FindElement(By.ID, 'err_message')
    forgot_password = sunbro.FindElement(By.ID, 'forgot_passwd_link')
    page_head = sunbro.FindElement(By.TAG_NAME, 'h1')

    def submit_password(self, pwd = None):
        """ It gets a password, and submits the form """
        if(pwd is not None):
            self.password.clear()
            self.password.send_keys(pwd)
            self.submit.click()
        return MobileAdForm(self._driver)
