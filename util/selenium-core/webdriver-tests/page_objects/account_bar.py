import sunbro
from selenium.webdriver.common.by import By

class AccountBar(sunbro.Page):
    """ this class describes the account bar """
    me = sunbro.FindElement(By.CLASS_NAME, 'account-bar')
    left_menu = sunbro.FindElement(By.ID, 'dropleft-menu-div')
    buttons = sunbro.FindElement(By.CLASS_NAME, 'container-login')
    login = sunbro.FindElement(By.ID, 'login-account-link')
    create_account = sunbro.FindElement(By.CLASS_NAME, 'create-account')

    input_email = sunbro.FindElement(By.ID, 'loginInputEmail')
    input_pass  = sunbro.FindElement(By.ID, 'loginInputPassword')
    login_btn   = sunbro.FindElement(By.ID, 'loginsubmit')

    def log_in(self, email, password):
        self.login.click()
        self.input_email.clear()
        self.input_email.send_keys(email)
        self.input_pass.clear()
        self.input_pass.send_keys(password)
        self.login_btn.click()
