# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage
import subprocess

class MobileBasePage(BasePage):
    """ Base page for the mobile page objects """

    def __init__(self, driver):
        self.base_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.driver = driver
        self.wait = ui.WebDriverWait(self.driver, 10)

