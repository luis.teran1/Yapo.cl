from selenium.webdriver.common.by import By
import sunbro
import time

class MainPage(sunbro.Page):
    """
    Page object used to interact with the Control Panel Main page
    """

    queues_link = sunbro.FindElement(By.LINK_TEXT, 'Queues')

    def go_to_queues(self):
        """ This method clicks on the queues link.

        :returns: A QueuesPage object.
        """
        self.queues_link.click()
        return QueuesPage(self._driver)

class QueuesPage(sunbro.Page):
    """
    Page object used to interact with the Control Panel Queues page.
    """

    # Use the queue code + '_queue_link' to make this queues available
    # See method go_to_queue for further details.
    edit_queue_link = sunbro.FindElement(By.ID, 'edit')
    normal_queue_link = sunbro.FindElement(By.ID, 'normal')
    whitelist_queue_link = sunbro.FindElement(By.XPATH, "//a[contains(text(),'Whitelist')]")
    autoaccept_queue_link = sunbro.FindElement(By.XPATH, "//a[contains(text(),'Automatically accept')]")

    def go_to_queue(self, queue_code):
        """ This method clicks on the edit queue link.

        :param queue_code: Code of the queue.
        :returns: A QueuePage object.
        """
        queue = getattr(self, queue_code+'_queue_link')
        queue.click()
        return QueuePage(self._driver)

class QueuePage(sunbro.Page):
    """ This page object represents the Ad Queue Page. """
    # this selector may not work correctly if you have more than 1 ad in 
    # the current page
    title = sunbro.FindElement(By.CLASS_NAME, 'da_title')
