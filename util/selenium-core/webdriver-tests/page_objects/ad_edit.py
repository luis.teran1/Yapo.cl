from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import sunbro, time

class AdEditPasswordVerificationPage(sunbro.Page):
    """ This page object represents the page that ask for your password,
        when you are trying to edit an ad, and you are not logged in
    """
    password = sunbro.FindElement(By.ID, 'passwd')
    submit_button = sunbro.FindElement(By.NAME, 'continue')

    def successful_password(self, password):
        """ This method fills in the form, that ask for the ad password.
            It is suppose to work with correct passwords
            Returns an AdEditPage object
        """
        self.password.clear()
        self.password.send_keys(password)
        self.submit_button.click()
        return AdEditPage(self._driver)

class AdEditPage(sunbro.Page):
    """ This page object represents the Ad Edit Page, when the form is displayed """

    price = sunbro.FindElement(By.ID, 'price')
    subject = sunbro.FindElement(By.ID, 'subject')
    want_bump = sunbro.FindElement(By.CLASS_NAME, 'icheckbox_minimal-green')
    submit_button = sunbro.FindElement(By.ID, 'submit_create_now')
    communes_select = sunbro.FindElement(By.ID, 'communes')
    accept_conditions = sunbro.FindElement(By.ID, 'accept_conditions')

    def edit(self, dict):
        """ Takes a dictionary with key/value to send to the inputs or textarea of the edit page  """
        for key, value in dict.items():
            # this is just horrible. but is what we have because we use some special checkbox
            # sorry if your eyes are bleeding
            if key in ['want_bump', 'accept_conditions']:
                attr = getattr(self, key)
                attr.click()
            elif key in ['communes']:
                attr = Select(self.communes_select)
                attr.select_by_value(value)
            else:
                attr = getattr(self, key)
                attr.clear()
                attr.send_keys(value)

        self.submit_button.click()
        return AdEditConfirmationPage(self._driver)

class AdEditConfirmationPage(sunbro.Page):
    """ This page object represents the Ad Edit confirmation Page, when the form is sent and everything is OK """
    pass
