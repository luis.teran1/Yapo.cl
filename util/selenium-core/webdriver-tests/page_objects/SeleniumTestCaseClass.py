# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri

class SeleniumTestCase(unittest.TestCase):
    """
    Base class for selenium testcases.

    It's responsible for opening the browser and leaving the driver in
    self.driver also set up some urls
    """
    def setUp(self, ua_string=None):
        if (ua_string):
            self.user_agent = ua_string
        if getattr(self, 'user_agent', None):
            profile = webdriver.FirefoxProfile()
            profile.set_preference("general.useragent.override", self.user_agent)
            self.driver = webdriver.Firefox(profile)
        else:
            self.driver = webdriver.Firefox()

        self.driver.implicitly_wait(5)
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)
        self.mobile_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.cpanel_url = self.secure_url + "/controlpanel"
        self.ai_url = self.base_url + "/ai"
        self.wait = ui.WebDriverWait(self.driver, 10) # timeout after 10 seconds

    def is_element_present(self, how, what, element = None):
        if element is None:
            driver = self.driver
        else:
            driver = element # "driver"
        try: driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def tearDown(self):
        self.driver.quit()

    def assertIn(self, this, into_this):
        message = u"'{0}' not found into '{1}'".format(this, into_this)
        self.assertTrue(this in into_this, message)

    def assertNotIn(self, this, into_this):
        message = u"'{0}' not found into '{1}'".format(this, into_this)
        self.assertFalse(this in into_this, message)
