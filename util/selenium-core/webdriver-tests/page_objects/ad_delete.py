from selenium.webdriver.common.by import By
from page_objects import ad_list
import sunbro

class AdDeletePage(sunbro.Page):
    """ This page object represents the Ad Edit Page, when the form is displayed """

    r1 = sunbro.FindElement(By.CSS_SELECTOR, '#radio1+.iCheck-helper')
    r2 = sunbro.FindElement(By.CSS_SELECTOR, '#radio2+.iCheck-helper')
    r3 = sunbro.FindElement(By.CSS_SELECTOR, '#radio3+.iCheck-helper')
    r4 = sunbro.FindElement(By.CSS_SELECTOR, '#radio4+.iCheck-helper')
    r5 = sunbro.FindElement(By.CSS_SELECTOR, '#radio5+.iCheck-helper')
    r6 = sunbro.FindElement(By.CSS_SELECTOR, '#radio6+.iCheck-helper')

    p1 = sunbro.FindElement(By.ID, 'pass1')
    p2 = sunbro.FindElement(By.ID, 'pass2')
    p3 = sunbro.FindElement(By.ID, 'pass3')
    p4 = sunbro.FindElement(By.ID, 'pass4')
    p5 = sunbro.FindElement(By.ID, 'pass5')
    p6 = sunbro.FindElement(By.ID, 'pass6')

    b1 = sunbro.FindElement(By.ID, 'bump1')
    b2 = sunbro.FindElement(By.ID, 'bump2')
    b3 = sunbro.FindElement(By.ID, 'bump3')
    b4 = sunbro.FindElement(By.ID, 'bump4')
    b5 = sunbro.FindElement(By.ID, 'bump5')
    b6 = sunbro.FindElement(By.ID, 'bump6')

    d1 = sunbro.FindElement(By.ID, 'delete1')
    d2 = sunbro.FindElement(By.ID, 'delete2')
    d3 = sunbro.FindElement(By.ID, 'delete3')
    d4 = sunbro.FindElement(By.ID, 'delete4')
    d5 = sunbro.FindElement(By.ID, 'delete5')
    d6 = sunbro.FindElement(By.ID, 'delete6')

    user_text2 = sunbro.FindElement(By.ID,'dlv2')
    user_text6 = sunbro.FindElement(By.ID,'dlv6')

    def delete(self, reason_id, password = None, user_text=None):
        reason = getattr(self, 'r'+str(reason_id))
        reason.click()

        if user_text:
           user_text_box = getattr(self,'user_text{0}'.format(reason_id))
           user_text_box.clear()
           user_text_box.send_keys(user_text)

        if password:
            password_box = getattr(self, 'p'+str(reason_id))
            password_box.clear()
            password_box.send_keys(password)

        submit = getattr(self, 'd'+str(reason_id))
        submit.click()

        return AdDeleteConfirmationPage(self._driver)

class AdDeleteConfirmationPage(sunbro.Page):
    """ This page object represents the Ad Edit confirmation Page, when the form is sent and everything is OK """

    btn_listing = sunbro.FindElement(By.LINK_TEXT, 'Ver avisos')

    def goto_listing_using_button(self):
        self.btn_listing.click()
        return ad_list.AdListPage(self._driver)
