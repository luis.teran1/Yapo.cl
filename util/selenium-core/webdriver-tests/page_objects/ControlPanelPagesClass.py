 # -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
from selenium.webdriver.common.by import By
from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage

class ControlPanelPages(BasePage):
    """
    Page object for the f*cking control panel page de los cojones
    """

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_controlpanel(self):
        self.driver.get(self.secure_url+"/controlpanel")

    def login_CP(self, user, password):
        self.driver.find_element_by_name("username").clear()
        self.driver.find_element_by_name("cpasswd").clear()
        self.driver.find_element_by_name("username").send_keys(user)
        self.driver.find_element_by_name("cpasswd").send_keys(password)
        self.driver.find_element_by_name("login").click()

    def logout(self):
        self.driver.get(self.secure_url + '/controlpanel?logout')
        self.driver.find_element_by_name("username").clear()

    def check_login(self):
        try:
            self.driver.find_element_by_name("username")
            return True
        except NoSuchElementException:
            return False

    def search_for_ad_by_list_id(self, list_id = None):
        self.driver.get(self.secure_url+"/controlpanel")
        self.driver.find_element_by_partial_link_text("Search for ad").click()
        self.driver.find_element_by_id('search_type_list_id').click()
        self.driver.find_element_by_id("search_field").send_keys(list_id)
        self.driver.find_element_by_name("search").click()

    def go_to_queue(self, queue):
        self.driver.find_element_by_xpath("//li/a[text() = 'Queues']").click()
        self.driver.find_element_by_partial_link_text(queue).click()

    def go_to_option_in_menu(self, option):
        self.driver.find_element_by_xpath("//li/a[text() = '" + option  + "']").click()

    def search_for_ads(self, email = None, list_id = None, ad_id = None):
        none_count = 0
        if (email == None):
            none_count += 1
        if (list_id == None):
            none_count += 1
        if (ad_id == None):
            none_count += 1

        if (none_count == 3):
            raise Exception("There must be one search criteria")
        if (none_count == 1):
            raise Exception("Only one search criteria can be selected at a time")

        self.go_to_controlpanel()
        self.driver.find_element_by_partial_link_text('Search for ad').click()
        if (email):
            self.driver.find_element_by_id('search_type_email').click()
            self.driver.find_element_by_id("search_field").send_keys(email)
        elif (ad_id):
            self.driver.find_element_by_id('search_type_ad_id').click()
            self.driver.find_element_by_id("search_field").send_keys(ad_id)
        else:
            self.driver.find_element_by_id('search_type_list_id').click()
            self.driver.find_element_by_id("search_field").send_keys(list_id)

        self.driver.find_element_by_name("search").click()


    def search_ad_by_list_id(self, list_id):
        self.driver.find_element_by_partial_link_text('Search for ad').click()
        self.driver.find_element_by_id('search_type_list_id').click()
        self.driver.find_element_by_id("search_field").send_keys(str(list_id))
        self.driver.find_element_by_name("search").click()

    def search_ad_by_ad_id(self, ad_id):
        self.driver.find_element_by_partial_link_text('Search for ad').click()
        self.driver.find_element_by_id('search_type_ad_id').click()
        self.driver.find_element_by_id("search_field").send_keys(str(ad_id))
        self.driver.find_element_by_name("search").click()

    def search_for_ad(self, email, subject = None, list_id = None, queue = None):
        self.driver.find_element_by_partial_link_text('Search for ad').click()
        self.driver.find_element_by_id("search_field").send_keys(email)
        if queue is not None:
            queueSelect = Select(self.driver.find_element_by_name("queue"))
            queueSelect.select_by_visible_text(queue)

        self.driver.find_element_by_name("search").click()
        if subject is not None:
            self.driver.find_element_by_partial_link_text(subject).click()
            self.driver.switch_to_window(self.driver.window_handles[1])
        if list_id is not None:
            self.driver.find_element_by_partial_link_text(list_id).click()
            self.driver.switch_to_window(self.driver.window_handles[1])

    def review_ad(self, action = None, refusal_reason_name = None):
        if (action == 'deny' or action == 'refuse') and refusal_reason_name is None:
            refusal_reason_name = 'Spam'
        if action:
            if action in ['accept', 'region', 'abuse', 'unsolved']:
                self.driver.find_element_by_xpath("//input[@name='queue_action'][@value='"+action+"']").click()
            elif action == "edit_and_accept":
                for option in self.driver.find_element_by_name('reasonAccept').find_elements_by_tag_name('option'):
                    if option.text == refusal_reason_name:
                        option.click()
                        break
            else:
                if (action == "deny" or action == 'refuse'):
                    for option in self.driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
                        if option.text == refusal_reason_name:
                            option.click()
                            break
            self.driver.find_element_by_xpath("//input[@name='commit'][@type='submit']").click()
            # it looks like this class is append to all the actions, so I am adding this to wait for the response
            self.driver.find_element_by_class_name("AjaxCommitOk")

        if action is None and refusal_reason_name == 'Fraude':
            for option in self.driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
                if option.text == refusal_reason_name:
                    option.click()
                    break
            self.driver.find_element_by_xpath("//input[@name='commit'][@type='submit']").click()

    def change_first_ad_status(self, action = None):
        self.driver.find_element_by_partial_link_text('Edit, delete, ').click()
        self.driver.switch_to_window(self.driver.window_handles[1])
        if action:
            if action in ['delete', 'edit', 'renew', 'hide' ,'show']:
                self.driver.find_element_by_id("cmd_" + action).click()
            self.driver.find_element_by_name('continue').click()

    def review_first_ad_on_search(self, refusal_condition_name = 'Spam'):
        self.driver.find_element_by_partial_link_text('Review').click()
        self.driver.switch_to_window(self.driver.window_handles[1])
        for option in self.driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
            if option.text == refusal_condition_name:
                option.click()
                break
        self.driver.find_element_by_name('commit').click()
        self.driver.find_element_by_class_name("AjaxCommitOk")

    def unhide_ad(self, list_id = None):
        self.search_for_ad_by_list_id(list_id)
        self.change_first_ad_status(action = 'show')
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def hide_ad(self, list_id = None):
        self.search_for_ad_by_list_id(list_id)
        self.change_first_ad_status(action = 'hide')
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def post_refuse_ad(self, list_id = None, reason_name = None):
        self.search_for_ad_by_list_id(list_id)
        if reason_name:
            self.review_first_ad_on_search(reason_name)
        else:
            self.review_first_ad_on_search()
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def search_for_users_accounts(self,email):
        self.driver.find_element_by_partial_link_text("Search for users accounts").click()
        self.driver.find_element_by_id('action_email').send_keys(email)
        self.driver.find_element_by_name("search").click()

    def deactivate_account(self,email):
        self.search_for_users_accounts(email)
        self.driver.find_element_by_partial_link_text("Deactivate").click()
        self.driver.switch_to_alert().accept()

    def go_and_login_CP(self, user, password):
        self.go_to_controlpanel()
        self.login_CP(user, password)

    def search_and_review_ad(self, email, subject, action, refusal_reason_name=None, list_id=None, queue=None):
        if list_id is None:
            self.search_for_ad(email, subject=subject, queue=queue)
        else:
            self.search_for_ad(email, list_id=list_id, queue=queue)
        self.review_ad(action, refusal_reason_name)
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def search_and_hide_ad(self, ad_id = None, list_id = None):
        self.search_for_ads(ad_id = ad_id, list_id = list_id)
        self.change_first_ad_status(action = 'hide')
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def search_and_show_ad(self, ad_id = None, list_id = None):
        self.search_for_ads(ad_id = ad_id, list_id = list_id)
        self.change_first_ad_status(action = 'show')
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def search_and_delete_ad(self, ad_id = None, list_id = None):
        self.search_for_ads(ad_id = ad_id, list_id = list_id)
        self.change_first_ad_status(action = 'delete')
        self.driver.close()
        self.driver.switch_to_window(self.driver.window_handles[0])

    def search_and_undelete_ad(self, ad_id = None, list_id = None):
        self.search_for_ads(ad_id = ad_id, list_id = list_id)
        try:
            self.driver.find_element_by_id('chk_0_0').click()
            self.driver.find_element_by_css_selector('input[value="Undelete selected"]').click()
        except NoSuchElementException:
            raise Exception('Your user sucks!')

    def get_changes_on_edition(self):
        try:
            elements = self.driver.find_elements_by_class_name('TextDiffNew')
        except NoSuchElementException, e:
            return []
        real_changes = [change.text for change in elements]
        return real_changes

    def apply_product(self,list_id,product_name):
        product_select = Select(self.driver.find_element_by_id('products_to_apply_'+list_id))
        product_select.select_by_visible_text(product_name)
        self.driver.find_element_by_id('apply_product_'+list_id).click()
        time.sleep(1)
        alert = self.driver.switch_to_alert()
        alert.accept()


class ControlPanelNewRefusalReasons(BasePage):
    """
    Page object for new refusal reasons management.
    """

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_new_reason_page(self):
        self.driver.get(self.secure_url+"/controlpanel?m=adqueue&a=refusals&refusal_action=new_refusal")

    # This method was specifically created for test that email isn't sent when the reject reason is fraud
    # so I think to assign default values is a good idea ^_^ (same prod values... useful for this test)
    def add_refusal_reason(self, reason = 'Fraude', id = '36', text = '.'):
        control_panel = ControlPanelPages(self.driver)
        control_panel.go_to_controlpanel()
        control_panel.login_CP('dany', 'dany')
        self.go_to_new_reason_page()
        # START WEB ELEMENTS DECLARATION SECTION (the ugly section).
        self.reason_input = self.driver.find_element_by_css_selector(".module_content input[name='refusal_new_subject']")
        self.id_input = self.driver.find_element_by_css_selector(".module_content input[name='refusal_id']")
        self.reason_select = self.driver.find_element_by_css_selector(".module_content select")
        self.reason_textarea = self.driver.find_element_by_css_selector(".module_content textarea")
        self.submit_button = self.driver.find_element_by_css_selector(".module_content input[name='refusal_submit']")
        # END WEB ELEMENTS DECLARATION SECTION
        self.reason_input.send_keys(reason)
        self.id_input.send_keys(id)
        self.reason_textarea.send_keys(text)
        self.submit_button.click()

class ControlPanelSpamfilter(BasePage):
    """
    Page object for spamfilter management.
    """

    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_filter_lists(self):
        self.driver.get(self.secure_url+"/controlpanel?m=filter&a=lists")

    def go_to_filter_rules(self):
        self.driver.get(self.secure_url+"/controlpanel?m=filter&a=rules")

    def go_to_filter_spamfilter(self):
        self.driver.get(self.secure_url+"/controlpanel?m=filter&a=spamfilter")
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.CLASS_NAME, 'ListingTable')))

    def add_email_to_filter(self, value,notice = 'default text', list = 'E-post adresser - spamfiltret'):
        control_panel = ControlPanelPages(self.driver)
        control_panel.go_to_controlpanel()
        if control_panel.check_login():
                control_panel.login_CP('dany', 'dany')
        self.go_to_filter_lists()
        self.driver.find_element_by_link_text(list).click()
        self.value = self.driver.find_element_by_name('value')
        self.notice = self.driver.find_element_by_name('notice')
        self.add_button = self.driver.find_element_by_xpath("//input[@type='submit'][@value='Agregar a la lista']")
        self.value.send_keys(value)
        self.notice.send_keys(notice)
        self.add_button.click()
        self.check_activation_email_rule()

    def check_activation_email_rule(self):
        self.driver.get(self.secure_url+"/controlpanel?m=filter&a=add_rule&rule_id=14")
        checkbox_email = self.driver.find_element_by_id('email[1]')
        save_button = self.driver.find_element_by_xpath("//input[@type='submit'][@value='Save rule']")
        if not checkbox_email.is_selected():
            checkbox_email.click()
            save_button.click()

    def action_mail_on_spam_filter(self,subject,action):
        control_panel = ControlPanelPages(self.driver)
        control_panel.go_to_controlpanel()
        if control_panel.check_login():
                control_panel.login_CP('dany', 'dany')
        self.go_to_filter_spamfilter()
        table = self.driver.find_element_by_class_name('ListingTable')
        WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.TAG_NAME, 'a')))
        table.find_element_by_tag_name('a').click()
        msgs = self.driver.find_element_by_class_name('Listing').find_elements_by_class_name('Message')
        action_buttons = self.driver.find_element_by_class_name('Listing').find_elements_by_xpath("//input[@type='submit'][@value='"+action+"']")
        for row in range(len(msgs)):
                if msgs[row].text == subject:
                     action_buttons[row].click()


class WebSQL(ControlPanelPages):

    def login(self, user, password):
        self.go_to_controlpanel()
        self.login_CP(user, password)

    def go_to_websql(self):
        self.driver.get(self.secure_url + '/controlpanel?m=Websql&a=websql')

    def select_query(self, name):
        sql = Select(self.driver.find_element_by_name('query'))
        sql.select_by_visible_text(name)
        self.driver.find_element_by_name('load').click()

    def fill_input(self, name, value):
        input = self.driver.find_element_by_name(name)
        if input.tag_name == 'select':
            input = Select(self.driver.find_element_by_name('query'))
            input.select_by_visible_text(name)
            return
        else:
            input.clear()
            input.send_keys(value)

    def call_websql(self, query_name, **options):
        """Calls a websql, filling the form with the supplied `options` data
        """
        self.go_to_websql()
        self.select_query(query_name)
        for field_name, value in options.iteritems():
            self.fill_input(field_name, value)
        self.run()

    def run(self):
        self.driver.find_element_by_name('run').click()

    def get_result_data(self):
        table = self.driver.find_element_by_css_selector('table.websql_result_table')
        rows = table.find_elements_by_tag_name('tr')
        data = []
        # We ignore the headers that are int the 1st row
        for row in rows[1:]:
            cols = row.find_elements_by_tag_name('td')
            data.append([c.text for c in cols])
        return data
