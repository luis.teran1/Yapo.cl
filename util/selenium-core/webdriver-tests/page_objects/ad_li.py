from selenium.webdriver.common.by import By
import conf
import sunbro

class Gallery(sunbro.Page):
    """ Page object used to interact with the ad li gallery """

    wrapper = sunbro.FindElement(By.CSS_SELECTOR, ".gallery-wrap")

    def __init__(self, driver):
        super(Gallery, self).__init__(driver)
        self.driver = driver

    def get_ads(self):
        ad_list = self.wrapper.find_elements_by_css_selector('.gallery-list li')
        return [ GalleryAd(ad) for ad in ad_list ]

class GalleryAd:

    link_locator = (By.CSS_SELECTOR, "a")
    title_locator = (By.CSS_SELECTOR, "a .gallery-item-title")
    price_locator = (By.CSS_SELECTOR, "a .gallery-item-price-text")
    no_image_locator = (By.CSS_SELECTOR, "a .gallery-image-empty")
    image_locator = (By.CSS_SELECTOR, "a .gallery-image")

    def __init__(self, element):
        self.element = element

class AdVi(sunbro.Page):
    subject = sunbro.FindElement(By.ID, "da_subject")
    delete_button = sunbro.FindElement(By.ID, "ad_admin_link_eliminar")

    def __init__(self, driver):
        super(AdVi, self).__init__(driver)
        self.driver = driver


class SearchBox(sunbro.Page):
    searchbox = sunbro.FindElement(By.ID, "search")
    region = sunbro.FindElement(By.ID, "searcharea_expanded")
    category = sunbro.FindElement(By.ID, "catgroup")
