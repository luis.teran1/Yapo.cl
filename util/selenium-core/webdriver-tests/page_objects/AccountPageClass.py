# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage

class AccountPage(BasePage):
    """A page object for account login actions"""
    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(5)
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def login(self, email, password):
        """
        Login the user using the top login form in almost every page
        """
        driver = self.driver
        self.driver.get(self.secure_url+"/login")
        driver.find_element_by_id("email_input").send_keys(email)
        driver.find_element_by_id("password_input").send_keys(password)
        driver.find_element_by_id("submit_button").click()
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda driver: 'Hola' in driver.find_element_by_class_name('__loggedIn').text)

    def mobile_login(self, email, password, short_session = False):
        driver = self.driver
        self.driver.get(self.secure_url+"/login")
        driver.find_element_by_id("acc_page_login").click()
        driver.find_element_by_id("email_input").send_keys(email)
        driver.find_element_by_id("password_input").send_keys(password)
        if short_session is True:
            driver.find_element_by_id("icheck_logindontclose").click()
        driver.find_element_by_id("submit_button").click()
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: '/dashboard' in browser.current_url)


    def attempt_login(self,email,password):
        """
        Attempt to Login the user using the top login form in almost every page
        """
        driver = self.driver
        self.display_login_form()
        driver.find_element_by_id("loginInputEmail").send_keys(email)
        driver.find_element_by_id("loginInputPassword").send_keys(password)
        driver.find_element_by_id('loginsubmit').submit()

    def logout(self):
        """
        Logout the user by any mean
        """
        driver = self.driver
        driver.find_element_by_class_name('header-userLogout').click()
        ui.WebDriverWait(driver, 10).until(lambda driver: 'Crear mi cuenta gratuita' in driver.find_element_by_css_selector('.create-account-title .title-account').text)

    def fill_form(self, **kwargs):
        """
        Fill the form
        """
        driver = self.driver
        options = {'name': "Ejemplo",
                   'phone': "5555556",
                   'email': "example@yapo.cl",
                   'password': "123123123",
                   'password_verify': "123123123" }
        options.update(kwargs)
        self.go_to_create_form()
        for key, value in options.iteritems():
            driver.find_element_by_id("account_" + key).send_keys(value)

        driver.find_element_by_id("accept_conditions").find_element_by_xpath('..').click()
        driver.find_element_by_id("is_company_p").click()

    def submit_form(self, **kwargs):
        """
        Submit the form
        """
        driver = self.driver
        driver.find_element_by_name("submit").click()

    def create(self, **kwargs):
        """
        Create an account using the supplied data
        """
        driver = self.driver

        create_acc_successful = True
        if kwargs.has_key('create_acc_with_errors'):
           create_acc_successful = False
           kwargs.pop('create_acc_with_errors')

        options = {'name': "Ejemplo",
                   'phone': "5555556",
                   'email': "example@yapo.cl",
                   'password': "123123123",
                   'password_verify': "123123123",
                   }
        options.update(kwargs)
        self.go_to_create_form()
        for key, value in options.iteritems():
            driver.find_element_by_id("account_"+key).clear()
            driver.find_element_by_id("account_"+key).send_keys(value)

        driver.find_element_by_id("accept_conditions").find_element_by_xpath('..').click()
        driver.find_element_by_css_selector("#is_company_p + ins").click()
        driver.find_element_by_name("submit").click()
        if create_acc_successful:
           wait = ui.WebDriverWait(self.driver, 10)
           wait.until(lambda driver: 'Felicidades!' in driver.find_element_by_class_name('title-account-success').text)

    def go_to_dashboard(self, page = None):
        """
        Reaches the dashboard somehow
        """
        if page:
            self.driver.get(self.secure_url+"/dashboard?page=" + page)
            wait = ui.WebDriverWait(self.driver, 10)
            wait.until(lambda browser: page in browser.find_element_by_class_name('current').text)
        else:
            self.driver.get(self.secure_url+"/dashboard")
            wait = ui.WebDriverWait(self.driver, 10)
            wait.until(lambda browser: '/dashboard' in browser.current_url)

    def go_to_dashboard_profile(self, is_mobile = False):
        """
        Reaches the dashboard profile link
        """
        self.go_to_dashboard()
        if is_mobile is False:
            self.driver.find_element_by_css_selector('ul.menu-account li:nth-child(2) a').click()
        else:
            self.driver.find_element_by_link_text("Mi perfil").click()

    def go_to_shopping_cart(self):
        """
        Load "/pagos" page directly without pressing the stickybar button
        """
        self.driver.get(self.secure_url+'/pagos')
        wait = ui.WebDriverWait(self.driver, 5)
        wait.until(lambda browser: '/pagos/form' in browser.current_url)

    def go_to_shopping_cart_using_link(self):
        """
        Load "/pagos" page using link on dashboard
        """
        self.go_to_dashboard()
        self.driver.find_element_by_class_name('shopping-cart').click()
        wait = ui.WebDriverWait(self.driver, 5)
        wait.until(lambda browser: '/pagos/form' in browser.current_url)

    def go_to_shopping_cart_using_button(self):
        """
        Press the button "Comprar" on dashboardPage
        """
        self.go_to_dashboard()
        self.driver.find_element_by_id('checkout').click()
        wait = ui.WebDriverWait(self.driver, 5)
        wait.until(lambda browser: '/pagos/form' in browser.current_url)


    def go_to_create_form(self):
        """
        Reaches the create form somehow
        """
        self.driver.get(self.secure_url+"/cuenta")
        wait = ui.WebDriverWait(self.driver, 5)
        wait.until(lambda browser: '/cuenta/form' in browser.current_url)

    def display_login_form(self):
        """
        Makes the login form magically appear
        """
        browser = self.driver
        ui.WebDriverWait(self.driver, 10).until(lambda browser: browser.find_element_by_id('login-account-link'))
        browser.find_element_by_id('login-account-link').click()
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: 'slide-left' in browser.find_element_by_id('dropleft-menu-div').get_attribute('class'))

    def change_account_status(self, email, status):
        """Overwrite a Bconf key"""
        os.system('printf "cmd:manage_account\nemail:' + email + '\nstatus:' + status + '\ncommit:1\nend\n" | nc localhost ' + conf.REGRESS_TRANS_PORT + ' > /dev/null 2>&1')

    def activate_account(self, email):
        """Overwrite a Bconf key"""
        self.change_account_status(email, 'active')

