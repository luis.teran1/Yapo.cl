import sunbro
from selenium.webdriver.common.by import By
from page_objects import account_dashboard

class AccountLoginPage(sunbro.Page):
    """ this class describes the account bar """
    input_email = sunbro.FindElement(By.ID, 'email_input')
    input_pass  = sunbro.FindElement(By.ID, 'password_input')
    login_btn   = sunbro.FindElement(By.ID, 'submit_button')

    def log_in(self, email, password):
        self.input_email.clear()
        self.input_email.send_keys(email)
        self.input_pass.clear()
        self.input_pass.send_keys(password)
        self.login_btn.click()

        return account_dashboard.AccountDashboard(self._driver)
