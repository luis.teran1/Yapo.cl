# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage

class AboutPage(BasePage):
    """
    Page object for the faq page
    """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def go_to_about(self):
        self.driver.get(self.base_url+"/ayuda/sobre_yapo.html")
