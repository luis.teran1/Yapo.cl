import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri, urllib, hashlib
from page_objects import BasePage, AccountPage

class AccountDashboardPage(AccountPage):
    """
    Page object for the account dashboard
    """

    def get_first_login_message(self):
        """
        Returns the element containing the first login message
        """
        return self.driver.find_element_by_id("user-message-text").text

    def has_insert_ad_arrow(self):
        """
        Retuns a true if the arrow for no inserted ads in account
        is found
        """
        try:
            self.driver.find_element_by_id("insert-ads-arrow")
        except NoSuchElementException:
            return False
        return True

    def get_ad_titles(self):
        return self.driver.find_elements_by_class_name('da-subject')

    def get_published_ads_stat(self):
        return self.driver.find_element_by_css_selector('.status-ads-active .status-amount')

    def get_unreviewed_ads_stat(self):
        return self.driver.find_element_by_css_selector('.status-ads-review .status-amount')

    def get_total_views_stat(self):
        return self.driver.find_element_by_css_selector('.status-visite-total .status-amount')

    def get_total_contacts_stat(self):
        return self.driver.find_element_by_css_selector('.status-contacts .status-amount')

    def get_ad_subjects(self):
        subjects = self.driver.find_elements_by_xpath('//*[@class="ad-subject"]') + self.driver.find_elements_by_xpath('//*[@class="ad-subject"]/a')
        return subjects

    def select_ad_product_in_dashboard(self, list_id, product_code):
        if product_code == "bump":
            element = self.driver.find_element_by_id('bump-' + list_id)
            if element.get_attribute("checked") != u"true":
                element.find_element_by_xpath('..').click()
        elif product_code == "daily_bump":
            element = self.driver.find_element_by_id('bump-7-' + list_id)
            if element.get_attribute("checked") != u"true":
                element.find_element_by_xpath('..').click()
        elif product_code == "gallery":
            element = self.driver.find_element_by_id('gallery-' + list_id)
            if element.get_attribute("checked") != u"true":
                element.find_element_by_xpath('..').click()
        elif product_code == "weekly_bump":
            element = self.driver.find_element_by_id('bump-4-' + list_id)
            if element.get_attribute("checked") != u"true":
                element.find_element_by_xpath('..').click()
        else:
            raise Exception ("product not implemented!!")
        
    def get_selected_ads_by_product(self, product):
        items = []
        if product == "bump":
            ads = self.driver.find_elements_by_css_selector('.ads .bump-1 .iradio_green-check')
            ads_range = range(len(ads))
            for index in ads_range:
                items.append(ads[index].get_attribute('class').find('checked') >= 0)
        elif product == "daily_bump":
            ads = self.driver.find_elements_by_css_selector('.ads .daily-bump .iradio_green-check')
            ads_range = range(len(ads))
            for index in ads_range:
                items.append(ads[index].get_attribute('class').find('checked') >= 0)
        elif product == "weekly_bump":
            ads = self.driver.find_elements_by_css_selector('.ads .bump-4 .iradio_green-check')
            ads_range = range(len(ads))
            for index in ads_range:
                items.append(ads[index].get_attribute('class').find('checked') >= 0)
        elif product == "gallery":
            ads = self.driver.find_elements_by_css_selector('.ads .gallery-col .iradio_blue-check')
            ads_range = range(len(ads))
            for index in ads_range:
                items.append(ads[index].get_attribute('class').find('checked') >= 0)
        else:
            raise Exception ("product not implemented!!")

        return items

