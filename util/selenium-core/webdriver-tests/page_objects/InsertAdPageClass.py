# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage, AccountPage
from selenium.webdriver.support.ui import Select

class InsertAdPage(AccountPage):
    """Page Object for the insert ad page"""

    ad_field_ids = ['subject', 'body', 'price']
    user_field_ids = ['name', 'email', 'email_confirm', 'phone', 'passwd', 'passwd_ver']
    elements_to_click = ['p_ad', 'accept_conditions', 'submit_create_now']
    elements_to_click_for_ad_insert = ['accept_conditions']

    def __init__(self, driver):
        self.driver = driver
        self.driver.implicitly_wait(20)
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)

    def submit_insert_page(self):
        self.driver.find_element_by_id("submit_create_now").click()

    def go_to_edit_page(self, list_id = None, password = "123123123", caller = "15"):
        """
           Goes to edit the requested ad
        """
        browser = self.driver
        if(list_id == None):
            self.go_to_region("region metropolitana")
            browser.find_element_by_class_name('title').click()
        else:
            url = "https://%s:%s/vi/%s.htm?ca=%s_s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT, list_id, caller)
            browser.get(url)

        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: browser.find_element_by_id('ad_admin_link_editar'))
        browser.find_element_by_id('ad_admin_link_editar').click()
        browser.find_element_by_id('passwd').clear()
        browser.find_element_by_id('passwd').send_keys(password)
        browser.find_element_by_name('continue').click()

    def go_to_insert_page(self, region = "region metropolitana"):
        browser = self.driver
        self.go_to_region(region)
        self.driver.find_element_by_css_selector('.btn-da-insert').click()
        wait = ui.WebDriverWait(self.driver, 5)
        wait.until(lambda browser: '/ai/form' in browser.current_url)

    def fill_insert_page(self, region = None, communes = None, category = None, subject = None, body = None, price = None):
        args = locals()
        for field in self.ad_field_ids:
            if not args[field] is None:
                input = self.driver.find_element_by_id(field)
                input.clear()
                input.send_keys(args[field])
        if region is not None:
            select_region = self.driver.find_element_by_id('region')
            for option in select_region.find_elements_by_tag_name('option'):
                if option.get_attribute("value") == region:
                    option.click()
        if communes is not None:
            select_communes = self.driver.find_element_by_id('communes')
            Select(select_communes).select_by_value(communes)
        if category is not None:
            select_category = self.driver.find_element_by_id('category_group')
            for option in select_category.find_elements_by_tag_name('option'):
                if option.get_attribute("value") == category:
                    option.click()

    def fill_insert_page_select_jobs(self, jobs):
        for i in jobs:
            self.driver.find_element_by_id('jc_'+str(i)).click()

    def fill_insert_page_user_info(self, name, email, phone, passwd, is_company = None, create_account = False):
        email_confirm = email
        passwd_ver = passwd
        args = locals()
        for field in self.user_field_ids:
            if not args[field] is None:
                input = self.driver.find_element_by_name(field)
                input.clear()
                input.send_keys(args[field])
        if not create_account:
            self.driver.find_element_by_id('create_account').click()
        if not is_company is None:
            if is_company:
                field = 'c_ad'
            else:
                field = 'p_ad'
            self.driver.find_element_by_id(field).click()
        for field in self.elements_to_click_for_ad_insert:
            if (field == 'accept_conditions'):
                if (not self.driver.find_element_by_id(field).is_selected()):
                    self.driver.find_element_by_css_selector('.icheckbox_minimal-blue ins').click()
            else:
                self.driver.find_element_by_id(field).click()

    def insert_ad(self, region, commune, category, subject, body, price, name, email, phone, passwd, is_company, create_account = False):
        self.go_to_insert_page()
        self.fill_insert_page(region, commune, category, subject, body, price)
        self.fill_insert_page_user_info(name, email, phone, passwd, is_company, create_account)
        self.submit_insert_page()

    def insert_ad_with_account(self, region = None, commune = None, category = None, subject = None, body = None, price = None, email = None, passwd = None):
        self.go_to_insert_page()
        self.login(email, passwd)
        self.fill_insert_page(region, commune, category, subject, body, price)
        self.driver.find_element_by_id("accept_conditions").click()
        self.submit_insert_page()

    def create_ad_and_account(self, region, category, subject, body, price, name, email, email_confirm, phone, passwd, passwd_ver):
        args = locals()
        select_region = self.driver.find_element_by_id('region')
        for option in select_region.find_elements_by_tag_name('option'):
            if option.get_attribute("value") == region:
                option.click()
                break
        select_category = self.driver.find_element_by_id('category_group')
        for option in select_category.find_elements_by_tag_name('option'):
            if option.get_attribute("value") == category:
                option.click()
                break

        for field in self.ad_field_ids:
            input = self.driver.find_element_by_id(field)
            input.clear()
            input.send_keys(args[field])
        for field in self.user_field_ids:
            input = self.driver.find_element_by_id(field)
            input.clear()
            input.send_keys(args[field])
        for field in self.elements_to_click:
            self.driver.find_element_by_id(field).click()

    def upload_image(self,path):
        path = os.path.abspath(os.path.join(conf.REGRESS_FINALDIR,'..',path))
        self.driver.find_element_by_name("file").send_keys(path)

    def check_want_bump(self):
        want_bump_checkbox = self.driver.find_element_by_class_name('icheckbox_minimal-green')
        want_bump_checkbox.click()

    def accept_conditions(self, option=True):
        checkbox = self.driver.find_element_by_id("accept_conditions")
        if checkbox.is_selected() != option:
            checkbox.click()

