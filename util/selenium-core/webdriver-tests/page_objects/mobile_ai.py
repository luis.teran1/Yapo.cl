# -*- coding: latin-1 -*-
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support.expected_conditions import (text_to_be_present_in_element,
                                                            invisibility_of_element_located,
                                                            visibility_of_element_located)
from os import path
import sunbro
import conf
import utils
import sys

class MobileAdForm(sunbro.Page):
    """ Page object for the mobile AI/edit form/page """

    success_message = u"Gracias! Tu aviso ser� revisado y si cumple nuestras reglas ser� publicado en las pr�ximas horas"


    ad_information = sunbro.FindElement(By.ID, 'ad_information')
    region = sunbro.FindElement(By.ID, 'region')
    category = sunbro.FindElement(By.ID, 'category')
    subject = sunbro.FindElement(By.ID, 'subject')
    body = sunbro.FindElement(By.ID, 'body')
    price = sunbro.FindElement(By.ID, 'price')
    type_s = sunbro.FindElement(By.ID, 'type_s')
    type_k = sunbro.FindElement(By.ID, 'type_k')

    # user information
    p_ad = sunbro.FindElement(By.ID, 'p_ad') # pri
    c_ad = sunbro.FindElement(By.ID, 'c_ad') # pro
    name = sunbro.FindElement(By.ID, 'name')
    email = sunbro.FindElement(By.ID, 'email')
    phone = sunbro.FindElement(By.ID, 'phone')
    phone_hidden = sunbro.FindElement(By.ID, 'phone_hidden')
    passwd = sunbro.FindElement(By.ID, 'passwd')
    passwd_ver = sunbro.FindElement(By.ID, 'passwd_ver')
    terms = sunbro.FindElement(By.ID, 'terms')
    submit = sunbro.FindElement(By.ID, 'newad_submit')

    not_my_account = sunbro.FindElement(By.ID,'not_my_account')

    # This is a list of categories that have special or specific fields

    # 1220, 1240, 1260
    communes = sunbro.FindElement(By.ID, 'communes')
    rooms = sunbro.FindElement(By.ID, 'rooms')
    size = sunbro.FindElement(By.ID, 'size')
    garage_spaces = sunbro.FindElement(By.ID, 'garage_spaces')
    condominio = sunbro.FindElement(By.ID, 'condominio')
    estate_type = sunbro.FindElement(By.ID, 'estate_type')
    bathrooms = sunbro.FindElement(By.ID, 'bathrooms')
    new_realestate = sunbro.FindElement(By.ID, 'new_realestate')
    services = sunbro.FindElement(By.ID, 'services')

    # 2020, 2060, 2040
    brand = sunbro.FindElement(By.ID, 'brand')
    model = sunbro.FindElement(By.CSS_SELECTOR, '#model:not([disabled])')
    version = sunbro.FindElement(By.CSS_SELECTOR, '#version:not([disabled])')
    regdate = sunbro.FindElement(By.ID, 'regdate')
    gearbox = sunbro.FindElement(By.ID, 'gearbox')
    fuel = sunbro.FindElement(By.ID, 'fuel')
    cartype = sunbro.FindElement(By.ID, 'cartype')
    mileage = sunbro.FindElement(By.ID, 'mileage')
    cubiccms = sunbro.FindElement(By.ID, 'cubiccms')

    # 5100, 5120, 5140
    condition = sunbro.FindElement(By.ID, 'condition')
    gender = sunbro.FindElement(By.ID, 'gender')

    # 3060
    internal_memory = sunbro.FindElement(By.ID, 'internal_memory')

    err_region = sunbro.FindElement(By.ID, 'param_region')
    err_category = sunbro.FindElement(By.ID, 'param_category')
    err_subject = sunbro.FindElement(By.ID, 'param_subject')
    err_body = sunbro.FindElement(By.ID, 'param_body')
    err_name = sunbro.FindElement(By.ID, 'param_name')
    err_email = sunbro.FindElement(By.ID, 'param_email')
    err_phone = sunbro.FindElement(By.ID, 'param_phone')
    err_communes = sunbro.FindElement(By.ID, 'param_communes')

    modal = sunbro.FindElement(By.CSS_SELECTOR, '.pgwModal')
    modal_message = sunbro.FindElement(By.CSS_SELECTOR, '.pgwModal .pm-message')
    modal_button = sunbro.FindElement(By.CSS_SELECTOR, '.pgwModal .btn.btn-primary')

    # just some random sub-categories
    jc_2 = sunbro.FindElement(By.ID, 'jc_2')
    jc_4 = sunbro.FindElement(By.ID, 'jc_4')
    jc_8 = sunbro.FindElement(By.ID, 'jc_8')
    sct_2 = sunbro.FindElement(By.ID, 'sct_2')
    sct_4 = sunbro.FindElement(By.ID, 'sct_4')
    sct_8 = sunbro.FindElement(By.ID, 'sct_8')

    success_page = sunbro.FindElement(By.CSS_SELECTOR, '.success-info h4')

    def go(self):
        return self._driver.get(utils.MOBILE_URL + "/mai")

    def insert(self, region, category, **kwargs):
        """ It inserts an ad using the mobile web page """

        if kwargs.has_key('data'):
            kwargs = kwargs.get('data')

        select_region = Select(self.region)
        select_region.select_by_value(region)
        if kwargs.has_key('errors'):
           errors = True
           kwargs.pop('errors')
        else: errors = False

        select_category = Select(self.category)
        select_category.select_by_value(category)

        if category == '7040':
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'de trabajo'))
        elif category == '7060':
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'Tipo'))
        elif category in ['1220', '1240', '1260']:
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'Tipo de inmueble'))

        for field, value in kwargs.items():
            element = getattr(self, field)
            if element.tag_name in ['input', 'textarea']:
                if element.get_attribute('type') in ['checkbox', 'radio']:
                    element.click()
                else:
                    element.clear()
                    element.send_keys(value)
            if element.tag_name == 'select':
                select_element = Select(element)
                select_element.select_by_value(value)

        self.terms.click()
        self.submit.click()
        if not errors:
           WebDriverWait(self._driver, 10).until(
                visibility_of_element_located((By.CSS_SELECTOR, '.success-info')))
        return self

    def edit(self, region, category, **kwargs):
        """ It edit an ad using the mobile web page """

        if kwargs.has_key('data'):
            kwargs = kwargs.get('data')

        select_region = Select(self.region)
        select_region.select_by_value(region)

        select_category = Select(self.category)
        select_category.select_by_value(category)

        if category == '7040':
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'de trabajo'))
        elif category == '7060':
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'Tipo'))
        elif category in ['1220', '1240', '1260']:
            WebDriverWait(self._driver, 10).until(text_to_be_present_in_element((By.ID, 'ad_information'), u'Tipo de inmueble'))

        for field, value in kwargs.items():
            element = getattr(self, field)
            if element.tag_name in ['input', 'textarea']:
                if element.get_attribute('type') in ['checkbox', 'radio']:
                    element.click()
                else:
                    element.clear()
                    element.send_keys(value)
            if element.tag_name == 'select':
                select_element = Select(element)
                select_element.select_by_value(value)

        self.submit.click()
        WebDriverWait(self._driver, 5).until(
                visibility_of_element_located((By.CSS_SELECTOR, '.success-info')))
        return self

    def get_image_frames(self):
        iframes = self._driver.find_elements_by_css_selector('iframe.single_upload')
        return [ iframe for iframe in iframes if 'mpu' in iframe.get_attribute('src') ]

    def count_uploaded_images(self):
        image_count = 0
        for frame in self.get_image_frames():
            self._driver.switch_to_frame(frame)
            loaded = self._driver.find_elements_by_css_selector('.loaded[style]')
            if len(loaded) > 0:
                image_count += 1
            self._driver.switch_to_default_content()
        return image_count

    def upload_image(self, image=None):
        if image is None:
            image = path.join(conf.REGRESS_FINALDIR, 'www', 'img', 'TryOutBg.jpg')
        single_images = self.get_image_frames()
        # are we getting always the first one?
        single_image = single_images[-1]
        self._driver.switch_to_frame(single_image)
        image_input = self._driver.find_element_by_css_selector('input.file')
        image_input.send_keys(image)
#        print >> f1, "after: image_input {0}, image {1}".format(image_input, image)
#        f1.close()
        self.wait = WebDriverWait(self._driver, 10) # timeout after 10 seconds
        self.wait.until(lambda b: self.are_image_upload_done())
        self._driver.switch_to_default_content()

    def are_image_upload_done(self):
        """ check if the iframes used with the image upload are not working """

        iframes = self.get_image_frames()
        for iframe in iframes:
            self._driver.switch_to_frame(iframe)
            submitting = self._driver.find_elements_by_css_selector('input#submitting')

            # the submitting input should exists
            if len(submitting) != 0:
                return False

            # but its value should be different of 1
            for sub in submitting:
                if sub.get_attr('value') == '1':
                    return False

        return True

    def remove_image(self, index=0):
        remove_btns = self._driver.find_elements_by_css_selector('input.remove')
        remove_btns[index].click()
        self.modal.find_element_by_css_selector('.pm-buttons .btn-primary').click()
        WebDriverWait(self._driver, 5).until(
                invisibility_of_element_located((By.CSS_SELECTOR, '.pm-message')))

    def do_submit(self, accept_terms=True):
        if accept_terms and not self.terms.is_selected():
            self.terms.click()
        self.submit.click()
