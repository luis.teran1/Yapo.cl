# -*- coding: latin-1 -*-
import unittest
from selenium.common.exceptions import NoSuchElementException
from selenium import webdriver
from selenium.webdriver.support import ui
import conf, time, os, re, quopri
from page_objects import BasePage

class IndexPage(BasePage):
    """
    Page object for the index page
    """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)
    #

    def go_to_default_index(self):
        self.driver.get(self.base_url + "/");
    #
#
