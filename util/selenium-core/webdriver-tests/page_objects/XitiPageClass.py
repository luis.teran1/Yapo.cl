# -*- coding: latin-1 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from page_objects import BasePage
import time, conf, os, glob, warnings

class XitiPage(BasePage):
    """ A page object for xiti testing actions """
    def __init__(self, driver):
        self.driver = driver
        self.mobile_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)

    def enable_local_hit_xiti(self):
        """
        change the url of xiti server to localhost, and enable it
        """
        self.bconf_overwrite('*.*.xiti.document', 'https://' + conf.REGRESS_HOST + ':' + conf.REGRESS_HTTPD_PORT)
        self.bconf_overwrite('*.*.xiti.document_secure', 'https://' + conf.REGRESS_HOST + ':' + conf.REGRESS_HTTPS_PORT)
        self.bconf_overwrite('*.*.xiti.hit_server', '/')
        self.bconf_overwrite('*.*.common.stat_counter.xiti.display', '1')
        self.bconf_overwrite_restart_apache()

    def disable_local_hit_xiti(self):
        """
        change back the xiti server url to default values
        """
        self.bconf_overwrite('*.*.xiti.document', 'https://logws1327')
        self.bconf_overwrite('*.*.xiti.document_secure', 'https://logws1327')
        self.bconf_overwrite('*.*.xiti.hit_server', '.ati-host.net')
        self.bconf_overwrite_restart_apache()

    def enable_xiti_bconf(self):
        self.bconf_overwrite('*.*.common.stat_counter.xiti.display', '1')
        self.bconf_overwrite_restart_apache()

    def disable_xiti_bconf(self):
        self.bconf_overwrite('*.*.common.stat_counter.xiti.display', '0')
        self.bconf_overwrite_restart_apache()

    def clear_hit_xiti_log(self, tag_name = None):
        """Delete the log file if it exists"""
        if tag_name == None:
            os.chdir(conf.REGRESS_FINALDIR + "/logs")
            files = glob.glob ('hit_xiti.*')
            for filepath in files:
                os.remove(filepath)
        else:
            filepath = conf.REGRESS_FINALDIR + "/logs/hit_xiti." + tag_name + ".log"
            if (os.path.exists(filepath)): os.remove(filepath)

    def is_hit_xiti_tag_in_log(self, tag_name, tag_key, tag_value):
        """Get the log file if it exists"""
        warnings.warn("deprecated", DeprecationWarning)
        filepath = conf.REGRESS_FINALDIR + "/logs/hit_xiti." + tag_name + ".log"
        with open(filepath,'r') as f:
            found = False
            for line in f.readlines():
                if tag_key in line:
                    if tag_value in line:
                        found = True
        return found

    def check_xiti_click_tag_values(self, page_name, s2):
        """Check if the values of a xiti tag hit the local server"""
        file_path = conf.REGRESS_FINALDIR + "/logs/hit_xiti." + page_name.replace('::', '-').lower() + ".click.log"
        self.wait_for_file(file_path, 10)
        if not os.path.exists(file_path):
            print 'the file {0} does not exists'.format(file_path)
            return False
        if not (self.find_string_in_file(file_path, "'p' => '{0}'".format(page_name))):
            print 'the "page_name = {0}" was not found:'.format(page_name)
            return False
        if not (self.find_string_in_file(file_path, "'s2' => '{0}'".format(s2))):
            print 'the "s2 = {0}" was not found:'.format(s2)
            return False
        return True
        

    def check_xiti_page_tag_values(self, page_name, page_type, s2=None):
        """Check if the values of a xiti tag hit the local server"""
        file_path = conf.REGRESS_FINALDIR + "/logs/hit_xiti." + page_name.lower() + ".page.log"
        file_json_path = conf.REGRESS_FINALDIR + "/logs/hit_xiti." + page_name.lower() + ".json.log"
        self.wait_for_file(file_path, 10)
        self.wait_for_file(file_json_path, 10)
        if not os.path.exists(file_path):
            print 'the file {0} does not exists'.format(file_path)
            return False
        if not os.path.exists(file_json_path):
            print 'the file {0} does not exists'.format(file_json_path)
            return False
        if not (self.find_string_in_file(file_json_path, "'page_type' => '{0}'".format(page_type))):
            print 'the "page_type = {0}" was not found:'.format(page_type)
            return False
        if s2 is not None and  not (self.find_string_in_file(file_path, "'s2' => '{0}'".format(s2))):
           print 'the "s2 = {0}" was not found:'.format(s2)
           return False
        return True

    def find_string_in_file(self, file_path, string):
        """Find a string in a text file"""
        with open(file_path,'r') as f:
            found = False
            for line in f.readlines():
                if string in line:
                    found = True
        return found

    def wait_for_file(self, file_path, timeout):
        """Wait for file to appear in the disk"""
        lapse = 1
        start_time = time.time()
        while (not (os.path.exists(file_path)) and (time.time() - start_time) < timeout):
            time.sleep(lapse)
        
    def bconf_overwrite(self, key, value):
        """Overwrite a Bconf key"""
        os.system('printf "cmd:bconf_overwrite\nkey:' + key + '\nvalue:' + value + '\ncommit:1\nend\n" | nc localhost ' + conf.REGRESS_TRANS_PORT + ' > /dev/null 2>&1')

    def bconf_overwrite_restart_apache(self):
        """Graceful Apache Restart"""
        os.system('make --quiet -C ' + conf.TOPDIR + ' apache-regress-graceful > /dev/null 2>&1')

