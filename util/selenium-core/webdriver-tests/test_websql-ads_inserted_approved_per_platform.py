# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.ui import Select
import unittest
import time
import os
import conf
from datetime import date,timedelta


class TestWebSqlAdsInsertedApprovedPerPlatform(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(40)
        self.base_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT)
        self.mobile_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.cp_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_00_cp_websql(self):
        driver = self.driver
        driver.get(self.cp_url + "/controlpanel")
        driver.find_element_by_name("username").send_keys("blocket")
        driver.find_element_by_name("cpasswd").send_keys("blocket")
        driver.find_element_by_name("login").click()
        driver.find_element_by_partial_link_text("SQL").click()
        Select(driver.find_element_by_name("query")).select_by_value("85")
        driver.find_element_by_name("load").click()
        time.sleep(2)
        driver.find_element_by_name("timestamp_from").clear()
        driver.find_element_by_name("timestamp_from").send_keys('2014-03-24 00:00:00')
        driver.find_element_by_name("timestamp_to").clear()
        driver.find_element_by_name("timestamp_to").send_keys('2014-03-25 00:00:00')
        time.sleep(2)
        driver.find_element_by_name('run').click()
        date_init = str(date(2014,2,1))
        websql_res=[['Fecha','inserted web','inserted msite','inserted android','inserted ios','approved web','approved msite','approved android','approved ios'],
                    ['2014-03-25 00:00:00']+['0']*8,
                    ['2014-03-24 00:00:00','8','8','8','3','4','8','8','3']]
        websql_result_table=driver.find_element_by_class_name('websql_result_table')
        for fila in range(len(websql_res)):
            for col in range(len(websql_res[fila])):
                if fila==0:
                   texto=websql_result_table.find_elements_by_tag_name('tr')[0].find_elements_by_tag_name('th')[col].text
                else: texto=websql_result_table.find_elements_by_tag_name('tr')[fila].find_elements_by_tag_name('td')[col].text
                self.assertEquals(websql_res[fila][col],texto)
        driver.close()

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
