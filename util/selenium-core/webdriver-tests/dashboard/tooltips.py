# coding: latin-1
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from page_objects import AccountPage, InsertAdPage, SeleniumTestCase, AdViewPage, ControlPanelPages
import selenium.webdriver.support.ui as ui
import unittest, time, subprocess, conf, os

class TooltipsTest(SeleniumTestCase):

    def init(self, driver):
        """
        init this thang
        """
        self.email = 'prepaid5@blocket.se'
        self.password = '123123123'
        self.accounts = AccountPage(driver)
        self.ad_view = AdViewPage(driver)

        #values
        self.visits = '6549875'
        self.contacts = '4567878'
        self.ad_id = '8000073'
        self.max_visits = '+10.000'
        self.max_contacts = '+1.000'
        self.max_total_visits = '+100.000'
        self.max_total_contacts = '+10.000'
        self.expected_visits = '6549875'
        self.expected_contacts = '4567878'

    def test_01_basics(self):
        """
        check the tooltips when the ad has lots of visits and contacts
        """
        self.init(self.driver)
        #Login
        self.ad_view.go_to_region_metropolitana()
        self.accounts.login(self.email, self.password);
        #Add views and contacts to my ad
        self.ad_view.redis_send_command('flushall', conf.REGRESS_REDISSTAT_PORT)
        self.ad_view.redis_send_command('zadd VIEW:total ' + self.visits + ' ' + self.ad_id, conf.REGRESS_REDISSTAT_PORT)
        self.ad_view.redis_send_command('zadd MAIL:total ' + self.contacts + ' ' + self.ad_id, conf.REGRESS_REDISSTAT_PORT)

        #Go to dashboard
        self.accounts.go_to_dashboard()

        #locators
        self.ad_visits_loc = self.driver.find_elements_by_css_selector('.amount-large.darktooltip')[0]
        self.ad_contacts_loc = self.driver.find_elements_by_css_selector('.amount-large.darktooltip')[1]
        self.total_visits_loc = self.driver.find_elements_by_css_selector('.status-amount.darktooltip')[0]
        self.total_contacts_loc = self.driver.find_elements_by_css_selector('.status-amount.darktooltip')[1]

        #Check the +10.000 and +1.000 on the ad
        self.assertEqual(self.total_visits_loc.text, self.max_total_visits)
        self.assertEqual(self.total_contacts_loc.text, self.max_total_contacts)

        #Check the +100.000 and +10.000 on the totals
        self.assertEqual(self.ad_visits_loc.text, self.max_visits)
        self.assertEqual(self.ad_contacts_loc.text, self.max_contacts)

        #check tooltips
        ActionChains(self.driver).move_to_element(self.total_visits_loc).perform()
        self.assertEqual(self.driver.find_elements_by_css_selector('.dark-tooltip.dark.medium.north.animated.flipIn')[0].text, self.expected_visits)

        ActionChains(self.driver).move_to_element(self.total_contacts_loc).perform()
        self.assertEqual(self.driver.find_elements_by_css_selector('.dark-tooltip.dark.medium.north.animated.flipIn')[1].text, self.expected_contacts)

        ActionChains(self.driver).move_to_element(self.ad_visits_loc).perform()

        # move_to_element does not scroll to the specified element, so the next line forces it. - UPDATE: maybe not needed anymore?
        # self.ad_visits_loc.click()
        self.assertEqual(self.driver.find_elements_by_class_name('dark-tooltip')[2].text, self.expected_visits)

        hover = ActionChains(self.driver).move_to_element(self.ad_contacts_loc).perform()
        self.ad_contacts_loc.click()
        self.assertEqual(self.driver.find_elements_by_class_name('dark-tooltip')[3].text, self.expected_contacts)
