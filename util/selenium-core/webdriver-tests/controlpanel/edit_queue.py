# -*- coding: utf-8 -*-
""" Module containing the EditQueueTestCase """

from page_objects import SeleniumTestCase
from page_objects import ad_view
from page_objects import ad_edit
from page_objects import ControlPanelPages
from page_objects import control_panel
from page_objects import EditMobile
from page_objects.mobile_ai import MobileAdForm
import utils, conf
from selenium.webdriver.common.by import By

class EditQueueTestCase(SeleniumTestCase):
    """ Edit Queue tests. """

    def test_00_edit(self):
        """ Editing some random ad, and checking that enters into the 'Edit' queue """

        list_id = '8348545'
        new_subject = 'Lorem ipsum dolor sit amet'

        ad_view_page = ad_view.AdViewPage(self.driver, list_id)
        ad_edit_pwd_conf_page = ad_view_page.go_to_edit()
        ad_edit_page = ad_edit_pwd_conf_page.successful_password('11111')
        ad_edit_page.edit({'price': '9990', 'subject': new_subject, 'communes': '217'})

        # go to controlpanel and check stuff
        controlpanel_page = ControlPanelPages(self.driver)
        controlpanel_page.go_to_controlpanel()
        controlpanel_page.login_CP('blocket1', 'blocket')

        cpanel_main_page = control_panel.MainPage(self.driver)
        cpanel_queues_page = cpanel_main_page.go_to_queues()

        # Check that the link shows then real amount of edited ads.
        self.assertEqual(cpanel_queues_page.edit_queue_link.text, '1 Edit')

        cpanel_edit_queue_page = cpanel_queues_page.go_to_queue('edit')

        # Check that the queue contains the edited ads.
        self.assertTrue(new_subject in cpanel_edit_queue_page.title.text)

    def test_02_edit_and_bump(self):
        """ Edit an ad selecting the bump option and see if it is in the 'Edit' queue. """

        utils.restore_db_snap('adqueues')
        utils.rebuild_asearch()

        list_id = '8348545'
        new_subject = 'Lorem ipsum dolor sit amet'

        ad_view_page = ad_view.AdViewPage(self.driver, list_id)
        ad_edit_pwd_conf_page = ad_view_page.go_to_edit()
        ad_edit_page = ad_edit_pwd_conf_page.successful_password('11111')
        ad_edit_page.edit({'price': '9990', 'subject': new_subject, 'want_bump': 'on', 'communes': '217'})

        # go to controlpanel and check stuff
        controlpanel_page = ControlPanelPages(self.driver)
        controlpanel_page.go_to_controlpanel()
        controlpanel_page.login_CP('blocket1', 'blocket')

        cpanel_main_page = control_panel.MainPage(self.driver)
        cpanel_queues_page = cpanel_main_page.go_to_queues()

        # Check that the link shows then real amount of edited ads.
        self.assertEqual(cpanel_queues_page.edit_queue_link.text, '1 Edit')

        cpanel_edit_queue_page = cpanel_queues_page.go_to_queue('edit')

        # Check that the queue contains the edited ads.
        self.assertTrue(new_subject in cpanel_edit_queue_page.title.text)

    def test_04_edit_refused(self):
        """ Edit a refused ad and see if it is in the edit queue. """

        ad_id = '600656'
        new_subject = 'Lorem ipsum dolor sit amet'

        url = self.base_url + '/ai?ca=15_s&id=R{0}_1&xtor=EREC-4'.format(ad_id)
        self.driver.get(url)

        #get email
        email = self.driver.find_element_by_id('email').text
        # Refuse ad
        ad_edit_page = ad_edit.AdEditPage(self.driver)
        ad_edit_page.edit({'price': '9990', 'subject': new_subject, 'accept_conditions': 'on', 'communes': '240'})

        # go to controlpanel and check stuff
        controlpanel_page = ControlPanelPages(self.driver)
        controlpanel_page.go_to_controlpanel()
        controlpanel_page.login_CP('blocket1', 'blocket')

        cpanel_main_page = control_panel.MainPage(self.driver)
        cpanel_queues_page = cpanel_main_page.go_to_queues()

        # Check that the link shows then real amount of edited ads.
        self.assertEqual(cpanel_queues_page.edit_queue_link.text, '0 Edit')

        self.driver.find_element_by_partial_link_text('Controlpanel').click()
        controlpanel_page.search_ad_by_ad_id(ad_id)

        # Check that the queue contains the edited ads.
        self.assertEquals(new_subject,self.driver.find_element_by_partial_link_text('Lorem ipsum dolor sit amet').text)
        self.assertEquals(self.driver.find_element_by_css_selector('label[for="chk_0"]').text,'Unreviewed')

    def _test_05_edit_mobile_add_images(self):
        """ Edit a ad and add images this ad should to go the edit queue."""

        utils.restore_db_snap('adqueues')
        utils.rebuild_asearch()

        list_id = '8349396'

        ad_view_page   = EditMobile(self.driver)
        mobile_ad_form = MobileAdForm(self.driver)
        ad_view_page.go_to_edit_page(list_id)
        #get original subject
        subject_ad = mobile_ad_form.subject.get_attribute('value')
        #upload new images
        self.assertEqual(self.is_element_present(By.CLASS_NAME, 'single_upload'), True)
        for i in range(3): mobile_ad_form.upload_image()
        mobile_ad_form.do_submit()

        # go to controlpanel and check stuff
        controlpanel_page = ControlPanelPages(self.driver)
        controlpanel_page.go_to_controlpanel()
        controlpanel_page.login_CP('blocket1', 'blocket')

        cpanel_main_page = control_panel.MainPage(self.driver)
        cpanel_queues_page = cpanel_main_page.go_to_queues()

        # Check that the link shows then real amount of edited ads.
        self.assertEqual(cpanel_queues_page.edit_queue_link.text, '1 Edit')

        cpanel_edit_queue_page = cpanel_queues_page.go_to_queue('edit')
        # Check that the queue contains the edited ads.
        self.assertEquals(self.driver.find_element_by_class_name('da_title').text,subject_ad)
