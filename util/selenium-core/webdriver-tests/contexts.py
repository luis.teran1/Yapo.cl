from functools import wraps
from selenium import webdriver

def use_selenium(test=None, user_agent=None):
    if test is None:
        return partial(use_selenium, user_agent=user_agent)
    @wraps(test)
    def decorated(self):
        if user_agent:
            profile = webdriver.FirefoxProfile()
            profile.set_preference("general.useragent.override", user_agent)
            wd = webdriver.Firefox(profile)
        else:
            wd = webdriver.Firefox()
        self._wds.append(wd)
        try:
            test(self, wd)
        finally:
            wd.quit()
            self._wds.remove(wd)
    return decorated

