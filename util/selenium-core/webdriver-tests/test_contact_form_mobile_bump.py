# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import ui
from page_objects import XitiPage, SeleniumTestCase
import unittest
import conf
import time
import os.path

class TestContactFormMobileBump(SeleniumTestCase):

    def setUp(self):
        user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"
        super(TestContactFormMobileBump, self).setUp(user_agent)
        self._ad_url = self.mobile_url + '/vi/6394118.htm'

    def test_010_check_popup_bump_page(self):
        browser = self.driver
        # Check the popup window in the bump page
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        self.assertTrue(browser.find_element_by_css_selector('#ct > h1').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('#ct > h1').text, U'Sube tu aviso!')
        browser.find_element_by_css_selector('#ct #more-info-payment').click()
        browser.find_element_by_css_selector('.pm-content #help-payment').click()
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #help-form h1').text, U'Por favor, contáctanos en Atención al Cliente')
        browser.find_element_by_css_selector('.pm-content #name').clear()
        browser.find_element_by_css_selector('.pm-content #email').clear()
        browser.find_element_by_css_selector('.pm-content #support_body').clear()
        browser.find_element_by_css_selector('.pm-content #name').send_keys('Chicken McFly')
        browser.find_element_by_css_selector('.pm-content #email').send_keys('chicken@mcfly.com')
        browser.find_element_by_css_selector('.pm-content #support_body').send_keys('cocorowhaaat....')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()

        self.wait.until(lambda browser: browser.find_elements_by_css_selector('.pm-content #ok-help').pop(0).is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #ok-help').text, U'Ok')
        browser.find_element_by_css_selector('.pm-content #ok-help').click()
        self.assertRaises(NoSuchElementException, lambda: browser.find_elements_by_css_selector('.pm-content #ok-help:visible'))

    def test_020_check_popup_bump_page_accept_payment(self):
        browser = self.driver
        # Check the popup window in the payment page - accepting the payment
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('submit-bill').click()
        browser.find_element_by_name('tbk_ok').click()
        browser.find_element_by_name('tbk_next').click()
        self.assertTrue(browser.find_element_by_css_selector('.title.center').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.title.center').text, '�Felicidades!')
        self.assertEqual(browser.find_element_by_css_selector('.message').text, 'Tu compra se realiz� exitosamente, recibir�s un correo confirmando tu compra')
        self.assertEqual(browser.find_element_by_css_selector('.bottom-info p span a').text, U'contactarnos')
        browser.find_element_by_css_selector('.nohistory a').click()
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #name').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #name').get_attribute('value'), U'Boris Felipe')
        browser.find_element_by_css_selector('.pm-content #name').clear()
        browser.find_element_by_css_selector('.pm-content #email').clear()
        browser.find_element_by_css_selector('.pm-content #support_body').clear()
        browser.find_element_by_css_selector('.pm-content #name').send_keys('Chicken McFly')
        browser.find_element_by_css_selector('.pm-content #email').send_keys('chicken@mcfly.com')
        browser.find_element_by_css_selector('.pm-content #support_body').send_keys('cocorowhaaat....')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()

        # This pause is requiered since the js logic change the layout just after the click
        self.wait.until(lambda browser: browser.find_elements_by_css_selector('.pm-content #ok-help').pop(0).is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #ok-help').text, U'Ok')
        browser.find_element_by_css_selector('.pm-content #ok-help').click()
        self.assertRaises(NoSuchElementException, lambda: browser.find_elements_by_css_selector('.pm-content #ok-help:visible'))

    def test_030_check_popup_bump_page_refuse_payment(self):
        browser = self.driver
        # Check the popup window in the payment page - accepting the payment
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('submit-bill').click()
        browser.find_element_by_name('tbk_voided').click()
        browser.find_element_by_name('tbk_next').click()
        self.assertEqual(browser.find_element_by_css_selector('.warning-top h1').text, U'Oops... La transacción no pudo ser realizada')
        self.assertEqual(browser.find_element_by_id('error-more-info').text, U'Más info')
        self.assertEqual(browser.find_element_by_id('help-payment').text, U'Avísanos')
        browser.find_element_by_id('help-payment').click()
        self.assertEqual(browser.find_element_by_id('name').get_attribute('value'), U'Boris Felipe')
        self.assertEqual(browser.find_element_by_id('submit-help').get_attribute("value"), U'Enviar')
        browser.find_element_by_css_selector('.pm-content #name').clear()
        browser.find_element_by_css_selector('.pm-content #email').clear()
        browser.find_element_by_css_selector('.pm-content #support_body').clear()
        browser.find_element_by_css_selector('.pm-content #name').send_keys('Chicken McFly')
        browser.find_element_by_css_selector('.pm-content #email').send_keys('chicken@mcfly.com')
        browser.find_element_by_css_selector('.pm-content #support_body').send_keys('cocorowhaaat....')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()

        self.wait.until(lambda browser: not browser.find_elements_by_id('submit-help').pop(0).is_displayed())
        self.assertFalse(browser.find_element_by_id('submit-help').is_displayed())
        self.wait.until(lambda browser: browser.find_elements_by_css_selector('.pm-content #ok-help').pop(0).is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #ok-help').text, U'Ok')
        browser.find_element_by_css_selector('.pm-content #ok-help').click()
        self.assertRaises(NoSuchElementException, lambda: browser.find_elements_by_css_selector('.pm-content #ok-help:visible'))

    def test_040_check_contact_form(self):
        browser = self.driver
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('more-info-payment').click()
        browser.find_element_by_css_selector('.pm-content #help-payment').click()
        # Check all the errors in the fom sending the form empty
        browser.find_element_by_css_selector('.pm-content #name').clear()
        browser.find_element_by_css_selector('.pm-content #email').clear()
        browser.find_element_by_css_selector('.pm-content #support_body').clear()
        browser.find_element_by_css_selector('.pm-content #submit-help').click()
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #help-form h1').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-name').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-name').text, U'Escribe tu nombre')
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-email').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-email').text, 'Escribe un e-mail v�lido')
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-body').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-body').text, 'Escribe una descripci�n')
        # Check all the errors fill only the name
        browser.find_element_by_css_selector('.pm-content #name').send_keys('Chicken McFly')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()

        self.assertFalse(browser.find_element_by_css_selector('.pm-content #error-name').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #help-form h1').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-email').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-body').is_displayed())

        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-email').text, 'Escribe un e-mail v�lido')
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-body').text, 'Escribe una descripci�n')
        # Check all the errors fill the name and the email
        browser.find_element_by_css_selector('.pm-content #email').send_keys('chicken@mcfly.com')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #help-form h1').is_displayed())
        self.assertFalse(browser.find_element_by_css_selector('.pm-content #error-name').is_displayed())
        self.assertFalse(browser.find_element_by_css_selector('.pm-content #error-email').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #error-body').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #error-body').text, 'Escribe una descripci�n')
        # Check all the errors fill the name and the email
        browser.find_element_by_css_selector('.pm-content #support_body').send_keys('cocorowhaaat....')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()

        self.wait.until(lambda browser: not browser.find_elements_by_css_selector('.pm-content #help-form h1').pop(0).is_displayed())
        self.assertFalse(browser.find_element_by_css_selector('.pm-content #help-form h1').is_displayed())
        self.assertTrue(browser.find_element_by_css_selector('.pm-content #help-ok-message h1').is_displayed())
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #help-ok-message h1').text, 'Mensaje enviado con �xito')
        self.assertEqual(browser.find_element_by_css_selector('.pm-content #ok-help').text, U'Ok')
        browser.find_element_by_css_selector('.pm-content #ok-help').click()
        # This pause is requiered since the js logic change the layout just after the click
        time.sleep(1)
        self.assertTrue(browser.find_element_by_id('more-info-payment').is_displayed())
        self.assertRaises(NoSuchElementException, lambda: browser.find_elements_by_css_selector('.pm-content:visible'))

    def test_050_check_send_mail(self):
        browser = self.driver
        # clear the mail log file
        logfilepath = conf.REGRESS_FINALDIR + "/logs/mail.txt"
        with open(logfilepath,'w') as f:
            f.write('')
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('more-info-payment').click()
        browser.find_element_by_css_selector('.pm-content #help-payment').click()
        browser.find_element_by_css_selector('.pm-content #name').clear()
        browser.find_element_by_css_selector('.pm-content #email').clear()
        browser.find_element_by_css_selector('.pm-content #support_body').clear()
        browser.find_element_by_css_selector('.pm-content #name').send_keys('Chicken McFly')
        browser.find_element_by_css_selector('.pm-content #email').send_keys('chicken@mcfly.com')
        browser.find_element_by_css_selector('.pm-content #support_body').send_keys('cocorowhaaat....')
        browser.find_element_by_css_selector('.pm-content #submit-help').click()
        # check the mail log file
        time.sleep(2)
        loglines = []
        with open(logfilepath,'r') as f:
            for line in f.readlines():
                loglines.append(line)
        self.assertEqual(loglines[0], '-t -i -r <chicken@mcfly.com>\n')
        self.assertEqual(loglines[1], 'From: =?ISO-8859-1?Q?Chicken_McFly?= <chicken@mcfly.com>\n')
        self.assertEqual(loglines[2], 'Subject: =?ISO-8859-1?Q?M=E1s_informaci=F3n_sobre_bump?=\n')
        self.assertEqual(loglines[3], 'To: <support@yapo.cl>\n')
        self.assertEqual(loglines[4], 'Content-Type: text/plain; charset=ISO-8859-1\n')
        self.assertEqual(loglines[5], 'Content-Transfer-Encoding: quoted-printable\n')
        self.assertEqual(loglines[10], 'cocorowhaaat....\n')

    def _test_060_check_xiti_tags_disabled(self):
        # Disabled because xiti actually always is enabled
        browser = self.driver
        xiti = XitiPage(browser)
        xiti.enable_local_hit_xiti()
        xiti.disable_xiti_bconf()
        xiti.clear_hit_xiti_log('support_lightbox')
        xiti.clear_hit_xiti_log('Bump_more_info_to_support_form')
        xiti.clear_hit_xiti_log('Bump_confirm_to_support_form')
        xiti.clear_hit_xiti_log('Bump_failure_buy_to_support_form')
        # navigate to bump page and click into bump page
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('more-info-payment').click()
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('onclick') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-tag') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-xtn2') is None)
        browser.find_element_by_id('help-payment').click()
        self.wait.until(lambda browser: browser.find_element_by_id('pay-help'))
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log"))
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_more_info_to_support_form.log"))
        # navigate to payment - accept
        browser.find_element_by_id('submit-bill').click()
        browser.find_element_by_name('tbk_ok').click()
        browser.find_element_by_name('tbk_next').click()
        print browser.find_element_by_css_selector('.bottom-info p span a').get_attribute('onclick') is None
        self.assertTrue(browser.find_element_by_css_selector('.bottom-info p span a').get_attribute('onclick') is None)
        self.assertTrue(browser.find_element_by_css_selector('.bottom-info p span a').get_attribute('data-xiti-page-tag') is None)
        self.assertTrue(browser.find_element_by_css_selector('.bottom-info p span a').get_attribute('data-xiti-page-xtn2') is None)
        browser.find_element_by_css_selector('.bottom-info p span a').click()
        time.sleep(1)
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log"))
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_confirm_to_support_form.log"))
        # navigate to payment - cancel
        browser.get(self._ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('submit-bill').click()
        browser.find_element_by_name('tbk_voided').click()
        browser.find_element_by_name('tbk_next').click()
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('onclick') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-tag') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-xtn2') is None)
        browser.find_element_by_id('help-payment').click()
        time.sleep(1)
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log"))
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_failure_buy_to_support_form.log"))
        # navigate to payment - refuse
        browser.get(self.ad_url)
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_id('submit-bill').click()
        browser.find_element_by_name('tbk_cancel').click()
        browser.find_element_by_name('tbk_next').click()
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('onclick') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-tag') is None)
        self.assertTrue(browser.find_element_by_id('help-payment').get_attribute('data-xiti-page-xtn2') is None)
        browser.find_element_by_id('help-payment').click()
        time.sleep(1)
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log"))
        self.assertFalse(os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_failure_buy_to_support_form.log"))

