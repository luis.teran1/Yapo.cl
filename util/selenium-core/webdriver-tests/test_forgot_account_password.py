# -*- coding: latin-1 -*-
import os, re
import conf
from page_objects import SeleniumTestCase
from utils import Utils

class Home(SeleniumTestCase):

    def test_01_validate_ask_email_form_test(self):
        browser = self.driver
        browser.get("https://%s:%s/reset_password" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT))
        # write no email address
        browser.find_element_by_css_selector('fieldset button').click()
        assert browser.find_element_by_id('err_status').text == u'Escribe tu e-mail'
        # invalid email
        browser.find_element_by_id('email').send_keys("asd")
        browser.find_element_by_css_selector('fieldset button').click()
        assert browser.find_element_by_id('err_status').text == u'Confirma que el correo electr�nico est� en el formato correcto.'
        # email with no account
        browser.find_element_by_id('email').send_keys("test@noemail.com")
        browser.find_element_by_css_selector('fieldset button').click()
        assert browser.find_element_by_id('err_status').text == u'La cuenta no existe'

    def test_02_validate_ask_password_form_test(self):
        browser = self.driver
        os.system('echo "set rsd1x_forgot_password_prepaid5@blocket.se test" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT))
        browser.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, "test", "1", "prepaid5@blocket.se"))
        # write no pasword
        browser.find_element_by_css_selector('fieldset button').click()
        self.assertEqual(browser.find_element_by_id('err_status').text,u'Tu contrase�a debe tener un m�nimo de 8 caracteres')
        # write short password
        browser.find_element_by_id('password').send_keys("12345")
        browser.find_element_by_id('password_verification').send_keys("12345")
        browser.find_element_by_css_selector('fieldset button').click()
        self.assertEqual(browser.find_element_by_id('err_status').text,u'Tu contrase�a debe tener un m�nimo de 8 caracteres')
        # write mismatch passwords
        browser.find_element_by_id('password').send_keys("123123124")
        browser.find_element_by_id('password_verification').send_keys("123123125")
        browser.find_element_by_css_selector('fieldset button').click()
        self.assertEqual(browser.find_element_by_id('err_status').text,
                         u'Las contrase�as no coinciden. Por favor int�ntalo de nuevo')

    def test_03_app_navigation_test(self):
        browser = self.driver
        # Check loading with op=1
        os.system('echo "set rsd1x_forgot_password_prepaid5@blocket.se test" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT))
        browser.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, "test", "1", "prepaid5@blocket.se"))
        browser.find_element_by_id('password').send_keys("123123123")
        browser.find_element_by_id('password_verification').send_keys("123123123")
        browser.find_element_by_css_selector('.btn.btn-primary').click()
        self.assertEqual(browser.find_element_by_css_selector('.btn.btn-primary').text,
                         u'Entrar a mi cuenta')
        # Check loading with op=3
        os.system('echo "set rsd1x_forgot_password_prepaid5@blocket.se test" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT))
        browser.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, "test", "3", "prepaid5@blocket.se"))
        browser.find_element_by_id('password').send_keys("123123123")
        browser.find_element_by_id('password_verification').send_keys("123123123")
        browser.find_element_by_css_selector('.btn.btn-primary').click()
        self.assertEqual(browser.find_element_by_css_selector('.btn.btn-primary').text,
                         u'Volver a publicar')
        # Check loading with op=2
        os.system('echo "set rsd1x_forgot_password_prepaid5@blocket.se test" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT))
        browser.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, "test", "2", "prepaid5@blocket.se"))
        browser.find_element_by_id('password').send_keys("123123123")
        browser.find_element_by_id('password_verification').send_keys("123123123")
        browser.find_element_by_css_selector('.btn.btn-primary').click()
        self.assertEqual(browser.find_element_by_css_selector('.btn.btn-primary').text,
                         u'Entrar a mi cuenta')

    def test_04_expired_password_mail_test(self):
        browser = self.driver
        #Try to enter to change password when redis key expired
        browser.get("https://%s:%s/reset_password/ask_password/?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, "test", "1", "prepaid5@blocket.se"))
        self.assertEqual(browser.find_element_by_css_selector('.title-account').text, u'�Olvidaste tu contrase�a?')
        assert browser.find_element_by_css_selector('.text-legend').find_element_by_tag_name('h3').text == u':S Est�s intentando recuperar desde una cuenta que no existe o no est� activa'
        self.assertEqual(browser.find_element_by_css_selector('fieldset button').get_attribute('value'), u'Solicitar cambio de contrase�a')
        #Return to email validation form
        browser.find_element_by_css_selector('fieldset button').click()
        self.assertEqual(browser.find_element_by_css_selector('.control-group').find_element_by_tag_name("p").text,
                         u'Introduce el correo electr�nico que utilizaste para crear tu cuenta y recibir�s un mensaje con el link para cambiar tu contrase�a')

    def test_05_forgot_password_on_ai(self):
        browser = self.driver
        browser.get("%s:%s/ai" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT))
        browser.find_element_by_id('email').send_keys("prepaid5@blocket.se")
        browser.find_element_by_id('email_confirm').send_keys("prepaid5@blocket.se")
        browser.find_element_by_id('passwd').send_keys("321321321")
        browser.find_element_by_id('passwd_ver').send_keys("321321321")
        browser.find_element_by_id('submit_create_now').click()
        self.assertEquals( browser.find_element_by_id('err_passwd').text, u'Contrase\xf1a incorrecta! El correo que ingresaste ya tiene cuenta, utiliza la misma contrase\xf1a para continuar.\n\xbfLa olvidaste?')
        self.assertEquals( browser.find_element_by_css_selector('#err_passwd a').text , u'�La olvidaste?')
        browser.find_element_by_css_selector('#err_passwd a').click()
        self.assertEquals( browser.find_element_by_css_selector('.control-group h3').text, u'Hemos enviado un correo con un link que te permitir� cambiar tu contrase�a!')

    def test_06_redis_hash_test(self):
        browser = self.driver
        os.system('echo "del rsd1x_forgot_password_prepaid5@blocket.se" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT))
        browser.get("https://%s:%s/reset_password" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT))
        browser.find_element_by_id('email').send_keys("prepaid5@blocket.se")
        browser.find_element_by_css_selector('fieldset button').click()

        redis_hash = os.popen('echo "get rsd1x_forgot_password_prepaid5@blocket.se" | %s -p %s' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT)).read().strip()
        self.assertEqual(len(redis_hash), 40)

        browser.get("https://%s:%s/reset_password/ask_password?h=%s&op=%s&email=%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT, redis_hash, '1', 'prepaid5@blocket.se'))
        browser.find_element_by_id('password').send_keys("123123123")
        browser.find_element_by_id('password_verification').send_keys("123123123")
        browser.find_element_by_css_selector('fieldset button').click()

        redis_hash = os.popen('echo "get rsd1x_forgot_password_prepaid5@blocket.se" | %s -p %s ' % (conf.REGRESS_REDIS_DIR, conf.REGRESS_REDISSESSION_PORT)).read().strip()
        self.assertEquals(redis_hash, '', "Expected empty string but got '%s'" % redis_hash)

    def test_07_forgot_password_no_account_with_ads(self):
        browser = self.driver
        browser.get("https://%s:%s/reset_password" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT))
        browser.find_element_by_id("email").send_keys("alan@brito.cl")
        browser.find_element_by_css_selector('fieldset button').click()
        self.assertEqual(browser.find_element_by_id("err_status").text, u"Este mail tiene avisos publicados, pero no una cuenta creada. Para crear una cuenta haz click aqu�")
        self.assertIn("/cuenta", browser.find_element_by_link_text(u"aqu�").get_attribute("href"))

    def test_reset_password_uppercase(self):
        """ Testing that the password is reset when the user enter an email with uppercase """
        browser = self.driver
        self.driver.get(self.secure_url + '/reset_password?op=1')
        email = self.driver.find_element_by_id('email')
        email.clear()
        email.send_keys('PREPAID5@BLOCKET.SE')
        self.driver.find_element_by_css_selector('.btn.btn-primary').click()

        mail_content = Utils().get_mail()
        url = re.findall('https://.+reset_password/ask_password.+', mail_content)[0]
        self.driver.get(url)
        self.driver.find_element_by_id('password').send_keys('123123123')
        self.driver.find_element_by_id('password_verification').send_keys('123123123')
        self.driver.find_element_by_id('password_verification').submit()
        self.assertEqual(
            self.driver.find_element_by_class_name('text-legend').text,
            u':) Tu contrase�a ha sido cambiada con exito!')
