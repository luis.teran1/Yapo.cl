# -*- coding: iso-8859-15 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
import unittest
import conf
import time
import datetime,urllib2
from page_objects import SeleniumTestCase
from page_objects import AdViewPage,BasePage

class TestAlexaCodeDisable(unittest.TestCase):

    _multiprocess_can_split_ = False

    def setUp(self):
        self.base_url        = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.mobile_url      = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.secure_url      = "https://{0}:{1}" . format(conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT) 
        self.bp              = BasePage()
        self.original_value  = self.bp.bconf_get_value("*.*.alexa.enabled")
        self.bp.bconf_overwrite("*.*.alexa.enabled","0")
        self.bp.bconf_overwrite_restart_apache()
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.bp.bconf_overwrite("*.*.alexa.enabled",str(self.original_value))
        self.bp.bconf_overwrite_restart_apache()
        self.driver.close()
        self.driver.quit()

    def verify_not_exits_alexa_code(self):
        script_srcs = [script.get_attribute("src") for script in self.driver.find_elements_by_tag_name("script")]
        #verify that exist src in tag script
        self.assertFalse(u"https://d31qbv1cthcecs.cloudfront.net/atrk.js" in script_srcs)
        self.driver.implicitly_wait(10)
        nosc_imgs = self.driver.execute_script("var noscript = document.getElementsByTagName('noscript');var imgs_src = [];\
                                           for(var i=0;i<noscript.length;i++){\
                                           imgs_src.push(noscript[i].textContent);}return imgs_src;\
                                         ")
        #verify that exist src in tag noscript
        self.assertFalse('<img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=43Iri1asyr00WR" style="display:none" height="1" width="1" alt="" >' in nosc_imgs)

    def test_01_web(self):
        """
        Test Alexa disabled on Desktop
        """
        wait = ui.WebDriverWait(self.driver, 10)
        #home
        self.driver.get(self.base_url)
        wait.until(lambda browser: self.driver.find_element_by_css_selector('.btn-home').is_displayed())
        self.verify_not_exits_alexa_code()
        #web_list
        self.driver.get(self.base_url+"/region_metropolitana")
        self.verify_not_exits_alexa_code()
        #web_vi
        self.driver.get(self.base_url+"/vi/16453216.htm")
        self.verify_not_exits_alexa_code()
        #web_edit_page
        self.driver.get(self.base_url+"/vi/16453216.htm")
        self.driver.find_element_by_id("ad_admin_link_editar").click()
        self.verify_not_exits_alexa_code()
        #web_delete_page
        self.driver.get(self.base_url+"/vi/16453216.htm")
        self.driver.find_element_by_id("ad_admin_link_eliminar").click()
        self.verify_not_exits_alexa_code()
        #web_payment_page_form
        self.driver.get(self.base_url+"/vi/16453216.htm")
        wait.until(lambda browser: self.driver.find_element_by_css_selector('#main_image').is_displayed())
        self.driver.find_element_by_link_text("Subir Ahora").click()
        wait.until(lambda browser: self.driver.find_element_by_css_selector('#submit-bill').is_displayed())
        self.verify_not_exits_alexa_code()
        #web_ai
        self.driver.get(self.base_url+"/ai")
        self.verify_not_exits_alexa_code()
        #web_page_not_found
        self.driver.get(self.base_url+"/holaquehace")
        self.verify_not_exits_alexa_code()
        #web_favorits
        self.driver.get(self.base_url+"/region_metropolitana")
        self.driver.find_element_by_link_text("Favoritos").click()
        self.verify_not_exits_alexa_code()
        #web_help
        self.driver.get(self.base_url+"/ayuda/preguntas_frecuentes.html")
        self.verify_not_exits_alexa_code()
        #web_my_ads
        self.driver.get(self.base_url+"/mis_avisos.html")
        self.verify_not_exits_alexa_code()
        #web_create_account
        self.driver.get(self.secure_url+"/cuenta/form/0")
        self.verify_not_exits_alexa_code()
        #web_login
        self.driver.get(self.base_url+"/login")
        self.verify_not_exits_alexa_code()


    #Begin test of msite
    def test_02_msite(self):
        """
        Test Alexa disabled on Mobile
        """
        wait = ui.WebDriverWait(self.driver, 10)
        self.driver.get(self.mobile_url)
        self.verify_not_exits_alexa_code()
        #msite_list
        self.driver.get(self.mobile_url+"/region_metropolitana")
        self.verify_not_exits_alexa_code()
        #msite_vi
        self.driver.get(self.mobile_url+"/vi/16453216.htm")
        self.verify_not_exits_alexa_code()
        #msite_delete_page
        self.driver.get(self.mobile_url+"/vi/16453216.htm")
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        self.driver.find_element_by_css_selector(".options-panel").click()
        self.driver.find_element_by_css_selector(".option.delete").click()
        self.verify_not_exits_alexa_code()
        #msite_payment_page_form
        self.driver.get(self.mobile_url+"/vi/16453216.htm")
        self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        self.driver.find_element_by_css_selector(".options-panel").click()
        wait.until(lambda browser: self.driver.find_element_by_css_selector('.option.edit').is_displayed())
        self.driver.find_element_by_css_selector(".option.bump").click()
        self.verify_not_exits_alexa_code()
        #msite_ai
        self.driver.get(self.mobile_url+"/mai")
        self.verify_not_exits_alexa_code()
        #msite_page_not_found
        self.driver.get(self.mobile_url+"/holaquehace")
        self.verify_not_exits_alexa_code()
        #msite_my_ads
        self.driver.get(self.mobile_url+"/region_metropolitana")
        wait.until(lambda browser: self.driver.find_element_by_css_selector('.listing-ads_pg').is_displayed())
        self.verify_not_exits_alexa_code()
