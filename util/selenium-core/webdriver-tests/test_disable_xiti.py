# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from page_objects import XitiPage
import unittest
import conf
import time
import os.path
from page_objects import SeleniumTestCase

class DisableXiti(SeleniumTestCase):
	def disable_xiti(self):
		xiti = XitiPage(browser)
		xiti.disable_local_hit_xiti()
		xiti.disable_xiti_bconf()

