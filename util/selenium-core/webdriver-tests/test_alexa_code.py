# -*- coding: iso-8859-15 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.support.ui as ui
import unittest
import conf
import time
import datetime,urllib2
from page_objects import SeleniumTestCase
from page_objects import AdViewPage,BasePage

class TestAlexaCode(SeleniumTestCase):

    _multiprocess_can_split_ = False

    def setUp(self):
        self.base_url        = "https://"+conf.REGRESS_HOST+":"+conf.REGRESS_HTTPD_PORT
        self.mobile_url      = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.secure_url      = "https://{0}:{1}" . format(conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT) 
        self.bp              = BasePage()
        self.original_value  = self.bp.bconf_get_value("*.*.alexa.enabled")
        self.bp.bconf_overwrite("*.*.alexa.enabled","1")
        self.bp.bconf_overwrite_restart_apache()
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)

    def tearDown(self):
        self.bp.bconf_overwrite("*.*.alexa.enabled",str(self.original_value))
        self.bp.bconf_overwrite_restart_apache()
        self.driver.close()
        self.driver.quit()

    def verify_alexa_code(self):
        wait = ui.WebDriverWait(self.driver, 10)
        wait.until(lambda browser: self.driver.find_element_by_xpath('/html/head/script[@src="https://d31qbv1cthcecs.cloudfront.net/atrk.js"]'))
        script_srcs = [script.get_attribute("src") for script in self.driver.find_elements_by_tag_name("script")]
        #verify that exist src in tag script
        self.assertIn(u"https://d31qbv1cthcecs.cloudfront.net/atrk.js",script_srcs)
        #verify response Ok
        self.assertEquals("OK",urllib2.urlopen("https://d31qbv1cthcecs.cloudfront.net/atrk.js").msg)
        self.driver.implicitly_wait(2)
        nosc_imgs = self.driver.execute_script("var noscript = document.getElementsByTagName('noscript');var imgs_src = [];\
                                           for(var i=0;i<noscript.length;i++){\
                                           imgs_src.push(noscript[i].textContent);}return imgs_src;\
                                        ")
        #verify that exist src in tag noscript
        self.assertIn('<img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=43Iri1asyr00WR" style="display:none" height="1" width="1" alt="" >',nosc_imgs)

    def test_01_web(self):
        """
        Test Alexa enabled for Desktop
        """
        wait = ui.WebDriverWait(self.driver, 10)
        #HOME
        self.driver.get(self.base_url)
        self.verify_alexa_code()
        #LISTING
        self.driver.get(self.base_url+"/region_metropolitana")
        self.verify_alexa_code()
        #VI
        self.driver.get(self.base_url+"/vi/16453216.htm")
        self.verify_alexa_code()
        #EDIT
        self.driver.get(self.base_url+"/ai?ca=15_s&id=16453216&cmd=edit")
        self.verify_alexa_code()
        #DELETE
        self.driver.get(self.base_url+"/ai?ca=15_s&id=16453216&cmd=delete")
        self.verify_alexa_code()
        #Payment
        self.driver.get(self.base_url+"/vi/16453216.htm")
        wait.until(lambda browser: self.driver.find_element_by_css_selector('#main_image').is_displayed())
        self.driver.find_element_by_link_text("Subir Ahora").click()
        wait.until(lambda browser: self.driver.find_element_by_css_selector('#submit-bill').is_displayed())
        self.verify_alexa_code()
        #AD INSERT
        self.driver.get(self.base_url+"/ai")
        self.verify_alexa_code()
        # page_not_found:
        self.driver.get(self.base_url+"/holaquehace")
        self.verify_alexa_code()
        #favorits():
        self.driver.get(self.base_url+"/region_metropolitana")
        self.driver.find_element_by_link_text("Favoritos").click()
        self.verify_alexa_code()
        #Help
        self.driver.get(self.base_url+"/ayuda/preguntas_frecuentes.html")
        self.verify_alexa_code()
        #my_ads
        self.driver.get(self.base_url+"/mis_avisos.html")
        self.verify_alexa_code()
        #Create account
        self.driver.get(self.secure_url+"/cuenta/form/0")
        self.verify_alexa_code()
        #Login
        self.driver.get(self.base_url+"/login")
        self.verify_alexa_code()
