# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
import unittest
import conf
from page_objects import SeleniumTestCase

class TestMobileAIUpload(SeleniumTestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)
        self.mobile_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_MOBILE_PORT)
        self.verificationErrors = []
        self.accept_next_alert = True

    def test_02_mai(self):
        driver = self.driver
        driver.get(self.mobile_url + "/")
        driver.find_element_by_link_text("III Atacama").click()
        driver.find_element_by_link_text("Publicar aviso").click()
        driver.find_element_by_css_selector("#region option:nth-child(6)").click()
        driver.find_element_by_css_selector("#category option:nth-child(10)").click()

        self.assertEqual(self.is_element_present(By.CSS_SELECTOR, '.single_upload')[4], False)

        imagename = '/tmp/TryOutBg-%s.jpg' % conf.REGRESS_MOBILE_PORT
        for i in range(3):
            driver.switch_to_frame(driver.find_elements_by_css_selector(".single_upload")[i])
            driver.find_element_by_css_selector("input[type='file']").send_keys(imagename)
            driver.switch_to_default_content();

        self.assertEqual(self.is_element_present(By.CSS_SELECTOR, '.single_upload')[4], True)

        self.is_element_present(By.CSS_SELECTOR, '#brand option')
        driver.find_element_by_css_selector("#brand option[value='44']").click()
        model = u'9000 Griffin'
        WebDriverWait(driver, 10).until(lambda driver: model == driver.find_element_by_css_selector("#model option:nth-child(2)").text)
        driver.find_element_by_css_selector("#model option[value='1']").click()
        version = u'9000 Griffin'
        WebDriverWait(driver, 10).until(lambda driver: version == driver.find_element_by_css_selector("#version option:nth-child(2)").text)
        driver.find_element_by_css_selector("#version option[value='1']").click()
        driver.find_element_by_css_selector("#regdate option[value='2013']").click()
        driver.find_element_by_css_selector("#gearbox option:nth-child(2)").click()
        driver.find_element_by_css_selector("#fuel option:nth-child(2)").click()
        driver.find_element_by_css_selector("#cartype option:nth-child(2)").click()
        driver.find_element_by_css_selector("#mileage").send_keys('1000')
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus luctus, dui quis sollicitudin vulputate, elit elit ullamcorper nunc, a accumsan justo nunc ac urna.")
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("Susana Oria")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("susana.oria@yapo.cl")
        driver.find_element_by_id("phone").clear()
        driver.find_element_by_id("phone").send_keys("31415926")
        driver.find_element_by_id("passwd").send_keys("11111")
        driver.find_element_by_id("passwd_ver").send_keys("11111")
        driver.find_element_by_id("terms").click()
        driver.find_element_by_id("newad_submit").click()
        driver.find_element_by_id("success_ad_insert")

    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True

    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True

    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True

    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
