# -*- coding: latin-1 -*-
from selenium.webdriver.common.by import By
from selenium import webdriver
import unittest
from page_objects import InsertAdPage, SeleniumTestCase
import conf

class TestUploadImageDesktopDav(SeleniumTestCase):

    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_dav_image_upload(self):
        """ Test dav in desktop AI  """
        # Go to Insert Ad page
        ia = InsertAdPage(self.driver)
        ia.go_to_insert_page()
        # Select Cars category
        ia.fill_insert_page(region = '3', category = '5040', subject = 'Testing image insertion with Ajax', body = 'Body of the ajax image insertion test', price = '1000')
        ia.fill_insert_page_user_info(name = 'Selenium Tester', email = 'selenium_tester@schibstediberica.es', phone = '9876543210', passwd = '11111', is_company = False, create_account = False)

        ia.upload_image('/tmp/TryOutBg-{0}.jpg'.format(conf.REGRESS_HTTPD_PORT))

        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, '#uploaded_images li:nth-child(1)'))
        self.driver.find_element_by_id('submit_preview').click()
        self.driver.find_element_by_id('edit_ad').click()

        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, 'img[alt="image"]'))
        self.assertTrue(self.is_element_present(By.CSS_SELECTOR, '#uploaded_images li:nth-child(1)'))

        self.driver.find_element_by_css_selector('#uploaded_images li a.bt_delete').click()
        self.assertFalse(self.is_element_present(By.CSS_SELECTOR, '#uploaded_images li'))

        ia.fill_insert_page_user_info(name = 'Selenium Tester', email = 'selenium_tester@schibstediberica.es', phone = '9876543210', passwd = '11111', is_company = False, create_account = True)
        self.driver.find_element_by_id('submit_preview').click()
        self.assertFalse(self.is_element_present(By.CLASS_NAME, 'main_photo'))
        self.driver.find_element_by_id('submit_create_t_prv').click()
        self.assertEqual(self.driver.find_element_by_tag_name('h2').text, '¡Gracias!')

        # Close the browser
        self.driver.close()
