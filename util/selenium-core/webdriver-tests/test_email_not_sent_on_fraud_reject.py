# coding: latin-1
from selenium import webdriver
from page_objects import SeleniumTestCase, InsertAdPage, ControlPanelNewRefusalReasons, ControlPanelPages
import os, conf

class RejectionByFraudEmail(SeleniumTestCase):

    # This suite must use accounts db snapshot
    def test_email_not_sent_on_reject_by_fraud(self):
        """
        This method test the special case of rejection when the reason
        is fraud, the functionallity has been broken many times on prod.
        The email shouldn't been sent.
        """
        new_reason_page = ControlPanelNewRefusalReasons(self.driver)
        new_reason_page.add_refusal_reason() # < this method has default values
        insert = InsertAdPage(self.driver)
        self.driver.get(self.ai_url)
        insert.insert_ad('15', '310','5020','cuchillo de ceramica','esta en perfectas condiciones','123000','test','diego@schibsted.cl','123123123','123123123',False, True)
        os.popen("cat /dev/null >"+ conf.REGRESS_FINALDIR + "/logs/mail.txt")
        cp = ControlPanelPages(self.driver)
        cp.go_to_controlpanel()
        cp.search_for_ad('diego@schibsted.cl', 'Cuchillo de ceramica')
        cp.review_ad(None, 'Fraude')
        email_size = os.stat(conf.REGRESS_FINALDIR + "/logs/mail.txt").st_size
        self.assertTrue(email_size == 0)
