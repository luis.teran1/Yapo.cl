# -*- coding: latin-1 -*-
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import ui
import conf, os, quopri
import subprocess
import datetime

class Utils:
    """
    Base page for the page objects
    """

    def get_mail(self):
        mail_path = os.path.join(conf.REGRESS_FINALDIR, 'logs', 'mail.txt')
        f = open(mail_path)
        t = f.read()
        decoded_mail = quopri.decodestring(t)
        return decoded_mail

    def decode_mail_log_to_text(self):
        os.system('make -C ' + conf.TOPDIR + '/regress/final decode-mail' )

    def get_link_from_mail_log(self, text_to_find):
        self.decode_mail_log_to_text()
        mydriver = webdriver.Firefox()
        mydriver.get (self.base_url + "/selenium-core/getMail.php")
        os.system('cp ' + conf.REGRESS_FINALDIR + '/logs/mail.txt.old ' + conf.REGRESS_FINALDIR + '/logs/mail.txt' )
        link = mydriver.find_element_by_partial_link_text(text_to_find).text
        mydriver.quit()
        return link

    def rebuild_asearch(self):
        """
        make rebuild asearch
        """
        subprocess.call(["make", "--quiet", "-C", conf.TOPDIR, "rebuild-asearch"], stdout=open(os.devnull, 'wb'))

    def restore_db_snap(self, snap_name):
        """
        make db-restore-snap
        """
        snap_name = "db-lrestore-"+snap_name
        subprocess.call(["make", "--quiet", "-C", conf.TOPDIR + "/regress/final", snap_name], stdout=open(os.devnull, 'wb'))

    def get_actual_date(self):
        diasemana = {'MONDAY':'Lunes','TUESDAY':'Martes','WEDNESDAY':'Miercoles','THURSDAY':'Jueves','FRIDAY':'Viernes','SATURDAY':'Sabado','SUNDAY':'Domingo'}
        mes = {'JANUARY':'Enero','FEBRUARY':'Febrero','MARCH':'Marzo','APRIL':'Abril','MAY':'Mayo','JUNE':'Junio','JULY':'Julio','AUGUST':'Agosto','SEPTEMBER':'Septiembre','OCTOBER':'Octubre','NOVEMBER':'Noviembre','DECEMBER':'Diciembre'}
        dic = {}
        dic['fecha'] = datetime.date.today()
        dic['fechora'] = datetime.datetime.today()
        dic['hms'] = dic['fechora'].time().__str__().split('.')[0]
        dic['hm'] = dic['hms'].split(':')[0]+':'+dic['hms'].split(':')[1]
        dic['h'] = dic['hms'].split(':')[0]
        dic['dia'] = diasemana[dic['fecha'].strftime('%A').upper()]
        dic['mes'] = mes[dic['fecha'].strftime('%B').upper()]
        return dic

MOBILE_URL = "https://{host}:{port}".format(host=conf.REGRESS_HOST, port=conf.REGRESS_MOBILE_PORT)
SECURE_URL = "https://{host}:{port}".format(host=conf.REGRESS_HOST, port=conf.REGRESS_HTTPS_PORT)

def restore_db_snap(snap_name):
    """
    make db-restore-snap
    """
    snap_name = "db-lrestore-"+snap_name
    subprocess.call(["make", "--quiet", "-C", conf.TOPDIR + "/regress/final", snap_name], stdout=open(os.devnull, 'wb'))

def load_db_snap(snap_name):
    """
    make db-load-snap
    """
    snap_name = "db-load-"+snap_name
    subprocess.call(["make", "--quiet", "-C", conf.TOPDIR,snap_name], stdout=open(os.devnull, 'wb'))

def rebuild_asearch():
    """
    make rebuild asearch
    """
    subprocess.call(["make", "--quiet", "-C", conf.TOPDIR, "rebuild-asearch"], stdout=open(os.devnull, 'wb'))
 
def check_output(*popenargs, **kwargs):
    """Run command with arguments and return its output as a byte string.
    Backported from Python 2.7 as it's implemented as pure python on stdlib.
    >>> check_output(['/usr/bin/python', '--version'])
    Python 2.6.2
    """
    process = subprocess.Popen(stdout=subprocess.PIPE,shell=True, *popenargs, **kwargs)
    output, unused_err = process.communicate()
    retcode = process.poll()
    if retcode:
        cmd = kwargs.get("args")
        if cmd is None:
            cmd = popenargs[0]
        error = subprocess.CalledProcessError(retcode, cmd)
        error.output = output
        raise error
    return output

def trans(cmd, commit=1, **kwargs):
    kwargs.update({'cmd': cmd, 'commit': commit})
    q = '\n'.join(map(lambda v: v[0] + ':' + str(v[1]), kwargs.items())) + '\nend\n'
    out = check_output(['printf "{query}" | nc localhost {port}'.format(query=q, port=conf.REGRESS_TRANS_PORT)])
    result = {}
    for line in out.splitlines():
        s = line.split(':', 1)
        if(len(s) >= 2):
            result[s[0]] = s[1]
    return result

def is_element_present(self, how, what):
    try: self.find_element(by=how, value=what)
    except NoSuchElementException, e: return False
    return True
