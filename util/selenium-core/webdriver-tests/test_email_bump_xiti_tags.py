# -*- coding: iso-8859-15 -*-
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
import unittest
import conf
import time
from page_objects import BasePage
from functools import wraps, partial

def use_selenium(test=None, user_agent=None):
    if test is None:
        return partial(use_selenium, user_agent=user_agent)
    @wraps(test)
    def decorated(self):
        if user_agent:
            profile = webdriver.FirefoxProfile()
            profile.set_preference("general.useragent.override", user_agent)
            wd = webdriver.Firefox(profile)
        else:
            wd = webdriver.Firefox()
        self._wds.append(wd)
        try:
            test(self, wd)
        finally:
            wd.quit()
            self._wds.remove(wd)
    return decorated


class BumpXITICheck(unittest.TestCase):

    _multiprocess_can_split_ = True
    secure_url = "https://%s:%s" % (conf.REGRESS_HOST, conf.REGRESS_HTTPS_PORT)
    _wds = []

    @classmethod
    def setUpClass(cls):
        cls.bp = BasePage()
        bp = cls.bp
        cls.bp.bconf_overwrite_restart_apache()
        cls.original_value = str(bp.bconf_get_value("*.*.common.stat_counter.xiti.display"))
        cls.host = str(bp.bconf_get_value("*.*.xiti.document_secure"))
        cls.bp.bconf_overwrite("*.*.common.stat_counter.xiti.display","1")
        cls.bp.bconf_overwrite_restart_apache()

    @classmethod
    def tearDownClass(cls):
        cls.bp.bconf_overwrite("*.*.common.stat_counter.xiti.display",cls.original_value)
        cls.bp.bconf_overwrite_restart_apache()
        for wd in cls._wds:
            wd.quit()


class TestxitiOpportunityCustomer(BumpXITICheck):

    @use_selenium
    def test_00_customer(self, wd):
        """Validate presence of xiti tags when an
           email link mail_bump_customer is open
        """
        wd.implicitly_wait(10)
        wd.get(self.secure_url+"/pagos?id=6394118&prod=1&xtor=EREC-36&sl=c")
        img_xiti=WebDriverWait(wd,6).until(lambda elem:wd.find_element_by_class_name("checkout").find_element_by_tag_name("img"))
        self.assertEquals("https://logws1327.ati-host.net/hit.xiti?s=535161&s2=15&p=mail_bump_customer&di=&ac=&an=",img_xiti.get_attribute("src"))

    @use_selenium
    def test_01_opportunity(self, wd):
        """Validate presence of xiti tags when an
           email link mail_bump_opportunity is open
        """
        wd.implicitly_wait(10)
        wd.get(self.secure_url+"/pagos?id=6394118&prod=1&xtor=EREC-37&sl=o")
        img_xiti=WebDriverWait(wd,6).until(lambda elem:wd.find_element_by_class_name("checkout").find_element_by_tag_name("img"))
        self.assertEquals("https://logws1327.ati-host.net/hit.xiti?s=535161&s2=15&p=mail_bump_opportunity&di=&ac=&an=",img_xiti.get_attribute("src"))

    @classmethod
    def setUpClass(cls):
        super(TestxitiOpportunityCustomer, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(TestxitiOpportunityCustomer, cls).tearDownClass()


class TestMobileBumpForm(BumpXITICheck):
    """Checks xiti tags on mobile bump form"""

    @use_selenium(user_agent='iphone')
    def test_00_customer(self, wd):
        """Validate presence of xiti tags when an
           email link mail_bump_customer is open
        """
        wd.implicitly_wait(10)
        wd.get(self.secure_url+"/pagos?id=6394118&prod=1&xtor=EREC-36&sl=c")
        img_xiti=wd.find_element_by_css_selector("html body div#site-wrapper div#ct.payment.grid-wrap img")
        self.assertEquals("https://logws1327.ati-host.net/hit.xiti?s=535498&s2=15&p=mail_bump_customer&di=&ac=&an=",img_xiti.get_attribute("src"))

    @use_selenium(user_agent='iphone')
    def test_01_opportunity(self, wd):
        """Validate presence of xiti tags when an
           email link mail_bump_opportunity is open
        """
        wd.implicitly_wait(10)
        wd.get(self.secure_url+"/pagos?id=6394118&prod=1&xtor=EREC-37&sl=o")
        img_xiti=wd.find_element_by_css_selector("html body div#site-wrapper div#ct.payment.grid-wrap img")
        self.assertEquals("https://logws1327.ati-host.net/hit.xiti?s=535498&s2=15&p=mail_bump_opportunity&di=&ac=&an=",img_xiti.get_attribute("src"))

    @classmethod
    def setUpClass(cls):
        super(TestMobileBumpForm, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(TestMobileBumpForm, cls).tearDownClass()

