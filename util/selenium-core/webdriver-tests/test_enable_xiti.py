# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from page_objects import XitiPage, SeleniumTestCase
import unittest
import conf
import time
import os.path

class TestContactFormMobileBumpXitiEnabled(SeleniumTestCase):
	def enable_xiti(self):
		xiti = XitiPage(browser)
		xiti.enable_local_hit_xiti()
		xiti.enable_xiti_bconf()
