#coding: latin-1
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from time import sleep
import unittest
import os
import conf
from page_objects import SeleniumTestCase

class Home(SeleniumTestCase):

    def test_logout(self):
        self.driver.get("%s:%s/ai" % (conf.REGRESS_HOST, conf.REGRESS_HTTPD_PORT))
        self.login("prepaid5@blocket.se", "123123123")
        self.driver.find_element_by_id("not_my_account").click()

        self.assertFalse(
                len(self.driver.find_elements_by_link_text(u"No eres t�?")) > 0)

    def login(self, email, password):
        self.driver.find_element_by_id("loginInputEmail").send_keys(email)
        self.driver.find_element_by_id("loginInputPassword").send_keys(password)
        self.driver.find_element_by_id("loginInputPassword").submit()


if __name__ == '__main__':
    unittest.main()
