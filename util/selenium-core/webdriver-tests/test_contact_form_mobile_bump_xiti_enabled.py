# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from page_objects import XitiPage, SeleniumTestCase
import unittest
import conf
import time
import os.path

class TestContactFormMobileBumpXitiEnabled(SeleniumTestCase):

    def setUp(self):
        super(TestContactFormMobileBumpXitiEnabled, self).setUp('iphone')

    def test_010_check_xiti_tags(self):
        browser = self.driver
        xiti = XitiPage(browser)
        xiti.enable_local_hit_xiti()
        xiti.enable_xiti_bconf()
        # navigate to bump page and click into bump page
        browser.get(self.mobile_url + "/")
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a').click()
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a/div[2]/h3').click()
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_xpath('//*[@id="more-info-payment"]').click()
        self.assertEqual(browser.find_element_by_xpath('//*[@id="help-payment"]').get_attribute('onclick'), "return xt_click(this,'C','','Bump::bump::bump::contact_us_link','N');")
        browser.find_element_by_xpath('//*[@id="help-payment"]').click()
        time.sleep(1)
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log")
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_more_info_to_support_form.log")
        assert xiti.is_hit_xiti_tag_in_log('support_lightbox', 's2', '11')
        assert not xiti.is_hit_xiti_tag_in_log('support_lightbox', 'clic', 'N')
        assert xiti.is_hit_xiti_tag_in_log('Bump_more_info_to_support_form', 's2', '11')
        assert xiti.is_hit_xiti_tag_in_log('Bump_more_info_to_support_form', 'clic', 'N')
        # navigate to payment - accept
        browser.find_element_by_xpath('//*[@id="submit-bill"]').click()
        browser.find_element_by_xpath('/html/body/form/div/input').click()
        browser.find_element_by_xpath('/html/body/form/input[3]').click()
        assert browser.find_element_by_xpath('/html/body/div/section/div[6]/p/span/a').get_attribute('onclick') == "xt_med('C', '11', 'Bump_confirm_to_support_form', 'N');"
        assert browser.find_element_by_xpath('/html/body/div/section/div[6]/p/span/a').get_attribute('data-xiti-page-tag') == 'support_lightbox'
        assert browser.find_element_by_xpath('/html/body/div/section/div[6]/p/span/a').get_attribute('data-xiti-page-xtn2') == '11'
        xiti.clear_hit_xiti_log('support_lightbox')
        xiti.clear_hit_xiti_log('Bump_confirm_to_support_form')
        browser.find_element_by_xpath('/html/body/div/section/div[6]/p/span/a').click()
        time.sleep(1)
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log")
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_confirm_to_support_form.log")
        assert xiti.is_hit_xiti_tag_in_log('support_lightbox', 's2', '11')
        assert not xiti.is_hit_xiti_tag_in_log('support_lightbox', 'clic', 'N')
        assert xiti.is_hit_xiti_tag_in_log('Bump_confirm_to_support_form', 's2', '11')
        assert xiti.is_hit_xiti_tag_in_log('Bump_confirm_to_support_form', 'clic', 'N')
        # navigate to payment - cancel
        browser.get(self.mobile_url + "/")
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a').click()
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a/div[2]/h3').click()
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_xpath('//*[@id="submit-bill"]').click()
        browser.find_element_by_xpath('/html/body/form/div/input[3]').click()
        browser.find_element_by_xpath('/html/body/form/input[3]').click()
        assert browser.find_element_by_xpath('//*[@id="help-payment"]').get_attribute('onclick') == "xt_med('C', '11', 'Bump_failure_buy_to_support_form', 'N');"
        assert browser.find_element_by_xpath('//*[@id="help-payment"]').get_attribute('data-xiti-page-tag') == 'support_lightbox'
        assert browser.find_element_by_xpath('//*[@id="help-payment"]').get_attribute('data-xiti-page-xtn2') == '11'
        xiti.clear_hit_xiti_log('support_lightbox')
        xiti.clear_hit_xiti_log('Bump_failure_buy_to_support_form')
        browser.find_element_by_xpath('//*[@id="help-payment"]').click()
        time.sleep(1)
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log")
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_failure_buy_to_support_form.log")
        assert xiti.is_hit_xiti_tag_in_log('support_lightbox', 's2', '11')
        assert not xiti.is_hit_xiti_tag_in_log('support_lightbox', 'clic', 'N')
        assert xiti.is_hit_xiti_tag_in_log('Bump_failure_buy_to_support_form', 's2', '11')
        assert xiti.is_hit_xiti_tag_in_log('Bump_failure_buy_to_support_form', 'clic', 'N')
        # navigate to payment - refuse
        browser.get(self.mobile_url + "/")
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a').click()
        browser.find_element_by_xpath('/html/body/div/section/section/ul/li/a/div[2]/h3').click()
        browser.find_element_by_class_name('bump').click()
        browser.find_element_by_xpath('//*[@id="submit-bill"]').click()
        browser.find_element_by_xpath('/html/body/form/div/input[2]').click()
        browser.find_element_by_xpath('/html/body/form/input[3]').click()
        assert browser.find_element_by_xpath('//*[@id="help-payment"]').get_attribute('onclick') == "xt_med('C', '11', 'Bump_failure_buy_to_support_form', 'N');"
        xiti.clear_hit_xiti_log('support_lightbox')
        xiti.clear_hit_xiti_log('Bump_failure_buy_to_support_form')
        browser.find_element_by_xpath('//*[@id="help-payment"]').click()
        time.sleep(1)
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.support_lightbox.log")
        assert os.path.isfile(conf.REGRESS_FINALDIR + "/logs/hit_xiti.Bump_failure_buy_to_support_form.log")
        assert xiti.is_hit_xiti_tag_in_log('support_lightbox', 's2', '11')
        assert not xiti.is_hit_xiti_tag_in_log('support_lightbox', 'clic', 'N')
        assert xiti.is_hit_xiti_tag_in_log('Bump_failure_buy_to_support_form', 's2', '11')
        assert xiti.is_hit_xiti_tag_in_log('Bump_failure_buy_to_support_form', 'clic', 'N')
