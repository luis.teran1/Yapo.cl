# -*- coding: utf-8 -*-
import unittest
from page_objects import SeleniumTestCase
from context import use_selenium
from selenium import webdriver
from selenum.webdriver.support.ui import WebDriverWait
from selenum.webdriver.support import expected_conditions as EC
from selenum.webdriver.common.by import By

class TestAdWatch(SeleniumTestCase):

    fixture = 'fads-and-stores-1030'

    def save_ad(self, wd):
        """Save an ad as favorite"""
        wd.get(self.base_url + '/vi/16376637.htm')
        wd.find_element_by_link_text('Guardar').click()

    def save_search(self, wd):
        """Save search as favorite"""
        self.save_ad(wd)
        wd.get(self.base_url + "/li?ca=11_s&amp;md=li")
        wd.find_element_by_link_text('Nuevo Favorito').click()
        select = wd.find_element_by_id('searcharea_expanded')
        select.select_by_index(0)
        wd.find_element_by_id('searchtext').send_keys('saab')
        wd.find_element_by_id('searchbutton').click()
        wd.find_element_by_xpath("//input[@value='Guardar como Favorito']").click()

    @use_selenium
    def test_save_ad(self, wd):
        self.save_ad(wd)
        msg = WebDriverWait(wd, 10).until(
                EC.presence_of_element_located((By.ID, 'save_ad_msg_ok')))
        self.assertEqual(msg.text == "")


    @use_selenium
    def test_save_search(self, wd):
        self.save_search(wd)
        date = wd.find_element_by_xpath("//table[@id='hl_aw']/tbody/tr[1]/td[1]/span[1]")
        self.assertEqual(date.text, '21 May')
        date = wd.find_element_by_xpath("//table[@id='hl_aw']/tbody/tr[2]/td[1]/span[1]")
        self.assertEqual(date.text, '20 May')

    @use_selenium
    def test_show_images(self, wd):
        self.save_search(wd)
        self.find_element_by_link_text('Mostrar im�genes').click()
        WebDriverWait(wd, 10).until(EC.presence_of_element_located((
                    By.XPATH, "//img[contains(@src,'thumb')]")))
        ad = wd.find_element_by_xpath("//table[@id='hl_aw']/tbody/tr[1]/td[3]")
        self.assertEqual(ad.text, "Saab 9-3 SE 2,2TiD 5 d�rr*$ 69.900")
        ad = self.find_element_by_xpath("//table[@id='hl_aw']/tbody/tr[2]/td[3]")
        self.assertEqual('Snygg Saab 900 V6 med 17" f�lgar och nya d�ck*$ 20.000')


    @use_selenium
    def test_sort_by_price(self, wd):
        self.save_search(wd)
        self.find_element_by_link_text("Mostrar lo m�s barato primero").click()
        ad = wd.find_element_by_xpath("//table[@id='hl_aw']/tbody/tr[1]/td[3]")
        self.assertEqual(ad.text, "R�d Saab 900 (den nya modellen)*$ 5.000")
