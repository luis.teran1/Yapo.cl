# coding: latin-1
from selenium import webdriver
from page_objects import SeleniumTestCase, BasePage, ControlPanelPages, AccountPage
from page_objects import mobile_ai
import selenium.webdriver.support.ui as ui
import unittest
import time
import conf
import quopri

class Account2Pro(SeleniumTestCase):

    def test_account_to_pro_specific_category(self):
        #Testing that the user is passed to pro, when his sixth ad is accepted (user with account)

        email = 'test4@cuenta.com'
        passwd = '123123123'
        subject = 'Now I am a pro test4'
        data = {'communes': '295', 'rooms': '2', 'subject': subject, 'body': 'lorem ipsum dolor sit amet test4',
            'price': '123000', 'p_ad': True, 'name': 'test 4 cuenta', 'email': email, 'passwd': passwd,
            'passwd_ver': passwd, 'phone': '87654321'}
        region = '15'
        category = '1020'
        listing_url = '/region_metropolitana/departamentos_piezas?ca=15_s&l=0&f=c&w=1&st=a'
        user_ads_subjects = ['Depto 1 test4', 'Depto 2 test4', 'Depto 3 test4', 'Depto 4 test4', 'Depto 5 test4', 'Now I am a pro test4']
        have_account = True

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)


    def test_ads_to_pro_specific_category(self):
        #Testing that the user is passed to pro, when his sixth ad is accepted (user without account) for a specific category

        email = 'usuario04@schibsted.cl'
        passwd = '123123123'
        subject = 'Now I am a pro usuario04'
        data = {'subject': subject, 'body': 'lorem ipsum dolor sit amet user4', 'sct_2': True,
            'price': '123000', 'p_ad': True, 'name': 'Usuario 04', 'email': email, 'passwd': passwd,
            'passwd_ver': passwd, 'phone': '87654321'}
        region = '15'
        category = '7060'
        listing_url = '/region_metropolitana/servicios?ca=15_s&l=0&w=1'
        user_ads_subjects = ['We speak english', 'Fiesta de despedida', u'Para cuidar a los ni�os', 'Aceleramos su Mac', 'Limpiamos su Windows', 'Now I am a pro usuario04']
        have_account = False

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)


    def test_account_to_pro_siblings_category(self):
        #Testing that the user is passed to pro, when his sixth ad is accepted (user with account) and ads in siblings category

        email = 'test5@cuenta.com'
        passwd = '123123123'
        subject = 'Now I am a pro test5'
        data = {'regdate': '2014', 'mileage': '100000', 'cubiccms': '6', 'subject': subject, 'body': 'lorem ipsum dolor sit amet test5',
            'price': '123000', 'p_ad': True, 'name': 'test 5 cuenta', 'email': email, 'passwd': passwd,
            'passwd_ver': passwd, 'phone': '87654321'}
        region = '15'
        category = '2060'
        listing_url = '/region_metropolitana/vehiculos?ca=15_s&l=0&f=c&w=1&st=a'
        user_ads_subjects = ['Now I am a pro test5', 'Aro Standard 2012', 'Moto bkn', 'Camion', 'Parachoque', 'Yate digo', 'Acura Legend 2012']
        have_account = True

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)


    def test_ads_to_pro_siblings_category(self):
        """ Testing that the user is passed to pro, when his sixth ad is accepted (user without account) and ads in siblings category """

        email = 'usuario05@schibsted.cl'
        passwd = '123123123'
        subject = 'Now I am a pro usuario05'
        data = {'communes': '295', 'rooms': '2', 'subject': subject, 'body': 'lorem ipsum dolor sit amet user5',
            'price': '123000', 'p_ad': True, 'name': 'Usuario 05', 'email': email, 'passwd': passwd,
            'passwd_ver': passwd, 'phone': '87654321'}
        region = '15'
        category = '1020'
        listing_url = '/region_metropolitana/inmuebles?ca=15_s&l=0&q=usuario05&w=1&st=a'
        user_ads_subjects = ['Dpto en arriendo usuario05', 'Casa usuario05', 'En ciudad empresarial usuario05', 'Bodega de almacenaje usuario05', 'Predio agricola usuario05', 'Now I am a pro usuario05']
        have_account = False

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def test_account_to_pro_siblings_category_pri_pro_ads(self):
        """
        Testing that the user is passed to pro, when his sixth ad is accepted (user with account) and ads in siblings category
        """

        email = 'test6@cuenta.com'
        passwd = '123123123'
        subject = 'Now I am a pro test6'
        data = {'communes': '295', 'rooms': '2', 'subject': subject, 'body': 'lorem ipsum dolor sit amet user6',
            'price': '123000', 'p_ad': True, 'name': 'Usuario 06', 'email': email, 'passwd': passwd,
            'passwd_ver': passwd, 'phone': '87654321'}
        region = '15'
        category = '1020'
        listing_url = '/region_metropolitana/inmuebles?ca=15_s&l=0&q=test6&f=c&w=1&st=a'
        user_ads_subjects = ['Local test6', 'Dedp test6', 'Depto test6', 'Casaaa test6', 'Depto 3 test6', 'Now I am a pro test6']
        have_account = True

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def _check_account_to_pro(self, *args, **kwargs):
        """ generic functionality checks & asserts """

        # insert the sixth ad
        ad_form = mobile_ai.MobileAdForm(self.driver)
        ad_form.go()
        ad_form.insert(kwargs.get('region'), kwargs.get('category'), **kwargs.get('data'))

        # accept it
        cpanel = ControlPanelPages(self.driver)
        cpanel.go_and_login_CP('admin', '123123123')
        cpanel.search_and_review_ad(kwargs.get('email'), kwargs.get('subject'), 'accept')

        # check pro e-mail
        self._check_email()

        # rebuild-index
        base = BasePage()
        base.rebuild_index()

        # check in listing
        self.driver.get(self.base_url + kwargs.get('listing_url'))
        for subject in kwargs.get('user_ads_subjects'):
            self.assertEqual(self.driver.find_element_by_link_text(subject).text, subject)

        self.driver.close()
        if kwargs.get('have_account'):
            # sign in, account must appear as pro, and in AI must be selected as pro

            profile = webdriver.FirefoxProfile()
            user_agent = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53"
            profile.set_preference("general.useragent.override",user_agent)
            self.driver=webdriver.Firefox(profile)
            account = AccountPage(self.driver)
            account.mobile_login(kwargs.get('email'), kwargs.get('passwd'))
            account.go_to_dashboard_profile(is_mobile = True)
            self.assertEqual(self.driver.find_element_by_id("is_company_c").get_attribute("checked"), u"true")
            self.assertEqual(self.driver.find_element_by_id("is_company_c").get_attribute("disabled"), u"true")
            self.driver.get(self.base_url + '/mai')
            self.assertEqual(self.driver.find_element_by_id("c_ad").get_attribute("checked"), u"true")


    def _check_email(self):
        path_to_mail = conf.REGRESS_FINALDIR + "/logs/mail.txt"
        with open(path_to_mail, 'r') as document:
            raw_email = document.read()
            decoded_email = quopri.decodestring(raw_email)
            self.assertTrue('Felicitaciones, para nosotros eres todo un pro' in decoded_email, 'The email was not sent')
