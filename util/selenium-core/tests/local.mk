INSTALLDIR=/selenium-core/tests

INSTALL_TARGETS = DogFoodTestSuite.html
INSTALL_TARGETS += ErrorCheckingTestSuite.html
INSTALL_TARGETS += FailingTestSuite.html
INSTALL_TARGETS += filter-tests-for-browser.js
INSTALL_TARGETS += GoogleTestSearch.html
INSTALL_TARGETS += GoogleTestSuite.html
INSTALL_TARGETS += PassingTestSuite.html
INSTALL_TARGETS += proxy_injection_meta_equiv_test.js
INSTALL_TARGETS += ShortTestSuite.html
INSTALL_TARGETS += TestAddLocationStrategy.html
INSTALL_TARGETS += TestAlerts.html
INSTALL_TARGETS += TestBasicAuth.html
INSTALL_TARGETS += TestBrowserVersion.html
INSTALL_TARGETS += TestCheckUncheck.html
INSTALL_TARGETS += TestClickBlankTarget.html
INSTALL_TARGETS += TestClick.html
INSTALL_TARGETS += TestClickJavascriptHrefChrome.html
INSTALL_TARGETS += TestClickJavascriptHref.html
INSTALL_TARGETS += TestCommandError.html
INSTALL_TARGETS += TestComments.html
INSTALL_TARGETS += TestConfirmations.html
INSTALL_TARGETS += TestCookie.html
INSTALL_TARGETS += TestCssLocators.html
INSTALL_TARGETS += TestCursorPosition.html
INSTALL_TARGETS += TestDojoDragAndDrop.html
INSTALL_TARGETS += TestDomainCookie.html
INSTALL_TARGETS += TestDragAndDrop.html
INSTALL_TARGETS += TestEditable.html
INSTALL_TARGETS += TestElementIndex.html
INSTALL_TARGETS += TestElementOrder.html
INSTALL_TARGETS += TestElementPresent.html
INSTALL_TARGETS += TestErrorChecking.html
INSTALL_TARGETS += TestEval.html
INSTALL_TARGETS += TestEvilClosingWindow.html
INSTALL_TARGETS += TestFailingAssert.html
INSTALL_TARGETS += TestFailingVerifications.html
INSTALL_TARGETS += TestFocusOnBlur.html
INSTALL_TARGETS += TestFramesClick.html
INSTALL_TARGETS += TestFramesClickJavascriptHref.html
INSTALL_TARGETS += TestFramesNested.html
INSTALL_TARGETS += TestFramesOpen.html
INSTALL_TARGETS += TestFramesSpecialTargets.html
INSTALL_TARGETS += TestFunkEventHandling.html
INSTALL_TARGETS += TestGet.html
INSTALL_TARGETS += TestGetTextContent.html
INSTALL_TARGETS += TestGoBack.html
INSTALL_TARGETS += TestHighlight.html
INSTALL_TARGETS += TestHtmlSource.html
INSTALL_TARGETS += TestImplicitLocators.html
INSTALL_TARGETS += TestJavaScriptAttributes.html
INSTALL_TARGETS += TestJavascriptParameters.html
INSTALL_TARGETS += TestJS-for-loops.html
INSTALL_TARGETS += TestJS-functions.html
INSTALL_TARGETS += TestJS-if-then-else.html
INSTALL_TARGETS += TestJSSuite.html
INSTALL_TARGETS += TestLocators.html
INSTALL_TARGETS += TestModalDialog.html
INSTALL_TARGETS += TestMouseSpeed.html
INSTALL_TARGETS += TestMultiSelect.html
INSTALL_TARGETS += TestOpen.html
INSTALL_TARGETS += TestOpenInTargetFrame.html
INSTALL_TARGETS += TestOpen_SSV_syntax.html
INSTALL_TARGETS += TestPatternMatching.html
INSTALL_TARGETS += TestPause.html
INSTALL_TARGETS += TestPrompt.html
INSTALL_TARGETS += TestProxy.html
INSTALL_TARGETS += TestQuickOpen.html
INSTALL_TARGETS += TestRefresh.html
INSTALL_TARGETS += TestRollup.html
INSTALL_TARGETS += TestSelect.html
INSTALL_TARGETS += TestSelectMultiLevelFrame.html
INSTALL_TARGETS += TestSelectPopUp.html
INSTALL_TARGETS += TestSelectWindow.html
INSTALL_TARGETS += TestSelectWindowTitle.html
INSTALL_TARGETS += TestSetSpeed.html
INSTALL_TARGETS += TestStore.html
INSTALL_TARGETS += TestSubmit.html
INSTALL_TARGETS += TestSuite.html
INSTALL_TARGETS += TestSuite-UserExtensions.html
INSTALL_TARGETS += TestTextWhitespace.html
INSTALL_TARGETS += TestType.html
INSTALL_TARGETS += TestTypeRichText.html
INSTALL_TARGETS += TestUIElementLocators.html
INSTALL_TARGETS += TestUserExtensions.html
INSTALL_TARGETS += TestUseXpathLibrary.html
INSTALL_TARGETS += TestVerifications.html
INSTALL_TARGETS += TestVisibility.html
INSTALL_TARGETS += TestWaitFor.html
INSTALL_TARGETS += TestWaitForNot.html
INSTALL_TARGETS += TestWait.html
INSTALL_TARGETS += TestWaitInPopupWindow.html
INSTALL_TARGETS += TestXPathLocatorInXHtml.html
INSTALL_TARGETS += TestXPathLocators.html

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
