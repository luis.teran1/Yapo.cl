INSTALLDIR=/selenium-core/tests/dogfood/aut

INSTALL_TARGETS = BaseUrl1TestSuite.html
INSTALL_TARGETS += BaseUrl2TestSuite.html
INSTALL_TARGETS += PauseTestSuite.html
INSTALL_TARGETS += TestBaseUrl1.html
INSTALL_TARGETS += TestBaseUrl2.html
INSTALL_TARGETS += TestTimeout.html

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
