INSTALLDIR=/selenium-core/tests/dogfood

INSTALL_TARGETS = DogfoodTestRefreshFrame.html
INSTALL_TARGETS += TestBaseUrl.html
INSTALL_TARGETS += TestBreakPoint.html
INSTALL_TARGETS += TestFailures.html
INSTALL_TARGETS += TestPauseAndResume.html
INSTALL_TARGETS += TestRunFailedTests.html
INSTALL_TARGETS += TestRunSuccessfulTests.html

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
