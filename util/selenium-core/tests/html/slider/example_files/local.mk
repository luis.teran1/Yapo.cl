INSTALLDIR=/selenium-core/tests/html/slider/example_files

INSTALL_TARGETS = addanevent.js
INSTALL_TARGETS += knob2.png
INSTALL_TARGETS += knob.png
INSTALL_TARGETS += slider.css
INSTALL_TARGETS += slider.js
INSTALL_TARGETS += slider-setup.js


include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
