INSTALLDIR=/selenium-core/tests/html/dojo-0.4.0-mini/src/html

INSTALL_TARGETS = color.js
INSTALL_TARGETS += common.js
INSTALL_TARGETS += display.js
INSTALL_TARGETS += iframe.js
INSTALL_TARGETS += layout.js
INSTALL_TARGETS += metrics.js
INSTALL_TARGETS += __package__.js
INSTALL_TARGETS += selection.js
INSTALL_TARGETS += shadow.js
INSTALL_TARGETS += style.js
INSTALL_TARGETS += util.js

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
