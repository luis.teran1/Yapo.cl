INSTALLDIR=/selenium-core/tests/html/dojo-0.4.0-mini/src

INSTALL_TARGETS = a11y.js
INSTALL_TARGETS += AdapterRegistry.js
INSTALL_TARGETS += animation.js
INSTALL_TARGETS += behavior.js
INSTALL_TARGETS += bootstrap1.js
INSTALL_TARGETS += bootstrap2.js
INSTALL_TARGETS += browser_debug.js
INSTALL_TARGETS += crypto.js
INSTALL_TARGETS += data.js
INSTALL_TARGETS += date.js
INSTALL_TARGETS += debug.js
INSTALL_TARGETS += Deferred.js
INSTALL_TARGETS += DeferredList.js
INSTALL_TARGETS += docs.js
INSTALL_TARGETS += dom.js
INSTALL_TARGETS += event.js
INSTALL_TARGETS += experimental.js
INSTALL_TARGETS += flash.js
INSTALL_TARGETS += hostenv_adobesvg.js
INSTALL_TARGETS += hostenv_browser.js
INSTALL_TARGETS += hostenv_dashboard.js
INSTALL_TARGETS += hostenv_jsc.js
INSTALL_TARGETS += hostenv_rhino.js
INSTALL_TARGETS += hostenv_spidermonkey.js
INSTALL_TARGETS += hostenv_svg.js
INSTALL_TARGETS += hostenv_wsh.js
INSTALL_TARGETS += html.js
INSTALL_TARGETS += iCalendar.js
INSTALL_TARGETS += io.js
INSTALL_TARGETS += json.js
INSTALL_TARGETS += lang.js
INSTALL_TARGETS += loader.js
INSTALL_TARGETS += loader_xd.js
INSTALL_TARGETS += math.js
INSTALL_TARGETS += ns.js
INSTALL_TARGETS += profile.js
INSTALL_TARGETS += regexp.js
INSTALL_TARGETS += storage.js
INSTALL_TARGETS += string.js
INSTALL_TARGETS += style.js
INSTALL_TARGETS += svg.js
INSTALL_TARGETS += validate.js

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
