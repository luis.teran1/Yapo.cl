INSTALLDIR=/selenium-core/tests/html/dojo-0.4.0-mini/src/lang

INSTALL_TARGETS += array.js
INSTALL_TARGETS += assert.js
INSTALL_TARGETS += common.js
INSTALL_TARGETS += declare.js
INSTALL_TARGETS += extras.js
INSTALL_TARGETS += func.js
INSTALL_TARGETS += __package__.js
INSTALL_TARGETS += repr.js
INSTALL_TARGETS += type.js

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
