INSTALLDIR=/selenium-core/tests/html/dojo-0.4.0-mini

INSTALL_TARGETS += build.txt
INSTALL_TARGETS += dojo.js
INSTALL_TARGETS += dojo.js.uncompressed.js
INSTALL_TARGETS += iframe_history.html
INSTALL_TARGETS += LICENSE
INSTALL_TARGETS += README

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
