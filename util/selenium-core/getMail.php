<?php if (!isset($_GET['htmlmail'])) { ?>
<html>
	<head>
		<title>Mail</title>
	</head>
<body>
<pre>
<?php
}

if (isset($_GET['filepath']))
	$mailfile = $_GET['filepath'];
else
	$mailfile = "../logs/mail.txt";

$tries=0;
while (!file_exists($mailfile) && $tries < 10) {
	$tries++;
	sleep(1);
}

$str = file_get_contents($mailfile);
if ($str === false)
	print "Couldn't read/find ".$mailfile;
else
	unlink($mailfile);

if (!isset($_GET['htmlmail'])) {
	$arr = preg_split(',(http://[^[:space:]>]*),', $str, -1, PREG_SPLIT_DELIM_CAPTURE);
	$str = '';
	$i = 0;
	foreach ($arr as $s) {
		if ($i & 1)
			$str .= "<a id='link$i' href='$s'>$s</a>";
		else
			$str .= $s;
		$i++;
	}
} else {
	$str = quoted_printable_decode($str);
	if (($htmlpos = strpos ($str, '<html>')) !== FALSE) {
		if (($dtpos = strpos ($str, '<!DOCTYPE')) !== FALSE)
			$htmlpos = $dtpos;
		$str = '<!-- ' . substr($str, 0, $htmlpos) . ' -->' . substr($str, $htmlpos);

		$hpos = strpos($str, '<head>');
		$str = substr($str, 0, $hpos + 6) . substr($str, $hpos + 6);
	}
}

echo $str;
if (isset($_GET['htmlmail']))
	exit(0);
?>
	</pre>
</body>
</html>
