INSTALLDIR=/selenium-core/core

INSTALL_TARGETS = Blank.html
INSTALL_TARGETS += iedoc-core.xml
INSTALL_TARGETS += iedoc.xml
INSTALL_TARGETS += InjectedRemoteRunner.html
INSTALL_TARGETS += postResults.php
INSTALL_TARGETS += RemoteRunner.html
INSTALL_TARGETS += selenium.css
INSTALL_TARGETS += SeleniumLog.html
INSTALL_TARGETS += selenium-logo.png
INSTALL_TARGETS += selenium-test.css
INSTALL_TARGETS += TestPrompt.html
INSTALL_TARGETS += TestRunner.hta
INSTALL_TARGETS += TestRunner.html
INSTALL_TARGETS += TestRunner-splash.html

include $(TOPDIR)/mk/all.mk
include $(TOPDIR)/mk/install.mk
