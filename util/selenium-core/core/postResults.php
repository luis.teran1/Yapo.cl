<?php
$date = date("Y-m-d H:i:s");
?>

<html>
<head>
	<title>Selenium test suite results <?php echo $date; ?></title>
</head>

<body <?php 
  if (@$_REQUEST['exit'] == 1)
     echo 'onload="window.close();"'; 
  ?>
>
        <h1>Result: <?php echo $date; ?></h1>
	<table border=1>
		<tr>
<?php 
        if (($fh = fopen("log.txt", "wb")) || ($fh = fopen("/tmp/selenium.log", "wb"))) {
		fwrite($fh, "Date:$date\n");
		$input = array("result", "totalTime", "numTestPasses",
			       "numTestFailures", "numCommandPasses",
			       "numCommandErrors", "suite");
		foreach ($input as $res) {
			printf("<tr><td>%-20s </td><td> %s </td></tr>\n",
			       $res, $_REQUEST[$res]);
			fwrite($fh, "$res:$_REQUEST[$res]\n");
		}
		echo "</table>";

		$i = 1;
		while (isset($_REQUEST["testTable_$i"])) {
			$data =  $_REQUEST["testTable_$i"];
                        if ($i > 1) {
                                print "\n<hr>\n\n";
                        }
			print $data;
			$i++;
			if ($_REQUEST["result"] == "failed") {
				fwrite($fh, "$res:$data\n");
			}
		}
		fclose($fh);
	}	

?>
</body>

</html>
