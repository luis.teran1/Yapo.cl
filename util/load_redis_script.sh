#!/bin/bash
port=$1
script=$2
if [[ -z $port || -z $script ]]
then
	echo "Usage: ./load_redis_script.sh <port> <script>"
	exit 1;
fi
redis-cli -p $1 SCRIPT LOAD "$(cat $2)"
exit 0;
