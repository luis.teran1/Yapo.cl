<?
// php translate.php blocket.po translated.html
// php translate.php blocket.po
//
// msgfmt -o blocket.mo output.po
//
// gettext_to_html($po)
// translate($po, $html)

function get_str($str) {
	$str = substr($str, strpos($str, '"') + 1);
	$str = substr($str, 0, strrpos($str, '"'));

	$str = str_replace("\\n", "\n", $str);
	$str = str_replace("\\t", "\t", $str);

	return $str;
}

function get_str_from_html($str) {
	if ($str[0] == "\t")
		$str = substr($str, 1);

	if ($str[strlen($str) - 1] == "\n")
		$str = substr($str, 0, strlen($str) - 1);

	$str = ereg_replace("\n", "\\n", $str);
	$str = ereg_replace("\t", "\\t", $str);
	$str = ereg_replace("\"", "\\\"", $str);

	return '"' . $str . '"' . "\n";
}

/*
 * Takes an translated html file and convert it to php array
 */
function html_to_array($file) {
	$str = file($file);
	$msg = '';
	$array = array();

	for ($block = false, $i = 0; $i < count($str); $i++) {
		if (!$block) {
			if (ereg("^<div id='", $str[$i])) {
				$block = true;
				$msg = '';
				$block_id = substr($str[$i], 9, 40);
			}
		} else if ($block) {
			if (ereg("^</div>", $str[$i])) {
				$array[$block_id] = get_str_from_html($msg);
				$block_id = null;
				$block = false;
			} else {
				$msg .= $str[$i];
			}
		}
	}

	return $array;
}

/*
 * Takes and gettext po file and returns html for translation
 */
function gettext_to_html($po) {
	$gettext = file($po);

	$output = '';

	for ($tag = '', $block = false, $i = 0; $i < count($gettext); $i++) {
		if (!$block) {
			if (ereg("^msgid", $gettext[$i])) {
				$block = true;
				$tag = '';
			}
			$tag .= get_str($gettext[$i]);
		} else if ($block) {
			if (ereg("^msgstr", $gettext[$i])) {
				/* Block DONE */
				$block_id = sha1($tag);
				$output .= "<div id='$block_id'>\n\t";
				$output .= utf8_decode($tag);
				$output .= "\n</div>\n<br />\n<br />\n";
				$block = false;
			} else {
				$tag .= get_str($gettext[$i]);
			}
		}
	}

	return $output;
}

/*
 * Create the translation using original gettext file with translated html
 */
function translate($po, $html) {
	/* Get translations from html file */
	$translated = html_to_array($html);

	/* Load original gettext po file */
	$gettext = file($po);
	$output = '';

	for ($tag = '', $block = false, $i = 0; $i < count($gettext); $i++) {
		if (!$block) {
			$output .= $gettext[$i];
			if (ereg("^msgid", $gettext[$i])) {
				$block = true;
				$block_id = null;
				$tag = '';
			}
			$tag .= get_str($gettext[$i]);
		} else if ($block) {
			if (ereg("^msgstr", $gettext[$i])) {
				/* Block DONE */
				$block_id = sha1($tag);
				$block = false;
				if (isset($translated[$block_id]) && $translated[$block_id] != "")
					$output .= "msgstr " . utf8_encode($translated[$block_id]) . "\n";
				else
					$output .= $gettext[$i];
			} else {
				$output .= $gettext[$i];
				$tag .= get_str($gettext[$i]);
			}
		} else {
			$output .= $gettext[$i];
		}
	}

	return $output;
}

if ($argc == 3) {
	// php translate.php blocket.po translated.html
	print(translate($argv[1], $argv[2]));
} else if ($argc == 2) {
	// php translate.php blocket.po
	print(gettext_to_html($argv[1]));
}
?>
