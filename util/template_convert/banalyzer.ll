%{
#include "util.h"
#include <libintl.h>
#include <locale.h>

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <iconv.h>
#include <libintl.h>
#include <locale.h>
#include <stdio.h>
#include <limits.h>

#define PACKAGE "translated"
#define LOCALEDIR "./locale"
#define LANGUAGE "en_US"

#define STEP_ORIGINAL 0
#define STEP_TRANSLATION 1

#include <sstream>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
//#include "bparse.hh"

#define YY_STACK_USED 1

extern int global_parse_error;

int step = STEP_ORIGINAL;
int last_state = 0;
int vtree_state = 0;
int vtree_level = 0;
int index_state = 0;
int hash_state = 0;
int comment_state = 0; /* Remember last state when in comment */

struct outervar {
	outervar(std::string _str, int _level) : str(_str), level(_level) {}
	std::string str;
	int			level;
};


static int nesting_level = 0;
static int blocktype = 0;
static int expr_level = 0;
extern int yylineno;
FlexLexer *lexer = new yyFlexLexer;
extern char *template_name;
static std::string filter_name;

char *buf = NULL;
int buf_len;
int buf_pos;
int inside = 0; /* inside an html tag */
int trail = 0; /* =1 if the gettext string is unneeded (=punctuation/spaces only) */
int debug = 0;
int html_buf = 0;
int filter = 0;


char *arg_buf = NULL;
int arg_buf_len;
int arg_buf_pos;

char *
iconv_string (const char *str, const char *from_codeset,
	      const char *to_codeset)
{
  char *dest = NULL;
  iconv_t cd;
  char *outp;
  char *p = (char *) str;
  size_t inbytes_remaining = strlen (p);
  /* Guess the maximum length the output string can have.  */
  size_t outbuf_size = (inbytes_remaining + 1) * MB_LEN_MAX;
  size_t outbytes_remaining = outbuf_size - 1; /* -1 for NUL */
  size_t err;
  int have_error = 0;

  if (strcmp (to_codeset, from_codeset) == 0)
    return strdup (str);

  cd = iconv_open (to_codeset, from_codeset);
  if (cd == (iconv_t) - 1)
    return NULL;

  outp = dest = (char *)malloc (outbuf_size);
  if (dest == NULL)
    goto out;

again:
  err = iconv (cd, &p, &inbytes_remaining, &outp, &outbytes_remaining);

  if (err == (size_t) - 1)
    {
      switch (errno)
	{
	case EINVAL:
	  /* Incomplete text, do not report an error */
	  break;

	case E2BIG:
	  {
	    size_t used = outp - dest;
	    size_t newsize = outbuf_size * 2;
	    char *newdest;

	    if (newsize <= outbuf_size)
	      {
		errno = ENOMEM;
		have_error = 1;
		goto out;
	      }
	    if (!(newdest = (char *)realloc (dest, newsize)))
	      {
		have_error = 1;
		goto out;
	      }
	    dest = newdest;
	    outbuf_size = newsize;

	    outp = dest + used;
	    outbytes_remaining = outbuf_size - used - 1;	/* -1 for NUL */

	    goto again;
	  }
	  break;

	case EILSEQ:
	  have_error = 1;
	  break;

	default:
	  have_error = 1;
	  break;
	}
    }

  *outp = '\0';

out:
  {
    int save_errno = errno;

    if (iconv_close (cd) < 0 && !have_error)
      {
	/* If we didn't have a real error before, make sure we restore
	   the iconv_close error below. */
	save_errno = errno;
	have_error = 1;
      }

    if (have_error && dest)
      {
	free (dest);
	dest = NULL;
	errno = save_errno;
      }
  }

  return dest;
}


void 
add_text(const char *text) {
	bufcat(&buf, &buf_len, &buf_pos, "%s", text);
}

void 
flush_text(int mode) {
	char *buf_out = NULL;

	char *punct = " .,/*()\t\n!?:-";
	char *trail_punct = " .,\"()/*\\!?:&\n\t-";
	char *c;
	int first_char = 0;
	int last_char;
	char *literal = NULL;

	if (buf && buf[0]) {
		if (mode == 0 && html_buf == 0) {
			if (debug)
				printf("<mode>");
			printf("%s", buf);
			if (debug)
				printf("</mode>");
		} else if (inside) {
			if (debug)
				printf("<!-- ");
			printf("%s", buf);
			if (debug)
				printf(" -->");
		} else if (trail == 1) {
			if (debug)
				printf("<pre>");
			printf("%s", buf);
			if (debug)
				printf("</pre>");
		} else {
			while( (buf[first_char]) && (c = strchr(punct, buf[first_char]) )) {
				printf("%c", *c);
				first_char++;
			}

			last_char = strlen(buf) - 1;
			while( (last_char >= first_char) && (c = strchr(trail_punct, buf[last_char])) ) {
				last_char--;
			}

			if (last_char >= first_char) {
				asprintf(&literal,"");

				if (step == STEP_ORIGINAL)
					printf("<%_\"");

				while (first_char <= last_char) {

					if (buf[first_char] == '"')
						asprintf(&literal,"%s%s",literal,step == STEP_ORIGINAL ? "\\" : "");


					if (!((buf[first_char] == '\\')&&(first_char!=last_char)&&(buf[first_char+1]!='\\')))
						asprintf(&literal,"%s%c",literal,buf[first_char]);


					first_char++;
				}

				if (literal) {
					if (step == STEP_TRANSLATION) {
						buf_out = iconv_string(literal, "ISO-8859-1", "UTF-8");
						if (buf_out != NULL) {
							literal = NULL;
							literal = iconv_string(gettext(buf_out), "UTF-8", "ISO-8859-1");
							if (literal != NULL) {
								printf("%s", literal);
							} else {
								printf("%s", gettext(buf_out));
							}
						}
					} else if (step == STEP_ORIGINAL)
						printf("%s", literal);
				} 

				if (step == STEP_ORIGINAL)
					printf("\"%>");

				printf("%s", buf + last_char + 1);
			} else 
				printf("%s", buf + last_char + 1);
		}
		buf_pos = 0;
		buf[0] = '\0';
	}
}

void
flush_arg() {
	char *buf_out = NULL;
	char *literal = NULL;
	int len = 0;

	/* XXX ? */
	flush_text(0);

	if (strcmp("", lexer->YYText()) != 0) {
		printf("%s", lexer->YYText());
	} else if (step == STEP_ORIGINAL) {
		bufcat(&arg_buf, &arg_buf_len, &arg_buf_pos, "<%_%s\%>\n", lexer->YYText());
		printf("%s", lexer->YYText());
	} else if (step == STEP_TRANSLATION) {
		asprintf(&literal, "%s", lexer->YYText() + 1);
		if (literal) {
			len = strlen(literal);
			literal[len - 2] = '\0';
			buf_out = iconv_string(literal, "ISO-8859-1","UTF-8");
			if (buf_out != NULL) {
				literal = NULL;
				literal = iconv_string(gettext(buf_out), "UTF-8", "ISO-8859-1");
				if (literal != NULL)
					printf("\"%s\"", literal);
			}
		}
	}
}


#define ECHO { flush_text(0); printf("%s", lexer->YYText()); }
#define ECHO_HTML { flush_text(1); }
#define ECHO_ARG { flush_arg();  }
#define PUSH add_text(lexer->YYText());

%}


%option yylineno
%option noyywrap
%x TMPL TMPLE EXPR FUNC INDEX VTREE COMMENT FILTERS FILTER HASH
ID [a-zA-Z][a-zA-Z0-9_]*
IDGLOB ([a-zA-Z][a-zA-Z0-9_]*)?\*
VTREE_PART [a-zA-Z0-9_]+
INT [0-9]+
OPER (==|!=|<|<=|>|>=)
%%

"<\%|"					{ BEGIN(FILTERS); filter = 1; ECHO; }
<FILTERS,FILTER>"|"			{ nesting_level++; BEGIN(TMPL); blocktype = 2; ECHO; filter = 0; }
<FILTERS>{ID}/"("			{ BEGIN(FILTER); ECHO; }
<FILTERS>{ID}/","			{ BEGIN(FILTERS); ECHO; }
<FILTERS>{ID}				{ BEGIN(FILTERS); ECHO; }
<FILTER>")"				{ BEGIN(FILTERS); ECHO; }
<FILTER>"("				{ ECHO; } /* Eat opening paren */
<FILTERS>","				{ BEGIN(FILTERS); ECHO; } /* comma to separate filters */


"<\%"					{ if (filter == 1) { ECHO; } else { nesting_level++; BEGIN(TMPL); blocktype = 1; ECHO; } }
"%>"					{ ECHO; }
"&[a-zA-Z]+;"				{ if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1; }
"<" 	  				{ ECHO_HTML; PUSH; trail = 0; inside = 1; html_buf = 0; }
">"					{ PUSH; ECHO_HTML; trail = 0; inside = 0; html_buf = 0; }

[ \.;:\n\t\\'"\$=_|+%/()!&-]            { if (!trail) { ECHO_HTML; } trail = 1; PUSH; html_buf = 0; }
(([^ ;:\n\t\\'"\$=_|+%/()!&<>-]*(&[a-zA-Z]+;)*[^ ;:\n\t\\'"\$=_|+%/()!&<>-]*)+([ \.,()/!\n\t-]{1,3})?([&][^a-zA-Z])?)+   { if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1; }
[^;:\n\t\\'"\$=_|+%/()!&-]             { if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1;}

<TMPL,TMPLE>"\%>"			{ nesting_level--; if(!nesting_level) BEGIN(INITIAL); ECHO; }
<TMPL>[ \t\n]+				ECHO;
<TMPL>"<\%|"				{ BEGIN(FILTERS); blocktype = 2; ECHO; }
<TMPL>"<\%"				{ nesting_level++; BEGIN(TMPL); blocktype = 1; ECHO; }
<TMPL>[^(]				{ BEGIN(TMPLE); yyless(0); }
<TMPL,TMPLE>"/*"			{ comment_state = YY_START; BEGIN(COMMENT); ECHO; }
<COMMENT>"*/"				{ BEGIN(comment_state); ECHO; }
<COMMENT>.				ECHO; 
<TMPL,EXPR>"("				{ expr_level++; BEGIN(EXPR); ECHO; }
<EXPR>")"				{ expr_level--; if(expr_level == 0) { BEGIN(TMPLE); } ECHO; }
<EXPR>{OPER}				{ ECHO; }
<EXPR>"=~"				{ ECHO; }
<EXPR>"!"				{ ECHO; }
<EXPR>"?"{ID}				{ ECHO; }
<EXPR>"?"/"["				{ index_state = YY_START; BEGIN(INDEX); ECHO; }
<EXPR>"?"				{ ECHO; }
<FILTER,EXPR,INDEX>{INT}		{ ECHO; }
<EXPR>"&&"				{ ECHO; }
<EXPR>"||"				{ ECHO; }
<EXPR,INDEX>"+"				{ ECHO; }
<EXPR,INDEX>"-"				{ ECHO; }
<EXPR,INDEX>"/"				{ ECHO; }
<EXPR,INDEX>"%"				{ ECHO; }
<FUNC,FILTER,TMPLE,EXPR,VTREE,INDEX>"%"{ID}/"["	{ index_state = YY_START; BEGIN(INDEX); ECHO; }
<FUNC,FILTER,TMPLE,EXPR,VTREE,INDEX>"%"/"["		{ index_state = YY_START; BEGIN(INDEX); ECHO; }
<FUNC,FILTER,TMPLE,EXPR,VTREE,INDEX>@@*/"["		{ index_state = YY_START; BEGIN(INDEX); ECHO; }
<INDEX>"["				{ ECHO; }
<INDEX>"]"/"["				{ ECHO; }
<INDEX>"]"				{ BEGIN(index_state); ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>"%"{ID}	{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>"%"/"$"	{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>[$][$]*\(	{ vtree_level++; if (vtree_level == 1) vtree_state = YY_START; BEGIN(VTREE); ECHO; }
<VTREE>")"				{ vtree_level--; if (vtree_level == 0) BEGIN(vtree_state); ECHO; }
<VTREE>"."				{ ECHO; }
<VTREE>"*"				{ ECHO; }
<VTREE>"@"				{ ECHO; }
<VTREE>"#"				{ ECHO; }
<VTREE>[ \t]				ECHO;
<VTREE>"=="				{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>%%*{IDGLOB}	{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>%#{IDGLOB}	{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX,VTREE>@@*{ID}	{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX>"_\""[^"\n]*["]	{ ECHO; }
<TMPL,TMPLE,EXPR,INDEX,FUNC,VTREE>"&"{ID}/"(" 		{ /* yytext++; */ last_state = YY_START; BEGIN(FUNC); ECHO; }
<FUNC>"("				ECHO;
<FUNC>")"				{ BEGIN(last_state); ECHO; }
<FILTER,FUNC,HASH>[ \t\n]*,[ \t\n]*	{ ECHO; }
<FILTER,FUNC,EXPR,VTREE,HASH>"\""([^"\n]|\\.)*["]	{ ECHO_ARG; }
<FILTER>PH\[				{ hash_state = YY_START; BEGIN(HASH); ECHO; }
<HASH>\]				{ BEGIN(hash_state); ECHO; }
<VTREE>{VTREE_PART}			{ ECHO }

<FILTER,EXPR,FUNC,TMPLE,INDEX>#@{ID} 		{ ECHO; }
<FILTER,EXPR,FUNC,TMPLE,INDEX>#@@* 		{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX>@@*#		{ ECHO; }
<FILTER,FUNC,TMPLE,EXPR,INDEX>@+\*+		{ ECHO; }

<TMPLE>"<\%|"				{ BEGIN(FILTERS); blocktype = 2; ECHO; }
<TMPLE>"<\%"				{ nesting_level++; BEGIN(TMPL); blocktype = 1; ECHO; }
<TMPLE>"::"				{ BEGIN(TMPL); ECHO; }
<TMPLE>[\\]"&[a-zA-Z]+;"			{ if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1;}
<TMPLE>"&[a-zA-Z]+;"				{ if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1;}
<TMPLE>[\\][n&]					ECHO;
<TMPLE>[ \.,"()/!&\n\t-]*"&"{ID}"(" 		{ /* yytext++; */ last_state = YY_START; BEGIN(FUNC); html_buf = 0; ECHO; }
<TMPLE>"<" 	  				{ ECHO_HTML; PUSH; trail = 0; inside = 1; html_buf = 0;}
<TMPLE>">"					{ PUSH; ECHO_HTML; trail = 0; inside = 0; html_buf = 0;}
<TMPLE>[ \.;:\n\t\\'"\$=_|+%/()!&@-]            { if (!trail) { ECHO_HTML; } trail = 1; PUSH; html_buf = 0;}
<TMPLE>(([^ ;:\n\t\\'"\$=_|+%/()!&@<>-]*(&[a-zA-Z]+;)*[^ ;:\n\t\\'"\$=_|+%/()!&@<>-]*)+([ \.,()/!\n\t-]{1,3})?([&][^a-zA-Z])?)+   { if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1;}
<TMPLE>[^;:\n\t\\'"\$=_|+%/()!&-]             { if (trail) { ECHO_HTML; } trail = 0; PUSH; html_buf = 1;}


<TMPLE>"%"   				{ PUSH; }
<TMPL,EXPR,INDEX,COMMENT>[ \t\n]+   	ECHO; /* eat up whitespace */

<*>.|\n				{ if (!trail) { ECHO_HTML; } trail = 1;  PUSH; html_buf = 0; } 
%%

using namespace std;

int
main(int argc, char **argv) {
	if ( !( argc > 1 && argc < 4 ) )
		exit(0);

	ifstream file (argv[1]);
	if (argc == 3 && !strcmp("translation", argv[2]))
		step = STEP_TRANSLATION;

	/* Gettext */
	setlocale(LC_ALL, LANGUAGE);
	bindtextdomain(PACKAGE, LOCALEDIR);
	textdomain(PACKAGE);

	lexer->yylex(&file, &std::cout);
//	printf("\n");
	ECHO;

	/* Argument buffer, text that are arguments in filter/func */
	if (arg_buf)
		printf("%s",  arg_buf);

	return 0;
}
