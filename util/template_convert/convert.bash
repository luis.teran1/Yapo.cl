#!/bin/bash

SCRIPT=$0

function usage () {
   cat <<EOF
Usage: $SCRIPT -p [-h] <template_directory> <html_file>
   -p 1		Extracts text strings from templates in
   		<template_directory> and outputs the text strings
		into <html_file>
   -p 2		Translate templates in <template_directory> with
   		strings from <html_file>
   -h		displays basic help
EOF
   exit 0
}

# Options
while getopts "p:h" opt; do
	case $opt in
	p )  PHASE=$OPTARG ;;
	h )  usage ;;
	\?)  usage ;;
	esac
done
shift $(($OPTIND - 1))

if [ -z $PHASE ] || [ -z $1 ] || [ -z $2 ] ; then
	usage
fi

BASE=`dirname $1`/`basename $1`
FILE=$2

################
################ PHASE 1
################
if [ "${PHASE}" -eq "1" ] ; then
	# Remove old templates
	rm -rf converted/

	# Create new templates with gettext around text blocks
	for file in `find $BASE -name '*tmpl'`
	do
		mkdir -p converted/`basename $BASE`/`dirname ${file/$BASE/}`

		# Create new file with gettext tags
		./convert $file > converted/`basename $BASE`/${file/$BASE/}
	done;

	# Copy make files for all template subdirs 
	for mkfile in `find $BASE -name 'Makefile'`
	do
		cp $mkfile converted/`basename $BASE`/${mkfile/$BASE/}
	done

	# Clean templates
#	find converted/templates/ -name '*.js.tmpl' | xargs rm -f
#	rm -rf converted/templates/admin
#	rm -rf converted/templates/docs/video_campaign_rules.html.tmpl
#	rm -rf converted/templates/docs/priceinfo.html.tmpl
#	rm -rf converted/templates/docs/copyright.html.tmpl
#	rm -rf converted/templates/docs/index.html.tmpl
#	rm -rf converted/templates/docs/cookies.html.tmpl
#	rm -rf converted/templates/docs/nbl.html.tmpl
#	rm -rf converted/templates/docs/skl.html.tmpl
#	rm -rf converted/templates/docs/reklam_sms.html.tmpl
#	rm -rf converted/templates/docs/salj_kontakt.html.tmpl
#	rm -rf converted/templates/ai/newad_payex*
#	rm -rf converted/templates/ai/newad_bj*
#	rm -rf converted/templates/ai/newad_fors*
#	rm -rf converted/templates/mail/export*
#	rm -rf converted/templates/mail/if_*
#	rm -rf converted/templates/mail/insur*
#	rm -rf converted/templates/mail/*market*
#	rm -rf converted/templates/mail/*salesmen*
#	rm -rf converted/templates/card_payment/
#	rm -rf converted/templates/sql/
#	rm -rf converted/templates/stats/
#	rm -rf converted/templates/test/
#	rm -rf converted/templates/marketing/
#	rm -rf converted/templates/news/
#	rm -rf converted/templates/export/
#	rm -rf converted/templates/partner/
#	rm -rf converted/templates/debug/
#	rm -rf converted/templates/paysim/
#	rm -rf converted/templates/docs/leta/
#	rm -rf converted/templates/docs/security/
#	rm -rf converted/templates/docs/trygt/
#	rm -rf converted/templates/support/job*
#	rm -rf converted/templates/support/insur*
#	rm -rf converted/templates/support/*konsume*
#	rm -rf converted/templates/docs/tryggt
#	rm -rf converted/templates/docs/villinte_annons*
#	rm -rf converted/templates/docs/omblocket*
#	rm -rf converted/templates/common/insight*

	# Compile templates
	cp converted_Makefile converted/Makefile
	cp converted_templates_Makefile converted/templates/Makefile
	make -C converted

	# Replace template_gettext with gettext
	find converted/ -name '*.c' > files.txt
	for file in `cat files.txt`
	do
		sed -i -e 's/template_gettext/gettext/g' $file
	done;

	# Extract text to PO file
	xgettext --from-code='ISO-8859-1' -f files.txt -o convert.po

	# Convert PO file to HTML div blocks
	php -q pohtml.php convert.po > $2
fi

################
################ PHASE 2
################
if [ "${PHASE}" -eq "2" ] ; then
	# Clean
	rm -rf translated

	# Convert HTML to PO file
	php -q pohtml.php convert.po $2 > translated.po

	# Create MO file outof PO file
	msgfmt -o translated.mo translated.po
	mkdir -p locale/en_US/LC_MESSAGES
	mv translated.mo locale/en_US/LC_MESSAGES/

	# Create new templates with translated text blocks
	for file in `find $BASE -name '*tmpl'`
	do
		mkdir -p translated/`basename $BASE`/`dirname ${file/$BASE/}`

		# Create new file with gettext tags
		./convert $file "translation" > translated/`basename $BASE`/${file/$BASE/}
	done;

	# Clean templates
#	find translated/templates/ -name '*.js.tmpl' | xargs rm -f
#	rm -rf translated/templates/admin
#	rm -rf translated/templates/docs/video_campaign_rules.html.tmpl
#	rm -rf translated/templates/docs/priceinfo.html.tmpl
#	rm -rf translated/templates/docs/copyright.html.tmpl
#	rm -rf translated/templates/docs/index.html.tmpl
#	rm -rf translated/templates/docs/cookies.html.tmpl
#	rm -rf translated/templates/docs/nbl.html.tmpl
#	rm -rf translated/templates/docs/skl.html.tmpl
#	rm -rf translated/templates/docs/reklam_sms.html.tmpl
#	rm -rf translated/templates/docs/salj_kontakt.html.tmpl
#	rm -rf translated/templates/ai/newad_payex*
#	rm -rf translated/templates/ai/newad_bj*
#	rm -rf translated/templates/ai/newad_fors*
#	rm -rf translated/templates/mail/export*
#	rm -rf translated/templates/mail/if_*
#	rm -rf translated/templates/mail/insur*
#	rm -rf translated/templates/mail/*market*
#	rm -rf translated/templates/mail/*salesmen*
#	rm -rf translated/templates/card_payment/
#	rm -rf translated/templates/sql/
#	rm -rf translated/templates/stats/
#	rm -rf translated/templates/test/
#	rm -rf translated/templates/marketing/
#	rm -rf translated/templates/news/
#	rm -rf translated/templates/export/
#	rm -rf translated/templates/partner/
#	rm -rf translated/templates/debug/
#	rm -rf translated/templates/paysim/
#	rm -rf translated/templates/docs/leta/
#	rm -rf translated/templates/docs/security/
#	rm -rf translated/templates/docs/trygt/
#	rm -rf translated/templates/support/job*
#	rm -rf translated/templates/support/insur*
#	rm -rf translated/templates/support/*konsume*
#	rm -rf translated/templates/docs/tryggt
#	rm -rf translated/templates/docs/villinte_annons*
#	rm -rf translated/templates/docs/omblocket*
#	rm -rf translated/templates/common/insight*
fi
