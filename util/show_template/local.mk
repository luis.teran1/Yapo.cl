TOPDIR?=../..

USE_BUILD=yes
IN_FILES=connection_string.php.in show_template.in php_wrapper.in

INSTALL_TARGETS+=show_template connection_string.php php_wrapper
INSTALLDIR=/bin

install-modules: modules/php_templates.so modules/php_bconf.so
	install ${TOPDIR}/modules/php_templates/php_templates.so modules
	install ${TOPDIR}/modules/php_bconf/php_bconf.so modules

modules/php_templates.so: ${TOPDIR}/modules/php_templates/php_templates.so
	make -C ${TOPDIR}/modules/php_templates all

modules/php_bconf.so: ${TOPDIR}/modules/php_bconf/php_bconf.so
	make -C ${TOPDIR}/modules/php_bconf all

include ${TOPDIR}/mk/all.mk
include ${TOPDIR}/mk/install.mk
include ${TOPDIR}/mk/in.mk
include ${TOPDIR}/mk/clean.mk
include ${TOPDIR}/mk/generate.mk
