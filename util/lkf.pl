#!/usr/bin/perl -w

use blocket::bconf;
use Getopt::Long;

my $configfile = "/opt/blocket/conf/bconf.conf";

GetOptions("configfile=s" => \$configfile);

my $bconf = blocket::bconf::init_conf($configfile);

my %lkf;
my %munics;
my %regs;

my %areas = (
	'18001' => '7',
	'18004' => '9',
	'18005' => '9',
	'18006' => '9',
	'18007' => '9',
	'18009' => '11',
	'18010' => '11',
	'18011' => '11',
	'18013' => '7',
	'18014' => '7',
	'18015' => '4',
	'18017' => '4',
	'18018' => '6',
	'18019' => '6',
	'18021' => '2',
	'18023' => '10',
	'18025' => '10',
	'18027' => '1',
	'18028' => '6',
	'18029' => '1',
	'18031' => '5',
	'18034' => '2',
	'18036' => '2',
	'18038' => '5',
	'18039' => '3',
	'18040' => '8',
	'18041' => '5',
	'18042' => '5',
	'148001' => '2',
	'148004' => '6',
	'148005' => '6',
	'148006' => '5',
	'148007' => '2',
	'148008' => '2',
	'148009' => '5',
	'148010' => '2',
	'148011' => '2',
	'148012' => '3',
	'148013' => '3',
	'148016' => '5',
	'148017' => '1',
	'148018' => '1',
	'148019' => '1',
	'148020' => '1',
	'148021' => '4',
	'148022' => '4',
	'148023' => '4',
	'148024' => '4',
	'148025' => '2',
	'148026' => '6',
	'148027' => '6',
	'148028' => '1',
	'148029' => '1',
	'148030' => '1',
	'148031' => '1',
	'148032' => '6',
	'148033' => '5',
	'148034' => '6',
	'148035' => '1',
	'148036' => '4',
	'148037' => '4',
	'148038' => '5',
	'148039' => '1',
	'148040' => '1',
	'148041' => '4',
	'128001' => '1',
	'128002' => '1',
	'128003' => '1',
	'128004' => '1',
	'128005' => '1',
	'128006' => '1',
	'128007' => '2',
	'128008' => '2',
	'128009' => '5',
	'128010' => '5',
	'128014' => '3',
	'128016' => '2',
	'128017' => '2',
	'128018' => '2',
	'128019' => '4',
	'128021' => '4'
);

while (<>) {
	chomp;
	/^([0-9]+);([0-9]+);([0-9]+);([^;]*)$/ && do {
		$lkf{int($3)} = int($2);
		next;
	};

	/^([0-9]+);([^;]*)$/ && do {
		$munics{int($1)} = $2;
	}
}

while (my ($reg, $regdata) = each(%{$$bconf{'*'}{'common'}{'region'}})) {
	if ($$regdata{'municipality'}) {
		while (my ($munic, $mundata) = each (%{$$regdata{'municipality'}})) {
			my $munname = $$mundata{'name'};
			$regs{$munname} = [$reg, 0, $munic];
		}
	} elsif ($$regdata{'city'}) {
		while (my ($city, $citydata) = each (%{$$regdata{'city'}})) {
			while (my ($munic, $mundata) = each (%{$$citydata{'municipality'}})) {
				my $munname = $$mundata{'name'};
				$regs{$munname} = [$reg, $city, $munic];
			}
		}
	}
}

while (my ($lkfnum, $munnum) = each (%lkf)) {
	my $munname = $munics{$munnum};

	if (!$munname) {
		print STDERR "Municipality name missing for $munnum.\n";
		next;
	}

	my $regdata = $regs{$munname};
	
	if (!$regdata) {
		print STDERR "Region data missing for $munname.\n";
		next;
	}

	my $reg = $$regdata[0];
	my $city = $$regdata[1];
	my $area = $$regdata[2];
	$area .= ":" . $areas{$lkfnum} if $areas{$lkfnum};

	print "*.importads.lkf.$lkfnum.r=$reg\n";
	print "*.importads.lkf.$lkfnum.c=$city\n" if $city > 0;
	print "*.importads.lkf.$lkfnum.a=$area\n";
}
