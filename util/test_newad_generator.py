#!/usr/bin/env python
# coding: iso-8859-1

import sys
import os.path
from optparse import OptionParser

### TODO: 
# implement debug mode if needed
# Add a custom header to .in  files
# svn add files?? That might be too dangerous...

# UPDATES:
## Now it is possible to set various invalid/ignored parameters, just set them into a list, like the '2020' category.
## Added a category parameter, to test only one .

###  Add input parameters: Debug mode, output directory, ...
parser = OptionParser()
parser.add_option("-d", "--debug", dest="debug", action="store_true",
                  help="Enable debug mode")
parser.add_option("-t", "--target",  dest="target_dir", default="./",
                  help="Target directory for test files, usually xxxxx/daemons/trans/test (ending in a slash!!!)")
parser.add_option("-l", "--lang",  dest="lang", default="es",
                  help="Language to be used in ad insertion (if any)")
parser.add_option("-s", "--suite",  dest="testsuite", default="",
                  help="If especified, program outputs a Makefile-like testsuite, for direct embedding into code")
parser.add_option("-c", "--category", dest="category", default="", 
		help="If specified, only will parse the specified category, if it exists.")
(options, args) = parser.parse_args()

# For making life easier
IGNORED_PARAM="ignored_param"
INVALID_PARAM="invalid_param"

# Main construction
category_params = {
	# Real Estate
	"1220" : {
		"errors": { IGNORED_PARAM:["estate_type","rooms"], INVALID_PARAM:["estate_type","rooms","bathrooms","new_realestate","condominio"]},
		"params": { "estate_type":1, "rooms":1, "bathrooms":1,"size":1, "garage_spaces":5, "condominio":10,"new_realestate":1}
	},
	"1240" : {
		"errors": { IGNORED_PARAM:["estate_type","rooms"], INVALID_PARAM:["estate_type","rooms","bathrooms","condominio"]},
		"params": { "estate_type":1, "rooms":1, "bathrooms":1,"size":1, "garage_spaces":5, "condominio":10},
		"type": "u"
	},
	"1260" : {
		"errors": { IGNORED_PARAM:["estate_type","rooms","capacity"], INVALID_PARAM:["estate_type","rooms","bathrooms","services","capacity"]},
        "params": { "estate_type":1,"rooms":1, "bathrooms":1, "garage_spaces":5,"services":1, "capacity":1},
		"type": "u"
	},

	# Vehicles
	"2020" : {
		"errors": { IGNORED_PARAM:["version","cartype", "fuel"] , INVALID_PARAM:["gearbox"]},
        "params": { "brand":1,"model":1,"version":1,"mileage":2, "regdate":2000, "cartype":1, "gearbox":1, "fuel":1},
	},
	"2040" : {
		"errors": { IGNORED_PARAM:["mileage","gearbox"],INVALID_PARAM:["gearbox","mileage"] },
		"params": { "mileage":2, "regdate":2000, "fuel":1, "gearbox":1},

	},
	"2060" : {
		"errors": { IGNORED_PARAM:"cubiccms", INVALID_PARAM:"cubiccms"},
		"params": { "mileage":2, "regdate":2000, "cubiccms":1}  ,
	},
	# Cellphones
	"3060" : {
		"errors": { INVALID_PARAM:"internal_memory"},
		"params": { "internal_memory":1024},
	},
	# House and clothing and stuff ...
	"4020" : {
        "errors": { IGNORED_PARAM:"condition", INVALID_PARAM:"gender"},
		"params": { "condition":1 ,"gender":1},
	},
    # Clothing for moms and kids
	"9020" : {
		"errors": { IGNORED_PARAM:"condition", INVALID_PARAM:"condition"},
		"params": { "condition":1},
	},

    "7020": {
        "errors": { IGNORED_PARAM:"job_category", INVALID_PARAM:"job_category"},
		"params": { "job_category":2},

	},
    "7040": {
		"errors": { IGNORED_PARAM:"job_category", INVALID_PARAM:"job_category"},
		"params": { "job_category":1},
	},
    "7060": {
		"errors": { IGNORED_PARAM:"service_type", INVALID_PARAM:"service_type"},
		"params": { "service_type":3},
	},
	# More categories here...
}


# To leave it open to customization, this should be an input argument...
trans_cmd = "newad"

# Just use whatever, this kind of follows naming scheme...
base_infile_name = "t-%s" % trans_cmd

# Picked up an already exixting one, may want to modify password
example_in_file = "cmd:%s\nregion:11\ncommunes:232\npasswd:$1024$j4jmSAY6cMUmA0ig8d830c8e2785dc4e3588b85d5a7b5b31ff714b82\nname:Automated Test User\nemail:autotest@bomnegocio.com\nphone:817471666\nphone_hidden:1\ncompany_ad:0\nsubject:Test ad.\nbody:Created using a custom infile generator.\nprice:4711\nlang:%s\npay_type:free\ncommit:1\n" % (trans_cmd, options.lang)

# Creates a unit test for a new ad with a missing parameter
def create_ignoredparam_file( category, ignored_param, param_list ):
	infile_name = ("%s%s_%s_missing_%s.in") % (options.target_dir, base_infile_name, category , ignored_param)
	outfile_name = ( "%s%s_%s_missing_%s.out") % (options.target_dir, base_infile_name, category , ignored_param)

	try:
		infile = open(infile_name,"w")
		infile.write(example_in_file)
		infile.write("category:" + str(category)+"\n")

		# Be careful with type...
		if "type" in category_params[category].keys():
			infile.write("type:%s\n" % category_params[category]["type"])
		else:
			infile.write("type:s\n")

		for param in param_list:
			if ignored_param=="estate_type":
				break
			if param==ignored_param:
				continue
			infile.write(param  + ":" + str(param_list[param]) + "\n" )

		# Param list ready, add the 'end' parameter
		infile.write("end\n")
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		infile.close()
		sys.exit(-1)

	# All ok!
	infile.close()	
	infile_list.append(infile_name)

	# In this case, the .out is easily generated
	try :
		outfile = open(outfile_name, "w")
		outfile.write( "220 Welcome.\naccount:0\n%s:ERROR_%s_MISSING\nstatus:TRANS_ERROR\nend\n" % (ignored_param, ignored_param.upper()) )
		outfile.close()
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		outfile.close()
		sys.exit(-1)

# Creates a unit test for a new ad with an invalid parameter
def create_invalidparam_file( category, invalid_param, param_list ):
	infile_name = ("%s%s_%s_invalid_%s.in") % (options.target_dir, base_infile_name, category, invalid_param)
	outfile_name = ("%s%s_%s_invalid_%s.out") % (options.target_dir, base_infile_name, category, invalid_param)

	try:
		infile = open(infile_name,"w")
		infile.write(example_in_file)
		infile.write("category:" + str(category)+"\n")

		# Be careful with type...
		if "type" in category_params[category].keys():
			infile.write("type:%s\n" % category_params[category]["type"])
		else:
			infile.write("type:s\n")

		for param in param_list:
			if param==invalid_param:
				# Most probably invalid in any parameter, save body and subject...
				if param == "zipcode":
					infile.write(param  + ":" + "22222222" + "\n" )
				else:
					infile.write(param  + ":" + "1o1" + "\n" )
			else:
				if invalid_param == "estate_type" and param != "estate_type":
					continue
				infile.write(param  + ":" + str(param_list[param]) + "\n" )

		# Param list ready, add the 'end' parameter
		infile.write("end\n")
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		infile.close()
		sys.exit(-1)

	# All ok!
	infile.close()
	infile_list.append(infile_name)

	# In this case, the .out is easily generated
	try :
		outfile = open(outfile_name, "w")
		if invalid_param == "zipcode":
			outfile.write( "220 Welcome.\naccount:0\n%s:ERROR_%s_NOT_IN_REGION\nstatus:TRANS_ERROR\nend\n" % (invalid_param, invalid_param.upper()) )
		else:
			outfile.write( "220 Welcome.\naccount:0\n%s:ERROR_%s_INVALID\nstatus:TRANS_ERROR\nend\n" % (invalid_param, invalid_param.upper()) )
		outfile.close()
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		outfile.close()
		sys.exit(-1)

def create_goodtest_file(category, param_list):
	infile_name = ("%s%s_%s_good.in") % (options.target_dir, base_infile_name, category )
	outfile_name = ("%s%s_%s_good.out") % (options.target_dir,base_infile_name, category )

	try:
		infile = open(infile_name,"w")
		infile.write(example_in_file)
		infile.write("category:" + str(category)+"\n")

		# Be careful with type...
		if "type" in category_params[category].keys():
			infile.write("type:%s\n" % category_params[category]["type"])
		else:
			infile.write("type:s\n")

		for param in param_list:
			infile.write(param  + ":" + str(param_list[param]) + "\n" )

		# Param list ready, add the 'end' parameter
		infile.write("end\n") 
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		infile.close()
		sys.exit(-1)

	# All ok!
	infile.close()	
	infile_list.append(infile_name)

	# In this case, the .out is easily generated
	try :
		outfile = open(outfile_name, "w")
		outfile.write( "220 Welcome.\naccount:0\nuid:.*\nad_id:.*\naction_id:1\npaycode:.*\norder_id:.*\nstatus:TRANS_OK\nend\n")
		outfile.close()
	except:
		print "Failure creating %s because of: %s " % (infile_name,  sys.exc_info()[0] )
		outfile.close()
		sys.exit(-1)


# On with the show....
if os.path.exists(options.target_dir)==False:
	print "[ERROR] Target directory %s does not exist or is not accesible..." % options.target_dir
	sys.exit(-1)
	
if options.category!="" and options.category not in category_params.keys():
	print "Category '%s' is not supported...." % options.category
	sys.exit(-1)

infile_list = []
for category in category_params.keys():

	# If user has specified category...
	if options.category!="" and options.category!=category:
		continue

	# If this category has not params to be tested, continue...
	if 'params' not in category_params[category].keys():
		continue

	# Diferent types of forced errors...
	invalid_param = []
	ignored_param = []
	for error in category_params[category]['errors']:
		is_string = ( type(category_params[category]['errors'][error]) == str )
		if error==INVALID_PARAM :
			if not is_string:
				for invalid in category_params[category]['errors'][error]:
					invalid_param.append(invalid)
			else:
				invalid_param.append(category_params[category]['errors'][error])

		elif error==IGNORED_PARAM:
			if not is_string:
				for ignored in category_params[category]['errors'][error]:
					ignored_param.append(ignored)
			else:
				ignored_param.append(category_params[category]['errors'][error])
		else:
			print "Unknown error type: " + error

	# For each error we generate a specific unit test
	if ( len(invalid_param)>0 ):
		for param in invalid_param:
			create_invalidparam_file(category, param, category_params[category]['params'])
	if ( len(ignored_param)>0 ):
		for param in ignored_param:
			create_ignoredparam_file(category, param, category_params[category]['params'])

	# Also, create a correct unit test...
	create_goodtest_file(category, category_params[category]['params'])

# Job done
if options.testsuite:
	print "REGRESS_TARGETS += testsuite-%s" % options.testsuite
	for item in infile_list:
		name = os.path.basename(item).split(".")[0]
		print "REGRESS_TARGETS += %s" % name
	print "REGRESS_TARGETS += endtestsuite-%s" % options.testsuite
	print "\nDone! Please add this code to your Makefile.\nDo not forget to 'svn add' new files!!!"

else:
	print "\nAll tests done, please integrate them into your testsuite...\n\nIn case you ALSO want that done for you, donate 15$ to juan@schibstediberica.es...\n\nNo animals were harmed in the development of this script."
