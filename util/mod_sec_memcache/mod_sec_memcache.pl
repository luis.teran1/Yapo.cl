#!/usr/bin/perl -w

use strict;
use blocket::bconf;
use Getopt::Long;
use Socket;
use IO::Handle;

my $configfile = '/opt/blocket/conf/bconf.conf';
my $debug = 0;

GetOptions (
	"configfile=s" => \$configfile,
	"debug" => \$debug
	);

my $BCONF = blocket::bconf::init_conf($configfile);

my %logs = ();
my $curr_sess;
my $curr_sect;
my %mc_sess = ();

sub save_memcache($$)
{
	my ($mc_sess, $log) = @_;

	my $re = $$BCONF{'*'}{'common'}{'session'}{'id_separator'} . ".*";
	my $mc_serv = $mc_sess;
	$mc_serv =~ s/$re//;
	print "mc_serv = $mc_serv\n" if $debug;
	my $servname = $$BCONF{'*'}{'common'}{'session'}{'servers'}{$mc_serv};

	print "servname = $servname\n" if $debug;
	return unless defined($servname) && $servname =~ /^(.+):([0-9]+)$/;

	my $hostname = $1;
	my $port = $2;

	my $addr = inet_aton($hostname);
	my $paddr = sockaddr_in($port, $addr);

	# XXX IPv6
	socket(MC, PF_INET, SOCK_STREAM, getprotobyname('tcp')) || return;
	# timeout connection after 60 seconds
        setsockopt(MC, SOL_SOCKET, SO_SNDTIMEO, pack('L!L!', 300, 0) ) || return;
        setsockopt(MC, SOL_SOCKET, SO_RCVTIMEO, pack('L!L!', 300, 0) ) || return;
	connect(MC, $paddr) || return;

	$log =~ s/\\/\\\\/g;
	$log =~ s/\n/\\n/g;
	$log =~ s/\r/\\r/g;

	print STDERR "set ${mc_sess}_mod_sec 0 300 " . length($log) . "\r\n$log\r\n" if $debug;

	MC->autoflush(1);
	print MC "set ${mc_sess}_mod_sec 0 300 " . length($log) . "\r\n$log\r\n";
	my $res = <MC>;
	close MC;
	chomp $res;
	return $res;
}

open LOG, '|-', '/usr/bin/logger -p local0.warn -t mod_security';

while (<>) {
	print LOG $_;

	/^--([0-9a-z]+)-([A-Z])--$/ && do {
		$curr_sess = $1;
		$curr_sect = $2;
		$logs{$curr_sess} = '' if $curr_sect eq 'A';
	};

	next if !defined $curr_sess || !defined $logs{$curr_sess};
	$logs{$curr_sess} .= $_;

	print STDERR "- $curr_sess $curr_sect -\n" if $debug;

	if ($curr_sect eq 'B' && /^Cookie:.* s=(mc[a-z0-9]*)/) {
		$mc_sess{$curr_sess} = $1;
		print STDERR "- mc_sess = ${mc_sess{$curr_sess}}\n" if $debug;
		next;
	}

	if ($curr_sect eq 'Z') {
		print STDERR "Z - ${mc_sess{$curr_sess}}\n" if $debug;
		if ($mc_sess{$curr_sess}) {
			my $res = save_memcache($mc_sess{$curr_sess}, $logs{$curr_sess});
			print "save_memcache = $res\n" if $debug;
			delete $mc_sess{$curr_sess};
		}
		delete $logs{$curr_sess};
	}
}
