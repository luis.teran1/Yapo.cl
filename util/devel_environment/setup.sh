#!/bin/bash

usage()
{
	SCRIPT=$(basename $0)
	echo "please run this script as root or with sudo"
	echo "$SCRIPT"
}

# copy the files to where belongs
rm -f /usr/local/bin/check_git_branch
cp scripts/check_git_branch /usr/local/bin/check_git_branch


# we need to update bashrc

