"""
This script add the text "INSERT INTO ad_params VALUES (ad_id, "commune", commune)
at the all snaps that they are not in exclude snap and the failure snaps.

IMPORTANT: Excludes the last four ad_id of the snaps.( Attribute exclude_last_ad_id)
"""
# -*- coding: latin-1 -*-
import subprocess
import random

class UpdateSnaps(object):
    """
    Class to add the commune parameter to the ads without commune parameter.
    """
    conf_communes = 'conf/common/bconf/bconf.txt.communes'
    tree_region = '.*.common.region.{region}.commune.*.name'
    exclude_last_ad_id = 4

    def __init__(self, snap):
        """
        Initial method
        """
        self.snap = snap
        print(self.snap)
        self.ads_without_commune_param = self.search_ad_id_in_snap()

    def search_communes_by_region(self, region_id):
        """
        Return value of the communes from bconf for a region.
        """
        tree_region = self.tree_region.format(region=region_id)
        value_communes_by_region = "grep -ri {tree_region} {conf_commune} | cut -d'=' -f 2".format(
            tree_region=tree_region, conf_commune=self.conf_communes)
        values = subprocess.check_output(value_communes_by_region, shell=True).split("\n")
        values.remove('')
        print(value_communes_by_region)
        return values

    def add_insert_into_commune_param_the_snap(self):
        """
        Write in the snap file.
        """
        archive = open("scripts/db/insert_blocketdb_{0}data.pgsql".format(snap), "a")

        if hasattr(self, 'exclude_last_ad_id') and isinstance(self.exclude_last_ad_id, int):
            list_ads = self.ads_without_commune_param[0:-self.exclude_last_ad_id]
        else:
            list_ads = self.ads_without_commune_param

        for ad_id, region in list_ads:
            if int(region) in range(1, 16):
                list_communes = self.search_communes_by_region(region)
                commune_selected = list_communes[random.randrange(0, len(list_communes))]
                print("snap: {0} ad:{1} commune:{2}".format(self.snap, ad_id, commune_selected))
                query = "INSERT INTO ad_params VALUES ({ad_id}, 'communes', '{communes}');\n".format(ad_id=ad_id, communes=commune_selected)
                print(query)
                archive.write(query)

        archive.close()

    def _clean_text_snap(self, text):
        """
        Return an Clean text.
        """
        delete_text = [
            'INSERT', 'INTO', 'ads', 'VALUES', 'ad_id', 'list_id', 'orig_list_time', 'list_time', 'status', 'type', 'name', 'phone', 'region', 'city', 'category',
            'user_id', 'passwd', 'phone_hidden', 'no_salesmen', 'company_ad', 'subject', 'body', 'price', 'old_price', 'image', 'infopage', 'infopage_title',
            'store_id', 'salted_passwd', '_hidden', '_title', 'old_', 'salted_', 'modified_at', 'lang', ')', ' ', 'ad_params', ';', 'name', 'value'
        ]
        result = str(text)
        for delt in delete_text:
            result = result.replace(delt, '')
        return result

    def get_list_ads_from_grep(self):
        """
        Return ads from the snaps
        """
        ads = []
        try:
            ads = subprocess.check_output("grep -ri \"INSERT INTO ads\" scripts/db/insert_blocketdb_{0}data.pgsql".format(self.snap), shell=True).split('\n')
            for i in range(ads.count('')): ads.remove('')
        except: pass
        return ads

    def get_list_ads_with_param_commune(self):
        """
        Return ads with commune parameter from snaps
        """
        ads_with_commune_params = []
        try:
            ads_with_commune = subprocess.check_output("grep -w ad_params scripts/db/insert_blocketdb_{0}data.pgsql | grep -w communes".format(self.snap), shell=True).split('\n')
            for i in ads_with_commune:
                list_ad_params = self._clean_text_snap(i).replace("(", "").split(',')
                for i in range(list_ad_params.count('')): list_ad_params.remove('')

                ad_id = list_ad_params[0]
                if ad_id != "":
                    ads_with_commune_params.append(int(ad_id))
        except: pass
        return ads_with_commune_params

    def search_ad_id_in_snap(self):
        """
        Return an tuple of ads without commune params.
        x.search_ad_id_in_snap() -> [(ad_id, region),...]
        """
        ads = self.get_list_ads_from_grep()
        ads_with_commune_params = self.get_list_ads_with_param_commune()
        print("{0}: ads_with_commune_params: {1} ".format(self.snap, ads_with_commune_params))
        ads_without_commune_param = []

        if len(ads) > 0:
            for ad in ads:
                ad_text = self._clean_text_snap(ad)
                list_columns_ad = [i for i in ad_text.replace('(', '').split(',') if len(i) > 0]
                try:
                    ad_id = int(list_columns_ad[0])
            
                    if ad_id not in ads_with_commune_params:
                        region_list = [str(i) for i in range(0, 16)]
                        if list_columns_ad[7] in region_list:
                            region = int(list_columns_ad[7])
                        else:
                            region = int(list_columns_ad[8])
                        
                        ads_without_commune_param.append((ad_id, region))
                except: pass

        return ads_without_commune_param

if __name__ == '__main__':
    snaps = "find scripts/db/* -iname \"insert_blocketdb_*\" | cut -d'/' -f3"

    list_snap = set([
        snap.replace("insert_blocketdb_", "").replace("data.pgsql", "") for snap in subprocess.check_output(snaps, shell=True).split("\n")  if len(snap) > 0
    ])

    failure_snaps = set([
        'adqueues', 'adsperhour', '1000ads', '40ads', 'ais', 'example', '1030ads', 'test',
        'motorcicleads', 'noreqs', 'bills', 'ruleslistfilters', 'gallery'])

    not_used_snaps = set([
        'acc2', 'acc', 'teststores', 'site', 'nga-tests', 'add100galleries', 'admins', 'mamabackup',
        'mailstats_multilang', 'diff_store_bad_words', 'statsmail', 'whitelist2'])

    exclude_snap = failure_snaps.union(not_used_snaps)

    for snap in list_snap - exclude_snap:
        update_snaps = UpdateSnaps(snap)
        update_snaps.add_insert_into_commune_param_the_snap()
