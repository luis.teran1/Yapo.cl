#!/bin/bash
. /home/pablo/code/yapo.cl/util/devel_environment/nubox_service/example/service.conf

checkService()
{
	if [ ! -f $PIDFILE ]
	then
		exit 0
	fi
	PID=$(cat $PIDFILE)
	if [ $PID -ne $$ ]
	then	
		exit -1
	fi
}

execute()
{
	($CMD 2>&1) > /dev/null 
}

run()
{
	while true
	do
		checkService
		HOUR=$(date +%H)
		MINUTE=$(date +%M)

		if [ $HOUR -ge $FROM_HOUR ]; then
			if [ $MINUTE -ge $FROM_MINUTE ]; then
				if [ $HOUR -le $TO_HOUR ]; then
					if [ $MINUTE -le $TO_MINUTE ]; then
						# do it!!
						execute
					fi
				fi
			fi
		fi
		
		sleep $PAUSE
	done
}

FROM_HOUR=${START_TIME:0:2}
FROM_MINUTE=${START_TIME:3:5}
TO_HOUR=${END_TIME:0:2}
TO_MINUTE=${END_TIME:3:5}

echo $$ > $PIDFILE

run
