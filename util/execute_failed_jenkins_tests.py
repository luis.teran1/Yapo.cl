#!/usr/bin/env python3
# WIP - We need to improve this script to support Regress Final tests
# Example of use: ./execute_failed_jenkins_tests.py -t narcos --jenkins 1 -j sasha_narcos -b 39

from collections import defaultdict
from jenkinsapi.jenkins import Jenkins
from optparse import OptionParser
import os
import re
import subprocess
import imp
import inspect
import sys

JENKINS = ''
JENKINS_JOB = ''
BUILD_NO = ''
TYPE = ''
JOB_REGEXP = re.compile('^job\d+.sh$')

def aim_to_jenkins(JENKINS):
    if JENKINS == '1':
        return 'http://172.21.10.221:8080'
    if JENKINS == '2':
        return 'http://172.21.10.216:8080'
    if JENKINS == '3':
        return 'http://172.21.10.121:8080'
    if JENKINS == '4':
        return 'http://172.21.10.217:8080'

def get_narcos_tests(jenkins, jenkins_job, build_no):
    print("Buscando pruebas fallidas de build indicada...")

    J = Jenkins(jenkins)
    job = J[jenkins_job]
    build = job.get_build(int(build_no))
    resultset = build.get_resultset()
    d = []
    for k, v in resultset.iteritems():
        try:
            if 'FAILED' in str(v) or 'REGRESSION' in str(v):
                class_name = v.className[v.className.index(".")+1:]
                splited = str(v).split()
                test = re.sub(r"([.])([A-Z])",r":\2",str(splited[0])[11:])+'.'+splited[1]
                class_name = v.className[v.className.index(".")+1:v.className.rfind('.')]
                d.append(test)
        except ValueError:
            print("Error ):")
            pass

    return d

def get_regress_tests(jenkins, jenkins_job, build_no):
    print("Buscando pruebas fallidas de build indicada...")

    J = Jenkins(jenkins)
    job = J[jenkins_job]
    build = job.get_build(int(build_no))
    resultset = build.get_resultset()
    d = []
    for k, v in resultset.iteritems():
        try:
            if 'FAILED' in str(v) or 'REGRESSION' in str(v):
                class_name = v.className[v.className.index(".")+1:]
                splited = str(v).split()
                test = re.sub(r"([.])([A-Z])",r":\2",str(splited[0])[11:])+'.'+splited[1]
                class_name = v.className[v.className.index(".")+1:v.className.rfind('.')]
                d.append(test)
        except ValueError:
            print("Error ):")
            pass

    return d

def list_jenkins_jobs():
    print("Finding Jenkins Jobs")

    J = Jenkins(aim_to_jenkins(JENKINS))
    for job in J.get_jobs():
        print(job[0])

def handle_options():
    parser = OptionParser()
    parser.add_option("-j", "--job", dest="job",
                      help="Jenkins job to take tests from")
    parser.add_option("--jenkins", dest="jenkins",
                      help="Jenkins to take tests from, it can be [1,2,3]")
    parser.add_option("-b", "--build", dest="build",
                      help="Jenkins build to take tests from")
    parser.add_option("-l", "--list", dest="list_only", action="store_true",
                      help="List available (configured) builds")
    parser.add_option("-t", "--type", dest="type",
                      help="Type of tests to be executed, Narcos or Regress")

    global JENKINS_JOB, JENKINS, BUILD_NO, TYPE
    options, args = parser.parse_args()

    if options.list_only:
        list_jenkins_jobs()
        exit()

    if options.job:
        JENKINS_JOB = options.job

    if options.jenkins:
        JENKINS = options.jenkins

    if options.build:
        BUILD_NO = options.build

    if options.type:
        TYPE = options.type

def main():
    handle_options()

    stri = ''
    if str(TYPE).upper() == 'REGRESS':
        tests = get_regress_tests(aim_to_jenkins(JENKINS), JENKINS_JOB, BUILD_NO)
        print('\nSe encontraron {0} tests fallidos\n'.format(len(tests)))
        stri = 'REGRESS_LOGERR_FILE=err.log make -s rd rs-load selenium-start'
        for t in tests:
            stri += " rs-{0}".format(t)
        stri += " 2>&1 | tee executionResult-{0}-{1}.log".format(JENKINS_JOB,BUILD_NO)
    elif str(TYPE).upper() == 'NARCOS':
        tests = get_narcos_tests(aim_to_jenkins(JENKINS), JENKINS_JOB, BUILD_NO)
        print('\nSe encontraron {0} tests fallidos\n'.format(len(tests)))
        stri = './nose2.sh'
        for t in tests:
            stri += " {0}".format(t)
        stri += " 2>&1 | tee executionResult-{0}-{1}.log".format(JENKINS_JOB,BUILD_NO)
    else:
        print('La opcion {0} es invalida, usa Narcos o Regress, no es case sensitive').format(TYPE)
    print(stri)

if __name__ == "__main__":
    main()

