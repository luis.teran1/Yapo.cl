#!/usr/bin/perl

use strict;
use warnings;
use Getopt::Long;

my $pattern;
my $string;

GetOptions(
		   "pattern|p=s" => \$pattern,
		   "string|s=s"  => \$string
		  ) or die("Usage: ./pcre_ucp -p <pattern> -s <string>");
if(!defined $pattern or !defined $string) {
	print("Usage: ./pcre_ucp -p <pattern> -s <string>\n");
	exit 1;
}

sub check_regexp {

	my $pattern = shift;
	my $string = shift;

	if($string =~ /$pattern/) {
		return 1;
	}
	return 0;
}

check_regexp($pattern, $string);
