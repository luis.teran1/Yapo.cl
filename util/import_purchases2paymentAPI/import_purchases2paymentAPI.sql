CREATE OR REPLACE FUNCTION import_purchase2paymentAPI() RETURNS integer AS $$
DECLARE
	l_rec 			record;
	l_national_id 		varchar;
	l_name 			varchar;
	l_company		varchar;
	l_remote_addr 		varchar;
	
	l_buyer_id 		pmnt_api_buyer.buyer_id%type;
	l_payment_id		pmnt_api_payment.payment_id%type;
	l_api_status		varchar;
	l_paylog_status		varchar;
	l_paid_at		timestamp;
	l_transaction_id	bigint;
	l_request_id		bigint;
	o_counter		integer;


BEGIN
	o_counter := 0;
	FOR
		l_rec
	IN
		select 
			p.doc_type,
			p.name,
			p.rut,
			d.ad_id,
			d.action_id,
			p.email,
			a.company_ad,
			a.name as ad_name,
			a.user_id,
			p.payment_group_id
		from 
			purchase p 
			join purchase_detail d using (purchase_id) 
			join ads a using(ad_id)

	LOOP

		PERFORM payment_id FROM pmnt_api_payment WHERE external_order_id = l_rec.payment_group_id::varchar;
		IF FOUND THEN
			CONTINUE; -- Don't add if the payment already exists in the API
		END IF;

		--USER DATA
		if l_rec.doc_type = 'invoice' then
			l_national_id := l_rec.rut;
			l_name := l_rec.name;
		else
			l_national_id = l_rec.email;
			l_name := l_rec.ad_name;
		end if;
		--USER TYPE
		if l_rec.company_ad = 'f' then
			l_company := 'PRIVATE';
		else
			l_company := 'COMPANY';
		end if;

		select 
			buyer_id
		into
			l_buyer_id
		from 
			pmnt_api_buyer
		where  national_id = l_national_id;
		if not found then
			insert into pmnt_api_buyer (buyer_id, national_id, type) values (DEFAULT, l_national_id, l_company::enum_pmnt_buyer_type)
			returning buyer_id into l_buyer_id;
		end if;

		select reference 
		into l_remote_addr
		from pay_log  pl join pay_log_references ref using(pay_log_id) 
		where pl.payment_group_id = l_rec.payment_group_id and ref.ref_type = 'remote_addr' and pl.pay_type = 'save';

		select paid_at, status
		into l_paid_at, l_paylog_status
		from pay_log
		where payment_group_id = l_rec.payment_group_id order by pay_log_id desc limit 1;

		if l_paylog_status = 'OK' then
			l_api_status := 'PAID';
		elsif l_paylog_status = 'SAVE' then
			l_api_status := 'UNPAID';
		elsif l_paylog_status LIKE 'ERR_%' then
			l_api_status := 'UNAUTHORIZED';
		else
			l_api_status := 'UNPAID';
		end if;

		INSERT INTO pmnt_api_payment 
		( external_user_id, 
			external_user_email,
			status,
			buyer_id, buyer_name, 
			external_order_id, 
			gateway_id, 
			method_id, 
			application_id,
			remote_addr, 
			price, 
			original_price,
			times, 
			receipt_version, 
			callback_url
		) VALUES ( 
			l_rec.user_id,
			l_rec.email,
			l_api_status::enum_pmnt_payment_status,
			l_buyer_id,
			l_name,
			l_rec.payment_group_id,
			2,5,1,
			l_remote_addr, 
			1000, 1000, 
			1,
			1,
			'https://www2.yapo.cl/payment_update/' )
		RETURNING payment_id INTO l_payment_id;
		
		INSERT INTO pmnt_api_item (payment_id, product_id, product_name, price, quantity, description) 
                VALUES (l_payment_id, 1, 'Subir', 1000, 1, 'Subida del aviso');

		IF l_api_status = 'PAID' THEN
			INSERT INTO pmnt_api_receipt
			(
				payment_id,
				payment_date,
				original_price,
				national_id,
				buyer_name,
				external_user_id,
				items,
				version
			) VALUES (
				l_payment_id,
				l_paid_at,
				1000,
				l_national_id,
				l_name,
				l_rec.user_id,
				'[{product_code:"1",product_name:"Subir",price:1000,quantity:"1",description:"Subida del aviso, IMPORTED"}]',
				1
			);
		END IF;

		INSERT INTO pmnt_api_transaction (
			payment_id,
			type,
			status,
			response_date
		) VALUES (
			l_payment_id,
			'PAYMENT',
			l_api_status::enum_pmnt_payment_status,
			l_paid_at
		) RETURNING transaction_id INTO l_transaction_id;

		INSERT INTO pmnt_api_request (transaction_id, date)
		VALUES (l_transaction_id, l_paid_at) RETURNING request_id INTO l_request_id;

		INSERT INTO pmnt_api_response (request_id, date, status, execution_time, error_code, returned_data)
		VALUES (l_request_id, l_paid_at, 'PENDING', 0, 'UNDEFINED', '{}');

		o_counter := o_counter + 1;

	END LOOP;
	RETURN o_counter;

END;
$$ LANGUAGE plpgsql;
