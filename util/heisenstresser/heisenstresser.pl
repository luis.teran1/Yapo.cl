#!/usr/bin/perl -w
#
# heisenstress -- Randomly send strings from a file to a service, validating that the result output is the expected one.
#
# The infile repeats one line with the string to send, followed by one line with 
# the ascii hex representation of the result of sending said string.
#
# You can create a stressfile interactively using the --create option:
#
# ./heisenstresser.pl --host localhost --port 21610 --file asearch.in --create
#
# Stresser can fill in missing hashes and rewrite the file when given the 'generate' option.
# 'localhost' is the default value for --host, so can be omitted.
#
# ./heisenstresser.pl --port 21610 --file asearch.in --generate
#
# ./heisenstresser.pl --port 21610 --file asearch.in --check=1000
#
#
# Dependencies:
#	perl-Digest-SHA1
#	perl-Net-Telnet
#
# To-do:
#  Option to do select 'pattern' for sending data when 'check' (linear, random, shuffled)
#
use strict;
use POSIX "strftime";
use Digest::SHA1 qw(sha1_hex);
use Net::Telnet();
use Getopt::Long;

my $script_path = rindex($0, '/') > 0 ? substr($0, 0, rindex($0, '/')) : '.';
my $debug = 0;
my $host = 'localhost';
my $port;
my $stressfile;
my $cmd_generate;
my $cmd_check;
my $cmd_create;

GetOptions ("host=s" => \$host,
	    "port=s" => \$port,
	    "debug" => \$debug,
	    "create" => \$cmd_create,
	    "generate" => \$cmd_generate,
	    "check=s" => \$cmd_check,
	    "file=s" => \$stressfile);

#require $script_path.'/heisenstresser.conf';
#our %cfg;

my %stress = ( );

sub read_stresstest($) {
	my $filename = shift;

	open(IN, "<".$filename) or die("I damn you to Heck!");

	$stress{'dataset'} = (); 
	my $i = 0;
	while(my $data = <IN>) {
		chomp $data;
		my $hash = <IN>;
		if(! defined $hash) {
			print "Warning: input line ignored. No hash given.\n";
			next;
		}
		chomp $hash;
		++$i;
		$stress{'dataset'}->{$i}->{'data'} = $data;
		$stress{'dataset'}->{$i}->{'hash'} = $hash;
		$stress{'dataset_size'} = $i;
	}
	close(IN);
	print "Read ".$i." entries.\n" if $debug;
}

sub write_stresstest($) {
	my $filename = shift;
	
	open(OUT, ">".$filename) or die("I damn you to Heck!");
	while ((my $idx, my $entry) = each(%{$stress{'dataset'}})) {
		print OUT $stress{'dataset'}->{$idx}->{'data'}."\n";
		print OUT $stress{'dataset'}->{$idx}->{'hash'}."\n";
	}
	close(OUT);	
}

sub send_data(\%) {
	my $entry = shift;;

	my $t = new Net::Telnet(Host => $host, Port => $port);
	
	print "connecting to ".$host.", port ".$port.."\n" if $debug;
	print "SENDING '".$entry->{'data'}."'\n" if $debug;
	$t->print($entry->{'data'});

	my $result = $t->get();

	if ($debug) {
		print "-[ result ]-------------------------------------------------------\n";
		print $result;
		print "------------------------------------------------------------------\n";
	}
	$entry->{'hash_gen'} = sha1_hex($result);
	$entry->{'result'} = $result;

	my $ok = $t->close;
	return;
}

sub create_stresstest($) {
	my $filename = shift;

	while(1) {
		print "Data to send (empty line to save and exit):\n";
		my $line = <STDIN>;
		chomp($line);
		if( $line ) {
			my %entry = ('data' => $line);
			send_data(%entry);
			print "-[ result ]-------------------------------------------------------\n";
			print $entry{'result'};
			print "------------------------------------------------------------------\n";
			print "Generated hash: ".$entry{'hash_gen'}."\n";
			print "Is output OK? [y/n]: ";
			$line = <STDIN>;
			chomp($line);
			if ($line =~ /^[y](es)?/i) {
				my $new_idx = ++$stress{'dataset_size'};
				print "Added to dataset at position ".$new_idx."\n";
				$entry{'hash'} = $entry{'hash_gen'};
				$stress{'dataset'}->{$new_idx} = \%entry;
			}
		} else {
			print "exit with save to '".$stressfile."'.\n";
			return 1;
		}
	}
}

sub validate_entry(\%) {
	my $entry = shift;

	if( $entry->{'hash'} ne $entry->{'hash_gen'}) {
		print "MISMATCH".$entry->{'hash_gen'}." <-> ".$entry->{'hash'}."\n";
		print "QUERY: ".$entry->{'data'}."\n";
		print "RESULT: ".$entry->{'result'}."\n";
		exit 1;
	}
	return 0;
}


#########################################################################
# main									#
#########################################################################

	if (! defined $host || $host eq '') {
		print "Must give --host\n";
		exit 1;
	}

	if (! defined $port || $port eq '') {
		print "Must give --port\n";
		exit 1;
	}

	if (defined $cmd_create) {
		if (create_stresstest($stressfile)) {
			write_stresstest($stressfile);
		}
		exit 0;
	}

	read_stresstest($stressfile);

	if (defined $cmd_generate) {
		print "Running queries.\n";
		while ((my $idx, my %entry) = each(%{$stress{'dataset'}})) {
			send_data(%entry);
		}
		write_stresstest($stressfile);
	}

	if (defined $cmd_check) {
		my $checked = 0;
		while ((my $idx, my $entry) = each(%{$stress{'dataset'}})) {
			send_data(%{$entry});
			validate_entry(%{$entry});
			++$checked;
		}
		print "Linear check completed OK.\n";
		my $num_entries = $checked;
		$checked = 0;
		my $loop = $cmd_check;
		while($loop--) {
			my $idx = int rand($num_entries-1) + 1; 
			send_data(%{$stress{'dataset'}->{$idx}});
			validate_entry(%{$stress{'dataset'}->{$idx}});
			if( ++$checked % 100 == 0) {
				print $checked." checked OK\n";
			}

		}
		print "Final: ".$checked." checked OK\n";
	}


