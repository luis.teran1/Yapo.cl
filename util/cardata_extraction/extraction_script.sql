COPY (SELECT
ap_brand.value AS brand,
ads.region,
ads.price,
ap_cartype.value AS cartype,
ads.list_time AS published,
acs.timestamp AS deleted,
acp.value AS reason
FROM ads
INNER JOIN ad_params ap_brand ON (ap_brand.ad_id = ads.ad_id AND ap_brand.name = 'brand')
INNER JOIN ad_params ap_cartype ON (ap_cartype.ad_id = ads.ad_id AND ap_cartype.name = 'cartype')
INNER JOIN ad_actions aa ON (aa.ad_id = ads.ad_id AND aa.action_type = 'delete' AND aa.state = 'deleted')
INNER JOIN action_states acs ON (acs.ad_id = ads.ad_id AND aa.action_id = acs.action_id AND acs.transition = 'user_deleted')
INNER JOIN action_params acp ON (acp.ad_id = acs.ad_id AND acp.action_id = acs.action_id AND acp.name = 'deletion_reason')
WHERE ads.category = 2020 AND ads.status = 'deleted' AND ads.type = 'sell'
AND ads.list_time BETWEEN '2014-01-01 00:00:00' AND '2014-07-31 23:59:59'
) TO '/var/lib/pgsql/soldcars_original.csv' WITH DELIMITER ';';
