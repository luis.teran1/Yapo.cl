# -- coding: utf-8 --
import redis
import sys

#########################################################
#########################################################
# i am a script in progress, be nice with me please (:
#########################################################
#########################################################

# redis
myPort = 6397
#myPort = 20248 # typical regress port as example, use your own port
myHost = '10.0.1.232'
#myHost = 'localhost' # for regress
myDbNumber = 0
myKey = 'rcd1xapp_id_cardata_brands'

filename_in = 'soldcars_original.csv'
filename_out = 'soldcars_processed.csv'

print 'Reading from redis...'
r = redis.StrictRedis(host=myHost, port=myPort, db=myDbNumber)
brands = r.hgetall(myKey)
brands['0'] = '- Otra -'

# regions
regions = { '1':'XV Arica & Parinacota',
            '2':'I Tarapac�',
            '3':'II Antofagasta',
            '4':'III Atacama',
            '5':'IV Coquimbo',
            '6':'V Valpara�so',
            '7':'VI OHiggins',
            '8':'VII Maule',
            '9':'VIII Biob�o',
            '10':'IX Araucan�a',
            '11':'XIV Los R�os',
            '12':'X Los Lagos',
            '13':'XI Ais�n',
            '14':'XII Magallanes & Ant�rtica',
            '15':'Regi�n Metropolitana' }

# car types
cartypes = {'1':'Autom�vil',
            '2':'Camioneta',
            '3':'4x4',
            '4':'Convertible',
            '5':'Cl�sico'}

# delete reasons
reasons = { '1':'Lo vend� en yapo.cl',
            '2':'Lo vend� por otro medio',
            '3':'Lo quiero modificar o eliminar para publicar uno nuevo',
            '4':'Ya no lo quiero vender',
            '5':'Todav�a no logro venderlo, me di por vencido(a)',
            '6':'Por otra raz�n' }

print 'Redis reading finished, ' + str(len(brands)) + ' brands loaded. Now executing replacement in ' + filename_in + ' file.' 

csv_in = open(filename_in, 'r')
csv_out = open(filename_out, 'w')
csv_out.write('brand;region;price;cartype;published;deleted;reason\n')
for line in csv_in:
    fields = line.split(';')
    # 0 - brand
    fields[0] = brands[fields[0]]
    # 1 - region
    fields[1] = regions[fields[1]]
    # 2 - price
    # 3 - cartype
    fields[3] = cartypes[fields[3]]
    # 4 - published
    # 5 - deleted
    # 6 - reason
    fields[6] = reasons[fields[6].replace('\n','')]
    csv_out.write(';'.join(i for i in fields) + '\n')
csv_in.close()
csv_out.close()
print 'Output ' + filename_out + ' written.'
