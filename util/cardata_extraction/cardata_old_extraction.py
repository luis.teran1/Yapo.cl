# -- coding: utf-8 --
import redis
import psycopg2
import sys

#########################################################
#########################################################
# i am a script in progress, be nice with me please (:
#########################################################
#########################################################

# redis
myHost = '10.0.1.232'
#myHost = 'localhost'
myPort = 6397
#myPort = 20248
myDbNumber = 0
myKey = 'rcd1xapp_id_cardata_brands'

# postgresql
myDb = 'blocketdb'
myDbHost = '/dev/shm/regress-mauricio/pgsql0/data'

print 'Reading from redis...'
r = redis.StrictRedis(host=myHost, port=myPort, db=myDbNumber)
brands = r.hgetall(myKey)

# delete reasons
reasons = ['Lo vend� en yapo.cl',
            'Lo vend� por otro medio',
            'Lo quiero modificar o eliminar para publicar uno nuevo',
            'Ya no lo quiero vender',
            'Todav�a no logro venderlo, me di por vencido(a)',
            'Por otra raz�n']

# car types
cartypes = ['Autom�vil',
            'Camioneta',
            '4x4',
            'Convertible',
            'Cl�sico']

# regions
regions = ['XV Arica & Parinacota',
            'I Tarapac�',
            'II Antofagasta',
            'III Atacama',
            'IV Coquimbo',
            'V Valpara�so',
            'VI OHiggins',
            'VII Maule',
            'VIII Biob�o',
            'IX Araucan�a',
            'XIV Los R�os',
            'X Los Lagos',
            'XI Ais�n',
            'XII Magallanes & Ant�rtica',
            'Regi�n Metropolitana']

con = None
try:
    con = psycopg2.connect(database=myDb , host=myDbHost)
    cur = con.cursor()
    cur.execute('DROP TABLE IF EXISTS delete_me_brand')
    cur.execute('CREATE TABLE delete_me_brand (id_brand integer, brand varchar(256))')
    con.commit()
    print 'Table was created'

    for brand in brands:
        cur.execute('INSERT INTO delete_me_brand(id_brand, brand) VALUES(' + brand + ',\'' + brands[brand] + '\')')

    con.commit()
    print 'Insert values is ready'

    # static tables
    cur.execute('DROP TABLE IF EXISTS delete_me_reason')
    cur.execute('CREATE TABLE delete_me_reason (id SERIAL, reason varchar(256))')
    for reason in reasons:
        cur.execute('INSERT INTO delete_me_reason(reason) VALUES(\'' + reason  + '\')')
    con.commit()
    print 'delete_me_reason was created'

    cur.execute('DROP TABLE IF EXISTS delete_me_cartype')
    cur.execute('CREATE TABLE delete_me_cartype (id SERIAL, type varchar(256))')
    for type in cartypes:
        cur.execute('INSERT INTO delete_me_cartype(type) VALUES(\'' + type  + '\')')
    con.commit()
    print 'delete_me_cartype was created'

    cur.execute('DROP TABLE IF EXISTS delete_me_region')
    cur.execute('CREATE TABLE delete_me_region (id SERIAL, region varchar(256))')
    for region in regions:
        cur.execute('INSERT INTO delete_me_region(region) VALUES(\'' + region  + '\')')
    con.commit()
    print 'delete_me_region was created'

    cur.execute('select \
        delb.brand as brand, \
        delrg.region, \
        ads.price, \
        delc.type as cartype, \
        ads.list_time as published, \
        acs.timestamp as deleted, \
        delrs.reason \
        from ads \
        inner join ad_params ap_brand on (ap_brand.ad_id = ads.ad_id and ap_brand.name = \'brand\') \
        inner join ad_params ap_cartype on (ap_cartype.ad_id = ads.ad_id and ap_cartype.name = \'cartype\') \
        inner join ad_actions aa on (aa.ad_id = ads.ad_id and aa.action_type = \'delete\' and aa.state = \'deleted\') \
        inner join action_states acs on (acs.ad_id = ads.ad_id and aa.action_id = acs.action_id and acs.transition = \'user_deleted\') \
        inner join action_params acp on (acp.ad_id = acs.ad_id and acp.action_id = acs.action_id and acp.name = \'deletion_reason\') \
        inner join delete_me_brand delb on (ap_brand.value = cast (delb.id_brand as text)) \
        inner join delete_me_cartype delc on (ap_cartype.value = cast (delc.id as text)) \
        inner join delete_me_reason delrs on (acp.value = cast (delrs.id as text)) \
        inner join delete_me_region delrg on (ads.region = delrg.id) \
        where ads.category = 2020 and ads.list_time between \'2014-01-01 00:00:00\' and \'2014-07-31 23:59:59\';')

    carfile = open('outputfile.csv', 'w')
    carfile.write('brand;region;price;cartype;published;deleted;reason\n')
    while True:
        row = cur.fetchone()
        if row == None:
            break;
        carfile.write('%s;%s;%s;%s;%s;%s;%s\n' %(row[0], row[1], row[2], row[3], row[4], row[5], row[6]))
    carfile.close()

except psycopg2.DatabaseError, e:
    if con:
        con.rollback()

    print 'Error %s' % e
    sys.exit(1)

finally:
    if con:
        con.close()

