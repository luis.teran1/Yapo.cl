var fs = require('fs');
var config = require("./config.in.json");
var data = require("./regress_data.json");

var getValue = function(value, defaultValue){
	if (value){
		return value;
	}
	return defaultValue;
}

config.workingDir = getValue(data.topdir, config.workingDir) + "/mock/data" ;
config.control = (config.control) ? config.control : {};
config.control.host = getValue(data.hostname, config.control.host);
config.control.port = getValue(data.control_port, config.control.port);;

// the roles are:
// 0 => mc_widget
// 1 => nextgen
// 2 => credits
// 3 => bifrost
// 4 => yapofact
// 5 => sms
// 6 => subcription
// 7 => ads_evaluator
// 8 => profile
// 9 => PaymentDelivery
var mc  = config.roles[0];
var ng  = config.roles[1];
var cr  = config.roles[2];
var bi  = config.roles[3];
var yf  = config.roles[4];
var sms = config.roles[5];
var su = config.roles[6];
var ads_evaluator = config.roles[7];
var profile = config.roles[8];
var pd = config.roles[9]

mc.connection.host = getValue(data.hostname, mc.connection.host);
mc.connection.port = getValue(data.mock_mc_port, mc.connection.port);
mc.connection.tls = (mc.connection.tls) ? mc.connection.tls : {} ;
mc.connection.tls.certPath = getValue(data.ssl_cert, mc.connection.tls.certPath);
mc.connection.tls.keyPath = getValue(data.ssl_key, mc.connection.tls.keyPath);
mc.server.host = getValue(data.hostname, mc.server.host);
mc.server.port = getValue(data.https_port, mc.server.port);
mc.server.protocol = getValue(data.mock_mc_protocol, mc.server.protocol);

ng.connection.host = getValue(data.hostname, ng.connection.host);
ng.connection.port = getValue(data.mock_ng_port, ng.connection.port);
ng.server.host = getValue(data.hostname, ng.server.host);
ng.server.port = getValue(data.next_gen_port, ng.server.port);
ng.server.protocol = getValue(data.next_gen_protocol, ng.server.protocol);

cr.connection.host = getValue(data.hostname, cr.connection.host);
cr.connection.port = getValue(data.mock_cr_port, cr.connection.port);
cr.server.host = getValue(data.hostname, cr.server.host);
cr.server.port = getValue(data.api_credits_port, cr.server.port);

bi.connection.host = getValue(data.hostname, bi.connection.host);
bi.connection.port = getValue(data.mock_bi_port, bi.connection.port);
bi.connection.tls = (bi.connection.tls) ? bi.connection.tls : {} ;
bi.connection.tls.certPath = getValue(data.ssl_cert, bi.connection.tls.certPath);
bi.connection.tls.keyPath = getValue(data.ssl_key, bi.connection.tls.keyPath);
bi.server.host = getValue(data.hostname, bi.server.host);
bi.server.port = getValue(data.https_port, bi.server.port);
bi.server.protocol = getValue(data.mock_bi_protocol, bi.server.protocol);

yf.connection.host = getValue(data.hostname, yf.connection.host);
yf.connection.port = getValue(data.mock_yf_port, yf.connection.port);
yf.server.host = getValue(data.hostname, yf.server.host);
yf.server.port = getValue(data.api_yapofact_port, yf.server.port);

sms.connection.host = getValue(data.hostname, sms.connection.host);
sms.connection.port = getValue(data.mock_sms_port, sms.connection.port);
sms.server.host = getValue(data.hostname, sms.server.host);
sms.server.port = getValue(data.api_sms_port, sms.server.port);

su.connection.host = getValue(data.hostname, su.connection.host);
su.connection.port = getValue(data.mock_su_port, su.connection.port);
su.server.host = getValue(data.hostname, su.server.host);
su.server.port = getValue(data.api_subscription_port, su.server.port);

ads_evaluator.connection.host = getValue(data.hostname, ads_evaluator.connection.host);
ads_evaluator.connection.port = getValue(data.mock_ads_evaluator_port, ads_evaluator.connection.port);
ads_evaluator.server.host = getValue(data.hostname, ads_evaluator.server.host);
ads_evaluator.server.port = getValue(data.api_ads_evaluator_port, ads_evaluator.server.port);

profile.connection.host = getValue(data.hostname, profile.connection.host);
profile.connection.port = getValue(data.mock_profile_port, profile.connection.port);
profile.server.host = getValue(data.hostname, profile.server.host);
profile.server.port = getValue(data.api_profile_port, profile.server.port);

pd.connection.host = getValue(data.hostname, pd.connection.host);
pd.connection.port = getValue(data.mock_pd_port, pd.connection.port);
pd.server.host = getValue(data.hostname, pd.server.host);
pd.server.port = getValue(data.api_Payment_Delivery_port, pd.server.port);

if (fs.writeFileSync) {
	fs.writeFileSync("./config.json", JSON.stringify(config, null, 4), "utf8");
} else {
	fs.writeFile("./config.json", JSON.stringify(config, null, 4), "utf-8");
}
