# -*- coding: latin-1 -*-
import yapo, time, os
from yapo.pages.desktop import CheckBill
from yapo import conf

class CheckBillTest(yapo.SeleniumTest):
    """ """
    chrome_profile_options = { '--disable-web-security', '--allow-file-access-from-files', '--allow-runnging-insecure-content', '--disable-site-isolation-trials'}

    snaps = ['accounts', 'diff_store_sales_report']

    def test_get_bill_empty(self, wd):
        """ Test error when empty email """
        cb = CheckBill(wd)
        cb.go()
        cb.submit_button.click()
        self.assertTrue(cb.is_element_present('err_email'))
        self.assertTrue(cb.is_element_present('err_bill'))

    def test_get_bill_bad_mail(self, wd):
        """ Test error when bad email """
        cb = CheckBill(wd)
        cb.go()
        cb.email.send_keys('LOLOLOL')
        cb.submit_button.click()
        self.assertEquals(cb.err_email.text, "Escribe un e-mail v�lido")

    def test_get_bill_god_mail(self, wd):
        """ test good email with and without account  """
        cb = CheckBill(wd)
        cb.go()
        cb.email.send_keys('susana.oria@foobar.cl')
        cb.bill.send_keys('123')
        cb.submit_button.click()
        self.assertEquals(cb.no_bills_msg.text, "No existen boletas asociadas a este e-mail")
        cb.go()
        cb.email.send_keys('prepaid5@blocket.se')
        cb.bill.send_keys('4')
        cb.submit_button.click()
        self.assertEquals(cb.no_bills_msg.text, "No existen boletas asociadas a este e-mail")

    def test_bill_download(self, wd):
        """ Bill Download """
        filepath = os.path.join(yapo.conf.TOPDIR, 'tests', 'tmp', '2.pdf')
        if os.path.exists(filepath):
            os.remove(filepath)

        cb = CheckBill(wd)
        cb.go()
        cb.email.send_keys('prepaid5@blocket.se')
        cb.bill.send_keys('2')
        cb.radio_bill.click()
        cb.submit_button.click()

        md5sum = yapo.utils.get_downloaded_file_content(wd, filepath)
        self.assertEquals('8156c03f522023d61b5976ba9fb1013d', md5sum)

    def test_invoice_download(self, wd):
        """ Invoice Download """
        filepath = os.path.join(yapo.conf.TOPDIR, 'tests', 'tmp', '4.pdf')
        if os.path.exists(filepath):
            os.remove(filepath)

        cb = CheckBill(wd)
        cb.go()
        cb.email.send_keys('many@ads.cl')
        cb.bill.send_keys('4')
        cb.radio_invoice.click()
        cb.submit_button.click()

        md5sum = yapo.utils.get_downloaded_file_content(wd, filepath)
        self.assertEquals('8156c03f522023d61b5976ba9fb1013d', md5sum)
