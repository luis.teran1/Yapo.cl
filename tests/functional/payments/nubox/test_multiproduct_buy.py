# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo import utils
from yapo.utils import Products
from yapo.pages import autobump
from yapo.pages.desktop import Login, DashboardMyAds, SummaryPayment, PaymentVerify
from yapo.pages.simulator import Paysim
from yapo.pages.generic import SentEmail

class MultiproductBuy(yapo.SeleniumTest):

    bconfs = {
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0'
    }
    snaps = ['accounts']
    LIST_IDS = {6000666: [Products.LABEL, Products.GALLERY],
                3456789: [Products.LABEL, Products.GALLERY],
                6000667: [Products.LABEL, Products.GALLERY],
                8000073: [Products.LABEL], 
                8000065: [Products.LABEL]
            }
    LIST_IDS_BUMP = {8000065: [Products.BUMP],
                3456789: [Products.BUMP],
                6000666: [Products.BUMP],
                6000667: [Products.BUMP]
            }
    success_title = 'Tu compra se realiz� exitosamente'

    @tags('accounts', 'desktop', 'nubox')
    def test_buy_many_products(self, wd):
        """ Buy many bumps and galleries and check the sent mail """

        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')
        dma = DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(self.LIST_IDS)
        dma.checkout_button.click()
        sp = SummaryPayment(wd)
        sp.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
        utils.rebuild_index()
        utils.run_nubox_php_script("bill_processing.php")
        utils.run_nubox_php_script("bill_send_email.php")

        sent_email = SentEmail(wd)
        sent_email.go()
        self.assertEqual("Hola BLOCKET", sent_email.body.find_element_by_css_selector("#greeting-box > h1").text)
        self.check_products_in_mail(wd, 0, '3', "Vitrina 7 d�as", "$21.150")
        self.check_products_in_mail(wd, 1, '5', "Etiqueta", "$11.650")
        self.assertEqual("Total $32.800", sent_email.body.find_element_by_css_selector("#total-box").text)

    @tags('payment', 'nubox', 'account', 'bump')
    def test_buy_multiple_bump(self, wd):
        """ Pay multiple bumps """

        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')

        for list_id in self.LIST_IDS_BUMP:
            autobump.direct_go(wd, list_id, 3, 4)

        sp = SummaryPayment(wd)
        sp.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()

        payment_verify = PaymentVerify(wd)
        self.assertEqual(payment_verify.text_congratulations.text, self.success_title)
        table_values = payment_verify.get_table_data('sale_table')

        expected_values = [ 
                ['Pesas muy grandes', 'Subir personalizado', '$ 36.000'],
                ['Aviso de prepaid5', 'Subir personalizado', '$ 36.000'],
                ['Departamento Tarapac�', 'Subir personalizado', '$ 69.600'],
                ['Race car', 'Subir personalizado', '$ 51.600'],
        ]
        table_values.sort()
        expected_values.sort()
        self.assertListEqual(table_values, expected_values)

        utils.run_nubox_php_script("bill_processing.php")
        utils.run_nubox_php_script("bill_send_email.php")

        sent_email = SentEmail(wd)
        sent_email.go()
        self.assertEqual("Hola BLOCKET", sent_email.body.find_element_by_css_selector("#greeting-box > h1").text)
        self.check_products_in_mail(wd, 0, '4', "Subir personalizado", "$193.200")
        self.assertEqual("Total $193.200", sent_email.body.find_element_by_css_selector("#total-box").text)


    def check_products_in_mail(self, wd, position, quantity, product, amount):
        rows = wd.find_elements_by_css_selector('#products-table tbody tr')
        cells = rows[position].find_elements_by_css_selector('td')
        self.assertEqual(quantity, cells[0].text, "the product {0} quantity's does not match '{1}'!='{2}'".format(position, quantity, cells[0].text))
        self.assertEqual(product, cells[1].text, "the product {0} product's name does not match '{1}'!='{2}'".format(position, product, cells[1].text))
        self.assertEqual(amount , cells[2].text, "the product {0} amount's does not match with '{1}'!='{2}'".format(position, amount, cells[2].text))

