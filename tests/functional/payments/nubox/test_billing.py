import yapo, os
from yapo.decorators import tags
from yapo.pages.generic import SentEmail
from yapo.utils import trans, restore_snaps_in_a_cooler_way, bconf_overwrite
import subprocess
import logging
import unittest

logger = logging.getLogger(__name__)


def run_php_script(filename):
    command = "php -c {0}/conf/php.ini {1}/scripts/batch/bill_processing/{2}".format(
            yapo.conf.REGRESS_FINAL, yapo.conf.TOPDIR, filename)
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    logger.debug(output)


class Billing(unittest.TestCase):
    """Billing process scripts"""

    def setUp(self):
        restore_snaps_in_a_cooler_way(['accounts', 'diff_store_3shops', 'diff_1purchasefail'])

    def tearDown(self):
        bconf_overwrite('*.*.billsim.purchase_with_bills.ids', "")
        bconf_overwrite('*.*.billsim.purchase_with_invoices.ids', "")
        bconf_overwrite('*.*.billsim.doc_num.6', "")
        bconf_overwrite('*.*.billsim.doc_type.6', "")


    @tags('nubox', 'billing')
    def test_bill_failed_but_ok_in_nubox(self):
        """Bill marked as failed, but reported as OK in nubox"""
        bconf_overwrite('*.*.billsim.fail_by_timeout.ids', "3,4")
        bconf_overwrite('*.*.billsim.purchase_with_invoices.ids', "3,4")
        run_php_script("bill_processing.php")
        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="failed")

        self.assertEquals(len(result), 76)
        # Clean list of ids to fail, so they get "sent"
        bconf_overwrite('*.*.billsim.fail_by_timeout.ids', "")

        run_php_script("verify_failed_bills.php")
        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="sent")
        logger.debug(result)
        self.assertEquals(result['purchase_get_bills.1.purchase_id'], "3")
        self.assertEquals(result['purchase_get_bills.2.purchase_id'], "4")

    @tags('nubox', 'billing')
    def test_bill_failed_and_resend(self):
        """Bill marked as failed and NOK in nubox"""
        bconf_overwrite('*.*.billsim.purchase_with_bills.ids', "3,4")
        run_php_script("bill_processing.php")
        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="failed")

        print(result);
        self.assertEquals(len(result), 76)

        run_php_script("verify_failed_bills.php")
        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="paid")
        print(result);
        self.assertEquals(result['purchase_get_bills.0.purchase_id'], "3")
        self.assertEquals(result['purchase_get_bills.1.purchase_id'], "4")

    @tags('nubox', 'billing')
    def test_sii_code_for_bill_and_invoice(self):
        """Verify if the SII code for bill/invoice it's OK"""
        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="failed")
        self.assertEquals(result['purchase_get_bills.0.purchase_id'], "6")

        bconf_overwrite('*.*.billsim.purchase_with_invoices.ids', "6")
        bconf_overwrite('*.*.billsim.doc_type.6', '33', False)
        bconf_overwrite('*.*.billsim.doc_num.6', '998877', False)
        bconf_overwrite('*.*.billsim.identificador.6', '665544')

        run_php_script("verify_failed_bills.php")

        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="failed")
        self.assertEquals(result['purchase_get_bills.0.purchase_id'], "6")

        bconf_overwrite('*.*.billsim.doc_type.6', '39')
        run_php_script("verify_failed_bills.php")

        result = trans("purchase_get_bills",
                       payment_status="paid",
                       purchase_status="sent")
        print(result)
        self.assertEquals(result['purchase_get_bills.0.purchase_id'], "6")


class BillingProcess(yapo.SeleniumTest):
    """Docuemnts downloading process"""
    snaps = ['bumpedads', 'bills']

    @tags('nubox', 'billing')
    def test_billing_email(self, driver):
        """Run the bill process and check the output"""
        run_php_script("bill_processing.php")
        run_php_script("bill_send_email.php")
        # Check the email
        sent_email = SentEmail(driver)
        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn('Tu Factura Yapo.cl', body_text)
        self.assertIn('de pedido\n9\nFecha\n07/04/2014\n', body_text)
        self.assertIn('\n439786', body_text)
        self.assertIn('Detalle\n1\nSubir Ahora $1.000', body_text)
        self.assertIn('Total $1.000\nDescargar Factura\n  Tu compra ha sido confirmada.', body_text)


class DocumentDownloader(yapo.SeleniumTest):
    """Docuemnts downloading process"""
    snaps = ['bumpedads', 'bills']
    chrome_profile_options = { '--disable-web-security', '--allow-file-access-from-files', '--allow-runnging-insecure-content', '--disable-site-isolation-trials'}

    def download_and_check_document(self, driver, url, expectedMd5, filepath):
        md5sum = yapo.utils.download_and_get_md5(driver, url, filepath)
        self.assertEquals(expectedMd5, md5sum)

    @tags('nubox', 'billing')
    def test_download_bill(self, driver):
        """ download a bill and check the file md5sum to make sure that is the expected file """
        expectedMd5 = '8156c03f522023d61b5976ba9fb1013d'
        filepath = os.path.join(yapo.conf.TOPDIR, 'tests', 'tmp', '1.pdf')
        url = yapo.conf.SSL_URL + '/consultaboleta?bill_number=1&p=8e1405fef93a09485487f7498199a4b6bea7d8fe&doc_type=bill'
        self.download_and_check_document(driver, url, expectedMd5, filepath)


    @tags('nubox', 'billing')
    def test_download_invoice(self, driver):
        """ download a invoice and check the file md5sum to make sure that is the expected file """
        expectedMd5 = '8156c03f522023d61b5976ba9fb1013d'
        filepath = os.path.join(yapo.conf.TOPDIR, 'tests', 'tmp', '2.pdf')
        url = yapo.conf.SSL_URL + '/consultaboleta?bill_number=2&p=b41350d7a65b8cec11d66518c7060b00824435fa&doc_type=invoice'
        self.download_and_check_document(driver, url, expectedMd5, filepath)
