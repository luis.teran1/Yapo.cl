# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo import utils
from yapo.utils import Products
from yapo.pages import autobump
from yapo.pages.desktop import Login, DashboardMyAds, SummaryPayment, PaymentVerify
from yapo.pages.simulator import Paysim
from yapo.pages.generic import SentEmail

class MultiproductBuyEmail(yapo.SeleniumTest):


    snaps = ['accounts']
    bconfs = {
        '*.*.payment_settings.product.1.3.1220.value': 'price:9250',
        '*.*.payment_settings.product.1.3.*.value': 'price:3650',
        '*.*.payment_settings.product.1.3.1220.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1240.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1260.value': 'price:9250',
        '*.*.payment_settings.product.1.3.2020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2080.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2120.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7060.value': 'price:6450',
        '*.*.payment_settings.product.1.1.1220.value': 'price:2450',
        '*.*.payment_settings.product.1.1.1240.value': 'price:2450',
        '*.*.payment_settings.product.1.1.1260.value': 'price:2450',
        '*.*.payment_settings.product.1.1.2020.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2040.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2060.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2080.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2120.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7020.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7040.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7060.value': 'price:1850',
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.3.1220.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1240.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1260.value': 'price:9250',
        '*.*.payment_settings.product.1.31.1220.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1240.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1260.value': 'price:2750',
        '*.*.payment_settings.product.1.31.*.value': 'price:1050',
        '*.*.payment_settings.product.1.32.2020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2060.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2080.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2120.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7060.value': 'price:22050',
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0',
    }

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts'])

    def _option_ab(self, wd, LIST_IDS, EMAIL_DATA):
        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')

        for list_id in LIST_IDS:
            autobump.direct_go(wd, list_id, 1, 1)

        sp = SummaryPayment(wd)
        sp.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
        self.assertIsNotNone(utils.search_mails('({0})'.format(EMAIL_DATA["success_message"])))

        for val in EMAIL_DATA["items"]:
            needle = '"{0}" - "{1}"'.format(val[0], val[1])
            print("SEARCHING FOR:{}".format(needle))
            self.assertIsNotNone(utils.find_in_mails(needle))

    def _option(self, wd, LIST_IDS, EMAIL_DATA):
        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')
        dma = DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(LIST_IDS)
        dma.checkout_button.click()
        sp = SummaryPayment(wd)
        sp.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
        self.assertIsNotNone(utils.search_mails('({0})'.format(EMAIL_DATA["success_message"])))

        for val in EMAIL_DATA["items"]:
            needle = '"{0}" - "{1}"'.format(val[0], val[1])
            print("SEARCHING FOR:{}".format(needle))
            self.assertIsNotNone(utils.find_in_mails(needle))

    def test_multiproduct_buy_email(self, wd):
        """ Email verification for multiproduct"""

        LIST_IDS = {8000073: [Products.LABEL],
                    8000065: [Products.LABEL],
                    6000667: [Products.LABEL, Products.GALLERY],
                    6000666: [Products.LABEL, Products.GALLERY],
                    3456789: [Products.LABEL, Products.GALLERY]
        }

        EMAIL_DATA = {  "items" :[["Etiqueta",  "$2.100"],
                                    ["Etiqueta", "$3.150"],
                                    ["Etiqueta", "$2.200"],
                                    ["Etiqueta", "$2.100"],
                                    ["Etiqueta", "$2.100"],
                                    ["Vitrina 7 d�as", "$3.650"],
                                    ["Vitrina 7 d�as", "$9.250"],
                                    ["Vitrina 7 d�as", "$6.450"],
                                    ],
                        "success_message" : "Resumen de la compra"
        }
        self._option(wd, LIST_IDS, EMAIL_DATA)


    def test_multiproduct_buy_one_bump(self, wd):
        """
        Buy one bump and check the sent mail
        """
        LIST_IDS = { 8000065: [Products.BUMP] }
        EMAIL_DATA = { "items": [["Subir personalizado", "$17.100"]],
                            "success_message": "Resumen de la compra"
        }
        self._option_ab(wd, LIST_IDS, EMAIL_DATA)


    def test_multiproduct_buy_many_bumps(self, wd):
        """
        Buy many bumps (for price group A,B and C) and check the sent mail
        """
        LIST_IDS = {6000667: [Products.BUMP],
                    6000666: [Products.BUMP],
                    3456789: [Products.BUMP]
        }

        EMAIL_DATA ={ "items": [['Subir personalizado', '$17.100'],
                                    ['Subir personalizado', '$44.100'],
                                    ['Subir personalizado', '$33.300']
                                    ],
                            "success_message": "Resumen de la compra"
        }
        self._option_ab(wd, LIST_IDS, EMAIL_DATA)

    def test_multiproduct_buy_one_gallery(self, wd):
        """
        Buy one gallery (for price category group C) and check the sent mail
        """

        LIST_IDS ={ 6000666: [Products.GALLERY]}
        EMAIL_DATA = {"items":[['Vitrina 7 d�as', '$3.650']],
                            "success_message": "Resumen de la compra"
        }
        self._option(wd, LIST_IDS, EMAIL_DATA)
        #TODO: self.assertEqual(u'�Tu aviso ya est� en vitrina!', message)


    def test_multiproduct_buy_one_gallery_real_state(self, wd):
        """
        Buy one gallery (for price category group A) and check the sent mail
        """
        LIST_IDS ={ 6000667: [Products.GALLERY]}
        EMAIL_DATA = { "items": [['Vitrina 7 d�as', '$9.250']],
                            "success_message": "Resumen de la compra"
        }
        self._option(wd, LIST_IDS, EMAIL_DATA)
        # self.check_ad_in_summary_page(0, "", "Vitrina 7 d�as", "$9.200")


    def test_multiproduct_buy_many_galleries(self, wd):
        """
        Buy many galleries
        """
        LIST_IDS = {6000667: [Products.GALLERY],
                    6000666: [Products.GALLERY_1],
                    3456789: [Products.GALLERY_30]
        }
        EMAIL_DATA = {
            "items": [
                ['Vitrina 7 d�as', '$9.250'],
                ['Vitrina 1 d�a', '$1.050'],
                ['Vitrina 30 d�as', '$22.050']
            ],
            "success_message": "Resumen de la compra"
        }
        self._option(wd, LIST_IDS, EMAIL_DATA)


