# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile, desktop
import yapo
from yapo import utils

BUMP_LIST_IDS = {
    'A': (8000010, '$ 2.450', 'Departamento en condominio', 'Subir Ahora'),
    'B': (8000017, '$ 1.850', 'Suzuki Maruti 800cc 2003', 'Subir Ahora'),
    'C': (8000042, '$ 950', 'Vestido de Novia', 'Subir Ahora'),
    'D': (8000021, '$ 5.000', 'Asesora del hogar', 'Subir Ahora')
}


class PaymentFormTest(yapo.SeleniumTest):

    snaps = ['accounts']

    bconfs = {
        '*.*.payment_settings.product.1.1.1220.value': 'price:2450',
        '*.*.payment_settings.product.1.1.2020.value': 'price:1850',
        '*.*.payment_settings.product.1.1.4020.value': 'price:950',
        '*.*.payment_settings.product.1.1.7020.value': 'price:5000',
        '*.*.payment_settings.product.1.60.7020.value': 'price:5000',
        '*.*.payment.product.1.name': 'Subir Ahora',
        '*.*.payment.product.60.name': 'Aviso de empleo'
    }
    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts'])
        utils.flush_redises()

    @tags('desktop', 'payment', 'price_segmentation')
    def test_payment_accept(self, wd):
        """ Testing the payment form with bill for an ad of category group """

        order_number = 166

        order_number = self._payment_accept_invoice(BUMP_LIST_IDS, 'bump', order_number, wd)

    def _payment_accept(self, list_id, product, order_number, wd):
        for group, data in list_id.items():
            list_id, product_price, ad_subject, product_name = data
            payment_page = mobile.Payment(wd)
            check = payment_page.go_to_pay_bump if product == 'bump' else payment_page.go_to_pay_inserting_fee
            check(list_id)
            payment_page.pay_button.click()
            paysim = mobile.Paysim(wd)
            paysim.pay_accept()
            payment_verify = desktop.PaymentVerify(wd)
            params = {
                "payment_verify": payment_verify,
                "ad_subject": ad_subject,
                "product_price": product_price,
                "order_number": order_number,
                "product_name": product_name
            }
            self._many_asserts(params)
            order_number += 1
        return order_number

    @tags('desktop', 'payment', 'price_segmentation')
    def test_payment_accept_invoice(self, wd):
        """ Test the payment form, with invoice for each category group """

        order_number = 166
        order_number = self._payment_accept_invoice(BUMP_LIST_IDS, 'bump', order_number, wd)

    def _payment_accept_invoice(self, list_ids, product, order_number, wd):

        for group, data in list_ids.items():
            list_id, product_price, ad_subject, product_name = data
            payment_page = desktop.Payment(wd)
            check = payment_page.go_to_pay_bump if product == 'bump' else payment_page.go_to_pay_inserting_fee
            check(list_id)
            payment_page.change_doc_type(payment_page.INVOICE)
            payment_page.pay_button_invoice.click()
            self.assertEqual(payment_page.err_lob.text, "Escribe tu giro")
            self.assertEqual(payment_page.err_rut.text, "El RUT es muy corto")
            self.assertEqual(payment_page.err_address.text, "La direcci�n es muy corta")

            payment_page.fill_form(name = 'Dany', lob = 'ocio', rut = '100000-4', address = 'en su casa 1000', commune = '295')
            payment_page.pay_button_invoice.click()
            paysim = mobile.Paysim(wd)
            paysim.pay_accept()
            payment_verify = desktop.PaymentVerify(wd)
            params = {
                "payment_verify": payment_verify,
                "ad_subject": ad_subject,
                "product_price": product_price,
                "order_number": order_number,
                "product_name": product_name
            }
            self._many_asserts(params)
            order_number += 1
            return order_number

    @tags('desktop', 'payment', 'price_segmentation')
    def test_payment_wrong_data(self, wd):
        """ Test the payment form forcing incorrect amount of money for bump """

        self._payment_wrong_data(BUMP_LIST_IDS, 'bump', wd)

    def _payment_wrong_data(self, list_ids, product, wd):

        for group, data in list_ids.items():
            list_id, product_price, ad_subject, product_name = data
            payment_page = mobile.Payment(wd)
            check = payment_page.go_to_pay_bump if product == 'bump' else payment_page.go_to_pay_inserting_fee
            check(list_id)
            payment_page.pay_button.click()
            paysim = mobile.Paysim(wd)
            paysim.paysim_populate(amount = 100)
            paysim.pay_accept()

            payment_fail = desktop.PaymentVerifyError(wd)
            self.assertEqual(payment_fail.title_oops.text, 'Oops... La transacci�n no pudo ser realizada')
            self.assertEqual(payment_fail.text_fail.text, 'Parece que los datos del documento no coinciden con los indicados.')

    @tags('desktop', 'payment', 'price_segmentation')
    def test_payment_refuse(self, wd):
        """ Test the payment form, then refuses payment on the simulator """

        order_number = 168
        order_number = self._payment_refuse(BUMP_LIST_IDS, 'bump', order_number, wd)

    def _payment_refuse(self, list_ids, product, order_number, wd):
        title_fail = "Oops... La transacci�n no pudo ser realizada"
        description_fail = "Parece que hubo un problema con tu compra .* y no pudo completarse el pago."
        for group, data in list_ids.items():
            list_id, product_price, ad_subject, product_name = data
            payment_page = desktop.Payment(wd)
            check = payment_page.go_to_pay_bump if product == 'bump' else payment_page.go_to_pay_inserting_fee
            check(list_id)
            payment_page.pay_button.click()

            paysim = mobile.Paysim(wd)
            paysim.pay_refuse()

            payment_fail = desktop.PaymentVerifyFail(wd)
            self.assertEqual(payment_fail.title_oops.text,title_fail)
            self.assertRegexpMatches(payment_fail.text_fail.text, description_fail)
            payment_page.pay_button.click()

            paysim = mobile.Paysim(wd)
            paysim.pay_cancel()
            payment_fail = desktop.PaymentVerifyFail(wd)
            self.assertEqual(payment_fail.title_oops.text,title_fail)
            self.assertRegexpMatches(payment_fail.text_fail.text, description_fail)
            payment_page.pay_button.click()

            paysim = mobile.Paysim(wd)
            paysim.pay_accept()
            payment_verify = desktop.PaymentVerify(wd)
            params = {
                "payment_verify": payment_verify,
                "ad_subject": ad_subject,
                "product_price": product_price,
                "order_number": order_number,
                "product_name": product_name
            }
            self._many_asserts(params)

            order_number += 3
        return order_number

    def _many_asserts(self, params, **kwargs):
        """ Such asserts """
        payment_verify = params['payment_verify']
        ad_subject = params['ad_subject']
        product_price = params['product_price']
        order_number = params['order_number']
        product_name = params['product_name']

        self.assertEqual(payment_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')

        table_values = payment_verify.get_table_data('sale_table')
        print(table_values)
        if len(ad_subject) > 25:
            ad_subject = '{}...'.format(ad_subject[:25])
        self.assertEqual(table_values[0][0], ad_subject)
        self.assertIn(product_name, table_values[0][1])
        self.assertIn(product_price, table_values[0][2])

        table_values = payment_verify.get_table_data('purchase_table')
        print("tabla")
        print(table_values)
        self.assertEqual(table_values[0][1], str(order_number))
        self.assertTrue(table_values[2][1].startswith("**** **** ****"))
        self.assertEquals(table_values[3][1], "Yapo.cl")
        self.assertEquals(table_values[4][1], "Venta")
        self.assertEquals(table_values[5][1], "Cr�dito")
        self.assertEquals(table_values[6][1], "Sin Cuotas")
        self.assertEquals(table_values[7][1], "1")
