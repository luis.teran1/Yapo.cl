# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import desktop_list, desktop, mobile
from yapo.pages.ad_insert import AdInsertResult
import yapo
import time
from yapo.utils import UserAgents, trans, search_mails, clean_mails
import unittest

LIST_IDS = {'A': (8000010, '2.900'), 'B': (8000017, '2.150'), 'C': (8000082,'1.500')}

class DesktopBumpUnloggedTest(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_products']

    @tags('bump', 'unlogged', 'desktop', 'price_segmentation', 'ad_delete')
    def test_desktop_delete_product_unlogged(self, wd):
        """ Price Segmentation: testing the price of a bump in ad delete page, being unlogged on desktop """

        for list_id, bump_price in LIST_IDS.values():
            ad_delete = desktop_list.AdDeletePage(wd)
            ad_delete.go(list_id)

            # iterating all the options that offers bump
            for reason in range(4, 7): # from 3 to 6
                ad_delete.select_reason(reason)
                bump_msg =  getattr(ad_delete, 'bump_msg_{0}'.format(reason))
                self.assertIn(bump_price, bump_msg.text)
                # checking the lightbox
                ad_delete.lightbox_btn.click()
                lightbox = desktop.BumpInfoLightbox(wd)
                lightbox.wait.until_visible('price_list_items')
                self.assertIn(bump_price, lightbox.price_list_items[1].text)
                self.assertIn(bump_price, lightbox.price_answer.text)
                lightbox.close()

    @tags('bump', 'unlogged', 'desktop', 'price_segmentation')
    def test_desktop_bump_unlogged_from_email(self, wd):
        """ Price Segmentation: testing the price of a bump shown in the page when coming from the opportunity e-mail """

        for list_id, bump_price in LIST_IDS.values():
            bump_payment_landing = desktop.Payment(wd)
            bump_payment_landing.go_to_pay_bump(list_id)
            bump_payment_landing.wait.until_loaded()
            self.assertIn(bump_price, bump_payment_landing.form1_product_price.text)
            bump_payment_landing.tab2.click()
            self.assertIn(bump_price, bump_payment_landing.form2_product_price.text)

class MobileBumpUnloggedTest(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_products']
    user_agent = UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('bump', 'unlogged', 'mobile', 'price_segmentation', 'ad_delete')
    def test_mobile_delete_product_unlogged(self, wd):
        """ Price Segmentation: testing the price of a bump in ad delete page, being unlogged on mobile """

        for list_id, bump_price in LIST_IDS.values():
            ad_delete = mobile.DeletePage(wd)
            ad_delete.go(list_id)

            # iterating all the options that offers bump
            for reason in (4, 5, 6): # 0-based
                ad_delete.select_reason(reason)
                bump_msg = ad_delete.active_reason_div.find_elements_by_css_selector('#form_{} .graybox'.format(reason))[1]
                self.assertIn(bump_price, bump_msg.text)

                # checking the lightbox
                ad_delete.active_reason_div.find_element_by_class_name('badge-help').click()
                lightbox = mobile.BumpInfoLightbox(wd)
                lightbox.q3.click()
                self.assertIn(bump_price, lightbox.a3_content.text)
                lightbox.q3.click()
                lightbox.close_button.click()

    @tags('bump', 'unlogged', 'mobile', 'price_segmentation', 'ad_edit')
    def test_mobile_bump_in_ad_edit(self, wd):
        """ Price Segmentation: testing the price of a bump in ad edit mobile """

        for list_id, bump_price in LIST_IDS.values():
            edit_pw = mobile.EditPasswordPage(wd)
            edit_pw.go(list_id)
            edit_pw.validate_password('123123123')
            edit = mobile.AdEdit(wd)
            self.assertIn(bump_price, edit.bump.text)

            # actually editing to avoid FSM shit
            edit.submit.click()
            result = AdInsertResult(wd)
            result.wait.until_loaded()
            self.assertTrue(result.was('success_mobile'))

class PriceSegmentationEmail(yapo.SeleniumTest):
    """ Price Segmentation: testing the e-mails without selenium """

    snaps = ['accounts', 'diff_ads_with_products']

    @tags('email', 'bump')
    def test_advertisement_email_prices(self, wd):
        """ Verifying prices in bump advertisement emails """

        AD_IDS = {8000010: (30, '2.900'), 8000017: (38, '2.150'), 8000042: (62, '1.500')}
        for list_id, data in AD_IDS.items():
            clean_mails()
            result = trans('admail', mail_type='mail_advertisement_bump', ad_id=data[0])
            price = search_mails('(\${0})'.format(data[1]))
            self.assertEqual(price, '$' + data[1])
