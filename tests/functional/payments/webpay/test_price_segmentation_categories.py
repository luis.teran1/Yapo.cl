# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import desktop_list, desktop, mobile
from yapo.pages.ad_insert import AdInsertWithoutAccountSuccess
import yapo
import time
from yapo.utils import UserAgents, trans, search_mails, clean_mails
import unittest

LIST = {'A': {"list_id" : [8000014, 8000614, 8000074, 8000644, 8000044, 8000674],
               "price" : '2.900'},
        'B-car': {"list_id" : [ 8000689, 8001289, 8001304, 8001319, 8000179], "price" : '2.150'},
        'B-job': {"list_id" : [8001694, 8001709, 8001139, 8001154], "price" : '2.500'},
        'C': {"list_id" : [8000194, 8001664, 8001064, 8001094, 8000254, 8000164, 8001574, 8001379, 8000809, 8000854, 8000269, 8000854, 8001049, 8000449, 8000359, 8001514, 8001604, 8000869, 8000314, 8000419, 8001754, 8000389, 8001544],
            "price" : '1.500'}
        }

class DesktopBumpUnloggedTest(yapo.SeleniumTest):

    snaps = ['reg_cat']

    @tags('bump', 'unlogged', 'desktop', 'price_segmentation', 'ad_delete')
    def test_desktop_delete_product_unlogged_each_cat(self, wd):
        """ Price Segmentation: testing the price of a bump in ad delete page, being unlogged on desktop for each category """

        for group, data in LIST.items():
            for list_id in data["list_id"]:
                ad_delete = desktop_list.AdDeletePage(wd)
                ad_delete.go(list_id)

                # iterating all the options that offers bump
                for reason in range(4, 7): # from 3 to 6
                    ad_delete.select_reason(reason)
                    bump_msg =  getattr(ad_delete, 'bump_msg_{0}'.format(reason))
                    self.assertIn(data["price"], bump_msg.text)
                    # checking the lightbox
                    ad_delete.lightbox_btn.click()
                    lightbox = desktop.BumpInfoLightbox(wd)
                    lightbox.wait.until_visible('price_list_items')
                    self.assertIn(data["price"], lightbox.price_list_items[1].text)
                    self.assertIn(data["price"], lightbox.price_answer.text)
                    lightbox.close()

    @tags('bump', 'unlogged', 'desktop', 'price_segmentation')
    def test_desktop_bump_unlogged_from_email_each_cat(self, wd):
        """ Price Segmentation: testing the price of a bump shown in the page when coming from the opportunity e-mail for each category """

        for group, data in LIST.items():
            print('Testing for ads in group {}'.format(group))
            for list_id in data["list_id"]:
                print(list_id)
                bump_payment_landing = desktop.Payment(wd)
                bump_payment_landing.go_to_pay_bump(list_id)
                print("Comparing: {} vs {}".format(data["price"], bump_payment_landing.form1_product_price.text))
                self.assertIn(data["price"], bump_payment_landing.form1_product_price.text)
                bump_payment_landing.tab2.click()
                self.assertIn(data["price"], bump_payment_landing.form2_product_price.text)


class MobileBumpUnloggedTest(yapo.SeleniumTest):

    snaps = ['reg_cat']
    user_agent = UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('bump', 'unlogged', 'mobile', 'price_segmentation', 'ad_delete')
    def test_mobile_delete_product_unlogged_each_cat(self, wd):
        """ Price Segmentation: testing the price of a bump in ad delete page, being unlogged on mobile for each category """

        for group, data in LIST.items():
            for list_id in data["list_id"]:
                ad_delete = mobile.DeletePage(wd)
                ad_delete.go(list_id)
                # iterating all the options that offers bump
                for reason in (4, 5, 6): # 0-based
                    ad_delete.select_reason(reason)
                    bump_msg = ad_delete.active_reason_div.find_elements_by_css_selector('#form_{} .graybox'.format(reason))[1]
                    self.assertIn(data["price"], bump_msg.text)
                    # checking the lightbox
                    ad_delete.active_reason_div.find_element_by_class_name('badge-help').click()
                    lightbox = mobile.BumpInfoLightbox(wd)
                    lightbox.q3.click()
                    self.assertIn(data["price"], lightbox.a3_content.text)
                    lightbox.q3.click()
                    lightbox.close_button.click()
