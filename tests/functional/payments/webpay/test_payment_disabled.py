# -*- coding: latin-1 -*-
import yapo
from yapo.pages.desktop import Payment
from yapo.pages.desktop_list import AdViewDesktop, AdDeletePage
from yapo.decorators import tags


class PaymentDisabled(yapo.SeleniumTest):

    snaps = ['accounts']
    list_id = '8000073'
    bconfs = {
        '*.*.payment.enabled': '0',
        '*.*.payment.allowedip.127.0.0.1': '0',
    }

    @tags("payment", "ad_view")
    def test_bump_on_vi_disabled(self, wd):
        """ Check bump on VI when payment is disabled """
        ad_view = AdViewDesktop(wd)
        ad_view.go(self.list_id)
        self.assertNotPresent(ad_view, 'product_link_bump')

    @tags("payment", "ad_delete")
    def test_bump_on_delete_page(self, wd):
        """ Check bump on delete page when payment is disabled """
        ad_delete = AdDeletePage(wd)
        ad_delete.go(self.list_id)
        ad_delete.select_reason(4)
        self.assertNotPresent(ad_delete, 'bum_btn_4')

    @tags("payment")
    def test_bump_with_direct_url(self, wd):
        """ Check adding bump to cart when payment is disabled """
        payment = Payment(wd)
        payment.go_to_pay_bump(self.list_id)
        payment.wait.until_visible('disabled_title')
        self.assertEquals(payment.disabled_title.text, '�Oops!')
