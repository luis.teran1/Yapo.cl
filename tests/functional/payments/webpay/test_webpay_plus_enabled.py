# -*- coding: latin-1 -*-
from yapo import conf, utils
from yapo.decorators import tags
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.simulator import Paysim, PaymentError
from yapo.pages.control_panel import WebSQL
import yapo


class TestWebpayPlusDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.*.payment.desktop.webpay_plus.enabled': '1',
        '*.*.payment.msite.webpay_plus.enabled': '1',
        '*.*.webpay_service.host': 'https://' + yapo.conf.REGRESS_HOSTNAME,
        '*.*.webpay_service.port': yapo.conf.REGRESS_HTTPD_PORT,
        '*.*.webpay_service.healthCheck.path': '/payment_rest/TyrionHealthCheck',
        '*.*.webpay_service.startPayment.path': '/payment_rest/TyrionStartPayment',
        '*.*.webpay_service.verifyPayment.path': '/payment_rest/TyrionVerifyPayment',
        '*.*.webpay_service.nullifyPayment.path': '/payment_rest/TyrionNullifyPayment',
    }

    platform = desktop
    subject = 'Aviso de prepaid5'
    list_id = '8000065'
    product = '1'
    user = 'prepaid5@blocket.se'
    passwd = '123123123'
    error_msj = 'Oops... La transacci�n no pudo ser realizada'

    def __common_actions(self, wd):

        login = self.platform.Login(wd)
        login.login(self.user, self.passwd)
        summary = self.platform.SummaryPayment(wd)
        summary.add_product(self.list_id, self.product)
        summary.wait.until_loaded()
        summary.wait.until_present('products_list_table')
        self.assertEqual(
            len(summary.get_displayed_table_data(table_name='products_list_table')), 1)
        self.assertIn('Subir Ahora', summary.product_row0.text)
        webpay = summary.radio_webpay.find_element_by_name(
            'payment_method').get_attribute('value')
        self.assertEqual(webpay, 'webpay_plus')
        summary.pay_with_webpay()

        paysim = Paysim(wd)
        self.assertTrue(paysim.input_token_ws.is_displayed())
        self.assertEqual(paysim.header_text.text,
                         'You are trying to pay a product with Webpay plus')
        return paysim

    def __scenario_success(self, wd, payment_params, text_success):
        paysim = self.__common_actions(wd)
        if payment_params is not None:
            paysim.fill_form(**payment_params)
        paysim.pay_accept()

        paysim.wait.until(lambda wd: Paysim(
            wd).header_text.text == 'Webpay plus voucher')
        paysim.button_accept.click()
        pay_success = self.platform.PaymentVerify(wd)
        pay_success.wait.until_present('text_congratulations')
        if self.platform == mobile:
            pay_success.buy_head.click()
        self.assertIn(text_success, pay_success.purchase_table.text)

    def __scenario_refuse(self, wd, payment_params, text_error, action='refuse'):
        paysim = self.__common_actions(wd)
        if payment_params is not None:
            paysim.fill_form(**payment_params)
        if action == 'refuse':
            paysim.pay_refuse()
        else:
            paysim.pay_cancel()
        errorpage = PaymentError(wd)
        self.assertEqual(errorpage.title.text, text_error)


class TestWebpayPlusMobile(TestWebpayPlusDesktop):
    user_agent = utils.UserAgents.IOS
    platform = mobile


class AlreadyPaid(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_adeptus_upsold']
    user_agent = utils.UserAgents.ANDROID
    platform = mobile
    bconfs = {
        '*.*.payment.desktop.webpay_plus.enabled': '1',
        '*.*.payment.msite.webpay_plus.enabled': '1',
        '*.*.webpay_service.host': 'https://' + yapo.conf.REGRESS_HOSTNAME,
        '*.*.webpay_service.port': yapo.conf.REGRESS_HTTPD_PORT,
        '*.*.webpay_service.healthCheck.path': '/payment_rest/TyrionHealthCheck',
        '*.*.webpay_service.startPayment.path': '/payment_rest/TyrionStartPayment',
        '*.*.webpay_service.verifyPayment.path': '/payment_rest/TyrionVerifyPayment',
        '*.*.webpay_service.nullifyPayment.path': '/payment_rest/TyrionNullifyPayment',
    }

    @tags('payment', 'webpay_plus')
    def test_pay_weekly_already_paid_check_report(self, wd):
        """ Try to pay an upselling by mobile and fail on apply product,
            websql should have new error
        """
        payment = self.platform.Payment(wd)
        payment.go_to_pay_bump('1220.1', '101')
        payment.press_pay_button()
        paysim = Paysim(wd)
        paysim.pay_accept()
        paysim.wait.until(lambda wd: Paysim(
            wd).header_text.text == 'Webpay plus voucher')
        paysim.button_accept.click()
        pay_error = self.platform.PaymentVerifyError(wd)
        pay_error.wait.until_present('title_oops')
        self.assertEqual(pay_error.title_oops.text,
                         '�Disculpa las molestias!')

        cp = WebSQL(wd)
        cp.login('dany', 'dany')
        cp.call_websql('Product fail Report', **{})
        search_data_expected_value = [
            'prepaid5@blocket.se', '- Subir Semanal -', '8350', '170', 'msite', 'credit']
        table_values = cp.get_table_data('table')
        found = False
        for row in table_values:
            if search_data_expected_value[0] in row:
                for data in search_data_expected_value:
                    self.assertIn(data, row)
                found = True

        self.assertTrue(found, "Expected data not found")
