# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.decorators import tags
from yapo.pages.credits import CreditsForm
from yapo.steps.accounts import login_and_go_to_dashboard
import unittest


class FixedCreditsDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    email = 'prepaid5@blocket.se'
    password = '123123123'
    is_desktop = True
    platform = desktop

    #{ value, expected_value }
    CREDITS_AMOUNTS = {
        3000: [['Yapesos', 'Personalizados', '$ 3.000', '']],
        10000: [['Yapesos', 'Personalizados', '$ 10.000', '']],
        25000: [['Yapesos', 'Personalizados', '$ 25.000', '']],
        150000: [['Yapesos', 'Personalizados', '$ 150.000', '']]
    }

    @tags('credits', 'payment', 'desktop')
    def test_add_fixed_credits(self, wd):
        """ That adding a fixed amount of credits to the cart"""
        login_and_go_to_dashboard(
            wd, self.email, self.password, from_desktop=self.is_desktop)
        credits_form = CreditsForm(wd)

        for amount, expected in self.CREDITS_AMOUNTS.items():
            credits_form.go()
            credits_form.click_fixed_credits_button(amount)
            summary = self.platform.SummaryPayment(wd)
            table_values = summary.get_table_data()
            self.assertListEqual(table_values, expected)


class FixedCreditsMobile(FixedCreditsDesktop):

    user_agent = utils.UserAgents.NEXUS_5
    is_desktop = False
    platform = mobile

    #{ value, expected_value }
    CREDITS_AMOUNTS = {
        3000: [['Yapesos', 'Personalizados', '$3.000']],
        10000: [['Yapesos', 'Personalizados', '$10.000']],
        25000: [['Yapesos', 'Personalizados', '$25.000']],
        150000: [['Yapesos', 'Personalizados', '$150.000']]
    }
