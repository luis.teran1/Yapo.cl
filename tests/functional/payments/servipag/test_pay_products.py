# -*- coding: latin-1 -*-
import yapo
from yapo.pages import desktop
from yapo.decorators import tags
from yapo.steps import pay_products_logged
from yapo.utils import Products
from yapo import utils


class PayProducts(yapo.SeleniumTest):
    """Suite for testing Servipag payments"""

    snaps = ['accounts']
    bconfs = {
        '*.*.payment.servipag.retry': 3
        }

    @tags('payment', 'servipag')
    def test_pay_bump(self, wd):
        """Pay bump with Servipag"""
        pay_products_logged.servipag_pay_autobump(wd, 8000073, 1, 2)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertEqual(payment_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')

    @tags('payment', 'servipag')
    def test_pay_gallery(self, wd):
        """Pay gallery with Servipag"""
        pay_products_logged.servipag_pay_product(wd, 6000667, Products.GALLERY)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertEqual(payment_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')

    @tags('payment', 'servipag')
    def test_retry_payment(self, wd):
        """Check correct payment when servipag retries"""
        pay_products_logged.servipag_pay_product(wd, 8000073, Products.GALLERY)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertEqual(payment_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')

