# -*- coding: latin-1 -*-
import yapo
from yapo.pages import desktop
from yapo.decorators import tags
from yapo.steps import pay_products_logged
from yapo.utils import Products
from yapo import utils


class PayProducts(yapo.SeleniumTest):
    """Suite for testing Servipag payments"""

    snaps = ['accounts']

    @tags('payment', 'servipag')
    def test_pay_bump_no_xml2(self, wd):
        """Pay wait for service message in payment BUMP when servipag not sends XML2"""
        pay_products_logged.servipag_pay_autobump(wd, 8000073, 1, 2, send_xml2=False)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertIn('demora en su activaci�n',payment_verify.text_congratulation_message.text)
        self.assertIsNotNone(utils.search_mails('generado una compra con Servipag con atraso en la activaci�n'))

    @tags('payment', 'servipag')
    def test_pay_gallery_no_xml2(self, wd):
        """Pay wait for service message in payment gallery when servipag not sends XML2"""
        pay_products_logged.servipag_pay_product(wd, 6000667, Products.GALLERY, send_xml2=False)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertIn('demora en su activaci�n',payment_verify.text_congratulation_message.text)
        self.assertIsNotNone(utils.search_mails('generado una compra con Servipag con atraso en la activaci�n'))

    #When servipag sends XML2
    @tags('payment', 'servipag')
    def test_pay_bump_with_xml2(self, wd):
        """Pay wait for service message in payment BUMP when servipag sends XML2"""
        pay_products_logged.servipag_pay_autobump(wd, 8000073, 1, 2)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertIn('Tu compra se realiz� exitosamente',payment_verify.text_congratulation_message.text)

    @tags('payment', 'servipag')
    def test_pay_gallery_with_xml2(self, wd):
        """Pay wait for service message in payment gallery when servipag sends XML2"""
        pay_products_logged.servipag_pay_product(wd, 6000667, Products.GALLERY)
        payment_verify = desktop.PaymentVerify(wd)
        self.assertIn('Tu compra se realiz� exitosamente',payment_verify.text_congratulation_message.text)
