# -*- coding: latin-1 -*-
import yapo, time
from yapo import utils
from yapo.utils import trans
from yapo.pages.simulator import Paysim
from yapo.pages.desktop import DashboardMyAds, Login, SummaryPayment, PaymentVerify
from yapo.pages.landing import Landing
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.generic import SentEmail

class DashboardAdStates(yapo.SeleniumTest):

    snaps = ['accounts']

    def test_activate_from_mail(self, wd):
        """ Test various cases related to activate from mail """
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        subject = 'Adeptus Astartes Codex'

        # Delete the ad
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.delete_ad_by_subject(subject)
        utils.rebuild_index()

        # check mail
        self.assertTrue(self.mail_ok(wd, subject, 1))

        # activate and checks landing
        act_link = wd.find_element_by_id('link_activate')
        act_link_href = act_link.get_attribute("href")
        act_link.click()
        land = Landing(wd)
        land.check_landing_url(self, wd)

        # check the error for already activated 
        wd.get(act_link_href)
        body_txt = wd.find_element_by_css_selector('body').text
        self.assertIn("Estamos activando tu aviso", body_txt)
        self.assertIn("Tu aviso estar� activo en los pr�ximos minutos", body_txt)

        # delete the ad (FOR GOOD THIS TIME)
        cmd = trans('authenticate',
            remote_addr = '10.0.1.139',
            username = 'blocket',
            passwd = '430985da851e9223368e820ebde5beaf6c6ef1cc')
        cmd2 = trans('deletead',
            remote_addr = '10.0.1.139',
            id = '8000073',
            source = 'web',
            reason = 'admin_deleted',
            token = cmd['token'])
        utils.rebuild_index()

        # check the error for ad deleted (should go to ad not found)
        wd.get(act_link_href)
        body_txt = wd.find_element_by_css_selector('body').text
        self.assertIn("Este aviso ya no est� disponible en yapo.cl", body_txt)
         
    def test_bump_from_mail(self, wd):
        """ Test various cases related to bump from mail """
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        subject = 'Aviso de prepaid5'

        # Delete the ad
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.delete_ad_by_subject(subject)

        # check mail 
        self.assertTrue(self.mail_ok(wd, subject))

        # bumps from mail and check
        wd.find_element_by_id('link_bump').click() 
        land = Landing(wd)
        land.check_pagos_url(self, wd)
        sum = SummaryPayment(wd)
        self.assertIn(subject, sum.product_row0.text)
        self.assertIn('Subir Ahora', sum.product_row0.text)

        # buy
        sum.link_to_pay_products.click()
        paysim = Paysim(wd)
        paysim.pay_accept()
        payment_verify = PaymentVerify(wd)
        utils.rebuild_index()
        ad_view = AdViewDesktop(wd)
        ad_view.go(8000065)
        self.assertIn(subject, wd.find_element_by_id('da_subject').text)
    
    def mail_ok(self, wd, subject, save_link=0):
        email = SentEmail(wd)
        email.go()
        email_txt = wd.find_element_by_css_selector('body').text
        long_part = 'Si dentro de los pr�ximos d�as quieres volver a activar tu aviso puedes hacerlo directamente desde este correo o en tu cuenta de usuario'
        cond1 = (subject in email_txt)
        cond2 = ('Aviso inactivo' in email_txt)
        cond3 = (long_part in email_txt)
        return cond1 and cond2 and cond3
