# -*- coding: latin-1 -*-
from yapo.component.promo import AdList, EmptyAdList
from yapo.component.promo import TabList, EmptyTabList
from yapo.component.promo import Pagination, NoPagination
from yapo.geography import Regions
from yapo.pages2.common import PromoPage, NoPromoPage
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.mobile_list import AdViewMobile
from selenium.webdriver.common.action_chains import ActionChains
from yapo import utils
from yapo import conf
from yapo import trans
import yapo
import copy
import datetime


class TestPromoPagesBase(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_uf']

    @classmethod
    def setUpClass(cls):
        utils.redis_load_wordsranking()
        super().setUpClass()
        utils.rebuild_josesearch()


class TestPromoPages(TestPromoPagesBase):

    def test_promo_page_without_tabs(self, wd):
        """ Promotional page without tab list component """
        promo = PromoPage(wd).go('consolas')
        self.assertIsInstance(promo.tab_list, EmptyTabList)


class TestPromoPagesNavigation(TestPromoPagesBase):
    AdView = AdViewDesktop

    bconfs = {
        '*.*.common.ppages.endpoint.limit': '5'
    }

    def _load_ad_and_test_navigation(self, wd, current_ad, exp, page=''):
        for direction, expected_ad in exp.items():
            ad = self._load_promo_page_ad(wd, current_ad, page)
            url = wd.current_url
            ad.click()
            self._check_nav_buttons(wd, direction, current_ad, expected_ad, url)

    def _load_promo_page_ad(self, wd, ad, page=''):
        promo = PromoPage(wd).go('arma-tu-primer-hogar')
        if page:
            promo.pagination.click('2')
            promo.pagination.click(page)

        pp_ad = promo.ad_list.get_ad(id=ad['list_id'])
        self.assertEqual(pp_ad.dump({'id', 'title'}), {
            'id': ad['list_id'],
            'title': ad['title']
        })

        return pp_ad

    def _check_nav_buttons(self, wd, direction, current_ad, expected_ad, url):
        adview = self.AdView(wd)
        self.assertRegex(direction, '^(next|prev)$')
        self.assertEqual(adview.ad_id.get_attribute('value'), current_ad['list_id'])

        for where in ['top', 'bottom']:
            link = where + '_' + direction + '_link'
            hover = where + '_' + direction + '_hover'

            if expected_ad:
                nav_link = getattr(adview, link)
                self._check_hover(wd, adview, hover, nav_link, expected_ad['title'])
                self._check_other_ad(wd, adview, nav_link, expected_ad['list_id'])
                wd.back()
            else:
                self.assertNotPresent(adview, link)

        self._check_back_to_results(wd, adview, url)

    def _check_other_ad(self, wd, adview, nav_link, list_id):
        nav_link.click()
        self.assertEqual(adview.ad_id.get_attribute('value'), list_id)

    def _check_hover(self, wd, adview, hover, nav_link, title):
        hover_action = ActionChains(wd).move_to_element(nav_link)
        hover_action.perform()
        navbar_hover = getattr(adview, hover)
        adview.wait.until(lambda x: title.lower() in navbar_hover.text.lower())
        self.assertEquals(title.lower(), navbar_hover.text.lower())

    def _check_back_to_results(self, wd, adview, url):
        adview.wait.until_present('top_back_link')
        adview.top_back_link.click()
        adview.wait.until(lambda x: wd.current_url == url)
        self.assertEquals(url, wd.current_url)


class TestPromoPagesNavigationMobile(TestPromoPagesNavigation):
    AdView = AdViewMobile
    user_agent = utils.UserAgents.IOS

    def _check_back_to_results(self, wd, adview, url):
        adview.back_to_results()
        adview.wait.until(lambda x: wd.current_url == url)
        self.assertEquals(url, wd.current_url)

    def _check_nav_buttons(self, wd, direction, current_ad, expected_ad, url):
        adview = self.AdView(wd)
        self.assertRegex(direction, '^(next|prev)$')
        self.assertEqual(adview.ad_id.get_attribute('value'), current_ad['list_id'])

        if expected_ad:
            adview.navigate(direction)
            self.assertEqual(adview.ad_id.get_attribute('value'), expected_ad['list_id'])
            wd.back()
        else:
            adview.is_extremal(direction)

        self._check_back_to_results(wd, adview, url)


class TestPromoPagesDates(TestPromoPagesBase):

    ppage_data = {
        'title': 'Consolas',
        'url': 'consolas',
        'image': 'image_order/bg_pp.png',
        'start_date': '2016-01-01',
        'category': '1220',
        'search_query': '0 lim:10000 category:1220 *:* casa',
        'pp_id': '98'
    }
    today = datetime.date.today()
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)
    yesterday = datetime.date.today() - datetime.timedelta(days=1)

    def _insert_promo_page_and_go(self, wd, dates={}):
        data = copy.copy(self.ppage_data)
        data.update(dates)
        val = trans.check_trans('set_promotional_page', **data)
        self.assertTrue('status' in val and val['status'] == 'TRANS_OK')
        val = trans.check_trans('publish_promotional_page', pp_id=data['pp_id'], queue=1)
        self.assertTrue('status' in val and val['status'] == 'TRANS_OK')
        val = trans.check_trans('publish_promotional_page')
        self.assertTrue('status' in val and val['status'] == 'TRANS_OK')
        utils.rebuild_josesearch()
        promo = PromoPage(wd).go(self.ppage_data['url'])

    def test_promo_page_inactive_start_date_in_future(self, wd):
        """ Promo page set to start Jan-3000 """
        dates = {'start_date': '3000-01-01'}
        self._insert_promo_page_and_go(wd, dates)
        promo = NoPromoPage(wd).go(self.ppage_data['url'])
        self.assertEqual(promo.PROMO_PAGE_NOT_FOUND_TITLE, promo.not_found_title.text)
        self.assertEqual(promo.PROMO_PAGE_NOT_FOUND_SUBTITLE, promo.not_found_subtitle.text)

    def test_promo_page_inactive_start_date_tomorrow(self, wd):
        """ Promo page set to start tomorrow """
        dates = {'start_date': self.tomorrow}
        self._insert_promo_page_and_go(wd, dates)
        promo = NoPromoPage(wd).go(self.ppage_data['url'])
        self.assertEqual(promo.PROMO_PAGE_NOT_FOUND_TITLE, promo.not_found_title.text)
        self.assertEqual(promo.PROMO_PAGE_NOT_FOUND_SUBTITLE, promo.not_found_subtitle.text)

