# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanelAddUser, ControlPanel, ControlPanelScarface, ControlPanelWhitelist
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from nose_selenium import use_selenium
import yapo
from yapo import utils
from yapo.pages.desktop_list import AdViewDesktop, ReportForm

class ScarfaceTest(yapo.SeleniumTest):

    snaps = ['android']

    @tags('ad_view', 'scarface')
    def test_scarface_duplicated(self, wd):
        """ Scarface: conversation duplicate """
        self._insert_scarface_type(wd, "8000066", "Austin 500kg 2012", "duplicate", 0)

    @tags('ad_view', 'scarface')
    def test_scarface_already_sold(self, wd):
        """ Scarface: accept already sold and user stays in whitelist"""
        self._insert_scarface_type(wd, "6394117", "Peluquera a domicilio", "already_sold", 1, "uid1@blocket.se")

    @tags('ad_view', 'scarface')
    def test_scarface_company(self, wd):
        """ Scarface: accept and notify company ad """
        self._insert_scarface_type(wd, "8000062", "Libro para Psicologo Manual Diagnostico", "company", 2)

    @tags('ad_view', 'scarface')
    def test_scarface_wrong_category(self, wd):
        """ Scarface: reject wrong category """
        self._insert_scarface_type(wd, "8000060", "Lego catalogo 1977 exclusivo coleccionistas", "wrong_category", 3, "android@yapo.cl")

    @tags('ad_view', 'scarface')
    def test_scarface_other_reason(self, wd):
        """ Scarface: reject and notify other reason"""
        self._insert_scarface_type(wd, "8000065", "Rechazado 4", "other_reason", 4)

    @tags('ad_view', 'scarface', 'controlpanel')
    def test_scarface_incorrect_price(self, wd):
        """ Scarface: accept and notify incorrect price"""
        self._insert_scarface_type(wd, "8000062", "Libro para Psicologo Manual Diagnostico", "other_reason", 2, None, "7")
        self.assertIsNotNone(utils.search_mails('(El precio indicado en el aviso no corresponde al ofrecido.)'))

    def _insert_scarface_type(self, wd, list_id, subject, scarface_type, opc, ad_email = None, option_notify_owner = "2"):
        """
        Scarface insert report
        """
        options = { 0 : self._conversation,
                    1 : self._accept,
                    2 : self._accept_notify_ad_owner,
                    3 : self._reject,
                    4 : self._reject_notify_report
                }

        user_email = "{0}@cabr.al".format(scarface_type)
        user_message ="this is a test message {0}".format(scarface_type)

        page = AdViewDesktop(wd)
        page.go(list_id)
        page.abuse_link_button.click()
        getattr(page, 'abuse_option_' + scarface_type).click()

        abuse_form = ReportForm(wd)
        abuse_form.fill_form(abuse_email = user_email, abuse_message = user_message)
        abuse_form.abuse_submit.click()

        options[opc](wd, list_id, subject, user_email, user_message, scarface_type, ad_email, option_notify_owner)

    def _conversation(self, wd, list_id, subject, user_email, user_message, scarface_type, ad_email = None, option_notify_owner = "2"):

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Show abuse queues")
        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content li").text,"{0} (1 report for 1 ad)".format(scarface_type))
        cp_scarface = ControlPanelScarface(wd)
        cp_scarface.go_link_text(scarface_type)
        self.assertEquals(cp_scarface.title.text, subject)
        self.assertEquals(cp_scarface.email_link_reply.text, user_email)

        cp_scarface.email_link_reply.click();
        cp_scarface.fill_form(reply_text = "Please, read this response {0}".format(scarface_type))
        cp_scarface.send_reply.click();

        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content p").text,"No reports found")
        cp.logout()

        self.assertIsNotNone(utils.search_mails('(Please, read this response {0})'.format(scarface_type)))
        self.assertIsNotNone(utils.search_mails('(https?://.*?vi/{0}.*?)[\'\"<\n\s>]'.format(list_id)))
        wd.get(utils.search_mails('(https?://.*?vi/{0}.*?)[\'\"<\n\s>]'.format(list_id)))

        self.assertIn("Please, read this response {0}".format(scarface_type), wd.find_elements_by_css_selector(".abuse-message-body")[1].text)
        self.assertIn(user_message, wd.find_elements_by_css_selector(".abuse-message-body")[0].text)

    def _accept(self, wd, list_id, subject, user_email, user_message, scarface_type, ad_email = None, option_notify_owner = "2"):
        if ad_email is not None:
            self._add_email_whitelist(wd, ad_email)

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Show abuse queues")
        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content li").text,"{0} (1 report for 1 ad)".format(scarface_type))
        cp_scarface = ControlPanelScarface(wd)
        cp_scarface.go_link_text(scarface_type)

        self.assertEquals(cp_scarface.title.text, subject)
        self.assertEquals(cp_scarface.email_link_reply.text, user_email)

        cp_scarface.accept.click()

        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content p").text,"No reports found")
        cp.logout()

        self.assertIsNotNone(utils.search_mails('(Hemos aceptado tu denuncia)'))
        self.assertIsNotNone(utils.search_mails('({0})'.format(subject)))

        if ad_email is not None:
            self._verify_email_whitelist(wd, ad_email)

    def _accept_notify_ad_owner(self, wd, list_id, subject, user_email, user_message, scarface_type, ad_email = None, option_notify_owner = "2"):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Show abuse queues")
        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content li").text,"{0} (1 report for 1 ad)".format(scarface_type))
        cp_scarface = ControlPanelScarface(wd)
        cp_scarface.go_link_text(scarface_type)

        self.assertEquals(cp_scarface.title.text, subject)
        self.assertEquals(cp_scarface.email_link_reply.text, user_email)

        select_accept = Select(cp_scarface.option_notify_owner)
        select_accept.select_by_value(str(option_notify_owner))

        cp_scarface.accept_notify_owner.click()

        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content p").text,"No reports found")
        cp.logout()

        self.assertIsNotNone(utils.search_mails('(Hemos aceptado tu denuncia)'))
        self.assertIsNotNone(utils.search_mails('({0})'.format(subject)))

    def _reject(self, wd, list_id, subject, user_email, user_message, scarface_type, ad_email = None, option_notify_owner = "2"):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Show abuse queues")
        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content li").text,"{0} (1 report for 1 ad)".format(scarface_type))
        cp_scarface = ControlPanelScarface(wd)
        cp_scarface.go_link_text(scarface_type)

        self.assertEquals(cp_scarface.title.text, subject)
        self.assertEquals(cp_scarface.email_link_reply.text, user_email)

        cp_scarface.reject.click()

        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content p").text,"No reports found")
        cp.logout()

        self.assertIsNotNone(utils.search_mails('(Despu�s de investigar tu denuncia)'))
        self.assertIsNotNone(utils.search_mails('({0})'.format(subject)))

    def _reject_notify_report(self, wd, list_id, subject, user_email, user_message, scarface_type, ad_email = None, option_notify_owner = "2"):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Show abuse queues")
        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content li").text,"{0} (1 report for 1 ad)".format(scarface_type))
        cp_scarface = ControlPanelScarface(wd)
        cp_scarface.go_link_text(scarface_type)

        self.assertEquals(cp_scarface.title.text, subject)
        self.assertEquals(cp_scarface.email_link_reply.text, user_email)

        select_accept = Select(cp_scarface.option_notify_report)
        select_accept.select_by_value("1")

        cp_scarface.reject_notify_report.click()

        self.assertEquals(wd.find_element(By.CSS_SELECTOR, ".module_content p").text,"No reports found")
        cp.logout()

        self.assertIsNotNone(utils.search_mails('(Yapo no interfiere en la relaci�n entre el comprador y el vendedor)'))
        self.assertIsNotNone(utils.search_mails('({0})'.format(subject)))
        self.assertIsNotNone(utils.search_mails('(https?://.*?vi/{0}.*?)[\'\"<\n\s>]'.format(list_id)))

    def _insert_admin_controlpanel(self, wd):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Add user")
        cp_adduser = ControlPanelAddUser(wd)
        cp_adduser.fill_form(username = "scarfaceadmin",
                            password = "scarfaceadmin",
                            verify_password = "scarfaceadmin",
                            name = "scarfaceadmin",
                            email = "scarfaceadmin@yapo.cl",
                            mobile  = "123456"
                        )
        cp_adduser.allprivilege.click()
        cp_adduser.submit.click()
        cp.logout()

    def _add_email_whitelist(self, wd, email):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Lists")
        cp_whitelist = ControlPanelWhitelist(wd)
        cp_whitelist.go_link_text("whitelist")

        cp_whitelist.fill_form(email = email,
                            message  = "comentarios"
                        )
        cp_whitelist.add_to_list.click()
        cp.logout()
        self._verify_email_whitelist(wd, email)

    def _verify_email_whitelist(self, wd, email, expected=True):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Lists")
        cp_whitelist = ControlPanelWhitelist(wd)
        cp_whitelist.go_link_text("whitelist")

        cp_whitelist.search.click()
        if expected:
            self.assertIn(email, wd.find_element_by_css_selector(".ListingTable").text)
        else:
            self.assertNotIn(email, wd.find_element_by_css_selector(".ListingTable").text)

        cp.logout()
