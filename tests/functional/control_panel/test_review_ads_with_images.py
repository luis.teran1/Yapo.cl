from yapo.pages.control_panel import ControlPanel
from yapo.decorators import tags
from yapo import utils, SeleniumTest


class ControlpanelAdsWithImage(SeleniumTest):
    """ Test ad's images when review it """

    snaps = ['accounts', 'diff_accounts_images']
    username = 'dany'
    password = 'dany'
    ad_id = 126
    ad_subject = 'Foto de rick astley'

    @tags('controlpanel')
    def test_ads_images_quantity(self, wd):
        """ CP: check ads' images """

        control_panel = ControlPanel(wd)
        ad_data = self._get_ad_data()
        self._go_to_ad(control_panel)

        self.assertEqual(len(control_panel.ad_thumbnails), len(ad_data))

    @tags('controlpanel')
    def test_ads_visible_image(self, wd):
        """ CP: check first ads' image """

        control_panel = ControlPanel(wd)
        ad_data = self._get_ad_data()
        self._go_to_ad(control_panel)

        image_src = control_panel.ad_image.get_attribute('src')
        image_src = image_src[(image_src.rindex('/')+1):]

        self.assertVisible(control_panel, 'ad_image')
        self.assertEqual(image_src, ad_data[0][1])

    def _go_to_ad(self, control_panel):
        """ Login and go to the ad """

        control_panel.go()
        control_panel.login(self.username, self.password)

        control_panel.search_ad_by_ad_id(self.ad_id)
        control_panel._driver.find_element_by_partial_link_text(
            self.ad_subject).click()
        control_panel._driver.switch_to_window(
            control_panel._driver.window_handles[1])
        control_panel.wait.until_visible('ad_image')

    def _get_ad_data(self):
        """ Get AD's data from database """

        query = "SELECT * FROM ad_images_digests WHERE ad_id=%s" % (self.ad_id)
        return utils.db_fetch_all(query)
