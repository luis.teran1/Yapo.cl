# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelPromotionalPageBanners
from yapo.pages.desktop import DesktopHome
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.mobile import MobileHome
from yapo.pages.mobile_list import AdListMobile
from yapo.pages2.common import PromoPage
from yapo import trans, conf, utils
import yapo, datetime
import random, string
import os


class PromotionalBanners(yapo.SeleniumTest):

    snaps = ['accounts']
    data = {
        'pp_id': 'Arma tu primer hogar',
        'platform': 'desktop',
        'position': 'home',
        'categories': {1220},
        'regions': {1,2,3,4,15},
        'start_date': '2016-01-01',
        'send_button': True,
    }

    def setUp(self):
        utils.flush_redis_banners()

    def _go(self, wd, user='dany', passwd='dany'):
        cp = ControlPanel(wd)
        cp.go()
        if cp.check_login():
            cp.login(user, passwd)
        cp.go_to_option_in_menu("Administrar banners")
        cp.wait.until_loaded()
        return ControlPanelPromotionalPageBanners(wd)

    def _insert_banner(self, wd, message, update, expect_error=False):

        data = self.data.copy()
        data.update(update)

        if 'position' in data and data['position'] != 'listing':
            if 'categories' in data:
                del data['categories']
            if 'regions' in data:
                del data['regions']

        if 'image' not in data and 'platform' in data and 'position' in data:
            if data['platform'] == 'desktop':
                data['image'] = '{platform}_{position}_banner.jpg'.format(**data)
            else:
                data['image'] = 'mobile_banner.jpg'

        cp = ControlPanelPromotionalPageBanners(wd)
        cp.fill_form(**data)

        if message:
            self.assertEqual(message, cp.message.text)

        banner = cp.get_banner(**{'position':data['position']})
        draft_url = banner.url.text if banner != None else ''
        utils.rebuild_josesearch()
        if banner != None and expect_error:
            wd.refresh()
            curr_url = cp.get_banner(**{'position':data['position']}).url.text
            self.assertEqual(draft_url, curr_url + ' [no publicado]')

    def _edit_banner(self, wd, index, message, update, expect_error=False):
        cp = ControlPanelPromotionalPageBanners(wd)
        if len(cp.edit) >= index:
            cp.edit[index].click()
            cp.wait.until_present('title')
            self.assertEquals(cp.title.text, cp.EDIT_TITLE)
            self._insert_banner(wd, message, update, expect_error)

class PromotionalBannersInsert(PromotionalBanners):

    @tags('controlpanel', 'promotional_pages')
    def test_create_simple_banner_home_desktop(self, wd):
        """Create simple banner home listing"""
        update = {
            'start_date': '2016-01-01'
        }
        end_date = {'end_date': '2016-02-02'}
        message='Accion realizada correctamente'
        cp = ControlPanelPromotionalPageBanners(wd)
        self._go(wd)
        cp.fill_form(**end_date)
        self._insert_banner(wd, message, update)

    @tags('controlpanel', 'promotional_pages')
    def test_create_duplicated_banner(self, wd):
        """Try to create a duplicated banner, get error"""
        update = {
            'position': 'home',
            'start_date': '2016-01-01'
        }
        end_date = {'end_date': '2016-02-02'}
        message = [
            'Accion realizada correctamente',
            'El tiempo activo del banner coincide con otro banner ya publicado en la misma posicion'
        ]
        cp = ControlPanelPromotionalPageBanners(wd)
        self._go(wd)
        for msg in message:
            cp.fill_form(**end_date)
            self._insert_banner(wd, msg, update)

    @tags('controlpanel', 'promotional_pages')
    def test_create_duplicated_banner_listing(self, wd):
        """Try to create a duplicated banner on listing, get duplicated category error"""
        update = {
            'position': 'listing',
            'start_date': '2016-01-01'
        }
        end_date = {'end_date': '2016-02-02'}
        message = [
            'Accion realizada correctamente',
            'Una de las categor�as seleccionadas coincide con la de otro banner ya publicado en la misma posici�n'
        ]
        cp = ControlPanelPromotionalPageBanners(wd)
        self._go(wd)
        for msg in message:
            cp.fill_form(**end_date)
            self._insert_banner(wd, msg, update)

    @tags('controlpanel', 'promotional_pages')
    def test_create_several_listing_banners(self, wd):
        """Creates several listing banners"""
        message = {
            'success': 'Accion realizada correctamente',
            'fail': 'Una de las categor�as seleccionadas coincide con la de otro banner ya publicado en la misma posici�n'
        }
        end_date_list = ['2016-11-03', '2016-11-04', '2016-11-12', '2016-12-04']
        updates = [
            { 'position': 'listing', 'start_date': '2016-11-01',
                'msg': message['success'], 'pp_id': 'Consolas', 'categories': {3020}},
            { 'position': 'listing', 'start_date': '2016-11-01',
                'msg': message['success'], 'pp_id': 'animales', 'categories': {6140, 6180}},
            { 'position': 'listing', 'start_date': '2016-11-02',
                'msg': message['success']},
            { 'position': 'listing', 'start_date': '2016-11-03',
                'msg': message['fail']}
        ]

        self._go(wd)
        counter = 0
        cp = ControlPanelPromotionalPageBanners(wd)
        for update in updates:
            cp.fill_form(**{'end_date': end_date_list[counter]})
            self._insert_banner(wd, update['msg'], update)
            counter += 1

    @tags('controlpanel', 'promotional_pages')
    def test_banner_insert_delete_many(self, wd):
        """ Add tons of banners, then delete some """
        start_date = {'start_date': '2016-01-01'}
        end_date = {'end_date': '2016-02-02'}
        combinations = [
            (1, 'desktop', 'home'),
            (2, 'desktop', 'listing'),
            (3, 'msite', 'home'),
            (4, 'msite', 'listing'),
        ]
        self._go(wd)
        cp = ControlPanelPromotionalPageBanners(wd)
        for n, platform, position in combinations:
            pos = {'platform': platform, 'position': position}
            update = start_date.copy()
            update.update(pos)
            cp.fill_form(**end_date)
            self._insert_banner(wd, None, update)
            self.assertEqual(len(cp.list), n)
            self.assertTrue(any(map(lambda row: row.dump({'platform', 'position'}) == pos, cp.list)))

        deleted = [
            ({'platform': 'desktop', 'position': 'home'}, True, 4),
            ({'platform': 'desktop', 'position': 'home'}, False, 3),
        ]

        for pos, cancel, remaining in deleted:
            banner = cp.get_banner(**pos)
            banner.delete(cancel=cancel)
            self.assertEqual(len(cp.list), remaining)
            if not cancel:
                self.assertEqual(cp.get_banner(**pos), None)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_banner_insert_no_categories(self, wd):
        """ Inserts a banner with no categories """
        self._go(wd)
        self._insert_banner(wd, 'Debes seleccionar al menos una categor�a para el banner', {'position': 'listing', 'categories':''}, True)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_banner_insert_no_categories(self, wd):
        """ Inserts a banner with no regions """
        self._go(wd)
        self._insert_banner(wd, 'Debes seleccionar al menos una regi�n para el banner', {'position': 'listing', 'regions':''}, True)


class PromotionalBannersCheckDesktop(PromotionalBanners):
    platform = 'desktop'
    home = DesktopHome
    listing = AdListDesktop
    home_image = 'desktop_home_banner.jpg'
    listing_image = 'desktop_listing_banner.jpg'

    @tags('controlpanel', 'promotional_pages')
    def test_banner_home(self, wd):
        """ Create banner in home and check it """
        self._go(wd)
        self._insert_banner(wd, 'Accion realizada correctamente', {'platform': self.platform})
        home = self.home(wd)
        home.go()
        home.wait.until_present('pp_banner')
        self.assertVisible(home, 'pp_banner')

class PromotionalBannersCheckMobile(PromotionalBannersCheckDesktop):
    platform = 'msite'
    home = MobileHome
    listing = AdListMobile
    home_image = 'mobile_banner.jpg'
    listing_image = 'mobile_banner.jpg'


class PromotionalBannersEdit(PromotionalBanners):

    def _check_banner(self, wd, Page, url='arma-tu-primer-hogar'):
        page = Page(wd)
        if Page == AdListDesktop:
            page.go('region_metropolitana/comprar')
        else:
            page.go()
        page.wait.until_present('pp_banner')
        page.pp_banner.click()
        self.assertIn(url, wd.current_url)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_banner_change_platform(self, wd):
        """ Edits a banner changing platform """
        self._go(wd)
        self._insert_banner(wd, 'Accion realizada correctamente', {'position': 'home'})
        self._go(wd)
        self._edit_banner(wd, 0, 'Accion realizada correctamente', {'platform':'msite', 'position':'home'})

    @tags('controlpanel', 'promotional_pages')
    def test_edit_banner_change_page(self, wd):
        """ Edits a banner changing the advertised promo page"""
        self._go(wd)
        self._insert_banner(wd, 'Accion realizada correctamente', {})
        self._check_banner(wd, DesktopHome)

        self._go(wd)
        self._edit_banner(wd, 0, 'Accion realizada correctamente', {'pp_id': 'Arma tu primer hogar'})
        self._check_banner(wd, DesktopHome)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_banner_without_mods(self, wd):
        """ Edits a banner without mods """
        self._go(wd)
        self._insert_banner(wd, 'Accion realizada correctamente', {})
        self._check_banner(wd, DesktopHome)

        self._go(wd)
        self._edit_banner(wd, 0, 'Accion realizada correctamente', {})
        self._check_banner(wd, DesktopHome)

