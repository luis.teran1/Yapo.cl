# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelPromotionalPages, ControlPanelPromotionalPagesPreview
from yapo import utils, conf, trans
import yapo, datetime
import random, string
import unittest


class PromotionalPages(yapo.SeleniumTest):

    pp_title = 'Arma tu primer hogar'
    pp_friendly_title = 'arma-tu-primer-hogar'
    start_date = '2016-01-11'
    end_date = ''
    added_words = ['Libro', 'departamento']
    blocked_words = ['Test']

    def setUp(self):
        utils.redis_load_wordsranking()

    def _go(self, wd, user='dany', passwd='dany'):
        cp = ControlPanel(wd)
        cp.go()
        if cp.check_login():
            cp.login(user, passwd)
        cp.go_to_option_in_menu("Administrar p�ginas promocionales")
        cp.wait.until_loaded()
        return ControlPanelPromotionalPages(wd)

    def _get_words(self, page, elements):
        words = []
        for element in elements:
            words.append(element.get_attribute('value'))
        return words;


    def _check_form(self, wd, values):
        # check that form is still filled
        cp = ControlPanelPromotionalPages(wd)
        pp_assert_title = self.pp_title if 'title' not in values else values['title']
        pp_assert_friendly_url = self.pp_friendly_title if 'friendly_title' not in values else values['friendly_title']
        pp_assert_added_words = values['added_words'] if 'added_words' in values else self.added_words
        pp_assert_blocked_words = values['blocked_words'] if 'blocked_words' in values else self.blocked_words
        pp_assert_start_date = self.start_date if 'start_date' not in values else values['start_date']
        pp_assert_end_date = self.end_date if 'end_date' not in values else values['end_date']

        self.assertEqual(cp.val('title'), pp_assert_title)
        self.assertEqual(cp.friendly_url.text, conf.DESKTOP_URL + '/promocional/' + pp_assert_friendly_url)
        self.assertEquals(pp_assert_added_words, self._get_words(cp, cp.pp_added_words))
        self.assertEquals(pp_assert_blocked_words, self._get_words(cp, cp.pp_blocked_words))
        self.assertEqual(cp.val('start_date'), pp_assert_start_date)
        self.assertEqual(cp.val('end_date'), pp_assert_end_date)

    def _check_preview(self, wd, publish=False):
        # review preview page
        preview = ControlPanelPromotionalPagesPreview(wd)
        preview.wait.until_present("publish_btn")
        self.assertEqual(preview.preview_title.text, preview.PP_PREVIEW_TITLE)
        self.assertEqual(preview.preview_subtitle.text, preview.PP_PREVIEW_SUBTITLE)
        self.assertEqual(preview.publish_btn.text, preview.PP_PUBLISH_BTN)
        self.assertEqual(preview.return_btn.text, preview.PP_RETURN_BTN)
        if publish:
            preview.publish_btn.click()
        else:
            preview.return_btn.click()


class PromotionalPagesInsert(PromotionalPages):

    snaps = ['accounts']

    def _insert(self, wd, values):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp = ControlPanelPromotionalPages(wd)
        cp.go()
        cp.fill_form(**values)
        cp.wait.until_visible('submit')
        cp.submit.click()
        return cp

    @tags('controlpanel', 'promotional_pages')
    def test_create_ppage_from_cp_and_publish(self, wd):
        """Create a basic ppage from admin ppages from CP"""
        pp = self._insert(wd, {
            'title' : 'Mi pp',
            'friendly_title' : 'mi-pp',
            'start_date' : '2016-01-01',
            'end_date' : '2016-01-01',
            'image' : 'image_order/bg_pp.png',
            'added_words': [],
            'blocked_words': [],
            'category': 'Comprar'})
        self._check_preview(wd, publish=True)
        self.assertEquals(pp.pp_list_message.text, pp.PP_MSG_SUCCESS)

    @tags('controlpanel', 'promotional_pages')
    def test_insert_error_end_date(self, wd):
        """Create a ppage and gets error (end date < start date) """
        added_words_mod = ['add3']
        blocked_words_mod = []
        values = {
            'title': 'Mi pp end date earlier',
            'friendly_title': 'mi-pp-end-date-earlier',
            'category': 'Comprar',
            'start_date': '2015-01-01',
            'end_date': '2000-01-01',
            'image' : 'image_order/bg_pp.png',
            'added_words': added_words_mod,
            'blocked_words': blocked_words_mod
        }
        pp = self._insert(wd, values)
        self._check_form(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_END_DATE_EARLIER )

    @tags('controlpanel', 'promotional_pages')
    def test_insert_error_date_invalid(self, wd):
        """Create a ppage and gets invalid date error """
        values = {
            'title' : 'Mi pp invalid date',
            'friendly_title' : 'mi-pp-invalid-date',
            'image' : 'image_order/bg_pp.png',
            'start_date' : '05/02/20111',
            'added_words': [],
            'blocked_words': [],
            'category': 'Comprar'
        }
        pp = self._insert(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_DATE_INVALID )

    @tags('controlpanel', 'promotional_pages')
    def test_insert_error_duplicated_title(self, wd):
        """Create a ppage and gets duplicated title error """
        title = 'Arma tu primer hogar'
        url = 'arma-tu-primer-hogar'
        values = {
            'title' : title,
            'friendly_title' : url,
            'image' : 'image_order/bg_pp.png',
            'start_date': '2015-01-01',
            'category': 'Comprar',
            'added_words': [],
            'blocked_words': []
        }
        pp = self._insert(wd, values)
        self._check_form(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_DUPLICATED_TITLE)

    @tags('controlpanel', 'promotional_pages')
    def test_insert_error_duplicated_url(self, wd):
        """Create a ppage and gets duplicated url error """
        title = 'Arma tu primer hogar!!'
        url = 'arma-tu-primer-hogar'
        values = {
            'title' : title,
            'friendly_title' : url,
            'image' : 'image_order/bg_pp.png',
            'start_date': '2015-01-01',
            'category': 'Comprar',
            'added_words': [],
            'blocked_words': []
        }
        pp = self._insert(wd, values)
        self._check_form(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_DUPLICATED_URL)

    @tags('controlpanel', 'promotional_pages')
    def test_insert_error_title_too_long(self, wd):
        """Create a ppage and gets title too long error """
        title = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(105))
        values = {
            'title' : title,
            'friendly_title' : title.lower(),
            'image' : 'image_order/bg_pp.png',
            'start_date' : '2015-05-05',
            'category': 'Comprar',
            'added_words': [],
            'blocked_words': []
        }
        pp = self._insert(wd, values)
        self._check_form(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_TITLE_TOO_LONG )

    @tags('controlpanel', 'promotional_pages')
    def test_insert_duplicated_category(self, wd):
        """Tries to create a PP with duplicated category, can't insert more than one"""
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp = ControlPanelPromotionalPages(wd)
        cp.go()
        cp.fill('category', 'Comprar')
        cp.fill('category', 'Comprar')
        self.assertEquals(len(cp.category_div), 1)

    @tags('controlpanel', 'promotional_pages')
    def test_insert_without_category(self, wd):
        """Tries to create a PP with no category"""
        title = 'promo-page-no-category'
        values = {
            'title' : title,
            'friendly_title' : title,
            'image' : 'image_order/bg_pp.png',
            'start_date' : '2015-05-05',
            'added_words': [],
            'blocked_words': []
        }
        pp = self._insert(wd, values)
        self._check_form(wd, values)
        self.assertEquals(pp.results.text, pp.PP_MSG_ERROR + ' ' + pp.PP_MSG_NO_CATEGORY )

    @tags('controlpanel', 'promotional_pages')
    def test_insert_empty_query(self, wd):
        """Tries to create a PP, filling added words only with spaces, word is not inserted"""

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp = ControlPanelPromotionalPages(wd)
        cp.go()
        cp.fill('category', 'Comprar')
        cp.fill('added_words', '     ')

        self.assertEquals(len(cp.pp_added_words), 0)

    @tags('controlpanel', 'promotional_pages')
    def test_create_ppage_max_categories(self, wd):
        """Tries to insert 5 categories to a promo page, only 4 are accepted"""
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp = ControlPanelPromotionalPages(wd)
        cp.go()
        cp.fill('category', ['Comprar', 'Muebles', 'Arrendar', 'Calzado', 'Servicios'])
        self.assertEquals(len(cp.category_div), 4)

    @tags('controlpanel', 'promotional_pages')
    def test_create_no_images(self, wd):
        """Create a ppage with only one valid ad due to image bconf"""
        title = 'promo-page-images-only'
        values = {
            'title' : title,
            'friendly_title' : title,
            'image' : 'image_order/bg_pp.png',
            'start_date' : '2015-05-05',
            'category' : 'Comprar',
            'added_words': [],
            'blocked_words': []
        }
        pp = self._insert(wd, values)
        wd.get(conf.DESKTOP_URL + '/promo/draft:' + title)
        data = wd.page_source
        self.assertIn('"total_data": 1', data)


class PromotionalPagesEdit(PromotionalPages):

    snaps = ['accounts']

    def _edit_pp(self, wd, values, publish=True):
        cp = self._go(wd)
        self.assertEqual(cp.pp_list_table_title[0].text, self.pp_title)
        self.assertVisible(cp, 'pp_list_table_edit')
        cp.pp_list_table_edit[0].click()

        # check that form is correctly filled
        self.assertEqual(cp.val('title'), self.pp_title)
        self.assertEquals(self.added_words, self._get_words(cp, cp.pp_added_words))
        self.assertEquals(self.blocked_words, self._get_words(cp, cp.pp_blocked_words))
        self.assertEqual(cp.val('start_date'), self.start_date)
        self.assertEqual(cp.val('end_date'), '')

        # modify page
        title = values['title'] if 'title' in values else self.pp_title
        values['friendly_title'] = title.replace(' ', '-').lower()
        values['image'] = 'image_order/bg_pp.png'
        img_value = cp.image.get_attribute("value")
        cp.fill_form(**values)
        cp.wait.until(lambda wd: img_value != cp.image.get_attribute("value"))
        cp.submit.click()

        # review preview page
        self._check_preview(wd, publish=publish)
        if publish:
            # verify in listing
            self.assertEquals(cp.pp_list_message.text, cp.PP_MSG_SUCCESS)
            pp_assert_title = self.pp_title if 'title' not in values else values['title']
            self.assertEqual(cp.pp_list_table_title[2].text, pp_assert_title + ' [borrador]')

            # verify on insert form
            cp.pp_list_table_edit[2].click()
            self._check_form(wd, values)

    def _discard_pp(self, wd):
        cp = ControlPanelPromotionalPages(wd)
        self.assertEqual(cp.draft_message.text, cp.PP_DRAFT_MESSAGE)
        self.assertEqual(cp.discard_btn.text, cp.PP_DRAFT_DELETE)
        cp.discard_btn.click()
        self.assertEqual(cp.pp_list_title.text, cp.PP_MSG_TITLE)
        self.assertEqual(cp.pp_list_message.text, '')
        self.assertEqual(cp.pp_list_table_title[0].text, self.pp_title)


    @tags('controlpanel', 'promotional_pages')
    def test_edit_pp_title(self, wd):
        """ Edits title from an existant PP """

        pp_title_mod = 'Mi pp 2'
        self._edit_pp(wd, {'title':pp_title_mod})

    @tags('controlpanel', 'promotional_pages')
    def test_edit_pp_start_date(self, wd):
        """ Edits start date from an existant PP """
        start_date_mod = '2011-11-11'
        self._edit_pp(wd, {'start_date':start_date_mod})

    @tags('controlpanel', 'promotional_pages')
    def test_edit_pp_category(self, wd):
        """ Edits category from an existant PP """

        added_words_mod = ['add3']
        blocked_words_mod = []
        category_mod = 'Arrendar'
        self._edit_pp(wd, {
            'category':category_mod,
            'added_words':added_words_mod,
            'blocked_words':blocked_words_mod})

    @tags('controlpanel', 'promotional_pages')
    def test_edit_discard_draft(self, wd):
        """ Discards draft """
        pp_title_mod = 'Mi promo page'
        self._edit_pp(wd, {'title':pp_title_mod}, publish=False)
        self._discard_pp(wd)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_publish_and_rebuild(self, wd):
        """ Checks that promo page is not a draft after josesearch is rebuilt """
        pp_title_mod = 'Mi promo page para publicar'
        self._edit_pp(wd, {'title':pp_title_mod})
        utils.rebuild_josesearch()
        cp = ControlPanelPromotionalPages(wd)
        cp.go('listPages')
        self.assertEqual(cp.pp_list_title.text, cp.PP_MSG_TITLE)
        self.assertEqual(cp.pp_list_message.text, '')
        self.assertEqual(cp.pp_list_table_title[0].text, pp_title_mod)

    @tags('controlpanel', 'promotional_pages')
    def test_edit_discard_draft(self, wd):
        """ Edits a promo page, reviews, goes back to results and check that form is filled """
        values = {
            'title': 'Mi pp end date earlier',
            'title' : 'inactive promo page',
            'friendly_title': 'inactive-promo-page',
            'category': 'Comprar',
            'start_date': '3000-01-01',
            'image' : 'image_order/bg_pp.png',
            'added_words': [],
            'blocked_words': []
        }
        self._edit_pp(wd, values, publish=False)
        self._check_form(wd, values)
        cp = ControlPanelPromotionalPages(wd)
        self.assertEqual(cp.draft_message.text, cp.PP_DRAFT_MESSAGE)
        self.assertEqual(cp.discard_btn.text, cp.PP_DRAFT_DELETE)

class PromotionalPagesList(PromotionalPages):

    snaps = ['accounts']

    def _delete(self, wd, page, title, index):
        page.pp_list_table_delete[0].click()
        alert = wd.switch_to_alert()
        self.assertIn(alert.text, page.PP_MSG_ALERT_BASE +self.pp_title + '?. ')
        alert.accept()

    @tags('controlpanel', 'promotional_pages')
    def test_list_empty(self, wd):
        """ Tests an empty pp list """
        page = self._go(wd)
        self.assertEqual(page.pp_list_title.text, page.PP_MSG_TITLE)
        page.remove_promo_pages(wd)
        self.assertEqual(page.pp_list_message.text, page.PP_MSG_LIST_EMPTY)
        self.assertNotPresent(page, 'pp_list_table_title')
        self.assertNotPresent(page, 'pp_list_table_edit')
        self.assertNotPresent(page, 'pp_list_table_delete')
        self.assertVisible(page, 'pp_new_btn')

    @tags('controlpanel', 'promotional_pages')
    def test_list_with_content(self, wd):
        """ Tests a pp list with active pp """
        page = self._go(wd)
        self.assertEqual(page.pp_list_title.text, page.PP_MSG_TITLE)
        self.assertEqual(page.pp_list_message.text, '')
        self.assertEqual(page.pp_list_table_title[0].text, self.pp_title)
        self.assertVisible(page, 'pp_list_table_delete')
        self.assertVisible(page, 'pp_list_table_edit')
        self.assertVisible(page, 'pp_new_btn')

    @tags('controlpanel', 'promotional_pages')
    def test_list_delete(self, wd):
        """ Tests PP deletion """
        page = self._go(wd)
        initial_active_ppages = len(page.pp_list_table_title)
        self.assertEqual(page.pp_list_title.text, page.PP_MSG_TITLE)
        self.assertEqual(page.pp_list_message.text, '')
        self.assertEqual(page.pp_list_table_title[0].text, self.pp_title)
        self.assertVisible(page, 'pp_list_table_delete')
        self.assertVisible(page, 'pp_list_table_edit')

        self._delete(wd, page, self.pp_title, 0)
        final_active_ppages = len(page.pp_list_table_title)
        self.assertEqual(initial_active_ppages - 1, final_active_ppages)

    @tags('controlpanel', 'promotional_pages')
    def test_list_draft_delete(self, wd):
        """ Tests draft deletion from list """
        title = 'mi promo draft'
        data = {
            'title': title,
            'url': 'mi-promo-draft',
            'image': 'image_order/bg_pp.png',
            'start_date': '2016-01-01',
            'category': '1220',
            'search_query': '0 lim:10000 category:1220 *:* casa',
        }
        trans.check_trans('set_promotional_page', **data)

        page = self._go(wd)
        initial_active_ppages = len(page.pp_list_table_title)
        draft_index = initial_active_ppages - 1

        self.assertEqual(page.pp_list_table_title[draft_index].text, title + ' [borrador]')
        self.assertVisible(page, 'pp_list_table_delete')
        self._delete(wd, page, title, draft_index)
        final_active_ppages = len(page.pp_list_table_title)
        self.assertEqual(initial_active_ppages - 1, final_active_ppages)
