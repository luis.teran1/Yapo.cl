# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.control_panel import ControlPanel, ControPanelServicesModule
from yapo.utils import Products, Label_texts, Label_values, Upselling
import yapo
import time
import yapo.conf


class CpanelApplyMasiveServices(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']
    module = "massiveassignment"

    def _login_cp(self, wd):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        return ControPanelServicesModule(wd)

    @tags('controlpanel', 'products')
    def test_masive_serv_one_ad(self, wd):
        """ Several services for one ad"""
        srv_mdl = self._login_cp(wd)
        srv_mdl.go(self.module)

        srv_mdl.search_ads("cuentaproconpack@yapo.cl")
        ad = '8000093';

        services = ['bump', 'daily_bump',  'weekly_bump', 'gallery',
                    'gallery_1', 'gallery_30', 'label_urgent', 'label_new']

        srv_mdl.apply_ad_serv(services, ad)

        success = srv_mdl.massive_success.text
        print(success)
        expected = "Audi A4 2011 Subir Semanal, Vitrina 30 d�as, Etiqueta"
        self.assertIn(expected, success)

        # Try to apply again with errors and success by gallery
        # (The time was extended)
        utils.flush_redises();
        srv_mdl.search_ads("cuentaproconpack@yapo.cl")
        srv_mdl.apply_ad_serv(services, ad)
        errors = srv_mdl.massive_error.text
        print(errors)

        expectedErrors = "Audi A4 2011 (8000093)\n- Este aviso a�n tiene un Subir Semanal activo.\n- Este aviso ya tiene una Etiqueta activa."

        self.assertIn(expectedErrors, errors)
