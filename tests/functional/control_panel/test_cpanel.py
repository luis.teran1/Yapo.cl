# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess, AdInsertResult
from yapo.pages.desktop_list import AdViewDesktop
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
from yapo import utils
import yapo
import yapo.conf
import datetime
import time
from yapo.utils import trans, execute_query, rebuild_index
from yapo.pages.generic import SentEmail


class ControlpanelStatsBar(yapo.SeleniumTest):

    snaps = ['unreviewed']

    @tags('controlpanel', 'statsbar')
    def test_stats_bar(self, wd):
        """Check revision bar CP change with every Ad reviewed"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.refuse_ad("Arriendo mi preciosa casa en �u�oa")
        cp.refuse_ad("Por viaje hermoso departamento comuna santiago")
        cp.go()
        cp.go_to_queue("Unreviewed")
        cp.wait.until_present('stats_bar_hourly_current')
        cp.wait.until_text_present('2 ads/h', cp.stats_bar_hourly_current)
        self.assertEqual(cp.stats_bar_hourly_current.text, '2 ads/h')
        self.assertEqual(cp.stats_bar_hourly_last.text, '0 ads/h')
        cp.logout()


class ControlpanelTests(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'search', 'licenseplate')
    def test_check_not_present_licenseplate(self, wd):
        """ Test for that not found licenseplate search"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        page_source = cp._driver.page_source.lower()
        self.assertNotIn('licenseplate', page_source)
        self.assertNotIn('id="search_type_reg_nr"', page_source)

    @tags('controlpanel', 'search', 'archive')
    def test_check_ad_in_search_for_ad(self, wd):
        """ Test for that find an ad of the scheme blocket_<current year> in search for ad"""
        self._move_ads_to_current_schema()
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_ad_by_ad_id_inside_search_for_ad("9")
        self.assertTrue(cp._driver.find_element_by_partial_link_text(
            "Damcykel med blahonga").is_displayed())

    def _move_ads_to_current_schema(self):
        """
        Move ad with ad_id equals to 9 in schema blocket_{current_year}
        """
        current_year = datetime.date.today().year
        result = trans("archive_ad_bulk")
        self.assertEquals(result['status'], 'TRANS_OK')
        self.assertEquals(result['total'], '4')
        self.assertEquals("b' ad_id \\n-------\\n(0 rows)\\n\\n'", execute_query(
            "select ad_id from public.ads where ad_id=9".format(year=current_year)))
        self.assertEquals("b' ad_id \\n-------\\n     9\\n(1 row)\\n\\n'", execute_query(
            "select ad_id from blocket_{year}.ads where ad_id=9".format(year=current_year)))

    @tags('controlpanel', 'email')
    def test_check_account_redis_after_accept_cp(self, wd):
        """ Testing the account session in redis is populated after his ad has been accepted """
        data = {
            'subject': 'Test accept ad and populate redis',
            'body': 'lorem ipsum yolo sit amet',
            'region': 15,
            'communes': 331,
            'category': 3040,
            'price': 9990,
            'private_ad': True,
            'name': 'yolo',
            'email': 'android@yapo.cl',
            'email_verification': 'android@yapo.cl',
            'phone': '9999666666',
            'password': '123123123',
            'password_verification': '123123123',
            'create_account': True,
            'accept_conditions': True,
            'submit': True
            }

        user_profile = [
            'account_id',
            '8',
            'name',
            'yolo',
            'email',
            'android@yapo.cl',
            'phone',
            '999996666',
            'is_company',
            'f',
            'region',
            '15',
            'account_status',
            'pending_confirmation',
            'commune',
            '331',
            'phone_hidden',
            'f',
            'user_id',
            '50',
            ]
        insert_an_ad(wd, AdInsert=DesktopAI, **data)

        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, '�Gracias por publicar!')

        # going to control panel and accept the ad
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.accept_ad(subject='Test accept ad and populate redis')
        rebuild_index()

        # validate account session in redis is populated
        raw_profile = utils.redis_command(yapo.conf.REGRESS_REDISSESSION_PORT, 'hgetall', 'rsd1xd525f2227c5b0d7e199ca21f74b5c1feb2967455')
        profile = [x.decode() for x in raw_profile]
        # check if all of the user_profile is contained in the fetched profile
        for value in user_profile:
            self.assertIn(value, profile)

    @tags('controlpanel', 'ad_edit', 'hide_ad', 'email')
    def test_check_edit_ad_hidden_by_cp(self, wd):
        """ Testing edition gives a 404 when using the "published e-mail" edit link, for an ad that is hidden by control panel """
        data = {
            'subject': 'Test hide ad, and then edit',
            'body': 'lorem ipsum yolo sit amet',
            'region': 15,
            'communes': 331,
            'category': 3040,
            'price': 9990,
            'private_ad': True,
            'name': 'yolo',
            'email': 'android@yapo.cl',
            'email_verification': 'android@yapo.cl',
            'phone': '9999666666',
            'password': '123123123',
            'password_verification': '123123123',
            'create_account': True,
            'accept_conditions': True,
            'submit': True
        }

        insert_an_ad(wd, AdInsert=DesktopAI, **data)

        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, '�Gracias por publicar!')

        # going to control panel and accept the ad, then hide the ad
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.accept_ad(subject='Test hide ad, and then edit')
        rebuild_index()
        cp.search_and_hide_ad(subject='Test hide ad, and then edit')

        # reading the e-mail
        sent_email = SentEmail(wd)
        sent_email.go()
        wd.find_element_by_css_selector('a[href*="cmd=edit"]').click()

        ad_view = AdViewDesktop(wd)
        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

    def test_default_search_options(self, wd):
        """Check the default options "No archives" and "Last 24h"
           are selected when goes to "Search for ad" menu"""

        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.go_to_option_in_menu("Search for ad")
        self.assertTrue(cp.radio_button_last_24h.is_selected())
        for option in cp.archive_options.find_elements_by_css_selector('option'):
            if option.text == 'No archives':
                self.assertTrue(option.is_selected())


class ControlPanelQueues(yapo.SeleniumTest):

    snaps = ['accounts']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'queues')
    def test_queue_time_message(self, wd):
        """Check if the time message in queues is working correcly"""

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        for button in cp.queues_button:
            button.click()
            break
        for queue_list in cp.queue_lists:
            self.assertEquals(queue_list.text, "21 Unreviewed [21 avisos con m�s de 30 minutos] [21 avisos con m�s de 2 horas]")
            break
        cp.logout()

        # This test is shitty as hell, as it is completely timing dependent. Expect for it to fail.
        utils.bconf_overwrite('*.controlpanel.modules.adqueue.unreviewed_time.0.sql', '6 SECONDS')
        utils.bconf_overwrite('*.controlpanel.modules.adqueue.unreviewed_time.1.sql', '10 SECONDS')
        data = {
            'subject': 'Aviso pal tiempito',
            'body': 'Pal tiempito tiempito',
            'region': 11,
            'communes': 233,
            'category': 6140,
            'private_ad': True,
            'name': 'Anito',
            'email': 'android@yapo.cl',
            'email_verification': 'android@yapo.cl',
            'phone': '1112223444',
            'password': '123123123',
            'password_verification': '123123123',
            'create_account': False,
            'accept_conditions': True,
            'submit': True
        }
        result = insert_an_ad(wd, AdInsert=DesktopAI, **data)
        self.assertTrue(result.was('success'))

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        for button in cp.queues_button:
            button.click()
            break
        for queue_list in cp.queue_lists:
            self.assertEquals(queue_list.text, "22 Unreviewed [22 avisos con m�s de 30 minutos] [21 avisos con m�s de 2 horas]")
            break
        time.sleep(3)
        wd.refresh()
        for button in cp.queues_button:
            button.click()
            break
        for queue_list in cp.queue_lists:
            self.assertEquals(queue_list.text, "22 Unreviewed [22 avisos con m�s de 30 minutos] [22 avisos con m�s de 2 horas]")
            break
