# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel
from yapo.decorators import tags
from yapo import utils, SeleniumTest


class ControlpanelAccountsPrivileges(SeleniumTest):

    snaps = ['accounts']
    @tags('controlpanel', 'accounts', 'privileges')
    def test_cp_bump_privilege(self, wd):
        """ Control Panel Bump privilege is revocable """

        cp = ControlPanel(wd)
        cp.go_and_login('blocket', 'blocket')
        cp.search_for_ad_by_list_id('8000078')
        cp.wait.until_present('products_to_apply')
        cp.logout()
        cp.login('dany', 'dany')
        cp.go_to_option_in_menu("Show users")
        cp.edit_user.click()
        cp.bump_privilege.click()
        cp.button_save.click()
        cp.logout()
        cp.login('blocket', 'blocket')
        cp.search_for_ad_by_list_id('8000078')
        cp.wait.until_not_present('products_to_apply')
