# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import FileConverter
import yapo
import yapo.conf
import datetime
from yapo.pages.generic import SentEmail
import os


class FileConverterTest(yapo.SeleniumTest):

    snaps = ['accounts']

    bconfs = {
        '*.*.bifrost.partner.convert.path': '/',
        '*.*.bifrost.host': 'http://127.0.0.1',
        '*.*.bifrost.port': '8000'
    }
    
    chk_tab = {
        'tabConv': True
    }

    def _get_expected_res(self, separator):
        return  '{}\n{}\n{}\n{}\n{}\n{}\n{}\n'.format(
            'Content-Disposition: form-data; name="UploadFile"; filename="filetoupload.txt"',
            'Content-Type: application/octet-stream',
            'HELLO!',
            'Content-Disposition: form-data; name="Separator"',
            separator,
            'Content-Disposition: form-data; name="Encoding"',
            'ISO-8859-1')

    @tags('controlpanel', 'FileConverter')
    def test_call(self, wd):
        """ 
        Creates a mock server to see that the module is sending the right 
        values in its post.
        """
        fc = FileConverter(wd)
        fc.start_server()
        fc.go(wd, 'dany')
        fc.convert(";", "filetoupload.txt") # this is inside "resources" folder
        result = fc.get_result()
        fc.exit(wd)
        expected_res = self._get_expected_res(';');
        self.assertEquals(result, expected_res)
        
    @tags('controlpanel', 'FileConverter')
    def test_call_with_tab(self, wd):
        """ 
        Creates a mock server to see that the module is sending the right 
        values in its post.
        """
        fc = FileConverter(wd)
        fc.start_server()
        fc.go(wd, 'dany')
        fc.fill_form(**self.chk_tab)
        fc.convert("", "filetoupload.txt", tab=True) # this is inside "resources" folder
        result = fc.get_result()
        fc.exit(wd)
        expected_res = self._get_expected_res('\\t');
        self.assertEquals(result, expected_res)
