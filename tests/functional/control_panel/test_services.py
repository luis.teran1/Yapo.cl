# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.control_panel import ControlPanel, ControPanelServicesModule
from yapo.utils import Products, Label_texts, Label_values, Upselling
import yapo
import time
import yapo.conf


class CpanelApplyServices(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
    }

    def _login_cp(self, wd):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")

    @tags('controlpanel', 'products')
    def test_check_error_first_form(self, wd):
        """ Errors on first form of Services module """
        self._login_cp(wd)
        error_messages = ['Ingresa un AD ID v�lido',
                          'Ingresa un AD ID v�lido',  'Aviso no encontrado']
        fake_ads = ['', '1XXX', '123123123']
        services = ['bump', 'daily',  'weekly', 'gallery',
                    'gallery_1', 'gallery_30', 'rmv_label']

        for i in range(len(fake_ads)):
            for service in services:
                srv_mdl = ControPanelServicesModule(wd)
                srv_mdl.go()
                srv_mdl.fill_form(ad_id=fake_ads[i], service=service)
                srv_mdl.apply_action.click()
                self.assertEquals(
                    srv_mdl.message_error.text, error_messages[i])

    @tags('controlpanel', 'products')
    def test_bump_service(self, wd):
        """ Basic bump """
        self._login_cp(wd)
        srv_mdl = ControPanelServicesModule(wd)
        srv_mdl.go()
        srv_mdl.fill_form(ad_id=100, service='bump')
        srv_mdl.apply_action.click()
        self.assertEquals(
            srv_mdl.message_success.text, 'Tu aviso fue promovido')
        self.assertEquals(
            srv_mdl.success_detail.text, 'Producto aplicado: Subir Ahora')

    @tags('controlpanel', 'products')
    def test_autobump_errors(self, wd):
        """ Errors on Autobump parameters selection """
        self._login_cp(wd)
        srv_mdl = ControPanelServicesModule(wd)
        srv_mdl.go()
        srv_mdl.fill_form(ad_id=100, service='autobump')
        srv_mdl.apply_action.click()

        self.assertEquals(
            srv_mdl.message_success.text, 'Servicio: Subir Personalizado')
        srv_mdl.apply_action.click()
        self.assertEquals(
            srv_mdl.message_error.text, 'Selecciona la frecuencia')
        srv_mdl.fill_form(frequency=2, num_days='xx')
        srv_mdl.apply_action.click()
        self.assertEquals(
            srv_mdl.message_error.text, 'Ingresa el n�mero de d�as (1-93)')

        srv_mdl.apply_directly('100', 'autobump')
        self.assertEquals(
            srv_mdl.message_success.text, 'Servicio: Subir Personalizado')
        srv_mdl.calculate_action.click()
        self.assertEquals(
            srv_mdl.message_error.text, 'Selecciona la frecuencia')
        srv_mdl.fill_form(frequency=2, num_days='xx')
        srv_mdl.calculate_action.click()
        self.assertEquals(
            srv_mdl.message_error.text, 'Ingresa el n�mero de d�as (1-93)')

    def _autobump_calculate_and_apply(self,
                                      wd,
                                      expected_info,
                                      success_message,
                                      use_night):
        self._login_cp(wd)
        srv_mdl = ControPanelServicesModule(wd)
        srv_mdl.go()
        srv_mdl.fill_form(ad_id=100, service='autobump')
        srv_mdl.apply_action.click()
        self.assertEquals(
            srv_mdl.message_success.text, 'Servicio: Subir Personalizado')
        srv_mdl.fill_form(frequency=2, num_days='2', use_night=use_night)
        srv_mdl.calculate_action.click()
        calculated_info = srv_mdl.get_table_data("calculate_info")

        for info in expected_info:
            self.assertIn(info, calculated_info)

        srv_mdl.apply_action.click()
        self.assertEquals(
            srv_mdl.message_success.text, 'Tu aviso fue promovido')
        self.assertEquals(
            srv_mdl.success_detail.text,
            success_message)

    @tags('controlpanel', 'products')
    def test_autobump_without_night(self, wd):
        """ Autobump calculate info and apply service without use night """
        expected_info = [['Categor�a del aviso', 'Otros - Otros productos'],
                         ['Total de bumps a ejecutar', '18'],
                         ['Valor unitario', '950'],
                         ['Valor total', '17.100']
                         ]
        success_message = ("Producto aplicado: Subir Personalizado "
                           "Cada 2 horas por 2 d�as "
                           "(no incluye horario nocturno)"
                           )
        self._autobump_calculate_and_apply(
            wd, expected_info, success_message, False)

    @tags('controlpanel', 'products')
    def test_autobump_with_night(self, wd):
        """ Autobump calculate info and apply service with use night """
        expected_info = [['Categor�a del aviso', 'Otros - Otros productos'],
                         ['Total de bumps a ejecutar', '24'],
                         ['Valor unitario', '950'],
                         ['Valor total', '22.800']
                         ]
        success_message = ("Producto aplicado: Subir Personalizado "
                           "Cada 2 horas por 2 d�as (incluye horario nocturno)"
                           )
        self._autobump_calculate_and_apply(
            wd, expected_info, success_message, True)
