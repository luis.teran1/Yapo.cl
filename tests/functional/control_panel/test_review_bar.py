# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel
from yapo.decorators import tags
from yapo import utils, SeleniumTest, conf
import time


class ReviewBar(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel')
    def test_review_bar(self, wd):
        """ with redis """
        utils.start_redis_session()
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        pages = 2
        cp.walk_reviewing_queue(queue='Unreviewed', pages=pages)
        self.assertEqual(
            cp.stats_bar_hourly_current.text, '4 ads/h')
        utils.flush_redises()

    @tags('controlpanel')
    def test_review_bar_redis_down(self, wd):
        """ without redis """
        utils.stop_redis_session()
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        pages = 1
        cp.walk_reviewing_queue(queue='Unreviewed', pages=pages)
        self.assertEqual(
            cp.stats_bar_hourly_current.text, '0 ads/h')
        utils.start_redis_session()
        utils.flush_redises()


class BarPercentages(SeleniumTest):

    snaps = ['accounts']

    def test_review_stats_bar(self, wd):
        """ Check stats bar changes on several actions """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_queue('Unreviewed')

        ads_review = [
            ['Libro 1', 'refuse'],
            ['Aviso de Alan', 'refuse'],
            ['Aviso de prueba 6', 'edit_and_accept'],
            ['Aviso de prueba 7', 'edit_and_accept'],
            ['Aviso de prueba 8', 'edit_and_accept'],
            ['Aviso de prueba 10', 'accept']
        ]
        stats_info = {
            'refuse': ['100.00 %', '0.00 %', '0.00 %'],
            'edit_and_accept': ['40.00 %', '0.00 %', '60.00 %'],
            'accept': ['33.33 %', '16.67 %', '50.00 %'],
        }

        old_action = 'refuse'
        for ad in ads_review:
            action = ad[1]
            subj = ad[0]
            if action != old_action:
                cp.next_in_queue()
                self.assertEquals(stats_info[old_action], cp.report_stats())
                old_action = action
            print("Reviewing {} action {}".format(subj, action))
            cp.review_by_queue(None, subj, action, keep_page=True)

        cp.next_in_queue()
        self.assertEquals(stats_info[action], cp.report_stats())
