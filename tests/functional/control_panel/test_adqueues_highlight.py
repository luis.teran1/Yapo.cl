# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess
from yapo.pages.desktop_list import AdViewDesktop
import yapo
import yapo.conf
import datetime
from yapo import utils
from yapo.utils import trans, execute_query, rebuild_index
from yapo.pages.generic import SentEmail
from yapo.steps import trans, insert_an_ad


class TestAdqueuesHighlight(yapo.SeleniumTest):

    snaps = ['cp_upselling']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'upselling')
    def test_upselling_header_new_ads(self, wd):
        """ CP: Check upselling header for new ads"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_for_ad(email='dupselling@yapo.cl', subject='Aviso upselling daily')

        self.assertTrue(cp.on_review_header_upselling.is_displayed())
        cp.close_window_and_back()

        cp.search_for_ad(email='wupselling@yapo.cl', subject='Aviso upselling weekly')
        self.assertTrue(cp.on_review_header_upselling.is_displayed())
        cp.close_window_and_back()

        cp.search_for_ad(email='gupselling@yapo.cl', subject='Aviso upselling gallery')
        self.assertTrue(cp.on_review_header_upselling.is_displayed())
        cp.close_window_and_back()

    @tags('controlpanel', 'upselling')
    def test_upselling_header_edit_ads(self, wd):
        """ CP: Check upselling header for edit ads"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_for_ad(email='editupselling@yapo.cl', subject='Aviso upselling on edit', queue='Unreviewed' )
        self.assertTrue(cp.on_review_header_upselling.is_displayed())
        cp.close_window_and_back()

    @tags('controlpanel', 'upselling')
    def test_upselling_header_renew_ads(self, wd):
        """ CP: Check upselling header for renew ads"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_for_ad(email='renewupselling@yapo.cl', subject='Aviso upselling on renew', queue='Unreviewed' )
        self.assertTrue(cp.on_review_header_upselling.is_displayed())
        cp.close_window_and_back()

    @tags('controlpanel', 'upselling')
    def test_not_upselling_header_new_ads(self, wd):
        """ CP: Check upselling header does not exist for ads without upselling"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_for_ad(email='noupselling@yapo.cl', subject='Aviso sin upselling', queue='Unreviewed' )
        cp.wait.until_not_present('on_review_header_upselling')
        cp.close_window_and_back()

    @tags('controlpanel', 'inserting_fee')
    def test_inserting_fee_header(self, wd):
        email = 'prepaid5@blocket.se'
        passwd = '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'
        paid = {'email': email, 'passwd': passwd, 'subject': 'Inserting Fee Ad', 'category': '7020', 'jc': ['17'], 'pay_type': 'card', 'auto_clear': True}
        not_paid = {'email': email, 'passwd': passwd, 'subject': 'Regular Ad', 'category': '7020', 'jc': ['17'], 'pay_type': 'free'}

        def check_adqueues_header(cp, subject, header_text, inserting_fee_visible):
            cp.go()
            cp.go_to_option_in_menu("Search for ad")
            cp.search_for_ad(email=email, subject=not_paid['subject'], queue='Unreviewed')
            self.assertEqual(cp.on_review_header.text, 'Queue-id: 26-1 \nNew')
            self.assertNotVisible(cp, 'on_review_header_inserting_fee')
            cp.close_window_and_back()

        insert_an_ad(wd, **not_paid)
        trans.make_user_pro(1, 'jobs')
        insert_an_ad(wd, **paid)

        cp = ControlPanel(wd).go()
        cp.login("dany", "dany")

        check_adqueues_header(cp, paid['subject'], 'Queue-id: 27-1 \nNew - Aviso Pagado', True)
        check_adqueues_header(cp, not_paid['subject'], 'Queue-id: 26-1 \nNew', False)


class TestAdqueuesHighlightInsertingFee(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_assigned_if']

    @tags('controlpanel', 'inserting_fee', 'pack2if')
    def test_pack2if_header(self, wd):
        list_id = 8000094
        #open ad review on cp
        cp = ControlPanel(wd).go()
        cp.login("dany", "dany")
        cp.search_for_ads(list_id=list_id)
        cp.open_ad_on_search()
        #check presence of highlighted header
        self.assertEqual(cp.on_review_header_inserting_fee.text, 'Aviso de veh�culo')
