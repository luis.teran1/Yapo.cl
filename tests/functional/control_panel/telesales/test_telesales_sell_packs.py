# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelTelesales 
from yapo.pages.desktop import DashboardMyAds
from yapo.pages.ad_insert import AdInsert, PackConfirm
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import utils
from selenium.common import exceptions
from functional.control_panel.telesales.test_telesales_sell_products import TelesalesUtil
from selenium.common.exceptions import NoSuchElementException
import yapo
import time
import yapo.conf


class TelesalesSellAdsProducts(TelesalesUtil):

    snaps = ['accounts', 'db-load-diff_pack_cars1']
    bconfs = {
        '*.controlpanel.modules.adqueue.auto.pack.timeout': '1',
        '*.*.payment.telesales.webpay_plus.enabled': '0',
        }

    @tags('controlpanel', 'accounts', 'telesales', 'payment', 'pack')
    def test_telesales_no_pro(self, wd):
        """Check telesales pro content is not shown for non pros """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search="prosincuenta@yapo.cl")
        cp_telesales.button_search.click()
        
        self.assertRaises(exceptions.NoSuchElementException, lambda: cp_telesales.pack_assign)
        self.assertRaises(exceptions.NoSuchElementException, lambda: cp_telesales.pack_selected)
        

    def _insert_ad(self, wd, subject):

        ad_insert = AdInsert(wd)
        ad_insert.go()
        ad_insert.insert_ad(subject=subject,
                       body="yoLo",
                       region=15,
                       communes=331,
                       category=2020,
                       price=3500000,
                       name="yolo",
                       phone="999966666",
                       accept_conditions=True,
                       brand=31,
                       model=44,
                       version=1,
                       regdate=2010,
                       gearbox=1,
                       fuel=1,
                       cartype=1,
                       mileage=15000)
        ad_insert.submit.click()
        page = PackConfirm(wd)
        self.assertTrue('pack_message_ok')
        time.sleep(3) # waiting for automatic review pack queue


    def _check_dashboard_ad(self, wd, ad_data):

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        self.assertIsNotNone(active_ad)

        #check ad status
        self.assertEqual(active_ad.status.text, ad_data['status'])

        if ad_data['vi_link_enabled']:
            self.assertIsNotNone(active_ad.subject.get_attribute('href'))
        else:
            self.assertIsNone(active_ad.subject.get_attribute('href'))

        if ad_data['bumps_enabled']:
            active_ad.wait.until_visible('bumps_enabled')
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.bumps_enabled)

        if ad_data['gallery_enabled']:
            self.assertIsNotNone(active_ad.gallery_enabled)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.gallery_enabled)

        ##check edit and delete
        if ad_data['options_enabled']:
            self.assertIsNotNone(active_ad.options_buttons)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.options_buttons)

class TelesalesSellPackInmo(TelesalesUtil):

    snaps = ['accounts', 'db-load-diff_pack_inmo1']
    bconfs = {
        '*.controlpanel.modules.adqueue.auto.pack.timeout': '1',
        '*.*.payment.telesales.webpay_plus.enabled': '0',
        }

    @tags('controlpanel', 'accounts', 'telesales', 'payment', 'pack')
    def test_telesales_no_pro(self, wd):
        """Check telesales pro content is not shown for non pros """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search="prosincuenta@yapo.cl")
        cp_telesales.button_search.click()
        
        self.assertRaises(exceptions.NoSuchElementException, lambda: cp_telesales.pack_assign)
        self.assertRaises(exceptions.NoSuchElementException, lambda: cp_telesales.pack_selected)
