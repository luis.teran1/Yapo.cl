# -*- coding: latin-1 -*-
from yapo import pages, conf
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
import requests
import csv
import os


class TelesalesWebSQL(yapo.SeleniumTest):

    snaps = ['accounts', 'difftelesalesbasic']

    filters = dict(timestamp_from='2014-07-15 00:00:00',
                   timestamp_to='2014-07-15 23:59:59')

    @tags('controlpanel', 'telesales', 'websql')
    def test_websql_telesales(self, wd):
        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Telesales Report', **self.filters)
        result_data = websql.get_result_data()

        data = [
                ['2014-07-15 12:48:42.71921', 'Dany', '8000066', '166', 'android@yapo.cl', 'Regi�n Metropolitana', 'Computadores & electr�nica - Celulares - tel�fonos y accesorios', '625810', 'Subir Ahora', '1000', 'bill', '', '7980', 'credit', 'VN', '1', 'confirmed', 'Persona'],
                ['2014-07-15 12:48:42.71921', 'Dany', '8000052', '166', 'android@yapo.cl', 'Regi�n Metropolitana', 'Tiempo libre - Arte - antig�edades y colecciones', '625810', 'Vitrina 7 d�as', '3990', 'bill', '', '7980', 'credit', 'VN', '1', 'confirmed', 'Persona'],
                ['2014-07-15 12:48:42.71921', 'Dany', '8000056', '166', 'android@yapo.cl', 'Regi�n Metropolitana', 'Tiempo libre - Arte - antig�edades y colecciones', '625810', 'Subir Semanal', '2990', 'bill', '', '7980', 'credit', 'VN', '1', 'confirmed', 'Persona']]

        self.assertEquals(result_data, data)

    @tags('controlpanel', 'telesales', 'websql')
    def test_stores_in_telesales(self, wd):
        """Query for a telesales store purchase & download csv"""
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Telesales Report',
                           timestamp_from='2014-10-22 00:00:00',
                           timestamp_to='2014-10-23 23:59:59')
        result_data = websql.get_result_data()

        data = [['2014-10-22 12:05:29.325122', 'Dany', '-', '170', 'prepaid5@blocket.se', '-', '-', '441576', 'Tienda Anual', '48000', 'invoice', '1', '48000', 'credit', 'VN', '1', 'confirmed', 'Profesional'],
                ['2014-10-23 19:15:46.649449', 'Dany', '-', '171', 'prepaid5@blocket.se', '-', '-', '421930', 'Tienda Mensual', '6000', 'invoice', '2', '6000', 'credit', 'VN', '1', 'confirmed', 'Profesional'],
                ['2014-10-23 19:17:28.483897', 'Dany', '-', '172', 'prepaid5@blocket.se', '-', '-', '375783', 'Tienda Trimestral', '15000', 'invoice', '3', '15000', 'credit', 'VN', '1', 'confirmed', 'Profesional'],
                ['2014-10-23 19:17:40.487087', 'Dany', '-', '173', 'prepaid5@blocket.se', '-', '-', '298536', 'Tienda Semestral', '27000', 'invoice', '4', '27000', 'credit', 'VN', '1', 'confirmed', 'Profesional']]
        self.assertEquals(result_data, data)
        request_script = """
          done = arguments[1]
          fetch(arguments[0]).then(response => response.text()).then(data => done(data));
        """
        response = wd.execute_async_script(request_script, websql.download_csv.get_attribute('href'))
        csvdata = list(csv.reader(response.splitlines(), delimiter=';'))
        expected_csvdata = [['Fecha', 'Telesales agent name', 'Id Aviso', 'Num. Pedido', 'Email', 'Regi\ufffdn', 'Category', 'Cod. Autorizaci\ufffdn', 'Producto', 'Precio Producto', 'Tipo Documento', 'Num. Documento', 'Precio', 'Tipo Pago', 'Tipo Cuota', 'Cuotas', 'Estado', 'Tipo Usuario', ''],
                            ['2014-10-22 12:05:29.325122', 'Dany', '', '170', 'prepaid5@blocket.se', '', ' - ', '441576', 'Tienda Anual', '48000', 'invoice', '1', '48000', 'credit', 'VN', '1', 'confirmed', 'Profesional', ''],
                            ['2014-10-23 19:15:46.649449', 'Dany', '', '171', 'prepaid5@blocket.se', '', ' - ', '421930', 'Tienda Mensual', '6000', 'invoice', '2', '6000', 'credit', 'VN', '1', 'confirmed', 'Profesional', ''],
                            ['2014-10-23 19:17:28.483897', 'Dany', '', '172', 'prepaid5@blocket.se', '', ' - ', '375783', 'Tienda Trimestral', '15000', 'invoice', '3', '15000', 'credit', 'VN', '1', 'confirmed', 'Profesional', ''],
                            ['2014-10-23 19:17:40.487087', 'Dany', '', '173', 'prepaid5@blocket.se', '', ' - ', '298536', 'Tienda Semestral', '27000', 'invoice', '4', '27000', 'credit', 'VN', '1', 'confirmed', 'Profesional', ''],
                            []]
        self.assertListEqual(csvdata, expected_csvdata)


    @tags('controlpanel', 'telesales', 'websql')
    def test_websql_telesales_daily(self, wd):
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Telesales Report',
                           timestamp_from='2014-11-19 00:00:00',
                           timestamp_to='2014-11-19 23:59:59')
        result_data = websql.get_result_data()

        data = [['2014-11-19 16:47:53.61302', 'Dany', '8000018', '174', 'android@yapo.cl', 'Regi�n Metropolitana', 'Inmuebles - Vendo', '789494', 'Subir Diario', '11500', 'bill', '5', '24000', 'credit', 'VN', '1', 'sent', 'Persona'],
                ['2014-11-19 16:47:53.61302', 'Dany', '8000021', '174', 'android@yapo.cl', 'Regi�n Metropolitana', 'Servicios - negocios y empleo - Ofertas de empleo', '789494', 'Subir Diario', '8000', 'bill', '5', '24000', 'credit', 'VN', '1', 'sent', 'Persona'],
                ['2014-11-19 16:47:53.61302', 'Dany', '8000022', '174', 'android@yapo.cl', 'Regi�n Metropolitana', 'Tiempo libre - Instrumentos musicales y accesorios', '789494', 'Subir Diario', '4500', 'bill', '5', '24000', 'credit', 'VN', '1', 'sent', 'Persona']]

        self.assertEquals(result_data, data)
