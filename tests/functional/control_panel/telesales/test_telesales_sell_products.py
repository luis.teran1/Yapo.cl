# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelTelesales, ControlPanelStores
from yapo.pages.stores import EditForm
from yapo.pages.simulator import Simulator
from yapo.pages.desktop import Login, Dashboard
from yapo import utils
from selenium.webdriver.support.select import Select
import yapo
import yapo.conf
import time
import unittest


class TelesalesUtil(yapo.SeleniumTest):


    def _telesales_options(self, wd, data):
        """ option telesales """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search=data['email'])
        cp_telesales.button_search.click()

        has_store_product = False
        has_ad_product = False
        has_pack_product = False

        for prod in data['products']:
            if(prod[0] in ['bump', 'daily_bump', 'weekly_bump', 'gallery']):
                cp_telesales.select_product_by_list_id(prod[0], prod[1])
                has_ad_product = True
            elif (prod[0].startswith('PCAR') or prod[0].startswith('PRES')):
                cp_telesales.select_pack(prod[1])
                has_pack_product = True
            else:
                cp_telesales.select_store_product(prod[0])
                has_store_product = True

        cp_telesales.fill_card_info(self.bconfs, data)

        if 'invoice_data' in data:
            cp_telesales.invoice.click()
            invoice = data['invoice_data']

            if 'errors' in invoice:
                cp_telesales.email.submit()
                # check form errors
                for error in invoice['errors']:
                    self.assertIn(error, cp_telesales.payment_form.text)
                cp_telesales.bill.click()
                cp_telesales.pay_button.click()
            else:
                self.assertEquals(cp_telesales.email.get_attribute('value'), invoice['email'])
                self.assertEquals(cp_telesales.lob.get_attribute('value'), invoice['lob'])
                self.assertEquals(cp_telesales.rut.get_attribute('value'), invoice['rut'])
                self.assertEquals(cp_telesales.address.get_attribute('value'), invoice['address'])
                self.assertEquals(cp_telesales.contact.get_attribute('value'), invoice['contact'])
                self.assertEquals(cp_telesales.name.get_attribute('value'), invoice['name'])
                self.assertEquals(Select(cp_telesales.region).first_selected_option.get_attribute('value'), invoice['region'])
                Select(cp_telesales.communes).select_by_value('295')

                cp_telesales.email.submit()
        else:
            cp_telesales.bill.click()
            cp_telesales.pay_button.click()



        if self.bconfs['*.*.payment.telesales.webpay_plus.enabled'] == '0':
            simulator = Simulator(wd)
            simulator.pay(data.get('payment'))

        if 'succes_data' in data:
            table = cp_telesales.get_elements_succes_page()
            self.assertEquals(table, data['succes_data'])
            # verify purchase
            purchase_table = cp_telesales.get_purchase_data_succes_page()

            self.assertEquals(purchase_table['Nombre de comercio'], 'Yapo.cl')
            self.assertEquals(purchase_table['N� de cuotas'], '1')
            self.assertEquals(purchase_table['Tipo de cuotas'], 'Sin Cuotas')
            self.assertEquals(purchase_table['Tipo de pago'], 'Cr�dito')
            self.assertEquals(purchase_table['Tipo de transacci�n'], 'Venta')

            if has_pack_product:
                self.assertIsNotNone(utils.search_mails('Pack comprado'))

            if has_store_product:
                self.assertIsNotNone(utils.search_mails('Felicitaciones! la compra de tu tienda se realiz� con �xito'))
                self.assertIsNotNone(utils.search_mails('https?://.*?store_edit.*'))

            utils.rebuild_index()
            time.sleep(1)
            if has_ad_product:
                self.assertIsNotNone(utils.search_mails('https?://.*?dashboard'))

            self.assertIsNotNone(utils.search_mails(data['email']))

        elif 'errors' in data:
            # Transbank Error
            self.assertIn(data['errors'], cp_telesales.errors.text)
        cp.logout()

    def _telesales_account_status(self, wd, data):
        """ option telesales """

        store_info_text = {'active': 'Usuario sin tienda',
                           'pending': 'Cuenta pendiente de confirmaci�n',
                           'inactive': 'Cuenta Inactiva',
                           'without_account': 'Usuario sin cuenta',
                           'admin_deactivated': 'Tienda bloqueada por Administrador'
                           }

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search=data['email'])
        cp_telesales.button_search.click()

        if 'status' in data:
            self.assertEquals(cp_telesales.plan_none.is_enabled(), data['status'] == 'active')
            self.assertEquals(cp_telesales.monthly_store.is_enabled(), data['status'] == 'active')
            self.assertEquals(cp_telesales.quarterly_store.is_enabled(),  data['status'] == 'active')
            self.assertEquals(cp_telesales.biannual_store.is_enabled(),  data['status'] == 'active')
            self.assertEquals(cp_telesales.annual_store.is_enabled(), data['status'] == 'active')
            # vefiry store info
            self.assertIn(cp_telesales.store_info.text, store_info_text[data['status']])

        if 'store_info' in data:
            self.assertIn(data['store_info'], cp_telesales.store_info.text)

        cp.logout()


class TelesalesWebpayPlusProducts(TelesalesUtil):

    snaps = ['accounts']

    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.8.2020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2060.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2080.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2120.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7060.value': 'price:8050',
        '*.*.payment_settings.product.1.2.2020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2060.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2080.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2120.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7060.value': 'price:4850',
        '*.*.payment_settings.product.1.3.2020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2080.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2120.value': 'price:6450',
        '*.*.payment_settings.product.1.2.1220.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1240.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1260.value': 'price:7050',
        '*.*.payment_settings.product.1.8.*.value': 'price:4550',
        '*.*.payment_settings.product.1.2.*.value': 'price:2750',
        '*.*.payment.telesales.webpay_plus.enabled': '1',
    }

    def setUp(self):
        utils.clean_mails()
        utils.restore_snaps_in_a_cooler_way(['accounts'])

    @tags('controlpanel', 'accounts', 'telesales', 'payment')
    def test_telesales_buy_products_with_missing_card(self, wd):
        """Check telesales buy products with webpay_plus missing card """

        data = {
                'email': 'many@ads.cl',
                'products': [['bump', '8000078'], ['bump', '8000077']],
                'card_info' : {
                    'card_number': '',
                    'card_shares': '1',
                    'card_exp_month': '03',
                    'card_exp_year': '18',
                    'card_cvv': '123',
                    },
                'errors': 'ERROR_CARD_PARAMS_MISSING',
                }

        self._telesales_options(wd, data)



class TelesalesSellAdsProducts(TelesalesUtil):

    snaps = ['accounts']

    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.8.2020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2060.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2080.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2120.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7060.value': 'price:8050',
        '*.*.payment_settings.product.1.2.2020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2060.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2080.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2120.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7060.value': 'price:4850',
        '*.*.payment_settings.product.1.3.2020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2080.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2120.value': 'price:6450',
        '*.*.payment_settings.product.1.2.1220.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1240.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1260.value': 'price:7050',
        '*.*.payment_settings.product.1.8.*.value': 'price:4550',
        '*.*.payment_settings.product.1.2.*.value': 'price:2750',
        '*.*.payment.telesales.webpay_plus.enabled': '0',
    }

    def setUp(self):
        utils.clean_mails()
        utils.restore_snaps_in_a_cooler_way(['accounts'])

class TelesalesSellStoreProducts(TelesalesUtil):

    snaps = ['accounts']
    bconfs = {
        "*.*.payment.product.4.expire_time": "5 minutes",
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.8.*.value': 'price:4550',
        '*.*.payment_settings.product.1.2.*.value': 'price:2750',
        '*.*.payment_settings.product.1.3.2020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2080.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2120.value': 'price:6450',
        '*.*.payment.telesales.webpay_plus.enabled': '0',
    }

    def setUp(self):
        utils.clean_mails()

    # ads products and store
    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_telesales_with_store_status(self, wd):
        """ Check telesales with store status active and admin deactivated"""

        cp = ControlPanel(wd)
        utils.flush_redises()
        cp.go()
        cp.login("dany", "dany")
        cp.link_editar_tienda.click()

        cp_stores = ControlPanelStores(wd)
        cp_stores.go("user.01@schibsted.cl")
        self.assertTrue(cp_stores.info_str_not_found.is_displayed())
        cp_stores.create_store()
        cp_stores.wait.until_present('success_message')
        self.assertEquals(cp_stores.success_message.text, "Exito:Tienda creada exitosamente por un per�odo de 5 minutes")
        self.assertEquals(cp_stores.info_str_status.text, "inactive")
        # Edit the store using CP tool
        cp_stores.edit_store()
        editForm = EditForm(wd)
        editForm.wait.until_present('cpanel_logged_info')
        self.assertEquals(editForm.cpanel_logged_info.text, 'Logged in as dany')
        editForm.fill_form(info_text='informaci�n para prueba de store\nusuario  user.01@schibsted.cl', name='user.01@schibsted.cl store')
        editForm.submit.click()
        # Verify success page
        cp_stores.wait.until_present('edit_success_message')
        cp_stores.edit_success_close_link.click()
        cp_stores.back_to_cp()

        cp.logout()

        data = {'email': 'user.01@schibsted.cl',
                'store_info': 'Estado:Activa'
                }

        self._telesales_account_status(wd, data)

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.link_editar_tienda.click()

        cp_stores = ControlPanelStores(wd)
        cp_stores.go("user.01@schibsted.cl")
        cp_stores.deactivate_button.click()
        cp_stores.wait.until_present('success_message')
        cp.logout()

        data = {'email': 'user.01@schibsted.cl',
                'status': 'admin_deactivated'
                }

        self._telesales_account_status(wd, data)

    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_telesales_cart(self, wd):
        """ Check cart telesales products"""

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search='many@ads.cl')
        cp_telesales.button_search.click()

        cp_telesales.select_store_product('quarterly_store')

        self.assertIn('Tienda = Trim. $15.000\nTotal $15.000', cp_telesales.cart.text)

        cp_telesales.select_store_product('monthly_store')
        self.assertIn('Tienda = Mens. $6.000\nTotal $6.000', cp_telesales.cart.text)

        cp_telesales.select_store_product('annual_store')
        self.assertIn('Tienda = Anua. $48.000\nTotal $48.000', cp_telesales.cart.text)

        cp_telesales.select_store_product('biannual_store')
        self.assertIn('Tienda = Seme. $27.000\nTotal $27.000', cp_telesales.cart.text)

        cp_telesales.select_store_product('plan_none')
        self.assertIn('Tienda = - $0\nTotal $0', cp_telesales.cart.text)


class TelesalesStoreAccountStatus(TelesalesUtil):

    snaps = ['accounts']

    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_telesales_active_account(self, wd):
        """ Check enabled store product for a active account"""

        data = {'email': 'many@ads.cl',
                'status': 'active'
                }

        self._telesales_account_status(wd, data)

    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_store_telesales_pending_account(self, wd):
        """ Check disabled store product for a pending account"""

        data = {'email': 'acc_pending@yapo.cl',
                'status': 'pending'
                }

        self._telesales_account_status(wd, data)

    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_store_telesales_inactive_account(self, wd):
        """ Check disabled store product for a inactive account"""

        data = {'email': 'acc_inactive@yapo.cl',
                'status': 'inactive'
                }

        self._telesales_account_status(wd, data)

    @tags('controlpanel', 'accounts', 'stores', 'telesales')
    def test_telesales_without_account(self, wd):
        """ Check disable store products for email without an account"""

        data = {'email': 'withoutt@accounts.cl',
                'status': 'without_account'
                }

        self._telesales_account_status(wd, data)
