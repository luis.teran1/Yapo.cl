# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
class TelesalesAdHistory(yapo.SeleniumTest):

    snaps = ['accounts', 'difftelesalesbasic']

    @tags('desktop', 'controlpanel', 'telesales', 'search')
    def test_adhistory_bump(self, wd):
        cp = control_panel.ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_vender_productos')
        cp.link_vender_productos.click()
        cp.fill_form(text_field_email='android@yapo.cl')
        cp.button_search.click()

        cp.table_rows_data_grid[0].find_element_by_link_text("Ad history").click()
        wd.switch_to_window(wd.window_handles[1])

        data_search = "2014-07-15 12:48 bump bump accepted dany"
        row_text = ""
        for row in cp.table_ad_history_rows:
            if(row.text == data_search):
                row_text = row.text
                break
        self.assertEqual(row_text, data_search)

    @tags('desktop', 'controlpanel', 'gallery')
    def test_adhistory_gallery(self, wd):
        cp = control_panel.ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_vender_productos')
        cp.link_vender_productos.click()
        cp.fill_form(text_field_email='android@yapo.cl')
        cp.button_search.click()

        cp.table_rows_data_grid[2].find_element_by_link_text("Ad history").click()
        wd.switch_to_window(wd.window_handles[1])

        data_search = "2014-07-15 12:48 gallery pay accepted dany"
        row_text = ""
        for row in cp.table_ad_history_rows:
            if(row.text == data_search):
                row_text = row.text
                break
        self.assertEqual(row_text, data_search)

    @tags('desktop', 'controlpanel', 'weekly')
    def test_adhistory_weekly(self, wd):
        cp = control_panel.ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_vender_productos')
        cp.link_vender_productos.click()
        cp.fill_form(text_field_email='android@yapo.cl')
        cp.button_search.click()

        cp.table_rows_data_grid[4].find_element_by_link_text("Ad history").click()
        wd.switch_to_window(wd.window_handles[1])

        data_search = "2014-07-15 12:48 weekly_bump pay accepted dany"
        row_text = ""
        for row in cp.table_ad_history_rows:
            if(row.text == data_search):
                row_text = row.text
                break
        self.assertEqual(row_text, data_search)



