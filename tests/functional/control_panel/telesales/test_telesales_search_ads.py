# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
class TelesalesAccountEdit(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'telesales', 'search')
    def test_basic_search(self, wd):
        cp = control_panel.ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_vender_productos')
        cp.link_vender_productos.click()
        cp.fill_form(text_field_email='prepaid5@blocket.se')
        cp.button_search.click()

        num_rows_before = len(cp.table_rows_data_grid)
        cp.fill_form(text_field_query = 'casa')
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_hidden)
        self.assertEqual(num_rows_before, num_rows_after)

        cp.fill_form(text_field_query = 'race')
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_hidden)
        self.assertFalse( num_rows_before == num_rows_after)

        cp.fill_form(text_field_query = '')
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_hidden)
        self.assertFalse( num_rows_before == num_rows_after)
        num_rows_after = len(r_visible)
        self.assertTrue( num_rows_before == num_rows_after)

    @tags('controlpanel', 'telesales', 'search')
    def test_selecting_products(self, wd):
        cp = control_panel.ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_vender_productos')
        cp.link_vender_productos.click()
        cp.fill_form(text_field_email='prepaid5@blocket.se')
        cp.button_search.click()

        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        r_visible[0].find_elements_by_css_selector('input[type="checkbox"]')[0].click()
        cp.fill_form(text_field_query = 'casa')
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_visible)
        self.assertTrue( 2 == num_rows_after)

        cp.fill_form(text_field_query = 'race')
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_visible)
        self.assertTrue( 4 == num_rows_after)

        cp.checkbox_persist_checked.click()
        cp.button_search_query.click()
        (r_visible,r_hidden) = cp.table_rows_data_grid_elements()
        num_rows_after = len(r_visible)
        self.assertTrue( 2 == num_rows_after)
