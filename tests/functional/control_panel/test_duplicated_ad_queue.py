# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertSuccess
from yapo.pages.ad_insert import AdInsertDuplicatedAd
from yapo.pages.control_panel import ControlPanel
import yapo


class AdInsertDuplicateAdDesktopQueue(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop

    snaps = ['accounts', 'diff_deactive_ads']

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject': 'Adeptus Astartes Codex',
           }
    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    isdesktop = True

    @tags('ad_insert', 'desktop', 'account', 'duplicate')
    def test_duplicate_ad_insert_logged_force_publish_review_queue(self, wd):
        """
        Review CP Queue:
        Duplicate ad - With an user logged in desktop
        Try to insert an ad with a previous deactived ad
        The ad is detected as duplicated
        The 'publicalo aqui' button redirects to result page
        Then the ad must be moved to Review Queue on CP
        """
        login_and_go_to_dashboard(wd, self.user['email'], self.user['passwd'], from_desktop=self.isdesktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_present('duplicated')
        if self.isdesktop:
            self.assertTrue(result.is_element_present('publish_f_2'))
            result.publish_f_2.click()
        else:
            self.assertTrue(result.is_element_present('publish_f'))
            result.publish_f.click()

        result = AdInsertSuccess(wd)
        result.wait.until_visible('info_box')
        self.assertIn(
            "Tu aviso Adeptus Astartes Codex ser� revisado y si est� de "
            "acuerdo con las reglas de Yapo.cl, lo publicaremos en los pr�ximos "
            "minutos.", result.success_message.text)

        # REVIEW ON CP QUEUES
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_queue('Duplicated by inactive')

        # TODO Check duplicated
        self.assertEqual(1, len(cp.duplicated_ads))
        duplicated = cp.duplicated_ads[95]
        self.assertIn("Deactivated", duplicated.status.text)
        self.assertIn(self.user['subject'], duplicated.subject.text)

        cp.review_ad("accept")

    def _insert_ad_logged(self, wd):
        ad_insert = self.AdInsert(wd)
        ad_insert.go()

        self.assertEqual(
            ad_insert.name.get_attribute('value'), self.user['name'])
        self.assertEqual(
            ad_insert.email.get_attribute('value'), "prepaid5@blocket.se")

        ad_insert.insert_ad(subject=self.user['subject'],
                            body="for teh emporer!",
                            region=15,
                            communes=331,
                            category=6020,
                            price=778889,
                            accept_conditions=True)
        ad_insert.submit.click()

    def _insert_ad_unlogged(self, wd):
        if self.AdInsert == AdInsertMobile:
            ad_insert = self.AdInsert(wd)
            ad_insert.go()
            ad_insert.insert_ad(subject="Adeptus Astartes Codex",
                                body="for teh emporer!",
                                region=15,
                                communes=331,
                                category=6020,
                                price=778889,
                                accept_conditions=True,
                                email=self.user['email'],
                                email_verification=self.user['email'],
                                name=self.user['name'],
                                password=self.user['passwd'],
                                password_verification=self.user['passwd'],
                                create_account=False,
                                phone="99999999",
                                phone_hidden=False)
            ad_insert.submit.click()
        else:
            insert_an_ad(wd, AdInsert=DesktopAI, subject="Adeptus Astartes Codex",
                                body="for teh emporer!",
                                region=15,
                                communes=331,
                                category=6020,
                                price=778889,
                                accept_conditions=True,
                                email=self.user['email'],
                                email_verification=self.user['email'],
                                name=self.user['name'],
                                password=self.user['passwd'],
                                password_verification=self.user['passwd'],
                                create_account=False,
                                phone="99999999",
                                phone_hidden=False)


class AdInsertDuplicateAdiMobileQueue(AdInsertDuplicateAdDesktopQueue):

    AdInsert = AdInsertMobile
    user_agent = yapo.utils.UserAgents.IOS
    snaps = ['accounts', 'diff_deactive_ads']

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject': 'Adeptus Astartes Codex',
           }

    isdesktop = False
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0',
        '*.trans.clear.enqueue.delay':'0'
    }
