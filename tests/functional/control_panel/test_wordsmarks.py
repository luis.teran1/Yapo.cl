# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, WordsMark
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess
import yapo
import time


class WordsMarks(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_wordsmark']

    @tags('controlpanel', 'adqueues')
    def test_add_new_wordmark(self, wd):
        """Adds a new 'regex' word mark"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login('dany', 'dany')

        wm = WordsMark(wd)
        wm.goAddMark()

        values = {
            'regex': '(\w){50,51}',
            'pureRegex': True,
            'adTypeAll': True,
            'searchBody': True,
            'searchSubject': True,
            'wordBoundary': False,
            'color': 0,
            'category': [2020, 2040, 2060],
            'frameAd': True,
            'warning': 'Phone in description'
        }

        wm.fill_form(**values)
        wm.submit()

        wd.refresh()
        marks = wm.get_marks()
        expected = [
            [],
            ['', 'Heading,Ad text', 'Alla', '        ', 'All', '(two|one){1,2}'],
            ['', 'Heading,Ad text', 'Alla', '        ', 'All', 'or'],
            ['', 'Heading,Ad text', 'Alla', '        ', 'Some', values['regex']],
            ['']
        ]
        self.assertListEqual(marks, expected)

    @tags('controlpanel', 'adqueues')
    def test_highlight_adqueue_warning_phone(self, wd):
        """Checks that the phone warning is visible if the word mark is triggered"""
        mail = 'fredy@schibsted.cl'
        subject = 'warning test'
        body = 'This body should have two regex matches or at least one'
        insert_an_ad(wd, category='2020', email=mail, email_verification=mail, subject=subject, body=body)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu("Search for ad")
        cp.search_for_ads(subject=subject)
        cp.open_ad_on_search(subject)
        self.assertTrue(cp.warning.is_displayed())

    @tags('controlpanel', 'adqueues')
    def test_highlight_adqueue(self, wd):
        """Checks that the word mark has the right amount of matches"""
        mail = 'fredy@schibsted.cl'
        subject = 'highlight test'
        body = 'This will have 2 or 5 matches... or 4?, or maybe just 1'
        insert_an_ad(wd, category='2020', email=mail, email_verification=mail, subject=subject, body=body)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu("Search for ad")
        cp.search_for_ads(subject=subject)
        cp.open_ad_on_search(subject)
        self.assertEquals(len(cp.highlights), 3)
