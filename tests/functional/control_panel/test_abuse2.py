# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.utils import Categories
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.control_panel import ControlPanel
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
import yapo
import random, string

class AdInsert(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_abuse']
    bconfs = {"*.trans.clear.enqueue.delay" : '0'}

    AdInsert = AdInsertDesktop

    def _insert_ad(self, browser, mail, category, subject = ""):
        page = self.AdInsert(browser)
        browser.delete_all_cookies()
        if len(subject) == 0:
            subject = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(10))
        body = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(100))
        price = ''.join(random.choice(string.digits) for _ in range(8))

        data = {
            'subject': subject,
            'body': body,
            'category': category,
            'price': price,
            'region': 15,
            'communes': 331,
            'private_ad': True,
            'name': 'yolo',
            'email': mail,
            'email_verification': mail,
            'phone': '86218476',
            'password': '999966666',
            'password_verification': '999966666',
            'create_account': False,
            'accept_conditions': True
        }
        insert_an_ad(browser, **data)


    @tags('ad_insert', 'controlpanel')
    def test_check_abuse_2(self, browser):
        """ Insert ad on desktop, dont click on Mostrar mapa """
        data = [
                    ["mail_1@falso.cl", "5020", ""],
                    ["mail_2@falso.cl", "6140", ""],
                    ["mail_3@falso.cl", "5020", "Compra la wea conchetumare"],
                    ["mail_4@falso.cl", "6140", "Compra la wea perkin"],
                    ["mail_5@falso.cl", "5020", "Subject"],
                    ["mail_6@falso.cl", "5020", "Subject"],
                    ["mail_7@falso.cl", "5020", "Subject"],
                    ["mail_8@falso.cl", "6140", "Subject"],
                    ["mail_9@falso.cl", "6140", "Subject"],
                    ["mail_9@falso.cl", "6140", "Subject"]
                ]

        for mail, category, subject in data:
            self._insert_ad(browser, mail, category, subject)

        cp = ControlPanel(browser)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad('mail_5@falso.cl', 'Subject')
        cp.review_ad('abuse')
        cp.go()
        cp.go_to_option_in_menu("Queues")

        print(cp.queue_lists_group[0].text)
        print(cp.queue_lists_group[1].text)
        if "22 Unreviewed" not in cp.queue_lists_group[0].text: assert False
        if "2 First Experience" not in cp.queue_lists_group[0].text: assert False
        if "4 Abuse" not in cp.queue_lists_group[0].text: assert False
        if "1 Abuse 2" not in cp.queue_lists_group[0].text: assert False
        if "2 Automatically accept" not in cp.queue_lists_group[1].text: assert False
