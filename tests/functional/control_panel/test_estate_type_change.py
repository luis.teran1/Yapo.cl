# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo import utils
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.steps import trans as transSteps
import yapo


class EstateTypeChangesCP(yapo.SeleniumTest):

    snaps = ['accounts']
    email = '@yapodev.cl'

    def data_update(self, region=1, category=1220, estate_type=1):
        dictionary = {
            'category': category,
            'estate_type': estate_type,
            'price': 1000000,
            'rooms': 1,
            'garage_spaces': 1,
            'bathrooms': 1,
            'name': 'yolo yolo',
            'company_ad': 1,
            'phone': '65432198',
            'password': '123123123',
            'region': region,
        }

        if category == 1220:
            dictionary['condominio'] = 20000
            dictionary['size'] = 50
            dictionary['type'] = 's'
        if category == 1240:
            dictionary['condominio'] = 20000
            dictionary['size'] = 50
            dictionary['type'] = 'u'
        if category == 1260:
            dictionary['type'] = 'u'

        dictionary['body'] = 'Test description {0}'.format(category)
        dictionary['email'] = '{0}_{1}_{2}'.format(
            category, region, self.email)
        dictionary['subject'] = 'Test {0} {1} {2}'.format(
            region, category, estate_type)
        dictionary['communes'] = utils.Regions.first_commune(region)

        return dictionary

    @tags('controlpanel', 'estate_type')
    def test_insert_and_change_estate_type(self, wd):
        """ On review ad page change the estate type parameter
            from Depto to Casa """
        dictionary = self.data_update(region=1)
        subject = dictionary['subject']
        # Insert
        transSteps.new_ad(dictionary)
        # Go to Cp change estate type & accept
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ads(subject=subject)
        cpanel.open_ad_on_search(subject)
        cpanel.fill('review_estate_type', 2)
        cpanel.review_ad(action='accept')
        cpanel.close_window_and_back()

        utils.rebuild_index()
        # Go to AdView and check new estate type
        ad_list = AdListDesktop(wd)
        ad_list.go()
        ad_list.search_for_ad(dictionary['subject'])
        ad_list.open_first_ad()

        ad_view = AdViewDesktop(wd)
        self.assertIn('Casa', ad_view.adparams_box.text)

    @tags('controlpanel', 'estate_type')
    def test_change_category(self, wd):
        """Change category auto select type if only have once
            also show or hide estate type"""
        dictionary = self.data_update(region=2)
        subject = dictionary['subject']
        # Insert
        transSteps.new_ad(dictionary)
        # Go to Cp change estate type & accept
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ads(subject=subject)
        cpanel.open_ad_on_search(subject)
        self.assertTrue(cpanel.review_estate_type.is_displayed())
        cpanel.fill('review_category', 2060)
        self.assertFalse(cpanel.review_estate_type.is_displayed())
        cpanel.fill('review_category', 1240)
        self.assertTrue(cpanel.currency[0].is_displayed())
        self.assertEquals(cpanel.currency_current.get_attribute('value'), 'peso')
        self.assertTrue(cpanel.review_estate_type.is_displayed())
        select = Select(cpanel.review_type)
        self.assertEqual('Arriendo', select.first_selected_option.text)
        cpanel.review_ad(action='accept')
        cpanel.close_window_and_back()

        utils.rebuild_index()
        # Go to AdView and check new estate type
        ad_list = AdListDesktop(wd)
        ad_list.go()
        ad_list.search_for_ad(dictionary['subject'])
        ad_list.open_first_ad()

        ad_view = AdViewDesktop(wd)
        self.assertIn('Departamento', ad_view.adparams_box.text)
        self.assertEqual(
            'Arriendo - Departamento', ad_view.breadcrumb_category.text)

    @tags('controlpanel', 'estate_type')
    def test_move_from_sell_to_temp_rent(self, wd):
        """ Change category sell to T rent and other estate_type """
        dictionary = self.data_update(region=3)
        subject = dictionary['subject']
        # Insert
        transSteps.new_ad(dictionary)
        # Go to Cp change estate type & accept
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ads(subject=subject)
        cpanel.open_ad_on_search(subject)
        self.assertTrue(cpanel.currency[0].is_displayed())
        self.assertEquals(cpanel.currency_current.get_attribute('value'), 'peso')
        cpanel.fill('review_category', 1260)
        self.assertFalse(cpanel.currency[0].is_displayed())
        cpanel.fill('review_estate_type', 2)

        cpanel.review_ad(action='accept')
        cpanel.close_window_and_back()

        utils.rebuild_index()
        # Go to AdView and check new estate type
        ad_list = AdListDesktop(wd)
        ad_list.go()
        ad_list.search_for_ad(dictionary['subject'])
        ad_list.open_first_ad()

        ad_view = AdViewDesktop(wd)
        self.assertIn('Casa', ad_view.adparams_box.text)
        self.assertEqual(
            'Arriendo de temporada - Casa', ad_view.breadcrumb_category.text)

    @tags('controlpanel', 'estate_type')
    def test_move_from_sell_to_rent(self, wd):
        """ Change category sell to rent and other estate_type """
        dictionary = self.data_update(region=4, category=1220, estate_type=1)
        subject = dictionary['subject']
        # Insert
        transSteps.new_ad(dictionary)
        # Go to Cp change estate type & accept
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ads(subject=subject)
        cpanel.open_ad_on_search(subject)
        select = Select(cpanel.review_type)
        self.assertEqual('Vendo', select.first_selected_option.text.strip())
        self.assertTrue(cpanel.currency[0].is_displayed())
        self.assertEquals(cpanel.currency_current.get_attribute('value'), 'peso')
        cpanel.fill('review_category', 1260)
        cpanel.fill('review_estate_type', 1)

        select = Select(cpanel.review_type)
        self.assertEqual('Arriendo', select.first_selected_option.text)
        self.assertFalse(cpanel.currency[0].is_displayed())
        self.assertEquals(cpanel.currency_current.get_attribute('value'), 'peso')

        cpanel.review_ad(action='accept')
        cpanel.close_window_and_back()

        utils.rebuild_index()
        # Go to AdView and check new estate type
        ad_list = AdListDesktop(wd)
        ad_list.go()
        ad_list.search_for_ad(dictionary['subject'])
        ad_list.open_first_ad()

        ad_view = AdViewDesktop(wd)
        self.assertIn('Departamento', ad_view.adparams_box.text)
        self.assertEqual(
            'Arriendo de temporada - Departamento', ad_view.breadcrumb_category.text)
