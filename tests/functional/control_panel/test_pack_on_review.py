# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo import utils
import yapo


class ControlpanelPackOnReview(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_assigned_if']

    @tags('controlpanel', 'pack')
    def test_pack_on_review_w_pack_auto(self, wd):
        """ Account with pack review car category """
        list_id = 8000092
        self._scenario(wd, list_id, True)

    @tags('controlpanel', 'pack')
    def test_pack_on_review_w_pack_inserting_fee(self, wd):
        """ Account with pack review an inserting fee inmo category """
        list_id = 8000097
        self._apply_pack(11)
        self._scenario(wd, list_id, True)

    @tags('controlpanel', 'pack')
    def test_pack_on_review_w_pack_other_cat(self, wd):
        """ Account with pack review an ad in other category """
        list_id = 8000062
        self._apply_pack(5)
        self._scenario(wd, list_id, False)

    @tags('controlpanel', 'pack')
    def test_pack_on_review_without_pack(self, wd):
        """ Account without pack review car category """
        list_id = 8000095
        self._scenario(wd, list_id, False)

    def _scenario(self, wd, list_id, should_be_pressent):

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ads(list_id=list_id)
        cp.open_ad_on_search()
        if should_be_pressent:
            cp.wait.until_present('account_with_pack')
            self.assertEquals(cp.account_with_pack.text, 'Cuenta con pack')
        else:
            cp.wait.until_not_present('account_with_pack')
        cp.close_window_and_back()

    def _apply_pack(self, account_id):
        utils.trans("insert_pack",
                    account_id=account_id,
                    product_id=11010,
                    type='car',
                    period=1,
                    slots='010')
