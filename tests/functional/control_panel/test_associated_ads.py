# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.steps import insert_an_ad
import yapo

class ControlPanelAssociatedAds(yapo.SeleniumTest):

    snaps = ['accounts']
    def _insert_ad(self, wd, currency):
        ad_data = {
            'subject': 'Parcelas frente a laguna de aculeo 100 hectareas',
            'body': ("dos parcelas frente a laguna de aculeo y frente a reserva natural altos de cantillana"
                "1 parcela de 480000 m2 lotiada en 30. otra parcela de 530000 m2. estas parcelas estan contiguas"
                "valor de 350 pesos el m2 168000000 cada parcela."),
            'price': '50000',
            'currency': currency,
            'category': '1220',
            'passwd': '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
            'email': 'android@yapo.cl',
            'region': '15',
            'communes': '331'
        }
        return insert_an_ad(wd, AdInsert=None, submit=True, **ad_data)

    def _deactivate_ad(self, wd, list_id):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.deactivate_ad(list_id)

    def _assertPrice(self, wd, related_ads, price):
        for ad_id in related_ads:
            self.assertIn(price[ad_id], related_ads[ad_id].detail.get_attribute('innerHTML'))

    @tags('controlpanel')
    def test_currency_associated_ads(self, wd):
        """ Test currencies on CP associated ads table """

        # "populate" associated ads column on CP review
        self._insert_ad(wd, 'peso')
        self._insert_ad(wd, 'uf')
        self._deactivate_ad(wd, 8000011)

        cp = ControlPanel(wd)
        cp.go()
        cp.go_to_queue('Duplicated by active')

        prices = {
            20: '$ 25.000.000',
            26: '$ 170.000',
            31: '$ 250.000.000',
            33: '-',
            126: '$ 50.000',
            127: 'UF 50.000,00',
        }

        associated_ads = [
            cp.duplicated_ads,
            cp.deactivated_ads,
            cp.published_ads
        ]

        for type in associated_ads:
            self._assertPrice(wd, type, prices)
