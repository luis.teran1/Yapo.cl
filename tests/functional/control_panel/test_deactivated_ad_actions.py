# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.control_panel import ControlPanel
import yapo
import time


class DeactivatedAdActionsCp(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(
            ['accounts', 'diff_deactive_ads'])
        yapo.utils.rebuild_csearch()

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject': 'Adeptus Astartes Codex',
            'list_id': '8000073',
            }

    @tags('contropanel', 'deactivated')
    def test_activate_deactivated(self, wd):
        """
        Deactive > Active > Deactive the same ad and check ad history
        """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.activate_ad(self.user['list_id'])
        cp.deactivate_ad(self.user['list_id'])

        cp.search_for_ad_by_list_id(self.user['list_id'])
        cp.table_on_search.find_element_by_link_text("Ad history").click()
        wd.switch_to_window(wd.window_handles[1])

        row = 6
        data_search = "deactivate user_deactivated deactivated"
        self.assertIn(data_search, cp.table_ad_history_rows[row + 0].text)
        data_search = "activate admin_activated activated dany"
        self.assertIn(data_search, cp.table_ad_history_rows[row + 1].text)
        data_search = "deactivate admin_deactivated deactivated dany"
        self.assertIn(data_search, cp.table_ad_history_rows[row + 2].text)
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
