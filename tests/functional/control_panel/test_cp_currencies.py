# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.steps import insert_an_ad
from yapo import utils
import yapo, copy


class ControlPanelEditCurrencies(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {'*.*.common.uf_conversion_factor':'22245,5290'}
    conv_error = {'uf': 0.02, 'peso':0.02*22245.5290}

    data = {
        'body':'yoLo',
        'subject':'Uf2peso test',
        'category':1220,
        'region':15,
        'communes':331,
        'price':'4500,30',
        'currency':'uf',
        'private_ad':True,
        'name':'yolo',
        'email':'prepaidX@blocket.se',
        'email_verification':'prepaidX@blocket.se',
        'phone':'99996666',
        'password':'123123123',
        'password_verification':'123123123',
        'create_account':False,
        'accept_conditions':True
    }

    def _other_prefix(self, prefix):
        return '$' if prefix == 'UF' else 'UF'

    def _get_prefix_by_currency(self, currency):
        return '$' if currency == 'peso' else 'UF'

    def format(self, price, toFloat = False):
        conv = price.replace('$ ','').replace('UF ','')
        if not toFloat:
            return conv;
        return conv.replace('.','').replace(',','.')

    def _cp_edit_price(self, wd, data, currency, price, expected):
        currency_curr = data['currency']
        currency_other = 'peso' if currency  == 'uf' else 'uf'
        currency_curr_prefix = self._get_prefix_by_currency(currency_curr)
        currency_exp_prefix = self._get_prefix_by_currency(currency)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(data['subject'])
        wd.find_element_by_partial_link_text(data['subject']).click()
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(data['price'], cp.val('price').replace('.',''))
        self.assertEqual(currency_curr_prefix, cp.currency_prefix.text)
        self.assertEqual(expected[0], cp.other_price.text)
        # Test conversion by changing radio buttons
        currency_radio = cp.get_currency_buttons()
        currency_radio[currency].click()
        self.assertEqual(expected[1], cp.val('price'))
        self.assertEqual(currency_exp_prefix, cp.currency_prefix.text)
        self.assertEqual(expected[2], cp.other_price.text)
        currency_radio[currency_other].click()
        self.assertEqual(self.format(expected[2]), cp.val('price'))
        self.assertEqual(self._other_prefix(currency_exp_prefix), cp.currency_prefix.text)
        self.assertTrue(
                abs(float(self.format(expected[1], True)) -
                float(self.format(cp.other_price.text, True)))
                < self.conv_error[currency])
        # Set new currency and price
        currency_radio[currency].click()
        cp.price.clear()
        cp.price.send_keys(price)
        self.assertEqual(expected[3], cp.other_price.text)
        cp.review_ad(action = 'accept')

    @tags('controlpanel', 'currency')
    def test_cp_edit_uf2peso(self, wd):
        """ Edits currency in controlpanel from uf to peso """
        data = copy.copy(self.data)
        insert_an_ad(wd, **data)

        expected_values = ['$ 100.111.513', '100.111.513', 'UF 4.500,29', 'UF 2.022,87']
        self._cp_edit_price(wd, data, currency='peso', price='45000000', expected = expected_values)

    @tags('controlpanel', 'currency')
    def test_cp_edit_peso2uf(self, wd):
        """ Edits currency in controlpanel from peso to uf"""
        data = copy.copy(self.data)
        data.update(currency='peso', price='95000000')
        data.update(subject='Peso2uf test')
        insert_an_ad(wd, **data)

        expected_values = ['UF 4.270,52', '4.270,52', '$ 94.999.938', '$ 55.620.918']
        self._cp_edit_price(wd, data, currency='uf', price='2500,32', expected = expected_values)

    @tags('controlpanel', 'currency')
    def test_cp_edit_uf2uf(self, wd):
        """ Edits price in controlpanel from uf to uf """
        data = copy.copy(self.data)
        data.update(currency='uf', price='4235,35')
        data.update(subject='Uf2uf test')
        insert_an_ad(wd, **data)

        expected_values = ['$ 94.217.563', '4.235,35', '$ 94.217.563', '$ 55.620.918']
        self._cp_edit_price(wd, data, currency='uf', price='2500,32', expected = expected_values)
