# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo import utils
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.steps import trans as transSteps
import yapo


class PackCategoryChangeCP(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.controlpanel.ad_queue.category.1220.block_cat_option': 1,
        '*.controlpanel.ad_queue.category.1240.block_cat_option': 1,
        '*.controlpanel.ad_queue.category.2020.block_cat_option': 1,
        '*.controlpanel.ad_queue.category.2040.block_cat_option': 1,
        '*.controlpanel.ad_queue.category.7020.block_cat_option': 1,
        '*.*.cat.7020.passwd.required': '0'
    }
    test_data = [
        {'category': '1220', 'text': 'Vendo', 'canmove': '1220, 1240, 1260, 2060'},
        {'category': '1240', 'text': 'Arriendo',  'canmove': '1220, 1240, 1260, 2060'},
        {'category': '1260', 'text': 'Arriendo de temporada', 'canmove': '1260, 2060'},
        {'category': '2020', 'text': 'Autos, camionetas y 4x4', 'canmove': '1260, 2020, 2040, 2060'},
        {'category': '2040', 'text': 'Buses, camiones y furgones', 'canmove': '1260, 2020, 2040, 2060'},
        {'category': '2060', 'text': 'Motos', 'canmove': '1260, 2060'},
        {'category': '7020', 'text': 'Ofertas de empleo', 'canmove': '1260, 2060'},
    ]

    def insert_ad(self, category):
        subject = "Test AD in category {}".format(category)
        data = {
            'subject': subject,
            'category': category,
            'price': 100000,
            'name': 'yolo',
            'company_ad': 1,
            'phone': '65432198',
            'password': '123123123',
        }

        to_update = {
            '1220': {
                'condominio': 20000,
                'size': 50,
                'type': 's'
            },
            '1240': {
                'condominio': 20000,
                'size': 50,
                'type': 'u'
            },
            '1260': {
                'type': 'u'
            },
            '2060': {
                'plates': 'ABC10'
            },
            '7020': {
                'working_day': '2',
                'contract_type': '1',
                'jc': ['20', '24'],
                'email': 'prepaid5@blocket.se',
                'passwd': '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
            }
        }
        if category in to_update:
            data.update(to_update[category])
        transSteps.new_ad(data)
        return subject

    @tags('controlpanel', 'estate_type')
    def test_change_category(self, wd):
        """Change category for an inserted ad and check if it can be moved
            to other categories based on their initial category"""
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        for data in self.test_data:
            # Insert an AD of the tested category
            subject = self.insert_ad(data['category'])
            # Go to Cp login and search the inserted ad
            cpanel.search_for_ads(subject=subject)
            cpanel.open_ad_on_search(subject)
            cpanel.wait.until_loaded()

            #check if the ad can be moved to the different categories
            for new_data in self.test_data:
                cpanel.fill('review_category', new_data['category'])
                select = Select(cpanel.review_category)
                if new_data['category'] in data['canmove']:
                    self.assertEqual(new_data['text'], select.first_selected_option.text.strip())
                else:
                    self.assertNotEqual(new_data['text'], select.first_selected_option.text.strip())
            cpanel.close_window_and_back()
