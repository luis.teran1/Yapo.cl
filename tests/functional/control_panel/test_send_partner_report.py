# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelPartnerReports
from yapo.pages.desktop_list import AdViewDesktop as AdView
from yapo.component import select
from yapo.pages.mails import PartnerReportAdReplyEmail
from yapo.pages.mails import PartnerReportMonthlyEmail
from yapo.pages.mails import PartnerReportActiveEmail
from yapo import utils
import yapo
import datetime


class TestPartnerReportsBase(yapo.SeleniumTest):
    def _go_partner_report(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp = ControlPanelPartnerReports(wd)
        return cp.go()

    def _get_partner_report(self, wd, report_type, partner, email, month, year):
        cp = self._go_partner_report(wd)
        cp.send_report(report_type, partner, email, month, year)
        return cp


class TestPartnerReports(TestPartnerReportsBase):
    
    snaps = ['accounts', 'diff_partners']

    @tags('controlpanel', 'reports')
    def test_send_partners_monthly_report_big_year(self, wd):
        """ Get monthly report with a big year, wait for an error in validation"""

        cp = self._get_partner_report(
            wd, 'monthly', 'blocket_mobile', 'asd@qwe.cl', 'Marzo', 'Selecciona el a�o a consultar')
        self.assertEqual(
            cp.error.text, ControlPanelPartnerReports.ERROR_INVALID_YEAR)
