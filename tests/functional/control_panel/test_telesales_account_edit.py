# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelTelesales
from yapo.pages.simulator import Simulator
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
from selenium.webdriver.support.select import Select
import time
import yapo
import yapo.conf

class TelesalesAccountEdit(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'accounts', 'telesales')
    def test_links_not_present(self, wd):
        """Check 'editar cuenta' and 'vender productos' links are not present
        when no permission was given"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.wait.until_not_present('link_vender_productos')
        cp.wait.until_not_present('link_editar_cuenta')

    @tags('controlpanel', 'accounts', 'telesales')
    def test_revoke_permissions(self, wd):
        """Check 'editar cuenta' and 'vender productos' links are not present
        when we revoke permissions"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go('?m=admin&a=editadmin&admin_id=9')
        cp.checkbox_televentas.click()
        cp.fill_form(checkbox_televentas_editar_cuenta = False)
        cp.fill_form(checkbox_televentas_vender_productos = False)
        cp.button_save.click()
        cp.logout()
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_not_present('link_vender_productos')
        cp.wait.until_not_present('link_editar_cuenta')

    @tags('controlpanel', 'accounts', 'telesales')
    def test_edit_account(self, wd):
        """Check an account can be edited completely and data persisted"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_editar_cuenta')
        cp.link_editar_cuenta.click()
        cp.fill_form(text_field_email='prepaid5@blocket.se')
        cp.button_search.click()
        cp.fill_form(text_field_name='BLOCKET EDITED',
                     select_region='13',   # XI Aisen
                     text_field_rut='66666666-6',
                     text_field_phone='765432144',
                     text_field_address='EDITED ADDRESS')
        cp.fill_form(select_commune='275') # Chile Chico
        cp.button_update_data.click()
        cp.wait.until_present('label_success_edition')
        cp.button_search.click()
        self.assertEqual(cp.text_field_name.get_attribute('value'), 'BLOCKET EDITED')
        self.assertEqual(cp.select_region.get_attribute('value'), '13')
        self.assertEqual(cp.text_field_rut.get_attribute('value'), '66666666-6')
        self.assertEqual(cp.text_field_phone.get_attribute('value'), '765432144')
        self.assertEqual(cp.text_field_address.get_attribute('value'), 'EDITED ADDRESS')


    @tags('controlpanel', 'accounts', 'telesales')
    def test_edit_account_form_changes(self, wd):
        """Check that changing is_company radio values actually changes the form"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_editar_cuenta')
        cp.link_editar_cuenta.click()
        cp.fill_form(text_field_email='prepaid5@blocket.se')
        cp.button_search.click()
        
        cp.radio_is_company_c.click()
        self.assertEqual(cp.label_person_cell.text, 'Nombre completo *')
        self.assertEqual(cp.label_business_name_cell.text, 'Raz�n social')
        self.assertEqual(cp.label_rut_cell.text, 'Rut *')
        self.assertEqual(cp.text_field_contact.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_lob.get_attribute('disabled'), None)

        cp.radio_is_company_p.click()
        self.assertEqual(cp.label_person_cell.text, 'Nombre completo *')
        self.assertEqual(cp.label_rut_cell.text, 'Rut')
        self.assertEqual(cp.text_field_contact.get_attribute('disabled'), 'true')
        self.assertEqual(cp.text_field_lob.get_attribute('disabled'), 'true')
        self.assertFalse(cp.label_business_name_cell.is_displayed())

        cp.radio_is_company_c.click()
        self.assertEqual(cp.label_person_cell.text, 'Nombre completo *')
        self.assertEqual(cp.label_business_name_cell.text, 'Raz�n social')
        self.assertEqual(cp.label_rut_cell.text, 'Rut *')
        self.assertEqual(cp.text_field_contact.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_lob.get_attribute('disabled'), None)


    @tags('controlpanel', 'accounts', 'telesales')
    def test_edit_pro_account(self, wd):
        """Check edit pro account no change user type"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_editar_cuenta')
        cp.link_editar_cuenta.click()
        cp.fill_form(text_field_email='prepaid3@blocket.se')
        cp.button_search.click()
        
        self.assertEqual(cp.radio_is_company_c.get_attribute('disabled'), 'true')
        self.assertEqual(cp.radio_is_company_p.get_attribute('disabled'), 'true')
        self.assertEqual(cp.text_field_name.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_rut.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_phone.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_address.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_contact.get_attribute('disabled'), None)
        self.assertEqual(cp.text_field_lob.get_attribute('disabled'), None)
        self.assertEqual(cp.select_region.get_attribute('disabled'), None)
        self.assertEqual(cp.select_commune.get_attribute('disabled'), None)

