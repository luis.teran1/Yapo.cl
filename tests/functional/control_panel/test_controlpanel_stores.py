# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelStores
from yapo.pages.stores import EditForm, MainImagePopup, Listing, Detail, NotFound
from yapo.pages.simulator import Simulator
from yapo.pages.desktop import Login, Dashboard
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
from selenium.webdriver.support.select import Select
import time
import yapo
import yapo.conf
import nose_selenium
import unittest


class ControlpanelStores(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = { "*.*.payment.product.4.expire_time":"31 days" }

    def common_actions(self,wd):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.wait.until_present('link_crear_tienda')
        cp.link_crear_tienda.click()

    @tags('desktop', 'controlpanel', 'stores')
    def test_create_store(self, wd):
        """Verify the full flow of create store from CP (create-edit-view-history)"""
        self.common_actions(wd)
        # Create store using CP tool
        cp_stores = ControlPanelStores(wd)
        cp_stores.go("many@ads.cl")
        self.assertTrue(cp_stores.info_str_not_found.is_displayed())
        cp_stores.create_store()
        cp_stores.wait.until_present('success_message')
        self.assertEquals(cp_stores.success_message.text,"Exito:Tienda creada exitosamente por un per�odo de 31 days")
        self.assertEquals(cp_stores.info_str_status.text,"inactive")
        # Edit the store using CP tool
        cp_stores.edit_store()
        editForm = EditForm(wd)
        editForm.wait.until_present('cpanel_logged_info')
        self.assertEquals(editForm.cpanel_logged_info.text,'Logged in as dany')
        editForm.fill_form(info_text='informaci�n para prueba de store\nusuario many@ads.cl',name='many ads store')
        editForm.submit.click()
        # Verify success page
        cp_stores.wait.until_present('edit_success_message')
        cp_stores.edit_success_close_link.click()
        cp_stores.back_to_cp()
        # Index store
        utils.rebuild_csearch()
        # Verify store is active
        detailpage = Detail(wd)
        detailpage.go('many-ads-store')
        detailpage.wait.until_present('name')
        self.assertEquals(detailpage.name.text,'Many ads store')
        # Verify history
        cp_stores.go("many@ads.cl")
        history = cp_stores.get_history()

        history_to_check = [
            ['new','reg','dany'],
            ['new','unpaid','dany'],
            ['status_change','reg (pending -> inactive)','dany'],
            ['status_change','accepted','dany'],
            ['extend','reg','dany'],
            ['extend','accepted','dany'],
            ['edit','reg','dany'],
            ['edit','accepted','dany'],
            ['status_change','reg (inactive -> active)','dany'],
            ['status_change','accepted','dany']
            ]

        for x in range(len(history)):
            for y in range(len(history[x])):
                self.assertEquals(history_to_check[x][y],history[x][y])
        self.assertTrue(len(history_to_check) == len(history))
        cp_stores.back_to_cp()

    @tags('desktop', 'stores')
    def test_create_and_extend_store(self,wd):
        """Check extend process changes the date end"""
        self.common_actions(wd)
        # Create store using CP tool
        cp_stores = ControlPanelStores(wd)
        cp_stores.go("prepaid5@blocket.se")
        cp_stores.wait.until(lambda x: cp_stores.info_str_not_found.is_displayed())
        cp_stores.create_store()
        cp_stores.wait.until_present('success_message')
        self.assertEquals(cp_stores.success_message.text,"Exito:Tienda creada exitosamente por un per�odo de 31 days")
        self.assertEquals(cp_stores.info_str_status.text,"inactive")
        # Edit the store using CP tool
        cp_stores.edit_store()
        editForm = EditForm(wd)
        editForm.wait.until_present('cpanel_logged_info')
        self.assertEquals(editForm.cpanel_logged_info.text,'Logged in as dany')
        editForm.fill_form(info_text='informaci�n para prueba de store\nusuario prepaid5',name='prepaid store')
        editForm.submit.click()
        # Verify success page
        cp_stores.wait.until_present('edit_success_message')
        cp_stores.wait.until_present('edit_success_close_link')
        cp_stores.edit_success_close_link.click()
        cp_stores.back_to_cp()
        # Index store
        utils.rebuild_csearch()
        # Edit again to save date end
        cp_stores.edit_store()
        editForm = EditForm(wd)
        editForm.wait.until_present('cpanel_logged_info')
        self.assertEquals(editForm.cpanel_logged_info.text,'Logged in as dany')
        expiration_date = editForm.get_expiration_date()
        editForm.fill_form(info_text='informaci�n para prueba de store\nusuario prepaid5 edited one time',name='prepaid store')
        editForm.submit.click()
        cp_stores.wait.until_present('edit_success_message')
        cp_stores.edit_success_close_link.click()
        cp_stores.back_to_cp()
        # Index store
        utils.rebuild_csearch()
        cp_stores.extend_store()
        cp_stores.wait.until_present('success_message')
        self.assertEquals(cp_stores.success_message.text,"Tienda extendida exitosamente por un per�odo de 31 days")
        # Edit again to check new date end
        cp_stores.edit_store()
        editForm = EditForm(wd)
        editForm.wait.until_present('cpanel_logged_info')
        self.assertEquals(editForm.cpanel_logged_info.text,'Logged in as dany')
        # Check the new expiration date is not the same after extend the store
        new_expiration_date = editForm.get_expiration_date()
        self.assertFalse(expiration_date == new_expiration_date)
        # Check the store type is visible on CP operations (FT1039)
        self.assertEquals(editForm.store_type.text, 'Mensual')

    @unittest.skip("Useless test: checks for Store tab presence (???), and selectors are wrong anyway")
    @tags('desktop', 'stores')
    def test_gifted_store(self, wd):
        """Gifted store must update the active sessions"""
        # We want another browser
        wd2 = nose_selenium.build_webdriver()
        desktop = Login(wd)
        dashboard = Dashboard(wd)
        cp = ControlPanel(wd2)
        cps = ControlPanelStores(wd2)
        try:
            desktop.login('user.01@schibsted.cl', '123123123')
            self.assertTrue(dashboard.without_store_tab)
            cp.go()
            cp.login("dany", "dany")
            cps.go('user.01@schibsted.cl')
            cps.create_store()
            self.assertTrue(dashboard.with_store_tab)
        finally:
            wd2.close()

