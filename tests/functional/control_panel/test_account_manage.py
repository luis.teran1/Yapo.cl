# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelAccounts
from yapo.pages.desktop import Login
from yapo.decorators import tags
from yapo import utils, SeleniumTest, conf
from yapo.pages import generic

class ControlpanelAccountManage(SeleniumTest):

    snaps = ['accounts']

    def _go_cpanel(self, wd):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for users accounts")

    @tags('controlpanel', 'accounts')
    def test_cp_deactivate_active_account(self, wd):
        """ CP Accounts, deactivate account with status active  """

        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email='prepaid3@blocket.se', action='deactivate')
        self.assertEqual(cp_account.result_message.text, 'The account was Deactivated')

    @tags('controlpanel', 'accounts')
    def test_cp_activate_inactive_account(self, wd):
        """ CP Accounts, activate account with status inactive """

        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email='acc_inactive@yapo.cl', action='activate')
        self.assertEqual(cp_account.result_message.text, 'The account was Activated')

    @tags('controlpanel', 'accounts')
    def test_cp_activate_account_status_pending(self, wd):
        """ CP Accounts, activate account with status pending confirmation """

        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email='acc_pending@yapo.cl', action='activate')
        self.assertEqual(cp_account.result_message.text, 'The account was Activated')

    @tags('controlpanel', 'accounts')
    def test_cp_deactivate_account_status_pending(self, wd):
        """ CP Accounts, deactivate account with status pending confirmation """

        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email='acc_pending@yapo.cl', action='deactivate')
        self.assertEqual(cp_account.result_message.text, 'The account was Deactivated')

    @tags('controlpanel', 'accounts')
    def test_cp_account_check_errors(self, wd):
        """ CP Accounts, Check errors  """

        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.fill_form(email_search = 'asdf')
        cp_account.button_search.click()
        self.assertEqual(cp_account.error_message.text, 'Confirma que el correo electr�nico est� en el formato correcto.')
        cp_account.fill_form(email_search = 'asdf@asdf.cl')
        cp_account.button_search.click()
        self.assertEqual(cp_account.error_message.text, 'La cuenta no existe')

    @tags('controlpanel', 'accounts')
    def test_cp_block_account(self, wd):
        """ CP Accounts, blocked account """
        email = 'many@ads.cl'
        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email=email, action='blocked')
        self.assertEqual(cp_account.result_message.text, 'La cuenta fue bloqueada')
        # Verify that the user is blocked
        login = Login(wd)
        login.login(email, '123123123')
        self.assertEqual(login.error_msg.text, 'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.')

    @tags('controlpanel', 'accounts')
    def test_cp_unlock_account(self, wd):
        """ CP Accounts, unlock account """
        email = 'many@ads.cl'
        loginbar = generic.AccountBar(wd)
        # blocked account by redis, set login attempts to max number
        utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'set', 'rsd1x_login_many@ads.cl', 3)
        # Unlock account by cp
        self._go_cpanel(wd)
        cp_account = ControlPanelAccounts(wd)
        cp_account.manage_account(email=email, action='unlocked')
        self.assertEqual(cp_account.result_message.text, 'La cuenta fue desbloqueada')
        # Verify that the user is unlocked
        login = Login(wd)
        login.login(email, '123123123')
        self.assertEqual(loginbar.message.text, 'Hola, ManyAdsAccount')
