# -*- coding: latin-1 -*-
import yapo
from yapo.steps.websql import get_websql
from yapo.decorators import tags

class FinancialSalesReport(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_websql_FSR']

    @tags('controlpanel', 'websql')
    def test_financial(self, wd):
        """Check result of purchases for Financial Sales Report"""
        page = get_websql(wd, 'dany', "Financial Sales Report",
                          timestamp_from="2010-01-01 00:00:00")
        headers = page.get_result_header()
        data = page.get_result_data()

        expected_headers = ['Id Pago', 'Fecha', 'Hora', 'Email',
                            'Tipo Usuario', 'Tipo Producto', 'Total de Bumps',
                            'Fecha de T�rmino', 'Id Aviso' , 'Categor�a padre', 'Categor�a',
                            'Regi�n', 'Medio de Pago', 'Estado de Pago', 'Precio', 'Tipo Documento',
                            'Num. Documento', 'Estado de Documento'
                            ]

        expected_data = [
                ['166', '2014-12-10', '15:21:47', 'prepaid5@blocket.se', 'Profesional', 'Tienda Trimestral', '', '', '-', '--', '--', '--', 'Webpay', 'Pagado', '15000', 'Factura', '1', 'Emitido'],
                ['167', '2014-12-10', '15:22:46', 'prepaid5@blocket.se', 'Persona', 'Vitrina 7 d�as', '', '', '3456789', 'Veh�culos', 'Autos, camionetas y 4x4', 'IX Araucan�a', 'Servipag', 'Pagado', '6400', 'Factura', '2', 'Emitido'],
                ['167', '2014-12-10', '15:22:46', 'prepaid5@blocket.se', 'Persona', 'Subir Diario', '', '', '6000667', 'Inmuebles', 'Vendo', 'I Tarapac�', 'Servipag', 'Pagado', '11500', 'Factura', '2', 'Emitido'],
                ['167', '2014-12-10', '15:22:46', 'prepaid5@blocket.se', 'Persona', 'Subir Diario', '', '', '6000666', 'Tiempo libre', 'Deportes, gimnasia y accesorios', 'X Los Lagos', 'Servipag', 'Pagado', '4500', 'Factura', '2', 'Emitido'],
                ['171', '2014-12-10', '15:23:59', 'android@yapo.cl', 'Persona', 'Subir Ahora', '', '', '8000013', 'Veh�culos', 'Autos, camionetas y 4x4', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '1600', 'Boleta', '3', 'Emitido'],
                ['172', '2014-12-10', '17:57:04', 'user.01@schibsted.cl', 'Persona', 'Subir Ahora', '', '', '8000080', 'Otros', 'Otros productos', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '900', 'Boleta', '', 'Por emitir'],
                ['173', '2014-12-10', '17:57:28', 'prepaid5@blocket.se', 'Persona', 'Subir Ahora', '', '', '8000073', 'Tiempo libre', 'Libros y revistas', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '900', 'Boleta', '', 'Por emitir']
                ]



        self.assertEqual(headers, expected_headers)
        self.assertEqual(data, expected_data)
