# -*- coding: latin-1 -*-
from yapo.pages.control_panel import WebSQL, ControlPanel
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo.pages.generic import SentEmail


class WebSQLTestAdsInQueue(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'websql')
    def test_ads_in_queue_for_more_than_2_hours(self, wd):
        """ Test to find an ad by category and region """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_queue('Unreviewed')
        cp.review_ad('abuse')
        cp.go()
        cp.go_to_queue('Abuse 2')
        cp.review_ad('accept')

        cp = WebSQL(wd)
        cp.go()
        cp.call_websql('Ads in queue for more than 2 hours')


        search_headers_expected_value = ['Ad Id', 'Locked?', 'Locked By','Queued At']
        search_data_expected_value = [
                ['92', 'True', 'dany', '2013-08-06 15:39:10.346908'],
                ['101', 'False', 'dany', '2014-01-15 10:24:13.496834'],
                ['102', 'False', 'dany', '2014-01-15 10:24:34.202111'],
                ['103', 'False', 'dany', '2014-01-15 10:24:51.635429'],
                ['104', 'False', 'dany', '2014-01-15 10:27:12.653354'],
                ['105', 'False', 'dany', '2014-01-15 10:27:25.727161'],
                ['106', 'False', 'dany', '2014-01-15 10:27:40.171792'],
                ['107', 'False', 'dany', '2014-01-15 10:27:53.778978'],
                ['108', 'False', 'dany', '2014-01-15 10:28:06.882071']
        ]
        table_values = cp.get_table_data('table')
        table_headers = cp.get_result_header()
        self.assertEqual(search_headers_expected_value, table_headers)
        self.assertEqual(search_data_expected_value, table_values[1:])


