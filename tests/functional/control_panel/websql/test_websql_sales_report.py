# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop import HeaderElements, Profile


class WebsqlSalesReportTest(yapo.SeleniumTest):

    maxDiff = None
    header = [
        'Id Pago', 'Fecha', 'Hora', 'Email', 'Tipo Usuario', 'Tipo Producto', 'Id Compra', 'Id Aviso', 'Categoria', 'Region',
        'Estado Pago', 'Tipo Pago', 'Cod. Auth', 'Monto TBK', 'Precio', 'Respuesta TBK', 'Estado Documento', 'Tipo Documento', 'Num. Documento', 'Plataforma de Pago', 'Medio de Pago'
    ]

    def assertData(self, wd, user, filter, data, websql_name='Sales Report', header=None):
        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        websql.call_websql(websql_name, **filter)
        result_data = websql.get_result_data()
        result_header = websql.get_result_header()
        if header is None:
            header = self.header

        # testing
        self.assertEquals(header, result_header)
        self.assertEquals(data, result_data)
        ControlPanel(wd).logout()


class WebsqlSalesReportInAppTest(WebsqlSalesReportTest):

    header = [
        'Id Pago', 'Fecha', 'Hora', 'Email', 'Tipo Usuario', 'Tipo Producto', 'Id Aviso', 'Categoria', 'Region',
        'Estado Pago', 'Precio', 'Moneda', 'Plataforma de Pago', 'Metodo de Pago'
    ]


class WebsqlSalesReportInapp(WebsqlSalesReportInAppTest):

    snaps = ['accounts']
    filter = {
        'timestamp_from': '2016-01-29 00:00:00',
        'timestamp_to':   '2016-01-29 23:59:59',
    }

    data = [
        ['1', u'2016-01-29', u'11:57:54', u'many@ads.cl', u'Persona', u'Subir Ahora', u'8000078',
            u'Otros - Otros productos', u'Regi\xf3n Metropolitana', u'confirmed', u'1.99', u'USD', u'ios', u'appstore'],
        ['2', u'2016-01-29', u'11:58:08', u'many@ads.cl', u'Persona', u'Subir Diario', u'8000077',
            u'Otros - Otros productos', u'Regi\xf3n Metropolitana', u'confirmed', u'6.99', u'USD', u'ios', u'appstore'],
        ['3', u'2016-01-29', u'11:58:23', u'many@ads.cl', u'Persona', u'Subir Semanal', u'8000075',
            u'Otros - Otros productos', u'Regi\xf3n Metropolitana', u'confirmed', u'3.99', u'USD', u'ios', u'appstore']
    ]

    @tags('websql', 'sales', 'report')
    def test_websql_sales_report_inapp(self, wd):
        """Websql Sales Report in app: Headers and data"""
        self.assertData(
            wd, 'dany', self.filter, self.data, 'Sales Report (in-app)')


class WebsqlSalesReportMultiproduct(WebsqlSalesReportTest):

    snaps = ['bumpedads', 'multiproductdiff']
    filter = {
        'timestamp_from': '2013-05-23 00:00:00',
        'timestamp_to':   '2014-03-18 23:59:59',
    }
    data = [
        ['62', u'2013-05-23', u'17:13:52', u'prepaid5@blocket.se', u'Persona', u'Subir Ahora', '3', u'6000666', u'Tiempo libre - Deportes - gimnasia y accesorios',
         u'X Los Lagos', u'paid', u'', u'', u'', u'1000', u'', u'sent', u'bill', u'1', 'unknown', 'webpay'],
        ['63', u'2013-05-23', u'17:14:17', u'uid1@blocket.se', u'Persona', u'Subir Ahora', '4', u'6431717', u'Hogar - Muebles',
         u'XIV Los R\xedos', u'paid', u'', u'', u'', u'1000', u'', u'sent', u'invoice', u'2', 'unknown', 'webpay'],
        ['64', u'2013-05-23', u'17:27:47', u'uid1@blocket.se', u'Persona', u'Subir Ahora', '5', u'6394118', u'Inmuebles - Arriendo',
         u'Regi\xf3n Metropolitana',  u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'3', 'unknown', 'webpay'],
        ['65', u'2013-05-23', u'17:28:16', u'prepaid5@blocket.se', u'Profesional', u'Subir Ahora', '6', u'6000667', u'Inmuebles - Vendo',
         u'I Tarapac\xe1', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'invoice', u'4', 'unknown', 'webpay'],
        ['71', u'2014-03-18', u'18:58:24', u'diego@schibsted.cl', u'Persona', u'Subir Ahora', '7', u'8000003', u'Hogar - Muebles',
         u'Regi\xf3n Metropolitana', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'5', 'unknown', 'oneclick'],
        ['71', u'2014-03-18', u'18:58:24', u'diego@schibsted.cl', u'Persona', u'Subir Ahora', '7', u'8000002', u'Hogar - Muebles',
         u'Regi\xf3n Metropolitana', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'5', 'unknown', 'oneclick'],
        ['71', u'2014-03-18', u'18:58:24', u'diego@schibsted.cl', u'Persona', u'Subir Ahora', '7', u'8000000', u'Hogar - Muebles',
         u'Regi\xf3n Metropolitana', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'5', 'unknown', 'oneclick'],
        ['75', u'2014-03-18', u'18:59:30', u'usuario1@schibsted.cl', u'Persona', u'Subir Ahora', '8', u'8000001', u'Tiempo libre - Animales y sus accesorios',
         u'Regi\xf3n Metropolitana', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'6', 'unknown', 'oneclick'],
        ['75', u'2014-03-18', u'18:59:30', u'usuario1@schibsted.cl', u'Persona', u'Subir Ahora', '8', u'8000004', u'Computadores & electr�nica - Computadores y accesorios',
         u'Regi\xf3n Metropolitana', u'paid', u'', u'', u'', u'1000', u'', u'confirmed', u'bill', u'6', 'unknown', 'oneclick'],
    ]

    @tags('websql', 'sales', 'report')
    def test_websql_sales_report(self, wd):
        """Websql Sales Report: Multiproduct headers and data"""
        self.assertData(wd, 'boris', self.filter, self.data)

class WebsqlSalesReportUpselling(WebsqlSalesReportTest):
    snaps = ['accounts', 'diff_upselling_products']
    filter = {
        'timestamp_from': '2016-07-27 00:00:00',
        'timestamp_to':   '2016-07-27 23:59:59'
    }

    data = [
        [u'168', u'2016-07-27', u'11:52:26', u'user.01@schibsted.cl', u'Persona', u'Est�ndar: Subir ahora', u'2', u'-', u'Otros - Otros productos', u'Regi�n Metropolitana', u'paid', u'', u'', u'', u'900', '', u'pending', u'bill', '', u'desktop', u'webpay' ],
        [u'170', u'2016-07-27', u'11:53:22', u'user.01@schibsted.cl', u'Persona', u'Avanzado: Subir diario + Vitrina 7', u'3', u'-', u'Otros - Otros productos', u'Regi�n Metropolitana', u'paid', u'', u'', u'', u'4500', '', u'pending', u'bill', '', u'desktop', u'webpay' ],
        [u'172', u'2016-07-27', u'11:54:36', u'user.01@schibsted.cl', u'Persona', u'Premium: Subir diario + Vitrina 7 + Etiqueta', u'4', u'-', u'Otros - Otros productos', u'Regi�n Metropolitana', u'paid', u'', u'', u'', u'6490', '', u'pending', u'bill', '', u'desktop', u'webpay' ],
    ]

    @tags('websql', 'sales', 'report')
    def test_websql_sales_report(self, wd):
        """Websql Sales Report: Report with upselling products"""
        self.assertData(
            wd, 'dany', self.filter, self.data)


class WebsqlFinancialSalesReportUpselling(WebsqlSalesReportTest):
    snaps = ['accounts', 'diff_upselling_products']
    header = [
        'Id Pago','Fecha','Hora','Email','Tipo Usuario','Tipo Producto','Total de Bumps','Fecha de T�rmino','Id Aviso','Categor�a padre',
        'Categor�a','Regi�n','Medio de Pago','Estado de Pago','Precio','Tipo Documento','Num. Documento','Estado de Documento'
    ]
    filter = {
        'timestamp_from': '2016-07-27 00:00:00',
        'timestamp_to':   '2016-07-27 23:59:59'
    }
    data = [
        ['168','2016-07-27', '11:52:26', 'user.01@schibsted.cl', 'Persona', 'Est�ndar: Subir ahora', '', '', '-', 'Otros', 'Otros productos', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '900', 'Boleta', '','Por emitir' ],
        ['170','2016-07-27', '11:53:22', 'user.01@schibsted.cl', 'Persona', 'Avanzado: Subir diario + Vitrina 7', '', '',  '-', 'Otros', 'Otros productos', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '4500', 'Boleta', '','Por emitir' ],
        ['172','2016-07-27', '11:54:36', 'user.01@schibsted.cl', 'Persona', 'Premium: Subir diario + Vitrina 7 + Etiqueta', '', '', '-', 'Otros', 'Otros productos', 'Regi�n Metropolitana', 'Webpay', 'Pagado', '6490', 'Boleta', '','Por emitir' ]
    ]

    @tags('websql', 'sales', 'report')
    def test_websql_financial_sales_report(self, wd):
        """Websql Financial Sales Report: Report with upselling products"""
        self.assertData(
            wd, 'dany', self.filter, self.data, 'Financial Sales Report', header=self.header)
