# -*- coding: latin-1 -*-
from yapo.pages.control_panel import WebSQL
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo.pages.generic import SentEmail


class WebSQLTest(SeleniumTest):

    snaps = ['example']

    @tags('controlpanel', 'search', 'region', 'categories')
    def test_check_search_by_category_region(self, wd):
        """ Test to find an ad by category and region """
        cp = WebSQL(wd)
        cp.login('blocket1', 'blocket')
        cp.call_websql('Active ads per category and region')
        search_headers_expected_value = ['Regi�n', 'Categor�a', 'Cantidad']
        search_data_expected_value = ['XV Arica & Parinacota', 'Veh�culos - Autos - camionetas y 4x4', '1']
        table_values = cp.get_table_data('table')
        table_headers = cp.get_result_header()
        self.assertEqual(search_headers_expected_value, table_headers)
        self.assertEqual(search_data_expected_value, table_values[1])


    @tags('controlpanel', 'websql', 'email')
    def test_websql_attachment(self, wd):
        """ Test to send websql result as email attachment """
        wsql = WebSQL(wd)
        wsql.login('blocket1', 'blocket')
        wsql.call_websql('Reviewing stats_Number of ads reviewed per action', recipient='boris@schibsted.cl')

        mailed = wd.find_element_by_css_selector('strong')
        self.assertIn('The result is sent to', mailed.text)
        self.assertTrue(mailed.is_displayed())

        email = SentEmail(wd)
        email.go()
        src = wd.page_source
        self.assertIn('Content-Disposition: attachment; filename="Websql.csv"', src)
        self.assertIn('Hora;Cola;Revisado;Revisor;Acci�n;Status;Raz�n;Categor�a', src)
