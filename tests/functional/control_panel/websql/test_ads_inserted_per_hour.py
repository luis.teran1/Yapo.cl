# -*- coding: latin-1 -*-
import yapo
from yapo.steps.websql import get_websql
from yapo.decorators import tags

class Ads_Inserted_Per_Hour(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_websql_Ads_Inserted_Per_Hour']

    @tags('controlpanel', 'websql')
    def test_results(self, wd):
        """Check result of Ads_Inserted_Per_Hour report"""
        page = get_websql(wd, 'dany', "Ads inserted per hour (new/edit/editrefused)", timestamp_from="2015-04-06 00:00:00", timestamp_to="2015-04-06 23:59:59")
        headers = page.get_result_header()
        data = page.get_result_data()

        expected_headers = ['Date / hour', 'Queue', 'New', 'Edit', 'Edit-refused', 'Renew', 'Total'] 
        expected_data = [
                ['2015-04-06 16:00:00', 'normal', '3', '0', '1', '0', '4'],
                ['2015-04-06 16:00:00', 'abuse', '3', '0', '0', '0', '3'],
                ['2015-04-06 16:00:00', 'edit', '0', '1', '0', '0', '1']
                ]

        self.assertEqual(headers, expected_headers)
        self.assertEqual(data, expected_data)
