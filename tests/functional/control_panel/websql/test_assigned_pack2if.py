# -*- coding: latin-1 -*-
import yapo
import datetime
import calendar
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo.pages.generic import SentEmail
import time


class AssignedPack2ifWebSql(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_assigned_if']

    @tags('websql', 'sales', 'report')
    def test_default(self, wd):
        """ Query with all days """
        page = self._login(wd, "dany")
        result = self._query(
            wd, page, qfrom="2017-01-01 00:00:00", qto="2017-12-31 23:59:59")
        expected = [
            ['137', '8000096', 'Camion sin cuenta', 'VehÝculos - Buses - camiones y furgones', 'prosincuenta@yapo.cl', '123', '123', '2017-07-20 10:28:09.158489', 'blocket'],
            ['138', '8000097', 'Pieza pro inmo', 'Inmuebles - Arriendo', 'cuentaproinmosinpack@yapo.cl', '', '', '2017-07-20 10:28:04.702361', 'blocket'],
            ['136', '8000095', 'Camion sin pack', 'VehÝculos - Buses - camiones y furgones', 'cuentaprosinpack@yapo.cl', '123', '123', '2017-07-20 10:27:23.857295', 'blocket']
        ]
        self.assertEquals(result, expected)

    @tags('websql', 'sales', 'report')
    def test_with_time_spawn(self, wd):
        """Query with less range """
        page = self._login(wd, "dany")
        result = self._query(
                wd, page, qfrom="2017-01-01 00:00:00", qto="2017-07-20 10:27:59")
        expected = [
                ['136', '8000095', 'Camion sin pack', 'VehÝculos - Buses - camiones y furgones', 'cuentaprosinpack@yapo.cl', '123', '123', '2017-07-20 10:27:23.857295', 'blocket']
        ]
        self.assertEquals(result, expected)


    def _login(self, wd, user):
        """ CP login """
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        return websql

    def _query(self, wd, page, thecolums=range(100), qfrom="", qto="", email=""):
        filters = dict()
        if email != "":
            filters['recipient'] = email
        if qfrom != "":
            filters['timestamp_from'] = qfrom
        if qto != "":
            filters['timestamp_to'] = qto
        page.call_websql('Assigned Inserting Fee Auto/Inmo', **filters)
        if email != "":
            return []
        return page.get_result_data(thecolums)
