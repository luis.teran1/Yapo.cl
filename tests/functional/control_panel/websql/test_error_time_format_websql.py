# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo import utils
import yapo
class ErrorTimeFormatWebsql(yapo.SeleniumTest):

    snaps = ['accounts']

    filters = {'timestamp_from':'2014-09-31 00:00:00','timestamp_to':'2014-09-45 23:59:59'}

    @tags('controlpanel', 'websql')
    def test_timestamp_websql(self, wd):
        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Telesales Report', **self.filters)

        # testing
        expected_errors = ['ERROR_TIME_FORMAT' for i in range(2)]
        self.assertListEqual(expected_errors, [i.text for i in websql.errors])
