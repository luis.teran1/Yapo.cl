# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo.pages.control_panel import ControlPanel


class WebSqlAbusesReport(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_sql_abuse']

    filter = {
        'timestamp_from': '2015-10-08 00:00:00',
        'timestamp_to':   '2015-10-09 23:59:59',
    }

    data = [['2015-10-09 17', '00:00', '.00', '1'], ['2015-10-09 17', '00:01', '.00', '1']]

    header = ['Hour', 'Average', 'Percent > 2H', 'Total of ads reviewed']

    @tags('websql', 'abuse', 'report')
    def test_websql_abuses_report(self, wd):
        """Check the sql queries for abuse and abuse 2 revision"""

        websql = control_panel.WebSQL(wd)
        websql.login("dany", "dany")
        websql.call_websql('Average reviewing time in abuse queue', **self.filter)
        result_data = websql.get_result_data()
        result_header = websql.get_result_header()
        print(result_data[0])
        print(result_header)
        # testing
        self.assertEquals(self.header, result_header)
        self.assertEquals(self.data[0], result_data[0])

        websql.call_websql('Average reviewing time in abuse 2 queue', **self.filter)
        result_data = websql.get_result_data()
        result_header = websql.get_result_header()
        print(result_data[0])
        print(result_header)
        # testing
        self.assertEquals(self.header, result_header)
        self.assertEquals(self.data[1], result_data[0])
        ControlPanel(wd).logout()
