# -*- coding: latin-1 -*-
import yapo
import datetime
import calendar
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo.pages.generic import SentEmail
import time


class AssignedPriProWebSql(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_websql_assignpripro']

    @tags('websql', 'sales', 'report')
    def test_default(self, wd):
        """ Query with all days """
        page = self._login(wd, "dany")
        result = self._query(
            wd, page, qfrom="2017-01-01 00:00:00", qto="2017-12-31 23:59:59")
        expected = [
            ['cuentaproconpack@yapo.cl', 'add_pro_category_param', 'is_pro_for',
                '2017-05-23 16:52:50', '2020,2040', '2020,2040,1220,1240', 'dany', 'Dany'],
            ['cuentaproconpack@yapo.cl', 'add_pro_category_param', 'is_pro_for',
             '2017-05-23 16:52:50', '2020,2040,1220,1240', '2020,2040,1220,1240,7020', 'dany', 'Dany'],
            ['many@ads.cl', 'add_pro_category_param', 'is_pro_for',
             '2017-05-23 16:52:22', '', '7020', 'dany', 'Dany'],
            ['prepaid5@blocket.se', 'add_pro_category_param', 'pack_type',
             '2017-05-22 16:52:37', '', 'car', 'dany', 'Dany'],
            ['prepaid5@blocket.se', 'add_pro_category_param', 'is_pro_for',
             '2017-05-22 16:52:37', '', '2020,2040', 'dany', 'Dany'],
            ['prepaid3@blocket.se', 'remove_pro_category_param', 'pack_type',
             '2017-05-21 16:53:14', 'car', '', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'remove_pro_category_param', 'is_pro_for',
             '2017-05-21 16:53:14', '2020,2040', '', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'add_pro_category_param', 'is_pro_for',
             '2017-05-21 16:53:14', '', '1220,1240', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'add_pro_category_param', 'pack_type',
             '2017-05-21 16:53:14', '', 'real_estate', 'blocket', 'Blocket']
        ]

        self.assertEquals(result, expected)

    @tags('websql', 'sales', 'report')
    def test_with_time_spawn(self, wd):
        """Query with less days """
        page = self._login(wd, "dany")
        result = self._query(
            wd, page, qfrom="2017-01-01 00:00:00", qto="2017-05-22 23:59:59")
        expected = [
            ['prepaid5@blocket.se', 'add_pro_category_param', 'pack_type',
                '2017-05-22 16:52:37', '', 'car', 'dany', 'Dany'],
            ['prepaid5@blocket.se', 'add_pro_category_param', 'is_pro_for',
             '2017-05-22 16:52:37', '', '2020,2040', 'dany', 'Dany'],
            ['prepaid3@blocket.se', 'remove_pro_category_param', 'pack_type',
             '2017-05-21 16:53:14', 'car', '', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'remove_pro_category_param', 'is_pro_for',
             '2017-05-21 16:53:14', '2020,2040', '', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'add_pro_category_param', 'is_pro_for',
             '2017-05-21 16:53:14', '', '1220,1240', 'blocket', 'Blocket'],
            ['prepaid3@blocket.se', 'add_pro_category_param', 'pack_type',
             '2017-05-21 16:53:14', '', 'real_estate', 'blocket', 'Blocket']
        ]
        self.assertEquals(result, expected)

    @tags('websql', 'sales', 'report')
    def test_with_to_email(self, wd):
        """ Query results are being sent to email """
        page = self._login(wd, "dany")
        result = self._query(
            wd, page, [0, 2], qfrom="2015-01-21 00:00:00", qto="2999-12-31 23:59:59", email='carlos@lol.com')
        self.assertEquals(result, [])
        sm = SentEmail(wd)
        sm.go()
        innerHTML = wd.find_element_by_tag_name(
            'body').get_attribute('innerHTML')
        self.assertIn('To: carlos@lol.com', innerHTML)
        self.assertEquals(innerHTML.count('\n'), 22)

    def _login(self, wd, user):
        """ CP login """
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        return websql

    def _query(self, wd, page, thecolums=range(100), qfrom="", qto="", email=""):
        filters = dict()
        if email != "":
            filters['recipient'] = email
        if qfrom != "":
            filters['timestamp_from'] = qfrom
        if qto != "":
            filters['timestamp_to'] = qto
        page.call_websql('Registro asignación PRI/PRO', **filters)
        if email != "":
            return []
        return page.get_result_data(thecolums)
