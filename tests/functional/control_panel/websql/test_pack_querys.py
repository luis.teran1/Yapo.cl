# -*- coding: latin-1 -*-
import yapo, datetime, calendar
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo.pages.generic import SentEmail
import time

class PackWebSql(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_websqlpacks']

    @tags('websql', 'sales', 'report')
    def test_default(self, wd):
        """ Test that the default query returns only data from the last 24 hours """
        page = self._login(wd, "dany")
        result = self._query(wd, page, [0,2,5,6,7])
        self.assertEquals(result,
            [['cuentaprosincupos@yapo.cl', '50', '-', '0','INMO'],
            ['cuentaprosincupos@yapo.cl', '50', '1003', '5600','INMO'],
            ['cuentaprosincupos@yapo.cl', '50', '1002', '4600','INMO'],
            ['cuentaprosincupos@yapo.cl', '50', '-', '0','AUTO'],
            ['cuentaprosincupos@yapo.cl', '50', '0003', '5600','AUTO'],
            ['cuentaprosincupos@yapo.cl', '50', '0002', '4600','AUTO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','INMO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','INMO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','INMO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','AUTO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','AUTO'],
            ['cuentaproconpack@yapo.cl',  '50', '-','0','AUTO']])

    @tags('websql', 'sales', 'report')
    def test_with_time_spawn(self, wd):
        """ Test that the time spawn parameters are working """ 
        page = self._login(wd, "dany")
        result = self._query(wd, page, [0,2], qfrom="2015-01-21 00:00:00")
        self.assertEquals(result, 
            [['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '50'],
            ['cuentaprosincupos@yapo.cl', '10'],
            ['cuentaprosincupos@yapo.cl', '10'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '50'],
            ['cuentaproconpack@yapo.cl', '10'],
            ['cuentaproconpack@yapo.cl', '10']])

    @tags('websql', 'sales', 'report')
    def test_with_to_email(self, wd):
        """ Test that the results are being sent to email """ 
        page = self._login(wd, "dany")
        result = self._query(wd, page, [0,2], qfrom="2015-01-21 00:00:00", qto="2999-12-31 23:59:59", email='carlos@lol.com')
        self.assertEquals(result, [])
        sm = SentEmail(wd)
        sm.go()
        innerHTML = wd.find_element_by_tag_name('body').get_attribute('innerHTML')
        self.assertIn('To: carlos@lol.com', innerHTML)
        self.assertEquals(innerHTML.count('\n'), 33)

    def _login(self, wd, user):
        """ CP login """
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        return websql

    def _query(self, wd, page, thecolums=range(100), qfrom="", qto="", email=""):
        filters = dict()
        if email != "":
            filters['recipient'] = email
        if qfrom != "":
            filters['timestamp_from'] = qfrom
        if qto != "":
            filters['timestamp_to'] = qto
        page.call_websql('Control Panel asigned Packs', **filters)
        if email != "":
            return []
        return page.get_result_data(thecolums)
        
