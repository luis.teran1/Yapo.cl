# -*- coding: latin-1 -*-
import yapo, datetime, calendar
from yapo.decorators import tags
from yapo.pages import control_panel

class WebsqlStatsTest(yapo.SeleniumTest):

    # control variable
    counted_days = 0

    def assertData(self, wd, user, sql_name, thenumber, col, iters):
        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        websql.call_websql(sql_name)
        result_data = websql.get_result_data()
        for i in range(iters):
            self.assertEquals(result_data[i][col], thenumber)

    def get_count_per_month(self, Y, M):
        date = datetime.datetime.today()
        today = date.day
        month = date.month
        dim = calendar.monthrange(int(Y), int(M))[1]
        if int(M) == month:
            self.counted_days = self.counted_days + today
            return today*12
        elif self.counted_days + dim < 63:
            self.counted_days = self.counted_days + dim
            return dim * 12
        else:
            ret = (63-self.counted_days)*12
            self.counted_days = self.counted_days + dim
            return ret

    def get_count_per_week(self):
        date = datetime.datetime.today()
        wday = date.weekday() + 1
        if self.counted_days == 0:
            self.counted_days = self.counted_days + wday
            return wday*12
        elif self.counted_days + 7 < 63:
            self.counted_days = self.counted_days + 7
            return 7 * 12
        else:
            ret = (63-self.counted_days)*12
            self.counted_days = self.counted_days + 7
            return ret

    def assertDataMonthly(self, wd, user, sql_name, col):
        # going to the controlpanel to get the timestamps
        self.counted_days = 0
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        websql.call_websql(sql_name)
        result_data = websql.get_result_data()
        i = 0
        while self.counted_days < 63:
            formatdate = result_data[i][0].split(' ')[0].split('-')
            thenumber = self.get_count_per_month(formatdate[0], formatdate[1])
            self.assertEquals(result_data[i][col], str(thenumber))
            i += 1

    def assertDataWeekly(self, wd, user, sql_name, col):
        # going to the controlpanel to get the timestamps
        self.counted_days = 0
        websql = control_panel.WebSQL(wd)
        websql.login(user, user)
        websql.call_websql(sql_name)
        result_data = websql.get_result_data()
        i = 0
        while self.counted_days < 63:
            thenumber = self.get_count_per_week()
            self.assertEquals(result_data[i][col], str(thenumber))
            i += 1

class WebsqlStatsUserType(WebsqlStatsTest):

    snaps = ['accounts', 'diff_addpaylogs']

    @tags('websql', 'sales', 'report')
    def test_01_verified_until_this_minute_by_weeday(self, wd):
        """ FT1182 test: websql query number 6 does not count public scheme twice"""
        self.assertData(wd, 'dany', 'Verified ads stats_Ads verified until this minute by weekday', '12', 2, 63)

    @tags('websql', 'sales', 'report')
    def test_02_verified_until_this_minute(self, wd):
        """ FT1182 test: websql query number 7 does not count public scheme twice"""
        self.assertData(wd, 'dany', 'Verified ads stats_Ads verified until this minute (last 32 days)', '12', 1, 63)

    @tags('websql', 'sales', 'report')
    def test_03_verified_until_this_minute_same_weekday(self, wd):
        """ FT1182 test: websql query number 8 does not count public scheme twice"""
        self.assertData(wd, 'dany', 'Verified ads stats_Ads verified until this minute on same weekday', '12', 1, 9)

    @tags('websql', 'sales', 'report')
    def test_04_verified_per_day(self, wd):
        """ FT1182 test: websql query number 9 does not count public scheme twice"""
        self.assertData(wd, 'dany', 'Verified ads stats_Ads verified (split new/prolong/total) per day(last 32 days)', '12', 3, 33)

    @tags('websql', 'sales', 'report')
    def test_05_verified_per_day_by_weekday(self, wd):
        """ FT1182 test: websql query number 10 does not count public scheme twice"""
        self.assertData(wd, 'dany', 'Verified ads stats_Ads verified (split new/prolong/total) per day by weekday (last 32 days)', '12', 4, 33)

    @tags('websql', 'sales', 'report')
    def test_06_verified_per_month(self, wd):
        """ FT1182 test: websql query number 11 does not count public scheme twice"""
        self.assertDataMonthly(wd, 'dany', 'Verified ads stats_Ads verified per month (last 13 days)', 1)
    
    @tags('websql', 'sales', 'report')
    def test_07_verified_per_week(self, wd):
        """ FT1182 test: websql query number 12 does not count public scheme twice"""
        self.assertDataWeekly(wd, 'dany', 'Verified ads stats_Ads verified per week (last 52 weeks)', 2)

