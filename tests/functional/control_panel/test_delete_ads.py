# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelMassiveActions
from yapo.decorators import tags
from yapo import utils, SeleniumTest
import time


class ControlpanelDeleteAds(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'ads', 'core3')
    def test_cp_delete_ads(self, wd):
        """ CP Delete Ads, check delete published ads by email """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu("Search for ad")
        previous_test_id = [
            'AdInsertCoreDesktop.test_insert_job_ad',
            'AdInsertCoreMobile.test_insert_job_ad',
            'AdInsertCoreDesktop.test_insert_with_map_20_images',
            'AdInsertCoreMobile.test_insert_with_map_20_images',
            'AdInsertCoreDesktop.test_insert_with_map_6_images',
            'AdInsertCoreMobile.test_insert_with_map_6_images',
            'AdInsertTest.test_ad_insert_6_cars_for_pack_auto',
            'AccountLoginTest.test_create_account_by_ad_insert',
            'AdInsertTest.test_insert_cell_phone_ad_by_preview'
        ]
        if utils.is_not_regress():
            emails = utils.JsonTool.read_tests_emails(previous_test_id)
        else:
            emails = ['uid1@blocket.se']
        for email in emails:
            print('Deleting ads for email:'+email)
            cp.search_for_ad(email)
            cp_actions = ControlPanelMassiveActions(wd)
            if cp_actions.is_element_present('check_all'):
                cp_actions.check_all.click()
                cp_actions.button_delete.click()
                if not utils.is_not_regress():
                    expected_data = [
                        'Executing batch delete:',
                        'Deleted ad 29.',
                        'Deleted ad 28.',
                        'Deleted ad 25.',
                        'Deleted ad 22.',
                        'Deleted ad 24.',
                        'Deleted ad 21.',
                        'Deleted ad 12.',
                        'Deleted ad 11.',
                        'Deleted ad 8.'
                    ]
                    self.assertListEqual(expected_data, [i.text for i in cp_actions.result_list])
