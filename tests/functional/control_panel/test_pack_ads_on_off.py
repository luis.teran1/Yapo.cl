# -*- coding: latin-1 -*-
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControPanelServicesModule
import yapo


class PackAdsOnOff(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_disabled']
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "0"}

    passwd = "123123123"
    category = 2020

    def _login_cp_search(self, wd, ad_id):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.search_for_ad_by_ad_id(ad_id)
        return cp

    def _get_link_text(self, new_status):
        if new_status == "active":
            return "Activate w/Pack"
        return "Disable w/Pack"

    def _scenario(self, wd, ad_id, new_status):
        cp = self._login_cp_search(wd, ad_id)

        link = self._get_link_text(new_status);

        cp.table_on_search.find_element_by_link_text(link).click()

        wd.switch_to_alert().accept()
        cp.wait.until(lambda wd: len(wd.window_handles) != 1)
        wd.switch_to_window(wd.window_handles[1])

        srv_mdl = ControPanelServicesModule(wd)
        srv_mdl.wait.until_loaded()
        self.assertEquals(srv_mdl.message_success.text, 'Tu aviso fue promovido')

        success_message = "Nuevo estado del aviso: " + new_status

        self.assertEquals(srv_mdl.success_detail.text, success_message)
        cp.back_to_cp()
        cp.logout()

    def test_disable_w_slots(self, wd):
        # Cuenta pro sin cupos
        self._scenario(wd, 133, "disabled")
        self._scenario(wd, 133, "active")

    def test_activate_w_slots(self, wd):
        self._scenario(wd, 134, "active")

    def _scenario_not_present(self, wd, ad_id, subject, new_status):
        cp = self._login_cp_search(wd, ad_id)
        link = self._get_link_text(new_status)
        cp.wait.until_loaded()
        self.assertFalse(link in wd.page_source)

    def test_link_not_present_active_no_pack(self, wd):
        ad_id = 132
        subject = "Cadillac Calais 1965"
        new_status = "disabled"
        self._scenario_not_present(wd, ad_id, subject, new_status)

    def test_link_not_present_disabled_no_pack(self, wd):
        ad_id = 136
        subject = "Camion sin pack"
        new_status = "active"
        self._scenario_not_present(wd, ad_id, subject, new_status)

    def test_err_active_no_slots(self, wd):
        ad_id = 135
        subject = "Camion sin cupo"
        new_status = "active"

        cp = self._login_cp_search(wd, ad_id)
        link = self._get_link_text(new_status);
        cp.table_on_search.find_element_by_link_text(link).click()

        wd.switch_to_alert().accept()
        cp.wait.until(lambda wd: len(wd.window_handles) != 1)
        wd.switch_to_window(wd.window_handles[1])

        srv_mdl = ControPanelServicesModule(wd)
        self.assertEquals(srv_mdl.message_error.text, 'Error al habilitar aviso: No hay cupos disponibles')
