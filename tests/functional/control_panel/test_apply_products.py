# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.desktop_list import AdViewDesktop, AdListDesktop, Galleries, GalleryAd
from yapo.pages.control_panel import ControlPanel, ControlPanelSearchForAd
from yapo.utils import Products, Label_texts, Label_values, Upselling
import yapo, time
import yapo.conf

def _apply_product_cp(test, wd, data, old_options=False):
    cp = ControlPanel(wd)
    cp.go()
    cp.login("blocket", "blocket")
    cp.go_to_option_in_menu("Search for ad")
    if old_options:
        cp.change_default_options()

    cp_search = ControlPanelSearchForAd(wd)
    cp_search.fill_form(search_field=data['email'], radio_all=True)
    cp_search.button_search.click()
    cp_search.apply_product_by_ad_id(
        ad_id=data['ad_id'],
        prod=data['prod'],
        prod_type=data.get('prod_type'),
        ab_frequency=data.get('ab_frequency'),
        ab_num_days=data.get('ab_num_days'),
        )

    cp.wait.until(lambda x: len(wd.window_handles) > 1)
    wd.switch_to_window(wd.window_handles[1])
    if 'success_message' in data:
        test.assertEqual(
            cp_search.message_success.text, data['success_message'])
    else:
        test.assertEqual(
            cp_search.message_error.text, data['error_message'])
    wd.close()
    wd.switch_to_window(wd.window_handles[0])
    cp.logout()


class CpanelApplyProductsAlreadyApplied(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    congrats = "Tu compra se realizó exitosamente"
    account_pass = "123123123"
    platform = mobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def _check_dashboard_not_selected_prod(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    product = prod['prod']
                    self.assertFalse(
                        active.is_selected(product, prod['param']))

    def _select_product(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    product = prod['prod']
                    if not prod['is_upselling']:
                        active.select_product(product, prod['param'])
                    else:
                        active.edit_button.click()
                        edit_page = mobile.NewAdEdit(wd)
                        edit_page.fill('want_bump', True);
                        edit_page.fill('body', 'lalalala');
                        edit_page.fill('want_bump', False);
                        edit_page.submit.click()
                        upselling = AdInsertResult(wd)
                        upselling.buy_upselling_label.click()

                        label_value = prod['param']
                        if label_value == Label_values.URGENT:
                            upselling.label_upselling_urgent.click()
                        if label_value == Label_values.NEW:
                            upselling.label_upselling_new.click()
                        if label_value == Label_values.LITTLE_USE:
                            upselling.label_upselling_little_use.click()
                        if label_value == Label_values.OPPORTUNITY:
                            upselling.label_upselling_opportunity.click()

    def _buy_products(self, wd, products):

        summary = self.platform.SummaryPayment(wd)
        summary.go_summary()
        summary.press_link_to_pay_products()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)

        # Run rebuild-index apply product
        utils.rebuild_index()

    @tags('accounts', 'dashboard', 'products', 'label')
    def test_select_product_dashboard_cp_ok(self, wd):
        """ Select a Label by dashboard, when CP apply product
            the prod is removed from cart"""
        lid = '8000065'
        user = "prepaid5@blocket.se"
        data_products = {
            lid: {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'is_upselling': False

            }
        }

        login = self.platform.Login(wd)
        login.login(user, self.account_pass)
        self._select_product(wd, data_products)
        data = {
            'email': user,
            'ad_id': '86',
            'prod': 'label',
            'prod_type': 'urgent',
            'success_message': 'Tu aviso fue promovido'
        }
        _apply_product_cp(self, wd, data)

        self._check_dashboard_not_selected_prod(wd, data_products)


class CpanelApplyProducts(yapo.SeleniumTest):

    snaps = ['accounts']
    account_pass = "123123123"
    congrats = "ˇFelicidades!"
    AdView = AdViewDesktop

    @tags('controlpanel', 'products', 'bump')
    def test_apply_bump_from_cp(self, wd):
        """ Search and ad by list id, and apply a bump"""
        user = "prepaid5@blocket.se"
        list_id = '8000065'
        data = {
            'email': user,
            'ad_id': '86',
            'prod': 'bump',
            'success_message': 'Tu aviso fue promovido'
        }
        _apply_product_cp(self, wd, data)
        utils.rebuild_index()
        d = utils.get_date_and_time()
        fecha_actual = str(d['fecha'].day) + ' '+ d['mes'] + ' ' + d['h']
        ad_view_page = AdViewDesktop(wd)
        ad_view_page.go(list_id)
        fecha = ad_view_page.get_date_ad
        self.assertIn(fecha_actual, fecha)


    @tags('controlpanel', 'products', 'weekly_bump')
    def test_apply_weekly_bump_from_cp(self, wd):
        """ Search and ad by list id, and apply a weekly bump"""
        user = "prepaid5@blocket.se"
        list_id = '8000073'
        data = {
            'email': user,
            'ad_id': '95',
            'prod': 'weekly',
            'success_message': 'Tu aviso fue promovido'
        }
        _apply_product_cp(self, wd, data)
        utils.rebuild_index()
        d = utils.get_date_and_time()
        fecha_actual = str(d['fecha'].day) + ' '+ d['mes'] + ' ' + d['h']
        ad_view_page = AdViewDesktop(wd)
        ad_view_page.go(list_id)
        fecha = ad_view_page.get_date_ad
        self.assertIn(fecha_actual, fecha)
        #Wait the interval beetwen 2 weekly bumps to check
        # the changes on the minute of the ad
        time.sleep(60)
        utils.rebuild_index()
        ad_view_page = AdViewDesktop(wd)
        ad_view_page.go(list_id)
        fecha_actual = ad_view_page.get_date_ad
        # Assert the diferents times
        self.assertFalse(fecha  == fecha_actual)
        fecha  = fecha_actual
        time.sleep(60)
        utils.rebuild_index()
        ad_view_page = AdViewDesktop(wd)
        ad_view_page.go(list_id)
        fecha_actual = ad_view_page.get_date_ad
        # Assert the diferents times
        self.assertFalse(fecha  == fecha_actual)
        fecha  = fecha_actual
        time.sleep(60)
        utils.rebuild_index()
        ad_view_page = AdViewDesktop(wd)
        ad_view_page.go(list_id)
        fecha_actual = ad_view_page.get_date_ad
        # Assert the diferents times
        self.assertFalse(fecha  == fecha_actual)

    @tags('controlpanel', 'products', 'gallery')
    def test_apply_gallery_from_cp(self, wd):
        """ Search and ad by list id, and apply a gallery"""
        user = "prepaid5@blocket.se"
        list_id = '3456789'
        ad_id = '6'
        data = {
            'email': user,
            'ad_id': '6',
            'prod': 'gallery',
            'success_message': 'Tu aviso fue promovido'
        }
        _apply_product_cp(self, wd, data)
        utils.rebuild_index()
        listing = AdListDesktop(wd)
        listing.go('araucania')
        galleries = Galleries(wd).ad_galleries
        self.assertTrue(len(galleries) == 2)
        self.assertEquals(galleries[0].subject.text, 'Race car')

    @tags('controlpanel', 'products', 'gallery')
    def test_apply_autobump_from_cp(self, wd):
        """ Search and ad by list id, and apply a autobump"""
        user = "prepaid5@blocket.se"
        list_id = '3456789'
        ad_id = '6'
        data = {
            'email': user,
            'ad_id': '6',
            'prod': 'autobump',
            'ab_frequency': '1',
            'ab_num_days': '1',
            'success_message': 'Tu aviso fue promovido'
        }
        _apply_product_cp(self, wd, data)

