from yapo.pages.control_panel import ControlPanel, ControlPanelSearchByReviewer
from yapo.decorators import tags
from yapo import SeleniumTest
from selenium.webdriver.support.select import Select


class CpSearchByReviewer(SeleniumTest):

    snaps = ['example']

    @tags('controlpanel', 'search', 'reviewer')
    def test_check_search_by_reviewer(self, wd):
        """ Test to find an ad by reviewer"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket1", "blocket")
        cp.go_to_option_in_menu("Search by reviewer")
        cpr = ControlPanelSearchByReviewer(wd)
        select_reviewer = Select(cpr.reviewer)
        select_reviewer.select_by_visible_text("Blocket admin")
        select_state = Select(cpr.ad_state)
        select_state.select_by_visible_text("Refused then admin approved")
        cpr.search_button.click()
        self.assertTrue(wd.find_element_by_partial_link_text("Ad 23, Refused, but accepted by admin").is_displayed())
        select_state = Select(cpr.ad_state)
        select_state.select_by_visible_text("Approved then admin refused")
        cpr.search_button.click()
        self.assertTrue(wd.find_element_by_partial_link_text("Ad 24, Accepted, but refused by admin").is_displayed())

