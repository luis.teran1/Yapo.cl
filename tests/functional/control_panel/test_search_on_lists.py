# -*- coding: latin-1 -*-
import yapo, time
from selenium.common.exceptions import NoSuchElementException
from yapo.pages.control_panel import ControlPanel, ControlPanelWhitelist

class Lists(yapo.SeleniumTest):

    #snaps = ["example"]
    snaps = ["example"]

    def test_search_list_empty(self, wd):
        """ Test the search when no input """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket1", "blocket")
        cp.go_to_option_in_menu("Lists")
        wd.find_element_by_partial_link_text("Tipsmottagare").click()
        cp = ControlPanelWhitelist(wd)
        cp.search.click()
        wd.find_element_by_partial_link_text("ful@fisk.se").click()

    def test_search_list_email(self, wd):
        """ Test the search with an email """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket1", "blocket")
        cp.go_to_option_in_menu("Lists")
        wd.find_element_by_partial_link_text("Tipsmottagare").click()
        cp = ControlPanelWhitelist(wd)
        wd.find_element_by_css_selector("#q").send_keys("ful")
        cp.search.click()
        wd.find_element_by_partial_link_text("ful@fisk.se").click()

    def test_create_category_list(self, wd):
        """ tests creating a category list """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket1", "blocket")
        cp.go_to_option_in_menu("Lists")
        wd.find_element_by_partial_link_text("New list").click()
        wd.find_element_by_css_selector('input[name=list_name]').send_keys('New Category List')
        wd.find_element_by_id('category_button').click()
        wd.find_element_by_css_selector('input[type="submit"]').click()
        wd.find_element_by_partial_link_text("New Category List").click()
        wd.find_element_by_id('2000_2060').click()
        wd.find_element_by_css_selector('input[type="submit"]').click()
        try:
            wd.find_element_by_id('q').click()
            self.assertTrue(False)
        except NoSuchElementException:
            self.assertTrue(True)

    def test_create_country_list(self, wd):
        """ tests creating a country list """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket1", "blocket")
        cp.go_to_option_in_menu("Lists")
        wd.find_element_by_partial_link_text("New list").click()
        wd.find_element_by_css_selector('input[name=list_name]').send_keys('New Country List')
        wd.find_element_by_id('country_button').click()
        wd.find_element_by_css_selector('input[type="submit"]').click()
        wd.find_element_by_partial_link_text("New Country List").click()
        wd.find_element_by_id('chk11_14').click()
        wd.find_element_by_css_selector('input[type="submit"]').click()
        try:
            wd.find_element_by_id('q').click()
            self.assertTrue(False)
        except NoSuchElementException:
            self.assertTrue(True)


