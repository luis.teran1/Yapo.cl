# -*- coding: latin-1 -*-
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import desktop_list
from yapo.pages import mobile_list
from yapo.pages.control_panel import ControlPanel, ControlPanelBanners
from yapo.pages.logs import BannersAdminLog
from yapo.pages.desktop import Dashboard
from yapo.pages.desktop_list import AdDeletePage, AdViewDesktop, AdListDesktop
from yapo.pages.generic import AccountBar
from yapo.pages.mobile import HeaderElements
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.steps.accounts import login_and_go_to_dashboard
import time
import urllib.request
import yapo


class CpanelHandler(yapo.SeleniumTest):

    def open_cp(self,wd,user='dany', passwd='dany'):
        cp = ControlPanel(wd)
        cp.go()
        if cp.check_login():
            cp.login(user, passwd)
        cp.go_to_option_in_menu("Administrar Banners")
        return ControlPanelBanners(wd)


class CotizaDesktop(CpanelHandler):

    snaps = ['android']
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('desktop', 'adview')
    def test_comparaonline_no_more(self, wd):
        """ test that the 'compara online' link is no longer shown """
        adview = desktop_list.AdViewDesktop(wd)
        adview.go('8000066')
        self.assertRaises(exceptions.NoSuchElementException, lambda: adview.compara_online_link)

    def _are_banners_in(self, wd, list_id, link='seguro', banner_name=''):
        """ HELPER FUN """
        adview = desktop_list.AdViewDesktop(wd)
        adview.go(list_id)
        if link == 'seguro':
            adview.cotiza_seguro_link.click()
        else:
            adview.cotiza_credito_link.click()
        self.assertTrue(len(adview.cotiza_banners) > 0)
        banners = len(adview.cotiza_banners)
        if banner_name:
            banners = 0
            for banner in adview.cotiza_banners:
                if banner_name in banner.get_attribute('href'):
                    banners += 1
        return banners


class CotizaMobile(CpanelHandler):
    snaps = ['android']
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('mobile', 'adview')
    def test_mobile_comparaonline_no_more(self, wd):
        """ Test that the 'compara online' link is no longer shown """
        adview = mobile_list.AdViewMobile(wd)
        adview.go('8000066')
        self.assertRaises(exceptions.NoSuchElementException, lambda: adview.compara_online_link)

    def _are_banners_in_mobile(self, wd, list_id, link='seguro', banner_name=''):
        """ HELPER FUN """
        adview = mobile_list.AdViewMobile(wd)
        adview.go(list_id)
        if link == 'seguro':
            adview.cotiza_seguro_link.click()
        else:
            adview.cotiza_credito_link.click()
        self.assertTrue(len(adview.cotiza_banners) > 0)
        banners = len(adview.cotiza_banners)
        if banner_name:
            banners = 0
            for banner in adview.cotiza_banners:
                if banner_name in banner.get_attribute('href'):
                    banners += 1
            self.assertTrue(banners > 0)
        return banners
