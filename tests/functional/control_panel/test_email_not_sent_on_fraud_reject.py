# coding: latin-1
import yapo, time
from yapo.pages.control_panel import ControlPanel, ControlPanelNewRefusalReasons
from yapo.pages.ad_insert import AdInsert
from yapo.pages.generic import SentEmail

class RejectionByFraudEmail(yapo.SeleniumTest):

    snaps = ['accounts']
    ad_data = {
        'subject':'Aviso pa probar',
        'region':'15',
        'communes':'322',
        'category':'5020',
        'body':'yoLo',
        'price':'3500000',
        'password':'123123123',
        'password_verification':'123123123',
        'phone':'999966666',
        'accept_conditions':True,
        'private_ad':'1',
        'name':'Carlos',
        'email':'carlos@schibsted.cl',
        'email_verification':'carlos@schibsted.cl',
        'create_account':False
    }

    def test_email_not_sent_on_reject_by_fraud(self, wd):
        """ Test that not email is sent when an ad is rejected by fraud """
        """ add the "fraude" option """
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login("dany", "dany")
        new_reason_page = ControlPanelNewRefusalReasons(wd)
        new_reason_page.go()
        new_reason_page.add_refusal_reason() # < this method has default values
        time.sleep(3)
        cp = ControlPanel(wd)
        cp.logout()
        """ insert ad to refuse """
        ad_insert = AdInsert(wd)
        ad_insert.go()
        ad_insert.insert_ad(**self.ad_data)
        ad_insert.submit.click()
        time.sleep(3)
        """ check the email """
        sent_email = SentEmail(wd)
        sent_email.go()
        time.sleep(1)
        """ Refuse ad for fraud """
        cp = ControlPanel(wd)
        cp.go()
        cp.login('dany', 'dany')
        cp.search_for_ad_by_subject("Aviso pa probar")
        wd.find_element_by_link_text('Aviso pa probar').click()
        wd.switch_to_window(wd.window_handles[-1])
        cp.review_ad(action='refuse', refusal_reason_name='Fraude')
        """ the email should not be there """
        sent_email = SentEmail(wd)
        sent_email.go()
        self.assertIn('No such file or directory', wd.find_element_by_tag_name('body').text)
