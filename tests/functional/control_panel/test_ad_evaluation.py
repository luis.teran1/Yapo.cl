# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult, AdInsertSuccess
from yapo.pages.ad_insert import AdInsertDuplicatedAd
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop import DashboardMyAds
from yapo import utils
import yapo


class AdInsertAdEvaluationPreQueue(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop

    snaps = ['accounts']

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject': 'codice marinos del espacio',
           }

    isdesktop = True

    def _clean_queue(self, queue, wd):
        #Accept all the ads to have a clean queue
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.walk_reviewing_queue(queue, 11)
        cp.logout()
        utils.rebuild_index()

    def _unreviewed_accept(self, wd):
        # REVIEW ON CP QUEUES
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        self.assertTrue(cp.queue_has_elements('Unreviewed', 1))
        cp.go_to_queue('Unreviewed')
        self.assertEqual(cp.count_reviews_in_page(), 1)
        cp.review_ad("accept")
        cp.logout()
        utils.rebuild_index()

    def _check_no_pending_reviews(self, wd):
        # check there is no pending reviews
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        self.assertTrue(cp.queue_has_elements('Unreviewed', 0))
        cp.go_to_queue('Unreviewed')
        self.assertEquals(cp.error.text, "Fila vac�a para su revisi�n")
        cp.go()
        cp.go_to_no_lock_queue('Unreviewed')
        self.assertEquals(cp.error.text, "Fila vac�a para su revisi�n")

    def _insert_ad_logged(self, wd):
        ad_insert = self.AdInsert(wd)
        ad_insert.go()

        self.assertEqual(
            ad_insert.name.get_attribute('value'), self.user['name'])
        self.assertEqual(
            ad_insert.email.get_attribute('value'), "prepaid5@blocket.se")

        ad_insert.insert_ad(subject=self.user['subject'],
                            body="for teh emporer!",
                            region=15,
                            communes=331,
                            category=6020,
                            price=778889,
                            accept_conditions=True)
        ad_insert.submit.click()

    @tags('ad_insert', 'desktop', 'account')
    def test_ad_insert_review_queue(self, wd):
        """
        Ad evaluation review CP Queue:
        Insert and ad  - With an user logged in desktop
        Try to insert an ad, receive the ad evaluation from
        an external evaluation service (i.e. Implio), review the
        ad only once, check that there are no pending ad states to be reviewed
        ad check the ad is published
        """

        self._clean_queue('Unreviewed', wd)

       # Insert a new ad
        login_and_go_to_dashboard(wd, self.user['email'], self.user['passwd'], from_desktop=self.isdesktop)

        self._insert_ad_logged(wd)
        result = AdInsertSuccess(wd)
        self.assertIn(
            "Tu aviso Codice marinos del espacio ser� revisado y si est� de "
            "acuerdo con las reglas de Yapo.cl, lo publicaremos en los pr�ximos "
            "minutos.", result.success_message.text)

        # set the ad evaluation
        actual_trans = utils.trans('set_ad_evaluation', ad_id='126', action_id='1',evaluation='approved',reason='Approved by ML')
        expected_trans = {
            'status': 'TRANS_OK'}
        self.assertTrans(actual_trans, expected_trans)

        self._unreviewed_accept(wd)

        #check the add is published
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Codice marinos del espacio'), 'Aviso publicado')

        self._check_no_pending_reviews(wd)


class AdInsertAdEvaluationPostQueue(AdInsertAdEvaluationPreQueue):

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject': 'libro polvoriento conservado',
           }

    @tags('ad_insert', 'desktop', 'account')
    def test_ad_insert_review_queue(self, wd):
        """
        Review CP Queue ad evaluation:
        Insert and ad  - With an user logged in desktop
        Try to insert an ad, review the ad only once,
        receive the ad evaluation from
        an external evaluation service (i.e. Implio), and
        check that there are no pending ad states to be reviewed
        ad check the ad is published
        """

        self._clean_queue('Unreviewed', wd)

       # Insert a new ad
        login_and_go_to_dashboard(wd, self.user['email'], self.user['passwd'], from_desktop=self.isdesktop)

        self._insert_ad_logged(wd)
        result = AdInsertSuccess(wd)
        self.assertIn(
            "Tu aviso Libro polvoriento conservado ser� revisado y si est� de "
            "acuerdo con las reglas de Yapo.cl, lo publicaremos en los pr�ximos "
            "minutos.", result.success_message.text)

        self._unreviewed_accept(wd)

        #check the add is published
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Libro polvoriento conservado'), 'Aviso publicado')

        # set the ad evaluation
        actual_trans = utils.trans('set_ad_evaluation', ad_id='126', action_id='1',evaluation='approved',reason='Approved by ML')
        expected_trans = {
            'status': 'TRANS_OK'}
        self.assertTrans(actual_trans, expected_trans)

        self._check_no_pending_reviews(wd)
