# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelTelesales
from yapo.pages.simulator import Simulator
from yapo.pages.stores import Detail
from yapo import utils
import yapo
import yapo.conf


class TelesalesStoreEdit(yapo.SeleniumTest):

    snaps = ['accounts', 'singlestore']
    bconfs = {'*.*.payment.telesales.webpay_plus.enabled': '0'}

    @tags('controlpanel', 'stores', 'telesales')
    def test_edit_store_active(self, wd):
        """Check telesales edit store status active"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")

        cp_telesales = ControlPanelTelesales(wd)
        cp_telesales.fill_form(email_search='many@ads.cl')
        cp_telesales.button_search.click()
        cp_telesales.go_store_edition()
        cp_telesales.fill_form(store_name="Tienda ManyAds", store_description="nueva descripcion tienda manyads", store_region="15")
        cp_telesales.button_save.click()

        # check errors
        cp_telesales.fill_form(store_name="zorruna", store_description="super mega hiper maraca", store_region="")
        cp_telesales.button_save.click()
        self.assertEqual(cp_telesales.store_name_error.text, "Has introducido palabras no permitidas en el nombre de tu tienda: \"zorruna\"")
        self.assertEqual(cp_telesales.store_description_error.text, "Has introducido palabras no permitidas en la descripci�n: \"maraca\"")
        self.assertEqual(cp_telesales.store_region_error.text, "Selecciona una regi�n")

        cp_telesales.back_to_cp()
        cp.logout()

        utils.rebuild_csearch()

        detail = Detail(wd)
        detail.go("tienda-manyads")

        store_view_data = {
            'name': "Tienda ManyAds",
            'description': "nueva descripcion tienda manyads",
            'phone': "666666666",
            'address': "Calle yolanda sultana 1313, Alhu�",
            'site': "www.fuerzasdemoniacas.com",
        }
        self.assertEqual(detail.get_data(), store_view_data)

    @tags('controlpanel', 'stores', 'telesales')
    def test_store_edit_link_isnot_present(self, wd):
        """test store edit link isnot_present"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Vender Productos")
        cp_telesales = ControlPanelTelesales(wd)
        # this is an acount whitout store
        cp_telesales.fill_form(email_search='user.01@schibsted.cl')
        cp_telesales.button_search.click()
        cp_telesales.wait.until_not_present('link_edit_store')
        # this valid the not present of the link edit store from a pending account
        cp_telesales.fill_form(email_search='acc_pendind@yapo.cl')
        cp_telesales.button_search.click()
        cp_telesales.wait.until_not_present('link_edit_store')
        # this valid the not present of the link edit store from a inactive account
        cp_telesales.fill_form(email_search='acc_inactive@yapo.cl')
        cp_telesales.button_search.click()
        cp_telesales.wait.until_not_present('link_edit_store')
