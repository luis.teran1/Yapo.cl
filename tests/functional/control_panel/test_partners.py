# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanelPartners, ControlPanelPartnerAdd
from yapo.pages.desktop_list import AdViewDesktop
from yapo import utils
import yapo, time

class TestPartners(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_partners']

    data = {
        'name': "PartnerTest",
        'fileName': "yapo_partner.xls",
        'insDelay': "10",
        'mailTo': "partner@yapo.cl",
        'keyName': "partner",
    }

    basic_data = {
        'pathImg': "./images/,"
    }

    advanced_data = {
        'rules': "{}",
        'iRules': "{}"
    }

    images_data = {
        'imgUser': "user",
        'imgPass': "pass",
        'imgDomain': "domain",
    }

    converter_data = {
        'downloadUser': 'partner',
        'downloadPassword': '123123',
        'downloadDomain': 'lisa',
        'downloadSeparator': ',',
        'downloadFilePath': 'habiter.csv',
        'uploadUser': 'partner',
        'uploadPassword': '123123',
        'uploadDomain': 'lisa',
        'uploadFilePath': 'habiter_test.xml'
    }

    chk_img_data = {
        'imgData': True
    }
    
    chk_converter = {
        'converter': True
    }

    chk_transmit = {
        'transmit': False
    }

    chk_rules = {
        'rulesCheck': True
    }

    def _login(self, wd, cp, option, login=False):
        if login:
            cp.go(wd, option, True, 'dany', 'dany')
        else:
            cp.go(wd, option)

    def _delete_partner_ads_scenario(self, wd, control_panel, expected_result=None, login=False, accept_alert=True,
        uncheck_verify=False, check_ad='', ad_still_there=True):
        # should login
        self._login(wd, control_panel, 'Borrar avisos de partner', login)

        # do the "delete"
        control_panel.partner_delete('blocket_mobile', accept_alert=accept_alert, uncheck_verify=uncheck_verify)

        # check the result
        if expected_result != None:
            self.assertEquals(expected_result, control_panel.get_result())

        # rebuild index
        utils.rebuild_index()

        # check an ad to see if the operation was successfull
        if check_ad:
            AdViewDesktop(wd).go(check_ad)
            if ad_still_there:
                wd.find_element_by_id('da_subject') # check for ad subject
            else:
                self.assertEquals(wd.find_element_by_tag_name('h1').text, 'Este aviso ya no est� disponible en yapo.cl')

    def _add_partner_scenario(self, wd, control_panel, args, menu, login=False,
        check_img_data=False, check_converter=False, check_rules=False, add=False, msg=None):
        # should login
        self._login(wd, control_panel, menu , login)
        dictionary = {}
        if check_img_data:
            control_panel.fill_form(**self.chk_img_data) # enable img params
            dictionary = dict(dictionary, ** self.images_data)
        if check_converter:
            control_panel.fill_form(**self.chk_converter) # enable converter params
            dictionary = dict(dictionary, ** self.converter_data)
        if check_rules:
            control_panel.fill_form(**self.chk_rules) # enable rules params
            dictionary = dict(dictionary, ** self.advanced_data)

        dictionary = dict(dictionary, ** args)

        if not add:
            for key, value in dictionary.items():
                old_value = value
                args[key] = "" #assign empty to the input
                control_panel.fill_form(**args)
                control_panel.submit.click()
                self.assertEqual(wd.switch_to.active_element.get_attribute('id').lower(), key.lower())
                self.assertEqual("", control_panel.statusMsg.text.strip())
                args[key] = old_value
        else:
            if check_img_data:
                control_panel.fill_form(**self.chk_img_data) # enable img params
            
            if check_converter:
                control_panel.fill_form(**self.chk_converter) # enable converter params

            control_panel.fill_form(**args) 
            control_panel.submit.click()
            self.assertEqual(msg, control_panel.successMsg.text.strip())

    def _page_scenario(self, wd, control_panel, args, check_delimiter=False, login=False):
        # should login
        self._login(wd, control_panel, 'Agregar Partner Avanzado', login)

        if check_delimiter:
            args['delimiter'] = "thisDelimiterIsSoLong"
            control_panel.fill_form(**args)
            self.assertEqual(control_panel.delimiter.get_attribute('value'),"t")
            args.pop('delimiter', None)


    @tags('controlpanel', 'partner')
    def test_delete_partner_ads_cancel_alert(self, wd):
        """ Go to menu and cancels on alert """
        # Create the page object
        cp = ControlPanelPartners(wd)

        # go to menu and cancels on alert -> Nothing should be shown
        self._delete_partner_ads_scenario(wd, cp,
            login=True, accept_alert=False, expected_result=[])

    @tags('controlpanel', 'partner')
    def test_delete_partner_ads_correctly(self, wd):
        """ Delete all ads from a partner """
        # Create the page object
        cp = ControlPanelPartners(wd)

        # accepts the alert / just check -> check result and check ads still there
        self._delete_partner_ads_scenario(wd, cp, login=True,
            expected_result=['126 - FAM28', '127 - FAP28', '128 - FPR450', '129 - FPR451', '130 - FPR453', '131 - AUT01'],
            check_ad='8000090', ad_still_there=True)

        # uncheck verify (DO THE DELETE) -> check result and ads not there
        self._delete_partner_ads_scenario(wd, cp,
            uncheck_verify=True,
            expected_result=['126 - FAM28', '127 - FAP28', '128 - FPR450', '129 - FPR451', '130 - FPR453', '131 - AUT01'],
            check_ad='8000091', ad_still_there=False)

        # with verify (But the partner has no ads) -> result should be empty
        self._delete_partner_ads_scenario(wd, cp, expected_result=[])

    @tags('controlpanel','partner')
    def test_required_params_add_partner(self, wd):
        """ Check validations to add partners """
        # Create the page object
        cp = ControlPanelPartnerAdd(wd)

        # Check all parameters are required
        form_data = dict(self.data, ** self.basic_data)
        form_data = dict(form_data, ** self.images_data)
        self._add_partner_scenario(wd, cp, form_data, 'Agregar Partner', login=True)
    
    @tags('controlpanel','partner')
    def test_required_params_add_advanced_partner(self, wd):
        """ Check validations to add partners with advanced options """
        # Create the page object
        cp = ControlPanelPartnerAdd(wd)

        # Check all parameters are required
        form_data = dict(self.data, ** self.advanced_data)
        self._add_partner_scenario(wd, cp, form_data, 'Agregar Partner Avanzado', login=True, check_rules=True)

    @tags('controlpanel','partner')
    def test_long_delimiter_add_partner(self, wd):
        """ Check delimiter length """
        # Create the page object
        cp = ControlPanelPartnerAdd(wd)

        # Check delimiter has max long one character
        self._page_scenario(wd, cp, self.data, login=True, check_delimiter=True)

    @tags('controlpanel','partner')
    def test_required_img_parameters(self, wd):
        """ Check validations to images connection parameters"""
        # Create the page object
        cp = ControlPanelPartnerAdd(wd)

        # Add new parameters to fill the form
        form_data = dict(self.data, ** self.advanced_data)
        form_data = dict(form_data, ** self.basic_data)
        form_data = dict(form_data, ** self.images_data)
        # Check the parameter is asked as required
        self._add_partner_scenario(wd, cp, form_data, 'Agregar Partner Avanzado', login=True, check_img_data=True, check_rules = True)
    
    @tags('controlpanel','partner')
    def test_required_converter_parameters(self, wd):
        """ Check validations to converter parameters """
        # Create the page object
        cp = ControlPanelPartnerAdd(wd)

        form_data = dict(self.data, ** self.basic_data)
        form_data = dict(form_data, ** self.converter_data)

        self._add_partner_scenario(wd, cp, form_data, 'Agregar Partner', login=True, check_converter=True)
