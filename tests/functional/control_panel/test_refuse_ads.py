# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelMassiveActions
from yapo.decorators import tags
from yapo.pages.desktop import AdEdit
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.generic import SentEmail
from yapo import utils, SeleniumTest
import time


class ControlpanelRefuseAds(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'ads', 'core3')
    def test_cp_refuse_ads(self, wd):
        """ Massive refuse Ads by email """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu("Search for ad")

        emails = ['uid1@blocket.se']
        for email in emails:
            print('Deleting ads for email:'+email)
            cp.search_for_ad(email)
            cp_actions = ControlPanelMassiveActions(wd)
            if cp_actions.is_element_present('check_all'):
                cp_actions.check_all.click()
                cp_actions.fill('refuse_reason', 'Spam')
                cp_actions.button_refuse.click()

                expected_data = [
                    'Executing batch refuse:',
                    'Refused ad 29 with reason "Spam".',
                    'Refused ad 28 with reason "Spam".',
                    'Refused ad 25 with reason "Spam".',
                    'Refused ad 22 with reason "Spam".',
                    'Refused ad 24 with reason "Spam".',
                    'Refused ad 21 with reason "Spam".',
                    'Refused ad 12 with reason "Spam".',
                    'Error refusing ad 11.',
                    'Error refusing ad 8.',
                ]

                self.assertListEqual(expected_data, [i.text for i in cp_actions.result_list])

class InsertingFeeRefuse(SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_assigned_if', 'diff_pack_cars1_plus_if_purchase']
    success = 'success'

    def _login(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')

    def _refuse_ad(self, wd, queue, list_id, expected, unexpected):
        """ Refuse an inserting fee Ad """
        cp = ControlPanel(wd)
        cp.go()
        cp.go_to_option_in_menu("Search for ad")
        cp.search_for_ads(list_id = list_id, queue = queue)
        cp_actions = ControlPanelMassiveActions(wd)
        cp_actions.check_all.click()
        cp_actions.fill('refuse_reason', 'Spam')
        cp_actions.button_refuse.click()

        email = SentEmail(wd)
        email.go()
        src = wd.page_source
        self.assertIn(expected, src)
        self.assertNotIn(unexpected, src)

    def _edit_an_ad(self, wd, list_id, passwd, parameters_changed = None, delete_images = None):
        edit = AdEdit(wd)
        edit.edit_an_ad(list_id = list_id, passwd = passwd, parameters_changed = parameters_changed, delete_images = delete_images)
        result = AdInsertResult(wd)
        self.assertTrue(result.was(self.success))

    @tags('controlpanel', 'refuse', 'inserting_fee')
    def test_cp_refuse_if_ad(self, wd):
        """ Refuse the edit of an inserting fee Ad, then refuse the ad and check the emails """
        list_id = "8000094"
        passwd = "123123123"
        email = "cuentaprosincupos@yapo.cl"
        edit_text = "Puedes editar tu aviso haciendo clic"
        refund_text = "Para iniciar el proceso de devoluc"
        self._edit_an_ad(wd, list_id, passwd, {'price': 12233})

        self._login(wd)
        self._refuse_ad(wd, 'Unreviewed', list_id, refund_text, "invalid text")

        self._refuse_ad(wd, 'Published', list_id, refund_text, edit_text)
