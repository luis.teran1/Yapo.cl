# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import desktop, mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.control_panel import ControlPanel
import yapo

class AutoacceptDesktop(yapo.SeleniumTest):

    snaps = ['android']
    platform = desktop
    AdEdit = desktop.AdEdit
    success = 'success'
    bconfs = {
        "*.*.movetoapps_splash_screen.enabled": '0',
        "*.trans.clear.enqueue.delay":'0'
    }

    @tags('edit', 'autoaccept')
    def test_autoaccept_edit_all(self, wd):
        """Edit an ad, delete all images, change phone and name"""
        list_id = "8000065"
        passwd = "123123123"
        parameters_changed = {'name' : 'yolito yoli', 'phone' : '12345678'}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = True)

    @tags('edit', 'autoaccept')
    def test_autoaccept_edit_without_changes(self, wd):
        """Edit an ad without changes"""
        list_id = "8000060"
        passwd = "123123123"

        self._edit_an_ad(wd, list_id, passwd, parameters_changed = {}, delete_images = None)

    @tags('edit', 'autoaccept')
    def test_autoaccept_edit_location(self, wd):
        """Edit an ad changing location"""
        list_id = "8000062"
        passwd = "123123123"
        parameters_changed = {
            'region' : '2',
            'communes' : '6'
        }

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'edit')

    @tags('edit', 'autoaccept')
    def test_edit_price_real_estate_autoaccept_queue(self, wd):
        """Edit price(90%) of an ad in Real estate category goes to autoaccept queue(10% lower in case of Real estate category)"""
        list_id = "8000012"
        passwd = "123123123"
        price_ad = 18000000
        parameters_changed = {'price': int(price_ad*0.9)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None)

    @tags('edit', 'autoaccept')
    def test_edit_price_real_estate_edit_queue(self, wd):
        """Edit price(89%) of an ad in Real estate category goes to edit queue(10% lower in case of Real estate category)"""
        list_id = "8000007"
        passwd = "123123123"
        price_ad = 380000
        parameters_changed = {'price': int(price_ad*0.89)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'edit')

    @tags('edit', 'autoaccept')
    def test_edit_price_cars_autoaccept_queue(self, wd):
        """Edit price(85%) of an ad in Cars category goes to autoaccept queue(15% lower in case of Cars category)"""
        list_id = "8000017"
        passwd = "123123123"
        price_ad = 1750000
        parameters_changed = {'price': int(price_ad*0.85)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None)

    @tags('edit', 'autoaccept')
    def test_edit_price_cars_edit_queue(self, wd):
        """Edit price(84%) of an ad in Cars category goes to edit queue(15% lower in case of Cars category)"""
        list_id = "8000014"
        passwd = "123123123"
        price_ad = 470000
        parameters_changed = {'price': int(price_ad*0.84)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'edit')

    @tags('edit', 'autoaccept')
    def test_edit_price_others_cat_autoaccept_queue(self, wd):
        """Edit price(70%) of an ad in Others category(3020) goes to autoaccept queue(30% lower in case of the rest of categories)"""
        list_id = "8000037"
        passwd = "123123123"
        price_ad = 20000
        parameters_changed = {'price': int(price_ad*0.7)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'autoaccept')

    @tags('edit', 'autoaccept')
    def test_edit_price_others_category_edit_queue(self, wd):
        """Edit price(69%) of an ad in Others category(3020) goes to edit queue(30% lower in case of the rest of categories)"""
        list_id = "8000036"
        passwd = "123123123"
        price_ad = 140000
        parameters_changed = {'price': int(price_ad*0.69)}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'edit')

    @tags('edit', 'autoaccept')
    def test_name_in_forbidden_word_edit_queue(self, wd):
        """Edit the name of an ad with forbidden word"""
        list_id = "8000036"
        passwd = "123123123"
        parameters_changed = {'name': 'vatten'}

        self._edit_an_ad(wd, list_id, passwd, parameters_changed, delete_images = None, queue = 'edit')

    def _edit_an_ad(self, wd, list_id, passwd, parameters_changed = None, delete_images = None, queue = 'autoaccept'):
        edit = self.platform.AdEdit(wd)
        edit.edit_an_ad(list_id = list_id, passwd = passwd, parameters_changed = parameters_changed, delete_images = delete_images)
        result = AdInsertResult(wd)
        if self.platform == desktop:
            self.assertTrue(result.success())
        else:
            self.assertTrue(result.success_mobile())
        self._verify_autoaccept_queue_in_cp(wd, list_id, queue)

    def _verify_autoaccept_queue_in_cp(self, wd, list_id, queue = 'autoaccept'):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ads(list_id = list_id, queue = 'Unreviewed')
        self.assertTrue((len(cp.list_table_ads_unreviewed) > 0), 'Ad not found')
        #first unreviewed ad
        self.assertIn('Queue: {queue}'.format(queue = queue), cp.list_table_ads_unreviewed[0].text)

class AutoacceptMobile(AutoacceptDesktop):

    user_agent = yapo.utils.UserAgents.IOS
    platform = mobile
    success = 'success_mobile'

