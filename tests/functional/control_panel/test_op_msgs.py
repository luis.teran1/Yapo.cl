# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop import Dashboard
from yapo.pages.mobile import HeaderElements
from yapo.pages.control_panel import ControlPanel, ControlPanelOpMsg
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.generic import AccountBar
from yapo import utils
import yapo
import time


class ControlpanelOpMsgs(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'opmsg')
    def test_form_errors(self, wd):
        """ Test that errors are showing """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Administrar Mensajes")

        cp_op_msg = ControlPanelOpMsg(wd)
        data = {
            'type_msg_global': True,
            'source_desktop': False,
        }
        cp_op_msg.fill_form(**data)
        errors = {
            'err_themsg': 'No hay mensaje',
            'err_expiration_date': 'Formato de fecha incorrecto',
            'err_source': 'Debes establecer el source'
        }
        cp_op_msg.ok()
        cp_op_msg.check_errors(self, **errors)

        # Mensaje muy corto y fecha vieja
        data = {
            'themsg': 'XD',
            'source_msite': True,
        }
        errors.pop('err_source')
        errors['err_themsg'] = 'El mensaje es muy corto'
        errors['err_expiration_date'] = 'Debes elegir una fecha igual o posterior a la de hoy'
        cp_op_msg.fill_form(**{'expiration_date': '14/04/2015'})
        cp_op_msg.op_msg_label.click()
        cp_op_msg.fill_form(**data)
        cp_op_msg.ok()
        cp_op_msg.check_errors(self, **errors)

        # Mensaje muy largo Msite
        data = {
            'themsg': '123456789 123456789 123456789 123456789 muy largo',
        }
        errors['err_themsg'] = 'Mensaje es muy largo para Msite'
        errors.pop('err_expiration_date')
        cp_op_msg.fill_form(**{'expiration_date': '30/12/2099'})
        cp_op_msg.op_msg_label.click()
        cp_op_msg.fill_form(**data)
        cp_op_msg.ok()
        cp_op_msg.check_errors(self, **errors)

        # Mensaje muy largo Msite
        data = {
            'themsg': '123456789 123456789 123456789 123456789 123456789 '
            '123456789 123456789 123456789 123456789 123456789 123456789 '
            '123456789 123456789 123456789 123456789 123456789 123456789 '
            '123456789 123456789 123456789  muy largo',
            'source_desktop': True,
            'source_msite': False,
        }
        errors['err_themsg'] = 'Mensaje es muy largo para Desktop'
        cp_op_msg.fill_form(**data)
        cp_op_msg.ok()
        cp_op_msg.check_errors(self, **errors)

    @tags('controlpanel', 'opmsg')
    def test_cp_list(self, wd):
        """ Test that saved messages are listing correctly """
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Administrar Mensajes")

        # Mensaje en Desktop y Msite
        first_data_block = {
            'type_msg_global': True,
            'expiration_date': '01/01/2099'
        }
        second_data_block = {
            'themsg': 'This is the message',
            'source_msite': True,
            'source_desktop': True,
        }
        list = [
            'desktop / dashboard\nThis is the message\nExpira en: 01/01/2099 '
            '23:59',
            'msite / dashboard\nThis is the message\nExpira en: 01/01/2099 '
            '23:59'
        ]
        cp_op_msg = ControlPanelOpMsg(wd)
        cp_op_msg.fill_form(**first_data_block)
        cp_op_msg.op_msg_label.click()
        cp_op_msg.fill_form(**second_data_block)
        cp_op_msg.ok()
        cp_op_msg.check_list(self, list)

        # Actualiza solo desktop
        first_data_block = {
            'type_msg_global': True,
            'expiration_date': '02/02/2099'
        }
        second_data_block = {
            'expiration_time': '12:34',
            'themsg': 'This is the NEW message',
            'source_msite': False,
            'source_desktop': True,
        }
        list = [
            'desktop / dashboard\nThis is the NEW message\nExpira en: '
            '02/02/2099 12:34',
            'msite / dashboard\nThis is the message\nExpira en: '
            '01/01/2099 23:59'
        ]
        cp_op_msg.fill_form(**first_data_block)
        cp_op_msg.op_msg_label.click()
        cp_op_msg.fill_form(**second_data_block)
        cp_op_msg.ok()
        cp_op_msg.check_list(self, list)

        # Expirar
        cp_op_msg.expire('desktop', 'dashboard')
        list = [
            'msite / dashboard\nThis is the message\nExpira en: 01/01/2099 '
            '23:59'
        ]
        cp_op_msg.check_list(self, list)
        cp_op_msg.expire('msite', 'dashboard')
        list = []
        cp_op_msg.check_list(self, list)


class DashboardOpMsgs(yapo.SeleniumTest):

    source = 'desktop'
    snaps = ['accounts']

    def _agregar(self, wd, msg=""):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Administrar Mensajes")

        first_data_block = {
            'type_msg_global': True,
            'expiration_date': '01/01/2099'
        }
        second_data_block = {
            'themsg': 'This is the message',
            'source_msite': True,
            'source_desktop': True,
        }
        if msg != "":
            second_data_block['themsg'] = msg;

        cp_op_msg = ControlPanelOpMsg(wd)
        cp_op_msg.fill_form(**first_data_block)
        cp_op_msg.op_msg_label.click()
        cp_op_msg.fill_form(**second_data_block)
        cp_op_msg.ok()
        cp.go()
        cp.logout()

    @tags('controlpanel', 'opmsg')
    def test_dashboard_msg(self, wd):
        """ Test that the message is showing """
        self._agregar(wd)

        # Mensaje se muestra en Desktop y Msite
        login_and_go_to_dashboard(wd, 'prepaid3@blocket.se', '123123123',
                                  self.source == 'desktop')
        dashboard = Dashboard(wd)
        self.assertEquals(getattr(dashboard, "op_msg_" + self.source).text,
                          'This is the message')

        # Solo se muestra la primera vez...
        wd.refresh()
        dashboard.wait.until_loaded()
        self.assertEquals(getattr(dashboard, "op_msg_" + self.source).text, '')
        if self.source == 'desktop':
            AccountBar(wd).logout.click()
        else:
            HeaderElements(wd).logout()
        dashboard.wait.until(lambda x: 'login' in wd.current_url)

        # si se agrega de nuevo, se muestra de nuevo
        self._agregar(wd)
        login_and_go_to_dashboard(
            wd, 'prepaid3@blocket.se', '123123123', self.source == 'desktop')
        dashboard = Dashboard(wd)
        self.assertEquals(
            getattr(dashboard, "op_msg_" + self.source).text,
            'This is the message')
        if self.source == 'desktop':
            AccountBar(wd).logout.click()
        else:
            HeaderElements(wd).logout()
        dashboard.wait.until(lambda x: 'login' in wd.current_url)

        # si se agrega otro con html, se renderiza bien
        self._agregar(wd, 'Link a google => <a '
                        'href="http://www.google.com">AQUI!<a>')
        login_and_go_to_dashboard(
            wd, 'prepaid3@blocket.se', '123123123', self.source == 'desktop')
        dashboard = Dashboard(wd)
        self.assertEquals(
            getattr(dashboard, "op_msg_" + self.source).text,
            'Link a google => AQUI!')
        wd.find_element_by_partial_link_text("AQUI!").click();
        dashboard.wait.until(lambda x: 'google' in wd.current_url)


class MsiteDashboardOpMsgs(DashboardOpMsgs):

    user_agent = utils.UserAgents.IOS
    source = 'msite'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
