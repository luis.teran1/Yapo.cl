# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelAcceptedReasons
from yapo.pages.desktop_list import AdViewDesktop
from yapo import utils
import yapo
import yapo.conf
import time
from yapo.utils import trans, execute_query, rebuild_index


class ControlpanelAcceptReasons(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'accept_reason')
    def test_add_new_accept_reason(self, wd):
        """ Create, Use & Delete - new accepted reason """
        # Login
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')

        # Create new reason
        ar = ControlPanelAcceptedReasons(wd)
        ar.go()
        data = { 'new_subject': 'Raz�n de prueba',
                'id': '123',
                'subject':'Sin fotos',
                'text': 'Raz�n de prueba para test de selenium'
            }
        ar.fill_form(**data)
        ar.submit.click()
        ar.wait.until_present("success")
        self.assertEqual(ar.success.text, 'Raz�n guardada') 
        # Review ad using new reason
        cp.go()
        cp.search_and_review_ad('uid1@blocket.se', 'Libro 1', 'edit_and_accept', 'Raz�n de prueba')
        # Check on ad_history
        cp.search_for_ad_by_ad_id(87)
        cp.table_on_search.find_element_by_link_text('Ad history').click()
        wd.switch_to_window(wd.window_handles[1])
        data_search_on_history = 'accept_w_chngs'
        found = False
        for rows in cp.table_ad_history_rows:
            if data_search_on_history in rows.text:
                self.assertIn(data_search_on_history, rows.text)
                found = True
                break
        self.assertTrue(found, '{} not found in AdHistory'.format(data_search_on_history)) 
        cp.back_to_cp()

        # Delete the reason
        ar = ControlPanelAcceptedReasons(wd)
        ar.go('delete')
        ar.fill('subject','Raz�n de prueba')
        ar.submit.click()
        ar.wait.until_present("success")
        self.assertEqual(ar.success.text, 'Raz�n ha sido borrada') 


    @tags('controlpanel', 'accept_reason')
    def test_edit_accept_reason(self, wd):
        """ Edit accepted reason """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')

        ar = ControlPanelAcceptedReasons(wd)
        ar.go('edit')

        ar.fill('subject','Sin fotos')
        ar.wait.until(lambda x: ar.text.is_enabled())
        ar.fill('text','Raz�n de prueba para test de selenium editada')
        ar.submit.click()
        ar.wait.until_present("success")
        self.assertEqual(ar.success.text, 'Raz�n ha sido modificada') 


