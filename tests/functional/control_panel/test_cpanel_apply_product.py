# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelSearchForAd
import yapo
import yapo.conf


class ControlpanelApplyProduct(yapo.SeleniumTest):

    snaps = ['accounts']

    def _apply_product_common(self, wd, data, old_options=False):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.go_to_option_in_menu("Search for ad")
        if old_options:
            cp.change_default_options()

        cp_search = ControlPanelSearchForAd(wd)
        cp_search.fill_form(search_field=data['email'], radio_all=True)
        cp_search.button_search.click()
        cp_search.apply_product_by_ad_id(
            ad_id=data['ad_id'], prod=data['prod'],
            prod_type=data.get('prod_type'))

        cp.wait.until(lambda x: len(wd.window_handles) > 1)
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(
            cp_search.message_success.text, data['success_message'])
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        cp_search.apply_product_by_ad_id(
            ad_id=data['ad_id'], prod=data['prod'],
            prod_type=data.get('prod_type'))
        cp.wait.until(lambda x: len(wd.window_handles) > 1)
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(cp_search.message_error.text, data['error_message'])
        wd.close()
        wd.switch_to_window(wd.window_handles[0])

        cp.logout()

    @tags('controlpanel', 'stores')
    def test_daily_bump_already_applied(self, wd):
        """check daily bump already applied """
        data = {'email': 'many@ads.cl',
                'ad_id': '100',
                'prod': 'daily',
                'success_message': 'Tu aviso fue promovido',
                'error_message': 'Este aviso a�n tiene un Subir Diario activo.'
                }

        self._apply_product_common(wd, data, True)

    @tags('controlpanel', 'stores')
    def test_weekly_bump_already_applied(self, wd):
        """check weekly bump already applied """
        data = {'email': 'many@ads.cl',
                'ad_id': '100',
                'prod': 'weekly',
                'success_message': 'Tu aviso fue promovido',
                'error_message': 'Este aviso a�n tiene un Subir Semanal activo.'
                }

        self._apply_product_common(wd, data, True)

    @tags('controlpanel', 'label')
    def test_label_already_applied(self, wd):
        """check label already applied """
        data = {'email': 'many@ads.cl',
                'ad_id': '100',
                'prod': 'label',
                'prod_type': 'opportunity',
                'success_message': 'Tu aviso fue promovido',
                'error_message': 'Este aviso ya tiene una Etiqueta activa.'
                }

        self._apply_product_common(wd, data, True)


class ControlpanelATComboApplyProduct(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_at_combo']

    bconfs = {"*.*.combos_at.enabled": "1"}

    def _apply_product_common(self, wd, data, old_options=False):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.go_to_option_in_menu("Search for ad")
        if old_options:
            cp.change_default_options()

        cp_search = ControlPanelSearchForAd(wd)
        cp_search.fill_form(search_field=data['email'], radio_all=True)
        cp_search.button_search.click()
        cp_search.apply_product_by_ad_id(
            ad_id=data['ad_id'], prod=data['prod'],
            prod_type=data.get('prod_type'))

        cp.wait.until(lambda x: len(wd.window_handles) > 1)
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(
            cp_search.message_success.text, data['success_message'])
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        cp.logout()

    def _check_product_not_available(self, wd, data, old_options=False):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("blocket", "blocket")
        cp.go_to_option_in_menu("Search for ad")
        if old_options:
            cp.change_default_options()

        cp_search = ControlPanelSearchForAd(wd)
        cp_search.fill_form(search_field=data['email'], radio_all=True)
        cp_search.button_search.click()

        self.assertFalse(
            cp_search.check_product_available_by_ad_id(
                ad_id=data['ad_id'], prod=data['prod']))
        cp.logout()

    @tags('controlpanel', 'stores')
    def test_standard_combo_applied(self, wd):
        """check at combo standard correctly applied """
        data = {'email': 'user.01@schibsted.cl',
                'ad_id': '126',
                'prod': 'at_combo1',
                'success_message': 'Tu aviso fue promovido',
                }

        self._apply_product_common(wd, data, True)

    @tags('controlpanel', 'stores')
    def test_advanced_combo_applied(self, wd):
        """check at combo advanced correctly applied """
        data = {'email': 'user.01@schibsted.cl',
                'ad_id': '127',
                'prod': 'at_combo2',
                'success_message': 'Tu aviso fue promovido',
                }

        self._apply_product_common(wd, data, True)

    @tags('controlpanel', 'stores')
    def test_advanced_combo_not_available(self, wd):
        """check at combo advanced not available for an ad without an image"""
        data = {'email': 'user.01@schibsted.cl',
                'ad_id': '126',
                'prod': 'at_combo2',
                }

        self._check_product_not_available(wd, data, True)

    @tags('controlpanel', 'stores', 'label')
    def test_premium_combo_applied(self, wd):
        """check at combo premium correctly applied """
        data = {'email': 'user.01@schibsted.cl',
                'ad_id': '128',
                'prod': 'at_combo3',
                'prod_type': 'opportunity',
                'success_message': 'Tu aviso fue promovido',
                }

        self._apply_product_common(wd, data, True)

    @tags('controlpanel', 'stores', 'label')
    def test_premium_combo_not_available(self, wd):
        """check at combo premium is not available for an ad with a tag"""
        data = {'email': 'user.01@schibsted.cl',
                'ad_id': '128',
                'prod': 'at_combo3',
                }

        self._check_product_not_available(wd, data, True)
