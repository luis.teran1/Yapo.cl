# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelUpdateProParam
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.accounts import Edit as EditAccount
from yapo.decorators import tags
from yapo import utils, SeleniumTest
import time

class ControlpanelAccountsPriPro(SeleniumTest):

    bconfs = {
        '*.*.packs.type.2.enabled':'1',
        '*.*.insertingfee.type.1.enabled':'1',
    }

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts','diff_pack_cars1'])

    @tags('controlpanel', 'packs')
    def test_check_search_errors(self, wd):
        """ CP PriPro, check search errors """

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Cambiar estado de usuario")

        cp_pripro = ControlPanelUpdateProParam(wd)
        cp_pripro.fill_form(search_box = 'asdasd')
        cp_pripro.search_button.click()
        self.assertEqual(cp_pripro.error_message.text, 'UID no es v�lido.')

        cp_pripro.search_by_email.click()
        cp_pripro.search_button.click()
        self.assertEqual(cp_pripro.error_message.text, 'Email no es v�lido.')


    @tags('controlpanel', 'packs')
    def test_check_search_results_by_uid_or_email(self, wd):
        """ CP PriPro, check search results by uid or email """

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Cambiar estado de usuario")

        cp_pripro = ControlPanelUpdateProParam(wd)
        cp_pripro.fill_form(search_box = '3')
        cp_pripro.search_button.click()
        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
                ['prepaid3@blocket.se', True, False, False],
                ['cuentaprosinpack@yapo.cl', True, False, False],
                ['cuentaprosincupos@yapo.cl', True, False, False]
        ]
        self.assertListEqual(table_data,expected_data )

        cp_pripro.search_by_email.click()
        cp_pripro.fill_form(search_box = 'prepaid5@blocket.se')
        cp_pripro.search_button.click()
        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
                ['prepaid5@blocket.se', False, False, False]
        ]
        self.assertListEqual(table_data,expected_data)

    @tags('controlpanel', 'packs')
    def test_check_update_accounts_by_uid_or_email(self, wd):
        """ CP PriPro, check update accounts by uid or email """

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Cambiar estado de usuario")

        cp_pripro = ControlPanelUpdateProParam(wd)
        cp_pripro.fill_form(search_box = '3')
        cp_pripro.search_button.click()
        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
                ['prepaid3@blocket.se', True, False, False],
                ['cuentaprosinpack@yapo.cl', True, False, False],
                ['cuentaprosincupos@yapo.cl', True, False, False]
        ]
        self.assertListEqual(table_data,expected_data )
       
        cp_pripro.select_pro_type_by_id("car_0")
        cp_pripro.select_pro_type_by_id("real_estate_1")
        cp_pripro.select_pro_type_by_id("jobs_2")
        cp_pripro.submit_button.click()
        self.assertEqual(cp_pripro.message.text, "Cambios guardados correctamente.") 

        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
                ['prepaid3@blocket.se', False, False, False],
                ['cuentaprosinpack@yapo.cl', True, True, False],
                ['cuentaprosincupos@yapo.cl', True, False, True]
        ]
        self.assertListEqual(table_data,expected_data )
        
        cp_pripro.search_by_email.click()
        cp_pripro.fill_form(search_box = 'prepaid5@blocket.se')
        cp_pripro.search_button.click()
        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
            ['prepaid5@blocket.se', False, False, False]
        ]
        self.assertListEqual(table_data,expected_data)
       
        cp_pripro.select_pro_type_by_id("car_0")
        cp_pripro.select_pro_type_by_id("real_estate_0")
        cp_pripro.select_pro_type_by_id("jobs_0")
        cp_pripro.submit_button.click()
        self.assertEqual(cp_pripro.message.text, "Cambios guardados correctamente.") 

        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
            ['prepaid5@blocket.se', True, True, True]
        ]
        self.assertListEqual(table_data,expected_data )
        
    @tags('controlpanel', 'packs', 'accounts')
    def test_check_accounts_profile_is_company(self, wd):
        """ CP PriPro, check account profile is company """

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Cambiar estado de usuario")
        email = 'prepaid5@blocket.se'
        cp_pripro = ControlPanelUpdateProParam(wd)
        cp_pripro.search_by_email.click()
        cp_pripro.fill_form(search_box = email)
        cp_pripro.search_button.click()
        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
            [email, False, False, False]
        ]
        self.assertListEqual(table_data,expected_data)

        cp_pripro.select_pro_type_by_id("car_0")
        cp_pripro.submit_button.click()
        self.assertEqual(cp_pripro.message.text, "Cambios guardados correctamente.") 

        table_data = cp_pripro.get_table_data('accounts_table')
        expected_data = [
            [email, True, False, False]
        ]
        self.assertListEqual(table_data,expected_data)

        login_and_go_to_dashboard(wd, email, '123123123')
        account = EditAccount(wd)
        account.go()
        self.assertTrue(account.pro_radio.is_selected())
        self.assertTrue(account.pro_radio.get_attribute("disabled"))
