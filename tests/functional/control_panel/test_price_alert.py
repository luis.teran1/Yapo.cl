from yapo.pages.control_panel import ControlPanel
from yapo.decorators import tags
from yapo.steps import trans as transSteps
from yapo import utils, SeleniumTest


class ControlpanelPriceAlert(SeleniumTest):

    snaps = ['accounts']
    username = 'dany'
    password = 'dany'

    brands = {'GREAT WALL': 35, 'ACADIAN': 1}
    models = {'FLORID': 2, 'BEAUMONT': 1}
    versions = {'LE' : 2, 'BEAUMONT': 1}

    def work(self, wd, css_expected, color, mail, price, brand = "GREAT WALL", model = "FLORID", version = "LE", regdate = "2011"):
        dictionary = {
                'category': 2020,
                'price': price,
                'brand': self.brands[brand],
                'model': self.models[model],
                'version':self.versions[version],
                'regdate':regdate,
                'mileage':15000,
                'plates':'ABCD10',
                'cartype':1,
                'gearbox':1,
                'fuel':1,
                'name': 'Boris',
                'email': mail,
                'subject': '{0} {1} {2}'.format(brand, model, regdate),
                'body': 'test for price color. This price must be show in {0}'.format(color),
                'company_ad': 1,
                'phone':'65962077',
                'password': "123123123",
                'region': 15,
                'communes': 310,
        }
        self._insert_ad(wd, **dictionary)

        cpanel = ControlPanel(wd)
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Search for ad")
        cp.change_default_options(archive='All archives', period='All')
        cp.search_for_ad(email=mail, subject=dictionary['subject'], queue='Unreviewed')
        self.assertEquals(css_expected, cp.price.get_attribute('class'))

    def _insert_ad(self, wd, **kwargs):
        result = transSteps.new_ad(kwargs)

    @tags('controlpanel')
    def test_blue(self, wd):
        """ blue = price + (5%-9.99%) """
        color="blue"
        mail = "blueprice@yapo.cl"
        price= "2347600"
        css = 'ad-price price_color_warning'
        self.work(wd, css, color, mail, price)

    @tags('controlpanel')
    def test_green(self, wd):
        """ green = price + 10% """
        color="green"
        mail = "greenprice@yapo.cl"
        price= "4587000"
        css = 'ad-price price_color_ok'
        self.work(wd, css, color, mail, price)

    @tags('controlpanel')
    def test_red(self, wd):
        """ red = price - $1 """
        color= "red"
        mail = "redprice@yapo.cl"
        price= "20000"
        css= 'ad-price price_color_refuse'
        self.work(wd, css, color, mail, price)

    @tags('controlpanel')
    def test_purple(self, wd):
        """ purple = not found in car data """
        color="purple"
        brand="ACADIAN"
        model="BEAUMONT"
        version="BEAUMONT"
        regdate = "2012"
        mail = "purpleprice@yapo.cl"
        price= "20000000"
        css='ad-price price_color_notfound'
        self.work(wd, css, color, mail, price, brand, model, version, regdate)
