# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelUpdateProParam, ControlPanelPackSearchAndAssign, ControlPanelFixPackSlots
from yapo.decorators import tags
from yapo.steps import insert_an_ad
from yapo import utils, SeleniumTest
import datetime


class ControlpanelPacks(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'packs', 'pack_auto', 'pack_inmo')
    def test_cp_change_user_state(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu('Cambiar estado de usuario')
        cp = ControlPanelUpdateProParam(wd)
        cp.search_by_email.click()
        cp.search_box.send_keys('many@ads.cl')
        cp.search_button.click()
        cp.car_checkbox.click()
        cp.inmo_checkbox.click()
        cp.submit_button.click()
        self.assertEqual(cp.message.text, "Cambios guardados correctamente.")
        cp.inmo_checkbox.click()
        cp.submit_button.click()
        self.assertEqual(cp.message.text, "Cambios guardados correctamente.")

    @tags('controlpanel', 'packs', 'pack_auto')
    def test_cp_assign_pack_auto(self, wd):
        utils.restore_snaps_in_a_cooler_way(['accounts'])
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cpup = ControlPanelUpdateProParam(wd)
        cpup.make_me_pro(wd, 'many@ads.cl', type='auto')
        cp.go()
        cp.go_to_option_in_menu('Buscar y/o Asignar Pack Auto')
        cpps = ControlPanelPackSearchAndAssign(wd)
        cpps.search_box.send_keys('many@ads.cl')
        cpps.search_button.click()
        self.assertEqual(cpps.table_active.text, "No Hay Packs")
        self.assertEqual(cpps.table_expired.text, "No Hay Packs")
        cp.go()
        cpup = ControlPanelPackSearchAndAssign(wd)
        d = datetime.date.today() + datetime.timedelta(days=+365)
        cpup.assign_pack(wd, 'auto', '1', d.strftime(
            "%d/%m/%Y"), 'many@ads.cl', order='69', mount='130000')
        self.assertEqual(cpup.pack_okay.text, "Pack asignado con exito")
        self.assertNotEqual(cpps.table_active.text, "No Hay Packs")

    @tags('controlpanel', 'packs', 'pack_inmo')
    def test_cp_assign_pack_inmo(self, wd):
        utils.restore_snaps_in_a_cooler_way(['accounts'])
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cpup = ControlPanelUpdateProParam(wd)
        cpup.make_me_pro(wd, 'many@ads.cl', type='inmo')
        cp.go()
        cp.go_to_option_in_menu('Buscar y/o Asignar Pack Inmo')
        cpps = ControlPanelPackSearchAndAssign(wd)
        cpps.search_box.send_keys('many@ads.cl')
        cpps.search_button.click()
        self.assertEqual(cpps.table_active.text, "No Hay Packs")
        self.assertEqual(cpps.table_expired.text, "No Hay Packs")
        cp.go()
        d = datetime.date.today() + datetime.timedelta(days=+365)
        cpps.assign_pack(wd, 'inmo', '17', d.strftime(
            "%d/%m/%Y"), 'many@ads.cl', order='69', mount='130000')
        self.assertEqual(cpps.pack_okay.text, "Pack asignado con exito")
        cpps = ControlPanelPackSearchAndAssign(wd)
        self.assertNotEqual(cpps.table_active.text, "No Hay Packs")

    @tags('controlpanel', 'packs', 'pack_inmo', 'expire')
    def test_cp_expire_pack(self, wd):
        utils.restore_snaps_in_a_cooler_way(['accounts'])
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cpup = ControlPanelUpdateProParam(wd)
        cpup.make_me_pro(wd, 'many@ads.cl', type='inmo')
        cp.go()
        cpup = ControlPanelPackSearchAndAssign(wd)
        d = datetime.date.today() + datetime.timedelta(days=+365)
        cpup.assign_pack(wd, 'inmo', '17', d.strftime(
            "%d/%m/%Y"), 'many@ads.cl', order='69', mount='130000')
        self.assertEqual(cpup.pack_okay.text, "Pack asignado con exito")
        self.assertNotEqual(cpup.table_active.text, "No Hay Packs")
        self.assertEqual(cpup.table_expired.text, "No Hay Packs")
        self.assertNotEqual(cpup.table_active.text, "No Hay Packs")
        cpup.expire_pack_button.click()
        wd.switch_to_alert().accept()
        self.assertEqual(cpup.table_active.text, "No Hay Packs")
        self.assertNotEqual(cpup.table_expired.text, "No Hay Packs")


class ControlpanelPacksVerify(SeleniumTest):
    snaps = ['accounts', 'diff_pack_cars1']

    @tags('controlpanel', 'packs', 'pack_auto', 'pack_inmo')
    def test_cp_verify_first_pack_type(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu('Cambiar estado de usuario')
        cp = ControlPanelUpdateProParam(wd)
        cp.search_by_email.click()
        cp.search_box.send_keys('cuentaproconpack@yapo.cl')
        cp.search_button.click()
        self.assertEqual('car', cp.first_pack_type.text)
        cp.car_checkbox.click()
        cp.inmo_checkbox.click()
        cp.submit_button.click()
        self.assertEqual('real_estate', cp.first_pack_type.text)


class FixPackSlots(SeleniumTest):
    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {'*.controlpanel.modules.adqueue.auto.pack.timeout': '100'}

    def _login_and_fix(self, wd, email):
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")
        saa = ControlPanelPackSearchAndAssign(wd)
        saa.search_box.send_keys(email)
        saa.search_button.click()

        fps = ControlPanelFixPackSlots(wd)
        fps.submit.click()
        alert = wd.switch_to_alert()
        alert.accept()

        return fps

    @tags('controlpanel', 'packs', 'pack_auto', 'pack_inmo')
    def test_cp_fix_with_pending_ad(self, wd):
        """ Checks that a pro user with a pending ad can not trigger the fix procedure """
        email = "cuentaproconpack@yapo.cl"
        result = insert_an_ad(wd, AdInsert=None,
                              category=2020,
                              email=email,
                              passwd="$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6"
                              )

        fps = self._login_and_fix(wd, email)

        self.assertEquals(fps.get_error_for(
            'fix_slots').text, 'El usuario tiene avisos pendientes de revisi�n.')

    @tags('controlpanel', 'packs', 'pack_auto', 'pack_inmo')
    def test_cp_fix_with_no_changes(self, wd):
        """ check that nothing changes when the user has no problems """
        expected = [
            ['Cupos liberados:', '0'],
            ['Cupos Totales:', '1'],
            ['Total Avisos:', '1'],
            ['']
        ]
        email = "cuentaprosincupos@yapo.cl"
        fps = self._login_and_fix(wd, email)
        results = fps.get_results()
        self.assertListEqual(results, expected)

    @tags('controlpanel', 'packs', 'pack_auto', 'pack_inmo')
    def test_cp_fix_pack_slots_for_real(self, wd):
        """ check that the blocked slots of the user can be fixed """
        used_packs = 10
        pack_id = 2
        expected = [
            ['Cupos liberados:', '9'],
            ['Cupos Totales:', '10'],
            ['Total Avisos:', '1'],
            ['']
        ]
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1'])
        query = "UPDATE packs set used = {} where pack_id = {}".format(
            used_packs, pack_id)
        utils.db_query(query)

        email = "cuentaproconpack@yapo.cl"
        fps = self._login_and_fix(wd, email)
        results = fps.get_results()
        self.assertListEqual(results, expected)
