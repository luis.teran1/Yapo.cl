# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import desktop, mobile
from selenium.webdriver.support.select import Select
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.ad_insert import AdInsert
from yapo.pages.control_panel import ControlPanel
from yapo import steps
import yapo

class ControlPanelAdParams(yapo.SeleniumTest):

    snaps = ['accounts', 'cars_adparams']

    @tags('edit', 'autoaccept')
    def test_check_cars_params(self, wd):
        """Check if the params are available in control panel"""
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject('Aviso de auto 01')
        wd.find_element_by_partial_link_text('Aviso de auto 01').click()
        wd.switch_to_window(wd.window_handles[1])
        self.assertIn('AMERICAN MOTORS HORNET HB AUTOMATICO', cp.ad_info.text)
        self.assertIn('A�o: 1977', cp.ad_info.text)
        self.assertIn('Kil�metros: 100000', cp.ad_info.text)
        self.assertIn('Combustible: Bencina', cp.ad_info.text)
        self.assertIn('Transmisi�n (cambio): Autom�tico', cp.ad_info.text)
        self.assertIn('Tipo de veh�culo: Autom�vil', cp.ad_info.text)
        self.assertIn('Tipo: Busco', cp.ad_info.text)

    @tags('controlpanel', 'adparams')
    def test_check_moda_vestuario_params(self, wd):
        """Check if the params are available in control panel for 4020 category"""
        steps.trans.insert_ad(AdInsert(wd),
            region="15",
            communes=327,
            category="4020",
            subject='testes params in cp',
            body="yoLo",
            price=999666,
            private_ad=True,
            name="yolo",
            email="yolo@cabral.es",
            email_verification="yolo@cabral.es",
            phone="99996666",
            password="123123123",
            password_verification="123123123",
            create_account=False,
            accept_conditions=True,
            type='s',
            clothing_size='2',
            condition='2',
            gender='3'
        )
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject('testes params in cp')
        wd.find_element_by_partial_link_text('testes params in cp').click()
        wd.switch_to_window(wd.window_handles[1])
        self.assertIn('Talla:', cp.ad_info.text)
        self.assertIn('S', cp.get_adparam_of_ad('clothing_size').text)
        self.assertIn('Usado', cp.get_adparam_of_ad('condition').text)
        self.assertIn('Unisex', cp.get_adparam_of_ad('gender').text)

    @tags('controlpanel', 'adparams')
    def test_check_inmo_new_params(self, wd):
        """Check if the params are available in control panel for 1220 category"""
        subject = "Test inmo new params in CP"
        steps.trans.insert_ad(AdInsert(wd),
            region="15",
            communes=327,
            category="1220",
            subject=subject,
            body="yoLo",
            price=999666,
            private_ad=True,
            name="yolo",
            email="yolo@cabral.es",
            email_verification="yolo@cabral.es",
            phone="99996666",
            password="123123123",
            password_verification="123123123",
            create_account=False,
            accept_conditions=True,
            type='s',
            estate_type='1',
            rooms='1',
            size='100',
            util_size='50',
            built_year='1950',
            ext_code='INMO300'
        )
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(subject)
        wd.find_element_by_partial_link_text(subject).click()
        wd.switch_to_window(wd.window_handles[1])
        print(cp.ad_info.text)
        self.assertIn('Superficie �til: 50 m�', cp.ad_info.text)
        self.assertIn('A�o de construcci�n: 1950', cp.ad_info.text)
        self.assertIn('C�digo inmobiliaria: INMO300', cp.ad_info.text)
