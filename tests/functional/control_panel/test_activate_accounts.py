# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanelActivateAccount
from yapo.pages.desktop_list import AdViewDesktop
from yapo import utils
import yapo, time

class TestAccounts(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_partners']

    data = {
        'nameAccount': "partner",
        'mailAccount': "tienda@mail.com",
        'phoneAccount': "911111111"
    }
    
    invalid_data = {
        'nameAccount': "X",
        'mailAccount': "not_mail",
        'phoneAccount': "not_phone"
    }

    def _login(self, wd, cp, option, login=False):
        if login:
            cp.go(wd, option, True, 'dany', 'dany')
        else:
            cp.go(wd, option)
    
    def _activate_account_scenario(self, wd, control_panel, args, login=False, chk_required=False,
        expected_result=None):
        # should login
        self._login(wd, control_panel, 'Activar Cuenta', login)

        if chk_required:
            for key, value in args.items():
                old_value = value
                args[key] = "" #assign empty to the input

                control_panel.fill_form(**args)
                control_panel.submit.click()

                self.assertEqual(wd.switch_to.active_element.get_attribute('id'), key)
                args[key] = old_value
        else:
            control_panel.fill_form(**args)
            control_panel.submit.click()

            if expected_result != None:
                self.assertIn(expected_result, control_panel.get_error_message())
    
    def test_required_params_activate_account(self, wd):
        """ Check validations to activate account """
        # Create the page object
        cp = ControlPanelActivateAccount(wd)

        # Check all parameters are required
        self._activate_account_scenario(wd, cp, self.data, login=True, chk_required=True)

    @tags('controlpanel','accounts')
    def test_format_required_params_activate_account(self, wd):
        """ Check format of required params """
        # Create the page object
        cp = ControlPanelActivateAccount(wd)

        # Check mail format
        self._activate_account_scenario(wd, cp, self.invalid_data, login=True, expected_result=cp.INVALID_EMAIL)
        self.invalid_data['mailAccount'] = self.data['mailAccount']

        # Check phone format
        self._activate_account_scenario(wd, cp, self.invalid_data, expected_result=cp.INVALID_PHONE)
        self.invalid_data['phoneAccount'] = self.data['phoneAccount']

        # Check name format
        self._activate_account_scenario(wd, cp, self.invalid_data, expected_result=cp.INVALID_NAME)

    @tags('controlpanel','accounts')
    def test_activate_account_successfully(self, wd):
        """ Activate account ok """
        # Create the page object
        cp = ControlPanelActivateAccount(wd)
        self._activate_account_scenario(wd, cp, self.data, login=True)
        self.assertEqual(cp.ACTIVE_OK, cp.get_success_message())

    @tags('controlpanel','accounts')
    def test_duplicate_account(self, wd):
        """ Check no activate duplicated account """
        # Create the page object
        cp = ControlPanelActivateAccount(wd)

        # Activate account twice
        self.data['mailAccount'] = "tienda2@mail.com"
        self._activate_account_scenario(wd, cp, self.data, login=True)
        self.assertEqual(cp.ACTIVE_OK, cp.get_success_message())
        self._activate_account_scenario(wd, cp, self.data, login=False, expected_result=cp.ALREADY_EXISTS)
