# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo import utils
import yapo


class ControlPanelBulkloadAdOnReview(yapo.SeleniumTest):

    snaps = ['newinmo_ads']

    @tags('controlpanel', 'cm')
    def test_ad_on_review_w_cm_inmo(self, wd):
        """ Bulkload inmo ad """
        ad_id = 128
        self._scenario(wd, ad_id, True, msg="Carga Masiva")

    @tags('controlpanel', 'cm')
    def test_ad_on_review_without_cm_inmo(self, wd):
        """ Normal inmo ad """
        list_id = 129
        self._scenario(wd, list_id, False)

    @tags('controlpanel', 'cm')
    def test_ad_on_review_w_cm_suto(self, wd):
        """ Bulkload auto ad """
        ad_id = 35
        self._scenario(wd, ad_id, True, msg="Integradores")

    @tags('controlpanel', 'cm')
    def test_ad_on_review_without_cm_auto(self, wd):
        """ Normal auto ad """
        list_id = 36
        self._scenario(wd, list_id, False)

    def _scenario(self, wd, ad_id, should_be_present, msg=None):

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_ad_id(ad_id=ad_id)
        cp.open_ad_on_search()
        if should_be_present:
            cp.wait.until_present('bulkload_ad_label')
            self.assertEquals(cp.bulkload_ad_label.text, msg)
        else:
            cp.wait.until_not_present('bulkload_ad_label')
        cp.close_window_and_back()
