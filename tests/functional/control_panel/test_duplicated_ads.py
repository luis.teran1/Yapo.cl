# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.control_panel import ControlPanel
import yapo


class DuplicatedAds(yapo.SeleniumTest):
    AdInsert = AdInsertDesktop
    snaps = ['accounts', 'diff_cp_rem_dup']
    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123',
            'subject1': 'Adeptus Astartes Codex'
            }

    @tags("control_panel", "duplicated")
    def test_duplicated_ads_on_show_ad(self, wd):
        """
        Verify the deleted is not present
        (The diff of DB have an deleted ad wich passed
        trought deactivated status before deleted)
        """
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(
            self.user['subject1'], 'Unreviewed')

        cp.open_ad_on_search(self.user['subject1'])

        # Check duplicated only have 2 because the deleted is not present
        self.assertEqual(2, len(cp.duplicated_ads))

        # First ad is deactivated
        duplicated = cp.duplicated_ads[126]
        self.assertEqual("Deactivated", duplicated.status.text)
        self.assertIn(self.user['subject1'], duplicated.subject.text)

        # Sencond ad is published
        duplicated = cp.duplicated_ads[127]
        self.assertEqual("Published", duplicated.status.text)
        self.assertIn(self.user['subject1'], duplicated.subject.text)

        # check deactivated ads list

        # First ad is deactivated
        deactivated = cp.deactivated_ads[126]
        self.assertEqual("Deactivated", deactivated.status.text)
        self.assertIn(self.user['subject1'], deactivated.subject.text)

        # Sencond ad is deactivated
        deactivated = cp.deactivated_ads[128]
        self.assertEqual("Deactivated", deactivated.status.text)
        self.assertIn(
            "Alfa romeo alfa sportwagon 2005", deactivated.subject.text)
        deactivated.subject.click()
        self.assertEqual("Adeptus Astartes Codex", cp.ad_title.text)

        cp.back_to_cp()
