# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.desktop import Login, PaymentVerify, SummaryPayment
from yapo.pages.control_panel import ControlPanel
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.utils import Categories
from yapo.pages.simulator import Paysim
import yapo

class TestConfirmUnpaid(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pro_account', 'diff_jobs_ads']
    AdInsert = DesktopAI
    AdView = AdViewDesktop
    site_login = { 'user': 'prepaid5@blocket.se', 'password': '123123123' }
    data = {
        'category': '7020',
        'working_day': '2',
        'contract_type': '1',
        'jc': ['20', '24'],
        'phone': '12312312',
        'subject': 'New ad params',
        'email': 'prepaid5@blocket.se',
        'accept_conditions': True,
        'email_confirm': 'prepaid5@blocket.se',
        'passwd': 123123123,
        'logged': True
    }

    @tags('ad_insert', 'controlpanel')
    def test_happy_path_7020(self, wd):
        """ Confirm an unpaid ad from control panel """

        # Login on yapo
        login = Login(wd)
        login.login(**self.site_login)

        result = insert_an_ad(wd, AdInsert=self.AdInsert, **self.data)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(self.data['subject'])
        cp.confirm_an_ad()
        wd.switch_to_window(wd.window_handles[1])
        payment_verify = PaymentVerify(wd)
        self.assertVisible(payment_verify, 'text_congratulation_message')

    @tags('ad_insert', 'controlpanel')
    def test_multiple_retry_unpaid(self, wd):
        """ Don't show failed payment retries when search from control panel """

        # Login on yapo
        login = Login(wd)
        login.login(**self.site_login)

        result = insert_an_ad(wd, AdInsert=self.AdInsert, **self.data)

        summary_payment = SummaryPayment(wd)

        if(len(summary_payment.get_table_data()) > 1):
            # clean the updated card modal message
            summary_payment.pay_with_webpay()

            summary_payment.wait.until_visible('modal_cart_button')
            self.assertEquals(summary_payment.modal_cart_body_title.text, '�Tu carro de compras ha sido actualizado!')
            summary_payment.modal_cart_button.click()
            summary_payment.wait.until_not_visible('modal_cart_button')

        summary_payment.pay_with_webpay()

        paysim = Paysim(wd)
        paysim.pay_refuse()

        summary_payment.pay_with_webpay()
        paysim.pay_refuse()

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(self.data['subject'])
        self.assertEqual(len(cp.link_confirm), 1)
