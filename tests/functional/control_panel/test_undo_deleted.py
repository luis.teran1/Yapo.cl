# -*- coding: latin-1 -*-
import yapo
from yapo.utils import trans
from yapo.pages import control_panel as cp
from yapo.pages import desktop_list
from yapo.decorators import tags

class UndoDeleted(yapo.SeleniumTest):

      snaps = ['accounts']
      title_ad_published = 'Departamento Tarapac�'
      list_id = '6000667'
      ad_id = '1'

      @tags('desktop', 'undo_deleted', 'controlpanel')
      def test_refuse_undo_delete(self, wd):
          """
           Refuse an ad with state undo_delete
          """
          self.__execute_trans_deletead()
          self.__execute_trans_undo_delete()

          cp_page = cp.ControlPanel(wd)
          cp_page.go()
          cp_page.login('blocket', 'blocket')
          cp_page.search_ad_by_ad_id(self.ad_id)
          cp_page.review_first_ad_on_search()

          yapo.utils.rebuild_asearch()

          desktop_page = desktop_list.AdListDesktop(wd)
          desktop_page.go()
          desktop_page.search_for_ad(self.title_ad_published)

          desktop_page.wait.until_present('no_entries_found')
          self.assertTextEquals(desktop_page.no_entries_found, 'No se encontraron entradas')

      def __execute_trans_authenticate(self):
          cmd = trans('authenticate',
                      remote_addr = '10.0.1.139',
                      username = 'blocket',
                      passwd = '430985da851e9223368e820ebde5beaf6c6ef1cc')
          self.assertTrue('token' in cmd)
          self.assertEquals('TRANS_OK', cmd['status'])
          self.token = cmd['token']

      def __execute_trans_deletead(self):
          self.__execute_trans_authenticate()
          cmd = trans('deletead',
                      remote_addr = '10.0.1.139',
                      id = self.list_id,
                      source = 'web',
                      reason = 'admin_deleted',
                      token = self.token)
          self.assertEquals('TRANS_OK:ok, ad {list_id} deleted'.format(list_id = self.list_id), cmd['status'])

      def __execute_trans_undo_delete(self):
          self.__execute_trans_authenticate()
          cmd = trans('undo_deletead',
                     remote_addr = '10.0.1.139',
                     ad_id = self.ad_id,
                     token = self.token)
          self.assertEquals('TRANS_OK', cmd['status'])
