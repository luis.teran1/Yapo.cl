# -*- coding: latin-1 -*-
from functional.control_panel.whitelist.whitelist import WhitelistTest
from yapo.decorators import tags
from yapo import utils

class AccountInWhitelistOtherCountry(WhitelistTest):

    snaps = ['accounts', 'diff_whitelist']
    bconfs = {
        # We are cheating, IP will always be UNK, but we change the expected country
        "*.*.ai.whitelist.expected_country": "PE",
        "*.trans.clear.enqueue.delay":'0'
    }

    @tags('controlpanel', 'whitelist')
    def test_not_chilean_ip(self, wd):
        """User in whitelist, insert one from foreign IP, keep in whitelist, ad goes to review"""
        email = '1_foreignip_to_review_other_whitelistqueue@yapo.cl'
        ads = [
            ('Aviso Peruano', 'Si pe!', 99999)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

class WhitelistDashboardStats(WhitelistTest):

    snaps = ['accounts', 'diff_whitelist']
    bconfs = {
        "*.controlpanel.modules.adqueue.auto.whitelist.timeout": "0",
        "*.trans.clear.enqueue.delay":'0'
    }

    @tags('controlpanel', 'whitelist')
    def test_accepted_ad_through_whitelist(self, wd):
        """Testing dashboard stats are ok when ads are accepted through whitelist"""
        email = '1_inserted_keep_whitelist@yapo.cl'
        ad = ('Accepted ad through whitelist', 'EL LIMITE QUE ROMPE EL DESEO', 99999)

        self._whitelist_ad_in_dashboard(wd, email, ad, True)


class AccountInWhitelist(WhitelistTest):

    snaps = ['accounts', 'diff_whitelist']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'whitelist')
    def test_publish_correct_ad_whitelist_user_whith_3_ads(self, wd):
        """User with 3 ads, publish a new ad, keep in whitelist, ad in whitelist"""

        email = '1_inserted_keep_whitelist@yapo.cl'
        ads = [
            ('Chaqueta de Pokito', 'Vuela como campeona', 234944),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_publish_correct_ad_whitelist_user_whithout_3_ads(self, wd):
        """User on whitelist, publish an ad, keep in whitelist, ad in whitelist"""

        email = 'user_doesnt_3accepted_in_3m_other_whitelist_queue@yapo.cl'
        ads = [
            ('Post it de colores', 'Pero colores feos', 2944),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_publish_ad_rejected_whitelist_user(self, wd):
        """User on whitelist, publish a new ad and it's rejected, removed from whitelist, ad goes to review"""

        email = '1_rejected_other_moved_review_queue@yapo.cl'
        ads = [
            ('Pelota de pepsi', 'Nada como una pepsi', 6000),
            ('Second ad to review queue', 'Fancy ad to impress girls', 7000),
        ]

        self._whitelist_test_case(wd, email, ads, 'refuse', True, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_forbidden_words_body_ad_whitelist_user(self, wd):
        """User on whitelist, publish an ad with forbidden words in the body, keep in whitelist, ad goes to review"""

        email = '1_forbiddenwords_other_whitelist_queue@yapo.cl'
        ads = [
            ('Mira mis badwords', 'Tri mamahuevo', 553322),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_forbidden_words_subject_ad_whitelist_user(self, wd):
        """User on whitelist, publish an ad with forbidden words in the subject, keep in whitelist, ad goes to review"""

        email = '1_forbiddenwords_other_whitelist_queue@yapo.cl'
        ads = [
            ('Mira mis badwords', 'Tri mamahuevo', 553322)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_forbidden_multi_words_subject_ad_whitelist_user(self, wd):
        """User on whitelist, publish an ad with forbidden words (multi) in the subject, keep in whitelist, ad goes to review"""

        email = '1_forbiddenwords_other_whitelist_queue@yapo.cl'
        ads = [
            ('For Sale', 'no debi escribir eso', 553322)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_duplicated_ad_whitelist_user(self, wd):
        """User on whitelist, publish duplicated ads, keep in whitelist, 2nd ad goes to review"""

        email = '1_duplicatewarning_other_whitelist_queue@yapo.cl'
        ads = [
            ('Casa pa gatos con olor a perro', 'Antes pertenecio a un perro con una fuerte y dolorosa diarrea, pero es adecuado para gatos peque�os', 5332),
            ('Casa pa gatos con olor a perro', 'Antes pertenecio a un perro con una fuerte y dolorosa diarrea, pero es adecuado para gatos peque�os', 1500),
        ]

        utils.bconf_overwrite('*.*.delete2bump.active.duplicate.enable', 1)
        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'duplicated_active')

    @tags('controlpanel', 'whitelist')
    def test_ad_without_green_price_whitelist_user(self, wd):
        """User on whitelist, publish a cer with not green price, keep in whitelist, ad goes to review"""

        email = '1_without_greenprice_other_whitelist_queue@yapo.cl'
        ads = [
            ('Toyota yaris sport 2012', 'El auto posee comodos asientos, perfecto para las relaciones asexuales de los backend', {
                'brand': '88',
                'model': '28',
                'version': '24',
                'regdate': '2012',
                'gearbox': '1',
                'fuel': '1',
                'cartype': '1',
                'mileage': '15000',
                'price': '4580000',
                'plates': 'ABCD10',
            })
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_ad_with_price_under_1000_whitelist_user(self, wd):
        """User on whitelist, publish an ad < 1000 CLP, keep in whitelist, ad goes to review"""

        email = '1_under1000_to_review_other_whitelistqueue@yapo.cl'
        ads = [
            ('Perro ovejero barato', 'Perro ovejero realmente barato, esto se debe a que no cuida a las ovejas, solo las mira...', 10),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_ad_with_notes_whitelist_user(self, wd):
        """User publish a new ad with notes, keep in whitelist, ad goes to review"""

        email = '1_withnote_to_review_other_whitelist_queue@yapo.cl'
        ads = [
            ('Pelota de tenis', 'Fue encontrada bajo un puente, tiene una pelicula de una cosa vizcosa', 7500),
            ('Puzzle de pillele la cola al burro', 'Estupenda pieza de coleccion, para los mas peque�ios del hogar', 9500),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal',note_on_ad='Pelota de tenis')

    @tags('controlpanel', 'whitelist')
    def test_ad_with_notes_whitelist_user(self, wd):
        """User publish a new ad with numbers, keep in whitelist, ad goes to whitelist"""

        email = '1_inserted_keep_whitelist@yapo.cl'
        ads = [
            ('Subject con numeros 3232322', 'Body con numeros 3232322', 7500),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_ad_with_notes_whitelist_user(self, wd):
        """User publish a new ad hidden phone, keep in whitelist, ad goes to whitelist"""

        email = '1_inserted_keep_whitelist@yapo.cl'
        ads = [
            ('Ad hidden phone', 'body Ad hidden phone', {'phone_hidden':True, 'category':6120, 'price':1500}),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_ad_with_notes_whitelist_user(self, wd):
        """User publish a new ad in others category, keep in whitelist, ad goes to whitelist"""

        email = '1_inserted_keep_whitelist@yapo.cl'
        ads = [
            ('Ad in other category', 'Body Ad in other category', {'category':8020, 'price':1500}),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_ad_without_green_price_whitelist_user(self, wd):
        """User on whitelist, publish a car with Other brand , keep in whitelist, ad goes to normal"""

        email = '1_without_greenprice_other_whitelist_queue@yapo.cl'
        ads = [
            ('Auto brand otro', 'Auto desconocido modelo otro.', {
                'brand': '0',
                'regdate': '2012',
                'gearbox': '1',
                'fuel': '1',
                'cartype': '1',
                'mileage': '15000',
                'price': '4580000',
                'plates': 'ABCD10',
            })
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_edit_and_accept_ads_with_valid_reason(self, wd):
        """User publish a new ad, its accept with changes (reason = 'SIN IM�GENES', keep in whitelist, ad goes to whitelist"""

        email = '1_inserted_keep_whitelist@yapo.cl'
        ads = [
            ('Ad edit and accept with BORRAMOS LINKS Y CORREOS', 'Body Ad edit and accept with BORRAMOS LINKS Y CORREOS', 1500),
            ('Ad 2 go to whitelist', 'body Ad 2 go to whitelist', 1500),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', True, True, 'whitelist')
