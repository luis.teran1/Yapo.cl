# -*- coding: latin-1 -*-
from functional.control_panel.whitelist.whitelist import WhitelistTest
from yapo.decorators import tags
from yapo import utils

class AccountNotInWhitelistOtherCountry(WhitelistTest):

    snaps = ['accounts', 'diff_whitelist']
    bconfs = {
        # We are cheating, IP will always be UNK, but we change the expected country
        "*.*.ai.whitelist.expected_country": "PE",
        '*.trans.clear.enqueue.delay':'0'
    }

    @tags('controlpanel', 'whitelist')
    def test_not_chilean_ip(self, wd):
        """User has 3 ads, insert one from foreign IP, user doesn't enter to whitelist"""
        email = 'user_to_insert_ad_foreign_ip@yapo.cl'
        ads = [
            ('Aviso Peruano', 'Si pe!', 99999)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')


class AccountNotInWhitelist(WhitelistTest):

    snaps = ['accounts', 'diff_whitelist']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'whitelist')
    def test_new_user_insert_4_ads(self, wd):
        """User has no ads, inserts 3, accept all 3, user goes to whitelist.
           Next ad is auto approved"""

        email = 'user_without_ads_not_in_whitelist@yapo.cl'
        ads = [
            ('Ropa de bebe', 'ropa de bebe', 15000),
            ('Sandwich de palta', 'Sin queso', 2500),
            ('Monitor samsung', 'se ve feoooo', 20000),
            ('Auto de Juan', 'Corre duro, huele feo', {
                'brand': '58',
                'model': '23',
                'version': '0',
                'regdate': '2010',
                'gearbox': '2',
                'fuel': '1',
                'cartype': '1',
                'mileage': '15000',
                'price': '23000000',
                'plates':'ABCD10',
            })
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, True, 'whitelist')

    @tags('controlpanel', 'whitelist')
    def test_older_than_3_months(self, wd):
        """User has 2 ads, one recent and one older than 3 months, 3rd ad doesn't go to whitelist"""

        email = 'border_user@yapo.cl'
        ads = [
            ('Rosa Espinoza', 'Toda una dama', 55000)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_rejection_in_3_months(self, wd):
        """User has 3 ads, 1 is rejected, 4th is accepted, 5th doesn't go to whitelist"""

        email = 'user_to_reject_ad@yapo.cl'
        ads = [
            ('Mesa de centro Madaka', 'Cuenta con tecnologia  hecha por un madafaka', 50000),
            ('Tarta de queso', 'sin nata', 4000),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_notes_in_ad(self, wd):
        """User has 3 ads, add a note to one of them, 4th doesn't go to whitelist"""

        email = 'user_to_insert_ad_notes@yapo.cl'
        ads = [
            ('Tarta de queso', 'con nata', 4500),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal', note_on_ad='Mu�eca inflable pal diego')

    @tags('controlpanel', 'whitelist')
    def test_price_under_luka(self, wd):
        """User has 3 ads, insert 4th with price < 1000, user doesn't go to whitelist"""

        email = 'user_to_price_under_1000@yapo.cl'
        ads = [
            ('Moco verde', 'jugoso', 999)
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_forbidden_words(self, wd):
        """User has 3 ads, publish an ad with forbidden words, user doesn't got to whitelist"""

        email = 'user_to_ad_forbidden_words@yapo.cl'
        ads = [
            ('Mira mis badwords', 'Tri mamahuevo', 553322),
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_duplicated_ad(self, wd):
        """User has 3 ads, insert duplicated subject/body, user doesn't go to whitelist"""

        email = 'user_to_ad_duplicate_warning@yapo.cl'
        ads = [
            ('Force ball', 'para ejercicios de fap', 25000)
        ]
        utils.bconf_overwrite('*.*.delete2bump.active.duplicate.enable', 1)
        utils.bconf_overwrite('*.*.delete2bump.inactive.duplicate.enable', 1)
        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'duplicated_active')

    @tags('controlpanel', 'whitelist')
    def test_price_not_green(self, wd):
        """User has 3 ads, 4th has not green price, user doesn't go to whitelist"""

        email = 'user_with_ad_without_green_price@yapo.cl'
        ads = [
            ('Autito', 'Confortable', {
                'brand': '58',
                'model': '23',
                'version': '12',
                'regdate': '2012',
                'gearbox': '2',
                'fuel': '1',
                'cartype': '1',
                'mileage': '15000',
                'price': '9960000',
                'plates':'ABCD10',
            })
        ]

        self._whitelist_test_case(wd, email, ads, 'accept', False, False, 'normal')

    @tags('controlpanel', 'whitelist')
    def test_user_with_deleted_ads(self, wd):
        """User has 3 ads, 2 are deleted, insert new ad, user enters whitelist"""

        email = 'user_with_approved_deleted_ads@yapo.cl'
        ads = [
            ('Taza de cafe', 'con chocolate y pizza', 10000)
        ]

        self._whitelist_test_case(wd, email, ads, 'edit_and_accept', False, True, 'whitelist')
