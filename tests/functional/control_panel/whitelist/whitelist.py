# -*- coding: latin-1 -*-
import yapo
from yapo.pages.desktop import Login, EditPasswordPage, AdEdit, DashboardMyAds
from yapo.pages.control_panel import ControlPanel, ControlPanelWhitelist
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from selenium.common import exceptions
from yapo import utils
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI

class WhitelistTest(yapo.SeleniumTest):

    def _whitelist_user_validation(self, cp, cp_whitelist, mail, exists):
        cp.go()
        cp.go_to_option_in_menu("Lists")
        cp_whitelist.go_link_text("whitelist")
        cp_whitelist.search_email.send_keys(mail)
        cp_whitelist.search.click()
        if exists:
            cp_whitelist.wait.until_present('search_results')
            self.assertEqual(mail, cp_whitelist.search_results.text)
        else:
            self.assertRaises(exceptions.NoSuchElementException, lambda: cp_whitelist.search_results)

    def _whitelist_ad_queue_validation(self, cp, cp_whitelist, subject, queue):
        cp.search_for_ad_by_subject_in_queue(subject, 'Unreviewed')

        self.assertIn(subject, cp_whitelist.whitelist_verify.text)
        self.assertIn('Ad history\nQueue: {0}'.format(queue), cp_whitelist.whitelist_verify.text)

    def _insert_ad(self, wd, subject, body, price=None, car=None, email=None, salted_passwd=None):
        if car:
           if isinstance(car, dict) and 'category' in car:
             category = car.pop('category')
           else:
             category = 2020
        else:
            category = 3040
        if price and not car:
            car = { 'price': price }

        print("{0} ({1}) [{2}]".format(subject, category, car))

        data = {
            'subject': subject,
            'body': body,
            'region': 15,
            'communes': 311,
            'category': category,
            'accept_conditions': True,
        }
        data.update(car)
        if email and salted_passwd:
            data.update({'email': email, 'passwd': salted_passwd})
            insert_an_ad(wd, **data)
        else:
            page = AdInsert(wd)
            page.go()
            page.insert_ad(
                        subject=subject,
                        body=body,
                        region=15,
                        communes=311,
                        category=category,
                        accept_conditions=True,
                        **car
                    )
            page.submit.click()
            result = AdInsertResult(wd)
            self.assertTrue(result.was('success'))

    def _insert_ads(self, wd, ads, email=None, salted_passwd=None):
        for subject, body, price_or_car in ads:
            if isinstance(price_or_car, dict):
                self._insert_ad(wd, subject, body, car=price_or_car, email=email, salted_passwd=salted_passwd)
            else:
                self._insert_ad(wd, subject, body, price=price_or_car, email=email, salted_passwd=salted_passwd)

    def _whitelist_test_case(self, wd, email, ads, action, on_whitelist_before, on_whitelist_after, expected_queue, note_on_ad=None, hiden_phone=None):
        salted_passwd = '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'
        # Insert first N-1 ads
        self._insert_ads(wd, ads[:-1], email=email, salted_passwd=salted_passwd)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')

        # Are we on whitelist before doing anything?
        cp_whitelist = ControlPanelWhitelist(wd)
        self._whitelist_user_validation(cp, cp_whitelist, email, on_whitelist_before)

        # If we are to refuse and ad, reason is Virus
        if action == 'refuse':
           reason = 'Virus'
        elif action == 'edit_and_accept':
           reason = 'BORRAMOS LINKS Y CORREOS'
        else:
            reason = None

        # Apply action to first N-1 ads
        for subject, _, _ in ads[:-1]:
            cp.go()
            cp.search_and_review_ad(email, subject, action, refusal_reason_name=reason)

        # Ad notes to an specific ad
        if note_on_ad:
            cp.go()
            cp.add_note_to_ad(note_on_ad, "D'oh!")

        # Insert last ad
        cp.logout()
        self._insert_ads(wd, ads[-1:], email=email, salted_passwd=salted_passwd)

        # Are we on whitelist after doing everything?
        cp.go_and_login('dany', 'dany')
        cp_whitelist = ControlPanelWhitelist(wd)
        self._whitelist_user_validation(cp, cp_whitelist, email, on_whitelist_after)

        # Did the ad land on the expected queue?
        cp.search_for_ad_by_subject_in_queue(ads[-1][0], 'Unreviewed')
        self.assertIn(ads[-1][0], cp_whitelist.whitelist_verify.text)
        self.assertIn('Ad history\nQueue: {0}'.format(expected_queue), cp_whitelist.whitelist_verify.text)

    def _whitelist_ad_in_dashboard(self, wd, email, ad, on_whitelist_before):
        login = Login(wd)
        login.login(email, '123123123')
        salted_passwd = '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'

        self._insert_ads(wd, [ad], email=email, salted_passwd=salted_passwd)

        cp_whitelist = ControlPanelWhitelist(wd)
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        self._whitelist_user_validation(cp, cp_whitelist, email, on_whitelist_before)

        utils.rebuild_asearch()

        dashboardmyads = DashboardMyAds(wd)
        dashboardmyads.go_dashboard()
        self.assertIn(ad[0], ' '.join([ad.text for ad in dashboardmyads.list_active_ads]))

class WhitelistVSAutoaccept(WhitelistTest):

      def _edit_ad(self, wd, list_id, password, params_to_change):
          edit_password_page = EditPasswordPage(wd)
          edit = AdEdit(wd)

          edit_password_page.go(list_id = list_id)
          edit_password_page.validate_password(password)
          if params_to_change:
             if 'upload_image' in params_to_change:
                num_imgs = params_to_change['upload_image'] if params_to_change['upload_image'] else 1
                params_to_change.pop('upload_image')
                [edit.upload_resource_image('image_order/01.png') for img in range(num_imgs)]

             edit.insert_ad(**params_to_change)

          edit.wait.until_visible('combo_upselling_box')
          edit.wait.until(lambda x: edit.combo_upselling_box.size['height'] >= 265,
            print("Waiting for combo_upselling_box animation to end... Size: {}".format(edit.combo_upselling_box.size)))
          edit.submit.click()
          result = AdInsertResult(wd)
          self.assertTrue(result.was('success'))

      def _whitelist_test_case_edit(self, wd, email, subject, list_id, password_ad, expected_queue, params_to_change=None):
          #edit an ad
          self._edit_ad(wd, list_id, password_ad, params_to_change)

          cp = ControlPanel(wd)
          cp.go_and_login('dany', 'dany')
          #User should be in whitelist
          cp_whitelist = ControlPanelWhitelist(wd)
          self._whitelist_user_validation(cp, cp_whitelist, email, True)

          cp.search_for_ad_by_subject_in_queue(subject=subject, queue='Unreviewed')
          cp.wait.until_present('list_table_ads')
          self.assertIn('Queue: {0}'.format(expected_queue), cp.list_table_ads[0].text)

      def _whitelist_test_case_refuse_edit_ad(self, wd, email, subject, list_id, password_ad, expected_queue_editad, expected_queue_newad, params_to_change_editad=None):
          """
          Edition of ad is refuse, then the new ad go to Normal queue and user go out whitelist.
          """

          self._whitelist_test_case_edit(wd, email, subject, list_id, password_ad, expected_queue_editad, params_to_change_editad)

          cp_whitelist = ControlPanelWhitelist(wd)
          cp = ControlPanel(wd)

          #Refuse the ad edition
          cp.go()
          cp.search_and_review_ad(email, subject, 'refuse', queue='Unreviewed')
          cp.logout()

          #Insert new ad
          subject_new_ad = '{0} {1}'.format('new ad', subject)
          body_new_ad = '{0} {1}'.format('Body new ad', subject)
          self._insert_ads(wd, [(subject_new_ad, body_new_ad, 10)])

          #User should be not in whitelist
          cp.go_and_login('dany', 'dany')
          self._whitelist_user_validation(cp, cp_whitelist, email, False)

          #The new ad go to normal queue
          cp.search_for_ad_by_subject_in_queue(subject=subject_new_ad, queue='Unreviewed')
          cp.wait.until_present('list_table_ads')
          self.assertIn('Queue: {0}'.format(expected_queue_newad), cp.list_table_ads[0].text)
