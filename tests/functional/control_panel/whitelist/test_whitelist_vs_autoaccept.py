# -*- coding: latin-1 -*-
from functional.control_panel.whitelist.whitelist import WhitelistTest, WhitelistVSAutoaccept
from yapo.decorators import tags

class AccountNotInWhitelist(WhitelistVSAutoaccept):

    snaps = ['accounts', 'diff_whitelist']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_with_bad_words(self, wd):
        """User in whitelist. Edit an ad adding badwords"""
        params = {
            'wd': wd,
            'email': 'edit_with_badwords@yapo.cl',
            'subject': 'Aviso para badwords',
            'password_ad': '123123123',
            'expected_queue':'whitelist',
            'list_id':'8000115',
            'params_to_change': {
                'name':'analakrobat'
            }
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_add_images(self, wd):
        """User in whitelist. Edit an ad adding images"""
        params = {
            'wd': wd,
            'email': 'edit_add_image@yapo.cl',
            'subject': 'Aviso to add image',
            'password_ad': '123123123',
            'expected_queue': 'whitelist',
            'list_id': '8000116',
            'params_to_change':{
                'upload_image': 3,
                'body': 'Body \n with \n whitespace\n '
            }
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_without_changes(self, wd):
        """User in whitelist. Edit an ad changing nothing"""
        params = {
            'wd': wd,
            'email': 'edit_nothing@yapo.cl',
            'subject': 'Aviso para hacer nada',
            'password_ad': '123123123',
            'expected_queue': 'autoaccept',
            'list_id': '8000117',
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_car_without_price(self, wd):
        """User in whitelist. Edit a car ad without price changing nothing"""
        params = {
            'wd': wd,
            'email': 'edit_nothing_free_car@yapo.cl',
            'subject': 'Auto gratis sin cambios',
            'password_ad': '123123123',
            'expected_queue': 'autoaccept',
            'list_id': '8000118',
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_car_with_price(self, wd):
        """User in whitelist. Edit a car ad with price changing nothing"""
        params = {
            'wd': wd,
            'email': 'edit_nothing_regular_car@yapo.cl',
            'subject': 'Auto normal no edit',
            'password_ad': '123123123',
            'expected_queue': 'autoaccept',
            'list_id': '8000119',
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_edit_car_lowering_price_keep_green(self, wd):
        """User in whitelist. Edit a car ad lowering price but keeping it green in autoaccept"""
        params = {
            'wd': wd,
            'email': 'edit_lower_car_price_still_green@yapo.cl',
            'subject': 'Auto verde lower price',
            'password_ad': '123123123',
            'expected_queue': 'autoaccept',
            'list_id': '8000120',
            'params_to_change':{
                'price': '6807735'
            }
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_car_price_not_green_autoaccept(self, wd):
        """User in whitelist. Edit a car lowering price until is not green in autoaccept, but green in whitelist"""
        params = {
            'wd': wd,
            'email': 'edit_lower_car_price_not_green@yapo.cl',
            'subject': 'Auto green para no green',
            'password_ad': '123123123',
            'expected_queue': 'whitelist',
            'list_id': '8000121',
            'params_to_change':{
                'price': '6727644'
            }
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_car_price_not_green_autoaccept_whitelist(self, wd):
        """User in whitelist. Edit a car lowering price until is not green in autoaccept and not green in whitelist"""
        params = {
            'wd': wd,
            'email': 'edit_totaly_not_green_user@yapo.cl',
            'subject': 'Car to price not green anywhere',
            'password_ad': '123123123',
            'expected_queue': 'edit',
            'list_id': '8000123',
            'params_to_change':{
                'price': '13000'
            }
        }
        self._whitelist_test_case_edit(**params)

    @tags('controlpanel', 'whitelist', 'autoaccept')
    def test_refused_ad_go_out_whitelist(self, wd):
        """User in whitelist. Edit an ad changing body. Edit is refused. Insert a new ad"""
        params = {
            'wd': wd,
            'email': 'edit_to_refuse_user@yapo.cl',
            'subject': 'Aviso to edit refuse',
            'password_ad': '123123123',
            'list_id': '8000124',
            'expected_queue_editad': 'whitelist',
            'expected_queue_newad': 'normal',
            'params_to_change_editad':{
                'body': 'Body \n with \n whitespace\n '
            }
        }
        self._whitelist_test_case_refuse_edit_ad(**params)
