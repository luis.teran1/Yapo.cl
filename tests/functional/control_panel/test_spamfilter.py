# -*- coding: latin-1 -*-
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages import desktop, mobile
from yapo.pages.mails import AdReplyEmail
from yapo.pages.control_panel import ControlPanel, ControlPanelSpamfilter
from yapo import utils, conf
import yapo


class Spamfilter(yapo.SeleniumTest):
    snaps = ['android']
    bconfs = { '*.*.safe.enabled':1, '*.adreply.spam.minutes':0 }

    def test_adreply_abuse(self, wd):
        """ Uses reports abuse on adreply mail and user is included in spamfilter """

        page = AdViewDesktop(wd)
        page.go('6394118')
        page.send_adreply("Jorge", "hola@mundo.cl", "66273075", "Hi, this is a scam", False)

        email = AdReplyEmail(wd)
        result = email.report_abuse()
        self.assertTrue(result)

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')

        cp = ControlPanelSpamfilter(wd)
        cp.go_to_filter_spamfilter()
        cp.go_to_first_item()
        msgs = cp.get_spam_messages()
        self.assertEqual(msgs[0].text, 'Hi, this is a scam')

        cp.add_to_spamfilter('because i can')
        cp.check_text_in_list('hola@mundo.cl', 'E-post adresser - spamfiltret')
        self.assertEqual(cp.listing_items[0].text, 'hola@mundo.cl')
