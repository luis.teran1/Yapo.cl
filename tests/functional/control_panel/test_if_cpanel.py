# -*- coding: latin-1 -*-
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControPanelServicesModule
import yapo


class InsertingFeeCP(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_disabled']
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "0"}

    passwd = "123123123"
    category = 2020

    def _login_cp(self, wd, is_admin=True):
        cp = ControlPanel(wd)
        cp.go()
        if is_admin:
            cp.login("blocket", "blocket")
        else:
            cp.login("revisor", "123123")

    def _scenario(self, wd, ad_id, expected_info, order, amount, use_search=False, hidden_order=False):
        self._login_cp(wd)
        srv_mdl = ControPanelServicesModule(wd)

        if use_search:
            cp = ControlPanel(wd)
            cp.search_for_ad_by_ad_id(ad_id)
            cp.table_on_search.find_element_by_link_text('Inserting fee').click()

        else:
            srv_mdl.go()
            srv_mdl.fill_form(ad_id=ad_id, service='pack2if')
            srv_mdl.apply_action.click()

        self.assertEquals(
            srv_mdl.message_success.text, 'Servicio: Activar aviso de pack')

        calculated_info = srv_mdl.info.text
        for info in expected_info:
            self.assertIn(info, calculated_info)

        if hidden_order:
            self.assertFalse(srv_mdl.is_element_present("service_order"))
            self.assertFalse(srv_mdl.is_element_present("service_amount"))
            self.assertFalse(srv_mdl.is_element_present("apply_action"))

        if order is not None or amount is not None:
            srv_mdl.fill_form(service_order=order, service_amount=amount)

            srv_mdl.apply_action.click()
            self.assertEquals(
                srv_mdl.message_success.text, 'Tu aviso fue promovido')

            success_message = "Producto aplicado: Inserting fee"
            if order != "" and amount != "":
                success_message += " nro de orden {} y monto {}".format(order, amount)

            self.assertEquals(
                srv_mdl.success_detail.text,
                success_message)

    def test_account_w_slots(self, wd):
        """ IF can be applied if user have slots """
        ad_id = 134;
        expected_info = [
            "Audi A4 2011 (cat: 2020, status:disabled)",
            "cuentaproconpack (email:cuentaproconpack@yapo.cl)",
            "active (Cupos disponibles:9)",
        ]
        self._scenario(wd, ad_id, expected_info, None, None, True)

    def test_account_wo_slots(self, wd):
        """ IF success for user without slots """
        # cuentaprosincupos@yapo.cl
        ad_id = 135;
        expected_info = [
            "Camion sin cupo (cat: 2040, status:disabled)",
            "cuentaprosincupos (email:cuentaprosincupos@yapo.cl)",
            "active (Cupos disponibles:0)",
        ]
        self._scenario(wd, ad_id, expected_info, "123", "123")

    def test_account_wo_pack(self, wd):
        """ IF success for user without pack """
        # cuentaprosinpack@yapo.cl
        ad_id = 136;
        expected_info = [
            "Camion sin pack (cat: 2040, status:disabled)",
            "cuentaprosinpack (email:cuentaprosinpack@yapo.cl)",
            "Sin pack (Cupos disponibles:0)",
        ]
        self._scenario(wd, ad_id, expected_info, "123", "123")

    def test_wo_account(self, wd):
        """ IF success for user without account """
        # prosincuenta@yapo.cl
        ad_id = 137
        expected_info = [
            "Camion sin cuenta (cat: 2040, status:disabled)",
            "(email:)",
            "(Cupos disponibles:)",
        ]
        self._scenario(wd, ad_id, expected_info, "123", "123")

    def test_active_ad(self, wd):
        """ IF cannot be applied if the ad is active """
        ad_id = 133
        expected_info = [
            "Chery Tiggo 2007 (cat: 2020, status:active)",
            "cuentaprosincupos (email:cuentaprosincupos@yapo.cl)",
            "active (Cupos disponibles:0)",
        ]
        self._scenario(wd, ad_id, expected_info, None, None)

    def test_inmo_case(self, wd):
        """ IF for inmo category """
        email = "cuentaproinmosinpack@yapo.cl"
        ad_id = 138
        expected_info = [
            "Pieza pro inmo (cat: 1240, status:disabled)",
            "cuentaproinmo (email:cuentaproinmosinpack@yapo.cl)",
            "Sin pack (Cupos disponibles:0)",
        ]
        self._scenario(wd, ad_id, expected_info, "", "")

    def test_admin_wo_privs_search(self, wd):
        """ Admin wo privs can not select IF on search module"""
        ad_id = 134;
        self._login_cp(wd, is_admin=False)

        cp = ControlPanel(wd)
        cp.search_for_ad_by_ad_id(ad_id)
        self.assertFalse(cp.is_element_present("inserting_fee"), 'Element Inserting fee was found')

    def test_admin_wo_privs_module(self, wd):
        """ Admin wo privs can not select IF on services module"""
        ad_id = 134;
        self._login_cp(wd, is_admin=False)

        srv_mdl = ControPanelServicesModule(wd)
        srv_mdl.go()

        all_options = Select(srv_mdl.service).options
        text_option = "Activar aviso de pack"
        found = False
        for option in all_options:
            if text_option in option.text:
                found = True
        self.assertFalse(found, "The option was found for an admin without privs")
