# -*- coding: latin-1 -*-
from yapo.pages.control_panel import ControlPanel, ControlPanelSearchForAd
from yapo import SeleniumTest
from yapo.decorators import tags


class ControlpanelDeleteAds(SeleniumTest):

    snaps = ['accounts']

    @tags('controlpanel', 'ads', 'mass_delete')
    def test_cp_delete_ads(self, wd):
        """ Test the mass delete option from control panel """

        cp = ControlPanel(wd)
        cp.go_and_login('dany','dany')
        cp.go_to_option_in_menu("Search for ad")
        cp.search_for_ads(email='many@ads.cl', search_interval='all')
        self.assertNotIn('Deleted, 2', cp.tabarea.text)
        for id in {'chk_0_0', 'chk_0_1', 'chk_1_0', 'chk_1_1'}:
            wd.find_element_by_id(id).click()
        cp.delete_selected.click()
        for ad in {"Deleted ad 100.", "Deleted ad 99.", "Ad 107 is not published; can't delete.", "Ad 108 is not published; can't delete."}:
            self.assertIn(ad, cp.module_content.text)
