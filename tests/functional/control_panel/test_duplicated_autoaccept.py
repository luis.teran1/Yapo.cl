# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.control_panel import ControlPanel
from yapo import utils
import yapo


class AdInsertDuplicatedAdAutoacceptCategory(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop

    snaps = ['accounts', 'diff_autoaccept_category_5020']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    isdesktop = True
    @tags('ad_insert', 'desktop', 'duplicate', 'autoaccept')
    def test_duplicated_ad_autoaccept_category_goes_to_duplicated_queue(self, wd):
        """ Insert an ad in autoaccept category, then insert another just like the other and check the queues """
        utils.bconf_overwrite('*.*.delete2bump.active.duplicate.enable', 1)
        utils.bconf_overwrite('*.*.delete2bump.inactive.duplicate.enable', 1)
        insert_an_ad(wd, AdInsert=DesktopAI, category="5020")
        insert_an_ad(wd, AdInsert=DesktopAI, category="5020")
        # REVIEW ON CP QUEUES
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_queue('Automatically accept')
        # Check duplicated
        self.assertEqual(1, len(cp.duplicated_ads))
        cp.go()
        cp.go_to_queue('Duplicated by inactive')
        # Check duplicated
        self.assertEqual(1, len(cp.duplicated_ads))
