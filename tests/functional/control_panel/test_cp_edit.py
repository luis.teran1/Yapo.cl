# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess
from yapo.pages.desktop_list import AdViewDesktop, AdListDesktop
from yapo.pages.desktop import HeaderElements, Login
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo import utils
import yapo


class ControlpanelTests(yapo.SeleniumTest):

    snaps = ['accounts']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'ad_edit', 'ad_insert')
    def test_cp_edition_logged_in(self, wd):
        """ Edit an ad while logged in on yapo should not steal the ad """

        site_login = { 'user': 'prepaid5@blocket.se', 'password': '123123123' }
        cp_login   = {'user': 'dany', 'password': 'dany' }
        list_id = '8000049'
        expected_logged = 'BLOCKET'
        expected_email = 'android@yapo.cl'
        edited_subject = 'Very edited subject'
        expected_extra_msg = 'Logged in as {}'.format(cp_login['user'])

        # Login on yapo
        login = Login(wd)
        login.login(**site_login)

        # Login on controlpanel and go to edit ad
        cp = ControlPanel(wd)
        cp.go_and_login(**cp_login)
        cp.search_by_list_id_and_go_edit(list_id)

        # Verify we are logged from controlpanel
        ad_insert = AdInsert(wd)
        self.assertNotVisible(ad_insert, 'login_box')
        print("extra_msg: {}".format(ad_insert.extra_msg.text))
        self.assertEqual(ad_insert.extra_msg.text, expected_extra_msg)

        # Verify we are logged on yapo
        header = HeaderElements(wd)
        logged = header.who_is_logged()
        print("logged: {}".format(logged))
        self.assertEqual(logged, expected_logged)

        # Verify the original ad email is unchanged
        email = ad_insert.val('email')
        print("email: {}".format(email))
        self.assertEqual(email, expected_email)

        # Edit the ad
        ad_insert.insert_ad(subject=edited_subject)
        ad_insert.submit.click()

        # Verify edition preserved the email
        cp.search_for_ad_by_list_id(list_id)
        ads = cp.get_table_data('table_on_search')[0][3].split('\n')
        print("ads: {}".format(ads))
        self.assertEqual(ads[0], edited_subject)
        self.assertIn(expected_email, ads[3])

    @tags('controlpanel', 'ad_edit', 'ad_view', 'ad_insert')
    def test_cp_edition_4020(self, wd):
        """ Inserting an ad in category 4020 then edit by cp and see the changes in adview """

        insert_an_ad(wd, AdInsert=DesktopAI, category='4020')
        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, '¡Gracias por publicar!')

        # going to control panel put a price on the ad and accept
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_queue("First Experience")
        cp.price.send_keys("40000")
        cp.review_ad('accept')
        utils.rebuild_index()

        #go to ad_view and assert the new price
        ad_list = AdListDesktop(wd)
        ad_view = AdViewDesktop(wd)
        ad_list.go()
        ad_list.go_to_ad(subject='My ad')
        self.assertEqual(ad_view.price_final.text, '$ 40.000')

    @tags('controlpanel', 'ad_edit', 'ad_insert')
    def test_cp_edition_7020(self, wd):
        """ Edit an ad in category 7020 from controlpanel """

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_by_list_id_and_go_edit('8000049')
        page = AdInsert(wd)
        self.assertNotVisible(page, 'login_box')
