# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop import AdEdit
from yapo.pages.generic import SentEmail
from yapo.pages.newai import DesktopAI
from yapo.steps import trans, insert_an_ad
import yapo, time

class AdQueues(yapo.SeleniumTest):

    bconfs = {"*.trans.clear.enqueue.delay" : '0'}

    @tags('controlpanel', 'adqueues')
    def test_ad_queue_duplicates(self, wd):
        """ Tests that ads are being detected on ad_queues """
        # ESTE ERA EL PRIMER TEST EN REGRESS/FINAL
        yapo.utils.trans('db_snap', action='snap', name='final')
        yapo.utils.load_snap('adqueues')
        yapo.utils.rebuild_asearch()
        cp = ControlPanel(wd)
        cp.go()
        cp.login('blocket', 'blocket')
        cp.better_go_to_no_lock_queue('normal')
        self.assertEquals(cp.get_duplicated_values(), ['92%', '100%', '100%', '92%', '100%', '100%', '92%', '50%', '0%'])
        self.assertEquals(cp.get_general_scores(), ['(82.75)', '(82.75)', '(-11.25)'])
        wd.find_element_by_partial_link_text('Next').click()
        self.assertEquals(cp.get_duplicated_values(), [])
        cp.logout()

        # SEGUNDO TEST EN REGRESS/FINAL
        AdEdit(wd).edit_an_ad('7910234', '11111',
            {'category': '7040', 'jc': ['19', '20'], 'communes':'Arica', 'area_code':'2', 'phone':'62184762'})

        # TERCER TEST EN REGRESS/FINAL
        cp = ControlPanel(wd)
        cp.go()
        cp.login('blocket', 'blocket')
        cp.better_go_to_queue('edit')
        self.assertEquals(cp.subject.text, "Testannons 1")
        self.assertEquals(cp.price.text, "")

    @tags('controlpanel', 'adqueues')
    def test_edit_queue_cases_1(self, wd):
        """ Normal edit -> go to edit queue """
        yapo.utils.trans('db_snap', action='snap', name='final')
        yapo.utils.load_snap('adqueues')
        yapo.utils.rebuild_asearch()
        AdEdit(wd).edit_an_ad('8348545', '11111',
            {'price': '9990', 'subject': 'Lorem ipsum dolor sit amet', 'communes': '217', 'phone':'962184762'})
        cp = ControlPanel(wd)
        cp.go()
        cp.login('blocket', 'blocket')
        cp.better_go_to_queue('edit')
        self.assertEquals(cp.new_title.text, "Lorem ipsum dolor sit amet")

    @tags('controlpanel', 'adqueues')
    def test_edit_queue_cases_2(self, wd):
        """ Edit-Bump -> go to edit queue """
        yapo.utils.trans('db_snap', action='snap', name='final')
        yapo.utils.load_snap('adqueues')
        yapo.utils.rebuild_asearch()
        AdEdit(wd).edit_an_ad('8348545', '11111',
            {'price': '9990', 'subject': 'Lorem ipsum dolor sit amet', 'communes': '217', 'phone':'962184762'}, do_bump=True)
        cp = ControlPanel(wd)
        cp.go()
        cp.login('blocket', 'blocket')
        cp.better_go_to_queue('edit')
        self.assertEquals(cp.new_title.text, "Lorem ipsum dolor sit amet")

    @tags('controlpanel', 'adqueues')
    def test_edit_queue_cases_3(self, wd):
        """ Refuse mail edit -> DO NOT go to edit queue"""
        yapo.utils.trans('db_snap', action='snap', name='final')
        yapo.utils.load_snap('adqueues')
        yapo.utils.rebuild_asearch()
        AdEdit(wd).edit_an_ad('8348545', '11111',
            {'price': '9990', 'subject': 'Lorem ipsum dolor sit amet', 'communes': '217', 'phone':'96218476'})
        trans.review_ad('user13@blocket.se', 'Orginal Mauser Oberndorf 9,3x62', action = 'refuse', reason='32')
        time.sleep(10)
        SentEmail(wd).go()
        wd.find_element_by_partial_link_text('aqu�').click()
        insert_an_ad(wd, AdInsert=DesktopAI, **{'price': '19990', 'subject': 'Lorem ipsum dolor sit amet 2', 'accept_conditions':'True'})

        cp = ControlPanel(wd)
        cp.go()
        cp.login('blocket', 'blocket')
        cp.better_go_to_queue('edit')
        self.assertEquals(cp.error.text, "Fila vac�a para su revisi�n")

    @tags('controlpanel', 'adqueues')
    def test_edit_queue_inserting_fee(self, wd):
        """ Normal edit on an Inserting fee AD goes to edit queue """
        yapo.utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1', 'diff_pack_cars1_plus_assigned_if'])
        yapo.utils.rebuild_asearch()
        AdEdit(wd).edit_an_ad('8000094', '123123123',
            {'price': '9990', 'subject': 'Inserting fee goes to edit', 'communes': '217', 'phone':'962184762'})
        cp = ControlPanel(wd)
        cp.go()
        cp.login('dany', 'dany')
        cp.better_go_to_queue('edit')
        cp.wait.until_present('real_status')
        self.assertEquals(cp.new_title.text, "Inserting fee goes to edit")
