# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel,ControlPanelRefund
import yapo
import yapo.conf
import datetime
from yapo.pages.generic import SentEmail


class ControlpanelRefund(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_upselling_cp_module']

    @tags('controlpanel', 'upselling', 'refund')
    def test_upselling_refund_module_success(self, wd):
        """ CP: Refund Module Search"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Buscar solicitudes de devoluci�n")

        cpr = ControlPanelRefund(wd)
        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='all')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['1', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '123', 'Otros productos', '- Subir Semanal -', '2.700', 'User', 'One', '100000-4', 'user.01@schibsted.cl', 'Cuenta de Ahorro', 'Rabobank', '12424-89877', '2015-07-13 11:25', 'Con error', 'Reintentar'],
                ['2', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '118', 'Otros productos', '- Vitrina 7 d�as -', '3.600', 'User2', 'One2', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Banco Santander', '5151515151551', '2015-07-13 11:26', 'Pendiente', 'Confirmar'],
                ['3', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '117', 'Otros productos', '- Subir Diario -', '4.500', 'User3', 'One3', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Rabobank', '55666996665522', '2015-07-13 11:26', 'Confirmada', 'Con error'],
                ['4', 'webpay', 'Alan', 'alan@brito.cl', '92', 'Audio, TV, video y fotograf�a', '- Subir Semanal -', '2.700', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
                ['5', 'webpay', 'Cristian', 'uid1@blocket.se', '87', 'Libros y revistas', '- Vitrina 7 d�as -', '3.600', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
                ['6', 'webpay', 'blocket', 'prepaid5@blocket.se', '95', 'Libros y revistas', '- Subir Semanal -', '2.700', 'Blocketa', 'Five', '100000-4', 'prepaid5@blocket.se', 'Libreta Ahorro', 'Corpbanca', '123124-124124-124', '2015-07-13 11:31', 'Pendiente', 'Confirmar']
        ]
        self.assertListEqual(data_to_check, table_data)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='wos')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')
        data_to_check = [
                [],
                ['4', 'webpay', 'Alan', 'alan@brito.cl', '92', 'Audio, TV, video y fotograf�a', '- Subir Semanal -', '2.700', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
                ['5', 'webpay', 'Cristian', 'uid1@blocket.se', '87', 'Libros y revistas', '- Vitrina 7 d�as -', '3.600', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
        ]

        self.assertListEqual(data_to_check, table_data)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='wsa')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['1', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '123', 'Otros productos', '- Subir Semanal -', '2.700', 'User', 'One', '100000-4', 'user.01@schibsted.cl', 'Cuenta de Ahorro', 'Rabobank', '12424-89877', '2015-07-13 11:25', 'Con error', 'Reintentar'],
                ['2', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '118', 'Otros productos', '- Vitrina 7 d�as -', '3.600', 'User2', 'One2', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Banco Santander', '5151515151551', '2015-07-13 11:26', 'Pendiente', 'Confirmar'],
                ['3', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '117', 'Otros productos', '- Subir Diario -', '4.500', 'User3', 'One3', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Rabobank', '55666996665522', '2015-07-13 11:26', 'Confirmada', 'Con error'],
                ['6', 'webpay', 'blocket', 'prepaid5@blocket.se', '95', 'Libros y revistas', '- Subir Semanal -', '2.700', 'Blocketa', 'Five', '100000-4', 'prepaid5@blocket.se', 'Libreta Ahorro', 'Corpbanca', '123124-124124-124', '2015-07-13 11:31', 'Pendiente', 'Confirmar']
        ]
        self.assertListEqual(data_to_check, table_data)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='wsc')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['3', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '117', 'Otros productos', '- Subir Diario -', '4.500', 'User3', 'One3', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Rabobank', '55666996665522', '2015-07-13 11:26', 'Confirmada', 'Con error'],
        ]
        self.assertListEqual(data_to_check, table_data)


        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='wsf')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['1', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '123', 'Otros productos', '- Subir Semanal -', '2.700', 'User', 'One', '100000-4', 'user.01@schibsted.cl', 'Cuenta de Ahorro', 'Rabobank', '12424-89877', '2015-07-13 11:25', 'Con error', 'Reintentar'],
        ]
        self.assertListEqual(data_to_check, table_data)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='wsp')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['2', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '118', 'Otros productos', '- Vitrina 7 d�as -', '3.600', 'User2', 'One2', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Banco Santander', '5151515151551', '2015-07-13 11:26', 'Pendiente', 'Confirmar'],
                ['6', 'webpay', 'blocket', 'prepaid5@blocket.se', '95', 'Libros y revistas', '- Subir Semanal -', '2.700', 'Blocketa', 'Five', '100000-4', 'prepaid5@blocket.se', 'Libreta Ahorro', 'Corpbanca', '123124-124124-124', '2015-07-13 11:31', 'Pendiente', 'Confirmar']
        ]
        self.assertListEqual(data_to_check, table_data)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-01 23:59:59', filter='all')
        cpr.search_button.click()
        cpr.wait.until_not_present('refund_table')
        self.assertEquals("Busqueda sin resultados", cpr.without_result.text)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='all')
        cpr.search_button.click()
        table_data = cpr.get_table_data('refund_table')
        cpr.fill_form(email='daniberto@gemail.com')
        cpr.search_email.click()
        self.assertEquals('Email enviado a daniberto@gemail.com',cpr.email_success.text)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='all')
        cpr.search_button.click()
        cpr.click_on_link('id_2')
        cpr.click_on_link('id_4')

        table_data = cpr.get_table_data('refund_table')

        data_to_check = [
                [],
                ['1', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '123', 'Otros productos', '- Subir Semanal -', '2.700', 'User', 'One', '100000-4', 'user.01@schibsted.cl', 'Cuenta de Ahorro', 'Rabobank', '12424-89877', '2015-07-13 11:25', 'Pendiente', 'Confirmar'],
                ['2', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '118', 'Otros productos', '- Vitrina 7 d�as -', '3.600', 'User2', 'One2', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Banco Santander', '5151515151551', '2015-07-13 11:26', 'Pendiente', 'Confirmar'],
                ['3', 'webpay', 'Usuario 01', 'user.01@schibsted.cl', '117', 'Otros productos', '- Subir Diario -', '4.500', 'User3', 'One3', '100000-4', 'user.01@schibsted.cl', 'Cuenta Vista', 'Rabobank', '55666996665522', '2015-07-13 11:26', 'Con error', 'Reintentar'],
                ['4', 'webpay', 'Alan', 'alan@brito.cl', '92', 'Audio, TV, video y fotograf�a', '- Subir Semanal -', '2.700', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
                ['5', 'webpay', 'Cristian', 'uid1@blocket.se', '87', 'Libros y revistas', '- Vitrina 7 d�as -', '3.600', '-', '-', '-', '-', '-', '-', '-', '-', '-', ''],
                ['6', 'webpay', 'blocket', 'prepaid5@blocket.se', '95', 'Libros y revistas', '- Subir Semanal -', '2.700', 'Blocketa', 'Five', '100000-4', 'prepaid5@blocket.se', 'Libreta Ahorro', 'Corpbanca', '123124-124124-124', '2015-07-13 11:31', 'Pendiente', 'Confirmar']
        ]
        self.assertListEqual(data_to_check, table_data)


    @tags('controlpanel', 'upselling', 'refund')
    def test_upselling_refund_module_errors(self, wd):
        """ CP: Refund Module Search errors"""
        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.go_to_option_in_menu("Buscar solicitudes de devoluci�n")
        cpr = ControlPanelRefund(wd)


        cpr.fill_form(date_start='assdasd', date_end='oqijwdoijd', filter='all')
        cpr.search_button.click()
        self.assertEquals('La fecha es inv�lida', cpr.error_date_start.text)
        self.assertEquals('La fecha es inv�lida', cpr.error_date_end.text)

        cpr.fill_form(date_start='', date_end='', filter='all')
        cpr.search_button.click()
        self.assertEquals('Ingresa fecha de inicio', cpr.error_date_start.text)
        self.assertEquals('Ingresa fecha de termino', cpr.error_date_end.text)

        cpr.fill_form(date_start='2015-07-01 00:00:01', date_end='2015-07-15 23:59:59', filter='all')
        cpr.search_button.click()

        cpr.fill_form(email='')
        cpr.search_email.click()
        self.assertEquals('Confirma que el correo electr�nico est� en el formato correcto.',cpr.error_email.text)
