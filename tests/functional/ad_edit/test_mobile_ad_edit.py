# -*- coding: latin-1 -*-
from selenium.common.exceptions import NoSuchElementException, ElementNotVisibleException
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.mobile_list import AdViewMobile
from yapo.pages.mobile import HeaderElements
from yapo.pages.control_panel import ControlPanel
from yapo.pages.accounts import ForgotPassword
from yapo import utils, conf
import yapo
import yapo.conf
import time
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait

class MobileEditAdview(yapo.SeleniumTest):

    snaps = ['accounts']
    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'ad_view', 'edit')
    def test_edit_from_ad_view(self, wd):
        """ Check the ad edition in mobile from ad view """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        wd.execute_script("window.scrollTo(0, 100000);")
        ad_view.admin_show.click()
        ad_view.admin_edit.click()
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.validate_password('123123123')
        edit = mobile.NewAdInsert(wd)
        self.assertTrue(edit.subject.get_attribute('value') == 'Adeptus Astartes Codex')
        self.assertTrue(edit.body.get_attribute('value') == 'for teh emporer!')
        self.assertTrue(edit.price.get_attribute('value') == '778889')
        self.assertTrue(edit.category.get_attribute('value') == '6120')
        edit.fill_form(
            subject='pa k kieres saver eso jaja slds',
            body='este aviso ser� editado para comprobar muchas cositas',
            price='25000'
        )
        edit.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        #Refuse the ad edition
        cp = ControlPanel(wd)
        cp.go()
        cp.login('dany', 'dany')
        cp.search_for_ad('prepaid5@blocket.se', 'Pa k kieres saver eso jaja slds', queue='Unreviewed')
        self.assertTrue(cp.price.get_attribute('value') == '25.000')
        self.assertTrue(cp.old_texts.text == 'Adeptus Astartes Codex')
        self.assertTrue(cp.new_texts.text == 'Pa k kieres saver eso jaja slds')
        cp.review_ad('refuse')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        cp.logout()

        edit = mobile.NewAdInsert(wd)
        wd.get(conf.MOBILE_URL + '/editar-aviso/formulario/2?id=RHNDM2OWQ0ZjAwNzcxYzFmMxzk23oEhBQIpzZxZ1lhkkI=');
        edit.wait.until_visible('subject')
        self.assertTrue("Spam" in edit.edit_feedback.text)
        self.assertTrue(edit.subject.get_attribute('value') == 'Pa k kieres saver eso jaja slds')
        self.assertTrue(edit.body.get_attribute('value') == 'este aviso ser� editado para comprobar muchas cositas')
        self.assertTrue(edit.price.get_attribute('value') == '25000')
        self.assertTrue(edit.category.get_attribute('value') == '6120')
        edit.fill_form(
            subject='aviso editado por segunda vez',
            body='este aviso fue editado po y ahi ta la cosa',
            price='2500'
        )
        edit.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        cp.go()
        cp.login('dany', 'dany')
        cp.search_for_ad('prepaid5@blocket.se', 'Aviso editado por segunda vez', queue='Unreviewed')
        self.assertTrue(cp.price.get_attribute('value') == '2.500')
        self.assertTrue(cp.old_texts.text == 'Adeptus Astartes Codex')
        self.assertTrue(cp.new_texts.text == 'Aviso editado por segunda vez')
        cp.review_ad('refuse', 'Ofensivo')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        cp.logout()

        edit = mobile.NewAdInsert(wd)
        wd.get(conf.MOBILE_URL + '/editar-aviso/formulario/4?id=RHMzE3YWMzZTNhZjU0Njg0MruuXcAl3L8FU%2fBE23Ma3%2fE%3D')
        edit.wait.until_visible('edit_feedback')
        self.assertTrue("Ofensivo" in edit.edit_feedback.text)

class MobileEdit(yapo.SeleniumTest):

    snaps = ['accounts']
    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'bump', 'edit')
    def test_edit_ad_without_account(self, wd):
        """ Check the ad in edit without account and want bump selected to mobile """
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000066')
        edit_pw.validate_password('123123123')

        edit = mobile.NewAdEdit(wd)
        edit.fill_form(
            phone='87654321',
            want_bump=True,
        )
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('without_account_create'))

    @tags('mobile', 'bump', 'edit')
    def test_edit_ad_without_editing_upper_subject(self, wd):
        """ Check the ad in edit without account editing subject with UPPPER CASE """
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000066')
        edit_pw.validate_password('123123123')

        edit = mobile.NewAdEdit(wd)
        edit.fill_form(
            subject='HOLA AMIGUITO',
            phone='87654321',
            want_bump=True,
        )
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        result = mobile.AdInsertedSuccessfuly(wd)
        self.assertIn('Hola amiguito', result.text_success_message.text)

    @tags('mobile', 'ad_view', 'edit')
    def test_edit_from_ad_vew_logged(self, wd):
        """ Check the ad edition in mobile from ad view logged """
        login = mobile.Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.admin_show.click()
        ad_view.admin_edit.click()
        edit = mobile.NewAdInsert(wd)
        self.assertTrue(edit.subject.get_attribute('value') == 'Adeptus Astartes Codex')
        self.assertTrue(edit.body.get_attribute('value') == 'for teh emporer!')
        self.assertTrue(edit.price.get_attribute('value') == '778889')
        self.assertTrue(edit.category.get_attribute('value') == '6120')
        edit.fill_form(
            subject='pa k kieres saver eso jaja slds',
        )
        edit.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    @tags('mobile', 'edit')
    def test_edit_ad_session_error(self, wd):
        """ Check that in edit one ad and delete cookie s, the page redirects to mai"""
        wait = Wait(wd, 10)
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000084')
        edit_pw.validate_password('123123123')

        edit = mobile.NewAdInsert(wd)

        edit.wait.until_present('subject')
        edit.wait.until_present('body')
        edit.wait.until_present('price')

        self.assertIn('{0}/editar-aviso'.format(yapo.conf.MOBILE_URL), wd.current_url)

        #Delete cookie of the session
        wd.delete_cookie('s')
        time.sleep(1)
        edit.submit.click()

        edit.wait.until_present('subject')
        edit.wait.until_present('body')
        edit.wait.until_present('price')

        time.sleep(1)

        self.assertTrue(wait.until(lambda x: '{0}/publica-un-aviso'.format(yapo.conf.MOBILE_URL) in wd.current_url, wd.current_url))

        self.assertFalse(edit.subject.text)
        self.assertFalse(edit.body.text)
        self.assertFalse(edit.price.text)


class MobileAndroidSnapEdit(yapo.SeleniumTest):

    # defining a user agent, the user_agent property of firefox is automagically changed
    snaps = ['android']
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts', 'bump', 'edit')
    def test_edit_ad_with_account(self, wd):
        """ Check that edit one ad with three images, appears the icon to add another pictures."""
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('3456789')
        edit_pw.validate_password('123123123')

        edit = mobile.NewAdInsert(wd)

        #this ad, should have three images
        self.assertVisible(edit, 'upload')
        self.assertEquals(len(edit.images), 3)

    def test_edit_ad_image_limits(self, wd):
        """ Add more images to an ad """
        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000018')
        edit_pw.validate_password('123123123')

        edit = mobile.NewAdInsert(wd)
        for _ in range(20):
            self.assertVisible(edit, 'upload')
            edit.upload_resource_image('image_order/01.png')

        self.assertNotVisible(edit, 'upload')
        self.assertEquals(len(edit.images), 20)


class MobileMainEdit(yapo.SeleniumTest):

    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}


    def _check_msg(self, ad_insert, ad_param, error_msg):
        print("expecting {}'s error to be: {}".format(ad_param, error_msg))
        if error_msg:
            ad_insert.wait.until(lambda _: ad_insert.get_error_for(ad_param).text == error_msg) #,
                    #'{} != {}'.format(ad_insert.get_error_for(ad_param).text, error_msg))
            self.assertEqual(ad_insert.get_error_for(ad_param).text, error_msg)
        else:
            ad_insert.wait.until(lambda _: ad_insert.not_in_errors(ad_param)) #,
                    #'{} had errors: {}'.format(ad_param, ad_insert.get_error_for(ad_param).text))
            self.assertTrue(ad_insert.not_in_errors(ad_param))


    def test_edit_on_mobile(self, wd):
        """  This is the main ad edit on mobile test, here we test that:
            - An "Edit" button should be placed at the ad view page
            - A page confirming the password should be displayed before the user can edit the ad
            - The ad edit form should be prefilled with the ad data and the user personal info
            - The edit form should not have the option to change the ad password
            - The email field on the edit form should not be editable
        """

        utils.restore_snaps_in_a_cooler_way(['accounts'])
        # any category
        kwargs = {'list_id': '8000062',
                  'pwd': '123123123',
                  'region': '15',
                  'category': '6120',
                  'subject': 'Libro para Psicologo Manual Diagnostico',
                  'body': 'Vendo en Excelente estado DSM-IV-TR Manual Diagnostico y estadistico de los trastornos mentales ,,, \n\nPrecio en Locales arriba de $200.000.-\n\nOferta $ 70.000',
                  'price':'70000',
                  'name': 'OtherRegionAccount',
                 }

        self._test_edit_on_mobile(wd, **kwargs)

        # cars
        kwargs = {'list_id': '8000017',
                  'pwd': '123123123',
                  'region': '15',
                  'category': '2020',
                  'brand': '86',
                  'model': '23',
                  'version': '9',
                  'regdate': '2003',
                  'gearbox': '1',
                  'fuel': '1',
                  'cartype': '1',
                  'mileage': '94000',
                  'subject': 'Suzuki Maruti 800cc 2003',
                  'body': 'Vendo Susuki Maruti motor 800cc color Gris 5 puertas\na�o 2003 3� due�o en excelentes condiciones.\nMantenciones al d�a, papeles al d�a, sin partes ni deudas. Precio conversable.',
                  'price': '1750000',
                  'name': 'Android',
                  'phone': '7764874',
                 }

        self._test_edit_on_mobile(wd, **kwargs)

    def test_edit_data(self, wd):
        """ Testing that the ad is actually edited """

        utils.restore_snaps_in_a_cooler_way(['accounts'])
        email = 'android@yapo.cl'

        # any category
        list_id = '8000052'
        form_check = {'pwd': '123123123'}
        to_edit = {'region': '15', 'category': '6160', 'body': '8000052 edited', 'price': '60000', 'communes': '323', 'area_code':'45'}
        to_check = {'body': '8000052 edited', 'price': '$ 60.000'}
        self._edit_data_and_check(wd, list_id, email, form_check, to_edit, to_check)

        # cars
        list_id = '8000013'
        form_check = {'pwd': '123123123'}
        to_edit = {'region': '15', 'regdate': '2004', 'body': '8000013 edited', 'price': '8650000', 'communes': '323', 'area_code':'45'}
        to_check = {'description': '8000013 edited', 'price': '$ 8.650.000'}
        self._edit_data_and_check(wd, list_id, email, form_check, to_edit, to_check)


    @tags('mobile', 'ad_edit', 'validations')
    def test_ad_edit_validations(self, wd):
        """ Testing: Ad validations messages """

        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000055')
        edit_pw.validate_password('123123123')
        edit = mobile.NewAdEdit(wd)
        edit.hotizontal_select.select('parent_category', '1000')
        for field in ['parent_category', 'region', 'subject', 'body', 'name', 'phone']:
            edit.clear(getattr(edit, field))
        edit.hotizontal_select.close_sidenav()

        edit.submit.click()
        errors = { 'category': 'Selecciona una categor�a',
                   'region': 'Selecciona una regi�n',
                   'subject': 'Escribe un t�tulo',
                   'body': 'Escribe una descripci�n',
                   'name': 'Escribe tu nombre',
                   'phone': 'Escribe tu n�mero de tel�fono',
                 }

        for ad_param, error_msg in errors.items():
            self._check_msg(edit, ad_param, error_msg)

    def test_passwd_validations(self, wd):
        """ Validation messages of "ask for password" form
            - The link "La olvidaste?" of the confirming pwd page, should have the same behavior than the desktop one"""

        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go('8000017')
        edit_pw.password_input.clear()
        edit_pw.submit_button.click()
        edit_pw.wait.until(lambda wd: edit_pw.error.text)
        self.assertEqual(edit_pw.error.text, 'Debes introducir tu contrase�a')

        edit_pw.password_input.send_keys('wrongpass')
        edit_pw.submit_button.click()
        edit_pw.wait.until_text_present('La contrase�a no es correcta', edit_pw.error)
        self.assertEqual(edit_pw.error.text, 'La contrase�a no es correcta')

        edit_pw.forgot_password_link.click()
        forgot_password = ForgotPassword(wd)
        self.assertEqual(forgot_password.title.text, '�Olvidaste tu contrase�a?')

    def _test_edit_on_mobile(self, wd, **kwargs):
        """ Used to test the whole path to edit, and the prefilled of the fields, and also:
            - Validate that header buttons are working
        """

        list_id = kwargs.pop('list_id')
        pwd = kwargs.pop('pwd')

        ad_view = mobile.AdView(wd)
        ad_view.go_vi(list_id)
        wd.execute_script("window.scrollTo(0, 100000);")
        ad_view.admin_show.click()
        ad_view.admin_edit.click()

        edit_pw = mobile.EditPasswordPage(wd)
        edit_pw.go(list_id)
        edit_pw.validate_password(pwd)
        edit = mobile.NewAdEdit(wd)

        for field, value in kwargs.items():
            current_value = getattr(edit, field).get_attribute('value')
            error = 'The {0} was suppose to have the value: {1}, but it has: {2}'.format(field, value, current_value)
            self.assertEqual(current_value, value, error)

        for field in ['passwd', 'passwd_ver']:
            self.assertRaises(NoSuchElementException, lambda: getattr(edit, field).text)

        self.assertIn(edit.email.get_attribute('type'), ['hidden', 'email'])

        return edit

    def _edit_data_and_check(self, wd, list_id, email, form_check, to_edit, to_check):
        """ Going to the edit form, then accepting that ad, and checking """

        form_check['list_id'] = list_id
        ad_edit_form = self._test_edit_on_mobile(wd, **form_check)
        text = ' {list_id} edited'.format(list_id=list_id)

        # the ad subject is forced to have this value, because we used that later to check on CP
        new_subject = ad_edit_form.subject.get_attribute('value')[:30] + text
        to_edit['subject'] = new_subject

        ad_edit_form.insert_ad(**to_edit)
        ad_edit_form.submit.click()
        ad_edit_form.wait.until_loaded()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_review_ad(email, new_subject, 'accept')
        cpanel.logout()
        utils.rebuild_asearch()

        # Now checking that the ad is actually edited
        ad_view = mobile.AdView(wd)
        ad_view.go_vi(list_id)
        print('current subject: ' + ad_view.subject.text)
        print('expected subject: ' + new_subject)
        self.assertEqual(ad_view.subject.text, new_subject)
        for field, value in to_check.items():
            current_value = getattr(ad_view, field).text
            error = 'The {0} was suppose to have the value: {1}, but it has: {2}'.format(field, value, current_value)
            self.assertEqual(current_value, value, error)

