# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdEdit as AdEditMobile
from yapo.pages.desktop import AdEdit as AdEditDesktop
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.mobile_list import AdViewMobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.control_panel import ControlPanel
from yapo.steps import trans, insert_an_ad
from yapo import utils
import yapo, re


class AdEditCurrencyDesktop(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_uf']
    bconfs = {
        '*.*.common.uf_conversion_factor':'22245,5290',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    AdView = AdViewDesktop
    AdEdit = AdEditDesktop
    platform = 'desktop'

    def _edit_price(self, wd, list_id, price, currency, conversions):

        self.AdEdit(wd).edit_an_ad(list_id, '123123123', {'currency':currency, 'price':price}, None, False, submit=False)
        ae = self.AdEdit(wd)
        subject = ae.subject.get_attribute("value")
        prefix = 'UF' if currency == 'uf' else '$'
        if self.platform == 'desktop':
            self.assertEqual(ae.pesos2uf.text, conversions)
        ae.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ads(subject=subject, time_from='2016-09-20')
        wd.find_element_by_partial_link_text(subject).click()
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(price, cp.price.get_attribute('value').replace('.',''))
        self.assertEqual(prefix, cp.currency_prefix.text)

    @tags('ad_edit', 'desktop', 'currency')
    def test_edit_peso2uf(self, wd):
        """ Edits ad changing price in pesos to UF """
        list_id = 8000088
        conversions = '($ 71.193.005)'
        self._edit_price(wd, list_id, '3200,33', 'uf', conversions)

    @tags('ad_edit', 'desktop', 'currency')
    def test_edit_uf2peso(self, wd):
        """ Edits ad changing price in UF to pesos """
        list_id = 8000087
        conversions = '(UF 3.820,99)'
        self._edit_price(wd, list_id, '85000000', 'peso', conversions)

    @tags('ad_edit', 'desktop', 'currency')
    def test_edit_uf2uf(self, wd):
        """ Edits ad changing price in UF to UF """
        list_id = 8000086
        conversions = '($ 656.687)'
        self._edit_price(wd, list_id, '29,52', 'uf', conversions)

class AdEditCurrencyMobile(AdEditCurrencyDesktop):
    AdView = AdViewMobile
    AdEdit = AdEditMobile
    platform = 'mobile'
