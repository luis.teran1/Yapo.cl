# -*- coding: latin-1 -*-

from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages import desktop
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages2.common import EditionExpired
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import steps
from yapo import utils
from yapo.utils import Categories
import unittest
import yapo


class TestCantEditDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    platform = desktop
    AdInsert = AdInsertDesktop
    bconfs = {
        '*.*.cat.7020.passwd.required': '0',
        '*.*.edit_allowed_days.cat.6020': '0',
        '*.*.edit_allowed_days.cat.7020': '3',
        '*.*.edit_allowed_days.cat.3020': '7',
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    @tags('jobs', 'ad_edit', 'publish_similar')
    def test_cant_edit_redirect(self, wd):
        """ Edit an old 7020 ad gets redirected to expire information page """
        AdEdit = self.platform.AdEdit
        from_desktop = self.platform == desktop

        data = [
            ('8000045', '7020', 'Ofertas de empleo'),
            ('8000035', '3020', 'Consolas, videojuegos y accesorios'),
        ]

        for ad, cat, cat_name in data:
            wd.get('http://google.com')
            page = AdEdit(wd)
            page.edit_an_ad(ad, '123123123', submit=False)

            page = EditionExpired(wd)
            self.assertEqual(page.title.text, page.EXPIRED_HEADER)
            self.assertEqual(page.publish.text, page.PUBLISH_SIMILAR)

            edit_allowed_days = self.bconfs['*.*.edit_allowed_days.cat.' + cat]
            self.assertEqual(page.info1.text, page.EXPIRED_INFO_1.format(
                category_name=cat_name,
                edit_allowed_days=edit_allowed_days
            ))

    @tags('jobs', 'ad_edit', 'publish_similar')
    def test_edit_similar(self, wd):
        """ Publish similar ad to the edited one """
        list_id = '8000046'

        data = {
            'subject': '',
            'price': '280000',
            'category': '7020',
            'region': '15',
            'communes': '302',
            'name': 'Android',
            'email': 'android@yapo.cl',
            'phone': '7764874',
            'job_category': ['20', '24'],
            'body': 'Para trabajar en remuneraciones debe conocer y saber trabajar con planilla exel, '
                  + 'no se requiere conocimientos extras se entrenara en manejo de programa de remuneraciones, '
                  + 'trabajo de lunes a viernes de 9:00 a 18:30 horas, ubicacion 18 1/2 de la comuna de '
                  + 'La Cisterna, sueldo liquido 280.000 pesos. se contrata plazo fijo y luego indefinido.',
        }

        self._similar_ad_case(wd, list_id, data)

    @tags('jobs', 'ad_edit', 'publish_similar')
    def test_edit_similar_w_account(self, wd):
        """ Publish similar ad to the edited one (with account) """
        list_id = '6000666'

        data = {
            'subject': '',
            'price': '90000',
            'category': '6020',
            'region': '12',
            'communes': '268',
            'company_ad': '1',
            'name': 'Cristobal Col�n',
            'email': 'prepaid5@blocket.se',
            'phone': '12131491',
            'body': 'Una de 120 kg y la otra de 57, mas ligera.'
        }

        self._similar_ad_case(wd, list_id, data)

    @tags('jobs', 'ad_edit', 'publish_similar')
    def test_publish_similar_from_mail(self, wd):
        """ Publish similar ad coming from edit link on email """

        email = 'android@yapo.cl'
        subject = 'Guitarra para Play 2 Buen Estado'
        log_as = {'user': 'many@ads.cl', 'password': '123123123'}
        self._similar_ad_from_mail_case(wd, email, subject, log_as, with_upselling=False, type_password=True)

    @tags('jobs', 'ad_edit', 'publish_similar')
    def test_publish_similar_from_mail_w_account(self, wd):
        """ Publish similar ad coming from edit link on email (with account) """

        email = 'prepaid5@blocket.se'
        subject = 'Pesas muy grandes'
        log_as = {'user': 'many@ads.cl', 'password': '123123123'}
        self._similar_ad_from_mail_case(wd, email, subject, log_as, with_upselling=False, type_password=False)

    @tags('jobs', 'ad_edit', 'publish_similar', 'upselling')
    def test_publish_similar_logged_no_account_upselling(self, wd):
        """ Publish similar ad with upselling coming from edit link on email  """

        email = 'android@yapo.cl'
        subject = 'Guitarra para Play 2 Buen Estado'
        log_as = {'user': 'prepaid5@blocket.se', 'password': '123123123'}
        self._similar_ad_from_mail_case(wd, email, subject, log_as, with_upselling=True, type_password=True)

    @tags('jobs', 'ad_edit', 'publish_similar', 'upselling')
    def test_publish_similar_logged_no_account_upselling_pro(self, wd):
        """ Publish similar ad with upselling coming from edit link on email (logged as pro) """

        email = 'android@yapo.cl'
        subject = 'Guitarra para Play 2 Buen Estado'
        log_as = {'user': 'prepaid5@blocket.se', 'password': '123123123'}
        self._make_pro_for('1', '2020,7020')
        self._similar_ad_from_mail_case(wd, email, subject, log_as, with_upselling=False, type_password=True)

    def _make_pro_for(self, account_id, categories):
        utils.db_query("""
            INSERT INTO account_params (account_id, name, value)
            VALUES ('{}', 'is_pro_for', '{}')
        """.format(account_id, categories))
        utils.flush_redises()

    def _similar_ad_case(self, wd, list_id, data):

        page = self.platform.AdEdit(wd)
        page.edit_an_ad(list_id, '123123123', submit=False)

        page = EditionExpired(wd)
        page.publish_similar()

        page = self.AdInsert(wd)

        if 'job_category' in data:
            job_category = data.pop('job_category')
            self.assertEqual(page.job_category, job_category)
        for ad_param, expected in data.items():
            print('{}: {}'.format(ad_param, expected))
            self.assertEqual(page.val(ad_param), expected)

        # Complete ad form so we can submit
        update = {
            'subject': 'Modified subject',
            'passwd': '123123123',
            'passwd_ver': '123123123',
            'accept_conditions': True,
            'rut': '11111111-1',
            'create_account': True,
        }

        page.insert_ad(**update)
        page.submit.click()

        page = AdInsertResult(wd)
        self.assertTrue(page.was('success'))


    def _similar_ad_from_mail_case(self, wd, email, subject, log_as, with_upselling, type_password):
        """ Publish similar ad coming from edit link on email """

        update = {
            'subject': 'Modified subject',
            'accept_conditions': True,
            'rut': '11111111-1',
        }

        if type_password:
            update.update({
                'passwd': '123123123',
                'passwd_ver': '123123123',
            })

        myads = steps.send_my_ads_mail(wd, self.platform, email)

        login = self.platform.Login(wd)
        login.login(**log_as)

        myads.go()
        myads.list.edit(subject=subject)

        page = EditionExpired(wd)
        page.publish_similar()

        page = self.AdInsert(wd)
        page.insert_ad(**update)

        if with_upselling:
            page.combo_upselling_standard.click()

        page.submit.click()
        page = AdInsertResult(wd)
        if with_upselling:
            self.assertTrue(page.was('with_payment_products'))
        else:
            self.assertTrue(page.was('success'))


class TestCantEditMobile(TestCantEditDesktop):

    platform = mobile
    user_agent = utils.UserAgents.IOS
    AdInsert = AdInsertMobile

    @tags('jobs', 'ad_edit')
    @unittest.skip("On msite, editad loads data from the account, instead of the edited ad")
    def test_edit_similar_w_account(self, wd):
        """ Publish similar ad to the edited one (with account) """
        super().test_edit_similar_w_account(wd)

    @unittest.skip("Upselling on msite not implemented for regular users")
    def test_publish_similar_logged_no_account_upselling(self, wd):
        """ Publish similar ad with upselling coming from edit link on email  """
        pass

    @unittest.skip("Upselling on msite not implemented for regular users")
    def test_publish_similar_logged_no_account_upselling_pro(self, wd):
        """ Publish similar ad with upselling coming from edit link on email (logged as pro) """
        pass
