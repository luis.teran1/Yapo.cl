# -*- coding: latin-1 -*-

from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages import desktop
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo import utils
import yapo


class TestChangeCategoryBlock(yapo.SeleniumTest):

    snaps = ['accounts']
    platform = desktop
    AdInsert = AdInsertDesktop
    bconfs = {
        '*.*.edit_category_block.from.3020': '1',
        '*.*.edit_category_block.to.3020': '1',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('ad_edit')
    def test_change_category_block_from_message(self, wd):
        list_id = '8000065'
        data = {
            'category': '5020'
        }
        self._change_category_block_scenario(wd, list_id, data, self.AdInsert.ERROR_CANT_CHANGE_CAT_FROM)

    @tags('ad_edit')
    def test_change_category_block_to_message(self, wd):
        list_id = '6000666'
        data = {
            'category': '3020'
        }
        self._change_category_block_scenario(wd, list_id, data, self.AdInsert.ERROR_CANT_CHANGE_CAT_TO)

    def _change_category_block_scenario(self, wd, list_id, data, expected_error):

        page = self.platform.AdEdit(wd)
        page.edit_an_ad(list_id, '123123123', submit=False)

        page = self.AdInsert(wd)
        page.insert_ad(**data)
        page.submit.click()

        cat_error = page.get_error_for('category')
        print(cat_error.text)
        self.assertEqual(cat_error.text, expected_error)


class TestChangeCategoryBlockMobile(TestChangeCategoryBlock):

    platform = mobile
    user_agent = utils.UserAgents.IOS
    AdInsert = AdInsertMobile
