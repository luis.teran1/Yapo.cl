# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.pages import desktop
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.generic import SentEmail
import yapo


class SendPassAdEditDesktop(yapo.SeleniumTest):
    snaps = ['example', '1000ads']
    user_agent = utils.UserAgents.FIREFOX_MAC

    @tags('desktop', 'ad_edit', 'mail')
    def test_send_passwd_on_delete(self, wd):
        """ Using forgot passwd mail from delete to delete the ad"""
        list_id = '8349385'

        edit_pw_page = desktop.EditPasswordPage(wd)
        edit_pw_page.go(list_id)
        edit_pw_page.forgot_password.click()

        forgot_password = desktop.ForgotPassword(wd)
        forgot_password.email_input.clear()
        forgot_password.email_input.send_keys('user15@blocket.se')
        forgot_password.submit_button.click()
        forgot_password.wait.until_loaded()

        sent_email = SentEmail(wd)
        sent_email.go()

        row = sent_email.body.find_elements_by_css_selector(
            'table tr.stripe_0')[0]
        edit_link = row.find_element_by_css_selector(
            '.edit').get_attribute('href')
        wd.get(edit_link)

        edit = desktop.AdEdit(wd)
        edit.insert_ad(communes=232,price=12300)
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('without_account_create'))

    @tags('desktop', 'ad_edit', 'mail')
    def test_password_recovery_success_texts(self, wd):
        """ Tests text on success page on password recovery """
        list_id = '8349385'

        edit_pw_page = desktop.EditPasswordPage(wd)
        edit_pw_page.go(list_id)
        edit_pw_page.forgot_password.click()

        forgot_password = desktop.ForgotPassword(wd)
        forgot_password.email_input.clear()
        forgot_password.email_input.send_keys('user15@blocket.se')
        forgot_password.submit_button.click()
        forgot_password.wait.until_loaded()

        self.assertEqual("El link para editar tu aviso fue enviado.", forgot_password.success_title.text)
        self.assertEqual("Hemos enviado un correo con un link que te permitirá editar tu aviso", forgot_password.success_msg.text)
