# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.control_panel import ControlPanel
from yapo.pages.generic import SentEmail
from yapo.pages.ad_insert import AdInsertResult
from yapo.steps import trans

class MobileMails(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def _post_refuse_ad(self, wd, list_id):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.post_refuse_ad(list_id)

    def test_edit_ad_from_published_mail(self, wd):
        """ Testing that an ad can be edited on mobile from the published ad email """

        ad_info = {
            'subject': 'Mobile edit from a published ad email link',
            'body': 'Testing that an ad can be edited on mobile from the published ad email',
            'price': '20000',
            'name': 'Ej',
            'email': 'larois@otup.com',
            'phone': '12312312',
            'password': '123123123',
            'password_verification': '123123123',
            'accept_conditions': True
        }

        edit_ad_info = {
            'subject': 'Edited subject from a published ad email link',
            'body': 'Edited body on testing that an ad can be edited on mobile from the published ad email',
            'price': '25000'
        }

        mai = mobile.NewAdInsert(wd)
        mai.go()
        mai.insert_ad(region=15, communes=327, category=8020, **ad_info)
        mai.submit.click()
        result = AdInsertResult(wd)
        result.wait.until_loaded()
        self.assertTrue(result.success_mobile())

        trans.review_ad(ad_info['email'], ad_info['subject'])
        utils.rebuild_index()

        # reading the e-mail
        sent_email = SentEmail(wd)
        sent_email.go()
        wd.find_element_by_css_selector('a[href*="cmd=edit"]').click()

        mai.insert_ad(region = "15", communes=327, category = "8020", **edit_ad_info)
        mai.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.success_mobile())

    def test_edit_ad_from_ad_refused_mail(self, wd):
        """ Testing that an ad can be edited on mobile from the ad refused email """

        ad_info = {
            'subject': 'Aviso de prueba 7'
        }

        edit_ad_info = {
            'subject': 'Mobile edit from a refused email link',
            'body': 'Testing that an ad can be edited on mobile from the ad refused email',
            'price': '8000'
        }

        trans.review_ad('many@ads.cl', ad_info['subject'], action = 'refuse')

        sent_email = SentEmail(wd)
        sent_email.go()
        edit_url = wd.find_element_by_css_selector('a[href*="ca=15_s"]').get_attribute('href')
        wd.get(edit_url)

        ai_page = mobile.NewAdInsert(wd)
        ai_page.insert_ad(region = "15", communes=327, category = "8020", **edit_ad_info)
        ai_page.submit.click()
        result = AdInsertResult(wd)
        # the element name in the page object is upselling_success, but this page does not have upselling
        result.wait.until_visible('upselling_success')
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('without_upselling'))

    def test_edit_ad_from_post_refused_mail(self, wd):
        """ Testing that an ad can be edited on mobile from the post-refused email """

        edit_ad_info = {
            'subject': 'Mobile edit from a post-refused email link',
            'body': 'Testing that an ad can be edited on mobile from the post-refused email',
            'price': '88888'
        }

        self._post_refuse_ad(wd, '8000027')
        utils.rebuild_index()

        # Get the edit link from the post-refused email and click on it
        sent_email = SentEmail(wd)
        sent_email.go()
        edit_link = wd.find_element_by_css_selector('a[href*="id=RH"]').get_attribute('href')
        wd.get(edit_link)

        ai_page = mobile.NewAdInsert(wd)
        ai_page.insert_ad(region = "15", communes=327,category = "3020", **edit_ad_info)
        ai_page.submit.click()
        result = AdInsertResult(wd)
        # the element name in the page object is upselling_success, but this page does not have upselling
        result.wait.until_visible('upselling_success')
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('without_upselling'))

class MultiproductBuyEmail(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.mobile_uas.Mozilla/5_0_(X11;_Linux_x86_64;_rv:10_0_11)_Gecko/20121120_Firefox/10_0_11.layout':'1',
        '*.*.mobile_uas.Mozilla/5_0_(X11;_Linux_x86_64;_rv:10_0_12)_Gecko/20130110_Firefox/10_0_12.layout':'1',
        '*.*.mobile_uas.Mozilla/5_0_(X11;_Linux_x86_64;_rv:10_0)_Gecko/20100101_Firefox/10_0.layout':'1',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    def test_edit_ad_from_published_mail(self, wd):
        """
        Edit an ad and check the sent mail
        """

        # Insert new ad
        ai = mobile.NewAdInsert(wd)
        ai.go()
        ai.insert_ad(
            subject = 'Mobile edit from a published ad email link',
            body = 'Testing that an ad can be edited on mobile from the published ad email',
            price="20000",
            region="15",
            communes=327,
            category="8020",
            private_ad=True,
            name="yolo",
            email="yolo@cabral.es",
            phone = '12312312312',
            password = '123123123',
            password_verification = '123123123',
            create_account=True,
            accept_conditions=True
        )
        ai.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        # Accept ad
        trans.review_ad('yolo@cabral.es', 'Mobile edit from a published ad email link')
        utils.rebuild_index(full = True)

        # Get the edit link from the published ad email and click on it
        mail = SentEmail(wd)
        mail.go()
        link = mail.body.find_element_by_css_selector(".edit").get_attribute("href")
        wd.get(link)

        # Assert if existing ad info are all equal
        ed = mobile.NewAdEdit(wd)
        self.assertEquals(ed.subject.get_attribute('value'), 'Mobile edit from a published ad email link')
        self.assertEquals(ed.body.get_attribute('value'), 'Testing that an ad can be edited on mobile from the published ad email')
        self.assertEquals(ed.price.get_attribute('value'), "20000")
        self.assertEquals(ed.region.get_attribute('value'), "15")
        self.assertEquals(ed.category.get_attribute('value'), "8020")
        self.assertEquals(ed.company_ad.get_attribute('checked'), None)
        self.assertEquals(ed.name.get_attribute('value'), "yolo")
        self.assertEquals(ed.phone.get_attribute('value'), '12312312')

        # Edit the published ad
        ai.fill_form (
            subject = 'Edited subject from a published ad email link',
            body = 'Edited body on testing that an ad can be edited on mobile from the published ad email',
            price = '25000'
        )
        ai.submit.click()

        # accept the edition
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject('Mobile edit from a published ad email link')
        wd.find_element_by_link_text('Edited subject from a published ad email link').click()
        wd.switch_to_window(wd.window_handles[1])
        cp.review_ad(action = 'accept')
        cp.close_window_and_back()
        cp.logout()

        # make rebuild-index to send out the email
        utils.rebuild_index(full = True)

        # Assert if existing ad info are all equal
        edit_pw_page = mobile.EditPasswordPage(wd)
        edit_pw_page.go('8000085')
        edit_pw_page.validate_password('123123123')

        self.assertEquals(ed.subject.get_attribute('value'), 'Edited subject from a published ad email link')
        self.assertEquals(ed.body.get_attribute('value'), 'Edited body on testing that an ad can be edited on mobile from the published ad email')
        self.assertEquals(ed.price.get_attribute('value'), "25000")
        self.assertEquals(ed.region.get_attribute('value'), "15")
        self.assertEquals(ed.category.get_attribute('value'), "8020")
        self.assertEquals(ed.company_ad.get_attribute('checked'), None)
        self.assertEquals(ed.name.get_attribute('value'), "yolo")
        self.assertEquals(ed.phone.get_attribute('value'), '12312312')
