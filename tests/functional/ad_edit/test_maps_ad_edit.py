# -*- coding: latin-1 -*-
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages import desktop, desktop_list, mobile
from yapo.pages.maps import MapPreview
from yapo.decorators import tags
from yapo.steps import trans
import yapo
from yapo import utils


class AdEditTest(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_maps']
    platform = desktop

    @tags('ad_edit', 'desktop', 'maps', 'mobile')
    def test_edit_old_ads_real_estate_without_map(self, browser):
        ''' Edit an old ad without map, real estates category '''

        edit_pw = self.platform.EditPasswordPage(browser)
        edit_pw.go('8000000')
        edit_pw.validate_password('123123123')

        edit = self.platform.AdEdit(browser)
        self.assertFalse(edit.has_map())
        edit.insert_ad(communes = '331', phone = '62184762')
        edit.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

    @tags('ad_edit', 'desktop', 'maps', 'mobile')
    def test_edit_old_ads_without_map(self, browser):
        ''' Edit an old ad without map, any category '''

        edit_pw = self.platform.EditPasswordPage(browser)
        edit_pw.go('8000082')
        edit_pw.validate_password('123123123')

        edit = self.platform.AdEdit(browser)
        self.assertFalse(edit.has_map())
        edit.insert_ad(communes = '331')
        edit.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))


class AdEditTestMobile(AdEditTest):
    user_agent = utils.UserAgents.IOS
    platform = mobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}


class AdEditDesktop(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_maps']
    platform = desktop

    @tags('ad_edit', 'desktop', 'maps', 'mobile')
    def test_edit_old_ad_without_map_including_them(self, browser):
        '''Edit old ads without maps including them'''

        edit_page = self.platform.AdEdit(browser)
        edit_page.edit_an_ad('8000080', '123123123', {'communes': '331',
                                                   'add_location': True,
                                                   'address': 'Avenida Antillanca',
                                                   'address_number': '123',
                                                   'show_location': True})
        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

    @tags('ad_edit', 'desktop', 'maps', 'mobile')
    def test_edit_ad_with_map(self, browser):
        '''edit map data from an ad with map '''

        edit_pw = self.platform.EditPasswordPage(browser)
        edit_pw.go('8000089')
        edit_pw.validate_password('123123123')

        edit = self.platform.AdEdit(browser)
        self.assertTrue(edit.has_map())
        edit.insert_ad(**{'communes': '331',
            'add_location': True,
            'address': 'Avenida san pablo',
            'address_number': '123',
            'show_location': True
        })
        edit.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

    @tags('ad_edit', 'desktop', 'maps', 'mobile')
    def test_edit_map_from_preview(self, browser):
        '''edit map data check on adview preview '''

        edit_pw = self.platform.EditPasswordPage(browser)
        edit_pw.go('8000099')
        edit_pw.validate_password('123123123')

        edit = self.platform.AdEdit(browser)
        edit.insert_ad(**{'communes': '331',
            'price': 100000,
            'add_location': True,
            'address': 'Avenida san pablo',
            'address_number': '123',
            'show_location': True
        })
        edit.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

        trans.review_ad('without_account_2@maps.cl', 'Ad with map with street Mar�ano S�nchez Fontecilla')
        utils.rebuild_index()

        ad_view = desktop_list.AdViewDesktop(browser)
        ad_view.go(8000099)

        map_preview = MapPreview(browser)
        self.assertEqual(map_preview.region_commune.text, 'Regi�n Metropolitana, Pudahuel')
