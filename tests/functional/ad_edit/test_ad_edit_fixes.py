# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import EditPasswordPage as EditPasswordPageMobile, NewAdEdit as AdEditMobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.mobile import NewAdInsert
from yapo.pages.control_panel import ControlPanel
import yapo
from yapo import utils

class AdEditFixes(yapo.SeleniumTest):

    snaps = ['accounts']
    EditPasswordPage = EditPasswordPageMobile
    AdEdit = AdEditMobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'ad_edit', 'ad_type')
    def test_deprecated_busco_option(self, wd):
        """ Edits an ad with type = busco of a category with only type=sell option
            this is any category but real estate or cars
            go to edition of this ad and save with no changes
            it should save with no problems and the type should now be sell, instead of busco
        """
        edit_pw_page = self.EditPasswordPage(wd)
        edit_pw_page.go('8000032')
        edit_pw_page.validate_password('123123123')
        edit_page = self.AdEdit(wd)
        edit_page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('android@yapo.cl', subject='Iphone 5 blanco', queue='Unreviewed')
        self.assertEqual(cpanel.ad_type.get_attribute('value'), 's')


    @tags('mobile', 'ad_insert', 'images')
    def test_more_images_yeah(self, wd):
        """ When changing between categories of 6 or 20 images, like 6 -> 20 -> 6 -> 20
            a message is displayed to the user to inform he/she can upload more images
        """

        ad_insert = NewAdInsert(wd)
        ad_insert.go()
        ad_insert.hotizontal_select.select('parent_category', 2000)
        ad_insert.hotizontal_select.select('category', 2020)
        ad_insert.hotizontal_select.close_sidenav()
        ad_insert.wait.until_present('more_images_yeah')
        self.assertEqual('�Puedes agregar hasta 20 fotos!', ad_insert.more_images_yeah.text)

        ad_insert.hotizontal_select.select('parent_category', 4000)
        ad_insert.hotizontal_select.select('category', 4040)
        ad_insert.hotizontal_select.close_sidenav()
        ad_insert.wait.until_not_visible('more_images_yeah')

        ad_insert.hotizontal_select.select('parent_category', 1000)
        ad_insert.hotizontal_select.select('category', 1220)
        ad_insert.hotizontal_select.select('estate_type', 1)
        ad_insert.hotizontal_select.close_sidenav()
        ad_insert.wait.until_present('more_images_yeah')
        self.assertEqual('�Puedes agregar hasta 20 fotos!', ad_insert.more_images_yeah.text)

        ad_insert.hotizontal_select.select('parent_category', 3000)
        ad_insert.hotizontal_select.select('category', 3020)
        ad_insert.hotizontal_select.close_sidenav()
        ad_insert.wait.until_not_visible('more_images_yeah')


    @tags('mobile', 'ad_insert', 'jobs', 'real_estate_services')
    def test_all_multiple_select(self, wd):
        """ When editing ads of categories real estate holidays rental, jobs and services,
            if you select all the params in the multiple select, after saving
            all the params should appear correctly
        """

        # inserting an ad with everything!
        ad_insert = NewAdInsert(wd)
        ad_insert.go()
        ad_insert.insert_ad(subject="Selenium",
                            body="asdfasdfasdf",
                            region=11,
                            communes=233,
                            category=1260,
                            estate_type=2,
                            rooms=1,
                            capacity=4,
                            bathrooms=1,
                            srv=['1', '2', '3', '4', '5', '6'],
                            private_ad=True,
                            name="Ana",
                            email="ana@schibstediberica.es",
                            phone="26346447",
                            password="22222222",
                            password_verification="22222222",
                            create_account=False,
                            accept_conditions=True)
        ad_insert.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        # checking in controlpanel
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('ana@schibstediberica.es', subject='Selenium', queue='Unreviewed')

        self.assertEqual(cpanel.real_estate_capacity.text, "4 personas")
        self.assertEqual(len(cpanel.real_estate_services), 6)
        expected_services = ['Piscina', 'WiFi', 'Terraza', 'TV Cable / Sat�lite',
                             'Calefacci�n', 'Cocina equipada']
        current_services = [s.text for s in cpanel.real_estate_services]
        self.assertEqual(expected_services, current_services)
        cpanel.review_ad('accept')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        utils.rebuild_index()

        # now we go to edit
        edit_pw_page = self.EditPasswordPage(wd)
        edit_pw_page.go('8000085')
        edit_pw_page.validate_password('22222222')
        edit_page = self.AdEdit(wd)
        edit_page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        # and the final assert ...
        cpanel = ControlPanel(wd)
        cpanel.go()
        cpanel.search_for_ad('ana@schibstediberica.es', subject='Selenium', queue='Unreviewed')
        self.assertEqual(len(cpanel.real_estate_services), 6)
        expected_services = ['Piscina', 'WiFi', 'Terraza', 'TV Cable / Sat�lite',
                             'Calefacci�n', 'Cocina equipada']
        current_services = [s.text for s in cpanel.real_estate_services]
        self.assertEqual(expected_services, current_services)
