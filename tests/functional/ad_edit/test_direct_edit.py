# -*- coding: latin-1 -*-
import yapo, time
from yapo.decorators import tags
from yapo import conf, utils
from yapo.pages.desktop import AdEdit
from yapo.pages.ad_insert import AdInsertSuccess
from yapo.pages.control_panel import ControlPanel

class DesktopDirectEdit(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('edit', 'ad_edit')
    def test_edit_ad_without_account(self, wd):
        """ Test that the direct link to an ad works """
        utils.rebuild_index()
        wd.get(conf.PHP_URL+'/ai?id=8000084&p=0ad0df74937ee40c5a5b29914cbc150cd5a5a43f&cmd=edit')
        av = AdEdit(wd)
        self.assertEquals("Test 09", av.subject.get_attribute('value'))
        self.assertEquals("aasd adadasds", av.body.text)

    @tags('edit', 'ad_edit', 'controlpanel')
    def test_edit_ad_change_category(self, wd):
        """ When an ad changes category, a message must be displayed on controlpanel """

        data_new = {
            'subject':'Mueble',
            'category':'5020',
            'body':'yoLo',
        }

        edit = AdEdit(wd)
        edit.edit_an_ad('8000019', '123123123', data_new, None, False)
        page = AdInsertSuccess(wd)
        self.assertEqual(page.title.text, "¡Gracias por publicar!")

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('android@yapo.cl', subject='Mueble', queue='Unreviewed')

        self.assertEquals(wd.find_element_by_css_selector('.message').text, 'The ad has switched from old category "Motos"')
