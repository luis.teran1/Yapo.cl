# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop import Login
from yapo.pages.accounts import Edit, Create
from yapo import utils
import time
import yapo
import yapo.conf
import re


class EditProAccountsDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}
    @tags('accounts', 'desktop')
    def test_desktop_edit_pro_account(self, wd):
        """ test desktop edit pro account """
        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se','123123123')
        profile = Edit(wd)
        profile.go()
        profile = Create(wd) # Get an instance of element where the radios are defined
        profile.wait.until_not_present('pro_radio')
        profile.wait.until_not_present('person_radio')

    @tags('accounts', 'desktop')
    def test_desktop_edit_account(self, wd):
        """ test desktop edit account no pro """
        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se','123123123')
        profile = Edit(wd)
        profile.go()
        profile = Create(wd) # Get an instance of element where the radios are defined
        profile.wait.until_present('pro_radio')
        profile.wait.until_present('person_radio')




class EditProAccountsMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('accounts', 'mobile')
    def test_mobile_edit_pro_account(self, wd):
        """ test mobile edit pro account """
        utils.flush_redises()
        login = mobile.Login(wd)
        login.login('prepaid3@blocket.se','123123123')
        profile = mobile.DashboardProfile(wd)
        profile.go_profile()
        profile.wait.until_not_present('radio_persona_input')
        profile.wait.until_not_present('radio_profesional_input')

    @tags('accounts', 'mobile')
    def test_mobile_edit_account_not_pro(self, wd):
        """ test mobile edit account no pro"""
        utils.flush_redises()
        login = mobile.Login(wd)
        login.login('prepaid5@blocket.se','123123123')
        profile = mobile.DashboardProfile(wd)
        profile.go_profile()
        profile.wait.until_present('radio_persona_input')
        profile.wait.until_present('radio_profesional_input')

