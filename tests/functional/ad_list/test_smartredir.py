# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.utils import UserAgents
from yapo import conf, SeleniumTest, utils


class DesktopSmartRedirections(SeleniumTest):

    def __case(self, wd, urlCalled, urlExpected):
        wd.get(urlCalled)
        self.assertEquals(wd.current_url, urlExpected)

    def test_verify_common_redirections(self, wd):
        urls = {
            "/autos": "/chile/autos",
            "/autos/metropolitana": "/region_metropolitana/autos",
            "/autos/arica": "/arica_parinacota/autos",
            "/autos/arica-parinacota": "/arica_parinacota/autos",
            "/autos/los.rios": "/los_rios/autos",
            "/camiones/magallanes": "/magallanes_antartica/camiones_furgones?q=camiones",
            "/motos/valparaiso": "/valparaiso/motos",
            "/lanchas/biobio": "/biobio/barcos_lanchas_aviones?q=lanchas",
        }

        for key, value in urls.items():
            load = conf.DESKTOP_URL + key
            expected = conf.DESKTOP_URL + value
            self.__case(wd, load, expected)

    def test_check_redis_handling(self, wd):
        urls = {"/buses": "/chile/camiones_furgones?q=bus"}

        for load, expected in urls.items():
            utils.redis_command(
                conf.REGRESS_REDIS_PORT,
                'HMSET',
                'mcp1:redirects',
                load,
                expected
            )

        for key, value in urls.items():
            load = conf.DESKTOP_URL + key
            expected = conf.DESKTOP_URL + value
            self.__case(wd, load, expected)

    def test_verify_car_redirections(self, wd):
        urls = {
            "/autos/toyota": "/chile/autos?br=88",
            "/autos/yaris": "/chile/autos?br=88&mo=28",
            "/autos/ford/focus": "/chile/autos?br=31&mo=33",
            "/autos/araucania/jeep/wrangler": "/araucania/autos?br=45&mo=7",
        }

        for key, value in urls.items():
            load = conf.DESKTOP_URL + key
            expected = conf.DESKTOP_URL + value
            self.__case(wd, load, expected)
