# -*- coding: latin-1 -*-
from selenium.webdriver.common.action_chains import ActionChains
import yapo
import time
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.pages import mobile
from yapo.pages.maps import MapPreview, Map
from yapo.decorators import tags
from yapo import utils

EXPECTED = {"8000073": {'has_address': False,'has_label': False},
            "8000100": {'has_address': True,'has_label': False},
            "8000099": {'has_address': True,'has_label': False},
            "8000098": {'has_address': False,'has_label': False},
            "8000097": {'has_address': True,'has_label': False},
            "8000096": {'has_address': False,'has_label': False},
            "8000095": {'has_address': True,'has_label': False},
            "8000094": {'has_address': True,'has_label': False},
            "8000093": {'has_address': True,'has_label': False},
            "8000092": {'has_address': False,'has_label': False},
            "8000091": {'has_address': True,'has_label': False},
            "8000090": {'has_address': True,'has_label': False},
            "8000089": {'has_address': True,'has_label': False},
            "8000088": {'has_address': True,'has_label': False},
            "8000087": {'has_address': False,'has_label': False},
            "8000086": {'has_address': False,'has_label': False},
            "8000085": {'has_address': False,'has_label': False},
            "8000084": {'has_address': False,'has_label': False}
           }


class MapsListingTests(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_maps']
    AdList = AdListDesktop
    EXPECTED = {"8000073": {'commune': 'Macul', 'region': 'Regi�n Metropolitana'},
                "8000100": {'commune': 'Las Condes', 'region': 'Regi�n Metropolitana'},
                "8000099": {'commune': 'Las Condes', 'region': 'Regi�n Metropolitana'},
                "8000098": {'commune': 'Chimbarongo', 'region': 'VI O\'Higgins'},
                "8000097": {'commune': 'Copiap�', 'region': 'III Atacama'},
                "8000096": {'region': 'XIV Los R�os'},
                "8000095": {'commune': 'Torres del Paine', 'region': 'XII Magallanes & Ant�rtica'},
                "8000094": {'commune': 'Zapallar', 'region': 'V Valpara�so'},
                "8000093": {'commune': 'Pedro Aguirre Cerda', 'region': 'Regi�n Metropolitana'},
                "8000092": {'commune': 'Pitrufqu�n', 'region': 'IX Araucan�a'},
                "8000091": {'commune': 'Quinta Normal', 'region': 'Regi�n Metropolitana'},
                "8000090": {'commune': 'Las Condes', 'region': 'Regi�n Metropolitana'},
                "8000089": {'commune': 'Santiago', 'region': 'Regi�n Metropolitana'},
                "8000088": {'commune': 'Las Condes', 'region': 'Regi�n Metropolitana'},
                "8000087": {'region': 'IX Araucan�a'},
                "8000086": {'commune': 'Pozo Almonte', 'region': 'I Tarapac�'},
                "8000085": {'region': 'V Valpara�so'},
                "8000084": {'commune': 'Las Condes', 'region': 'Regi�n Metropolitana'}
               }

    @tags('desktop', 'searchbox', 'listing', 'maps')
    def test_check_listing_ad_info(self, wd):
        """ Check is the correct info appears in the listing """
        listing = self.AdList(wd)
        listing.go()
        for list_id, data in self.EXPECTED.items():
            print(list_id)
            data.update(EXPECTED[list_id])
            node = listing.get_ad_data(list_id)
            for key in data:
                self.assertEqual(data[key], node[key])


class MapsListingTestsM(MapsListingTests):

    AdList = AdListMobile
    user_agent = utils.UserAgents.IOS
    EXPECTED = {}


class MapsInAdView(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_maps']
    AdView = AdViewDesktop
    region_commune_field = 'ad_reply_region_commune'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0', '*.*.new_seller_info.categories.disabled': '0'}

    @tags('desktop', 'maps', 'ad_view')
    def test_check_old_ads(self, browser):
        """ Check old ads with and without commune """
        ad_view = self.AdView(browser)
        # With commune
        ad_view.go(6394118)
        field_text = getattr(ad_view, self.region_commune_field).text
        self.assertEqual(field_text, 'Regi�n Metropolitana, �u�oa')

        # Without commune
        ad_view.go(8000087)
        field_text = getattr(ad_view, self.region_commune_field).text
        self.assertEqual(field_text, 'IX Araucan�a')

    @tags('desktop', 'maps', 'ad_view')
    def test_draggable_in_vi(self, browser):
        """ Checking that map marker is not draggable in vi """

        ad_view = self.AdView(browser)
        # With commune
        ad_view.go(8000100)
        self._open_map(browser)
        map = Map(browser)
        self.assertNotPresent(map, 'marker_draggable')

        # Without commune
        ad_view.go(8000099)
        self._open_map(browser)
        map = Map(browser)
        self.assertFalse(map.is_element_present('marker'))

    def _open_map(self, browser):
        map_preview = MapPreview(browser)
        ActionChains(browser).move_to_element(map_preview.map_preview).perform()
        map_preview.map_view_hover.click()

class MapsInAdViewM(MapsInAdView):

    AdView = mobile.AdView
    user_agent = utils.UserAgents.IOS
    region_commune_field = 'map_commune'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def _open_map(self, browser):
        map_preview = MapPreview(browser)
        ActionChains(browser).move_to_element(map_preview.map_preview).perform()
        map_preview.map_preview.click()

    @tags('desktop', 'maps', 'ad_view')
    def test_check_old_ads(self, browser):
        """ Check old ads with and without commune """

        ad_view = self.AdView(browser)
        # With commune
        ad_view.go(6394118)
        self.assertEqual(getattr(ad_view, 'map_commune').text, '�u�oa')
        self.assertEqual(getattr(ad_view, 'map_region').text, 'Regi�n Metropolitana')

        # Without commune
        ad_view.go(8000087)
        self.assertEqual(getattr(ad_view, 'map_region').text, 'IX Araucan�a')
