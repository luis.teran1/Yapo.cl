# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.mobile_list import AdListMobile, SearchBox
from yapo.utils import UserAgents
import yapo
import time


class MobileSearchbox(yapo.SeleniumTest):

    snaps = ['android']
    user_agent = UserAgents.IOS

    data = {
        'estate_type': 'Departamento',
        'min_price': '$ 0',
        'max_price': '$ 1.000.000',
        'min_size': '0 m�',
        'max_size': '400 m�'
    }
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'listing', 'searchbox')
    def test_search_with_params_inverted(self, wd):
        """Search ads with params from high to low in listing mobile."""
        ad_list = AdListMobile(wd)
        ad_list.go_to_region('region_metropolitana')
        ad_list.fill('category', 'Arrendar')
        ad_list.wait.until_visible('filter_button')
        ad_list.open_filters()
        search_box = SearchBox(wd)
        search_box.fill('estate_type', self.data['estate_type'])
        time.sleep(1)   # waiting for the rest of the params to show up
        search_box.ordered_fill_form([
            ('min_price', self.data['max_price']),
            ('max_price', self.data['min_price']),
            ('min_size', self.data['max_size']),
            ('max_size', self.data['min_size']),
        ])
        search_box.search()
        titles_expected = [
            'Departamentos nuevos en santiago centro',
            'Departamento Amoblado 2 Dormitorios',
            'Depto amoblado un ambiente',
            'Departamento 3 dormitorios 2 ba�os'
        ]
        # verify search
        self.assertListEqual(
            titles_expected, [i.text for i in ad_list.get_ads_title()])
        self.assertListEqual(
                ['$ 190.000', '$ 320.000', '$ 380.000', '$ 540.000'],
                [i.text for i in ad_list.get_ads_price()]
                )
        # verify params
        ad_list.open_filters()
        self.assertEquals(self.data['estate_type'],
                          search_box.selected('estate_type'))
        self.assertEquals(self.data['min_price'],
                          search_box.selected('min_price'))
        self.assertEquals(self.data['max_price'],
                          search_box.selected('max_price'))
        self.assertEquals(self.data['min_size'],
                          search_box.selected('min_size'))
        self.assertEquals(self.data['max_size'],
                          search_box.selected('max_size'))

    @tags('mobile', 'listing', 'searchbox')
    def test_mobile_searchbox_cars(self, wd):
        """ Cars category behaviour """
        ad_list = AdListMobile(wd)
        ad_list.go_to_region('region_metropolitana')
        ad_list.open_filters()
        ad_list.fill('category', 2020)
        search_box = SearchBox(wd)

        self.assertVisible(search_box, 'label_st_a')
        self.assertVisible(search_box, 'label_st_s')
        self.assertVisible(search_box, 'label_st_k')

        self.assertEquals(search_box.type_a.get_attribute('checked'), None)
        self.assertEquals(search_box.type_s.get_attribute('checked'), 'true')
        self.assertEquals(search_box.type_k.get_attribute('checked'), None)

        # Please ignore the following couple of blocks. It's just testing data
        regdate = [
                'Anterior a 1960', '1960', '1965', '1970', '1975', '1980',
                '1985', '1990', '1991', '1992', '1993', '1994', '1995', '1996',
                '1997', '1998', '1999', '2000', '2001', '2002', '2003', '2004',
                '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012',
                '2013', '2014', '2015', '2016', '2017', '2018', '2019','2020',
                '2021'
                ]
        mileage = [
                '0 Km', '5.000 Km', '10.000 Km', '20.000 Km', '30.000 Km',
                '40.000 Km', '60.000 Km', '80.000 Km', '100.000 Km',
                '120.000 Km', '140.000 Km', '160.000 Km', '180.000 Km',
                '200.000 Km', '250.000 Km', '300.000 Km', '400.000 Km',
                '500.000 Km', 'M�s de 500.000 Km'
                ]
        price = [
                '$ 0', '$ 250.000', '$ 500.000', '$ 1.000.000', '$ 1.500.000',
                '$ 2.000.000', '$ 2.500.000', '$ 3.000.000', '$ 3.500.000',
                '$ 4.000.000', '$ 4.500.000', '$ 5.000.000', '$ 5.500.000',
                '$ 6.000.000', '$ 6.500.000', '$ 7.000.000', '$ 7.500.000',
                '$ 8.000.000', '$ 8.500.000', '$ 9.000.000', '$ 9.500.000',
                '$ 10.000.000', '$ 10.500.000', '$ 11.000.000', '$ 11.500.000',
                '$ 12.000.000', '$ 12.500.000', '$ 13.000.000', '$ 13.500.000',
                '$ 14.000.000', '$ 14.500.000', '$ 15.000.000', '$ 15.500.000',
                '$ 16.000.000', '$ 16.500.000', '$ 17.000.000', '$ 17.500.000',
                '$ 18.000.000', '$ 18.500.000', '$ 19.000.000', '$ 19.500.000',
                '$ 20.000.000', '$ 40.000.000', '$ 60.000.000',
                'M�s de $ 60.000.000'
                ]
        brand = [
                'Marca', 'CHEVROLET', 'HYUNDAI', 'TOYOTA', 'NISSAN',
                'SUZUKI', 'KIA MOTORS', 'PEUGEOT', 'MAZDA', 'FORD',
                'VOLKSWAGEN', 'ACADIAN', 'ACURA', 'ALFA ROMEO',
                'AMERICAN MOTORS', 'ARO', 'ASIA MOTORS', 'ASTON MARTIN',
                'AUDI', 'AUSTIN', 'AUTORRAD', 'BAIC', 'BEIGING', 'BENTLEY',
                'BMW', 'BRILLIANCE', 'BUICK', 'BYD', 'CADILLAC', 'CATERHAM',
                'CHANGAN', 'CHANGHE', 'CHERY', 'CHEVROLET', 'CHRYSLER',
                'CITROEN', 'COMMER', 'DACIA', 'DAEWOO', 'DAIHATSU', 'DATSUN', 'DFSK',
                'DODGE', 'DONGFENG', 'DS AUTOMOBILES', 'F.S.O.', 'FAW', 'FERRARI', 'FIAT',
                'FORD', 'FOTON', 'G.M.C.', 'GAC GONOW', 'GEELY', 'GREAT WALL',
                'HAFEI', 'HAIMA', 'HAVAL', 'HILLMAN', 'HONDA', 'HYUNDAI',
                'INFINITI', 'INTERNATIONAL', 'ISUZU', 'JAC', 'JAGUAR', 'JEEP',
                'JMC', 'KARMA', 'KENBO', 'KIA MOTORS', 'KYC', 'LADA', 'LAMBORGHINI', 'LANCIA',
                'LAND ROVER', 'LANDWIND', 'LEXUS', 'LIFAN', 'LINCOLN', 'LOTUS',
                'MAHINDRA', 'MASERATI', 'MAXUS', 'MAZDA', 'MCLAREN', 'MERCEDES BENZ',
                'MERCURY', 'MG', 'MINI', 'MITSUBISHI', 'MORGAN', 'MORRIS',
                'NISSAN', 'NSU', 'OLDSMOBILE', 'OPEL', 'PEUGEOT', 'PLYMOUTH',
                'POLSKI FIAT', 'PONTIAC', 'PORSCHE', 'PROTON', 'PUMA', 'RAM',
                'RENAULT', 'ROLLS ROYCE', 'ROVER', 'SAAB', 'SAEHAN', 'SAMSUNG',
                'SEAT', 'SG', 'SIMCA', 'SKODA', 'SMA', 'SSANGYONG', 'SUBARU',
                'SUZUKI', 'TATA', 'TOYOTA', 'VOLKSWAGEN', 'VOLVO', 'WILLYS',
                'YUGO', 'ZASTAVA', 'ZNA', 'ZOTYE', 'ZX'
                ]

        data = {
            search_box.min_regdate: ['A�o m�n'] + regdate,
            search_box.max_regdate: ['A�o m�x'] + regdate,
            search_box.mileage_min: ['Km m�n'] + mileage,
            search_box.mileage_max: ['Km m�x'] + mileage,
            search_box.gearbox: [
                'Transmisi�n (Cambio)',
                'Manual',
                'Autom�tico'
                ],
            search_box.fuel: [
                'Combustible',
                'Bencina',
                'H�brido',
                'Gas',
                'Diesel',
                'Otros'
                ],
            search_box.cartype: [
                'Tipo de auto',
                'Autom�vil',
                'Camioneta',
                '4x4',
                'Convertible',
                'Cl�sico'
                ],
            search_box.min_price: ['Precio m�n'] + price,
            search_box.max_price: ['Precio m�x'] + price,
            search_box.brand: brand
        }

        # Verify all the search options contents
        for field, expected in data.items():
            self.assertEquals(
                [e.text for e in Select(field).options], expected)

        self.assertVisible(search_box, 'model')
        self.assertTrue(search_box.model.get_attribute('disabled'))

        search_box.type_s.click()
        search_box.search()

        expected = [
            'Suzuki Maruti 800cc 2003',
            'Chevrolet Aveo sport',
            'Dodge Ram 1500 V8',
        ]
        # Verify search
        self.assertEquals([i.text for i in ad_list.get_ads_title()], expected)

        ad_list.open_filters()
        search_box = SearchBox(wd)
        search_box.fill('brand', 'SUZUKI')

        model = [
                'Modelo', '600 KGS', 'AERIO', 'ALTO', 'APV', 'APV', 'APV',
                'APV', 'APV', 'BALENO', 'CARRY', 'CELERIO', 'CERVO', 'CIAZ',
                'DZIRE', 'DZIRE SEDAN', 'ERTIGA',  'FORZA', 'FRONTE', 'GRAND', 'GRAND NOMADE',
                'GRAND VITARA', 'IGNIS', 'JAZZ', 'JIMNY', 'KIZASHI', 'LIANA',
                'LJ60', 'LJ80', 'MARUTI', 'MASTERVAN', 'NOMADE', 'PICK UP',
                'PICK-UP', 'S-CROSS', 'SAMURAI', 'SB', 'SIDEKICK', 'SJ', 'SJ408', 'SJ410',
                'SJ413', 'SPRESSO', 'SS', 'SWIFT', 'SX4', 'VITARA', 'WAGON', 'XL7', '- Otro -'
                ]

        self.assertEquals(
            [e.text for e in Select(search_box.model).options], model)
        self.assertFalse(search_box.model.get_attribute('disabled'))

        search_box.ordered_fill_form([
            ('model', 'MARUTI'),
            ('brand', 'Marca'),
        ])
        # Verify that unselecting brand disables model
        self.assertTrue(search_box.model.get_attribute('disabled'))

        search_box.fill('brand', 'SUZUKI')
        # Verify that models are back if I reselect a brand
        self.assertEquals(
            [e.text for e in Select(search_box.model).options], model)
        self.assertFalse(search_box.model.get_attribute('disabled'))

        search_box.fill('model', 'MARUTI')
        search_box.search()

        expected = [
            'Suzuki Maruti 800cc 2003',
        ]
        # Verify search
        self.assertEquals([i.text for i in ad_list.get_ads_title()], expected)
