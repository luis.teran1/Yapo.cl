# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from yapo.decorators import tags
from yapo.pages import desktop_list
from yapo import utils
import yapo
import yapo.conf

class FriendlyUrls(yapo.SeleniumTest):

    _multiprocess_shared_ = True
    categories = {
        '1220':'comprar',
        '1240':'arrendar',
        '1260':'arriendo_temporada',
        '2020':'autos',
        '2040':'camiones_furgones',
        '2060':'motos',
        '2080':'barcos_lanchas_aviones',
        '2100':'accesorios_vehiculos',
        '2120':'otros_vehiculos',
        '4020':'moda-vestuario',
        '4040':'bolsos-bisuteria-accesorios',
        '4060':'salud-belleza',
        '5020':'muebles',
        '5040':'electrodomesticos',
        '5060':'jardin_herramientas',
        '6020':'deportes_gimnasia',
        '6060':'bicicletas_ciclismo',
        '6080':'instrumentos_musicales',
        '6100':'musica_peliculas',
        '6120':'libros_revistas',
        '6140':'animales_accesorios',
        '6160':'arte_antiguedades_colecciones',
        '6180':'hobbies_outdoor',
        '3020':'consolas_videojuegos',
        '3040':'computadores',
        '3060':'celulares',
        '3080':'television_camaras',
        '7020':'ofertas_de_empleo',
        '7040':'busco_empleo',
        '7060':'servicios',
        '7080':'negocios_maquinaria_construccion',
        '8020':'otros_productos',
        '9020':'vestuario-futura-mama-ninos',
    }
    regions = {
        'arica_parinacota': 'XV Arica & Parinacota',
        'tarapaca': 'I Tarapac�',
        'antofagasta': 'II Antofagasta',
        'atacama': 'III Atacama',
        'coquimbo': 'IV Coquimbo',
        'valparaiso': 'V Valpara�so',
        'ohiggins': 'VI O\'Higgins',
        'maule': 'VII Maule',
        'biobio': 'VIII Biob�o',
        'araucania': 'IX Araucan�a',
        'los_rios': 'XIV Los R�os',
        'los_lagos': 'X Los Lagos',
        'aisen': 'XI Ais�n',
        'magallanes_antartica': 'XII Magallanes & Ant�rtica',
        'region_metropolitana': 'Regi�n Metropolitana'
    }
    cat_names = {
        'inmuebles': 'Inmuebles',
        'vehiculos': 'Veh�culos',
        'computadores_electronica': 'Computadores & electr�nica',
        'hogar': 'Hogar',
        'tiempo_libre': 'Tiempo libre',
        'servicios_negocios_empleo': 'Servicios, negocios y empleo',
        'otros': 'Otros',
        'comprar': 'Comprar',
        'arrendar': 'Arrendar',
        'arriendo_temporada': 'Arriendo de temporada',
        'autos': 'Autos, camionetas y 4x4',
        'camiones_furgones': 'Buses, camiones y furgones',
        'motos': 'Motos',
        'barcos_lanchas_aviones': 'Barcos, lanchas y aviones',
        'accesorios_vehiculos': 'Accesorios y piezas para veh�culos',
        'otros_vehiculos': 'Otros veh�culos',
        'muebles': 'Muebles',
        'electrodomesticos': 'Electrodom�sticos',
        'jardin_herramientas': 'Jard�n, herramientas y exteriores',
        'cama_mesa_bano': 'Cama, mesa y ba�o',
        'moda-vestuario': 'Moda y vestuario',
        'bolsos-bisuteria-accesorios': 'Bolsos, bisuter�a y accesorios',
        'bisuteria_relojes_joyas': 'Bisuter�a, relojes y joyas',
        'deportes_gimnasia': 'Deportes, gimnasia y accesorios',
        'camping_caza_pesca': 'Camping, caza y pesca',
        'bicicletas_ciclismo': 'Bicicletas, ciclismo y accesorios',
        'instrumentos_musicales': 'Instrumentos musicales y accesorios',
        'musica_peliculas': 'M�sica y pel�culas (DVDs, etc.)',
        'libros_revistas': 'Libros y revistas',
        'animales_accesorios': 'Animales y sus accesorios',
        'arte_antiguedades_colecciones': 'Arte, antig�edades y colecciones',
        'hobbies_outdoor': 'Hobbies y outdoor',
        'salud-belleza': 'Salud y belleza',
        'consolas_videojuegos': 'Consolas, videojuegos y accesorios',
        'computadores': 'Computadores y accesorios',
        'celulares': 'Celulares, tel�fonos y accesorios',
        'television_camaras': 'Audio, TV, video y fotograf�a',
        'ofertas_de_empleo': 'Ofertas de empleo',
        'busco_empleo': 'Busco empleo',
        'servicios': 'Servicios',
        'negocios_maquinaria_construccion': 'Negocios, maquinaria y construcci�n',
        'otros_productos': 'Otros productos',
        'vestuario-futura-mama-ninos': 'Vestuario futura mam� y ni�os'
    }

    snaps = ['accounts','diff_reg_cat']
    def _test_options(self, wd, reg_name):
        adlist = desktop_list.AdListDesktop(wd)
        for cat,cat_name in self.categories.items():
            adlist.browse(reg_name, cat_name)
            adlist.wait.until_loaded()

            # Test we are in the right region and category
            adlist.wait.until(lambda d: adlist.tab_region.text.startswith(self.regions[reg_name]))
            adlist.wait.until(lambda d: adlist.side_search_box.selected_category.text == self.cat_names[cat_name]) # fails on race condition. Improve me :(
            #self.assertIn(self.regions[reg_name], adlist.tab_region.text)
            #self.assertEquals(adlist.side_search_box.selected_category.text, self.cat_names[cat_name])

            adlist.open_first_ad()
            #wd.find_element_by_css_selector(".ad a").click()

            adv = desktop_list.AdViewDesktop(wd)
            adv.breadcrumb_category.click()

            self.assertIn("/" + reg_name + "/" + cat_name, wd.current_url)

    @tags('desktop', 'accounts')
    def ptest_frurl_arica_parinacota(self, wd):
        """ test friendly url arica"""
        self._test_options(wd,'arica_parinacota')

    @tags('desktop', 'accounts')
    def ptest_frurl_tarapaca(self, wd):
        """ test friendly url tarapaca"""
        self._test_options(wd,'tarapaca')

    @tags('desktop', 'accounts')
    def ptest_frurl_antofagasta(self, wd):
        """ test friendly url antofagasta"""
        self._test_options(wd,'antofagasta')

    @tags('desktop', 'accounts')
    def ptest_frurl_atacama(self, wd):
        """ test friendly url atacama"""
        self._test_options(wd,'atacama')

    @tags('desktop', 'accounts')
    def ptest_frurl_coquimbo(self, wd):
        """ test friendly url coquimbo"""
        self._test_options(wd,'coquimbo')

    @tags('desktop', 'accounts')
    def ptest_frurl_valparaiso(self, wd):
        """ test friendly url valparaiso"""
        self._test_options(wd,'valparaiso')

    @tags('desktop', 'accounts')
    def ptest_frurl_ohiggins(self, wd):
        """ test friendly url ohiggins"""
        self._test_options(wd,'ohiggins')

    @tags('desktop', 'accounts')
    def ptest_frurl_maule(self, wd):
        """ test friendly url maule"""
        self._test_options(wd,'maule')

    @tags('desktop', 'accounts')
    def ptest_frurl_biobio(self, wd):
        """ test friendly url biobio"""
        self._test_options(wd,'biobio')

    @tags('desktop', 'accounts')
    def ptest_frurl_araucania(self, wd):
        """ test friendly url araucania"""
        self._test_options(wd,'araucania')

    @tags('desktop', 'accounts')
    def ptest_frurl_los_rios(self, wd):
        """ test friendly url los_rios"""
        self._test_options(wd,'los_rios')

    @tags('desktop', 'accounts')
    def ptest_frurl_los_lagos(self, wd):
        """ test friendly url los_lagos"""
        self._test_options(wd,'los_lagos')

    @tags('desktop', 'accounts')
    def ptest_frurl_aisen(self, wd):
        """ test friendly url aisen"""
        self._test_options(wd,'aisen')

    @tags('desktop', 'accounts')
    def ptest_frurl_magallanes_antartica(self, wd):
        """ test friendly url magallanes_antartica"""
        self._test_options(wd,'magallanes_antartica')

    @tags('desktop', 'accounts')
    def ptest_frurl_region_metropolitana(self, wd):
        """ test friendly url region_metropolitana"""
        self._test_options(wd,'region_metropolitana')
