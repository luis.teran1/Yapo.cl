# -*- coding: latin-1 -*-
import yapo
import time
from yapo.steps.trans import insert_ad, review_ad
from yapo.pages.desktop_list import AdListDesktop, SearchBoxSideBar
from yapo.pages.mobile_list import AdListMobile, SearchBox
from yapo.utils import rebuild_index, UserAgents

class TestCalzadoSearch(yapo.SeleniumTest):
    """
    Tests that "Calzado" is searcheable by new params in Desktop
    Class created for US3214387344
    """
    snaps=['accounts']

    def _insert_ad(self, subject, fwtype, fwgend, fwsize):
        insert_ad(None,
            category=4080,
            footwear_type=fwtype,
            footwear_size=fwsize,
            footwear_gender=fwgend,
            password="123123123",
            email="carlos@schibsted.cl",
            region="15",
            condition=1,
            name="carlito",
            phone="99996666",
            create_account=False,
            private_ad=True,
            subject=subject,
            body=subject,
            communes=327,
        )
        review_ad(
            email="carlos@schibsted.cl",
            subject=subject
        )
        rebuild_index()

    def _in_listing(self, wd, should_be_there, subject, category, footwear_gend=None, footwear_size=None, **kwargs):
        # load list
        list = AdListDesktop(wd)
        list.go()

        # fill SB with new params
        sb = SearchBoxSideBar(wd)
        sb.fill('category', category)
        if footwear_gend != None:
            sb.fill('footwear_gend', footwear_gend)
        if footwear_size != None:
            sb.fill('footwear_size', footwear_size)
        for val in kwargs:
            sb.fill(val, kwargs[val])
        sb.search_button.click()

        if should_be_there:
            # Zapatin should be on list
            self.assertTrue('Hemos eliminado los filtros para ampliar tu b�squeda' not in wd.find_element_by_tag_name('body').text)
            list.go_to_ad(subject=subject)
        else:
            self.assertTrue(sb.red_failover.text != "")

    def test_calzado_type(self, wd):
        """ Insert footwear AD, it should be a result when searching by type """
        self._insert_ad('Zapatin', '1|3|5', 1, 1)
        self._in_listing(wd, True, subject='Zapatin', category=4080, footwear_type_1=True)

    def test_calzado_size(self, wd):
        """ Insert footwear AD, it should be a result when searching by size """
        self._insert_ad('Zapaton', '1|3|5', 1, 5)
        self._in_listing(wd, True, subject='Zapaton', category=4080, footwear_gend=1, footwear_size=5)

    def test_calzado_not_in(self, wd):
        """ Insert footwear AD, it should not be a result when searching for different params """
        self._insert_ad('Zapatito', '1|3|5', 1, 1)
        self._in_listing(wd, False, subject='Zapatito', category=4080, footwear_gend=2)
        self._in_listing(wd, False, subject='Zapatito', category=4080, footwear_gend=1, footwear_size=2)
        self._in_listing(wd, False, subject='Zapatito', category=4080, footwear_type_2=True, footwear_type_4=True, footwear_type_6=True)

class TestCalzadoSearchMobile(TestCalzadoSearch):
    """
    Tests that "Calzado" is searcheable by new params in mobile
    Class created for US3214387344
    """
    snaps=['accounts']
    user_agent = UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def _in_listing(self, wd, should_be_there, subject, category, footwear_gend=None, footwear_size=None, **kwargs):
        # load list
        list = AdListMobile(wd)
        list.go_to_region('region_metropolitana')

        # fill SB with new params
        sb = SearchBox(wd)
        sb.fill('category', category)
        sb.wait.until_visible('filter_button')
        sb.open_filters()

        if footwear_gend != None:
            sb.fill('footwear_gend', footwear_gend)
        if footwear_size != None:
            sb.fill('footwear_size', footwear_size)
        for val in kwargs:
            sb.fill(val, kwargs[val])

        sb.search_button.click()

        if should_be_there:
            # Zapatin should be on list
            list.go_to_ad(subject=subject)
        else:
            if subject in [a.text for a in list.titles]:
                raise Exception('SubjectFound', '{0} should not be there'.format(subject))
