# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop
import yapo
from yapo.utils import Categories, setconf, bconf_overwrite
from yapo.geography import Regions
from selenium.common.exceptions import NoSuchElementException, TimeoutException
import time
from yapo.pages.ad_insert import AdInsert
from yapo.pages.mobile_list import AdListMobile
from yapo.pages.mobile import NewAdInsert as AdInsertMobile

class CategoryHidderAdSearchTest(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('desktop', 'category', 'hider', 'search')
    def test_desktop_category_hider_search(self, wd):
        """ Category hider: hide a category for desktop on ad search"""

        cat = Categories.ANIMALES_Y_SUS_ACCESORIOS
        listing = AdListDesktop(wd)
        listing.go()
        try:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '0')
            self.assertTrue(is_category_visible(listing.side_search_box, cat))
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '1')
            self.assertFalse(is_category_visible(listing.side_search_box, cat))
        finally:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '0')

    @tags('mobile', 'category', 'hider', 'search')
    def test_mobile_category_hider_search(self, wd):
        """ Category hider: hide a category for mobile on ad search"""

        cat = Categories.BICICLETAS_CICLISMO_Y_ACCESORIOS
        listing = AdListMobile(wd)
        listing.go()

        try:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '0')
            self.assertTrue(is_category_visible(listing, cat))
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '1')
            self.assertFalse(is_category_visible(listing, cat))
        finally:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(cat), '0')

    @tags('desktop', 'category', 'hider', 'search')
    def test_desktop_parent_category_hider_search(self, wd):
        """ Category hider: hide a parent category and its children categories for desktop on ad search"""
        parent_cat = Categories.VEHICULOS
        children_cats = [Categories.AUTOS_CAMIONETAS_Y_4X4,
                        Categories.MOTOS,
                        Categories.CAMIONES_Y_FURGONES,
                        Categories.ACCESORIOS_Y_PIEZAS_PARA_VEHICULOS,
                        Categories.BARCOS_LANCHAS_Y_AVIONES,
                        Categories.OTROS_VEHICULOS]

        listing = AdListDesktop(wd)
        listing.go()
        try:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '0')
            self.assertTrue(is_category_visible(listing.side_search_box, parent_cat))
            for cat in children_cats:
                self.assertTrue(is_category_visible(listing.side_search_box, cat))

            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '1')
            self.assertFalse(is_category_visible(listing.side_search_box, parent_cat))
            for cat in children_cats:
                self.assertFalse(is_category_visible(listing.side_search_box, cat))
        finally:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '0')

    @tags('mobile', 'category', 'hider', 'search')
    def test_mobile_parent_category_hider_search(self, wd):
        """ Category hider: hide a parent category and its children categories for mobile on ad search"""
        parent_cat = Categories.SERVICIOS_NEGOCIOS_Y_EMPLEO
        children_cats = [Categories.OFERTAS_DE_EMPLEO,
                        Categories.BUSCO_EMPLEO,
                        Categories.SERVICIOS,
                        Categories.NEGOCIOS_MAQUINARIA_Y_CONSTRUCCION]

        listing = AdListMobile(wd)
        listing.go()
        try:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '0')
            self.assertTrue(is_category_visible(listing, parent_cat))
            for cat in children_cats:
                self.assertTrue(is_category_visible(listing, cat))

            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '1')
            self.assertFalse(is_category_visible(listing, parent_cat))
            for cat in children_cats:
                self.assertFalse(is_category_visible(listing, cat))
        finally:
            setconf('*.*.cat.{0}.searchbox_hidden'.format(parent_cat), '0')


class CategoryHidderAdInsertTest(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('desktop', 'category', 'hider', 'ad_insert')
    def test_desktop_category_hider_insert(self, wd):
        """ Category hider: hide a category on desktop on ad insert"""
        cat = Categories.DEPORTES_GIMNASIA_Y_ACCESORIOS
        ad_insert = AdInsert(wd)
        ad_insert.go()
        try:
            setconf('*.*.cat.{0}.ai_hidden'.format(cat), '0')
            self.assertTrue(is_category_visible(ad_insert, cat))

            setconf('*.*.cat.{0}.ai_hidden'.format(cat), '1')
            self.assertFalse(is_category_visible(ad_insert, cat))

        finally:
            setconf('*.*.cat.{0}.ai_hidden'.format(cat), '0')

    @tags('mobile', 'category', 'hider', 'ad_insert')
    def test_mobile_category_hider_insert(self, wd):
        """ Category hider: hide a category on mobile on ad insert"""
        cat = Categories.HOBBIES_Y_OUTDOOR
        ad_insert = AdInsertMobile(wd)
        try:
            setconf('*.*.cat.{0}.ai_hidden'.format(cat), '0')
            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(cat), '0')
            ad_insert.go()
            ad_insert.insert_ad(category=cat)
            self.assertTrue(is_category_visible(ad_insert, cat))

            setconf('*.*.cat.{0}.ai_hidden'.format(cat), '1')
            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(cat), '1')
            ad_insert.go()
            self.assertRaises(NoSuchElementException, ad_insert.insert_ad, category=cat)

        finally:
            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(cat), '0')

    @tags('desktop', 'category', 'hider', 'ad_insert')
    def test_desktop_parent_category_hider_insert(self, wd):
        """ Category hider: hide a parent category and its children categories for desktop on ad insert"""
        parent_cat = Categories.COMPUTADORES_Y_ELECTRONICA
        children_cats = [Categories.COMPUTADORES_Y_ELECTRONICA,
                        Categories.CELULARES_TELEFONOS_Y_ACCESORIOS,
                        Categories.CONSOLAS_VIDEOJUEGOS_Y_ACCESORIOS,
                        Categories.AUDIO_TV_VIDEO_Y_FOTOGRAFIA]

        ad_insert = AdInsert(wd)
        ad_insert.go()
        try:
            setconf('*.*.cat.{0}.ai_hidden'.format(parent_cat), '0')
            self.assertTrue(is_category_visible(ad_insert, parent_cat))
            for cat in children_cats:
                self.assertTrue(is_category_visible(ad_insert, cat))

            setconf('*.*.cat.{0}.ai_hidden'.format(parent_cat), '1')
            self.assertFalse(is_category_visible(ad_insert, parent_cat))
            for cat in children_cats:
                self.assertFalse(is_category_visible(ad_insert, cat))
        finally:
            setconf('*.*.cat.{0}.ai_hidden'.format(parent_cat), '0')

    @tags('mobile', 'category', 'hider', 'ad_insert')
    def test_mobile_parent_category_hider_insert(self, wd):
        """ Category hider: hide a parent category and its children categories for mobile on ad insert"""
        parent_cat = Categories.OTROS
        child_cat = Categories.OTROS_PRODUCTOS

        ad_insert = AdInsertMobile(wd)
        try:
            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(parent_cat), '0')
            ad_insert.go()
            ad_insert.insert_ad(category=child_cat)
            self.assertTrue(is_category_visible(ad_insert, parent_cat, selector = '#category option[value="{0}"]'.format(child_cat)))
            self.assertTrue(is_category_visible(ad_insert, child_cat))

            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(parent_cat), '1')
            ad_insert.go()
            self.assertRaises(NoSuchElementException, ad_insert.insert_ad, category=child_cat)
        finally:
            bconf_overwrite('*.*.cat.{0}.ai_hidden'.format(parent_cat), '0')

def is_category_visible(page, cat, selector = None):
    try:
        page.wait.until(CategoryVisible(page, cat, selector))
    except TimeoutException:
        return False
    else:
        return True

class CategoryVisible(object):
    """ An expectation to locate the category """
    def __init__(self, page, category, selector = None):
        self.page = page
        self.category = category
        self.selector = selector and selector or 'option[value="{0}"]'.format(self.category)

    def __call__(self, driver):
        driver.refresh()
        driver.implicitly_wait(1)
        try:
            self.page.category.find_element_by_css_selector(self.selector)
        except NoSuchElementException:
            return False
        else:
            return True
