from yapo.pages.desktop_list import AdListDesktop
from yapo import SeleniumTest
from yapo import conf
from yapo.decorators import tags
import subprocess


class TestSearchEngineDown(SeleniumTest):

    @tags('desktop')
    def test_url_when_search_engine_is_down(self, wd):
        """ stops asearch, verifies error page and restarts it """

        make = lambda target: subprocess.check_output("make -C {0} {1} 2> /dev/null".format(conf.TOPDIR, target), shell=True)
        make('asearch-regress-stop')

        listing = AdListDesktop(wd)
        listing.go()

        link = wd.find_element_by_css_selector('.maintext a')
        self.assertTrue(link.get_attribute('href').endswith('support/form/?id=3'))
        make('asearch-regress-start')
