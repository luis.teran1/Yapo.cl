# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import SearchBoxSideBar as SearchBoxDesktop, AdListDesktop
from yapo.pages.mobile_list import AdListMobile, SearchBox as SearchBoxMobile
from yapo import utils
import yapo

class SearchFiltersInmoDesktop(yapo.SeleniumTest):
    """
    TestSuite for search filters in cat real estate
    """
    user_agent = utils.UserAgents.FIREFOX_MAC
    SearchBoxSideBar = SearchBoxDesktop
    AdList = AdListDesktop
    snaps = ['noreqs']
    estate_types = {
        '1': 'Departamento',
        '2': 'Casa',
        '3': 'Oficina',
        '4': 'Comercial e industrial',
        '5': 'Terreno',
        '6': 'Estacionamiento, bodega u otro',
        '7': 'Pieza',
        '8': 'Caba�a',
        '9': 'Habitaci�n'
    }

    etypes_in_cat = {
        '1220': ['1', '2', '3', '4', '5', '6'],
        '1240': ['1', '2', '3', '4', '5', '6', '7'],
        '1260': ['1', '2', '8', '9']
    }
    etype_filters = {
        '1000': ['min_price', 'max_price'],
        '1260': {
            '1': ['min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_bathrooms', 'max_bathrooms', 'garage_spaces'],
            '2': ['min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_bathrooms', 'max_bathrooms', 'garage_spaces'],
            '8': ['min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_bathrooms', 'max_bathrooms', 'garage_spaces'],
            '9': ['min_price', 'max_price']
        },
        'default': {
            '1': [
                'min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_size', 'max_size', 'min_bathrooms',
                'max_bathrooms', 'min_condominio', 'max_condominio', 'garage_spaces'
            ],
            '2': [
                'min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_size', 'max_size', 'min_bathrooms',
                'max_bathrooms', 'min_condominio', 'max_condominio', 'garage_spaces'
            ],
            '3': ['min_price', 'max_price', 'min_size', 'max_size', 'garage_spaces'],
            '4': ['min_price', 'max_price', 'min_size', 'max_size', 'garage_spaces'],
            '5': ['min_price', 'max_price', 'min_size', 'max_size'],
            '6': ['min_price', 'max_price', 'min_size', 'max_size', 'min_condominio', 'max_condominio'],
            '7': ['min_price', 'max_price', 'min_condominio', 'max_condominio'],
            '8': ['min_price', 'max_price', 'min_rooms', 'max_rooms', 'min_bathrooms', 'max_bathrooms', 'garage_spaces'],
            '9': ['min_price', 'max_price']
        }
    }

    placeholders = {
        '1000': 'Ej: Departamento con terraza',
        '1220': 'Ej: Casa con patio',
        '1240': 'Ej: Departamento con terraza',
        '1260': 'Ej: Caba�a con vista a la playa'
    }
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('desktop', 'searchbox')
    def test_real_estate_cat_1000(self, driver):
        """Check estate types and search in cat 1000"""
        self.__search_ad(driver, '1000')

    @tags('desktop', 'searchbox')
    def test_real_estate_cat_1220(self, driver):
        """Check estate types and search in cat 1220"""
        self.__search_ad(driver, '1220')

    @tags('desktop', 'searchbox')
    def test_real_estate_cat_1240(self, driver):
        """Check estate types and search in cat 1240"""
        self.__search_ad(driver, '1240')

    @tags('desktop', 'searchbox')
    def test_real_estate_cat_1260(self, driver):
        """Check estate types and search in cat 1260"""
        self.__search_ad(driver, '1260')

    def __search_ad(self, driver, category):
        """
        Helper function
        """
        search_box = self.SearchBoxSideBar(driver)
        search_box.go('/region_metropolitana')
        search_box.select_by_value('category', category)
        self._check_placeholder(search_box, category)

        filter_type = 'default'
        if category in self.etype_filters:
            filter_type = category

        if category in self.etypes_in_cat:
            for etype in self.etypes_in_cat[category]:
                search_box.open_filters()
                self.assertVisible(search_box, 'estate_type')
                search_box.select_by_value('estate_type', etype)
                search_box.wait.until_visible('estate_type')
                self.assertEqual(self.estate_types[etype], search_box.get_first_selected_option('estate_type').text.strip())

                for filter in self.etype_filters[filter_type][etype]:
                    search_box.wait.until_visible(filter)
                    self.assertVisible(search_box, filter)
                search_box.wait.until_visible('search_button')
                search_box.search_button.click()
                search_box.wait.until_loaded()
                ad_list = self.AdList(driver)
                title = "{} {}".format(category, etype)
                first_ad = ad_list.get_first_ad()
                data_first_ad = ad_list.get_ad_data(ad=first_ad)
                self.assertIn(title, data_first_ad['title'])
        else:
            for filter in self.etype_filters[filter_type]:
                search_box.open_filters()
                print(filter)
                search_box.wait.until_visible(filter)
                self.assertVisible(search_box, filter)
            search_box.wait.until_visible('search_button')
            search_box.search_button.click()

    def _check_placeholder(self, search_box, category):
        self.assertEqual(self.placeholders[category], search_box.search_box.get_attribute('placeholder'))


class SearchFiltersInmoMobile(SearchFiltersInmoDesktop):

    user_agent = utils.UserAgents.IOS
    SearchBoxSideBar = SearchBoxMobile
    AdList = AdListMobile

    def _check_placeholder(self, search_box, category):
        pass

    def test_real_estate_cat_1000(self, driver):
        """Check estate types and search in cat 1000"""
        pass


class SearchFilterPricesDesktop(yapo.SeleniumTest):
    """
    TestSuite for search filters prices in desktop
    """
    SearchBoxSideBar = SearchBoxDesktop
    AdList = AdListDesktop
    snaps = ['accounts']
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('desktop', 'searchbox')
    def test_search_by_price_inmo(self, driver):
        """ Search ads by price in inmo """
        list_values = [
            ('1220', [
                ('$ 0', '$ 10.000.000', ['$ 170.000', '$ 40.000']),
                ('$ 20.000.000', '$ 30.000.000', [ '$ 30.000.000', '$ 25.000.000']),
                ('$ 30.000.000', '$ 30.000.000', ['$ 30.000.000']),
                ('$ 200.000.000', '$ 300.000.000', ['$ 250.000.000'])]),
            ('1240', [
                ('$ 0', '$ 200.000', ['$ 190.000']),
                ('$ 300.000', '$ 400.000', ['$ 320.000', '$ 380.000']),
                ('$ 500.000', '$ 600.000', ['$ 540.000', '$ 500.000'])])
        ]
        search_box = self.SearchBoxSideBar(driver)
        ad_list = self.AdList(driver)
        for category, list_prices in list_values:
            for min_price, max_price, price_ads_list in list_prices:
                search_box.go('/region_metropolitana')
                if hasattr(self, 'user_agent'):
                    search_box.open_filters()
                search_box.ordered_fill_form([
                    ('category', category),
                    ('min_price', (min_price)),
                    ('max_price', str(max_price)),
                ])
                search_box.search_button.click()
                self.assertListEqual(price_ads_list, [price.text for price in ad_list.get_ads_price()])


class SearchFilterPricesMobile(SearchFilterPricesDesktop):
    """
    TestSuite for search filters prices in mobile
    """
    user_agent = utils.UserAgents.IOS
    SearchBoxSideBar = SearchBoxMobile
    AdList = AdListMobile


class SearchFilterPricesOnlyDesktop(yapo.SeleniumTest):
    """
    TestSuite for search filters prices only in desktop
    """
    SearchBoxSideBar = SearchBoxDesktop
    AdList = AdListDesktop
    snaps = ['accounts']
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('desktop', 'searchbox')
    def test_search_by_price_checkbox_inmo(self, driver):
        """ Search ads by price in 'inmuebles' checkbox 'comprar' and 'arrendar' """
        list_values = [
            ('type_s', [
                ('$ 0', '$ 10.000.000', ['$ 170.000', '$ 40.000']),
                ('$ 20.000.000', '$ 30.000.000', ['$ 30.000.000', '$ 25.000.000']),
                ('$ 30.000.000', '$ 30.000.000', ['$ 30.000.000']),
                ('$ 200.000.000', '$ 300.000.000', ['$ 250.000.000'])]),
            ('type_u', [
                ('$ 0', '$ 200.000', ['$ 190.000']),
                ('$ 300.000', '$ 400.000', ['$ 320.000', '$ 380.000']),
                ('$ 500.000', '$ 600.000', ['$ 540.000', '$ 500.000'])])
        ]
        search_box = self.SearchBoxSideBar(driver)
        ad_list = self.AdList(driver)
        for checkbox_type, list_prices in list_values:
            for min_price, max_price, price_ads_list in list_prices:
                search_box.go('/region_metropolitana')
                if hasattr(self, 'user_agent'):
                    search_box.open_filters()
                search_box.ordered_fill_form([
                    ('category', '1000'),
                    (checkbox_type, True),
                    ('min_price', (min_price)),
                    ('max_price', str(max_price)),
                ])
                search_box.search_button.click()
                self.assertListEqual(price_ads_list, [price.text for price in ad_list.get_ads_price()])
