# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.pages.desktop import DesktopHome
from yapo.geography import Regions
import yapo


class TestUFPriceListing(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_uf']
    skip_on_ad_list = {}

    def _to_ref_price(self, price):
        return price.replace('(', '').replace(')', ' (*)')

    def _to_bottom_price(self, price):
        return price.replace(')', ')*')

    def _check_on_ad_view(self, wd, list_id, expected):
        ad_view = self.AdView(wd)
        ad_view.go(list_id)

        if 'price' in expected:
            self.assertEqual(ad_view.price.text, expected['price'])
        else:
            self.assertFalse(ad_view.has_price())

        if self.AdView == AdViewDesktop:
            if 'alt_price' in expected:
                ref_price = self._to_ref_price(expected['alt_price'])
                bottom_price = expected['price'] + ' ' + self._to_bottom_price(expected['alt_price'])
                self.assertEqual(ad_view.referencial_price.text, ref_price)
                self.assertEqual(ad_view.price_final.text, bottom_price)
                self.assertEqual(
                    ad_view.reference_price_disclaimer.text,
                    ad_view.THIS_PRICE_IS_REFERENCIAL
                )
            else:
                self.assertFalse(ad_view.has_ref_price())

    def _uf_price_case(self, wd, region, category, search_text, expected):

        ad_list = self.AdList(wd)
        ad_list.search([
            ('region', region),
            ('category', category),
            ('search_box', search_text),
        ])
        ads = ad_list.get_all_ads()

        for id, data in expected.items():
            self.assertTrue(id in ads)
            ad = ads[id]
            print (ad)
            for k, v in data.items():
                if k not in self.skip_on_ad_list:
                    self.assertTrue(k in ad)
                    self.assertEqual(ad[k], v)

        for id, data in expected.items():
            self._check_on_ad_view(wd, id, data)


class TestUFPriceListingDesktop(TestUFPriceListing):

    AdList = AdListDesktop
    AdView = AdViewDesktop

    @tags('desktop', 'ad_list', 'ad_view', 'price')
    def test_uf_price_on_inmo_category(self, wd):
        """All prices on inmo category (1000) must have prices in pesos and uf"""
        self._uf_price_case(
            wd,
            region=Regions.REGION_METROPOLITANA,
            category=1000,
            search_text='central',
            expected={
                '8000088': {'category': 'Vendo', 'price': '$ 85.000.000', 'alt_price': '(UF 3.820,99)'},
                '8000087': {'category': 'Vendo', 'price': 'UF 3.245,98', 'alt_price': '($ 72.208.513)'},
                '8000086': {'category': 'Arriendo', 'price': 'UF 31,55', 'alt_price': '($ 701.846)'},
            }
        )

    @tags('desktop', 'ad_list', 'ad_view', 'price')
    def test_uf_price_not_on_home_category(self, wd):
        """No ad on any other category must have uf price (testing only on home (5000))"""
        self._uf_price_case(
            wd,
            region=Regions.REGION_METROPOLITANA,
            category=5000,
            search_text='',
            expected={
                '8000085': {'category': 'Muebles'},
                '8000069': {'category': 'Muebles', 'price': '$ 678'},
                '8000040': {'category': 'Muebles', 'price': '$ 110.000'},
            }
        )


class TestUFPriceListingMobile(TestUFPriceListing):

    AdList = AdListMobile
    AdView = AdViewMobile
    skip_on_ad_list = {'alt_price'}
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'ad_list', 'ad_view', 'price')
    def test_uf_price_on_inmo_category(self, wd):
        """All prices on inmo category (1000) must have prices in pesos and uf"""
        self._uf_price_case(
            wd,
            region=Regions.REGION_METROPOLITANA,
            category=1000,
            search_text='central',
            expected={
                '8000088': {'price': '$ 85.000.000', 'alt_price': '(UF 3.820,99)'},
                '8000087': {'price': 'UF 3.245,98', 'alt_price': '($ 72.208.513)'},
                '8000086': {'price': 'UF 31,55', 'alt_price': '($ 701.846)'},
            }
        )

    @tags('mobile', 'ad_list', 'ad_view', 'price')
    def test_uf_price_not_on_home_category(self, wd):
        """No ad on any other category must have uf price (testing only on home (5000))"""
        self._uf_price_case(
            wd,
            region=Regions.REGION_METROPOLITANA,
            category=5000,
            search_text='',
            expected={
                '8000085': {},
                '8000069': {'price': '$ 678'},
                '8000040': {'price': '$ 110.000'},
            }
        )
