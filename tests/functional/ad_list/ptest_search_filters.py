# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import SearchBoxSideBar, AdListDesktop, AdViewDesktop
import yapo

class SearchFilters(yapo.SeleniumTest):
    """
    TestSuite for search filters of the listing
    """

    snaps = ['reg_cat']
    _multiprocess_shared_ = True

    @tags('desktop', 'searchbox')
    def ptest_4040(self, driver):
        """
        Test gender selector is visible for category 4040 and that throws
        one result female gender.
        """
        self.__search_ad(driver, '4040', 'Femenino')

    @tags('desktop', 'searchbox')
    def ptest_4060(self, driver):
        """
        Test gender selector is visible for category 4060 and that throws
        one result female gender.
        """
        self.__search_ad(driver, '4060', 'Unisex')

    def __search_ad(self, driver, category, gender):
        """
        Helper function
        """
        search_box = SearchBoxSideBar(driver)
        ad_list = AdListDesktop(driver)
        ad_view = AdViewDesktop(driver)

        search_box.go('/region_metropolitana')
        search_box.fill('category', category)

        self.assertVisible(search_box, 'gender_selector')

        search_box.fill('gender', gender)
        search_box.search_button.click()
        ad_list.open_first_ad()

        self.assertIn(gender, ad_view.adparams_box.text)
