""" Commune Tests """
# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo
from yapo.pages.desktop_list import SearchBoxSideBar, AdListDesktop
from yapo.pages.mobile_list import SearchBox, AdListMobile
from yapo.geography import Regions, Communes
from selenium.common import exceptions


class CommunesInAllCategories(yapo.SeleniumTest):
    """ Commune Desktop Tests """

    snaps = ['accounts', 'diff_maps']
    maxDiff = None

    @tags('commune', 'desktop', 'ad_list')
    def test_communes_loaded(self, webdriver):
        """Tests communes is loaded in all categories"""
        side_search_box = SearchBoxSideBar(webdriver)
        for region, name in Regions._number_to_simple_name.items():
            side_search_box.go('/' + name)
            side_search_box.wait.until_visible('communes_selector')
            side_search_box.communes_selector.click()
            communes_expected = ['Todas'] + [Communes.name(commune) for commune in Regions.communes(region)]
            self.assertEquals(communes_expected, [label.text for label in side_search_box.label_communes])

    def test_communes_in_chile(self, webdriver):
        """Tests communes select in chile not visible"""
        side_search_box = SearchBoxSideBar(webdriver)
        side_search_box.go('/chile')
        side_search_box.wait.until_not_visible('communes_selector')

    def test_search_all_communes(self, webdriver):
        """Selecting all communes of a specific region to display exactly the same messages that selecting only the specific region"""
        adlist_desktop = AdListDesktop(webdriver)
        adlist_desktop.side_search_box.go('/' + Regions.simple_name(15))

        # Selecting all communes "Todas" label
        adlist_desktop.side_search_box.fill('communes', ['Todas'])
        adlist_desktop.side_search_box.search_button.click()
        communes_selected_all = [commune.text for commune in adlist_desktop.communes_ads_list]

        # Selecting all communes one by one
        adlist_desktop.side_search_box.fill('communes', [Communes.name(commune) for commune in Regions.communes(15)])
        adlist_desktop.side_search_box.search_button.click()
        communes_selected_one_by_one = [commune.text for commune in adlist_desktop.communes_ads_list]

        self.assertEquals(communes_selected_one_by_one[:-2], communes_selected_all) # two ads without communes

    def test_search_commune_no_ads(self, webdriver):
        """When there are not ads in a commune must show the rest ads within the same region"""
        num_region = 15
        adlist_desktop = AdListDesktop(webdriver)
        adlist_desktop.side_search_box.go('/' + Regions._number_to_simple_name[num_region])
        adlist_desktop.side_search_box.ordered_fill_form([
            ('category', 2020),
            ('communes', [Communes.name(298)]),
        ])
        adlist_desktop.side_search_box.search_button.click()

        self.assertEquals([Regions.name(num_region)] * 3, [region.text for region in adlist_desktop.regions_ads_list[:3]])

    def test_persistence_communes(self, webdriver):
        """Verify persistence of the values in the communes, when it do a search"""
        num_region = 15
        communes_expected = [Communes.name(commune) for commune in Regions.communes(num_region)]

        side_search_box = SearchBoxSideBar(webdriver)
        side_search_box.go('/' + Regions.simple_name(num_region))
        side_search_box.ordered_fill_form([
            ('category', '2020'),
            ('communes', communes_expected),
        ])
        side_search_box.search_button.click()
        side_search_box.wait.until_visible('communes_selector')
        side_search_box.communes_selector.click()

        communes_selected = [Communes.name(int(value)) for value in [input.get_attribute('data-comid') for input in side_search_box.input_communes_selected]]
        self.assertEquals(communes_expected, communes_selected)

class CommunesInAllCategoriesMobile(yapo.SeleniumTest):
    """ Communes Mobile Tests """

    snaps = ['accounts', 'diff_maps']
    maxDiff = None
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def test_communes_loaded(self, webdriver):
        """Tests communes is loaded in all categories"""
        for region, name in Regions._number_to_simple_name.items():
            adlist = AdListMobile(webdriver)
            adlist.go(name)
            adlist.filter_button.click()
            searchbox = SearchBox(webdriver)
            communes_expected = ['Todas las comunas'] + [Communes.name(commune) for commune in Regions.communes(region)]
            self.assertEquals(communes_expected, [label.text for label in searchbox.comunes_filter.find_elements_by_tag_name('option')])

    def test_communes_in_chile(self, webdriver):
        """ Tests communes select in chile not visible """
        adlist = AdListMobile(webdriver)
        adlist.go('chile')
        adlist.filter_button.click()
        searchbox = SearchBox(webdriver)
        self.assertRaises(exceptions.NoSuchElementException, lambda: searchbox.comunes_filter)

    def test_persistence_communes(self, webdriver):
        """ Verify persistence of the values in the communes, when it do a search """
        adlist = AdListMobile(webdriver)
        adlist.go('region_metropolitana')
        adlist.filter_button.click()
        searchbox = SearchBox(webdriver)
        searchbox.fill('comunes_filter', 'Santiago')
        adlist.search_button.click()
        adlist.filter_button.click()
        self.assertEquals(searchbox.selected('comunes_filter'), 'Santiago')
