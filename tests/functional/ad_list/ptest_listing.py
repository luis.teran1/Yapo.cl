# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from selenium.webdriver.common.action_chains import ActionChains
from yapo.decorators import tags
from yapo.pages import accounts, control_panel, generic, desktop_list
from yapo import utils
import time
import yapo
import yapo.conf

class AdListTabsTest(yapo.SeleniumTest):

    snaps = ['accounts']
    _multiprocess_shared_ = True

    # Test listing tabs text

    @tags('desktop', 'listing')
    def ptest_tab_region(self, wd):
        """test tab_country/tab_region text format with region selected"""
        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse('region_metropolitana')

        # Empty search should have a number on the region
        self.assertRegex(adlist.tab_country.text, '^Chile$')
        self.assertRegex(adlist.tab_region.text, '^Regi�n Metropolitana [0-9]+ - [0-9]+ de [0-9]+')

        # No result search should have nothing but names
        adlist.search_for_ad('asdcsadasd')
        self.assertRegex(adlist.tab_country.text, '^Chile$')
        self.assertRegex(adlist.tab_region.text, '^Regi�n Metropolitana$')

    @tags('desktop', 'listing')
    def ptest_tab_country(self, wd):
        """test tab_country/tab_region text format with no region selected"""
        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse('chile')

        # Empty search should have a number on both
        self.assertRegex(adlist.tab_country.text, '^Chile [0-9]+ - [0-9]+ de [0-9]+')
        self.assertRegex(adlist.tab_region.text, '^Regi�n Metropolitana, [0-9]+')

        # No result search should have nothing on Chile and 0 on region
        adlist.search_for_ad('asdcsadasd')
        self.assertRegex(adlist.tab_country.text, '^Chile$')
        self.assertRegex(adlist.tab_region.text, '^Regi�n Metropolitana, 0$')
