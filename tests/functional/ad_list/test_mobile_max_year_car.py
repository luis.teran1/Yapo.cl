# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.mobile_list import AdListMobile, SearchBox
from yapo.steps import trans
from yapo import utils
import yapo
import datetime

class MobileAdInsertMaxYear(yapo.SeleniumTest):
    """ Testing maximum year car insert and search for mobile """

    snaps=['accounts']
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    now = datetime.datetime.now()
    this_month=now.month
    this_year=now.year

    if this_month >= 9:
        this_year += 1

    @tags('mobile', 'ad_insert', 'searchbox')
    def test_mobile_max_year_car(self, wd):
        """ Insert maximum year car ad for mobile """

        params = {
            'subject': str(self.this_year)+' Car',
            'body': 'yoLo',
            'price': '3500000',
            'private_ad': 'True',
            'name': 'yolo',
            'email': 'carlos@schibsted.cl',
            'phone': '999966666',
            'password': '999966666',
            'password_verification': '999966666',
            'create_account': 'False',
            'accept_conditions': 'True',
            'brand': '3',
            'model': '7',
            'version': '3',
            'regdate': str(self.this_year),
            'gearbox': '1',
            'fuel': '1',
            'cartype': '1',
            'type': 's',
            'mileage': '15000',
            'plates': 'ABCD10'
        }
        mai = NewAdInsert(wd)
        mai.go()
        mai.insert_ad(region="15", communes=327, category="2020", **params)

        mai.submit()
        result = AdInsertResult(wd)
        result.wait.until_present("info_box")
        self.assertTrue(result.was('success'))

        trans.review_ad('carlos@schibsted.cl', str(self.this_year) + ' Car')
        utils.rebuild_index()

        ad_list = AdListMobile(wd)
        ad_list.go_to_region('region_metropolitana')
        ad_list.open_filters()
        ad_list.fill('category', 'Autos, camionetas y 4x4')
        search_box = SearchBox(wd)
        search_box.ordered_fill_form([
            ('min_regdate', str(self.this_year)),
            ('max_regdate', str(self.this_year)),
        ])
        search_box.search()

        expected_title=[
            str(self.this_year) + " Car"
        ]

        self.assertListEqual(expected_title, [i.text for i in ad_list.get_ads_title()])
