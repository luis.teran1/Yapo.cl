# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
import yapo
from yapo.pages.desktop_list import SearchBoxSideBar, AdViewDesktop, AdListDesktop
from yapo.pages.desktop import HeaderElements
from yapo.pages.help import FAQItem

class AisOff(yapo.SeleniumTest):

    bconfs = {'*.*.ais.threshold':'-1'}# For deactivate AIS

    @tags('ais')
    def test_ais_off(self, wd):
        """AIS is not shown when disabled on bconf"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/tarapaca?ca=2_s')
        search_form.fill_form(search_box='autos')
        search_form.search_button.click()
        search_form.wait.until_not_present('search_suggestion')
        self.assertTrue(search_form.red_failover)
        self.assertTrue(search_form.black_failover)

class AisOn(yapo.SeleniumTest):

    bconfs = {'*.*.ais.threshold':'500'}# For deactivate AIS
    snaps = ['1030ads', 'newcat','ais']

    @tags('ais')
    def test_ais_on(self, wd):
        """AIS is shown when enabled on bconf"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/tarapaca?ca=2_s')
        search_form.fill_form(search_box='autos')
        search_form.search_button.click()
        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')

    @tags('ais')
    def test_ais_synonym(self, wd):
        """AIS is shown for a synonym"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/tarapaca?ca=2_s')
        search_form.fill_form(search_box='carro')
        search_form.fill_form(region='3') # To select Chile
        search_form.search_button.click()
        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')
        self.assertEqual(search_form.ais_result_hint.text,
                             "Busca seleccionando 'Autos, camionetas y 4x4' como la categor�a (262 resultados).")

    @tags('ais')
    def test_ais_geography(self, wd):
        """AIS is shown for geography"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/tarapaca?ca=2_s')
        search_form.fill_form(search_box='coquimbo')
        search_form.search_button.click()
        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')
        self.assertEqual(search_form.ais_result_hint.text,
                             "Busca seleccionando 'IV Coquimbo' como la regi�n (45 resultados).")

    @tags('ais')
    def test_ais_misspelling(self, wd):
        """AIS is shown for misspelling"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/region_metropolitana?ca=15_s')
        search_form.fill_form(search_box='vlokswagen')
        search_form.fill_form(region='3') # To select Chile
        search_form.search_button.click()

        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')
        self.assertEqual(search_form.ais_result_hint.text, "VOLKSWAGEN reescribiendo 'vlokswagen' como 'volkswagen' (13 resultados).")
        search_form.ais_result_hint.click()

        self.assertEqual(search_form.val('search_box'), 'volkswagen')

    @tags('ais')
    def test_ais_all(self, wd):
        """AIS is shown for all occurrences"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/coquimbo?ca=5_s')
        search_form.fill_form(search_box='carros sedan en coquimbo')
        search_form.fill_form(region='3') # To select Chile
        search_form.search_button.click()

        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')
        self.assertEqual(search_form.ais_result_hint.text, "SEDAN seleccionando 'IV Coquimbo' como la regi�n, seleccionando 'Autos, camionetas y 4x4' como la categor�a (2 resultados).")
        search_form.result_hint_all.click()

        self.assertEqual(search_form.val('search_box'), 'sedan')

    @tags('ais')
    def test_ais_missing_category(self, wd):
        """AIS is shown for a missing category"""
        search_form = SearchBoxSideBar(wd)
        search_form.go('/los_rios?ca=11_s&q=carro&w=6')
        self.assertEqual(search_form.search_suggestion.text, 'Para mejores resultados, te sugerimos:')
        self.assertEqual(search_form.ais_result_hint.text,
                            "Busca seleccionando 'Autos, camionetas y 4x4' como la categor�a (23 resultados).")

    @tags('ais')
    def test_ais_ads_not_found(self, wd):
        """AIS is shown_for_ads_not_found """
        ad_list = AdListDesktop(wd)
        search_form = SearchBoxSideBar(wd)
        search_form.go('/tarapaca?ca=2_s')
        search_form.fill_form(search_box='carro')
        search_form.search_button.click()
        self.assertEqual(ad_list.no_entries_found.text, 'No se encontraron entradas')
        self.assertEqual(ad_list.suggestion_alternative.text, 'Puedes ampliar tu b�squeda utilizando OR y * en el cuadro de b�squedas.')

        ad_view = ad_list.go_to_ad(subject='Grand danois valpar')
        self.assertEqual(ad_view.subject.text, 'Grand danois valpar')

        header = HeaderElements(wd)
        header.search_link.click()
        search_form.fill_form(search_box='carro')
        search_form.search_button.click()
        ad_list.more_search_suggestions.click()

        help = FAQItem(wd)
        self.assertEqual(help.suggestions.text,'Encuentra lo que est�s buscando. �Es f�cil!')
