# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.desktop_list import AdViewDesktop, SearchBoxSideBar
from yapo.pages.desktop import DesktopHome
from yapo.pages.mobile_list import AdListMobile, SearchBox
from yapo.utils import UserAgents
import yapo


class DesktopBrandsMostWanted(yapo.SeleniumTest):

    snaps = ['android']
    Adlist = AdListDesktop
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
    SearchBoxTest = SearchBoxSideBar

    @tags('searchbox', 'desktop')
    def test_verify_brands_most_wanted(self, wd):
        """ Check if 10� most wanted brands are first"""
        listing = self.Adlist(wd)
        listing.go('region_metropolitana')
        search_box = self.SearchBoxTest(wd)
        search_box.open_filters()
        search_box.fill('category', '2020')
        search_box.search_button.click()
        brands = Select(search_box.brand)
        expected_brands = {
            1: ['18', 'CHEVROLET'],
            2: ['40', 'HYUNDAI'],
            3: ['88', 'TOYOTA'],
            4: ['64', 'NISSAN'],
            5: ['86', 'SUZUKI'],
            6: ['47', 'KIA MOTORS'],
            7: ['68', 'PEUGEOT'],
            8: ['57', 'MAZDA'],
            9: ['31', 'FORD'],
            10: ['89', 'VOLKSWAGEN']
        }
        for index, (key, value) in enumerate(sorted(expected_brands.items(), key=lambda t: t[0])):
            option_value = brands.options[index+1].get_attribute("value")
            option_text = brands.options[index+1].get_attribute("text")
            self.assertEquals(value[0], option_value)
            self.assertEquals(value[1], option_text)

    @tags('searchbox', 'desktop')
    def test_search_brands_most_wanted_by_filters(self, wd):
        """ Check if select a most wanted brands and filter """
        listing = self.Adlist(wd)
        listing.go('region_metropolitana')
        search_box = self.SearchBoxTest(wd)
        if self.SearchBoxTest == SearchBox:
            search_box.filter_button.click()
        search_box.ordered_fill_form([
            ('category', '2020'),
            ('brand', '18'),
        ])
        search_box.search_button.click()

        first_ad = listing.get_first_ad()
        data_first_ad = listing.get_ad_data(ad=first_ad)
        self.assertEquals(len(listing.ads_list), 1)
        self.assertEquals(data_first_ad['title'], 'Chevrolet Aveo sport')

        if self.SearchBoxTest == SearchBox:
            search_box.filter_button.click()

        search_box.ordered_fill_form([
            ('brand', '86'),
        ])
        search_box.search_button.click()

        self.assertEquals(len(listing.ads_list), 1)
        first_ad = listing.get_first_ad()
        data_first_ad = listing.get_ad_data(ad=first_ad)
        self.assertEquals(data_first_ad['title'], 'Suzuki Maruti 800cc 2003')

        if self.SearchBoxTest == SearchBox:
            search_box.filter_button.click()
        search_box.fill('brand', '40')
        search_box.search_button.click()

        if self.SearchBoxTest == SearchBox:
            validate_len_ad_list = 0
        else:
            validate_len_ad_list = 3
        self.assertEquals(len(listing.ads_list), validate_len_ad_list)

    @tags('searchbox', 'desktop')
    def test_verify_brands_most_wanted_selected(self, wd):
        """ Verify most wanted brand are selected """
        listing = self.Adlist(wd)
        listing.go('region_metropolitana')
        search_box = self.SearchBoxTest(wd)
        if self.SearchBoxTest == SearchBox:
            search_box.filter_button.click()
        search_box.ordered_fill_form([
            ('category', '2020'),
            ('brand', '18'),
        ])
        if self.SearchBoxTest == SearchBox:
            search_box.filter_button.click()
        search_box.search_button.click()
        brands = Select(search_box.brand)
        selected_option = brands.first_selected_option.get_attribute("text")

        self.assertEquals(selected_option, "CHEVROLET")


class MobileBrandsMostWanted(DesktopBrandsMostWanted):

    user_agent = UserAgents.IOS
    Adlist = AdListMobile
    SearchBoxTest = SearchBox
