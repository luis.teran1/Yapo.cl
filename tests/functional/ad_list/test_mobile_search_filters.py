# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile_list import AdListMobile, SearchBox, AdViewMobile
from yapo.pages import mobile
import yapo


class SearchFiltersMobile(yapo.SeleniumTest):
    """
    TestSuite for search filters of the listing
    """

    snaps = ['reg_cat']
    _multiprocess_shared_ = True
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'searchbox')
    def test_4040(self, driver):
        """
        Test gender selector is visible for category 4040 and that throws
        one result female gender.
        """
        self.__search_ad(driver, 'Bolsos, bisuter�a y accesorios', 'Femenino')

    @tags('mobile', 'searchbox')
    def test_4060(self, driver):
        """
        Test gender selector is visible for category 4060 and that throws
        one result female gender.
        """
        self.__search_ad(driver, 'Salud y belleza', 'Unisex')

    def __search_ad(self, driver, category, gender):
        """
        Helper function
        """
        ad_list = AdListMobile(driver)
        search_box = SearchBox(driver)

        ad_list.go_to_region('chile')
        ad_list.fill('category', category)
        ad_list.open_filters()

        search_box.wait.until_visible('gender')
        search_box.fill('gender', gender)
        search_box.search_filter.click()
        ad_list.first_ad_link.click()

        ad_view = mobile.AdView(driver)
        self.assertIn(['G�nero', gender], ad_view.get_adparams())

    @tags('mobile', 'listing')
    def test_listing_radio_filters_default_cars(self, wd):
        """
        The radio filters, on 2020, 2040 or 2060 by default must be sell otherwise all.
        """
        search_box = SearchBox(wd)

        search_box.go('/region_metropolitana')
        search_box.filter_button.click()

        self._change_and_check_cat_radio(wd, '2100', search_box, 'type_a')

        for cat in ['2020', '2040', '2060']:
            self._change_and_check_cat_radio(wd, cat, search_box, 'type_s')

        self._change_and_check_cat_radio(wd, '2080', search_box, 'type_a')

    def _change_and_check_cat_radio(self, wd, cat, search_box, radio):
        search_box.fill('category', cat)
        search_box.wait.until_loaded_jquery_ajax()
        self.assertEquals(getattr(search_box, radio).get_attribute('checked'), 'true')
