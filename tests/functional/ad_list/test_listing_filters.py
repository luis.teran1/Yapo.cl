# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import SearchBoxSideBar, AdListDesktop
from yapo import utils
import yapo


class ListingFilters(yapo.SeleniumTest):
    """
    TestSuite for order filters of the listing
    """

    @tags('desktop', 'listing')
    def test_listing_order(self, driver):
        """
        Test behaviour of order by price selector with several categories.
        """
        search_box = SearchBoxSideBar(driver)
        ad_list = AdListDesktop(driver)

        search_box.go('/region_metropolitana')
        self.assertVisible(ad_list, 'order_by_price')
        search_box.fill('category', '2040')
        search_box.search_button.click()
        self.assertVisible(ad_list, 'order_by_price')
        search_box.fill('category', '6000')
        search_box.search_button.click()
        self.assertVisible(ad_list, 'order_by_price')
        search_box.fill('category', '7020')
        search_box.search_button.click()
        self.assertVisible(ad_list, 'order_by_price')
        search_box.fill('category', '7040')
        search_box.search_button.click()
        self.assertNotVisible(ad_list, 'order_by_price')

    @tags('desktop', 'listing')
    def test_listing_radio_filters_default_cars(self, wd):
        """
        The radio filters, on 2020, 2040 or 2060 by default must be sell otherwise all.
        """
        search_box = SearchBoxSideBar(wd)

        search_box.go('/region_metropolitana')

        self.assertEquals(search_box.type_a.get_attribute('checked'), 'true')

        for cat in ['2020', '2040', '2060']:
            search_box.fill('category', cat)
            self.assertEquals(search_box.type_s.get_attribute('checked'), 'true')

        search_box.fill('category', '2080')
        self.assertEquals(search_box.type_a.get_attribute('checked'), 'true')
