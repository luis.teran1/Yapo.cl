# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.desktop import DesktopHome
import yapo


class SearchFailover(yapo.SeleniumTest):

    snaps = ['android']

    @tags('searchbox', 'desktop')
    def test_search_failover_by_filters(self, wd):
        """ Failover works when searching using only filters """
        list = AdListDesktop(wd)
        list.go('region_metropolitana')

        list.side_search_box.ordered_fill_form([
            ('category', '2020'),
            ('brand', 'CHEVROLET'),
            ('model', 'AVEO'),
        ])
        list.side_search_box.search_button.click()

        self.assertEquals(len(list.ads_list), 1)
        self.assertNotVisible(list.side_search_box, 'red_failover')
        
        first_ad = list.get_first_ad()
        data_first_ad = list.get_ad_data(ad=first_ad)
        self.assertEquals(data_first_ad['title'], 'Chevrolet Aveo sport')

        list.side_search_box.ordered_fill_form([
            ('brand', 'SUZUKI'),
            ('model', 'MARUTI'),
        ])
        list.side_search_box.search_button.click()

        self.assertEquals(len(list.ads_list), 1)
        self.assertNotVisible(list.side_search_box, 'red_failover')
        first_ad = list.get_first_ad()
        data_first_ad = list.get_ad_data(ad=first_ad)
        self.assertEquals(data_first_ad['title'], 'Suzuki Maruti 800cc 2003')
        
        list.side_search_box.fill('model', 'XL7')
        list.side_search_box.search_button.click()

        self.assertEquals(len(list.ads_list), 3)
        self.assertVisible(list.side_search_box, 'red_failover')

    @tags('searchbox', 'desktop')
    def test_search_failover_by_user_search(self, wd):
        """ Failover works when searching using keywords """

        list = AdListDesktop(wd)
        list.go('los_lagos')

        list.side_search_box.ordered_fill_form([
            ('category', '5040'),
            ('search_box', 'cama'),
        ])
        list.side_search_box.search_button.click()
        self.assertVisible(list.side_search_box, 'red_failover')

    @tags('searchbox', 'desktop', 'home')
    def test_search_failover_from_home(self, wd):
        """ Clicking regions from home fails over when there are less than 30 results """
        home = DesktopHome(wd)
        home.go()
        home.click_region(8)

        list = AdListDesktop(wd)
        self.assertEquals(list.side_search_box.red_failover.text, 'Fueron encontrados menos de 30 avisos.')
        self.assertEquals(list.tab_country.text, 'Chile 1 - 50 de 70')
        self.assertEquals(list.tab_region.text, 'VII Maule, 0')
        
        ad = list.go_to_ad(subject='Rechazado 4')
        self.assertEquals(ad.subject.text, 'Rechazado 4')

        ad.top_next_link.click()
        self.assertEquals(ad.subject.text, 'Rejected Ad 2')

        ad.top_next_link.click()
        self.assertEquals(ad.subject.text, 'Libro para Psicologo Manual Diagnostico')

        ad.top_back_link.click()
        self.assertEquals(list.tab_country.text, 'Chile 1 - 50 de 70')
        self.assertEquals(list.tab_region.text, 'VII Maule, 0')

        region = list.side_search_box.get_first_selected_option('region')
        self.assertEquals(region.text, 'Chile')


