# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo import utils
import time
import yapo
import yapo.conf
from selenium import webdriver


class MobileARPrefilledFieldsTest(yapo.SeleniumTest):

    snaps = ['accounts']
    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts', 'adreply')
    def test_prefilled_fields_when_logged(self, wd):
        """ Check if fields are pre filled for a logged user in mobile """
        account_page = mobile.Login(wd)
        account_page.go()
        account_page.login('prepaid5@blocket.se', '123123123')
        adview_page = mobile.AdView(wd)
        adview_page.go_vi('8000038')
        adview_page.check_prefilled_form('BLOCKET', 'prepaid5@blocket.se', '987654321')
