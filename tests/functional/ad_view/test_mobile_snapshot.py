from yapo.decorators import tags
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.pages import mobile
import yapo
import time

class MobileSnapshot(yapo.SeleniumTest):
    snaps = ['android']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0', '*.*.new_seller_info.categories.disabled': '0', '*.*.new_seller_info.percent_show': 100}
    @tags('mobile', 'listing')
    def test_mobile_snapshot(self, wd):
        wd.get(yapo.conf.MOBILE_URL + "/region_metropolitana");
        page = AdListMobile(wd)
        page.fill_form(categories = '2020')
        time.sleep(3)
        page.search_button.click()
        ad = page.ads_li[1].find_element_by_id("8000015")
        ad.click()
        page = mobile.AdView(wd)
        self.assertEquals(page.subject.text,"Chevrolet Aveo sport")
        self.assertEquals(page.body.text,"Auto en perfectas condiciones, cuenta con cierre centralizado, alza vidrios con interfaz, sunroof.")
        self.assertEquals(page.ad_reply_advertiser_name.text,"Android")
        self.assertEquals(page.price.text,"$ 3.500.000")
        wd.execute_script("window.scrollTo(0, 100000);")
        page.sticky_call_phone.click()
        self.assertEquals(page.call_show_number.text,"457764874")
