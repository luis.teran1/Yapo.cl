# -*- coding: latin-1 -*-
import yapo, re
from redis import Redis
from subprocess import call
from yapo import utils, conf
from yapo.decorators import tags
from yapo.pages.desktop import Login
from yapo.pages.control_panel import ControlPanel
from yapo.steps import delete_ad, insert_and_publish_ad
from yapo.pages.desktop_list import AdDeletePage, AdViewDesktop, AdListDesktop


class AdNotFound(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('desktop', 'ad_not_found', 'ad_view')
    def test_number_of_suggestions(self, wd):
        """Check if the number of suggestions is correct"""

        list_id = '8000074'
        delete_ad(wd, list_id)
        utils.rebuild_index()

        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)

        num_of_ads = 9
        self.assertEqual(num_of_ads, len(ad_view.not_found_ad_list))

    @tags('desktop', 'ad_not_found', 'ad_view')
    def test_suggest_list_no_results(self, wd):
        """Check if suggest list is correct by the searchflow in asearch"""

        Redis(port=int(conf.REGRESS_REDISWORDSRANKING_PORT)).flushall()
        topdir = conf.TOPDIR
        ad_list = AdListDesktop(wd)
        ad_list.go()
        ad_listing_titles = ad_list.titles_ads_list[:9]
        title_list_text = [title_list.text for title_list in ad_listing_titles]
        list_id = '6000666'
        delete_ad(wd, list_id)
        utils.rebuild_index()

        call(["make", "-C", topdir, "cleanlogs"])
        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)
        suggest_list_titles = ad_view.not_found_ad_list_titles
        title_list_sug_text = [title_list.text for title_list in suggest_list_titles]
        self.assertEqual(title_list_text, title_list_sug_text)

        log_path = "{}/regress_final/logs/asearch.log".format(topdir)
        queries = []
        expected_queries = ['0 lim:20 type:s,k,h,u,b category:6020 region:12 communes:268 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 region:12 communes:268 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 region:12 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 region:12 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 pesas muy grandes',
                            '0 lim:20 type:s,k,h,u,b category:6020 region:12 communes:268',
                            '0 lim:20 type:s,k,h,u,b category:6020 region:12',
                            '0 lim:20 type:s,k,h,u,b category:6020', '0 lim:20 type:s,k,h,u,b region:12',
                            '0 lim:20 type:s,k,h,u,b *:*']
        with open(log_path) as file:
            for line in file.readlines():
                if 'Search (0 lim:20' in line:
                    query = line[line.find("S")+8:line.find(") from")]
                    queries.append(query)
        for exp in expected_queries:
            self.assertIn(exp,queries)

    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete')
    def test_ad_delete_over_30_elements(self, wd):
        """Checking presence of elements when an ad is deleted over 30 days ago"""

        list_id = '69696969'
        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)

        ad_view.wait.until_visible("not_found_ad_list")
        ad_view.wait.until_visible("not_found_button")
        self.assertEqual("Este aviso ya no est� disponible en yapo.cl", ad_view.not_found_speech.text)
        self.assertEqual("Te recomendamos avisos similares al que buscabas.", ad_view.not_found_sub_speech.text)
        self.assertEqual("Quiero ver m�s!", ad_view.not_found_button.text)

    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete')
    def test_ad_hidden(self, wd):
        """Checking presence of elements when an ad is hidden"""

        list_id = '8000014'
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(list_id=list_id)
        utils.rebuild_index()
        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)

        ad_view.wait.until_visible("not_found_ad_list")
        ad_view.wait.until_visible("not_found_button")
        self.assertEqual("Este aviso ya no est� disponible en yapo.cl", ad_view.not_found_speech.text)
        self.assertEqual("Te recomendamos avisos similares al que buscabas.", ad_view.not_found_sub_speech.text)
        self.assertEqual("Quiero ver m�s!", ad_view.not_found_button.text)

    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete', 'maps')
    def test_elements_in_suggested_ad(self, wd):
        """Checking presence of elements in the ads suggested"""

        insert_and_publish_ad(wd,
                              category='6120',
                              price='15000',
                              communes='327',
                              company_ad=True,
                              subject='Libro de Harry el sucio Potter',
                              email='LarryPotter@hotmail.com',
                              email_verification='LarryPotter@hotmail.com')

        list_id = '8000072'
        delete_ad(wd, list_id)
        utils.rebuild_index()
        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)
        ad_view.wait.until_visible("not_found_first_sug_img")
        self.assertEqual("Libro de Harry el sucio Potter", ad_view.not_found_first_sug_info_title.text)
        self.assertEqual("$ 15.000", ad_view.not_found_first_sug_info_price.text)
        self.assertTrue(re.search('^Hoy \d\d:\d\d$', ad_view.not_found_first_sug_info_date.text))
        self.assertNotEqual(" Aviso Profesional", ad_view.not_found_first_sug_info_pripro.text)

    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete')
    def test_check_messages_accord_delete_option(self, wd):
        """Checking messages in anf page accord to delete option"""

        ads_to_check = { 1:'8000041', 2:'8000040', 4:'8000039', 5:'8000038', 6:'8000037'}
        ad_delete_page = AdDeletePage(wd)
        for reason, list_id in ads_to_check.items():
            ad_delete_page.go(list_id)
            ad_delete_page.delete_ad('123123123', reason)
        utils.rebuild_index()

        ad_view = AdViewDesktop(wd)
        for list_id in ads_to_check:
            ad_view.go(list_id)
            if list_id == '8000041':
                self.assertEqual("Este aviso se ha vendido exitosamente a trav�s de yapo.cl", ad_view.not_found_speech.text)
            else:
                self.assertEqual("Este aviso ya no est� disponible en yapo.cl", ad_view.not_found_speech.text)
            self.assertEqual("Te recomendamos avisos similares al que buscabas.", ad_view.not_found_sub_speech.text)
            self.assertEqual("Quiero ver m�s!", ad_view.not_found_button.text)

    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete')
    def test_check_deleted_ad_from_diferent_urls(self, wd):
        """Checking presence of elements ad not found page, from facebook/twitter/favorites url"""

        delete_ad(wd, '8000073')
        utils.rebuild_index()
        wd.get(conf.DESKTOP_URL + "/region_metropolitana/libros_revistas/adeptus_astartes_codex_8000073.htm")

        ad_view = AdViewDesktop(wd)
        ad_view.wait.until_visible("not_found_ad_list")
        ad_view.wait.until_visible("not_found_button")
        self.assertEqual("Este aviso ya no est� disponible en yapo.cl", ad_view.not_found_speech.text)
        self.assertEqual("Te recomendamos avisos similares al que buscabas.", ad_view.not_found_sub_speech.text)
        self.assertEqual("Quiero ver m�s!", ad_view.not_found_button.text)

class AdNotFoundCategories(yapo.SeleniumTest):

    snaps = ['accounts']


    @tags('desktop', 'ad_not_found', 'ad_view', 'ad_delete')
    def test_check_elements_anf_different_categories(self, wd):
        """Checking presence of elements ad not found page, from different categories ads"""

        ad_delete = AdDeletePage(wd)
        login = Login(wd)
        ad_list = ['8000056', '8000049', '8000042', '8000036', '8000023', '8000018', '8000070', '8000051']
        subject_list = ['Lechuza embalsamada', 'Guardias de seguridad', 'Gateadores N� 20 Calpany',
                        'Aviso de prepaid5', 'Djembe yembe jembe tambor', '9 hectareas de riego tranquilidad',
                        'Adeptus Astartes Codex', 'Lechuza embalsamada']
        for list_id in ad_list:
            if list_id in ['8000070', '8000051']:
                login.login('prepaid3@blocket.se', '123123123')
                ad_delete.go(list_id)
                ad_delete.delete_ad_logged()
                login.logout()
            else:
                delete_ad(wd, list_id)

        utils.rebuild_index()
        ad_view = AdViewDesktop(wd)
        for list_id, subject in zip(ad_list, subject_list):
            ad_view.go(list_id)
            self.assertEqual(subject, ad_view.not_found_first_sug_info_title.text)


