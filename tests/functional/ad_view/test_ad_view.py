# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop import EditPasswordPage as EditPasswordPageDesktop, AdEdit as AdEditDesktop, Payment
from yapo.pages.mobile import EditPasswordPage as EditPasswordPageMobile, AdEdit as AdEditMobile
from yapo.pages.desktop_list import AdViewDesktop, ReportForm, SearchBoxSideBar, AdListDesktop
from yapo.pages.mobile_list import AdViewMobile, AdListMobile, SearchBox as SearchBoxMobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.steps import accept_and_return_ad_id
from yapo.pages import desktop, mobile
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.action_chains import ActionChains
from yapo.pages.maps import MapView
from yapo.mail_generator import Email
from yapo.utils import JsonTool
from yapo import utils
import yapo
import time

class AdView(yapo.SeleniumTest):

    snaps = ['android']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('desktop', 'adview', 'phone')
    def test_phone_image_src(self, wd):
        """ Verify image src in phone """
        adview = AdViewDesktop(wd)
        adview.go('6394118')
        self.assertIn('pg/00Is07YxLB/JiEoAm07jsrmGe44ju8U8VPs4=.gif', adview.ad_reply_advertiser_phone_img.get_attribute('src'))

    @tags('desktop', 'adview', 'scarface')
    def test_scarface_prefilled_email(self, wd):
        """Look for the prefilled email on scarface form when the user is logged in"""
        login = desktop.Login(wd)
        login.login('acc_android@yapo.cl', '123123123')
        ad_view = AdViewDesktop(wd)
        ad_view.go('8000067')
        reasons = [
            'abuse_option_fraud',
            'abuse_option_duplicate',
            'abuse_option_wrong_category',
            'abuse_option_already_sold',
            'abuse_option_company',
            'abuse_option_other_reason',
        ]
        for reason in reasons:
            ad_view.abuse_link_button.click()
            # This clicks on each web elements defined in the "reasons" list
            getattr(ad_view, reason).click()
            abuse_form = ReportForm(wd)
            self.assertEqual(abuse_form.abuse_email.get_attribute('value'), 'acc_android@yapo.cl')
            wd.refresh()


class AdViewCoreDesktop(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_maps']
    AdView = AdViewDesktop
    SearchBox = SearchBoxSideBar
    AdList = AdListDesktop
    EditPasswordPage = EditPasswordPageDesktop
    AdEdit = AdEditDesktop
    ad_insert_class = 'AdInsertCoreDesktop'
    ad_edit_class = 'AdEditCoreDesktop'
    regions = {'14':'magallanes_antartica', '15':'region_metropolitana'}
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0', '*.*.new_seller_info.categories.disabled': '0', '*.*.new_seller_info.percent_show': 100}

    @tags('desktop', 'adview', 'advertiser_name','core2')
    def test_adview_6_images_and_map(self, wd):
        """ Verify a previously inserted ad with 6 images and map """
        test_data_key = 'usb_fan_with_map'
        previous_test_id = '{0}.test_insert_with_map_6_images'.format(self.ad_insert_class)
        ad_data = JsonTool.read_previous_data(test_data_key, previous_test_id)
        sbs = self.SearchBox(wd)
        ald = self.AdList(wd)

        sbs.go_to_region(self.regions[ad_data['region']])
        sbs.fill_form(**{
            'search_box': ad_data['subject'],
            'category': ad_data['category'],
        })
        sbs.search_button.click()
        actions = ActionChains(wd)
        actions.move_to_element(ald.get_first_ad()).click().perform()
        adview = self.AdView(wd)
        self.assertEqual(adview.subject.text, ad_data['subject'])
        self.assertEqual(adview.body.text, ad_data['body'])
        self.assertEqual(adview.ad_reply_advertiser_name.text, ad_data['name'])
        map_view = MapView(wd)
        self.assertTrue(map_view.is_present())
        if utils.is_not_regress():
            self.assertEqual(map_view.region_commune.get_attribute('innerHTML'), 'XII Magallanes &amp; Antártica, Timaukel')
            mlt = Email.create(wd)
            mlt.check_published_ad_mail(self, ad_data['email'], ad_data['subject'])
        else:
            self.assertIn('Quinta Normal', map_view.get_commune(self.AdView == mobile.AdView))

    @tags('desktop', 'adview', 'advertiser_name','core2')
    def test_adview_20_images_and_map(self, wd):
        """ Verify a previously inserted ad with 20 images and a map """
        test_data_key = 'niva_car_with_map'
        previous_test_id = '{0}.test_insert_with_map_20_images'.format(self.ad_insert_class)
        ad_data = JsonTool.read_previous_data(test_data_key, previous_test_id)

        sbs = self.SearchBox(wd)
        ald = self.AdList(wd)

        sbs.go_to_region(self.regions[ad_data['region']])
        sbs.fill_form(**{
            'search_box': ad_data['subject'],
            'category': ad_data['category'],
        })
        sbs.search_button.click()
        actions = ActionChains(wd)
        actions.move_to_element(ald.get_first_ad()).click().perform()
        adview = self.AdView(wd)
        self.assertEqual(adview.subject.text, ad_data['subject'])
        self.assertEqual(adview.body.text, ad_data['body'])
        self.assertEqual(adview.ad_reply_advertiser_name.text, ad_data['name'])
        map_view = MapView(wd)
        self.assertTrue(map_view.is_present())
        if utils.is_not_regress():
            self.assertEqual(map_view.region_commune.get_attribute('innerHTML'), 'XII Magallanes &amp; Antártica, Timaukel')
            mlt = Email.create(wd)
            mlt.check_published_ad_mail(self, ad_data['email'], ad_data['subject'])
        else:
            self.assertIn('Quinta Normal', map_view.get_commune(self.AdView == mobile.AdView))

    @tags('desktop', 'adview', 'advertiser_name','core2')
    def test_adview_job_ad(self, wd):
        """ Verify a previously inserted job ad """
        test_data_key = 'job_cobol_developer'
        previous_test_id = '{0}.test_insert_job_ad'.format(self.ad_insert_class)
        ad_data = JsonTool.read_previous_data(test_data_key, previous_test_id)

        sbs = self.SearchBox(wd)
        ald = self.AdList(wd)

        sbs.go_to_region(self.regions[ad_data['region']])
        sbs.fill_form(**{
            'search_box': ad_data['subject'],
            'category': ad_data['category'],
        })
        sbs.search_button.click()
        actions = ActionChains(wd)
        actions.move_to_element(ald.get_first_ad()).click().perform()
        adview = self.AdView(wd)
        self.assertEqual(adview.subject.text, ad_data['subject'])
        self.assertEqual(adview.body.text, ad_data['body'])
        self.assertEqual(adview.ad_reply_advertiser_name.text, ad_data['name'])

        if utils.is_not_regress():
            mlt = Email.create(wd)
            mlt.check_published_ad_mail(self, ad_data['email'], ad_data['subject'])

    @tags('desktop', 'adview', 'advertiser_name','core2')
    def test_adview_edited_ad(self, wd):
        """ Verify a previously edited ad """

        ad_data = {}
        if utils.is_not_regress():
            previous_test_id = '{0}.test_edit_ad'.format(self.ad_edit_class)
            ad_data = JsonTool.read_data_of_test(previous_test_id)
        else:
            ad_data['list_id'] = '8000099'
            ad_data['subject'] = 'Edited Ad'
            ad_data['body'] = 'Edited Body'
            ad_data['email'] = 'without_account_2@maps.cl'
            ad_data['phone'] = '96218476'

            edit_pw = self.EditPasswordPage(wd)
            edit_pw.go(ad_data['list_id'])
            edit_pw.validate_password('123123123')

            edit = self.AdEdit(wd)
            edit.insert_ad(subject=ad_data['subject'], body=ad_data['body'], phone_type_mobile=True, phone=ad_data['phone'], price='100000')
            edit.submit.click()

            result = AdInsertResult(wd)
            self.assertTrue(result.was('success'))

            accept_and_return_ad_id(wd, email = ad_data['email'], subject = ad_data['subject'])
            utils.rebuild_index()

        time.sleep(5)
        adview = self.AdView(wd)
        adview.go(ad_data['list_id'])
        self.assertEqual(adview.subject.text, ad_data['subject'])
        self.assertEqual(adview.body.text, ad_data['body'])

        if utils.is_not_regress():
            mlt = Email.create(wd)
            mlt.check_published_edited_ad_mail(self, ad_data['email'], ad_data['subject'])

    @tags('desktop', 'adview')
    def test_adview_4020_category(self, wd):
        """ Verify ad param condition and clothing_size in adview 4020 category """
        ad_view = self.AdView(wd)
        ad_view.go(list_id=8000042)
        self.assertIn('Talla XS o menos', ad_view.adparams_box.text)
        self.assertIn('Nuevo/Usado Usado', ad_view.adparams_box.text)


class AdViewCoreMobile(AdViewCoreDesktop):

    AdView = mobile.AdView
    SearchBox = SearchBoxMobile
    AdList = AdListMobile
    EditPasswordPage = EditPasswordPageMobile
    AdEdit = AdEditMobile
    user_agent = utils.UserAgents.NEXUS_5
    ad_insert_class = 'AdInsertCoreMobile'
    ad_edit_class = 'AdEditCoreMobile'
