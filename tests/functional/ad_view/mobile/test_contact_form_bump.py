#-*- coding: latin-1 -*-
import yapo
import time
import locale
from selenium.webdriver.common.keys import Keys
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages import mobile, mobile_list, stores, generic
from yapo import utils

class Tests(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    def _do_bump(self, wd):
        """  """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_bump('bump_now')
        sp = mobile.SummaryPayment(wd)
        self.assertTrue(sp.big_bump_arrow.is_displayed())

    def _assert_form(self, sp):
        """  """
        self.assertIn("Por favor, cont�ctanos en Atenci�n al Cliente", sp.form_title.text)
        sp.fill_form(
            name = 'carlos',
            email = 'carlos@lol.com',
            body = 'duda',
        )
        sp.send.click()
        time.sleep(1)
        self.assertIn("Mensaje enviado con �xito", sp.send_ok.text)

    def _assert_form_errors(self, sp):
        """  """
        self.assertIn("Por favor, cont�ctanos en Atenci�n al Cliente", sp.form_title.text)
        sp.fill_form(
            name = '',
            email = '',
            body = '',
        )
        sp.send.click()
        self.assertEquals(sp.contact_error_name.text, "Escribe tu nombre")
        self.assertEquals(sp.contact_error_email.text, "Escribe un e-mail v�lido")
        self.assertEquals(sp.contact_error_body.text, "Escribe una descripci�n")

    @tags('mobile', 'ad_view')
    def test_form_message_ok(self, wd):
        """ test that the contact form is desplayed in the bump pay page and is sending mail """
        self._do_bump(wd)
        sp = mobile.SummaryPayment(wd)
        sp.link_to_more_info.click()
        time.sleep(1)
        wd.find_element_by_partial_link_text("aqu�").click()
        time.sleep(1)
        self._assert_form(sp)
        email = generic.SentEmail(wd)
        email.go()
        innerHTML = email.body.get_attribute('innerHTML')
        self.assertIn('-t -i -r <carlos@lol.com>',innerHTML)
        self.assertIn('Subject: =?ISO-8859-1?Q?M�s_informaci�n_sobre_bump?',innerHTML)
        self.assertIn('To: <support@yapo.cl>',innerHTML)
        self.assertIn('Content-Type: text/plain; charset=ISO-8859-1',innerHTML)
        self.assertIn('Content-Transfer-Encoding: quoted-printable',innerHTML)
        self.assertIn('duda',innerHTML)

    @tags('mobile', 'ad_view')
    def test_form_pay_success(self, wd):
        """ test that the contact form is desplayed in the bump pay successful page """
        self._do_bump(wd)
        pay = mobile.Payment(wd)
        pay.pay_button.click()
        ps = mobile.Paysim(wd)
        ps.button_accept.click()
        ps.button_go_site.click()
        sp = mobile.SummaryPayment(wd)
        sp.contactanos.click()
        self._assert_form(sp)

    @tags('mobile', 'ad_view')
    def test_form_pay_refuse(self, wd):
        """ test that the contact form is desplayed in the bump pay refuse page """
        self._do_bump(wd)
        pay = mobile.Payment(wd)
        pay.pay_button.click()
        ps = mobile.Paysim(wd)
        ps.button_refuse.click()
        ps.button_go_site.click()
        sp = mobile.SummaryPayment(wd)
        sp.avisanos.click()
        self._assert_form(sp)

    @tags('mobile', 'ad_view')
    def test_form_error_messages(self, wd):
        """ test that the contact form is showing the error messages """
        self._do_bump(wd)
        sp = mobile.SummaryPayment(wd)
        sp.link_to_more_info.click()
        time.sleep(1)
        wd.find_element_by_partial_link_text("aqu�").click()
        time.sleep(1)
        self._assert_form_errors(sp)

