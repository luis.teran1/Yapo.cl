#-*- coding: latin-1 -*-
import yapo
import time
import locale
from selenium.webdriver.common.keys import Keys
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages import mobile, mobile_list, stores
from yapo import utils

class BackToList(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_ad_from_listing(self, wd):
        """ Test that the back to results button works on ad view from listing """
        listing = mobile_list.AdListMobile(wd)
        listing.go_to_region("aisen")
        listing.go_to_ad(subject="Peluquera a domicilio")
        ad_view = mobile.AdView(wd)
        ad_view.back_to_results.click()
        listing = mobile_list.AdListMobile(wd)
        self.assertEqual(len(listing.ads_list), 1)

    @tags('mobile', 'ad_view')
    def test_vi(self, wd):
        """ Test that the back to results do not works on ad view from vi """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000075')
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.back_to_results)

    @tags('mobile', 'ad_view')
    def test_store(self, wd):
        """ Test that the back to results do not works on ad view from a store """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.store.click()
        store = stores.StoreView(wd)
        store.go_ad_by_subject('Adeptus Astartes Codex')
        ad_view = mobile.AdView(wd)
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.back_to_results)

    @tags('mobile', 'ad_view')
    def test_dashboard(self, wd):
        """ Test that the back to results do not works on ad view from dashboard """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        dash = mobile.DashboardMyAds(wd)
        dash.click_ad_by_subject("Adeptus Astartes Codex")
        ad_view = mobile.AdView(wd)
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.back_to_results)

class Images(yapo.SeleniumTest):
    
    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_no_images(self, wd):
        """ Test that an ad with no images is showing what it should """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertTrue(ad_view.is_element_present('img_noimage'))

    def test_images(self, wd):
        """ Test that an ad with images is showing what it should """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('3456789')
        ad_view.image.click()
        time.sleep(1)
        self.assertIn("images/01/0100012344.jpg", ad_view.get_image_url())
        wd.find_element_by_tag_name('body').send_keys(Keys.ARROW_RIGHT)
        time.sleep(1)
        self.assertIn("images/01/0100012345.jpg", ad_view.get_image_url())
        wd.find_element_by_tag_name('body').send_keys(Keys.ARROW_RIGHT)
        time.sleep(1)
        self.assertIn("images/01/0100012346.jpg", ad_view.get_image_url())

class Title(yapo.SeleniumTest):
    
    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_title_format(self, wd):
        """ Test that the ad title is showing""" 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertEquals(ad_view.subject.text, 'Adeptus Astartes Codex')

class Price(yapo.SeleniumTest):
    
    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_price_showing(self, wd):
        """ Test that the price is showing on the ad """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertEquals(ad_view.price.text, '$ 778.889')

    @tags('mobile', 'ad_view')
    def test_price_not_showing(self, wd):
        """ Test that the price is not showing on an ad without price """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000084')
        self.assertEquals(ad_view.price.text, '')

class Date(yapo.SeleniumTest):
    
    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_date_showing(self, wd):
        """ Test that the date is showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        locale.setlocale(locale.LC_ALL, 'es_CL')
        self.assertEquals(ad_view.date.text.lower(), time.strftime("%-d %B %Y").lower())

class AdParams(yapo.SeleniumTest):
    
    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_simple_params(self, wd):
        """ Test that simple params are showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertEquals(ad_view.get_adparams(), [['Categor�a', 'Libros y revistas - Vendo'], ['C�digo', '8000073']])

    @tags('mobile', 'ad_view')
    def test_car_params(self, wd):
        """ Test that car params are showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000015')
        self.assertEquals(ad_view.get_adparams(), [['Categor�a', 'Autos, camionetas y 4x4 - Vendo'], ['Marca', 'CHEVROLET'], ['Modelo', 'AVEO'], ['Versi�n', 'II LS HB 5P 1.4 MT'],['A�o', '2006'], ['Transmisi�n (cambio)', 'Manual'], ['Combustible', 'Bencina'], ['Tipo de veh�culo', 'Autom�vil'], ['Kil�metros', '140.000 Km'], ['C�digo', '8000015']])

    @tags('mobile', 'ad_view')
    def test_depto_params(self, wd):
        """ Test that depto params are showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000009')
        self.assertEquals(ad_view.get_adparams(), [['Categor�a', 'Arriendo'],['Tipo de inmueble', 'Departamento'],['Dormitorios', '1 dormitorio'],['Superficie', '30m�'],['Estacionamiento', '1'],['C�digo', '8000009']])

    @tags('mobile', 'ad_view')
    def test_service_params(self, wd):
        """ Test that service params are showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('6394117')
        self.assertEquals(ad_view.get_adparams(), [['Categor�a', 'Servicios'],['Tipo', 'Salud/Belleza'],['C�digo', '6394117']])

    @tags('mobile', 'ad_view')
    def test_job_params(self, wd):
        """ Test that jobs params are showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000024')
        self.assertEquals(ad_view.get_adparams(), [['Categor�a', 'Busco empleo'],['Categor�a de trabajo','Administrativo / Secretariado / Finanzas\nComercial / Ventas'],['C�digo', '8000024']])

class Description(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_simple_description(self, wd):
        """ Test that a simple description is showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertEquals(ad_view.description.text, 'for teh emporer!')
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.description_arrow_down)
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.description_arrow_up)


    @tags('mobile', 'ad_view')
    def test_long_description(self, wd):
        """ Test that a long description is showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000049')
        self.assertIn('BUSCAMOS PERSONAS COMO T�! SI ERES DIN�MICO Y CON GANAS DE CRECER EN UNA GRAN EMPRESA', ad_view.description.text) 
        self.assertIn('S�lo debes tener disponibilidad para trabajar en turnos rotativos', ad_view.description.text) 
        self.assertIn('Si est�s interesado EN EL LOCAL DE QUILICURA', ad_view.description.text) 
        self.assertIn('entrevistas el dia JUEVES 6 DE JUNIO desde las 9:00 hasta las 12:30 y desde las 14:30 hasta las 17:00', ad_view.description.text) 
        self.assertIn('interesados en el LOCAL DE LA REINA dirigirse a: Av Francisco Bilbao 8750, en las oficinas de recursos humanos', ad_view.description.text) 
        self.assertTrue(ad_view.is_element_present('description_arrow_down'))

class Map(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_maps']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_showing(self, wd):
        """ Test that the map is showing  """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000100')
        self.assertTrue(ad_view.is_element_present('map'))
        wd.execute_script("$('#map_container').click()")
        time.sleep(1)
        ad_view.wait.until_visible("map_header")

    @tags('mobile', 'ad_view')
    def test_not_showing(self, wd):
        """ Test that the map is not showing """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertTrue(ad_view.is_element_present('map'))

class UserProfile(yapo.SeleniumTest):

    snaps = ['accounts','singlestore'] 
    user_agent = utils.UserAgents.IOS


    @tags('mobile', 'ad_view')
    def test_no_private(self, wd):
        """ person user information shows correctly """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000002')
        self.assertEquals(ad_view.user_name.text, "AndroidTest")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.user_region)
        self.assertEquals(ad_view.user_pro.text, "PRO")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.user_store)

    @tags('mobile', 'ad_view')
    def test_private(self, wd):
        """ private user information shows correctly """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        self.assertEquals(ad_view.user_name.text, "blocket")
        self.assertEquals(ad_view.user_region.text, "Regi�n Metropolitana")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.user_pro)
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.user_store)

    @tags('mobile', 'ad_view')
    def test_store(self, wd):
        """ pro user information shows correctly with store"""
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000078')
        self.assertEquals(ad_view.user_name.text, "ManyAdsAccount")
        self.assertEquals(ad_view.user_region.text, "Regi�n Metropolitana")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.user_pro)
        self.assertTrue(ad_view.is_element_present('user_store'))

class Admin(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_show_hide_admin(self, wd):
        """ test that the admin tools are showable and hideable """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        wd.execute_script("window.scrollTo(0, 100000);")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.admin_hide)
        ad_view.admin_show.click()
        wd.execute_script("window.scrollTo(0, 100000);")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.admin_show)
        ad_view.admin_hide.click()
        wd.execute_script("window.scrollTo(0, 100000);")
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.admin_hide)

    @tags('mobile', 'ad_view')
    def test_delete(self, wd):
        """ test that the delete buttom works """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_delete()
        del_page = mobile.DeletePage(wd)
        self.assertTrue(del_page.is_element_present('reasons'))
        del_page.select_reason(1)
        del_page.select_timer(1)
        self.assertTrue(del_page.is_element_present('password'))

    @tags('mobile', 'ad_view')
    def test_delete_logged(self, wd):
        """ test that the delete buttom works when logged """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_delete()
        del_page = mobile.DeletePage(wd)
        self.assertTrue(del_page.is_element_present('reasons'))
        del_page.select_reason(1)
        del_page.select_timer(1)
        self.assertRaises(exceptions.NoSuchElementException, lambda: del_page.password.click())

    @tags('mobile', 'ad_view')
    def test_edit(self, wd):
        """ test that the edit button works """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_edit()
        edit_page = mobile.EditPasswordPage(wd)
        self.assertEquals(edit_page.title.text, "Editar aviso")

    @tags('mobile', 'ad_view')
    def test_edit_logged(self, wd):
        """ test that the edit button works when logged """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_edit()
        time.sleep(5)
        self.assertIn("editar-aviso", wd.current_url)

    @tags('mobile', 'ad_view')
    def test_bump(self, wd):
        """ test that the bump button works """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_bump('bump_now')
        sp = mobile.SummaryPayment(wd)
        self.assertTrue(sp.big_bump_arrow.is_displayed())

    @tags('mobile', 'ad_view')
    def test_bump_logged(self, wd):
        """ test that the bump button works when logged """
        login = mobile.Login(wd)
        login.login("prepaid5@blocket.se", "123123123")
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        ad_view.do_bump('bump_now')
        self.assertEquals(wd.find_element_by_css_selector("#dashboard_summary").text, "Comprar\n1")

class NextPrevAd(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view')
    def test_next_ad(self, wd):
        """ Test that "next ad" in adview working"""
        listing = mobile_list.AdListMobile(wd)
        ad_view = mobile.AdView(wd)

        listing.go_to_region("region_metropolitana")
        listing.fill('category', 'Comprar')
        listing.search_button.click()

        list_ids = listing.get_list_ids_in_li()
        listing.first_ad_link.click()

        # Compare ads listing with adview
        for list_id in list_ids[0:5]:
            ad_view.wait.until_visible('list_id')

            self.assertEqual(list_id,  ad_view.list_id.text)
            # next ad
            wd.execute_script("window.scrollTo(0, 100000);")
            ad_view.wait.until_visible('next_ad')
            ad_view.next_ad.click()

    @tags('mobile', 'ad_view')
    def test_prev_ad(self, wd):
        """ Test that "prev ad" in adview working"""
        listing = mobile_list.AdListMobile(wd)
        ad_view = mobile.AdView(wd)

        listing.go_to_region("region_metropolitana")
        listing.fill('category', 'Comprar')
        listing.search_button.click()

        list_ids = listing.get_list_ids_in_li()
        # last ad
        listing.ads_list[-1].click()

        list_ids.reverse()

        # Compare ads listing with adview
        for list_id in list_ids[0:5]:
            ad_view.wait.until_visible('list_id')

            self.assertEqual(list_id,  ad_view.list_id.text)
            # prev ad
            wd.execute_script("window.scrollTo(0, 100000);")
            ad_view.wait.until_visible('prev_ad')
            ad_view.prev_ad.click()

    @tags('mobile', 'ad_view')
    def test_prev_ad_limit(self, wd):
        """ Test that "prev ad" limit in adview working"""
        listing = mobile_list.AdListMobile(wd)
        ad_view = mobile.AdView(wd)

        listing.go_to_region("region_metropolitana")
        listing.fill('category', 'Comprar')
        listing.search_button.click()

        listing.first_ad_link.click()
        wd.execute_script("window.scrollTo(0, 100000);")
        ad_view.wait.until_visible('prev_ad')

        self.assertFalse(ad_view.prev_ad.get_attribute('href'))

    @tags('mobile', 'ad_view')
    def test_next_ad_limit(self, wd):
        """ Test that "next ad" limit in adview working"""
        listing = mobile_list.AdListMobile(wd)
        ad_view = mobile.AdView(wd)

        listing.go_to_region("region_metropolitana")
        listing.fill('category', 'Comprar')
        listing.search_button.click()

        # last ad
        listing.ads_list[-1].click()

        wd.execute_script("window.scrollTo(0, 100000);")
        ad_view.wait.until_visible('next_ad')

        self.assertFalse(ad_view.next_ad.get_attribute('href'))

    @tags('mobile', 'ad_view')
    def test_next_and_prev_block(self, wd):
        """ Test that "next ad" and "prev ad" limit in adview working whe url is vi/{list_id}.html"""
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        wd.execute_script("window.scrollTo(0, 100000);")
        ad_view.wait.until_visible('next_ad')

        self.assertFalse(ad_view.next_ad.get_attribute('href'))
        self.assertFalse(ad_view.prev_ad.get_attribute('href'))
