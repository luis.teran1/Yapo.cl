#-*- coding: latin-1 -*-
import yapo
import time
from selenium.webdriver.common.keys import Keys
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages import mobile, mobile_list, stores
from yapo import utils


class TestAdReplyMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_button(self, wd):
        """ Test that the AR button works """ 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.click()
        ad_view.ar_name.send_keys("lololol")

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_empty(self, wd):
        """ Test that the AR show errors on empty fields """ 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        time.sleep(1)
        ad_view.ar_send.click()
        time.sleep(1)
        self.assertEquals(ad_view.ar_error_name.text, "Escribe tu nombre")
        self.assertEquals(ad_view.ar_error_email.text, "Escribe tu e-mail")
        self.assertEquals(ad_view.ar_error_body.text, "Escribe algo")
    
    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_name_error(self, wd):
        """ Test that the AR shows correct errors on bad names """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.send_keys("C")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys("Hola soy carlos :eeehhhh:")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_name.text, "Escribe por lo menos dos letras")
        ad_view.ar_name.clear()
        ad_view.ar_name.send_keys("1111111")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_name.text, "El nombre contiene caracteres no v�lidos")


    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_email_error(self, wd):
        """ Test that the AR show correct errors on bad email """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos_arroba_schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys("Hola soy carlos :eeehhhh:")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_email.text, "Escribe un e-mail v�lido")

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_phone_error(self, wd):
        """ Test that the AR show correct errors on bad phone """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("123")
        ad_view.ar_body.send_keys("Hola soy carlos :eeehhhh:")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_phone.text, "Ingresa un n�mero de tel�fono v�lido")
        ad_view.ar_phone.send_keys("123123123123123123")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_phone.text, "N�mero de tel�fono demasiado largo")

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_body_error(self, wd):
        """ Test that the AR show correct errors on bad body """ 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys(":)")
        ad_view.ar_send.click()
        self.assertEquals(ad_view.ar_error_body.text, "Debes colocar al menos tres letras")
        for i in range(0,300):
            ad_view.ar_body.send_keys("0123456789")
        time.sleep(1)
        ad_view.ar_send.click()
        self.assertRaises(exceptions.NoSuchElementException, lambda: ad_view.ar_error_body)

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_ok_nophone(self, wd):
        """ Test that the AR button works with no phone""" 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()
        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys(":)")
        ad_view.ar_send.click()

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_ok(self, wd):
        """ Test that the AR button works with phone """ 
        ad_view = mobile.AdView(wd)
        ad_view.go_vi('8000073')
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()

        self.assertEqual(ad_view.ar_title.text, "Env�a un mensaje")
        self.assertEqual(ad_view.ar_send.text, "Enviar")

        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys(":) (:")
        ad_view.ar_send.click()

        ad_view.wait.until(lambda wd: ad_view.ar_result_ok.text == "�Mensaje enviado con �xito!")

    @tags('mobile', 'ad_view', 'ad_reply')
    def test_ar_jobs(self, wd):
        """ Test AR texts for jobs category """
        ad_view = mobile.AdView(wd)
        ad_view.go_vi("8000048")
        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        ad_view.sticky_ar.click()

        self.assertEqual(ad_view.ar_title.text, "Postula a este empleo")
        self.assertEqual(ad_view.ar_send.text, "Postular")

        ad_view.ar_name.send_keys("Carlitos")
        ad_view.ar_email.send_keys("carlos@schibsted.cl")
        ad_view.ar_phone.send_keys("")
        ad_view.ar_body.send_keys(":) (:")
        ad_view.ar_send.click()

        ad_view.wait.until(lambda wd: ad_view.ar_result_ok.text == "�Postulaci�n enviada!")
