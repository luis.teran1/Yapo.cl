from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert
from yapo.pages.mobile_list import AdListMobile
from yapo.steps import insert_an_ad
from yapo.steps import trans
from yapo import utils
import yapo


class MobileCarAdView(yapo.SeleniumTest):
    snaps = ['accounts']

    @tags("desktop", "ad_insert", "ad_view")
    def test_brand_0_doesnt_kill_subject(self, wd):
        """ Selecting the brand '0 (Otros)' shows subject in adparams"""
        email = 'yo@lo.cl'
        subject = 'No brand will kill this subject'
        self.insert_car(wd,
                        email=email,
                        email_verification=email,
                        subject=subject,
                        brand='0')
        trans.review_ad(email=email, subject=subject)
        utils.rebuild_asearch()
        self.verify_subject(wd, subject, subject)

    @tags("desktop", "ad_insert", "ad_view")
    def test_model_0_doesnt_kill_subject(self, wd):
        """ Selecting the model '0 (Otros)' shows subject in adparams """
        email = 'yo@lo.cl'
        subject = 'No model will kill this subject'
        self.insert_car(wd,
                        email=email,
                        email_verification=email,
                        subject=subject,
                        brand='1',
                        model='0')
        trans.review_ad(email=email, subject=subject)
        utils.rebuild_asearch()
        self.verify_subject(wd, subject, subject)

    @tags("desktop", "ad_insert", "ad_view")
    def test_version_0_doesnt_kill_subject(self, wd):
        """ Selecting brand model and version shows them in adparams """
        email = 'yo@lo.cl'
        subject = 'This subject will be murdered'
        self.insert_car(wd,
                        email=email,
                        email_verification=email,
                        subject=subject,
                        brand='1',
                        model='1',
                        version='1')
        trans.review_ad(email=email, subject=subject)
        utils.rebuild_asearch()
        self.verify_subject(wd, subject, 'ACADIAN BEAUMONT BEAUMONT')

    def insert_car(self, wd, **kwargs):
        data = {
            'category': '2020',
            'mileage': '5000',
            'regdate': '2015',
            'gearbox': '2',
            'fuel': '1',
            'cartype': '1',
            'private_ad': True,
            'plates':'ABCD10',
        }
        data.update(kwargs)
        insert_an_ad(wd, AdInsert=None, **data)

    def verify_subject(self, wd, subject, adparam_subject):
        list = AdListMobile(wd)
        list.go()
        view = list.go_to_ad(subject=subject)
        self.assertEquals(view.subject.text, subject)
