# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from yapo.decorators import tags
from yapo.pages import desktop_list
from yapo import utils
import yapo
import yapo.conf


class NoImageTest(yapo.SeleniumTest):

    snaps = ['accounts']
    _multiprocess_shared_ = True

    @tags('desktop', 'adview', 'gallery')
    def ptest_no_image_state(self, wd):
        """test no image div for state category"""
        self.check_no_image_div(wd, '8000006', 'state')

    @tags('desktop', 'adview', 'gallery')
    def ptest_no_image_car(self, wd):
        """test no image div for car category"""
        self.check_no_image_div(wd, '8000016', 'car')

    @tags('desktop', 'adview', 'gallery')
    def ptest_no_image_general(self, wd):
        """test no image div for any other category"""
        self.check_no_image_div(wd, '8000039', 'general')

    # helpers

    def check_no_image_div(self, wd, ad_id, type):
        """determines whether or not target div is present"""
        adview = desktop_list.AdViewDesktop(wd)
        adview.go(ad_id)
        target_div = getattr(adview, 'no_image_' + type + '_div')

