# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from selenium.webdriver.common.action_chains import ActionChains
from yapo.decorators import tags
from yapo.pages import accounts, control_panel, generic, desktop_list
from yapo.pages.stores import EditForm 
from yapo import utils
import os
import time
import yapo
import yapo.conf


class NavigationBarTest(yapo.SeleniumTest):

    snaps = ['accounts']
    _multiprocess_shared_ = True
    
    # Test link existence

    @tags('desktop', 'adview')
    def ptest_top_prev_link(self, wd):
        """test navbar top prev link functionality"""
        self._check_nav_link(wd, 'top', 'prev', '8000084', '8000073', 'Aviso anterior')

    @tags('desktop', 'adview')
    def ptest_bottom_prev_link(self, wd):
        """test navbar bottom prev link functionality"""
        self._check_nav_link(wd, 'bottom', 'prev', '8000084', '8000073', 'Aviso anterior')

    @tags('desktop', 'adview')
    def ptest_top_next_link(self, wd):
        """test navbar top next link functionality"""
        self._check_nav_link(wd, 'top', 'next', '8000000', '6394118', 'Pr�ximo aviso')

    @tags('desktop', 'adview')
    def ptest_bottom_next_link(self, wd):
        """test navbar bottom next link functionality"""
        self._check_nav_link(wd, 'bottom', 'next', '8000000', '6394118', 'Pr�ximo aviso')

    # Test hover with no category selected

    @tags('desktop', 'adview')
    def ptest_hover_top_prev_link(self, wd):
        """test navbar top hover previous ad title funcionality"""
        self._check_hover_link(wd, '8000084', 'top', 'prev', 'Adeptus')

    @tags('desktop', 'adview')
    def ptest_hover_bottom_prev_link(self, wd):
        """test navbar bottom hover previous ad title funcionality"""
        self._check_hover_link(wd, '8000084', 'bottom', 'prev', 'Adeptus')

    @tags('desktop', 'adview')
    def ptest_hover_top_next_link(self, wd):
        """test navbar top hover next ad title funcionality"""
        self._check_hover_link(wd, '8000084', 'top', 'next', 'Test 08')

    @tags('desktop', 'adview')
    def ptest_hover_bottom_next_link(self, wd):
        """test navbar bottom hover next ad title funcionality"""
        self._check_hover_link(wd, '8000084', 'bottom', 'next', 'Test 08')

    # Test hover with category selected

    @tags('desktop', 'adview')
    def ptest_hover_top_prev_link_category(self, wd):
        """test navbar top hover previous ad title funcionality within a category"""
        self._check_hover_link(wd, '8000015', 'top', 'prev', 'Suzuki Maruti 800cc 2003', 'autos')

    @tags('desktop', 'adview')
    def ptest_hover_bottom_prev_link_category(self, wd):
        """test navbar bottom hover previous ad title funcionality within a category"""
        self._check_hover_link(wd, '8000015', 'bottom', 'prev', 'Suzuki Maruti 800cc 2003', 'autos')

    @tags('desktop', 'adview')
    def ptest_hover_top_next_link_category(self, wd):
        """test navbar top hover next ad title funcionality within a category"""
        self._check_hover_link(wd, '8000015', 'top', 'next', 'Dodge Ram 1500 V8', 'autos')

    @tags('desktop', 'adview')
    def ptest_hover_bottom_next_link_category(self, wd):
        """test navbar bottom hover next ad title funcionality within a category"""
        self._check_hover_link(wd, '8000015', 'bottom', 'next', 'Dodge Ram 1500 V8', 'autos')

    # MISC

    @tags('desktop', 'adview')
    def ptest_nav_direct_link(self, wd):
        """test navbar behavior when entering adview with a direct link"""
        adview = desktop_list.AdViewDesktop(wd)
        adview.go('8000084')

        # no prev or next link should be available
        self.assertNotPresent(adview, 'top_prev_link')
        self.assertNotPresent(adview, 'top_next_link')

    # helpers

    def _check_nav_link(self, wd, where, what, start_ad, target_ad, expected_text):
        """ checks coherence of next/prev links"""
        self.assertRegex(where, '^(top|bottom)$')
        self.assertRegex(what, '^(next|prev)$')

        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse()

        # check for next requires extra work
        if ( what == 'next' ):
            adlist.last_page_link.click()
        adview = adlist.go_to_ad(start_ad)

        # see that we are where we are suponsed to
        target_link = getattr(adview, where + '_' + what + '_link')
        self.assertEqual(adview.ad_id.get_attribute('value'), start_ad)
        self.assertEqual(target_link.text.lower(), expected_text.lower())

        # follow it and see where we land
        target_link.click()
        self.assertEqual(adview.ad_id.get_attribute('value'), target_ad)

        # now, we are at the first, so there should be no prev link
        self.assertNotPresent(adview, where + '_' + what + '_link')


    def _check_hover_link(self, wd, start_ad, navbar_position, where, title_text, category=None):
        """Function that verifies div that appears when hovering on navigation bar links."""
        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse(category=category)
        adview = adlist.go_to_ad(start_ad)

        link = navbar_position + '_' + where + '_link'
        hover = navbar_position + '_' + where + '_hover'

        navbar_link = getattr(adview, link)
        hover_action = ActionChains(wd).move_to_element(navbar_link)
        hover_action.perform()

        navbar_hover = getattr(adview, hover)
        adview.wait.until(lambda x: title_text.lower() in navbar_hover.text.lower())
        self.assertIn(title_text.lower(), navbar_hover.text.lower())
