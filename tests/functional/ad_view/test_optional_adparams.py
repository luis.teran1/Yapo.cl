# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from yapo.decorators import tags
from yapo.pages.desktop_list import AdViewDesktop
from yapo import utils
import time
import yapo
import yapo.conf
import re


class OptionalAdParams(yapo.SeleniumTest):


    categories = {
        '1220_1':'comprar?ret=1',
        '1220_2':'comprar?ret=2',
        '1220_3':'comprar?ret=3',
        '1220_4':'comprar?ret=4',
        '1220_5':'comprar?ret=5',
        '1220_6':'comprar?ret=6',
        '1240_1':'arrendar?ret=1',
        '1240_2':'arrendar?ret=2',
        '1240_3':'arrendar?ret=3',
        '1240_4':'arrendar?ret=4',
        '1240_5':'arrendar?ret=5',
        '1240_6':'arrendar?ret=6',
        '1240_7':'arrendar?ret=7',
        '1260_1':'arriendo_temporada?ret=1',
        '1260_2':'arriendo_temporada?ret=2',
        '1260_8':'arriendo_temporada?ret=8',
        '1260_9':'arriendo_temporada?ret=9',
        '2020':'autos',
        '2040':'camiones_furgones',
        '2060':'motos',
        '2080':'barcos_lanchas_aviones',
        '2100':'accesorios_vehiculos',
        '2120':'otros_vehiculos',
        '5020':'muebles',
        '5040':'electrodomesticos',
        '5060':'jardin_herramientas',
        '4020':'moda-vestuario',
        '4040':'bolsos-bisuteria-accesorios',
        '4060':'salud-belleza',
        '6020':'deportes_gimnasia',
        '6060':'bicicletas_ciclismo',
        '6080':'instrumentos_musicales',
        '6100':'musica_peliculas',
        '6120':'libros_revistas',
        '6140':'animales_accesorios',
        '6160':'arte_antiguedades_colecciones',
        '6180':'hobbies_outdoor',
        '3020':'consolas_videojuegos',
        '3040':'computadores',
        '3060':'celulares',
        '3080':'television_camaras',
        '7020':'ofertas_de_empleo',
        '7040':'busco_empleo',
        '7060':'servicios',
        '7080':'negocios_maquinaria_construccion',
        '8020':'otros_productos',
        '9020':'vestuario-futura-mama-ninos',
    }

    params_required = {
        '1220_1':['estate_type', 'price', 'rooms', 'size', 'garage_spaces', 'condominio', 'commune', 'bathrooms'],
        '1220_2':['estate_type', 'price', 'rooms', 'size', 'garage_spaces', 'condominio', 'commune', 'bathrooms'],
        '1220_3':['estate_type', 'price', 'size', 'garage_spaces', 'commune'],
        '1220_4':['estate_type', 'price', 'size', 'garage_spaces', 'commune'],
        '1220_5':['estate_type', 'price', 'size', 'condominio', 'commune'],
        '1220_6':['estate_type', 'price', 'size', 'garage_spaces', 'condominio', 'commune'],
        '1240_1':['estate_type', 'price', 'rooms', 'size', 'condominio', 'commune', 'bathrooms', 'garage_spaces'],
        '1240_2':['estate_type', 'price', 'rooms', 'size', 'garage_spaces', 'condominio', 'commune', 'bathrooms'],
        '1240_3':['estate_type', 'price', 'size', 'garage_spaces', 'commune'],
        '1240_4':['estate_type', 'price', 'size', 'garage_spaces', 'commune'],
        '1240_5':['estate_type', 'price', 'size', 'condominio', 'commune'],
        '1240_6':['estate_type', 'price', 'size', 'garage_spaces', 'condominio', 'commune'],
        '1240_7':['estate_type', 'price', 'condominio', 'commune'],
        '1260_1':['estate_type', 'price', 'rooms', 'garage_spaces', 'commune', 'bathrooms', 'capacity',
                    'service_pool', 'service_wifi', 'service_terrace' , 'service_cable_tv', 'service_heating', 'service_kitchen'],
        '1260_2':['estate_type', 'price', 'rooms', 'garage_spaces', 'commune', 'bathrooms', 'capacity',
                    'service_pool', 'service_wifi', 'service_terrace' , 'service_cable_tv', 'service_heating', 'service_kitchen'],
        '1260_8':['estate_type', 'price', 'rooms', 'garage_spaces', 'commune', 'bathrooms', 'capacity',
                    'service_pool', 'service_wifi', 'service_terrace' , 'service_cable_tv', 'service_heating', 'service_kitchen'],
        '1260_9':['estate_type', 'price', 'commune', 'capacity',
                    'service_wifi', 'service_terrace' , 'service_cable_tv', 'service_heating', 'service_bathroom'],
        '2020':['mileage','regdate','cartype','gearbox','fuel'],
        '2040':['mileage','regdate','gearbox','fuel'],
        '2060':['mileage','regdate','cubiccms'],
        '5100':['condition'],
        '5120':['condition'],
        '5140':['condition'],
        '5160':['condition'],
        '7020':['job_category'],
        '7040':['job_category'],
        '7060':['service_type']
    }

    params_label = {
        'rooms':'Dormitorios',
        'capacity':'Capacidad',
        'price':'',
        'size':'Superficie',
        'garage_spaces':'Estacionamiento',
        'condominio':'Gastos comunes',
        'commune':'Comuna',
        'currency':'',
        'mileage':'Kil�metros',
        'regdate':'A�o',
        'cartype':'Tipo de veh�culo',
        'gearbox':'Transmisi�n (cambio)',
        'fuel':'Combustible',
        'cubiccms':'Cilindrada',
        'condition':'Nuevo/Usado',
        'gender':'G�nero',
        'job_category':'Categor�a de trabajo',
        'service_type':'Tipo',
        'estate_type':'Tipo de inmueble',
        'bathrooms':'Ba�os',
        'service_pool': 'Piscina',
        'service_wifi': 'WiFi',
        'service_terrace': 'Terraza',
        'service_cable_tv': 'TV Cable / Sat�lite',
        'service_heating': 'Calefacci�n',
        'service_kitchen': 'Cocina equipada',
        'service_bathroom': 'Ba�o privado'
    }
    snaps = ['noreqs']

    def _test_options(self, wd, reg_name):
            for cat,cat_name in self.categories.items():
                wd.get(yapo.conf.DESKTOP_URL + "/" + reg_name + "/" + cat_name )
                wd.find_element_by_css_selector(".ad a").click()

                adv = AdViewDesktop(wd)

                if cat in self.params_required:
                    for param in self.params_required[cat]:
                        self.assertIn(self.params_label[param], adv.adparams_box.text)

    @tags('desktop', 'accounts', 'splash')
    def test_optional_ad_params_region_metropolitana(self, wd):
        """ test optional ad params region_metropolitana"""
        self._test_options(wd,'region_metropolitana')
