# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from yapo.pages import mobile, control_panel
import yapo
import time
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.steps import trans


class AdMotoEngineTest(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'ad_params', 'moto_engine')
    def test_insert_search_category_with_new_engine_sizes(self, browser):
        """ Testing ad insertion for new values for cubiccms in desktop from a list of predefined cubiccms values"""
        cubiccm_ids = {"8": "100", "10": "180", "16": "550"}
        subject = lambda v: 'Desktop moto {0} cc'.format(v)
        for key, value in cubiccm_ids.items():
            self._insert_ad(browser, AdInsert, subject(value), key)

        for key, value in cubiccm_ids.items():
            print(subject(value))
            trans.review_ad("yolo@cabral.es", subject(value))

        yapo.utils.rebuild_asearch()

        """ validating results """
        ad_list = AdListMobile(browser)
        values = [(('100', '400'), ('100', '180')), (('200', '600'), ('550',))]
        for filters, ads in values:
            print('search with ranges: {0}-{1}'.format(filters[0], filters[1]))
            ad_list.search_for_moto(filters[0], filters[1])
            url = browser.current_url
            for ad in ads:
                print('validating ad with subject {0}'.format(ad))
                ad_list.go_to_ad(subject=subject(ad))
                ad_view = mobile.AdView(browser)
                self.assertIn(['Cilindrada', ad+' cc'], ad_view.get_adparams())
                browser.get(url)

    @tags('ad_insert', 'ad_params', 'moto_engine', 'mobile', 'ucai')
    def test_insert_category_with_new_engine_sizes_mobile(self, browser):
        """ Testing ad insertion for new values for cubiccms in mobile from a list of predefined cubiccms values"""
        cubiccm_ids = {"19": "700", "13": "350", "6": "1000"}
        subject = lambda v: 'Mobile moto {0} cc'.format(v)
        for key, value in cubiccm_ids.items():
            self._insert_ad(browser, mobile.NewAdInsert, subject(value), key)

        for key, value in cubiccm_ids.items():
            trans.review_ad("yolo@cabral.es", subject(value))

        yapo.utils.rebuild_asearch()

        # validating results
        ad_list = AdListDesktop(browser)
        values = [(('200', '750'), ('350', '700')), (('750', '1000'), ('1000',))]
        for filters, ads in values:
            print('search with ranges: {0}-{1}'.format(filters[0], filters[1]))
            ad_list.search_for_moto(filters[0], filters[1])
            url = browser.current_url
            for ad in ads:
                print('validating ad with subject {0}'.format(ad))
                import time; time.sleep(5)
                ad_list.go_to_ad(subject=subject(ad))
                ad_view = AdViewDesktop(browser)
                self.assertEqual(ad_view.cubiccms.text.replace('.', ''), ad + " cc")
                browser.get(url)

    def _insert_ad(self, wd, AdInsert, subject, cubiccms_id):
        trans.insert_ad(AdInsert(wd),
            region="15",
            communes=327,
            category="2060",
            subject=subject,
            body="yoLo",
            regdate="2000",
            mileage="99998",
            cubiccms=cubiccms_id,
            price=999666,
            private_ad=True,
            name="yolo",
            email="yolo@cabral.es",
            email_verification="yolo@cabral.es",
            phone="99996666",
            password="123123123",
            password_verification="123123123",
            create_account=False,
            accept_conditions=True,
            type='s',
            plates='ABC10'
        )
