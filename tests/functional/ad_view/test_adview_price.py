# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdViewDesktop, ReportForm, SearchBoxSideBar, AdListDesktop
from yapo.pages.mobile_list import AdViewMobile, AdListMobile, SearchBox as SearchBoxMobile
from yapo.pages import desktop, mobile
import yapo

class AdViewPriceDesktop(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_ads_with_uf']
    AdView = AdViewDesktop
    platform = desktop

    def _check_price(self, wd, list_id, expected):
        adview = self.AdView(wd)
        adview.go(list_id)
        if len(expected[0]) == 0:
            self.assertFalse(adview.has_price())
            return

        self.assertEqual(adview.price.text, expected[0])
        if self.platform == 'desktop':
            self.assertEqual(adview.referencial_price.text, expected[1])
            self.assertEqual(adview.price_final.text, expected[0] + ' ' + expected[1])

    @tags('desktop', 'adview', 'currency')
    def test_adview_uf_price(self, wd):
        """ Verifies proper display of ad price when currency if UF """
        list_id = 8000088
        expected = ['$ 85.000.000', 'UF 3.820,99.(*)']
        self._check_price(wd, list_id, expected)

    @tags('desktop', 'adview', 'currency')
    def test_adview_peso_price(self, wd):
        """ Verifies proper display of ad price when currency if peso """
        list_id = 8000087
        expected = ['UF 3.245,98', '$ 72.206.825.(*)']
        self._check_price(wd, list_id, expected)

    @tags('desktop', 'adview', 'currency')
    def test_adview_no_price(self, wd):
        """ Verifies proper display of ad view when ad has no price """
        list_id = 8000085
        expected = ['', '', '']
        self._check_price(wd, list_id, expected)


class AdViewPriceMobile(AdViewPriceDesktop):
    AdView = AdViewMobile
    platform = mobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
