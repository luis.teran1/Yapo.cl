#-*- coding: latin-1 -*-
import yapo
import time
from yapo.decorators import tags
from yapo.pages.desktop_list import AdViewDesktop


class TestAdReplyDesktop(yapo.SeleniumTest):

    snaps = ['accounts']

    def _ad_reply_check_texts_case(self, wd, list_id, title, submit, success):
        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)

        self.assertEqual(ad_view.ad_reply_title.text, title)
        self.assertEqual(ad_view.ad_reply_send_button.text, submit)

        ad_view.send_adreply('Carlitos', 'carlos@schibsted.cl', '', ':) (:')
        self.assertEqual(ad_view.ad_reply_success.text, success)

    @tags('desktop', 'ad_view', 'ad_reply')
    def test_ar_ok(self, wd):
        """ Check ad reply texts for regular ad """ 
        data = {'list_id': '8000073', 'title': 'Env�ale un mensaje', 'submit': 'Enviar', 'success': '�Mensaje enviado con �xito!'}
        self._ad_reply_check_texts_case(wd, **data)

    @tags('desktop', 'ad_view', 'ad_reply')
    def test_ar_jobs(self, wd):
        """ Check ad reply texts for jobs category """
        data = {'list_id': '8000048', 'title': 'Postula a este empleo', 'submit': 'Postular', 'success': '�Postulaci�n enviada!'}
        self._ad_reply_check_texts_case(wd, **data)
