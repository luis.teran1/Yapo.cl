# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages.desktop_list import AdViewDesktop

class AdViewViewer(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_accounts_ad_with_many_photos']

    @tags('desktop', 'viewer', 'adview')
    def test_click_thumbnail(self, wd):
        """ Check if main image is updated after clicked a thumbnail """
        adview = self._go_to_ad(wd)
        thumb = adview.images[1]
        thumb_source = self._get_image_source_from_thumbnail(thumb)
        thumb.click()
        self._wait_until_change_image(adview)
        image_source = self._get_current_image_source(adview)
        self.assertEqual(thumb_source, image_source)


    @tags('desktop', 'viewer', 'adview')
    def test_click_main_image(self, wd):
        """ Check if main image is updated after clicked on it """
        adview = self._go_to_ad(wd)
        current_src = self._get_current_image_source(adview)
        adview.main_image.click()
        self._wait_until_change_image(adview)
        new_src = self._get_current_image_source(adview)
        self.assertNotEqual(current_src, new_src)


    @tags('desktop', 'viewer', 'adview')
    def test_check_thumbnail_selected(self, wd):
        """ Check if the first thumbnail is showed as main image """
        adview = self._go_to_ad(wd)
        current_source = self._get_current_image_source(adview)
        thumb = adview.images[0]
        thumb_source = self._get_image_source_from_thumbnail(thumb)
        self.assertEqual(thumb_source, current_source)


    @tags('desktop', 'viewer', 'adview')
    def test_check_change_thumbnail_list(self, wd):
        """ Check if change the main image and the thumbnail selected after click in Arrow Right """
        adview = self._go_to_ad(wd)
        adview.thumb_arrow_right.click()
        self._wait_until_change_image(adview)
        thumb = wd.find_element_by_css_selector(".thumbs .selected img")
        thumb_source = self._get_image_source_from_thumbnail(thumb)
        image_source = self._get_current_image_source(adview)
        self.assertEqual(thumb_source, image_source)


    @tags('desktop', 'viewer', 'adview')
    def test_count_thumbnails(self, wd):
        """ Check if there are all thumbnails """
        adview = self._go_to_ad(wd)
        self.assertEqual(13, len(adview.images))


    def _go_to_ad(self, wd):
        adview = AdViewDesktop(wd)
        adview.go('8000085')
        return adview


    def _get_current_image_source(self, adview):
        return adview.main_image.get_attribute('src')


    def _get_image_source_from_thumbnail(self, thumb):
        src = thumb.get_attribute('src')
        return src.replace('thumbs', 'images')


    def _wait_until_change_image(self, adview):
        return adview.wait.until(lambda x: adview.main_image.get_attribute('src').find('img/wait.gif') == -1)


