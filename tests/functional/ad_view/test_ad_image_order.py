# -*- coding: latin-1 -*-
import yapo
import yapo.conf
from yapo import utils
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from yapo.pages.control_panel import ControlPanel
from yapo.pages import desktop
from yapo.pages.desktop_list import AdViewDesktop
from yapo.decorators import tags
from selenium.common.exceptions import NoSuchElementException

class ImageOrder(yapo.SeleniumTest):

    snaps = ['newinmo']
    password = '123123123'
    list_id = '8000085'
    subject = 'Test with 3 images'
    email = 'yolo@cabral.es'
    image_list = []

    def insert_ad(self, wd):
        """ insert an ad with 3 images and approve it """
        page = AdInsert(wd)
        page.go()
        page.insert_ad(subject=ImageOrder.subject,
                       body="this image",
                       region=15,
                       communes=295,
                       category=8020,
                       price=123456,
                       private_ad=True,
                       name="Yolo",
                       email=ImageOrder.email,
                       email_verification=ImageOrder.email,
                       phone="999966666",
                       password=ImageOrder.password,
                       password_verification=ImageOrder.password,
                       create_account=False,
                       accept_conditions=True)
        page.upload_resource_image('image_order/01.png')
        page.upload_resource_image('image_order/02.png')
        page.upload_resource_image('image_order/03.png')
        page.wait.until_not_present("img_loader")
        page.wait.until_elements_equals(wd, 'img[alt="image"]', 3)

        img1 = wd.find_element_by_css_selector('#uploaded_images li:nth-child(1) img')
        img2 = wd.find_element_by_css_selector('#uploaded_images li:nth-child(2) img')
        img3 = wd.find_element_by_css_selector('#uploaded_images li:nth-child(3) img')

        # controlpanel used thumbs, but ad insert (msite and desktop) use thumbsai.
        ImageOrder.image_list = [
            img1.get_attribute('src').replace('thumbsai', 'thumbs'),
            img2.get_attribute('src').replace('thumbsai', 'thumbs'),
            img3.get_attribute('src').replace('thumbsai', 'thumbs'),
        ]

        wd.execute_script("window.scrollTo(0, 11000000);")
        page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    def delete_image(self, wd, images_to_delete):
        self.edit_ad(
            wd,
            None,
            images_to_delete = images_to_delete,
            subject = None
        )

    def delete_images_and_add_one(self, wd):
        new_images = [
            'image_order/01.png',
        ]
        self.edit_ad(
            wd,
            new_images = new_images,
            images_to_delete = [1],
            subject = None
        )

    def add_two_images(self, wd):
        new_images = [
            'image_order/02.png',
            'image_order/03.png',
        ]
        self.edit_ad(
            wd,
            new_images = new_images,
            images_to_delete = None,
            subject = None
        )

    def change_subject(self, wd, subject):
        ImageOrder.subject = subject
        self.edit_ad(
            wd,
            new_images = None,
            images_to_delete = None,
            subject = subject
        )

    def edit_ad(self, wd, new_images = None, images_to_delete = None, subject = None):
        edit_pw = desktop.EditPasswordPage(wd)
        edit_pw.go(ImageOrder.list_id)
        edit_pw.validate_password(ImageOrder.password)
        edit = desktop.AdEdit(wd)

        if images_to_delete:
            for img in images_to_delete:
                wd.find_element_by_css_selector('#uploaded_images li:nth-child({0}) .bt_delete'.format(img)).click()

        if new_images:
            # upload the new images and update the images_list
            ai_form = AdInsert(wd)
            previous_img = len(ai_form.images)
            for img in new_images:
                edit.upload_resource_image(img)
            ai_form.wait.until_not_present("img_loader")
            ai_form.wait.until_elements_equals(wd, 'img[alt="image"]', previous_img +len(new_images))

        ImageOrder.image_list = []
        ad_images = wd.find_elements_by_css_selector('#uploaded_images li img')
        for i in range(1, len(ad_images)+1):
            img = wd.find_element_by_css_selector('#uploaded_images li:nth-child({0}) img'.format(i))
            ImageOrder.image_list.append(img.get_attribute('src'))

        if subject:
            edit.insert_ad(subject = subject)

        insert = AdInsert(wd)
        insert.wait.until_visible('submit')
        wd.execute_script("window.scrollTo(0, 11000000);")
        import time; time.sleep(5)
        insert.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    def check_first_image_cp(self, wd, first_img, many, queue):
        cp = ControlPanel(wd)
        cp.go()
        if cp.check_login():
            cp.login("dany", "dany")
        cp.search_for_ad(ImageOrder.email, queue = queue)

        first_image = wd.find_element_by_css_selector('.shadowed_thumb img[src*="{0}"]'.format(first_img))
        self.assertTrue(first_img in first_image.get_attribute('src'))

        if many:
            border = wd.find_element_by_css_selector('.extra_bottom_center img[src="/img/thumb_extra_left_bottom.gif"]')
            self.assertTrue('/img/thumb_extra_left_bottom.gif' in border.get_attribute('src'))
        else:
            border = wd.find_element_by_css_selector('.single_bottom_center img[src="/img/thumb_single_left_bottom.gif"]')
            self.assertTrue('/img/thumb_single_left_bottom.gif' in border.get_attribute('src'))

    def review_ad(self, wd, queue, ordered_images = None, new_first = None, subject = None):
        """ var """
        # Workaround: can't use the class itself on function signature
        if not subject:
            subject = ImageOrder.subject
        cp = ControlPanel(wd)
        cp.go()
        cp.search_check_image_order_and_review_ad(ImageOrder.email, subject, 'accept', ordered_images, queue = queue, new_first = new_first)
        cp.logout()

    def rebuildindex(self):
        """ rebuild index """
        utils.rebuild_index()

    def check_image_order_vi(self, wd, ordered_images):
        page = AdViewDesktop(wd)
        page.go(ImageOrder.list_id)
        page.wait.until_loaded()

        i = 0
        if len(ordered_images) > 1:
            wd.execute_script("window.scrollTo(0, document.querySelector('.thumbs').offsetTop);")

            for img in ordered_images:
                image = wd.find_element_by_css_selector('#thumb{0} img[src*="{1}"]'.format(i, img))
                self.assertTrue(img in image.get_attribute('src'))
                i += 1
        else:
            image = wd.find_element_by_id('main_image')
            self.assertTrue(ordered_images[0].split('thumbs')[1] in image.get_attribute('src'))

        self.assertRaises(NoSuchElementException, lambda:
            wd.find_element_by_css_selector('#thumb{0}'.format(i)))
