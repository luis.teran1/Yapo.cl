# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.utils import rebuild_index, find_string_in_file
from yapo.utils import Categories
from yapo.geography import Regions
from yapo.steps import trans, insert_an_ad

class DuplicateIndexedAttribute(yapo.SeleniumTest):

    snaps = ["accounts"]

    @tags('desktop', 'rebuild_index')
    def test_insert_newad_ad_rebuild_index(self, wd):
        """ Check that in the rebuild index not show the error "duplicate indexed attribute: [job_category:12]" in the jobs category"""
        self._insert_and_accept_ad(wd)
        rebuild_index()
        self.assertEquals(False, find_string_in_file(yapo.conf.REGRESS_FINAL + "/logs/aindex.log", "(WARNING): duplicate indexed attribute: [job_category:1] in document id:"))

    def _insert_and_accept_ad(self, wd):
        """
           Insert and accept an ad of the job category by trans.
        """
        #insert an ad in job category
        ad_data = {'subject': "Check that in the rebuild index not show the error",
                   'body': "Check that in the rebuild index not show the error duplicate indexed attribute: [job_category:12] in the jobs category",
                   'region': Regions.REGION_METROPOLITANA,
                   'communes': 331,
                   'category': Categories.SERVICIOS,
                   'private_ad': True,
                   'name': "yolo",
                   'email': "yolito@yoli.cl",
                   'email_verification': "yolito@yoli.cl",
                   'phone': "12345678",
                   'password': "123123123",
                   'password_verification': "123123123",
                   'create_account': False,
                   'accept_conditions': True,
                   'sct': '1'}
        insert_an_ad(wd, **ad_data)

        trans.review_ad(ad_data['email'], ad_data['subject'])

