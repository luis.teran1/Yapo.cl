# -*- coding: latin-1 -*-
import yapo
import copy
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.mobile import NewAdInsert
from yapo.utils import UserAgents, trans, db_fetch_one
from yapo.steps import DEFAULTS
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI

class AdInsertUIDDesktop(yapo.SeleniumTest):
    AdInsert = AdInsertDesktop

    snaps = ['accounts']

    def run_invalid_uid(self, wd):
        def _set_cookie(wd):
            expire_script = 'var expires = new Date();expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));return expires.toUTCString();'
            cookie = {'name': 'uid',
                      'value': '0',
                      'path': '/',
                      'domain': '.schibsted.cl',
                      'expires': wd.execute_script(expire_script),
                      }
            wd.add_cookie(cookie)

        wd.delete_all_cookies()
        page = self.AdInsert(wd)
        page.go()
        _set_cookie(wd)
        self.assertTrue(wd.get_cookie('uid'))
        ad_params = copy.copy(DEFAULTS)
        ad_params.update({'category': '6080', 'email': 'many@ads.cl', 'email_verification': 'many@ads.cl'})
        ad_params.pop('estate_type')
        ad_params.pop('rooms')
        page.insert_ad(**ad_params)
        page.submit.click()
        page = AdInsertResult(wd)
        self.assertTrue(page.was('success'))

    def insert_ad_with_email(self, wd, page, email):
        data = {
           'subject': 'aviso de ' + email,
           'body': 'yoLo map',
           'category': 3040,
           'region': 15,
           'communes': 331,
           'add_location': True,
           'address': 'Avenida San Pablo',
           'address_number': '1361',
           'price': 999666,
           'private_ad': True,
           'name': 'yolo',
           'email': email,
           'email_verification': email,
           'phone': '99996666',
           'password': '999966666',
           'password_verification': '999966666',
           'create_account': True,
           'accept_conditions': True}
        page = insert_an_ad(wd, AdInsert=self.AdInsert, **data)
        self.assertTrue(page.was('success'))

    def insert_and_check_uid(self, wd, page, email, uid):
        self.insert_ad_with_email(wd, page, email)
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        self.assertEqual(wd.get_cookie('uid')['value'], uid)

    def run_uid_create_on_ad_insert(self, wd):
        """ Check if a successful ad insert creates a new uid cookie"""
        wd.delete_all_cookies()
        page = self.AdInsert(wd)
        page.go()
        self.assertIsNone(wd.get_cookie('uid'))
        self.insert_and_check_uid(wd, page, 'yolito@putito.cl', '55')
        self.insert_and_check_uid(wd, page, 'putito@yolito.cl', '55')
        self.insert_and_check_uid(wd, page, 'yolito@putito.cl', '55')

    @tags('ad_insert', 'uid')
    def test_uid_create_on_ad_insert(self, wd):
        """ Check if a successful ad insert creates a new uid cookie"""
        self.run_uid_create_on_ad_insert(wd)

    @tags('ad_insert', 'uid')
    def test_invalid_uid(self, wd):
        """ Insert an ad with UID=0 should succeed instead of throwing ERROR_UID_INVALID """
        self.run_invalid_uid(wd)

class AdInsertUIDMobile(AdInsertUIDDesktop):

    AdInsert = NewAdInsert
    user_agent = UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'uid', 'mobile', 'ucai')
    def test_invalid_uid(self, wd):
        """ Insert an ad with UID=0 should succeed instead of throwing ERROR_UID_INVALID on mobile """
        self.run_invalid_uid(wd)

    @tags('ad_insert', 'uid', 'mobile', 'ucai')
    def test_uid_create_on_ad_insert(self, wd):
        """ Check if a successful ad insert creates a new uid cookie on mobile """
        self.run_uid_create_on_ad_insert(wd)


class AdInsertFocuses(yapo.SeleniumTest):

    override_browser = 'CHROME'

    @tags('ad_insert', 'desktop')
    def test_focus_on_price(self, wd):
        """Check the price is not in focus when reload the ad insert page"""
        ai = AdInsertDesktop(wd)
        ai.go()
        self.assertFalse(ai.is_element_present('focused_price'))

    @tags('ad_insert', 'desktop')
    def test_focus_on_errors(self, wd):
        """Check the focus is on the upper validation error after submit the form"""

        # fields to test (name and value to insert)
        fill_data = [
            {'category': '5020'},
            {'subject': 'titulo de yolo cabral'},
            {'body': 'body de yolo cabral'},
            {'region': '15'},
            {'communes': '296'},
            {'private_ad': True},
            {'name': 'yolo cabral'},
            {'email': 'yolo@cabral.cl'},
            {'email_verification': 'yolo@cabral.cl'},
            {'phone': '87123123'},
            {'password': '123123123'},
            {'password': '123123123', 'password_verification': '123123123'},
            {'accept_conditions': True},
        ]

        focus_check = [
            'subject',
            'body',
            'region',
            'communes',
            'c_ad',
            'name',
            'email',
            'email_confirm',
            'passwd',
            'submit_create_now',
            'passwd'
        ]

        ai = AdInsertDesktop(wd)
        ai.go()

        # test each error
        for i in range(len(fill_data)):
            ai.insert_ad(**fill_data[i])
            ai.submit.click()
            ai.wait.until_loaded()
            focused = wd.switch_to.active_element.get_attribute('id')
            if i + 1 < len(focus_check):
                should_focus = focus_check[i]
                print("{} focused and sould be focused {}".format(focused, should_focus))
                self.assertEqual(focused, should_focus)


class CheckPasswordErrorMessages(yapo.SeleniumTest):

    """ Password validations in desktop ad insert.  """

    AdInsert = AdInsertDesktop
    snaps = ['accounts']

    @tags('ad_insert', 'desktop')
    def test_check_passwd_validation_on_ai_form(self, wd):
        """ Checks password validations in desktop ad insert """

        def fill_password(ai, passwd):
            """ just to fill both passwords """
            ai.fill_form(password=passwd, password_verification=passwd)

        ai = self.AdInsert(wd)
        ai.go()
        ai.fill_form(name='test', email='prepaid5@blocket.se',
                     email_verification='prepaid5@blocket.se', phone='123123')
        fill_password(ai, '1234')
        ai.submit.click()
        self.assertTrue(ai.get_error_for('passwd').is_displayed())
        self.assertEqual(ai.get_error_for('passwd').text,
                         'Tu contrase�a debe tener un m�nimo de 8 caracteres.')
        # Verify max length
        long_password = '1'
        long_password  *= 30
        fill_password(ai, long_password)
        self.assertEquals(len(ai.passwd.get_attribute('value')), 20)

        fill_password(ai, '12345678')
        ai.submit.click()
        self.assertTrue(ai.get_error_for('passwd').is_displayed())
        self.assertIn('Contrase�a incorrecta! El correo que ingresaste ya tiene cuenta, utiliza la misma contrase�a para continuar',
                      ai.get_error_for('passwd').text)
        fill_password(ai, '123123123')
        ai.submit.click()
        self.assertTrue(ai.is_logged())
        self.assertTrue(ai.is_element_present('not_my_account'))


class MobileAdInsertValidateSubject(yapo.SeleniumTest):

    user_agent = UserAgents.IOS
    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    # FT839: Inserts ads in mobile site with lower case in the subject
    # and verifies that the first letter is transformed to uppercase

    def _get_last_ad_id(self):
        return db_fetch_one('select max(ad_id) from ads')[0] + 1

    def test_first_letter_to_uppercase(self, wd):
        """ Verifies that the first letter is transformed to uppercase """
        subject = 'hola que hace'
        expected_subject = 'Hola que hace'
        ad_id = self._get_last_ad_id()
        self.validate_subject(wd, ad_id, subject, expected_subject)

    def test_multiple_spaces_to_one(self, wd):
        """ Verifies that one or more spaces are replaced by one.  """
        subject = '         h   o   l   a    q   u    e     h    a ce'
        expected_subject = 'H o l a q u e h a ce'
        ad_id = self._get_last_ad_id()
        self.validate_subject(wd, ad_id, subject, expected_subject)

    def test_capitalize(self, wd):
        """ Verifies that transform the upper letters to lower letters
            except the first letter """
        subject = 'H O L A Q U E H A C E'
        expected_subject = 'H o l a q u e h a c e'
        ad_id = self._get_last_ad_id()
        self.validate_subject(wd, ad_id, subject, expected_subject)

    def test_03_some_characters_are_removed(self, wd):
        """ Verifies that one or more of the following characters:
            *#<>~- are removed. """
        subject = '***#<>~-***((((hola que hace))))*****'
        expected_subject = '(hola que hace)'
        ad_id = self._get_last_ad_id()
        self.validate_subject(wd, ad_id, subject, expected_subject)

    def validate_subject(self, wd, ad_id, subject, expected_subject):
        """ Inserts ads in mobile site and verifies the subject in trans """

        page = insert_an_ad(wd, AdInsert=NewAdInsert,
            subject=subject,
            body='yoLo map',
            category=3040,
            region=15,
            communes=331,
            add_location=True,
            address='Avenida San Pablo',
            address_number='1361',
            price=999666,
            private_ad=True,
            name='yolo',
            email='joaco@meial.com',
            email_verification='joaco@meial.com',
            phone='99996666',
            password='999966666',
            password_verification='999966666',
            create_account=True,
            accept_conditions=True
        )
        self.assertTrue(page.was('success'))

        result = trans('authenticate', remote_addr='10.0.1.117', username='blocket',
                       passwd='430985da851e9223368e820ebde5beaf6c6ef1cc')
        result = trans('search_ads', search_type='ad_id', ad_id=ad_id,
                       archive_group='all', count_all='1', token=result['token'], remote_addr='10.0.1.117')

        self.assertIn(expected_subject, result['ad.0.ad.subject'])
        self.assertIn('TRANS_OK', result['status'])

class CheckRutErrorMessages(yapo.SeleniumTest):
    @tags('desktop', 'ad_insert', 'rut_validation')
    def test_check_rut_validation_on_ai_form(self, wd):
        """Test each error message over rut field"""

        page = AdInsertDesktop(wd)
        page.go()

        self.assertFalse(page.rut.is_displayed())
        page.company_ad.click()
        self.assertTrue(page.rut.is_displayed())

        page.submit.click()
        self.assertEqual(page.get_error_for('rut').text, 'Escribe tu RUT')

        test_list = [
            ('ase', 'Escribe un rut v�lido'),
            ('1-9', 'Ok!')
        ]

        for input_text, expected_result in test_list:
            page.fill('rut', input_text)
            page.email.click()
            self.assertEqual(page.get_error_for('rut').text, expected_result)

        page.submit.click()
        self.assertEqual(page.get_error_for('rut').text, 'El RUT es muy corto')

        page.fill('rut', '1000004')
        page.email.click()
        self.assertEqual(page.rut.get_attribute('value'), '100.000-4')
        self.assertEqual(page.get_error_for('rut').text, 'Ok!')

        page.submit.click()
        page.wait.until_not_visible('err_rut')


class CheckPriceVisibility(yapo.SeleniumTest):
    AdInsert = AdInsertDesktop

    @tags('ad_insert', 'desktop')
    def test_price_visibility_on_cat_change(self, wd):
        """Test price visibility on category change"""
        page = self.AdInsert(wd)
        page.go()

        page.insert_ad(category=1220, currency='uf', price='1234')
        page.submit.click()

        self.assertEqual(page.get_error_for('subject').text, 'Escribe un t�tulo')

        page.insert_ad(category=1260)
        page.wait.until_visible('price')
        self.assertEqual(page.val('price'), '')

    @tags('ad_insert', 'desktop')
    def test_price_not_cleared_on_cat_change(self, wd):
        """Price is not erased on category change"""
        categories = [2020, 2060, 1240, 5020, 1260]
        price = '45000'

        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(category=1220, currency='peso', price=price)
        self.assertEqual(page.val('price'), price)
        for category in categories:
            page.insert_ad(category=category)
            page.wait.until_visible('price')
            self.assertEqual(page.val('price'), price)


class CheckPriceVisibilityMobile(CheckPriceVisibility):
    AdInsert = NewAdInsert
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
