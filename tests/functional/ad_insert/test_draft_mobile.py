# -*- coding: latin-1 -*-
import yapo, unittest
from yapo import utils, conf
from yapo.decorators import tags
from yapo.geography import Regions, Communes
from yapo.pages import mobile, mobile_list
from yapo.pages.mobile import NewAdInsert
from yapo.pages.control_panel import ControlPanel


class DraftMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.NEXUS_5
    bconfs = {
        '*.*.forced_login.modal.newad.7020.disabled': '1',
        '*.*.forced_login.modal.editad.7020.disabled': '1',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    def _assertRegion(self, ad_insert, region):
        if region is None:
            region = ''
        ad_insert.wait.until(lambda x: ad_insert.region.get_attribute('value') == str(region))

    def _assertAIForm(self, ad_insert, params):
        for k, v in params.items():
            ad_insert.wait.until_present(k)
            elem = ad_insert.__getattribute__(k)
            ad_insert.wait.until(lambda x: ad_insert.val(k) == v, '{} should be [{}] but was [{}]'.format(k, v, ad_insert.val(k)))

    def _change_region(self, ad_insert, region):
        ad_insert.insert_ad(
            category='1220',
            region=region,
            communes=Regions.random_commune(region),
        )
        self._assertRegion(ad_insert, region)

    def _go_home_and_return(self, wd, ad_insert, region=None):
        home = mobile.MobileHome(wd)
        home.go()

        if region:
            list = mobile_list.AdListMobile(wd)
            list.go_to_region(Regions.simple_name(region))

        ad_insert.go()

    @tags('ad_insert', 'draft', 'mobile', 'ucai')
    def test_draft_region_behaviour(self, wd):
        """ Default region is used when no draft. Otherwise use draft region """
        ad_insert = NewAdInsert(wd)
        ad_insert.go()

        # Default region
        self._assertRegion(ad_insert, None)

        # Region saved in draft
        self._change_region(ad_insert, Regions.III_ATACAMA)
        self._go_home_and_return(wd, ad_insert)
        self._assertRegion(ad_insert, Regions.III_ATACAMA)

        # Entering from another region doesn't affect draft
        self._go_home_and_return(wd, ad_insert, Regions.IV_COQUIMBO)
        self._assertRegion(ad_insert, Regions.III_ATACAMA)

        # Clearing form return region
        ad_insert.clean_draft.click()
        self._assertRegion(ad_insert, None)


    @tags('ad_insert', 'draft', 'mobile', 'ucai')
    def test_draft_category_behaviour(self, wd):
        """ Categories from draft should display their ad params """
        ad_insert = NewAdInsert(wd)
        ad_insert.go()

        # Select a category
        ad_insert.insert_ad(category='1220')

        # Draft should save it
        self._go_home_and_return(wd, ad_insert)
        self.assertEqual(ad_insert.parent_category.get_attribute('value'), '1000')
        self.assertEqual(ad_insert.category.get_attribute('value'), '1220')

        # Clearing draft should clear it
        ad_insert.clean_draft.click()
        ad_insert.wait.until(lambda x: ad_insert.parent_category.get_attribute('value') == '')

        # No draft, no category
        self._go_home_and_return(wd, ad_insert)
        self.assertEqual(ad_insert.parent_category.get_attribute('value'), '')

    @tags('ad_insert', 'draft', 'mobile', 'ucai')
    def test_draft_cat_2020_behaviour(self, wd):
        """ All ad params should be saved. Changing s <-> k clears model/version """
        ad_insert = mobile.NewAdInsert(wd)
        ad_insert.go()

        vehicle_sell = {
            'category': '2020',
            'type': 's',
            'brand': '18',
            'model': '19',
            'version': '3',
            'regdate': '2015',
            'gearbox': '2',
            'fuel': '1',
            'cartype': '1',
            'mileage': '200000',
        }

        vehicle_buy = {
            'category': '2020',
            'type': 'k',
            'brand': '18',
            'model': '19',
            'version': '5',
            'regdate': '2015',
            'gearbox': '1',
            'fuel': '1',
            'cartype': '1',
            'mileage': '300000',
        }

        # Fill 'Sell a car' data
        ad_insert.insert_ad(**vehicle_sell)
        ad_insert.insert_ad(name = 'yolito') # This is writed to activate draft
        self._go_home_and_return(wd, ad_insert)
        self._assertAIForm(ad_insert, vehicle_sell)

        # Fill 'Find a car' data
        ad_insert.insert_ad(**vehicle_buy)
        ad_insert.insert_ad(name = 'yolito') # This is writed to activate draft
        self._go_home_and_return(wd, ad_insert)
        self._assertAIForm(ad_insert, vehicle_buy)


    @tags('ad_insert', 'draft', 'mobile', 'ucai')
    def test_draft_real_estate(self, wd):
        """ Check draft for real estate """
        ad_insert = NewAdInsert(wd)
        ad_insert.go()

        real_estate_sell = {
            'category': '1220',
            'estate_type': '1',
            'rooms': '3',
            'bathrooms': '3',
            'size': '100',
            'garage_spaces': '3',
            'condominio': '100000',
            'price': '200000',
        }

        real_estate_rent = {
            'category': '1260',
            'estate_type': '9',
            'srv': ['2', '3'],
        }

        # Fill 'Sell real estate' data
        ad_insert.insert_ad(**real_estate_sell)
        self._go_home_and_return(wd, ad_insert)
        self._assertAIForm(ad_insert, real_estate_sell)

    @unittest.skip("Thei test check that draft message appears in home if do you have a draft, but functionality is broken until all site get to https")
    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_present_information_unlogged(self, wd):
        """ Test the draft information presence when is unlogged """
        data = {
            'category':         '1220',
            'estate_type':      '2',
            'subject':          'Terreno de los industriales',
            'body':             'Terreno drafteador',
            'rooms':            '2',
            'bathrooms':        '3',
            'size':             '1400',
            'garage_spaces':    '3',
            'condominio':       '30000',
            'price':            '5000000',
            'region':           '1',
            'communes':         '1',
            'phone_hidden':     '1',

            'name':             'Rick Astley',
            'phone':            '69969696',
            'email':            'never_gonna@giveyou.up',
            'passwd':           '123123123',
            'passwd_ver':       '123123123',
        }

        self._draft_present_case(wd, data, False)

    @unittest.skip("Thei test check that draft message appears in home if do you have a draft, but functionality is broken until all site get to https")
    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_present_information_logged(self, wd):
        """ Test the draft information presence when is logged """
        data = {
            'category':         '1220',
            'estate_type':      '2',
            'subject':          'Terreno de los industriales',
            'body':             'Terreno drafteador',
            'rooms':            '2',
            'bathrooms':        '3',
            'size':             '1400',
            'garage_spaces':    '3',
            'condominio':       '30000',
            'price':            '5000000',
            'region':           '1',
            'communes':         '1',
            'phone_hidden':     '1',
        }

        self._draft_present_case(wd, data, True)

    def _draft_present_case(self, wd, data, logged):

        if logged:
            page = mobile.Login(wd)
            page.login('prepaid5@blocket.se', '123123123')

        page = mobile.NewAdInsert(wd)
        page.go()
        page.insert_ad(**data)

        page = mobile.MobileHome(wd)
        self._go_home_and_return(wd, page)
        page.wait.until_visible('draft_wrap')

        page = mobile_list.AdListMobile(wd)
        page.go()
        page.wait.until_visible('draft_wrap')

        page = mobile.NewAdInsert(wd)
        page.go()
        page.wait.until_visible('draft_wrap')

        if logged:
            # Name and phone from the account
            data.update({
                'name': 'BLOCKET',
                'phone': '87654321',
            })
        else:
            # Comming back from draft clears the password
            data.update({
                'passwd': '',
                'passwd_ver': '',
            })

        self._assertAIForm(page, data)
        page.clean_draft.click()

        cleared = {
            'parent_category':  '',
            'subject':          '',
            'body':             '',
            'price':            '',
            'region':           '',
            'communes':         '',
            'name':             data['name'],
            'phone':            data['phone'],
            'phone_hidden':     data['phone_hidden'],
        }
        if not logged:
            cleared.update({
                'email': data['email'],
                'passwd':           '',
                'passwd_ver':       '',
            })

        self._assertAIForm(page, cleared)

    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_various_categories(self, wd):
        """ Check the draft information presense for all the adparams """
        mai = mobile.NewAdInsert(wd)
        mai.go()

        params = {
            'category': '2020',
            'type': 's',
            'brand': '25',
            'model': '2',
            'version': '4',
            'regdate': '2007',
            'gearbox': '1',
            'fuel': '1',
            'cartype': '1',
            'mileage': '13000'
        }

        mai.insert_ad(**params)
        mai.insert_ad(name = 'yolito') # This is writed to activate draft
        self._go_home_and_return(wd, mai)
        self._assertAIForm(mai, params)

        mai.insert_ad(category='2060', type='s', regdate='2013', cubiccms='10')
        mai.insert_ad(name = 'yolita') # This is writed to activate draft
        self._go_home_and_return(wd, mai)
        params2 = {
            'category': '2060',
            'type': 's',
            'regdate': '2013',
            'cubiccms': '10',
        }
        self._assertAIForm(mai, params2)
        self.assertEquals(mai.val('cubiccms'), '10')

        mai.insert_ad(category='7020', jc=['20'], contract_type=1, working_day=2)
        mai.insert_ad(name = 'yolitu') # This is writed to activate draft
        self._go_home_and_return(wd, mai)
        self.assertEquals(mai.val('jc'), ['20'])
        self.assertEquals(mai.val('contract_type'), '1')
        self.assertEquals(mai.val('working_day'), '2')

        mai.insert_ad(category='7060', sct=['3'])
        mai.insert_ad(name = 'yoliti') # This is writed to activate draft
        self._go_home_and_return(wd, mai)
        self.assertEquals(mai.val('sct'), ['3'])

        mai.insert_ad(category='4020', condition='1', gender='2')
        mai.insert_ad(name = 'yolite') # This is writed to activate draft
        self._go_home_and_return(wd, mai)
        self.assertEquals(mai.val('condition'), '1')
        self.assertEquals(mai.val('gender'), '2')

    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_images_saved(self, wd):
        """ Test if draft mobile save our images when we left the page """

        def get_image(mai, i):
            return mai.images[i].get_attribute('data-image')

        mai = mobile.NewAdInsert(wd)
        mai.go()

        self.assertVisible(mai, 'upload')
        mai.upload_resource_image('image_order/01.png')
        mai.upload_resource_image('image_order/02.png')
        mai.upload_resource_image('image_order/03.png')
        mai.wait.until_not_present('img_loader')
        self.assertEquals(len(mai.images), 3)

        first_image = get_image(mai, 0)
        last_image = get_image(mai, 2)

        self._go_home_and_return(wd, mai)
        self.assertEquals(len(mai.images), 3)

        mai.images[1].find_element_by_class_name('remove').click()
        self.assertEquals(len(mai.images), 2)
        self._go_home_and_return(wd, mai)

        self.assertEquals(len(mai.images), 2)
        self.assertEquals(first_image, get_image(mai, 0))
        self.assertEquals(last_image, get_image(mai, 1))

    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_add_images_missing_button(self, wd):
        """ Test that the clean draft button does show back the add more images button """
        page = mobile.NewAdInsert(wd)
        page.go()

        self.assertVisible(page, 'upload')
        self.assertNotVisible(page, 'clean_draft')

        page.upload_resource_image('image_order/01.png')
        page.upload_resource_image('image_order/01.png')
        page.upload_resource_image('image_order/02.png')
        page.upload_resource_image('image_order/02.png')
        page.upload_resource_image('image_order/03.png')
        page.upload_resource_image('image_order/03.png')

        self.assertNotVisible(page, 'upload')
        self.assertVisible(page, 'clean_draft')
        self.assertEquals(len(page.images), 6)

        # clean draft
        page.clean_draft.click()
        page.wait.until_visible('upload')

        self.assertEquals(len(page.images), 0)
        self.assertFalse(page.clean_draft.is_displayed())
        self.assertTrue(page.upload.is_displayed())


class DraftWithoutStorageMobile(yapo.SeleniumTest):
    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'draft', 'mobile')
    def test_draft_with_storage_disabled(self, wd):
        """ Test if the draft does not break the javascript code if the storage is disabled"""
        page = mobile.NewAdInsert(wd)
        page.go()
        wd.execute_script(
            'delete window["localStorage"];delete window["sessionStorage"];window.localStorage=null;window.sessionStorage=null;delete window["yapoDeviceStorage"]')
        self.assertNotVisible(page, 'clean_draft')
        page.insert_ad(category='1220', region='15', communes='295')
        self.assertNotVisible(page, 'clean_draft')
