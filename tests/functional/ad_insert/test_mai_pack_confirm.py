# -*- coding: latin-1 -*-
import yapo
import time
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages import mobile
from yapo.steps import insert_an_ad

class MAIAdInsertedSuccessfuly(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']

    data_2020 = {
        'region': '15',
        'communes': '331',
        'category': '2020',
        'subject': 'Auto Car',
        'body': 'yoLo',
        'price': '3500000',
        'phone': '99966666',
        'password': '123123123',
        'password_verification': '123123123',
        'brand': '31',
        'model': '44',
        'version': '1',
        'regdate': '2014',
        'gearbox': '1',
        'fuel': '1',
        'cartype': '1',
        'mileage': '15000',
        'type': 's',
        'plates': 'ABCD10',
    }

    data_2040 = {
        'subject': 'Auto Car',
        'region': '15',
        'communes': '331',
        'category': '2040',
        'body': 'yoLo',
        'price': '3500000',
        'regdate': '2014',
        'phone': '99966666',
        'password': '123123123',
        'password_verification': '123123123',
        'gearbox': '1',
        'fuel': '1',
        'mileage': '15000',
        'type': 's',
        'plates': 'ABCD10',
    }
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'mobile', 'account', 'packs', 'ucai')
    def test_mai_2020_pack_confirm_without_account(self, wd):
        """ Pack Mobile: Check AI category 2020 when user has not account"""
        self._pack_confirm_without_account(wd, self.data_2020)

    def _pack_confirm_without_account(self, wd, data):
        data.update(name='prosincuenta', email='prosincuenta@yapo.cl')
        self._insert_ad(wd, data)
        page = mobile.AdInsertedSuccessfuly(wd)
        self.assertTrue(page.pack_message_noaccount.is_displayed())

    @tags('ad_insert', 'mobile', 'account', 'packs', 'ucai')
    def test_mai_2020_pack_confirm_without_pack(self, wd):
        """ Pack Mobile: Check AI category 2020 when user no pack """
        self._pack_confirm_without_pack(wd, self.data_2020)

    def _pack_confirm_without_pack(self, wd, data):
        data.update(name='cuentaprosinpack', email='cuentaprosinpack@yapo.cl')
        self._insert_ad(wd, data)
        page = mobile.AdInsertedSuccessfuly(wd)
        self.assertTrue(page.pack_message_nopack.is_displayed())

    @tags('ad_insert', 'mobile', 'account', 'packs', 'ucai')
    def test_mai_2020_no_pack_confirm(self, wd):
        """ Pack Mobile: Check AI category 2020 when user is not pro """
        self._no_pack_confirm(wd, self.data_2020, 'emailnoexiste', 'emailnoexiste@yapo.cl')

    @tags('ad_insert', 'mobile', 'account', 'packs', 'ucai')
    def test_mai_2040_no_pack_confirm(self, wd):
        """ Pack Mobile: Check AI category 2040 when user is not pro """
        self._no_pack_confirm(wd, self.data_2040, 'emailno_existe', 'emailno_existe@yapo.cl')

    def _no_pack_confirm(self, wd, data, name, email):
        data.update(name=name, email=email)
        self._insert_ad(wd, data)
        page = mobile.AdInsertedSuccessfuly(wd)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_ok)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_nopack)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_noaccount)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_noquota)

    def _insert_ad(self, wd, data):
        insert_an_ad(wd, AdInsert=mobile.NewAdInsert, **data)
