# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
import yapo
import json
import os
import collections
from yapo import utils
from yapo.utils import Categories
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.mobile import NewAdInsert
from yapo.pages.mobile import Login


class MobileAdInsertByCategory(yapo.SeleniumTest):
    """ We are testing the ad insert by category """

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.forced_login.modal.newad.7020.disabled': '1',
        '*.*.forced_login.modal.editad.7020.disabled': '1',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    def mobile_ai_by_category_file(self, wd, file):
        """ Category by category, inserting an ad and expecting an OK message """

        test_data_file = os.path.join(yapo.conf.RESOURCES_DIR, file)
        data_file_content = open(test_data_file).read()
        self.data = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(data_file_content)

        disabled = {
            str(Categories.OFERTAS_DE_EMPLEO): 7,
            str(Categories.BUSCO_EMPLEO): 0,
        }

        mai = NewAdInsert(wd)
        login = Login(wd)
        for cat, cat_data in self.data.items():
            mai.go()
            mai.insert_ad(**cat_data)

            if cat_data['category'] in disabled:
                disabled_count = sum(1 for jc in mai.jcs if 'disabled' in jc.get_attribute('class'))
                print('disabled: {}, expected: {}'.format(disabled_count, disabled[cat_data['category']]))
                self.assertEqual(disabled_count, disabled[cat_data['category']])
            mai.submit()

            result = AdInsertResult(wd)
            self.assertTrue(result.was('success'))
            self.assertTrue(result.was('with_account_create'))
            login.logout()


    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_1220(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_1220.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_1240(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_1240.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_1260(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_1260.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2020(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2020.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2040(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2040.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2060(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2060.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2080(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2080.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2100(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2100.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_2120(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_2120.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_3080(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_3080.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_4020(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_4020.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_4040(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_4040.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_4060(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_4060.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_4080(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_4080.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_6020(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_6020.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_6060(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_6060.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_6080(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_6080.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_6120(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_6120.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_6140(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_6140.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_7020(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_7020.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_7040(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_7040.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_7060(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_7060.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_7080(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_7080.json')

    @tags('mobile', 'ad_insert', 'categories', 'ucai')
    def test_mai_cat_9020(self, wd):
        self.mobile_ai_by_category_file(wd, 'data_mai/cat_9020.json')

    @tags('mobile', 'ad_insert', 'categories', 'ad_params', 'ucai')
    def test_mandatory_fields(self, wd):
        """ Mandatory ad params per category """

        mai = mobile.NewAdInsert(wd)
        mai.go()

        fields = {'communes': 'Comuna *',
                  'rooms': 'Dormitorios *',
                  'brand': 'Marca *',
                  'model': 'Modelo *',
                  'version': 'Versi�n *',
                  'regdate': 'A�o *',
                  'gearbox': 'Transmisi�n *',
                  'fuel': 'Combustible *',
                  'cartype': 'Tipo de veh�culo *',
                  'mileage': 'Kil�metros *',
                  'cubiccms': 'Cilindrada *',
                  'condition': 'Nuevo/Usado *'}

        mandatory_per_category = [
            (Categories.VENDO, ['estate_type']),
            (Categories.ARRIENDO, ['estate_type']),
            (Categories.MOTOS, ['regdate', 'cubiccms', 'mileage']),
            (Categories.CAMIONES_Y_FURGONES, ['regdate', 'fuel', 'gearbox', 'mileage']),
            (Categories.MODA_VESTUARIO_Y_CALZADO, ['condition']),
            (Categories.VESTUARIO_FUTURA_MAMA_Y_NINOS, ['condition']),
            (Categories.OFERTAS_DE_EMPLEO, ['jc']),
            (Categories.BUSCO_EMPLEO, ['jc']),
            (Categories.SERVICIOS, ['sct']),
            (Categories.AUTOS_CAMIONETAS_Y_4X4, ['brand', 'model', 'version', 'regdate', 'gearbox', 'fuel', 'cartype', 'mileage'])
        ]

        for category, expected_errors in mandatory_per_category:
            mai.insert_ad(category=category, type='s')
            mai.submit()
            errors = list(map(lambda e: e.get_attribute('id').replace('-error', '') , mai.ad_param_errors))
            self.assertEquals(errors, expected_errors)
            import time; time.sleep(1)

    @tags('mobile', 'ad_insert', 'categories', 'fields', 'ad_params', 'ucai')
    def test_format_number_by_category(self, wd):
        """ Category by category validate numeric field format """

        mai = mobile.NewAdInsert(wd)
        mai.go()

        #Categories with number fields
        fields_per_category = {
            Categories.VENDO : ['size', 'garage_spaces', 'condominio'],
            Categories.ARRIENDO : ['size', 'garage_spaces', 'condominio'],
            Categories.ARRIENDO_TEMPORADA: ['garage_spaces'],
            Categories.AUTOS_CAMIONETAS_Y_4X4 : ['mileage'],
            Categories.MOTOS : ['mileage'],
            Categories.CAMIONES_Y_FURGONES : ['mileage'],
            Categories.CELULARES_TELEFONOS_Y_ACCESORIOS : ['internal_memory'],
        }

        category_w_estate_type = [ Categories.VENDO, Categories.ARRIENDO, Categories.ARRIENDO_TEMPORADA]

        for category, fields in fields_per_category.items():
            if category in category_w_estate_type:
                mai.insert_ad(category=category, estate_type='1')
            else:
                mai.insert_ad(category=category, type='s')
            for field_name in fields:
                field = getattr(mai, field_name)
                mai.wait.until_element_clickable(field_name)
                field.clear()
                # firefox allows the user to enter anything in number inputs
                # and we are not stopping that now, so we only check for input type number
                self.assertEquals('number', field.get_attribute('type'))

                # this code should work with all the browsers but firefox
                # I am not sure about lenghts, though
                # field.send_keys('1,2-3_4;5"6')
                # field_id = field.get_attribute('id')
                # mai.cat_info.click()
                # print('trying: {} - {}'.format(category, field_name))
                # expected_content = '123456'
                # if field_name in ['garage_spaces']:
                #     expected_content = '12345'
                # self.assertEquals(expected_content, field.get_attribute('value'))

    @tags('mobile', 'ad_insert', 'ucai')
    def test_category_fields_order(self, wd):
        """ Check the ad_param order by category """
        mai = mobile.NewAdInsert(wd)
        mai.go()

        ad_params_per_category = {
            Categories.VENDO : ['estate_type'],
            Categories.ARRIENDO : ['estate_type'],
            Categories.ARRIENDO_TEMPORADA : ['estate_type'],
            Categories.AUTOS_CAMIONETAS_Y_4X4 : ['brand', 'model', 'version', 'regdate', 'gearbox', 'fuel', 'cartype', 'plates', 'mileage'],
            Categories.MOTOS : ['regdate', 'cubiccms', 'mileage', 'plates'],
            Categories.CAMIONES_Y_FURGONES : ['regdate', 'fuel', 'gearbox', 'mileage', 'plates'],
            Categories.MODA_VESTUARIO_Y_CALZADO : ['condition', 'gender', 'clothing_size'],
            Categories.CALZADO : ['condition', 'footwear_gender', 'footwear_size', 'fwtype'],
            Categories.VESTUARIO_FUTURA_MAMA_Y_NINOS : ['condition'],
            Categories.BOLSOS_BISUTERIA_Y_ACCESORIOS : ['condition', 'gender'],
            Categories.SALUD_Y_BELLEZA : ['gender'],
            Categories.CELULARES_TELEFONOS_Y_ACCESORIOS : ['internal_memory'],
            Categories.OFERTAS_DE_EMPLEO : ['working_day', 'contract_type', 'jc'],
            Categories.BUSCO_EMPLEO : ['jc'],
            Categories.SERVICIOS : ['sct'],
            Categories.JUGUETES : ['condition'],
            Categories.COCHES_Y_OTROS_ARTICULOS : ['condition']
        }

        for category, expected_ad_params in ad_params_per_category.items():
            mai.insert_ad(category=category, type='s')
            ad_params = list(map(lambda e: e.get_attribute('id'), mai.ad_params))
            # Need to get rid of the first 3 (parent_category, category, type)
            self.assertEquals(ad_params[3:], expected_ad_params)
