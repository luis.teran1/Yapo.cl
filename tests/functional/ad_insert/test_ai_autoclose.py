# -*- coding: latin-1 -*-

from yapo.decorators import tags
from yapo.pages.mobile import NewAdEdit, NewAdInsert
from yapo.utils import Categories
from yapo import SeleniumTest
from yapo import utils
from yapo.steps import trans, accounts
import time
import copy


class MobileAdInsertAutoClose(SeleniumTest):

    AdInsert = NewAdInsert
    AdEdit = NewAdEdit
    ad_data = {
        'category': str(Categories.OFERTAS_DE_EMPLEO),
        'subject': 'Ofertas de empleo - yolo ad',
        'body': 'Ofertas de empleo - Yolo ad description',
        'region': '15',
        'communes': '296',
        'working_day': '2',
    }
    rest_ad_data = {
        'jc': ['16', '17'],
    }
    snaps=['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
    user_agent = utils.UserAgents.IOS

    @tags('ad_insert', 'autoclose')
    def test_unexpected_close_after_reload(self, wd):
        """Ad insert must be filled after reload without close unexpectedly"""
        page = self.AdInsert(wd)
        self._fill_form_and_reload(wd)
        self._fill_form(page, self.ad_data)

    @tags('ad_insert', 'autoclose')
    def test_fill_ad_data_after_reload(self, wd):
        """Ad insert should fill all fields ignoring the empty"""
        self._fill_form_and_reload(wd)

        self._check_ad_params(wd)

    @tags('ad_edit')
    def test_fill_ad_data_on_edition(self, wd):
        """Ad edit should fill all fields ignoring the empty"""
        self._fill_form_and_reload(wd, reload=False)
        page = self.AdInsert(wd)
        page.submit.click()
        trans.review_ad(
            email='prepaid5@blocket.se',
            subject=self.ad_data['subject']
        )
        utils.rebuild_index()
        page = self.AdEdit(wd)
        page.go('8000085')
        self._check_ad_params(wd)


    def _fill_form_and_reload(self, wd, reload=True):
        accounts.login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123', from_desktop=False)
        page = self.AdInsert(wd)
        page.go()

        self._fill_form(page, self.ad_data)
        self._fill_form(page, self.rest_ad_data, trigger='jc')

        if reload:
            page.go()


    def _fill_form(self, page, data, trigger=None):
        if trigger:
            data_copy = copy.deepcopy(data)
            page.hotizontal_select.fill_sidenav(data_copy, trigger=trigger)
        else:
            page.insert_ad(**data)

    def _check_ad_params(self, wd):
        page = self.AdInsert(wd)
        for field, value in self.ad_data.items():
            self.assertEqual(value, getattr(page, field).get_attribute('value'))

        self.assertEqual(page.contract_type.get_attribute('value'), '')
        self.assertEqual(page.job_category, ['16', '17'])

