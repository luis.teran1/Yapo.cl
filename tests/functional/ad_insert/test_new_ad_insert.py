# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.utils import Categories
from yapo.pages.mobile import NewAdInsert
from yapo.pages.ad_insert import AdInsertResult
from selenium.webdriver.common.keys import Keys
from collections import OrderedDict

class MobileNewAdInsert(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = yapo.utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'ad_insert', 'ucai')
    def test_validations(self, wd):
        """ Validation errors on empty MAI """

        expected = {
            'subject-error': 'Escribe un t�tulo',
            'body-error': 'Escribe una descripci�n',
            'parent_category-error': 'Selecciona una categor�a',
            'region-error': 'Selecciona una regi�n',
            'communes-error': 'Selecciona una comuna',
            'name-error': 'Escribe tu nombre',
            'email-error': 'Escribe tu e-mail',
            'phone-error': 'Escribe tu n�mero de tel�fono',
            'passwd-error': 'Debes introducir tu contrase�a',
            'passwd_ver-error': 'Debes introducir tu contrase�a',
        }

        mai = NewAdInsert(wd)
        mai.go()
        mai.submit()

        print('Every error found should be expected')
        for error in mai.errors:
            id = error.get_attribute('id')
            print('Validating error message for: {}'.format(id))
            self.assertEquals(expected.pop(id), error.text)

        print('Validating whether all the expected errors were found')
        self.assertEquals(len(expected), 0)

        fields = OrderedDict([
            ('subject', 'Some'),
            ('body', 'Ad'),
            ('category', 2020),
            ('name', 'Some'),
            ('email', 'ugly@email.ok'),
            ('passwd', '123123123'),
            ('passwd_ver', '123123123'),
            ('phone', '62184762'),
            ('region', 15),
            ('communes', 315),
        ])

        print('Filling every field')
        mai.insert_ad(**fields)

        self.assertEquals(len(mai.errors), 0)

    @tags('mobile', 'ad_insert', 'ucai')
    def test_validation_change(self, wd):
        """ Valitation errors that change depending on input """

        mai = NewAdInsert(wd)
        mai.go()
        mai.insert_ad(company_ad=1)
        mai.submit()

        # body
        self.assertEquals(mai.body_error.text, 'Escribe una descripci�n')
        mai.body.send_keys('w' + Keys.TAB)
        self.assertEquals(mai.body_error.text, 'Escribe por lo menos dos letras')

        # phone
        mai.phone.send_keys('12345' + Keys.TAB)
        self.assertEquals(mai.phone_error.text, 'N�mero de tel�fono muy corto')
        mai.phone.send_keys('8789012' + Keys.TAB)

        # rut
        self.assertEquals(mai.rut_error.text, 'Escribe tu RUT')
        mai.rut.send_keys('123123' + Keys.TAB)
        self.assertEquals(mai.rut_error.text, 'El RUT es incorrecto')
        mai.rut.send_keys('123' + Keys.TAB)
        self.assertTrue(mai.not_in_errors('rut'))
        mai.rut.send_keys('123' + Keys.TAB)
        self.assertEquals(mai.rut_error.text, 'El RUT es incorrecto')

        # passwd
        self.assertEquals(mai.passwd_error.text, 'Debes introducir tu contrase�a')
        for c in '1234567':
            mai.passwd.send_keys(c + Keys.TAB)
            self.assertEquals(mai.passwd_error.text, 'Tu contrase�a debe tener un m�nimo de 8 caracteres.')

        mai.passwd.send_keys('8' + Keys.TAB)
        self.assertTrue(mai.not_in_errors('passwd'))

        # passwd_ver
        self.assertEquals(mai.passwd_ver_error.text, 'Debes introducir tu contrase�a')
        mai.passwd_ver.send_keys('1234567' + Keys.TAB)
        self.assertEquals(mai.passwd_ver_error.text, 'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.')
        mai.passwd_ver.send_keys('8' + Keys.TAB)
        self.assertTrue(mai.not_in_errors('passwd_ver'))

    @tags('mobile', 'ad_insert', 'ucai')
    def test_success_page_subject(self, wd):
        """ Success page shows subject with only the first letter on uppercase """

        mai = NewAdInsert(wd)
        mai.go()

        mai.insert_ad(**{
            'region': '15',
            'communes': '315',
            'category': '5020',
            'subject': 'SUBJECT CON PURAS MAYUS',
            'body': 'yoLo',
            'name': 'yoLo',
            'email': 'yoLo@yolo.cl',
            'price': '3500000',
            'phone': '26437660',
            'password': '123123123',
            'password_verification': '123123123',
            'type': 's',
        })
        mai.submit()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('with_account_create'))
        self.assertTrue(result.was('with_upselling'))
        self.assertEquals(result.subject.text, 'Subject con puras mayus')

    @tags('mobile', 'ad_insert', 'ucai')
    def test_upload_images(self, wd):
        """ Upload an image after every ad_insert step """

        mai = NewAdInsert(wd)
        mai.go()

        mai.upload_resource_image('image_order/01.png')

        mai.insert_ad(**{
            'region': 15,
            'communes': 315,
        })

        mai.upload_resource_image('image_order/02.png')

        mai.insert_ad(**{
            'parent_category': '1000',
            'type': 's',
            'category': '1220',
            'estate_type': '2',
            'rooms': '2',
            'bathrooms': '1',
            'garage_spaces': '1',
        })

        mai.upload_resource_image('image_order/03.png')

        mai.insert_ad(**{
            'subject': 'Empanada',
            'body': 'De queso o de jamon',
            'price': '15000',
            'name': 'Yo Lo',
            'email': 'yo@lo.cl',
            'phone': '26437660',
            'passwd': '123123123',
            'passwd_ver': '123123123',
        })

        mai.upload_resource_image('image_order/01.png')
        mai.upload_resource_image('image_order/02.png')

        mai.upload_resource_image('image_order/03.png')

        mai.insert_ad(company_ad=1)

        mai.upload_resource_image('image_order/01.png')

        mai.insert_ad(company_ad=0)

        mai.upload_resource_image('image_order/02.png')

        self.assertEquals(len(mai.images), 8)

        mai.submit()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('with_account_create'))
        self.assertTrue(result.was('with_upselling'))

