# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert
from yapo.pages.ad_insert import AdInsert, AdInsertSuccess
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import yapo
from yapo.utils import Categories, UserAgents
from yapo.geography import Regions

class AdInsertImageMobileCompressed(yapo.SeleniumTest):

    user_agent = UserAgents.IOS
    AdInsert = NewAdInsert

    bconfs = {
        "*.*.mpu_settings.upload.1.newad.value": "compression:1",
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        "*.*.movetoapps_splash_screen.enabled": '0',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    @tags('ad_insert', 'images', 'upload', 'ucai')
    def test_upload_and_change_category(self, browser):
        """ Upload 20 images, change to a category with lower limit and change back """
        ad_insert = self._upload_images(browser, None, Categories.AUTOS_CAMIONETAS_Y_4X4, 20)
        self.assertNotVisible(ad_insert, 'upload')
        self.assertVisible(ad_insert, 'images_error')
        self.assertEquals(ad_insert.images_error.text, 'Ya has alcanzado el m�ximo de im�genes en esta categor�a')

        ad_insert.insert_ad(category=Categories.COMPUTADORES_Y_ACCESORIOS)
        self.assertEquals(len(ad_insert.images), 20)
        self.assertEquals(self._visible_images(ad_insert), 6)
        self.assertVisible(ad_insert, 'images_error')
        self.assertEquals(ad_insert.images_error.text, 'Ya has alcanzado el m�ximo de im�genes en esta categor�a')

        ad_insert.insert_ad(category=Categories.AUTOS_CAMIONETAS_Y_4X4)
        self.assertEquals(len(ad_insert.images), 20)
        self.assertEquals(self._visible_images(ad_insert), 20)

        ad_insert.insert_ad(category=Categories.COMPUTADORES_Y_ACCESORIOS)
        self.assertEquals(len(ad_insert.images), 20)
        self.assertEquals(self._visible_images(ad_insert), 6)

        self._delete_images(browser, ad_insert, 2)
        self.assertNotVisible(ad_insert, 'images_error')

    @tags('ad_insert', 'images', 'upload', 'ucai')
    def test_upload_20_images(self, browser):
        """ Upload 20 images """
        ad_insert = self._upload_images(browser, None, Categories.AUTOS_CAMIONETAS_Y_4X4, 20)
        self.assertNotVisible(ad_insert, 'upload')

    @tags('ad_insert', 'images', 'upload', 'ucai')
    def test_upload_and_delete(self, browser):
        """ Upload an image and delete it """
        ad_insert = self._upload_images(browser, None, Categories.COMPUTADORES_Y_ACCESORIOS, 1)
        self.assertVisible(ad_insert, 'upload')
        ad_insert = self._delete_images(browser, ad_insert, 1)

    def _upload_images(self, browser, ad_insert, category, num_images):
        if not ad_insert:
            ad_insert = self.AdInsert(browser)
            ad_insert.go()

        ad_insert.insert_ad(category=category)
        for _ in range(num_images):
            self.assertVisible(ad_insert, 'upload')
            ad_insert.insert_ad(images=['image_order/01.png'])

        self.assertEquals(len(ad_insert.images), num_images)
        self.assertEquals(self._visible_images(ad_insert), num_images)
        return ad_insert

    def _delete_images(self, browser, ad_insert, num_images):
        if not ad_insert:
            ad_insert = self.AdInsert(browser)
            ad_insert.go()

        start_images = self._visible_images(ad_insert)
        for img in ad_insert.images[:num_images]:
            img.find_element_by_class_name('remove').click()

        self.assertEquals(len(ad_insert.images), start_images - num_images)

    def _visible_images(self, ad_insert):
        return sum(1 for img in ad_insert.images if img.is_displayed())


class AdInsertImageMobileUncompressed(AdInsertImageMobileCompressed):

    bconfs = {
        "*.*.mpu_settings.upload.1.newad.value": "compression:0",
        "*.*.movetoapps_splash_screen.enabled": '0'
    }


class AdInsertImageTest(yapo.SeleniumTest):

    bconfs = {
        "*.*.mpu_settings.upload.1.newad.value": "compression:0",
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    @tags('ad_insert', 'images', 'upload')
    def test_educational_messages_to_use_real_images_is_shown(self, browser):
        """ AI: Checks the correct display of the educational messages to upload only real images """

        page = AdInsert(browser)
        page.go()
        cats_with_message = [Categories.COMPUTADORES_Y_ACCESORIOS, Categories.CELULARES_TELEFONOS_Y_ACCESORIOS,
                             Categories.CONSOLAS_VIDEOJUEGOS_Y_ACCESORIOS, Categories.AUDIO_TV_VIDEO_Y_FOTOGRAFIA]

        for category_name, category_code in Categories.get_subcategories():
            page.insert_ad(category = category_code)
            page.subject.click() # used only to trigger the event
            element_id = 'image_upload_tip_{}'.format(category_code)
            if category_code not in cats_with_message:
                page.wait.until(EC.invisibility_of_element_located((By.ID, element_id)))
            else:
                page.wait.until(EC.visibility_of_element_located((By.ID, element_id)))

    @tags('ad_insert', 'images', 'upload')
    def test_educational_messages_to_use_real_images_is_hidden(self, browser):
        """ AI: Checking if the tip is not visible when an image is uploaded """

        page = AdInsert(browser)
        page.go()
        category_code = Categories.COMPUTADORES_Y_ACCESORIOS
        page.insert_ad(category = category_code)
        element_id = 'image_upload_tip_{}'.format(category_code)
        page.wait.until(EC.visibility_of_element_located((By.ID, element_id)))
        page.upload_resource_image('autorotation/img_270.jpg')
        page.wait.until(EC.invisibility_of_element_located((By.ID, element_id)))
        img_div = page.get_image_div_by_index(1)
        img_div.find_element_by_class_name('bt_delete').click()
        page.wait.until(EC.visibility_of_element_located((By.ID, element_id)))

    @tags('ad_insert', 'images', 'upload')
    def test_duplicated_images(self, browser):
        """ AI: Multiple Image Upload with Duplicated images """

        page = AdInsert(browser)
        page.go()
        page.insert_ad(subject='Multiple Image Upload with Duplicated images',
                       body = """This test validates that Ad Insertion works well even if there are duplicated images.
                                To do so, it changes the values of the form to simulate that two images are identical.""",
                       region=Regions.REGION_METROPOLITANA,
                       communes=331,
                       category=Categories.OTROS_PRODUCTOS,
                       price=123456,
                       private_ad=True,
                       name='Jose Miguel',
                       email='josemiguel@schibsted.cl',
                       email_verification='josemiguel@schibsted.cl',
                       phone="82880650",
                       password="11111",
                       password_verification="11111",
                       create_account=False,
                       accept_conditions=True)
        page.upload_resource_image('autorotation/img_270.jpg')
        page.upload_resource_image('autorotation/img_180.jpg')
        page.wait.until_not_present("img_loader")
        page.wait.until_elements_equals(browser, 'img[alt="image"]', 2)
        browser.execute_script("""var a=document.getElementsByName('image[]');a[0].value='1234567890.jpg';a[1].value='1234567890.jpg';""")
        page.submit.click()
        page = AdInsertSuccess(browser)
        with self.assertRaises(NoSuchElementException):
            # this is the shit that appear when a "Problemas t�cnicos" is raised
            browser.find_element_by_css_selector('.maintext h2')

    @tags('ad_insert', 'images', 'upload')
    def test_max_image(self, browser):
        """ AI: Checking the max images limitations per category """

        page = AdInsert(browser)
        page.go()
        page.insert_ad(region=Regions.II_ANTOFAGASTA,
                       category=Categories.VENDO,
                       estate_type='2',
                       subject='Testing image insertion with Ajax',
                       body='Body of the ajax image insertion test',
                       price=500,
                       private_ad=True,
                       communes = '12',
                       rooms = '1',
                       garage_spaces = '2',
                       condominio = '10',
                       name='Selenium Tester',
                       email='selenium_tester@schibstediberica.es',
                       email_verification='selenium_tester@schibstediberica.es',
                       phone='9876543210',
                       password='123123123',
                       password_verification='123123123',
                       create_account=False,
                       accept_conditions=True)

        for i in range(20):
            page.upload_resource_image('autorotation/img_270.jpg')
        page.wait.until_not_present("img_loader")
        page.wait.until_elements_equals(browser, 'img[alt="image"]', 20)
        self.assertIn('Has llegado al m�ximo de im�genes', page.image_upload_status.text)

        # now we change to a category with less images allowed
        page.insert_ad(category = Categories.JARDIN_Y_HERRAMIENTAS)
        page.wait.until_text_present('Ya has alcanzado el m�ximo de im�genes en esta categor�a', page.image_upload_status)
        self.assertIn('Ya has alcanzado el m�ximo de im�genes en esta categor�a', page.image_upload_status.text)
        page.submit.click()
        self.assertIn('Has alcanzado el m�ximo de im�genes, favor borra algunas', page.err_extra_image.text)
        page.password.send_keys('123123123')
        page.password_verification.send_keys('123123123')

        # delete images until max is reached
        for i in range(14):
            img_div = page.get_image_div_by_index(1)
            img_div.find_element_by_class_name('bt_delete').click()
        # submit and expect and ok
        page.submit.click()
        page = AdInsertSuccess(browser)
        self.assertEquals(page.title.text, page.SUCCESS_MESSAGE)
        self.assertIn('Gracias', page.title.text)


class AdInsertImageLimit(yapo.SeleniumTest):

    bconfs = {
        '*.*.category_settings.extra_images.1.2020.value': 'max:1,price:0',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    def test_can_set_image_limit_by_bconf(self, browser):
        """ Ensure we can modify image limit per category via bconf """
        page = AdInsert(browser)
        page.go()

        page.insert_ad(category=2020)
        page.upload_resource_image('image_order/01.png')
        page.upload_resource_image('image_order/02.png')

        page.wait.until(lambda wd: len(page.images) == 2)
        self.assertEquals(page.image_upload_status.text, 'Has llegado al m�ximo de im�genes')
