# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.utils import Categories
from yapo.pages.desktop import Login as LoginDesktop
from yapo.pages.mobile import Login as LoginMobile
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop_list import AdListDesktop
from selenium.webdriver.common.keys import Keys
from yapo.pages.newai import DesktopAI as AdInsertDesktopNew
from yapo.pages.maps import MapPreview, MapView
from yapo import utils
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.steps import insert_an_ad, insert_and_publish_ad
import yapo
import time
import random


class AdInsertTest(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    autocomplete = 5

    @tags('ad_insert', 'desktop', 'maps', 'ad_view', 'map_autocomplete')
    def test_streets_autocomplete(self, browser):
        ''' Tests the streets autocomplete adding letters and waiting suggestions'''
        page = self.AdInsert(browser)
        result= insert_an_ad(browser, AdInsert=self.AdInsert, category=3040, region=15, communes=331, add_location=True, Submit=False)
        page.map_form.address.clear()
        page.map_form.address.send_keys('San')
        streets = ['Avenida Fundo San Pedro',
                   'Avenida Papa San Lino',
                   'Avenida Papa San Pedro',
                   'Avenida San Francisco',
                   'Avenida San Pablo'][:self.autocomplete]
        self.assertEqual(page.map_form.get_suggestions(), streets)
        page.map_form.address.send_keys(' p')
        streets = ['Avenida Fundo San Pedro',
                   'Avenida Papa San Pedro',
                   'Avenida San Pablo',
                   'Calle Papa San Pio',
                   'Calle San Pablo'][:self.autocomplete]
        page.map_form.wait.until(lambda b: page.map_form.get_suggestions() == streets)
        page.map_form.address.clear()
        page.map_form.address.send_keys('calle'*11)
        self.assertEqual(page.map_form.address.get_attribute('value'), 'calle'*10)

    @tags('ad_insert', 'desktop', 'maps', 'ad_view', 'map_autocomplete')
    def test_streets_autocomplete_largests_street(self, browser):
        ''' Tests the largest street suggested by autocomplete '''
        page = self.AdInsert(browser)
        result= insert_an_ad(browser, AdInsert=self.AdInsert, category=3040, region=15, communes=323, add_location=True, Submit=False)
        page.map_form.address.clear()
        page.map_form.address.send_keys('dire')
        streets = ['Calle General Director Jos� Bernales']
        self.assertEqual(page.map_form.get_suggestions(), streets)
        page.map_form.address.send_keys(Keys.DOWN)
        self.assertEqual(page.map_form.address.get_attribute('value'), 'Calle General Director Jos� Bernales')

    @tags('ad_insert', 'desktop', 'maps', 'ad_view', 'map_autocomplete')
    def test_streets_autocomplete_priority(self, browser):
        ''' Tests the autocomplete priority system by streets hierarchy, then by ads number '''
        yapo.utils.rebuild_msearch()
        page = self.AdInsert(browser)
        result = insert_an_ad(browser, AdInsert=self.AdInsert,
                       subject='Test map',
                       body='yoLo map',
                       category=3040,
                       region=15,
                       communes=323,
                       add_location=True,
                       price=999666,
                       private_ad=True,
                       name='yolo',
                       email='yolo@cabral.es',
                       email_verification='yolo@cabral.es',
                       phone='999966666',
                       password='999966666',
                       password_verification='999966666',
                       create_account=True,
                       accept_conditions=True,
                       Submit=False)
        page.map_form.address.clear()
        page.map_form.address.send_keys('san')
        streets = ['Avenida Jos� Pedro Alessandri',
                   'Calle Cruchaga Santa Mar�a',
                   'Calle San Andr�s',
                   'Calle San Eugenio',
                   'Calle San Felipe'][:self.autocomplete]
        self.assertEqual(page.map_form.get_suggestions(), streets)
        page.map_form.address.send_keys(Keys.DOWN * 2)
        page.map_form.address.send_keys(Keys.TAB)
        page.submit.click()
        yapo.utils.rebuild_msearch()
        result= insert_an_ad(browser, AdInsert=self.AdInsert, category=3040, region=15, communes=323, add_location=True, Submit=False)
        page.map_form.address.clear()
        page.map_form.address.send_keys('san')
        streets = ['Calle Cruchaga Santa Mar�a',
                   'Avenida Jos� Pedro Alessandri',
                   'Calle San Andr�s',
                   'Calle San Eugenio',
                   'Calle San Felipe'][:self.autocomplete]
        self.assertEqual(page.map_form.get_suggestions(), streets)

    @tags('ad_insert', 'desktop', 'maps')
    def test_more_than_5_requests(self, browser):
        """On the 6th geolocalization request, the 'sad face' should be displayed"""
        page = self.AdInsert(browser)
        result=insert_an_ad(browser, AdInsert=self.AdInsert, category=5020, region=8, communes=123, add_location=True, Submit=False)
        for i in range(6):
            page.search_location(address='Avenida Arturo Prat', address_number=str(10+i), show_location=True)
            time.sleep(2)

        page.map_form.wait.until_visible('add_location_disabled')
        self.assertTrue(bool(page.map_form.add_location.get_attribute('disabled')))
        self.assertNotVisible(page.map_form.map, 'show_location')
        page.map_form.wait.until_visible('map_error_requests_exceeded')
        self.assertVisible(page.map_form, 'map_error_requests_exceeded')


class AdInsertBrokenURL(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {'*.*.maps.geocoder_url': 'https://wtf/broken'}
    AdInsert = AdInsertDesktop

    @tags('ad_insert', 'desktop', 'maps')
    def test_broken_nokia_geocoder(self, browser):
        """geocoder request failure should set the 'sad face'"""
        page = self.AdInsert(browser)
        result=insert_an_ad(browser, AdInsert=self.AdInsert, category=5020, region=8, communes=123, add_location=True,
                       address='Avenida Arturo Prat', address_number=str(430), show_location=True, Submit=False)

        self.assertTrue(bool(page.map_form.add_location.get_attribute('disabled')))
        self.assertNotVisible(page.map_form.map, 'show_location')
        self.assertVisible(page.map_form, 'map_error_provider')


class AdInsertWithMapPerCategory(yapo.SeleniumTest):

    """ Ad is published successfully and the map is displayed has it was inserted """
    snaps = ['accounts']
    AdInsert = AdInsertDesktopNew
    MapView = MapPreview
    Login = LoginDesktop

    def _test_ad_insert_map(self, browser, categories, logout=False, email=None):
        for cat in categories:
            subject = 'My ad in cat {0}'.format(cat)
            data = {
                'category': cat,
                'subject': subject,
                'add_location': True,
                'address': 'Providencia',
                'show_location': True,
                'price': 11000
            }

            if email:
                data.update({
                    'email': email,
                    'email_verification': email,
                })
                if not logout:
                    data['logged'] = True

            insert_and_publish_ad(browser, AdInsert=self.AdInsert, **data)

            ad_list = AdListDesktop(browser)
            ad_list.go()
            ad_view = ad_list.go_to_ad(subject=subject)
            # Ad is published successfully and the map is displayed has it was inserted
            map_view = self.MapView(browser)
            self.assertTrue(map_view.is_present())
            # TODO: for some unknown reason at this point map_view.region_commune.text returns an empty string, so I need to use 'innerHTML'
            self.assertEqual(map_view.region_commune.get_attribute('innerHTML'), 'Regi�n Metropolitana, Pudahuel')
            if logout:
                self.Login(browser).logout()

    def _category_sample(self, parent_categories):
        categories = []
        for parent in parent_categories:
            for name, cat in Categories.get_subcategories(parent):
                categories.append(cat)
        return random.sample(categories, 3)

    @tags('ad_insert', 'desktop', 'maps')
    def test_ad_insert_map_wo_account(self, browser):
        """ Insert without account """

        parent_categories = [Categories.INMUEBLES, Categories.VEHICULOS, Categories.HOGAR]
        categories = self._category_sample(parent_categories)
        print('Gonna test on {}'.format(categories))

        self._test_ad_insert_map(browser, categories)

    @tags('ad_insert', 'desktop', 'maps')
    def test_ad_insert_map_logged_user(self, browser):
        """ Insert with account, logged """
        email = 'prepaid5@blocket.se'
        self.Login(browser).login(email, '123123123')

        parent_categories = [Categories.MODA_BELLEZA_SALUD, Categories.FUTURA_MAMA_BEBES_Y_NINOS]
        categories = self._category_sample(parent_categories)
        print('Gonna test on {}'.format(categories))

        self._test_ad_insert_map(browser, categories, email=email)

    @tags('ad_insert', 'desktop', 'maps')
    def test_ad_insert_map_unlogged_user(self, browser):
        """ Insert with account, unlogged """

        email = 'prepaid5@blocket.se'
        parent_categories = [Categories.TIEMPO_LIBRE, Categories.COMPUTADORES_Y_ELECTRONICA,
                             Categories.OTROS, Categories.SERVICIOS_NEGOCIOS_Y_EMPLEO]
        categories = self._category_sample(parent_categories)
        print('Gonna test on {}'.format(categories))

        # We need to logout after every insertion, because AI logs the account in
        self._test_ad_insert_map(browser, categories, logout=True, email=email)
