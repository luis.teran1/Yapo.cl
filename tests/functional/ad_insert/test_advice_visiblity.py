# -*- coding: latin-1 -*-

from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdEdit, NewAdInsert
from yapo.pages.desktop import AdEdit as AdEditDesktop
from yapo.utils import Categories
from yapo import utils
from yapo.steps import trans, insert_an_ad, accounts
from yapo import SeleniumTest
from selenium.common.exceptions import NoSuchElementException


class DesktopEditionAdvice(SeleniumTest):
    bconfs={
        '*.*.category_settings.ai_alert.1.{}.value'.format(Categories.OFERTAS_DE_EMPLEO): 'edit_allowed_days:1',
        '*.*.category_settings.ai_alert.1.{}.value'.format(Categories.BUSCO_EMPLEO): 'edit_allowed_days:1',
        '*.*.edit_allowed_days.cat.{}'.format(Categories.OFERTAS_DE_EMPLEO): 3,
        '*.*.edit_allowed_days.cat.{}'.format(Categories.BUSCO_EMPLEO): 2,
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    snaps=['accounts']
    categories = {
        str(Categories.OFERTAS_DE_EMPLEO): {
            'is_visible': True,
            'name': Categories.get_name(Categories.OFERTAS_DE_EMPLEO),
            'expected': 3,
            'list_id': '8000085'
        },
        str(Categories.BUSCO_EMPLEO): {
            'is_visible': True,
            'name': Categories.get_name(Categories.BUSCO_EMPLEO),
            'expected': 2,
            'list_id': '8000086'
        },
        str(Categories.ARRIENDO_TEMPORADA): {
            'is_visible': False,
        }
    }

    AdInsert = AdInsertDesktop
    AdEdit = AdEditDesktop

    @tags('ad_insert', 'advice')
    def test_advice_visibility(self, wd):
        """check insertion advice visibility for some categories"""
        page = self.AdInsert(wd)
        expected_text = 'Los avisos de "{}" s�lo podr�n ser editados durante los {} primeros'

        page.go()

        for cat, category in self.categories.items():
            page.insert_ad(category=cat)

            if category.get('is_visible'):
                page.wait.until(lambda wd: page.advice.text != '')
                self.assertIn(
                    expected_text.format(category.get('name'), category.get('expected')),
                    page.advice.text
                )
            else:
                self.assertNotVisible(page, 'advice')

    @tags('ad_insert', 'advice')
    def test_advice_hidden(self, wd):
        """The advice must be hidden on ad edit"""

        cat = str(Categories.OFERTAS_DE_EMPLEO)
        category = self.categories.get(cat)
        cat_name = category.get('name')

        ad_data = {
            'category': str(cat),
            'jc': ['16', '17'],
            'subject': '{} - yolo ad'.format(cat_name),
            'body': '{} - Yolo ad description'.format(cat_name),
            'region': '15',
            'name': 'Osto name',
            'email': 'prepaid5@blocket.se',
            'passwd': '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'
        }

        insert_an_ad(wd, AdInsert=None, submit=True, **ad_data)
        trans.review_ad(
            email=ad_data.get('email'),
            subject=ad_data.get('subject')
        )
        utils.rebuild_index()

        page = self.AdEdit(wd)
        page.edit_an_ad(category.get('list_id'), '123123123', submit=False)
        self.assertRaises(NoSuchElementException, lambda: page.advice)
        page.submit.click()


class MobileEditionAdvice(DesktopEditionAdvice):
    AdInsert = NewAdInsert
    AdEdit = NewAdEdit


class DesktopInsertionAdvice(SeleniumTest):

    AdInsert = AdInsertDesktop
    AdEdit = AdEditDesktop

    snaps=[
        'accounts',
        'diff_pro_account',
        'diff_jobs_ads',
    ]

    def test_advice_hidden_user_logged_pro_ad_edit(self, wd):
        """Insertion advice must be hidden when user is editing an ad"""
        accounts.login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123')
        page = self.AdEdit(wd)
        page.go('8000189')
        page = self.AdInsert(wd)
        self.assertNotVisible(page, 'insertion_advice')

    def test_advice_hidden_user_unlogged(self, wd):
        """Insertion advice must be hidden when user is not logged"""
        self._advice_visibility_scenario(wd, is_visible=False)

    def test_advice_hidden_user_logged_nopro(self, wd):
        """Insertion advice must be hidden when user is logged, is a normal user"""
        self._advice_visibility_scenario(wd, email='many@ads.cl', is_visible=False)

    def test_advice_hidden_user_pro_bconf_on_other_category(self, wd):
        """
            Insertion advice must be hidden when user is logged, is a pro user
            and the current category is not `OFERTAS DE EMPLEO`
        """
        self._advice_visibility_scenario(wd, email='prepaid5@blocket.se', category=Categories.ARRIENDO_TEMPORADA, is_visible=False)

    def test_advice_visible_expected_category(self, wd):
        """Insertion advice must be visible when user is logged, and current category is `OFERTAS DE EMPLEO`"""
        self._advice_visibility_scenario(wd, email='prepaid5@blocket.se')

    def test_advice_visible_multiple_categories(self, wd):
        """Insertion advice must be toggled when the logged user change the current category"""
        accounts.login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123')
        page = self.AdInsert(wd)
        page.go()

        page.insert_ad(category=Categories.OFERTAS_DE_EMPLEO)
        self.assertVisible(page, 'insertion_advice')
        page.insert_ad(category=Categories.ARRIENDO_TEMPORADA)
        self.assertNotVisible(page, 'insertion_advice')

    def _advice_visibility_scenario(self, wd, email=None, category=Categories.OFERTAS_DE_EMPLEO, is_visible=True):
        if email:
            accounts.login_and_go_to_dashboard(wd, email, '123123123')
        self._check_visibility(wd, category, is_visible)

    def _check_visibility(self, wd, category=Categories.OFERTAS_DE_EMPLEO, is_visible=True):
        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(category=category)

        if is_visible:
            self.assertIn('el valor por publicar este aviso es de $5.990', page.insertion_advice.text)
            self.assertVisible(page, 'insertion_advice')
        else:
            self.assertNotVisible(page, 'insertion_advice')


class MobileInsertionAdvice(DesktopInsertionAdvice):
    AdInsert = NewAdInsert
    AdEdit = NewAdEdit

