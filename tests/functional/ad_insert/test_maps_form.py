# -*- coding: latin-1 -*-
import yapo
from yapo import conf
from yapo.pages.ad_insert import AdInsert
from yapo.pages.generic import AccountBar
from yapo.pages.desktop import HeaderElements
from yapo.decorators import tags
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By

class CommuneAndRegionCheckInAdInsert(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_accounts_nocommune']

    @tags('desktop', 'maps')
    def test_region_and_commune_with_maps(self, wd):
        """Maps should keep the commune and region info after insert an ad with maps"""
        page = AdInsert(wd)
        page.go()
        # check default value
        self.assertEquals('0', page.map_form.region.get_attribute('value'))
        self.assertEquals('', page.map_form.communes.get_attribute('value'))

        # login
        accounts = AccountBar(wd)
        accounts.login('user.01@schibsted.cl', '123123123')
        self.assertEquals('15', page.map_form.region.get_attribute('value'))
        self.assertEquals('', page.map_form.communes.get_attribute('value'))

        # insert new ad with commune value in a different region than the
        # account values, so it should not update the communes field in account
        params = {
            'subject': 'test con region y comuna',
            'body': 'asd awer was da sdxas sd',
            'price': '1234',
            'accept_conditions': 'True',
            'communes': '208'
        }
        page.insert_ad(region="10", category="8020", **params)
        page.submit.click()
        page.go()
        self.assertEquals('15', page.map_form.region.get_attribute('value'))
        self.assertEquals('', page.map_form.communes.get_attribute('value'))

        # Save a new ad with the same region that the account
        # here we should save the commune value
        params = {
            'subject': 'test con region y comuna',
            'body': 'asd awer was da sdxas sd',
            'price': '1234',
            'accept_conditions': 'True',
            'communes': '310'
        }
        page.insert_ad(region="15", category="8020", **params)
        page.submit.click()
        page.go()
        self.assertEquals('15', page.map_form.region.get_attribute('value'))
        self.assertEquals('310', page.map_form.communes.get_attribute('value'))

        # login with different user with have set the commune value in the profile
        accounts.logout.click()
        accounts.login('prepaid3@blocket.se', '123123123')
        page.go()
        self.assertEquals('10', page.map_form.region.get_attribute('value'))
        self.assertEquals('225', page.map_form.communes.get_attribute('value'))



class AdInsertFormWithMapsCheck(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_maps']

    @tags('desktop', 'maps', 'ad_insert')
    def test_check_form_persistence(self, wd):
        """check if the form inputs get clean when you insert some data, then press back button, the forward"""
        wd.get(conf.DESKTOP_URL)
        page = AdInsert(wd)
        form = MapFormElementsStates(self, page)
        page.go()
        # check the initial status
        form.assert_form('check the default state of the form')

        # set values in the form
        Select(page.map_form.region).select_by_value('15')
        Select(page.map_form.communes).select_by_value('310')
        page.wait.until(lambda x: page.map_form.add_location.is_enabled())
        page.map_form.add_location.click()
        page.wait.until(lambda x: page.map_form.map.show_location.is_displayed())
        page.map_form.address.send_keys('qwerty')
        page.map_form.address_number.send_keys('123' + Keys.TAB)
        wd.back()
        page.wait.until(lambda b: self.is_element_present(wd, By.CSS_SELECTOR, '.btn-da-insert'))
        wd.forward()
        # check that all the inputs are empty
        form.assert_form('check the default state of the form after back/forward')
        Select(page.map_form.region).select_by_value('15')
        Select(page.map_form.communes).select_by_value('310')
        page.map_form.add_location.click()
        form.state_active()
        form.assert_form('check the default state of the form after back/forward')


    @tags('desktop', 'maps', 'ad_insert')
    def test_check_validations_in_map_form(self, wd):
        """Check the validations in the maps form"""
        # payload
        page = AdInsert(wd)
        page.go()
        form = MapFormElementsStates(self, page)
        form.assert_form('check the default state of the form')

        # Select region and commune (Region Metropolitana/La Florida)
        Select(page.map_form.region).select_by_value('15')
        Select(page.map_form.communes).select_by_value('310')
        form.state_enabled()
        form.wait_for = lambda x: page.map_form.add_location.is_enabled()
        form.assert_form('check how the form looks after select region and commune')

        # check "Add Location"
        page.map_form.add_location.click()
        form.state_active()
        form.wait_for = lambda x: page.map_form.map.show_location.is_displayed()
        form.assert_form('check the form after click the add_location check')

        # write an address, less than 2 letters
        page.map_form.address.send_keys('qw')
        form.address_value = 'qw'
        form.assert_form('if you wrote less than 2 letter in the address input the form should not change')

        # write an address, now more than 2 letters
        page.map_form.address.send_keys('erty')
        form.state_active_with_address('qwerty')
        form.wait_for = lambda x: page.map_form.map.show_location.is_enabled()
        form.assert_form('if you wrote more than 2 letters in the address input the form map should be active')

        # click on show_location
        page.map_form.map.show_location.click()
        form.state_active_with_address_map()
        form.condition_error_address_not_found()
        form.wait_for = lambda x: page.map_form.map.marker.is_displayed()
        form.assert_form('check the form after clicking the show_location button, with a non valid address and without number')

        # write a number
        page.map_form.address_number.send_keys('123' + Keys.TAB)
        form.state_active_with_number(number='123')
        form.wait_for = lambda x: page.map_form.map.show_location.is_enabled()
        form.assert_form('check the form with the map active, and writing a new number in the address_number input, the map should be inactive')

        # write some chars
        page.map_form.address_number.send_keys('abc' + Keys.TAB)
        form.assert_form('check that no letters can be wrote in the address_number input')
        page.map_form.address_number.send_keys('- c' + Keys.TAB)
        form.assert_form('check that no chars can be wrote in the address number input')

        # write a larger number
        page.map_form.address_number.send_keys('456' + Keys.TAB)
        form.state_active_with_number(number='12345')
        form.assert_form('check that address_number input only accept 5 chars')

        # click on show_location
        page.map_form.map.show_location.click()
        form.state_active_with_number_map()
        form.condition_error_address_not_found()
        form.wait_for = lambda x: page.map_form.map.marker.is_displayed() and page.map_form.address_nok.is_displayed()
        form.assert_form('check that the map can be opened after make changes in the address_number input')

        # click on show_location, after write more numbers and change to approx area
        page.map_form.address_number.send_keys('456' + Keys.TAB)
        page.map_form.approx_area.click()
        page.map_form.map.show_location.click()
        form.state_active_with_number_map()
        form.condition_approx_area_checked()
        form.condition_error_address_not_found()
        # here I should do a pretty dirty thing, when you do change the marker in the map from exact shape to approx
        # in the map container appears some divs, copies from the marker object, but non visible, most of the time
        # we are able to delete it, using the cleanMarkers method inside the map class, but in cases like this one,
        # this method is not able to clean all the divs, since there are created later, and this garbage does not allow
        # the page object to reach the right element at this point so the test fail, so I'll do is a manual cleaning
        # of this elements in the page.
        page.wait.until(lambda x: not page.map_form.map.show_location.is_displayed())
        wd.execute_script("$('.leaflet-marker-icon').each(function(){if (! $(this).is(':visible')) $(this).remove();})")
        form.wait_for = lambda x: page.map_form.map.marker.is_displayed()
        form.assert_form('click on show_location, after write more numbers and change to approx area')

        # clear the address field
        page.map_form.address.clear()
        page.map_form.address.send_keys(Keys.TAB)
        form.state_active('', '')
        form.wait_for = lambda x: not page.map_form.address_number.is_enabled()
        form.assert_form('check that the map closes after clear the address input text')

        # write "Calle 13" as address/street and send the Return key
        page.map_form.address.send_keys('Calle 13')
        form.state_active_with_address('Calle 13')
        form.wait_for = lambda x: page.map_form.map.show_location.is_enabled()
        form.assert_form('Check the form after write a valid address')

        # write a number
        page.map_form.address_number.send_keys('234' + Keys.TAB)
        form.state_active_with_number(number='234')
        form.wait_for = lambda x: page.map_form.exact_area.is_enabled()
        form.assert_form('Check the form after insert a number in the address_number field')

        # click in approx_area
        page.map_form.approx_area.click()
        form.condition_approx_area_checked()
        form.assert_form('Check the form after click in the approx_area object with the address_number input with a valid number')

        # click in exact_area
        page.map_form.exact_area.click()
        form.condition_exact_area_checked()
        form.assert_form('Check the form after click in the exact_area radio with a valid address number')

        # click on show_location
        page.map_form.map.show_location.click()
        form.state_active_with_number_map()
        form.condition_info_address_found()
        page.wait.until(lambda x: not page.map_form.map.show_location.is_displayed())
        wd.execute_script("$('.leaflet-marker-icon').each(function(){if (! $(this).is(':visible')) $(this).remove();})")
        form.wait_for = lambda x: page.map_form.address_ok.is_displayed()
        form.assert_form('Check that the map open after make changes on the address and address_number inputs')

        # clear the number field
        page.map_form.address_number.clear()
        form.state_active_with_address(number='')
        form.wait_for = lambda x: not page.map_form.exact_area.is_enabled()
        form.assert_form('Check the form after delete de address_number field')



class MapFormElementsStates:

    def __init__(self, test, page):
        self.address_value = ''
        self.address_number_value = ''
        self.map_marker_is_loaded = False
        self.wait_for = None
        self.page = page
        self.test = test
        self.state_default()

    def state_default(self):
        # Set the form default values
        self.preview_active_is_present = False
        self.preview_active_is_displayed = True
        self.preview_active_is_enabled = True
        self.preview_inactive_is_present = True
        self.preview_inactive_is_displayed = False
        self.preview_inactive_is_enabled = True
        self.add_location_is_present = True
        self.add_location_is_displayed = True
        self.add_location_is_enabled = False
        self.add_location_is_checked = False
        self.map_is_present = True
        self.map_is_displayed = False
        self.map_marker_is_present = False
        self.map_marker_is_displayed = False
        self.map_marker_is_exact = False
        self.maps_help_is_present = True
        self.maps_help_is_enabled = True
        self.maps_help_is_displayed = True
        self.address_is_present = True
        self.address_is_enabled = True
        self.address_is_displayed = False
        self.address_number_is_present = True
        self.address_number_is_enabled = False
        self.address_number_is_displayed = False
        self.approx_area_is_present = True
        self.approx_area_is_displayed = False
        self.approx_area_is_enabled = True
        self.approx_area_is_checked = True
        self.exact_area_is_present = True
        self.exact_area_is_displayed = False
        self.exact_area_is_enabled = False
        self.exact_area_is_checked = False
        self.show_location_is_present = True
        self.show_location_is_displayed = False
        self.show_location_is_enabled = False
        self.address_nok_is_present = True
        self.address_nok_is_displayed = False
        self.address_nok_text = ''
        self.address_ok_is_present = True
        self.address_ok_is_displayed = False

    def state_enabled(self):
        self.state_default()
        self.add_location_is_enabled = True

    def state_active(self, address=None, number=None):
        self.state_enabled()
        if address is not None:
            self.address_value = address
        if number is not None:
            self.address_number_value = number
        self.add_location_is_checked = True
        self.address_is_displayed = True
        self.address_number_is_displayed = True
        self.preview_inactive_is_displayed = False
        self.preview_inactive_is_present = False
        self.preview_active_is_displayed = True
        self.preview_active_is_present = True
        self.approx_area_is_displayed = True
        self.exact_area_is_displayed = True
        self.show_location_is_displayed = True

    def state_active_with_address(self, address=None, number=None):
        self.state_active(address, number)
        self.address_number_is_enabled = True
        self.show_location_is_enabled = True

    def state_active_with_address_map(self, address=None):
        self.state_active_with_address(address)
        self.show_location_is_displayed = False
        self.preview_active_is_displayed = False
        self.map_is_displayed = True
        self.map_marker_is_loaded = True
        self.map_marker_is_present = True
        self.map_marker_is_displayed = True

    def state_active_with_number(self, address=None, number=None):
        self.state_active_with_address(address, number)
        self.show_location_is_displayed = True
        self.map_marker_is_present = True
        self.approx_area_is_checked = False
        self.exact_area_is_enabled = True
        self.exact_area_is_checked = True
        self.preview_active_is_displayed = True
        self.address_nok_is_displayed = False
        self.address_nok_text = ''

    def state_active_with_number_map(self, address=None, number=None):
        self.state_active_with_number(address, number)
        self.show_location_is_displayed = False
        self.preview_active_is_displayed = False
        self.map_is_displayed = True
        self.map_marker_is_exact = True
        self.map_marker_is_loaded = True
        self.map_marker_is_present = True
        self.map_marker_is_displayed = True


    def condition_error_address_not_found(self):
        self.address_nok_text = 'La direcci�n que ingresaste no existe en �sta comuna y/o regi�n'
        self.address_nok_is_displayed = True

    def condition_approx_area_checked(self):
        self.approx_area_is_checked = True
        self.exact_area_is_checked = False
        self.map_marker_is_exact = False

    def condition_exact_area_checked(self):
        self.approx_area_is_checked = False
        self.exact_area_is_checked = True
        self.map_marker_is_exact = True

    def condition_info_address_found(self):
        self.address_ok_is_displayed = True


    def assert_form(self, scenario_description):
        # wait for the wait_for expression goes True
        self.wait(scenario_description)

        # preview inactive
        self.makeAssert(self.preview_inactive_is_present,
                        self.page.map_form.map.is_element_present('map_preview_inactive', no_wait=True),
                        'preview_inactive is not correctly drawn', scenario_description)
        if self.preview_inactive_is_present:
            self.makeAssert(self.preview_inactive_is_displayed,
                            self.page.map_form.map.map_preview_inactive.is_displayed(),
                            'preview_inactive is not correctly displayed', scenario_description)
            self.makeAssert(self.preview_inactive_is_enabled,
                            self.page.map_form.map.map_preview_inactive.is_enabled(),
                            'preview_inactive is not correctly enabled', scenario_description)

        # preview active
        self.makeAssert(self.preview_active_is_present,
                        self.page.map_form.map.is_element_present('map_preview_active', no_wait=True),
                        'preview_active is not correctly drawn', scenario_description)
        if self.preview_active_is_present:
            self.makeAssert(self.preview_active_is_displayed,
                            self.page.map_form.map.map_preview_active.is_displayed(),
                            'preview_active is not correctly displayed', scenario_description)
            self.makeAssert(self.preview_active_is_enabled,
                            self.page.map_form.map.map_preview_active.is_enabled(),
                            'preview_active is not correctly enabled', scenario_description)

        # add_location
        self.makeAssert(self.add_location_is_present,
                        self.page.map_form.is_element_present('add_location', no_wait=True),
                        'add_location is not correctly drawn', scenario_description)
        if self.add_location_is_present:
            self.makeAssert(self.add_location_is_displayed,
                            self.page.map_form.add_location.is_displayed(),
                            'add_location is not correctly displayed', scenario_description)
            self.makeAssert(self.add_location_is_enabled,
                            self.page.map_form.add_location.is_enabled(),
                            'add_location is not correctly enabled', scenario_description)
            self.makeAssert(self.add_location_is_checked,
                            self.page.map_form.add_location.is_selected(),
                            'add_location is not correctly checked', scenario_description)

        # address
        self.makeAssert(self.address_is_present,
                        self.page.map_form.is_element_present('address', no_wait=True),
                        'address is not correctly drawn', scenario_description)
        if self.address_is_present:
            self.makeAssert(self.address_is_displayed,
                            self.page.map_form.address.is_displayed(),
                            'address is not correctly displayed', scenario_description)
            self.makeAssert(self.address_is_enabled,
                            self.page.map_form.address.is_enabled(),
                            'address is not correctly enabled', scenario_description)
            self.makeAssert(self.address_value,
                            self.page.map_form.address.get_attribute('value'),
                            'address does not have the correct value', scenario_description)

        # address_number
        self.makeAssert(self.address_number_is_present,
                        self.page.map_form.is_element_present('address_number', no_wait=True),
                        'address_number is not correctly drawn', scenario_description)
        if self.address_number_is_present:
            self.makeAssert(self.address_number_is_displayed,
                            self.page.map_form.address_number.is_displayed(),
                            'address_number is not correctly displayed', scenario_description)
            self.makeAssert(self.address_number_is_enabled,
                            self.page.map_form.address_number.is_enabled(),
                            'address_number is not correctly enabled', scenario_description)
            self.makeAssert(self.address_number_value,
                            self.page.map_form.address_number.get_attribute('value'),
                            'address_number does not have the correct value', scenario_description)

        # approx_area
        self.makeAssert(self.approx_area_is_present,
                        self.page.map_form.is_element_present('approx_area', no_wait=True),
                        'approx_area_area is not correctly drawn', scenario_description)
        if self.approx_area_is_present:
            self.makeAssert(self.approx_area_is_displayed,
                            self.page.map_form.approx_area.is_displayed(),
                            'approx_area is not correctly displayed', scenario_description)
            self.makeAssert(self.approx_area_is_enabled,
                            self.page.map_form.approx_area.is_enabled(),
                            'approx_area is not correctly enabled', scenario_description)
            self.makeAssert(self.approx_area_is_checked,
                            self.page.map_form.approx_area.is_selected(),
                            'approx_area does not have the correct value', scenario_description)

        # exact_area
        self.makeAssert(self.exact_area_is_present,
                        self.page.map_form.is_element_present('exact_area', no_wait=True),
                        'exact_area_area is not correctly drawn', scenario_description)
        if self.exact_area_is_present:
            self.makeAssert(self.exact_area_is_displayed,
                            self.page.map_form.exact_area.is_displayed(),
                            'exact_area is not correctly displayed', scenario_description)
            self.makeAssert(self.exact_area_is_enabled,
                            self.page.map_form.exact_area.is_enabled(),
                            'exact_area is not correctly enabled', scenario_description)
            self.makeAssert(self.exact_area_is_checked,
                            self.page.map_form.exact_area.is_selected(),
                            'exact_area is not correctly checked', scenario_description)

        # show_location
        self.makeAssert(self.show_location_is_present,
                        self.page.map_form.map.is_element_present('show_location', no_wait=True),
                        'show_location is not correctly drawn', scenario_description)
        if self.show_location_is_present:
            self.makeAssert(self.show_location_is_displayed,
                            self.page.map_form.map.show_location.is_displayed(),
                            'show_location is not correctly displayed', scenario_description)
            self.makeAssert(self.show_location_is_enabled,
                            self.page.map_form.map.show_location.is_enabled(),
                            'show_location is not correctly enabled', scenario_description)

        # address_nok
        self.makeAssert(self.address_nok_is_present,
                        self.page.map_form.is_element_present('address_nok', no_wait=True),
                        'address_nok is not correctly drawn', scenario_description)
        if self.address_nok_is_present:
            self.makeAssert(self.address_nok_is_displayed,
                            self.page.map_form.address_nok.is_displayed(),
                            'address_nok is not correctly displayed', scenario_description)
            self.makeAssert(self.address_nok_text,
                            self.page.map_form.address_nok.text,
                            'address_nok does not have the correct value', scenario_description)

        # address_ok
        self.makeAssert(self.address_ok_is_present,
                        self.page.map_form.is_element_present('address_ok', no_wait=True),
                        'address_ok is not correctly drawn', scenario_description)
        if self.address_ok_is_present:
            self.makeAssert(self.address_ok_is_displayed,
                            self.page.map_form.address_ok.is_displayed(),
                            'address_ok is not correctly displayed', scenario_description)

        # map
        self.makeAssert(self.map_is_present,
                        self.page.map_form.map.is_element_present('map_container', no_wait=True),
                        'map_container is not correctly drawn', scenario_description)

        # map.marker
        self.makeAssert(self.map_marker_is_present or self.map_marker_is_loaded,
                        self.page.map_form.map.is_element_present('marker', no_wait=True),
                        'map_marker is not correctly drawn', scenario_description)
        if self.map_marker_is_present and self.map_is_displayed:
            self.makeAssert(self.map_marker_is_displayed,
                            self.page.map_form.map.marker.is_displayed(),
                            'map_marker is not correctly displayed', scenario_description)
            print(self.page.map_form.map.marker.tag_name == 'img')
            self.makeAssert(self.map_marker_is_exact,
                            self.page.map_form.map.marker.tag_name == 'img',
                            'map_marker does not have the right icon', scenario_description)


    def wait(self, message=None):
        """wait for the wait_for expression goes True"""
        if self.wait_for is not None:
            self.page.wait.until(self.wait_for, message)
            self.wait_for = None


    def makeAssert(self, expected, value, assert_description, scenario_description):
        """make the assertions and generate a friendly developer message"""
        failed_message = scenario_description + ': ' + assert_description
        self.test.assertEqual(expected, value, failed_message)
