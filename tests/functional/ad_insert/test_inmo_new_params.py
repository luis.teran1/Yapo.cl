# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.steps import insert_an_ad, trans
from yapo.pages.newai import DesktopAI
from yapo.pages.desktop import Login
from yapo.steps import insert_an_ad, trans
from yapo.steps.accounts import login_and_go_to_dashboard
import yapo


class InmoNewParams(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_inmo1']
    AdInsert = AdInsertDesktop

    def _insert_ad(self, wd, update={}, expected=True):
        data = {
            'category': '1220',
            'estate_type': 1,
            'rooms': 1,
            'bathrooms': 1,
        }
        data.update(update)
        insert_an_ad(wd, AdInsert=DesktopAI, **data)
        try:
            wd.find_element_by_css_selector('.PackConfirmationPage')
            self.assertTrue(expected)
        except:
            self.assertTrue(not expected)

    def test_insert_new_params(self, wd):
        """ Tests insertion using inmo new params """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        self._insert_ad(wd, {
            'ext_code': 'INMO123123',
            'util_size': '120',
            'built_year': '2010',
            'eqp[1_3]': True,
            'eqp[2_2]': True,
            'eqp[3_1]': True,
            'logged': True,
            'price': 10000,
            }, True)

    def _get_label(self, wd, elem):
        return wd.find_element_by_css_selector('label[for="{}"]'.format(elem)).text

    def test_new_params_label(self, wd):
        """ Asserts util size label for every estate type """
        cases = [
            ({'category':1220, 'estate_type':1, 'price': '100000', 'built_year': '2000'},
                {'ext_code': None,
                 'util_size': 'Superficie �til',
                 'built_year': 'A�o de construcci�n'}),
            ({'category':1220, 'estate_type':1, 'price': '100000', 'built_year': '2000', 'log_as':'cuentaproconpack@yapo.cl'},
                {'ext_code': 'C�digo inmobiliaria',
                 'util_size': 'Superficie �til',
                 'built_year': 'A�o de construcci�n'}),
            ({'category':1220, 'estate_type':1, 'price': '100000', 'built_year': '2000'},
                {'equipment': 'Equipamiento',
                 'util_size': 'Superficie �til',
                 'built_year': 'A�o de construcci�n'}),
        ]

        ai = self.AdInsert(wd)
        for config, messages in cases:
            do_log = 'log_as' in config
            if do_log:
                login_and_go_to_dashboard(wd, config['log_as'], '123123123')
                del config['log_as']

            ai.go()
            ai.insert_ad(**config)
            for param in messages:
                if messages[param]:
                    self.assertEqual(self._get_label(wd, param), messages[param])

            if do_log:
                Login(wd).logout()


class InmoNewParamsMsite(yapo.SeleniumTest):

    AdInsert = AdInsertMobile
    snaps = ['accounts', 'diff_pack_inmo1']

    def _insert_ad(self, wd, update={}, expected=True):
        data = {
            'category': '1220',
            'estate_type': 1,
            'rooms': 1,
            'bathrooms': 1,
            'subject': 'msite test',
            'body': 'the body',
            'region': 4,
            'communes': 21
        }
        data.update(update)
        ai = self.AdInsert(wd)
        ai.go()
        ai.insert_ad(**data)
        ai.submit.click()

        try:
            wd.find_element_by_css_selector('.paymentHeader info-box').get_attribute('title')
            self.assertTrue(expected)
        except:
            self.assertTrue(not expected)

    def test_insert_new_params(self, wd):
        """ Tests insertion using inmo new params """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        self._insert_ad(wd, {
            'eqp': ['1_3', '2_2', '3_1'],
            'logged': True,
            'price': 10000,
            'ext_code': 'INMO123123',
            'util_size': '120',
            'built_year': '2010',
            }, True)
