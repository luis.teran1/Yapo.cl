# -*- coding: latin-1 -*-
""" Class for FT227869075: Javascript injection """

from yapo.decorators import tags
from selenium.common.exceptions import NoAlertPresentException
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
import yapo
import time

def _is_alert_present(browser):
    """ Checks if an alert popup is present """
    try:
        time.sleep(3)
        browser.switch_to.alert.accept()
        return True
    except NoAlertPresentException:
        return False

class TestFT(yapo.SeleniumTest):
    """ Test class for FT227869075 """

    @tags('ad_insert', 'desktop', 'maps', 'ad_view')
    def test_js_injection(self, browser):
        """ tries to insert js alert, see if alert is present """
        insert_an_ad(browser, AdInsert=DesktopAI, subject='Test map',
                       body='yoLo map',
                       category=3040,
                       region=15,
                       communes=331,
                       add_location=True,
                       address='que shuuusha!',
                       address_number='1361',
                       price=999666,
                       private_ad=True,
                       name='yolo',
                       email='yolo@cabral.es',
                       email_verification='yolo@cabral.es',
                       phone='999966666',
                       password='999966666',
                       password_verification='999966666',
                       create_account=True,
                       accept_conditions=True)
        browser.execute_script('$("#address").val("\\"><script>alert(\\"YOLO\\");</script>\\"")')
        alert_present = _is_alert_present(browser)
        self.assertFalse(alert_present)
