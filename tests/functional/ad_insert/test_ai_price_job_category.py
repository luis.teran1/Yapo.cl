from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
import yapo


class PriceJobCategory(yapo.SeleniumTest):

    @tags('desktop', 'accounts')
    def test_ai_no_price_in_job_category(self, wd):
        """ Ai: No price in job category """

        ai = AdInsert(wd)
        ai.go()
        insert_an_ad(wd, AdInsert=DesktopAI, category=7040, Submit=False)
        ai.wait.until_not_present('price')
        insert_an_ad(wd, AdInsert=DesktopAI, category=2020, Submit=False)
        ai.wait.until_present('price')
