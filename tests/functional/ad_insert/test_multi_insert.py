# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, AdInsertedSuccessfuly as PackSuccessMobile
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop,  PackConfirm as PackSuccessDesktop
from yapo.steps import insert_an_ad
import yapo


class AdInsertMultiInsertDesktop(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {
        '*.*.multi_insert_checker.enabled': 1,
        '*.*.multi_insert_checker.attempts': 0,
        '*.*.multi_insert_checker.category.5020.enabled': 1,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    AdInsert = AdInsertDesktop
    PackSuccess = PackSuccessDesktop

    def test_many_inserts(self, wd):
        """Check many inserts  """
        page = self.AdInsert(wd)
        page.go()
        data = {
            'email': 'ana@lele.le',
            'email_verification': 'ana@lele.le'
        }
        insert_an_ad(wd, AdInsert=self.AdInsert, **data)
        page.submit.click()
        page.wait.until_visible('err_error')
        self.assertTrue(page.err_error.text, page.ERROR_MULTI_INSERT)

    def test_pro_user_shoud_pass(self, wd):
        """Verify if pro users can insert several times"""
        page = self.AdInsert(wd)
        page.go()
        email = 'cuentaprosinpack@yapo.cl'
        data = {
            'category': 2020,
            'type': 's',
            'email': email,
            'email_verification': email
        }

        insert_an_ad(wd, AdInsert=self.AdInsert, **data)
        result = self.PackSuccess(wd)
        self.assertTrue(result.pack_message_nopack.is_displayed())


class AdInsertMultiInsertMsite(AdInsertMultiInsertDesktop):

    AdInsert = AdInsertMobile
    PackSuccess = PackSuccessMobile


class AdInsertMultiInsert(yapo.SeleniumTest):

    AdInsert = AdInsertMobile
    PackSuccess = PackSuccessMobile

    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {
        '*.*.multi_insert_checker.enabled': 1,
        '*.*.multi_insert_checker.attempts': 2,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    def _insert(self, wd, email='perico@yapo.cl'):
        page = self.AdInsert(wd)
        page.go()
        data = {
            'category': 2020,
            'type': 's',
            'email': email,
            'email_verification': email
        }
        insert_an_ad(wd, AdInsert=self.AdInsert, **data)

    def test_insert_several_times(self, wd):
        """ Insert several times and 3rd time the error should be displayed  """
        for i in range(2):
            self._insert(wd)
            result = self.PackSuccess(wd)
            result.wait.until_present('info_box')
            self.assertTrue(result.text_success_message.is_displayed())

        self._insert(wd)
        page = self.AdInsert(wd)
        page.wait.until_visible('err_error')
        self.assertTrue(page.err_error.text, page.ERROR_MULTI_INSERT)
