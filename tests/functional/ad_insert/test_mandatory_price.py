# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages.mobile import NewAdEdit as AdEditMobile
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.desktop import AdEdit as AdEditDesktop
from yapo.steps import insert_an_ad
from yapo import utils
import time, yapo


class MandatoryPriceTest(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    AdEdit = AdEditDesktop
    ERROR_INSERT_PRICE = 'Escribe un precio'

    def _insert_ad(self, wd, cat, price, expected, AdInsert):
        result = insert_an_ad(wd, AdInsert=AdInsert,
            category=cat,
            price=price
        )
        self.assertEquals(result.was('success'), expected)

    def _edit_ad(self, wd, list_id, price, expected):
      
        edit = self.AdEdit(wd)
        edit.edit_an_ad(list_id, '123123123', {'price':price}, submit=expected)
        if not expected:
            edit.submit.click()
        result = AdInsertResult(wd)
        self.assertEquals(result.was('success'), expected)


    @tags('ad_insert')
    def test_insert_on_1220_without(self, wd):
        """ Tries to insert ads on 1220 category without price """
        self._insert_ad(wd, 1220, '', False, self.AdInsert)

    @tags('ad_insert')
    def test_insert_on_1240_without(self, wd):
        """ Tries to insert ads on 1240 category without price """
        self._insert_ad(wd, 1240, '', False, self.AdInsert)

    @tags('ad_insert')
    def test_insert_on_1220_with(self, wd):
        """ Tries to insert ads on 1220 category with price """
        self._insert_ad(wd, 1220, '10000', True, self.AdInsert)

    @tags('ad_insert')
    def test_insert_on_1240_with(self, wd):
        """ Tries to insert ads on 1240 category with price """
        self._insert_ad(wd, 1240, '20000', True, self.AdInsert)

    @tags('ad_insert')
    def test_insert_on_1260_without(self, wd):
        """ Tries to insert ads on 1260 category without price """
        self._insert_ad(wd, 1260, '', True, self.AdInsert)

    def test_price_input_validation(self, wd):
        """ Tests price input validation """
        page = self.AdInsert(wd)
        page.go()
        page.ordered_fill_form([
            ('category', 1220),
            ('price', ''),
            ('subject', 'moving focus...'),
        ])
        self.assertEqual(page.get_error_for('price').text, self.ERROR_INSERT_PRICE)

    @tags('ad_edit')
    def test_edit_1220_without(self, wd):
        """ Edits ad removing price on cat 1220: error """
        self._edit_ad(wd, 8000018, '', False)

    @tags('ad_edit')
    def test_edit_2000_without(self, wd):
        """ Edits ad removing price on cat 2000: allowed """
        self._edit_ad(wd, 8000017, '', True)

class MandatoryPriceTestMobile(MandatoryPriceTest):
    AdInsert = AdInsertMobile
    AdEdit = AdEditMobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def test_price_input_validation(self, wd):
        pass
