# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertSuccess, AdInsertWithAccountSuccess, AdInsertWithoutAccountSuccess, AdInsertResult, AdInsertPreview
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.steps import accept_and_return_ad_id
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.pages.desktop import Login, HeaderElements
from yapo.pages.generic import SentEmail
from yapo.pages import accounts
from yapo import utils
from yapo import conf
from yapo.utils import JsonTool
from yapo.mail_generator import Email
import time, yapo
import unittest

class AdInsertTest(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    pro_data = {}

    @tags('ad_insert', 'validations', 'migrated_frontend')
    def test_ai_js_priceformat(self, wd):
        """Check price autoformater (Test migrated from Frontend)"""
        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(subject="test",
                       body="some test text",
                       region=15,
                       category=1220,
                       private_ad=True,
                       communes=315)
        page.fill_form(price="10000")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "10.000")
        page.fill_form(price="1000")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "1.000")
        page.fill_form(price="1000000")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "1.000.000")
        page.fill_form(price="12as4.asd,1")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "1.241")
        self.assertEqual(page.warning2.text, '�Seguro que el precio es correcto? Recuerda que se trata de pesos.')
        self.assertTrue(page.warning2.is_displayed())
        page.fill_form(price="4900")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "4.900")
        page.fill_form(price="100000000000")
        page.body.click()
        self.assertEqual(page.price.get_attribute("value"), "1.000.000.000")

    @tags('ad_insert', 'validations')
    def test_validations(self, browser):
        page = self.AdInsert(browser)
        page.go()
        page.phone.clear()
        page.submit.click()
        page.wait.until_visible("errors")
        expected_errors = [
                "Selecciona una categor�a",
                "Escribe un t�tulo",
                "Escribe una descripci�n",
                "Selecciona una regi�n",
                "Selecciona una comuna",
                "Selecciona el tipo de aviso.",
                "Escribe tu nombre",
                "Escribe tu e-mail",
                "Debes introducir tu contrase�a",
                "Tienes que aceptar las condiciones", ]
        self.assertEqual(
                [e.text for e in page.errors if e.is_displayed()],
                expected_errors)

        self.assertEqual(page.err_phone.text,
                "Tel�fono muy corto, c�digo de �rea + n�mero completo")

    @tags('ad_insert', 'desktop', 'account')
    def test_check_mail_on_ad_insert_with_create_account(self, wd):
        """ Check mail on ad insert with create account"""
        result = insert_an_ad(wd, AdInsert=DesktopAI, subject="Test test",
                       body="yoLo",
                       region=15,
                       communes=331,
                       category=3040,
                       price=999666,
                       private_ad=True,
                       name="tarta",
                       email="tarta@dequeso.es",
                       email_verification="tarta@dequeso.es",
                       phone="99996666",
                       password="999966666",
                       password_verification="999966666",
                       create_account=True,
                       accept_conditions=True)

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('with_account_create'))

        email = SentEmail(wd)
        email.go(old = True)
        link = email.body.find_element_by_css_selector("a").get_attribute("href")
        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual('Hola, tarta', header.button_logged_user.text)

    @tags('ad_insert', 'desktop', 'migrated_frontend')
    def test_ai_dynamic_state_regions(self, wd):
        """Dynamic change of comuna for region (Test migrated from Frontend)"""
        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(region=3, category=1240)
        page.insert_ad(communes=13)
        self.assertEqual(page.map_form.communes.get_attribute('value'),'13')
        self.assertEqual(page.map_form.get_first_selected_option('communes').text, 'Calama')
        page.insert_ad(region=11)
        self.assertEqual(page.map_form.communes.get_attribute('value'),'')
        page.insert_ad(communes=233)
        self.assertEqual(page.map_form.get_first_selected_option('communes').text, 'Futrono')
        self.assertFalse('Calama' in page.map_form.get_all_options_text('communes'))
        page.insert_ad(region=2)
        self.assertEqual(page.map_form.communes.get_attribute('value'),'')
        page.insert_ad(communes=8)
        self.assertEqual(page.map_form.get_first_selected_option('communes').text, 'Huara')
        self.assertFalse('Futrono' in page.map_form.get_all_options_text('communes'))

    @tags('ad_insert', 'desktop', 'maps', 'preview', 'core1')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_insert_cell_phone_ad_by_preview(self, browser):
        """ Insert a cell phone ad by preview on desktop """

        test_data_key = 'cell_phone_with_map'
        test_name = '{0}.{1}'.format(type(self).__name__,self._testMethodName)
        test_data = JsonTool.read_test_data(test_data_key)
        page = self.AdInsert(browser)
        page.go()

        email = '{0}@mailinator.com'.format(time.time())
        test_data['email'] = email
        test_data['email_verification'] = email
        page.insert_ad(**test_data)
        page.wait.until_not_present("img_loader")
        AdInsertTest.pro_data[test_name] = {'email':test_data['email'], 'subject':test_data['subject']}
        page.preview.click()
        preview = AdInsertPreview(browser)
        self.assertEqual(preview.advertiser_name.text,test_data['name'])
        self.assertEqual(preview.da_subject.text,test_data['subject'])
        self.assertEqual(preview.description.text,test_data['body'])
        preview.create.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

        ad_id = accept_and_return_ad_id(browser, **AdInsertTest.pro_data[test_name])
        AdInsertTest.pro_data[test_name].update(ad_id)

    @tags('ad_insert', 'desktop', 'maps', 'images', 'core1')
    def test_ad_insert_6_cars_for_pack_auto(self, browser):
        """ Insert 6 car ads on desktop """
        test_data_key = ["mitsubishi_car", "byd_car", "brilliance_car", "buick_car",
                         "agrale_car", "lifan_car"]
        test_name = '{0}.{1}'.format(type(self).__name__,self._testMethodName)
        email = '{0}@mailinator.com'.format(time.time())
        for i in range(0,6):
            test_data = JsonTool.read_test_data(test_data_key[i])
            test_data['email'] = email
            test_data['email_verification'] = email

            result = insert_an_ad(browser, AdInsert=DesktopAI, **test_data)
            self.assertTrue(result.was('success'))

            ad_id = accept_and_return_ad_id(browser, email = test_data['email'], subject = test_data['subject'])
        mlt = Email.create(browser)
        mlt.check_account_activation(self, email)

class AdInsertAccountTest(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop

    @tags('ad_insert', 'desktop', 'account')
    def test_ai_update_account_phone_if_is_empty(self, wd):
        """ Insert ad on desktop update account phone if is empty"""
        account_phone = '87654321'

        # Update account phone, value is empty
        utils.db_query("UPDATE accounts SET phone='' WHERE email = 'prepaid5@blocket.se'")
        utils.flush_redises()

        # Login and insert ad
        login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123')
        ad_insert = self.AdInsert(wd)
        ad_insert.go()

        ad_insert.insert_ad(subject="Test test",
                       body="yoLo",
                       region=15,
                       communes=331,
                       category=3040,
                       price=999666,
                       accept_conditions=True,
                       phone=account_phone)

        ad_insert.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('without_account_create'))

        # Verify phone in profile
        edit_account = accounts.Edit(wd)
        edit_account.go()
        self.assertEquals(account_phone, edit_account.phone.get_attribute('value'), 'Phone of the ad not update account phone')


class AdInsertCoreDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    pro_data = {}
    AdInsert = AdInsertDesktop
    img_selector = 'img[alt="image"]'
    
    @tags('ad_insert', 'desktop', 'maps', 'images', 'core1')
    def test_insert_with_map_6_images(self, browser):
        """ Insert ad on desktop with map and 6 images"""
        test_data_key = 'usb_fan_with_map'
        test_name = '{0}.{1}'.format(type(self).__name__,self._testMethodName)
        test_data = JsonTool.read_test_data(test_data_key)
        page = self.AdInsert(browser)
        page.go()
        email = '{0}@mailinator.com'.format(time.time())
        test_data['email'] = email
        test_data['email_verification'] = email
        if self.AdInsert == AdInsertMobile:
            page.insert_ad(**test_data)
        else:
            insert_an_ad(browser, AdInsert=DesktopAI, Submit=False, **test_data)
        for i in range(6):
            page.upload_resource_image('qa_tests/ventilador-{0}.jpg'.format(i))

        page.wait.until_not_present("img_loader")
        page.wait.until_elements_equals(browser, self.img_selector, 6)
        AdInsertTest.pro_data[test_name] = {'email':test_data['email'], 'subject':test_data['subject']}
        page.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

        ad_id = accept_and_return_ad_id(browser, **AdInsertTest.pro_data[test_name])
        AdInsertTest.pro_data[test_name].update(ad_id)

    @tags('ad_insert', 'desktop', 'maps', 'images', 'core1')
    def test_insert_with_map_20_images(self, browser):
        """ Insert ad on desktop with map and 20 images"""
        test_data_key = 'niva_car_with_map'
        test_name = '{0}.{1}'.format(type(self).__name__,self._testMethodName)
        test_data = JsonTool.read_test_data(test_data_key)
        page = self.AdInsert(browser)
        page.go()
        email = '{0}@mailinator.com'.format(time.time())
        test_data['email'] = email
        test_data['email_verification'] = email

        if self.AdInsert == AdInsertMobile:
            page.insert_ad(**test_data)
        else:
            insert_an_ad(browser, AdInsert=DesktopAI, Submit=False, **test_data)
        for i in range(6):
            page.upload_resource_image('qa_tests/niva-{0}.jpg'.format(i))

        if utils.is_not_regress():
            for i in range(6,20):
                page.upload_resource_image('qa_tests/niva-{0}.jpg'.format(i))
            page.wait.until_not_present("img_loader")
            page.wait.until_elements_equals(browser, self.img_selector, 20)
        else:
            page.wait.until_elements_equals(browser, self.img_selector, 6)
        AdInsertTest.pro_data[test_name] = {'email':test_data['email'], 'subject':test_data['subject']}
        page.submit.click()

        result = AdInsertResult(browser)
        self.assertTrue(result.was('success'))

        ad_id = accept_and_return_ad_id(browser, **AdInsertTest.pro_data[test_name])
        AdInsertTest.pro_data[test_name].update(ad_id)


class AdInsertCoreMobile(AdInsertCoreDesktop):

    AdInsert = AdInsertMobile
    user_agent = utils.UserAgents.NEXUS_5
    img_selector = '.image'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}


class DesktopAdInsertLoginAttempts(yapo.SeleniumTest):
    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    error_messages = [
        'Contrase�a incorrecta! El correo que ingresaste ya tiene cuenta, utiliza la misma contrase�a para continuar.\n�La olvidaste?',
        'Contrase�a incorrecta! El correo que ingresaste ya tiene cuenta, utiliza la misma contrase�a para continuar.\n�La olvidaste?',
        'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
    ]

    @tags('desktop', 'ad_insert', 'accounts', 'login_attempts')
    def test_ad_insert_with_wrong_password(self, wd):
        """
        Verify that the account is locked when the user tries insert an ad
        more than 3 times with wrong password for an email with account
        """
        email = 'prepaid3@blocket.se'
        title_email = 'Hemos detectado que no puedes ingresar a tu cuenta Yapo.cl'
        utils.clean_mails()
        # Go to ad
        ad_insert_page = self.AdInsert(wd)
        # tries to access more than 3 times with wrong password
        for i, msg in enumerate(self.error_messages):
            ad_insert_page.go()
            from yapo.utils import Categories
            from yapo.geography import Regions
            if self.AdInsert == AdInsertMobile:
                ad_insert_page.insert_ad(**{
                'email': email,
                'email_verification': email,
                'password': 'incorrect password',
                'password_verification': 'incorrect password',
                'category': Categories.OTROS_PRODUCTOS,
                'region': Regions.REGION_METROPOLITANA,
                'communes': 331,
                'subject': 'My ad',
                'body': 'The body of my ad',
                'create_account': False,
                'name': 'Juan Perez',
                'phone': '55555555',
                })
            else:
                insert_an_ad(wd, AdInsert=DesktopAI, Submit=False, **{
                    'email': email,
                    'email_verification': email,
                    'password': 'incorrect password',
                    'password_verification': 'incorrect password',
                    'category': Categories.OTROS_PRODUCTOS,
                    'region': Regions.REGION_METROPOLITANA,
                    'communes': 331,
                    'subject': 'My ad',
                    'body': 'The body of my ad',
                    'create_account': False,
                    'name': 'Juan Perez',
                    'phone': '55555555',
                })
            ad_insert_page.submit.click()
            # Verify message
            ad_insert_page.wait.until_present('errors')
            self.assertIn(msg, [msg.text for msg in ad_insert_page.errors])
            # Verify increment key in redis
            self.assertEqual(int(utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'get', 'rsd1x_login_{0}'.format(email))), i+1)
            # Verify that the email is sent in third intent
            if (i + 1) == 3:
                self.assertIsNotNone(utils.search_mails('({0})'.format(title_email)))
            else:
                self.assertIsNone(utils.search_mails('({0})'.format(title_email), max_retries=1))


class MobileAdInsertLoginAttempts(DesktopAdInsertLoginAttempts):
    AdInsert = AdInsertMobile
    user_agent = utils.UserAgents.IOS
    error_messages = [
        'Contrase�a incorrecta. El E-mail ingresado ya tiene cuenta.',
        'Contrase�a incorrecta. El E-mail ingresado ya tiene cuenta.',
        'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
    ]
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
