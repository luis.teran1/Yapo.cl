# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from selenium.webdriver.common.keys import Keys
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult, AdInsertSuccess, AdInsertDuplicatedAd, AdInsertDuplicatedAdActiveResult, AdInsertDuplicatedAdPublishResult, AdInsertPreview
from yapo.pages.desktop import Login, DashboardMyAds, SummaryPayment as SummaryPaymentDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, SummaryPayment as SummaryPaymentMobile
from yapo.steps.newai import insert_ad
from yapo.pages.newai import DesktopAI
from yapo.steps import trans
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.utils import Categories
import time
import unittest
import yapo


class AdInsertDuplicateAdDesktop(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop
    SummaryPayment = SummaryPaymentDesktop
    is_desktop = True

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(
            ['accounts', 'diff_deactive_ads'])
        yapo.utils.rebuild_csearch()

    user = {'name': 'BLOCKET',
            'email': 'prepaid5@blocket.se',
            'passwd': '123123123'}

    test_message = 'Tienes un aviso INACTIVO similar al que acabas de insertar.'

    @tags('ad_insert', 'account', 'minimal', 'duplicate')
    def test_duplicate_ad_insert_account_logged(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected
        as duplicated """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertEqual(result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text, "Puedes Revivir + Subir ahora para ubicar tu aviso en el top del listado o Activar " + \
            "para dejarlo en la misma posici�n anterior.")
        self.assertTrue(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present(
            'publish_f_2' if self.is_desktop else 'publish_f'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_account_logged_activate_bump(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected
        as duplicated, The 'activar y subir' button redirects to the payment page """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('activate_and_bump')
        result.activate_and_bump.click()

        payment = self.SummaryPayment(wd)
        payment.wait.until_visible('link_to_pay_products')
        self.assertTrue(payment.is_element_present('link_to_pay_products'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_account_logged_activate(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected
        as duplicated, The 'activar' button redirects to result page """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('activate_da'))

        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)
        if self.is_desktop:
            result.wait.until_present('modal_message')
            result.wait.until(lambda wd: result.modal_message.text != '')
            self.assertEqual(
                result.modal_message.text, 'Tu aviso "Adeptus Astartes Codex" estar� activo en los pr�ximos minutos.')
        else:
            result.wait.until_present('message')
            result.wait.until(lambda wd: result.message.text != '')
            self.assertIn('Reviviendo Aviso\nTu aviso estar� publicado en los pr�ximos minutos.', result.message.text)
        self.assertTrue(result.is_element_present('see_das'))
        self.assertTrue(result.is_element_present('my_account'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_account_logged_force_publish(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected
        as duplicated, The 'publicalo aqui' button redirects to result page, Upselling is not present """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')

        self.assertTrue(result.is_element_present('publish_f_2'))

        result.publish_f_2.click()
        result = AdInsertSuccess(wd)
        self.assertIn(
            "Tu aviso Adeptus Astartes Codex ser� revisado y si est� de acuerdo con las reglas de Yapo.cl, " + \
            "lo publicaremos en los pr�ximos minutos.", result.success_message.text)
        result = AdInsertResult(wd)
        self.assertFalse(result.is_element_present('products_box'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_account_unlogged(self, wd):
        """ Duplicate ad - With an user unlogged (with account), Try to insert an ad with a previous deactived ad,
        The ad is detected as duplicated """
        self._insert_ad_unlogged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')

        self.assertEqual(
            result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text, "Puedes Revivir + Subir ahora para ubicar tu aviso en el top del listado o Activar " + \
            "para dejarlo en la misma posici�n anterior.")
        self.assertTrue(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present(
            'publish_f_2' if self.is_desktop else 'publish_f'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_uf(self, wd):
        """ Duplicate ad - inserts duplicated ad in UF and asserts display of price in delete to bump page """
        self._insert_ad_uf(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')

        self.assertEqual(
            result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text, "Puedes Revivir + Subir ahora para ubicar tu aviso en el top del listado o " \
            "Activar para dejarlo en la misma posici�n anterior.")
        self.assertEquals(result.price.text, 'UF 4.500,32')
        self.assertEquals(result.subject.text, 'Parcelas frente a laguna de aculeo 101 hectareas')
        self.assertTrue(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present(
            'publish_f_2' if self.is_desktop else 'publish_f'))

    @tags('ad_insert', 'duplicate', 'minimal')
    def test_duplicate_ad_insert_without_account_activate(self, wd):
        """ Duplicate ad - With an user without account, Try to insert an ad with a previous deactived ad,
        The ad is detected as duplicated, The password is required again, The 'activar' button redirects to
        result page, The 'Ir a mi cuenta' button is not present """
        self._insert_ad_with_email(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('password_input'))
        result.password_input.send_keys(Keys.ENTER)

        result.wait.until_present('password_err')
        result.wait.until(lambda x: result.password_err.text != '')

        self.assertEqual(result.password_err.text, 'Debes introducir tu contrase�a')
        result.confirm(self.user['passwd'])
        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)

        if self.is_desktop:
            result.wait.until_present('modal_message')
            self.assertEqual(result.modal_message.text, 'Tu aviso "Libro 1" estar� activo en los pr�ximos minutos.')
        else:
            result.wait.until_visible('message')
            self.assertEqual(result.message.text, 'Reviviendo Aviso\nTu aviso estar� publicado en los pr�ximos minutos.')

        self.assertTrue(result.is_element_present('see_das'))
        self.assertFalse(result.is_element_present('my_account'))

    @tags('ad_insert', 'duplicate')
    def test_duplicate_ad_insert_without_account_publish_f(self, wd):
        """ Duplicate ad - With an user without account, Try to insert an ad with a previous deactived ad, The ad is
        detected as duplicated, The password is required again, The 'publicalo aqui' button redirects to result page """
        self._insert_ad_with_email(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('password_input'))
        result.confirm(self.user['passwd'])
        result.publish_f_2.click(
        ) if self.is_desktop else result.publish_f.click()
        result = AdInsertSuccess(wd)
        result.wait.until_visible('info_box')
        self.assertIn(
                "Tu aviso Libro 1 ser� revisado y si est� de acuerdo con las reglas de Yapo.cl, lo " + \
                "publicaremos en los pr�ximos minutos.", result.success_message.text)


    def _insert_ad_logged(self, wd):
        ad_insert = self.AdInsert(wd)
        ad_insert.go()
        self.assertEqual(ad_insert.name.get_attribute('value'), self.user['name'])
        self.assertEqual(ad_insert.email.get_attribute('value'), "prepaid5@blocket.se")
        if self.is_desktop:
            insert_ad(
                DesktopAI(wd),
                subject="Adeptus Astartes Codex",
                body="for teh emporer!",
                region=15,
                communes=331,
                category=6020,
                price=778889,
                logged=True
            )
        else:
            ad_insert.insert_ad(
                subject="Adeptus Astartes Codex",
                body="for teh emporer!",
                region=15,
                communes=331,
                category=6020,
                price=778889,
                accept_conditions=True)
            ad_insert.submit.click()

    def _insert_ad_uf(self, wd):
        if self.is_desktop:
            insert_ad(
                DesktopAI(wd),
                subject="Parcelas frente a laguna de aculeo 101 hectareas",
                body="dos parcelas frente a laguna de aculeo y frente a reserva natural altos de cantillana 1 parcela " + \
                "de 480000 m2 lotiada en 30. otra parcela de 530000 m2. estas parcelas estan contiguas valor de 350 pesos " + \
                "el m2 168000000 cada parcela.",
                region=15,
                communes=331,
                category=1220,
                email='android@yapo.cl',
                email_confirm='android@yapo.cl',
                name=self.user['name'],
                passwd='123123123', passwd_ver='123123123',
                ai_create_account=False,
                phone="99999999",
                price='7789,45',
                currency='uf'
            )
        else:
            ad_insert = self.AdInsert(wd)
            ad_insert.go()
            ad_insert.insert_ad(
                subject="Parcelas frente a laguna de aculeo 101 hectareas",
                body="dos parcelas frente a laguna de aculeo y frente a reserva natural altos de cantillana 1 parcela " + \
                "de 480000 m2 lotiada en 30. otra parcela de 530000 m2. estas parcelas estan contiguas valor de 350 pesos " + \
                "el m2 168000000 cada parcela.",
                region=15,
                communes=331,
                category=1220,
                estate_type=5,
                accept_conditions=True,
                email='android@yapo.cl',
                email_verification='android@yapo.cl',
                password='123123123',
                password_verification='123123123',
                name=self.user['name'],
                create_account=False,
                phone="99999999",
                price='7789,45',
                currency='uf'
            )
            ad_insert.submit.click()

    def _insert_ad_unlogged(self, wd):
        if self.is_desktop:
            insert_ad(
                DesktopAI(wd),
                subject="Adeptus Astartes Codex",
                body="for teh emporer!",
                region=15,
                communes=331,
                category=6020,
                price=778889,
                email=self.user['email'],
                email_confirm=self.user['email'],
                name=self.user['name'],
                passwd=self.user['passwd'],
                passwd_ver=self.user['passwd'],
                ai_create_account=False,
                phone="99999999",
            )
        else:
            ad_insert = self.AdInsert(wd)
            ad_insert.go()
            ad_insert.insert_ad(
                subject="Adeptus Astartes Codex",
                body="for teh emporer!",
                region=15,
                communes=331,
                category=6020,
                price=778889,
                accept_conditions=True,
                email=self.user['email'],
                email_verification=self.user['email'],
                name=self.user['name'],
                password=self.user['passwd'],
                password_verification=self.user['passwd'],
                create_account=False,
                phone="99999999")
            ad_insert.submit.click()

    def _insert_ad_with_email(self, wd):
        if self.is_desktop:
            insert_ad(
                DesktopAI(wd),
                subject='Libro 1',
                body='Este es el libro 1',
                category=8020,
                region=15,
                communes=331,
                add_location=True,
                address='Avenida San Pablo',
                address_number='1361',
                price=9966,
                company_ad=True,
                name='yolo',
                email='pablo@schibsted.cl',
                email_confirm='pablo@schibsted.cl',
                phone='99996666',
                passwd='123123123',
                passwd_ver='123123123',
                ai_create_account=False,
            )
        else:
            ad_insert = self.AdInsert(wd)
            ad_insert.go()
            ad_insert.insert_ad(
                subject='Libro 1',
                body='Este es el libro 1',
                category=8020,
                region=15,
                communes=331,
                add_location=True,
                address='Avenida San Pablo',
                address_number='1361',
                price=9966,
                private_ad=True,
                name='yolo',
                email='pablo@schibsted.cl',
                email_verification='pablo@schibsted.cl',
                phone='99996666',
                password='123123123',
                password_verification='123123123',
                create_account=False,
                accept_conditions=True)
            ad_insert.submit.click()


class AdInsertDuplicateAdiMobile(AdInsertDuplicateAdDesktop):

    AdInsert = AdInsertMobile
    SummaryPayment = SummaryPaymentMobile
    user_agent = yapo.utils.UserAgents.IOS
    is_desktop = False
    test_message = 'Tienes un aviso INACTIVO\nsimilar al que acabas de insertar.'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicate_ad_insert_account_logged_force_publish(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected as
        duplicated, The 'publicalo aqui' button redirects to result page """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('publish_f'))
        result.publish_f.click()
        result = AdInsertDuplicatedAdPublishResult(wd)
        result.wait.until_present('info_box')
        self.assertEqual(result.message.text,
                         'Tu aviso Adeptus Astartes Codex ser� revisado y si est� de acuerdo con las reglas de ' + \
                         'Yapo.cl, lo publicaremos en los pr�ximos minutos.')

    @tags('ad_insert', 'account', 'duplicate', 'draft')
    def test_duplicate_ad_insert_account_logged_force_publish_clean_draft(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected as
        duplicated, The 'publicalo aqui' button redirects to result page, Go to insert page, The draft is empty (clean
        draft button is not visible) """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('publish_f'))

        result.publish_f.click()
        result = AdInsertDuplicatedAdPublishResult(wd)
        result.wait.until_visible('info_box')

        self.assertEqual(result.message.text,
                         'Tu aviso Adeptus Astartes Codex ser� revisado y si est� de acuerdo con las reglas de ' + \
                         'Yapo.cl, lo publicaremos en los pr�ximos minutos.')

        page = AdInsertMobile(wd)
        page.go()
        self.assertFalse(page.clean_draft.is_displayed())

    @tags('ad_insert', 'account', 'duplicate', 'draft')
    def test_duplicate_ad_insert_account_logged_activate_clean_draft(self, wd):
        """ Duplicate ad - With logged user, Try to insert an ad with a previous deactived ad, The ad is detected
        as duplicated, The 'activar' button redirects to result page, Go to insert page, The draft is NOT empty
        (clean draft button is visible) """
        login_and_go_to_dashboard(
            wd, self.user['email'], self.user['passwd'], self.is_desktop)

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('activate_da'))

        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)
        result.wait.until_visible('message')

        self.assertEqual(
            result.message.text, 'Reviviendo Aviso\nTu aviso estar� publicado en los pr�ximos minutos.')

        page = AdInsertMobile(wd)
        page.go()
        self.assertTrue(page.clean_draft.is_displayed())

