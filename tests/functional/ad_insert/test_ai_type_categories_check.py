# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo
from yapo.pages.ad_insert import AdInsert

ALL = set(('rs', 'rh', 'rk', 'ru'))
SHKU = set(('rs', 'rh', 'rk', 'ru'))
SK = set(('rs', 'rk'))
NONE = set()


class TestAITypeCategories(yapo.SeleniumTest):

    categories = {
        '1220': NONE,
        '1240': NONE,
        '1260': NONE,
        '2020': SK,
        '2060': SK,
        '2040': SK,
        '2100': SK,
        '2080': SK,
        '5020': NONE,
        '5040': NONE,
        '5060': NONE,
        '4020': NONE,
        '9020': NONE,
        '4040': NONE,
        '6140': NONE,
        '6120': NONE,
        '6060': NONE,
        '6180': NONE,
        '6080': NONE,
        '6100': NONE,
        '6160': NONE,
        '4060': NONE,
        '3040': NONE,
        '3060': NONE,
        '3020': NONE,
        '3080': NONE,
        '7020': NONE,
        '7040': NONE,
        '7060': NONE,
        '7080': NONE,
        '8020': NONE
    }

    @tags('category', 'ad_insert')
    def test_ai_types_categories(self, wd):
        """test ad categories selling types"""
        wd.implicitly_wait(0.5)
        ai_page = AdInsert(wd)
        ai_page.go()
        for category, values in self.categories.items():

            ai_page.insert_ad(category = category)
            # Types that we need to be present per category
            for category_type in values:
                ai_page.wait.until_present(category_type, '{0} type is not present in category {1}'.format(category_type, category))
                attribute = getattr(ai_page, category_type)
                self.assertTrue(attribute.get_attribute('value') == category_type[1])
            # Types that we dont need to be present per category
            for category_type in ALL - values:
                ai_page.wait.until_not_present(category_type, '{0} type is present in category {1}'.format(category_type, category))
