# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.newai import DesktopAI
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.utils import Upselling
from yapo.steps import trans, insert_an_ad
from yapo import utils
import time
import yapo
import random


class AIComboUpsellingDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    combo_prices_by_cat_group = {
            'a' : {
                'standard': {'price': '$12.500'},
                'advanced': {'price':'$9.890'},
                'premium':  {'price':'$41.380', 'gallery':True}
            },
            'b' : {
                'standard': {'price':'$9.300'},
                'advanced': {'price':'$14.400'},
                'premium':  {'price':'$16.390', 'gallery':True}
            },
            'c' : {
                'standard': {'price':'$5.500'},
                'advanced': {'price':'$9.100', 'gallery':True},
                'premium':  {'price':'$11.200', 'gallery':True}
            }
    }

    #category_group = {
    #        'a':[1220,1240,1260],
    #        'b':[2020,2040,2060,2080,2120,7020,7060,7040],
    #        'c':[2100,7080,5020,5040,5060,5160,4020,4040,4060,4080,9020,9040,9060,6140,6020,6060,6180,6080,6100,6120,6160,3040,3060,3020,3080,8020]
    #}
    category_group = {
            'a':[1220],
            'b':[2020],
            'c':[2100]
    }

    AdInsert = AdInsertDesktop
    platform = desktop
    ftype = 1

    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0',
        "*.*.payment_settings.product.1.1.*.value": "price:950",
        "*.*.payment_settings.product.1.1011.*.value": "price:8000",
        "*.*.payment_settings.product.1.1012.*.value": "price:14400",
        "*.*.payment_settings.product.1.1013.*.value": "price:16390",
        "*.*.payment_settings.product.1.1021.*.value": "price:6900",
        "*.*.payment_settings.product.1.1022.*.value": "price:9890",
        "*.*.payment_settings.product.1.1023.*.value": "price:41380",
        "*.*.payment_settings.product.1.1031.*.value": "price:900",
        "*.*.payment_settings.product.1.1032.*.value": "price:4500",
        "*.*.payment_settings.product.1.1033.*.value": "price:6490",
        "*.*.payment.product.1021.name": "Estandar: Subir diario",
        "*.*.payment.product.1022.name": "Avanzado: Subir diario + Vitrina 7",
        "*.*.payment.product.1023.name": "Premium: Subir diario + Vitrina 7 + Etiqueta",
    }

    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_combo_check_prices_and_flow_with_images(self, wd):
        """ Upselling Combo: Check upselling combo prices, """

        login = self.platform.Login(wd)
        login.login('many@ads.cl', '123123123')
        ad_insert = self.AdInsert(wd)
        ad_insert.go()

        for group, categories in self.category_group.items():
            cat =random.choice(categories)
            ad_insert.choose_category(cat) #choice randomically one cat in group

            for combo in self.combo_prices_by_cat_group[group].keys():
                print("group, cat, combo ->'{}','{}','{}' ".format(group, cat, combo))
                if 'gallery' in self.combo_prices_by_cat_group[group][combo]:
                    print('gallery' in self.combo_prices_by_cat_group[group][combo])
                    print(ad_insert.combo_is_disabled(wd, combo))
                    ad_insert.wait.until(lambda wd: True == ad_insert.combo_is_disabled(wd, combo))
            ad_insert.upload_resource_image('image_order/01.png')

            for combo in self.combo_prices_by_cat_group[group].keys():
                #Check combo is enabled
                if 'gallery' in self.combo_prices_by_cat_group[group][combo]:
                    ad_insert.wait.until(lambda wd: True == (not ad_insert.combo_is_disabled(wd, combo)))
                #Check combo prices by cat group
                print("Comparnding: combo:{}".format(combo))
                print(ad_insert.get_combo_price(wd, combo))
                print(self.combo_prices_by_cat_group[group][combo]['price'])
                ad_insert.wait.until(lambda wd:  ad_insert.get_combo_price(wd, combo) == self.combo_prices_by_cat_group[group][combo]['price'])

            ad_insert.wait.until_present('images')
            self._delete_images(wd)

    user = 'prepaid5@blocket.se'
    passwd = '123123123'

    def _upselling_combo_option(self, wd, data):
        login = self.platform.Login(wd)
        login.login(self.user, self.passwd)
        if self.platform == mobile :
            insert_an_ad(wd, AdInsert=self.AdInsert, Submit=False,
                    category=data['category'],
                    images=['image_order/01.png'],
                    price='10000',
                    type='s',
                    logged=True)
        else:
            insert_an_ad(wd, AdInsert=self.AdInsert, Submit=False,
                    category=data['category'],
                    price='10000',
                    images=['image_order/01.png'],
                    logged=True)
        ad_insert = self.AdInsert(wd)
        ad_insert.wait.until(lambda wd:  ad_insert.combo_is_disabled(wd, data['combo']) == False)

        combo_prod = 'combo_upselling_{}'.format(data['combo'])
        ad_insert.wait.until_visible(combo_prod)

        getattr(ad_insert, combo_prod).click()
        ad_insert.is_element_present('combo_upselling_box_expanded')
        ad_insert.submit.click()
        success_url = "&prod={}&ftype={}&from=newad".format(data['combo_code'], self.ftype)
        ad_insert.wait.until(lambda wd: success_url in wd.current_url)



    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_premium(self, wd):
        """ Upselling Combo: Check redirect to summary when user select premium combo"""
        data = {
            'category': random.choice(self.category_group['a']), #choice randomically one cat in group
            'combo': 'premium',
            'combo_code': '1023'
        }
        self._upselling_combo_option(wd, data)

    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_standard(self, wd):
        """ Upselling Combo: Check redirect to summary when user select standard combo"""
        data = {
            'category': random.choice(self.category_group['b']), #choice randomically one cat in group
            'combo': 'standard',
            'combo_code': '1011'
        }
        self._upselling_combo_option(wd, data)

    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_advanced(self, wd):
        """ Upselling Combo: Check redirect to summary when user select advanced combo"""
        data = {
            'category': random.choice(self.category_group['c']), #choice randomically one cat in group
            'combo': 'advanced',
            'combo_code': '1042'
        }
        self._upselling_combo_option(wd, data)

    def _delete_images(self, browser):
        browser.find_element_by_css_selector('.ai_image .bt_delete').click()
        ad_insert = self.AdInsert(browser)
        self.assertEquals(len(ad_insert.images), 0)

class AIComboUpsellingMobile(AIComboUpsellingDesktop):

    user_agent = utils.UserAgents.IOS
    platform = mobile
    AdInsert = AdInsertMobile
    ftype = 2

    def _delete_images(self, browser):
        ad_insert = self.AdInsert(browser)
        ad_insert.delete_images()
