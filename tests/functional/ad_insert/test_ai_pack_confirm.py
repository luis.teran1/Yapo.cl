# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages.ad_insert import AdInsert, PackConfirm
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad

class AIPackConfirm(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']

    data_2020 = {
        'subject':'Auto',
        'region':'15',
        'communes': '331',
        'category':'2020',
        'body':'yoLo',
        'price':'3500000',
        'password':'123123123',
        'password_verification':'123123123',
        'phone':'999966666',
        'accept_conditions':True,
        'brand':'31',
        'model':'44',
        'version':'1',
        'private_ad':'1',
        'create_account':'0',
        'regdate':'2010',
        'gearbox':'1',
        'fuel':'1',
        'cartype':'1',
        'mileage':'15000',
        'plates':'ABCD10'
    }

    bconfs = {'*.*.pack2if.newad.enabled': '1'}

    def setUp(self):
        yapo.utils.enable_local_hit_xiti()
        yapo.utils.xiti_clear_logs()

    @tags('ad_insert', 'desktop', 'account', 'packs', 'xiti')
    def test_ai_2020_pack_confirm_without_account(self, wd):
        """ Pack: Check AI category 2020 when user has not account
            XitiPage when load this case.
            Xiti click to create account.
        """
        
        self.data_2020.update(email='prosincuenta@yapo.cl', email_verification='prosincuenta@yapo.cl', name='prosincuenta')
        self._insert_ad(wd, self.data_2020)
        page = PackConfirm(wd)
        self.assertTrue(page.pack_message_noaccount.is_displayed())
        logs = yapo.utils.get_page_xiti_log('ad_insertion::confirmation::confirmation_2000_pack_auto::ad_confirmation_inactive_noaccount', [1,2,0])
        self.assertEquals(logs['s2'], '10')
        page.pack_message_noaccount_link_create_account.click()
        xiti_click = yapo.utils.get_click_xiti_log('create_account::create_account::create_account::ad_confirmation_inactive_noaccount')
        self.assertEquals(xiti_click['clic'], 'N')

    @tags('ad_insert', 'desktop', 'account', 'packs', 'xiti')
    def test_ai_no_pack_confirm(self, wd):
        """ Pack: Check AI when user is not pro """

        self.data_2020.update(email='emailnoexiste@yapo.cl', email_verification='emailnoexiste@yapo.cl', name='emailnoexiste')
        self._insert_ad(wd, self.data_2020)
        page = PackConfirm(wd)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_ok)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_nopack)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_noaccount)
        self.assertRaises(exceptions.NoSuchElementException, lambda: page.pack_message_noquota)

    def _insert_ad(self, wd, data):
        insert_an_ad(wd, AdInsert=DesktopAI, **data)
