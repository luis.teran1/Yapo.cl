# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.control_panel import ControlPanel
import yapo
import json
import time
import os
import collections

class PriProAIMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        "*.controlpanel.modules.adqueue.auto.pack.timeout": "1000",
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    user_agent = yapo.utils.UserAgents.ANDROID

    @tags('mobile', 'ad_insert', 'categories', 'pri_pro')
    def test_mobile_ai_by_category(self, wd):
        """ Honor category dependent pri/pro on mobile ad insert """

        # The "problem" is that when a user is NOT pro for a category, MAI set's the ad as PRO
        # Even if this is decided as the current behaviour (by team's knowledge), I think we can
        # REALLY do better. Thus, I'm leaving this "broken" until we can talk to Max/Juan and
        # Decide what the actual behaviour should be. If the current behaviour is the desired one,
        # I, Miguel, will change the test myself to reflect that.
    
        ok = {'true', 'True', 'y', '1', 'yes'}
        test_data_file = os.path.join(yapo.conf.RESOURCES_DIR, 'data_pri_pro_mobile.json')
        with open(test_data_file) as f:
            self.data = json.load(f)

        for _, cat_data in self.data.items():
            subject = cat_data['subject']
            email = cat_data['email']
            should_be_pro = cat_data.pop('should_be_pro') in ok
            should_be_logged = cat_data.pop('should_be_logged') in ok

            login_page = mobile.Login(wd)
            if should_be_logged:
                login_page.go()
                cat_data.pop('email')
                passwd = cat_data.pop('password')
                cat_data.pop('password_verification')
                login_page.login(email, passwd)

            mai = mobile.NewAdInsert(wd)
            mai.go()
            mai.insert_ad(**cat_data)
            mai.submit.click()
            success_page = mobile.AdInsertedSuccessfuly(wd)
            time.sleep(3)
            self.assertIn('Tu aviso {0} ser� revisado'.format(subject).lower(), success_page.text_success_message.text.lower())
            login_page.logout()

            cp = ControlPanel(wd)
            cp.go()
            cp.login("dany", "dany")
            new_subject = subject.strip().lower()
            new_subject = new_subject[0].upper() + new_subject[1:]
            cp.search_for_ad(email, new_subject, None, "Unreviewed")

            self.assertEqual(cp.radio_review_pri_pro[0].is_selected(), not should_be_pro)
            self.assertEqual(cp.radio_review_pri_pro[1].is_selected(), should_be_pro)

            cp.close_window_and_back()
            cp.logout()
