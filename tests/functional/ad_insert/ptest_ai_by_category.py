# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from nose.plugins.skip import SkipTest
from nose.plugins.multiprocess import MultiProcess
import time
import yapo


class AdInsertTest(yapo.SeleniumTest):
    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    email = 'yolo@yapo.cl'
    price = '999666'
    name = 'yoloberto'
    passwd = '99996666'
    private_ad = True
    add_location = False
    body = 'pericolos palotes'
    create_account = False
    accept_conditions = True
    _multiprocess_shared_ = True

    def _insert_ad(self, wd, **kwargs):
        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(**kwargs)
        page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    # Vehiculos
    def ptest_ai_2020(self, wd):
        """ AI: Autos, camionetas y 4x4"""
        self._insert_ad(wd,
                        subject='Test 2020',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=2020,
                        region=3,
                        communes=12,
                        brand=18,
                        model=19,
                        version=1,
                        regdate=1990,
                        cartype=1,
                        gearbox=1,
                        fuel=5,
                        mileage=100,
                        type='s',
                        )

    def ptest_ai_2040(self, wd):
        """ AI: Buses, camiones y furgones"""
        self._insert_ad(wd,
                        subject='Test 2040',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=2040,
                        region=3,
                        communes=12,
                        regdate=1975,
                        gearbox=1,
                        fuel=5,
                        mileage=50000,
                        type='s',
                        )

    def ptest_ai_2060(self, wd):
        """ AI: Motos"""
        self._insert_ad(wd,
                        subject='Test 2060',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=2060,
                        region=3,
                        communes=12,
                        regdate=1975,
                        mileage=50000,
                        cubiccms=2,
                        type='s',
                        )

    def ptest_ai_5020(self, wd):
        """ AI: Muebles y art�culos del hogar"""
        self._insert_ad(wd,
                        subject='Test 5020',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=5020,
                        region=3,
                        communes=12,
                        type='s',
                        )

    def ptest_ai_4020(self, wd):
        """ AI: Moda y vestuario"""
        self._insert_ad(wd,
                        subject='Test 4020',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=4020,
                        region=3,
                        communes=12,
                        condition=1,
                        gender=2,
                        clothing_size=2,
                        type='s',
                        )

    def ptest_ai_9020(self, wd):
        """ AI: Embarazadas, beb�s y ni�os"""
        self._insert_ad(wd,
                        subject='Test 9020',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=9020,
                        region=3,
                        communes=12,
                        condition=1,
                        type='s',
                        )

    def ptest_ai_4040(self, wd):
        """ AI: Bolsos, bisuter�a y accesorios"""
        self._insert_ad(wd,
                        subject='Test 4040',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=4040,
                        region=3,
                        communes=12,
                        condition=1,
                        type='s',
                        )

    def ptest_ai_4080(self, wd):
        """ AI: Calzado """
        self._insert_ad(wd,
                        subject='Test 4080',
                        body=self.body,
                        add_location=self.add_location,
                        price=self.price,
                        private_ad=self.private_ad,
                        name=self.name,
                        email=self.email,
                        email_verification=self.email,
                        phone=self.passwd,
                        password=self.passwd,
                        password_verification=self.passwd,
                        create_account=self.create_account,
                        accept_conditions=self.accept_conditions,
                        category=4080,
                        region=3,
                        communes=12,
                        condition=1,
                        footwear_gender=3,
                        footwear_size=103,
                        footwear_type=['1','2'],
                        )

class AdInsertTestM(AdInsertTest):

    AdInsert = AdInsertMobile
    user_agent = utils.UserAgents.IOS
