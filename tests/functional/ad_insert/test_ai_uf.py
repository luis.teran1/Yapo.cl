# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertPreview
import yapo

class TestUF(yapo.SeleniumTest):

    snaps = ['android']

    def _uf_test_case(self, wd, cat):
        adinsert = AdInsert(wd)
        adinsert.go()
        adinsert.insert_ad(category=cat)
        adinsert.ordered_fill_form([
            ('price', 30000000),
            ('subject', 'dummy'),
        ])
        return adinsert

    @tags('desktop', 'advertiser_name')
    def test_uf_price_on_1220(self, wd):
        """ UF price should be displayed on Vendo category """
        adinsert = self._uf_test_case(wd, '1220')
        self.assertEquals(adinsert.pesos2uf.text, '(UF 1.348,58)')

    def test_uf_price_on_1240(self, wd):
        """ UF price should be displayed on Arriendo category """
        adinsert = self._uf_test_case(wd, '1240')
        self.assertEquals(adinsert.pesos2uf.text, '(UF 1.348,58)')

    def test_uf_price_on_1260(self, wd):
        """ UF price should be displayed on Arriendo de temporada category """
        adinsert = self._uf_test_case(wd, '1260')
        self.assertEquals(adinsert.pesos2uf.text, '(UF 1.348,58)')

    def test_uf_price_on_2040(self, wd):
        """ UF price should not be displayed on Buses, camiones y furgones category """
        adinsert = self._uf_test_case(wd, '2040')
        self.assertNotPresent(adinsert, 'pesos2uf')
