# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, AdView as AdViewMobile
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult, AdInsertPreview
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.desktop import Login
from yapo.pages.control_panel import ControlPanel
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo import utils
from yapo.utils import Categories
import yapo

class TestJobsAdParams(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = DesktopAI
    AdView = AdViewDesktop
    bconfs={
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    @tags('ad_insert', 'controlpanel')
    def test_happy_path_7020(self, wd):
        data = {
            'category': '7020',
            'working_day': '2',
            'contract_type': '1',
            'jc': ['20', '24'],
            'phone': '12312312',
            'subject': 'New ad params',
            'email': 'prepaid5@blocket.se',
            'email_confirm': 'prepaid5@blocket.se',
            'passwd': 123123123,
        }

        result = insert_an_ad(wd, AdInsert=self.AdInsert, **data)
        self.assertTrue(result.was('success'))

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(data['subject'])
        wd.find_element_by_link_text(data['subject']).click()
        wd.switch_to_window(wd.window_handles[1])

        for adparam in ['contract_type', 'working_day']:
            self.assertEqual(
                sorted([el.get_attribute('data-value') for el in getattr(cp, adparam)]),
                sorted([data[adparam]])
            )

        cp.review_ad('accept')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])

        cp.search_for_ad_by_subject(data['subject'])
        list_id = cp.list_id[0].text
        print(('list_id', list_id))

        utils.rebuild_index()

        expected = [
            ['Categor�a', 'Ofertas de empleo'],
            ['Categor�a de trabajo', 'Profesional / T�cnico\nOtros'],
            ['Tipo de jornada', 'Media jornada / Part time'],
            ['Tipo de contrato', 'Contrato Indefinido'],
            ['C�digo', list_id]
        ]
        if self.AdView == AdViewDesktop:
            expected = expected[1:]

        adview = self.AdView(wd)
        adview.go(list_id)
        print(adview.get_adparams())
        self.assertEqual(adview.get_adparams(), expected)



class TestJobsAdParamsMobile(TestJobsAdParams):

    AdInsert = AdInsertMobile
    AdView = AdViewMobile
    user_agent = utils.UserAgents.IOS
