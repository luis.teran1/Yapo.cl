# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop_list import AdViewDesktop
from yapo.steps import delete_ad, insert_and_publish_ad
import yapo
import urllib, hashlib

class AdInsertImages(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('ad_insert', 'images')
    def test_upload_images(self, wd):
        """ Testing that the images are rotated when these have exif data """
        ai = AdInsert(wd)
        ai.go()
        data = [
            (1, ['e53b244144d6669d7cb019fc825fbfb1', 'b97b53de18f921554719d54b426b0a48'], 'autorotation/img_000.jpg'),
            (2, ['0661f9c24d2ab26ef868a6b585ad9e70', '5530b9a42c9a556e57aee994e5a61a77'], 'autorotation/img_090.jpg'),
            (3, ['de82844a134d6d532222fe3d46d1becf', 'efbcc1a416ed05706d0b997129571904'], 'autorotation/img_180.jpg'),
            (4, ['8bb9cb3f6a408e4759009a3e21208246', '3bd8e1f6c4dba92efdf62c588270b444'], 'autorotation/img_270.jpg'),
        ]
        for index, expected_hash, image_path in data:
            ai.upload_resource_image(image_path)
            ai.wait.until_not_present("img_loader")
            ai.wait.until_elements_equals(wd, 'img[alt="image"]', index)
            image = ai.get_image_by_index(index)
            self._validate_image(image.get_attribute('src'), expected_hash)

    def test_load_image(self, wd):
        """ load an image with the alpha channel with issues
            From an e-mail with subject "FT 463":
            El problema se generaba bajo condiciones muy particulares:
            - La imagen debia ser PNG con transparencia
            - La imagen era creada con Photoshop
            - El canal alpha de la imagen tenia algo raro :D (in some cases)
            - El tama�o de la imagen era menor o igual al maximo permitido

            Al no redimensionar la imagen, se copia directamente el buffer del png
            sobre sobre una imagen nueva la cual es guardada como JPG, lo cual
            genera que la transparencia tenga "ruido"

            La soluci�n agrega un fondo blanco a la imagen, suprimiendo la
            transparencia, antes de copiarla a JPG.
        """
        ad_insert = AdInsert(wd)
        ad_insert.go()
        ad_insert.upload_resource_image("PngAlpha.png")
        image = ad_insert.get_image_by_index(1)
        # gets the url and replaces the path and port to the dav server and full size image
        url = image.get_attribute('src').replace('98/thumbs', '02/images')
        expected_md5 = ['5cba22ac6b526a58c241788297e5f436']
        self._validate_image(url, expected_md5)

    def _validate_image(self, url, expected_md5):
        f = urllib.request.urlopen(url)
        image_content = f.read()
        m = hashlib.md5()
        m.update(image_content)
        self.assertIn(m.hexdigest(), expected_md5)


    def test_upload_evil_image(self, wd):
        """ Test uploading an image with exif data that crash apache """

        insert_and_publish_ad(wd, AdInsert=AdInsert,
            subject='Evil upload image test',
            images=[
                'image.bad_exif.jpg',
                'image.image_without_yapo_watermark.jpg',
                'image.image_with_yapo_watermark.jpg'
            ]
        )
        ad_view = AdViewDesktop(wd)
        ad_view.go('8000085')
        self.assertEquals(3, len(ad_view.images))
