# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from selenium.webdriver.common.keys import Keys
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult, AdInsertSuccess, AdInsertDuplicatedAd, AdInsertDuplicatedAdActiveResult, AdInsertDuplicatedAdPublishResult, AdInsertPreview, PackConfirm
from yapo.pages.desktop import Login, DashboardMyAds, SummaryPayment as SummaryPaymentDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, SummaryPayment as SummaryPaymentMobile
from yapo.steps.newai import insert_ad
from yapo.pages.newai import DesktopAI
from yapo.steps import trans
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.utils import Categories
import time
import unittest
import yapo


class AdInsertDuplicateDisabledDesktop(yapo.SeleniumTest):
    """
        This testsuite going to test this cases:
        - User pack logged disable ad and publish the same ad.
            - Check the button activar enable the old ad
        - User pack logged disable ad and publish the same ad.
            - Check the link publicar show success but the ad was moved to duplicated queue
        - User pack try to publish an ad equals to disabled ad, from other account having the same UID
            - Check the password is required and use one slot from other account
        - User pack logged without slots publish an ad like one disabled
            - Check the error msg and the button comprar pack should redirect to dashboard-pack-something
    """

    bconfs = {'*.*.delete2bump.disabled.duplicate.enable': '1'}

    AdInsert = AdInsertDesktop

    user = {'name': 'Cuenta Pro con Pack',
            'email': 'cuentaproconpack@yapo.cl',
            'passwd': '123123123'}

    user_sp = {'name': 'Cuenta Pro sin Pack',
            'email': 'cuentaprosinpack@yapo.cl',
            'passwd': '123123123'}

    test_message = 'Tienes un aviso INACTIVO similar al que acabas de insertar.'
    force_message = 'Tu aviso "Audi A4 2011" ser� revisado y si est� de acuerdo con nuestras reglas, se publicar� y quedar� activo y visible en el listado de avisos.'
    
    adinfo_default = {
        "subject": "Audi A4 2011",
        "body": "aviso para Luis",
        "category": "2020",
        "price": "11399000"
        }

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1', 'diff_pack_cars1-disable-ads'])
        yapo.utils.rebuild_asearch()

    def _insert_ad_logged(self, wd, adinfo=None):
        if adinfo is None:
            adinfo = self.adinfo_default
        ad_insert = self.AdInsert(wd)
        ad_insert.go()
        if self.is_desktop():
            insert_ad(
                DesktopAI(wd),
                subject=adinfo.get("subject"),
                body=adinfo.get("body"),
                brand=8,
                model=6,
                version=12,
                regdate=2011,
                mileage=100000,
                region=15,
                communes=331,
                category=adinfo.get("category"),
                price=adinfo.get("price"),
                logged=True
            )
        else:
            ad_insert.insert_ad(
                subject=adinfo.get("subject"),
                body=adinfo.get("body"),
                brand=8,
                model=6,
                version=12,
                regdate=2011,
                mileage=100000,
                region=15,
                communes=331,
                category=adinfo.get("category"),
                price=adinfo.get("price"),
                type='s',
                phone_type_mobile=True,
                phone=65962077,
                accept_conditions=True)
            ad_insert.submit.click()

    def is_desktop(self):
        return self.AdInsert == AdInsertDesktop

    @tags('ad_insert', 'account', 'minimal', 'duplicate')
    def test_duplicated_activate_wo_slot(self, wd):
        login_and_go_to_dashboard(wd, "cuentaprosincupos@yapo.cl", self.user_sp['passwd'], self.is_desktop())
        adinfo = self.adinfo_default.copy()
        adinfo["subject"] = "Chery Tiggo 2007"
        adinfo["body"] = "aviso 1"
        adinfo["price"] = "3200000"

        self._insert_ad_logged(wd, adinfo)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertEqual(result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text,
            "Puedes Activar este aviso y nuevamente aparecer� publicado."
        )
        self.assertFalse(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present('publish_f_2' if self.is_desktop() else 'publish_f'))

        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)

        if self.is_desktop():
            result.wait.until_present('modal_failure')
            self.assertEqual(result.modal_failure.text, 'Error al habilitar aviso: No hay cupos disponibles')
        else:
            result.wait.until_visible('failed')
            self.assertEqual(result.failed.text, 'Error al habilitar aviso: No hay cupos disponibles')

        self.assertTrue(result.is_element_present('see_das'))
        self.assertTrue(result.is_element_present('my_account'))
        
        link = result.buy_pack if self.is_desktop() else result.see_das
        self.assertEquals (link.text, "Comprar pack");

    @tags('ad_insert', 'account', 'minimal', 'duplicate')
    def test_duplicated_activate_old(self, wd):
        login_and_go_to_dashboard(wd, self.user['email'], self.user['passwd'], self.is_desktop())

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertEqual(result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text,
            "Puedes Activar este aviso y nuevamente aparecer� publicado."
        )
        self.assertFalse(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present('publish_f_2' if self.is_desktop() else 'publish_f'))

        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)

        if self.is_desktop():
            result.wait.until_present('modal_message')
            self.assertEqual(result.modal_message.text, 'Tu aviso "Audi A4 2011 " estar� activo en los pr�ximos minutos.')
        else:
            result.wait.until_visible('message')
            self.assertEqual(result.message.text, 'Reviviendo Aviso\nTu aviso estar� publicado en los pr�ximos minutos.')

        self.assertTrue(result.is_element_present('see_das'))
        self.assertTrue(result.is_element_present('my_account'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicated_force_publish(self, wd):
        login_and_go_to_dashboard(wd, self.user['email'], self.user['passwd'], self.is_desktop())

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')

        if self.is_desktop():
            self.assertTrue(result.is_element_present('publish_f_2'))
            result.publish_f_2.click()
        else:
            self.assertTrue(result.is_element_present('publish_f'))
            result.publish_f.click()

        page = PackConfirm(wd)
        self.assertTrue(page.pack_message_ok.is_displayed())
        self.assertIn(
            self.force_message,
            page.pack_message_ok.text)
        result = AdInsertResult(wd)
        self.assertFalse(result.is_element_present('products_box'))

    @tags('ad_insert', 'account', 'duplicate')
    def test_duplicated_by_uid(self, wd):
        login_and_go_to_dashboard(wd, self.user_sp['email'], self.user_sp['passwd'], self.is_desktop())

        self._insert_ad_logged(wd)
        result = AdInsertDuplicatedAd(wd)
        result.wait.until_visible('message')
        self.assertEqual(result.message.text, self.test_message)
        self.assertEqual(
            result.notice.text,
            "Puedes Activar este aviso y nuevamente aparecer� publicado."
        )
        self.assertFalse(result.is_element_present('activate_and_bump'))
        self.assertTrue(result.is_element_present('activate_da'))
        self.assertTrue(result.is_element_present('publish_f_2' if self.is_desktop() else 'publish_f'))


        result.wait.until_visible('message')
        self.assertTrue(result.is_element_present('password_input'))
        result.password_input.send_keys(Keys.ENTER)

        result.wait.until_present('password_err')
        result.wait.until(lambda x: result.password_err.text != '')

        self.assertEqual(result.password_err.text, 'Debes introducir tu contrase�a')
        result.confirm(self.user['passwd'])

        result.activate_da.click()
        result = AdInsertDuplicatedAdActiveResult(wd)

        if self.is_desktop():
            result.wait.until_present('modal_message')
            self.assertEqual(result.modal_message.text, 'Tu aviso "Audi A4 2011 " estar� activo en los pr�ximos minutos.')
        else:
            result.wait.until_visible('message')
            self.assertEqual(result.message.text, 'Reviviendo Aviso\nTu aviso estar� publicado en los pr�ximos minutos.')

        self.assertTrue(result.is_element_present('see_das'))
        self.assertTrue(result.is_element_present('my_account'))


class AdInsertDuplicateDisabledMobile(AdInsertDuplicateDisabledDesktop):

    AdInsert = AdInsertMobile
    user_agent = yapo.utils.UserAgents.IOS
    test_message = 'Tienes un aviso INACTIVO\nsimilar al que acabas de insertar.'
    force_message = '�Aviso activo!\nAl publicarse, tu aviso estar� activo autom�ticamente en el listado de yapo.cl'
