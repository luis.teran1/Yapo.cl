# -*- coding: latin-1 -*-
import yapo
from selenium.webdriver.support.select import Select
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertUpselling, AdInsertResult
from yapo.utils import Upselling
from yapo.pages.desktop import SummaryPayment, PaymentVerify, HeaderElements
from yapo.pages.simulator import Paysim
from selenium.common import exceptions
from yapo.pages.desktop import Login
import time
        
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI

class AIUpsellingAccount(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pro_account']
    bconfs = {
        '*.*.payment.product_specific.upselling_daily_bump.first_execution_delay': '1',
        '*.*.payment.product_specific.upselling_weekly_bump.first_execution_delay': '1',
        '*.*.payment.product_specific.weekly_bump.interval_seconds':'1',
        '*.*.payment.product_specific.daily_bump.interval_seconds':'1'
    }

    ad_data = {
        'subject':'Auto',
        'region':'15',
        'communes':'322',
        'category':'2020',
        'body':'yoLo',
        'price':'3500000',
        'password':'123123123',
        'password_verification':'123123123',
        'phone':'999966666',
        'accept_conditions':True,
        'brand':'31',
        'model':'44',
        'version':'1',
        'private_ad':'1',
        'create_account':False,
        'regdate':'2010',
        'gearbox':'1',
        'fuel':'1',
        'cartype':'1',
        'mileage':'15000'
    }

    @tags('ad_insert', 'desktop', 'upselling')
    def test_ai_prods_not_present_pro_account(self, webdriver):
        """ AI products aren't presents with pro account """
        ad_data = self.ad_data.copy()
        ad_data.update(email='prepaid3@blocket.se', email_verification='prepaid3@blocket.se', name='cuenta pro')
        self._insert_ad(webdriver, ad_data)

        ai_upselling = AdInsertUpselling(webdriver)
        ai_upselling.wait.until_not_present('upselling_products')

    def _insert_and_buy_upselling_product(self, webdriver, ad_data, product, expected_values, success_values, image=None):

        self._insert_ad(webdriver, ad_data, image)
        ai_upselling = AdInsertUpselling(webdriver)
        if product == Upselling.DAILY_BUMP:
            ai_upselling.daily_button.click()
        elif product == Upselling.WEEKLY_BUMP:
            ai_upselling.weekly_button.click()
        elif product == Upselling.GALLERY:
            ai_upselling.gallery_button.click()
        else:
            raise Exception("Product type {} is not Upselling".format(product))
        summary_page = SummaryPayment(webdriver)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)
        summary_page.press_link_to_pay_products()
        paysim = Paysim(webdriver)
        paysim.pay_accept()
        payment_verify = PaymentVerify(webdriver)
        table_values = payment_verify.get_table_data('sale_table')
        self.assertListEqual(table_values, success_values)

    def _check_product_in_mail(self, webdriver, position, subject, product):
        rows = webdriver.find_elements_by_css_selector('#products-table tbody tr')
        cells = rows[position].find_elements_by_css_selector('td')
        self.assertEqual(subject, cells[0].text, "the product {0} quantity's does not match '{1}'!='{2}'".format(position, subject, cells[0].text))
        self.assertEqual(product, cells[1].text, "the product {0} product's name does not match '{1}'!='{2}'".format(position, product, cells[1].text))

    def product_is_disabled(self, webdriver, product):
        ai_upselling = AdInsertUpselling(webdriver)
        if product == Upselling.WEEKLY_BUMP:
            return self.is_element_disabled(ai_upselling.upselling_products, selector='#weekly_box.is-bw')
        elif product == Upselling.DAILY_BUMP:
            return self.is_element_disabled(ai_upselling.upselling_products, selector='#daily_box.is-bw')
        elif product == Upselling.GALLERY:
            return self.is_element_disabled(ai_upselling.upselling_products, selector='#gallery_box.is-bw')

    def is_element_disabled(self, page, selector=None):
        try:
            page.find_element_by_css_selector(selector)
        except exceptions.NoSuchElementException:
            return False
        else:
            return True

    def _insert_ad(self, webdriver, data, image=None):
        ad_insert = AdInsert(webdriver)
        insert_an_ad(webdriver, AdInsert=DesktopAI, Submit=False, **data)
        if image is not None:
            ad_insert.upload_resource_image(image)
            time.sleep(2)
        ad_insert.submit.click()




