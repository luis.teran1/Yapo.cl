from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.utils import Categories
from yapo import SeleniumTest


class AdInsertMaxCategorySelection(SeleniumTest):
    AdInsert = AdInsertDesktop

    @tags('ad_insert', 'categories')
    def test_job_max_category_selection(self, wd):
        """Check sub category selection limit for job category"""

        category_list = {
            str(Categories.OFERTAS_DE_EMPLEO): {
                'disabled': 7,
                'jcs': ['17', '18']
            },
            str(Categories.BUSCO_EMPLEO): {
                'disabled': 0,
                'jcs': ['19']
            },
        }

        page = self.AdInsert(wd)

        for cat in category_list:
            category = category_list.get(cat)

            page.go()
            page.insert_ad(
                category=cat,
                jc=category.get('jcs')
            )

            disabled_count = sum(1 for j in page.jcs if j.get_attribute('disabled'))
            print('disabled: {}, expected: {}'.format(disabled_count, category.get('disabled')))
            self.assertEqual(disabled_count, category.get('disabled'))

