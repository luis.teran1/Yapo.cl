# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad
from yapo.steps import trans
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
import yapo


class TestAdInsertUF(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop

    @staticmethod
    def format_price(price):
        if not price:
            return ''
        price = str(price).replace('.','')
        parts = str(price).split(',')
        decimal = ''
        if len(parts) > 1:
            decimal = ','+parts[1]
        return "{:,}".format(int(parts[0])).replace(',', '.') + decimal

    @tags('ad_insert', 'uf')
    def test_uf_conversion(self, wd):
        """Check the uf/peso transformation in price for inmo ads"""
        page = self.AdInsert(wd)
        page.go()
        page.ordered_fill_form([
            ('category', 1220),
            ('currency_peso', True),
            ('price', 42000000),
            ('subject', 'hack to move focus'),
        ])
        if self.AdInsert == AdInsertDesktop:
            self.assertEqual(page.pesos2uf.text, "(UF 1.888,01)")
        self.assertEqual(page.val('price'), self.format_price("42000000"))

        page.ordered_fill_form([
            ('currency_uf', True),
            ('uf_price', 1200),
            ('subject', 'hack to move focus'),
        ])
        if self.AdInsert == AdInsertDesktop:
            self.assertEqual(page.pesos2uf.text, "($ 26.694.624)")
        self.assertEqual(page.val('uf_price'), self.format_price("1.200"))

    @tags('ad_insert', 'uf')
    def test_uf_limits(self,wd):
        """Ensure input validations for UF on Ad Insert"""
        page = self.AdInsert(wd)
        page.go()
        page.ordered_fill_form([
            ('category', 1220),
            ('currency_uf', True),
        ])

        data = [
            ('10000', '10.000'),
            ('100000', '100.000'),
            ('1000000', '100.000'),
            (',', ''),
            (',1', '1'),
            ('pepsi', ''),
            ('12pepsi34', '1.234'),
            ('1,', '1'),
            ('1,2', '1,2'),
            ('1,23', '1,23'),
            ('1,234', '1,23'),
            ('1,2,3,4', '1,23'),
            ('12,,34', '12,34'),
        ]

        for input, expected in data:
            page.ordered_fill_form([
                ('uf_price', input),
                ('subject', 'hack to move focus'),
            ])
            print (input + ' - ' + expected)
            self.assertEqual(page.val('uf_price'), self.format_price(expected))

    @tags('ad_insert', 'uf')
    def test_insert_with_uf(self, wd):
        """Insert an ad with UF price"""
        email = 'yo@lo.cl'
        subject = 'UF priced ad'
        price = '9200,33'
        result = insert_an_ad(wd, self.AdInsert,
            category = 1220,
            subject = subject,
            email = email,
            email_confirm = email,
            currency = 'uf',
            price = price,
        )
        self.assertTrue(result.was('success'))

        ad = trans.get_ad(email, subject)
        self.assertEqual(ad['ad']['price'], str(int(float(price.replace(",","."))*100)))
        self.assertEqual(ad['params']['currency'], 'uf')


class TestAdInsertUFMobile(TestAdInsertUF):

    AdInsert = AdInsertMobile

    format_price = str
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'uf')
    def test_uf_limits(self,wd):
        """Ensure input validations for UF on Ad Insert"""
        page = self.AdInsert(wd)
        page.go()
        page.ordered_fill_form([
            ('category', 1220),
            ('currency_uf', True),
        ])

        data = [
            ('10000', '10.000'),
            ('1,', '1'),
            ('1,2', '1,2'),
            ('1,23', '1,23'),
        ]

        for input, expected in data:
            page.ordered_fill_form([
                ('uf_price', input),
                ('subject', 'hack to move focus'),
            ])
            self.assertEqual(page.val('uf_price'), self.format_price(expected))

        error_data = [
            ('314001', page.UF_PRICE_TOO_HIGH),
            ('1,234', page.UF_TOO_MANY_DECIMALS),
            ('1,2,3,4', page.INVALID_CHARACTERS),
            ('12,,34', page.INVALID_CHARACTERS),
        ]

        for input, expected_error in error_data:
            print("{} => {}".format(input, expected_error))
            page.ordered_fill_form([
                ('uf_price', input),
                ('subject', 'hack to move focus'),
            ])
            self.assertEqual(page.get_error_for('uf_price').text, expected_error)
