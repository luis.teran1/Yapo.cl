# -*- coding: latin-1 -*-

from yapo import SeleniumTest
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.desktop import AdEdit as AdEditDesktop, Login
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, Login as LoginMobile, AdView
from yapo.utils import Categories
from yapo import utils, steps
from yapo.steps import trans


class ForcedLoginDesktop(SeleniumTest):
    snaps = ['accounts', 'diff_pro_account', 'diff_jobs_ads']
    AdInsert = AdInsertDesktop
    AdEdit = AdEditDesktop
    bconfs = {
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '0',
        '*.*.forced_login.modal.newad.{}.title'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Jobs',
        '*.*.forced_login.modal.newad.{}.subtitle'.format(Categories.OFERTAS_DE_EMPLEO): 'Login subtitle',
        '*.*.forced_login.modal.newad.{}.footer'.format(Categories.OFERTAS_DE_EMPLEO): '',
        '*.*.forced_login.modal.newad.{}.css_modifier'.format(Categories.OFERTAS_DE_EMPLEO): '__jobs',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '0',
        '*.*.forced_login.modal.editad.{}.title'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Jobs',
        '*.*.forced_login.modal.editad.{}.subtitle'.format(Categories.OFERTAS_DE_EMPLEO): 'Login subtitle Jobs',
        '*.*.forced_login.modal.editad.{}.footer'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Footer description Jobs EDITAD',
        '*.*.forced_login.modal.editad.{}.css_modifier'.format(Categories.OFERTAS_DE_EMPLEO): '__jobs',

        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.ARRIENDO_TEMPORADA): '0',
        '*.*.forced_login.modal.newad.{}.title'.format(Categories.ARRIENDO_TEMPORADA): 'Login Inmo',
        '*.*.forced_login.modal.newad.{}.subtitle'.format(Categories.ARRIENDO_TEMPORADA): 'Login subtitle Inmo',
        '*.*.forced_login.modal.newad.{}.footer'.format(Categories.ARRIENDO_TEMPORADA): 'Login Footer description Inmo NEWAD',
        '*.*.forced_login.modal.newad.{}.css_modifier'.format(Categories.ARRIENDO_TEMPORADA): '__inmo',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.ARRIENDO_TEMPORADA): '0',
        '*.*.forced_login.modal.editad.{}.title'.format(Categories.ARRIENDO_TEMPORADA): 'Login Inmo',
        '*.*.forced_login.modal.editad.{}.subtitle'.format(Categories.ARRIENDO_TEMPORADA): 'Login subtitle Inmo',
        '*.*.forced_login.modal.editad.{}.footer'.format(Categories.ARRIENDO_TEMPORADA): '',
        '*.*.forced_login.modal.editad.{}.css_modifier'.format(Categories.ARRIENDO_TEMPORADA): '__inmo',

        '*.*.edit_allowed_days.cat.{}'.format(Categories.OFERTAS_DE_EMPLEO): 2615321,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    ad_data = {
        'category': '7020',
        'jc': ['16', '17'],
        'subject': 'Ofertas de empleo - yolo ad',
        'body': 'Ofertas de empleo - Yolo ad description',
        'region': '15',
        'name': 'Osto name',
        'email': 'prepaid5@blocket.se',
        'passwd': '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
    }

    def _forced_login_scenario(self, page, ad_data, modal=None):
        page.insert_ad(**ad_data)
        check = self.assertVisible if modal else self.assertNotVisible
        check(page, 'login_box')

        if modal:
            # Title could be category specific
            self.assertEqual(page.login_box_title.text, modal['title'])
            # The current category should be reset after login box closing
            page.login_box_close.click()
            self.assertEqual(page.category.get_attribute('value'), '0')

    def _insert_and_review(self, wd):
        steps.insert_an_ad(wd, AdInsert=None, submit=True, **self.ad_data)
        trans.review_ad(
            email=self.ad_data['email'],
            subject=self.ad_data['subject']
        )
        utils.rebuild_index()

    @tags('ad_insert', 'forced_login')
    def test_ad_insert_unlogged(self, wd):
        """ Login box must appear for configured categories (ad insert) """
        cases = [
            {
                'ad_data': {
                    'category': Categories.OFERTAS_DE_EMPLEO,
                },
                'modal': {
                    'title': 'Login Jobs',
                    'footer': '',
                }
            },
            {
                'ad_data': {
                    'category': Categories.BUSCO_EMPLEO,
                },
            },
            {
                'ad_data': {
                    'category': Categories.ARRIENDO_TEMPORADA,
                },
                'modal': {
                    'title': 'Login Inmo',
                    'footer': 'Login Footer description Inmo NEWAD',
                }
            },
        ]

        page = self.AdInsert(wd)
        page.go()
        for data in cases:
            print('Case: {}'.format(data))
            self._forced_login_scenario(page, **data)

    @tags('ad_insert', 'forced_login')
    def test_ad_insert_logged(self, wd):
        """ Login box should never appear for logged users (ad insert) """
        cases = [
            {
                'ad_data': {
                    'category': Categories.OFERTAS_DE_EMPLEO,
                },
            },
            {
                'ad_data': {
                    'category': Categories.BUSCO_EMPLEO,
                },
            },
            {
                'ad_data': {
                    'category': Categories.ARRIENDO_TEMPORADA,
                },
            },
        ]

        page = Login(wd)
        page.login('prepaid5@blocket.se', '123123123')
        page = self.AdInsert(wd)
        page.go()

        for data in cases:
            print('Case: {}'.format(data))
            self._forced_login_scenario(page, **data)

    @tags('ad_edit', 'forced_login')
    def test_ad_edit_without_account(self, wd):
        """ Login box must appear for configured categories (ad edit) """
        cases = [
            {
                'ad_data': {
                    'category': Categories.ARRIENDO_TEMPORADA,
                },
                'modal': {
                    'title': 'Login Inmo',
                    'footer': '',
                }
            },
            {
                'ad_data': {
                    'category': Categories.BUSCO_EMPLEO,
                },
            },
            {
                'ad_data': {
                    'category': Categories.OFERTAS_DE_EMPLEO,
                },
                'modal': {
                    'title': 'Login Jobs',
                    'footer': 'Login Footer description Jobs EDITAD',
                }
            },
        ]

        page = self.AdEdit(wd)
        page.edit_an_ad('8000048', '123123123', submit=False)
        page = self.AdInsert(wd)

        for data in cases:
            print('Case: {}'.format(data))
            self._forced_login_scenario(page, **data)

    @tags('ad_edit', 'forced_login')
    def test_ad_edit_with_account(self, wd):
        """ Login box should never appear for logged users (ad edit) """
        cases = [
            {
                'ad_data': {
                    'category': Categories.ARRIENDO_TEMPORADA,
                },
            },
            {
                'ad_data': {
                    'category': Categories.BUSCO_EMPLEO,
                },
            },
            {
                'ad_data': {
                    'category': Categories.OFERTAS_DE_EMPLEO,
                },
            },
        ]

        self._insert_and_review(wd)
        page = Login(wd)
        page.login('prepaid5@blocket.se', '123123123')
        page = self.AdEdit(wd)
        page.go('8000085')
        page = self.AdInsert(wd)

        for data in cases:
            print('Case: {}'.format(data))
            self._forced_login_scenario(page, **data)


class ForcedLoginMobile(SeleniumTest):
    snaps = ['accounts', 'diff_pro_account', 'diff_jobs_ads']
    user_agent = utils.UserAgents.IOS
    AdInsert = AdInsertMobile

    bconfs = {
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '0',
        '*.*.forced_login.modal.newad.{}.title'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Jobs',
        '*.*.forced_login.modal.newad.{}.subtitle'.format(Categories.OFERTAS_DE_EMPLEO): 'Login subtitle',
        '*.*.forced_login.modal.newad.{}.footer'.format(Categories.OFERTAS_DE_EMPLEO): '',
        '*.*.forced_login.modal.newad.{}.css_modifier'.format(Categories.OFERTAS_DE_EMPLEO): '__jobs',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '0',
        '*.*.forced_login.modal.editad.{}.title'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Jobs',
        '*.*.forced_login.modal.editad.{}.subtitle'.format(Categories.OFERTAS_DE_EMPLEO): 'Login subtitle Jobs',
        '*.*.forced_login.modal.editad.{}.footer'.format(Categories.OFERTAS_DE_EMPLEO): 'Login Footer description Jobs EDITAD',
        '*.*.forced_login.modal.editad.{}.css_modifier'.format(Categories.OFERTAS_DE_EMPLEO): '__jobs',

        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.ARRIENDO_TEMPORADA): '0',
        '*.*.forced_login.modal.newad.{}.title'.format(Categories.ARRIENDO_TEMPORADA): 'Login Inmo',
        '*.*.forced_login.modal.newad.{}.subtitle'.format(Categories.ARRIENDO_TEMPORADA): 'Login subtitle Inmo',
        '*.*.forced_login.modal.newad.{}.footer'.format(Categories.ARRIENDO_TEMPORADA): 'Login Footer description Inmo NEWAD',
        '*.*.forced_login.modal.newad.{}.css_modifier'.format(Categories.ARRIENDO_TEMPORADA): '__inmo',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.ARRIENDO_TEMPORADA): '0',
        '*.*.forced_login.modal.editad.{}.title'.format(Categories.ARRIENDO_TEMPORADA): 'Login Inmo',
        '*.*.forced_login.modal.editad.{}.subtitle'.format(Categories.ARRIENDO_TEMPORADA): 'Login subtitle Inmo',
        '*.*.forced_login.modal.editad.{}.footer'.format(Categories.ARRIENDO_TEMPORADA): '',
        '*.*.forced_login.modal.editad.{}.css_modifier'.format(Categories.ARRIENDO_TEMPORADA): '__inmo',

        '*.*.edit_allowed_days.cat.{}'.format(Categories.OFERTAS_DE_EMPLEO): 2615321,
        '*.*.movetoapps_splash_screen.enabledr': '0'
    }


    def _forced_login_scenario(self, wd, expected_url, ad_data):
        page = self.AdInsert(wd)
        page.go()

        page.insert_ad(**ad_data)
        expected = expected_url.format(ad_data['category'])
        page.wait.until(lambda wd: expected in wd.current_url)

    def _forced_login_edit_scenario(self, wd, ad_data, expected_url):
        page = AdView(wd)
        page.go(ad_data['list_id'])
        page.do_edit()

        expected = expected_url.format(ad_data['list_id'], ad_data['category'])
        page.wait.until(lambda wd: expected in wd.current_url)


    @tags('ad_insert', 'forced_login')
    def test_ad_insert_unlogged(self, wd):
        cases = [{
            'ad_data': {
                'category': Categories.OFERTAS_DE_EMPLEO,
            },
            'expected_url': '/login?cat={}'
        }, {
            'ad_data': {
                'category': Categories.BUSCO_EMPLEO,
            },
            'expected_url': '/publica-un-aviso'
        }, {
            'ad_data': {
                'category': Categories.ARRIENDO_TEMPORADA,
            },
            'expected_url': '/login?cat={}'
        }]

        for case in cases:
            self._forced_login_scenario(wd, case['expected_url'], case['ad_data'])

    @tags('ad_insert', 'forced_login')
    def test_ad_insert_logged(self, wd):
        cases = [{
            'ad_data': {
                'category': Categories.OFERTAS_DE_EMPLEO,
            },
            'expected_url': '/publica-un-aviso'
        }, {
            'ad_data': {
                'category': Categories.BUSCO_EMPLEO,
            },
            'expected_url': '/publica-un-aviso'
        }, {
            'ad_data': {
                'category': Categories.ARRIENDO_TEMPORADA,
            },
            'expected_url': '/publica-un-aviso'
        }]

        page = LoginMobile(wd)
        page.login('prepaid5@blocket.se', '123123123')
        page = self.AdInsert(wd)

        for case in cases:
            self._forced_login_scenario(wd, case['expected_url'], case['ad_data'])

    @tags('ad_edit', 'forced_login')
    def test_ad_edit_without_account(self, wd):
        cases = [{
            'ad_data': {
                'category': Categories.OFERTAS_DE_EMPLEO,
                'list_id': 8000046,
            },
            'expected_url': '/login?list_id={}&cat={}'
        }, {
            'ad_data': {
                'category': Categories.INMUEBLES,
                'list_id': 8000018,
            },
            'expected_url': 'editar-aviso/validacion/',
        }]

        for case in cases:
            self._forced_login_edit_scenario(wd, case['ad_data'], case['expected_url'])

    @tags('ad_edit', 'forced_login')
    def test_ad_edit_with_account(self, wd):
        cases = [{
            'ad_data': {
                'category': Categories.OFERTAS_DE_EMPLEO,
                'list_id': 8000188,
            },
            'expected_url': 'editar-aviso/formulario',
        }, {
            'ad_data': {
                'category': Categories.VENDO,
                'list_id': 6000667,
            },
            'expected_url': 'editar-aviso/formulario',
        }]

        page = LoginMobile(wd)
        page.login('prepaid5@blocket.se', '123123123')

        for case in cases:
            self._forced_login_edit_scenario(wd, case['ad_data'], case['expected_url'])
