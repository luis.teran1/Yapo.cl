# -*- coding: latin-1 -*-
import datetime
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.control_panel import ControlPanel
from yapo.pages import desktop
from yapo.steps import trans
from yapo import utils
from yapo import steps
import yapo

class DesktopAdInsertMaxYear(yapo.SeleniumTest):

    snaps = ['accounts']
    now = datetime.datetime.now()
    this_month = now.month
    this_year = now.year

    if this_month >= 9:
        this_year += 1

    @tags('ad_insert', 'desktop', 'searchbox')
    def test_desktop_max_year_car(self, wd):
        """ Insert maximum year car ad for desktop"""
        steps.insert_an_ad(wd, subject=""+str(self.this_year)+" Car",
                       body="yoLo",
                       region=15,
                       communes=331,
                       category=2020,
                       price=3500000,
                       private_ad=True,
                       name="yolo",
                       email="carlos@schibsted.cl",
                       email_verification="carlos@schibsted.cl",
                       phone="99996669",
                       passwd="$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6",
                       create_account=False,
                       accept_conditions=True,
                       brand=31,
                       model=44,
                       version=1,
                       regdate=self.this_year,
                       gearbox=1,
                       fuel=1,
                       cartype=1,
                       plates='ABCD10',
                       mileage=15000)

        trans.review_ad('carlos@schibsted.cl', str(self.this_year) + ' Car')
        utils.rebuild_index()

        list_desktop = AdListDesktop(wd)
        list_desktop.go()
        list_desktop.side_search_box.ordered_fill_form([
            ('category', '2020'),
            ('regdate_rs', str(self.this_year)),
            ('regdate_re', str(self.this_year)),
        ])
        list_desktop.side_search_box.search_button.click()

        adview = list_desktop.go_to_ad(subject=str(self.this_year)+" Car")
        self.assertIn("A�o "+str(self.this_year), adview.adparams_box.text)

        #FT227892681
        edit_pw = desktop.EditPasswordPage(wd)
        edit_pw.go('8000085')
        edit_pw.validate_password('123123123')

        edit = desktop.AdEdit(wd)
        self.assertTrue(edit.brand.get_attribute("value") == "31")
        self.assertTrue(edit.model.get_attribute("value") == "44")


class AdInsertOtherBrand(yapo.SeleniumTest):

    """ Tests Brand other and model = other in desktop AdInsert """

    snaps = ['accounts']
    data = {'subject': 'Car ad 2016',
            'body': 'yoLo',
            'region': 15,
            'communes': 331,
            'category': 2020,
            'price': 3500000,
            'private_ad': True,
            'name': 'yolo',
            'email': 'carlos@schibsted.cl',
            'email_verification': 'carlos@schibsted.cl',
            'phone': '99996666',
            'passwd': '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
            'create_account': False,
            'accept_conditions': True,
            'brand': 0,
            'regdate': 2016,
            'gearbox': 1,
            'fuel': 1,
            'cartype': 1,
            'mileage': 15000,
            'plates': 'ABCD10',
           }

    @tags('ad_insert', 'cars', 'brand', 'model')
    def test_ad_insert_other_brand(self, wd):
        """ Inserts an ad with brand = other and another with brand GREAT WALL and model = other """

        steps.insert_an_ad(wd, **self.data)

        self.data.update({'brand': 35, 'model': 0, 'subject': 'Car ad GREAT WALL 2016'})
        steps.insert_an_ad(wd, **self.data)

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('carlos@schibsted.cl', subject='Car ad 2016', queue='Unreviewed')
        element = wd.find_element_by_css_selector('.GreyOutlineTop table tr:nth-child(2) td:nth-child(2)')
        self.assertEqual(element.text, 'Brand # Other')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])

        cpanel.search_for_ad('carlos@schibsted.cl', subject='Car ad GREAT WALL 2016', queue='Unreviewed')
        element = wd.find_element_by_css_selector('.GreyOutlineTop table tr:nth-child(2) td:nth-child(2)')
        self.assertEqual(element.text, 'Model # Other')
