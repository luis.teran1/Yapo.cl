from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.desktop import LoginModal
from yapo.utils import Categories
from yapo.steps import insert_an_ad
import yapo


class TestAdInsertLogin(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.*.cat.7020.passwd.required': '1',
        '*.*.forced_login.modal.newad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
        '*.*.forced_login.modal.editad.{}.disabled'.format(Categories.OFERTAS_DE_EMPLEO): '1',
    }

    def test_login_after_errors(self, wd):
        """ Publish an Ad after receiving AdInsert errors and Logging in (https://jira.schibsted.io/browse/JOBS-495) """

        ad = {
            'category': '7020',
            'email': 'yo@lo.cl',
            'email_verification': 'yo@lo.cl',
        }

        account = {
            'email': 'prepaid5@blocket.se',
            'password': '123123123',
        }

        # Insert and ad with errors
        insert_an_ad(wd, AdInsert=AdInsertDesktop, Submit=True, **ad)
        ai = AdInsertDesktop(wd)
        self.assertEqual(ai.get_error_for('passwd').text, ai.ERROR_ACCOUNT_REQUIRED)

        # Login using the modal
        login = LoginModal(wd)
        login.login(with_go_login=True, **account)
        login.wait.until_not_visible('email')

        # The ad user info should be the same from the account
        self.assertEqual(ai.val('email'), account['email'])

        # The ad should be published
        ai.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
