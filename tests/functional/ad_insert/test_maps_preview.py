# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess, AdInsertPreview
from yapo.geography import Regions, Communes
import yapo
import unittest

class AdInsertPreviewTest(yapo.SeleniumTest):

    @tags('ad_insert', 'desktop', 'maps', 'preview')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_approx_map_preview(self, browser):
        """ Map preview with approx location """
        self._preview_test_case(browser,
            region=9,
            communes=171,
            add_location=True,
            address='Calle Las Araucarias',
            show_location=True,
        )

    @tags('ad_insert', 'desktop', 'maps', 'preview')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_precise_map_preview(self, browser):
        """ Map preview with precise location """
        self._preview_test_case(browser,
            region=9,
            communes=171,
            add_location=True,
            address='Calle Las Araucarias',
            address_number=123,
            show_location=True,
            from_preview=True,
        )

    @tags('ad_insert', 'desktop', 'maps', 'preview')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_no_map_preview(self, browser):
        """ Map preview without clicking show_location """
        self._preview_test_case(browser,
            region=9,
            communes=171,
            add_location=True,
            address='Calle Las Araucarias',
            address_number=123,
            from_preview=True,
        )

    def _preview_test_case(self, browser, **kwargs):
        # Base parameters
        ai_args = {
            'category': 5040, 'subject': 'Le preview', 'body' :'wow, such preview',
            'price': 12342, 'private_ad': True, 'create_account': True, 'accept_conditions': True,
            'name': 'yolo', 'email': 'yolo@cabral.es', 'email_verification': 'yolo@cabral.es',
            'phone': '999966666', 'password': '999966666', 'password_verification': '999966666',
        }

        # Copy original args, remove the ones that break AI
        input = kwargs.copy()
        if 'from_preview' in kwargs:
            kwargs.pop('from_preview')
        ai_args.update(kwargs)

        # Determine the kind of location
        if 'exact' in ai_args:
            precise = ai_args['exact']
        elif 'approx' in ai_args:
            precise = not ai_args['approx']
        else:
            precise = 'address_number' in ai_args

        # Calculate expected address to be seen on preview
        street = ''
        region = Regions.name(ai_args['region'])
        commune = Communes.name(ai_args['communes'])
        if precise:
            street = '{} {}\n'.format(ai_args['address'], ai_args['address_number'])

        expected_address = '{}{}, {}'.format(street, region, commune)

        # Insert the ad
        ad_insert = AdInsert(browser)
        ad_insert.go()
        ad_insert.insert_ad(**ai_args)

        # Go to preview
        ad_insert.preview.click()
        preview = AdInsertPreview(browser)
        map_view = preview.map_view

        # Validate the preview page
        if 'show_location' in ai_args and ai_args['show_location']:
            self.assertEqual(map_view.address.text, expected_address)

            if precise:
                self.assertNotVisible(map_view, 'circle_marker')
                self.assertVisible(map_view, 'precise_marker')
            else:
                self.assertNotVisible(map_view, 'precise_marker')
                self.assertVisible(map_view, 'circle_marker')
        else:
            self.assertNotPresent(map_view, "address")
            self.assertNotVisible(map_view, 'precise_marker')
            self.assertNotVisible(map_view, 'circle_marker')

        # Go back to ad insert and see if parameters stay
        preview.edit_ad.click()
        if 'region' in input:
            self.assertEqual(ad_insert.map_form.region.get_attribute('value'), str(input['region']))
        if 'communes' in input:
            self.assertEqual(ad_insert.map_form.communes.get_attribute('value'), str(input['communes']))
        if 'add_location' in input:
            self.assertEqual(ad_insert.map_form.add_location.is_selected(), input['add_location'])
        if 'address' in input:
            self.assertEqual(ad_insert.map_form.address.get_attribute('value'), str(input['address']))
        if 'address_number' in input:
            self.assertEqual(ad_insert.map_form.address_number.get_attribute('value'), str(input['address_number']))
        if 'show_location' in input:
            self.assertNotEqual(ad_insert.map_form.geoposition.get_attribute('value'), '')
        else:
            self.assertEqual(ad_insert.map_form.geoposition.get_attribute('value'), '')


        # Fill back the password
        ad_insert.password.send_keys([ai_args['password']])
        ad_insert.password_verification.send_keys([ai_args['password_verification']])

        # Publish the ad, whether from the preview or directly from AI
        if 'from_preview' in input and input['from_preview']:
            ad_insert.preview.click()
            preview.create.click()
        else:
            ad_insert.submit.click()

        # Verify Ad Insert success
        page = AdInsertWithAccountSuccess(browser)
        self.assertEqual(page.title.text, '¡Gracias por publicar!')
