# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.control_panel import ControlPanel
from yapo.pages.ad_insert import AdInsertResult
from yapo import utils
import yapo
import time

class MobileAdInsert(yapo.SeleniumTest):

    snaps = ['accounts']
    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.forced_login.modal.newad.7020.disabled': '1',
        '*.*.forced_login.modal.editad.7020.disabled': '1',
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('mobile', 'accounts', 'ad_insert')
    def test_recreate_account_on_ai(self, wd):
        """Check that create account checkbox doesn't exists when unlogged
        and account pending confirmation"""
        utils.clean_mails()
        mai = mobile.NewAdInsert(wd)
        mai.go()
        params = {'subject': "Foo",
                  'body': "Foo Bar Bazz Qux",
                  'price': "123123123",
                  'region': "15",
                  'communes': "331",
                  'category': "5020",
                  'private_ad': True,
                  'name': "Foobar",
                  'phone': "67875436",
                  'phone_hidden': False,
                  'password': "123123123",
                  'password_verification': "123123123",
                  'accept_conditions': True, }

        mai.insert_ad(email='acc_pending@yapo.cl', create_account=True, **params)
        mai.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))
        self.assertTrue(result.was('with_account_create'))
        self.assertIsNotNone(utils.search_mails('(https?://.*?cuenta/confirm.*?)[\'\"<\n\s>]'))


    def test_ai_data_logged(self, wd):
        """ Logged in on AI show account info on inputs """
        # Verify input status before login
        ad_insert = mobile.NewAdInsert(wd)
        ad_insert.go()
        self.assertEqual(ad_insert.name.get_attribute("value"), '')
        self.assertEqual(ad_insert.email.get_attribute("value"), '')
        # The user now logs in
        login_page = mobile.Login(wd)
        login_page.login('prepaid5@blocket.se', '123123123')
        # Getting back to ad insert
        ad_insert = mobile.NewAdInsert(wd)
        ad_insert.go()
        self.assertEqual(ad_insert.name.get_attribute('value'), 'BLOCKET')
        self.assertEqual(ad_insert.email.get_attribute('type'), 'hidden')
