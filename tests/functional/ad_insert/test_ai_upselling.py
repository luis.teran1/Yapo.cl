# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertUpselling, AdInsertPreview
from yapo.utils import Upselling
from yapo.pages.desktop import Payment, PaymentVerify, PaymentVerifyFail
from yapo.pages.generic import SentEmail
from yapo.pages.newai import DesktopAI
from yapo.pages.simulator import Paysim
from yapo.steps import trans, insert_an_ad
from selenium.common import exceptions
import time


class AIUpsellingUnlogged(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.*.payment.product_specific.upselling_daily_bump.first_execution_delay': '1',
        '*.*.payment.product_specific.upselling_weekly_bump.first_execution_delay': '1',
        '*.*.payment.product_specific.weekly_bump.interval_seconds':'1',
        '*.*.payment.product_specific.daily_bump.interval_seconds':'1'
    }

    ad_data = {
        'subject':'Auto',
        'region':'15',
        'communes':'322',
        'category':'2020',
        'body':'yoLo',
        'price':'3500000',
        'password':'123123123',
        'password_verification':'123123123',
        'phone':'999966666',
        'accept_conditions':True,
        'brand':'31',
        'model':'44',
        'version':'1',
        'private_ad':'1',
        'create_account':False,
        'regdate':'2010',
        'gearbox':'1',
        'fuel':'1',
        'cartype':'1',
        'mileage':'15000'
    }

    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_products_ai_without_image(self, wd):
        """ Upselling: Check upselling products when insert an ad without a image """
        ad_data = self.ad_data.copy()
        ad_data.update(
            email='manyads@yapo.cl',
            email_verification='manyads@yapo.cl',
            name='super many'
        )
        self._insert_ad(wd, ad_data)
        ai = AdInsert(wd)
        self.assertTrue(wd.find_element_by_css_selector('#basic').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#standard').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#advanced.__disabled').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#premium.__disabled').is_displayed())


    @tags('ad_insert', 'desktop', 'upselling')
    def test_upselling_products_ai_with_image(self, wd):
        """ Upselling: Check upselling products when insert an ad with a image """
        ad_data = self.ad_data.copy()
        ad_data.update(
            email='sincuenta@yapo.cl',
            email_verification='sincuenta@yapo.cl',
            name='sin cuenta para upsellin',
            images=['image_order/01.png']
        )
        self._insert_ad(wd, ad_data)
        ai = AdInsert(wd)
        self.assertTrue(wd.find_element_by_css_selector('#basic').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#standard').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#advanced').is_displayed())
        self.assertTrue(wd.find_element_by_css_selector('#premium').is_displayed())

    def _insert_ad(self, wd, data):
        ad_insert = AdInsert(wd)
        insert_an_ad(wd, AdInsert=DesktopAI, Submit=False, **data)
