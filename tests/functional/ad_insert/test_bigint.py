# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert
from yapo.pages.control_panel import ControlPanel
from yapo.utils import UserAgents, rebuild_index
from yapo.steps import insert_an_ad
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.mobile_list import AdListMobile
import time


class TestBigintAI(yapo.SeleniumTest):

    AdInsert = AdInsertDesktop
    snaps = ['accounts']
    price_peso = ['9999999999', '9.999.999.999']
    price_uf = ['313000,50', '313.000,50']
    listing = AdListDesktop

    def _check_bigint(self, wd, subject, price, currency):
        # Insert ad
        data = {'subject': subject,
                'body': 'yoLo map',
                'category': 1220,
                'region': 15,
                'communes': 331,
                'price': price[0],  # biguf!
                'private_ad': True,
                'currency': currency,
                'name': 'yolo',
                'email': "lol@gitgut.org",
                'email_verification': "lol@gitgut.org",
                'phone': '99996666',
                'password': '999966666',
                'password_verification': '999966666',
                'create_account': False,
                'accept_conditions': True}
        insert_an_ad(wd, AdInsert=self.AdInsert, **data)
        time.sleep(1)

        # CP review
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject(subject)
        wd.find_element_by_link_text(subject).click()
        wd.switch_to_window(wd.window_handles[1])
        self.assertEqual(cp.price.get_attribute('value'), price[1])
        cp.review_ad('accept')
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        cp.logout()

        # See in listing
        rebuild_index()
        list = self.listing(wd)
        list.go('chile')
        self.assertIn(price[1], list.first_ad_list.text)

    @tags('ad_insert')
    def test_bigint_ai(self, wd):
        """ Test that the AI works with an allowed bigint"""
        self._check_bigint(wd, 'Aviso de prueba1', self.price_peso, 'peso')

    @tags('ad_insert')
    def test_bigint_uf_ai(self, wd):
        """ Test that the AI works with an allowed 'biguf'"""
        self._check_bigint(wd, 'Aviso de prueba2', self.price_uf, 'uf')


class TestBigintAIMobile(TestBigintAI):

    AdInsert = NewAdInsert
    user_agent = UserAgents.IOS
    listing = AdListMobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
