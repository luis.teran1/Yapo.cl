# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.pages.ad_insert import AdInsert
from yapo.pages.control_panel import ControlPanel
from yapo.pages.generic import SentEmail
from yapo.steps import trans


class TestAICreateAccountCheckbox(yapo.SeleniumTest):

    snaps = ['adqueues']

    def test_check_insert_ad_form(self, wd):
        """ Check on AI form create account is selected by default """
        ad_insert = AdInsert(wd)
        ad_insert.go()
        self.assertTrue(ad_insert.create_account.is_selected())

    def test_check_refused_mail(self, wd):
        """ Check on AI form create account is selected when the user open the refuse mail """
        subject = 'Testannons 8'
        email = 'user3_uid2@blocket.se'

        trans.review_ad(email, subject, action = 'refuse')

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_partial_link_text(
            "aqu�").get_attribute("href")

        ad_insert = AdInsert(wd)
        wd.get(link)
        self.assertTrue(ad_insert.create_account.is_selected())

    def test_check_post_refuse_ad(self, wd):
        """ Check on AI form create account is selected when the user open the post refuse mail """
        yapo.trans.review(
            ad_id='600347',
            token=yapo.trans.authenticate()['token'],
            action_id = '1',
            action = 'refuse',
            reason = '31'
        )

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_partial_link_text(
            "aqu�").get_attribute("href")

        ad_insert = AdInsert(wd)
        wd.get(link)
        self.assertTrue(ad_insert.create_account.is_selected())
