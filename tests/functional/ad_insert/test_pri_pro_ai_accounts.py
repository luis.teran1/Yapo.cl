# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert
from yapo.pages import mobile
from yapo.pages.desktop import Login
from yapo.utils import UserAgents, Categories
import yapo
import yapo.conf


class AdInsertProAccounts(yapo.SeleniumTest):

    pro_categories = [
        '1220',
        '1240',
        '2020',
        '2040',
        '2060',
        '2080',
        '2120',
        '7020',
        '7060',
        '7080'
    ]
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def _test_options_desktop(self, wd, is_pro=False):
        ai = AdInsert(wd)
        ai.go()
        for _, cat in Categories.get_subcategories():
            ai.fill('category', cat)
            if cat in self.pro_categories and is_pro:
                ai.wait.until_not_present('private_ad')
                ai.wait.until_not_present('company_ad')
            else:
                ai.wait.until_present('private_ad')
                ai.wait.until_present('company_ad')

    def _test_options_mobile(self, wd, is_pro=False):
        ai = mobile.NewAdInsert(wd)
        ai.go()
        for _, cat in Categories.get_subcategories():
            ai.insert_ad(category=cat)
            self.assertEquals(ai.company_ad_buttons, [])


class AdInsertProAccountsDesktop(AdInsertProAccounts):

    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('accounts', 'desktop')
    def test_desktop_ai_with_account_pro(self, wd):
        """ test desktop ai with account pro"""
        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se', '123123123')
        self._test_options_desktop(wd, True)

    @tags('accounts', 'desktop')
    def test_desktop_ai_with_account_not_pro(self, wd):
        """ test desktop ai with account not pro"""
        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')
        self._test_options_desktop(wd, False)


class AdInsertProAccountsMobile(AdInsertProAccounts):

    snaps = ['accounts']
    user_agent = UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('accounts', 'mobile')
    def test_mobile_ai_with_account_pro(self, wd):
        """ test mobile ai with account pro"""
        login = mobile.Login(wd)
        login.login('prepaid3@blocket.se', '123123123')
        self._test_options_mobile(wd, True)

    @tags('accounts', 'mobile')
    def test_mobile_ai_with_account_not_pro(self, wd):
        """ test mobile ai with account not pro"""
        login = mobile.Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        self._test_options_mobile(wd, False)
