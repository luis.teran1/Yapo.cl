# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from datetime import datetime
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop import Login as LoginDesktop
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.mobile import NewAdInsert as AdInsertMobile, Login as LoginMobile
from yapo.steps import trans, insert_an_ad
import random, string

class FirstAdQueue(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_first_ad']
    AdInsert = AdInsertDesktop
    Login = LoginDesktop
    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    def insert_ad_with_email(self, wd, subject, email, is_logged_in = False):
        body = ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(100))
        price = ''.join(random.choice(string.digits) for _ in range(8))
        if is_logged_in:
             result = insert_an_ad(wd,
                       AdInsert=self.AdInsert,
                       subject=subject,
                       body=body,
                       price=price,
                       category=3040,
                       region=15,
                       communes=331,
                       add_location=True,
                       address='Avenida San Pablo',
                       address_number='1361',
                       accept_conditions=True,
                       logged=True)
        else:
            result=insert_an_ad(wd,
                       AdInsert=self.AdInsert,
                       subject=subject,
                       body=body,
                       price=price,
                       category=3040,
                       region=15,
                       communes=331,
                       add_location=True,
                       address='Avenida San Pablo',
                       address_number='1361',
                       private_ad=True,
                       name='yolo',
                       email=email,
                       email_verification=email,
                       phone='99996666',
                       password='123123123',
                       password_verification='123123123',
                       create_account=True,
                       accept_conditions=True)

        self.assertTrue(result.was('success'))

    def check_first_ad_queue(self, wd):
        page = self.AdInsert(wd)
        cp = ControlPanel(wd)
        # first ad inserted
        wd.delete_all_cookies()
        ad_subject = 'primer aviso de first ad queue'
        self.insert_ad_with_email(wd, ad_subject, 'yolito@putito.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: first_da', cp.list_table_ads_unreviewed[0].text)
        # second ad inserted
        wd.delete_all_cookies()
        ad_subject = 'segundo aviso de first ad queue'
        self.insert_ad_with_email(wd, ad_subject, 'putito@yolito.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: first_da', cp.list_table_ads_unreviewed[0].text)
        # third ad inserted
        wd.delete_all_cookies()
        ad_subject = 'este es el tercer aviso de first ad queue'
        self.insert_ad_with_email(wd, ad_subject, 'yolito@putito.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: normal', cp.list_table_ads_unreviewed[0].text)
        # fourth ad inserted
        wd.delete_all_cookies()
        ad_subject = 'cuarto aviso de first ad queue'
        self.insert_ad_with_email(wd, ad_subject, 'putito@yolito.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: normal', cp.list_table_ads_unreviewed[0].text)


    def check_first_ad_queue_with_uid_cookie(self, wd):
        page = self.AdInsert(wd)
        cp = ControlPanel(wd)
        # first ad inserted
        wd.delete_all_cookies()
        ad_subject = 'primer aviso de first ad queue with uid cookie'
        self.insert_ad_with_email(wd, ad_subject, 'tulita@putito.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: first_da', cp.list_table_ads_unreviewed[0].text)
        # second ad inserted
        cp.logout()
        ad_subject = 'segundo aviso de first ad queue with uid cookie'
        self.insert_ad_with_email(wd, ad_subject, 'putito@tulita.cl')
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: normal', cp.list_table_ads_unreviewed[0].text)


    def check_first_ad_queue_with_accounts(self, wd):
        page = self.AdInsert(wd)
        login = self.Login(wd)
        cp = ControlPanel(wd)
        wd.delete_all_cookies()
        # login
        login.login('user.01@schibsted.cl', '123123123')
        # first ad inserted
        ad_subject = 'primer aviso de first ad queue de user.01'
        self.insert_ad_with_email(wd, ad_subject, 'user.01@schibsted.cl', True)
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: first_da', cp.list_table_ads_unreviewed[0].text)
        # second ad inserted
        cp.logout()
        ad_subject = 'segundo aviso de first ad queue de user.01'
        self.insert_ad_with_email(wd, ad_subject, 'user.01@schibsted.cl', True)
        page.wait.until(lambda x: wd.get_cookie('uid') is not None)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: normal', cp.list_table_ads_unreviewed[0].text)


class FirstAdQueueDesktop(FirstAdQueue):
    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'first_ad', 'ad_insert', 'desktop')
    def test_first_ad_queue(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on desktop"""
        self.check_first_ad_queue(wd)

    @tags('controlpanel', 'first_ad', 'ad_insert', 'desktop')
    def test_first_ad_queue_with_uid_cookie(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on desktop with the uid cookie"""
        self.check_first_ad_queue_with_uid_cookie(wd)

    @tags('controlpanel', 'first_ad', 'ad_insert', 'desktop')
    def test_first_ad_queue_with_account(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on desktop with account"""
        self.check_first_ad_queue_with_accounts(wd)

class FirstAdQueueMobile(FirstAdQueue):
    AdInsert= AdInsertMobile
    Login = LoginMobile
    user_agent = utils.UserAgents.NEXUS_5
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0',
        '*.trans.clear.enqueue.delay':'0'
    }

    @tags('controlpanel', 'first_ad', 'ad_insert', 'mobile')
    def test_first_ad_queue(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on mobile"""
        self.check_first_ad_queue(wd)

    @tags('controlpanel', 'first_ad', 'ad_insert', 'mobile')
    def test_first_ad_queue_with_uid_cookie(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on mobile with the uid cookie"""
        self.check_first_ad_queue_with_uid_cookie(wd)

    @tags('controlpanel', 'first_ad', 'ad_insert', 'mobile')
    def test_first_ad_queue_with_account(self, wd):
        """Check if the first ad from a new uid is going to the first_ad queue on desktop with account"""
        self.check_first_ad_queue_with_accounts(wd)


class FirstAdQueueUnlogged(FirstAdQueue):

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'first_ad', 'ad_insert')
    def test_first_ad_queue_unlogged_w_account(self, wd):
        """Check if a user with account and inserting unlogged his first ad is going to first_ad queue"""
        # Going to first ad queue, first ad, with account not logged
        wd.delete_all_cookies()
        ad_subject = 'Inserto sin logearme por q soy entero loh gi'
        self.insert_ad_with_email(wd, ad_subject, 'user.01@schibsted.cl')

        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: first_da', cp.list_table_ads_unreviewed[0].text)
        cp.logout()

        self.insert_ad_with_email(wd, ad_subject, 'user.01@schibsted.cl', is_logged_in=True)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: normal', cp.list_table_ads_unreviewed[0].text)

class FirstAdQueueWhitelist(FirstAdQueue):

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('controlpanel', 'first_ad', 'ad_insert', 'whitelist')
    def test_first_ad_from_user_in_whitelist(self, wd):
        """Check if a user in whitelist inserting his first ad is going to first_ad queue"""

        page = self.AdInsert(wd)
        cp = ControlPanel(wd)
        wd.delete_all_cookies()
        ad_subject = 'User in whitelist going to first_ad queue'
        self.insert_ad_with_email(wd, ad_subject, 'uid1@blocket.se')
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue(ad_subject, queue='Unreviewed')
        self.assertIn('Ad history\nQueue: whitelist', cp.list_table_ads_unreviewed[0].text)

class FirstAdBumpConfirmation(FirstAdQueue):

    @tags('controlpanel', 'first_ad', 'bump')
    def test_first_ad_bump_confirmation(self, wd):
        """Check if the first ad bump is working"""

        utils.rebuild_index()
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.search_for_ad_by_subject_in_queue('Call of Duty Black ops II Xbox 360', queue='Published')
        cp.table_on_search.find_element_by_link_text("Ad history").click()

        data_search ="bump bump accepted"
        row_text = ""
        wd.switch_to_window(wd.window_handles[1])
        for row in cp.table_ad_history_rows:
            print(row.text)
            if(row.text[17:] == data_search):
                row_text = row.text
                break
        self.assertEqual(row_text[17:], data_search)
