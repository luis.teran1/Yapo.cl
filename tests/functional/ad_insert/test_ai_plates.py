# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import NewAdInsert as AdInsertMobile
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.steps.accounts import login_and_go_to_dashboard
import yapo
from yapo.pages.newai import DesktopAI
from yapo.steps import insert_an_ad


class AIPlatesMsjDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    msj_plates_text = ("No publicaremos la patente en tu aviso,"
    " la solicitamos para mayor seguridad en tu venta.")
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'desktop')
    def test_check_plates_message(self, wd):
        """ Checks plates msj displayed by category """
        ai = self.AdInsert(wd)
        ai.go()
        data = [
            { 'category':'2020', 'type':'s', 'show_msj':True},
            { 'category':'2080', 'type':'s', 'show_msj':False},
            { 'category':'2040', 'type':'s', 'show_msj':True},
            { 'category':'2060', 'type':'s', 'show_msj':True}
        ]

        for info in data:
            show_msj = info.pop('show_msj')
            ai.insert_ad(**info)
            if show_msj:
                ai.wait.until_visible('plates_msj')
                self.assertEquals(ai.plates_msj.text, self.msj_plates_text)
            else:
                ai.wait.until_not_visible('plates_msj')


class AIPlatesMsjMobile(AIPlatesMsjDesktop):
    AdInsert = AdInsertMobile
    user_agent = yapo.utils.UserAgents.IOS


class AdInsertPlates(yapo.SeleniumTest):

    snaps = ['accounts']
    data = {
        'name': 'yolo',
        'email': 'joaco@meial.com',
        'email_verification': 'joaco@meial.com',
        'phone': '99996666',
        'password': '999966666',
        'password_verification': '999966666',
        'create_account': True,
        'accept_conditions': True,
        'subject': 'Auto',
        'region': '15',
        'communes': '322',
        'category': '2020',
        'body': 'yoLo',
        'price': '3500000',
        'password': '123123123',
        'password_verification': '123123123',
        'phone': '999966666',
        'accept_conditions': True,
        'brand': '31',
        'model': '44',
        'version': '1',
        'private_ad': '1',
        'create_account': False,
        'regdate': '2010',
        'gearbox': '1',
        'fuel': '1',
        'cartype': '1',
        'mileage': '15000',
        'plates': 'ABCD10',
    }
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'desktop')
    def test_check_plates_validations(self, wd):
        """ Checks errors related with plates format """

        plates_data = [
           [2020, '', ''],
           [2020, 'FAILPL', 'La patente ingresada no corresponde a un automóvil'],
           [2020, 'XXXX12', ''],
           [2020, 'XX1212', ''],
           [2020, 'XXX12', 'La patente ingresada no corresponde a un automóvil'],
           [2020, 'XX121', 'La patente ingresada no corresponde a un automóvil'],
           [2060, 'FAILPL', 'La patente ingresada no corresponde a una motocicleta'],
           [2060, 'XX0212', 'La patente ingresada no corresponde a una motocicleta'],
           [2060, 'XX212', ''],
           [2060, 'XXX12', ''],
           [2060, 'XXX012', ''],
           [2040, 'FAILPL', 'La patente ingresada no corresponde a un bus o camión'],
        ]

        ai = AdInsertDesktop(wd)

        for info in plates_data:
            category = info[0]
            plates = info[1]
            expected = info[2]
            ai.go()
            ai.insert_ad(category=category)
            ai.fill('plates', plates)
            ai.submit.click()
            ai.wait.until_loaded()
            error = ai.get_error_for('plates').text
            self.assertEqual(error, expected)

    @tags('ad_insert', 'desktop')
    def test_check_hide_plates(self, wd):
        """ Checks that the 'plates' input is hidden
            when the user selects 'Busco' instead of 'Vendo' """
        ai = AdInsertDesktop(wd)
        ai.go()
        ai.insert_ad(category=2020)
        self.assertVisible(ai, "plates")
        ai.rk.click()
        ai.wait.until_not_visible("plates")
        self.assertNotVisible(ai, "plates")

    @tags('ad_insert', 'desktop')
    def test_check_plates_shown(self, wd):
        """ Checks that 'plates' input is shown
            only on categories 2020 2040 and 2060 """

        categories = {
            2020: True,
            2040: True,
            2060: True,
            2080: False,
            2120: False
        }
        ai = AdInsertDesktop(wd)
        ai.go()

        for cat, is_shown in categories.items():
            ai.insert_ad(category=cat)

            if (is_shown):
                self.assertVisible(ai, "plates")
            else:
                self.assertNotVisible(ai, "plates")

