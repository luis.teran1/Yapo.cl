# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert as AdInsertDesktop, AdInsertResult
from yapo.pages.control_panel import ControlPanel
from yapo.steps import trans
from nose.plugins.skip import SkipTest
from nose.plugins.multiprocess import MultiProcess
import time
import yapo
import json
import os
import collections


class AdInsertNewInmo(yapo.SeleniumTest):
    snaps = ['accounts']
    AdInsert = AdInsertDesktop
    email = 'yolo@yapo.cl'
    price = '999666'
    name = 'yoloberto'
    passwd = '123123123'
    private_ad = True
    add_location = False
    body = 'pericolos palotes'
    create_account = False
    accept_conditions = True
    _multiprocess_shared_ = True

    # This data have duplicated info made to generate data only
    # the tests only run the first line for Category/Estate_type
    test_data_file = os.path.join(yapo.conf.RESOURCES_DIR, 'data_ai_new_inmo_categories.json')
    data_file_content = open(test_data_file).read()
    data = json.JSONDecoder(object_pairs_hook=collections.OrderedDict).decode(data_file_content)

    def _insert_ad(self, wd, **kwargs):
        page = self.AdInsert(wd)
        page.go()
        page.insert_ad(**kwargs)
        page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    def _review_ad(self, wd, **kwargs):
        trans.review_ad(kwargs.get('email'), kwargs.get('subject'))

    def _add_common_params(self, key):
        dictionary = self.data[key]
        dictionary['subject'] = 'Test {}'.format(key)
        dictionary[
            'body'] = 'Test description {0}-{1}'.format(dictionary['category'], dictionary['estate_type'])
        dictionary['private_ad'] = self.private_ad
        dictionary['name'] = self.name
        dictionary['email'] = '{}_{}'.format(key.replace(' ','_'), self.email)
        dictionary['email_verification'] = dictionary['email']
        dictionary['phone'] = self.passwd
        dictionary['password'] = self.passwd
        dictionary['password_verification'] = self.passwd
        dictionary['create_account'] = self.create_account
        dictionary['accept_conditions'] = self.accept_conditions
        dictionary['region'] = 15
        return dictionary

    def _run_test(self, wd, key):
        data = self._add_common_params(key)
        self._insert_ad(wd, **data)

    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_1_1(self, wd):
        """ New real estate categories Vendo (1220) Depto (1) """
        key = '1220 1 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_2_1(self, wd):
        """ New real estate categories Vendo (1220) Casa (2) """
        key = '1220 2 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_3_1(self, wd):
        """ New real estate categories Vendo (1220) Ofi (3) """
        key = '1220 3 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_4_1(self, wd):
        """ New real estate categories Vendo (1220) ComInd (4) """
        key = '1220 4 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_5_1(self, wd):
        """ New real estate categories Vendo (1220) Terreno (5) """
        key = '1220 5 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1220_6_1(self, wd):
        """ New real estate categories Vendo (1220) Estacionamiento (6) """
        key = '1220 6 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_1_1(self, wd):
        """ New real estate categories Arriendo (1240) Depto (1) """
        key = '1240 1 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_2_1(self, wd):
        """ New real estate categories Arriendo (1240) Casa (2) """
        key = '1240 2 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_3_1(self, wd):
        """ New real estate categories Arriendo (1240) Ofi (3) """
        key = '1240 3 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_4_1(self, wd):
        """ New real estate categories Arriendo (1240) ComInd (4) """
        key = '1240 4 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_5_1(self, wd):
        """ New real estate categories Arriendo (1240) Terreno (5) """
        key = '1240 5 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_6_1(self, wd):
        """ New real estate categories Arriendo (1240) Estacionamiento (6) """
        key = '1240 6 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1240_7_1(self, wd):
        """ New real estate categories Arriendo (1240) Pieza (7) """
        key = '1240 7 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1260_1_1(self, wd):
        """ New real estate categories A.Temp (1260) Depto (1) """
        key = '1260 1 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1260_2_1(self, wd):
        """ New real estate categories A.Temp (1260) Casa (2) """
        key = '1260 2 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1260_8_1(self, wd):
        """ New real estate categories A.Temp (1260) Caba�a (8) """
        key = '1260 8 1'
        self._run_test(wd,key)
        
    @tags('ad_insert', 'categories', 'ad_params', 'new_inmo')
    def ptest_1260_9_1(self, wd):
        """ New real estate categories A.Temp (1260) Habitaci�n(9) """
        key = '1260 9 1'
        self._run_test(wd,key)
