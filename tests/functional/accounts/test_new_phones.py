# coding: latin-1
from selenium.common.exceptions import NoSuchElementException
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages import accounts, generic
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import utils
import yapo


class CreateAccountPhoneDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    email = 'asd.desktop@asd.cl'
    emailEdit = 'prepaid5@blocket.se'
    CreateAccountClass = accounts.Create
    EditAccountClass = accounts.Edit

    def _assert_error_not_visible(self, wp, element):
        try:
            webelement = getattr(wp, element)
            self.assertFalse(webelement.is_displayed())
        except NoSuchElementException:
            self.assertFalse(False, 'Element {} not found'.format(element))

    def _verify_phones(self, page):
        # By default the phone type is mobile and area code is hidden
        self.assertFalse(page.area_code.is_displayed())
        self.assertEqual(page.phone_prefix.text, "+56 9")

        # First toggle of phone type (Fijo) verify area code visible and empty,
        # also changes of prefix
        page.fill_form(phone_type_fixed=True)
        self.assertTrue(page.area_code.is_displayed())
        self.assertEqual(page.phone_prefix.text, "+56")
        if page.area_code.get_attribute('value') != "":
            self.assertEqual(page.area_code.get_attribute('value'), "2")
            self.assertEqual(page.phone.get_attribute('maxlength'), '8')
            self.assertEqual(page.phone.get_attribute('minlength'), '8')
        else:
            self.assertEqual(page.phone.get_attribute('maxlength'), '7')
            self.assertEqual(page.phone.get_attribute('minlength'), '7')

        # Second toggle of phone type (Movil) verify area code hidden and
        # changes of prefix
        page.fill_form(phone_type_mobile=True)
        self.assertEqual(page.phone_prefix.text, "+56 9")
        self.assertFalse(page.area_code.is_displayed())
        self.assertEqual(page.phone.get_attribute('maxlength'), '8')
        self.assertEqual(page.phone.get_attribute('minlength'), '8')

        # Third toggle of phone type (Fijo + Commune selected) verify area code
        # auto filled
        page.fill_form(region=10, commune=218)
        page.fill_form(phone_type_fixed=True)
        self.assertEqual(page.area_code.get_attribute('value'), '45')
        self.assertEqual(page.phone.get_attribute('maxlength'), '7')
        self.assertEqual(page.phone.get_attribute('minlength'), '7')

        # On change commune change area code
        page.area_code.clear()
        page.fill_form(region=6, commune=50)
        self.assertEqual(page.area_code.get_attribute('value'), '32')

        # On change to RM put the code and increase lenghts of phone
        page.area_code.clear()
        page.fill_form(region=15, commune=310)
        self.assertEqual(page.area_code.get_attribute('value'), '2')
        self.assertEqual(page.phone.get_attribute('maxlength'), '8')
        self.assertEqual(page.phone.get_attribute('minlength'), '8')

        # On change area code change the lengths of phone
        page.fill_form(area_code=45)
        page.phone.click()
        self.assertEqual(page.phone.get_attribute('maxlength'), '7')
        self.assertEqual(page.phone.get_attribute('minlength'), '7')

        # Verify error area code does not exist
        page.fill_form(area_code=1)
        page.phone.click()
        self.assertTrue(page.err_area_code.is_displayed())
        self.assertEqual(
            page.err_area_code.text, "El c�digo de �rea no existe")

        # Verify min length
        page.fill_form(area_code='')
        page.phone.click()
        self.assertTrue(page.err_area_code.is_displayed())
        self.assertEqual(
            page.err_area_code.text, "Debes ingresar un c�digo de �rea")

        # Verify error disappear
        page.fill_form(area_code=64, phone=6596277)
        page.area_code.click()
        self._assert_error_not_visible(page, 'err_area_code')

        # Verify phone errors
        page.fill_form(phone=321)
        page.area_code.click()
        self.assertTrue(page.err_phone.is_displayed())
        self.assertEqual(page.err_phone.text, "Tu n�mero debe tener 7 d�gitos")
        page.fill_form(area_code=2, phone=87654321)
        page.area_code.click()
        self._assert_error_not_visible(page, 'err_phone')
        page.fill_form(phone=321)
        page.area_code.click()
        self.assertTrue(page.err_phone.is_displayed())
        self.assertEqual(page.err_phone.text, "Tu n�mero debe tener 8 d�gitos")
        page.fill_form(phone=87654321)
        page.area_code.click()
        self._assert_error_not_visible(page, 'err_phone')

    @tags('accounts', 'phone_standarization')
    def test_changes_phone_type(self, wd):
        """ Create Toggle phone types, area codes, and phone validations """
        create = self.CreateAccountClass(wd)
        create.go()
        self._verify_phones(create)

    @tags('accounts', 'phone_standarization')
    def test_changes_phone_type_on_edit(self, wd):
        """ Edit Toggle phone types, area codes, and phone validations """
        login_and_go_to_dashboard(
            wd, 'prepaid5@blocket.se',
            '123123123', not hasattr(self, 'user_agent'))
        edit = self.EditAccountClass(wd)
        edit.go()
        self._verify_phones(edit)

    @tags('accounts', 'phone_standarization')
    def test_create_account_with_area_code(self, wd):
        """ Create success account w/area code"""
        create = self.CreateAccountClass(wd)
        create.go()
        create.fill_form(
            person_radio=True,
            name="Yolo",
            region=10,
            commune=204,
            gender_male=True,
            phone_type_fixed=True,
            area_code=45,
            phone="7654321",
            email=self.email,
            password="11111111",
            password_verify="11111111",
            accept_conditions=True)
        create.submit_btn.click()
        page = accounts.CreationSuccessDesktop(wd)
        page.wait.until_loaded()
        self.assertEqual(page.title.text, "Felicidades!")

    @tags('accounts', 'phone_standarization')
    def test_edit_account_with_area_code(self, wd):
        """ Edit w/area codes """
        login_and_go_to_dashboard(
            wd, self.emailEdit, '123123123', not hasattr(self, 'user_agent'))
        edit = self.EditAccountClass(wd)
        edit.go()
        edit.fill_form(phone_type_fixed=True, area_code=45, phone="7654321")
        edit.submit_btn.click()
        edit.wait.until_loaded()
        self.assertTrue(edit.success_message.is_displayed())
        edit = self.EditAccountClass(wd)
        edit.go()
        self.assertEqual(edit.area_code.get_attribute('value'), '45')
        self.assertEqual(edit.phone.get_attribute('value'), '7654321')


class CreateAccountPhoneMobile(CreateAccountPhoneDesktop):

    user_agent = utils.UserAgents.IOS
    email = 'asd.mobile@asd.cl'
    emailEdit = 'many@ads.cl'
    CreateAccountClass = accounts.CreateMobile
    EditAccountClass = accounts.EditMobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
