# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop, AdDeletePage
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.utils import Products, Label_texts, Label_values
from yapo.steps import trans
import time
import yapo
import yapo.conf


class LabelsMobile(yapo.SeleniumTest):

    snaps = ['accounts', 'addaccountandroid']
    # defining a user agent, the user_agent property of firefox is
    # automagically changed
    user_agent = utils.UserAgents.IOS
    congrats = "Tu compra se realiz� exitosamente"
    account_pass = "123123123"
    data_listing = {'has_address': False, 'has_label': Label_texts.URGENT}
    platform = mobile
    AdView = AdViewMobile
    AdList = AdListMobile
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    # Users availabel on snap
    # user = 'prepaid3@blocket.se'
    # user = 'user.01@schibsted.cl'
    # user = "prepaid5@blocket.se"
    # user = 'many@ads.cl'
    # user = 'android@yapo.cl'

    def _delete_page(self, wd):
        # Delete with the first reason "lo vendi en yapo.cl"
        delete_page = mobile.DeletePage(wd)
        delete_page.reasons[0].click()
        delete_page.sell_time_options[2].click()
        delete_page.submit.click()
        # Verify Ad has been deleted
        delete_page.wait.until_visible('modal_message', 'Modal is not visible')
        msj = "Tu aviso fue borrado. En pocos minutos ser� eliminado de la web"
        self.assertIn(msj, delete_page.modal_message.text)

    def _buy_products(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    active.select_product(prod['prod'], prod['param'])

        summary = self.platform.SummaryPayment(wd)
        summary.go_summary()
        summary.press_link_to_pay_products()
        paysim = self.platform.Paysim(wd)
        paysim.wait.until_loaded()
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        pay_verify.wait.until_loaded()
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)

        # Run rebuild-index apply product
        utils.rebuild_index()

    def _check_on_dashboard(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        wd.execute_script("window.scrollTo(0, 500);")
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    if prod['text'] is not None:
                        self.assertTrue(
                            active.has_product_selector_disabled(prod['prod']))
                        self.assertEqual(active.label_tag.text, prod['text'])

    def _check_upselling(self, wd):
        result = AdInsertResult(wd)

        self.assertTrue(result.was('success'))
        self.assertFalse(result.is_element_present('upselling_l'))

class LabelsDesktop(LabelsMobile):

    user_agent = utils.UserAgents.FIREFOX_MAC
    # defining a user agent, the user_agent property of firefox is
    # automagically changed
    congrats = "Tu compra se realiz� exitosamente"
    account_pass = "123123123"
    data_listing = {
        'commune': 'Macul',
        'has_address': False,
        'has_label': 'Urgente',
        'region': 'Regi�n Metropolitana'
    }
    platform = desktop
    AdView = AdViewDesktop
    AdList = AdListDesktop

    def _delete_page(self, wd):
        # Delete with the first reason "lo vendi en yapo.cl"
        delete_page = AdDeletePage(wd)
        delete_page.select_reason()
        delete_page.select_timer()
        delete_page.submit.click()
        # Verify Ad has been deleted
        delete_page.wait.until_visible('result_message', 'no lo encuentro!!')
        msj = "Tu aviso ser� eliminado dentro de los pr�ximos minutos"
        self.assertEqual(delete_page.result_message.text,
                         msj,
                         "Check modal window message")
