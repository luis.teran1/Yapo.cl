# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import Login
from yapo.pages2.abstract import login as newLogin
from yapo.pages import desktop
from yapo.pages import mobile
import yapo
from yapo import utils
from yapo.pages.mobile import MobileHome, HeaderElements, MyAds, DashboardMyAds, NewAdEdit
from yapo import conf
from yapo.pages2.abstract.header import Header
import time
import unittest

skip_message = 'Disabled because new implementation is in progress, requested by QA Chief'


class TestLogin(yapo.SeleniumTest):
    # This test was migrated from regress/final tests

    space_presses = 8
    user_agent = utils.UserAgents.IOS
    snaps = ['accounts', 'addaccountandroid']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts')
    def test_myads(self, wd):
        """
        My ads
        """
        login = Login(wd)
        login.go()
        login.my_ads_without_account.click()
        my_ads = MyAds(wd)
        my_ads.send_input.click()
        self.assertTrue(login.header_error.text == 'Debes insertar un email v�lido.')
        login.my_ads_without_account.click()
        my_ads.email_input.send_keys("whoaohohahahohhaah")
        my_ads.send_input.click()
        self.assertTrue(login.header_error.text == 'Debes insertar un email v�lido.')
        login.my_ads_without_account.click()
        my_ads.email_input.send_keys("theman@withoutaccount.cl")
        my_ads.send_input.click()
        self.assertTrue(login.header_error.text == 'No existen avisos activos para este e-mail: theman@withoutaccount.cl')
        login.my_ads_without_account.click()
        my_ads.email_input.send_keys("prepaid5@blocket.se")
        my_ads.send_input.click()
        self.assertTrue(my_ads.confirmation_info.text == "Hemos enviado un correo con el listado de tus avisos")

    def test_dashboard_links(self, wd):
        """
        Verifies for a set of expected links (edit and delete)
        """
        login = Login(wd)
        login.go()
        login.login("prepaid5@blocket.se", "123123123")
        dash = DashboardMyAds(wd)
        print(dash.buttons_edit[0].get_attribute("href"))
        self.assertTrue("/editar-aviso?id=8000073" in dash.buttons_edit[0].get_attribute("href"))
        dash.buttons_edit[0].click()
        edition = NewAdEdit(wd)
        self.assertTrue(edition.subject.get_attribute("value") == "Adeptus Astartes Codex")
        self.assertTrue(wd.current_url[-37:] == "/editar-aviso/formulario/0?id=8000073")
        login.go()
        dash.buttons_delete[0].click()
        print (wd.current_url[-26:])
        self.assertTrue(wd.current_url[-26:] == "/delete_reasons?id=8000073")

class TestDesktopModalLogin(yapo.SeleniumTest):
    snaps = ['accounts']

    @tags('desktop', 'accounts', 'login', 'login_attempts')
    def test_login_page_lock_in_modal(self, wd):
        """
        Verify that the account is locked when the user tries to access
        more than 3 times with wrong password from modal login
        """
        email = 'many@ads.cl'
        title_email = 'Hemos detectado que no puedes ingresar a tu cuenta Yapo.cl'
        utils.clean_mails()
        error_messages = [
            'El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.',
            'El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.',
            'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
        ]
        login = desktop.Login(wd)
        login_modal = desktop.LoginModal(wd)
        for i, msg in enumerate(error_messages):
            login.go()
            login_modal.login(email, 'yolito yoli', with_go_login=True)
            # Verify message
            login_modal.wait.until_present('error_msg')
            login_modal.wait.until_loaded_jquery_ajax()
            self.assertIn(msg, [error.text for error in login_modal.error_msg])
            # Verify increment key in redis
            self.assertEqual(int(utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'get', 'rsd1x_login_{0}'.format(email))), i+1)
            # Verify that the email is sent in third intent
            if (i + 1) == 3:
                self.assertIsNotNone(utils.search_mails('({0})'.format(title_email)), 10)
            else:
                self.assertIsNone(utils.search_mails('({0})'.format(title_email), max_retries=1))


class LoginAttemptsDesktopTests(yapo.SeleniumTest):
    snaps = ['accounts']
    platform = desktop

    @tags('desktop', 'accounts', 'login', 'login_attempts')
    def test_login_page_lock(self, wd):
        """
        Verify that the account is locked when the user tries to access
        more than 3 times with wrong password from login page
        """
        email = 'many@ads.cl'
        title_email = 'Hemos detectado que no puedes ingresar a tu cuenta Yapo.cl'
        utils.clean_mails()
        error_messages = [
            'El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.',
            'El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.',
            'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
        ]
        login = self.platform.Login(wd)
        for i, msg in enumerate(error_messages):
            from_desktop = False if hasattr(self, 'user_agent') else True
            kwargs = {} if from_desktop else {'do_login': False}
            login.login(email, 'yolito yoli', **kwargs)
            # Verify message
            if from_desktop:
                login.wait.until_present('error_msg')
                self.assertEqual(login.error_msg.text, msg)
            else:
                login.wait.until_present('errors')
                login.wait.until(lambda x: [0 if len(msg.text) else 1 for msg in login.errors] != len(login.errors))
                self.assertIn(msg, [msg.text for msg in login.errors])
            # Verify increment key in redis
            self.assertEqual(int(utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'get', 'rsd1x_login_{0}'.format(email))), i+1)
            # Verify that the email is sent in third intent
            if (i + 1) == 3:
                self.assertIsNotNone(utils.search_mails('({0})'.format(title_email)))
            else:
                self.assertIsNone(utils.search_mails('({0})'.format(title_email), max_retries=1))


class TestLoginInNewListing(yapo.SeleniumTest):
    snaps = ['accounts']
    bconfs = {'*.*.merken.status': 1}

    @unittest.skip(skip_message)
    @tags('accounts', 'login')
    def test_login_ok(self, wd):
        """
        Tested login ok
        """
        header = Header(wd)
        login = newLogin.Login(wd)
        login.go()
        header.open_login()
        ok = login.request(email='prepaid5@blocket.se', password='123123123', expected_error=None)
        self.assertTrue(ok)

    @unittest.skip(skip_message)
    @tags('accounts', 'login')
    def test_login_invalid(self, wd):
        """
        Tested login invalid
        """
        header = Header(wd)
        login = newLogin.Login(wd)
        login.go()
        header.open_login()
        error = login.request(
            email='prepaid5@blocket.se',
            password='4324234243',
            expected_error=newLogin.Login.EMAIL_INVALID
        )
        self.assertFalse(error)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'login_attempts')
    def test_login_account_lock(self, wd):
        """
        Tested login lock
        """
        header = Header(wd)
        login = newLogin.Login(wd)
        login.go()
        header.open_login()
        list_errors = [newLogin.Login.EMAIL_INVALID]*2 + [newLogin.Login.EMAIL_LOCK]
        for error in list_errors:
            error = login.request(
                email='prepaid3@blocket.se',
                password='4324234243',
                expected_error=error
            )
            self.assertFalse(error)
