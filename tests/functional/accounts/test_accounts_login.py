# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.mail_generator import Email
from yapo.pages import accounts, control_panel, generic
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess
from yapo.pages.desktop import BumpInfoLightboxDashboard, EditPasswordPage
from yapo.pages.desktop import DashboardMyAds, PageNotFound, HeaderElements
from yapo.pages.desktop import MyAds, Login
from yapo.pages.desktop import SummaryPayment, Dashboard as DashboardDesktop
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.help import Help
from yapo.pages.mobile import Dashboard as DashboardMobile
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.steps.accounts import login_from_bar
from yapo.steps.newai import insert_ad
from yapo.pages.newai import DesktopAI
from yapo.utils import JsonTool
import hashlib
import time
import yapo
import yapo.conf


class AccountLoginTest(yapo.SeleniumTest):

    snaps = ['accounts']
    pro_data = {}
    bconfs = {
        '*.*.accounts.dashboard.ads_per_page': 10,
    }

    account_info = dict(
        person_radio=True,
        name='yolo',
        phone='69696969',
        region=15,
        email='yolo@cabral.es',
        password='123123123',
        password_verify='123123123',
        accept_conditions=True
    )

    @tags('desktop', 'accounts', 'splash')
    def test_splash_login(self, wd):
        """ Check if splash login message is present """
        wd.get(yapo.conf.DESKTOP_URL)
        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid3@blocket.se', '123123123')
        acc_bar.wait.until_present('message')
        self.assertIn('Hola', acc_bar.message.text)

    @tags('desktop', 'accounts', 'login')
    def test_combo_login(self, wd):
        """ Check if a user can press the login button """
        page = accounts.Login(wd)
        page.go()
        page.email_input.send_keys('prepaid3@blocket.se')
        page.password_input.send_keys('123123123')
        page.submit_btn.click()

    @tags('desktop', 'accounts', 'login')
    def test_login_errors(self, wd):
        """Check errors account login"""
        login = accounts.Login(wd)
        login.go()
        login.email_input.send_keys('prepaid3')
        login.password_input.click()
        login.wait.until_present('err_email_input')
        self.assertEqual(login.err_email_input.text,
                         "Confirma que el email est� en el formato correcto")
        login.fill_form(
            email_input='prepaid3@blocket.se', password_input='wrongpasswd')
        login.submit_btn.click()
        self.assertEqual(
            login.err_form.text,
            "El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.")
        login.fill_form(
            email_input='corr3ono3exist3@mail.com', password_input='xxx')
        login.submit_btn.click()
        self.assertEqual(
            login.err_form.text,
            "El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.")

    @tags('desktop', 'accounts', 'splash', 'minimal', 'core1')
    def test_login_page_success(self, wd):
        """ Check login success page """
        test_data_key = 'pro_user_with_list_id'
        test_data = JsonTool.read_test_data(test_data_key)

        login_and_go_to_dashboard(
            wd, test_data['email'], test_data['password'])
        dashboard = DashboardDesktop(wd)
        self.assertEqual(
            'Hola {0}'.format(test_data['username']),
            dashboard.title_legend.text)
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, {0}'.format(test_data['username']),
            header.button_logged_user.text)

    @tags('desktop', 'accounts', 'splash', 'minimal', 'core1')
    def test_login_account_bar_success(self, wd):
        """ Check login from account bar """
        test_data_key = 'pro_user_with_list_id'
        test_name = self._testMethodName
        test_data = JsonTool.read_test_data(test_data_key)

        wd.get(yapo.conf.DESKTOP_URL)
        acc_bar = generic.AccountBar(wd)
        acc_bar.login(test_data['email'], test_data['password'])
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, {0}'.format(test_data['username']),
            header.button_logged_user.text)

    @tags('desktop', 'accounts', 'splash', 'minimal', 'core1')
    def test_login_account_on_ad_view_success(self, wd):
        """ Check login from ad view """
        test_data_key = 'pro_user_with_list_id'
        test_name = self._testMethodName
        test_data = JsonTool.read_test_data(test_data_key)

        pass_page = EditPasswordPage(wd)
        pass_page.go(test_data['list_id'])
        pass_page.validate_password(test_data['password'])
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, {0}'.format(test_data['username']),
            header.button_logged_user.text)

    @tags('desktop', 'accounts', 'login')
    def test_login_from_edit_other_user(self, wd):
        """ Test login from ad edit of other user """
        pass_page = EditPasswordPage(wd)
        pass_page.go('8000070')

        pass_page.wait.until_visible('edit_message')
        self.assertEqual(pass_page.edit_message.text,
                         'Editar tu aviso no lo ubica en '
                         'las primeras posiciones.')

        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid5@blocket.se', '123123123')
        acc_bar.wait.until_present('message')
        self.assertIn('Hola, BLOCKET', acc_bar.message.text)

        pass_page.wait.until_visible('edit_message')
        self.assertEqual(pass_page.edit_message.text,
                         'Editar tu aviso no lo ubica en '
                         'las primeras posiciones.')

    @tags('desktop', 'accounts', 'login')
    def test_loginbar_max_length(self, wd):
        """ Test max length on loginbar """
        listing = AdListDesktop(wd)
        listing.go("/region_metropolitana")
        acc_bar = generic.AccountBar(wd)
        acc_bar.login_link.click()
        acc_bar.input_email.send_keys("prepaid5@blocket.se")
        acc_bar.input_pass.send_keys(
            "we are putting a really long password here")
        password = acc_bar.input_pass.get_attribute("value")
        self.assertEquals(len(password), 20)
        wd.execute_script(
            'arguments[0].setAttribute("maxlength", "30")', acc_bar.input_pass)
        acc_bar.input_pass.send_keys(
            "we are putting a really long password here "
            "we are putting a really long password here")
        password = acc_bar.input_pass.get_attribute("value")
        self.assertEquals(len(password), 30)
        acc_bar.login_btn.click()
        self.assertEquals(
            u"El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.",
            acc_bar.error_message.text)

    @tags('desktop', 'accounts', 'login')
    def test_login_from_login_max_length(self, wd):
        """ Test max length on login page """
        login = Login(wd)
        login.go()

        login.wait.until_visible('input_email')
        login.wait.until_visible('input_password')
        self.assertEquals(
            "20", login.input_password.get_attribute("maxlength"))
        login.input_password.send_keys(
            "we are putting a really long password here")
        password = login.input_password.get_attribute("value")
        self.assertEquals(len(password), 20)
        wd.execute_script("arguments[0].maxLength=16;", login.input_password)
        self.assertEquals(
            "16", login.input_password.get_attribute("maxlength"))

        login.login('prepaid5@blocket.se', 'Lorem ipsum dolo')

        self.assertEquals(
            u"El correo electr�nico y/o la contrase�a que ingresaste son incorrectos.",
            login.error_msg.text)

        wd.execute_script("arguments[0].maxLength=21;", login.input_password)
        self.assertEquals(
            "21", login.input_password.get_attribute("maxlength"))

        login.input_email.send_keys("prepaid5@blocket.se")
        login.input_password.send_keys("Lorem ipsum dolor sit")
        self.assertEquals(21, len(login.input_password.get_attribute("value")))
        login.button_login.click()
        tech_error = generic.TechnicalError(wd)
        tech_error.wait.until_present("title")
        self.assertEquals(tech_error.title.text, "Problemas t�cnicos")

    @tags('desktop', 'accounts', 'login')
    def test_login_from_li(self, wd):
        """ Test login from listing """
        listing = AdListDesktop(wd)
        listing.go("/region_metropolitana")

        listing.wait.until_visible('tab_region')

        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid5@blocket.se', '123123123')
        acc_bar.wait.until_visible('message')
        self.assertIn('Hola, BLOCKET', acc_bar.message.text)

        listing.wait.until_visible('tab_region')

    @tags('desktop', 'accounts', 'login')
    def test_login_from_wrong(self, wd):
        """ Test login from wrong page """
        page_nf = PageNotFound(wd)
        page_nf.go_page_not_found_desktop_url()

        page_nf.wait.until_visible('title')
        self.assertEqual(page_nf.title.text, 'Esta p�gina no existe')

        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid5@blocket.se', '123123123')
        acc_bar.wait.until_present('message')
        self.assertIn('Hola, BLOCKET', acc_bar.message.text)

        page_nf.wait.until_visible('title')
        self.assertEqual(page_nf.title.text, 'Esta p�gina no existe')

    @tags('desktop', 'accounts', 'login')
    def test_login_from_mis_avisos(self, wd):
        """ Test login from mis avisos page """
        my_ads = MyAds(wd)
        my_ads.go()

        title = wd.find_element_by_id('user_ads_title').text

        self.assertEqual(title, 'Mis avisos')

        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid5@blocket.se', '123123123')
        acc_bar.wait.until_present('message')
        self.assertIn('Hola, BLOCKET', acc_bar.message.text)

    @tags('desktop', 'accounts', 'login')
    def test_login_from_reset_password(self, wd):
        """ Test login from reset password page """
        wd.get(yapo.conf.SSL_URL + "/reset_password")

        h2Titles = wd.find_element_by_class_name("title-account").text
        self.assertEqual(h2Titles, '�Olvidaste tu contrase�a?')

        acc_bar = generic.AccountBar(wd)
        acc_bar.login('prepaid5@blocket.se', '123123123')
        acc_bar.wait.until_present('message')
        self.assertIn('Hola, BLOCKET', acc_bar.message.text)

    @tags('ad_insert', 'desktop', 'maps', 'preview', 'core1')
    def test_create_account_by_ad_insert(self, wd):
        """ Create account by ad insert """

        test_data_key = 'pendrive_with_create_account'
        test_name = '{0}.{1}'.format(type(self).__name__, self._testMethodName)
        test_data = JsonTool.read_test_data(test_data_key)

        # NEW AD INSERT
        email = '{0}@mailinator.com'.format(time.time())
        test_data['email'] = email
        test_data['email_verification'] = email
        insert_ad(DesktopAI(wd), ai_create_account=True, **test_data)
        AccountLoginTest.pro_data[test_name] = {'email': test_data['email'], 'subject': test_data['subject']}

        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, "�Gracias por publicar!")
        #self.assertTrue(page.success_message.is_displayed())

        mlt = Email.create(wd)
        mlt.check_account_activation(self, test_data['email'])
        dash = DashboardDesktop(wd)
        self.assertEqual(dash.title_legend.text, "Hola {0}".format(test_data['name']))

    @tags('ad_insert', 'desktop')
    def test_create_account_by_ad_insert_with_account_already_active(self, wd):
        """ Create account by ad insert with activated account,
            it shouldn't send the account activation mail """

        test_data_key = 'pendrive_with_create_account'
        test_name = '{0}.{1}'.format(type(self).__name__, self._testMethodName)
        test_data = JsonTool.read_test_data(test_data_key)

        test_data['email'] = 'prepaid5@blocket.se'
        test_data['email_verification'] = 'prepaid5@blocket.se'
        insert_ad(DesktopAI(wd), ai_create_account=True, **test_data)
        AccountLoginTest.pro_data[test_name] = {'email': test_data['email'], 'subject': test_data['subject']}

        page = AdInsertWithAccountSuccess(wd)
        self.assertTrue(page.success_message.is_displayed())

    @tags('desktop', 'accounts')
    def test_account_my_ads(self, wd):
        """Check accounts my ads """

        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123')
        dashboard_my_ads = DashboardMyAds(wd)
        dashboard_my_ads.next_page.click()
        dashboard_my_ads.wait.until_visible('list_inactive_ads')
        dashboard_my_ads.wait.until_visible('list_active_ads')
        self.assertEqual(len(dashboard_my_ads.list_inactive_ads), 5)
        self.assertEqual(len(dashboard_my_ads.list_active_ads), 5)

    @tags('desktop', 'accounts')
    def test_account_my_ads_pagination(self, wd):
        """Check accounts my ads pagination """

        try:
            utils.bconf_overwrite('*.*.accounts.dashboard.ads_per_page', 2)
            login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123')
            dashboard_my_ads = DashboardMyAds(wd)
            self.assertEqual(len(dashboard_my_ads.pages), 9)
            self.assertEqual(len(dashboard_my_ads.first_last_page), 1)
            self.assertEqual(dashboard_my_ads.current_page.text, '1')
            dashboard_my_ads.wait.until_present('slide_prev')
            dashboard_my_ads.wait.until_present('slide_next')
            dashboard_my_ads.next_page.click()
            self.assertEqual(dashboard_my_ads.current_page.text, '2')
            self.assertEqual(len(dashboard_my_ads.first_last_page), 2)

        finally:
            utils.bconf_overwrite('*.*.accounts.dashboard.ads_per_page', 10)

    @tags('desktop', 'accounts')
    def test_account_my_ads_pagination_do_not_exists(self, wd):
        """Check accounts my ads pagination do not exists"""

        login_and_go_to_dashboard(wd, 'prepaid3@blocket.se', '123123123')
        dashboard_my_ads = DashboardMyAds(wd)
        self.assertEqual(dashboard_my_ads.pagination.text.strip(), "")

    @tags('desktop', 'summary')
    def test_empty_summary_to_help(self, wd):
        """Check if empty summary has a link to 'mas info'
           and it displays help"""
        login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123')
        sp = SummaryPayment(wd)
        sp.go_summary()
        sp.press_link_to_more_info()
        wd.switch_to_window(wd.window_handles[1])
        help = Help(wd)
        faq = help.get_faq(Help.SELLERS, 17)
        faq.wait.until_visible('table')
        self.assertVisible(faq, 'table')

    @tags('desktop', 'dashboard', 'popup')
    def test_dashboard_lightbox_to_help(self, wd):
        """ Dashboard: Help popup takes to help page """
        login_and_go_to_dashboard(wd, 'prepaid5@blocket.se', '123123123')
        popup = BumpInfoLightboxDashboard(wd)
        popup.open()
        popup.press_more_info()
        wd.switch_to_window(wd.window_handles[1])
        help = Help(wd)
        faq = help.get_faq(Help.SELLERS, 17)
        faq.wait.until_visible('table')
        wd.switch_to_window(wd.window_handles[0])
        popup.close()
        self.assertNotVisible(popup, 'bump_more_info_popup')

    @tags('desktop', 'accounts')
    def test_steal_user_session(self, wd):
        """Try to steal other user's account via cookie manipulation"""
        victim = 'prepaid5@blocket.se'
        le_evil_cookie = {
            'name':     'acc_session',
            'value':    hashlib.sha1(victim.encode('utf8')).hexdigest(),
        }

        # Force profile data to be loaded on redis
        login = accounts.Login(wd)
        login_and_go_to_dashboard(wd, victim, 'wrong passworsss', skip_dashboard_wait=True)
        login.wait.until_visible('err_form')

        # Inject the evil cookie that will log us in and reload
        wd.add_cookie(le_evil_cookie)
        login.go()

        # Enjoy the joy of stolen account
        dashboard = DashboardDesktop(wd)
        dashboard.go()

        # We should NOT be logged in...
        self.assertNotVisible(dashboard, 'title_legend')
