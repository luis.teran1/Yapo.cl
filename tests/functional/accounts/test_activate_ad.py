# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.pages.desktop import DashboardMyAds, Login, SummaryPayment, PaymentVerify
from yapo.pages.simulator import Paysim
from yapo.pages import desktop
from yapo.pages import mobile


class DashActivateAdDesktop(yapo.SeleniumTest):
    platform = desktop
    snaps = ['accounts', 'diff_deactive_ads']
    user = 'prepaid5@blocket.se'
    passwd = '123123123'
    # ad state
    ad_inactive = 'Aviso inactivo'
    ad_pending_review = 'Aviso en revisi�n'
    ad_publish = 'Aviso publicado'
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('dashboard')
    def test_activate_ad(self, wd):
        """ Activate ad and check new state in dashboard"""
        subject = 'Adeptus Astartes Codex'
        list_id = '8000073'

        login = self.platform.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = self.platform.DashboardMyAds(wd)

        my_ads.go()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_inactive)

        my_ads.activate_ad(ad)

        my_ads.go()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_pending_review)

        utils.rebuild_index()

        my_ads.go()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_publish)

    @tags('dashboard')
    def test_activate_ad_with_bump(self, wd):
        """ Bump & activate ad, finish buying process and check new state in dashboard"""
        subject = 'Aviso de prepaid5'
        list_id = '8000065'

        login = self.platform.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = self.platform.DashboardMyAds(wd)
        my_ads.go_dashboard()
        my_ads.wait.until_loaded()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_inactive)
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        ad.option_bump_activate.click()
        summary = self.platform.SummaryPayment(wd)
        summary.wait.until_loaded()
        summary.wait.until_present('products_list_table')
        self.assertEqual(
            len(summary.get_displayed_table_data( table_name = 'products_list_table')), 1)
        self.assertIn('Subir Ahora', summary.product_row0.text)

        if self.platform == mobile:
            self.assertIn(subject[:16], summary.product_row0.text)
        else:
            self.assertIn(subject, summary.product_row0.text)

        summary.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
        payment_verify = PaymentVerify(wd)

        my_ads.go_dashboard()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_pending_review)
        utils.rebuild_index()
        my_ads = self.platform.DashboardMyAds(wd)
        my_ads.go_dashboard()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.get_status, self.ad_publish)


class DashActivateAdMobile(DashActivateAdDesktop):
    user_agent = utils.UserAgents.IOS
    platform = mobile

