# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo import utils
import yapo
import yapo.conf
import time
from yapo.pages.help import Help

class MobilePaymentSummary(yapo.SeleniumTest):

    snaps = ['accounts']
    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS

    account_user = "prepaid5@blocket.se"
    account_pass = "123123123"
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_mobile_summary_page_is_displayed(self, wd):
        """Check that mobile page is displayed"""
        utils.flush_redises()
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
        header = mobile.HeaderElements(wd)
        self.assertTrue(header.is_element_present('button_publish'))

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_empty_cart(self, wd):
        """Check the advertisement on summary when no products were added"""
        utils.flush_redises()
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
        self.assertEqual(summary.text_products_count.text, "0")

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_add_products_to_summary(self, wd):
        """Check that added product are shown in the summary page"""
        utils.flush_redises()
        list_ids = ['8000073', '8000065', '6000667']
        prod_ids = ['1', '2', '3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.wait.until_not_present('text_empty_cart_message')
        self.assertTrue(summary.rows_product_list)
        for row in summary.rows_product_list:
            inputs = row.find_elements_by_tag_name('input')
            self.assertTrue(inputs[0].get_attribute('value') in list_ids)
            self.assertTrue(inputs[1].get_attribute('value') in prod_ids)

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_ad_deletion(self, wd):
        """Check ad deletion on summary"""
        utils.flush_redises()
        list_ids = ['8000073', '8000065', '6000667']
        prod_ids = ['1', '2', '3']
        total_prices = ['$ 1.500', '$ 4.900', '$ 14.900']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        self.assertEquals(summary.text_products_count.text, '3')
        # We iterate on reversed order
        for row in reversed(range(len(summary.rows_product_list))):
            self.assertEquals(summary.text_amount.text, total_prices[row])
            tr = wd.find_element_by_id('row' + str(row))
            tr.find_element_by_css_selector('.summaryTable-closeIcon').click()
            tr.find_element_by_css_selector('.summaryTable-cell.__delete').click()
            time.sleep(3)
        self.assertEquals(summary.text_products_count.text, '0')

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_check_document_type(self, wd):
        """Check that the document type match with the role of the account
        'boleta' for 'persona' and 'factura' for 'profesional'"""
        utils.flush_redises()
        list_ids = ['8000073', '8000065', '6000667']
        prod_ids = ['1', '2', '3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        profile = mobile.DashboardProfile(wd)
        profile.go_profile()
        profile.radio_persona.click()
        wd.execute_script("window.scrollTo({0}, {1})".format(profile.button_save.location['x'], profile.button_save.location['y']))
        wd.execute_script("arguments[0].click()", profile.button_save)
        profile.wait.until_loaded()
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
        time.sleep(10)
        self.assertTrue(summary.radio_bill.is_selected())
        self.assertFalse(summary.radio_invoice.is_selected())
        profile.go_profile()
        profile.radio_profesional.click()
        wd.execute_script("window.scrollTo({0}, {1})".format(profile.button_save.location['x'], profile.button_save.location['y']))
        wd.execute_script("arguments[0].click()", profile.button_save)
        profile.wait.until_loaded()
        summary.go_summary()
        self.assertFalse(summary.radio_bill.is_selected())
        self.assertTrue(summary.radio_invoice.is_selected())

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_change_document_type(self, wd):
        """Check that the document type change when the user click on other type
        of document"""
        utils.flush_redises()
        list_ids = ['8000073']
        prod_ids = ['1']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
    
        self.assertFalse(summary.radio_bill.is_selected())
        self.assertTrue(summary.radio_invoice.is_selected())
        summary.choose_bill()
        self.assertTrue(summary.radio_bill.is_selected())
        self.assertFalse(summary.radio_invoice.is_selected())

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_display_email(self, wd):
        """Check that the email field come hidden and is displayable"""
        utils.flush_redises()
        list_ids = ['8000073']
        prod_ids = ['1']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
        self.assertTrue(summary.input_email.is_displayed())

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_full_payment(self, wd):
        """Check the full payment process (end to end)"""
        utils.flush_redises()
        list_ids = ['8000073', '8000065', '6000667']
        prod_ids = ['1', '2', '3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.sticky_bar.pay()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = mobile.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')
        pay_verify.buy_head.click()
        table_values = pay_verify.get_table_data('buy_table')
        search_for = [
            ["Tipo de pago", "Cr�dito"],
            ["Tipo de cuotas", "Sin Cuotas"]
        ]
        for element in search_for:
            self.assertIn( element, table_values)

    @tags('mobile', 'accounts', 'payment', 'summary')
    def test_check_validators(self, wd):
        """Check javascript validators for inputs"""
        utils.flush_redises()
        list_ids = ['8000073', '8000065', '6000667']
        prod_ids = ['1', '2', '3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.input_name.clear()
        summary.input_giro.clear()
        summary.input_rut.clear()
        summary.input_address.clear()
        self.assertEqual(summary.error_name_account.text, 'Escribe tu nombre')
        self.assertEqual(summary.error_giro.text, 'Escribe tu giro')
        self.assertEqual(summary.error_rut.text, 'Escribe tu rut')
        self.assertEqual(summary.error_address.text, 'Escribe tu direcci�n')
        summary.input_rut.send_keys('1-0')
        summary.sticky_bar.btn_pay.click()
        summary.radio_invoice.click()
        self.assertEqual(summary.error_rut.text, 'Escribe un rut v�lido')
        summary.input_email.clear()
        summary.input_email.send_keys('asda')
        summary.radio_invoice.click()
        self.assertEqual(summary.error_email_account.text, 'Confirma que el email est� en el formato correcto')
        summary.input_email.clear()
        summary.input_email.send_keys('asda@asda')
        summary.radio_invoice.click()
        self.assertEqual(summary.error_email_account.text, 'Confirma que el email est� en el formato correcto')
        summary.input_email.clear()
        summary.input_email.send_keys('asda@asda.cl')
        summary.radio_invoice.click()
        self.assertEqual(summary.error_email_account.text, '')

    @tags('mobile', 'summary', 'help')
    def test_empty_summary_to_help(self, wd):
        """Check if empty summary (in mobile) has a link to 'mas info' and it displays help"""
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        sp = mobile.SummaryPayment(wd)
        sp.go_summary()
        sp.press_link_to_more_info()
        sp.wait.until(lambda x: len(wd.window_handles) > 1)
        wd.switch_to_window(wd.window_handles[1])
        help = Help(wd)
        faq = help.get_faq(Help.SELLERS, 17)
        faq.wait.until_visible('table')
        self.assertVisible(faq, 'table')

    def _add_list_of_products(self, wd, list_ids, prod_ids):
        """Add new products to the summary page (lists must have the same length)"""
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = mobile.SummaryPayment(wd)
        for i in range(0, len(list_ids)):
            summary.add_product(list_ids[i], prod_ids[i])
        return summary
