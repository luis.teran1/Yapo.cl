# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdDeletePage, AdViewDesktop
from yapo.pages import desktop
from yapo.pages.generic import SentEmail
from yapo.pages.desktop import HeaderElements, MyAds
import yapo


class AccountLoggedAdViewActions(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('desktop', 'adview', 'login')
    def test_adview_ad_edit_wrong_password(self, wd):
        """ Check user not logged when doing ad edit in adview with wrong password """
        adview = AdViewDesktop(wd)
        edit_password_page = desktop.EditPasswordPage(wd)
        adview.go('8000073')
        adview.ad_admin_edit_link.click()
        edit_password_page.validate_password('wrongpassword')

        header = HeaderElements(wd)
        header.wait.until_present('button_my_account')
        self.assertEqual(header.who_is_logged(), '')

    @tags('desktop', 'adview', 'login')
    def test_adview_ad_edit_with_correct_password(self, wd):
        """ Check user logged when doing ad edit in adView with correct password"""
        adview = AdViewDesktop(wd)
        edit_password_page = desktop.EditPasswordPage(wd)
        adview.go('8000073')
        adview.ad_admin_edit_link.click()
        edit_password_page.validate_password('123123123')

        header = HeaderElements(wd)
        self.assertEqual(header.who_is_logged(), 'BLOCKET')

    @tags('desktop', 'adview', 'login')
    def test_adview_edit_forgot_password(self, wd):
        """ Check user logged from mail when doing ad edit with forgot password"""
        adview = AdViewDesktop(wd)
        adview.go('8000073')
        adview.ad_admin_edit_link.click()
        edit_password_page = desktop.EditPasswordPage(wd)
        edit_password_page.forgot_password.click()
        edit_password_page.fill_form(
            email_forgot_password="prepaid5@blocket.se")
        edit_password_page.email_submit_button.click()

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_css_selector(
            "a.edit").get_attribute("href")
        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual('Hola, BLOCKET', header.button_logged_user.text)

    @tags('desktop', 'adview', 'login')
    def test_adview_delete_forgot_password(self, wd):
        """ Check user logged from mail when doing ad delete with forgot password"""

        delete_page = AdDeletePage(wd)
        delete_page.go('8000070')
        delete_page.select_reason()
        delete_page.select_timer()
        delete_page.forgot_password_link.click()

        forgot_password_page = desktop.EditPasswordPage(wd)
        forgot_password_page.fill_form(
            email_forgot_password="prepaid3@blocket.se")
        forgot_password_page.email_submit_button.click()

        sent_email = SentEmail(wd)
        sent_email.go()
        row = sent_email.body.find_elements_by_css_selector(
            'table tr.stripe_0')[0]
        delete_link = row.find_element_by_css_selector(
            'td:nth-child(5) a').get_attribute('href')
        wd.get(delete_link)
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, OtherRegionAccount', header.button_logged_user.text)

    @tags('desktop', 'adview', 'login')
    def test_check_login_from_my_ads_mail(self, wd):
        """ Check user logged from my ads mail"""

        myads = MyAds(wd)
        myads.go_and_request('many@ads.cl')
        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_css_selector(
            "a.edit").get_attribute("href")
        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, ManyAdsAccount', header.button_logged_user.text)
