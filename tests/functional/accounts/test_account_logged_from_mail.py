# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel
from yapo.pages.generic import SentEmail
from yapo.pages.desktop import HeaderElements
from yapo.steps import trans
from yapo import utils
import time
import yapo
import yapo.conf


class AccountLoginFromMail(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('desktop', 'accounts', 'login')
    def test_check_user_logged_from_refused_mail(self, wd):
        """ Check user logged from refuse mail """
        subject = 'Aviso de prueba 12'
        email = 'many@ads.cl'

        trans.review_ad(email, subject, action = 'refuse')

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_partial_link_text(
            "aqu�").get_attribute("href")

        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual(
            'Hola, ManyAdsAccount', header.button_logged_user.text)

    @tags('desktop', 'accounts', 'login')
    def test_check_user_logged_from_post_refuse_ad(self, wd):
        """ Check user logged from post refuse ad mail """

        trans.review_ad("prepaid5@blocket.se", "Adeptus Astartes Codex", "refuse", filter="published")

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_partial_link_text(
            "aqu�").get_attribute("href")

        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual('Hola, BLOCKET', header.button_logged_user.text)

    @tags('desktop', 'accounts', 'login')
    def test_check_user_logged_from_half_time_mail(self, wd):
        """ For a looged user, Half time email was sent, Email contains the delete link (puedes borrarlo haciendo clic aqui), Go to link and user is BLOCKET """
        utils.trans('admail', ad_id='95', mail_type='half_time')
        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_id("delete").get_attribute("href")

        wd.get(link)
        header = HeaderElements(wd)
        self.assertEqual('Hola, BLOCKET', header.button_logged_user.text)
