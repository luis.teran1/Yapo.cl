# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo.pages.desktop import DashboardMyAds, Login
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.generic import AdReplyUpdateStats


class UtilAdReplies(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    user = 'android@yapo.cl'
    passwd = '123123123'
    bconfs = {
        "*.adreply.spam.minutes": "0",
        "*.trans.flushmail.msg_center.send_enabled": "0"
    }

    def wait_for_dashboard_contact_count(self, wd, list_id, contacts):
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        loops = 0
        while dashboard.get_contact_count(list_id) != str(contacts):
            time.sleep(1)
            dashboard.go_dashboard()
            loops = loops + 1
            if loops > 20:
                raise Exception("adreply update does not works!")


class DashboardAdRepliesDesktop(UtilAdReplies):

    @tags('desktop', 'dashboard', 'adreply')
    def test_check_adreply_domain_with_safe(self, wd):
        """ Check the dashboard contacts counter,
            using the adreply_update_stats template """
        user = 'user.01@schibsted.cl'
        list_id = '8000084'
        login = Login(wd)

        dashboard = DashboardMyAds(wd)
        adreply = AdReplyUpdateStats(wd)
        login.login(user, self.passwd)

        dashboard.go_dashboard()
        self.assertEquals("0", dashboard.get_contact_count(list_id))
        adreply.go(list_id)
        self.wait_for_dashboard_contact_count(wd, list_id, 1)
        self.assertEquals("1", dashboard.get_contact_count(list_id))
        adreply.go(list_id)
        adreply.go(list_id)
        self.wait_for_dashboard_contact_count(wd, list_id, 3)
        self.assertEquals("3", dashboard.get_contact_count(list_id))

    @tags('desktop', 'dashboard', 'adreply')
    def test_check_adreply_domain_without_safe(self, wd):
        """ Check the dashboard contacts counter,
            using domain not handled safe, so counted by flushmail trans."""
        list_id = '8000066'
        ad_reply_params = {
            'name': 'yolo',
            'email': 'yolo@cabral.cl',
            'phone': '123123123',
            'message': 'yolito'
        }

        login = Login(wd)
        dashboard = DashboardMyAds(wd)
        adreply = AdReplyUpdateStats(wd)
        login.login(self.user, self.passwd)

        dashboard.go_dashboard()
        self.assertEquals("0", dashboard.get_contact_count(list_id))

        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)

        self.wait_for_dashboard_contact_count(wd, list_id, 1)
        self.assertEquals("1", dashboard.get_contact_count(list_id))

        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)
        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)

        self.wait_for_dashboard_contact_count(wd, list_id, 3)
        self.assertEquals("3", dashboard.get_contact_count(list_id))


class DashboardDesktopMsgCenter(UtilAdReplies):

    bconfs = {
        "*.adreply.spam.minutes": "0",
        "*.trans.flushmail.msg_center.send_enabled": "1"
    }

    @tags('desktop', 'dashboard', 'adreply')
    def test_check_adreply_domain_without_safe_msg_center(self, wd):
        """ Check the dashboard contacts counter,
            using domain not handled safe,
            so counted by flushmail trans with msg-center active."""
        list_id = '8000066'
        ad_reply_params = {
            'name': 'yolo',
            'email': 'yolo@cabral.cl',
            'phone': '123123123',
            'message': 'yolito'
        }

        login = Login(wd)
        dashboard = DashboardMyAds(wd)
        adreply = AdReplyUpdateStats(wd)
        login.login(self.user, self.passwd)

        dashboard.go_dashboard()
        self.assertEquals("0", dashboard.get_contact_count(list_id))

        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)

        self.wait_for_dashboard_contact_count(wd, list_id, 1)
        self.assertEquals("1", dashboard.get_contact_count(list_id))

        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)
        ad_view.go(list_id)
        ad_view.send_adreply(**ad_reply_params)

        self.wait_for_dashboard_contact_count(wd, list_id, 3)
        self.assertEquals("3", dashboard.get_contact_count(list_id))
