# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import accounts, generic
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import utils
from yapo.steps.accounts import login_from_bar
import yapo
import yapo.conf

class CrossAccount(yapo.SeleniumTest):

    """
    Tests for actions over an account while logged into other
    """

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts'])

    @tags('desktop', 'accounts')
    def test_cross_account_forgot_password(self, wd):
        """Reset password for an account while logged in another account"""
        forgot = accounts.ForgotPassword(wd)
        forgot.go()
        forgot.email_input.send_keys("prepaid5@blocket.se")
        forgot.submit_btn.click()
        reset_password_url = utils.search_mails(
            yapo.conf.SSL_URL + "/reset_password/ask_password/.*")
        login_from_bar(wd, "many@ads.cl", "123123123")
        wd.get(reset_password_url)
        loginbar = generic.AccountBar(wd)
        reset = accounts.ResetPassword(wd)
        reset.fill_form(password_input="11111111",
                        password_verification_input="11111111")
        reset.submit_btn.click()
        self.assertEqual(loginbar.message.text, "Hola, BLOCKET")
        wd.get(yapo.conf.SSL_URL + "/logout?exit=1")
        login_and_go_to_dashboard(wd, "prepaid5@blocket.se", "11111111")
        self.assertEqual(
            loginbar.message.text, "Hola, BLOCKET", "Login failed with new password")

    @tags('desktop', 'accounts')
    def test_cross_account_activate(self, wd):
        """Activate an account while logged into another account"""
        create = accounts.Create(wd)
        create.go()
        create.fill_form(person_radio=True, name="Yolo", phone="87654321", email="acc_pending@yapo.cl",
                         password="11111111", password_verify="11111111",
                         accept_conditions=True)
        create.submit_btn.click()
        activation_url = utils.search_mails(
            yapo.conf.SSL_URL + "/cuenta/confirm.*")

        login_and_go_to_dashboard(wd, "prepaid5@blocket.se", "123123123")
        loginbar = generic.AccountBar(wd)
        self.assertEqual(loginbar.message.text, "Hola, BLOCKET")
        wd.get(activation_url)
        self.assertEqual(loginbar.message.text, "Hola, PendingAccount")


