# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertSuccess
from yapo.pages.desktop import DashboardMyAds, Login, AdEdit, MyAds, SummaryPayment, PaymentVerify, DeleteUnpaidModal
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop_list import AdViewDesktop, AdDeletePage
from yapo.pages.generic import SentEmail, AdReplyUpdateStats
from yapo.pages.newai import DesktopAI
from yapo.steps import trans, insert_an_ad
from yapo import steps
import time


class DashboardAdStatesDesktop(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    user = 'android@yapo.cl'
    passwd = '123123123'
    bconfs = {
        "*.adreply.spam.minutes": "0",
        "*.trans.flushmail.msg_center.send_enabled": "0",
        "*.*.category_settings.price.1.7020.1.value": "price:5000",
        "*.*.insertingfee.type.1.enabled": "1",
        '*.*.movetoapps_splash_screen.enabled': '0',
        "*.*.payment_settings.product.1.9.*.value": "price:2000",
        "*.*.payment_settings.product.1.1.1220.value": "price:2750"
    }

    status = {'published': 'Aviso publicado',
              'reviewing': 'Revisando edici�n',
              'deleting': 'Borrando aviso',
              'inactive': 'Aviso inactivo'}

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_store_3shops'])
        utils.rebuild_asearch()
        utils.flush_redises()

    def test_active_hidden_active(self, wd):
        """Check Dashboard Desktop with hide and show ad from CP"""
        subject = 'Aviso de usuario Android'
        list_id = '8000066'
        status = self.status['published']

        utils.rebuild_asearch()
        login = Login(wd)
        login.login(self.user, self.passwd)
        my_ads = DashboardMyAds(wd)
        self.assertEqual(my_ads.search_status_by_subject(subject), status)

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(None, list_id)
        cpanel.logout()

        my_ads.go_dashboard()
        self.assertNotIn([subject, status], my_ads.create_ad_list())

        ad_view = AdViewDesktop(wd)
        ad_view.go(list_id)
        ad_view.wait.until_present('subject')
        self.assertEqual(ad_view.subject.text, subject)

        utils.rebuild_index()

        ad_view.go(list_id)
        ad_view.wait.until_present('ad_not_found_title')
        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

        my_ads.go_dashboard()
        self.assertNotIn([subject, status], my_ads.create_ad_list())

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_show_ad(None, list_id)
        cpanel.logout()

        my_ads.go_dashboard()
        self.assertNotIn([subject, status], my_ads.create_ad_list())

        ad_view.go(list_id)
        ad_view.wait.until_present('ad_not_found_title')
        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

        utils.rebuild_index()

        my_ads.go_dashboard()
        self.assertIn([subject, status], my_ads.create_ad_list())

        ad_view.go(list_id)
        ad_view.wait.until_present('subject')
        self.assertEqual(ad_view.subject.text, subject)

    @tags('desktop', 'dashboard')
    def test_edit_review_admin_delete(self, wd):
        """ Admin delete an ad with a edition, the ad must disapear from dashboard after rebuild index"""
        subject = 'Lechuza embalsamada'
        list_id = '8000052'
        my_ads = DashboardMyAds(wd)

        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(wd, my_ads, list_id, subject)

        # Delete from CP
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_delete_ad(list_id=list_id)
        cpanel.logout()
        my_ads.go_dashboard()
        self.assertEqual(
            my_ads.search_status_by_subject(subject), self.status['deleting'])

        utils.rebuild_index()

        my_ads.go_dashboard()
        self.assertNotIn(
            [subject, self.status['deleting']], my_ads.create_ad_list())

    @tags('desktop', 'dashboard')
    def test_edit_review_user_delete(self, wd):
        """ User delete an ad with a edition, the ad must disapear from dashboard after rebuild index """
        subject = 'Lote de motos en miniatura'
        list_id = '8000055'

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(wd, my_ads, list_id, subject)

        # delete and review if ad is inactive
        self._delete_and_check_inactive_status(wd, my_ads, list_id, subject)

    @tags('desktop', 'dashboard')
    def test_edit_review_admin_hide(self, wd):
        """ Admin hide an ad with edition, the ad must disapear from dashboard """
        subject = 'Juego de 7 Llaves Antiguas'
        list_id = '8000053'

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(wd, my_ads, list_id, subject)

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(None, list_id)
        cpanel.logout()

        my_ads.go_dashboard()
        self.assertNotIn(
            [subject, self.status['reviewing']], my_ads.create_ad_list())

    @tags('desktop', 'dashboard')
    def test_hide_admin_delete(self, wd):
        """ Admin delete an ad in hidden status, the ad reapears as deleting and after rebuild index disapear  """
        subject = 'Expectacular Blackberry 9810 o Permuta'
        list_id = '8000034'

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(wd, my_ads, list_id, subject)

        # delete and review if ad is inactive
        self._delete_and_check_inactive_status(wd, my_ads, list_id, subject)

        utils.rebuild_index()

        my_ads.go_dashboard()
        self.assertNotIn(
            [subject, self.status['reviewing']], my_ads.create_ad_list())

    @tags('desktop', 'dashboard')
    def test_edit_review_user_email_delete(self, wd):
        """ User edit ad and then delete the ad from email link """
        subject = 'Call of Duty Black ops II Xbox 360'
        list_id = '8000037'

        myads = MyAds(wd)
        myads.go_and_request(self.user)
        _, link_del = self.get_ad_links_from_mail(wd, subject)

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(wd, my_ads, list_id, subject)

        # delete and review if ad is inactive
        self._delete_and_check_inactive_status(wd, my_ads, list_id, subject)

        utils.rebuild_index()

        my_ads.go_dashboard()
        self.assertNotIn(
            [subject, self.status['reviewing']], my_ads.create_ad_list())

    @tags('desktop', 'dashboard')
    def test_edit_review_user_email_edit(self, wd):
        """ User edit ad and then edit the ad from email link """
        subject = 'Tetera y Azucarero'
        list_id = '8000039'

        myads = MyAds(wd)
        myads.go_and_request(self.user)
        link_edit, _ = self.get_ad_links_from_mail(wd, subject)

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        edit = AdEdit(wd)
        # edit ad and check if status is reviewing
        self._edit_and_check_reviewing_status(
            wd, my_ads, list_id, subject, edit)

        wd.get(link_edit)
        edit.insert_ad(body='edited again')
        edit.submit.click()

        my_ads.go_dashboard()
        self.assertEqual(
            my_ads.search_status_by_subject(subject), self.status['reviewing'])

    @tags('desktop', 'dashboard')
    def test_delete_user_email_delete(self, wd):
        """ User delete ad and then delete the ad from email link """
        subject = 'Asistente RR HH'
        list_id = '8000046'

        myads = MyAds(wd)
        myads.go_and_request(self.user)
        _, link_del = self.get_ad_links_from_mail(wd, subject)

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # delete and review if ad is inactive
        self._delete_and_check_inactive_status(wd, my_ads, list_id, subject)

        ad_view = AdViewDesktop(wd)
        wd.get(link_del)

        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

    @tags('desktop', 'dashboard')
    def test_delete_user_email_edit(self, wd):
        """ User delete ad and then edit the ad from email link """
        subject = 'Control de calidad en metalurgica'
        list_id = '8000044'

        myads = MyAds(wd)
        myads.go_and_request(self.user)
        link_edit, _ = self.get_ad_links_from_mail(wd, subject)

        my_ads = DashboardMyAds(wd)
        # login and review if ad is published
        self._login_and_check_published_ad(wd, my_ads, subject)

        # delete and review if ad is inactive
        self._delete_and_check_inactive_status(wd, my_ads, list_id, subject)

        ad_view = AdViewDesktop(wd)
        wd.get(link_edit)

        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

    @tags('desktop', 'dashboard')
    def test_insert_refuse_refuse(self, wd):
        """ Check the counter of ads on dashboard, when edit refused are refused again the counter of ads must be 0  """
        login = Login(wd)
        login.login('no-ads@yapo.cl', '123123123')
        my_ads = DashboardMyAds(wd)
        self.assertEquals(my_ads.flag_ads_counter.text, "0")

        insert_an_ad(wd, AdInsert=DesktopAI, submit=True,
                subject='ropa de bebe',
                body="ropa de bebe",
                region=15,
                communes=331,
                category=3040,
                price=999666,
                logged=True)

        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "1")

        # Refuse on CP
        trans.review_ad('no-ads@yapo.cl', 'Ropa de bebe', action = 'refuse')
        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "0")

        email = SentEmail(wd)
        email.go()
        link_email = email.body.find_element_by_link_text(
            "aqu�").get_attribute("href")

        wd.get(link_email)
        edit = AdInsert(wd)
        edit.fill_form(body='edited', accept_conditions=True)
        edit.submit.click()
        success = AdInsertSuccess(wd)
        self.assertEquals(success.title.text, success.SUCCESS_MESSAGE)

        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "1")

        # Refuse on CP
        trans.review_ad('no-ads@yapo.cl', 'Ropa de bebe', action = 'refuse')

        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "0")

    def test_pay_unpaid(self, wd):

        subject = 'Empleo de bebe'
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.status.text, 'Pendiente de pago')
        self.assertEqual(ad.activate_tip.text, 'Paga este aviso de empleo para que est� activo')

        ad.go_to_pay()
        steps.pay(wd, SummaryPayment, PaymentVerify)

        my_ads.go_dashboard()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.status.text, 'Aviso en revisi�n')
        self.assertNotVisible(ad, 'activate_tip')
        self.assertNotVisible(ad, 'btn_unpaid')

    def test_delete_unpaid(self, wd):

        self._insert_an_inmo_ad(wd)
        login = Login(wd)
        login.logout()
        login.wait.until(lambda x: trans.get_ad('prepaid5@blocket.se', 'Aviso if inmo'))

        subject = 'Empleo de bebe'
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        ad = my_ads.go_to_delete_ad(subject)

        delete_unpaid_modal = DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        ad_list = my_ads.create_ad_list()

        delete_unpaid_modal.reject()
        self.assertNotVisible(delete_unpaid_modal, 'close_button')
        self.assertIn([subject, ad.status.text], ad_list)

        ad = my_ads.go_to_delete_ad(subject)
        delete_unpaid_modal.confirm()

        self.assertNotVisible(delete_unpaid_modal, 'close_button')
        self.assertEquals(len(ad_list) - 1, len(my_ads.create_ad_list()))

    def test_delete_unpaid_clear_cart_without_other_items(self, wd):

        subject = "Aviso a eliminarse"
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        ad = my_ads.go_to_delete_ad(subject)

        delete_unpaid_modal = DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        delete_unpaid_modal.confirm()
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        summary_payment = SummaryPayment(wd)
        self.assertNotVisible(summary_payment, "bar_sticky")

    def test_delete_unpaid_clear_cart_with_more_items(self, wd):

        subject = "Aviso a eliminarse"
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        products_to_add = {
            6000667: [utils.Products.BUMP],
            8000073: [utils.Products.LABEL]
        }
        my_ads.select_products_by_ids(products_to_add)

        ad = my_ads.go_to_delete_ad(subject)
        delete_unpaid_modal = DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        delete_unpaid_modal.confirm()

        summary_payment = SummaryPayment(wd)
        self.assertVisible(summary_payment, "bar_sticky")
        self.assertEqual(summary_payment.text_amount.text, "4.750")

    def get_ad_links_from_mail(self, wd, subject):
        email = SentEmail(wd)
        email.go()
        subjects_list = email.body.find_elements_by_css_selector(".subject")
        link_edit_list = email.body.find_elements_by_css_selector(".edit")
        link_del_list = email.body.find_elements_by_css_selector(".delete")
        position = -1
        for i in range(len(subjects_list)):
            if subjects_list[i].text == subject:
                position = i
                break
        if position == -1:
            return None
        else:
            return [link_edit_list[position].get_attribute("href"), link_del_list[position].get_attribute("href")]


    def _login_and_check_published_ad(self, wd, my_ads, subject):
        login = Login(wd)
        login.login(self.user, self.passwd)
        self.assertEqual(
            my_ads.search_status_by_subject(subject), self.status['published'])

    def _edit_and_check_reviewing_status(self, wd, my_ads, list_id, subject, edit=None):
        if not edit:
            edit = AdEdit(wd)
        edit.go(list_id)
        edit.insert_ad(body='edited')
        edit.submit.click()
        my_ads.go_dashboard()
        self.assertEqual(
            my_ads.search_status_by_subject(subject), self.status['reviewing'])

    def _delete_and_check_inactive_status(self, wd, my_ads, list_id, subject):
        ad_delete = AdDeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()

        my_ads.go_dashboard()
        self.assertEqual(
            my_ads.search_status_by_subject(subject), self.status['inactive'])

    def _insert_an_unpaid_ad(self, wd, subject):
        trans.make_user_pro(1, 'jobs')

        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')

        my_ads = DashboardMyAds(wd)

        insert_an_ad(wd, AdInsert=DesktopAI, submit=True,
                subject=subject,
                body="para bebe!",
                region=15,
                communes=331,
                category=7020,
                jc=['20'],
                price=999666,
                logged=True)

        my_ads.go_dashboard()
        return my_ads

    def _insert_an_inmo_ad(self, wd):
        trans.make_user_pro(1, 'inmo')

        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')

        insert_an_ad(wd, AdInsert=DesktopAI, submit=True,
                subject="Aviso if inmo",
                body="casita para bebe!",
                region=15,
                communes=331,
                category=1220,
                price=999666,
                logged=True)
