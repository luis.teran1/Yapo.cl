# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop import DashboardMyAds, Login
from yapo.pages.control_panel import ControlPanel
from yapo.pages.desktop_list import AdViewDesktop, AdDeletePage
from yapo.pages.mobile_list import AdViewMobile
from yapo.pages.generic import SentEmail
from yapo.steps import trans
from yapo import steps
import time


class DashboardAdStatesPaginationDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    bconfs = {
        '*.*.accounts.dashboard.ads_per_page': '1',
    }
    user = 'no-ads@yapo.cl'
    passwd = '123123123'

    def test_check_insert_refuse_refuse_pagination(self, wd):
        """ Check the pagination of ads on dashboard, when edit refused are refused  """
        utils.flush_redises()
        login = Login(wd)
        login.login('no-ads@yapo.cl', '123123123')
        my_ads = DashboardMyAds(wd)
        self.assertEquals(my_ads.flag_ads_counter.text, "0")
        my_ads.wait.until_not_present('next_page')

        steps.insert_an_ad(wd,
            subject='Aviso aceptado',
            body="aviso aceptado",
            region=15,
            communes=331,
            category=3040,
            price=999666,
            accept_conditions=True,
            email='no-ads@yapo.cl',
            passwd="$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6")

        trans.review_ad("no-ads@yapo.cl", 'Aviso aceptado')

        utils.rebuild_index()
        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "1")

        steps.insert_an_ad(wd,
            subject='Ropa de bebe',
            body="ropa de bebe",
            region=15,
            communes=331,
            category=3040,
            price=999666,
            accept_conditions=True,
            email='no-ads@yapo.cl',
            passwd="$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6")

        my_ads.go_dashboard()
        time.sleep(5)
        self.assertEquals(my_ads.flag_ads_counter.text, "2")
        my_ads.wait.until_present('next_page')

        self.assertEqual(
            my_ads.search_status_by_subject("Ropa de bebe"), "Aviso en revisi�n")
        my_ads.next_page.click()
        self.assertEqual(
            my_ads.search_status_by_subject("Aviso aceptado"), "Aviso publicado")

        # Refuse on CP
        trans.review_ad("no-ads@yapo.cl", 'Ropa de bebe', action = 'refuse')
        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "1")
        my_ads.wait.until_not_present('next_page')
        self.assertEqual(
            my_ads.search_status_by_subject("Aviso aceptado"), "Aviso publicado")

        email = SentEmail(wd)
        email.go()
        link_email = email.body.find_element_by_link_text(
            "aqu�").get_attribute("href")

        wd.get(link_email)
        edit = AdInsert(wd)
        edit.fill_form(body='edited', accept_conditions=True)
        edit.submit.click()

        my_ads.go_dashboard()
        my_ads.wait.until_visible('flag_ads_counter')
        self.assertEquals(my_ads.flag_ads_counter.text, "2")
        my_ads.wait.until_present('next_page')
        self.assertEqual(
            my_ads.search_status_by_subject("Ropa de bebe"), "Aviso en revisi�n")
        my_ads.next_page.click()
        self.assertEqual(
            my_ads.search_status_by_subject("Aviso aceptado"), "Aviso publicado")

        # Refuse on CP
        trans.review_ad("no-ads@yapo.cl", 'Ropa de bebe', action = 'refuse')

        my_ads.go_dashboard()
        self.assertEquals(my_ads.flag_ads_counter.text, "1")
        my_ads.wait.until_not_present('next_page')
        self.assertEqual(
            my_ads.search_status_by_subject("Aviso aceptado"), "Aviso publicado")
