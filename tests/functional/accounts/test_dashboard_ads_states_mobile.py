# -*- coding: latin-1 -*-
import yapo, nose_selenium
from yapo import utils
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.control_panel import ControlPanel
from yapo.pages.mobile_list import AdViewMobile
from yapo.pages.generic import SentEmail
from yapo.pages.ad_insert import AdInsertResult
from yapo.steps import trans
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import steps


class DashboardAdStatesMobile(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    user = 'android@yapo.cl'
    passwd = '123123123'
    user_agent = utils.UserAgents.IOS
    ad_status = {'published': 'Aviso publicado',
                 'reviewed': 'Revisando edici�n',
                 'deleted': 'Aviso inactivo'}
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_store_3shops'])
        utils.rebuild_asearch()
        utils.flush_redises()

    @tags('mobile', 'dashboard', 'ad_edit', 'controlpanel')
    def test_check_hidden_process_mobile(self, wd):
        """Check Dashboard Mobile with hide and show ad from CP"""
        ad = ['Lote de motos en miniatura', 'Aviso publicado']
        list_id = '8000055'

        utils.rebuild_asearch()

        mobile_login = mobile.Login(wd)
        mobile_login.login(self.user, self.passwd)

        mobile_my_ads = mobile.DashboardMyAds(wd)
        dash_ad = mobile_my_ads.search_ad_by_subject(ad[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad[1])

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(None, list_id)
        cpanel.logout()

        mobile_my_ads.go_dashboard()
        self.assertNotIn(ad, mobile_my_ads.create_ad_list())

        mobile_ad_view = AdViewMobile(wd)
        mobile_ad_view.go(list_id)
        self.assertEqual(mobile_ad_view.subject.text, ad[0])

        utils.rebuild_index()

        mobile_ad_view.go(list_id)
        self.assertTrue(mobile_ad_view.ad_not_found_title.is_displayed())

        mobile_my_ads.go_dashboard()
        self.assertNotIn(ad, mobile_my_ads.create_ad_list())

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_show_ad(None, list_id)
        cpanel.logout()

        mobile_my_ads.go_dashboard()
        self.assertNotIn(ad, mobile_my_ads.create_ad_list())

        mobile_ad_view.go(list_id)
        self.assertTrue(mobile_ad_view.ad_not_found_title.is_displayed())

        utils.rebuild_index()

        mobile_my_ads.go_dashboard()
        self.assertIn(ad, mobile_my_ads.create_ad_list())

        mobile_ad_view.go(list_id)
        self.assertEqual(mobile_ad_view.subject.text, ad[0])

    @tags('mobile', 'dashboard', 'ad_edit', 'ad_delete', 'controlpanel')
    def test_edit_review_admin_delete_mobile(self, wd):
        """ Admin delete an ad with a edition, the ad must disapear from dashboard mobile after rebuild index"""
        ad1 = ['Lechuza embalsamada', 'Aviso publicado']
        ad2 = ['Lechuza embalsamada', 'Revisando edici�n']
        ad3 = ['Lechuza embalsamada', 'Borrando aviso']
        list_id = '8000052'

        utils.rebuild_asearch()

        mobile_my_ads = mobile.DashboardMyAds(wd)
        mobile_login = mobile.Login(wd)
        mobile_login.login(self.user, self.passwd)
        dash_ad = mobile_my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # edition
        passedit = mobile.EditPasswordPage(wd)
        passedit.go(list_id)

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited',phone='5465768', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        mobile_my_ads.go_dashboard()
        dash_ad = mobile_my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        # Delete from CP
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_delete_ad(list_id=list_id)
        cpanel.logout()

        mobile_my_ads.go_dashboard()
        dash_ad = mobile_my_ads.search_ad_by_subject(ad3[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad3[1])

        utils.rebuild_index()

        mobile_my_ads.go_dashboard()
        self.assertNotIn(ad3, mobile_my_ads.create_ad_list())

    @tags('mobile', 'dashboard', "ad_edit", "ad_delete")
    def test_edit_review_user_delete_mobile(self, wd):
        """ User delete an ad with a edition, the ad must disapear from dashboard mobile after rebuild index """

        ad1 = ['Aviso de usuario Android', 'Aviso publicado']
        ad2 = ['Aviso de usuario Android', 'Revisando edici�n']
        ad3 = ['Aviso de usuario Android', 'Aviso inactivo']
        list_id = '8000066'

        mobile_login = mobile.Login(wd)
        mobile_login.login(self.user, self.passwd)
        mobile_my_ads = mobile.DashboardMyAds(wd)
        dash_ad = mobile_my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # edition
        # mobile_my_ads.go_dashboard()
        passedit = mobile.EditPasswordPage(wd)
        passedit.go(list_id)

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        mobile_my_ads.go_dashboard()
        dash_ad = mobile_my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        # delete
        ad_delete = mobile.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()
        self.assertIn("Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado.",
                      ad_delete.result_message.text,
                      "Check modal window message")

        mobile_my_ads = mobile.DashboardMyAds(wd)
        mobile_my_ads.go_dashboard()
        dash_ad = mobile_my_ads.search_ad_by_subject(ad3[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad3[1])

        utils.rebuild_index()

        mobile_my_ads = mobile.DashboardMyAds(wd)
        mobile_my_ads.go_dashboard()
        # Since DTB the ad will still be present, but inactive
        self.assertIn(ad3, mobile_my_ads.create_ad_list())

    @tags('mobile', 'dashboard', 'ad_edit', 'ad_delete', 'controlpanel')
    def test_edit_review_admin_hide_mobile(self, wd):
        """ Admin hide an ad with edition, the ad must disapear from dashboard mobile """
        ad1 = ['Vestido de Novia', 'Aviso publicado']
        ad2 = ['Vestido de Novia', 'Revisando edici�n']
        list_id = '8000042'

        mobile_login = mobile.Login(wd)
        mobile_login.login(self.user, self.passwd)
        mobile_my_ads = mobile.DashboardMyAds(wd)
        dash_ad = mobile_my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # edition
        passedit = mobile.EditPasswordPage(wd)
        passedit.go(list_id)

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited',phone='5657687', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        mobile_my_ads.go_dashboard()
        dash_ad = mobile_my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(None, list_id)
        cpanel.logout()

        mobile_my_ads.go_dashboard()
        self.assertNotIn(ad2, mobile_my_ads.create_ad_list())

    @tags('mobile', 'dashboard', 'ad_edit', 'ad_delete', 'controlpanel')
    def test_hide_admin_delete_mobile(self, wd):
        """ Admin delete an ad in hidden status, the ad reapears in dashboard mobile as deleting and after rebuild index disapear  """
        ad1 = ['Guardias de seguridad', 'Aviso publicado']
        ad2 = ['Guardias de seguridad', 'Borrando aviso']
        list_id = '8000048'

        login = mobile.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = mobile.DashboardMyAds(wd)
        dash_ad = my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_hide_ad(None, list_id)
        cpanel.logout()

        my_ads.go_dashboard()
        self.assertNotIn(ad1[0], my_ads.create_ad_list())

        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_and_delete_ad(None, list_id)
        cpanel.logout()

        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        utils.rebuild_index()

        my_ads.go_dashboard()
        self.assertNotIn(ad2, my_ads.create_ad_list())

    @tags('mobile', 'dashboard', 'ad_edit', 'ad_delete')
    def test_edit_review_user_email_delete_mobile(self, wd):
        """ User edit ad by mobile dashboard and then delete the ad from email link """
        short_title = 'Empresa necesita Secretaria Rec...'

        ad = {'title': 'Empresa necesita Secretaria Recepcionista',
              'short_title': short_title,
              'list_id': '8000043'}

        # Ask for my ads by email
        myads = mobile.MyAds(wd)
        myads.go_and_request(self.user)
        # get delete link from the email
        link_edit, link_del = self.get_ad_links_from_mail(wd, ad['title'])

        # search the state for and ad into dashboard
        login = mobile.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = mobile.DashboardMyAds(wd)
        dash_ad = my_ads.search_ad_by_subject(ad['title'])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, self.ad_status['published'])

        # change pass in ad edition and save
        passedit = mobile.EditPasswordPage(wd)
        passedit.go(ad['list_id'])

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited',phone='5657687', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad['title'])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, self.ad_status['reviewed'])

        # delete ad from email link
        ad_delete = mobile.DeletePage(wd)
        wd.get(link_del)
        ad_delete.delete_ad_logged()
        self.assertIn("Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado.",
                      ad_delete.result_message.text,
                      "Check modal window message")
        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad['title'])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, self.ad_status['deleted'])

        utils.rebuild_index()

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        self.assertIn(ad['title'], my_ads.ads_list())

    @tags('mobile', 'dashboard', 'ad_edit')
    def test_edit_review_user_email_edit_mobile(self, wd):
        """ User edit ad and then edit the ad from email link """
        ad = ['Ca�on Modelo Dahlgreen 1861 Nuevo De Madera Y Meta']
        ad1 = ['Ca�on Modelo Dahlgreen 1861 Nuevo De Mad...', 'Aviso publicado']
        ad2 = ['Ca�on Modelo Dahlgreen 1861 Nuevo De Mad...', 'Revisando edici�n']
        list_id = '8000056'

        myads = mobile.MyAds(wd)
        myads.go_and_request(self.user)
        link_edit = self.get_ad_links_from_mail(wd, ad[0])[0]

        login = mobile.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = mobile.DashboardMyAds(wd)
        dash_ad = my_ads.search_ad_by_subject(ad[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # edition
        passedit = mobile.EditPasswordPage(wd)
        passedit.go(list_id)

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited',phone='5465787', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])
        wd.get(link_edit)

        edit.insert_ad(body='edited again',phone='5657687', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

    @tags('mobile', 'dashboard', 'ad_delete')
    def test_delete_user_email_delete_mobile(self, wd):
        """ User delete ad in mobile and then delete the ad from email link """
        ad1 = ['Asistente RR HH', 'Aviso publicado']
        ad2 = ['Asistente RR HH', 'Aviso inactivo']
        list_id = '8000046'

        myads = mobile.MyAds(wd)
        myads.go_and_request(self.user)
        link_edit, link_del = self.get_ad_links_from_mail(wd, ad1[0])

        login = mobile.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = mobile.DashboardMyAds(wd)
        dash_ad = my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # delete
        ad_delete = mobile.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()
        self.assertIn("Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado.",
                      ad_delete.result_message.text,
                      "Check modal window message")

        myads = mobile.MyAds(wd)
        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        utils.rebuild_index()

        my_ads.go_dashboard()
        # Since DTB the ad will still be present, but inactive
        self.assertIn(ad2, my_ads.create_ad_list())

        ad_view = AdViewMobile(wd)
        wd.get(link_del)

        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

    @tags('mobile', 'dashboard', 'ad_edit', 'ad_delete')
    def test_delete_user_email_edit_mobile(self, wd):
        """ User delete ad in mobile and then edit the ad from email link """
        ad1 = ['Control de calidad en metalurgica', 'Aviso publicado']
        ad2 = ['Control de calidad en metalurgica', 'Aviso inactivo']
        list_id = '8000044'

        myads = mobile.MyAds(wd)
        myads.go_and_request(self.user)
        link_edit, link_del = self.get_ad_links_from_mail(wd, ad1[0])

        login = mobile.Login(wd)
        login.login(self.user, self.passwd)
        my_ads = mobile.DashboardMyAds(wd)
        dash_ad = my_ads.search_ad_by_subject(ad1[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad1[1])

        # delete
        ad_delete = mobile.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()

        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(ad2[0])
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, ad2[1])

        utils.rebuild_index()

        my_ads.go_dashboard()
        # Since DTB the ad will still be present, but inactive
        self.assertIn(ad2, my_ads.create_ad_list())

        ad_view = AdViewMobile(wd)
        wd.get(link_edit)

        self.assertTrue(ad_view.ad_not_found_title.is_displayed())

    @tags('mobile', 'dashboard', 'ad_edit', 'controlpanel', 'ad_refuse')
    def test_insert_refuse_refuse(self, wd):
        """ Check the counter of ads on mobile dashboard, when edit refused are refused again the counter of ads must be 0  """
        login = mobile.Login(wd)
        login.login('no-ads@yapo.cl', '123123123')
        my_ads = mobile.DashboardMyAds(wd)
        self.assertEquals(len(my_ads.list_all_ads), 0)

        mai = mobile.NewAdInsert(wd)
        mai.go()
        mai.insert_ad(
            subject='ropa de bebe',
            body="ropa de bebe",
            region="15",
            communes=331,
            category="3040",
            price="999666",
        )

        mai.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        self.assertEquals(len(my_ads.list_all_ads), 1)

        # Refuse on CP
        trans.review_ad('no-ads@yapo.cl', 'Ropa de bebe', action = 'refuse')
        my_ads.go_dashboard()
        self.assertEquals(len(my_ads.list_all_ads), 0)

        email = SentEmail(wd)
        email.go()
        link_email = email.body.find_element_by_link_text("aqu�").get_attribute("href")

        wd.get(link_email)

        edit = mobile.NewAdInsert(wd)
        edit.fill_form(body='edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads.go_dashboard()
        self.assertEquals(len(my_ads.list_all_ads), 1)

        # Refuse on CP
        trans.review_ad('no-ads@yapo.cl', 'Ropa de bebe', action = 'refuse')

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        self.assertEquals(len(my_ads.list_all_ads), 0)

    def get_ad_links_from_mail(self, wd, subject):
        email = SentEmail(wd)
        email.go()
        subjects_list = email.body.find_elements_by_css_selector(".subject")
        link_edit_list = email.body.find_elements_by_css_selector(".edit")
        link_del_list = email.body.find_elements_by_css_selector(".delete")

        self.assertIsNotNone(link_del_list)
        self.assertIsNotNone(link_edit_list)
        self.assertIsNotNone(subjects_list)
        position = -1
        for i in range(len(subjects_list)):
            if subjects_list[i].text == subject:
                position = i
                break
        if position == -1:
            return None
        else:
            return [link_edit_list[position].get_attribute("href"),
                    link_del_list[position].get_attribute("href")]


class DashboardAdStatesMobile2(yapo.SeleniumTest):

    snaps = ['accounts', 'addaccountandroid']
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.insertingfee.type.1.enabled': '1',
        '*.*.category_settings.price.1.7020.1.value': 'price:5000',
        "*.*.movetoapps_splash_screen.enabled": '0'
    }

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_store_3shops'])
        utils.rebuild_asearch()
        utils.flush_redises()

    def test_dashboard_delete_status(self, wd):
        """ Tests dashboard delete status """
        # the user now logs in
        mobile_login = mobile.Login(wd)
        mobile_login.login("android@yapo.cl", "123123123")

        # delete
        ad_delete = mobile.DeletePage(wd)
        ad_delete.go("8000066")
        ad_delete.delete_ad_logged()
        self.assertIn("Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado.",
                      ad_delete.result_message.text,
                      "Check modal window message")

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject("Aviso de usuario Android")
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, "Aviso inactivo")
        utils.rebuild_index()
        my_ads.go_dashboard()
        self.assertEqual(len(my_ads.active_ads) + len(my_ads.inactive_ads), len(my_ads.all_ads))

    def test_dashboard_edit_status(self, wd):
        """ Tests dashboard edit status """

        subject = 'Lechuza embalsamada'
        # the user now logs in
        mobile_login = mobile.Login(wd)
        mobile_login.login("android@yapo.cl", "123123123")

        passedit = mobile.EditPasswordPage(wd)
        passedit.go("8000052")

        edit = mobile.NewAdInsert(wd)
        edit.insert_ad(body='edited', phone='5781234', area_code='32')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        my_ads = mobile.DashboardMyAds(wd)
        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(subject)
        dash_ad.wait.until_visible('subject')
        self.assertEqual(dash_ad.get_status, "Revisando edici�n")

        trans.review_ad(email='android@yapo.cl', subject=subject)
        utils.rebuild_index()

        my_ads.go_dashboard()
        dash_ad = my_ads.search_ad_by_subject(subject)
        dash_ad.wait.until_visible('subject')
        self.assertNotEqual(dash_ad.get_status, "Revisando edici�n")

    def test_pay_unpaid(self, wd):

        subject = 'Empleo de bebe'
        my_ads = self._insert_an_unpaid_ad(wd, subject)
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.status.text, 'Pendiente de pago')

        ad.go_to_pay()
        steps.pay(wd, mobile.SummaryPayment, mobile.PaymentVerify)

        my_ads.go_dashboard()
        ad = my_ads.search_ad_by_subject(subject)
        self.assertEqual(ad.status.text, 'Aviso en revisi�n')
        self.assertNotVisible(ad, 'btn_unpaid')

    def test_delete_unpaid(self, wd):

        subject = 'Empleo de bebe'
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        ad = my_ads.go_to_delete_ad(subject)

        delete_unpaid_modal = mobile.DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        ad_list = my_ads.create_ad_list()

        delete_unpaid_modal.reject()
        self.assertIn([subject, ad.status.text], ad_list)

        ad = my_ads.go_to_delete_ad(subject)
        delete_unpaid_modal.confirm()

        self.assertEquals(len(ad_list) - 1, len(my_ads.create_ad_list()))

    def test_delete_unpaid_clear_cart_without_other_items(self, wd):

        subject = "Aviso a eliminarse"
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        ad = my_ads.go_to_delete_ad(subject)

        delete_unpaid_modal = mobile.DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        delete_unpaid_modal.confirm()
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        sticky_bar_payment = mobile.StickyBarPayment(wd)
        self.assertVisible(sticky_bar_payment, "new_bar_off")

    def test_delete_unpaid_clear_cart_with_more_items(self, wd):

        subject = "Aviso a eliminarse"
        my_ads = self._insert_an_unpaid_ad(wd, subject)

        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")

        products_to_add = {
            6000667: [utils.Products.BUMP],
            8000073: [utils.Products.LABEL]
        }
        my_ads.select_products_by_ids(products_to_add)

        ad = my_ads.go_to_delete_ad(subject)
        delete_unpaid_modal = mobile.DeleteUnpaidModal(wd) 
        delete_unpaid_modal.open()

        delete_unpaid_modal.confirm()

        sticky_bar_payment = mobile.StickyBarPayment(wd)
        self.assertVisible(sticky_bar_payment, "new_bar")
        self.assertEqual(sticky_bar_payment.new_amount.text, "$ 5.000")

    def _insert_an_unpaid_ad(self, wd, subject):
        trans.make_user_pro(1, 'jobs')

        login = mobile.Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        my_ads = mobile.DashboardMyAds(wd)

        mai = mobile.NewAdInsert(wd)
        mai.go()
        mai.insert_ad(
            subject=subject,
            body="para bebe!",
            region=15,
            communes=331,
            category=7020,
            working_day=1,
            contract_type=2,
            jc=['20'],
            price=999666,
        )
        mai.submit.click()

        summary = mobile.SummaryPayment(wd)
        summary.wait.until_visible('products_list_table')

        return my_ads.go()
