# -*- coding: latin-1 -*-
import yapo
import time
from yapo import utils
from yapo.pages.desktop import DashboardMyAds, Login, MyAds, AdEdit
from yapo.pages.desktop_list import AdDeletePage
from yapo.pages.control_panel import ControlPanel
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from yapo.pages.generic import SentEmail
from yapo.steps import trans, insert_an_ad


class DashboardAdStates(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']

    def test_published_to_user_delete(self, wd):
        """Published ad and user_delete action
           status without RI: "Aviso inactivo"
           status with RI: Aviso inactivo"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        # Delete the ad
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.delete_ad_by_subject("Aviso de usuario Android")

        # Check the status before rebuild-index
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            "Aviso de usuario Android"), "Aviso inactivo")
        utils.rebuild_index()
        # Check the status after rebuild-index
        # The ad is now inactive for 7 days
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            "Aviso de usuario Android"), "Aviso inactivo")

    def test_published_to_admin_delete(self, wd):
        """Published ad and admin_delete action
           status without RI: "Borrando aviso"
           status with RI: Not present"""

        # Delete ad in CP
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.change_ad_status(subject="Lechuza embalsamada", action="delete")

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        # Check the status before rebuild-index
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject("Lechuza embalsamada"),
                         "Borrando aviso")

        utils.rebuild_index()

        time.sleep(2)
        # Check the status after rebuild-index
        dash_my_ads.go_dashboard()
        self.assertIsNone(
            dash_my_ads.search_status_by_subject("Lechuza embalsamada"))

    def test_deleted_to_undeleted(self, wd):
        """Deleted ad and admin_undeleted action
           status with RI: It's present"""

        # Delete and Undelete by CP
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.change_ad_status(subject="Asistente RR HH", action="delete")
        cp.search_and_undelete_ad_by_subject(
            subject="Asistente RR HH", time_from='2011-01-01')
        utils.rebuild_index()

        # Check ad in Dashborad after RI
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(
            dash_my_ads.search_status_by_subject("Asistente RR HH"), "Aviso publicado")

    def test_published_to_refused(self, wd):
        """Published ad and admin_refused action
           status without RI: "Borrando aviso"
           status with RI: Not present"""

        # Refused ad by CP
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.post_refuse_ad_by_subject(subject='Juego de 7 Llaves Antiguas')

        # Check ad in Dashboard, is none
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertIsNone(
            dash_my_ads.search_status_by_subject('Juego de 7 Llaves Antiguas'))

    def test_reviewing_to_admin_refused(self, wd):
        """Reviewing ad refused by admin action
           status without RI: NOT PRESENT
           status with RI: NOT PRESENT"""

        # Ad insert
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        result = insert_an_ad(wd, AdInsert=AdInsert,
            subject='ropa de bebe',
            body="ropa de bebe",
            region=15,
            communes=315,
            category=3040,
            price=999666,
            accept_conditions=True,
            logged=True
        )
        self.assertTrue(result.was('success'))

        # Refuse Ad
        trans.review_ad('android@yapo.cl', 'Ropa de bebe', action = 'refuse')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        # Without RI
        self.assertIsNone(dash_my_ads.search_status_by_subject("Ropa de bebe"))
        utils.rebuild_index()
        # With RI
        dash_my_ads.go_dashboard()
        self.assertIsNone(dash_my_ads.search_status_by_subject("Ropa de bebe"))

    def test_refused_to_reviewing(self, wd):
        """Refused ad edited by mail action
           status without RI: 'Aviso en revisi�n'
           status with RI: 'Aviso en revisi�n'"""

        # Ad insert
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        result = insert_an_ad(wd, AdInsert=AdInsert,
            subject='ropa de adulto',
            body="ropa de adulto",
            region=15,
            communes=315,
            category=3040,
            price=999666,
            accept_conditions=True,
            logged=True
        )
        self.assertTrue(result.was('success'))

        # Refuse Ad
        trans.review_ad('android@yapo.cl', 'Ropa de adulto', action = 'refuse')

        # Edit by email
        self.edit_ad_by_mail(wd)

        # Check in Dashboard
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        # Check without RI
        self.assertIsNone(
            dash_my_ads.search_status_by_subject('Ropa de adulto'))

        utils.rebuild_index()

        # Check with RI
        dash_my_ads.go_dashboard()
        self.assertIsNone(
            dash_my_ads.search_status_by_subject('Ropa de adulto'))

    def test_refused_to_postrefuse_email_edit(self, wd):
        """Refused ad edited by mail action refused again, status without RI: 'Aviso en revisi�n', status with RI: 'Aviso en revisi�n'"""

        # Ad insert
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        result = insert_an_ad(wd, AdInsert=AdInsert,
            subject='ropa de ni�o',
            body="ropa de ni�o",
            region=15,
            communes=315,
            category=3040,
            price=999666,
            accept_conditions=True,
            logged=True
        )
        self.assertTrue(result.was('success'))

        # Refuse Ad
        trans.review_ad('android@yapo.cl', 'Ropa de ni�o', action = 'refuse')

        # Edit by email
        self.edit_ad_by_mail(wd)

        trans.review_ad('android@yapo.cl', 'Ropa de ni�o', action = 'refuse')
        # Check in Dashboard
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        # Check without RI
        self.assertEqual(dash_my_ads.search_status_by_subject('Ropa de ni�o edited'),
                         None)

        utils.rebuild_index()

        # Check with RI
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Ropa de ni�o edited'),
                         None)

    def test_reviewing_to_accept(self, wd):
        """Inserted ad reviewed
           status without RI: 'Aviso en revisi�n'
           status with RI: 'Aviso publicado'"""

        # Ad insert
        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        result = insert_an_ad(wd, AdInsert=AdInsert,
            subject='ropa de mujer',
            body="ropa de mujer",
            region=15,
            communes=315,
            category=3040,
            price=999666,
            accept_conditions=True,
            logged=True
        )
        self.assertTrue(result.was('success'))

        # Accept ad
        trans.review_ad('android@yapo.cl', 'Ropa de mujer')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        # Check without RI
        self.assertEqual(dash_my_ads.search_status_by_subject('Ropa de mujer'),
                         'Aviso en revisi�n')

        utils.rebuild_index()

        # Check with RI
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Ropa de mujer'),
                         'Aviso publicado')

    def test_accepted_to_reviewing(self, wd):
        """Accepted ad edited by user
           status without RI: 'Revisando edici�n'
           status with RI: 'Revisando edici�n'"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')
        subject = 'Expectacular Blackberry 9810 o Permuta'

        myads = MyAds(wd)
        myads.go_and_request('android@yapo.cl')
        link_edit, link_del = self.get_ad_links_from_mail(
            wd, subject)

        # edition
        edit = AdEdit(wd)
        wd.get(link_edit)
        edit.insert_ad(body='edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            subject), 'Revisando edici�n')
        utils.rebuild_index()
        dash_my_ads.go_dashboard()

        # Check without RI
        self.assertEqual(dash_my_ads.search_status_by_subject(
            subject), 'Revisando edici�n')

    def test_reviewing_to_reviewing(self, wd):
        """Reviewing ad edited by user
           status without RI: 'Revisando edici�n'
           status with RI: 'Revisando edici�n'"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')
        subject = 'Call of Duty Black ops II Xbox 360'

        myads = MyAds(wd)
        myads.go_and_request('android@yapo.cl')
        link_edit, link_del = self.get_ad_links_from_mail(
            wd, subject)

        # edition
        edit = AdEdit(wd)
        wd.get(link_edit)
        edit.insert_ad(body='edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        myads.go_and_request('android@yapo.cl')
        link_edit, link_del = self.get_ad_links_from_mail(
            wd, subject)

        wd.get(link_edit)
        edit.fill_form(body='edited 2')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            subject), 'Revisando edici�n')
        utils.rebuild_index()
        dash_my_ads.go_dashboard()

        # Check without RI
        self.assertEqual(dash_my_ads.search_status_by_subject(
            subject), 'Revisando edici�n')

    def test_reviewing_to_accepted(self, wd):
        """Edited ad accepted by admin
           status without RI: 'Revisando edici�n'
           status with RI: 'Aviso publicado'"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        myads = MyAds(wd)
        myads.go_and_request('android@yapo.cl')
        link_edit, link_del = self.get_ad_links_from_mail(
            wd, 'Vestido de Novia')

        # edition
        edit = AdEdit(wd)
        wd.get(link_edit)
        edit.insert_ad(body='edited edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        # Accept ad
        trans.review_ad('android@yapo.cl', 'Vestido de Novia')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Vestido de Novia'), 'Aviso publicado')
        utils.rebuild_index()
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Vestido de Novia'), 'Aviso publicado')

    def test_reviewing_to_refuse(self, wd):
        """Edited ad refused by admin
           status without RI: 'Revisando edici�n'
           status with RI: 'Aviso publicado'"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        myads = MyAds(wd)
        myads.go_and_request('android@yapo.cl')
        link_edit, link_del = self.get_ad_links_from_mail(
            wd, 'Cama rosen modelo tfx-2')

        # edition
        edit = AdEdit(wd)
        wd.get(link_edit)
        edit.insert_ad(body='edited edited')
        edit.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        # Refuse ad
        trans.review_ad('android@yapo.cl', 'Cama rosen modelo tfx-2', action = 'refuse')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Cama rosen modelo tfx-2'), 'Aviso publicado')
        utils.rebuild_index()
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Cama rosen modelo tfx-2'), 'Aviso publicado')

    def test_published_to_refuse(self, wd):
        """Published ad edited by admin
           status without RI: 'Aviso publicado'
           status with RI: 'Aviso publicado'"""

        login = Login(wd)
        login.login('android@yapo.cl', '123123123')

        # edition
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.edit_ad_by_subject(subject='Guitarra electrica')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Guitarra electrica'), 'Aviso publicado')
        utils.rebuild_index()
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(
            'Guitarra electrica'), 'Aviso publicado')

    def edit_ad_by_mail(self, wd):
        """ goes to the last sent email and click on 'aqui' to reach the edition form """

        def refresh_until_refuse_email(wd):
            is_refuse_email = 'Aviso Rechazado' in wd.find_element_by_id('title').text
            if not is_refuse_email:
                wd.refresh()
            return is_refuse_email

        sent_email = SentEmail(wd)
        sent_email.go()
        sent_email.wait.until(refresh_until_refuse_email)
        wd.find_element_by_partial_link_text('aqu�').click()
        ad_insert = AdInsert(wd)
        ad_insert.subject.send_keys(' edited')
        ad_insert.fill_form(accept_conditions=True)
        ad_insert.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    def get_ad_links_from_mail(self, wd, subject):
        email = SentEmail(wd)
        email.go()
        subjects_list = email.body.find_elements_by_css_selector(".subject")
        link_edit_list = email.body.find_elements_by_css_selector(".edit")
        link_del_list = email.body.find_elements_by_css_selector(".delete")
        position = -1
        for i in range(len(subjects_list)):
            if subjects_list[i].text == subject:
                position = i
                break
        if position == -1:
            return None
        else:
            return [link_edit_list[position].get_attribute("href"),
                    link_del_list[position].get_attribute("href")]
