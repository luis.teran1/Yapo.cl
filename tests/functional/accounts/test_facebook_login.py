# coding: latin-1
from yapo.decorators import tags
from yapo.pages.desktop import LoginModal, DesktopHome, Dashboard
from yapo.pages import mobile
from yapo import utils
from yapo.steps.accounts import login_with_facebook
from yapo.mail_generator import Email
import yapo
import os
import urllib
import time
import unittest

skip_message = 'Disabled because It there is no api simulator and these tests'\
                ' fail if you do not have the nextgen repository'

class FacebookBaseTests(yapo.SeleniumTest):
    def setUp(self):
        """ For these tests we need the nextgen api up & running, here we deal with that """

        def is_api_up():
            """ Checks if the API is up & running """
            url = yapo.conf.NEXTGEN_API_URL + '/api/v1.1/public/config'
            try:
                r = urllib.request.urlopen(url)
                if r.getcode() == 200:
                    is_up = True
            except:
                is_up = False
            return is_up

        api_dir = os.path.abspath(os.path.join(yapo.conf.TOPDIR, '../nextgen-api'))
        if not is_api_up():
            cmd = '{0}/tests/start_nextgen_api.sh {1}'.format(yapo.conf.TOPDIR, api_dir)
            os.system(cmd)

        while not is_api_up():
            time.sleep(1)


class FacebookLoginTestCase(FacebookBaseTests):
    """ Tests for facebook login """

    snaps = ['accounts']
    facebook_email = 'dcggtcu_warmanman_1456766679@tfbnw.net'
    facebook_passwd = 'yapo123'
    LoginModalClass = LoginModal
    DashboardClass = Dashboard
    greeting = lambda self, name: "Hola {}".format(name)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'facebook_login')
    def test_login_facebook_email_not_in_yapo(self, wd):
        """
        Tested login with facebook, email of facebook is not in yapo, create account and success login
        """
        utils.restore_db_snap('accounts')
        utils.flush_redises()
        # Login by facebook
        from_desktop = False if hasattr(self, 'user_agent') else True
        login_with_facebook(wd, self.facebook_email, self.facebook_passwd, from_desktop)
        login_modal = self.LoginModalClass(wd)

        # Create facebook account with facebook email
        login_modal.wait.until_visible('email')
        self.assertEqual(self.facebook_email, login_modal.email.get_attribute('value'), 'Facebook email is not equal to email that was shown')
        login_modal.button_email_fb_confirm.click()

        # Success login
        dashboard = self.DashboardClass(wd)
        dashboard.wait.until_visible('title_legend')
        self.assertEqual(self.greeting('Dorothy Alaadjhddeahb Warmanman'), dashboard.title_legend.text)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'facebook_login')
    def test_login_facebook_email_in_yapo(self, wd):
        """
        Tested login with facebook, email of facebook is in yapo, success login
        """
        utils.restore_db_snap('accounts')
        utils.flush_redises()
        # Facebook email, exists in yapo (account table)
        utils.db_query("UPDATE accounts SET email='{0}' WHERE email='many@ads.cl'".format(self.facebook_email))
        utils.flush_redises()

        # Login by facebook
        from_desktop = False if hasattr(self, 'user_agent') else True
        login_with_facebook(wd, self.facebook_email, self.facebook_passwd, from_desktop)

        # Success login
        dashboard = self.DashboardClass(wd)
        dashboard.wait.until_visible('title_legend')
        self.assertEqual(self.greeting('ManyAdsAccount'), dashboard.title_legend.text)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'facebook_login')
    def test_login_facebook_other_email_not_in_yapo(self, wd):
        """
        Tested login with facebook, email diferent from facebook is not in yapo, create account,
        send confirmation email and success login
        """
        utils.restore_db_snap('accounts')
        utils.flush_redises()

        from_desktop = False if hasattr(self, 'user_agent') else True
        login_with_facebook(wd, self.facebook_email, self.facebook_passwd, from_desktop)

        login_modal = self.LoginModalClass(wd)
        email = 'joak_traicionero@selacome.pi.co'
        login_modal.fill_email_form_and_send(email)

        login_modal.wait.until_not_visible('spiner')
        login_modal.wait.until_not_visible('email')
        msg_send_email = 'Est�s a un paso de crear tu cuenta. Hemos enviado un correo que te permitir� activarla.'
        self.assertEqual(login_modal.facebook_description.text, msg_send_email)

        # Send account activation email
        mlt = Email.create(wd)
        mlt.check_account_activation(self, email)

        # Success login
        dash = self.DashboardClass(wd)
        self.assertIn(self.greeting('Dorothy Alaadjhddeahb Warmanman'), dash.title_legend.text)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'facebook_login')
    def test_login_facebook_other_email_in_yapo(self, wd):
        """
        Tested login with facebook, email diferent from facebook is in yapo, enter password and success login
        """
        utils.restore_db_snap('accounts')
        utils.flush_redises()

        # Login by facebook
        from_desktop = False if hasattr(self, 'user_agent') else True
        login_with_facebook(wd, self.facebook_email, self.facebook_passwd, from_desktop)
        login_modal = self.LoginModalClass(wd)
        login_modal.wait.until_visible('email')

        # Enter email with account and associate facebook account with yapo account
        login_modal.fill_password_form_and_send(email='many@ads.cl', password='123123123')

        # Success login
        dashboard = self.DashboardClass(wd)
        dashboard.wait.until_visible('title_legend')
        self.assertEqual(self.greeting('ManyAdsAccount'), dashboard.title_legend.text)

    @unittest.skip(skip_message)
    @tags('accounts', 'login', 'facebook_login')
    def test_login_facebook_success(self, wd):
        """
        Tested login with facebook, user already has Facebook account, success login.
        """
        utils.restore_db_snap('accounts')
        utils.load_snap('diff_facebooklogin')
        utils.flush_redises()

        # Login by facebook
        from_desktop = False if hasattr(self, 'user_agent') else True
        login_with_facebook(wd, self.facebook_email, self.facebook_passwd, from_desktop)
        dashboard = self.DashboardClass(wd)
        self.assertEqual(self.greeting('Dorothy Alaadjhddeahb Warmanman'), dashboard.title_legend.text)


class FacebookLoginMobileTestCase(FacebookLoginTestCase):
    user_agent = utils.UserAgents.NEXUS_5
    LoginModalClass = mobile.LoginFacebook
    DashboardClass = mobile.Dashboard
    # one liner just for fun, it is probably more readable in a fuction but, who wants to live forever?
    def greeting(self, name):
        if name.startswith("Many"):
            return "Hola, ManyAdsA..."
        else:
            return "Hola, Dorothy A..."
