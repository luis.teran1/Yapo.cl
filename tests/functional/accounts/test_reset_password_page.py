from yapo.decorators import tags
from yapo.pages.accounts import ForgotPassword
from yapo.steps.newai import insert_ad
from yapo.pages.newai import DesktopAI
import yapo, time
from yapo import conf, utils
from yapo.pages.generic import SentEmail

class TestResetPasswordPage(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('password', 'accounts', 'errors')
    def test_validate_error(self, wd):
        """ validates that error are showing """
        page = ForgotPassword(wd)
        page.go()
        page.send("")
        self.assertEquals(page.error_msg.text, page.error_email_empty)

        page.send("a")
        self.assertEquals(page.error_msg.text, page.error_bad_email_too_short)

        page.send("asdiasas")
        self.assertEquals(page.error_msg.text, page.error_bad_email)

    @tags('password', 'accounts', 'errors')
    def test_from_email_validation(self, wd):
        """ validates that when entering through email link errors are showing"""
        page = ForgotPassword(wd)
        hash = page.go_email('prepaid5@blocket.se', 1)

        page.send_passwd("", "")
        self.assertEquals(page.error_msg.text, page.error_passwd_too_short)

        page.send_passwd("12345", "12345")
        self.assertEquals(page.error_msg.text, page.error_passwd_too_short)

        page.send_passwd("123123124", "123123125")
        self.assertEquals(page.error_msg.text, page.error_passwd_different)

        page.send_passwd("123123123", "123123123")
        self.assertEquals(page.submit_btn.text, page.confirmation_login_msg)

        page.go_email('prepaid5@blocket.se', 2)
        page.send_passwd("123123123", "123123123")
        self.assertEquals(page.submit_btn.text, page.confirmation_login_msg)

        page.go_email('prepaid5@blocket.se', 3)
        page.send_passwd("123123123", "123123123")
        self.assertEquals(page.submit_btn.text, page.confirmation_publish_msg)

        page.go_email('prepaid5@blocket.se', 1, False)
        self.assertEquals(page.message.text, page.error_msg_expired)

    @tags('password', 'accounts', 'ad_insert')
    def test_from_ad_insert(self, wd):
        """ when te users ask for password through AI, the page should show "email sent" message """
        ai_page = DesktopAI(wd)
        insert_ad(ai_page, 
            email='prepaid5@blocket.se', 
            email_confirm='prepaid5@blocket.se', 
            password_verification='123123123123', 
            password="123123123123")
        ai_page.click_forgot_pass()

        page = ForgotPassword(wd)
        self.assertEqual(page.mail_sent_text.text, page.mail_sent_msg)

    @tags('password', 'accounts', 'redis', 'email')
    def test_redis_and_email(self, wd):
        """ test that the redis hash is being generated correctly and Email are being sent """
        # cleans redis
        utils.redis_command(
            conf.REGRESS_REDISSESSION_PORT,
            'del', 'rsd1x_forgot_password_{0}'.format('prepaid5@blocket.se'))

        # sends password reset mail
        page = ForgotPassword(wd)
        page.go()
        page.send('PREPAID5@blocket.se')

        # go to mail
        mail = SentEmail(wd)
        mail.go()
        mail.body.find_element_by_partial_link_text("reset_password").click()

        page.send_passwd('123123123','123123123')
        self.assertEquals(page.message.text, page.msg_success)

        self.assertEquals(None, 
            utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 
                'get', 'rsd1x_forgot_password_{0}'.format('prepaid5@blocket.se')))
