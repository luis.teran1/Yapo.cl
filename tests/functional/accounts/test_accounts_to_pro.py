# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertResult, AdInsertSuccess, AdInsertWithAccountSuccess, AdInsertWithoutAccountSuccess, PackConfirm
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.accounts import Edit as EditAccount
from yapo.pages.desktop import HeaderElements, AdEdit
from yapo.pages.generic import SentEmail
from yapo.pages.desktop_list import AdListDesktop
from yapo.steps import trans
import yapo
from yapo import utils
from yapo.steps import insert_an_ad, trans
from yapo.pages.newai import DesktopAI


class Account2Pro(yapo.SeleniumTest):

    snaps = ['acc2pro']
    bconfs = {"*.*.accounts.pro.enabled": "1"}

    def _check_account_to_pro(self, *args, **kwargs):
        """ generic functionality checks & asserts """
        print(kwargs)
        wd = kwargs.get('wd')

        # insert the sixth ad
        insert_an_ad(wd, AdInsert=DesktopAI, submit=True, **kwargs.get('data' ))

        # if edition is sent
        if kwargs.get('edit_ad_list_id'):
            edit_ad = AdEdit(wd)
            edit_ad.go(kwargs.get('edit_ad_list_id'))
            edit_ad.insert_ad(body=kwargs.get('edit_ad_body'), price=kwargs.get('price'))
            edit_ad.submit.click()
            result = AdInsertResult(wd)
            self.assertTrue(result.was('success'))
            trans.review_ad(kwargs['email'], kwargs['edit_ad_subject'])
            account = EditAccount(wd)
            account.go()
            self.assertFalse(account.pro_radio.is_selected())
            self.assertFalse(account.pro_radio.get_attribute("disabled"))

        trans.review_ad(kwargs['email'], kwargs['subject'])

        # check pro e-mail
        self._check_email(wd)

        # rebuild-index
        utils.rebuild_index()

        if kwargs.get('have_account'):
            # sign in, account must appear as pro, and in AI must be selected
            # as pro
            account = EditAccount(wd)
            account.go()
            self.assertTrue(account.pro_radio.is_selected())
            self.assertTrue(account.pro_radio.get_attribute("disabled"))

    def _check_email(self, wd):
        sent_email = SentEmail(wd)
        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn('Felicitaciones, para nosotros eres todo un pro!', body_text)

    def test_account_to_pro_specific_category(self, wd):
        """ Testing that the user is not passed to pro on edition and passed to pro on new ad, when his sixth ad is accepted (user with account) """

        email = 'test4@cuenta.com'
        password = '123123123'
        subject = 'Now I am a pro test4'
        price = '123000'
        data = {
            'region': '15',
            'category':'1220',
            'estate_type': '1',
            'communes': '295',
            'rooms': '2',
            'subject': subject,
            'body': 'lorem ipsum dolor sit amet test4',
            'price': price,
            'private_ad': True,
            'name': 'test 4 cuenta',
            'email': email,
            'email_verification': email,
            'password': password,
            'password_verification': password,
            'phone': '87654321',
            'accept_conditions': True,
            'create_account': False
        }
        listing_url = '/region_metropolitana/departamentos_piezas?ca=15_s&l=0&f=c&w=1&st=a'
        user_ads_subjects = ['Depto 1 test4', 'Depto 2 test4', 'Depto 3 test4',
                             'Depto 4 test4', 'Depto 5 test4', 'Now I am a pro test4']
        have_account = True

        # Edit data
        edit_ad_body = 'edited'
        edit_ad_list_id = 8000024
        edit_ad_subject = 'Depto 1 test4'

        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def test_ads_to_pro_specific_category(self, wd):
        """Testing that the user is passed to pro, when his sixth ad is accepted (user without account) for a specific category"""

        email = 'usuario04@schibsted.cl'
        passwd = '123123123'
        subject = 'Now I am a pro usuario04'
        price = '123000'
        data = {
            'region': '15',
            'category': '7060',
            'communes': '295',
            'subject': subject,
            'body': 'lorem ipsum dolor sit amet user4',
            'sct_2': True,
            'price': price,
            'private_ad': True,
            'name': 'Usuario 04',
            'email': email,
            'email_verification': email,
            'password': passwd,
            'password_verification': passwd,
            'phone': '87654321',
            'accept_conditions': True,
            'create_account': False
        }
        listing_url = '/region_metropolitana/servicios?ca=15_s&l=0&w=1'
        user_ads_subjects = ['We speak english', 'Fiesta de despedida', u'Para cuidar a los ni�os',
                             'Aceleramos su Mac', 'Limpiamos su Windows', 'Now I am a pro usuario04']
        have_account = False

        # sorry for the ugly hack
        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def test_account_to_pro_siblings_category(self, wd):
        """Testing that the user is passed to pro, when his sixth ad is accepted (user with account) and ads in siblings category"""

        email = 'test5@cuenta.com'
        passwd = '123123123'
        subject = 'Now I am a pro test5'
        price = '123000'
        data = {
            'region': '15',
            'category': '2060',
            'communes': '295',
            'regdate': '2014',
            'mileage': '100000',
            'cubiccms': '6',
            'subject': subject,
            'body': 'lorem ipsum dolor sit amet test5',
            'price': price,
            'private_ad': True,
            'name': 'test 5 cuenta',
            'email': email,
            'email_verification': email,
            'password': passwd,
            'password_verification': passwd,
            'phone': '87654321',
            'accept_conditions': True,
            'create_account': False,
            'plates': 'ABC10'
        }
        listing_url = '/region_metropolitana/vehiculos?ca=15_s&l=0&f=c&w=1&st=a'
        user_ads_subjects = ['Now I am a pro test5', 'Aro Standard 2012',
                             'Moto bkn', 'Camion', 'Parachoque', 'Yate digo', 'Acura Legend 2012']
        have_account = True

        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def test_ads_to_pro_siblings_category(self, wd):
        """ Testing that the user is passed to pro, when his sixth ad is accepted (user without account) and ads in siblings category """

        email = 'usuario05@schibsted.cl'
        passwd = '123123123'
        subject = 'Now I am a pro usuario05'
        price = '123000'
        data = {
            'region': '15',
            'category': '1220',
            'estate_type':'1',
            'communes': '295',
            'rooms': '2',
            'subject': subject,
            'body':
            'lorem ipsum dolor sit amet user5',
            'price': price,
            'private_ad': True,
            'name': 'Usuario 05',
            'email': email,
            'email_verification': email,
            'password': passwd,
            'password_verification': passwd,
            'phone': '87654321',
            'accept_conditions': True,
            'create_account': False


        }
        listing_url = '/region_metropolitana/inmuebles?ca=15_s&l=0&q=usuario05&w=1&st=a'
        user_ads_subjects = ['Dpto en arriendo usuario05', 'Casa usuario05', 'En ciudad empresarial usuario05',
                             'Bodega de almacenaje usuario05', 'Predio agricola usuario05', 'Now I am a pro usuario05']
        have_account = False

        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)

    def test_account_to_pro_siblings_category_pri_pro_ads(self, wd):
        """
        Testing that the user is passed to pro, when his sixth ad is accepted (user with account) and ads in siblings category
        """

        email = 'test6@cuenta.com'
        passwd = '123123123'
        subject = 'Now I am a pro test6'
        price = '123000'
        data = {
            'region': '15',
            'category': '1220',
            'estate_type':'1',
            'communes': '295',
            'rooms': '2',
            'subject': subject,
            'body': 'lorem ipsum dolor sit amet user6',
            'price': price,
            'private_ad': True,
            'name': 'Usuario 06',
            'email': email,
            'email_verification': email,
            'password': passwd,
            'password_verification': passwd,
            'phone': '87654321',
            'accept_conditions': True,
            'create_account': False
        }
        listing_url = '/region_metropolitana/inmuebles?ca=15_s&l=0&q=test6&f=c&w=1&st=a'
        user_ads_subjects = ['Local test6', 'Dedp test6', 'Depto test6',
                             'Casaaa test6', 'Depto 3 test6', 'Now I am a pro test6']
        have_account = True

        data = locals()
        data.pop('self')
        self._check_account_to_pro(**data)


class NoAccount2Pro(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_account_user_no_acc']
    bconfs = {"*.*.accounts.pro.enabled": "1"}

    def test_user_without_account_to_pro_ads_pro(self, wd):
        """Test show message "no tienes cuenta de usuario y tampoco Pack"
        when user, without account, has only pro ads."""
        trans.review_ad('ventas@tucarro.cl', 'Volkswagen amazon 2016') 

        username = 'tucarro'
        email = 'ventas@tucarro.cl'
        data = {
            'category': '2020',
            'private_ad': False,
            'name': username,
            'email': email,
            'email_verification': email,
            'accept_conditions': True,
            'create_account': False
        }

        insert_an_ad(wd, AdInsert=DesktopAI, submit=True, **data)
        page = PackConfirm(wd)
        self.assertTrue(page.pack_message_noaccount.is_displayed())
