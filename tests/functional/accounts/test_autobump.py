# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo

from yapo.pages.simulator import Paysim
from yapo.pages.generic import SentEmail
from yapo.pages.desktop import DashboardMyAds, Dashboard, SummaryPayment
from yapo.pages.desktop_list import AdListDesktop, AdDeletePage, AdViewDesktop
from yapo.pages import autobump
from yapo import utils

from yapo.steps.accounts import login_from_bar

class TestAutobump(yapo.SeleniumTest):

    snaps = ['accounts']

    ads = {
        8000073:[1,6],
        6000667:[3,1],
        3456789:[6,6]
    }
    ad_titles = ['Adeptus Astartes Codex', 'Departamento Tarapac�', 'Race car']
    sum_expected_values = [
        ['Adeptus Astartes Codex', 'Subir personalizado', '$ 162.000', ''],
        ['Departamento Tarapac�', 'Subir personalizado', '$ 17.400', ''],
        ['Race car', 'Subir personalizado', '$ 38.700', '']
    ]
    email_expected_value = [
        ['3', 'Subir personalizado', '$218.100']
    ]
    ads_refused = {
        8000073:[1,6]
    }
    ref_sum_expected_values = [
        ['Adeptus Astartes Codex', 'Subir personalizado', '$ 162.000', '']
    ]

    @tags('accounts', 'autobump')
    def test_frecuency_and_price(self, wd):
        """ Buys several autobump (with differenct conf & category) and checks prices """

        # Logs in and gets a bunch of autobumps
        login_from_bar(wd, 'prepaid5@blocket.se', '123123123')
        for list_id, params in self.ads.items():
            Dashboard(wd).go_dashboard()
            dashb = DashboardMyAds(wd)
            dashb.autobump(list_id)
            fpage = autobump.Frequency(wd)
            fpage.do_select(params[0], params[1])
            fpage.do_checkout()

        # check summary payment
        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, self.sum_expected_values)

        # pays
        summary_page.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()

        # checks email
        utils.run_nubox_php_script("bill_processing.php")
        utils.run_nubox_php_script("bill_send_email.php")
        sent_email = SentEmail(wd)
        sent_email.go()
        table_values = sent_email.get_table_data('products_table')
        self.assertListEqual(table_values, self.email_expected_value)

        # TEST THAT THOSE BUMPS ARE APPLIED
        autobump.flush_autobump()
        list = AdListDesktop(wd)
        list.go('chile')

        # ads must be in top 3
        checked = 0
        for adtitle in list.titles_ads_list:
            self.assertIn(adtitle.text, self.ad_titles)
            checked += 1
            if checked >= 3:
                break

    @tags('accounts', 'autobump')
    def test_refused_payment(self, wd):
        """ test that autobump payment refused keeps products in cart """

        # Logs in and get a bunch of autobumps
        login_from_bar(wd, 'prepaid5@blocket.se', '123123123')
        for list_id, params in self.ads_refused.items():
            Dashboard(wd).go_dashboard()
            dashb = DashboardMyAds(wd)
            dashb.autobump(list_id)
            fpage = autobump.Frequency(wd)
            fpage.do_select(params[0], params[1])
            fpage.do_checkout()

        # refuse payment
        summary_page = SummaryPayment(wd)
        summary_page.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_refuse()
        self.assertEqual(summary_page.warning_message.text, 'Oops... La transacci�n no pudo ser realizada')
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, self.ref_sum_expected_values)

    @tags('accounts', 'autobump')
    def test_of_deleted_ads(self, wd):
        """ Test that when the add is deleted autobumps are not aplied """
        # Buys autobump
        login_from_bar(wd, 'prepaid5@blocket.se', '123123123')
        autobump.direct_go(wd, 6000666, 2, 2)
        summary_page = SummaryPayment(wd)
        summary_page.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()

        # Deletes the ad
        delete_page = AdDeletePage(wd)
        delete_page.go(6000666)
        delete_page.delete_ad_logged()

        # do the AB flush
        autobump.flush_autobump()

        # ad shouldn't exist
        adview = AdViewDesktop(wd)
        adview.go(6000666)
        self.assertInRetry(adview.not_found_speech.text, [AdViewDesktop.ad_not_found_text], retry_fn=lambda: adview.go(6000666))

        # Revives ad
        Dashboard(wd).go_dashboard()
        dashb = DashboardMyAds(wd)
        dashb.activate_ad(subject='Pesas muy grandes')
        utils.rebuild_index()

        # the ad should be in listing top
        list = AdListDesktop(wd)
        list.go('chile')
        self.assertInRetry(list.titles_ads_list[0].text, ['Pesas muy grandes'], retry_fn=lambda: self._go_chile_with_time(wd), retries=6)

    def _go_chile_with_time(self, wd):
        time.sleep(1)
        list = AdListDesktop(wd)
        list.go('chile')

