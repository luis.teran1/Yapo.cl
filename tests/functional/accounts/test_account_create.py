# coding: latin-1
from yapo.decorators import tags
from yapo.pages.desktop import LoginModal, DashboardMyAds
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages import accounts, generic
from yapo import utils
import yapo

class CreateAccount(yapo.SeleniumTest):
    """ Tests for account creation & new user related stuff """

    snaps = ['accounts']

    @tags('accounts')
    def test_goto_create_form_from_li(self, wd):
        """ Reaches the create form from the listing page """

        listing = AdListDesktop(wd)
        listing.go()
        login_modal = LoginModal(wd)
        login_modal.open()
        login_modal.wait.until_visible('create_account')
        login_modal.create_account.click()

        self.assertIn('/cuenta/form', wd.current_url)
        self.assertTrue(wd.find_element_by_css_selector('.listFeatures li:nth-child(1) i').get_attribute('title') == 'Lista de avisos activos')
        self.assertTrue(wd.find_element_by_css_selector('.create-account-title .title-account').text == 'Crear mi cuenta gratuita')

    @tags('accounts')
    def test_account_creation_and_ai_arrow(self, wd):
        """ Tests the account creation and the ad insert arrow in dashboard's header """
        create = accounts.Create(wd)
        create.go()
        create.fill_form(
                person_radio=True,
                name="Yolo",
                gender_male=True,
                phone="65432109",
                email="lolailo@lei.lo",
                password="11111111",
                password_verify="11111111",
                accept_conditions=True)
        create.submit_btn.click()
        page = accounts.CreationSuccessDesktop(wd)
        page.wait.until_loaded()
        self.assertEqual(page.title.text, "Felicidades!")

        activation_url = utils.search_mails(
            yapo.conf.SSL_URL + "/cuenta/confirm.*")

        wd.get(activation_url)
        dashboard = DashboardMyAds(wd)
        loginbar = generic.AccountBar(wd)
        self.assertEqual(loginbar.message.text, "Hola, Yolo")
        self.assertTrue(dashboard.is_element_present('publish_ad_arrow'))

    @tags('accounts')
    def test_create_account_chrs_not_printables(self, wd):
        """ Tests create account with strange characters and characters not printables in the name. """
        create = accounts.Create(wd)
        create.go()
        create.fill_form(
                person_radio=True,
                name="Yolo{0}".format(chr(1)),
                phone="87654321",
                email="lolalo@lei.lo",
                password="11111111",
                password_verify="11111111",
                accept_conditions=True)
        create.submit_btn.click()
        # Hi! I am leaving this here, this error should not appear now
        # self.assertNotEqual('El nombre contiene caracteres no v�lidos', wd.find_element_by_id('err_name').text)
        page = accounts.CreationSuccessDesktop(wd)
        self.assertEqual(page.title.text, "Felicidades!")



