# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import accounts, control_panel
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import utils
from yapo.utils import JsonTool
import time
import yapo
import yapo.conf


class AccountCreateTest(yapo.SeleniumTest):

    snaps = ['accounts']
    pro_data = {}
    bconfs = {
        '*.*.accounts.dashboard.ads_per_page': 10,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    CreateAccountClass = accounts.Create
    EditAccountClass = accounts.Edit

    account_info = dict(
        region=15,
        person_radio=True,
        name='yolo',
        phone='69696969',
        email='yolo@cabral.es',
        password='123123123',
        password_verify='123123123',
        accept_conditions=True,
    )
    rut_errors_1 = 'Escribe tu RUT'
    rut_errors_2 = 'El RUT es incorrecto'
    rut_errors_3 = 'El RUT es incorrecto'
    phone_errors_1 = 'Escribe tu n�mero de tel�fono'
    name_error_empty = 'Escribe por lo menos dos letras'

    expected_errors = [
        'Selecciona persona o profesional',
        'Escribe tu nombre',
        'Escribe tu e-mail',
        'Debes introducir tu contrase�a',
        'Tienes que aceptar las condiciones']

    errors = [
        'Selecciona persona o profesional',
        'Escribe tu nombre',
        'Escribe el n�mero de tel�fono',
        'Selecciona una regi�n',
        'Escribe tu e-mail',
        'Debes introducir tu contrase�a',
        # Notice this dodgy bastard!
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Tienes que aceptar las condiciones']

    @tags('desktop', 'accounts')
    def test_create_account_form_validation(self, wd):
        """ Check validation message are present for create account """
        page = self.CreateAccountClass(wd)
        page.go()
        page.submit_btn.click()

        text_errors = [e.text for e in page.errors]
        # Delete blank errors
        [text_errors.remove('') for i in range(text_errors.count(''))]

        self.assertEqual(self.expected_errors, text_errors)
        import re
        self.assertTrue(re.findall(r"Escribe \w+ n�mero de tel�fono", page.err_phone.text))

        page.fill_form(person_radio=True)
        from collections import OrderedDict
        from selenium.webdriver.common.keys import Keys
        # ugly dict is ugly and doesn't keep order, so we use this instead
        sample_data = OrderedDict([
            ("person_radio", True),
            ("name", 'yolo'),
            ("phone", '69696969'),
            ("region", 15),
            ("email", 'yolo@cabral.es'),
            ("password", '123123123'),
            ("password_verify", '123123123'),
            ("accept_conditions", True)])
        self.errors.reverse()
        # set value and check that the message is dismissed
        for k, v in sample_data.items():
            page.fill_form(**{k: v})
            page.submit_btn.click()
            time.sleep(1)
            error = self.errors.pop()
            self.assertNotIn(error, [e.text for e in page.errors])

    @tags('desktop', 'accounts')
    def test_create_account_rut_validation(self, wd):
        """Test create account form rut validation """
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update(email="valrut@account.cl")
        account_info.update(pro_radio=True)
        account_info.pop('person_radio')
        page.fill_form(**account_info)
        page.submit_btn.click()

        self.assertEqual(self.rut_errors_1, page.err_rut.text)

        account_info.update(rut='1-22')
        page.fill_form(**account_info)
        page.submit_btn.click()
        self.assertEqual(self.rut_errors_2, page.err_rut.text)

        account_info.update(rut="123456789")
        page.fill_form(**account_info)
        page.submit_btn.click()
        self.assertEqual(self.rut_errors_3, page.err_rut.text)

        account_info.update(rut="100000-4")
        page.fill_form(**account_info)
        page.scroll_to_bottom()
        page.submit_btn.click()

        page = accounts.CreationSuccessDesktop(wd)
        page.wait.until_visible('title')
        self.assertEqual(page.title.text, "Felicidades!")

    @tags('desktop', 'accounts')
    def test_account_activation(self, wd):
        """ Test account activation """
        utils.clean_mails()
        from_desktop = not hasattr(self, 'user_agent')
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update(email="doge@cabral.es")
        page.fill_form(**account_info)
        page.submit_btn.click()
        activation_url = utils.search_mails(
            '(https?://.*?cuenta/confirm.*?)[\'\"<\n\s>]')
        self.assertIsNotNone(activation_url)
        wd.get(activation_url)
        actual_trans = utils.trans('get_account', email='doge@cabral.es')
        expected_trans = {
            'account_status': 'active',
            'email': 'doge@cabral.es',
            'is_company': 'f',
            'name': 'yolo',
            'phone': '969696969',
            'region': '15',
            'status': 'TRANS_OK'}

        self.assertTrans(actual_trans, expected_trans)

    @tags('desktop', 'accounts', 'control_panel', 'websql')
    def test_account_creation_activation_timestamps(self, wd):
        """ Testing that we store the creation and activation date,
        and these values appear on a websql """

        # account creation
        utils.clean_mails()
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update(email="new@account.cl")
        page.fill_form(**account_info)
        page.submit_btn.click()
        created_at = time.time()

        # account activation
        activation_url = utils.search_mails(
            '(https?://.*?cuenta/confirm.*?)[\'\"<\n\s>]')
        self.assertIsNotNone(activation_url)
        wd.get(activation_url)
        activated_at = time.time()

        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Accounts created')
        result_data = websql.get_result_data()

        # testing
        cp_created_at = result_data[0][0]
        cp_activated_at = result_data[0][1]
        cp_created_at = time.mktime(
            time.strptime(cp_created_at[:19], '%Y-%m-%d %H:%M:%S'))
        cp_activated_at = time.mktime(
            time.strptime(cp_activated_at[:19], '%Y-%m-%d %H:%M:%S'))

        # we are checking that the timestamps are almost equal with a delta of
        # 7 seconds
        self.assertAlmostEqual(created_at, cp_created_at, delta=7)
        self.assertAlmostEqual(activated_at, cp_activated_at, delta=7)

    @tags('desktop', 'accounts')
    def test_account_duplicated(self, wd):
        """Test check create account duplicated"""
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update(email="many@ads.cl")
        page.fill_form(**account_info)
        page.submit_btn.click()
        expected_error = "Este email ya tiene una cuenta asociada"
        found = 0
        for e in page.errors:
            if expected_error in e.text:
                self.assertIn(expected_error, e.text)
                found = 1
                break
        if found == 0:
            self.assertIn(expected_error, page.error_dupl.text)

    @tags('desktop', 'accounts')
    def test_account_existent_user(self, wd):
        """Test check create account existent user"""

        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update(email="uid1@blocket.se")
        page.fill_form(**account_info)
        page.submit_btn.click()
        page = accounts.CreationSuccessDesktop(wd)
        self.assertEqual(page.title.text, "Felicidades!")
        self.assertIn("Est�s a un paso de crear tu cuenta.", page.message.text)

    @tags('desktop', 'accounts')
    def test_account_confirm_invalid_link(self, wd):
        """Test account confirm invalid link"""

        wd.get(yapo.conf.SSL_URL + "/cuenta/confirm")
        page = accounts.Confirm(wd)
        self.assertEqual("Ops! Esta p�gina no existe", page.title.text)
        self.assertEqual(
            "Estas intentando acceder a una p�gina que ya no existe.",
            page.error_message.text)

    @tags('desktop', 'accounts', 'core1')
    def test_edit_account(self, wd):
        """Check edit account """
        if utils.is_not_regress():
            test_data_key = 'my_ads_user'
        else:
            test_data_key = 'user_with_ad_for_edit'
        test_data = JsonTool.read_test_data(test_data_key)

        from_desktop = not hasattr(self, 'user_agent')
        login_and_go_to_dashboard(
            wd, test_data['email'], test_data['password'], from_desktop)

        account_edit = self.EditAccountClass(wd)
        account_edit.go()
        account_edit.wait.until_loaded()

        account_edit.fill_form(
            person_radio=True,
            name='Usuario 01 EDITED',
            rut="16.634.300-3",
            phone_type_fixed=True,
            area_code=45,
            phone="6123432",
            region=14,
            commune=284,
            gender_female=True
        )

        account_edit.submit_btn.click()

        account_edit.go()
        self.assertTrue(account_edit.person_radio.is_selected())
        self.assertEqual(
            'Usuario 01 EDITED', account_edit.name.get_attribute('value'))
        self.assertEqual('6123432', account_edit.phone.get_attribute('value'))
        if not from_desktop:
            self.assertEqual(
                '16634300-3', account_edit.rut.get_attribute('value'))
        else:
            self.assertEqual(
                '16.634.300-3', account_edit.rut.get_attribute('value'))
        self.assertEqual('14', account_edit.region.get_attribute('value'))
        self.assertEqual('284', account_edit.commune.get_attribute('value'))

        account_edit.fill_form(
            person_radio=True,
            name=test_data['username'],
            rut="",
            phone_type_mobile=True,
            phone=test_data['phone'],
            region=test_data['region'],
            commune=test_data['communes']
        )

        account_edit.submit_btn.click()

        if not from_desktop:
            account_edit.wait.until_visible('edit_profile_btn')
            account_edit.edit_profile_btn.click()

        account_edit.go()
        self.assertTrue(account_edit.person_radio.is_selected())
        self.assertEqual(
            test_data['username'], account_edit.name.get_attribute('value'))
        self.assertEqual(
            test_data['phone'], account_edit.phone.get_attribute('value'))
        self.assertEqual('', account_edit.rut.get_attribute('value'))
        self.assertEqual(
            test_data['region'], account_edit.region.get_attribute('value'))
        self.assertEqual(
            str(test_data['communes']),
            account_edit.commune.get_attribute('value'))
        self.assertTrue(account_edit.gender_female.is_selected())

    @tags('desktop', 'accounts')
    def test_errors_edit_account_form_validation(self, wd):
        """Check edit account form errors validation"""
        from_desktop = not hasattr(self, 'user_agent')
        login_and_go_to_dashboard(
            wd, 'prepaid5@blocket.se', '123123123', from_desktop)
        account_edit = self.EditAccountClass(wd)
        account_edit.go()
        account_edit.fill_form(name='', rut='', phone='')
        account_edit.submit_btn.click()

        self.assertEqual(self.name_error_empty, account_edit.err_name.text)

        from_desktop = not hasattr(self, 'user_agent')
        if from_desktop:
            # Is mandatory only for desktop
            self.assertEqual(self.rut_errors_1, account_edit.err_rut.text)

        self.assertEqual(
            self.phone_errors_1, account_edit.err_phone.text)

        account_edit.fill_form(rut='1234567')
        account_edit.submit_btn.click()
        self.assertEqual("El RUT es incorrecto", account_edit.err_rut.text)

    @tags('desktop', 'accounts')
    def test_account_create_gender_and_verify_in_profile(self, wd):
        """ Test account create with gender and verify in edit account """
        utils.clean_mails()
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        account_info.update({
            'email': 'doge2@cabral.es',
            'gender_male': True})
        page.fill_form(**account_info)
        page.submit_btn.click()
        activation_url = utils.search_mails(
            '(https?://.*?cuenta/confirm.*?)[\'\"<\n\s>]')
        self.assertIsNotNone(activation_url)
        wd.get(activation_url)
        account_edit = self.EditAccountClass(wd)
        account_edit.go()
        self.assertTrue(
            account_edit.gender_male.is_selected(),
            'Input gender is not selected')

    @tags('desktop', 'accounts')
    def tests_password_verify(self, wd):
        """ Verify password min / max length """
        page = self.CreateAccountClass(wd)
        page.go()
        account_info = self.account_info.copy()
        # With 6 chars should fail
        account_info.update({
            'password': '123123',
            'password_verify': '123123',
            'email': 'superpass@word.cl',
            'gender_male': True
        })
        page.fill_form(**account_info)
        page.submit_btn.click()
        text_errors = [e.text for e in page.errors]
        [text_errors.remove('') for i in range(text_errors.count(''))]

        expected_errors = ['Tu contrase�a debe tener un m�nimo de 8 caracteres.']
        self.assertEqual(expected_errors, text_errors)
        page.fill('password','1234567890123456789012345')
        page.fill('password_verify','1234567890123456789012345')
        self.assertEqual( page.password.get_attribute('value'), '12345678901234567890')
        self.assertEqual( page.password_verify.get_attribute('value'), '12345678901234567890')



class AccountsCreateTestMobile(AccountCreateTest):
    user_agent = utils.UserAgents.NEXUS_5

    CreateAccountClass = accounts.CreateMobile
    EditAccountClass = accounts.EditMobile

    rut_errors_1 = 'Escribe tu RUT'
    rut_errors_2 = 'El RUT es incorrecto'
    rut_errors_3 = 'El RUT es incorrecto'

    name_error_empty = 'Escribe tu nombre'

    phone_errors_1 = 'Escribe tu n�mero de tel�fono'

    expected_errors = [
        'Selecciona persona o profesional',
        'Escribe tu nombre',
        'Selecciona una regi�n',
        'Escribe tu e-mail',
        'Debes introducir tu contrase�a',
        'Debes introducir tu contrase�a',
        'Tienes que aceptar las condiciones']

    errors = [
        'Debe seleccionar entre persona natural o profesional',
        'Escribe por lo menos dos letras',
        'Escribe entre 6 y 11 caracteres v�lidos, '
        'Por favor, no utilices puntos ni comas',
        'Selecciona una regi�n',
        'Escribe un e-mail v�lido',
        'La password no es v�lida, debes ingresar un '
        'texto de entre 5 y 15 caracteres',
        # Notice this dodgy bastard!
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Tienes que aceptar las condiciones']
