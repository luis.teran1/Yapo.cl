# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from yapo.pages import accounts, generic, desktop, desktop_list, ad_insert, mobile
import yapo


class MobileHttpOnlyCookie(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    username = 'prepaid3@blocket.se'
    password = '123123123'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts')
    def test_httponly_cookie(self, wd):
        """ Check if the acc_session cookie is httpOnly: mobile"""
        login = mobile.Login(wd)
        login.go()
        login.login(self.username, self.password)
        acc_cookie = wd.get_cookie('acc_session');
        httponly = acc_cookie.get('httpOnly', False);
        self.assertTrue(httponly);


class DesktopHttpOnlyCookie(yapo.SeleniumTest):

    snaps = ['accounts']
    username = 'prepaid3@blocket.se'
    password = '123123123'

    @tags('desktop', 'accounts')
    def test_httponly_cookie_home(self, wd):
        """ Check if the acc_session cookie is httpOnly: Home"""
        self._login_and_check_cookie(wd, desktop.DesktopHome)

    @tags('desktop', 'accounts')
    def test_httponly_cookie_listing(self, wd):
        """ Check if the acc_session cookie is httpOnly: Listing"""
        self._login_and_check_cookie(wd, desktop_list.AdListDesktop)

    @tags('desktop', 'accounts')
    def test_httponly_cookie_login(self, wd):
        """ Check if the acc_session cookie is httpOnly: Login Page"""
        self._login_and_check_cookie(wd, desktop.Login)

    @tags('desktop', 'accounts')
    def test_httponly_cookie_ai(self, wd):
        """ Check if the acc_session cookie is httpOnly: Ad Insert"""
        self._login_and_check_cookie(wd, ad_insert.AdInsert)

    @tags('desktop', 'accounts')
    def test_httponly_cookie_login_form(self, wd):
        """ Check if the acc_session cookie is httpOnly: Login Page Form"""
        login = desktop.Login(wd)
        login.go()
        login.login(self.username, self.password)
        self._check_httponly_cookie(wd)

    def _login_and_check_cookie(self, wd, page_object):
        page = page_object(wd)
        page.go()
        acc_bar = generic.AccountBar(wd)
        acc_bar.login(self.username, self.password)
        self._check_httponly_cookie(wd)
        self._read_cookie_from_js(wd)

    def _check_httponly_cookie(self, wd):
        acc_cookie = wd.get_cookie('acc_session');
        httponly = acc_cookie.get('httpOnly', False);
        self.assertTrue(httponly);

    def _read_cookie_from_js(self, wd):
        cookies = wd.execute_script('return document.cookie')
        self.assertNotIn('acc_session', cookies);

