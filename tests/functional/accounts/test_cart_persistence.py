# -*- coding: latin-1 -*-
import yapo
import time
from yapo import utils
from yapo.utils import Products, PacksPeriods
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds, Dashboard, AdDeletePage, AdEdit
from yapo.decorators import tags
from yapo.pages.packs import BuyPacks
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.simulator import Paysim


class CartPersistence(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_prepaid3_11ads_2packs',
             'diff_prepaid3_cat_2040_ads']
    bconfs = {
        '*.*.payment_settings.product.1.3.1220.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1240.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1260.value': 'price:9250',
        '*.*.payment_settings.product.1.31.1220.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1240.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1260.value': 'price:2750',
        '*.*.payment_settings.product.1.31.2020.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2040.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2060.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2080.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2120.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7020.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7040.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7060.value': 'price:2050',
        '*.*.payment_settings.product.1.31.*.value': 'price:1050',
        '*.*.payment_settings.product.1.32.2020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2060.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2080.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2120.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7060.value': 'price:22050',
        '*.*.payment_settings.product.1.32.*.value': 'price:12550',
    }

    def setUp(self):
        utils.flush_redises()

    @tags('desktop', 'cart')
    def test_empty_selection(self, wd):
        """ Cart: Check no products are selected at the beginning"""
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        summary = SummaryPayment(wd)
        summary.go_summary()
        summary.wait.until_present('empty_cart')
        self.assertNotPresent(summary, 'products_list_table')

    @tags('desktop', 'cart', 'ads')
    def test_select_ads(self, wd):
        """ Cart: Load the page and select some ads"""
        LIST_IDS = {
            6000667: [Products.GALLERY],
            6000666: [Products.GALLERY_1],
            3456789: [Products.GALLERY_30]
        }

        expected_values = [
            ['Departamento Tarapac�', 'Vitrina 7 d�as', '$ 9.250', ''],
            ['Pesas muy grandes', 'Vitrina 1 d�a', '$ 1.050', ''],
            ['Race car', 'Vitrina 30 d�as', '$ 22.050', '']
        ]
        self._add_prod_and_check(wd, LIST_IDS, expected_values)
        self._check_preselected_products(wd, expected_values)

    @tags('desktop', 'cart', 'ads')
    def test_select_galleries_same_ad(self, wd):
        """ Cart: Select all galleries same ad"""
        LIST_IDS = {
            6000666: [Products.GALLERY, Products.GALLERY_1, Products.GALLERY_30]
        }

        expected_values = [
            ['Pesas muy grandes', 'Vitrina 30 d�as', '$ 12.550', '']
        ]
        self._add_prod_and_check(wd, LIST_IDS, expected_values)
        self._check_preselected_products(wd, expected_values)

    @tags('desktop', 'cart')
    def test_empty_cart(self, wd):
        """ Cart: Check if the user is logged and open payment page he can see an empty list (The user come from dashboard page)
        - Also check the sticky bar is not visible
        - Also check the messages on empty cart
        """
        self._check_preselected_products(wd, [])

    # Pack Tests
    def test_selected_pack(self, wd):
        """ Check selected pack """
        login = Login(wd)
        login.login('prepaid3@blocket.se', '123123123')
        buy_pack = BuyPacks(wd)
        buy_pack.go()
        buy_pack.select_slots("020")
        buy_pack.select_period(PacksPeriods.QUARTERLY)
        buy_pack.pay_button.click()
        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', '']
        ]
        self._check_preselected_products(wd, expected_values, with_login=False)
        login.logout()
        self._check_preselected_products(
            wd, expected_values, user="prepaid3@blocket.se")

    # Check that cart does not get stuck with products from deleted ads
    def test_unstuck_cart(self, wd):
        """ Check unstuck cart """
        utils.flush_redises()
        LIST_IDS = {
            6000667: [Products.GALLERY_1],
            3456789: [Products.GALLERY_1]
        }

        expected_values = [
            ['Departamento Tarapac�', 'Vitrina 1 d�a', '$ 2.750', ''],
            ['Race car', 'Vitrina 1 d�a', '$ 2.050', '']
        ]

        to_delete = ['Departamento Tarapac�', 'Race car']

        self._add_prod_and_check(wd, LIST_IDS, expected_values)
        self._ads_to_delete(wd, to_delete)
        self._check_preselected_products(wd, [], with_login=False)

    def test_deactivated_pack_unstuck_cart(self, wd):
        """ Check that products for deactivated pack ads get cleaned """
        LIST_IDS = {
            8000096: [Products.GALLERY]
        }

        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se', '123123123')
        dma = DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(LIST_IDS)
        dma.active_ads[8000096].enable_toggle.click()  # disable ad
        dma.go_dashboard()
        utils.rebuild_index()
        self._check_preselected_products(wd, [], with_login=False)

    def _add_prod_and_check(self, wd, LIST_IDS, expected_values):
        login = Login(wd)
        login.go()
        login.login('prepaid5@blocket.se', '123123123')
        dma = DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(LIST_IDS)
        dma.checkout_button.click()
        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)
        login.logout()

    def _check_preselected_products(self, wd, expected_values, with_login=True, user="prepaid5@blocket.se"):
        """Goes directly to summary page and check the selected products"""
        summary_page = SummaryPayment(wd)
        if with_login:
            login = Login(wd)
            login.go()
            login.login(user, '123123123')
        summary_page.go_summary()

        if len(expected_values) != 0:
            table_values = summary_page.get_table_data('products_list_table')
            self.assertListEqual(table_values, expected_values)
        else:
            summary_page.wait.until_not_present('products_list_table')
            self.assertTrue(summary_page.empty_cart)

    def _ads_to_delete(self, wd, ad_list, with_login=True, user="prepaid5@blocket.se"):
        if with_login:
            login = Login(wd)
            login.go()
            login.login(user, '123123123')
        dash_my_ads = DashboardMyAds(wd)
        summary_page = SummaryPayment(wd)
        for subject in ad_list:
            dash_my_ads.go_dashboard()
            dash_my_ads.delete_ad_by_subject(subject)
        utils.rebuild_index()



class CartPersistenceBasic(yapo.SeleniumTest):
    snaps = ['accounts']

    AdViewPO = AdViewDesktop
    SummaryPO = SummaryPayment
    DeletePO = AdDeletePage
    EditPO = AdEdit
    DashboardPO = DashboardMyAds

    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:1050',
        '*.*.payment_settings.product.1.10.*.value': 'price:1050',
        '*.*.payment_settings.product.1.3.1220.value': 'price:9550',
        '*.*.payment_settings.product.1.3.1240.value': 'price:9550',
        '*.*.payment_settings.product.1.3.1260.value': 'price:9550',
        '*.*.payment_settings.product.1.31.*.value': 'price:1750',
        '*.*.payment_settings.product.1.32.*.value': 'price:13550',
    }

    def setUp(self):
        utils.flush_redises()

    def test_add_cart_in_adview(self, wd):
        """Check that the bump button in ad view actually put the bump into the summary page"""
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        adview = self.AdViewPO(wd)
        adview.go(list_id=8000073)
        adview.product_link_bump.click()
        summary = self.SummaryPO(wd)
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Subir Ahora $ 1.050', summary.product_row0.text)
        self.assertFalse(summary.is_element_present('product_row1'))

    def test_add_cart_from_ad_delete(self, wd):
        """Check that the bump button in ad delete actually put the bump into the summary page"""
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        adview = self.AdViewPO(wd)
        adview.go(list_id=8000073)
        adview.ad_admin_remove_link.click()
        delete = self.DeletePO(wd)
        delete.select_reason(4)
        delete.bum_btn_4.click()
        summary = self.SummaryPO(wd)
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Subir Ahora $ 1.050', summary.product_row0.text)
        self.assertFalse(summary.is_element_present('product_row1'))

    def test_add_cart_from_ad_edit(self, wd):
        """Check that the "I want bump" in ad edit actually put the bump into the summary page"""
        login = Login(wd)
        login.login('prepaid3@blocket.se', '123123123')
        adview = self.AdViewPO(wd)
        adview.go(list_id='8000070')
        adview.ad_admin_edit_link.click()
        adedit = self.EditPO(wd)
        wd.execute_script("window.scrollTo(0, 100000);")
        adedit.wait.until_visible('more_info')
        adedit.want_bump(type='buy_bump')
        adedit.submit.click()
        summary = self.SummaryPO(wd)
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Libro 3 Subir Ahora $ 1.050', summary.product_row0.text)
        self.assertFalse(summary.is_element_present('product_row1'))

    def test_add_cart_from_ad_edit_upselling(self, wd):
        login = Login(wd)
        login.login('many@ads.cl', '123123123')
        adview = self.AdViewPO(wd)
        adview.go(list_id='8000076')
        adview.ad_admin_edit_link.click()
        adedit = self.EditPO(wd)
        wd.execute_script("window.scrollTo(0, 100000);")
        adedit.wait.until_visible('combo_upselling_box')
        adedit.want_bump()
        adedit.submit.click()
        summary = self.SummaryPO(wd)
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Est�ndar: Subir ahora $ 1.500', summary.product_row0.text)
        self.assertFalse(summary.is_element_present('product_row1'))

    def test_check_payment_ok_failed_bump(self, wd):
        """Check that the elements remains in the cart after a failed payment """
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        dashboard = self.DashboardPO(wd)
        dashboard.active_ads[8000073].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[6000666].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[8000065].select_product(utils.Products.GALLERY_1)
        dashboard.checkout_button.click()
        summary = self.SummaryPO(wd)

        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Vitrina 1 d�a $ 1.750', summary.product_row0.text)

        self.assertTrue(summary.is_element_present('product_row1'))
        self.assertTrue(summary.product_row1.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 1 d�a $ 1.750', summary.product_row1.text)

        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Aviso de prepaid5 Vitrina 1 d�a $ 1.750', summary.product_row2.text)
        summary.radio_webpay.click()
        summary.link_to_pay_products.click()
        paysim = Paysim(wd)
        paysim.pay_cancel()
        summary = self.SummaryPO(wd)

        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Vitrina 1 d�a $ 1.750', summary.product_row0.text)

        self.assertTrue(summary.is_element_present('product_row1'))
        self.assertTrue(summary.product_row1.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 1 d�a $ 1.750', summary.product_row1.text)

        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Aviso de prepaid5 Vitrina 1 d�a $ 1.750', summary.product_row2.text)

    def test_check_payment_refused_failed_bump(self, wd):
        """Check that the elements remains in the cart after a refused payment """
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        dashboard = self.DashboardPO(wd)
        dashboard.active_ads[8000073].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[6000666].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[8000065].select_product(utils.Products.GALLERY_1)
        dashboard.checkout_button.click()
        summary = self.SummaryPO(wd)
        summary.wait.until_present('product_row0')
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Vitrina 1 d�a $ 1.750', summary.product_row0.text)
        summary.wait.until_present('product_row1')
        self.assertTrue(summary.is_element_present('product_row1'))
        self.assertTrue(summary.product_row1.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 1 d�a $ 1.750', summary.product_row1.text)
        summary.wait.until_present('product_row2')
        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Aviso de prepaid5 Vitrina 1 d�a $ 1.750', summary.product_row2.text)
        summary.radio_webpay.click()
        summary.link_to_pay_products.click()
        paysim = Paysim(wd)
        paysim.pay_refuse()
        summary = self.SummaryPO(wd)

        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Adeptus Astartes Codex Vitrina 1 d�a $ 1.750', summary.product_row0.text)

        self.assertTrue(summary.is_element_present('product_row1'))
        self.assertTrue(summary.product_row1.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 1 d�a $ 1.750', summary.product_row1.text)

        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Aviso de prepaid5 Vitrina 1 d�a $ 1.750', summary.product_row2.text)

    def test_remove_items_from_cart(self, wd):
        """Check that the elements are correctly removed from cart"""
        summary = self.SummaryPO(wd)
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')

        dashboard = self.DashboardPO(wd)
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        dashboard.active_ads[6000667].select_product(utils.Products.GALLERY)
        summary.wait.until(lambda x:summary.amount.text == '9.550')

        dashboard.active_ads[6000666].select_product(utils.Products.GALLERY_30)
        summary.wait.until(lambda x:summary.amount.text == '23.100')

        dashboard.active_ads[8000065].select_product(utils.Products.GALLERY_1)
        summary.wait.until(lambda x:summary.amount.text == '24.850')

        dashboard.checkout_button.click()
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Departamento Tarapac� Vitrina 7 d�as $ 9.550', summary.product_row0.text)

        self.assertTrue(summary.is_element_present('product_row1'))
        self.assertTrue(summary.product_row1.is_displayed())
        self.assertIn('Aviso de prepaid5 Vitrina 1 d�a $ 1.750', summary.product_row1.text)

        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 30 d�as $ 13.550', summary.product_row2.text)

        summary.remove_product(1)
        summary.wait.until_not_present('product_row1')
        self.assertEqual(
            [['Departamento Tarapac�', 'Vitrina 7 d�as', '$ 9.550', ''], ['Pesas muy grandes', 'Vitrina 30 d�as', '$ 13.550', '']],
            summary.get_table_data()
        )

        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Departamento Tarapac� Vitrina 7 d�as $ 9.550', summary.product_row0.text)

        self.assertTrue(summary.is_element_present('product_row2'))
        self.assertTrue(summary.product_row2.is_displayed())
        self.assertIn('Pesas muy grandes Vitrina 30 d�as $ 13.550', summary.product_row2.text)
        summary.remove_product(1)
        summary.wait.until_not_visible('product_row2')
        self.assertEqual(
            [['Departamento Tarapac�', 'Vitrina 7 d�as', '$ 9.550', '']],
            summary.get_table_data()
        )
        self.assertTrue(summary.is_element_present('product_row0'))
        self.assertTrue(summary.product_row0.is_displayed())
        self.assertIn('Departamento Tarapac� Vitrina 7 d�as $ 9.550', summary.product_row0.text)
        summary.remove_product(0)
        summary.wait.until_not_visible('product_row0')

    def test_follow_items_from_summary(self, wd):
        """Check that the ad links are working in the summary page"""
        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        dashboard = self.DashboardPO(wd)
        dashboard.active_ads[6000667].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[6000666].select_product(utils.Products.GALLERY_1)
        dashboard.active_ads[8000065].select_product(utils.Products.GALLERY_1)
        dashboard.checkout_button.click()
        summary = self.SummaryPO(wd)
        summary.wait.until_visible('product_row0')
        self.assertTrue(summary.product_row0.is_displayed())
        summary.product_row_link0.click()

        adview = self.AdViewPO(wd)
        adview.wait.until_present('subject')
        self.assertEqual('Departamento Tarapac�', adview.subject.text)
        summary.go()
        summary.product_row_link1.click()
        adview = self.AdViewPO(wd)
        adview.wait.until_present('subject')
        self.assertEqual('Pesas muy grandes', adview.subject.text)
        summary.go()
        summary.product_row_link2.click()
        adview = self.AdViewPO(wd)
        adview = self.AdViewPO(wd)
        adview.wait.until_present('subject')
        self.assertEqual('Aviso de prepaid5', adview.subject.text)
