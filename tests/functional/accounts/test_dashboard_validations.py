# -*- coding: latin-1 -*-
import yapo
from yapo.pages import accounts
from yapo.decorators import tags

from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop import DashboardMyAds, Dashboard, DashboardAd, Login, ProductsInfoLightboxDashboard
from yapo.mail_generator import Email
from yapo import steps

class DashboardValidations(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_ads_with_uf']
    passwd = '123123123'

    @tags('desktop', 'dashboard', 'currency')
    def test_verify_elements(self, wd):
        """
        Verifies for a set of expected ads and elements in the first page
        """
        login = Login(wd)
        login.login("prepaid5@blocket.se", self.passwd)

        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()

        dash = Dashboard(wd)
        self.assertEqual("Mis avisos", dash.link_my_ads.text)
        self.assertEqual("Perfil", dash.link_profile.text)

        ads = [
            ['Depto central muy bien cuidado', '$ 85.000.000'],
            ['Departamento central', 'UF 3.245,98'],
            ['Oficina central', 'UF 31,55'],
            ['Silla vieja', ''],
            ['Barncykel','$ 666']
        ]

        for subject, price in ads:
            ad = dashboard.search_ad_by_subject(subject)
            self.assertIsNotNone(ad)
            self.assertEqual(price, ad.price.text)

        bump_buttons = dashboard.list_all_bump_buttons
        for button in bump_buttons:
            self.assertEqual(button.text, u"Subir")

    @tags('desktop', 'dashboard')
    def test_dashboard_more_info_boxes(self, wd):
        """
        Verifies the info boxes of the products in the dashboard
        """
        login = Login(wd)
        login.login("prepaid5@blocket.se", self.passwd)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        products_modal = ProductsInfoLightboxDashboard(wd)

        products_modal.open_bump()
        self.assertTrue(products_modal.autobump_title.is_displayed())
        self.assertTrue(products_modal.autobump_preview.is_displayed())
        self.assertTrue(products_modal.autobump_popup.is_displayed())
        self.assertTrue(products_modal.autobump_note.is_displayed())
        self.assertTrue(products_modal.autobump_foot.is_displayed())
        products_modal.close()

        products_modal.open_gallery()
        self.assertTrue(products_modal.gallery_info_foot.is_displayed())
        self.assertTrue(products_modal.gallery_wrap.is_displayed())
        self.assertTrue(products_modal.gallery_info_bottom.is_displayed())
        self.assertTrue(products_modal.gallery_info_top.is_displayed())
        products_modal.close()

        products_modal.open_label()
        self.assertTrue(products_modal.label_preview.is_displayed())
        self.assertTrue(products_modal.label_mesage.is_displayed())
        self.assertTrue(products_modal.label_listing.is_displayed())
        self.assertTrue(products_modal.label_info_top.is_displayed())

    @tags('desktop', 'dashboard')
    def test_user_with_no_ads(self, wd):
        """
        Test the dashboard message for a user with 0 ads
        activo or inactive
        """
        login = Login(wd)
        login.login("no-ads@yapo.cl", self.passwd)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        self.assertEquals(u"Sin avisos", dashboard.no_ads.text)

    @tags('desktop', 'dashboard')
    def test_no_ads_arrow(self, wd):
        """
        Searches for the arrow displayed on the dashboard when the user has
        no ads
        """
        login = Login(wd)
        login.login("no-ads@yapo.cl", self.passwd)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        self.assertVisible(dashboard, 'publish_ad_arrow')

    @tags('desktop', 'dashboard')
    def test_ads_with_price_empty(self, wd):
        """
        Checks that dollar sign doesn't appear when ads have no price
        """
        email = "test@mail.cl"
        account_info = dict(
            person_radio=True,
            name='Tester',
            phone='45454545',
            region=14,
            email=email,
            password='123123123',
            password_verify='123123123',
            accept_conditions=True
        )

        page = accounts.Create(wd)
        page.go()
        page.fill_form(**account_info)
        page.submit_btn.click()
        page = accounts.CreationSuccessDesktop(wd)
        self.assertEqual(page.title.text, "Felicidades!")
        self.assertIn("Estás a un paso de crear tu cuenta.", page.message.text)

        mlt = Email.create(wd)
        mlt.check_account_activation(self, account_info['email'])
        dash = Dashboard(wd)
        success_login = "Hola {0}".format(account_info['name'])
        self.assertIn(success_login, dash.title_legend.text)

        page = AdInsert(wd)
        page.go()
        steps.insert_an_ad(wd, AdInsert, subject='olallamakase',
                       body="olakasease",
                       region=15,
                       communes=310,
                       category=8020,
                       price='',
                       accept_conditions=True,
                       logged=True)

        my_ads = DashboardMyAds(wd)
        my_ads.go_dashboard()

        dash_ad = DashboardAd(wd)
        self.assertEquals(dash_ad.subject.text, u"Olallamakase")
        self.assertEquals(dash_ad.price.text, u"")


class DashboardMessages(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_facebooklogin']

    def test_dashboard_message_missing_info(self, wd):
        """ Tests message missing information in dashboard header, when phone is missing"""
        login = Login(wd)
        login.login("dcggtcu_warmanman_1456766679@tfbnw.net", '123123123')
        edit_account = accounts.Edit(wd)
        edit_account.go()
        self.assertFalse(edit_account.phone.text)
        dashboard = Dashboard(wd)
        self.assertIn('Debes actualizar la información de tu perfil.', [m.text for m in dashboard.dashboard_messages])

    def test_dashboard_close_message_missing_info(self, wd):
        """ Tests close message missing information in dashboard header, when phone is missing"""
        login = Login(wd)
        login.login("dcggtcu_warmanman_1456766679@tfbnw.net", '123123123')
        dashboard = Dashboard(wd)
        dashboard.go()
        self.assertIn('Debes actualizar la información de tu perfil.', [m.text for m in dashboard.dashboard_messages])

        [close.click() for close in dashboard.close_message_icon]

        dashboard.wait.until_not_visible('dashboard_messages')
