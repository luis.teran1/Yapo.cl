# -*- coding: latin-1 -*-
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import Dashboard as DashboardDesktop, DashboardJobsTab
from yapo.decorators import tags
import yapo
import time


class DashboardJobsBase(yapo.SeleniumTest):

    def _jobs_tab_visibility_case(self, wd, email, should_be_displayed):
        login_and_go_to_dashboard(wd, email, '123123123')
        check = self.assertVisible if should_be_displayed else self.assertNotVisible
        dashboard = DashboardDesktop(wd)
        check(dashboard, 'link_job')

    def _get_report_case(self, wd, email, recipient_email, index, result):
        login_and_go_to_dashboard(wd, email, '123123123')
        dashboard = DashboardDesktop(wd)
        jobs_tab = dashboard.go_jobs()
        wd.execute_script("window.scrollTo(0, 100000);")
        jobs_tab.fill_form(index, email)
        self.assertEqual(result, jobs_tab.response.text)


class DashboardJobsTabDisabled(DashboardJobsBase):

    snaps = ['accounts', 'diff_pro_account', 'diff_jobs_ads']
    bconfs = {
        '*.*.jobs_tab.enabled': '0'
    }

    @tags('desktop', 'accounts')
    def test_jobs_tab_not_displayed(self, wd):
        """Tab must not be displayed if disabled"""

        data = {'email': 'prepaid5@blocket.se', 'should_be_displayed': False}
        self._jobs_tab_visibility_case(wd, **data)


class TestDashboardJobsTab(DashboardJobsBase):

    snaps = ['accounts', 'diff_pro_account', 'diff_jobs_ads']
    bconfs = {
        '*.*.jobs_tab.enabled': '1'
    }

    @tags('desktop', 'accounts')
    def test_account_my_jobs_ads(self, wd):
        """ Pro account should see jobs tab """

        data = {'email': 'prepaid5@blocket.se', 'should_be_displayed': True}
        self._jobs_tab_visibility_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_account_my_jobs_ads_no_pro_user(self, wd):
        """ Regular user should not see jobs tab """

        data = {'email': 'no-ads@yapo.cl', 'should_be_displayed': False}
        self._jobs_tab_visibility_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_account_my_jobs_ads_pro_user_other_categories(self, wd):
        """ Pro account for other categories should not see jobs tab """

        data = {'email': 'prepaid3@blocket.se', 'should_be_displayed': False}
        self._jobs_tab_visibility_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_with_pro_unpaid(self, wd):
        """ Get report with pro user without paid any ad"""

        data = {
            'email': 'prepaid5@blocket.se',
            'recipient_email': 'test@test.cl',
            'index': 1,
            'result': DashboardJobsTab.ERROR_USER_HAS_NOT_PAID_FOR_AN_AD
        }
        self._get_report_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_with_pro_paid(self, wd):
        """ Get report with pro user with paid ad"""

        data = {
            'email': 'paid_jobs_inserting_fee@test.cl',
            'recipient_email': 'test@test.cl',
            'index': 2,
            'result': DashboardJobsTab.SUCCESS
        }
        self._get_report_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_pro_jobs_and_other_categories(self, wd):
        """ Get report with pro user in jobs and other categories """

        data = {
            'email': 'prepaid5@blocket.se',
            'recipient_email': 'test@test.cl',
            'index': 3,
            'result': DashboardJobsTab.ERROR_USER_HAS_NOT_PAID_FOR_AN_AD
        }
        self._get_report_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_ad_hasnt_paid_inserting_fee(self, wd):
        """ Get report using an ad without paid inserting fee """

        data = {
            'email': 'prepaid5@blocket.se',
            'recipient_email': 'test@test.cl',
            'index': 1,
            'result': DashboardJobsTab.ERROR_USER_HAS_NOT_PAID_FOR_AN_AD
        }
        self._get_report_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_ad_unpaid_pro_hasnt_contacts(self, wd):
        """ Get report using an ad without contacts """

        data = {
            'email': 'prepaid5@blocket.se',
            'recipient_email': 'test@test.cl',
            'index': 1,
            'result': DashboardJobsTab.ERROR_USER_HAS_NOT_PAID_FOR_AN_AD
        }
        self._get_report_case(wd, **data)

    @tags('desktop', 'accounts')
    def test_get_jobs_report_with_pro_paid_without_contacts(self, wd):
        """ Get report for an ad without contacts using pro user who had paid for an ad"""

        data = {
            'email': 'paid_jobs_inserting_fee@test.cl',
            'recipient_email': 'test@test.cl',
            'index': 1,
            'result': DashboardJobsTab.ERROR_NO_CONTACTS
        }
        self._get_report_case(wd, **data)
