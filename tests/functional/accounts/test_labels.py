# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
import selenium
from yapo import utils
from yapo.decorators import tags
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.utils import Products, Label_texts, Label_values
import time
import yapo
import yapo.conf


class LabelsMobile(yapo.SeleniumTest):

    snaps = ['accounts', 'addaccountandroid']
    user_agent = utils.UserAgents.IOS
    account_pass = "123123123"
    congrats = "Tu compra se realiz� exitosamente"
    data_listing = {'has_address': False, 'has_label': Label_texts.URGENT}
    platform = mobile
    AdView = AdViewMobile
    AdList = AdListMobile
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0',
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0',
    }
    shadowList = True

    @tags('accounts', 'dashboard', 'products', 'label')
    def test_check_counter_header(self, wd):
        """ Label increase and decrease counter on header """
        user = 'prepaid3@blocket.se'
        login = self.platform.Login(wd)
        login.login(user, self.account_pass)
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        count = 0
        for list_id, active in dashboard.active_ads.items():
            active.select_product(Products.LABEL, Label_values.NEW)
            count += 1
            self.assertEquals(dashboard.products_in_cart.text, str(count))
        for list_id, active in dashboard.active_ads.items():
            active.unselect_product(Products.LABEL)
            count -= 1
            self.assertEquals(dashboard.products_in_cart.text, str(count))

    @tags('accounts', 'dashboard', 'products', 'label')
    def test_already_selected(self, wd):
        """ Label is already selected on dashboard (Cart persistence) """
        user = 'user.01@schibsted.cl'
        login = self.platform.Login(wd)
        login.login(user, self.account_pass)
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            active.select_product(Products.LABEL, Label_values.OPPORTUNITY)

        login.logout()
        login.login(user, self.account_pass)
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            self.assertTrue(
                active.is_selected(Products.LABEL, Label_values.OPPORTUNITY))

    def _buy_products(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    wd.execute_script("return arguments[0].scrollIntoView();", active.subject)
                    active.select_product(prod['prod'], prod['param'])

        summary = self.platform.SummaryPayment(wd)
        summary.go_summary()
        summary.press_link_to_pay_products()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)

        # Run rebuild-index apply product
        utils.rebuild_index()

    def _check_on_dashboard(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        wd.execute_script("window.scrollTo(0, 500);")
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    if prod['text'] is not None:
                        self.assertTrue(
                            active.has_product_selector_disabled(prod['prod']))
                        self.assertEqual(active.label_tag.text, prod['text'])

    @tags('accounts', 'dashboard', 'products', 'label')
    def test_label_pricing(self, wd):
        """ Check price segmentation for labels """
        user = 'android@yapo.cl'

        login = self.platform.Login(wd)
        login.login(user, self.account_pass)

        data_products = {
            '8000031': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '3020',
                'price': '$2.100'},

            '8000026': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '3060',
                'price': '$2.100'},

            '8000025': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '6080',
                'price': '$2.100'},

            '8000020': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '7040',
                'price': '$2.200'},

            '8000016': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '2060',
                'price': '$2.200'},

            '8000013': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '2020',
                'price': '$2.200'},

            '8000018': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '1220',
                'price': '$3.150'},

            '8000003': {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT,
                'cat': '1240',
                'price': '$3.150'},

        }

        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard('page=2')
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in data_products.items():
                if str(list_id) == p_lid:
                    active.select_product(prod['prod'], prod['param'])
                    self.assertEquals(
                        active.get_product_price(
                            prod['prod'],
                            prod['param']),
                        prod['price'])


class LabelsDesktop(LabelsMobile):

    user_agent = utils.UserAgents.FIREFOX_MAC
    # defining a user agent, the user_agent property of firefox is
    # automagically changed
    congrats = "Tu compra se realiz� exitosamente"
    account_pass = "123123123"
    data_listing = {
        'region': 'Regi�n Metropolitana',
        'date': 'Hoy',
        'commune': 'Macul',
        'title': 'Adeptus Astartes Codex',
        'has_address': False,
        'price': '$ 778.889',
        'has_label': 'Urgente'
    }
    platform = desktop
    AdView = AdViewDesktop
    AdList = AdListDesktop
    shadowList = False
