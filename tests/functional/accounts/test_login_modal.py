# coding: latin-1
from yapo.decorators import tags
from yapo.pages.desktop import Login, LoginModal, HeaderElements, FavoritePage, DesktopHome
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages import accounts, generic, help, ad_insert
from yapo import utils
import yapo

class LoginModalTest(yapo.SeleniumTest):
    """ Testing that login modal works as expected """

    snaps = ['accounts']


    @tags('accounts', 'login')
    def test_from_li(self, wd):
        """ Is modal working in li? """
        self._test_from_page(wd, AdListDesktop)

    @tags('accounts', 'login', 'help')
    def test_from_help(self, wd):
        """ Is modal working in FAQ? """
        self._test_from_page(wd, help.Help)

    @tags('accounts', 'login', 'ad_view')
    def test_from_vi(self, wd):
        """ Is modal working in ad view? """
        self._test_from_page(wd, AdViewDesktop, list_id=8000072)

    @tags('accounts', 'login', 'ad_insert')
    def test_from_ai(self, wd):
        """ Is modal working in ad insert? """
        self._test_from_page(wd, ad_insert.AdInsert, Login)

    @tags('accounts', 'login', 'home')
    def test_from_home(self, wd):
        """ Is modal working in home page? """
        self._test_from_page(wd, DesktopHome)

    def _test_from_page(self, wd, start_page, end_page=None, **kwargs):
        """ Is modal working in `page`? """

        # default para la pagina de destino
        # XXX por qu� mierda comentamos en espa�ol?
        if not end_page:
            end_page = start_page

        # borra todas las cookies
        wd.delete_all_cookies()

        # go to the page
        page = start_page(wd)
        page.go(**kwargs)

        # hace click en el boton de mi cuenta
        login_modal = LoginModal(wd)
        login_modal.open()

        # verifica que esten y sean visibles los inputs de email y password y submit
        login_modal.wait.until_visible('account_email')
        login_modal.wait.until_visible('account_password')

        # hace click en submit y espera que aparezca errores
        login_modal.account_submit.click()
        msgs = ['Escribe tu e-mail', 'Debes introducir tu contrase�a']
        actual_msgs = [error_msg.text for error_msg in login_modal.account_errors]
        self.assertEqual(msgs, actual_msgs)

        # ingresa el email y pass
        login_modal.fill_form(account_email='lala@lolo', account_password='123123123')
        login_modal.account_submit.click()
        msgs = ['Escribe un e-mail v�lido']
        actual_msgs = [error_msg.text for error_msg in login_modal.account_errors if error_msg.text !='']
        self.assertEqual(msgs, actual_msgs)

        # ingresa datos correctas y espera que loguee y que muestre los datos del usuario
        login_modal.fill_form(account_email='prepaid5@blocket.se', account_password='123123123')
        login_modal.account_submit.click()

        # el usuario deber�a seguir en el page
        page = start_page(wd)
        page.wait.until_loaded()
        self.assertTrue(page.am_i_here())
        header_elements = HeaderElements(wd)
        self.assertEqual('BLOCKET', header_elements.who_is_logged())

        # hace click para desloguearse
        header_elements.button_logout_user.click()

        # verifica que se desloguee, que siga en el page y que ya no exista la cookie
        page = end_page(wd)
        header_elements.wait.until_not_present('button_logged_user')
        self.assertEqual('', header_elements.who_is_logged())
        self.assertTrue(page.am_i_here())
