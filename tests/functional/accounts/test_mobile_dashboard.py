# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo.pages.ad_insert import AdInsertResult
from yapo import utils
import yapo


class DashboardProducts(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.accounts.dashboard.ads_per_page': "10",
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    account_user = "many@ads.cl"
    account_pass = "123123123"

    @tags("mobile", "dashboard", "ad_insert")
    def test_insertad_mobile_appear_in_dashboard(self, wd):
        """ Inserts ads in mobile site and they appear in the dashboard """

        subject_prefix = 'Aviso de prueba {0}'
        ad_insert = mobile.NewAdInsert(wd)
        ad_insert.go()
        ad_insert.insert_ad(subject=subject_prefix.format("21"),
                            body="BIBA BENESUELA MAR PARA VOLIVYA CARAJOS",
                            category="8020",
                            price="1337",
                            region="15",
                            communes="310",
                            name="mr many",
                            email=self.account_user,
                            phone="1234566547",
                            passwd=self.account_pass,
                            passwd_ver=self.account_pass
                           )
        ad_insert.submit.click()

        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()

        ads_subjects = [subject_prefix.format(i) for i in [21, 20, 19, 18, 17, 16, 15, 14, 13, 12]]
        for ad, ad_subject in zip(dashboard.inactive_ads_list, ads_subjects):
            self.assertEqual(ad.subject.text, ad_subject)
