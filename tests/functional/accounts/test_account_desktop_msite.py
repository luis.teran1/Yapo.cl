# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import accounts
from yapo.pages.desktop import Dashboard as DashboardDesktop
from yapo.pages.mobile import Dashboard as DashboardMobile
from yapo import utils
from yapo.mail_generator import Email
import time
import yapo
import yapo.conf


class TestAccountsDesktop(yapo.SeleniumTest):

    snaps = ['accounts']
    pro_data = {}
    bconfs = {
        '*.*.accounts.dashboard.ads_per_page': 10,
        "*.*.movetoapps_splash_screen.enabled": '0'
    }
    CreationSuccess = accounts.CreationSuccessDesktop
    DashBoard = DashboardDesktop
    CreateAccountClass = accounts.Create
    account_info = dict(
        person_radio=True,
        name='Humberto Schibsted',
        phone='94545454',
        region=14,
        email='{0}@mailinator.com'.format(time.time()),
        password='123123123',
        password_verify='123123123',
        accept_conditions=True
    )
    success_login = "Hola {0}".format(account_info['name'])

    @tags('accounts', 'minimal', 'core1')
    def test_create_account(self, wd):
        """ Create an account in desktop """
        page = self.CreateAccountClass(wd)
        page.go()
        page.fill_form(**self.account_info)
        page.submit_btn.click()
        time.sleep(30)
        page = self.CreationSuccess(wd)
        self.assertEqual(page.title.text, "Felicidades!")
        self.assertIn("Est�s a un paso de crear tu cuenta.", page.message.text)

        mlt = Email.create(wd)
        mlt.check_account_activation(self, self.account_info['email'])
        dash = self.DashBoard(wd)
        self.assertIn(self.success_login, dash.title_legend.text)


class TestAccountsMobile(TestAccountsDesktop):
    user_agent = utils.UserAgents.NEXUS_5
    CreationSuccess = accounts.CreationSuccessMobile
    DashBoard = DashboardMobile
    success_login = "Hola, "
    CreateAccountClass = accounts.CreateMobile
