# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from selenium.webdriver.common.by import By
from yapo import utils
from yapo.decorators import tags
from yapo.decorators import retries_function_exception
from yapo.pages import mobile
from yapo.utils import Products
import time
import yapo
import yapo.conf
import selenium.webdriver.support.expected_conditions as EC


class DashboardProducts(yapo.SeleniumTest):
    user_agent = utils.UserAgents.IOS
    snaps = ['accounts', 'addaccountandroid']
    bconfs = {
        '*.*.payment.product.3.expire_time': '10 seconds',
        '*.*.upselling_settings.command.3.default_params.3.value': 'expire_time:10 seconds',
        '*.*.payment.product_specific.weekly_bump.interval_seconds': '5',
        '*.*.payment.product_specific.weekly_bump.iterations': '2',
        '*.*.upselling_settings.command.2.default_params.2.value': 'counter:2',
        '*.*.autobump.visible_last_digit.1': '0',
        '*.*.movetoapps_splash_screen.enabled': '0',
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0',
    }
    account_user = "prepaid5@blocket.se"
    account_pass = "123123123"

    def setUp(self):
        utils.flush_redises()
        utils.restore_snaps_in_a_cooler_way(['accounts', 'addaccountandroid'])

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_highlight_ads_active_inactive(self, wd):
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for inactive in dashboard.list_inactive_ads:
            self.assertRaises(exceptions.NoSuchElementException, lambda: inactive.find_element_by_css_selector(dashboard.selector_css_highlight))
        for active in dashboard.list_active_ads:
            self.assertTrue(active.find_element_by_css_selector(dashboard.selector_css_highlight))

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_show_hide_highlight(self, wd):
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        active = list(reversed(dashboard.list_active_ads))[0]
        dashboard.wait.until(EC.element_to_be_clickable((By.CSS_SELECTOR, dashboard.selector_css_highlight)));
        active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        dashboard.wait.until(lambda x: active.find_element_by_css_selector(dashboard.selector_css_product_list).is_displayed(), 'Waiting for hightlight option to be displayed')
        self.assertTrue(active.find_element_by_css_selector(dashboard.selector_css_product_list).is_displayed())
        active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        self.assertFalse(active.find_element_by_css_selector(dashboard.selector_css_product_list).is_displayed())

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_check_counter_header(self, wd):
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        dash_header = mobile.CommonAccountElements(wd)
        count = 0
        for list_id, active in dashboard.active_ads.items():
            active.select_product(Products.LABEL, "1")
            count += 1
            self.assertEquals(dash_header.text_cart_amount.text, str(count))
        for list_id, active in dashboard.active_ads.items():
            active.unselect_product(Products.LABEL)
            count -= 1
            self.assertEquals(dash_header.text_cart_amount.text, str(count))

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_already_selected(self, wd):
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            active.select_product(Products.LABEL, "1")

        login.logout()
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            self.assertTrue(active.is_selected(Products.LABEL, "1"))

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_no_prod_deleting(self, wd):
        login = mobile.Login(wd)
        login.go()
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        inactiveAdsNumber = len(dashboard.list_inactive_ads)
        # DeletingAd
        dashboard.buttons_delete[1].click()
        delete_page = mobile.DeletePage(wd)
        delete_page.reasons[0].click()
        delete_page.sell_time_options[2].click()
        delete_page.submit.click()

        def ad_deleted():
            return inactiveAdsNumber < len(dashboard.list_inactive_ads)

        def transaction_ends(wd):
            dashboard.go_dashboard()
            return ad_deleted()

        dashboard.wait.until(transaction_ends)
        self.assertTrue(ad_deleted())

        for inactive in dashboard.list_inactive_ads:
            self.assertRaises(exceptions.NoSuchElementException, lambda: inactive.find_element_by_css_selector(dashboard.selector_css_highlight))

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_yes_prod_editing(self, wd):
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        active_ads_number = len(dashboard.list_active_ads)
        dashboard.buttons_edit[0].click()
        edit_page = mobile.NewAdEdit(wd)
        edit_page.form.submit()
        edit_page.wait.until_not_visible('want_bump')
        dashboard.go_dashboard()
        new_active_ads_number = len(dashboard.list_active_ads)
        self.assertTrue(active_ads_number == new_active_ads_number)
        self.assertTrue(dashboard.list_active_ads[0].find_element_by_css_selector(dashboard.selector_css_highlight))

    @tags('mobile', 'accounts', 'dashboard', 'products', 'gallery')
    def test_gallery_check_images(self, wd):
        utils.restore_snaps_in_a_cooler_way(self.snaps)
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad_no_image = dashboard.list_active_ads[0]
        ad_no_image_id = ad_no_image.get_attribute(dashboard.selector_attribute_id)
        ad_no_image.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        self.assertTrue(ad_no_image.find_element_by_id(dashboard.selector_id_gallery + ad_no_image_id).get_attribute("disabled"))
        ad_wt_image = dashboard.list_active_ads[4]
        ad_wt_image_id = ad_wt_image.get_attribute(dashboard.selector_attribute_id)
        ad_wt_image.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        self.assertFalse(ad_wt_image.find_element_by_id(dashboard.selector_id_gallery + ad_wt_image_id).get_attribute("disabled"))

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_buy_weekly(self, wd):
        """ Buy a weekly bump in mobile """
        # Buy Weekly
        list_ids = ['8000073']
        prod_ids = ['2']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.sticky_bar.pay()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = mobile.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')
        # Run rebuild-index apply product
        utils.rebuild_index()
        # Verify weekly disabled
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            if str(list_id) == list_ids[0]:
                self.assertTrue(Products.WEEKLY_BUMP not in active.get_product_options(Products.WEEKLY_BUMP, wd))
        # Wait 25 seconds for product to be enabled again
        time.sleep(11)
        # Firt rebuild index ejecute the first weekly
        utils.rebuild_index()
        time.sleep(10)
        # Run rebuild-index removing product
        utils.rebuild_index()
        time.sleep(5)
        # Go to dashboard again
        dashboard.go_dashboard()
        # Verify the weekly is checkeable
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            if str(list_id) == list_ids[0] :
                self.assertTrue("weekly_bump" in active.get_product_options(Products.WEEKLY_BUMP, wd) )

    @tags('mobile', 'accounts', 'dashboard', 'products', 'gallery')
    def test_buy_gallery(self, wd):
        """ Buy a gallery in mobile """
        # Buy Weekly
        list_ids = ['3456789']
        prod_ids = ['3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.sticky_bar.pay()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = mobile.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')
        dashboard = mobile.DashboardMyAds(wd)
        # Verify gallery disabled
        self.__go_dashboard_and_assert_gallery(dashboard, list_ids, disabled=True, with_rebuild_index=True)
        # Verify gallery enabled, before rebuild-index
        self.__go_dashboard_and_assert_gallery(dashboard, list_ids, disabled=False, with_rebuild_index=True)

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_buy_reject_and_error(self, wd):
        list_ids = ['8000073', '3456789']
        prod_ids = ['2', '3']
        summary = self._add_list_of_products(wd, list_ids, prod_ids)
        summary.sticky_bar.pay()
        paysim = mobile.Paysim(wd)
        paysim.pay_refuse()
        self.assertEqual(summary.warning_message.text, 'Oops... La transacci�n no pudo ser realizada')
        summary.sticky_bar.pay()
        paysim.pay_cancel()
        self.assertEqual(summary.warning_message.text, 'Oops... La transacci�n no pudo ser realizada')
        summary.sticky_bar.pay()
        self.assertEquals(paysim.input_amount.get_attribute("value"), '1040000')
        paysim.input_amount.clear()
        paysim.input_amount.send_keys('4000')
        paysim.pay_accept()
        self.assertEqual(summary.warning_message.text, 'Oops... La transacci�n no pudo ser realizada')
        summary.sticky_bar.wait.until_not_visible('bar')

    @tags('mobile', 'accounts', 'dashboard', 'products')
    def test_prod_already_selected_lazy_loading(self, wd):
        login = mobile.Login(wd)
        login.login("android@yapo.cl", self.account_pass)
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        number_ads = len(dashboard.list_active_ads)
#        for active in dashboard.list_active_ads:
#            active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
#            self.assertTrue(active.find_element_by_css_selector(dashboard.selector_css_product_list).is_displayed())
#            active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wai for next 10 ads loaded
        dashboard.wait.until(lambda wd: len(mobile.DashboardMyAds(wd).list_active_ads) > number_ads)

        # Interact with the ad in position 11
        active = dashboard.list_active_ads[number_ads]
        active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        self.assertTrue(active.find_element_by_css_selector(dashboard.selector_css_product_list).is_displayed())
        list_id = active.get_attribute(dashboard.selector_attribute_id)

        # Select bump for this ad
        for lid, ad in dashboard.active_ads.items():
            if str(lid) == list_id:
                self.assertFalse(ad.is_selected(Products.BUMP))
                ad.select_product(Products.BUMP)
                self.assertTrue(ad.is_selected(Products.BUMP))

        # Go to summary
        summary = mobile.SummaryPayment(wd)
        summary.go_summary()
        self.assertTrue(summary.product_row0)

        # Go to dashboard again
        dashboard.go_dashboard()
        number_ads = len(dashboard.list_active_ads)

        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        # Wait for next 10 ads loaded
        dashboard.wait.until(lambda wd: len(mobile.DashboardMyAds(wd).list_active_ads) > number_ads)

        # Interact with the ad in position 11
        dashboard = mobile.DashboardMyAds(wd)
        active = dashboard.list_active_ads[number_ads]
        active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
        list_id = active.get_attribute(dashboard.selector_attribute_id)
        for lid, ad in dashboard.active_ads.items():
            if str(lid) == list_id:
                self.assertTrue(ad.is_selected(Products.BUMP))

    def _add_list_of_products(self, wd, list_ids, prod_ids):
        """Add new products to the summary page (lists must have the same length)"""
        login = mobile.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = mobile.SummaryPayment(wd)
        for i in range(0, len(list_ids)):
            summary.add_product(list_ids[i], prod_ids[i])
        return summary

    @retries_function_exception(num_retries=10)
    def __go_dashboard_and_assert_gallery(self, dashboard, list_ids, disabled, with_rebuild_index=False):
        # Run rebuild-index apply product
        utils.rebuild_index()
        # Go to dashboard again
        dashboard.go_dashboard()
        # Verify gallery
        for active in dashboard.list_active_ads:
            list_id = active.get_attribute(dashboard.selector_attribute_id)
            if list_id in list_ids:
                active.find_element_by_css_selector(dashboard.selector_css_highlight).click()
                gallery_disabled = active.find_element_by_id(dashboard.selector_id_gallery + list_id).get_attribute("disabled")
                self.assertTrue(gallery_disabled) if disabled else self.assertFalse(gallery_disabled)
