# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import accounts
from yapo.pages.desktop import Login
import yapo

class CheckPasswordErrorMessages(yapo.SeleniumTest):
    """ Password validations in desktop accounts forms. """

    snaps = ['accounts']

    @tags('accounts', 'desktop')
    def test_check_passwd_validation_on_create_account(self, wd):
        """ Tests passwords validations on create account """

        create = accounts.Create(wd)
        create.go()
        create.ordered_fill_form([
            ('password', '1111'),
            ('email', 'some@email.com')
        ])
        create.wait.until_present("err_password")
        create.wait.until(lambda x: create.err_password.text != '')
        self.assertTrue(create.err_password.is_displayed())
        self.assertEqual(create.err_password.text, 'Tu contrase�a debe tener un m�nimo de 8 caracteres.')

    @tags('accounts', 'desktop')
    def test_check_passwd_validation_on_edit_account(self, wd):
        """ edit account password validations """

        login = Login(wd)
        login.login('prepaid5@blocket.se', '123123123')
        edit = accounts.Edit(wd)
        edit.go()
        edit.change_password.click()
        edit.fill_form(password='', password_verify='')
        edit.submit_btn.click()
        self.assertFalse(edit.password.is_displayed())
        self.assertFalse(edit.password_verify.is_displayed())
        edit.change_password.click()
        edit.fill_form(password='1234', password_verify='1234')
        edit.submit_btn.click()
        self.assertEqual(edit.err_password.text, 'Tu contrase�a debe tener un m�nimo de 8 caracteres.')
        edit.fill_form(password='12345', password_verify='123456')
        edit.submit_btn.click()
        self.assertEqual(edit.err_password_verify.text, 'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.')

class ValidatesCreateAccountLanding(yapo.SeleniumTest):
    """ Validations in desktop accounts creation page. """

    @tags('accounts', 'desktop')
    def test_elements_on_create_account(self, wd):
        """ Tests elements on create account """

        create = accounts.Create(wd)
        create.go()
        self.assertTrue(create.is_element_present('submit_btn'))
        # not using page object for this test, dont hate me
        self.assertEqual("Crear cuenta | yapo.cl", wd.title)
        self.assertEqual("Crear mi cuenta gratuita", wd.find_element_by_css_selector(".create-account-title .title-account").text)
        self.assertEqual("Tu informaci�n centralizada", wd.find_element_by_css_selector("h2.featuresAccounts-title").text)
        self.assertEqual("Administra tus avisos en un solo lugar.", wd.find_element_by_css_selector("h3.featuresAccounts-subtitle").text)
        self.assertEqual("Lista de avisos activos", wd.find_element_by_css_selector(".listFeatures li:nth-child(1) p").text)
        self.assertEqual("Herramientas de administraci�n", wd.find_element_by_css_selector(".listFeatures li:nth-child(2) p").text)
        self.assertEqual("Destaca tus avisos", wd.find_element_by_css_selector(".listFeatures li:nth-child(3) p").text)
        self.assertEqual("T�rminos y Condiciones", wd.find_element_by_css_selector(".link_use_terms").text)
        self.assertEqual("Crear mi cuenta", create.submit_btn.text)
