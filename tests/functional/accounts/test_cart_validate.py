# -*- coding: latin-1 -*-
import yapo
import time
from yapo.pages import desktop
from yapo.pages import mobile
from yapo import utils
from yapo import trans
from yapo.utils import Products, PacksPeriods
from yapo.decorators import tags
from yapo.pages.simulator import Paysim


class CartValidateDesktop(yapo.SeleniumTest):
    """ Test to Validate all products before payment process """

    #user_agent = utils.UserAgents.IOS

    account_pass = "123123123"
    account_email = "prepaid5@blocket.se"

    snaps = ['accounts']

    platform = desktop

    expected_cart = [
            ['Race car', 'Subir Diario', '$ 9.300', ''],
            ['Pesas muy grandes', 'Etiqueta', '$ 2.100', '']
        ]

    def _add_to_cart(self, wd, elements):
        summary = self.platform.SummaryPayment(wd)
        for prod, list_id in elements.items():
            summary.add_element_to_cart(prod, list_id)
        return summary

    def _login(self, wd):
        login = self.platform.Login(wd)
        login.login(self.account_email, self.account_pass)

    def test_check_products_clean(self, wd):
        """ Check ad prods are deleted from cart on validation """

        self._login(wd)
        ads_prod = {
                '8': 3456789,
                '9': 6000666
            }
        summary = self._add_to_cart(wd, ads_prod)
        summary.choose_bill()
        elements_in = summary.get_table_data()
        self.assertListEqual(self.expected_cart, elements_in)

        for loop in range(2):
            if loop == 0:
                trans.add_daily_bump(6)
            elif loop == 1:
                trans.add_label(10)
        summary.link_to_pay_products.click()
        summary.wait.until_visible('modal_cart_button')
        self.assertEquals(summary.modal_cart_body_title.text, '�Tu carro de compras ha sido actualizado!')
        summary.modal_cart_button.click()
        summary.wait.until_not_visible('modal_cart_button')
        if loop > 1:
            summary.wait.until(lambda x: len(self.platform.SummaryPayment(wd).get_table_data()) < len(self.expected_cart))
            elements_in = summary.get_table_data()
            self.expected_cart.pop(0)
            self.assertListEqual(self.expected_cart, elements_in)
        else:
            summary.wait.until_visible('empty_cart')

class CartValidateMobile(CartValidateDesktop):
    """ Test to Validate all products before payment process """

    user_agent = utils.UserAgents.IOS
    platform = mobile

    expected_cart = [
            ['Race car', 'Subir Diario', '$9.300', '', ''],
            ['Pesas muy grande...', 'Etiqueta', '$2.100', '', '']
        ]
