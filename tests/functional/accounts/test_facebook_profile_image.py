# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import HeaderElements
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.mobile import AdView, Dashboard
from yapo.pages.accounts import ViewAccount
from yapo import utils
from yapo.steps import ad_insert_preview
from yapo.pages.ad_insert import AdInsert, AdInsertPreview
import yapo
import unittest


class ProfileImageDesktopTest(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_facebooklogin']
    bconfs = {'*.*.facebook.enabled': 1, '*.*.new_seller_info.percent_show': 100}
    AdViewClass = AdViewDesktop
    adviewSizeImage = (90, 90)

    @tags('desktop', 'accounts', 'profile_image')
    def test_profile_image_adview(self, wd):
        """ check profile image in adview """
        adview = self.AdViewClass(wd)
        adview.go(list_id=8000078)
        url = 'https://graph.facebook.com/1556373078006590/picture?width={0}&height={1}'.format(
            self.adviewSizeImage[0], self.adviewSizeImage[1]
        )

        self.assertEqual(url, adview.profile_image.get_attribute('src'))


class ProfileImageMobileTest(ProfileImageDesktopTest):
    AdViewClass = AdView
    adviewSizeImage = (90, 90)
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0', '*.*.new_seller_info.percent_show': 100}

    @tags('desktop', 'accounts', 'profile_image')
    def test_profile_image_view_account(self, wd):
        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123', from_desktop=False)
        view_account = ViewAccount(wd)
        view_account.go()
        profile_image_url = 'https://graph.facebook.com/1556373078006590/picture?width=160&height=160'
        self.assertEqual(profile_image_url, view_account.profile_image.get_attribute('src'))


class ProfileImageOnlyDesktopTest(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_facebooklogin']
    bconfs = {'*.*.facebook.enabled': 1}

    @tags('desktop', 'accounts', 'profile_image')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_profile_image_preview(self, wd):
        """ check profile image in preview adinsert loggued"""
        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123', from_desktop=True)
        ad_insert_preview(wd, logged=True)
        adinsert_preview = AdInsertPreview(wd)
        adinsert_preview.wait.until_visible('profile_image')
        self.assertEqual('https://graph.facebook.com/1556373078006590/picture?width=90&height=90', adinsert_preview.profile_image.get_attribute('src'))

    @tags('desktop', 'accounts', 'profile_image')
    def test_profile_image_login_bar(self, wd):
        """ check profile image in login bar """
        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123', from_desktop=True)
        header_elements = HeaderElements(wd)
        self.assertEqual('https://graph.facebook.com/1556373078006590/picture?width=30&height=30', header_elements.profile_image.get_attribute('src'))

    @tags('desktop', 'accounts', 'profile_image')
    def test_profile_image_adinsert_loggued_in(self, wd):
        """ check profile image in adinsert loggued """
        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123', from_desktop=True)
        ad_insert = AdInsert(wd)
        ad_insert.go()
        self.assertEqual('https://graph.facebook.com/1556373078006590/picture?width=90&height=90', ad_insert.profile_image.get_attribute('src'))

    @tags('desktop', 'accounts', 'profile_image')
    def test_profile_image_dashboard(self, wd):
        """ check profile image in dashboard """
        login_and_go_to_dashboard(wd, 'many@ads.cl', '123123123', from_desktop=True)
        dashboard = Dashboard(wd)
        self.assertEqual('https://graph.facebook.com/1556373078006590/picture?width=56&height=56', dashboard.profile_image.get_attribute('src'))
