# -*- coding: latin-1 -*-
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds
import time
import yapo


class CartUpselling(yapo.SeleniumTest):

    snaps = ['accounts', 'addaccountandroid']
    products = {
        1: 'Subir Ahora',
        2: 'Subir Semanal',
        3: 'Vitrina 7 d�as',
        8: 'Subir Diario',
        9: 'Etiqueta',
        101: '- Subir Semanal -',
        102: '- Vitrina 7 d�as -',
        103: '- Subir Diario -',
        104: '- Etiqueta -',
    }

    bconfs = {
        '*.*.payment_settings.product.1.1.1220.value': 'price:2450',
        '*.*.payment_settings.product.1.8.1220.value': 'price:11550',
        '*.*.payment_settings.product.1.2.*.value': 'price:2750',
        '*.*.payment_settings.product.1.3.*.value': 'price:3650',
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.9.*.value': 'price:2000',
        '*.*.payment_settings.product.1.8.*.value': 'price:4550'
    }

    def _go_to_summary(self, wd, user):
        login = Login(wd)
        login.login(user, '123123123')
        return SummaryPayment(wd)

    def _check_val(self, wd, summary, prod, ad_info, price):
        expected_values = [[
            ad_info['subject'],
            self.products[prod],
            '$ {}'.format(price),
            ''
        ]]
        list_id = ad_info['list_id']
        if prod > 100:
            list_id = '{}.{}'.format(ad_info['category'], ad_info['ad_id'])
        summary.add_element_to_cart(prod=prod, list_id=list_id)
        table_values = summary.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

    @tags('cart', 'desktop', 'upselling')
    def test_upselling_daily_cart_replacement(self, wd):
        """ Check bump, daily, and upselling daily replacements in cart"""
        # This is the flow
        #    bump                >>> upselling daily
        #    upselling daily     >>> daily
        #    daily               >>> upselling daily
        #    upselling daily     >>> bump

        ad_info = {
            'subject': 'Departamento Tarapac�',
            'category': 1020,
            'email': 'prepaid5@blocket.se',
            'ad_id': 1,
            'list_id': 6000667,
        }

        summary = self._go_to_summary(wd, ad_info['email'])

        # Start adding a bump to cart
        self._check_val(wd, summary, 1, ad_info, '2.450')
        # Replace bump by upselling daily
        self._check_val(wd, summary, 103, ad_info, '11.550')
        # Replace upselling daily by daily
        self._check_val(wd, summary, 8, ad_info, '11.550')
        # Replace daily by upselling daily
        self._check_val(wd, summary, 103, ad_info, '11.550')
        # Replace upselling daily by bump
        self._check_val(wd, summary, 1, ad_info, '2.450')

    @tags('cart', 'desktop', 'upselling')
    def test_upselling_weekly_cart_replacement(self, wd):
        """ Check bump, weekly, and upselling weekly replacements in cart"""

        # This is the flow
        #    bump                >>> upselling weekly
        #    upselling weekly    >>> weekly
        #    weekly bump         >>> upselling weekly
        #    upselling weekly    >>> bump

        ad_info = {
            'subject': 'Libro para Psicologo Manual Diagnostico',
            'email': 'prepaid3@blocket.se',
            'ad_id': 82,
            'list_id': 8000062,
            'category': 6120,
        }

        summary = self._go_to_summary(wd, ad_info['email'])
        # Start adding a bump to cart
        self._check_val(wd, summary, 1, ad_info, '950')
        # Replace bump by upselling weekly
        self._check_val(wd, summary, 101, ad_info, '2.750')
        # Replace upselling weekly by weekly
        self._check_val(wd, summary, 2, ad_info, '2.750')
        # Replace weekly by upselling weekly
        self._check_val(wd, summary, 101, ad_info, '2.750')
        # Replace upselling weekly by bump
        self._check_val(wd, summary, 1, ad_info, '950')

    @tags('cart', 'desktop', 'upselling')
    def test_upselling_prods_cart_replacement(self, wd):
        """ Check upselling product with normals replacements in cart"""

        # This is the flow
        #    upselling daily     >>> weekly
        #    weekly              >>> upselling daily
        #    daily               >>> upselling weekly
        #    upselling weekly    >>> daily
        #
        ad_info = {
            'subject': 'Aviso de prueba 1',
            'email': 'many@ads.cl',
            'category': 8020,
            'ad_id': 96,
            'list_id': 8000074,
        }
        summary = self._go_to_summary(wd, ad_info['email'])

        # Start adding upselling daily to cart
        self._check_val(wd, summary, 103, ad_info, '4.550')
        # Replace upselling daily with weekly
        self._check_val(wd, summary, 2, ad_info, '2.750')
        # Replace weekly with upselling daily
        self._check_val(wd, summary, 103, ad_info, '4.550')
        # Replace upselling daily with daily
        self._check_val(wd, summary, 8, ad_info, '4.550')
        # Replace daily with upselling weekly
        self._check_val(wd, summary, 101, ad_info, '2.750')
        # Replace upselling weekly with daily
        self._check_val(wd, summary, 8, ad_info, '4.550')

    @tags('cart', 'desktop', 'upselling')
    def test_upselling_gallery_cart_replacement(self, wd):
        """ Check upselling gallery replaced by gallery"""

        # This is the flow
        #    upselling gallery  >>> gallery

        ad_info = {
            'subject': 'Test 04',
            'email': 'user.01@schibsted.cl',
            'category': 8020,
            'ad_id': 119,
            'list_id': 8000079,
        }
        summary = self._go_to_summary(wd, ad_info['email'])

        # Replace gallery with upselling gallery
        self._check_val(wd, summary, 102, ad_info, '3.650')
        # Start adding gallery to cart
        self._check_val(wd, summary, 3, ad_info, '3.650')

    @tags('cart', 'desktop', 'upselling')
    def test_upselling_label_cart_replacement(self, wd):
        """ Check upselling label replaced by label"""

        # This is the flow
        #    upselling gallery  >>> gallery

        ad_info = {
            'subject': 'Aviso de usuario Android',
            'email': 'android@yapo.cl',
            'category': 3060,
            'ad_id': 90,
            'list_id': 8000066,
        }
        summary = self._go_to_summary(wd, ad_info['email'])

        # Replace gallery with upselling gallery
        self._check_val(wd, summary, 104, ad_info, '2.000')
        # Start adding gallery to cart
        self._check_val(wd, summary, 9, ad_info, '2.000')
