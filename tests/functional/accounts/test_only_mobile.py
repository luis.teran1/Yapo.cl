# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import accounts
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import utils
import time
import yapo
import yapo.conf


class AccountsOnlyMobileTest(yapo.SeleniumTest):
    user_agent = utils.UserAgents.NEXUS_5
    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def test_view_profile(self, driver):
        """ Tested view profile in msite"""
        login_and_go_to_dashboard(
            driver, 'prepaid5@blocket.se', '123123123', from_desktop=False)
        view_account = accounts.ViewAccount(driver)
        view_account.go()
        self.assertEqual(
            [
                ('RUT', '100000-4'),
                ('Tel�fono', '87654321'),
                ('Fecha de Nacimiento', 'No ingresado'),
                ('Direcci�n', 'Alonso de C�rdova 5670'),
                ('Regi�n', 'Regi�n Metropolitana'),
                ('Comuna', 'Las Condes'),
                ('Medio de Pago\nOneClick WebPay', 'No Inscrito')
            ], view_account.get_account_params())

    def test_edit_profile(self, driver):
        """ Tested edit birthdate in msite"""
        login_and_go_to_dashboard(driver, 'many@ads.cl', '123123123', from_desktop=False)
        edit_account = accounts.EditMobile(driver)
        edit_account.go()
        edit_account.fill_form(birth_day=1, birth_month=2)
        edit_account.submit_btn.click()
        edit_account.wait.until_loaded()
        self.assertEqual(edit_account.err_birthdate.text, 'La fecha es inv�lida')
        edit_account.fill_form(birth_day=1)
        edit_account.submit_btn.click()
        edit_account.wait.until_loaded()
        self.assertEqual(edit_account.err_birthdate.text, 'La fecha es inv�lida')
        edit_account.fill_form(birth_day=1, birth_month=2, birth_year=1994)
        edit_account.submit_btn.click()
        edit_account.wait.until_loaded()

        view_account = accounts.ViewAccount(driver)
        self.assertEqual(
            [
                ('RUT', 'No ingresado'),
                ('Tel�fono', '87654321'),
                ('Fecha de Nacimiento', '01 - 02 - 1994'),
                ('Direcci�n', 'No ingresado'),
                ('Regi�n', 'Regi�n Metropolitana'),
                ('Comuna', 'Las Condes'),
                ('Medio de Pago\nOneClick WebPay', 'No Inscrito')
            ], view_account.get_account_params())

    def test_edit_account_data_with_errors(self, driver):
        """ Tested edit account data with errors """
        login_and_go_to_dashboard(driver, 'many@ads.cl', '123123123', from_desktop=False)
        edit_account = accounts.EditMobile(driver)

        # Update account data and error in birthdate
        edit_account.go()
        edit_account.fill_form(
            birth_day=1,
            gender_male=True,
            phone='62184762',
            rut='11111111-1')
        edit_account.submit_btn.click()
        time.sleep(2)
        edit_account.wait.until_loaded()
        edit_account.wait.until_visible('err_birthdate')
        self.assertEqual(edit_account.err_birthdate.text, 'La fecha es inv�lida')

        # Verify data account update
        view_account = accounts.ViewAccount(driver)
        view_account.go()
        self.assertEqual(
            [
                ('RUT', 'No ingresado'),
                ('Tel�fono', '87654321'),
                ('Fecha de Nacimiento', 'No ingresado'),
                ('Direcci�n', 'No ingresado'),
                ('Regi�n', 'Regi�n Metropolitana'),
                ('Comuna', 'Las Condes'),
                ('Medio de Pago\nOneClick WebPay', 'No Inscrito')
            ], view_account.get_account_params())

        # Verify gender male not selected
        edit_account.go()
        self.assertFalse(edit_account.gender_male.is_selected())
