# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import SummaryPayment, DashboardMyAds
from yapo import utils
from yapo.steps.accounts import login_and_go_to_dashboard
import yapo
from yapo.utils import Products

def got_to_dashboard_and_select_products(wd, classDashboardMyAds, LIST_IDS):
    login_and_go_to_dashboard(wd,
                              email='prepaid5@blocket.se',
                              password='123123123',
                              from_desktop=False)
    dashboard = classDashboardMyAds(wd)
    dashboard.select_products_by_ids(LIST_IDS)
    return dashboard

class StickyBarDashboardMobileTest(yapo.SeleniumTest):

    snaps = ['accounts']
    LIST_IDS = {6000666: [Products.GALLERY],
                3456789: [Products.GALLERY],
                6000667: [Products.GALLERY]}
    user_agent = utils.UserAgents.IOS
    classDashboardMyAds = DashboardMyAds
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts', 'dashboard')
    def test_display_sticky_in_footer_dashboard(self, wd):
        """The sticky bar should display in the footer of dashboard page."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.wait.until_visible('new_bar')
        self.assertTrue(dashboard.sticky_bar.new_bar.is_displayed())
        self.assertEquals(dashboard.sticky_bar.new_bar.value_of_css_property('position'), 'fixed')

    @tags('mobile', 'accounts', 'dashboard')
    def test_labels_dashboard(self, wd):
        """Tested label of buttons "comprar" and "total" in dashboard page."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.wait.until_visible('new_btn_pay')
        self.assertEquals(dashboard.sticky_bar.new_btn_pay.text, 'Comprar')
        self.assertEquals(dashboard.sticky_bar.new_amount.text, '$ 21.150')

    @tags('mobile', 'accounts', 'dashboard')
    def test_price_of_product_in_dashboard(self, wd):
        """sticky bar disappears when the price is zero."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.wait.until_visible('new_amount')
        self.assertEquals(dashboard.sticky_bar.new_amount.text, '$ 21.150')

    @tags('mobile', 'accounts', 'dashboard')
    def test_price_0_in_dashboard(self, wd):
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.wait.until_visible('new_amount')
        dashboard.delete_products_by_ids(self.LIST_IDS)
        self.assertEquals(dashboard.sticky_bar.new_bar.value_of_css_property('display'), 'flex')
        self.assertEquals(dashboard.sticky_bar.new_amount.text, '')

class StickyBarSummaryMobileTest(yapo.SeleniumTest):

    snaps = ['accounts']
    LIST_IDS = {6000666: [Products.GALLERY],
                3456789: [Products.GALLERY],
                6000667: [Products.GALLERY]}
    user_agent = utils.UserAgents.IOS
    classDashboardMyAds = DashboardMyAds
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'accounts', 'summary_page')
    def test_display_sticky_in_footer_summary_page(self, wd):
        """The sticky bar should display in the footer of summary page."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.new_btn_pay.click()

        summary = SummaryPayment(wd)
        summary.sticky_bar.wait.until_visible('bar')

        self.assertTrue(summary.sticky_bar.bar.is_displayed())
        self.assertEquals(summary.sticky_bar.bar.value_of_css_property('position'), 'fixed')

    @tags('mobile', 'accounts', 'summary_page')
    def test_labels_summary_page(self, wd):
        """Tested label of buttons "comprar" and "total" in dashboard page."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.new_btn_pay.click()

        summary = SummaryPayment(wd)
        summary.sticky_bar.wait.until_visible('btn_pay')

        self.assertEquals(summary.sticky_bar.btn_pay.text, 'Pagar')
        self.assertEquals(summary.sticky_bar.total_text.text, 'Total:\n$ 21.150')

    @tags('mobile', 'accounts', 'summary_page')
    def test_price_of_product_in_summary_page(self, wd):
        """The sticky bar should display with the price of product in dashboard page."""
        dashboard = got_to_dashboard_and_select_products(wd, getattr(self, 'classDashboardMyAds'), self.LIST_IDS)
        dashboard.sticky_bar.new_btn_pay.click()

        summary = SummaryPayment(wd)
        summary.sticky_bar.wait.until_visible('amount')

        self.assertEquals(summary.sticky_bar.amount.text, '$ 21.150')
