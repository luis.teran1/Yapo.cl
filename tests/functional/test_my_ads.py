# -*- coding: latin-1 -*-
import sunbro
from yapo import SeleniumTest
from yapo.pages2 import desktop, mobile
from yapo.pages2.desktop import myads
from yapo.pages2.common import myadsmail
from yapo.decorators import tags

class TestMyAdsDesktop(SeleniumTest):
    snaps = ['accounts']
    platform = desktop

    @tags('my_ads')
    def test_my_ads_error(self, wd):
        """ Testing my ads on error cases """
        myads = self.platform.MyAds(wd)
        myads.go()
        ok = myads.request('a@b.c')
        self.assertFalse(ok)
        ok = myads.request('')
        self.assertFalse(ok)

    @tags('my_ads')
    def test_my_ads_ok_and_mail(self, wd):
        """ Testing my ads success case and mail verification """
        my_ads = self.platform.MyAds(wd)
        my_ads.go()
        ok = my_ads.request('prepaid3@blocket.se')
        self.assertTrue(ok)
        my_ads_mail = myadsmail.MyAdsMail(wd)
        my_ads_mail.go()
        self.assertTrue(my_ads_mail._check_email([
                '30 Aug\n12:23 Libro 3 $ 14.000',
                '23 Aug\n12:22 El joven manos de tijera poster pelicula 54 x 41 $ 1.200',
                '23 Aug\n12:22 Cinegrama star wars - natalie portman $ 1.200',
                '23 Aug\n12:22 Maquina Singer antigua $ 75.000',
                '23 Aug\n12:22 Libro para Psicologo Manual Diagnostico $ 70.000']))

class TestMyAdsMobile(TestMyAdsDesktop):
    platform = mobile
