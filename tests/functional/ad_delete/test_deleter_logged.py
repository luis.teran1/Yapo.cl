# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import desktop, mobile, desktop_list, mobile_list, simulator, generic
from yapo import utils
from selenium.common import exceptions
import yapo

class AdDRLoggedMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS

    LoginPage = mobile.Login
    AVPage = mobile.AdView
    DeletePage = mobile.DeletePage
    DashboardPage = mobile.DashboardMyAds
    SummaryPage = mobile.SummaryPayment
    PaysimPage = mobile.Paysim
    MyAdsPage = mobile.MyAds
    EmailPage = generic.SentEmail
    is_desktop = False
    success_message = ""
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_delete', 'desktop')
    def test_del_ad_other_account(self, wd):
        """ when logged delete an ad from other user """
        self._login(wd)
        if self.is_desktop:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa al correo que te hemos enviado. Luego de 7 d�as desactivado se eliminar�."
        else:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
        self._ad_view_delete(wd, '8000035') # ad: "Play Station 3 con 19 juegos 1 control, 1 guitarra"
        self._delete_reason(wd, '8000035', 1, 2, '123123123')

    @tags('ad_delete', 'desktop')
    def test_del_from_dashboard(self, wd):
        """ delete from dashboard - check on dashboard """
        self._login(wd)
        if self.is_desktop:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa a tu cuenta. Luego de 7 d�as se eliminar�."
        else:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
        self._go_dashboard_and_delete(wd, 'Test 09')
        self._go_dashboard_and_check(wd, 'Test 09', 'deactivated')

    @tags('ad_delete', 'desktop')
    def test_del_from_adview_plus_bump(self, wd):
        """ deletes an ad from adview then bumps it """
        self._login(wd)
        self._ad_view_delete(wd, '8000083')
        self._delete_reason(wd, '8000083', 4, 2, '', True)
        self._go_dashboard_and_check(wd, 'Test 08', 'active')

    @tags('ad_delete', 'desktop')
    def test_del_from_myads(self, wd):
        """ deletes and from myads, then checks it from dashboard  """
        if self.is_desktop:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa a tu cuenta. Luego de 7 d�as se eliminar�."
        else:
            self.success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
        self._myads(wd)
        self._delete_from_mail(wd, 'Test 06')
        self._delete_reason(wd, '8000081', 1, 2, '123123123')
        self._go_dashboard_and_check(wd, 'Test 06', 'deactivated')

    def _login(self, wd):
        """  """
        l = self.LoginPage(wd)
        l.login("user.01@schibsted.cl", "123123123")

    def _ad_view_delete(self, wd, list_id):
        """  """
        av = self.AVPage(wd)
        av.go(list_id)
        if not self.is_desktop:
            av.admin_show.click()
        av.delete_button.click()

    def _delete_reason(self, wd, list_id, reas, tim, passs, bump=False):
        ad_delete = self.DeletePage(wd)
        if list_id != '':
            ad_delete.go(list_id)
        ad_delete.select_reason(reason=reas)
        if reas == 1:
            ad_delete.select_timer(timer=tim)
        if bump:
            if self.is_desktop:
                wd.find_element_by_id('bump'+str(reas)).click()
            else:
                wd.find_element_by_class_name('btn-payment').click()
            self._finish_payment(wd)
        else:
            if passs != '':
                ad_delete.fill_form(password=passs)
            ad_delete.submit.click()
            self.assertEqual(self.success_message, ad_delete.result_message.text)

    def _go_dashboard_and_delete(self, wd, subj):
        """  """
        db = self.DashboardPage(wd)
        db.go_dashboard()
        ad = db.search_ad_by_subject(subj)
        ad.delete_button.click()
        self._delete_reason(wd, '', 1, 2, '')

    def _go_dashboard_and_check(self, wd, subj, status):
        """  """
        db = self.DashboardPage(wd)
        db.go_dashboard()
        ad = db.search_ad_by_subject(subj)
        if status == 'active':
            self.assertRaises(exceptions.NoSuchElementException, lambda: ad.option_activate)
        if status == 'deactivated':
            ad.wait.until_present('option_activate')

    def _finish_payment(self, wd):
        """  """
        sumpay = self.SummaryPage(wd)
        sumpay.press_link_to_pay_products()
        paysim = self.PaysimPage(wd)
        paysim.pay_accept()


    def _myads(self, wd):
        """  """
        myads = self.MyAdsPage(wd)
        myads.go_and_request('user.01@schibsted.cl')

    def _delete_from_mail(self, wd, subj):
        """  """
        email = self.EmailPage(wd)
        email.go()
        tbody = wd.find_elements_by_tag_name('tbody')[1]
        rows = tbody.find_elements_by_tag_name('tr')
        r = 0
        while r < len(rows):
            r = r+1
            if rows[r].find_element_by_class_name('subject').text == subj:
                print(">>>>>>>>>>>>>>>> "+rows[r].find_element_by_class_name('subject').text)
                rows[r].find_element_by_class_name('delete').click()
                break

class AdDRLoggedDesktop(AdDRLoggedMobile):
    user_agent = utils.UserAgents.FIREFOX_MAC

    LoginPage = desktop.Login
    AVPage = desktop_list.AdViewDesktop
    DeletePage = desktop_list.AdDeletePage
    DashboardPage = desktop.DashboardMyAds
    SummaryPage = desktop.SummaryPayment
    PaysimPage = simulator.Paysim
    MyAdsPage = desktop.MyAds
    EmailPage = generic.SentEmail

    is_desktop = True

    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa al correo que te hemos enviado. Luego de 7 d�as desactivado se eliminar�."
