# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdDeletePage as DeleteDesktop
from yapo.pages.mobile import DeletePage as DeleteMobile
from yapo import conf
from yapo import utils
import yapo

class AdDeletionMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    DeletePage = DeleteMobile
    error_message = "Las contrase�as no coinciden. Por favor int�ntalo de nuevo."
    error_message_empty = "" # TODO: this should be updated when this message is fixed on mobile
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
    text_reasons = {
        1: [
            '�En cu�nto tiempo?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        2: [
            '�D�nde?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        4: [
            'Sube tu aviso',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        5: [
            'Sube tu aviso',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        6: [
            'Sube tu aviso',
            '�Cu�l raz�n?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ]
    }
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_delete', 'desktop')
    def test_by_reasons_1(self, wd):
        """Check the deletion flow by reason 1"""
        self._check_delete_by_reason(wd, 1, '8000073')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_2(self, wd):
        """Check the deletion flow by reason 2"""
        self._check_delete_by_reason(wd, 2, '8000074')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_4(self, wd):
        """Check the deletion flow by reason 4"""
        self._check_delete_by_reason(wd, 4, '8000076')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_5(self, wd):
        """Check the deletion flow by reason 5"""
        self._check_delete_by_reason(wd, 5, '8000077')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_6(self, wd):
        """Check the deletion flow by reason 6"""
        self._check_delete_by_reason(wd, 6, '8000078')

    def _check_delete_by_reason(self, wd, reason_number, list_id):
        ad_delete = self.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.select_reason(reason=reason_number)
        if reason_number == 1:
            ad_delete.select_timer(timer=2)

        for text in self.text_reasons[reason_number]:
            self.assertIn(text, wd.find_element_by_tag_name('body').text)

        # Try no passwd
        ad_delete.wait.until_present('submit')
        ad_delete.submit.click()
        ad_delete.wait.until_present('error_message')
        if self.error_message_empty:
            self.assertEqual(self.error_message_empty, ad_delete.error_message.text)

        # Try bad passwd
        ad_delete.fill_form(password='ola q ase')
        ad_delete.submit.click()
        ad_delete.wait.until_present('error_message')
        self.assertEqual(self.error_message, ad_delete.error_message.text)

        # Try good passwd
        ad_delete.fill_form(password='123123123')
        ad_delete.submit.click()
        ad_delete.wait.until_visible('result_message')
        self.assertEqual(self.success_message, ad_delete.result_message.text)

class AdDeletionDesktop(AdDeletionMobile):
    user_agent = utils.UserAgents.FIREFOX_MAC
    DeletePage = DeleteDesktop
    error_message = "Las contrase�as no coinciden. Por favor int�ntalo de nuevo."
    error_message_empty = "Debes introducir tu contrase�a"
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa a tu cuenta. Luego de 7 d�as se eliminar�."
    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:950'
    }
    text_reasons = {
        1: [
            '�En cu�nto tiempo?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        2: [
            '�D�nde?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        4: [
            'Sube tu aviso!',
            '�Tu aviso volver� al top de la lista y multiplicar�s tus contactos por s�lo $950!',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        5: [
            'Sube tu aviso!',
            '�Tu aviso volver� al top de la lista y multiplicar�s tus contactos por s�lo $950!',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ],
        6: [
            'Sube tu aviso!',
            '�Tu aviso volver� al top de la lista y multiplicar�s tus contactos por s�lo $950!',
            '�Cu�l raz�n?',
            'Introduce tu contrase�a',
            '�La olvidaste?'
        ]
    }


class DesktopDeleteLoginAttempts(yapo.SeleniumTest):
    snaps = ['accounts']
    DeletePage = DeleteDesktop
    error_messages = [
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
    ]

    @tags('desktop', 'ad_delete', 'accounts', 'login_attempts')
    def test_delete_ad_with_account_wrong_password(self, wd):
        """
        Verify that the account is locked when the user tries to delete
        more than 3 times with wrong password to an ad with account
        """
        email = 'user.01@schibsted.cl'
        list_id = '8000084'
        title_email = 'Hemos detectado que no puedes ingresar a tu cuenta Yapo.cl'
        utils.clean_mails()
        # Go to ad
        delete_page = self.DeletePage(wd)
        # tries to access more than 3 times with wrong password
        for i, msg in enumerate(self.error_messages):
            delete_page.go(list_id)
            delete_page.delete_ad('invalid password')
            delete_page.wait.until_visible('error_message')
            # Verify message
            self.assertEqual(delete_page.error_message.text, msg)
            # Verify increment key in redis
            self.assertEqual(int(utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'get', 'rsd1x_login_{0}'.format(email))), i+1)
            # Verify that the email is sent in third intent
            if (i + 1) == 3:
                self.assertIsNotNone(utils.search_mails('({0})'.format(title_email)))
            else:
                self.assertIsNone(utils.search_mails('({0})'.format(title_email), max_retries=1))


class MobileDeleteLoginAttempts(DesktopDeleteLoginAttempts):
    snaps = ['accounts']
    DeletePage = DeleteMobile
    error_messages = [
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Las contrase�as no coinciden. Por favor int�ntalo de nuevo.',
        'Superaste la cantidad de intentos permitidos. Te enviamos un correo electr�nico para que puedas cambiar tu contrase�a.'
    ]
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}
