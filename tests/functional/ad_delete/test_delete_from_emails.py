# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.desktop_list import AdDeletePage as DeleteDesktop
from yapo.pages.mobile import DeletePage as DeleteMobile
from yapo.pages.desktop import ForgotPassword
from yapo.pages.generic import SentEmail
import yapo

class PasswordLessDeleteMobile(yapo.SeleniumTest):

    snaps = ['android']
    user_agent = utils.UserAgents.IOS
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
    success_element = 'result_message'
    success_element_result = 'Modal is not visible'
    DeletePage = DeleteMobile
    platform = mobile
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('ad_delete', 'my_ads')
    def fixtest_delete_from_my_ads(self, wd):
        """ My ads Delete: Testing that is possible to delete an ad with the "my ads" e-mail, without password"""
        
        my_ads = self.platform.MyAds(wd)
        my_ads.go()
        my_ads.request('android@yapo.cl')

        sent_email = SentEmail(wd)
        sent_email.go()
        # yes, I am using a selector here. Please cry.
        row = sent_email.body.find_elements_by_css_selector('table tr.stripe_0')[0]
        delete_link = row.find_element_by_css_selector('td:nth-child(6) a').get_attribute('href')
        wd.get(delete_link)
        self._assert_delete(wd)

    @tags('ad_delete', 'forgot_password')
    def test_delete_from_forgot_passwd(self, wd):
        """ Forgot password delete ad from email """
        
        # Delete with the first reason "lo vendi en yapo.cl"
        delete_page = self.DeletePage(wd)
        delete_page.go('8000003')
        delete_page.select_reason()
        delete_page.select_timer(timer=2)
        delete_page.forgot_password_link.click()

        forgot_password = ForgotPassword(wd)
        forgot_password.email_input.clear()
        forgot_password.email_input.send_keys('android@yapo.cl')
        forgot_password.submit_button.click()
        forgot_password.wait.until_loaded()

        sent_email = SentEmail(wd)
        sent_email.go()
        # yes, I am using a selector here. Please cry.
        row = sent_email.body.find_elements_by_css_selector('table tr.stripe_0')[0]
        delete_link = row.find_element_by_css_selector('td:nth-child(5) a').get_attribute('href')
        wd.get(delete_link)
        self._assert_delete(wd)

    def _assert_delete(self, wd):
        """ Asserting that is possible to delete with no password """

        # Delete with the first reason "lo vendi en yapo.cl"
        delete_page = self.DeletePage(wd)
        delete_page.select_reason()
        delete_page.select_timer(timer=2)
        delete_page.submit.click()
        # Verify Ad has been deleted
        delete_page.wait.until_visible(self.success_element, self.success_element_result)
        self.assertIn(self.success_message, delete_page.result_message.text)

class PasswordLessDeleteDesktop(PasswordLessDeleteMobile):
    user_agent = utils.UserAgents.FIREFOX_MAC
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos"
    success_element = 'result_message'
    success_element_result = 'no lo encuentro!!'
    DeletePage = DeleteDesktop
    platform = desktop
