# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdDeletePage as DeleteDesktop
from yapo.pages.desktop import ForgotPassword
from yapo.pages.generic import SentEmail
from yapo import utils
import yapo


class SendPassAdDeletionDesktop(yapo.SeleniumTest):
    snaps = ['example', '1000ads']
    user_agent = utils.UserAgents.FIREFOX_MAC
    DeletePage = DeleteDesktop
    success_message = ("Tu aviso ser� desactivado en los pr�ximos minutos,"
                       " si quieres volver a activarlo ingresa al correo que"
                       " te hemos enviado."
                       " Luego de 7 d�as desactivado se eliminar�.")

    @tags('ad_delete', 'desktop')
    def test_send_passwd_on_delete(self, wd):
        """ Using forgot passwd mail from delete to delete the ad"""
        list_id = '8349385'

        ad_delete = self.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.select_reason()
        ad_delete.select_timer(timer=2)
        ad_delete.forgot_password_link.click()

        forgot_password = ForgotPassword(wd)
        forgot_password.email_input.clear()
        forgot_password.email_input.send_keys('user15@blocket.se')
        forgot_password.submit_button.click()
        forgot_password.wait.until_loaded()

        sent_email = SentEmail(wd)
        sent_email.go()

        row = sent_email.body.find_elements_by_css_selector(
            'table tr.stripe_0')[0]
        delete_link = row.find_element_by_css_selector(
            '.delete').get_attribute('href')
        wd.get(delete_link)
        ad_delete = self.DeletePage(wd)
        ad_delete.select_reason()
        ad_delete.select_timer(timer=2)
        ad_delete.submit.click()
        ad_delete.wait.until_visible('result_message')
        self.assertEqual(self.success_message, ad_delete.result_message.text)
