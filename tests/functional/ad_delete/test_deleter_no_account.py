# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdDeletePage as DeleteDesktop
from yapo.pages.mobile import DeletePage as DeleteMobile
from yapo import utils
import yapo, time

class AdDRNoAccountMobile(yapo.SeleniumTest):

    snaps = ['accounts']
    user_agent = utils.UserAgents.IOS
    DeletePage = DeleteMobile
    error_message = "Las contrase�as no coinciden. Por favor int�ntalo de nuevo."
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado."
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('ad_delete', 'desktop')
    def test_by_reasons_1(self, wd):
        """Check the deletion flow by reason 1"""
        self._check_delete_by_reason(wd, 1, '8000066', '123123123')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_2(self, wd):
        """Check the deletion flow by reason 2"""
        self._check_delete_by_reason(wd, 2, '8000052', '123123123')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_4(self, wd):
        """Check the deletion flow by reason 4"""
        self._check_delete_by_reason(wd, 4, '8000043', '123123123')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_5(self, wd):
        """Check the deletion flow by reason 5"""
        self._check_delete_by_reason(wd, 5, '8000055', '123123123')

    @tags('ad_delete', 'desktop')
    def test_by_reasons_6(self, wd):
        """Check the deletion flow by reason 6"""
        self._check_delete_by_reason(wd, 6, '8000054', '123123123')

    def _check_delete_by_reason(self, wd, reason_number, list_id, passs):
        ad_delete = self.DeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.select_reason(reason=reason_number)
        if reason_number == 1:
            ad_delete.select_timer(timer=2)
        if reason_number >= 4:
            ad_delete.wait.until_present('bum_btn_'+str(reason_number))

        ad_delete.wait.until_present('submit')
        ad_delete.submit.click()
        ad_delete.fill_form(password='ola q ase')
        ad_delete.submit.click()
        ad_delete.wait.until_present('error_message')
        self.assertEqual(self.error_message, ad_delete.error_message.text)
        ad_delete.fill_form(password=passs)
        ad_delete.submit.click()
        ad_delete.wait.until_visible('result_message')
        self.assertEqual(self.success_message, ad_delete.result_message.text)
        
class AdDRNoAccountDesktop(AdDRNoAccountMobile):
    user_agent = utils.UserAgents.FIREFOX_MAC
    DeletePage = DeleteDesktop
    error_message = "Las contrase�as no coinciden. Por favor int�ntalo de nuevo."
    success_message = "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa al correo que te hemos enviado. Luego de 7 d�as desactivado se eliminar�."
