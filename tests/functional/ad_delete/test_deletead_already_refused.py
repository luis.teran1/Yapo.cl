# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo
from yapo.utils import trans
from yapo.pages.desktop_list import AdDeletePage

class DeleteAdAlreadyRefused(yapo.SeleniumTest):

    snaps = ['accounts']

    @tags('desktop', 'ad_delete')
    def test_deletead_already_refused(self, wd):
        """Delete ad already refused"""
        password = '123123123'
        list_id = '8000004'
        ad_id = '24'

        self._refuse_ad_publish(ad_id)
        ad_delete = AdDeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.select_reason(reason=6)
        ad_delete.fill_form(password='123123123')
        ad_delete.wait.until_present('submit')
        ad_delete.submit.click()

        ad_delete.wait.until_present('error_message')
        self.assertTextEquals(ad_delete.error_message, "Aviso ya fue eliminado")

    def _refuse_ad_publish(self, ad_id):
        result_authenticate = trans("authenticate",
                       username = "dany",
                       passwd = "118ab5d614b5a8d4222fc7eee1609793e4014800",
                       remote_addr = "10.0.1.139")
        self.assertTrue(("token" in result_authenticate))
        self.assertEqual("TRANS_OK" , result_authenticate['status'])

        result_review = trans("review",
                       ad_id=ad_id,
                       action_id="1",
                       action="refuse",
                       reason="20",
                       filter_name="all",
                       token=result_authenticate['token'],
                       remote_addr="10.0.1.139")
        self.assertEqual("TRANS_OK" , result_review['status'])
