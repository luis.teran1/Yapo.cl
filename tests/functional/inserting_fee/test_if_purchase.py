# -*- coding: latin-1 -*-
import yapo
import unittest
from yapo import utils
from yapo.steps import trans as steps_trans
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo import conf
from yapo.decorators import tags
from yapo.pages import desktop, mobile
from yapo.pages.simulator import Paysim


class InsertingFee(yapo.SeleniumTest):
    email = "cuentaprosincupos@yapo.cl"
    passwd = "123123123"
    password = "$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6"

    def _login_and_go_to_dashboard(self, wd):
        login_and_go_to_dashboard(
            wd, self.email, self.passwd, from_desktop=self.is_desktop)

    def _login(self, wd):
        login = self.platform.Login(wd)
        login.login(self.email, self.passwd)

    def _go_dashboard(self, wd):
        dash_my_ads = self.platform.DashboardMyAds(wd)
        dash_my_ads.go()
        dash_my_ads.wait.until_loaded()

    def _add_if_to_cart(self, wd, ad_id):
        url = conf.SSL_URL + \
            '/pagos?prod={}&id={}'.format(self.if_prod_id, ad_id)
        wd.get(url)

    def _add_if_to_cart_by_modal(self, wd, price=None):
        pack = self.platform.Pack(wd)
        disabled_ads = pack.filter_disabled_ads(
            self.platform.DashboardMyAds(wd).active_ads)
        key, first_ad = disabled_ads.popitem()
        if price is not None:
            price_on_page = pack.get_inserting_fee_price(first_ad)
            self.assertEqual(price_on_page, price)
        pack.select_inserting_fee(first_ad)

    def _insert_ad(self, wd, category):
        data = {
            'email': self.email,
            'passwd': self.password,
            'region': '15',
            'communes': '331',
            'category': category,
            'subject': 'Aviso con inserting fee',
            'body': 'body del aviso',
            'price': '35000',
        }
        result = steps_trans.insert_ad(None, **data)
        return result.get('ad_id')

    def _reload_snaps(self):
        utils.restore_snaps_in_a_cooler_way(self.snaps)
        utils.flush_redises()

    def _pay_and_check(self, wd):
        # Confirm payment
        summary = self.platform.SummaryPayment(wd)
        summary.change_doc_type(self.platform.Payment.BILL)
        summary.pay_with_webpay()

        # Simulator
        paysim = self.platform.Paysim(wd)
        paysim.pay_accept()

        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEqual(payment_verify.text_congratulations.text,
                         'Tu compra se realiz� exitosamente')

    def _check_disabled_ads(self, wd, expected):
        disabled_ads = self.platform.Pack(wd).filter_disabled_ads(
            self.platform.DashboardMyAds(wd).active_ads)
        self.assertEquals(len(disabled_ads), expected)

    def _check_inserting_fee_tags(self, wd, expected):
        tags = self.platform.DashboardMyAds(wd).inserting_fee_tags
        self.assertEquals(len(tags), expected)

    def _check_delete_modal(self, wd):
        trash_btn = wd.find_element_by_xpath(
            '//div[@class="single-loop-das active"][1]/div/div/a[@class="tealium-click delete-unpaid"]'
        )
        trash_btn.click()

        delete_unpaid_modal = self.platform.DeleteUnpaidModal(wd)
        self.assertVisible(delete_unpaid_modal, 'yes_button')

    def _check_delete_page(self, wd):
        delete_unpaid_modal = self.platform.DeleteUnpaidModal(wd)
        delete_unpaid_modal.click_yes()

        title = wd.find_element_by_xpath(
            '//div[@id="content"]/h1[@class="middle"]')
        self.assertEquals(title.text, '�Por qu� quieres eliminar tu aviso?')

    def _change_toggle(self, wd, subject, status):
        dash_my_ads = self.platform.DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(subject)
        pack = self.platform.Pack(wd)
        pack.change_status(active_ad, status)


class InsertingFeeVehiculosDesktop(InsertingFee):
    snaps = ['accounts', 'diff_pack_cars1']
    category = 2020
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "0"}
    if_prod_id = 61
    if_prod_price = '6.900'
    is_desktop = True
    platform = desktop

    @tags('payment', 'inserting_fee')
    def test_activate_remove_from_cart(self, wd):
        """ Activate using a pack slot should remove inserting fee from cart """
        self._reload_snaps()
        ad_id = self._insert_ad(wd, self.category)
        self._login_and_go_to_dashboard(wd)
        self._add_if_to_cart_by_modal(wd, self.if_prod_price)  # , ad_id)
        self.platform.DashboardMyAds(wd).go()
        self._change_toggle(wd, "Chery Tiggo 2007", "disabled")
        self._change_toggle(wd, "Aviso con inserting fee", "active")
        summary = self.platform.SummaryPayment(wd).go()
        self.assertTrue(summary.empty_cart.is_displayed())

    @tags('payment', 'inserting_fee')
    def test_delete_remove_from_cart(self, wd):
        """ Delete ad should remove inserting fee from cart """
        self._reload_snaps()
        ad_id = self._insert_ad(wd, self.category)
        self._login_and_go_to_dashboard(wd)
        self._add_if_to_cart_by_modal(wd, self.if_prod_price)  # , ad_id)
        dash = self.platform.DashboardMyAds(wd).go()
        dash.delete_ad_by_subject("Aviso con inserting fee")
        summary = self.platform.SummaryPayment(wd).go()
        self.assertTrue(summary.empty_cart.is_displayed())

    @tags('payment', 'inserting_fee')
    def test_purchase_after_insert_without_slots(self, wd):
        """ Purchase full after insert ad """
        self._reload_snaps()
        ad_id = self._insert_ad(wd, self.category)
        self._login_and_go_to_dashboard(wd)
        # check that the user has one disabled add
        self._check_disabled_ads(wd, 1)
        self._add_if_to_cart_by_modal(wd, self.if_prod_price)  # , ad_id)
        self._pay_and_check(wd)
        utils.rebuild_index()
        # check that the user does not have a disabled ad
        self._go_dashboard(wd)
        self._check_disabled_ads(wd, 0)
        self.assertEquals(len(self.platform.DashboardMyAds(wd).active_ads), 2)
        self._check_inserting_fee_tags(wd, 1)
        summary = self.platform.SummaryPayment(wd).go()
        self.assertTrue(summary.empty_cart.is_displayed())


class InsertingFeeVehiculosDesktopExclusive(InsertingFee):
    snaps = ['accounts', 'diff_pack_cars1']
    category = 2020
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "0"}
    if_prod_id = 61
    if_prod_price = '6.900'
    is_desktop = True
    platform = desktop

    @tags('payment', 'inserting_fee', 'delete_modal', 'desktop')
    def test_delete_modal_in_ad_with_if(self, wd):
        """
        If ad with inserting fee have its modal to delete it only for cars
        """
        ad_id = self._insert_ad(wd, self.category)
        self._login_and_go_to_dashboard(wd)
        # check that the user has one disabled add
        self._check_disabled_ads(wd, 1)
        self._add_if_to_cart(wd, ad_id)
        self._pay_and_check(wd)
        utils.rebuild_index()
        # check that the user does not have a disabled ad
        self._go_dashboard(wd)
        self._check_disabled_ads(wd, 0)
        # check the delete modal
        self._check_delete_modal(wd)
        self._check_delete_page(wd)


class InsertingFeeInmoDesktop(InsertingFeeVehiculosDesktop):
    snaps = ['accounts', 'diff_pack_inmo1']
    category = 1220
    if_prod_id = 62
    if_prod_price = '9.900'
