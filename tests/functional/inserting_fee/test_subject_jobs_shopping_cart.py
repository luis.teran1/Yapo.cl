# -*- coding: latin-1 -*-
import yapo
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.credits import CreditsForm
from yapo.decorators import tags
from yapo.steps.newai import insert_ad
from yapo.pages.newai import DesktopAI
from yapo.pages.desktop import SummaryPayment
from yapo.pages import desktop, mobile
from yapo.pages.mobile import NewAdInsert
from yapo import utils


class SubjectJobsShoppingCartDesktop(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_pack_cars2']
    from_desktop = True
    platform = desktop

    @tags('payment', 'inserting_fee')
    def test_purchase_after_insert_without_slots(self, wd):
        # login with account pro of jobs
        login_and_go_to_dashboard(wd, email='cuentaprojobs@yapo.cl', from_desktop=self.from_desktop)
        # buy credits (only shopping cart)
        credits_form = CreditsForm(wd)
        credits_form.go()
        credits_form.click_fixed_credits_button(3000)
        # inserting ad jobs (7020)
        subject_ad = "Insert Ads jobs (7020) subject"
        self._inser_ad(wd, subject_ad)
        # verify subject from ad in shopping cart ad insert
        summary_payment = self.platform.SummaryPayment(wd)
        self.assertEquals('"{0}"'.format(subject_ad), summary_payment.text_message_payment_subject.text)


    def _inser_ad(self, wd, subject_ad):
        if self.from_desktop:
            insert_ad(
                DesktopAI(wd),
                subject=subject_ad,
                body="Insert Ads jobs (7020) body",
                region=15,
                communes=331,
                category=7020,
                price=778889,
                logged=True,
                Preview=False
            )
        else:
            ad_insert = NewAdInsert(wd)
            ad_insert.go()
            ad_insert.insert_ad(
                subject=subject_ad,
                body="Insert Ads jobs (7020) body",
                region=15,
                communes=331,
                category=7020,
                price=9966,
                private_ad=True,
                jc=['20'],
                contract_type=1,
                working_day=2
                )
            ad_insert.submit.click()

class SubjectJobsShoppingCartMobile(SubjectJobsShoppingCartDesktop):
    snaps = ['accounts', 'diff_pack_cars2']
    from_desktop = False
    user_agent = utils.UserAgents.NEXUS_5
    platform = mobile
