# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages import mobile
from yapo.utils import enable_local_hit_xiti, clear_hit_xiti_log, get_click_xiti_log, UserAgents, enable_clear_xiti, rebuild_index

class TestDesktopXitiAdviewMaps(SeleniumTest):

    snaps = ['accounts', 'diff_maps']

    @tags('xiti', 'maps', 'desktop', 'adview')
    def test_xiti_click_map_adview_desktop(self, wd):
        """
        Tested xiti tag (click) send in map modal of adview.
        """
        enable_clear_xiti()
        ad_view = AdViewDesktop(wd)
        ad_view.go('8000100')
        ad_view.map_preview.open_modal()

        self.assertTrue(get_click_xiti_log('ad_view::form::form::map_view'))

class TestDesktopAdinsertXitiMaps(SeleniumTest):

    @tags('xiti', 'maps', 'desktop')
    def test_xiti_click_map_ai(self, wd):
        """
        Tested xiti tag (click) send in map form of adinsert.
        """
        enable_clear_xiti()
        ad_insert = AdInsert(wd)

        ad_insert.go()
        ad_insert.insert_ad(region=15, communes=296, add_location=True)
        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_add'))

        ad_insert.map_form.address.send_keys('te gusta el pico')
        ad_insert.map_form.address_number.send_keys('1313')
        ad_insert.map_form.approx_area.click()

        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_show_average'))

        ad_insert.map_form.exact_area.click()
        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_show_exact'))

        ad_insert.map_form.map.show_location.click()
        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_address_error'))
        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_display'))

        for i in range(0,4):
            enable_clear_xiti()
            ad_insert.map_form.address_number.send_keys('p')
            ad_insert.map_form.wait_for = lambda x: ad_insert.map_form.map.show_location.is_displayed()
            ad_insert.map_form.map.show_location.click()
            self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_display'))

        enable_clear_xiti()
        ad_insert.map_form.address_number.send_keys('p')
        ad_insert.map_form.wait_for = lambda x: ad_insert.map_form.map.show_location.is_displayed()
        ad_insert.map_form.map.show_location.click()
        self.assertTrue(get_click_xiti_log('ad_insertion::form::form::map_request_error'))

