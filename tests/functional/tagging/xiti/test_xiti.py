# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.mobile import AdView as AdViewMobile
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.desktop_list import AdListDesktop, ReportForm, SearchBoxSideBar
from yapo.pages.desktop import HeaderElements, FooterElements
from yapo import utils
import yapo
import yapo.conf
import urllib.parse
import unittest
import time

IP_LAST_DIGIT = not yapo.conf.REGRESS_HOSTIP.endswith(
    '0') and yapo.conf.REGRESS_HOSTIP[-1:] or '10'


class AdView(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_store_3shops']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def setup(self, driver):
        utils.enable_local_hit_xiti()
        self.vi = AdViewDesktop(driver)
        self.list = AdListDesktop(driver)
        self.searchbox = SearchBoxSideBar(driver)
        self.header = HeaderElements(driver)
        self.vi.go('8000082')
        utils.xiti_clear_logs()

    @tags('xiti', 'ad_view', 'desktop')
    def test_onload_tag(self, wd):
        """Xiti: Check the Ad View Load tag"""
        self.setup(wd)
        wd.refresh()
        logs = utils.get_page_xiti_log('ad')
        self.assertEquals(logs['page_type'], 'AdView')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')

    @tags('xiti', 'header', 'ad_view', 'desktop')
    def test_header_click_tags(self, wd):
        """Xiti: Check the Header On Click tag in ad view"""
        self.setup(wd)
        self.header.logo_link.click()
        logs = utils.get_click_xiti_log('Logo::Logo::Logo::vi')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        self.header.button_my_account.click()
        logs = utils.get_page_xiti_log('ad')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Account_top_navbar')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        utils.xiti_clear_logs()
        self.header.button_create_account.click()
        logs = utils.get_click_xiti_log(
            'Register_page::step_1::step_1::step_1')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        self.header.button_publish_ad.click()
        logs = utils.get_click_xiti_log(
            'Ad_insert::ad_insertion::ad_insertion::otherpages')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

    @tags('xiti', 'adreply', 'ad_view', 'desktop')
    def test_vi_ad_reply_tags(self, wd):
        """Xiti: Check the Ad Reply tags in ad view"""
        self.setup(wd)
        # error in ad_reploy
        self.vi.ad_reply_send_button.click()
        logs = utils.get_click_xiti_log(
            'Sent_email_confirm::8000::8020::Sent_email_confirm')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('form_error')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Contact_seller_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        utils.xiti_clear_logs()
        # error in ad_reploy
        self.vi.ad_reply_name_input.send_keys('Cordero Palo AL')
        self.vi.ad_reply_email_input.send_keys('Cordero@alpalo.com')
        self.vi.ad_reply_phone_input.send_keys('123123123')
        self.vi.ad_reply_message_input.send_keys(
            'asda sda sdasd asda dasd xasdasda')
        utils.xiti_clear_logs()
        self.vi.ad_reply_send_button.click()
        logs = utils.get_click_xiti_log(
            'Sent_email_confirm::8000::8020::Sent_email_confirm')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Confirmation_email')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Contact_seller_confirmation')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')

    @tags('xiti', 'scarface', 'ad_view', 'desktop')
    def test_vi_abuse_options_tags(self, wd):
        """Xiti: Check the abuse options tags in ad view"""
        self.setup(wd)
        # error in ad_reploy
        self.vi.abuse_link_button.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad::8000::8020::report_the_ad')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Step_1')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_title'], 'Test_07')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        # Fraud
        utils.xiti_clear_logs()
        self.vi.abuse_option_fraud.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Fraude')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Fraude')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # Duplicate
        wd.refresh()
        self.vi.abuse_link_button.click()
        utils.xiti_clear_logs()
        self.vi.abuse_option_duplicate.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Duplicado')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Duplicado')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # Wrong Category
        wd.refresh()
        self.vi.abuse_link_button.click()
        utils.xiti_clear_logs()
        self.vi.abuse_option_wrong_category.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Categor_a_equivocada')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Categor_a_equivocada')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # Already Sold
        wd.refresh()
        self.vi.abuse_link_button.click()
        utils.xiti_clear_logs()
        self.vi.abuse_option_already_sold.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Producto_vendido')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Producto_vendido')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # Company
        wd.refresh()
        self.vi.abuse_link_button.click()
        utils.xiti_clear_logs()
        self.vi.abuse_option_company.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Aviso_de_empresa')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Aviso_de_empresa')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # Other Reason
        wd.refresh()
        self.vi.abuse_link_button.click()
        utils.xiti_clear_logs()
        self.vi.abuse_option_other_reason.click()
        logs = utils.get_click_xiti_log(
            'Report_this_ad_sent::8000::8020::Otros')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Otros')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')

    @tags('xiti', 'scarface', 'ad_view', 'desktop')
    def test_vi_abuse_form_tags(self, wd):
        """Xiti: Check the abuse form tags in ad view"""
        self.setup(wd)
        self.vi.abuse_link_button.click()
        self.vi.abuse_option_other_reason.click()
        utils.xiti_clear_logs()
        form = ReportForm(wd)
        # missing email
        form.abuse_submit.click()
        self.assertEquals(form.error_message.text, 'Escribe tu e-mail')
        logs = utils.get_page_xiti_log('Form_error')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # missing body
        form.abuse_email.send_keys('tulo@cabral.com')
        utils.xiti_clear_logs()
        form.abuse_submit.click()
        self.assertEquals(form.error_message.text, 'Escribe el mensaje')
        logs = utils.get_page_xiti_log('Form_error')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')
        # successful form
        form.abuse_message.send_keys('teste te este')
        utils.xiti_clear_logs()
        form.abuse_submit.click()
        logs = utils.get_page_xiti_log('Form_error')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_report_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_id'], '8000082')

    @tags('xiti', 'management_buttons', 'ad_view', 'desktop')
    def test_vi_management_buttons(self, wd):
        """Xiti: Test the xiti tags in the management toolbar"""
        self.setup(wd)
        # bump button
        self.vi.product_link_bump.click()
        logs = utils.get_click_xiti_log('Subir::8000::8020::Subir')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        # edit button
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        self.vi.ad_admin_edit_link.click()
        logs = utils.get_click_xiti_log('Modify::8000::8020::Modify')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        # delete button
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        self.vi.ad_admin_remove_link.click()
        logs = utils.get_click_xiti_log('Remove::8000::8020::Remove')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

    @tags('xiti', 'footer', 'ad_view', 'desktop')
    def test_vi_footer_tags(self, wd):
        """Xiti: Test the xiti tags in the footer"""
        self.setup(wd)
        footer = FooterElements(wd)
        # rules link
        footer.rules.click()
        logs = utils.get_click_xiti_log('Footer::footer::footer::Rules')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        # advices and security
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        footer.advices_and_security.click()
        logs = utils.get_click_xiti_log('Footer::footer::footer::Security')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        # terms and conditions
        self.vi.go('8000082')
        utils.xiti_clear_logs()
        footer.terms_and_conditions.click()
        logs = utils.get_click_xiti_log('Footer::footer::footer::Copy')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

    @tags('xiti', 'share')
    def test_vi_share_buttons_tags(self, wd):
        """Xiti: Test the share buttons tags"""
        self.setup(wd)
        # share by mail
        utils.xiti_clear_logs()
        self.vi.share_email_button.click()
        logs = utils.get_click_xiti_log('Share_tip_email::8000::8020::email')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Tip_form')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_tip_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')
        # mail error form
        utils.xiti_clear_logs()
        self.vi.wait.until_present('send_tip_submit_button')
        self.vi.send_tip_submit_button.click()
        self.assertEquals(self.vi.send_tip_error_message.text, 'Escribe tu nombre')
        logs = utils.get_page_xiti_log('Tip_form_error')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_tip_process')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')
        # mail success form
        utils.xiti_clear_logs()
        self.vi.send_tip_name_input.send_keys('tulo')
        self.vi.send_tip_email_input.send_keys('tulo@cabral.cl')
        self.vi.send_tip_message_input.send_keys('ola q yolo')
        self.vi.send_tip_submit_button.click()
        logs = utils.get_page_xiti_log('Mail')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_tip_confirmation')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')
        self.assertEquals(logs['ad_id'], '8000082')
        # facebook button
        utils.xiti_clear_logs()
        self.vi.share_facebook_button.click()
        logs = utils.get_click_xiti_log('Share_tip_FB::8000::8020::FB')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Facebook')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_tip_confirmation')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')
        wd.switch_to_window(wd.window_handles[1])
        wd.close()
        wd.switch_to_window(wd.window_handles[0])
        # twitter button
        utils.xiti_clear_logs()
        self.vi.share_twitter_button.click()
        logs = utils.get_click_xiti_log('Share_tip_TW::8000::8020::TW')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = utils.get_page_xiti_log('Twitter')
        self.assertEquals(logs['site_type'], 'web')
        self.assertEquals(logs['page_type'], 'Ad_tip_confirmation')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')
        wd.switch_to_window(wd.window_handles[1])
        wd.close()
        wd.switch_to_window(wd.window_handles[0])

    @tags('xiti', 'store')
    def navigate_to_ad(self, driver):
        driver.get(yapo.conf.DESKTOP_URL + "/region_metropolitana?ca=15_s")
        driver.find_elements_by_css_selector('.title')[3].click()

    def print_logs(self, logs):
        for key in logs:
            print('{}: {}'.format(key, logs[key]))
