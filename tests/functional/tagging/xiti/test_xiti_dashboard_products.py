# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import SeleniumTest, utils
from yapo.utils import enable_local_hit_xiti, clear_hit_xiti_log, check_xiti_page_tag_values, Products, get_click_xiti_log
from yapo.pages.desktop import Login, Dashboard, BumpInfoLightbox
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
import time

class TestXitiDashboardLightboxesDesktop(SeleniumTest):

    snaps = ['accounts']

    @tags('xiti','desktop')
    def test_10_check_lightboxes_in_dashboard(self, wd):
        '''
        Test xiti tags in help icons in dashboard.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        dashboard = Dashboard(wd)
        dashboard.help_gallery.click()
        self.assertTrue(check_xiti_page_tag_values('Gallery_more_info_lightbox', 'Bump_pages', None))
        self._check_customer_service_tag(wd)
        dashboard.go()
        dashboard.help_bump.click()
        self.assertTrue(check_xiti_page_tag_values('Bump_weekly_more_info_light', 'Bump_pages', None))
        self._check_customer_service_tag(wd)
        dashboard.go()
        dashboard.help_label.click()
        self.assertTrue(check_xiti_page_tag_values('Label_more_info_lightbox', 'Bump_pages', None))
        self._check_customer_service_tag(wd)

    def _enable_clear_xiti(self):
        enable_local_hit_xiti()
        clear_hit_xiti_log()

    def _login(self, wd):
        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se','123123123')

    def _check_customer_service_tag(self, wd):
        popup = BumpInfoLightbox(wd)
        popup.customer_service_link.click()
        logs = utils.get_click_xiti_log('Bump::bump::bump::contact_us_link')
        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

