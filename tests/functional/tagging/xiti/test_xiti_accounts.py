"""
Tested xiti tags of Accounts pages in desktop and msite.
"""
# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo.pages.accounts import Create, ForgotPassword
from yapo.pages.desktop import Login, Profile
from yapo.utils import clear_hit_xiti_log
from yapo.utils import get_page_xiti_log
from yapo.utils import enable_clear_xiti
from yapo.pages.generic import SentEmail

class TestXitiAccounts(SeleniumTest):
    """
    Testes xiti tags in account pages desktop.
    """

    snaps = ['accounts']

    @tags('desktop', 'accounts', 'xiti')
    def test_accounts_xiti_form_errors(self, driver):
        """
        Tested xiti tags create account with errors.
        """
        enable_clear_xiti()
        create = Create(driver)

        create.go()
        create.submit_btn.click()
        create.wait.until_present('errors')

        # Xiti page in register page
        xiti = get_page_xiti_log('Register_page')
        self.assertEqual(xiti['page_type'], 'form_error')

    @tags('desktop', 'accounts', 'xiti')
    def test_xiti_create_accounts(self, driver):
        """
        Tested xiti tags create account.
        """
        enable_clear_xiti()
        create = Create(driver)
        create.go()

        # Xiti page when the page is loaded in "crear cuenta"
        xiti = get_page_xiti_log('Register_page')
        self.assertEqual(xiti['page_type'], 'step1')
        clear_hit_xiti_log()

        create.fill_form(person_radio=True,
                         accept_conditions=True,
                         name='La loca sofia',
                         phone='56765676',
                         email='la_loca_sofia@laviejachica.cl',
                         password='123123123',
                         password_verify='123123123')

        create.submit_btn.click()
        create.wait.until_present('title_account_success')

        # Xiti page when form is send in "crear cuenta"
        xiti = get_page_xiti_log('Registerpage')
        self.assertEqual(xiti['page_type'], 'confirmation')
        clear_hit_xiti_log()

        email = SentEmail(driver)
        email.go_to_link()

        # Xiti page when user go to dashboard from email
        xiti = get_page_xiti_log('Account_dashboard')
        self.assertEqual(xiti['page_type'], 'account_dashboard')

    @tags('desktop', 'accounts', 'xiti', 'profile')
    def test_xiti_edit_accounts(self, driver):
        """
        Tested xiti tags Account_my_info in profile.
        """
        enable_clear_xiti()
        profile = Profile(driver)
        profile.go_profile('many@ads.cl', '123123123')

        # Xiti in profile
        xiti = get_page_xiti_log('Account_my_info')
        self.assertEqual(xiti['page_type'], 'my_info')

    @tags('desktop', 'accounts', 'xiti', 'forgot_password')
    def test_xiti_reset_password(self, driver):
        """
        Tested xiti tags in reset passwords
        """
        enable_clear_xiti()
        login = Login(driver)
        login.go()

        # Xiti in login page
        xiti = get_page_xiti_log('Register_page')
        self.assertEqual(xiti['page_type'], 'step1')

        forgot_password = ForgotPassword(driver)
        forgot_password.go()

        # Xiti in reset password page
        xiti = get_page_xiti_log('Form_resetpassword')
        self.assertEqual(xiti['page_type'], 'step1')
        clear_hit_xiti_log()

        forgot_password.send('many@ads.cl')

        # Xiti in reset password confirm
        xiti = get_page_xiti_log('Form_resetpassword')
        self.assertEqual(xiti['page_type'], 'step2')
