# -*- coding: latin-1 -*-
from selenium.common import exceptions
from yapo import conf
from yapo.decorators import tags

from yapo.pages import desktop
from yapo.pages import mobile

from yapo.pages.ad_insert import AdInsert as AdInsertDesktop
from yapo.pages.mobile import NewAdInsert as AdInsertMobile

import yapo.pages.desktop
import yapo.pages.mobile

from yapo.utils import Upselling, UserAgents

import time
import yapo

class XitiUpselling(yapo.SeleniumTest):

    snaps = ['accounts']

    urls = {
        'weekly_bump': '{0}/pagos?id=6120.95&prod={1}&ftype=1'.format(conf.SSL_URL, Upselling.WEEKLY_BUMP),
        'daily_bump': '{0}/pagos?id=6120.95&prod={1}&ftype=1'.format(conf.SSL_URL, Upselling.DAILY_BUMP),
        'gallery': '{0}/pagos?id=6120.95&prod={1}&ftype=1'.format(conf.SSL_URL, Upselling.GALLERY),
        'adInsert': '{0}/ai'.format(conf.PHP_URL)
    }

    AdInsert = AdInsertDesktop
    platform = desktop

    def setUp(self):
        yapo.utils.enable_local_hit_xiti()

    def tearDown(self):
        yapo.utils.xiti_clear_logs()

    # Lets check unlogged scenarios
    def test_xiti_upselling_weekly_bump(self, wd):
        """Click tag when user clicks on weekly bump in confirmation page"""
        wd.get(self.urls['weekly_bump'])
        logs = yapo.utils.get_click_xiti_log('ad_insertion::insertar_aviso::Ad_insertion_confirmation::weekly_bump')
        self._assert_tags(logs)

    def test_xiti_upselling_daily_bump(self, wd):
        """Click tag when user clicks on daily bump in confirmation page"""
        wd.get(self.urls['daily_bump'])
        logs = yapo.utils.get_click_xiti_log('ad_insertion::insertar_aviso::Ad_insertion_confirmation::daily_bump')
        self._assert_tags(logs)

    def test_xiti_upselling_gallery(self, wd):
        """Click tag when user clicks on gallery in confirmation page"""
        wd.get(self.urls['gallery'])
        logs = yapo.utils.get_click_xiti_log('ad_insertion::insertar_aviso::Ad_insertion_confirmation::gallery')
        self._assert_tags(logs)

    # Lets check logged scenarios
    def test_xiti_upselling_weekly_bump_logged(self, wd):
        """Click tag when logged user clicks on weekly bump in confirmation page"""
        self._login_prepaid5(wd)
        wd.get(self.urls['weekly_bump'])
        time.sleep(2)
        logs = yapo.utils.get_click_xiti_log('ad_insertion::insertar_aviso::Ad_insertion_confirmation::weekly_bump')
        self._assert_tags(logs)

    def test_xiti_upselling_gallery_logged(self, wd):
        """Click tag when logged user clicks on gallery in confirmation page"""
        self._login_prepaid5(wd)
        wd.get(self.urls['gallery'])
        time.sleep(2)
        logs = yapo.utils.get_click_xiti_log('ad_insertion::insertar_aviso::Ad_insertion_confirmation::gallery')
        self._assert_tags(logs)

    def test_xiti_new_upselling_logged(self, wd):
        """Click tag when logged user chooses a combo"""
        self._login_prepaid5(wd)

        ai = self.AdInsert(wd)
        ai.go()

        ai.upload_resource_image('autorotation/img_270.jpg')

        ai.choose_category(1220)
        ai.wait.until_visible('combo_upselling_standard')
        ai.combo_upselling_standard.click()

        ai.choose_category(2020)
        ai.wait.until_visible('combo_upselling_advanced')
        ai.combo_upselling_advanced.click()

        ai.choose_category(5020)
        ai.wait.until_visible('combo_upselling_premium')
        ai.combo_upselling_premium.click()

        standard_inmo_log = yapo.utils.get_click_xiti_log('Ad_insertion::1000::1220::estandar_inmo')
        advanced_cars_log = yapo.utils.get_click_xiti_log('Ad_insertion::2000::2020::avanzado_cars')
        premium_others_log = yapo.utils.get_click_xiti_log('Ad_insertion::5000::5020::premium_others')

        self._assert_tags(standard_inmo_log, 'A')
        self._assert_tags(advanced_cars_log, 'A')
        self._assert_tags(premium_others_log, 'A')

    def _login_prepaid5(self, wd):
        login = self.platform.Login(wd)
        login.login('prepaid5@blocket.se', '123123123')

    def _assert_tags(self, logs, letter = 'N'):
        self.assertEquals(logs['clic'], letter)
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

class XitiUpsellingMsite(XitiUpselling):
    AdInsert = AdInsertMobile
    snaps = ['accounts']
    user_agent = UserAgents.IOS
    platform = mobile

