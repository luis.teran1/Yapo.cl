# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.pages.desktop import SummaryPayment, DashboardMyAds, Pack, Dashboard, Payment
from yapo.pages.packs import DashboardPacks, BuyPacks
from yapo.steps.pay_products_logged import transbank_pay_product
from yapo.pages.simulator import Paysim
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.ad_insert import AdInsert, AdInsertSuccess, PackConfirm
from yapo.utils import Products
from yapo import steps
import time
import logging

logger = logging.getLogger(__name__)

class PacksXiti(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {'*.controlpanel.modules.adqueue.auto.pack.timeout':'1',
              '*.*.autobump.visible_last_digit.5': 0,
              '*.*.autobump.visible_last_digit.0': 0}

    def setUp(self):
        utils.enable_local_hit_xiti()
        utils.xiti_clear_logs()

    @tags('desktop', 'account', 'packs', 'dashboard', 'xiti')
    def test_xiti_pack_info_and_pack_status_page(self, wd):
        """ Check xiti load for pack info page and xiti click purchase first pack,
            furthermore check xiti pack status page and click for purchase new pack.
        """
        login_and_go_to_dashboard(wd, 'prepaid3@blocket.se', '123123123')
        self._insert_ad(wd, 'Test test')

        # buying a first pack
        buy_pack = BuyPacks(wd)
        buy_pack.go()
        logs = yapo.utils.get_page_xiti_log('account_dashboard::pack::pack_auto::pack_auto_info', [1,2,0])
        self.assertEquals(logs['s2'], '12')

        buy_pack.select_pack("020", utils.PacksPeriods.QUARTERLY)
        time.sleep(2) # This xiti click is executed with 2 seconds of delay
        xiti_click = yapo.utils.get_click_xiti_log('purchase_pack::purchase_pack_auto::purchase_pack_auto_info::pack_auto_info')
        self.assertEquals(xiti_click['clic'], 'N')

        summary = SummaryPayment(wd)
        summary.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()


        # buying a new pack
        buy_pack = BuyPacks(wd)
        buy_pack.go(url = 'dashboard-pack-autos')
        logs = yapo.utils.get_page_xiti_log('account_dashboard::pack::pack_auto::pack_auto_status')
        buy_pack.select_pack("020", utils.PacksPeriods.QUARTERLY)
        time.sleep(2)
        xiti_click = yapo.utils.get_click_xiti_log('purchase_pack::purchase_pack_auto::purchase_pack_auto_status::pack_auto_status')
        self.assertEquals(xiti_click['clic'], 'N')


    @tags('desktop', 'account', 'packs', 'dashboard', 'xiti')
    def test_xiti_pack_assistance_form(self, wd):
        """ Check xiti clicks for steps pack assistance form"""

        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        dashboard_packs = DashboardPacks(wd)
        dashboard_packs.go()
        dashboard_packs.next.click()
        dashboard_packs.call_name.send_keys('Luis')
        dashboard_packs.next.click()
        xiti_click = yapo.utils.get_click_xiti_log('help_pack_auto::help_pack_auto::help_pack_auto_step1::pack_auto_status')
        self.assertEquals(xiti_click['clic'], 'N')

        dashboard_packs.call_mail.send_keys('luis@yolo.cl')
        dashboard_packs.next.click()
        xiti_click = yapo.utils.get_click_xiti_log('help_pack_auto::help_pack_auto::help_pack_auto_step2::pack_auto_status')
        self.assertEquals(xiti_click['clic'], 'N')

        dashboard_packs.call_phone.send_keys('12312312')
        dashboard_packs.next.click()
        xiti_click = yapo.utils.get_click_xiti_log('help_pack_auto::help_pack_auto::help_pack_auto_step3::pack_auto_status')
        self.assertEquals(xiti_click['clic'], 'N')


    @tags('desktop', 'account', 'packs', 'dashboard', 'xiti')
    def test_xiti_deactive_ad_with_service(self, wd):
        """ Check xiti click deactive ad with service"""

        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        transbank_pay_product(wd, 8000093, Products.WEEKLY_BUMP, doc_type = Payment.BILL)

        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.search_ad_by_subject("Audi A4 2011")

        pack = Pack(wd)
        pack.change_status(ad, 'inactive')
        xiti_click = yapo.utils.get_click_xiti_log('deactive_ad_with_service::deactive_ad_with_service::deactive_ad_with_service::My_Account')
        self.assertEquals(xiti_click['clic'], 'N')

    def _insert_ad(self, wd, subject):
        steps.insert_an_ad(wd,
            AdInsert=AdInsert,
            subject=subject,
            body="yoLo",
            region=15,
            communes=331,
            category=2020,
            price=3500000,
            name="yolo",
            phone="999966666",
            accept_conditions=True,
            brand=31,
            model=44,
            version=1,
            regdate=2010,
            gearbox=1,
            fuel=1,
            cartype=1,
            mileage=15000,
            logged=True)
        page = PackConfirm(wd)
        self.assertTrue('pack_message_ok')
        time.sleep(3) # waiting for automatic review pack queue
