# -*- coding: latin-1 -*-
from yapo import utils, SeleniumTest
from yapo.decorators import tags
from yapo.pages.autofactlanding import AutofactLandingForm
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages import desktop


class XitiAutofactBanner(SeleniumTest):
    snaps = ['accounts', 'diff_accounts_ad_with_many_photos']

    AdView = AdViewDesktop
    ad_yes_plates = 8000017
    tag_autofact_banner = 'Autofact::2000::2020::pedir_informe'

    def setUp(self):
        utils.enable_local_hit_xiti()
        utils.enable_clear_xiti()

    #def tearDown(self):
    #    utils.xiti_clear_logs()

    @tags('desktop', 'autofact', 'xiti')
    def test_autofact_banner_tag(self, wd):
        """Check xiti click tag on autofact banner link of adview"""

        adview = self.AdView(wd)
        adview.go(self.ad_yes_plates)
        adview.wait.until_present('autofact')
        adview.autofact.click()

        logs = utils.get_click_xiti_log(self.tag_autofact_banner)

        self.assertEquals(logs['clic'], 'N')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

class XitiAutofactLandingPage(SeleniumTest):
    snaps = ['accounts', 'diff_accounts_ad_with_many_photos']

    platform = desktop
    ad_yes_plates = 8000017
    tag_page_name = 'autofact'
    tag_page_type = 'Landing'
    tag_request_report = 'Landing::landing_autofact::landing_autofact::comprar'
    tag_terms_conditions = 'Landing::landing_autofact::landing_autofact::ver_terminos_condiciones'

    def setUp(self):
        utils.enable_local_hit_xiti()
        utils.enable_clear_xiti()

    def tearDown(self):
        utils.xiti_clear_logs()

    @tags('desktop', 'autofact', 'xiti')
    def test_autofact_landing_tag(self, wd):
        """Check xiti page tag of autofact landing page"""

        AFL = AutofactLandingForm(wd)
        AFL.go(self.ad_yes_plates)
        AFL.BuyButton.click()

        logs = utils.get_page_xiti_log(self.tag_page_name)

        self.assertEquals(logs['page_name'], self.tag_page_name)
        self.assertEquals(logs['page_type'], self.tag_page_type)

    @tags('desktop', 'autofact', 'xiti')
    def test_request_report_autofact_landing_tag(self, wd):
        """Check xiti click tag on request report button of autofact landing page"""

        AFL = AutofactLandingForm(wd)
        AFL.go(self.ad_yes_plates)
        AFL.BuyButton.click()

        logs = utils.get_click_xiti_log(self.tag_request_report)

        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

    @tags('desktop', 'autofact', 'xiti')
    def test_terms_conditions_autofact_landing_tag(self, wd):
        """Check xiti click tag on terms and conditions button of autofact landing page"""

        AFL = AutofactLandingForm(wd)
        AFL.go(self.ad_yes_plates)
        AFL.TermsConditionsButton.click()

        logs = utils.get_click_xiti_log(self.tag_terms_conditions)

        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
