# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
import time
from yapo import SeleniumTest
from yapo.utils import enable_local_hit_xiti, clear_hit_xiti_log, get_click_xiti_log, get_page_xiti_log, UserAgents
from yapo.pages.mobile import AdView, NewAdInsert

class TestXitiMobile(SeleniumTest):

    user_agent = UserAgents.IOS
    snaps = ['accounts']
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('xiti','confirmation_email','mobile')
    def test_01tag_xiti_confirmation_email(self, wd):
        '''
        Tested xiti tag confirmation email in ad reply form mobile.
        '''
        self._enable_clear_xiti()
        adview = AdView(wd)
        adview.go_vi("8000082")
        adview.open_top_adreply()
        adview.send_adreply('dedeed', 'a@aa.cl', '232323', 'dioewjdiewdew')
        logs = get_click_xiti_log('Sent_email_confirm::8000::8020::Sent_email_confirm')
        self.assertEquals(logs['clic'], 'A')
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')
        logs = get_page_xiti_log('Confirmation_email')
        self.assertEquals(logs['site_type'], 'mobile')
        self.assertEquals(logs['page_type'], 'Contact_seller_confirmation')
        self.assertEquals(logs['ad_cat'], '8000')
        self.assertEquals(logs['ad_sub_cat'], '8020')
        self.assertEquals(logs['ad_title'], 'Test_07')

    def _enable_clear_xiti(self):
        enable_local_hit_xiti()
        clear_hit_xiti_log()


    @tags('xiti','ad_insert','mobile', 'ucai')
    def test_ad_insert_xiti_tags(self, wd):
        ''' Test the xiti tags on new ad insert mobile'''
        ad_data = {
            'subject': 'this new ad subject',
            'body': 'this new body content',
            'price': '100000',
            'type': 's',
            'name': 'Pablo',
            'email': 'pablo@ppamo.cl',
            'phone': '123123123',
            'passwd': '123123123',
            'passwd_ver': '123123123'
        }
        self._enable_clear_xiti()
        mai= NewAdInsert(wd)
        mai.go()
        logs = get_page_xiti_log('form', [1,2,0])
        self.assertEqual(logs['s2'], '')
        logs = get_page_xiti_log('form', [2,1,0])
        self.assertEqual(logs['page_type'], 'Ad_insertion_process')
        self.assertEqual(logs['page_name'], 'Form')
        self.assertEqual(logs['vtag'], '44000')
        mai.insert_ad(region = "15", communes=327, category = "8020", **ad_data)
        mai.submit.click()
        logs = get_page_xiti_log('8000', [1,2,0])
        self.assertEqual(logs['s2'], '')
        logs = get_page_xiti_log('8000', [2,1,0])
        self.assertEqual(logs['ad_type'], 'sell')
        self.assertEqual(logs['ad_region'], '16')
        self.assertEqual(logs['ad_title'], 'This_new_ad_subject')
        self.assertEqual(logs['page_type'], 'Ad_insertion_confirmation')
        self.assertEqual(logs['page_name'], '8000')
        self.assertEqual(logs['ad_cat'], '8000')
        self.assertEqual(logs['site_type'], 'mobile')
        self.assertEqual(logs['vtag'], '44000')
