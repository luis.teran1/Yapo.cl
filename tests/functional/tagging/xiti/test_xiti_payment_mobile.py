# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
import time
from yapo import SeleniumTest
from yapo.utils import enable_local_hit_xiti, clear_hit_xiti_log, check_xiti_page_tag_values, UserAgents, Products
from yapo.pages.mobile import Payment, Login, DashboardMyAds, CommonAccountElements, SummaryPayment
from yapo.pages.simulator import Paysim
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait

class TestXitiPaymentMobile(SeleniumTest):

    user_agent = UserAgents.IOS
    snaps = ['accounts']
    bconfs = {
        '*.*.autobump.visible_last_digit.5': 0,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('xiti','bump_confirm','mobile')
    def test_01tag_bump_confirm(self, wd):
        '''
        Tested xiti tag bump_confirm, with normal bump in mobile.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_confirm(wd)

    @tags('xiti','bump_confirm','mobile')
    def test_02tag_bump_fail_cancel(self, wd):
        '''
        Tested xiti tag bump_fail_cancel, with normal bump in mobile.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_cancel(wd)

    @tags('xiti','bump_fail_session_expired','mobile')
    def test_04tag_bump_fail_session_expired(self, wd):
        '''
        Tested xiti tag bump_fail_session_expired, with normal bump in mobile.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_session_expired(wd)

    @tags('xiti','bump_fail_page_not_found','mobile')
    def test_05tag_bump_fail_page_not_found(self, wd):
        '''
        Tested xiti tag bump_fail_page_not_found, with normal bump in Mobile.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_page_not_found(wd)

    @tags('xiti','Bump_fail_cancel','mobile')
    def test_06dashboard_tag_bump_fail_cancel(self, wd):
        '''
        Tested xiti tag bump_fail_cancel, in multiproduct in mobile.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_cancel(wd)

    @tags('xiti','bump_fail_session_expired','mobile')
    def test_08dashboard_tag_bump_fail_session_expired(self, wd):
        '''
        Tested xiti tag bump_fail_session_expired, in multiproduct in mobile.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_session_expired(wd)

    @tags('xiti','bump_fail_page_not_found','mobile')
    def test_09dashboard_tag_bump_fail_page_not_found(self, wd):
        '''
        Tested xiti tag bump_fail_page_not_found, in multiproduct in Mobile.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_page_not_found(wd)

    @tags('xiti','bump_fail_page_not_found','mobile')
    def test_10_payment_more_info(self, wd):
        '''
        Tested xiti tag payment more info.
        '''
        self._enable_clear_xiti()
        payment = Payment(wd)
        payment.go_to_pay_bump(list_id='6394118')
        data_xtpage_tag = payment.more_info.get_attribute('data-xtpage-tag')
        data_xtpage_xtpage = payment.more_info.get_attribute('data-xtpage-xtpage')
        self.assertFalse(data_xtpage_tag)
        self.assertFalse(data_xtpage_xtpage)
        self.assertTrue(payment.more_info.get_attribute('onclick'))
        payment.more_info.click()
        self.assertTrue(check_xiti_page_tag_values('Bump_more_info_lightbox', 'Bump_pages', None))

    def _login(self, wd):
        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se','123123123')

    def _verify_tag_bump_fail_cancel(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.pay_cancel()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_cancel', 'Bump_pages', None)))

    def _verify_tag_bump_confirm(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.pay_accept()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_confirm', 'Bump_pages', None)))

    def _verify_tag_bump_fail_session_expired(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.change_order('2050001')
        paysim.pay_accept()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_session_expired', 'Bump_pages', None)))

    def _verify_tag_bump_fail_page_not_found(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.change_order('2050001')
        paysim.pay_accept()
        paysim._driver.refresh()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_page_not_found', 'Bump_pages', None)))

    def _add_prods_dashboard_myads(self, wd):
        dashboard = DashboardMyAds(wd)
        for list_id, active in dashboard.active_ads.items():
            active.select_product(Products.BUMP);

    def _summary_page(self, wd):
        sp = SummaryPayment(wd)
        sp.go_summary()
        sp.press_link_to_pay_products()

    def _go_pay_and_press_pay_btn(self, wd):
        self._enable_clear_xiti()
        payment = Payment(wd)
        payment.go_to_pay_bump('8000084')
        payment.press_pay_button()

    def _enable_clear_xiti(self):
        enable_local_hit_xiti()
        clear_hit_xiti_log()
