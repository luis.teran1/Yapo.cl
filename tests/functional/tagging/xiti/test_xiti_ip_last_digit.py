# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo import conf, utils
from yapo.pages.mobile import AdView
from yapo.pages.desktop_list import AdViewDesktop
import urllib.parse
import unittest

IP_LAST_DIGIT = not conf.REGRESS_HOSTIP.endswith('0') and conf.REGRESS_HOSTIP[-1:] or '10'

skip_message = 'Disabled because in Jenkins JS always returns 1 as IP last digit'

class TestXitiIpLastDigit(SeleniumTest):

    snaps = ['accounts']
    urls = [
        conf.DESKTOP_URL, # Check in homepage
        conf.DESKTOP_URL + '/region_metropolitana', # Check in listing page
        conf.DESKTOP_URL + '/vi/6000667.htm', # Check in ad view/detail page
        conf.DESKTOP_URL + '/ayuda/preguntas_frecuentes.html', # Check in FAQ page
        conf.DESKTOP_URL + '/ayuda/sobre_yapo.html', # Check in About page
        conf.DESKTOP_URL + '/ayuda/reglas.html', # Check in Rules page
        conf.DESKTOP_URL + '/mis_avisos.html', # Check in My Ads page
        conf.PHP_URL + '/ai', # Check in Insert Ad form page
        conf.PHP_URL + '/adwatch', # Check in favorites page
        conf.SSL_URL + '/cuenta', # Check in Create Account page
    ]
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @unittest.skip(skip_message)
    @tags('xiti', 'desktop')
    def test_ip_last_digit_www(self, wd):
        """ Check Xiti ip last digit for www server """
        for url in self.urls:
            wd.get(url)
            ip_last_digit = wd.execute_script("return xtcustom.ip_last_digit")
            self.assertEquals(IP_LAST_DIGIT, ip_last_digit)


class TestDesktopARXitiTagging(SeleniumTest):
    """ Xiti: desktop ad reply """

    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @unittest.skip(skip_message)
    @tags('xiti', 'adreply', 'desktop')
    def test_xiti_adreply_with_js(self, wd):
        """ Xiti: adreply with js, correct tagging """

        ad_view = AdViewDesktop(wd)
        ad_view.go('6394118')
        error_click = urllib.parse.unquote(ad_view.ad_reply_error.get_attribute("value"))
        confirm_click = urllib.parse.unquote(ad_view.ad_reply_confirm.get_attribute("value"))
        self.assertEquals(u'foo&stc={"page_name":"Form_error","site_type":"web","identified_visitors":"2","visitor_type":"","page_type":"Contact_seller_process","ad_cat":"1000","ad_sub_cat":"1240","ad_region":"16","ad_source":"","ad_type":"","ad_id":"6394118","ip_last_digit":"' + IP_LAST_DIGIT  + '","ad_title":"Arriendo_mi_preciosa_casa_en_nunoa","searched_kw":"","searched_pgnb":"1","searched_posclick":"1"}', error_click)
        self.assertEquals(u'foo&stc={"page_name":"Confirmation_email","site_type":"web","identified_visitors":"2","visitor_type":"","page_type":"Contact_seller_confirmation","ad_cat":"1000","ad_sub_cat":"1240","ad_region":"16","ad_source":"","ad_type":"","ad_id":"6394118","ip_last_digit":"' + IP_LAST_DIGIT + '","ad_title":"Arriendo_mi_preciosa_casa_en_nunoa","searched_kw":"","searched_pgnb":"1","searched_posclick":"1"}', confirm_click)
