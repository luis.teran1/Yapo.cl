# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo
from yapo import pages
from yapo.pages.simulator import Simulator, PaymentSuccess
from yapo.pages.generic import AccountBar
from yapo.pages.stores import BuyStore as BuyStorePage
from yapo.pages.stores import EditForm
from yapo import conf
import time
import logging

logger = logging.getLogger(__name__)


class BuyStoreXiti(yapo.SeleniumTest):
    snaps = ['accounts']

    store_info = {
        'name': "El gabacho de Moniacas",
        'info_text': "La descripci�n de la tinedita esta overrated",
        'phone': "666666666",
        'region': "15",
        'commune': "295",
        'address': "Calle yolanda sultana",
        'address_number': "1313",
        'website_url': "www.fuerzasdemoniacas.com",
        'hide_phone': True,
    }

    def setUp(self):
        yapo.utils.enable_local_hit_xiti()
        yapo.utils.xiti_clear_logs()

    @tags("stores", "payment", "xiti")
    def test_buy_store_options(self, wd):
        """Check xiti click tags in store buy page and buttons"""
        page = pages.stores.BuyStore(wd)
        page.go()
        logs = yapo.utils.get_page_xiti_log('account_dashboard::shop::info_shop::info_shop')
        self.assertEquals(logs['s2'], '12')
        click_tags = {
            'monthly': "shop_buy_month::shop_buy_month::shop_buy_month::shop_buy_month",
            'quarterly': "shop_buy_quarter::shop_buy_quarter::shop_buy_quarter::shop_buy_quarter",
            'biannual': "shop_buy_semester::shop_buy_semester::shop_buy_semester::shop_buy_semester",
            'annual': "shop_buy_year::shop_buy_year::shop_buy_year::shop_buy_year",
        }
        for button_name, tag in click_tags.items():
            getattr(page, button_name).click()
            logs = yapo.utils.get_click_xiti_log(tag)
            self.assertIsNotNone(logs)
            self.assertEqual(logs['clic'], 'N')
            getattr(page, button_name).click()

    @tags("stores", "payment", "xiti")
    def test_buy_store_options(self, wd):
        """Check xiti click tags for store activation"""
        buystore = pages.stores.BuyStore(wd)
        buystore.go("prepaid5@blocket.se")
        buystore.monthly.click()
        summary = pages.desktop.SummaryPayment(wd)
        summary.radio_webpay.click()
        buystore.submit.click()
        page = Simulator(wd)
        page.accept.click()
        page.go_to_site.click()
        page = PaymentSuccess(wd)
        page.edit_store.click()
        xiti_click = yapo.utils.get_click_xiti_log('activate_shop::activate_shop::activate_shop::activate_shop')
        self.assertEquals(xiti_click['clic'], 'N')


class ExistingStore(yapo.SeleniumTest):

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(['accounts', 'singlestore'])
        yapo.utils.enable_local_hit_xiti()
        yapo.utils.xiti_clear_logs()

    def test_side_bar_link(self,wd):
        """Xiti page tags when going to edit the store and sidebar link"""
        page = pages.stores.EditForm(wd)
        page.go()
        page.wait.until_visible('sidebar_store')
        page.sidebar_store.click()
        page.wait.until_loaded()

        xiti_click = yapo.utils.get_click_xiti_log('my_shop::my_shop::my_shop::my_shop')
        self.assertEquals(xiti_click['clic'], 'N')
        xiti_page = yapo.utils.get_page_xiti_log('account_dashboard::shop::manage_shop::manage_shop', [1,2,0])
        self.assertEquals(xiti_page['s2'], '12')

    @tags("desktop", "stores", "xiti", "dashboard")
    def test_edit(self, wd):
        """Xiti click and page tags when going to edit the store"""
        page = pages.stores.EditForm(wd)
        page.go()
        # the test is failing in this part. The xiti log is not created
        page.info_text.clear()
        page.info_text.send_keys('Nueva descripcion informativa de la tienda')
        page.wait.until_visible('submit')
        page.submit.click()
        page.wait.until_loaded()
        xiti_click = yapo.utils.get_click_xiti_log('save_shop::save_shop::save_shop::save_shop')
        logger.debug(xiti_click)
        self.assertEquals(xiti_click['clic'], 'A')

    @tags("desktop", "stores", "xiti")
    def test_listing(self, wd):
        """Tags in store search / list"""
        listing = pages.stores.Listing(wd)
        listing.go()
        listing.search_button.click()
        listing.wait.until_loaded()
        time.sleep(5)
        xiti_page = yapo.utils.get_page_xiti_log('shops::search_shops::search_shops::search_shops', [1,2,0])
        self.assertEquals(xiti_page['s2'], '14')
        xiti_click = yapo.utils.get_click_xiti_log('shop_search::shop_search::shop_search::shop_search')
        self.assertEquals(xiti_click['clic'], 'N')

    @tags("desktop", "stores", "xiti")
    def test_store_view(self, wd):
        """Tags in store view"""
        store = pages.stores.Detail(wd)
        store.go('el-gabacho-de-moniacas')
        xiti_page = yapo.utils.get_page_xiti_log('shops::search_shops::search_shops::search_shops')

    @tags("desktop", "stores", "xiti")
    def test_share(self, wd):
        """Store's share button click tags"""
        yapo.utils.rebuild_csearch()
        store = pages.stores.Detail(wd)
        store.go('el-gabacho-de-moniacas')
        store.share_facebook.click()
        xiti_click = yapo.utils.get_click_xiti_log('share_shop_fb::share_shop_fb::share_shop_fb::share_shop_fb')
        self.assertEquals(xiti_click['clic'], 'A')
        store.share_twitter.click()
        xiti_click = yapo.utils.get_click_xiti_log('share_shop_tw::share_shop_tw::share_shop_tw::share_shop_tw')
        self.assertEquals(xiti_click['clic'], 'A')

    @tags("desktop", "stores", "xiti")
    def test_activation_deactivation(self, wd):
        """Store activation and deactivation click tags"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        editpage.deactivate.click()
        editpage.deactivate_yes.click()
        self.assertEqual(editpage.deactivate_message.text, "�Tu tienda ha sido desactivada!")
        xiti_click = yapo.utils.get_click_xiti_log('deactivate_shop::deactivate_shop::deactivate_shop::deactivate_shop')
        self.assertEquals(xiti_click['clic'], 'N')
