# -*- coding: latin-1 -*-
from selenium.common import exceptions
from yapo import conf, trans, utils, SeleniumTest
from yapo.decorators import tags

from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.mobile_list import AdListMobile

from yapo.pages.mobile import MobileHome
from yapo.pages.desktop import DesktopHome

from yapo.pages2.common import PromoPage
from datetime import date


class XitiPromoPages(SeleniumTest):
    snaps = ['accounts']
    tag_base = 'arma-tu-primer-hogar'

    def setUp(self):
        utils.enable_local_hit_xiti()

    @tags('xiti', 'promotional_pages')
    def test_promotional_pages_tag(self, wd):
        """Xiti: Check promo pages tag """
        urls = ['arma-tu-primer-hogar', 'consolas']

        for url in urls:
            promo = PromoPage(wd).go(url)
            logs = utils.get_page_xiti_log(self.tag_base)
            self.assertEquals(logs['page_name'], self.tag_base)


class XitiPpages(SeleniumTest):

    platform = ''
    position = ''
    PageObject = None
    snaps = ['accounts']

    def setUp(self):
        utils.enable_local_hit_xiti()
        utils.flush_redis_banners()

    def tearDown(self):
        utils.xiti_clear_logs()

    def set_banner(self, banner_data):
        trans_result = trans.check_trans('set_pp_banner', **banner_data)
        self.assertTrue('status' in trans_result and trans_result['status'] == 'TRANS_OK')

    def _get_logs(self, position):
        tag = ('Pag_promo_{}::'.format(position) * 4)[:-2]
        return utils.get_click_xiti_log(tag)

    def _assert_tags(self, position, tag_type = 'A'):
        logs = self._get_logs(position)

        self.assertEquals(logs['clic'], tag_type)
        self.assertEquals(logs['s2'], '')
        self.assertEquals(logs['vtag'], '44000')

    def _get_banner_data(self, platform, position):
        today = date.today()
        return {
           'pp_id': 99,
           'url': 'arma-tu-primer-hogar',
           'platform': platform,
           'position': position,
           'start_date': today,
           'image': '/ppageimages/61/6108523319.jpg'
        }


class XitiPpagesListingDesktop(XitiPpages):
    platform = 'desktop'
    position = 'listing'
    PageObject = AdListDesktop


class XitiPpagesListingMsite(XitiPpages):
    platform = 'msite'
    position = 'listing'
    PageObject = AdListMobile


class XitiPpagesHomeMsite(XitiPpages):
    platform = 'msite'
    position = 'home'
    PageObject = MobileHome
