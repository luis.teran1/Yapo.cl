# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import SeleniumTest
from yapo.utils import enable_local_hit_xiti, clear_hit_xiti_log, check_xiti_page_tag_values, Products, get_click_xiti_log
from yapo.pages.simulator import Paysim
from yapo.pages.desktop import Payment, Login, SummaryPayment, DashboardMyAds
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
import time

class TestXitiPaymentDesktop(SeleniumTest):

    snaps = ['accounts']

    @tags('xiti','bump_confirm','desktop')
    def test_01tag_bump_confirm(self, wd):
        '''
        Tested xiti tag bump_confirm, with normal bump in desktop.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_confirm(wd)

    @tags('xiti','bump_confirm','desktop')
    def test_02tag_bump_fail_cancel(self, wd):
        '''
        Tested xiti tag bump_fail_cancel, with normal bump in desktop.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_cancel(wd)

    @tags('xiti','bump_fail_ad_not_found','desktop')
    def test_03tag_bump_fail_ad_not_found(self, wd):
        '''
        Tested xiti tag bump_fail_ad_not_found, with normal bump in desktop.
        '''
        self._enable_clear_xiti()
        payment = Payment(wd)
        payment.go_to_pay_bump('8000084')
        payment._driver.delete_all_cookies()
        payment.press_pay_button()
        self.assertTrue(check_xiti_page_tag_values('Bump_fail_ad_not_found', 'Bump_pages', None))

    @tags('xiti','bump_fail_session_expired','desktop')
    def test_04tag_bump_fail_session_expired(self, wd):
        '''
        Tested xiti tag bump_fail_session_expired, with normal bump in desktop.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_session_expired(wd)

    @tags('xiti','bump_fail_page_not_found','desktop')
    def test_05tag_bump_fail_page_not_found(self, wd):
        '''
        Tested xiti tag bump_fail_page_not_found, with normal bump in desktop.
        '''
        self._go_pay_and_press_pay_btn(wd)
        self._verify_tag_bump_fail_page_not_found(wd)

    @tags('xiti','Bump_fail_cancel','desktop')
    def test_06dashboard_tag_bump_fail_cancel(self, wd):
        '''
        Tested xiti tag bump_fail_cancel, in multiproduct in desktop.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_cancel(wd)


    @tags('xiti','bump_fail_session_expired','desktop')
    def test_08dashboard_tag_bump_fail_session_expired(self, wd):
        '''
        Tested xiti tag bump_fail_session_expired, in multiproduct in desktop.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_session_expired(wd)

    @tags('xiti','bump_fail_page_not_found','desktop')
    def test_09dashboard_tag_bump_fail_page_not_found(self, wd):
        '''
        Tested xiti tag bump_fail_page_not_found, in multiproduct in desktop.
        '''
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        self._summary_page(wd)
        self._verify_tag_bump_fail_page_not_found(wd)

    @tags('xiti', 'payment')
    def test_payment_options(self, wd):
        """
        Toggling between payment method triggers xiti click tag
        """
        self._enable_clear_xiti()
        self._login(wd)
        self._add_prods_dashboard_myads(wd)
        sp = SummaryPayment(wd)
        sp.go_summary()
        time.sleep(3)
        page = Payment(wd)
        page.pay_with_servipag.click()
        page.pay_with_transbank.click()
        webpay_log = get_click_xiti_log("payment::premium_products::radio_webpay")
        servipag_log = get_click_xiti_log("payment::premium_products::radio_servipag")
        self.assertEqual(webpay_log['clic'], "A")
        self.assertEqual(servipag_log['clic'], "A")

    def _login(self, wd):
        login = Login(wd)
        login.go()
        login.login('prepaid3@blocket.se','123123123')

    def _verify_tag_bump_fail_cancel(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.pay_cancel()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_cancel', 'Bump_pages', None)))

    def _verify_tag_bump_confirm(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.pay_accept()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_confirm', 'Bump_pages', None)))

    def _verify_tag_bump_fail_session_expired(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.change_order('2050001')
        paysim.pay_accept()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_session_expired', 'Bump_pages', None)))

    def _verify_tag_bump_fail_page_not_found(self, wd):
        paysim = Paysim(wd)
        wait = Wait(wd, 10)
        paysim.change_order('2050001')
        paysim.pay_accept()
        paysim._driver.refresh()
        self.assertTrue(wait.until(lambda bool: check_xiti_page_tag_values('Bump_fail_page_not_found', 'Bump_pages', None)))

    def _add_prods_dashboard_myads(self, wd):
        dashboard = DashboardMyAds(wd)
        for list_id, active_ad in dashboard.active_ads.items():
            active_ad.select_product(Products.LABEL)

    def _summary_page(self, wd):
        sp = SummaryPayment(wd)
        sp.go_summary()
        sp.press_link_to_pay_products()

    def _go_pay_and_press_pay_btn(self, wd):
        self._enable_clear_xiti()
        payment = Payment(wd)
        payment.go_to_pay_bump('8000084')
        payment.press_pay_button()

    def _enable_clear_xiti(self):
        enable_local_hit_xiti()
        clear_hit_xiti_log()
