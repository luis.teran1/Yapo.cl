# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.control_panel import ControlPanel, ControlPanelBanners
from yapo.pages.desktop_list import AdViewDesktop
from yapo.pages.mobile_list import AdViewMobile
from yapo.pages.desktop_list import AdListDesktop, ReportForm, SearchBoxSideBar
from yapo.pages.desktop import HeaderElements, FooterElements
from yapo import utils
import yapo
import yapo.conf
import urllib.parse
import unittest
import time

class CpanelHandler(yapo.SeleniumTest):

    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    def open_cp(self,wd):
        cp = ControlPanel(wd)
        cp.go()
        if cp.check_login():
            cp.login("dany", "dany")
        cp.go_to_option_in_menu("Administrar Banners")
        return ControlPanelBanners(wd)
