# -*- coding: latin-1 -*-
from yapo import SeleniumTest
from yapo.decorators import retries_function_exception
from yapo.pages import desktop
from yapo.pages.help import Help
from yapo.pages import mobile
from yapo.pages.desktop_list import AdListDesktop, SearchBoxSideBar, AdViewDesktop
from yapo.pages.mobile_list import AdListMobile, SearchBox
from yapo.pages import mobile
from yapo.pages.tealium import Tealium
from yapo.pages.tealium import TealiumEventNames, TealiumGlobalVariables
from yapo.pages.tealium import TealiumListingVariables
from yapo import utils
import subprocess
from yapo import conf

# Build js of regress
utils.makebjr()

class TealiumBaseTest(TealiumGlobalVariables, SeleniumTest):
    """
    Base class of tealium
    """

    bconfs = {
        "*.*.common.tealium.display": "1",
        '*.*.edit_allowed_days.cat.7020': 2615321,
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    def tearDown(self):
        super(TealiumBaseTest, self).tearDown()
        # Delete xiti log files
        utils.clear_hit_xiti_log()

    def _do_loggin(self, wd):
        if hasattr(self, 'account_email') and hasattr(self, 'account_password'):
            if self.is_msite:
                login = mobile.Login(wd)
            else:
                login = desktop.Login(wd)
            login.login(getattr(self, 'account_email'), getattr(self, 'account_password'))

        else:
            raise Exception('Is not define account_email and account_pass as attribute of class.')

    @retries_function_exception(num_retries=3)
    def assert_utag_data(self, wd, utag_data_expected, type='page'):
        if 'event_name' not in utag_data_expected:
            raise Exeption('event_name not found in utag_data_expected')
        tealium = Tealium(wd)
        event_name = utag_data_expected['event_name']
        self.assertDictContainsSubset({str(k):str(v) for k,v in utag_data_expected.items()}, tealium.utag_data(event_name, type))

    @property
    def is_msite(self):
        return hasattr(self, 'is_mobile') and getattr(self, 'is_mobile')

    def _assert_utag_data_in_vi(self, wd, list_id, utag_data_expected):
        if self.is_msite:
            ad_view = mobile.AdView(wd)
        else:
            ad_view = AdViewDesktop(wd)

        ad_view.go(list_id)
        self.assert_utag_data(wd, utag_data_expected)
        utils.clear_hit_xiti_log()

class TealiumListingTest(TealiumListingVariables, TealiumBaseTest):
    """
    Base class of tealium in listing page
    """

    def _search_and_assert_utagdata(self, wd, slug, params_to_fill, utag_data_expected):
        if self.is_msite:
            self._search_and_assert_utagdata_mobile(wd, slug, params_to_fill, utag_data_expected)
        else:
            self._search_and_assert_utagdata_desktop(wd, slug, params_to_fill, utag_data_expected)

    def _search_and_assert_utagdata_desktop(self, wd, slug, params_to_fill, utag_data_expected):
        adlistdesktop = AdListDesktop(wd)
        searchboxsidebar = SearchBoxSideBar(wd)
        adlistdesktop.go(slug)

        category = int(params_to_fill.get('category')) if 'category' in params_to_fill else None

        if category:
            searchboxsidebar.fill_form(category=category)
            if category in [7020, 7040]:
                if 'job_category_link' in params_to_fill:
                    params_to_fill.pop('job_category_link')
                    getattr(searchboxsidebar, 'job_category_link').click()

            if category in [1220, 1240, 1260]:
                searchboxsidebar.fill('estate_type', 1)

            if category in [2020]:
               self._insert_first_parameters(searchboxsidebar, ['brand', 'model', 'version'], params_to_fill)

        if params_to_fill: searchboxsidebar.fill_form(**params_to_fill)

        searchboxsidebar.search_button.click()
        adlistdesktop.wait.until_visible('listing_top')
        adlistdesktop.wait.until_loaded()
        self.assert_utag_data(wd, utag_data_expected)

    def _search_and_assert_utagdata_mobile(self, wd, slug, params_to_fill, utag_data_expected):
        adlistmobile = AdListMobile(wd)
        searchboxsidebar = SearchBox(wd)
        adlistmobile.go(slug)

        category = int(params_to_fill.pop('category')) if 'category' in params_to_fill else None

        if category:
            adlistmobile.fill_form(category=category)

        adlistmobile.filter_button.click()
        searchboxsidebar.wait.until_loaded()

        if category:
            if category in [7020, 7040] and 'job_category_link' in params_to_fill:
                params_to_fill.pop('job_category_link')

            if category in [2020]:
               self._insert_first_parameters(searchboxsidebar, ['brand', 'model', 'version'], params_to_fill)

            if category in [1220, 1240, 1260]:
                searchboxsidebar.fill('estate_type', 1)

        if params_to_fill: searchboxsidebar.fill_form(**params_to_fill)
        searchboxsidebar.search_filter.click()
        adlistmobile.wait.until_loaded()

        self.assert_utag_data(wd, utag_data_expected)

    def _insert_first_parameters(self, page, params_order, params_to_fill):
        for param_key in params_order:
            if param_key in params_to_fill:
                param_value = params_to_fill.pop(param_key)
                params_fill_form = {param_key: param_value}
                page.fill_form(**params_fill_form)

class TealiumGeneralPagesTest(TealiumBaseTest):
    """
    Base class of tealium in general page
    """

    def _assert_utag_data_in_home(self, wd, utag_data_expected):
        pagehome = mobile.MobileHome(wd) if self.is_msite else desktop.DesktopHome(wd)
        pagehome.go()
        self.assert_utag_data(wd, utag_data_expected)

    def _assert_utag_data_in_faqs(self, wd, faqs, utag_data_expected):
        help_page = Help(wd)

        for event_name, page in faqs:
            utag_data = utag_data_expected
            utag_data.update({self.EVENT_NAME: event_name})
            if event_name == TealiumEventNames.REPORT_AN_ANNOUNCER:
                help_page.go_report_an_announcer()
            else:
                help_page.go(page=page)

            self.assert_utag_data(wd, utag_data_expected)

    def _assert_utag_data_in_favorite(self, wd, utag_data_expected):
        favorite = desktop.FavoritePage(wd)
        self.assert_utag_data(wd, utag_data_expected)

    def _assert_utag_data_in_404(self, wd, utag_data_expected):
        if self.is_msite:
            page_404 = mobile.PageNotFound(wd)
            page_404.go()
        else:
            page_404 = desktop.PageNotFound(wd)
            page_404.go_page_not_found_desktop_url()

        self.assertEqual('Esta p�gina no existe', page_404.title.text)
        self.assert_utag_data(wd, utag_data_expected)


class TealiumAdEditPagesTest(TealiumBaseTest):
    """
    Base class of tealium in ad edit page
    """

    def assert_utag_data_ad_edit_password(self, wd, list_id, utag_data):
        edit_page, edit_password_page = self._get_edit_pages(wd)
        edit_page.go(list_id)
        self.assert_utag_data(wd, utag_data)

    def assert_utag_data_ad_edit_password_error(self, wd, list_id, utag_data):
        edit_page, edit_password_page = self._get_edit_pages(wd)

        edit_page.go(list_id)
        edit_password_page.validate_password('wrong password')
        edit_password_page.validate_password('wrong password')
        edit_password_page.wait.until_present('error')
        self.assert_utag_data(wd, utag_data)

    def assert_utag_data_ad_edit(self, wd, list_id, passwd, utag_data):
        edit_page, edit_password_page = self._get_edit_pages(wd)

        edit_page.go(list_id)
        edit_password_page.validate_password(passwd)

        if self.is_msite:
            edit_page.wait_for_image_upload()

        self.assert_utag_data(wd, utag_data)

    def assert_utag_data_ad_edit_submit(self, wd, list_id, passwd, utag_data):
        edit_page, edit_password_page = self._get_edit_pages(wd)
        edit_successpage = mobile.EditSuccessPage(wd) if self.is_msite else desktop.EditSuccessPage(wd)
        edit_page.go(list_id)
        edit_password_page.validate_password(passwd)
        edit_page.wait_bump_animation()
        edit_page.submit.click()

        edit_successpage.wait.until_present('text_thanks')
        self.assert_utag_data(wd, utag_data)

    def _get_edit_pages(self, wd):
        if self.is_msite:
            return (mobile.AdEdit(wd), mobile.EditPasswordPage(wd))
        return (desktop.AdEdit(wd), desktop.EditPasswordPage(wd))
