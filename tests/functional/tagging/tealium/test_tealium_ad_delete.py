# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
from yapo.pages.tealium import TealiumAiVariables, TealiumEventNames, TealiumGlobalVariables
from .tealium import TealiumBaseTest
from yapo.pages import desktop, mobile
from yapo.utils import Categories as cg


class TestTealiumDesktopAdDelete(TealiumBaseTest):

    snaps = ['accounts']
    DELETE_CLASS = desktop.AdDeletePage

    @tags('tealium', 'desktop', 'delete_ad')
    def test_ad_delete(self, wd):
        """
        Tested tealium tag page delete_ad_show (desktop)
        """
        ad_delete = self.DELETE_CLASS(wd)
        ad_delete.go('8000083')
        self.assert_utag_data(wd, {self.EVENT_NAME: TealiumEventNames.DELETE_AD_SHOW})

        ad_delete.select_reason(4)
        ad_delete.fill_form(password='321321')
        ad_delete.submit.click()
        ad_delete.wait.until_visible('error_message')
        self.assert_utag_data(wd, {self.EVENT_NAME: TealiumEventNames.DELETE_AD_ERROR})

        ad_delete.go('8000083')
        ad_delete.select_reason(4)
        ad_delete.fill_form(password='123123123')
        ad_delete.submit.click()
        ad_delete.wait.until_present('view_ads')
        self.assert_utag_data(wd, {self.EVENT_NAME: TealiumEventNames.DELETE_AD_CONFIRMATION})

class TestTealiumMobileAdDelete(TestTealiumDesktopAdDelete):

    snaps = ['accounts']
    DELETE_CLASS = mobile.DeletePage
    user_agent = utils.UserAgents.IOS
    bconfs = {
        '*.*.movetoapps_splash_screen.enabled': '0',
        '*.*.common.tealium.display': '1'
    }

    @tags('tealium', 'desktop', 'delete_ad')
    def test_ad_delete(self, wd):
        """
        Tested tealium tag page delete_ad_show (mobile)
        """
        super(TestTealiumMobileAdDelete, self).test_ad_delete()
