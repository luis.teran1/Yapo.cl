# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
from yapo.pages.tealium import TealiumEventNames
from .tealium import TealiumBaseTest
from yapo.pages import accounts, generic
from yapo.pages import desktop, mobile

class UtagDataExpectedTest(TealiumBaseTest):
    @property
    def utag_data_expected_global(self):
        return {
            self.ENVIRONMENT: 'dev',
            self.LANGUAGE: 'es', self.LANGUAGE_ID: '1'}

    @property
    def utag_data_expected_global_logged(self):
        utag_data = {
            self.USER_EMAIL: 'prepaid5@blocket.se',
            self.USER_REGION_LEVEL1: 'Chile', self.USER_REGION_LEVEL1_ID: '0',
            self.USER_REGION_LEVEL2: 'Regi�n Metropolitana', self.USER_REGION_LEVEL2_ID: '15',
            self.USER_REGION_LEVEL3: 'Las Condes', self.USER_REGION_LEVEL3_ID: '315',
            self.USER_ROLE: 'Profesional', self.USER_ROLE_ID: '2'}

        utag_data.update(self.utag_data_expected_global)

        return utag_data

class TestTealiumDesktopAccounts(UtagDataExpectedTest):

    account_email = 'prepaid5@blocket.se'
    account_password = '123123123'
    snaps = ['accounts']
    ACCOUNT_DASHBOARD = desktop.Dashboard
    CreateAccountClass = accounts.Create
    EditAccountClass = accounts.Edit
    LoginClass = desktop.Login

    @tags('tealium', 'desktop', 'accounts')
    def test_register_page_step1(self, wd):
        """
        Tested tealium tag page register_page_step1 (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.REGISTER_PAGE_STEP1}
        utag_data.update(self.utag_data_expected_global)

        create = self.CreateAccountClass(wd)
        create.go()
        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'accounts')
    def test_register_page_account(self, wd):
        """
        Tested tealium tag page register_page_account (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.REGISTER_PAGE_ACCOUNT}
        utag_data.update(self.utag_data_expected_global)

        login = self.LoginClass(wd)
        login.go()
        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'accounts')
    def test_register_page_form_error(self, wd):
        """
        Tested tealium tag page register_page_form_error (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.REGISTER_PAGE_FORM_ERROR}
        utag_data.update(self.utag_data_expected_global)

        create = self.CreateAccountClass(wd)
        create.go()
        create.submit_btn.click()
        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'accounts')
    def test_my_account(self, wd):
        """
        Tested tealium tag page my_account (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.MY_ACCOUNT}
        utag_data.update(self.utag_data_expected_global_logged)

        self._do_loggin(wd)
        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'accounts')
    def test_my_information(self, wd):
        """
        Tested tealium tag page my_information (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.MY_INFORMATION}
        utag_data.update(self.utag_data_expected_global_logged)

        self._do_loggin(wd)
        account_edit = self.ACCOUNT_DASHBOARD(wd)
        account_edit.wait.until_loaded()
        account_edit.link_profile.click()
        account_edit.wait.until_loaded()

        edit = self.EditAccountClass(wd)
        edit.wait.until_present('edit_profile_btn')
        is_mobile = True if hasattr(self, 'user_agent') else False
        if is_mobile:
            edit.edit_profile_btn.click()

        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'accounts')
    def test_accounts_verified(self, wd):
        """
        Tested tealium tag page accounts_verified (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.ACCOUNTS_VERIFIED}
        utag_data.update(self.utag_data_expected_global)

        create = self.CreateAccountClass(wd)
        create.go()
        create.fill_form(
            name='yolo',
            phone='69696969',
            region=15,
            email='yolo@cabral.es',
            password='123123123',
            password_verify='123123123',
        )
        create.label_is_company_p.click()
        create.label_accept_conditions.click()
        create.submit_btn.click()

        creation_success = accounts.CreationSuccessDesktop(wd)

        self.assert_utag_data(wd, utag_data)

    @unittest.skip("the new header doesn't have tealium class")
    @tags('tealium', 'desktop', 'accounts')
    def test_account_logged_out(self, wd):
        """
        Tested tealium tag page account_logged_out (desktop)
        """
        login = self.LoginClass(wd)
        if self.is_msite:
            login = mobile.Login(wd)

        login.login(self.account_email, self.account_password)

        self.assertIn('tealium-view', login.button_logout.get_attribute('class'))
        self.assertIn('account_logged_out', login.button_logout.get_attribute('data-tealium-view'))

class TestTealiumDesktopAccountsResetPassword(UtagDataExpectedTest):

    account_email = 'prepaid5@blocket.se'
    account_password = '123123123'
    snaps = ['accounts']

    @tags('tealium', 'desktop', 'accounts')
    def test_reset_password(self, wd):
        """
        Tested tealium tag page all reset_passwords accounts (desktop)
        """
        self.reset_password(wd)
        self.reset_password_sent_success(wd)
        self.reset_password_new(wd)
        self.reset_password_confirm(wd)

    def reset_password(self, wd):
        """
        Tested tealium tag page reset_password (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.RESET_PASSWORD}
        utag_data.update(self.utag_data_expected_global)

        forgot_password = accounts.ForgotPassword(wd)
        forgot_password.go()
        self.assert_utag_data(wd, utag_data)

    def reset_password_sent_success(self, wd):
        """
        Tested tealium tag page reset_password_sent_success (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.RESET_PASSWORD_SENT_SUCCESS}
        utag_data.update(self.utag_data_expected_global)

        forgot_password = accounts.ForgotPassword(wd)
        forgot_password.send(self.account_email)

        self.assert_utag_data(wd, utag_data)


    def reset_password_new(self, wd):
        """
        Tested tealium tag page reset_password_new (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.RESET_PASSWORD_NEW}
        utag_data.update(self.utag_data_expected_global)

        sent_email = generic.SentEmail(wd)
        sent_email.go_to_link()

        self.assert_utag_data(wd, utag_data)

    def reset_password_confirm(self, wd):
        """
        Tested tealium tag page reset_password_confirm (desktop)
        """
        utag_data = {self.EVENT_NAME: TealiumEventNames.RESET_PASSWORD_CONFIRM}
        utag_data.update(self.utag_data_expected_global)

        reset_password = accounts.ResetPassword(wd)
        reset_password.send(self.account_email)

        self.assert_utag_data(wd, utag_data)

class TestMobileTealiumTagPageAccounts(TestTealiumDesktopAccounts):
    user_agent = utils.UserAgents.IOS
    is_mobile = True
    ACCOUNT_DASHBOARD = mobile.CommonAccountElements
    CreateAccountClass = accounts.CreateMobile
    EditAccountClass = accounts.EditMobile
    LoginClass = mobile.Login

    @tags('tealium', 'mobile', 'accounts')
    def test_register_page_step1(self, wd):
        """
        Tested tealium tag page register_page_step1 (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_register_page_step1()

    @tags('tealium', 'mobile', 'accounts')
    def test_register_page_account(self, wd):
        """
        Tested tealium tag page register_page_account (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_register_page_account()

    @tags('tealium', 'mobile', 'accounts')
    def test_register_page_form_error(self, wd):
        """
        Tested tealium tag page register_page_form_error (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_register_page_form_error()

    @tags('tealium', 'mobile', 'accounts')
    def test_my_account(self, wd):
        """
        Tested tealium tag page my_account (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_my_account()

    @tags('tealium', 'mobile', 'accounts')
    def test_my_information(self, wd):
        """
        Tested tealium tag page my_information (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_my_information()

    @tags('tealium', 'mobile', 'accounts')
    def test_accounts_verified(self, wd):
        """
        Tested tealium tag page accounts_verified (mobile)
        """
        super(TestMobileTealiumTagPageAccounts, self).test_accounts_verified()


class TestMobileTealiumTagAccountsResetPassword(TestTealiumDesktopAccountsResetPassword):
    user_agent = utils.UserAgents.IOS
    is_mobile = True

    @tags('tealium', 'mobile', 'accounts')
    def test_reset_password(self, wd):
        """
        Tested tealium tag page all reset_passwords accounts (mobile)
        """
        super(TestMobileTealiumTagAccountsResetPassword, self).test_reset_password()
