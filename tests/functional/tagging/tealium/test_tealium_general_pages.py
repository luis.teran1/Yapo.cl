# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
from yapo.pages import desktop
from yapo.pages.tealium import TealiumEventNames as ev
from .tealium import TealiumGeneralPagesTest
from yapo.pages.generic import TechnicalError
import unittest

class UtagDataExpected(TealiumGeneralPagesTest):
    @property
    def utag_data_expected_global(self):
        return {
            self.ENVIRONMENT: 'dev',
            self.LANGUAGE: 'es', self.LANGUAGE_ID: '1',
            self.USER_EMAIL: 'prepaid5@blocket.se',
            self.USER_REGION_LEVEL1: 'Chile', self.USER_REGION_LEVEL1_ID: '0',
            self.USER_REGION_LEVEL2: 'Regi�n Metropolitana', self.USER_REGION_LEVEL2_ID: '15',
            self.USER_REGION_LEVEL3: 'Las Condes', self.USER_REGION_LEVEL3_ID: '315',
            self.USER_ROLE: 'Profesional', self.USER_ROLE_ID: '2'}

    @property
    def utag_data_expected_home(self):
        utag_data = {self.EVENT_NAME: ev.HOME_PAGE}
        utag_data.update(self.utag_data_expected_global)
        return utag_data

class TestTealiumDesktopGeneralPages(UtagDataExpected):
    """
    Tested tealium tags home, ad not found, 404.
    """
    account_email = 'prepaid5@blocket.se'
    account_password = '123123123'
    snaps = ['accounts']

    skip_message = 'Disabled until the functionality is fixed'

    @unittest.skip(skip_message)
    @tags('tealium', 'desktop', 'listing')
    def test_page_home(self, wd):
        """
        Tested tealium tag page in home (desktop)
        """
        self._do_loggin(wd)
        self._assert_utag_data_in_home(wd, self.utag_data_expected_home)

    @tags('tealium', 'desktop', 'ad_not_found')
    def test_page_ad_not_found_404(self, wd):
        """
        Tested tealium tag page in ad not found (desktop)
        """
        self._do_loggin(wd)
        utag_data = self.utag_data_expected_global
        utag_data.update({self.EVENT_NAME: ev.AD_NOT_FOUND_404})

        self._assert_utag_data_in_vi(wd, list_id='99999', utag_data_expected=utag_data)

    @tags('tealium', 'desktop', '404')
    def test_page_error_404(self, wd):
        """
        Tested tealium tag page in 404 (desktop)
        """
        self._do_loggin(wd)
        utag_data = self.utag_data_expected_global
        utag_data.update({self.EVENT_NAME: ev.ERROR_404})

        self._assert_utag_data_in_404(wd, utag_data)

class TestTealiumOnlyDesktopGeneralPages(UtagDataExpected):
    """
    Tested tealium tag in faqs pages, my ads and technical_error.
    """
    account_email = 'prepaid5@blocket.se'
    account_password = '123123123'
    snaps = ['accounts']

    @tags('tealium', 'desktop', 'faqs')
    def test_page_faqs(self, wd):
        """
        Tested tealium tag page in faqs (desktop)
        """
        self._do_loggin(wd)

        faqs = [
            (ev.FREQUENT_QUESTIONS, 'preguntas_frecuentes'),
            (ev.SEARCH_ADVICES, 'sugerencias_busqueda'),
            (ev.BUYING_SAFELY, 'seguridad/consejos'),
            (ev.DELIVERY_OF_PRODUCTS, 'seguridad/envios'),
            (ev.STOLEN_ITEMS, 'seguridad/robados'),
            (ev.VEHICLES, 'seguridad/vehiculos'),
            (ev.BRANDED_ITEMS_AND_SOFTWARE, 'seguridad/marcas'),
            (ev.SENDING_MONEY_OR_PRODUCTS, 'seguridad/envios_extranjero'),
            (ev.MONEY_TRANSFERS, 'seguridad/transferencias'),
            (ev.PREVENTION, 'seguridad/prevencion'),
            (ev.WHO_ARE_WE, 'sobre_yapo'),
            (ev.RULES_FOR_ADS, 'reglas'),
            (ev.REPORT_AN_ANNOUNCER, 'form/0?ca=15_s&id=4'),
            (ev.TERMS_AND_CONDITIONS, 'terminos')
        ]

        self._assert_utag_data_in_faqs(wd, faqs, self.utag_data_expected_global)

    @tags('tealium', 'desktop', 'my_ads')
    def test_page_my_ads(self, wd):
        """
        Tested tealium tag page in my ads (desktop)
        """
        self._do_loggin(wd)
        utag_data = self.utag_data_expected_global
        utag_data.update({self.EVENT_NAME: ev.MY_ADS})

        myads = desktop.MyAds(wd)
        myads.go()

        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'technical_error')
    def test_page_technical_error(self, wd):
        """
        Tested tealium tag page in technical error (desktop)
        """
        self._do_loggin(wd)
        utag_data = self.utag_data_expected_global
        utag_data.update({self.EVENT_NAME: ev.TECHNICAL_ERROR})

        technical_error = TechnicalError(wd)
        technical_error.generate_technical_error()
        self.assertEqual('Problemas t�cnicos', technical_error.title.text)

        self.assert_utag_data(wd, utag_data)

    @tags('tealium', 'desktop', 'favorites')
    def test_page_empty_favorites(self, wd):
        """
        Tested tealium tag empty favorites (desktop)
        """
        favorite_page = desktop.FavoritePage(wd)
        favorite_page.go()
        self.assert_utag_data(wd, {self.EVENT_NAME: ev.EMPTY_FAVORITES})

class TestTealiumMobileGeneralPages(TestTealiumDesktopGeneralPages):
    user_agent = utils.UserAgents.IOS
    is_mobile = True

    @tags('tealium', 'mobile', 'listing')
    def test_page_home(self, wd):
        """
        Tested tealium tag page in home (mobile)
        """
        super(TestTealiumMobileGeneralPages, self).test_page_home()

    @tags('tealium', 'mobile', 'ad_not_found')
    def test_page_ad_not_found_404(self, wd):
        """
        Tested tealium tag page in ad not found (mobile)
        """
        super(TestTealiumMobileGeneralPages, self).test_page_ad_not_found_404()

    @tags('tealium', 'mobile', '404')
    def test_page_error_404(self, wd):
        """
        Tested tealium tag page in 404 (mobile)
        """
        super(TestTealiumMobileGeneralPages, self).test_page_error_404()
