# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.tealium import TealiumEventNames, TealiumGlobalVariables, TealiumEventNames, TealiumStoreVariables
from .tealium import TealiumBaseTest
from yapo.pages import desktop, stores
from yapo.utils import Categories as cg
from yapo import conf


class TestDesktopTealiumStores(TealiumBaseTest, TealiumGlobalVariables):
    """
    Tested utag_data of tealium in stores pages.
    """
    snaps = ['accounts', 'singlestore']

    utag_data_store = {
        TealiumStoreVariables.STORE_ADDRESS_NUMBER: '1313',
        TealiumStoreVariables.STORE_NAME: 'El gabacho de Moniacas',
        TealiumStoreVariables.STORE_WEBSITE: 'www.fuerzasdemoniacas.com',
        TealiumStoreVariables.STORE_PHONE: '666666666',
        TealiumStoreVariables.STORE_ADDRESS: 'Calle yolanda sultana',
        TealiumStoreVariables.STORE_INFO_TEXT: 'La descripción de la tinedita esta overrated',
        TealiumStoreVariables.STORE_EXPIRATION_DATE: '2014-10-10 11:41:27.774177',
        TealiumStoreVariables.STORE_PURCHASE_DATE: '2014-09-09 11:41:26.381784',
    }

    @tags('tealium', 'stores', 'desktop')
    def test_tealium_stores(self, wd):
        """
        Tested tealium tag stores_search_pages, STORE_SINGLE_PAGE and store_404 in store page (desktop).
        """
        self.assert_utag_data_store_search(wd)
        self.assert_utag_data_store_single(wd)
        self.assert_utag_data_store_404(wd)

    def assert_utag_data_store_search(self, wd):
        li = stores.Listing(wd)
        li.go()
        self.assert_utag_data(wd, {self.EVENT_NAME: TealiumEventNames.STORES_SEARCH_PAGES})

    def assert_utag_data_store_single(self, wd):
        detail = stores.Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        utag_data = {}
        utag_data.update(self.utag_data_store)
        utag_data.update({TealiumStoreVariables.STORE_URL: 'el_gabacho_de_moniacas'})
        utag_data.update({self.EVENT_NAME: TealiumEventNames.STORE_SINGLE_PAGE})
        self.assert_utag_data(wd, utag_data)

    def assert_utag_data_store_404(self, wd):
        detail = stores.Detail(wd)
        detail.go('store-not-found-yolo')
        utag_data = {}
        utag_data.update({self.EVENT_NAME: TealiumEventNames.STORE_404})
        self.assert_utag_data(wd, utag_data)

    def test_tealium_stores_manage(self, wd):
        """
        Tested tealium tag store_manage_page in store page (desktop).
        """
        login = desktop.Login(wd)
        edit_form = stores.EditForm(wd)
        dashboard = desktop.Dashboard(wd)
        login.login('many@ads.cl', '123123123')
        dashboard.link_store.click()
        edit_form.wait.until_visible('go_to_store')
        utag_data = {}
        utag_data.update(self.utag_data_store)
        utag_data.update({self.EVENT_NAME: TealiumEventNames.STORE_MANAGE_PAGE})
        utag_data.update({
            TealiumStoreVariables.STORE_ACTIVE: 'true',
            TealiumStoreVariables.STORE_URL: '{0}/tiendas/el-gabacho-de-moniacas'.format(conf.DESKTOP_URL)
        })

        self.assert_utag_data(wd, utag_data)

    def test_tealium_stores_info(self, wd):
        """
        Tested tealium tag store_info_page in store page (desktop).
        """
        buy_store = stores.BuyStore(wd)
        buy_store.go('prepaid3@blocket.se', '123123123')
        utag_data = {}
        utag_data.update({self.EVENT_NAME: TealiumEventNames.STORE_INFO_PAGE})
        self.assert_utag_data(wd, utag_data)
