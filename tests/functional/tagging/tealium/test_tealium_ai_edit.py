# -*- coding: latin-1 -*-
import unittest
from yapo.decorators import tags
from yapo.decorators import retries_function_exception
from yapo import utils
from yapo.pages.tealium import TealiumEventNames
from .tealium import TealiumAdEditPagesTest
from .tealium_utag_data_expected import TealiumUtagDataExpectedAi, TealiumUtagDataExpectedVf
from yapo.utils import trans, bconf_overwrite, flush_overwrite
from yapo.pages import generic, desktop
from yapo.pages.tealium import Tealium
import unittest

class TestDesktopTealiumTagPageAdEdit(TealiumUtagDataExpectedAi, TealiumUtagDataExpectedVf, TealiumAdEditPagesTest):
    """
    Tested utag_data of teleaium in ad edit desktop.
    """

    snaps = ['accounts', 'diff_maps']
    categories = [
        ('8000099', '1240', '123123123'), ('8000017', '2020', '123123123'), ('8000016', '2060', '123123123'), ('8000026', '3060', '123123123'),
        ('8000038', '4020', '123123123'), ('8000047', '7020', '123123123'),
        ('6394117', '7060', '123123123'),
    ]
    UTAG_DATA_EXPECTED_EDIT = 'utag_data_expected_ai'

    def _is_mobile(self):
        return self.__class__ == TestDesktopTealiumTagPageAdEdit

    @tags('tealium', 'edit', 'desktop')
    def test_tealium_edit_passwd(self, wd):
        """
        Tested tealium tag page ad_edit_password, ad_edit_password_error in some categories.
        """
        categories = [
            ('8000099', '1240', '123123123')
        ]
        for list_id, cat, passwd in categories:
            print(list_id, cat)
            utag_data = {self.EVENT_NAME: TealiumEventNames.AD_EDIT_PASSWORD}
            self.assert_utag_data_ad_edit_password(wd, list_id, utag_data)

            utag_data = {self.EVENT_NAME: TealiumEventNames.AD_EDIT_PASSWORD_ERROR}
            self.assert_utag_data_ad_edit_password_error(wd, list_id, utag_data)

class TestDesktopTealiumTagPageAdEdit2(TealiumUtagDataExpectedAi, TealiumAdEditPagesTest):
    """
    Tested utag_data of teleaium in ad edit preview and ad edit refused desktop.
    """

    snaps = ['accounts', 'diff_maps']
    categories = [
        ('8000099', '1240', '123123123', '140'), ('8000017', '2020', '123123123', '38'), ('8000016', '2060', '123123123', '37'), ('8000026', '3060', '123123123', '46'),
        ('8000038', '4020', '123123123', '58'), ('8000047', '7020', '123123123', '67'), ('6394117', '7060', '123123123', '11'), ('8000082', '8020', '123123123', '122')
    ]

    @tags('tealium', 'edit', 'desktop')
    @unittest.skip('Disabled because we are running an experiment (submit_preview)')
    def test_tealium_edit_preview(self, wd):
        """
        Tested tealium tag ad_edit_preview page in some categories.
        """
        categories = [
            ('8000099', '1240', '123123123'),
            ('8000017', '2020', '123123123'),
            ('8000016', '2060', '123123123'),
            ('8000026', '3060', '123123123'),
            ('8000038', '4020', '123123123'),
            ('8000047', '7020', '123123123'),
            ('6394117', '7060', '123123123'),
        ]
        for list_id, cat, passwd in categories:
            print(list_id, cat)
            utag_data = getattr(self, 'utag_data_expected_ai_{}'.format(cat))
            utag_data.pop(self.PUBLISH_DATE)
            utag_data.update({self.EVENT_NAME: TealiumEventNames.AD_EDIT_PREVIEW})

            edit_page, edit_password_page = desktop.AdEdit(wd), desktop.EditPasswordPage(wd)
            edit_page.go(list_id)
            edit_password_page.validate_password(passwd)
            self.go_preview_and_assert_utag_data(wd, edit_page, utag_data)

    @retries_function_exception(num_retries=3)
    def go_preview_and_assert_utag_data(self, wd, edit_page, utag_data):
        edit_page.preview.click()
        tealium = Tealium(wd)
        self.assertDictContainsSubset(
            {str(k):str(v) for k,v in utag_data.items()},
            tealium.utag_data(utag_data['event_name'], 'page')
        )

    def _refuse_ad_publish(self, ad_id):
        result_authenticate = trans("authenticate",
                                    username="dany",
                                    passwd="118ab5d614b5a8d4222fc7eee1609793e4014800",
                                    remote_addr="10.0.1.139")
        self.assertTrue(("token" in result_authenticate))
        self.assertEqual("TRANS_OK", result_authenticate['status'])

        result_review = trans("review",
                              ad_id=ad_id,
                              action_id="1",
                              action="refuse",
                              reason="20",
                              filter_name="all",
                              token=result_authenticate['token'],
                              remote_addr="10.0.1.139")
        self.assertEqual("TRANS_OK", result_review['status'])

class TestMobileTealiumTagPageAdEdit(TestDesktopTealiumTagPageAdEdit):
    """
    Tested utag_data of teleaium in ad edit desktop.
    """
    is_mobile = True
    user_agent = utils.UserAgents.IOS
    UTAG_DATA_EXPECTED_EDIT = 'utag_data_expected'
    categories = [
        ('8000099', '1240', '123123123'), ('8000017', '2020', '123123123'), ('8000016', '2060', '123123123'), ('8000026', '3060', '123123123'),
        ('8000038', '4020', '123123123'), ('8000047', '7020', '123123123'),
    ]

    @unittest.skip("Disabled because this functionality is not implemented in mobile ad edit yet")
    @tags('tealium', 'edit', 'mobile')
    def test_tealium_edit(self, wd):
        """
        Tested tealium tag page ad_edit_ad in some categories (mobile).
        """
        super(TestMobileTealiumTagPageAdEdit, self).test_tealium_edit()

    @tags('tealium', 'edit', 'mobile')
    def test_tealium_edit_passwd(self, wd):
        """
        Tested tealium tag page ad_edit_password, ad_edit_password_error in some categories (mobile).
        """
        super(TestMobileTealiumTagPageAdEdit, self).test_tealium_edit_passwd()

    @unittest.skip("Disabled because this functionality is not implemented in mobile ad edit yet")
    @tags('tealium', 'edit', 'mobile')
    def test_tealium_edit_submit(self, wd):
        """
        Tested tealium tag page ad_edit_submit in some categories (mobile).
        """
        super(TestMobileTealiumTagPageAdEdit, self).test_tealium_edit_submit()

