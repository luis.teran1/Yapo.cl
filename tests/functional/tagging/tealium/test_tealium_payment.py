# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.tealium import TealiumEventNames, TealiumPurshaseVariables
from .tealium import TealiumBaseTest
from yapo.pages import desktop, simulator
from yapo.utils import Products

class TestTealiumDesktopPayment(TealiumBaseTest, TealiumPurshaseVariables):
    snaps = ['accounts', 'diff_pack_cars1']

    account_email = 'prepaid5@blocket.se'
    account_password = '123123123'

    LIST_IDS = {6000666: [Products.LABEL, Products.GALLERY],
                3456789: [Products.GALLERY],
                6000667: [Products.GALLERY],
               }
    UTAG_DATA_PURSHASE = {TealiumPurshaseVariables.PURCHASE:
                          "[{'subject':'Pesas muy grandes','product': 'vitrina_7_d�as','price': '4150'},"\
                           "{'subject':'Pesas muy grandes','product': 'etiqueta','price': '2100'},"\
                           "{'subject':'Departamento Tarapac�','product': 'vitrina_7_d�as','price': '10000'},"\
                           "{'subject':'Race car','product': 'vitrina_7_d�as','price': '7000'}]"
                         }

    @tags('tealium', 'payment', 'desktop')
    def test_tealium_purchase_fail(self, wd):
        self._do_loggin(wd)
        self._go_to_summary_page(wd)
        sp = desktop.SummaryPayment(wd)
        sp.press_link_to_pay_products()
        paysim = simulator.Paysim(wd)
        paysim.paysim_populate(order='11111')
        paysim.pay_accept()

        self.assert_utag_data_payment_fail(wd)

    def assert_utag_summary_page(self, wd):
        self.UTAG_DATA_PURSHASE.update({self.EVENT_NAME: TealiumEventNames.SUMMARY_PAGE})

        self.assert_utag_data(wd, self.UTAG_DATA_PURSHASE)

    def assert_utag_purchase_confirmation(self, wd):
        utag_data = {self.EVENT_NAME: TealiumEventNames.PURCHASE_CONFIRMATION,
                    self.OPERATION_TYPE: 'Venta',
                    self.PAYMENT_TYPE: 'credit',
                    self.INSTALLMENTS_TYPE: 'VN',
                    self.INSTALLMENTS_NUMBER: '1',
                    self.PRICE: '23250'
                    }
        self.UTAG_DATA_PURSHASE.update(utag_data)

        self.assert_utag_data(wd, self.UTAG_DATA_PURSHASE)

    def assert_utag_data_payment_fail(self, wd):
        utag_data = {self.EVENT_NAME: TealiumEventNames.PAYMENT_FAIL,
                     self.PAYMENT_MESSAGE_ERROR: 'ERROR_TRANSACTION_FAIL_MESSAGE',
                     self.PAYMENT_TITLE_ERROR: 'ERROR_SESSION_EXPIRED_OOPS',
                    }
        self.assert_utag_data(wd, utag_data)

    def _go_to_summary_page(self, wd):
        dashboard_myads = desktop.DashboardMyAds(wd)
        dashboard_myads.select_products_by_ids(self.LIST_IDS)
        dashboard_myads.checkout_button.click()
        dashboard_myads._driver.refresh()

