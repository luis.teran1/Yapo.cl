# -*- coding: latin-1 -*-
from yapo.pages.tealium import TealiumAdVariables, TealiumEventNames, TealiumGlobalVariables, TealiumAiVariables
from yapo.utils import Categories as cg

class TealiumUtagDataExpected(TealiumAdVariables, TealiumGlobalVariables):
    """
    This class defines the expected variables by category in an ad.
    """

    @property
    def utag_data_expected_1240(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Las Condes', self.REGION_LEVEL3_ID: '315',
            self.CATEGORY_LEVEL1_ID: cg.INMUEBLES, self.CATEGORY_LEVEL1: 'Inmuebles',
            self.CATEGORY_LEVEL2_ID: cg.ARRIENDO, self.CATEGORY_LEVEL2: 'Arriendo',
            self.PRICE: '321300', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Ad with map with street Mar�ano S�nchez Fontecilla',
            self.AD_ID: '8000099', self.PUBLISH_DATE: '2015-05-28 11:21:21',
            self.NUM_PICTURES: '0', self.ROOMS: '4',
            self.SURFACE: '2323', self.SURFACE_UNITS: 'm�',
            self.DESCRIPTION: 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)',
            self.CONDOMINIO: '3213123', self.PARKING: '31',
            self.GEOPOSITION: '-33.4256935,-70.590889', self.ADDRESS_NUMBER: '1001'
    }

    @property
    def utag_data_expected_2020(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Melipilla', self.REGION_LEVEL3_ID: '322',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Veh�culos',
            self.CATEGORY_LEVEL2_ID: cg.AUTOS_CAMIONETAS_Y_4X4, self.CATEGORY_LEVEL2: 'Autos, camionetas y 4x4',
            self.PRICE: '1750000', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Suzuki Maruti 800cc 2003',
            self.AD_ID: '8000017', self.PUBLISH_DATE: '2013-07-30 12:22:09',
            self.NUM_PICTURES: '0', self.AD_TYPE: 'Vendo',
            self.DESCRIPTION: 'Vendo Susuki Maruti motor 800cc color Gris 5 puertas a�o 2003 3� due�o en excelentes '+\
            'condiciones. Mantenciones al d�a, papeles al d�a, sin partes ni deudas. Precio conversable.',
            self.BRAND: 'SUZUKI', self.MODEL: 'MARUTI',
            self.VERSION: 'MARUTI', self.CARTYPE: 'Autom�vil',
            self.YEAR: '2003', self.FUEL: 'Bencina',
            self.TRANSMISSION: 'Manual', self.KM: '94000',
            self.KM_UNITS: 'km'}

    @property
    def utag_data_expected_2060(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Maip�', self.REGION_LEVEL3_ID: '320',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Veh�culos',
            self.CATEGORY_LEVEL2_ID: cg.MOTOS, self.CATEGORY_LEVEL2: 'Motos',
            self.PRICE: '1550000', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Moto Wolken xy 400cc,',
            self.AD_ID: '8000016', self.PUBLISH_DATE: '2013-07-30 12:22:09',
            self.NUM_PICTURES: '0', self.AD_TYPE: 'Vendo',
            self.DESCRIPTION: 'Vendo mi joyita Wolken xy 400cc,  a�o 2009 $1.550.000  esta impecable apenas 14000. Con toda su documentaci�n '+\
            'al d�a asta 2014 sin deudas alguna La moto tienen 6 cambios tiene mucha fuerza. frenos de disco. delantero y trasero '+\
            'Transferencia inmediata. se puede ver sin compromiso solo interesados conversableVendo mi joyita Wolken xy 400cc,  a�o 2009 $1.550.000',
            self.YEAR: '2009', self.CUBICCMS: '500 cc',
            self.KM_UNITS: 'km', self.CUBICCMS_UNITS: 'cc',
            self.KM: '14000'}

    @property
    def utag_data_expected_3060(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Pedro Aguirre Cerda', self.REGION_LEVEL3_ID: '326',
            self.CATEGORY_LEVEL1_ID: cg.COMPUTADORES_Y_ELECTRONICA, self.CATEGORY_LEVEL1: 'Computadores & electr�nica',
            self.CATEGORY_LEVEL2_ID: cg.CELULARES_TELEFONOS_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Celulares, tel�fonos y accesorios',
            self.PRICE: '220000', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Samsung Galaxi SIII (GT19300)',
            self.AD_ID: '8000026', self.PUBLISH_DATE: '2013-07-30 12:22:09',
            self.NUM_PICTURES: '0',
            self.DESCRIPTION: 'Posee una pantalla Super AMOLED HD 720p de 4.8 pulgadas, procesador Exynos 4 Quad de cuatro n�cleos a 1.4GHz, 1GB '+\
            'de RAM, 16GB de memoria externa, ranura microSD y corre Android 4.0 Ice Cream Sandwich con la interfaz TouchWiz, con solo 6 meses de'+\
            ' uso, no tiene cargador ni manos libres, viene en su caja junto con el manual, ENTEL',
            self.MEMORY: '24'}

    @property
    def utag_data_expected_4020(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Paine', self.REGION_LEVEL3_ID: '325',
            self.CATEGORY_LEVEL1_ID: cg.MODA_BELLEZA_SALUD, self.CATEGORY_LEVEL1: 'Moda, calzado, belleza y salud',
            self.CATEGORY_LEVEL2_ID: cg.MODA_VESTUARIO_Y_CALZADO, self.CATEGORY_LEVEL2: 'Moda y vestuario',
            self.PRICE: '4500', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Mario y Luigi crochet',
            self.AD_ID: '8000038', self.PUBLISH_DATE: '2013-07-30 12:22:09',
            self.NUM_PICTURES: '0',
            self.DESCRIPTION: 'Tejidos a crochet .$ 4500 retiro en mi casa y $ 5000 te lo llevo a domicilio dentro de la ciudad.',
            self.GENDER: 'Unisex', self.CONDITION: 'Nuevo'}

    @property
    def utag_data_expected_7020(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Huechuraba', self.REGION_LEVEL3_ID: '306',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.OFERTAS_DE_EMPLEO, self.CATEGORY_LEVEL2: 'Ofertas de empleo',
            self.PRICE: '300000', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Secretaria Recepcionista',
            self.AD_ID: '8000047', self.PUBLISH_DATE: '2013-07-30 12:22:09',
            self.NUM_PICTURES: '0',
            self.DESCRIPTION: 'Climaroca, empresa chilena de servicios de ingenier�a t�rmica busca Asistente de Servicio para hacerse cargo de la '+\
            'atenci�n de clientes en relaci�n a cotizaciones, entrega de informaci�n de servicios y en general, manejo de todas las situaciones '+\
            'relacionadas al servicio y soporte comercial.  Principales actividades: -Atenci�n telef�nica y/o presencial de clientes -Atenci�n de '+\
            'consultas v�a e-mail, entregando orientaci�n general de los servicios -Coordinaci�n de visitas, agendamiento de reuniones -Contacto con'+\
            ' proveedores y contratistas -Administraci�n de registros y documentos de gerencia -Administraci�n de caja chica',
            self.JOB_CATEGORY: 'Profesional / T�cnico|Otros'}

    @property
    def utag_data_expected_7060(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'XI Ais�n', self.REGION_LEVEL2_ID: '13',
            self.REGION_LEVEL3: 'Cochrane', self.REGION_LEVEL3_ID: '277',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.SERVICIOS, self.CATEGORY_LEVEL2: 'Servicios',
            self.PRICE: '12000', self.CURRENCY: 'peso',
            self.AD_TITLE: 'Peluquera a domicilio',
            self.AD_ID: '6394117', self.PUBLISH_DATE: '2006-04-07 11:21:31',
            self.NUM_PICTURES: '0',
            self.DESCRIPTION: 'Voy, corto el pelo, me pagas y m voy....',
            self.SERVICE_CATEGORY: 'Salud/Belleza'}

    @property
    def utag_data_expected_8020(self):
        return {
            self.EVENT_NAME: TealiumEventNames.AD_VIEW,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: 'Regi�n Metropolitana', self.REGION_LEVEL2_ID: '15',
            self.REGION_LEVEL3: 'Pudahuel', self.REGION_LEVEL3_ID: '331',
            self.CATEGORY_LEVEL1_ID: cg.OTROS, self.CATEGORY_LEVEL1: 'Otros',
            self.CATEGORY_LEVEL2_ID: cg.OTROS_PRODUCTOS, self.CATEGORY_LEVEL2: 'Otros productos',
            self.AD_TITLE: 'Test 07',
            self.AD_ID: '8000082', self.PUBLISH_DATE: '2014-03-31 17:22:15',
            self.NUM_PICTURES: '1',
            self.DESCRIPTION: 'asdad asdasdaasd'}

    @property
    def utag_data_expected_global(self):
        return {
        self.USER_ID: 'e6c3dd630428fd54834172b8fd2735fed9416da4',
        self.ENVIRONMENT: 'dev', self.LANGUAGE: 'es', self.LANGUAGE_ID: '1',
        self.USER_ROLE_ID: '1', self.USER_ROLE: 'Persona',
        self.USER_REGION_LEVEL1: 'Chile', self.USER_REGION_LEVEL1_ID: '0',
        self.USER_REGION_LEVEL2: 'V Valpara�so', self.USER_REGION_LEVEL2_ID: '6',
        self.USER_REGION_LEVEL3: 'Zapallar', self.USER_REGION_LEVEL3_ID: '82',
        self.USER_EMAIL: 'with_account_and_commune@maps.cl'}

class TealiumUtagDataExpectedAi(TealiumAiVariables, TealiumUtagDataExpected):
    """
    This class defines the expected variables by category in an ad in ai.
    """

    @property
    def utag_data_expected_ai_1240(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_1240
        utag_data.update({
            self.AD_TITLE: 'Ad with map with street Mar&iacute;ano S&aacute;nchez Fontecilla',
            self.DESCRIPTION: 'ad with map with street Mar&iacute;ano S&aacute;nchez Fontecilla (user without account)',
        })
        return utag_data

    @property
    def utag_data_expected_ai_2020(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_2020
        utag_data.update({
            self.AD_TITLE: 'Suzuki Maruti 800cc 2003',
            self.DESCRIPTION: 'Vendo Susuki Maruti motor 800cc color Gris 5 puertas a&ntilde;o 2003 3&ordm; due&ntilde;o en '+\
            'excelentes condiciones. Mantenciones al d&iacute;a, papeles al d&iacute;a, sin partes ni deudas. Precio conversable.'})
        return utag_data

    @property
    def utag_data_expected_ai_2060(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_2060
        utag_data.update({
            self.AD_TITLE: 'Moto Wolken xy 400cc,',
            self.DESCRIPTION: 'Vendo mi joyita Wolken xy 400cc,  a&ntilde;o 2009 $1.550.000  esta impecable apenas 14000. Con toda '+\
            'su documentaci&oacute;n al d&iacute;a asta 2014 sin deudas alguna La moto tienen 6 cambios tiene mucha fuerza. frenos de '+\
            'disco. delantero y trasero Transferencia inmediata. se puede ver sin compromiso solo interesados conversableVendo mi joyita'+\
            ' Wolken xy 400cc,  a&ntilde;o 2009 $1.550.000'
        })
        return utag_data

    @property
    def utag_data_expected_ai_3060(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_3060
        utag_data.update({
            self.AD_TITLE: 'Samsung Galaxi SIII (GT19300)',
            self.DESCRIPTION: 'Posee una pantalla Super AMOLED HD 720p de 4.8 pulgadas, procesador Exynos 4 Quad de cuatro n&uacute;cleos a 1.4GHz'+\
            ', 1GB de RAM, 16GB de memoria externa, ranura microSD y corre Android 4.0 Ice Cream Sandwich con la interfaz TouchWiz, con solo 6 meses'+\
            ' de uso, no tiene cargador ni manos libres, viene en su caja junto con el manual, ENTEL',
        })
        return utag_data

    @property
    def utag_data_expected_ai_4020(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_4020
        utag_data.update({
            self.AD_TITLE: 'Mario y Luigi crochet',
            self.DESCRIPTION: 'Tejidos a crochet .$ 4500 retiro en mi casa y $ 5000 te lo llevo a domicilio dentro de la ciudad.',
        })
        return utag_data

    @property
    def utag_data_expected_ai_7020(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_7020
        utag_data.update({
            self.AD_TITLE: 'Secretaria Recepcionista',
            self.DESCRIPTION: 'Climaroca, empresa chilena de servicios de ingenier&iacute;a t&eacute;rmica busca Asistente de Servicio para'+\
            ' hacerse cargo de la atenci&oacute;n de clientes en relaci&oacute;n a cotizaciones, entrega de informaci&oacute;n de servicios y '+\
            'en general, manejo de todas las situaciones relacionadas al servicio y soporte comercial.  Principales actividades: -Atenci&oacute;n'+\
            ' telef&oacute;nica y/o presencial de clientes -Atenci&oacute;n de consultas v&iacute;a e-mail, entregando orientaci&oacute;n general de'+\
            ' los servicios -Coordinaci&oacute;n de visitas, agendamiento de reuniones -Contacto con proveedores y contratistas -Administraci&oacute;n'+\
            ' de registros y documentos de gerencia -Administraci&oacute;n de caja chica',
        })
        return utag_data

    @property
    def utag_data_expected_ai_7060(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_7060
        return utag_data

    @property
    def utag_data_expected_ai_8020(self):
        utag_data = super(TealiumUtagDataExpectedAi, self).utag_data_expected_8020
        return utag_data

    @property
    def utag_data_expected_ai_global(self):
        return {
        self.USER_ID: 'e6c3dd630428fd54834172b8fd2735fed9416da4',
        self.ENVIRONMENT: 'dev', self.LANGUAGE: 'es', self.LANGUAGE_ID: '1',
        self.USER_ROLE_ID: '1', self.USER_ROLE: 'Persona',
        self.USER_REGION_LEVEL1: 'Chile', self.USER_REGION_LEVEL1_ID: '0',
        self.USER_REGION_LEVEL2: 'V Valpara�so', self.USER_REGION_LEVEL2_ID: '6',
        self.USER_REGION_LEVEL3: 'Zapallar', self.USER_REGION_LEVEL3_ID: '82',
        self.USER_EMAIL: 'with_account_and_commune@maps.cl'}


class TealiumUtagDataExpectedVf(TealiumAiVariables, TealiumUtagDataExpected):
    """
    This class defines the expected variables by category in an ad.
    """

    @property
    def utag_data_expected_vf_1240(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_1240
        utag_data.update({
            self.AD_TITLE: 'Ad with map with street Mar�ano S�nchez Fontecilla',
            self.DESCRIPTION: 'ad with map with street Mar�ano S�nchez Fontecilla (user without account)'
        })
        return utag_data

    @property
    def utag_data_expected_vf_2020(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_2020
        utag_data.update({
            self.AD_TITLE: 'Suzuki Maruti 800cc 2003',
            self.DESCRIPTION: 'Vendo Susuki Maruti motor 800cc color Gris 5 puertas  a�o 2003 3� due�o en excelentes condiciones.  '+\
            'Mantenciones al d�a, papeles al d�a, sin partes ni deudas. Precio conversable.'
        })
        return utag_data

    @property
    def utag_data_expected_vf_2060(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_2060
        utag_data.update({
            self.AD_TITLE: 'Moto Wolken xy 400cc,',
            self.DESCRIPTION: 'Vendo mi joyita Wolken xy 400cc,   a�o 2009 $1.550.000    esta impecable apenas 14000. Con toda su documentaci�n'+\
            ' al d�a asta 2014 sin deudas alguna La moto tienen 6 cambios tiene mucha fuerza. frenos de disco. delantero y trasero Transferencia'+\
            ' inmediata. se puede ver sin compromiso solo interesados conversableVendo mi joyita Wolken xy 400cc,   a�o 2009 $1.550.000',
        })
        return utag_data

    @property
    def utag_data_expected_vf_3060(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_3060
        return utag_data

    @property
    def utag_data_expected_vf_4020(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_4020
        return utag_data

    @property
    def utag_data_expected_vf_7020(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_7020
        #job category is not available in vf
        utag_data.pop(self.JOB_CATEGORY)
        utag_data.update({
            self.AD_TITLE: 'Secretaria Recepcionista',
            self.DESCRIPTION: 'Climaroca, empresa chilena de servicios de ingenier�a t�rmica busca Asistente de Servicio para hacerse cargo de la '+\
            'atenci�n de clientes en relaci�n a cotizaciones, entrega de informaci�n de servicios y en general, manejo de todas las situaciones relacionadas'+\
            ' al servicio y soporte comercial.    Principales actividades:  -Atenci�n telef�nica y/o presencial de clientes  -Atenci�n de consultas v�a e-mail,'+\
            ' entregando orientaci�n general de los servicios  -Coordinaci�n de visitas, agendamiento de reuniones  -Contacto con proveedores y contratistas  '+\
            '-Administraci�n de registros y documentos de gerencia  -Administraci�n de caja chica',
        })
        return utag_data

    @property
    def utag_data_expected_vf_7060(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_7060
        #service category is not available in vf
        utag_data.pop(self.SERVICE_CATEGORY)
        return utag_data

    @property
    def utag_data_expected_vf_8020(self):
        utag_data = super(TealiumUtagDataExpectedVf, self).utag_data_expected_8020
        return utag_data
