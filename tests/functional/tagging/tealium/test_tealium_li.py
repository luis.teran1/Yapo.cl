# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils
from yapo.pages.desktop_list import AdListDesktop
from yapo.pages.tealium import TealiumEventNames as ev
from .tealium import TealiumListingTest
from yapo.utils import Categories as cg

class TealiumUtagDataExpected(TealiumListingTest):
    @property
    def data_1220(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "II Antofagasta", self.REGION_LEVEL2_ID: '3',
            self.CATEGORY_LEVEL1_ID: cg.INMUEBLES, self.CATEGORY_LEVEL1: 'Inmuebles',
            self.CATEGORY_LEVEL2_ID: cg.VENDO, self.CATEGORY_LEVEL2: 'Vendo',
            self.PRICE_MIN: '$ 750.000', self.PRICE_MAX: '$ 6.000.000',
            self.ROOMS_MIN: '1', self.ROOMS_MAX: '3',
            self.PARKING: '2'}

        params_to_fill = {
            'category': cg.VENDO, 'garage_spaces': '2',
            'min_price': '2', 'max_price': '6',
            'min_rooms': '1', 'max_rooms': '3',
            'min_size': '1', 'max_size': '3'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_1240(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "II Antofagasta", self.REGION_LEVEL2_ID: '3',
            self.CATEGORY_LEVEL1_ID: cg.INMUEBLES, self.CATEGORY_LEVEL1: 'Inmuebles',
            self.CATEGORY_LEVEL2_ID: cg.VENDO, self.CATEGORY_LEVEL2: 'Vendo',
            self.PRICE_MIN: '$ 750.000', self.PRICE_MAX: '$ 6.000.000',
            self.ROOMS_MIN: '1', self.ROOMS_MAX: '3',
        }

        params_to_fill = {
            'category': cg.VENDO, 'garage_spaces': '2',
            'min_price': '2', 'max_price': '6',
            'min_rooms': '1', 'max_rooms': '3',
            'min_size': '1', 'max_size': '3'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_1260(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "II Antofagasta", self.REGION_LEVEL2_ID: '3',
            self.CATEGORY_LEVEL1_ID: cg.INMUEBLES, self.CATEGORY_LEVEL1: 'Inmuebles',
            self.CATEGORY_LEVEL2_ID: cg.ARRIENDO_TEMPORADA, self.CATEGORY_LEVEL2: 'Arriendo de temporada',
            self.PRICE_MIN: '$ 5.000', self.PRICE_MAX: '$ 25.000', self.PARKING: '2'
        }

        params_to_fill = {
            'category': cg.ARRIENDO_TEMPORADA, 'garage_spaces': '2',
            'min_price': '2', 'max_price': '6',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_2020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.AUTOS_CAMIONETAS_Y_4X4, self.CATEGORY_LEVEL2: 'Autos, camionetas y 4x4',
            self.PRICE_MIN: '$ 0', self.PRICE_MAX: '$ 3.000.000',
            'brand': 'ACADIAN', 'model': 'BEAUMONT',
            'year_min': '1995', 'year_max': '2005',
            'km_min': '80000', 'km_max': '180000',
            'fuel': 'Bencina', 'cartype': 'Automóvil',
            'transmission': 'Manual'
        }

        params_to_fill = {
            'category': cg.AUTOS_CAMIONETAS_Y_4X4,
            'min_price': '1', 'max_price': '8',
            'brand': '1', 'model': '1',
            'min_regdate': '1995', 'max_regdate': '2005',
            'mileage_min': '80000', 'mileage_max': '180000',
            'fuel': '1', 'cartype': '1', 'gearbox': '1'
        }
        return (params_to_fill, utag_data_expected)
    @property
    def data_2040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.CAMIONES_Y_FURGONES, self.CATEGORY_LEVEL2: 'Buses, camiones y furgones',
            self.PRICE_MIN: '$ 0', self.PRICE_MAX: '$ 3.000.000',
            'year_min': '1995', 'year_max': '2005',
            'km_min': '80000', 'km_max': '180000',
            'transmission': 'Manual', 'fuel': 'Bencina'
        }

        params_to_fill = {
            'category': cg.CAMIONES_Y_FURGONES,
            'min_price': '1', 'max_price': '8',
            'min_regdate': '1995', 'max_regdate': '2005',
            'mileage_min': '80000', 'mileage_max': '180000',
            'fuel': '1', 'cartype': '1', 'gearbox': '1'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_2060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.MOTOS, self.CATEGORY_LEVEL2: 'Motos',
            self.PRICE_MIN: '$ 0', self.PRICE_MAX: '$ 3.000.000',
            'year_min': '1995', 'year_max': '2005',
            'km_min': '80000', 'km_max': '180000',
            'cubiccms_min': '50', 'cubiccms_max': '200',
        }

        params_to_fill = {
            'category': cg.MOTOS,
            'min_price': '1', 'max_price': '8',
            'min_regdate': '1995', 'max_regdate': '2005',
            'mileage_min': '80000', 'mileage_max': '180000',
            'ccs': '50', 'cce': '200',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_2080(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.BARCOS_LANCHAS_Y_AVIONES, self.CATEGORY_LEVEL2: 'Barcos, lanchas y aviones',
            self.PRICE_MIN: '$ 100.000', self.PRICE_MAX: '$ 750.000',
        }

        params_to_fill = {
            'category': cg.BARCOS_LANCHAS_Y_AVIONES,
            'min_price': '1', 'max_price': '8',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_2100(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.ACCESORIOS_Y_PIEZAS_PARA_VEHICULOS, self.CATEGORY_LEVEL2: 'Accesorios y piezas para vehículos',
            self.PRICE_MIN: '$ 100.000', self.PRICE_MAX: '$ 750.000',
        }

        params_to_fill = {
            'category': cg.ACCESORIOS_Y_PIEZAS_PARA_VEHICULOS,
            'min_price': '1', 'max_price': '8',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_2120(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.VEHICULOS, self.CATEGORY_LEVEL1: 'Vehículos',
            self.CATEGORY_LEVEL2_ID: cg.OTROS_VEHICULOS, self.CATEGORY_LEVEL2: 'Otros vehículos',
            self.PRICE_MIN: '$ 100.000', self.PRICE_MAX: '$ 750.000',
        }

        params_to_fill = {
            'category': cg.OTROS_VEHICULOS,
            'min_price': '1', 'max_price': '8',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_5020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.HOGAR, self.CATEGORY_LEVEL1: 'Hogar',
            self.CATEGORY_LEVEL2_ID: cg.MUEBLES, self.CATEGORY_LEVEL2: 'Muebles',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.MUEBLES,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_5040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.HOGAR, self.CATEGORY_LEVEL1: 'Hogar',
            self.CATEGORY_LEVEL2_ID: cg.ELECTRODOMESTICOS, self.CATEGORY_LEVEL2: 'Electrodomésticos',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.ELECTRODOMESTICOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_5060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.HOGAR, self.CATEGORY_LEVEL1: 'Hogar',
            self.CATEGORY_LEVEL2_ID: cg.JARDIN_Y_HERRAMIENTAS, self.CATEGORY_LEVEL2: 'Jardín, herramientas y exteriores',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.JARDIN_Y_HERRAMIENTAS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_5160(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.HOGAR, self.CATEGORY_LEVEL1: 'Hogar',
            self.CATEGORY_LEVEL2_ID: cg.ARTICULOS_DEL_HOGAR, self.CATEGORY_LEVEL2: 'Otros artículos del hogar',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.ARTICULOS_DEL_HOGAR,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_4020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.MODA_BELLEZA_SALUD, self.CATEGORY_LEVEL1: 'Moda, calzado, belleza y salud',
            self.CATEGORY_LEVEL2_ID: cg.MODA_VESTUARIO_Y_CALZADO, self.CATEGORY_LEVEL2: 'Moda y vestuario',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.GENDER: 'Femenino', self.CONDITION: 'Usado'
        }

        params_to_fill = {
            'category': cg.MODA_VESTUARIO_Y_CALZADO,
            'min_price': '1', 'max_price': '5',
            'condition': '2','gender': '1'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_4040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.MODA_BELLEZA_SALUD, self.CATEGORY_LEVEL1: 'Moda, calzado, belleza y salud',
            self.CATEGORY_LEVEL2_ID: cg.BOLSOS_BISUTERIA_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Bolsos, bisutería y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.GENDER: 'Masculino', self.CONDITION: 'Nuevo'
        }

        params_to_fill = {
            'category': cg.BOLSOS_BISUTERIA_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
            'condition': '1','gender': '2'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_4060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.MODA_BELLEZA_SALUD, self.CATEGORY_LEVEL1: 'Moda, calzado, belleza y salud',
            self.CATEGORY_LEVEL2_ID: cg.SALUD_Y_BELLEZA, self.CATEGORY_LEVEL2: 'Salud y belleza',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.GENDER: 'Masculino'
        }

        params_to_fill = {
            'category': cg.SALUD_Y_BELLEZA,
            'min_price': '1', 'max_price': '5',
            'gender': '2'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_9020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.FUTURA_MAMA_BEBES_Y_NINOS, self.CATEGORY_LEVEL1: 'Futura mamá, bebés y niños',
            self.CATEGORY_LEVEL2_ID: cg.VESTUARIO_FUTURA_MAMA_Y_NINOS, self.CATEGORY_LEVEL2: 'Vestuario futura mamá y niños',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.CONDITION: 'Nuevo'
        }

        params_to_fill = {
            'category': cg.VESTUARIO_FUTURA_MAMA_Y_NINOS,
            'min_price': '1', 'max_price': '5',
            'condition': '1'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_9040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.FUTURA_MAMA_BEBES_Y_NINOS, self.CATEGORY_LEVEL1: 'Futura mamá, bebés y niños',
            self.CATEGORY_LEVEL2_ID: cg.JUGUETES, self.CATEGORY_LEVEL2: 'Juguetes',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.CONDITION: 'Nuevo'
        }

        params_to_fill = {
            'category': cg.JUGUETES,
            'min_price': '1', 'max_price': '5',
            'condition': '1'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_9060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.FUTURA_MAMA_BEBES_Y_NINOS, self.CATEGORY_LEVEL1: 'Futura mamá, bebés y niños',
            self.CATEGORY_LEVEL2_ID: cg.COCHES_Y_OTROS_ARTICULOS, self.CATEGORY_LEVEL2: 'Coches y artículos infantiles',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
            self.CONDITION: 'Nuevo'
        }

        params_to_fill = {
            'category': cg.COCHES_Y_OTROS_ARTICULOS,
            'min_price': '1', 'max_price': '5',
            'condition': '1'
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6140(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.ANIMALES_Y_SUS_ACCESORIOS, self.CATEGORY_LEVEL2: 'Animales y sus accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.ANIMALES_Y_SUS_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.DEPORTES_GIMNASIA_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Deportes, gimnasia y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.DEPORTES_GIMNASIA_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.BICICLETAS_CICLISMO_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Bicicletas, ciclismo y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.BICICLETAS_CICLISMO_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6180(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.HOBBIES_Y_OUTDOOR, self.CATEGORY_LEVEL2: 'Hobbies y outdoor',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.HOBBIES_Y_OUTDOOR,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6080(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.INSTRUMENTOS_MUSICALES_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Instrumentos musicales y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.INSTRUMENTOS_MUSICALES_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6100(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.MUSICA_Y_PELICULAS, self.CATEGORY_LEVEL2: 'Música y películas (DVDs, etc.)',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.MUSICA_Y_PELICULAS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6120(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.LIBROS_Y_REVISTAS, self.CATEGORY_LEVEL2: 'Libros y revistas',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.LIBROS_Y_REVISTAS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_6160(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.TIEMPO_LIBRE, self.CATEGORY_LEVEL1: 'Tiempo libre',
            self.CATEGORY_LEVEL2_ID: cg.ARTE_ANTIGUEDADES_Y_COLECCIONES, self.CATEGORY_LEVEL2: 'Arte, antigüedades y colecciones',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.ARTE_ANTIGUEDADES_Y_COLECCIONES,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_3040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.COMPUTADORES_Y_ELECTRONICA, self.CATEGORY_LEVEL1: 'Computadores & electrónica',
            self.CATEGORY_LEVEL2_ID: cg.COMPUTADORES_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Computadores y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.COMPUTADORES_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_3060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.COMPUTADORES_Y_ELECTRONICA, self.CATEGORY_LEVEL1: 'Computadores & electrónica',
            self.CATEGORY_LEVEL2_ID: cg.CELULARES_TELEFONOS_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Celulares, teléfonos y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.CELULARES_TELEFONOS_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_3020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.COMPUTADORES_Y_ELECTRONICA, self.CATEGORY_LEVEL1: 'Computadores & electrónica',
            self.CATEGORY_LEVEL2_ID: cg.CONSOLAS_VIDEOJUEGOS_Y_ACCESORIOS, self.CATEGORY_LEVEL2: 'Consolas, videojuegos y accesorios',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.CONSOLAS_VIDEOJUEGOS_Y_ACCESORIOS,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_3080(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.COMPUTADORES_Y_ELECTRONICA, self.CATEGORY_LEVEL1: 'Computadores & electrónica',
            self.CATEGORY_LEVEL2_ID: cg.AUDIO_TV_VIDEO_Y_FOTOGRAFIA, self.CATEGORY_LEVEL2: 'Audio, TV, video y fotografía',
            self.PRICE_MIN: '$ 10.000', self.PRICE_MAX: '$ 100.000',
        }

        params_to_fill = {
            'category': cg.AUDIO_TV_VIDEO_Y_FOTOGRAFIA,
            'min_price': '1', 'max_price': '5',
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_7020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.OFERTAS_DE_EMPLEO, self.CATEGORY_LEVEL2: 'Ofertas de empleo',
            self.JOB_CATEGORY: 'Administrativo / Secretariado / Finanzas|Telecomunicaciones / Informática / Multimedia|Banca / Seguros / Consultoría / Jurídica'
        }

        params_to_fill = {
            'category': cg.OFERTAS_DE_EMPLEO,
            'job_category_link': 1, 'job_category_1': 1,
            'job_category_5': 1, 'job_category_7': 1
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_7040(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.BUSCO_EMPLEO, self.CATEGORY_LEVEL2: 'Busco empleo',
            self.JOB_CATEGORY: 'Administrativo / Secretariado / Finanzas|Telecomunicaciones / Informática / Multimedia|Banca / Seguros / Consultoría / Jurídica'
        }

        params_to_fill = {
            'category': cg.BUSCO_EMPLEO,
            'job_category_link': 1, 'job_category_1': 1,
            'job_category_5': 1, 'job_category_7': 1
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_7060(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.SERVICIOS, self.CATEGORY_LEVEL2: 'Servicios',
            self.SERVICE_CATEGORY: 'Servicios domésticos y alimenticios|Construcción / Reparación|Informática'
        }

        params_to_fill = {
            'category': cg.SERVICIOS, 'service_type_1': 1,
            'service_type_5': 1, 'service_type_7': 1
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_7080(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.SERVICIOS_NEGOCIOS_Y_EMPLEO, self.CATEGORY_LEVEL1: 'Servicios, negocios y empleo',
            self.CATEGORY_LEVEL2_ID: cg.NEGOCIOS_MAQUINARIA_Y_CONSTRUCCION, self.CATEGORY_LEVEL2: 'Negocios, maquinaria y construcción',
        }

        params_to_fill = {
            'category': cg.NEGOCIOS_MAQUINARIA_Y_CONSTRUCCION,
        }
        return (params_to_fill, utag_data_expected)

    @property
    def data_8020(self):
        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0', self.REGION_LEVEL1: 'Chile',
            self.REGION_LEVEL2: "IV Coquimbo", self.REGION_LEVEL2_ID: '5',
            self.CATEGORY_LEVEL1_ID: cg.OTROS, self.CATEGORY_LEVEL1: 'Otros',
            self.CATEGORY_LEVEL2_ID: cg.OTROS_PRODUCTOS, self.CATEGORY_LEVEL2: 'Otros productos',
        }

        params_to_fill = {
            'category': cg.OTROS_PRODUCTOS
        }
        return (params_to_fill, utag_data_expected)

class TestDesktopTealiumTagPageLi(TealiumUtagDataExpected):

    @tags('tealium', 'listing', 'desktop')
    def test_page_li_all_categories(self, wd):
        """
        Tested tealium tag page in li, all categories (desktop).
        """
        adlistdesktop = AdListDesktop(wd)

        utag_data_expected = {
            self.EVENT_NAME: ev.LIST,
            self.REGION_LEVEL1_ID: '0',
            self.REGION_LEVEL1: 'Chile',
        }
        adlistdesktop.go('ohiggins')
        self.assert_utag_data(wd, utag_data_expected)

class TestMobileTealiumTagPageLi(TestDesktopTealiumTagPageLi):
    user_agent = utils.UserAgents.IOS
    is_mobile = True

    @tags('tealium', 'listing', 'mobile')
    def test_page_li_all_categories(self, wd):
        """
        Tested tealium tag page in li, all categories (mobile).
        """
        super(TestMobileTealiumTagPageLi, self).test_page_li_all_categories()
