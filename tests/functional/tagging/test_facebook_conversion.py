# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertSuccess, AdInsertWithAccountSuccess
from yapo.pages.desktop_list import AdViewDesktop as AdView
from yapo.utils import Categories
from yapo.geography import Regions
from yapo import steps
import yapo

class TestFacebookConversionTag(yapo.SeleniumTest):
    """ Testing that the FB conversion tag is present where it should be """

    snaps = ['bumpedads']
    bconfs = {'*.*.facebook_conversion.enabled': '1'}
    user_data = {'subject': "equipo de m�sica",
                 'body': "est� en muy buen estado",
                 'region': Regions.REGION_METROPOLITANA,
                 'communes': 331,
                 'category': Categories.ELECTRODOMESTICOS,
                 'private_ad': True,
                 'name': "diego",
                 'email': "diego@schibsted.cl",
                 'email_verification': "diego@schibsted.cl",
                 'phone': "123182821",
                 'password': "123123123",
                 'password_verification': "123123123",
                 'create_account': False,
                 'accept_conditions': True
    }

    @tags('ad_insert', 'desktop', 'facebook')
    def test_ad_insert(self, browser):
        """ Facebook Conversion: Looking for added javascript code for facebook tags after ad insert """
        steps.insert_an_ad(browser, AdInsert, **self.user_data)
        page = AdInsertSuccess(browser)
        page.wait.until_present('facebook_noscript')
        img_code = '<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010333939832&amp;value=0&amp;currency=EUR" />'
        print('fb: {}'.format(page.facebook_noscript.get_attribute('innerHTML')))
        self.assertIn(img_code, page.facebook_noscript.get_attribute('innerHTML'))
        page.wait.until_present('facebook_script')
        self.assertTrue(bool(page.facebook_script))


    @tags('ad_insert', 'desktop', 'facebook', 'accounts')
    def test_ad_insert_with_create_account(self, browser):
        """ Facebook Conversion: Looking for added javascript code for facebook tags after ad insert with create account selected """
        self.user_data.update({'create_account': True})
        steps.insert_an_ad(browser, AdInsert, **self.user_data)
        page = AdInsertWithAccountSuccess(browser)
        page.wait.until_present('facebook_noscript')
        img_code = '<img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/offsite_event.php?id=6010333939832&amp;value=0&amp;currency=EUR" />'
        self.assertIn(img_code, page.facebook_noscript.get_attribute('innerHTML'))
        page.wait.until_present('facebook_script')
        self.assertTrue(bool(page.facebook_script))

    @tags('adreply', 'desktop', 'facebook')
    def test_ad_reply(self, browser):
        """ Facebook Conversion: Looking for facebook tags after an ad reply """

        page = AdView(browser)
        page.go('6394118')
        page.send_adreply('diego', 'diego@schibsted.cl', '123123123', 'Quiero lo que vendes')
        page.wait.until_present('ad_reply_facebook_tag')
        self.assertTrue(bool(page.wait.until_present('ad_reply_facebook_tag')))
