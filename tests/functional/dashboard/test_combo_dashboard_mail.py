# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.generic import SentEmail
from yapo.pages.simulator import Paysim
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds, Dashboard
from yapo import utils
import yapo
import yapo.conf
import time

class DashboardReviewForm(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_at_combo']

    bconfs = {
        '*.*.combos_at.enabled': '1',
    }
    
    products_mail = {
        1051: 'Est�ndar: Subir por 3 d�as',
        1052: 'Avanzado: Subir por 4 d�as + Vitrina x1',
        1053: 'Premium: Subir por 4 d�as + Vitrina x1 + Etiqueta',
    }

    products_val = {
        1051: 'Est�ndar: Subir por 3 d�as',
        1052: 'Avanzado: Subir por 4 d�as + Vitrina x1',
        1053: 'Premium: Subir por 4 d�as + Vitrina x1 + Etiqueta (Urgente)',
    }

    prices = {
        1051: '8.250',
        1052: '14.450',
        1053: '17.450'
    }
    
    def _go_to_summary(self, wd, user):
        login = Login(wd)
        login.login(user, '123123123')
        return SummaryPayment(wd)

    def _check_val(self, wd, summary, prod, ad_info, price):
        expected_values = [[
            ad_info['subject'],
            self.products_val[prod],
            '$ {}'.format(price),
            ''
        ]]
        list_id = ad_info['list_id']
        summary.add_element_to_cart(prod=prod, list_id=list_id)
        table_values = summary.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

    def _get_expected_value(self, prod_id):
        return [[], [self.products_mail[prod_id], '$' + self.prices[prod_id]]]

    def _check_mail(self, wd, summary, prod_id):
        # pays
        summary.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
       
        # check mail
        sent_email = SentEmail(wd)
        sent_email.go()
        table_values = sent_email.get_table_data('products_table')
        self.assertListEqual(table_values, self._get_expected_value(prod_id))

    @tags('desktop', 'dashboard')
    def test_mail_from_dashboard_combo(self, wd):
        """ Check mail content when purchasing dashboard combos"""
        
        list_combos = [
            {   'subject': 'Test at combo 1',
                'list_id': 8000087,
                'prod_id': 1051
            },
            {   'subject': 'Test at combo 2',
                'list_id': 8000085,
                'prod_id': 1052
            },
            {   'subject': 'Test at combo 3',
                'list_id': 8000086,
                'prod_id': 1053
            }
        ]
        
        summary = self._go_to_summary(wd, 'user.01@schibsted.cl')
        for ad_info in list_combos:
            prod_id = ad_info['prod_id']
            self._check_val(wd, summary, prod_id, ad_info, self.prices[prod_id])
            self._check_mail(wd, summary, prod_id)
