# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.help import Help
import logging
import yapo, time

logger = logging.getLogger(__name__)


class HelpTesting(yapo.SeleniumTest):

    """Tests for Help Pages & Support Forms"""

    maxDiff = None

    category_groups = [
        ['Vendo', 'Arriendo', 'Arriendo de temporada'],
        ['Autos, camionetas y 4x4', 'Buses, camiones y furgones', 'Motos', 'Barcos, lanchas y aviones', 'Otros veh�culos', 'Ofertas de empleo',
         'Busco empleo', 'Servicios', 'Negocios, maquinaria y construcci�n'],
        ['Accesorios y piezas para veh�culos', 'Muebles', 'Electrodom�sticos', 'Jard�n, herramientas y exteriores', 'Otros art�culos del hogar', 'Animales y sus accesorios', 
         'Computadores y accesorios', 'Celulares, tel�fonos y accesorios', 'Consolas, videojuegos y accesorios', 'Audio, TV, video y fotograf�a'],
        ['Moda y vestuario', 'Bolsos, bisuter�a y accesorios', 'Salud y belleza', 'Calzado', 'Vestuario futura mam� y ni�os',
         'Juguetes', 'Coches y art�culos infantiles', 'Deportes, gimnasia y accesorios', 'Bicicletas, ciclismo y accesorios',
         'Hobbies y outdoor', 'Instrumentos musicales y accesorios', 'M�sica y pel�culas (DVDs, etc.)', 'Libros y revistas',
         'Arte, antig�edades y colecciones', 'Otros productos']
    ]

    bump_prices = [
        ["\n".join(category_groups[0])] + ['$2.450', '$11.550', '$7.050'],
        ["\n".join(category_groups[1])] + ['$1.850', '$8.050', '$4.850'],
        ["\n".join(category_groups[2])] + ['$950', '$4.550', '$2.750'],
        ["\n".join(category_groups[3])] + ['$950', '$4.550', '$2.750'],
    ]

    gallery_prices = [
        ["\n".join(category_groups[0])] + ['$2.750','$9.250','$31.550'],
        ["\n".join(category_groups[1])] + ['$2.050','$6.450','$22.050'],
        ["\n".join(category_groups[2])] + ['$1.050','$3.650','$12.550'],
        ["\n".join(category_groups[3])] + ['$1.050','$3.650','$12.550'],
    ]

    label_prices = [
        ["\n".join(category_groups[0])] + ['$3.150'],
        ["\n".join(category_groups[1])] + ['$2.200'],
        ["\n".join(category_groups[2])] + ['$2.100'],
        ["\n".join(category_groups[3])] + ['$2.100'],
    ]

    BUMP = 1
    GALLERY = 2
    LABEL = 3

    bconfs = {
        '*.*.payment_settings.product.1.1.1220.value': 'price:2450',
        '*.*.payment_settings.product.1.1.1240.value': 'price:2450',
        '*.*.payment_settings.product.1.1.1260.value': 'price:2450',
        '*.*.payment_settings.product.1.1.2020.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2040.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2060.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2080.value': 'price:1850',
        '*.*.payment_settings.product.1.1.2120.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7020.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7040.value': 'price:1850',
        '*.*.payment_settings.product.1.1.7060.value': 'price:1850',
        '*.*.payment_settings.product.1.1.*.value': 'price:950',
        '*.*.payment_settings.product.1.8.1220.value': 'price:11550',
        '*.*.payment_settings.product.1.8.1240.value': 'price:11550',
        '*.*.payment_settings.product.1.8.1260.value': 'price:11550',
        '*.*.payment_settings.product.1.8.2020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2060.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2080.value': 'price:8050',
        '*.*.payment_settings.product.1.8.2120.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7020.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7040.value': 'price:8050',
        '*.*.payment_settings.product.1.8.7060.value': 'price:8050',
        '*.*.payment_settings.product.1.8.*.value': 'price:4550',
        '*.*.payment_settings.product.1.2.1220.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1240.value': 'price:7050',
        '*.*.payment_settings.product.1.2.1260.value': 'price:7050',
        '*.*.payment_settings.product.1.2.2020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2060.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2080.value': 'price:4850',
        '*.*.payment_settings.product.1.2.2120.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7020.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7040.value': 'price:4850',
        '*.*.payment_settings.product.1.2.7060.value': 'price:4850',
        '*.*.payment_settings.product.1.2.*.value': 'price:2750',
        '*.*.payment_settings.product.1.3.1220.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1240.value': 'price:9250',
        '*.*.payment_settings.product.1.3.1260.value': 'price:9250',
        '*.*.payment_settings.product.1.3.2020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2080.value': 'price:6450',
        '*.*.payment_settings.product.1.3.2120.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7020.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7040.value': 'price:6450',
        '*.*.payment_settings.product.1.3.7060.value': 'price:6450',
        '*.*.payment_settings.product.1.3.*.value': 'price:3650',
        '*.*.payment_settings.product.1.31.1220.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1240.value': 'price:2750',
        '*.*.payment_settings.product.1.31.1260.value': 'price:2750',
        '*.*.payment_settings.product.1.31.2020.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2040.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2060.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2080.value': 'price:2050',
        '*.*.payment_settings.product.1.31.2120.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7020.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7040.value': 'price:2050',
        '*.*.payment_settings.product.1.31.7060.value': 'price:2050',
        '*.*.payment_settings.product.1.31.*.value': 'price:1050',
        '*.*.payment_settings.product.1.32.1220.value': 'price:31550',
        '*.*.payment_settings.product.1.32.1240.value': 'price:31550',
        '*.*.payment_settings.product.1.32.1260.value': 'price:31550',
        '*.*.payment_settings.product.1.32.2020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2060.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2080.value': 'price:22050',
        '*.*.payment_settings.product.1.32.2120.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7020.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7040.value': 'price:22050',
        '*.*.payment_settings.product.1.32.7060.value': 'price:22050',
        '*.*.payment_settings.product.1.32.*.value': 'price:12550',
    }

    question_for_all = 'Si a�n tienes dudas sobre este tema, puedes comunicarte con nosotros aqu�.'
    question_and_answers = {1: ['�Es realmente gratis publicar en Yapo.cl?', 'Insertar un aviso en yapo.cl es gratis y no se cobra ning�n tipo de comisi�n.', question_for_all],
2: ['�C�mo puedo insertar un aviso?', 'Insertar un aviso en yapo.cl es gratis y puedes hacerlo rellenando el formulario que se accede desde el bot�n insertar aviso situado en la cabecera de todas las p�ginas.', 'Una vez insertado, recibir�s un correo electr�nico inform�ndote que tu aviso ser� revisado por nuestro equipo. Si tu aviso cumple con las reglas de yapo.cl, se publicar� en los pr�ximos minutos y recibir�s un segundo correo electr�nico con el enlace para acceder a tu publicaci�n.', 'Recuerda que si insertas tu aviso durante la noche, �ste se publicar� la ma�ana siguiente.', question_for_all],
3: ['No puedo ingresar im�genes o fotograf�as en mi aviso', 'Las im�genes deben estar en formato jpg, png o gif y no pueden pesar m�s de 2MB. Para que tu aviso sea �ptimo, te recomendamos ingresar im�genes con un peso de 300KB c/u.', question_for_all],
4: ['�Es necesario que mi tel�fono se publique en mi aviso?', 'Al insertar un aviso solicitamos ingresar tu tel�fono para que tengas m�s contactos y as� vendas m�s r�pido. Sin embargo, si no deseas que se vea en el aviso, puedes marcar el campo de "no mostrar" en el formulario.', question_for_all],
5: ['�Puedo utilizar HTML en mis avisos?', 'No es posible utilizar c�digo HTML al insertar avisos en yapo.cl.', question_for_all],
6: ['�C�mo puedo acceder a mis avisos?', 'Puedes acceder al listado de tus avisos ingresando a tu cuenta en yapo.cl.', 'Si no tienes cuenta, puedes solicitarlos desde la p�gina Mis avisos. Ingresa tu correo electr�nico y recibir�s el listado con todas tus publicaciones.', question_for_all],
7: ['�C�mo puedo modificar mi aviso?', 'Ingresa a tu cuenta yapo.cl donde encontrar�s el listado de todos tus avisos publicados. Puedes modificar haciendo click en el icono de l�piz de tu aviso.', 'Si no tienes cuenta, busca tu aviso mediante el buscador de yapo.cl. Accede a la p�gina de tu aviso y en la zona "Administra tu aviso", elige la opci�n "Editar".', question_for_all],
8: ['�C�mo puedo eliminar mi aviso?', 'Para eliminar un aviso puedes hacerlo ingresando a tu cuenta yapo.cl donde encontrar�s el listado de todos tus avisos publicados. Puedes eliminarlo haciendo click en el icono de basurero de tu aviso.', 'Si no tienes cuenta, busca tu aviso mediante el buscador de yapo.cl. Accede a la p�gina de tu aviso y en la zona "Administra tu aviso", elige la opci�n "Eliminar".', 'Al eliminar tu aviso este quedar� desactivado por un periodo de 7 d�as, despu�s de los cuales el aviso de borrar� autom�ticamente.', 'Durante el periodo de 7 d�as en que el aviso est� inactivo, podr�s volver a activarlo ingresando a tu cuenta o bien a trav�s del correo electr�nico de desactivaci�n.', 'Las formas de volver a activarlo son:', '- Activar: El aviso volver� a quedar activo, por el tiempo de vida que le reste, y quedar� en la misma posici�n en el listado en que se encontraba al momento de desactivarlo.', '- Activar y subir: Si quieres reactivar el aviso con esta opci�n, este se activar� y volver� a las primeras posiciones del listado. Esta opci�n funciona como servicio pago.' , question_for_all],
9: ['No recuerdo la contrase�a de mi aviso', 'Si quieres modificar tu anuncio y no recuerdas tu contrase�a, ingresa al v�nculo �La olvidaste? de tu aviso e introduce tu correo electr�nico.', 'Si tienes cuenta, accede al enlace Mi cuenta ubicado en la cabecera del sitio e ingresa al v�nculo �La olvidaste? e introduce tu correo electr�nico. En ambos casos recibir�s un mensaje con el v�nculo para cambiar tu contrase�a.', question_for_all],
10: ['No reconoce la contrase�a de mi aviso', 'Cuando quieras administrar tu aviso, comprueba que la contrase�a ingresada sea la misma en ambos campos. La contrase�a tiene que respetar min�sculas y/o may�sculas.', 'Si tienes cuenta en yapo.cl, la contrase�a de �sta te servir� para todos tus avisos publicados anteriormente con el mismo correo.', question_for_all],
11: ['�Cu�nto se demora mi aviso en ser publicado?', 'Una vez insertado tu aviso, recibir�s un correo electr�nico inform�ndote que tu aviso ser� revisado por nuestro equipo. Si tu aviso cumple con las reglas de yapo.cl, en los pr�ximos minutos, recibir�s un segundo correo inform�ndote que tu aviso fue publicado.', 'Recuerda que si insertas tu aviso durante la noche, �ste se publicar� la ma�ana siguiente.', question_for_all],
12: ['�Por qu� no aparece publicado mi aviso?', 'Para garantizar calidad de contenidos, nuestro equipo editorial revisa cada aviso insertado y lo valida de acuerdo a las reglas de yapo.cl.', 'Si tu aviso no aparece publicado en el sitio, puede que a�n est� siendo revisado. Cuando el proceso de validaci�n se haya completado, recibir�s un correo electr�nico informando si tu aviso fue aprobado o rechazado.', question_for_all],
13: ['Mi aviso fue rechazado por duplicado', 'Tu aviso ha sido rechazado por duplicado, ya que tienes otro similar publicado en nuestro sitio.', 'Recuerda que puedes editarlo, o bien eliminarlo e insertar uno nuevo.', question_for_all],
14: ['Mi aviso fue rechazado por estar en diferentes regiones', 'Los avisos s�lo se pueden publicar en la regi�n donde se encuentra el producto o servicio que se ofrece.', 'Cuando los usuarios buscan un producto o servicio en yapo.cl, tienen la opci�n de seleccionar una regi�n o bien todo el pa�s. En ambos casos tu aviso aparecer� listado.', question_for_all],
15: ['Mi aviso fue rechazado por ofrecer m�ltiples productos', 'Yapo.cl s�lo permite la publicaci�n de un producto por aviso, salvo en el caso de que sean conjuntos y se vendan todos por un precio �nico. Incluye un precio para todos los productos o divide tu aviso en varios por producto para que podamos aprobarlo.', question_for_all],
16: ['�C�mo respondo a un contacto?', 'Si tienes interesados en tu aviso, �stos te contactar�n por correo electr�nico o al tel�fono proporcionado en tu aviso. Puedes responderles por cualquiera de estos medios.', question_for_all],

18: ['�Qu� es "Subir Personalizado"?',
    'Con este servicio puedes personalizar la frecuencia y la cantidad de d�as en los que tu aviso subir� en los listados de yapo.',
    'Las frecuencias para seleccionar son las siguientes:',
    'El per�odo m�ximo para seleccionar es de 90 d�as.',
    question_for_all],

19: ['�Qu� es "Subir Ahora"?', '"Subir Ahora" es lo mismo que "Subir aviso". Tu aviso vuelve a estar en el top de la lista. Los precios var�an seg�n la categor�a del aviso.', question_for_all],
20: ['�Qu� es "Subir Diario"?', 'Tu aviso sube 7 veces, una vez al d�a por una semana a la misma hora que compraste. Los precios var�an seg�n la categor�a del aviso.', question_for_all],
21: ['�Qu� es "Subir Semanal"?', 'Tu aviso sube 4 veces, una vez por semana durante 4 semanas seguidas, mismo d�a y hora que compraste. Los precios var�an seg�n la categor�a del aviso.', question_for_all],

23: ['�Qu� es "Vitrina 1"?', 'Tu aviso aparece en la vitrina aleatoriamente junto a otros durante 1 d�a.', question_for_all],
24: ['�Qu� es "Vitrina 7"?', 'Tu aviso aparece en la vitrina aleatoriamente junto a otros durante 7 d�as.', question_for_all],
25: ['�Qu� es "Vitrina 30"?', 'Tu aviso aparece en la vitrina aleatoriamente junto a otros durante 30 d�as.', question_for_all],

27: ['�Cu�nto tiempo se encuentra publicado mi aviso en el sitio?', 'A partir de 3 meses tu aviso podr�a ser eliminado del sitio por caducidad. Si ya vendiste, elimina tu aviso. Si a�n quieres vender y no lo has logrado, modif�calo o sube tu aviso.', question_for_all],
28: ['�Es necesario crear una cuenta para utilizar yapo.cl?', 'Puedes navegar e insertar avisos en el sitio sin necesidad de tener una cuenta en yapo.cl.','Al crear una cuenta tendr�s tu informaci�n centralizada y podr�s administrar tus avisos en un solo lugar.', question_for_all],
29: ['�C�mo se crea una cuenta?', 'Crear una cuenta es muy f�cil, s�lo tienes que acceder a "Crear cuenta" en la parte superior del sitio y completar el formulario que te aparecer�, o bien puedes ingresar aqu�', 'Al insertar un aviso, tambi�n lo puedes hacer seleccionando la opci�n al final del formulario.', 'De las dos formas, recibir�s un correo electr�nico con un enlace que te permitir� activarla.', question_for_all],
30: ['�C�mo inicio sesi�n en mi cuenta?', 'S�lo tienes que acceder a Mi cuenta en la parte superior del sitio e ingresar tu usuario y contrase�a, o bien, puedes ingresar aqu�.', question_for_all],
31: ['No puedo iniciar sesi�n en mi cuenta', 'Ingresa tu correo electr�nico como usuario y la contrase�a que ingresaste al crear tu cuenta. Si no recuerdas la contrase�a, puedes recuperarla accediendo al enlace �La olvidaste? ubicado en la misma secci�n de inicio de sesi�n.', question_for_all],
32: ['No recuerdo la contrase�a de mi cuenta', 'Accede al enlace Mi cuenta ubicado en la cabecera del sitio y haz click en el v�nculo �La olvidaste?. Introduce el correo electr�nico que utilizaste para crear tu cuenta y recibir�s un mensaje con el v�nculo para cambiar tu contrase�a.', question_for_all],
33: ['�C�mo cambio la contrase�a de mi cuenta?', 'Puedes cambiar tu contrase�a en la secci�n "Mi perfil" dentro de tu cuenta.', question_for_all],
34: ['�C�mo puedo modificar los datos de mi cuenta?', 'Puedes cambiar tus datos en la secci�n "Mi perfil" dentro de tu cuenta.', question_for_all],
35: ['�Puedo eliminar mi cuenta?', 'Si deseas eliminar tu cuenta puedes hacerlo comunic�ndote con Atenci�n al Cliente indicando el motivo.', question_for_all],
36: ['�Qu� es una Tienda y cu�nto cuesta?', 'Una tienda es una p�gina dentro de yapo.cl que te permite reunir todos tus avisos publicados en un mismo lugar. Al crear una tienda puedes agregar im�genes, un logotipo, descripci�n, datos de tu negocio y tener un link �nico para compartir donde quieras.', 'Las tiendas tienen un precio asociado a un periodo de tiempo:\nMensual: $6.000\nTrimestral: $15.000\nSemestral: $27.000\nAnual: $48.000', question_for_all],
37: ['�C�mo puedo crear una tienda?', 'Para crear una tiemda debes acceder al men� de tu cuenta de usuario en el link Tienda. Ah� debes seleccionar el per�odo de tiempo por el que quieres contratar tu Tienda y pagarla a trav�s de los medios de pago disponibles. Una vez confirmado el pago te llegar� un e-mail para acceder directamente al formulario de creaci�n de tu tienda.', question_for_all],
38: ['�Puedo elegir cualquier nombre para mi tienda?', 'Puedes elegir cualquier nombre que se ajuste a las reglas de uso de yapo.cl. No se permiten los nombres de marcas o empresas de terceros y nombres de otros usuarios.', 'Yapo.cl se reserva el derecho a eliminar informaci�n de tu tienda si ello es contrario a nuestras reglas.', '', question_for_all],
39: ['�Puedo elegir cualquier imagen para mi tienda?', 'Tanto el logotipo como la imagen de portada de las tiendas deben ajustarse a las reglas de uso de yapo.cl. No se permiten im�genes o logotipos de marcas o empresas de terceros.', 'Yapo.cl se reserva el derecho a eliminar im�genes de su tienda si ello es contrario a nuestras reglas.', question_for_all],
40: ['�Por qu� mi tienda fue eliminada?', 'Las Tiendas se desactivan transcurrido el tiempo por el que el usuario pag�.\nSi este no fuera el caso, es posible que la tienda se haya desactivado por que no cumpl�a con las reglas de yapo.cl. En este caso te debes contactar directamente con nuestro servicio al cliente.', question_for_all],
41: ['�Qu� es un pack de autos?', 'Un Pack de Autos es un servicio de pago para usuarios profesionales de la categor�a "Autos, Camionetas y 4x4" y "camiones y furgones" que permite comprar una cantidad de cupos determinados, por un tiempo espec�fico para que los avisos publicados se desplieguen en el listado de avisos.', question_for_all],
42: ['�Qu� es un pack de inmobiliario?', 'Un Pack Inmobiliario es un servicio de pago para usuarios profesionales de la categor�a "Vendo" y "Arriendo" que permite comprar una cantidad de cupos determinados, por un tiempo espec�fico para que los avisos publicados se desplieguen en el listado de avisos.', question_for_all],
43: ['�Qu� pasa si no compro un pack de autos y soy profesional de la categor�a?', 'Si un usuario es profesional de la categor�a "Autos, Camionetas y 4x4" y "camiones y furgones" y no tiene habilitado un Pack de Autos, todos sus avisos publicados no ser�n visibles por los compradores en el listado de avisos.', question_for_all],
44: ['�Qu� pasa si no compro un pack inmobiliario y soy profesional de la categor�a?', 'Si un usuario es profesional de la categor�a inmobiliaria "Vendo" y "Arriendo" y no tiene habilitado un Pack Inmobiliario, todos sus avisos publicados no ser�n visibles por los compradores en el listado de avisos.', question_for_all],
45: ['�Si mi Pack expira, los avisos se borran?', 'Los avisos siguen publicados pero con estado inactivo. Esto quiere decir que los avisos al expirar el Pack ya no ser�n desplegados en el listado de avisos.', question_for_all],
46: ['�Cu�ndo se habilita el pack?', 'El Pack se habilita de forma inmediata cuando se realiza el pago y el usuario puede inmediatamente administrar sus avisos desde su cuenta de usuario.', question_for_all],
47: ['�Cu�les son los estados de los avisos al tener un pack?', 'Aviso publicado - activo: Son aquellos avisos publicados que aparecen en el listado de avisos. Es decir los compradores lo pueden ver en el listado.\nAviso publicado - inactivo: Son aquellos avisos publicados pero que no aparecen en el listado de avisos. Es decir los compradores no lo podr�n ver en el listado.', question_for_all],
48: ['�Si publico un aviso que no sea de la categor�a "Autos, camionetas y 4x4" y "camiones y furgones", se utilizar�n cupos del Pack?', 'Cualquier aviso que no corresponda a la categor�a "Autos, Camionetas y 4x4" y "camiones y furgones" no utilizar�n ning�n cupo del Pack.', question_for_all],
49: ['�Si publico un aviso que no sea de la categor�a inmobiliaria "Vendo" y/o "Arriendo", se utilizar�n cupos del Pack?', 'Cualquier aviso que no corresponda a las categor�as "Vendo" y/o "Arriendo" no utilizar�n ning�n cupo del Pack.', question_for_all],
50: ['�Puedo congelar un Pack que no est� utilizando y activarlo m�s tarde?', 'No, la temporalidad del Pack transcurre de corrido.', question_for_all],
51: ['�Cuando me convert� en profesional de la categor�a de ofertas de empleo?', 'Seg�n tu comportamiento de publicaci�n de ofertas de empleo para nuestro sitio pasas a ser un profesional de esta categor�a. Siendo profesional deber�s pagar por la publicaci�n de tus ofertas de empleo tendr�s una revisi�n m�s r�pida de tus avisos.', question_for_all],
52: ['�Puedo dejar de ser profesional en la categor�a de ofertas de empleo?', 'No. De acuerdo a tu historial de publicaciones en el sitio, una vez que te transformas en un usuario profesional de la categor�a no puedes dejar de serlo.', question_for_all],
53: ['�Hay alguna diferencia entre no pagar o pagar por ser profesional en la categor�a de ofertas de empleo?', 'Los avisos ser�n revisados de manera m�s r�pida, reduciendo el tiempo de espera para su publicaci�n y se integrar�n m�s funcionalidades para poder administrar de forma m�s eficiente este tipo de publicaciones. Por otro lado Yapo.cl recibe cerca de 4 millones de postulantes en el mes lo que la hace una vitrina inigualable frente a otros portales de empleo.', question_for_all],
54: ['�Cu�ndo puedo editar los avisos de ofertas de empleo?', 'En esta categor�a s�lo puedes editar tu aviso hasta el tercer d�a desde que fue aprobado y publicado. Los 3 d�as son para corregir informaci�n o a�adir datos faltantes. Si se trata de una nueva oferta con caracter�sticas similares, te recomendamos utilizar el bot�n publicar similar, el cu�l copiara tu anuncio original y podr�s modificar los datos que necesites.', question_for_all],
55: ['�Puedo cambiar de categor�a el aviso de ofertas de empleo?', 'No. Una vez publicado el aviso en ofertas de empleo, no puedes cambiar la categor�a.', question_for_all],
56: ['�C�mo Comprar?', 'El pago por el aviso se realiza al momento de realizar la publicaci�n. Puedes realizar el pago con tarjetas de cr�dito o d�bito mediante Servipag online, Webpay o Kiphu; o puedes utilizar tus yapesos, si tienes saldo disponible. La duraci�n de los avisos es de 60 d�as.', question_for_all],

57: ['�Qu� es "Aviso de Veh�culo"?', 'Un "Aviso de Veh�culo" es un servicio de pago para usuarios profesionales de la categor�a "Autos, Camionetas y 4x4" y "Buses, camiones y furgones" que permite publicar pagando un valor unitario por aviso, sin la necesidad de tener la contrataci�n de un "Pack Auto".', question_for_all],
58: ['�Puedo comprar un "Pack Auto" y "Aviso de Veh�culo" para mi cuenta?', 'Si eres un usuario profesional, puedes comprar la opci�n de tener un "Pack Auto" con cupos de publicaciones y/o varios a avisos por separado en el sitio, pagando el valor unitario de un "Aviso de Veh�culo".\nDebes considerar que los avisos que pertenecen a un Pack tienen distintas condiciones a los de "Aviso de Veh�culo".', question_for_all],
59: ['�Si tengo cupos disponibles en mi Pack y quiero agregar un nuevo Auto, es necesario comprar por separado el cupo adicional?', 'Si tienes un Pack y cupos disponibles, puedes utilizarlos de la manera en que actualmente lo haces. Si no te quedan cupos disponibles en tu Pack, puedes desactivar un aviso para activar uno nuevo, o pagar solo por el aviso mediante el servicio "Aviso de Veh�culo".', question_for_all],
60: ['�Si compr� el servicio "Aviso de Veh�culo" y quiero eliminar el aviso, el valor pagado se elimina con el aviso?', 'Una vez que eliminas el aviso no podr�s volver a activarlo y el monto cancelado por tu "Aviso de Veh�culo" no es transferible a otra publicaci�n.', question_for_all],
61: ['�Cu�l es el valor de "Aviso de Veh�culo"?', 'El valor unitario por aviso del servicio "Aviso de Veh�culo" es de $6.900.', question_for_all],
62: ['�Cu�l es la duraci�n de "Aviso de Veh�culo"?', 'La vigencia es de 1 mes desde realizada la compra en el sitio. Si editas o desactivas por un periodo tu publicaci�n, la fecha de caducidad no se ver� modificada.', question_for_all],
63: ['�Se puede editar el aviso?', 'Se puede editar cuentas veces quieras, mientras el aviso cumpla con nuestras reglas.', question_for_all],

64: ['�Qu� es "Aviso de Inmueble"?', 'Un "Aviso de Inmueble" es un servicio de pago para usuarios profesionales de la categor�a "Vendo" y "Arriendo" que permite publicar pagando un valor unitario por aviso, sin la necesidad de tener la contrataci�n de un "Pack Inmo".', question_for_all],
65: ['�Puedo comprar un "Pack Inmo" y "Aviso de Inmueble" para mi cuenta?', 'Si eres un usuario profesional, puedes comprar la opci�n de tener un "Pack Inmo" con cupos de publicaciones y/o varios a avisos por separado en el sitio, pagando el valor unitario de un "Aviso de Inmueble".\nDebes considerar que los avisos que pertenecen a un Pack tienen distintas condiciones a los de "Aviso de Veh�culo".', question_for_all],
66: ['�Si tengo cupos disponibles en mi Pack y quiero agregar un nuevo Inmueble, es necesario comprar por separado el cupo adicional?', 'Si tienes un Pack y cupos disponibles, puedes utilizarlos de la manera en que actualmente lo haces. Si no te quedan cupos disponibles en tu Pack, puedes desactivar un aviso para activar uno nuevo, o pagar solo por el aviso mediante el servicio "Aviso de Inmueble".', question_for_all],
67: ['�Si compr� el servicio "Aviso de Inmueble" y quiero eliminar el aviso, el valor pagado se elimina con el aviso?', 'Si quieres eliminar tu aviso, �ste se desactivar� y desaparecer� despu�s de 7 d�as en tu cuenta. Durante estos 7 d�as podr�s activar nuevamente tu aviso, pero manteniendo la fecha en la que insertaste. El monto pagado por este aviso no es transferible a otro si eliminas tu publicaci�n.', question_for_all],
68: ['�Cu�l es el valor de "Aviso de Inmueble"?', 'El valor unitario por aviso del servicio "Aviso de Veh�culo" es de $9.900.', question_for_all],
69: ['�Cu�l es la duraci�n de "Aviso de Inmueble"?', 'La vigencia es de 2 meses una vez realizada la compra en el sitio. Si desactivas por un periodo tu publicaci�n y vuelves a activar, la fecha de caducidad no se ver� modificada.', question_for_all],
70: ['�Se puede editar el aviso?', 'Se puede editar cuentas veces quieras, mientras el aviso cumpla con nuestras reglas.', question_for_all],
71: ['�Si compr� una inserci�n de inmueble y quiero eliminar el aviso, cuanto debo esperar?', 'Debes esperar 7 d�as para publicarlo nuevamente, sin embargo puedes revivir el aviso pagando el servicio de "Revivir", que se encuentra en tu panel de avisos.', question_for_all]
}

    @tags('desktop', 'tac')
    def test_terms_prod_info(self, wd):
        """Verify existence of products on Terms and Conditions page"""
        help = Help(wd)
        help.go_terms()

        prods_on_terms = [
            '1) Servicios "Subir", "Subir Ahora", "Subir Diario" y "Subir Semanal"',
            '2) Servicios "Vitrina 1 d�a", "Vitrina 7 d�as" y "Vitrina 30 d�as"',
            '3) Servicio Tienda',
            '4) Servicio Pack Autos',
            '5) Servicio Etiqueta',
            '6) Servicio Pack Inmobiliario',
            '7) Yapesos',
            '8) Servicio Profesionales de Empleo',
            '9) Servicio Informe Autofact',
            '10) Servicio Aviso de Veh�culo',
            '11) Servicio Aviso de Inmueble'
        ]

        index = 0
        for prod in help.terms_products:
            print(">>>>"+str(index)+" = "+prod.text)
            self.assertEqual(prod.text, prods_on_terms[index])
            index = index + 1

    @tags('desktop', 'faq')
    def test_faq_last_faq(self, wd):
        """Verify last FAQ"""
        help = Help(wd)
        help.go_faq()
        self.assertEqual(help.faq_last_seller_head.text, '�Puedo congelar un Pack que no est� utilizando y activarlo m�s tarde?')
        help.faq_last_seller_head.click()
        self.assertTrue(help.faq_last_seller_body.is_displayed())
        self.assertIn('No, la temporalidad del Pack transcurre de corrido.', help.faq_last_seller_body.text)

        help.faq_last_seller_link.click()
        self.assertTrue(help.support_input_name.is_displayed())
        self.assertTrue(help.support_input_email.is_displayed())
        self.assertTrue(help.support_input_support_body.is_displayed())
        self.assertTrue(help.support_button_send.is_displayed())

    @tags('desktop', 'faq')
    def test_faq_sellers_17(self, wd):
        """FAQ: Verify the content of the table �Qu� es "Subir" y cu�nto cuesta?"""
        help = Help(wd)
        help.go_faq()
        self._check_faq_price_and_display(help, Help.SELLERS, 17, '�Qu� es "Subir" y cu�nto cuesta?')
        self._check_faq_price_table(help, Help.SELLERS, 17, self.BUMP)

    @tags('desktop', 'faq')
    def test_faq_sellers_22(self, wd):
        """FAQ: Verify the content of the table �Qu� es "Vitrina" y cuanto cuesta?"""
        help = Help(wd)
        help.go_faq()
        self._check_faq_price_and_display(help, Help.SELLERS, 22, '�Qu� es "Vitrina" y cu�nto cuesta?')
        self._check_faq_price_table(help, Help.SELLERS, 22, self.GALLERY)

    @tags('desktop', 'faq')
    def test_faq_sellers_26(self, wd):
        """FAQ: Verify the content of the table �Qu� es "Etiqueta"?"""
        help = Help(wd)
        help.go_faq()
        self._check_faq_price_and_display(help, Help.SELLERS, 26, '�Qu� es "Etiqueta"?')
        self._check_faq_price_table(help, Help.SELLERS, 26, self.LABEL)

    def test_faq_all_question_and_answer(self, wd):
        """ FAQ: Verify the all questions and answers without the numbers 22,26 and 17 """
        help = Help(wd)
        help.go_faq()
        self.maxDiff = None
        for key, expected_text in self.question_and_answers.items():
            q = help.get_faq(Help.SELLERS, key)
            q.wait.until_visible('head')
            self.assertEqual(q.head.text, expected_text[0])
            q.head.click()
            del expected_text[0]
            total = q.faq_answer
            actual_answers = []
            for par in total:
                actual_answers.append(par.text)
            self.assertListEqual(expected_text, actual_answers)

    def _check_faq_price_and_display(self, help, type, num, text):
        faq = help.get_faq(type, num)
        faq.wait.until_visible('head')
        self.assertEqual(faq.head.text, text)
        faq.head.click()
        faq.wait.until_visible('body')
        self.assertVisible(faq, 'body')

    def _check_faq_price_table(self, help, type, num, product_type):
        faq = help.get_faq(type, num)
        table = faq.get_table_data('table')[1::]  # 0,0 is empty list
        if product_type == self.BUMP:
            self.assertEquals(table, self.bump_prices)
        elif product_type == self.GALLERY:
            self.assertEquals(table, self.gallery_prices)
        elif product_type == self.LABEL:
            self.assertEquals(table, self.label_prices)
        else:
            raise Exception("WHAT?? invalid product type")
