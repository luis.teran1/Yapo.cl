# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages import mobile
from yapo import utils
import yapo
import yapo.conf
from yapo.pages.mobile import RulesDisallowed, RulesIllegal, HelpPrivacity

class TestMobileRules(yapo.SeleniumTest):

    # defining a user agent, the user_agent property of firefox is automagically changed
    user_agent = utils.UserAgents.IOS

    @tags('mobile', 'help', 'faq','rules_disallowed')
    def test_rules_disallowed(self, wd):
        rules_disallowed = RulesDisallowed(wd)
        products_not_allowed = [
          'Alimentos perecibles.', 'Bebidas o licores abiertos.', 'Paquetes de viajes y pasajes.', 'Pornograf�a.', 'Productos de sex shop.', 'Solicitudes de donaciones.',
          'Cigarros y cualquier producto a base de tabaco.', 'Entradas de cualquier tipo (eventos, festivales, shows, fiestas , deportes, cine, etc.)',
          'Cuentas corrientes, cr�ditos, d�bitos.', 'Medicamentos.', 'Masajes que no sean de empresa o personas sin certificaci�n.',
          'Acciones, t�tulos, productos financieros, seguros y planes de salud.',
          'Recetas m�dicas, cosm�ticas y qu�micas, productos de hospitales, vitaminas, quema grasas, pastillas rejuvenecedoras.',
          'Ropa interior, sabanas y toallas usadas, toallas sanitarias tipo Anion.', 'Cilindros de oxigeno o de gas.', 'Lentes de contacto.',
          'Bases de datos de correos electr�nicos.', 'Registros personales o de empresas.', 'Documentos de identificaci�n (CI, pasaporte, diplomas, etc.).',
          'Autos con orden de embargo.', 'Tarot, esoterismo.', 'Marketing de multi-nivel, piramidal y trabajos desde casa o comisi�n.',
          'Aprobaci�n y registro en organismos gubernamentales como: registro civil, municipios, etc.', 'Cupones de descuentos, km a�reos.',
          'Licencias concedidas por organismos p�blicos.', 'Hacking y Cracking.', 'Art�culos de beneficencia (sin permiso Yapo).', 'Decodificadores de tv por cable gratis.',
          'Avisos de citas.', 'Sustancias qu�micas y peligrosas.', 'Flora y fauna en peligro de extinci�n o sin debida autorizaci�n del entidades gubernamentales.',
          'Productos que infrinjan la ley de protecci�n del patrimonio hist�rico y cultural del pa�s.', 'Apuestas, bingos y cualquier juego de azar.',
          'Integrantes para equipos deportivos, bandas, etc.', 'Acupuntura', 'Gorgojos', 'Apiterapia', 'Hongos tibetanos', 'Ara�as pollitos', 'Ibox, Dongles, IKS/SKS',
          'Armas con postones (y de fuego)', 'Fotocopias de libro (Producto original)', 'Art�culos nazis', 'Jeringas', 'Auriculoterapia', 'Libros en PDF (Producto original)',
          'Autos sin papeles y con orden de embargo', 'Mel�n de reuma', 'Bosque nativo', 'Moringa', 'Socios capitalistas', 'Membres�as',
          'Celulares bloqueados por IMEI / chip bloqueado', 'Yacimientos', 'Cirope de sabia', 'Ondulaci�n y permanente de pesta�as', 'Cirug�as dentales', 'Ouijas',
          'Compromisos de compraventa', 'Gift card', 'Cuentas online', 'Ropa y accesorios de militares', 'Cuentas Steam', 'Servicio de importaci�n/exportaci�n',
          'Te de kombucha / manchuria', 'Skimmer', 'Extintores / Recarga extintores', 'Fono copete', 'Preservativos',
          'Armas (habilitadas y que no indiquen ser de colecci�n), fuegos artificiales, explosivos, municiones.'
        ]

        rules_disallowed.go_rules_disallowed()
        self.assertEquals('Productos y servicios no permitidos', rules_disallowed.title.text)
        self.assertEquals('Los productos y servicios no permitidos incluyen, pero no se limitan, a los elementos especificados a continuaci�n. Los avisos de productos, bienes o servicios que violen la legislaci�n chilena ser�n rechazados.', rules_disallowed.subtitle.text)
        self.assertListEqual(products_not_allowed, [i.text for i in rules_disallowed.products_not_allowed])
        self.assertEqual('{0}{1}'.format(yapo.conf.MOBILE_URL,'/ayuda/reglas.html'), rules_disallowed.link_go_back_rules.get_attribute('href'))

    @tags('mobile', 'help', 'faq','rules_illegal')
    def test_rules_illegal(self, wd):
        products_illegal = [
             'Drogas de cualquier tipo. Equipamientos y objetos destinados a consumo o fabricaci�n de drogas.',
             '�rganos humanos , cad�veres, fluidos corporales.',
             'Productos robados, falsificados, pirateados, contrabando y todo lo que viole patentes, marcas , modelos y propiedad intelectual en general.',
             'Empleos a menores de 18, para edades entre 15 y 17 a�os se solicitar� permiso notarial.'
        ]
        rules_illegal = RulesIllegal(wd)

        rules_illegal.go_rules_illegal()
        self.assertEquals('Productos ilegales', rules_illegal.title.text)
        self.assertEquals('Los productos considerados ilegales incluyen, pero no se limitan, a los elementos especificados a continuaci�n. Los avisos de productos, bienes o servicios, que violen la legislaci�n chilena, ser�n rechazados.', rules_illegal.subtitle.text)
        self.assertListEqual(products_illegal, [i.text for i in rules_illegal.products_illegal])
        self.assertEqual('{0}{1}'.format(yapo.conf.MOBILE_URL,'/ayuda/reglas.html'), rules_illegal.link_go_back_rules.get_attribute('href'))

    @tags('mobile', 'help', 'faq','privacy')
    def test_privacy(self, wd):
        subtitles = ['Pol�tica de Privacidad yapo.cl',
                     'Recopilaci�n y uso de datos',
                     'Seguridad',
                     'Calidad',
                     'Publicaci�n e intercambio',
                     'Env�o de informaci�n publicitaria a tu correo electr�nico',
                     'Verificaci�n, correcci�n y eliminaci�n de datos',
                     'Cambios a la Pol�tica de Privacidad',
                     'Consultas']
        help_privacity = HelpPrivacity(wd)
        help_privacity.go_help_privacity()
        self.assertListEqual(subtitles, [i.text for i in help_privacity.subtitles])
