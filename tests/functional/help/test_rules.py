# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.help import Help, Rules, RuleItem
import logging
import yapo
from yapo import conf
import time

class HelpRules(yapo.SeleniumTest):

    rules = {
        '0': 'No est�n permitidos los avisos duplicados',
        '1': 'Enlaces, correos, sistemas de mensajer�a o redes sociales',
        '2': 'Precio',
        '3': 'Aviso \"Persona\"',
        '4': 'Aviso \"Profesional\"',
        '5': 'Productos y servicios prohibidos',
        '6': 'T�tulo del aviso',
        '7': 'Descripci�n del aviso',
        '8': 'Publicidad',
        '9': 'Un solo producto o servicio por aviso',
        '10':'Im�genes',
        '11':'Categor�as',
        '12':'Productos pirateados o falsificados',
        '13':'Localizaci�n',
        '14':'Productos ilegales',
        '15':'Idioma',
        '16':'Contenido ofensivo',
        '17':'Informaci�n privada',
        '18':'Servicios',
        '19':'Mascotas y animales en general'
    }


    @tags('desktop', 'rules')
    def test_all_rules(self, wd):
        """ check all rules """ 

        help = Help(wd)
        help.go_rules()
        all_rules = Rules(wd) 

        for rule_id,rule_name in self.rules.items():
            link_rule_title = all_rules.get_rule_title(rule_id)
            link_rule_title.click()
            self.assertEqual(rule_name, link_rule_title.text)
            self.assertEqual('{0}/ayuda/reglas.html#{1}'.format(conf.DESKTOP_URL, rule_id), wd.current_url)
            rule_item = all_rules.get_rule_item_by_id(rule_id)  
            rule_item.top_link.click()
            self.assertEqual('{0}/ayuda/reglas.html#TOP'.format(conf.DESKTOP_URL), wd.current_url)
