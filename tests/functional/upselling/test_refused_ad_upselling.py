# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.generic import SentEmail
from yapo.pages.ad_insert import AdInsert
from yapo.pages import mobile, desktop
from yapo.steps import trans
from yapo import utils
import yapo


class RefusedAdUpselling(yapo.SeleniumTest):

    snaps = ['accounts']

    def _get_refused_link(self, wd):
        email = SentEmail(wd)
        email.go()
        return email.body.find_element_by_link_text("aqu�").get_attribute("href")

    def _check_upselling(self, wd):
        edit = desktop.AdEdit(wd)
        edit.wait.until_not_visible('combo_upselling_box')

    @tags('upselling', 'refused_ad')
    def test_upselling_refused_ad(self, wd):
        """ Checks that a refused ad can not buy an upselling"""
        trans.insert_ad(AdInsert(wd),
                        region="15",
                        communes=327,
                        category="4020",
                        subject='you shall not show upselling',
                        body="yoLo",
                        price=999666,
                        private_ad=True,
                        name="yolo",
                        email="yolo@cabral.es",
                        email_verification="yolo@cabral.es",
                        phone="99996666",
                        password="123123123",
                        password_verification="123123123",
                        create_account=False,
                        accept_conditions=True,
                        type='s',
                        clothing_size='2',
                        condition='2',
                        gender='3'
                        )

        # Refuse on CP
        trans.review_ad(
            'yolo@cabral.es', 'you shall not show upselling', action='refuse')

        link_email = self._get_refused_link(wd)
        wd.get(link_email)

        self._check_upselling(wd)


class RefusedAdUpsellingMobile(RefusedAdUpselling):

    user_agent = utils.UserAgents.IOS

    def _check_upselling(self, wd):
        edit = mobile.NewAdInsert(wd)
        edit.wait.until_not_visible('combo_upselling_box')
