# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.responsive import Refund
from yapo.pages.generic import SentEmail
from yapo.steps import trans
import yapo
import yapo.conf


class UpsellingRefundForm(yapo.SeleniumTest):

    snaps = ['cp_upselling']

    @tags('controlpanel', 'upselling')
    def test_upselling_refund_form_desktop(self, wd):
        """ Check multi cases of refund form with desktop layout"""
        email = 'dupselling@yapo.cl'
        subject = 'Aviso upselling daily'
        device = 'desktop'
        self._upselling_refund_form(wd, email, subject, device)

    @tags('controlpanel', 'upselling')
    def test_upselling_refund_form_mobile(self, wd):
        """ Check multi cases of refund form with mobile layout"""
        email = 'gupselling@yapo.cl'
        subject = 'Aviso upselling gallery'
        device = 'iphone5'
        self._upselling_refund_form(wd, email, subject, device)

    @tags('controlpanel', 'upselling')
    def test_upselling_refund_invalid_mobile(self, wd):
        """ Check wrong params for refund form with mobile layout """
        msj = 'No encontr�mos la p�gina a la que est�s intentando acceder'
        link = (yapo.conf.SSL_URL + '/refund?'
                'id=23&a=2&p=66&h=0359af4631f94c92e2d858aae91bcdfac31c061c')
        device = 'iphone5'
        refund = Refund(wd)
        refund.go(link, device)
        self.assertEquals(refund.error_page.text, msj)

    @tags('controlpanel', 'upselling')
    def test_upselling_refund_invalid_desktop(self, wd):
        """ Check wrong params for refund form with desktop layout """
        msj = 'No encontr�mos la p�gina a la que est�s intentando acceder'
        link = (yapo.conf.SSL_URL + '/refund?'
                'id=23&a=2&p=66&h=0359af4631f94c92e2d858aae91bcdfac31c061c')
        device = 'iphone5'
        refund = Refund(wd)
        refund.go(link, device)
        self.assertEquals(refund.error_page.text, msj)

    def _get_link_from_email(self, wd):
        sent_email = SentEmail(wd)
        sent_email.go()
        link = sent_email.body.find_element_by_partial_link_text('aqu�')
        return link.get_attribute('href')

    def _validate_errors(self, refund, errors, values=None):
        if values is not None:
            refund.fill_form(**values)
        refund.submit.click()
        for key, value in errors.items():
            print('validating error:{}'.format(key))
            self.assertEquals(value, refund.get_err(key))

    def _upselling_refund_form(self, wd, email, subject, device):
        trans.review_ad(email, subject, action = 'refuse')
        link = self._get_link_from_email(wd)
        refund = Refund(wd)
        refund.go(link, device)

        # Empty errors
        errors = {
            'first_name': 'Escribe el nombre',
            'last_name': 'Escribe tu apellido',
            'rut': 'Escribe tu RUT',
            'email': 'Escribe tu e-mail',
            'bank_account_type': 'Debes seleccionar un tipo de cuenta',
            'bank_id': 'Debes seleccionar un banco',
            'bank_account_number': 'Debes escribir el n�mero de tu cuenta',
        }
        self._validate_errors(refund, errors)

        # Wrong values
        values = {
            'first_name': '0'.zfill(55),
            'last_name': '1'.zfill(55),
            'rut': '2'.zfill(40),
            'email': '3'.zfill(70),
            'bank_account_number': 'a'.rjust(5, 'a'),
        }
        errors = {
            'first_name': 'El nombre debe contener letras',
            'last_name': 'El nombre debe contener letras',
            'rut': 'El RUT es muy largo',
            'email': (
                'Confirma que el email '
                'est� en el formato correcto'
                ),
            'bank_account_number': 'Debes escribir el n�mero de tu cuenta',
        }
        self._validate_errors(refund, errors, values)

        # Rut special cases
        values = {'rut': '123123123'}
        errors = {'rut': 'El RUT no est� en el formato adecuado: XX.XXX.XXX-Y'}
        self._validate_errors(refund, errors, values)

        values = {'rut': '1'}
        errors = {'rut': 'El RUT es muy corto'}
        self._validate_errors(refund, errors, values)

        values = {'rut': '100000-5'}
        errors = {'rut': 'El RUT es incorrecto'}
        self._validate_errors(refund, errors, values)

        values = {
            'first_name': 'Daniberto',
            'last_name': 'Del Carmen',
            'rut': '100000-4',
            'email': 'dany@schibsted.cl',
            'bank_account_type': 1,
            'bank_id': 1,
            'bank_account_number': '2321239237',
        }

        refund.fill_form(**values)
        refund.submit.click()
        success_message = (
                'Hemos recibido tu informaci�n\n'
                'Se envi� un e-mail confirmando el env�o de tus datos\n'
                'La devoluci�n de tu compra se har� efectiva en un plazo'
                ' de 7 d�as h�biles.'
        )
        refund.wait.until_present('success_page')
        self.assertIn(refund.success_page.text, success_message)

        refund.go(link, device)
        for key, value in values.items():
            self.assertFalse(refund.is_enabled(key))
