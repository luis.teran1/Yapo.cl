from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import DashboardMyAds, Pack
import yapo


class OrderAdsByStates(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_carpacks']
    sort_type = ['all', 'active', 'inactive']

    @tags('desktop', 'sort', 'packs', 'states')
    def test_order_ads_all_types(self, wd):
        """ Order by published date, active and inactive ads """

        login_and_go_to_dashboard(wd, 'enabled_disabled_normal_ads@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        pack = Pack(wd)
        for option in self.sort_type:
            pack.sort_button.click()
            pack.sort_items(option)

    @tags('desktop', 'sort', 'packs', 'inactive')
    def test_order_dashboard_with_inactive_ads(self, wd):
        """ Order only with inactive ads """

        login_and_go_to_dashboard(wd, '15_disabled_ads_without_packs@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        pack = Pack(wd)
        for option in self.sort_type:
            pack.sort_button.click()
            pack.sort_items(option)

    @tags('desktop', 'sort', 'packs', 'active')
    def test_order_dashboard_with_active_ads(self, wd):
        """ Order only with active ads """

        login_and_go_to_dashboard(wd, 'active_ads_expired_pack@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        pack = Pack(wd)
        for option in self.sort_type:
            pack.sort_button.click()
            pack.sort_items(option)
