# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import Login, DashboardMyAds, HeaderElements
from yapo.pages.ad_insert import AdInsert, PackConfirm, AdInsertResult, AdInsertDuplicatedAd
from yapo.pages.packs import DashboardPacks
from yapo.pages.control_panel import ControlPanel
from yapo import utils
import yapo
import time
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI


class PacksDuplicatedAndDeleteReasonsInmo(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_inmo1']
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "0"}
    message = 'Tienes un aviso INACTIVO similar al que acabas de insertar.'

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_inmo_duplicated(self, wd):
        """Check the duplicated warning and deactivation of a duplicated ad for pro inmo user"""

        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_inmo1'])
        login = Login(wd)
        dma = DashboardMyAds(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, email=email, use_trans=True)
        utils.rebuild_asearch()
        dma.go_dashboard()
        dma.delete_ad_by_subject('House AD 1')
        self._insert_ad(wd, 1)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_inmo_del_reason_inactive_pack_inmo_cat(self, wd):
        """Check the ad deactivation for pro inmo with inactive pack when delete"""

        login = Login(wd)
        dma = DashboardMyAds(wd)
        he = HeaderElements(wd)
        email = 'cuentaprosincupos@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, email=email, use_trans=True)
        time.sleep(1)
        dma.go_dashboard()
        dma.delete_ad_by_subject('House AD 1')
        utils.rebuild_asearch()
        self._insert_ad(wd, 1)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_inmo_del_reason_expired_pack_inmo_cat(self, wd):
        """Check the ad deactivation for pro inmo with expired pack when delete"""

        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_inmo1'])
        utils.expire_packs()
        login = Login(wd)
        dma = DashboardMyAds(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, email=email, use_trans=True)
        dma.go_dashboard()
        dma.delete_ad_by_subject('House AD 1')
        utils.rebuild_asearch()
        self._insert_ad(wd, 1)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_inmo_del_reason_active_pack_no_inmo_cat(self, wd):
        """Check the ad deactivation for pro inmo with active pack when delete other category"""

        login = Login(wd)
        dma = DashboardMyAds(wd)
        he = HeaderElements(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, other=True, email=email, use_trans=True)
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('cuentaproconpack@yapo.cl', subject='House AD 1')
        cpanel.review_ad('accept')
        cpanel.logout()
        utils.rebuild_asearch()
        dma.go_dashboard()
        dma.delete_ad_by_subject('House AD 1')
        self._insert_ad(wd, 1, other=True)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    def _insert_ad(self, wd, number, account=True, other=False, rsubject='House AD {0}', email='', use_trans=False):

        desktop_ai = DesktopAI
        subject = rsubject.format(number)
        data = {
            'region': '15',
            'communes': '331',
            'category': '1220',
            'subject': subject,
            'body': 'House for check Email',
            'price': '3500000',
            'estate_type': '1',
            'rooms': '1',
            'bathrooms': '1',
        }

        if other:
            data = {
                'region': '15',
                'communes': '331',
                'category': '5020',
                'subject': subject,
                'body': 'House for check Email',
            }

        if use_trans:
            data['email'] = email
            data[
                'passwd'] = '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'
            desktop_ai = None
        else:
            data['logged'] = True

        insert_an_ad(wd, AdInsert=desktop_ai, submit=True, **data)


class PacksDuplicatedAndDeleteReasonsAuto(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout": "1"}
    message = 'Tienes un aviso INACTIVO similar al que acabas de insertar.'
    deac_message = '�Aviso inactivo!'

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_auto_del_reason_active_pack_car_cat(self, wd):
        """Check the ad deactivation for pro auto with active pack when delete"""

        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1'])
        login = Login(wd)
        dma = DashboardMyAds(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, email=email, use_trans=True)
        dma.go_dashboard()
        dma.delete_ad_by_subject('Car AD 1')
        utils.rebuild_asearch()
        self._insert_ad(wd, 1)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_auto_del_reason_expired_pack_car_cat(self, wd):
        """Check the ad deactivation for pro auto with expired pack when delete"""

        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1'])
        utils.expire_packs(2)
        login = Login(wd)
        dma = DashboardMyAds(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, email=email, use_trans=True)
        dma.go_dashboard()
        dma.delete_ad_by_subject('Car AD 1')
        utils.rebuild_asearch()
        self._insert_ad(wd, 1)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    @tags('desktop', 'account', 'packs', 'delete_reasons', 'delete_to_bump')
    def test_auto_del_reason_active_pack_no_car_cat(self, wd):
        """Check the ad deactivation for pro auto with active pack when delete other category"""

        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1'])
        login = Login(wd)
        dma = DashboardMyAds(wd)
        he = HeaderElements(wd)
        email = 'cuentaproconpack@yapo.cl'
        passwd = '123123123'
        login.login(email, passwd)

        self._insert_ad(wd, 1, other=True, email=email, use_trans=True)
        cpanel = ControlPanel(wd)
        cpanel.go_and_login('dany', 'dany')
        cpanel.search_for_ad('cuentaproconpack@yapo.cl', subject='Car AD 1')
        cpanel.review_ad('accept')
        cpanel.logout()
        utils.rebuild_asearch()
        dma.go_dashboard()
        dma.delete_ad_by_subject('Car AD 1')
        self._insert_ad(wd, 1, other=True)
        ai = AdInsertDuplicatedAd(wd)
        self.assertEqual(ai.message.text, self.message)

    def _insert_ad(self, wd, number, other=False, account=True, rsubject='Car AD {0}', email='', use_trans=False):

        desktop_ai = DesktopAI
        subject = rsubject.format(number)
        data = {
            'region': '15',
            'communes': '331',
            'category': '2020',
            'subject': subject,
            'body': 'Autito de los wenos',
            'price': '3500000',
            'brand': '1',
            'model': '1',
            'version': '1',
            'regdate': '2016',
            'mileage': '15000',
            'cartype': '1',
            'plates': 'ABCD10'
        }

        if other:
            data = {
                'region': '15',
                'communes': 331,
                'category': '5020',
                'subject': subject,
                'body': 'House for check Email',
            }

        if use_trans:
            data['email'] = email
            data[
                'passwd'] = '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6'
            desktop_ai = None
        else:
            data['logged'] = True

        insert_an_ad(wd, AdInsert=desktop_ai, submit=True, **data)
