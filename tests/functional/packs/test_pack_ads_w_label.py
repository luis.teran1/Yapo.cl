# -*- coding: latin-1 -*-
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium.common import exceptions
from yapo import utils
from yapo.decorators import tags
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.desktop_list import AdListDesktop, AdViewDesktop
from yapo.pages.mobile_list import AdListMobile, AdViewMobile
from yapo.utils import Products, Label_texts, Label_values
import time
import yapo
import yapo.conf


class PackLabelsDesktop(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']
    user_agent = utils.UserAgents.FIREFOX_MAC
    # defining a user agent, the user_agent property of firefox is
    # automagically changed
    congrats = "Tu compra se realiz� exitosamente"
    account_pass = "123123123"
    data_listing = {
        'commune': 'Macul',
        'has_address': False,
        'has_label': 'Urgente',
        'region': 'Regi�n Metropolitana'
    }
    platform = desktop
    AdView = AdViewDesktop
    AdList = AdListDesktop

    def _buy_products(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    active.select_product(prod['prod'], prod['param'])

        summary = self.platform.SummaryPayment(wd)
        summary.go_summary()
        summary.change_doc_type(desktop.Payment.BILL)
        summary.press_link_to_pay_products()
        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)

        # Run rebuild-index apply product
        utils.rebuild_index()

    def _check_on_dashboard(self, wd, products):
        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        for list_id, active in dashboard.active_ads.items():
            for p_lid, prod in products.items():
                if str(list_id) == p_lid:
                    if prod['text'] is not None:
                        self.assertTrue(
                            active.has_product_selector_disabled(prod['prod']))
                        self.assertEqual(active.label_tag.text, prod['text'])

    @tags('accounts', 'dashboard', 'products', 'label')
    def test_buy_label_disable_ad(self, wd):
        """ Buy Label & verify this is visible after disable the ad"""
        lid = '8000093'
        user = "cuentaproconpack@yapo.cl"
        data_products = {
            lid: {
                'prod': Products.LABEL,
                'param': Label_values.URGENT,
                'text': Label_texts.URGENT
            }
        }

        login = self.platform.Login(wd)
        login.login(user, self.account_pass)
        self._buy_products(wd, data_products)
        self._check_on_dashboard(wd, data_products)
        utils.rebuild_index()

        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[int(lid)]
        pack = self.platform.Pack(wd)
        pack.change_status(ad, 'inactive')
        utils.rebuild_index()
        wd.implicitly_wait(0.5)

        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[int(lid)]
        self.assertEqual(ad.label_tag.text, Label_texts.URGENT)
        pack = self.platform.Pack(wd)
        pack.change_status(ad, 'active')
        utils.rebuild_index()
        wd.implicitly_wait(0.5)

        dashboard = self.platform.DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[int(lid)]
        self.assertEqual(ad.label_tag.text, Label_texts.URGENT)
