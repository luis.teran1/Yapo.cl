# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo import steps
from yapo.decorators import tags
from yapo.utils import Products, PacksPeriods, PacksType
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds, Dashboard, PaymentVerify, Payment
from yapo.pages.ad_insert import AdInsert
from yapo.pages.packs import BuyPacks
from yapo.pages.simulator import Paysim
from yapo.pages.generic import SentEmail
from selenium.common.exceptions import NoSuchElementException
import datetime
import time

class PackPurchaseActivate(yapo.SeleniumTest):
    snaps = ['accounts','diff_pack_cars1']
    bconfs = {
            "*.*.packs.expire_time.1.value":"31 days",
            "*.*.packs.expire_time.2.value":"93 days",
            "*.*.packs.expire_time.3.value":"186 days",
            "*.*.packs.expire_time.4.value":"366 days",
            "*.controlpanel.modules.adqueue.auto.pack.timeout": "1"
        }
    # ev = expected_values
    def pay_packs(self,wd,user,passwd,elements,ev_cart,payment,ev_verify,do_login):
        if do_login:
            login = Login(wd)
            login.login(user,passwd)
        summary = SummaryPayment(wd)
        # Add elements to cart
        for key, data in elements.items():
            if key[0] == 'P':
                data_slots, data_period = data
                summary.add_pack_to_cart(data_slots,data_period)
            if key[0] == 'S':
                data_store = data
                summary.add_element_to_cart(data_store)
            if key[0] == 'A':
                data_list_id, data_prod = data
                summary.add_element_to_cart(data_prod, data_list_id)

        # Check cart
        if ev_cart is not None:
            table_values = summary.get_table_data('products_list_table')
            table_values.sort()
            ev_cart.sort()
            self.assertListEqual(table_values, ev_cart)

        # Pay with servipag
        if payment == 'servipag':
            summary.radio_servipag.click()
        summary.change_doc_type(Payment.BILL)
        # Confirm payment
        summary.press_link_to_pay_products()
        # Simulator
        paysim = Paysim(wd)
        paysim.pay_accept()

        # Verify success
        if ev_verify is not None:
            payment_verify = PaymentVerify(wd)
            table_values = payment_verify.get_table_data('sale_table')
            table_values.sort()
            ev_verify.sort()
            self.assertListEqual(table_values, ev_verify)

        # Verify cart clean
        summary.go_summary()
        self.assertTrue(summary.empty_cart.is_displayed())


    @tags('payment','pack')
    def test_purchase_pack_activate_truck(self, wd):
        """ When purchase a pack the ad from trucks category is activated """
        user = 'cuentaprosinpack@yapo.cl'
        passwd = '123123123'
        elements = {'P01':('010',PacksPeriods.QUARTERLY)}
        payment = 'webpay'
        ev_cart = [['Pack Auto', '10/trimestral', '$ 96.000', '']]
        ev_verify = [['', 'Pack Auto 10/trimestral', '$ 96.000']]
        date_pack = utils.get_date(addDays = 93)
        ev_success_mail = [
                ['Cupos totales de packs', 'Fecha vencimiento'], 
                ['10', date_pack]
                ]
        ev_invoice_mail = [['1', 'Pack Auto 10/trimestral','$ 96.000']]

        self._insert_ad(wd,user,"yoLo")
        time.sleep(2)
        self.pay_packs(wd,user,passwd,elements,ev_cart,payment,ev_verify, False)
        time.sleep(2)
    
        ad_data = {
                'subject' : 'Camion',
                'status' : 'Aviso publicado',
                'vi_link_enabled' : False,
                'options_enabled' : True,
                'bumps_enabled' : False,
                'gallery_enabled' : False
        }
        
        self._check_dashboard_ad(wd,ad_data)

    def _insert_ad(self, wd, email, name):
        steps.insert_an_ad(wd, AdInsert=AdInsert,
            category=2040,
            subject="Camion",
            body="yoLo",
            region="15",
            communes=331,
            email=email,
            email_confirm=email,
            name=name,
            regdate="2010",
            gearbox="1",
            fuel="1",
            mileage='15000',
            submit=True
        )

    def _check_dashboard_ad(self, wd, ad_data):

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        self.assertIsNotNone(active_ad)

        #check ad status
        self.assertEqual(active_ad.status.text, ad_data['status'])

        if ad_data['vi_link_enabled']:
            self.assertIsNotNone(active_ad.subject.get_attribute('href'))
        else:
            self.assertIsNone(active_ad.subject.get_attribute('href'))

        if ad_data['bumps_enabled']:
            active_ad.wait.until_visible('bumps_enabled')
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.bumps_enabled)

        if ad_data['gallery_enabled']:
            self.assertIsNotNone(active_ad.gallery_enabled)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.gallery_enabled)

        ##check edit and delete
        if ad_data['options_enabled']:
            self.assertIsNotNone(active_ad.options_buttons)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.options_buttons)
