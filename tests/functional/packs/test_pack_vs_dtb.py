from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop_list import AdDeletePage
from yapo.pages.desktop import DashboardMyAds
from yapo import utils
import yapo

class PackVsDTB(yapo.SeleniumTest):

    def setUp(self):
        # The user is still pro for 2020, but not as the first option
        utils.execute_query(
            """UPDATE account_params SET value = '"'"'{}'"'"' WHERE account_id = 10 AND name = '"'"'is_pro_for'"'"'""".format(self.is_pro_for)
        )

    def _delete_ad(self, wd, list_id, DTB):
        """ Deleting a pack auto ad does delete ad immediately (not DTB) """

        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        dashboard = DashboardMyAds(wd)
        ads = dashboard.create_ad_list()
        self.assertEquals(ads, [['Audi A4 2011', 'Aviso publicado']])

        ad_delete = AdDeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()

        dashboard.go()
        ads = dashboard.create_ad_list()

        expected = 'Aviso inactivo' if DTB else 'Borrando aviso'
        self.assertEquals(ads, [['Audi A4 2011', expected]])

class PackAutoVsDTB(PackVsDTB):

    is_pro_for = '2120,2020'
    snaps = ['accounts', 'diff_pack_cars1']
    
    def test_pack_auto_ad_delete_deletes(self, wd):
        """ Deleting a pack auto ad deactivates the ad (DTB) """
        self._delete_ad(wd, 8000093, True)

class PackInmoVsDTB(PackVsDTB):

    is_pro_for = '1240,1220'
    snaps = ['accounts', 'diff_pack_inmo1']

    def test_pack_inmo_ad_delete_deletes(self, wd):
        """ Deleting a pack inmo ad deactivates the ad (DTB) """
        self._delete_ad(wd, 8000093, True)
