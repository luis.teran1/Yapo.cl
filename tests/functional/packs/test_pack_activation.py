# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.steps import accept_and_return_ad_id
from yapo.pages.simulator import Paysim
from yapo.utils import PacksPeriods
from yapo.pages.desktop import Login, DashboardMyAds, Dashboard, Pack, SummaryPayment, Modal
from yapo.pages.generic import SentEmail
from yapo.utils import JsonTool
from yapo.mail_generator import Email
from yapo import utils
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI

import yapo
import time

class CarPackDashboard(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_carpacks', 'packstodisable10_15']
    bconfs = {
        "*.controlpanel.modules.adqueue.auto.pack.timeout":"1",
        "*.*.packs.expire_time.2.value":"1 seconds",
        '*.*.movetoapps_splash_screen.enabled': '0'
    }
    sort_type = ['all', 'active', 'inactive']
    pro_data = {}

class CarPackExpirationAndActivation(CarPackDashboard):

    @tags('desktop', 'account', 'packs')
    def test_packauto_expired_pack_with_active_ads(self, wd):
        """ Check disabled ads after pack expiration """
        utils.expire_packs()
        login_and_go_to_dashboard(wd, 'active_ads_expired_pack@yapo.cl', '123123123')
        dash = Dashboard(wd)
        pack = Pack(wd)
        dash_my_ads = DashboardMyAds(wd)
        self.purchase_pack(wd, '010', PacksPeriods.QUARTERLY)
        dash_my_ads.go_dashboard()
        active_pack_ads = pack.filter_active_ads(DashboardMyAds(wd).active_ads)
        time.sleep(3)
        utils.expire_packs()
        dash.go_dashboard()
        modal = Modal(wd)
        self.assertTrue(modal.expired_pack.is_displayed())
        dash_my_ads = DashboardMyAds(wd)
        disabled_ads = pack.filter_disabled_ads(dash_my_ads.active_ads)
        self.assertEquals(len(active_pack_ads), len(disabled_ads))

    @tags('desktop', 'account', 'packs')
    def test_pack_auto_enable_ads_by_buying_pack(self, wd):
        """ Check enabled ads after purchasing a pack """

        login_and_go_to_dashboard(wd, 'without_pack_5_disabled_ads@yapo.cl', '123123123')
        dash = Dashboard(wd)
        pack = Pack(wd)
        disabled_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        self.assertEquals(len(disabled_ads), 5)
        self.purchase_pack(wd, '010', PacksPeriods.QUARTERLY)
        dash.go_dashboard()
        active_pack_ads = pack.filter_active_ads(DashboardMyAds(wd).active_ads)
        self.assertEquals(len(active_pack_ads), 5)

    @tags('desktop', 'account', 'packs', 'core2')
    def test_pack_auto_enable_pack_user(self, wd):
        """ From pri to pro user then passing to packauto user by accepting the 7th ad """

        test_name = '{0}.{1}'.format(type(self).__name__,self._testMethodName)
        test_data_key = ["toyota_car", "ford_car"]
        test_data = JsonTool.read_test_data(test_data_key[0])
        if utils.is_not_regress():
            previous_test_id = 'AdInsertTest.test_ad_insert_6_cars_for_pack_auto'
            ad_data = JsonTool.read_data_of_test(previous_test_id)
            email = ad_data['email']
        else:
            email = '5_published_ads_1d_review_pending@yapo.cl'
        test_data['email'] = email
        test_data['email_verification'] = email
        CarPackExpirationAndActivation.pro_data[test_name] = {'email':email}

        self._insert_ad(wd, test_data)

        CarPackExpirationAndActivation.pro_data[test_name].update({test_data_key[0]:test_data['subject']})
        ad_id = accept_and_return_ad_id(wd, email = test_data['email'], subject = test_data['subject'])

        CarPackExpirationAndActivation.pro_data[test_name].update({'id_{0}'.format(test_data_key[0]):ad_id})

        mlt = Email.create(wd)
        mlt.check_pro_user_mail(self, mail=test_data['email'], username = test_data['name'])

        test_data = JsonTool.read_test_data(test_data_key[1])
        test_data['email'] = email
        test_data['email_verification'] = email
        test_data['logged'] = True

        # The user is logged and pro, no private_ad option
        if 'private_ad' in test_data:
            test_data.pop('private_ad')

        result = insert_an_ad(wd, AdInsert=DesktopAI, **test_data)
        self.assertTrue(result.was('pack_inactive_ad'))

        dash = Dashboard(wd)
        dash.go_dashboard()
        time.sleep(5) # waiting for automatic review pack queue
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        pack = Pack(wd)
        ad = pack.filter_ads(DashboardMyAds(wd).active_ads)
        if utils.is_not_regress():
            status = 'Aviso en revisi�n'
        else:
            status = 'Aviso publicado'
        self.assertEqual(len(ad), 1)
        for key, ad_object in ad.items():
            self.assertEqual(ad_object.status.text, status)
            self.assertTrue(ad_object.toggle_wrap.is_displayed())
        self.assertTrue(dash.link_pack.is_displayed())

    @tags('desktop', 'account', 'packs')
    def test_packauto_enable_recent_ads_after_buy_pack(self, wd):
        """ Check buy Pack 10 and after 10 recent ads published """

        login_and_go_to_dashboard(wd, '15_disabled_ads_without_packs@yapo.cl', '123123123')
        dash = Dashboard(wd)
        pack = Pack(wd)
        disabled_pack_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        ad_list = []
        for key, ad in reversed(sorted(disabled_pack_ads.items())):
            p_hour = ad.published_date.text[10:]
            if len(ad_list) >= 10: break
            if len(ad_list) == 0:
                ad_list.append([p_hour, ad.subject.text])
            last_ad = ad_list[-1][0]
            if last_ad > p_hour:
                ad_list.append([p_hour, ad.subject.text])
        self.purchase_pack(wd, '010', PacksPeriods.QUARTERLY)
        dash.go_dashboard()
        active_pack_ads = pack.filter_active_ads(DashboardMyAds(wd).active_ads)
        ad_list2 = []
        for key, ad in reversed(sorted(active_pack_ads.items())):
            p_hour2 = ad.published_date.text[10:]
            ad_list2.append([p_hour2, ad.subject.text])
        self.assertListEqual(ad_list, ad_list2, msg="Las listas difieren")

    @tags('desktop', 'account', 'packs')
    def test_pack_auto_disable_oldest_ads_after_pack_expiration(self, wd):
        """ Check that when a Pack 10 is expired the 10 oldest ads are disabled """

        login_and_go_to_dashboard(wd, '20_packs_to_disabled@yapo.cl', '123123123')
        self.purchase_pack(wd, '010', PacksPeriods.QUARTERLY)
        dash = Dashboard(wd)
        dash.go_dashboard()
        pack = Pack(wd)
        enabled_pack_ads = pack.filter_active_ads(DashboardMyAds(wd).active_ads)
        ad_list = []
        for key, ad in sorted(enabled_pack_ads.items()):
            p_hour = ad.published_date.text[7:]
            if len(ad_list) >= 10: break
            if len(ad_list) == 0:
                ad_list.append([p_hour, ad.subject.text])
            last_ad = ad_list[-1][0]
            if last_ad <= p_hour and ad_list[-1][-1] != ad.subject.text:
                ad_list.append([p_hour, ad.subject.text])
        utils.expire_packs()
        dash.go_dashboard()
        disabled_pack_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        ad_list2 = []
        for key, ad in sorted(disabled_pack_ads.items()):
            p_hour2 = ad.published_date.text[7:]
            if ad_list.count([p_hour2, ad.subject.text]) == 1:
                ad_list2.append([p_hour2, ad.subject.text])
        self.assertListEqual(ad_list, ad_list2, msg="Las listas difieren")

    def purchase_pack(self, wd, data_slots, data_period):
        summary = SummaryPayment(wd)
        # Add elements to cart
        summary.add_pack_to_cart(data_slots, data_period)
        # Confirm payment
        summary.press_link_to_pay_products()
        # Simulator
        paysim = Paysim(wd)
        paysim.pay_accept()
        # Verify cart clean

    def check_email(self, wd):
        sent_email = SentEmail(wd)
        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn('Felicitaciones, para nosotros eres todo un pro', body_text)

    def _insert_ad(self, webdriver, data):
        result = insert_an_ad(webdriver, AdInsert=DesktopAI, **data)
        self.assertTrue(result.was('success'))

class CarPackIFModalDashboard(CarPackDashboard):
    bconfs = {
        "*.controlpanel.modules.adqueue.auto.pack.timeout":"1",
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('desktop', 'account', 'packs')
    def test_pack_auto_user_without_quotas_modal_window_validation(self, wd):
        """ Check user without quotas modal window """
        dialog_text = 'Desactiva alguno de\ntus avisos publicados\npara activar �ste'
        login_and_go_to_dashboard(wd, 'enabled_disabled_normal_ads@yapo.cl', '123123123')
        pack = Pack(wd)
        disabled_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        disabled_ads[8000088].enable_toggle.click()
        modal = Modal(wd)
        if modal.price_inserting_fee.text:
            self.assertEqual(modal.without_quotas_dialog.text, dialog_text)
            self.assertEqual(modal.without_quotas_buy_pack_button.text, 'Elige un Pack')
        else:
            ## the new modal does not have any dialog_text
            self.assertEqual(modal.buy_inserting_fee_premium.text, 'Elige un Pack')

    @tags('desktop', 'account', 'packs', 'xiti')
    def test_pack_auto_user_modal_xiti_buy_pack(self, wd):
        """ Check xiti click buy pack """
        self._setup_xiti()
        login_and_go_to_dashboard(wd, 'enabled_disabled_normal_ads@yapo.cl', '123123123')
        pack = Pack(wd)
        disabled_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        disabled_ads[8000088].enable_toggle.click()
        modal = Modal(wd)
        self._check_xiti_buy_pack(modal)

    @tags('desktop', 'account', 'packs', 'xiti')
    def test_pack_auto_user_modal_xiti_buy_if(self, wd):
        """ Check xiti click buy inserting fee """
        self._setup_xiti()
        login_and_go_to_dashboard(wd, 'enabled_disabled_normal_ads@yapo.cl', '123123123')
        pack = Pack(wd)
        disabled_ads = pack.filter_disabled_ads(DashboardMyAds(wd).active_ads)
        disabled_ads[8000088].enable_toggle.click()
        modal = Modal(wd)
        self._check_xiti_buy_if(modal)

    def _setup_xiti(self):
        yapo.utils.enable_local_hit_xiti()
        yapo.utils.xiti_clear_logs()

    def _check_xiti_buy_pack(self, page_pack):
        page_pack.without_quotas_buy_pack_button.click()
        xiti_click = yapo.utils.get_click_xiti_log('ad_insert_confirmation::::::elige_pack_auto')
        self.assertEquals(xiti_click['clic'], 'N')

    def _check_xiti_buy_if(self, page_if):
        page_if.buy_inserting_fee.click()
        xiti_click = yapo.utils.get_click_xiti_log('ad_insert_confirmation::::::paga_aviso_auto')
        self.assertEquals(xiti_click['clic'], 'N')
