# -*- coding: latin-1 -*-
import yapo, time
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import DashboardMyAds, SummaryPayment
from yapo.pages.packs import BuyPacks
from yapo.pages.simulator import Paysim

class BuyNDelete(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']

    @tags('desktop', 'account', 'packs', 'dashboard', 'validations')
    def test_functionalty(self, wd):
        """ After buying a pack, deleting any ad should deactivate it for 7 days (DTB) """
        login_and_go_to_dashboard(wd, "cuentaproconpack@yapo.cl", "123123123")

        # Expire the user's pack
        yapo.utils.trans('expire_packs', pack_id='2')

        # delete the only ad the user has
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        dash_my_ads.delete_ad_by_subject("Audi A4 2011")

        yapo.utils.rebuild_index()

        # the ad should be inactive and no ad left active
        dash_my_ads.go_dashboard()
        self.assertFalse(bool(dash_my_ads.active_ads))
        self.assertTrue(len(dash_my_ads.list_inactive_ads) > 0)

        # buy a pack
        buy_pack = BuyPacks(wd)
        buy_pack.go()
        buy_pack.select_pack("020", yapo.utils.PacksPeriods.QUARTERLY)
        summary = SummaryPayment(wd)
        summary.change_doc_type('BOLETA')
        summary.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()

        # This user should have no active, but only one inactive ad
        dash_my_ads.go_dashboard()
        self.assertFalse(bool(dash_my_ads.active_ads))
        self.assertTrue(len(dash_my_ads.list_inactive_ads) == 1)
