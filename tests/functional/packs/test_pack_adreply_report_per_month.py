# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.packs import DashboardPacks, DashboardPacksAdReply

class PackMonthlyAdReplyTest(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_prepaid3_11ads_2packs']
    account_mail = "prepaid3@blocket.se"
    account_password = "123123123"
    cases = [
        {
            "month": "0",
            "expected_text": "Un documento CSV fue enviado al correo",
            "logout": False
        },
        {
            "month": "-2",
            "expected_text": "No existen contactos para este mes",
            "logout": False
        },
        {
            "month": "0",
            "expected_text": "Ha ocurrido un problema, intente de nuevo m�s tarde.",
            "logout": True
        }
    ]

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_prepaid3_11ads_2packs'])
        utils.rebuild_asearch()
        utils.flush_redises()

    def _execute_test(self, dashboard_packs_adreply_report, case):
        if case["logout"]:
            dashboard_packs_adreply_report.logout_on_new_tab()
        dashboard_packs_adreply_report.fill_form(case['month'], self.account_mail)
        assert dashboard_packs_adreply_report.error_msj.text == case['expected_text']

    def test_get_pack_monthly_cases(self, wd):
        login_and_go_to_dashboard(wd, self.account_mail, self.account_password)
        dashboard_packs_adreply_report = DashboardPacksAdReply(wd)
        dashboard_packs_adreply_report.go()
        for c in self.cases:
            self._execute_test(dashboard_packs_adreply_report, c)
