# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.utils import Products, PacksPeriods
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds, Dashboard
from yapo.pages.packs import BuyPacks 
from yapo.pages.ad_insert import AdInsert
from yapo import steps
import time

class PacksOnShoppingCart(yapo.SeleniumTest):
    snaps = ['accounts', 'diff_pack_cars1']

    bconfs = {
        '*.*.payment_settings.product.1.1.*.value': 'price:1050',
    }
    
    @tags('payment','pack')
    def test_select_multiple_packs(self, wd):
        """ Add multiple packs into shopping cart """
        utils.flush_redises()
        login = Login(wd)
        login.login('cuentaproconpack@yapo.cl','123123123')
        buy_pack = BuyPacks(wd) 
        buy_pack.go()
        buy_pack.select_pack("020",PacksPeriods.QUARTERLY)

        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', '']
        ]

        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

        buy_pack.go()
        buy_pack.select_pack("030",PacksPeriods.BIANNUAL)

        table_values = summary_page.get_table_data('products_list_table')
        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', ''],
            ['Pack Auto', '30/semestral', '$ 486.000', '']
        ]

        table_values.sort()
        expected_values.sort()
        self.assertListEqual(table_values, expected_values)

    @tags('payment','pack')
    def test_select_the_same_pack(self, wd):
        """ Try to add the same pack two times """
        utils.flush_redises()
        login = Login(wd)
        login.login('cuentaproconpack@yapo.cl','123123123')
        buy_pack = BuyPacks(wd) 
        buy_pack.go()
        buy_pack.select_pack("020",PacksPeriods.QUARTERLY)

        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', '']
        ]

        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

        buy_pack.go()
        buy_pack.select_pack("020",PacksPeriods.QUARTERLY)

        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

    @tags('payment','pack')
    def test_error_msj_pack(self, wd):
        """ Error message is displayed when the slots and period wasn't selected and you try to buy """
        utils.flush_redises()
        login = Login(wd)
        login.login('cuentaproconpack@yapo.cl','123123123')
        buy_pack = BuyPacks(wd) 
        buy_pack.go()
        buy_pack.pay_button.click()
        self.assertIn('pack-autos', wd.current_url)

    @tags('payment','pack')
    def test_check_price_pack(self, wd):
        """ Selectors of pay pack page changes the price """
        utils.flush_redises()
        email = 'cuentaproconpack@yapo.cl'
        login = Login(wd)
        login.login(email, '123123123')

        ad_insert = AdInsert(wd)
        ad_insert.go()
        steps.insert_an_ad(wd,
            subject="Auto",
            body="yoLo",
            region=15,
            category=2020,
            price=3500000,
            accept_conditions=True,
            brand=31,
            model=44,
            version=1,
            regdate=2010,
            gearbox=1,
            fuel=1,
            cartype=1,
            mileage=15000,
            passwd='$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
            email=email,
            email_confirm=email)
        ad_insert.submit.click()

        buy_pack = BuyPacks(wd) 
        buy_pack.go()
        buy_pack.select_slots('020')
        buy_pack.select_period(PacksPeriods.QUARTERLY)
        self.assertEqual(buy_pack.price.text, '189.000')
        buy_pack.select_slots('030')
        self.assertEqual(buy_pack.price.text, '267.000')
        buy_pack.select_period(PacksPeriods.BIANNUAL)
        self.assertEqual(buy_pack.price.text, '486.000')


    @tags('payment','pack')
    def test_add_pack_with_other_products(self, wd):
        """ Add a pack into shopping cart not empty """
        utils.flush_redises()
        login = Login(wd)
        login.login('cuentaproconpack@yapo.cl','123123123')
        summary_page = SummaryPayment(wd)

        summary_page.add_element_to_cart(Products.BUMP, 8000070 )
        time.sleep(3)
        summary_page = SummaryPayment(wd)
        buy_pack = BuyPacks(wd) 
        buy_pack.go()
        buy_pack.select_pack("020",PacksPeriods.QUARTERLY)

        expected_values = [
            ['Libro 3', 'Subir Ahora', '$ 1.050', ''],
            ['Pack Auto', '20/trimestral', '$ 189.000', '']
        ]

        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

    @tags('payment', 'pack')
    def test_remove_pack_from_cart(self, wd):
        """ Remove pack from shopping cart """
        utils.flush_redises()
        login = Login(wd)
        login.login('cuentaproconpack@yapo.cl','123123123')
        summary_page = SummaryPayment(wd)
        summary_page.add_pack_to_cart('020',PacksPeriods.QUARTERLY)
        summary_page.add_pack_to_cart('020',PacksPeriods.BIANNUAL)
        summary_page.add_pack_to_cart('020',PacksPeriods.ANNUAL)

        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', ''],
            ['Pack Auto', '20/semestral', '$ 342.000', ''],
            ['Pack Auto', '20/anual', '$ 636.000', ''],
        ]

        table_values = summary_page.get_table_data('products_list_table')
        table_values.sort()
        expected_values.sort()
        self.assertListEqual(table_values, expected_values)

        summary_page.remove_product(1)
        time.sleep(5)

        expected_values = [
            ['Pack Auto', '20/trimestral', '$ 189.000', ''],
            ['Pack Auto', '20/anual', '$ 636.000', ''],
        ]

        table_values = summary_page.get_table_data('products_list_table')
        table_values.sort()
        expected_values.sort()
        self.assertListEqual(table_values, expected_values)

class MultipleAdsShoppingCart(yapo.SeleniumTest):
    snaps = ['accounts']
    bconfs = {
        '*.*.autobump.visible_last_digit.1': 0,
    }

    @tags('shopping_cart', 'bump')
    def test_select_multiple_ads_to_bump(self, wd):
        """Select multiple ads to bump and check the link to ad works"""

        login = Login(wd)
        login.login('prepaid5@blocket.se','123123123')
        dashboard = Dashboard(wd)
        dashboard.go_my_ads()
        dashboard = DashboardMyAds(wd)

        time.sleep(1)
        wd.execute_script("window.scrollTo(0, 100000);") # because floating elements, sue me.
        active_ad = dashboard.active_ads[8000073]
        active_ad.select_product(utils.Products.DAILY_BUMP)


        dashboard = DashboardMyAds(wd)
        active_ad = dashboard.active_ads[8000065]
        active_ad.select_product(utils.Products.BUMP)

        dashboard = DashboardMyAds(wd)
        active_ad = dashboard.active_ads[6000667]
        active_ad.select_product(utils.Products.WEEKLY_BUMP)

        dashboard.checkout_button.click()

