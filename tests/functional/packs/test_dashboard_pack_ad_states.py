# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertResult
from yapo.pages.desktop import DashboardMyAds, AdEdit, Pack, Dashboard
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.ad_insert import AdInsertSuccess
from yapo.steps import trans
from selenium.common.exceptions import NoSuchElementException
from yapo  import utils
import yapo
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI

class DashboarPackCarsAdStates(yapo.SeleniumTest):

    snaps = ['accounts','diff_pack_cars1']
    bconfs = {'*.controlpanel.modules.adqueue.auto.pack.timeout': '60'}
    deleting_ad_text = 'Borrando aviso'

    @tags('desktop', 'account', 'packs')
    def test_edit_ad_toggle_disabled_not_in_asearch(self, wd):
        """
            Check pack edit ad not in asearch, toggle disabled.
        """

        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')

        ad_data = {
                'subject' : 'Audi disabled para edicion',
                'status' : 'Aviso publicado',
                'vi_link_enabled' : False,
                'options_enabled' : True,
                'bumps_enabled' : False,
                'gallery_enabled' : False,
                'email': 'cuentaproconpack@yapo.cl',
        }

        self._insert_ad(wd, ad_data)

        trans.review_ad('cuentaproconpack@yapo.cl', ad_data['subject'])

        self._check_dashboard_ad(wd, ad_data)
        self._change_toogle(wd, ad_data, 'inactive')
        utils.rebuild_index()

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        dash_my_ads.edit_ad_by_subject(ad_data['subject'])

        edit = AdEdit(wd)
        edit.insert_ad(subject="Audi disabled editado")
        edit.submit.click()
        result = AdInsertSuccess(wd)

        ad_data.update(status='Revisando edici�n')
        self._check_dashboard_ad(wd, ad_data)


    @tags('desktop', 'account', 'packs')
    def test_pack_ad_edit_toogle_disabled_not_in_asearch(self, wd):
        """
            Check pack edit ad  not in asearch, toggle disabled.
        """

        login_and_go_to_dashboard(wd, 'cuentaprosincupos@yapo.cl', '123123123')

        ad_data = {
                'subject' : 'Audi disabled no en asearch',
                'status' : 'Aviso publicado',
                'vi_link_enabled' : True,
                'options_enabled' : True,
                'bumps_enabled' : True,
                'gallery_enabled' : False,
                'email': 'cuentaprosincupos@yapo.cl',
        }

        self._insert_ad(wd, ad_data)

        trans.review_ad('cuentaprosincupos@yapo.cl', ad_data['subject'])

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        dash_my_ads.edit_ad_by_subject(ad_data['subject'])

        edit = AdEdit(wd)
        edit.insert_ad(subject="{} editado".format(ad_data['subject']))
        edit.submit.click()

        ad_data.update(status='Revisando edici�n',vi_link_enabled = False, bumps_enabled = False)
        self._check_dashboard_ad(wd, ad_data)

    @tags('desktop', 'account', 'packs')
    def test_pack_contact_list_search_box(self, wd):
        """ Contact list supports specials chars like quotes """

        ad_data = {
            'subject' : 'Aviso inmuebles con "comillas"',
            'email': 'cuentaproconpack@yapo.cl',
        }

        login_and_go_to_dashboard(wd, ad_data['email'], '123123123')

        self._insert_ad(wd, ad_data)
        trans.review_ad('cuentaproconpack@yapo.cl', ad_data['subject'])
        utils.rebuild_index()

        dashboard = Dashboard(wd)
        pack = dashboard.go_pack()

        pack.fill_form(contact_list='Aviso')

        self.assertEqual(ad_data['subject'], pack.search_list.text)


    def _check_dashboard_ad(self, wd, ad_data):
        print('AD SUBJECT:{}'.format(ad_data['subject']))

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        self.assertIsNotNone(active_ad)

        #check ad status
        self.assertEqual(active_ad.status.text, ad_data['status'])

        if ad_data['vi_link_enabled']:
            self.assertIsNotNone(active_ad.subject.get_attribute('href'))
        else:
            self.assertIsNone(active_ad.subject.get_attribute('href'))

        if ad_data['bumps_enabled']:
            active_ad.wait.until_visible('bumps_enabled')
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.bumps_enabled)

        if ad_data['gallery_enabled']:
            self.assertIsNotNone(active_ad.gallery_enabled)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.gallery_enabled)

        ##check edit and delete
        if ad_data['options_enabled']:
            self.assertIsNotNone(active_ad.options_buttons)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.options_buttons)


    def _change_toogle(self, wd, ad_data, status):

        dash_my_ads = DashboardMyAds(wd)
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        pack = Pack(wd)
        pack.change_status(active_ad, status)

    def _insert_ad(self, wd, ad_data):

        ad_form_data = {
            "subject": ad_data['subject'],
            "body": "yoLo yola",
            "region": "15",
            "communes": 331,
            "category": "2020",
            "price": 999666,
            "brand": 31,
            "model": 44,
            "version": 1,
            "regdate": 2010,
            "gearbox": 1,
            "fuel":1,
            "cartype":1,
            "mileage":15000,
            "plates":'ABCD10',
            #"logged": True
            "email": ad_data['email'],
            "passwd": '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
        }

        return insert_an_ad(wd, AdInsert=None, submit=True, **ad_form_data)

class DashboarPackInmoAdStates(DashboarPackCarsAdStates):

    snaps = ['accounts','diff_pack_inmo1']
    bconfs = {'*.controlpanel.modules.adqueue.auto.pack.timeout': '60'}
    deleting_ad_text = 'Aviso inactivo'


    def _insert_ad(self, wd, ad_data):

        ad_form_data = {
            "subject": ad_data['subject'],
            "body": "yoLo yola",
            "region": "15",
            "communes": 331,
            "category": "1220",
            "estate_type":"1",
            "rooms":"1",
            "bathrooms":"1",
            "price": 999666,
            "email": ad_data['email'],
            "passwd": '$1024$2y1bQ}VYRD}zF4QCbf07a971b784888658a20e62156de64dfde5ffb6',
        }

        return insert_an_ad(wd, AdInsert=None, submit=True, **ad_form_data)
