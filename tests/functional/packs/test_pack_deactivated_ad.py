# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import SummaryPayment
from yapo.pages.simulator import Paysim, PaymentError
from yapo import utils
from yapo import conf
from yapo.trans import change_ad_status
from redis import StrictRedis
import yapo

class PackDeactivatedAd(yapo.SeleniumTest):
    list_id = 8000093
    email = 'cuentaproconpack@yapo.cl'
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    def setUp(self):
        utils.flush_redises()
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_inmo1'])
        utils.rebuild_asearch()

    @tags('accounts', 'packs', 'products')
    def test_01_buy_bump_to_ad_deactivated_with_pack(self, wd):
        """Tested Error buy bump to ad deactivated with pack"""
        # Deactivated the Ad
        change_ad_status({
            'list_id': self.list_id,
            'new_status': 'deactivated',
            'deletion_reason': 1
        })
        # Add ad with pack at the shopping car
        redis = StrictRedis(
            host='localhost',
            port=int(conf.REGRESS_REDISACCOUNTS_PORT),
            db=2
        )
        redis.hset("rad1:cart_info:{}:1".format(self.email), self.list_id, '{\"p\":\"900\"}')
        # Go to shopping car
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        summary = SummaryPayment(wd)
        summary.go_summary()
        # Buying the product
        summary.change_doc_type(None)
        summary.press_link_to_pay_products()
        paysim = Paysim(wd)
        paysim.pay_accept()
        # Expected error page
        payment_error = PaymentError(wd)
        self.assertEquals(payment_error.title.text, 'Oops... La transacci�n no pudo ser realizada')
        self.assertEquals(payment_error.subtitle.text, 'Parece que hubo un problema con tu compra 175 y no pudo completarse el pago.')

    @tags('accounts', 'packs', 'products')
    def test_02_not_add_to_shopping_cart_ad_deactivated_with_pack(self, wd):
        """Tested try add to cart a 'ad deactivate' with pack"""
        summary = SummaryPayment(wd)

        # Deactivated ad with pack
        change_ad_status({
            'list_id': '8000093',
            'new_status': 'deactivated',
            'deletion_reason': 1
        })

        # Go to landing url for add list_id to shopping cart
        params_url = [
            ('id', self.list_id),
            ('a', 3),
            ('h', 'cb47a6f5d16687751d81e6f4e4b89a27429baeb8'),
            ('cmd', 'bump')
        ]
        url = conf.SSL_URL + '/landing?' + '&'.join(['{0}={1}'.format(k, v) for k,v in params_url])
        print(url)
        wd.get(url)

        # Verify that element is not added in the shopping cart
        from selenium.common.exceptions import NoSuchElementException
        with self.assertRaises(NoSuchElementException):
            list_elements_shopping_cart = summary.products_list_table

    @tags('accounts', 'packs', 'products')
    def test_03_delete_ad_deactivated_with_pack(self, wd):
        """ Tested deleted an ad deactivated with pack expired"""
        from yapo.pages.mobile import DeletePage

        # Disabled one ad of cars, with expire_packs
        result = utils.trans('expire_packs', pack_id=2)
        self.assertEqual(result['status'], 'TRANS_OK')
        self.assertEqual(result['disabled_ads'], '1')

        login_and_go_to_dashboard(wd, email='cuentaproconpack@yapo.cl')
        delete_page = DeletePage(wd)
        delete_page.go(list_id='8000093')
        delete_page.delete_ad_logged()
        delete_page.wait.until_visible('result_message')
        # Deactivate ad
        text_result = 'Tu aviso ser� desactivado en los pr�ximos minutos, luego de 7 d�as ser� eliminado.'
        self.assertEqual(delete_page.result_message.text, text_result)
