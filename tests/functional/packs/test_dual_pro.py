# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop import Login
from yapo.pages.ad_insert import AdInsert
import yapo
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI


class DualProBehaviour(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_dual']

    @tags('desktop', 'account', 'packs', 'dual_pro')
    def test_dual_pro_page(self, wd):
        """Check the dual pro page when a user is pro in two categories"""

        data = {
            'region': '15',
            'category': '1220',
            'communes': '331',
            'subject': 'House AD 1',
            'body': 'House for check Email',
            'price': '3500000',
            'estate_type': '1',
            'rooms': '1',
            'bathrooms': '1',
            'accept_conditions': True
        }

        email = 'many@ads.cl'
        passwd = '123123123'
        login = Login(wd)
        login.login(email, passwd)

        insert_an_ad(wd, AdInsert=DesktopAI, Submit=True, logged=True,  **data)
        ai = AdInsert(wd)
        self.assertTrue('Llegaste al l�mite de avisos en la categor�a', ai.packDualPro_first_title.text)
        self.assertTrue('Inmobiliaria como usuario Privado.', ai.packDualPro_second_title.text)
