# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages import control_panel
import yapo 
import time

class PacksInSalesReportWebSQL(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_prepaid3_11ads_2packs']

    @tags('controlpanel', 'websql', 'report')
    def test_websql_packs_in_sales_report(self, wd):
        """ Websql sales report for Packs """
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Sales Report')
        result_data = websql.get_result_data()

        # testing
        data = [
            ['166', time.strftime("%Y-%m-%d"), u'prepaid3@blocket.se', u'Profesional', u'Pack Auto 10/mensual', u'2', u'-', u'-', u'-', u'paid', u'', u'', u'', u'23000', u'', u'pending', u'invoice', u'', 'unknown', 'webpay'],
            ['178', time.strftime("%Y-%m-%d"), u'prepaid3@blocket.se', u'Profesional', u'Pack Auto 10/mensual', u'3', u'-', u'-', u'-', u'paid', u'', u'', u'', u'23000', u'', u'pending', u'invoice', u'', 'unknown', 'webpay'],
        ]

        for row in range(len(data)):
            result_data[row].pop(2)
            self.assertListEqual(data[row], result_data[row])
