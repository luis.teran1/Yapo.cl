# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.steps.pay_products_logged import transbank_pay_product
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
from yapo.pages.ad_insert import AdInsert, PackConfirm
from yapo.pages.desktop_list import AdDeletePage
from yapo.pages.desktop import SummaryPayment, DashboardMyAds, Pack, Dashboard, Payment
from yapo.pages.packs import DashboardPacks, BuyPacks
from yapo.pages.generic import SentEmail
from yapo.pages.simulator import Paysim
from yapo import utils
import yapo
import datetime
import time

TIME = 20


class PackPremiumProductBase(yapo.SeleniumTest):

    bconfs = {"*.*.payment.product_specific.weekly_bump.interval_seconds": TIME,
              "*.*.payment.product_specific.daily_bump.interval_seconds": TIME,
              "*.*.payment.product.3.expire_time": '1 minute',
              "*.*.payment.product.32.expire_time": '31 days',
              '*.*.autobump.visible_last_digit.5': 0,
              "*.*.upselling_settings.command.3.default_params.3.value":"expire_time:1 minute",
              "*.*.upselling_settings.command.32.default_params.3.value":"expire_time:31 days",
              "*.*.packs.autos.slots.005.visible": "desktop,controlpanel,telesales",
              "*.*.packs.inmobiliario.slots.005.visible": "desktop,controlpanel,telesales",
              } #gallery 7 expire time

    @staticmethod
    def _rebuild_index_get_ad(wd, list_id):
        started_at = time.time()
        utils.rebuild_index()
        rebuild_took = time.time() - started_at
        print( "Rebuild took: {0}".format(rebuild_took))
        wd.refresh()
        dashboard = DashboardMyAds(wd)
        ad = dashboard.active_ads[list_id]
        return ad, rebuild_took

    @staticmethod
    def _delete_ad(wd, list_id):
        """ Delete ad by list_id, you must be logged in """
        ad_delete = AdDeletePage(wd)
        ad_delete.go(list_id)
        ad_delete.delete_ad_logged()
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()

    @staticmethod
    def _buy_product(wd, product, list_id):
        """ Buy premium product for the given list_id and the goes to dashboard """
        transbank_pay_product(wd, list_id, product)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        return dashboard


class PackPremiumProductSlowTest(PackPremiumProductBase):

    """ Do we REALLY need to load the snaps every damn time? """

    test_packs1 = [["020", utils.PacksPeriods.QUARTERLY]]
    test_packs2 = []
    test_acc1 = "prepaid3@blocket.se"
    test_acc2 = "prepaid3@blocket.se"
    list_id1 = 8000096
    list_id2 = 8000086

    def _insert_ad(self, wd):
        # Inserting a car ad
        insert_an_ad(wd, AdInsert=DesktopAI,
                            submit=True,
                            logged=True,
                            body="yoLo",
                            region=15,
                            communes=331,
                            category=2020,
                            price=3500000,
                            email="prepaid3@blocket.se",
                            accept_conditions=True,
                            brand=31,
                            model=44,
                            version=1,
                            regdate=2010,
                            gearbox=1,
                            fuel=1,
                            cartype=1,
                            mileage=15000)

    def _buy_packs(self, wd, packs):
        for p in packs:
            # buying an extra pack
            buy_pack = BuyPacks(wd)
            buy_pack.go()
            buy_pack.select_pack(p[0], p[1])
            summary = SummaryPayment(wd)
            summary.change_doc_type(Payment.BILL)
            summary.press_link_to_pay_products()
            paysim = Paysim(wd)
            paysim.pay_accept()

    def setUp(self):
        utils.flush_redises()
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_prepaid3_11ads_2packs', 'diff_prepaid3_cat_2040_ads'])
        utils.rebuild_asearch()
        self.today_string = datetime.date.today().strftime("%d/%m/%Y")
        self.tomorrow_string = (datetime.date.today() + datetime.timedelta(days=1)).strftime("%d/%m/%Y")
        self.row_data1 = [self.today_string, self.today_string, '20/trimestral', '0']
        self.row_data2 = [self.today_string, self.tomorrow_string, '10/mensual', '2']
        self.row_data3 = [self.today_string, self.today_string, '10/mensual', '10']
        self.t2_row_data1 = [self.today_string, self.tomorrow_string, '10/mensual', '1']
        self.t2_row_data2 = [self.today_string, self.today_string, '10/mensual', '10']
        self.t2_row_data3 = []

    @tags('desktop', 'account', 'packs', 'dashboard', 'ad_insert')
    def test_oldest_pack_is_used(self, wd):
        """ Test oldest pack is used """
        login_and_go_to_dashboard(wd, self.test_acc1, '123123123')
        dashboard_packs = DashboardPacks(wd)
        dashboard_packs.go()

        self._buy_packs(wd, self.test_packs1)
        self._insert_ad(wd)

        dashboard_packs = DashboardPacks(wd)
        dashboard_packs.go()
        self.assertEqual(dashboard_packs.get_row(0), self.row_data1)
        self.assertEqual(dashboard_packs.get_row(1), self.row_data2)
        if self.row_data3 != []:
            self.assertEqual(dashboard_packs.get_row(2), self.row_data3)

    @tags('desktop', 'account', 'packs', 'dashboard', 'products', 'gallery')
    def test_pack_disabled_ad_with_gallery_7(self, wd):
        """ Testing that premium products Gallery 7 is still working if the user has his/her ads disabled """
        product = utils.Products.GALLERY
        message_l = '�ltimo minuto'
        message_1 = '1 minutos restante'
        list_id = self.list_id2
        login_and_go_to_dashboard(wd, self.test_acc2, '123123123')
        transbank_pay_product(wd, list_id, product)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[list_id]
        self.assertTrue((message_l in ad.active_gallery_message.text) or (message_1 in ad.active_gallery_message.text))
        pack = Pack(wd)
        pack.change_status(ad, 'inactive')
        self.assertTrue((message_l in ad.active_gallery_message.text) or (message_1 in ad.active_gallery_message.text))

    @tags('desktop', 'account', 'packs', 'dashboard', 'products', 'gallery')
    def test_pack_disabled_ad_with_gallery_30(self, wd):
        """ Testing that premium products Gallery 30 is still working if the user has his/her ads disabled """
        product = utils.Products.GALLERY_30
        message_30 = '30 D�as restantes'
        message_31 = '31 D�as restantes'
        list_id = self.list_id1
        login_and_go_to_dashboard(wd, self.test_acc2, '123123123')
        transbank_pay_product(wd, list_id, product)
        dashboard = DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[list_id]
        self.assertTrue((message_30 in ad.active_gallery_message.text) or (message_31 in ad.active_gallery_message.text))
        pack = Pack(wd)
        pack.change_status(ad, 'inactive')
        self.assertTrue((message_30 in ad.active_gallery_message.text) or (message_31 in ad.active_gallery_message.text))

class PackPremiumProductTest(PackPremiumProductBase):

    """ These tests may fail if it takes too long to rebuild the index, sorry about that """

    snaps = ['accounts', 'diff_prepaid3_11ads_2packs', 'diff_prepaid3_cat_2040_ads']


    @tags('desktop', 'account', 'packs', 'dashboard', 'validations')
    def test_pack_dashboard_support_form_validations(self, wd):
        """ Test pack dashboard support form validations """

        login_and_go_to_dashboard(wd, 'prepaid3@blocket.se', '123123123')
        dashboard_packs = DashboardPacks(wd)
        dashboard_packs.go()
        dashboard_packs.next.click()
        dashboard_packs.wait.until_visible('call_name_error')
        self.assertEqual(dashboard_packs.call_name_error.text, 'Escribe un nombre.')
        dashboard_packs.call_name.send_keys('Luis')
        dashboard_packs.next.click()
        dashboard_packs.next.click()
        dashboard_packs.wait.until_visible('call_mail_error')
        self.assertEqual(dashboard_packs.call_mail_error.text, 'Escribe un correo v�lido.')
        dashboard_packs.call_mail.send_keys('luis@yolo.cl')
        dashboard_packs.next.click()
        dashboard_packs.next.click()
        dashboard_packs.wait.until_visible('call_phone_error')
        self.assertEqual(dashboard_packs.call_phone_error.text, 'Escribe un n�mero v�lido.')
        dashboard_packs.call_phone.send_keys('12312312312')
        dashboard_packs.next.click()
        dashboard_packs.wait.until_visible('call_message')
        self.assertEqual('Listo, te llamaremos en los pr�ximos minutos.', dashboard_packs.call_message.text)

class PackInmoPremiumProductSlowTest (PackPremiumProductSlowTest):

    test_packs1 = [ ["005", utils.PacksPeriods.QUARTERLY],
                    ["010", utils.PacksPeriods.QUARTERLY] ]
    test_packs2 = [ ["005", utils.PacksPeriods.QUARTERLY],
                    ["010", utils.PacksPeriods.QUARTERLY],
                    ["005", utils.PacksPeriods.QUARTERLY] ]
    test_acc1 = "cuentaproconpack@yapo.cl"
    test_acc2 = "cuentaproconpack@yapo.cl"
    list_id1 = 8000093
    list_id2 = 8000093

    def setUp(self):
        utils.flush_redises()
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_inmo1'])
        utils.rebuild_asearch()
        self.today_string = datetime.date.today().strftime("%d/%m/%Y")
        self.tomorrow_string = (datetime.date.today() + datetime.timedelta(days=1)).strftime("%d/%m/%Y")
        self.row_data1 = [self.today_string, self.today_string, '10/trimestral', '0']
        self.row_data2 = [self.today_string, self.today_string, '5/trimestral', '0']
        self.row_data3 = ['09/03/2015', self.tomorrow_string, '10/mensual', '2']
        self.t2_row_data1 = [self.today_string, self.today_string, '5/trimestral', '0']
        self.t2_row_data2 = [self.today_string, self.today_string, '10/trimestral', '0']
        self.t2_row_data3 = [self.today_string, self.today_string, '5/trimestral', '0']

    def _insert_ad(self, wd):
        # Inserting a car ad
        insert_an_ad(wd, AdInsert=DesktopAI,
                            submit=True,
                            logged=True,
                            subject="Casa",
                            body="yoLo",
                            region=15,
                            communes=331,
                            category=1220,
                            estate_type=1,
                            rooms=1,
                            bathrooms=1,
                            price=3500000,
                            email="cuentaproconpack@yapo.cl",
                            accept_conditions=True)
