# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import DashboardMyAds, Pack, AdEdit
from yapo.pages.desktop_list import AdDeletePage
from yapo.pages import mobile
from yapo  import utils
import yapo


class PackDisabledAdsDesktop(yapo.SeleniumTest):

    """ Testing that premium products are not buyable if are disabled  """

    snaps = ['accounts', 'diff_prepaid3_11ads_2packs']

    @tags('desktop', 'packs', 'products', 'disabled_ads', 'ad_delete', 'ad_edit')
    def test_pack_disabled_ad(self, wd):
        """ Testing that product: bump is not shown when the user deletes/edits a disabled ad  """

        list_id = 8000094
        login_and_go_to_dashboard(wd, 'prepaid3@blocket.se', '123123123')
        dashboard = DashboardMyAds(wd)
        ad = dashboard.active_ads[list_id]
        pack = Pack(wd)
        pack.change_status(ad, 'inactive')
        utils.rebuild_index()
        wd.implicitly_wait(0.5)
        self._delete_no_product(wd, list_id)
        self._edit_no_product(wd, list_id)

    def _delete_no_product(self, wd, list_id):
        """ Testing that product bump is not shown when the user deletes a disabled ad  """
        ad_delete = AdDeletePage(wd)
        ad_delete.go(list_id)
        self.assertFalse(ad_delete.is_element_present('lightbox_btn'))

        for reason in range(4, 7):
            ad_delete.select_reason(reason)
            self.assertFalse(ad_delete.is_element_present('bum_btn_{0}'.format(reason)))
            self.assertFalse(ad_delete.is_element_present('bump_msg_{0}'.format(reason)))

    def _edit_no_product(self, wd, list_id):
        """ Testing that product bump is not shown when the user edits a disabled ad  """
        ad_edit = AdEdit(wd)
        ad_edit.go(list_id)
        self.assertFalse(ad_edit.is_element_present('bump_container'))
        self.assertFalse(ad_edit.is_element_present('more_info'))


class PackDisabledAdsMobile(yapo.SeleniumTest):

    """ Testing that premium products are not buyable if are disabled in mobile """

    snaps = ['accounts', 'diff_prepaid3_11ads_2packs']
    user_agent = utils.UserAgents.IOS
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('mobile', 'packs', 'products', 'disabled_ads', 'ad_delete', 'ad_edit')
    def test_pack_disabled_ad(self, wd):
        """ Testing that product: bump is not shown when the user deletes/edits a disabled ad in mobile, and the ad at dashboard """

        list_id = 8000094
        args = {'list_id': list_id, 'new_status': 'disabled', 'email': 'prepaid3@blocket.se',
                'remote_addr': '10.0.1.208'}
        utils.trans(cmd='pack_change_ad_status', commit=1, **args)
        utils.rebuild_index()
        wd.implicitly_wait(0.5)

        login = mobile.Login(wd)
        login.login('prepaid3@blocket.se', '123123123')

        self._delete_no_product(wd, list_id)
        self._edit_no_product(wd, list_id)
        self._at_dashboard(wd, list_id)

    def _delete_no_product(self, wd, list_id):
        """ Testing that product bump is not shown when the user deletes a disabled ad in mobile  """
        ad_delete = mobile.DeletePage(wd)
        ad_delete.go(list_id)
        self.assertEqual(len(ad_delete.bump_promos), 0)

    def _edit_no_product(self, wd, list_id):
        """ Testing that product bump is not shown when the user edits a disabled ad in mobile """
        ad_edit = mobile.AdEdit(wd)
        ad_edit.go(list_id)
        self.assertFalse(ad_edit.is_element_present('bump'))

    def _at_dashboard(self, wd, list_id):
        """ Testing a disabled ad in mobile """
        dashboard = mobile.DashboardMyAds(wd)
        dashboard.go_dashboard()
        ad = dashboard.active_ads[list_id]
        self.assertEqual(ad.subject.tag_name, 'p')
        self.assertFalse(ad.is_element_present('highlight'))
        self.assertIn('-inactive', ad.edit_button.get_attribute('class'))
        self.assertIn('-inactive', ad.delete_button.get_attribute('class'))
