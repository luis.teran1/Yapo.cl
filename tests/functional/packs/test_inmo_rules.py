# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.packs import DashboardPacks
from yapo.steps.accounts import login_and_go_to_dashboard
import yapo


class PackInmoRules(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_inmo1']

    def test_the_packs(self, wd):
        """ Test that the pack widged is showing the correct packs """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        dashboard_packs = DashboardPacks(wd)
        dashboard_packs.go()
        options = [
                'Cupos del Pack',
                ' 125 cupos',
                ' 100 cupos',
                ' 75 cupos',
                ' 50 cupos',
                ' 40 cupos',
                ' 30 cupos',
                ' 20 cupos',
                ' 10 cupos',
                'Periodo del Pack',
                'Mensual',
                'Trimestral',
                'Semestral',
                'Anual']
        self.assertEquals(options, dashboard_packs.get_packs_options())
