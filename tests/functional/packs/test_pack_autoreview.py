# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.ad_insert import AdInsert, AdInsertSuccess, PackConfirm
from yapo.pages.generic import SentEmail
from yapo.pages.desktop import DashboardMyAds, AdEdit
from yapo.pages.desktop_list import AdDeletePage
from yapo.pages.control_panel import ControlPanel
from yapo  import utils
from yapo.steps import insert_an_ad
from yapo.pages.newai import DesktopAI
import yapo
import time

class CarPackAutoReview(yapo.SeleniumTest):

    bconfs = { "*.controlpanel.modules.adqueue.auto.pack.timeout":"1" }
    test_data = {
        "subject":"",
        "body":"yoLo",
        "region":"15",
        "communes":"331",
        "category":"2020",
        "price":"3500000",
        "accept_conditions":"True",
        "brand":"31",
        "model":"44",
        "version":"1",
        "regdate":"2010",
        "gearbox":"1",
        "fuel":"1",
        "cartype":"1",
        "mileage":"15000",
        "logged":True
    }

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_pack_cars1'])
        utils.rebuild_asearch()

    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_auto_review_insert_ad(self, wd):
        """ check auto review for new ad with account pack"""
        subject = 'FORD MUSTANG 2010 Test test'
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        self._insert_ad(wd, subject)

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject(subject), 'Aviso publicado')
        self.assertEqual(len(dash_my_ads.list_all_ads), 2)


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_auto_review_edit_ad_from_dashboard(self, wd):
        """Check auto review for edit ad with pack"""
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.edit_ad_by_subject("Audi A4 2011")

        edit = AdEdit(wd)
        edit.insert_ad(subject="Audi A4 2011 edited")
        edit.submit.click()

        success_page = AdInsertSuccess(wd)
        self.assertIn('Audi A4 2011 edited', success_page.success_message.text)

        time.sleep(3) # waiting for automatic review pack queue

        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Audi A4 2011 edited'), 'Aviso publicado')


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_auto_review_edit_ad_from_mail_after_post_refused(self, wd):
        """Check edit ad from mail after post refused """

        # Refused ad by CP
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.post_refuse_ad_by_subject(subject = 'Audi A4 2011')
        cp.logout()

        #Check ad in Dashboard, is none
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertIsNone(dash_my_ads.search_status_by_subject('Audi A4 2011'))

        #Edit by email
        sent_email = SentEmail(wd)
        sent_email.go()
        sent_email.body.find_element_by_partial_link_text('aqu�').click()
        ad_insert = AdInsert(wd)
        ad_insert.subject.send_keys(' edited')
        ad_insert.fill_form(communes=331, accept_conditions=True)
        ad_insert.submit.click()

        time.sleep(3) # waiting for automatic review pack queue
        dash_my_ads.go_dashboard()
        self.assertEqual(dash_my_ads.search_status_by_subject('Audi A4 2011 edited'), 'Aviso publicado')


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_delete_disabled_ad_from_dashboard(self, wd):
        """ Delete disable ad on dashboard, the car wait for 7 days"""

        subject = 'FORD MUSTANG 2010 Test test'
        login_and_go_to_dashboard(wd, 'cuentaprosincupos@yapo.cl', '123123123') 
        self._insert_ad(wd, subject)

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        dash_my_ads.delete_ad_by_subject(subject)
        dash_my_ads.go_dashboard()
        self.assertEqual(len(dash_my_ads.list_all_ads), 2)


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_edit_disabled_ad_from_dashboard(self, wd):
        """ Edit disabled ad on dashboard"""

        subject = 'FORD MUSTANG 2010 Test test'
        login_and_go_to_dashboard(wd, 'cuentaprosincupos@yapo.cl', '123123123') 
        self._insert_ad(wd, subject)

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        dash_my_ads.edit_ad_by_subject(subject)

        edit = AdEdit(wd)
        edit.insert_ad(subject=subject+" edited")
        edit.submit.click()

        success_page = AdInsertSuccess(wd)
        self.assertIn('Test test edited', success_page.success_message.text)

        dash_my_ads.go_dashboard()
        self.assertEqual(len(dash_my_ads.list_all_ads), 2)


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_delete_disabled_ad_from_email(self, wd):
        """ Delete disable ad from email"""

        login_and_go_to_dashboard(wd, 'cuentaprosincupos@yapo.cl', '123123123') 
        self._insert_ad(wd, 'Test test')
        utils.rebuild_index()

        sent_email = SentEmail(wd)
        sent_email.go()
        row = sent_email.body.find_element_by_css_selector("a[href*='delete']")
        delete_link = row.get_attribute('href')
        wd.get(delete_link)

        delete_page = AdDeletePage(wd)
        delete_page.delete_ad_logged()
        delete_page.wait.until_visible('result_message', 'no lo encuentro!!')
        self.assertEqual(
            delete_page.result_message.text,
            "Tu aviso ser� desactivado en los pr�ximos minutos, si quieres volver a activarlo ingresa a tu cuenta. Luego de 7 d�as se eliminar�.",
            "Check modal window message")

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(len(dash_my_ads.list_all_ads), 2)


    @tags('ad_insert', 'desktop', 'account', 'packs')
    def test_pack_edit_disabled_ad_from_email(self, wd):
        """ Edit disable ad from email"""

        login_and_go_to_dashboard(wd, 'cuentaprosincupos@yapo.cl', '123123123') 
        self._insert_ad(wd, 'Test test')
        utils.rebuild_index()

        sent_email = SentEmail(wd)
        sent_email.go()
        row = sent_email.body.find_element_by_css_selector("a[href*='edit']")
        delete_link = row.get_attribute('href')
        wd.get(delete_link)

        edit = AdEdit(wd)
        edit.insert_ad(subject="Test test edited")
        edit.submit.click()

        success_page = AdInsertSuccess(wd)
        self.assertIn('Test test edited', success_page.success_message.text)

        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        self.assertEqual(len(dash_my_ads.list_all_ads),2)



    def _insert_ad(self, wd, subject):
        self.test_data['subject'] = subject
        ad_insert = AdInsert(wd)
        insert_an_ad(wd, AdInsert=DesktopAI, Submit=True, **self.test_data)

        page = PackConfirm(wd)
        self.assertTrue('pack_message_ok')
        time.sleep(5) # waiting for automatic review pack queue
