# -*- coding: latin-1 -*-
import yapo
from yapo import utils
from yapo.decorators import tags
from yapo.utils import Products, PacksPeriods, PacksType
from yapo.pages.desktop import SummaryPayment, Login, DashboardMyAds, Dashboard, PaymentVerify, Payment
from yapo.pages.packs import BuyPacks
from yapo.pages.simulator import Paysim
from yapo.pages.generic import SentEmail
import datetime
import time

class PackPurchase(yapo.SeleniumTest):
    snaps = ['accounts']
    bconfs = {
        "*.*.packs.expire_time.1.value":"31 days",
        "*.*.packs.expire_time.2.value":"93 days",
        "*.*.packs.expire_time.3.value":"186 days",
        "*.*.packs.expire_time.4.value":"366 days",
        "*.*.payment_settings.product.1.8.*.value": "price:4750",
        '*.*.payment_settings.product.1.1.*.value': 'price:1050',
        '*.*.payment_settings.product.1.3.*.value': 'price:3950',
        '*.*.payment_settings.product.1.2.*.value': 'price:2950',
        '*.*.payment.desktop.khipu.enabled': '1',
        '*.*.payment.msite.khipu.enabled': '1',
    }
    # ev = expected_values

    def check_purchase_email(self, wd, ev):
        # Check Purchase email
        sent_email = SentEmail(wd)
        sent_email.go()
        if len(ev) == 2:
            self.assertEqual('Tu Pack ya est� activo', sent_email.body.find_element_by_css_selector("#greeting-box > h1").text)
        else:
            self.assertEqual('Tus Packs ya est�n activos', sent_email.body.find_element_by_css_selector("#greeting-box > h1").text)

        table_values = sent_email.get_table_data('products_table')
        table_values.sort()
        ev.sort()
        self.assertListEqual(table_values, ev)

    def check_invoice_email(self, wd, ev):
        # Check invoice email
        utils.run_nubox_php_script("bill_processing.php")
        utils.run_nubox_php_script("bill_send_email.php")
        sent_email = SentEmail(wd)
        sent_email.go()
        table_values = sent_email.get_table_data('products_table')
        table_values.sort()
        ev.sort()
        self.assertListEqual(table_values, ev)

    def pay_packs(self,wd,user,passwd,elements,ev_cart,payment,ev_verify, ev_success_mail = None, ev_invoice_mail = None):
        utils.flush_redises();
        login = Login(wd)
        login.login(user,passwd)
        summary = SummaryPayment(wd)
        # Add elements to cart
        for key, data in elements.items():
            if key[0] == 'P':
                data_slots, data_period, data_pack = data
                summary.add_pack_to_cart(data_slots,data_period,data_pack)
            if key[0] == 'S':
                data_store = data
                summary.add_element_to_cart(data_store)
            if key[0] == 'A':
                data_list_id, data_prod = data
                summary.add_element_to_cart(data_prod, data_list_id)

        # Check cart
        if ev_cart is not None:
            table_values = summary.get_table_data('products_list_table')
            table_values.sort()
            ev_cart.sort()
            self.assertListEqual(table_values, ev_cart)

        # Pay with servipag
        if payment == 'servipag':
            summary.radio_servipag.click()
        #if payment == 'khipu':
        #    summary.radio_khipu.click()

        # Confirm payment
        summary.change_doc_type(Payment.BILL)
        summary.press_link_to_pay_products()
        # Simulator
        paysim = Paysim(wd)
        paysim.pay_accept()

        # Verify success
        if ev_verify is not None:
            payment_verify = PaymentVerify(wd)
            table_values = payment_verify.get_table_data('sale_table')
            table_values.sort()
            ev_verify.sort()
            self.assertListEqual(table_values, ev_verify)

        # Verify cart clean
        summary.go_summary()
        self.assertTrue(summary.empty_cart.is_displayed())

        # Check purchase pack mail
        if ev_success_mail is not None:
            self.check_purchase_email(wd,ev_success_mail)
        # Check invoice mail
        if ev_invoice_mail is not None:
            self.check_invoice_email(wd,ev_invoice_mail)

class PackInmoPurchase(PackPurchase):

    snaps = ['accounts', 'diff_pack_inmo1']
    bconfs = {
        "*.*.packs.expire_time.1.value":"31 days",
        "*.*.packs.expire_time.2.value":"93 days",
        "*.*.packs.expire_time.3.value":"186 days",
        "*.*.packs.expire_time.4.value":"366 days",
        "*.*.payment_settings.product.1.8.*.value": "price:4750",
        '*.*.payment_settings.product.1.3.*.value': 'price:3950',
        '*.*.payment_settings.product.1.1.*.value': 'price:1050',
        '*.*.payment_settings.product.1.2.*.value': 'price:2950',
    }

    user1 = 'cuentaprosinpack@yapo.cl'
    passwd1 = '123123123'
    elements1 = {'P01':('005',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE)}
    payment1 = 'webpay'
    ev_cart1 = [['Pack Inmobiliaria', '5/trimestral', '$ 106.500', '']]
    ev_verify1 = [
            ['', 'Pack Inmobiliaria 5/trimestral', '$ 106.500'],
            ]
    date_pack1 = utils.get_date(addDays = 93)
    ev_success_mail1 = [
            ['Cupos totales de packs', 'Fecha vencimiento'],
            ['5/trimestral', date_pack1]
            ]
    ev_invoice_mail1 = [['1', 'Pack Inmobiliaria 5/trimestral','$106.500']]

    user2 = 'cuentaprosinpack@yapo.cl'
    passwd2 = '123123123'
    elements2 = {
            'P01':('020',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P02':('030',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE)
            }
    ev_cart2 = [
            ['Pack Inmobiliaria', '20/mensual', '$ 68.000', ''],
            ['Pack Inmobiliaria', '30/mensual', '$ 89.000', '']
            ]
    payment2 = 'webpay'
    ev_verify2 = [
            ['', 'Pack Inmobiliaria 20/mensual', '$ 68.000'],
            ['', 'Pack Inmobiliaria 30/mensual', '$ 89.000'],
            ]

    date_pack2 = utils.get_date(addDays = 31)
    ev_success_mail2 = [
            ['Cupos totales de packs', 'Fecha vencimiento'],
            ['20/mensual', date_pack2],
            ['30/mensual', date_pack2]
            ]
    ev_invoice_mail2 = [
            ['1', 'Pack Inmobiliaria 20/mensual','$68.000'],
            ['1', 'Pack Inmobiliaria 30/mensual','$89.000']
            ]

    user3 = 'cuentaprosinpack@yapo.cl'
    passwd3 = '123123123'
    elements3 = {'P01':('020',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE), 'P02':('030',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE)}
    ev_cart3 = [
            ['Pack Inmobiliaria', '20/mensual', '$ 68.000', ''],
            ['Pack Inmobiliaria', '30/mensual', '$ 89.000', '']
            ]
    payment3 = 'servipag'
    ev_verify3 = [
            ['', 'Pack Inmobiliaria 20/mensual', '$ 68.000'],
            ['', 'Pack Inmobiliaria 30/mensual', '$ 89.000'],
            ]

    user4 = 'cuentaprosinpack@yapo.cl'
    passwd4 = '123123123'
    elements4 = {
            'P01':('005',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P02':('010',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P03':('020',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P04':('030',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P05':('040',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P06':('050',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P07':('075',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P08':('100',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),
            'P09':('125',PacksPeriods.MONTHLY, PacksType.REAL_ESTATE),

            'P10':('005',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P11':('010',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P12':('020',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P13':('030',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P14':('040',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P15':('050',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P16':('075',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P17':('100',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'P18':('125',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),

            'P19':('005',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P20':('010',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P21':('020',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P22':('030',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P23':('040',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P24':('050',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P25':('075',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P26':('100',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),
            'P27':('125',PacksPeriods.BIANNUAL, PacksType.REAL_ESTATE),

            'P28':('005',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P29':('010',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P30':('020',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P31':('030',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P32':('040',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P33':('050',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P34':('075',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P35':('100',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            'P36':('125',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            }
    ev_cart4 = [
            ['Pack Inmobiliaria', '5/mensual',      '$ 35.500', ''],
            ['Pack Inmobiliaria', '10/mensual',     '$ 35.500', ''],
            ['Pack Inmobiliaria', '20/mensual',     '$ 68.000', ''],
            ['Pack Inmobiliaria', '30/mensual',     '$ 89.000', ''],
            ['Pack Inmobiliaria', '40/mensual',     '$ 106.000', ''],
            ['Pack Inmobiliaria', '50/mensual',     '$ 122.000', ''],
            ['Pack Inmobiliaria', '75/mensual',     '$ 180.000', ''],
            ['Pack Inmobiliaria', '100/mensual',    '$ 232.000', ''],
            ['Pack Inmobiliaria', '125/mensual',    '$ 281.000', ''],

            ['Pack Inmobiliaria', '5/trimestral',   '$ 106.500', ''],
            ['Pack Inmobiliaria', '10/trimestral',  '$ 106.500', ''],
            ['Pack Inmobiliaria', '20/trimestral',  '$ 204.000', ''],
            ['Pack Inmobiliaria', '30/trimestral',  '$ 267.000', ''],
            ['Pack Inmobiliaria', '40/trimestral',  '$ 318.000', ''],
            ['Pack Inmobiliaria', '50/trimestral',  '$ 366.000', ''],
            ['Pack Inmobiliaria', '75/trimestral',  '$ 540.000', ''],
            ['Pack Inmobiliaria', '100/trimestral', '$ 696.000', ''],
            ['Pack Inmobiliaria', '125/trimestral', '$ 843.000', ''],

            ['Pack Inmobiliaria', '5/semestral',    '$ 192.000', ''],
            ['Pack Inmobiliaria', '10/semestral',   '$ 192.000', ''],
            ['Pack Inmobiliaria', '20/semestral',   '$ 372.000', ''],
            ['Pack Inmobiliaria', '30/semestral',   '$ 486.000', ''],
            ['Pack Inmobiliaria', '40/semestral',   '$ 582.000', ''],
            ['Pack Inmobiliaria', '50/semestral',   '$ 666.000', ''],
            ['Pack Inmobiliaria', '75/semestral',   '$ 984.000', ''],
            ['Pack Inmobiliaria', '100/semestral',  '$ 1.266.000', ''],
            ['Pack Inmobiliaria', '125/semestral',  '$ 1.530.000', ''],

            ['Pack Inmobiliaria', '5/anual',        '$ 360.000', ''],
            ['Pack Inmobiliaria', '10/anual',       '$ 360.000', ''],
            ['Pack Inmobiliaria', '20/anual',       '$ 672.000', ''],
            ['Pack Inmobiliaria', '30/anual',       '$ 912.000', ''],
            ['Pack Inmobiliaria', '40/anual',       '$ 1.080.000', ''],
            ['Pack Inmobiliaria', '50/anual',       '$ 1.248.000', ''],
            ['Pack Inmobiliaria', '75/anual',       '$ 1.836.000', ''],
            ['Pack Inmobiliaria', '100/anual',      '$ 2.388.000', ''],
            ['Pack Inmobiliaria', '125/anual',      '$ 2.868.000', ''],
            ]
    payment4 = 'webpay'
    ev_verify4 = [
            ['', 'Pack Inmobiliaria 5/mensual', '$ 35.500'],
            ['', 'Pack Inmobiliaria 10/mensual', '$ 35.500'],
            ['', 'Pack Inmobiliaria 20/mensual', '$ 68.000'],
            ['', 'Pack Inmobiliaria 30/mensual', '$ 89.000'],
            ['', 'Pack Inmobiliaria 40/mensual', '$ 106.000'],
            ['', 'Pack Inmobiliaria 50/mensual', '$ 122.000'],
            ['', 'Pack Inmobiliaria 75/mensual', '$ 180.000'],
            ['', 'Pack Inmobiliaria 100/mensual', '$ 232.000'],
            ['', 'Pack Inmobiliaria 125/mensual', '$ 281.000'],
            ['', 'Pack Inmobiliaria 5/trimestral', '$ 106.500'],
            ['', 'Pack Inmobiliaria 10/trimestral', '$ 106.500'],
            ['', 'Pack Inmobiliaria 20/trimestral', '$ 204.000'],
            ['', 'Pack Inmobiliaria 30/trimestral', '$ 267.000'],
            ['', 'Pack Inmobiliaria 40/trimestral', '$ 318.000'],
            ['', 'Pack Inmobiliaria 50/trimestral', '$ 366.000'],
            ['', 'Pack Inmobiliaria 75/trimestral', '$ 540.000'],
            ['', 'Pack Inmobiliaria 100/trimestral', '$ 696.000'],
            ['', 'Pack Inmobiliaria 125/trimestral', '$ 843.000'],
            ['', 'Pack Inmobiliaria 5/semestral', '$ 192.000'],
            ['', 'Pack Inmobiliaria 10/semestral', '$ 192.000'],
            ['', 'Pack Inmobiliaria 20/semestral', '$ 372.000'],
            ['', 'Pack Inmobiliaria 30/semestral', '$ 486.000'],
            ['', 'Pack Inmobiliaria 40/semestral', '$ 582.000'],
            ['', 'Pack Inmobiliaria 50/semestral', '$ 666.000'],
            ['', 'Pack Inmobiliaria 75/semestral', '$ 984.000'],
            ['', 'Pack Inmobiliaria 100/semestral', '$ 1.266.000'],
            ['', 'Pack Inmobiliaria 125/semestral', '$ 1.530.000'],
            ['', 'Pack Inmobiliaria 5/anual', '$ 360.000'],
            ['', 'Pack Inmobiliaria 10/anual', '$ 360.000'],
            ['', 'Pack Inmobiliaria 20/anual', '$ 672.000'],
            ['', 'Pack Inmobiliaria 30/anual', '$ 912.000'],
            ['', 'Pack Inmobiliaria 40/anual', '$ 1.080.000'],
            ['', 'Pack Inmobiliaria 50/anual', '$ 1.248.000'],
            ['', 'Pack Inmobiliaria 75/anual', '$ 1.836.000'],
            ['', 'Pack Inmobiliaria 100/anual', '$ 2.388.000'],
            ['', 'Pack Inmobiliaria 125/anual', '$ 2.868.000'],

            ]

    user5 = 'cuentaprosinpack@yapo.cl'
    passwd5 = '123123123'
    elements5 = {
            'P01':('005',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'S01':(Products.STORE_MONTHLY),
            }
    payment5 = 'webpay'
    ev_cart5 = [
            ['Pack Inmobiliaria', '5/trimestral', '$ 106.500', ''],
            ['Tienda', 'Mensual', '$ 6.000', '']
        ]

    ev_verify5 = [
            ['', 'Pack Inmobiliaria 5/trimestral', '$ 106.500'],
            ['', 'Tienda Mensual', '$ 6.000']
        ]

    user6 = 'cuentaprosinpack@yapo.cl'
    passwd6 = '123123123'
    elements6 = {
            'P01':('010',PacksPeriods.QUARTERLY, PacksType.REAL_ESTATE),
            'A01':(8000070, Products.BUMP),
            'A02':(8000062, Products.WEEKLY_BUMP),
            'A03':(8000059, Products.DAILY_BUMP),
            'A04':(8000051, Products.GALLERY),
            'P02':('125',PacksPeriods.ANNUAL, PacksType.REAL_ESTATE),
            }
    payment6 = 'webpay'
    ev_cart6 = [
            ['Cinegrama star wars - natalie portman', 'Subir Diario', '$ 4.750', ''],
            ['El joven manos de tijera poster pelicula 54 x 41','Vitrina 7 d�as','$ 3.950', ''],
            ['Libro para Psicologo Manual Diagnostico', 'Subir Semanal', '$ 2.950', ''],
            ['Libro 3', 'Subir Ahora', '$ 1.050' , ''],
            ['Pack Inmobiliaria', '10/trimestral', '$ 106.500', ''],
            ['Pack Inmobiliaria', '125/anual', '$ 2.868.000', ''],
        ]
    ev_verify6 = [
            ['Cinegrama star wars - nat...', 'Subir Diario', '$ 4.750'],
            ['El joven manos de tijera ...','Vitrina 7 d�as', '$ 3.950'],
            ['Libro para Psicologo Manu...', 'Subir Semanal', '$ 2.950'],
            ['Libro 3', 'Subir Ahora', '$ 1.050' ],
            ['', 'Pack Inmobiliaria 10/trimestral', '$ 106.500'],
            ['', 'Pack Inmobiliaria 125/anual', '$ 2.868.000'],
        ]
