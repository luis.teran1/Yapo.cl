# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop import Login
from yapo.steps.accounts import login_from_bar
from yapo.component import select
import yapo


class PackInmoCommuneMsg(yapo.SeleniumTest):

    snaps = ['accounts']
    
    #       loged,       email,                         {cat: is_showing, cat2: is_showing2, ...}
    scenarios = [
        [   False,      '',                             {'2020': False, '1220': True, '1240': True, '1260': True}],
    ]

    def check_msg(self, AI, cat, result):
        select(AI.category, cat)
        if result:
            AI.wait.until_visible('advicecommune')
        elif AI.is_element_present('advicecommune'):
            self.assertEquals(AI.advicecommune.text, "")

    def do_logout(self, wd):
        login = Login(wd)
        login.logout()

    @tags('ad_insert', 'packs')
    def test_escenarios(self, wd):
        """ Test message is showing in the following scenarios """
        for esce in self.scenarios:
            if esce[0]: #should log?
                login_from_bar(wd, esce[1], '123123123')
            # go to AI
            ad_insert = AdInsert(wd)
            ad_insert.go()
            for cat in esce[2]:
                self.check_msg(ad_insert, cat, esce[2][cat])
            if esce[0]: #should logout?
                self.do_logout(wd)