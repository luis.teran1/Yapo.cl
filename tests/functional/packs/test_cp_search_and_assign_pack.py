# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import utils, SeleniumTest
from yapo.pages.control_panel import ControlPanel, ControlPanelAddUser, ControlPanelPackSearchAndAssign
import yapo.conf
import datetime
import time

class ControlpanelPASearchAndAssign(SeleniumTest):

    snaps = ['accounts', 'diff_prepaid3_11ads_2packs', 'diff_prepaid3_1expired_pack']

    def _login(self, wd, user, passw):
        cp = ControlPanel(wd)
        cp.go()
        cp.login(user, passw)
        return cp

    @tags('controlpanel', 'search', 'packs')
    def test_for_link(self, wd):
        """ Test that the link is shown """
        cp = self._login(wd, "dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")

    @tags('controlpanel', 'search', 'packs')
    def test_search_bad_email(self, wd):
        """ Test that an no-email returns error """
        cp = self._login(wd, "dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")
        saa = ControlPanelPackSearchAndAssign(wd)
        saa.search_box.send_keys("lolololo")
        saa.search_button.click()
        self.assertIn("Email no v�lido", saa.email_error.text)

    @tags('controlpanel', 'search', 'packs')
    def test_search_email_no_account(self, wd):
        """ Test that an email with no account returns error """
        cp = self._login(wd, "dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")
        saa = ControlPanelPackSearchAndAssign(wd)
        saa.search_box.send_keys("lolololo@yolo.cl")
        saa.search_button.click()
        self.assertIn("El usuario", saa.email_error.text)
        self.assertIn("no tiene cuenta", saa.email_error.text)

    @tags('controlpanel', 'search', 'packs')
    def test_search_email_account_no_pro(self, wd):
        """ Test that an email with pro account returns error """
        cp = self._login(wd, "dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")
        saa = ControlPanelPackSearchAndAssign(wd)
        saa.search_box.send_keys("no-ads@yapo.cl")
        saa.search_button.click()
        self.assertIn("Este usuario no es un profesional de la categor�a Autos", saa.email_error.text)

    @tags('controlpanel', 'search', 'packs')
    def test_search_and_assign(self, wd):
        """ Test that the page show what it should """
        cp = self._login(wd, "dany", "dany")
        cp.go_to_option_in_menu("Buscar y/o Asignar Pack Auto")
        saa = ControlPanelPackSearchAndAssign(wd)
        saa.search_box.send_keys("prepaid3@blocket.se")
        saa.search_button.click()

        active = saa.get_table_data("table_active")
        expired = saa.get_table_data("table_expired")
        self.assertEquals(len(expired), 1)

        end_date = datetime.datetime.now().date() + datetime.timedelta(days=1)
        active_data = [
                    ['Autos', '10/mensual', '10 / 10', time.strftime("%d/%m/%y"), time.strftime("%d/%m/%y"), 'Expirar'],
                    ['Autos', '10/mensual', '10 / 1', time.strftime("%d/%m/%y"), end_date.strftime("%d/%m/%y"), 'Expirar']
        ]

        self.assertListEqual(active, active_data)

        cp.go()
        saa.assign_pack(wd, "auto", "50", end_date.strftime("%d/%m/%y"), "prepaid3@blocket.se")

        active = saa.get_table_data("table_active")
        expired = saa.get_table_data("table_expired")
        self.assertEquals(len(expired), 1)
        active_data = [
                ['Autos', '10/mensual', '10 / 10', time.strftime("%d/%m/%y"), time.strftime("%d/%m/%y"), 'Expirar'],
                ['Autos', '10/mensual', '10 / 1', time.strftime("%d/%m/%y"), end_date.strftime("%d/%m/%y"), 'Expirar'],
                ['Autos', 'Personalizado', '50 / 0', time.strftime("%d/%m/%y"), end_date.strftime("%d/%m/%y"), 'Expirar']
        ]
        self.assertListEqual(active, active_data)

