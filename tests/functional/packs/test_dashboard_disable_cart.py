# -*- coding: latin-1 -*-
from selenium.common.exceptions import NoSuchElementException
from yapo.decorators import tags
from yapo.pages.desktop import DashboardMyAds, Pack, SummaryPayment
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.utils import Products
import yapo


class DashboarPackDisableCart(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_pack_cars1']

    @tags('desktop', 'account', 'packs', 'cart')
    def test_pack_ad_accepted_and_toggle_enable(self, wd):
        """
        Check cart for PACK user when disabling ad with
        bump product in pack. This should be removed
        """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': True,
            'options_enabled': True,
            'bumps_enabled': True,
            'gallery_enabled': True,
            'labels_enabled': True,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }

        self._check_dashboard_ad(wd, ad_data)

        LIST_IDS = {
            8000093: [Products.LABEL],
        }

        expected_values = [
            ['Audi A4 2011', 'Etiqueta', '$ 2.200', ''],
        ]

        self._add_product(wd, LIST_IDS, expected_values)
        self._change_toggle(wd, ad_data, 'inactive')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': False,
            'options_enabled': True,
            'bumps_enabled': False,
            'gallery_enabled': False,
            'labels_enabled': False,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }
        self._check_dashboard_ad(wd, ad_data)
        self._change_toggle(wd, ad_data, 'active')

    @tags('desktop', 'account', 'packs', 'cart')
    def test_pack_ad_accepted_select_gallery(self, wd):
        """
        Check cart for PACK user when disabling ad with
        gallery product in pack. This should be removed
        """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': True,
            'options_enabled': True,
            'bumps_enabled': True,
            'gallery_enabled': True,
            'labels_enabled': True,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }

        self._check_dashboard_ad(wd, ad_data)

        LIST_IDS = {
            8000093: [Products.GALLERY_1],
        }

        expected_values = [
            ['Audi A4 2011', 'Vitrina 1 d�a', '$ 2.900', '']
        ]

        self._add_product(wd, LIST_IDS, expected_values)
        self._change_toggle(wd, ad_data, 'inactive')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': False,
            'options_enabled': True,
            'bumps_enabled': False,
            'gallery_enabled': False,
            'labels_enabled': False,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }
        self._check_dashboard_ad(wd, ad_data)
        self._change_toggle(wd, ad_data, 'active')

    @tags('desktop', 'account', 'packs', 'cart')
    def test_pack_ad_accepted_select_label(self, wd):
        """
        Check cart for PACK user when disabling ad with
        label product in pack. This should be removed
        """
        login_and_go_to_dashboard(wd, 'cuentaproconpack@yapo.cl', '123123123')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': True,
            'options_enabled': True,
            'bumps_enabled': True,
            'gallery_enabled': True,
            'labels_enabled': True,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }

        self._check_dashboard_ad(wd, ad_data)

        LIST_IDS = {
            8000093: [Products.LABEL],
        }

        expected_values = [
            ['Audi A4 2011', 'Etiqueta', '$ 2.200', '']
        ]

        self._add_product(wd, LIST_IDS, expected_values)
        self._change_toggle(wd, ad_data, 'inactive')

        ad_data = {
            'subject': 'Audi A4 2011',
            'status': 'Aviso publicado',
            'vi_link_enabled': False,
            'options_enabled': True,
            'bumps_enabled': False,
            'gallery_enabled': False,
            'labels_enabled': False,
            'check_number_in_cart': True,
            'products_in_cart': '0'
        }
        self._check_dashboard_ad(wd, ad_data)
        self._change_toggle(wd, ad_data, 'active')

    def _check_dashboard_ad(self, wd, ad_data):
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        self.assertIsNotNone(active_ad)

        # check ad status
        self.assertEqual(active_ad.status.text, ad_data['status'])

        if ad_data['vi_link_enabled']:
            self.assertIsNotNone(active_ad.subject.get_attribute('href'))
        else:
            self.assertIsNone(active_ad.subject.get_attribute('href'))

        if ad_data['bumps_enabled']:
            active_ad.wait.until_visible('bumps_enabled')
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.bumps_enabled)

        if ad_data['gallery_enabled']:
            self.assertIsNotNone(active_ad.gallery_enabled)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.gallery_enabled)

        if ad_data['labels_enabled']:
            self.assertIsNotNone(active_ad.labels_enabled)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.labels_enabled)

        # check edit and delete
        if ad_data['options_enabled']:
            self.assertIsNotNone(active_ad.options_buttons)
        else:
            self.assertRaises(NoSuchElementException, lambda: active_ad.options_buttons)

        if ad_data['check_number_in_cart']:
            self.assertEqual(dash_my_ads.products_in_cart.text, ad_data['products_in_cart'])

    def _add_product(self, wd, LIST_IDS, expected_values):
        dma = DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(LIST_IDS)
        dma.checkout_button.click()
        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        self.assertListEqual(table_values, expected_values)

    def _change_toggle(self, wd, ad_data, status):
        dash_my_ads = DashboardMyAds(wd)
        dash_my_ads.go_dashboard()
        active_ad = dash_my_ads.search_ad_by_subject(ad_data['subject'])
        pack = Pack(wd)
        pack.change_status(active_ad, status)
