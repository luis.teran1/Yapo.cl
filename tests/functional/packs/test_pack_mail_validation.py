# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.desktop import Login
from yapo.pages.ad_insert import AdInsert, PackConfirm, AdInsertSuccess, AdInsertResult
from yapo.pages.packs import DashboardPacks
from yapo.steps import trans, insert_an_ad
from yapo import utils
from yapo.pages.generic import SentEmail
import yapo
import time
import random, string

class MailValidationCarPack(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_carpacks']
    bconfs = {"*.controlpanel.modules.adqueue.auto.pack.timeout":"1"}
    pack_header = 'Tu aviso se encuentra en estado inactivo. Sigue los siguientes pasos para activarlo:'
    pack_text1 = 'Esta opci�n te da una cantidad determinada de cupos por un tiempo espec�fico para que los avisos que publicas se muestren en el listado de Yapo.cl'
    pack_text2 = 'Esta opci�n te permite agregar un aviso con un valor unitario.'
    inactive_text = 'Tu aviso se encuentra en estado inactivo. Sigue los siguientes pasos para activarlo:'
    review_header = 'Revisi�n express'
    review_text = 'Tus avisos ser�n revisados y publicados en menos de 20 minutos.'
    message_text = 'Por favor, ingresa desde un computador a tu cuenta en Yapo.cl para continuar'

    def test_pack_auto_ai_without_account(self, wd):
        """ Mail AdInsert without account """

        sent_email = SentEmail(wd)
        for num in range(0, 3):
            self._insert_ad(wd, num, False, 'WA ad {0}', expected_success = True, use_pack_confirm = False)
            utils.rebuild_index()
            trans.review_ad('without_account@yapo.cl', 'WA ad {0}'.format(num))
        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn('Felicitaciones, para nosotros eres todo un pro', body_text)
        utils.rebuild_index()

        self._insert_ad(wd, 4, False, 'WA ad 4', expected_success = True, use_pack_confirm = True, w_account=False)
        success_insert = PackConfirm(wd)
        self.assertTrue(success_insert.pack_message_noaccount)
        utils.rebuild_index()

        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn(self.inactive_text, body_text)
        self.assertIn('�Crea una cuenta en Yapo.cl y compra un pack para activar tu aviso!', body_text)
        self.assertIn(self.pack_text1, body_text)
        self.assertIn(self.pack_text2, body_text)
        #self.assertIn(self.review_header, body_text)
        #self.assertIn(self.review_text, body_text)
        self.assertIn(self.message_text, body_text)
        #self.assertIn(self.assistance_header, body_text)
        #self.assertIn(self.assistance_text, body_text)

    @tags('desktop', 'account', 'packs', 'email')
    def test_mail_validation_ad_insert_active_pack(self, wd):
        """ Check email message after insert an ad with an active pack """

        email = 'active_ads_expired_pack@yapo.cl'
        passwd = '123123123'
        login = Login(wd)
        login.login(email, passwd)

        self._insert_ad(wd, 1, expected_success = True, use_pack_confirm = True)
        success_insert = PackConfirm(wd)
        self.assertTrue(success_insert.pack_message_ok.is_displayed())
        self.assertEqual(success_insert.pack_data_total.text, '10')
        self.assertEqual(success_insert.pack_data_quota.text, '4')
        self.assertEqual(success_insert.pack_data_available.text, '6')
        utils.rebuild_index()

        sent_email = SentEmail(wd)
        sent_email.go()
        body_text = sent_email.body.text
        self.assertIn('Tu aviso ha sido publicado', body_text)
        self.assertIn('Estado de tu Pack', body_text)
        self.assertIn('Cupos totales', body_text)
        self.assertIn('Cupos utilizados', body_text)
        self.assertIn('Cupos disponibles', body_text)
        self.assertIn('10', body_text)
        self.assertIn('4', body_text)
        self.assertIn('6', body_text)

    def _insert_ad(self, wd, number, account=True, rsubject='Car AD {0}', expected_success=False, use_pack_confirm=False, w_account=True):

        data = {
            'logged': account,
            'category': 2020,
            'subject': rsubject.format(number),
            'body': ''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(100)),
            'price': ''.join(random.choice(string.digits) for _ in range(8)),
            'email': 'without_account@yapo.cl',
            'email_verification': 'without_account@yapo.cl',
        }

        insert_an_ad(wd, AdInsert=AdInsert, **data)

        if expected_success:
            if use_pack_confirm:
                page = PackConfirm(wd)
                if w_account:
                    page.wait.until_visible("pack_message_ok")
                else:
                    page.wait.until_visible("pack_message_noaccount")
            else:
                result = AdInsertSuccess(wd)
        else:
            result = AdInsertResult(wd)
            result.wait.until_present('result_deac_message')
