# -*- coding: latin-1 -*-
import yapo
from yapo.pages.desktop import DesktopHome
from yapo.decorators import tags

class HomeStatistics(yapo.SeleniumTest):
    """ Verifies the home statistics"""
    snaps = ['1030ads_a_user']
    bconfs = {"*.*.common.items_sold_last_week": "30"}

    @tags('desktop', 'home')
    def test_home_statistics(self, wd):
        """ home statistics validations """
        home_page = DesktopHome(wd)
        home_page.go()
        self.assertEqual("1.001 avisos disponibles", home_page.site_statistics[0].text)
        self.assertEqual("30 productos vendidos en los �ltimos 7 d�as", home_page.site_statistics[1].text)

