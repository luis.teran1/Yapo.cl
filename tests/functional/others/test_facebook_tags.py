# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import pages, conf
from yapo.pages.desktop_list import AdViewDesktop, AdListDesktop
from yapo.pages.mobile_list import AdViewMobile, AdListMobile
from yapo.pages.mobile import MobileHome
from yapo.pages.desktop import DesktopHome
from yapo.pages.generic import MetaTags
import yapo, copy

class FacebookTagsDesktop(yapo.SeleniumTest):
    snaps = ['accounts']
    AdView = AdViewDesktop
    Home = DesktopHome
    AdList = AdListDesktop
    baseUrl = conf.DESKTOP_URL

    metadata = {
        'og:url': conf.DESKTOP_URL,
        'og:site_name': 'yapo.cl',
        'og:title': 'Yapo.cl - Compra y vende cerca de ti',
        'og:description': 'Publica lo que ya no usas o simplemente encuentra lo que andas buscando',
    }

    images = [
        [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
    ]

    def _check_metadata(self, wd, meta, images=[], ad_arr=[]):
        """ Check og tags from an ad in desktop and mobile"""

        if ad_arr:
            ad_view = self.AdView(wd)
            ad_view.go(ad_arr[0])
            meta.update(ad_arr[1])
            images = ad_arr[2]

        tags = MetaTags(wd).get_og_tags()
        print("MetaTags[0] = {}".format(tags[0]))
        print("MetaTags[1] = {}".format(tags[1]))
        self.assertEquals(tags, [meta, images]);

    @tags("desktop","mobile","meta","tags","facebook")
    def test_home(self, wd):
        """ Tests og tags on home """

        meta = copy.copy(self.metadata)
        imgs = copy.copy(self.images)
        page = self.Home(wd)
        page.go()
        self._check_metadata(wd, meta, imgs)

    @tags("desktop","mobile","meta","tags","facebook")
    def test_listing(self, wd):
        """ Tests og tags on listing """

        meta = copy.copy(self.metadata)
        imgs = copy.copy(self.images)
        page = self.AdList(wd)
        page.go()
        self._check_metadata(wd, meta, imgs)

    @tags("desktop","mobile","meta","tags","facebook")
    def test_ads_no_pictures(self, wd):
        """ Tests og tags on ad with no pictures """
        meta = copy.copy(self.metadata)
        imgs = copy.copy(self.images)

        update_8000073 = {
            'og:title': 'Adeptus Astartes Codex',
            'og:description': 'for teh emporer!',
            'og:url': self.baseUrl + "/vi/" + str(8000073) + ".htm"
        }

        images_8000073 = [
            [ conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]

        self._check_metadata(wd, meta, ad_arr = [8000073, update_8000073, images_8000073])

    @tags("desktop","mobile","meta","tags","facebook")
    def test_ads_one_picture(self, wd):
        """ Tests og tags on ad with one picture """
        meta = copy.copy(self.metadata)
        imgs = copy.copy(self.images)

        update_8000082 = {
            'og:title': 'Test 07',
            'og:description': 'asdad asdasdaasd',
            'og:url': self.baseUrl + "/vi/" + str(8000082) + ".htm"
        }

        images_8000082 = [
            [ conf.MOD_IMAGE_URL + '/images/01/0100012349.jpg', '640', '480'],
            [ conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]

        self._check_metadata(wd, meta, ad_arr = [8000082, update_8000082, images_8000082])

    @tags("desktop","mobile","meta","tags","facebook")
    def test_ads_multiple_pictures(self, wd):
        """ Tests og tags on ad with multiple pictures """
        meta = copy.copy(self.metadata)
        imgs = copy.copy(self.images)

        update_3456789= {
            'og:title': 'Race car',
            'og:description': 'Car with two doors ... 33.2-inch wheels made of plastic.',
            'og:url': self.baseUrl + "/vi/" + str(3456789) + ".htm"
        }

        images_3456789 = [
            [ conf.MOD_IMAGE_URL + '/images/01/0100012344.jpg', '640', '480'],
            [ conf.MOD_IMAGE_URL + '/images/01/0100012345.jpg', '640', '480'],
            [ conf.MOD_IMAGE_URL + '/images/01/0100012346.jpg', '640', '480'],
            [ conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]

        self._check_metadata(wd, meta, ad_arr = [3456789, update_3456789, images_3456789])

class FacebookTagsMobile(FacebookTagsDesktop):
    AdView = AdViewMobile
    Home = MobileHome
    AdList = AdListMobile
    baseUrl = conf.MOBILE_URL

    metadata = {
        'og:url': conf.DESKTOP_URL,
        'og:site_name': 'yapo.cl',
        'og:title': 'Yapo.cl - Compra y vende cerca de ti',
        'og:description': 'Publica lo que ya no usas o simplemente encuentra lo que andas buscando',
        'al:ios:url': 'yapoapp://',
        'al:ios:app_store_id':'767503903',
        'al:ios:app_name':'Yapo.cl',
        'al:android:url':'yapoapp://',
        'al:android:app_name':'Yapo.cl',
        'al:android:package':'cl.yapo'
    }
