# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.utils import JsonTool
from yapo.mail_generator import Email
from yapo.pages.desktop import MyAds as DesktopMyAds
from yapo.pages.mobile import MyAds as MobileMyAds, HeaderElements, MobileHome
from yapo import utils
import yapo


class TestMyAdsDesktop(yapo.SeleniumTest):

    snaps = ['1030ads_a_user']
    MyAds = DesktopMyAds
    test_data = JsonTool.read_test_data('my_ads_user')
    success_message = '�Tu e-mail fue enviado! {0}'.format(test_data['email'])
    bconfs = {"*.*.movetoapps_splash_screen.enabled": '0'}

    @tags('my_ads')
    def test_my_ads_validations(self, wd):
        """ Testing my ads validations """
        my_ads = self.MyAds(wd)
        my_ads.go_and_fail_request('')
        self.assertEqual(my_ads.error_message.text, 'Debes insertar un email v�lido.')
        my_ads.go_and_fail_request('123436634@�uble')
        self.assertEqual(my_ads.error_message.text, 'No existen avisos activos para este e-mail: 123436634@xn--uble-fqa')
        my_ads.go_and_request('a@aa.cl')
        self.assertEqual(my_ads.ok_message.text, self.success_message)
        ml = Email.create(wd)
        ml.check_my_ads_mail(self, wd, 'a@aa.cl', self.test_data['password'])

    @tags('core1', 'my_ads')
    def test_my_ads_email(self, wd):
        """ Testing my ads email """

        my_ads = self.MyAds(wd)
        if not utils.is_not_regress():
            self.test_data['email'] = 'a@aa.cl'
        my_ads.go_and_request(self.test_data['email'])
        self.assertEqual(my_ads.ok_message.text, self.success_message)
        ml = Email.create(wd)
        ml.check_my_ads_mail(self, wd, self.test_data['email'], self.test_data['password'])


class TestMyAdsMobile(TestMyAdsDesktop):

    MyAds = MobileMyAds
    user_agent = utils.UserAgents.NEXUS_5
    success_message = 'Hemos enviado un correo con el listado de tus avisos'
