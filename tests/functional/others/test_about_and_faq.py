# -*- coding: latin-1 -*-
from yapo.decorators import tags
from selenium.common import exceptions
from yapo.pages.desktop import FaqPage, AboutPage
from yapo.pages.generic import SentEmail
from selenium.webdriver.common.by import By
import yapo, time

class AboutAndFAQ(yapo.SeleniumTest):

    def test_1_verify_about_texts(self, wd):
        about = AboutPage(wd)
        about.go_to_about()


        expected_buyer_texts = (
            u"�Quieres comprar?",
            u"B�squedas y filtros que te ayudan a encontrar exactamente lo que necesitas",
            u"Filtros que te permiten elegir entre los avisos de negocios o de personas",
            u"Calidad: Los avisos son revisados por nuestro equipo, uno por uno",
            )

        expected_seller_texts = (
            u"�Quieres vender?",
            u"F�cil administraci�n de tus avisos desde tu cuenta",
            u"B�squeda local por regi�n que permite que tu aviso se vea con m�s frecuencia",
            u"Igualdad de oportunidades para todos los vendedores, ya sean personas o profesionales",
            )

        real_buyer_texts = wd.find_elements_by_xpath("//div[@id='about_buyer']/ul/*")
        real_seller_texts = wd.find_elements_by_xpath("//div[@id='about_seller']/ul/*")

        for expected, real in zip(expected_buyer_texts, real_buyer_texts):
            self.assertEqual(expected, real.text)

        for expected, real in zip(expected_seller_texts, real_seller_texts):
            self.assertEqual(expected, real.text)

    def test_1_verify_faq_texts(self, wd):
        faq = FaqPage(wd)
        faq.go_to_faq()

        vendedores = wd.find_element_by_class_name("vendedores")
        item = vendedores.find_element_by_class_name("item")

        #Se utiliza primer elemento de lista para probar funcionalidad.
        expected_question = u"�Es realmente gratis publicar en Yapo.cl?"
        expected_answer = u"Insertar un aviso en yapo.cl es gratis y no se cobra ning�n tipo de comisi�n."
        self.assertTrue(faq.check_item(item, expected_question, expected_answer ))

    @tags('desktop', 'ad_edit')
    def test_2_support_account(self, wd):
        """ Check 'red arrow' not being offered when editing recently inserted ad """
        self.faq = FaqPage(wd)
        self.faq.go_to_faq()
        time.sleep(10)
        self.faq.go_to_support_form('37')
        time.sleep(10)
        self.assertTrue(len(wd.find_elements(By.CLASS_NAME,'wo_border')) == 0,'Error subject "Dudas con cuentas" found.')
        datas =  ['www.yapo.cl','soila','soila@cerda.cl','support body','']
        for id in ['url','name','email','support_body','file']:
            self.assertTrue(len(wd.find_elements(By.ID,id)) == 1)
            data = datas.pop(0)
            if data: wd.find_element_by_id(id).send_keys(data)
        wd.find_element_by_class_name('input_submit').click()
        self.assertEquals(u'Mensaje enviado con �xito',wd.find_element_by_tag_name('h1').text)
        mail = SentEmail(wd)
        self.assertIn(u'Subject: Dudas con cuentas',mail.get_html())
