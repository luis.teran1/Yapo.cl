# -*- coding: latin-1 -*-
from yapo import utils
from yapo import steps
from yapo.decorators import tags
from yapo.pages import desktop, desktop_list
from yapo.pages.ad_insert import AdInsertResult
from yapo.pages.control_panel import ControlPanel
from selenium.common import exceptions
import yapo

class RedArrowPrice(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_whitelist']

    bconfs = {'*.trans.clear.enqueue.delay':'0'}

    @tags('desktop', 'ad_edit')
    def test_edit_ad_without_red_arrow(self, wd):
        """ Check 'red arrow' not being offered when editing recently inserted ad """
        ad_to_edit = '8000073'
        edit_pw_page = desktop.EditPasswordPage(wd)
        edit_pw_page.go(ad_to_edit)
        edit_pw_page.validate_password('123123123')
        edit_page = desktop.AdEdit(wd)
        self.assertNotPresent(edit_page, "red_arrow_suggestion")
        edit_page.insert_ad(price = '755522')
        edit_page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        cp = ControlPanel(wd)
        cp.go()
        cp.login("dany", "dany")
        cp.review_by_queue(
            "Edit", "Adeptus Astartes Codex", "accept")
        cp.logout()

        utils.rebuild_asearch()
        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse('region_metropolitana')
        adlist.search_for_ad('Adeptus Astartes Codex')
        adlist.wait.until_text_present("Regi�n Metropolitana 1 - 1 de 1", adlist.tab_region)
        self.assertRaises(exceptions.NoSuchElementException, lambda: adlist._driver.find_element_by_id('rarrow_' + ad_to_edit))


    @tags('desktop', 'ad_edit')
    def test_edit_ad_with_red_arrow(self, wd):
        """ Check 'red arrow' being offered when editing an older ad """
        ad_to_edit = '8000122'
        edit_pw_page = desktop.EditPasswordPage(wd)
        edit_pw_page.go(ad_to_edit)
        edit_pw_page.validate_password('123123123')
        edit_page = desktop.AdEdit(wd)
        self.assertVisible(edit_page, "red_arrow_suggestion")
        edit_page.insert_ad(price = '117000')
        edit_page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

        steps.review_ad(wd, email='edit_refuse@yapo.cl', subject='Aviso to edit refuse')

        utils.rebuild_asearch()
        adlist = desktop_list.AdListDesktop(wd)
        adlist.browse('region_metropolitana')
        adlist.search_for_ad('Aviso to edit refuse')
        adlist.wait.until_text_present("Regi�n Metropolitana 1 - 2 de 2", adlist.tab_region)
        red_arrow = adlist._driver.find_element_by_id('rarrow_' + ad_to_edit)
        self.assertTrue(red_arrow.is_displayed())

