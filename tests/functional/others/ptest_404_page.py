# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop import PageNotFound, DesktopHome
import yapo
from yapo.conf import DESKTOP_URL


class Tests404Page(yapo.SeleniumTest):

    snaps = ['accounts']
    _multiprocess_shared_ = True

    @tags('desktop')
    def ptest_go_home_404_page_ssl_port(self, wd):
        """ Check that "href" of the link "aqu�" has base_url in ssl port"""
        page_not_found = PageNotFound(wd)
        page_not_found.go_page_not_found_ssl_port()
        self._ver_page_not_found_and_go_home(wd, page_not_found)

    @tags('desktop')
    def ptest_go_home_404_page_php_port(self, wd):
        """ Check that "href" of the link "aqu�" has base_url in php port"""
        page_not_found = PageNotFound(wd)
        page_not_found.go_page_not_found_php_port()
        self._ver_page_not_found_and_go_home(wd, page_not_found)

    @tags('desktop')
    def ptest_go_home_404_page_desktop_url(self, wd):
        """ Check that "href" of the link "aqu�" has base_url in desktop url"""
        page_not_found = PageNotFound(wd)
        page_not_found.go_page_not_found_desktop_url()
        self._ver_page_not_found_and_go_home(wd, page_not_found)

    def _ver_page_not_found_and_go_home(self, wd, page_not_found):
        self.assertEquals('Esta p�gina no existe', page_not_found.title.text)
        page_not_found.link_aqui.click()
        self.assertEquals('{0}{1}'.format(DESKTOP_URL, '/'), wd.current_url)
        desktop_home = DesktopHome(wd)
        self.assertRegexpMatches(desktop_home.site_description.text,
                                 'La idea es simple\nEncuentra clasificados en tu regi�n\n[0-9]+ avisos disponibles\n[0-9]+ productos vendidos en los �ltimos [0-9]{1} d�as',)
