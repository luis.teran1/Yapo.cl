# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.ad_insert import AdInsert, AdInsertWithAccountSuccess, AdInsertResult
from yapo.pages import mobile
from yapo.pages.desktop_list import AdViewDesktop, AdListDesktop
from yapo.steps import trans
from yapo import steps
import yapo

""" CHUS661 New parameter for cellphones
    - Insert new cellphone on the normal Site
        TestCases: Invalid Data/Too Long Data/Correct Data
    - Insert new cellphone from mobile
    - Review ads in controlpanel validating if the new parameter is present
    - rebuild index
    - View the ad in the normal site
    - View the ad in the mobile site
"""

class MemoryOnCellphonesValidations(yapo.SeleniumTest):
    # uncomment this if you want to run this in parallel
    # _multiprocess_shared_ = True

    snaps = ['accounts']
    bconfs = {'*.*.movetoapps_splash_screen.enabled': '0'}

    @tags('ad_insert', 'validations', 'desktop')
    def test_validations_desktop(self, wd):
        """ Testing cellphone memory validations in desktop """

        page = AdInsert(wd)
        page.go()
        page.insert_ad(region = '15', communes=331, category = '3060', subject = 'Iphone 4s', body = 'En perfecto estado')

        # Testing server side type validation
        # XXX hack to avoid inputs to clean themselves
        wd.execute_script("document.getElementById('internal_memory').value = 'texto'")
        self.assertEqual(page.internal_memory_label.text, 'Memoria interna (GB)')
        page.submit.click()
        self.assertIn(u'Inv�lido, utiliza solo n�meros.', [e.text for e in page.errors])

        # Testing maxlength of input in client
        page.internal_memory.send_keys('1234567890')
        self.assertEqual(page.internal_memory.get_attribute('value'), '123456')
        self.assertEqual(page.internal_memory.get_attribute('maxlength'), '6')

    @tags('ad_insert', 'validations', 'mobile')
    def test_validations_mobile(self, wd):
        """ Testing cellphone memory validations in mobile """

        page = mobile.NewAdInsert(wd)
        page.go()
        page.insert_ad(region = '15', communes=331, category = '3060', subject = 'Telefono Usado',
                                    body = 'en muy buen estado', price = '23000',
                                    name = 'tester', email = 'tester@schibsted.cl',
                                    phone = '78123123', password= '123123123',
                                    password_verification = '123123123')

        # Testing server side type validation
        self.assertEqual(page.label_for('internal_memory').text, 'Memoria interna (GB)')
        self.assertEqual(page.internal_memory.get_attribute('type'), 'number')

        # Testing maxlength of input client and server side
        page.insert_ad(internal_memory = '1234567890')
        page.internal_memory.send_keys('\t')
        self.assertEqual(page.get_error_for('internal_memory').text, 'El largo m�ximo es de 6 d�gitos')

    @tags('ad_insert', 'internal_memory', 'ad_param')
    def test_memory_ad_param(self, wd):
        """ Testing memory on cellphones """
        self._insert_desktop(wd)
        self._insert_mobile(wd)
        self._insert_internal_memory_is_empty(wd)
        self._approve_ads(wd)
        self._check_published_ads(wd)

    @tags('ad_insert', 'internal_memory', 'ad_param')
    def _insert_desktop(self, wd):
        """ Testing ad insert in desktop with memory ad param """
        page = AdInsert(wd)
        page.go()
        steps.insert_an_ad(wd, AdInsert, region = '15', communes=331, category = '3060', subject = 'Iphone 4s', body = 'En perfecto estado',
                        price = '150000', private_ad = True, name = 'UserTest', email='test@yapo.cl', phone = '78786565',
                        email_verification = 'test@yapo.cl', password = '123123123', password_verification = '123123123',
                        accept_conditions = True, internal_memory='32')
        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, "�Gracias por publicar!")

    @tags('ad_insert', 'mobile')
    def _insert_mobile(self, wd):
        """ Testing ad insert in mobile with memory ad_param """

        page = mobile.NewAdInsert(wd)
        page.go()
        page.insert_ad(region = '15', communes=331, category = '3060', subject = 'Telefono Usado',
                                    body = 'en muy buen estado', price = '23000',
                                    name = 'tester', email = 'tester@schibsted.cl',
                                    phone = '78786565', password= '123123123',
                                    password_verification = '123123123', internal_memory = '16')
        page.submit.click()
        result = AdInsertResult(wd)
        self.assertTrue(result.was('success'))

    def _insert_internal_memory_is_empty(self, wd):
        """
        FT754:When a phone does not have info for "Memoria interna",
        this parameter should appear empty
        """

        page = AdInsert(wd)
        page.go()
        steps.insert_an_ad(wd, AdInsert, region = '15', communes=331, category = '3060', subject = 'Subject silacerda', body = 'body silacerda',
                        price = '5000', private_ad = True, name = 'soila', email='acc_pending@yapo.cl', phone = '687678667',
                        email_verification = 'acc_pending@yapo.cl', password = '123123123', password_verification = '123123123',
                        internal_memory='', accept_conditions = True)
        page = AdInsertWithAccountSuccess(wd)
        self.assertEqual(page.title.text, "�Gracias por publicar!")

    def _approve_ads(self, wd):
        """ Testing ad insert in mobile with memory ad_param """

        trans.review_ad("test@yapo.cl", 'Iphone 4s')
        trans.review_ad("tester@schibsted.cl", 'Telefono Usado')
        trans.review_ad('acc_pending@yapo.cl','Subject silacerda')
        yapo.utils.rebuild_asearch()

    def _check_published_ads(self, wd):
        """ Checking the published ads and its data """

        ad_list = AdListDesktop(wd)
        ad_list.browse()
        url = wd.current_url
        ad_list.go_to_ad(subject='Telefono Usado')
        ad_view = AdViewDesktop(wd)
        self.assertEqual(ad_view.internal_memory.text, '16')
        wd.get(url)

        ad_list.go_to_ad(subject='Iphone 4s')
        ad_view = AdViewDesktop(wd)
        self.assertEqual(ad_view.internal_memory.text, '32')
        wd.get(url)

        ad_list.go_to_ad(subject='Subject silacerda')
        ad_view = AdViewDesktop(wd)
        self.assertEqual(ad_view.internal_memory.text, '')
