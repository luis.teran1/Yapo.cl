# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.desktop_list import AdListDesktop, SearchBoxSideBar
from yapo import utils
import os
import yapo
from yapo import conf

class AdSenseListingAbcTest(yapo.SeleniumTest):

    snaps = ['accounts']

    bconfs = {
            "*.*.adsense.enabled": "1",
            "*.*.adsense.on_listing.enabled": "1"
            }

    @tags('desktop', 'adsense', 'abctest')
    def test_adsense_b_version(self, wd):
        """Check that adsense B on the footer of listing is the winner"""
        wd.get(conf.DESKTOP_URL + "/region_metropolitana")
        search_box = SearchBoxSideBar(wd)
        search_box.fill_form(search_box ='credito')
        search_box.search_button.click()

        listing = AdListDesktop(wd)
        listing.wait.until_present('adsense_footer_bc')
