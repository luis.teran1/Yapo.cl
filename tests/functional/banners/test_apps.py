from yapo import SeleniumTest
from yapo.decorators import tags
from yapo.pages import mobile, mobile_list
from yapo.utils import UserAgents
import yapo


class TestsMobileSplashEnabled(SeleniumTest):

    bconfs = {'*.*.movetoapps_splash_screen.enabled':'1'}

    @tags('mobile', 'banner', 'android')
    def test_mobile_splash_screen_enabled(self, wd):
        """ Test splash screen on msite home """

        listing = mobile_list.AdListMobile(wd)
        listing.go()
        import time; time.sleep(1)
        self.assertVisible(listing, "splash_screen")


class TestsMobileSplashDisabled(SeleniumTest):

    bconfs = {'*.*.movetoapps_splash_screen.enabled':'0'}

    @tags('mobile', 'banner', 'android')
    def test_mobile_splash_screen_disabled(self, wd):
        """ Test splash screen on msite home """

        listing = mobile_list.AdListMobile(wd)
        listing.go()
        import time; time.sleep(1)
        self.assertNotVisible(listing, "splash_screen")
