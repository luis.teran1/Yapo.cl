import yapo
import logging
from yapo import pages
from yapo.pages.stores import Listing, EditForm, Detail
from yapo import conf
from yapo import utils
import os
import time
from yapo.decorators import tags

logger = logging.getLogger(__name__)


class ListDetail(yapo.SeleniumTest):

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'singlestore'])
        utils.rebuild_csearch()

    @tags('desktop', 'stores', 'listing')
    def test_no_images(self, wd):
        """ Store with default image """
        utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        logger.debug(listing.store_items)
        store = listing.get_store(0)
        self.assertIn('no-logo', store.logo_image.get_attribute('class'))
        self.assertRegex(store.bg_image.get_attribute('src'),
                         '.*/img/stores-listing-bg-others.jpg')
        self.assertEquals(store.name.text, "El gabacho de Moniacas")
        store.name.click()
        detail = Detail(wd)
        self.assertIn('no-logo', detail.logo_image.get_attribute('class'))
        self.assertRegex(detail.main_image.get_attribute('src'),
                         '.*/img/stores-main-image-others.png')

    @tags('desktop', 'edit')
    def test_no_address(self, wd):
        """ Store without address """
        edit = EditForm(wd)
        edit.go()
        # Ugly hack to trigger save button switch
        edit.fill_form(address=" ", address_number=" ")
        edit.fill_form(address="", address_number="")
        edit.address.click()
        edit.submit.click()
        utils.rebuild_csearch()
        detail = Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        self.assertNotPresent(detail, 'address')

    @tags('desktop', 'edit')
    def test_no_phone(self, wd):
        """ Store without phone """
        edit = EditForm(wd)
        edit.go()
        edit.fill_form(phone="")
        edit.submit.click()
        utils.rebuild_csearch()
        detail = Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        self.assertNotPresent(detail, 'phone')

    @tags('desktop', 'edit')
    def test_phone_hidden(self, wd):
        """ Store with phone hidden """
        edit = EditForm(wd)
        edit.go()
        edit.hide_phone_label.click()
        edit.address.click()
        edit.submit.click()
        utils.rebuild_csearch()
        detail = Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        self.assertNotPresent(detail, 'phone')

    @tags('desktop', 'edit')
    def test_no_website(self, wd):
        """ Store without website url """
        edit = EditForm(wd)
        edit = EditForm(wd)
        edit.go()
        edit.fill_form(website_url="")
        edit.submit.click()
        utils.rebuild_csearch()
        detail = Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        self.assertNotPresent(detail, 'site')
