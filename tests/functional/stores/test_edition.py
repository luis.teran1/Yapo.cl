# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo import pages, conf
from yapo.pages.stores import EditForm, MainImagePopup, Listing, Detail, NotFound
from yapo.pages.desktop import Login
from yapo.pages.accounts import Create
from yapo.pages.generic import MetaTags
import logging
import yapo
import os, time
from yapo.steps import stores
import subprocess
from yapo import utils

logger = logging.getLogger(__name__)


class StoreEdit(yapo.SeleniumTest):

    """Tests for store edition"""

    store_info = {
        'name': "El gabacho de Moniacas",
        'info_text': "La descripci�n de la tinedita esta overrated",
        'phone': "666666666",
        'region': "15",
        'commune': "295",
        'address': "Calle yolanda sultana",
        'address_number': "1313",
        'website_url': "www.fuerzasdemoniacas.com",
        'hide_phone': True,
    }

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(['accounts', 'singlestore'])
        yapo.utils.rebuild_csearch()

    def assert_form_data(self, page, info, message=""):
        for fieldname, value in info.items():
            if fieldname in ('hide_phone',):
                self.assertEqual(getattr(page, fieldname).is_selected(),
                                 value == "1",
                                 "{message} Values for field {field} don't match".format(message=message,
                                                                                         field=fieldname))
            else:
                self.assertEqual(getattr(page, fieldname).get_attribute('value'),
                                 value,
                                 "{message} Values for field {field} don't match".format(message=message,
                                                                                         field=fieldname))

    @tags("desktop", "stores", 'minimal')
    def test_loaded_info(self, wd):
        """Loads store edit form data and save a single changed value"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        self.assert_form_data(editpage, self.store_info, "Prefilled form values are incorrect")
        new_info_text = "La descripci�n de la tiendita es la leshe"
        editpage.fill_form(info_text=new_info_text)
        editpage.submit.click()
        newinfo = self.store_info.copy()
        newinfo.update(info_text=new_info_text)
        editpage.go('many@ads.cl', "123123123")
        self.assert_form_data(editpage, newinfo)

    @tags("desktop", "stores")
    def test_hide_phone(self, wd):
        """Hide and Show phone testing this on store view"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        editpage.fill_form(hide_phone=False, phone="123456")
        editpage.submit.click()
        yapo.utils.rebuild_csearch()
        detailpage = Detail(wd)
        detailpage.go("el-gabacho-de-moniacas")
        self.assertTrue(detailpage.phone)
        editpage.go_without_login()
        editpage.hide_phone_label.click()
        editpage.submit.click()
        yapo.utils.rebuild_csearch()
        detailpage.go("el-gabacho-de-moniacas")
        detailpage.wait.until_not_present('phone')

    @tags("desktop", "stores")
    def test_edit_bad_words(self, wd):
        """Submit bad words into store's name and description and get errors"""
        stores.login_and_go_to_edit(wd, 'many@ads.cl', "123123123")
        editpage = EditForm(wd)
        store_info = {
            'name': "Toma tu puta store",
            'info_text': "Esta es la mierda de descripci�n",
        }

        errors = ['Has introducido palabras no permitidas en la descripci�n: "mierda"',
                  'Has introducido palabras no permitidas en el nombre de tu tienda: "puta"']
        editpage.fill_form(**store_info)
        # Big Ugly Hack to force save button to activate (blame chamo and his js stuff)
        keyup_script = 'document.getElementById("{selector}").dispatchEvent(new Event("keyup"));'
        wd.execute_script(
            keyup_script.format(selector=editpage.selector('name')[1]))
        wd.execute_script(
            keyup_script.format(selector=editpage.selector('info_text')[1]))
        editpage.submit.click()
        editpage.wait.until_present('info_text_error')
        self.assertEqual([e.text for e in editpage.errors], errors)

    @tags("desktop", "stores")
    def test_edit_error_messages(self, wd):
        """Submit incorrect data and get validation errors"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        store_info = {
            'name': "s",
            'phone': "123",
            'info_text': "ola<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>",
            'region': "",
            'address_number': "234",
            'website_url': "olakea23432��k*",
        }

        errors = [
            "La descripci�n de tu negocio es demasiado corta",
            "Escribe al menos 2 caracteres",
            'N�mero de tel�fono muy corto',
            "La direcci�n web contiene caracteres inv�lidos o no tiene el formato adecuado",
        ]

        editpage.fill_form(**store_info)
        editpage.submit.click()
        self.assertEqual([e.text for e in editpage.errors], errors)

    @tags("desktop", "stores")
    def test_deactivate(self, wd):
        """Deactivate store and try to view the store (and fail)"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        store_url = editpage.go_to_store.get_attribute("href")
        editpage.deactivate.click()
        editpage.deactivate_yes.click()
        self.assertEqual(editpage.deactivate_message.text,
                         "�Tu tienda ha sido desactivada!")
        # Load form again to check that the messages are kept
        editpage.go('many@ads.cl', "123123123")
        self.assertEqual(editpage.deactivate_message.text,
                         "�Tu tienda ha sido desactivada!")
        yapo.utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        listing.search_input.send_keys('moniacas')
        listing.search_button.click()
        self.assertEquals(len(listing.store_items), 0,
                          "Search was expected to return no results :(")
        wd.get(store_url)
        notfound = NotFound(wd)
        self.assertEquals(notfound.title.text, "Esta tienda no est� disponible")
        self.assertEquals(notfound.other_stores.text, "Ver otras tiendas")
        # editpage.activate.click()
        # editpage.deactivate


class StoreImageEdit(yapo.SeleniumTest):
    snaps = ['accounts', 'singlestore']
    stock_images = {
        'stock_real_estate':  "/img/stores-main-image-realestate.png",
        'stock_cars':  "/img/stores-main-image-vehicles.png",
        'stock_others':  "/img/stores-main-image-others.png",
    }
    metadata = {
        'og:url':          conf.DESKTOP_URL + '/tiendas/el-gabacho-de-moniacas',
        'og:site_name':    'yapo.cl',
        'og:title':        'El gabacho de Moniacas | Tienda yapo.cl ',
        'og:description':  'La descripci�n de la tinedita esta overrated',
        'og:locale': 'es_CL',
    }


    @tags("desktop", "stores")
    def test_stock_real_estate(self, wd):
        """ Switch to default 'real state' image """
        self._stock_image_switch(wd, 'stock_real_estate')

        img = [
            [conf.PHP_URL + '/img/stores-main-image-realestate.png', '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

    @tags("desktop", "stores")
    def test_stock_cars(self, wd):
        """ Switch to default 'cars' image """
        self._stock_image_switch(wd, 'stock_cars')

        img = [
            [conf.PHP_URL + '/img/stores-main-image-vehicles.png', '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

    @tags("desktop", "stores")
    def test_stock_others(self, wd):
        """ Switch to default 'others' image """
        self._stock_image_switch(wd, 'stock_others')
        img = [
            [conf.PHP_URL + '/img/stores-main-image-others.png', '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

    def _stock_image_switch(self, wd, name):
        """Switch images on the edit page"""
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")

        image = self.stock_images.get(name)
        editpage.main_image_wrapper.click()

        popup = MainImagePopup(wd)
        getattr(popup, '{0}_label'.format(name)).click()
        self.assertRegexpMatches(popup.small_preview.get_attribute('src'), '.*' + image + '$')
        popup.submit.click()
        self.assertRegexpMatches(editpage.main_image.get_attribute('src'), '.*' + image + '$')
        editpage.submit.click()

        yapo.utils.rebuild_csearch()
        listing = pages.stores.Listing(wd)
        listing.go()
        item = listing.get_store(0)
        self.assertRegexpMatches(item.bg_image.get_attribute('src'), '.*{0}$'.format(image.replace('-main-image', '-listing-bg').replace('png', 'jpg')))
        item.name.click()
        detail = pages.stores.Detail(wd)
        self.assertRegexpMatches(detail.main_image.get_attribute('src'), '.*{0}$'.format(image))

    def _check_metadata(self, wd, images = []):
        """ Entering a store from desktop """

        head = wd.find_element_by_css_selector("head")
        meta = head.find_elements_by_css_selector("meta")

        tags = MetaTags(wd).get_og_tags()
        print("MetaTags[0] = {}".format(tags[0]))
        print("MetaTags[1] = {}".format(tags[1]))
        self.assertEquals(tags, [self.metadata, images]);

    @tags("desktop", "stores")
    def test_main_image_long(self, wd):
        """Upload a long image on a store and view it in list and detail"""
        edit = EditForm(wd)
        edit.go()
        edit.main_image_wrapper.click()
        popup = MainImagePopup(wd)
        edit.main_image_input.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/long_image.jpg'))
        popup.wait.until_visible('small_preview')
        edit.submit.click()
        yapo.utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        store = listing.get_store(0)
        self.assertRegex(store.bg_image.get_attribute('src'),
                         '.*/store_libg/00/[\d]{10}.jpg/201/0')
        self.assertEquals(store.name.text, "El gabacho de Moniacas")
        store.name.click()
        detail = Detail(wd)
        self.assertRegex(detail.main_image.get_attribute('src'),
                         '.*/[\d]{2}/[\d]{10}.jpg/0/0')
        fb_img_uri = detail.main_image.get_attribute('src').replace('store_main', 'store_fb')
        img = [
            [fb_img_uri, '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

    @tags("desktop", "stores")
    def test_main_image_fit(self, wd):
        """Upload an image with the exact dimensions required
        """
        edit = EditForm(wd)
        edit.go()
        edit.main_image_wrapper.click()
        popup = MainImagePopup(wd)
        edit.main_image_input.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/fit_image.png'))
        popup.wait.until_visible('small_preview')
        edit.submit.click()
        yapo.utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        store = listing.get_store(0)
        self.assertRegex(store.bg_image.get_attribute('src'),
                         '.*/store_libg/00/[\d]{10}.jpg/201/0')
        self.assertEquals(store.name.text, "El gabacho de Moniacas")
        store.name.click()
        detail = Detail(wd)
        self.assertRegex(detail.main_image.get_attribute('src'),
                         '.*/[\d]{2}/[\d]{10}.jpg/0/0')
        fb_img_uri = detail.main_image.get_attribute('src').replace('store_main', 'store_fb')
        img = [
            [fb_img_uri, '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

        expected_md5 = b'4e43956e4ff62edf0a39b900bd56a4fc'
        command = 'wget --no-check-certificate ' + fb_img_uri + ' -O- 2> /dev/null | md5sum | awk "{print $1}"'
        print (command)
        output = subprocess.check_output(command, shell=True)
        self.assertEquals(expected_md5, output[:-4])

    @tags("desktop", "stores")
    def test_main_image_small(self, wd):
        """Upload a small image for store and fail with error"""
        edit = EditForm(wd)
        edit.go()
        edit.main_image_wrapper.click()
        edit.main_image_input.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/imagen_shica.jpg'))
        self.assertEquals([e.text for e in edit.errors], ['La imagen es muy peque�a', ])

    @tags("desktop", "stores")
    def test_logo_small(self, wd):
        """Upload a small custom logo on a store and fail with error"""
        edit = EditForm(wd)
        edit.go()
        edit.logo_upload_field.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/small.png'))
        errors = [e.text for e in edit.errors]
        self.assertEqual(errors[0], "La imagen es muy peque�a")

    @tags("desktop", "stores")
    def test_logo_ok(self, wd):
        """Upload a custom logo on a store and view it in list and detail"""
        edit = EditForm(wd)
        edit.go()
        edit.logo_upload_field.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/logo_ok.jpg'))
        edit.wait.until_visible('logo_uploaded')
        edit.submit.click()
        yapo.utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        store = listing.get_store(0)
        self.assertRegex(store.logo_image.get_attribute('src'),
                         r'.*/logo/00/\d{10}.jpg')
        self.assertEquals(store.name.text, "El gabacho de Moniacas")
        store.name.click()
        detail = Detail(wd)
        self.assertRegex(detail.logo_image.get_attribute('src'),
                         r'.*/logo/00/\d{10}.jpg')

    @tags("desktop", "stores")
    def test_stock_two_image_lost(self, wd):
        """Stores: Saving a store with stock image 2 times

        When saving a store with a stock image then editing it again, changing
        another value (i.e name) and saving; the displayed form comes with no
        image selected.
        """
        name = "stock_cars"
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        image = self.stock_images.get(name)
        editpage.set_stock_image(name)
        editpage.submit.click()
        editpage.fill_form(hide_phone=False, phone="223456")
        editpage.submit.click()
        self.assertRegexpMatches(editpage.main_image.get_attribute('src'),
                                 '.*' + image + '$')
        yapo.utils.rebuild_csearch()
        listing = Listing(wd)
        listing.go()
        store = listing.get_store(0)
        store.name.click()
        detail = Detail(wd)
        img = [
            [conf.PHP_URL + '/img/stores-main-image-vehicles.png', '600', '315'],
            [conf.DESKTOP_URL + '/img/yapo_fb.png', '250', '250']
        ]
        self._check_metadata(wd, img)

    @tags("desktop", "stores")
    def test_edit_logout_and_back(self, wd):
        """Stores: Edit store, logout and try to go back displays login form

        Regression test for case when:
            1. Login & go to edit store
            2. Logout
            3. Press "back" browser button
            4. BAM! your info is still there, even if not logged in
        """
        edit = EditForm(wd)
        edit.go()
        edit.wait.until_visible('name')
        Login(wd).logout()
        create = Create(wd)
        create.name.send_keys("esto no va a aparecer")
        wd.back()
        create.wait.until(lambda x: create.name.get_attribute('value') == "")
        self.assertEqual(create.title.text, "Mi cuenta")
