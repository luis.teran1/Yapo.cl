# -*- coding: latin-1 -*-
from yapo.pages import mobile
import logging
import yapo
from yapo.decorators import tags
import datetime
from yapo import utils
import time


class BuyStoresByCartOnMobile(yapo.SeleniumTest):

    """Test the extension of stores"""

    snaps = ['accounts', 'diff_store_3shops']
    user_agent = utils.UserAgents.IOS
    bconfs = {
        "*.*.payment.product.4.expire_time": "31 days",
        '*.*.movetoapps_splash_screen.enabled': '0'
    }

    @tags('stores', 'mobile')
    def test_buy_store_on_mobile(self, wd):
        """Buy a montly store in mobile """
        mlogin_page = mobile.Login(wd)
        mlogin_page.login(user='prepaid3@blocket.se', password='123123123')
        msummary_page = mobile.SummaryPayment(wd)
        msummary_page.add_store(str(utils.Products.STORE_MONTHLY))

        table_values = msummary_page.get_table_data()
        expected_values = [['Tienda', 'Mensual', '$6.000', '', '']]

        self.assertListEqual(table_values, expected_values)
        self.assertEqual(msummary_page.text_products_count.text, '1')

        msummary_page.choose_bill()
        msummary_page.pay_with_webpay()

        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = mobile.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, 'Tu compra se realiz� exitosamente')

        msummary_page.go_summary()
        self.assertFalse(msummary_page.is_element_present('products_list_table'))

        self.assertListEqual(table_values, expected_values)
        self.assertEqual(msummary_page.text_products_count.text, '0')
        self.assertTrue(msummary_page.text_empty_cart_message.is_displayed())
