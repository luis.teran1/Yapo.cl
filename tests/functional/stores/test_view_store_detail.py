# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages import stores
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
class StoresViewDetail(yapo.SeleniumTest):

    snaps = ['accounts','diff_store_3shops']

    stores_info = [
            {'url':'tienda-test','name':'Tienda test','desc':'tienda test\ntienda test\ntienda test','nads':'5'},
            {'url':'tienda-android','name':'Tienda android','desc':'tienda android\ntienda android\ntienda android','nads':'50'},
            {'url':'tienda-no-ads','name':'Tienda no-ads','desc':'tienda no-ads\ntienda no-ads\ntienda no-ads','nads':'0'}
            ]

    store_test_ads = [
            {
                'date': 'Hoy\n18:41',
                'subj': 'Adeptus Astartes Codex',
                'price':'$ 778.889',
                'cat':  'Libros y revistas',
                'reg':  'Regi�n Metropolitana',
                'sbp':  '4'
             },
            {
                'date': '30 Ago\n12:23',
                'subj': 'Aviso de prepaid5',
                'price':'$ 564.456',
                'cat':  'Consolas, videojuegos y accesorios',
                'reg':  'Regi�n Metropolitana',
                'sbp':  '3'
            },
            {
                'date': '9 Nov\n11:00',
                'subj': 'Departamento Tarapac�',
                'price':'$ 45.000.000',
                'cat':  'Comprar',
                'reg':  'I Tarapac�',
                'sbp':  '5'
            },
            {
                'date': '5 Abr\n10:21',
                'subj': 'Pesas muy grandes',
                'price':'$ 90.000',
                'cat':  'Deportes, gimnasia y accesorios',
                'reg':  'X Los Lagos',
                'sbp':  '2'
            },
            {
                'date': '5 Abr\n09:21',
                'subj': 'Race car',
                'price':'$ 11.667',
                'cat':  'Autos, camionetas y 4x4',
                'reg':  'IX Araucan�a',
                'sbp':  '1'
            }
            ]

    @tags('desktop', 'stores')
    def test_nonexistent_store(self, wd):
        """ Verify store not available """
        storePage = stores.Detail(wd)
        storePage.go('esta-store-no-ta')
        self.assertEqual(storePage.store_not_found_message.text,"Esta tienda no est� disponible")
        self.assertEqual(storePage.store_not_found_message_button.text,"Ver otras tiendas")
        storePage.store_not_found_message_button.click()
        listingPage = stores.Listing(wd)
        self.assertTrue(listingPage.head_title.is_displayed())

    @tags('desktop', 'stores')
    def test_basic_elements(self, wd):
        """ Verify basic elements of stores are available """
        storePage = stores.Detail(wd)
        for i in range(len(self.stores_info)):
            url = ''
            name = ''
            desc = ''
            for key, value in self.stores_info[i].items():
                if( key == 'url' ):
                    url = value
                if( key == 'name' ):
                    name = value
                if( key == 'desc' ):
                    desc = value
            storePage.go(url)
            self.assertEqual(storePage.name.text,name)
            self.assertTrue(storePage.name.is_displayed())
            self.assertEqual(storePage.description.text,desc)
            self.assertTrue(storePage.description.is_displayed())

    @tags('desktop', 'stores')
    def test_store_without_ads(self,wd):
        """ Verify the message of store without ads """
        storePage = stores.Detail(wd)
        storePage.go('tienda-no-ads')
        self.assertEqual(storePage.store_no_ads_found_message.text,"Tienda sin avisos")

    @tags('desktop', 'stores')
    def test_store_ads_newest_first(self,wd):
        """ Verify the ads of test store are sort by date (default) """
        storePage = stores.Detail(wd)
        storePage.go('tienda-test')
        for i in range(len(self.store_test_ads)):
            date = ''
            subject = ''
            price = ''
            for key, value in self.store_test_ads[i].items():
                if( key == 'date' ):
                    date = value
                if( key == 'subj' ):
                    subject = value
                if( key == 'price' ):
                    price = value
            #self.assertEqual(storePage.listing_ads_dates[i].text,date)
            self.assertEqual(storePage.listing_ads_subj[i].text,subject)
            self.assertEqual(storePage.listing_ads_price[i].text,price)

    @tags('desktop', 'stores')
    def test_search_without_ads(self,wd):
        """ Verify the message of search without ads """
        storePage = stores.Detail(wd)
        storePage.go('tienda-test')
        storePage.search_text.send_keys("lalala")
        storePage.search_button.click()
        self.assertEqual(storePage.store_no_ads_found_message.text,"No encontramos resultados para tu b�squeda")
        storePage.store_no_ads_found_go_back.click()
        self.assertEqual(storePage.search_text.text, "")

    @tags('desktop', 'stores')
    def test_search_ads_by_text(self,wd):
        """ Verify the message of search ads by text"""
        storePage = stores.Detail(wd)
        storePage.go('tienda-test')
        self.assertEqual(len(storePage.listing_ads_price),5)
        storePage.search_text.send_keys("Departamento")
        storePage.search_button.click()
        self.assertEqual(len(storePage.listing_ads_price),1)
        self.assertEqual(storePage.listing_ads_subj[0].text, "Departamento Tarapac�")

    @tags('desktop', 'stores')
    def test_store_ads_sort_by_price(self,wd):
        """ Verify the ads of test store are sort by price  """
        storePage = stores.Detail(wd)
        storePage.go('tienda-test')
        storePage.sort_by_price("M�s barato")
        #Sort array by price
        newlist = sorted(self.store_test_ads, key=lambda k: k['sbp'])
        for i in range(len(newlist)):
            date = ''
            subject = ''
            price = ''
            for key, value in newlist[i].items():
                if( key == 'date' ):
                    date = value
                if( key == 'subj' ):
                    subject = value
                if( key == 'price' ):
                    price = value
            #self.assertEqual(storePage.listing_ads_dates[i].text,date)
            self.assertEqual(storePage.listing_ads_subj[i].text,subject)
            self.assertEqual(storePage.listing_ads_price[i].text,price)

    @tags('desktop', 'stores')
    def test_store_ads_filter_by_category(self,wd):
        """ Verify the ads of test store are filter by category  """
        storePage = stores.Detail(wd)
        storePage.go('tienda-test')
        self.assertEqual(len(storePage.listing_ads_price),5)
        storePage.filter_by_category("Comprar")
        storePage.search_button.click()
        self.assertEqual(len(storePage.listing_ads_price),1)
        self.assertEqual(storePage.listing_ads_subj[0].text, "Departamento Tarapac�")

    @tags('desktop', 'stores')
    def test_store_ads_persist_filters_scrolling_loading_more_ads(self,wd):
        """ Verify the filters are persistent when use scroll and load a new page """
        storePage = stores.Detail(wd)
        storePage.go('tienda-android')
        self.assertEqual(len(storePage.listing_ads_subj),5)
        storePage.filter_by_category("Consolas, videojuegos y accesorios")
        storePage.sort_by_price("M�s barato")
        storePage.search_text.send_keys("estado OR xbox")
        storePage.search_button.click()
        self.assertEqual(len(storePage.listing_ads_subj),2)
        self.assertEqual(storePage.listing_ads_subj[0].text, "Call of Duty Black ops II Xbox 360")
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        wd.execute_script("$('#hl').infinitescroll('retrieve');")
        self.assertEqual(len(storePage.listing_ads_subj),2)
        self.assertEqual(storePage.listing_ads_subj[1].text, "Xbox 360 con sensor kinect y juegos")
        #Verify filters parameters persist
        self.assertEqual(storePage.search_text.get_attribute("value"),"estado OR xbox")
        self.assertEqual(storePage.search_filter_by_category_actual.text,"Consolas, videojuegos y accesorios")
        self.assertEqual(storePage.search_sort_by_price_actual.text,"M�s barato")
