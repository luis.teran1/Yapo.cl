# -*- coding: latin-1 -*-
from nose.plugins.skip import SkipTest
from selenium.webdriver.common.action_chains import ActionChains
from yapo.decorators import tags
from yapo.pages import accounts, control_panel, generic, desktop_list
from yapo.pages.stores import EditForm, Listing 
from yapo import utils
import os
import time
import yapo
import yapo.conf


class NavigationBarTest(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']

    @tags('desktop', 'stores')
    def test_store_icon(self, wd):
        """Check that the icon is shown when no logo is configured for the store"""
        adview = desktop_list.AdViewDesktop(wd)
        adview.go('8000073')
        self.assertTrue(adview.store_icon)

    @tags('desktop', 'stores')
    def test_store_logo(self, wd):
        """Check that the logo is shown when is set the logo for the store"""
        edit = EditForm(wd)
        edit.go(user='prepaid5@blocket.se')
        edit.logo_upload_field.send_keys(
            os.path.join(yapo.conf.RESOURCES_DIR, 'store_images/logo_ok.jpg'))
        edit.wait.until_visible('logo_uploaded')
        edit.submit.click()
        utils.rebuild_csearch()
        adview = desktop_list.AdViewDesktop(wd)
        adview.go('8000073')
        self.assertTrue(adview.store_logo)

    @tags('desktop', 'adsense')
    def test_adsense(self, wd):
        """Check that the adsense links does not appear on the footer"""
        adview = desktop_list.AdViewDesktop(wd)
        adview.go('8000073')
        adview.wait.until_not_present('adsense_links')

    @tags('desktop', 'adsense')
    def test_enable_adsense(self, wd):
        """Check that the adsense links appears when enabled on bconf"""
        utils.bconf_overwrite('*.*.adsense.adview.enabled', 1)
        try:
            adview = desktop_list.AdViewDesktop(wd)
            adview.go('8000073')
            adview.wait.until_present('adsense_links')
        finally:
            utils.bconf_overwrite('*.*.adsense.adview.enabled', 0)

    @tags('desktop', 'stores')
    def test_unlogged_new_store_link(self, wd):
        """Check that the 'create store' link inside stores takes you to login page when unlogged"""
        adlist = desktop_list.AdListDesktop(wd)
        adlist.go()
        adlist.stores_link.click()
        self.assertEquals('{0}{1}'.format(yapo.conf.DESKTOP_URL,'/tiendas'), wd.current_url)
        storelist = Listing(wd)
        storelist.create_store_link.click()
        self.assertEquals('{0}{1}'.format(yapo.conf.SSL_URL,'/login'), wd.current_url)
