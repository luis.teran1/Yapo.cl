# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo import utils
from yapo.utils import Products, Label_texts
from yapo.steps.pay_products_logged import transbank_pay_product
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.stores import StoreView


class LabelStoreDetail(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']

    def test_check_label_in_store_view(self, wd):
        """
        Check label in store view after purchase from dashboard.
        """
        LIST_IDS = {8000066: [Products.LABEL]}
        login_and_go_to_dashboard(wd, 'android@yapo.cl', '123123123')
        transbank_pay_product(wd, 8000066, Products.LABEL)
        utils.rebuild_index()
        utils.rebuild_csearch()

        storeView = StoreView(wd)
        storeView.go_store('tienda-android')

        for list_id, ad in storeView.ads.items():
            if list_id == 8000066:
                self.assertEqual(ad.label_tag.text, Label_texts.URGENT)
