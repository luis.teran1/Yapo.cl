# -*- coding: latin-1 -*-
from yapo.pages import autobump
from yapo.pages.stores import BuyStore
from yapo.pages.desktop import SummaryPayment, DashboardMyAds
import logging
import yapo
from yapo import utils
from yapo.decorators import tags
import datetime
import time

class BuyStoresByCart(yapo.SeleniumTest):
    """Test the extension of stores"""

    snaps = ['accounts','diff_store_3shops']
    bconfs = {
        "*.*.payment.product.4.expire_time":"31 days",
        '*.*.autobump.visible_last_digit.6': '0',
    }


    @tags("stores")
    def test_replace_store_payment(self, wd):
        """ Store: buy an montly store, then go out and try to buy a new store"""
        buy_stores_page = BuyStore(wd)
        buy_stores_page.go(user='prepaid3@blocket.se', password='123123123')
        summary_page = SummaryPayment(wd)
        summary_page.add_store(utils.Products.STORE_MONTHLY)
        table_values = summary_page.get_table_data('products_list_table')
        expected_values = [['Tienda', 'Mensual', '$ 6.000', '']]
        self.assertListEqual(table_values, expected_values)

        buy_stores_page.go_without_login()
        summary_page.add_store(utils.Products.STORE_ANNUAL)
        table_values = summary_page.get_table_data('products_list_table')
        expected_values = [['Tienda', 'Anual', '$ 48.000', '']]
        self.assertListEqual(table_values, expected_values)

    @tags("stores")
    def test_replace_store_extension(self, wd):
        buy_stores_page = BuyStore(wd)
        buy_stores_page.go(user='prepaid5@blocket.se', password='123123123', wait_animation=None)
        buy_stores_page.monthly.click()
        summary_page = SummaryPayment(wd)
        table_values = summary_page.get_table_data('products_list_table')
        expected_values = [['Tienda', 'Mensual', '$ 6.000', '']]
        self.assertListEqual(table_values, expected_values)
        buy_stores_page.go_without_login(wait_animation=None)
        buy_stores_page.annual.click()

        table_values = summary_page.get_table_data('products_list_table')
        expected_values = [['Tienda', 'Anual', '$ 48.000', '']]
        self.assertListEqual(table_values, expected_values)

