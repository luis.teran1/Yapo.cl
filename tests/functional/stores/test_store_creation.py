# -*- coding: latin-1 -*-
from yapo.decorators import tags
import yapo
from yapo import pages
from yapo.pages import desktop
from yapo.pages.desktop import SummaryPayment
from yapo.pages.simulator import Simulator, PaymentSuccess
from yapo.pages.generic import AccountBar, SentEmail
from yapo.pages.stores import BuyStore as BuyStorePage
from yapo.pages.stores import EditForm
from yapo.pages.simulator import PaymentError, PaymentWarning

from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.steps.stores import buy_store_with_invoice, buy_store_with_bill, edit_store, start_payment, buy_store_servipag_no_xml2

from yapo import conf
import logging
import datetime

logger = logging.getLogger(__name__)


class BuyStoreTest(yapo.SeleniumTest):
    snaps = ['accounts']

    store_info = {
        'name': "El gabacho de Moniacas",
        'info_text': "La descripci�n de la tinedita esta overrated",
        'phone': "666666666",
        'region': "15",
        'commune': "295",
        'address': "Calle yolanda sultana",
        'address_number': "1313",
        'website_url': "�and�.cl",
        'hide_phone': False,
    }

    store_view_data = {
        'name': "El gabacho de Moniacas",
        'description': 'La descripci�n de la tinedita esta overrated',
        'phone': "666666666",
        'address': "Calle yolanda sultana 1313, Alhu�",
        'site': "�and�.cl",
    }

    bconfs = {
        '*.*.payment.product.4.expire_time': '31 days',
        '*.*.payment.product.5.expire_time': '93 days',
        '*.*.payment.product.6.expire_time': '186 days',
        '*.*.payment.product.7.expire_time': '372 days',
    }

    def setUp(self):
        yapo.utils.restore_snaps_in_a_cooler_way(['accounts'])
        yapo.utils.flush_redises()


    def assert_form_data(self, page, info):
        for fieldname, value in info.items():
            if fieldname in ('hide_phone',):
                self.assertEqual(getattr(page, fieldname).is_selected(), value,
                                "Values for field {0} don't match".format(fieldname))
            else:
                self.assertEqual(getattr(page, fieldname).get_attribute('value'),
                                value,
                                "Values for field {0} don't match".format(fieldname))

    @tags("stores", "account", "payment")
    def test_mod_security_store_email(self, wd):
        """Purchase a store set it up via email"""
        yapo.utils.rebuild_csearch()
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")

        invoice_data = {
            'lob': "Venta de liquidos espirituosos",
            'rut': "19057825-9",
            'communes': "295",
            'address': "Calle yolanda sultana 1313",
        }
        buy_store_with_invoice(wd, 'monthly', invoice_data)

        page = PaymentSuccess(wd)
        self.assertEqual(page.total.text, "$ 6.000")

        email = SentEmail(wd)
        email.go()
        link = email.body.find_element_by_partial_link_text(
            "Estrena tu tienda").get_attribute("href")

        wd.get(link)

        edit_store(wd, self.store_info)
        page = EditForm(wd)
        page.wait.until_visible('save_message')
        self.assertTrue(page.save_message.is_displayed())
        self.assert_form_data(page, self.store_info)
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(page.get_expiration_date(), expected_expiration)

        yapo.utils.rebuild_csearch()
        listing = pages.stores.Listing(wd)
        listing.go()
        listing.get_store(0).name.click()
        detail = pages.stores.Detail(wd)
        self.assertEqual(detail.get_data(), self.store_view_data)

    @tags("stores", "account", "payment", 'minimal')
    def test_buy_store_invoice(self, wd):
        """Purchase a store with invoice option"""
        yapo.utils.rebuild_csearch()
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")

        invoice_data = {
            'lob': "Venta de liquidos espirituosos",
            'rut': "19057825-9",
            'communes': "295",
            'address': "Calle yolanda sultana 1313",
        }
        buy_store_with_invoice(wd, 'monthly', invoice_data)

        page = PaymentSuccess(wd)
        self.assertEqual(page.total.text, "$ 6.000")
        page.edit_store.click()

        edit_store(wd, self.store_info)
        page = EditForm(wd)
        page.wait.until_visible('save_message')
        self.assertTrue(page.save_message.is_displayed())
        self.assert_form_data(page, self.store_info)
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(page.get_expiration_date(), expected_expiration)

        yapo.utils.rebuild_csearch()
        listing = pages.stores.Listing(wd)
        listing.go()
        listing.get_store(0).name.click()
        detail = pages.stores.Detail(wd)
        self.assertEqual(detail.get_data(), self.store_view_data)

    @tags("stores", "payment")
    def test_buy_store_bill(self, wd):
        """Purchase a store with bill option"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")
        buy_store_with_bill(wd, 'monthly')

        page = PaymentSuccess(wd)
        self.assertEqual(page.total.text, "$ 6.000")
        page.edit_store.click()

        edit_store(wd, self.store_info)
        page = EditForm(wd)
        page.wait.until_visible('save_message')
        self.assertTrue(page.save_message.is_displayed())
        self.assert_form_data(page, self.store_info)
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(page.get_expiration_date(), expected_expiration)

        yapo.utils.rebuild_csearch()
        listing = pages.stores.Listing(wd)
        listing.go()
        listing.get_store(0).name.click()
        detail = pages.stores.Detail(wd)
        self.assertEqual(detail.get_data(), self.store_view_data)

    @tags("stores", "payment")
    def test_buy_store_invoice_errors(self, wd):
        """Tries to buy a store with invalid invoice data"""
        page = BuyStorePage(wd)
        page.go("many@ads.cl", "123123123")
        page.monthly.click()
        SummaryPayment(wd).radio_webpay.click()
        page.wait.until_visible('bill')
        page.bill.click()
        form = page.form
        form.email.send_keys('emailinvalido@lol')
        page.submit.click()

        expected_errors = ['Confirma que el email est� en el formato correcto']
        self.assertEqual(expected_errors, [e.text for e in page.form.errors])

        page = BuyStorePage(wd)
        page.wait.until_visible('invoice')
        page.invoice.click()
        invoice_data = [
            ('name', ""),
            ('lob', ""),
            ('rut', ""),
            ('address', ""),
            ('region', ""),
            ('communes', ""),
            ('contact', ""),
            ('email', "emailinvalido@lol")
        ]
        form = page.form
        form.ordered_fill_form(invoice_data)

        page.submit.click()
        expected_errors = [
            "Confirma que el email est� en el formato correcto",
            "Escribe tu nombre",
            "Escribe tu giro",
            "Escribe un rut v�lido",
            "Escribe tu direcci�n",
            "Selecciona una regi�n",
            "Selecciona una comuna"]
        self.assertEqual(expected_errors, [e.text for e in page.form.errors])

    @tags("stores", "payment")
    def test_buy_and_cancel(self, wd):
        """Tries to buy a store but cancels the operation in paysim"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")
        simulator = start_payment(wd, 'monthly')
        simulator.cancel.click()
        simulator.go_to_site.click()
        errorpage = PaymentError(wd)
        self.assertEqual(errorpage.title.text, "Oops... La transacci�n no pudo ser realizada")

    @tags("stores", "payment")
    def test_buy_and_refuse(self, wd):
        """Tries to buy a store but refuses the operation in paysim"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")
        simulator = start_payment(wd, 'monthly')
        simulator.refuse.click()
        simulator.go_to_site.click()
        errorpage = PaymentError(wd)
        self.assertEqual(errorpage.title.text, "Oops... La transacci�n no pudo ser realizada")

    @tags("stores", "payment")
    def test_buy_price_mismatch(self, wd):
        """Tries to buy a store but change the price in paysim"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")
        simulator = start_payment(wd, 'monthly')
        simulator.fill_form(amount="599000")
        simulator.accept.click()
        simulator.go_to_site.click()
        errorpage = PaymentWarning(wd)
        self.assertEqual(errorpage.title_error.text,
                         "Oops... La transacci�n no pudo ser realizada")
        self.assertEqual(errorpage.description_selector.text,
                         "Parece que los datos del documento no coinciden con los indicados.")

    @tags("stores", "account", "payment")
    def test_buy_store_invoice_quoted_name(self, wd):
        """Purchase a store with invoice option using quotes in the name (FT1050)"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")

        invoice_data = {
            'name': "Analakrobat's store",
            'lob': "Venta de liquidos espirituosos",
            'rut': "19057825-9",
            'communes': 295,
            'address': "Calle yolanda sultana 1313",
        }
        buy_store_with_invoice(wd, 'monthly', invoice_data)

        page = PaymentSuccess(wd)
        self.assertEqual(page.total.text, "$ 6.000")

    @tags("stores", "payment", "servipag")
    def test_buy_store_servipag_no_xml2(self, wd):
        """Purchase a store with servipag not sending xml2"""
        login_and_go_to_dashboard(wd, "many@ads.cl", "123123123")
        buy_store_servipag_no_xml2(wd, 'monthly')
        payment_verify = desktop.PaymentVerify(wd)
        self.assertIn('demora en su activaci�n', payment_verify.text_congratulation_message.text)
        wd.find_elements_by_css_selector('.bottom-options a')

