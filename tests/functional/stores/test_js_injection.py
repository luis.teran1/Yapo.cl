# -*- coding: latin-1 -*-
from yapo.decorators import tags
from yapo.pages.stores import EditForm, Detail
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import logging
import yapo

logger = logging.getLogger(__name__)


class StoreJsInjection(yapo.SeleniumTest):


    snaps = ['accounts', 'singlestore']

    @tags("desktop", "stores", 'minimal')
    def test_js_sanitization(self, wd):
        """
        Test that Javascript tags are removed from the description
        """
        editpage = EditForm(wd)
        editpage.go('many@ads.cl', "123123123")
        new_info_text = "lorem ipsum\ndolor sit amet\n<script>alert('yolo')</script>"
        editpage.fill_form(info_text=new_info_text)
        editpage.submit.click()
        yapo.utils.rebuild_csearch()
        detail = Detail(wd)
        detail.go('el-gabacho-de-moniacas')
        self.assertFalse(self._is_alert_present(wd))


    def _is_alert_present(self, wd):
        """
        Returns True if there is an alert, False otherwise
        """
        try:
            alert = wd.switch_to_alert()
            alert.accept()
            return True
        except:
            return False
