# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags
from yapo import utils
from yapo.pages import stores


class EnterStoreByMobile(yapo.SeleniumTest):

    """Test the extension of stores"""

    snaps = ['accounts', 'singlestore']
    user_agent = utils.UserAgents.IOS

    @tags('stores', 'mobile')
    def test_buy_store_on_mobile(self, wd):
        """ Entering a store from mobile """
        detail = stores.Detail(wd)
        detail.go("el-gabacho-de-moniacas")
        self.assertTrue(detail.name.is_displayed())
        self.assertEquals("El gabacho de Moniacas", detail.name.text)
