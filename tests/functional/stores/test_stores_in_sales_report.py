# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo.pages import control_panel
from yapo import utils
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
class StoresInSalesReportWebSQL(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_sales_report']

    filters = dict(timestamp_from='2014-10-01 00:00:00',
                   timestamp_to='2014-10-01 23:59:59')

    @tags('controlpanel', 'telesales', 'websql')
    def test_websql_sales_report(self, wd):
        """ Websql sales report for stores (data) """

        # going to the controlpanel to get the timestamps
        websql = control_panel.WebSQL(wd)
        websql.login('dany', 'dany')
        websql.call_websql('Sales Report', **self.filters)
        result_data = websql.get_result_data()

        # testing
        data = [
            ['166', u'2014-10-01', u'11:17:14', u'prepaid5@blocket.se', u'Profesional', u'Tienda Anual',      u'',        u'---',                                  u'',                        u'paid',     u'', u'', u'', u'48000', u'', u'', u'confirmed', u'invoice', u'1', 'unknown', 'webpay'],
            ['167', u'2014-10-01', u'11:17:32', u'prepaid5@blocket.se', u'Persona',     u'Subir 1',           u'8000073', u'Tiempo libre - Libros y revistas',     u'Regi\xf3n Metropolitana', u'paid',     u'', u'', u'', u'1000' , u'', u'', u'confirmed', u'bill',    u'2', 'unknown', 'webpay'],
            ['168', u'2014-10-01', u'11:22:29', u'prepaid5@blocket.se', u'Persona',     u'Subir 1',           u'8000083', u'Otros - Otros productos',              u'Regi\xf3n Metropolitana', u'paid',     u'', u'', u'', u'1000' , u'', u'', u'confirmed', u'invoice', u'3', 'unknown', 'webpay'],
            ['168', u'2014-10-01', u'11:22:29', u'prepaid5@blocket.se', u'Persona',     u'Subir 4',           u'3456789', u'Veh�culos - Autos - camionetas y 4x4', u'IX Araucan\�a',           u'paid',     u'', u'', u'', u'2990' , u'', u'', u'confirmed', u'invoice', u'3', 'unknown', 'webpay'],
            ['171', u'2014-10-01', u'11:23:02', u'many@ads.cl',         u'Profesional', u'Tienda Trimestral', u'',        u'---',                                  u'',                        u'paid',     u'', u'', u'', u'15000', u'', u'', u'confirmed', u'bill',    u'4', 'unknown', 'webpay'],
        ]

        for row in range(len(data)):
            for cel in range(row):
                self.assertEquals(result_data[row][cel], data[row][cel])

