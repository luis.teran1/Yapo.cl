# -*- coding: latin-1 -*-
from yapo.pages.stores import EditForm, MainImagePopup, Listing, Detail
import logging
import yapo
from yapo.decorators import tags
from yapo.steps.stores import buy_store_with_invoice
import datetime
from yapo import utils

class StoreExtension(yapo.SeleniumTest):
    """Test the extension of stores"""

    bconfs = { "*.*.payment.product.4.expire_time":"31 days" }

    def setUp(self):
        utils.restore_snaps_in_a_cooler_way(['accounts', 'diff_store_3shops'])
        utils.rebuild_csearch()

    @tags("desktop", "stores")
    def test_expiration_date(self, wd):
        """ Stores: Check Expiration date """
        edit_form = EditForm(wd)
        edit_form.go(user='prepaid5@blocket.se', password='123123123')
        buy_store_with_invoice(wd, 'monthly', None)
        edit_form.go_without_login()
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(edit_form.get_expiration_date(), expected_expiration)

    @tags("desktop", "stores")
    def test_extend_expired_store(self, wd):
        """ Stores: Check expiration date after renew an expired store """
        edit_form = EditForm(wd)
        edit_form.go(user='user.01@schibsted.cl', password='123123123')
        buy_store_with_invoice(wd, 'monthly', None)
        edit_form.go_without_login()
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(edit_form.get_expiration_date(), expected_expiration)

    @tags("desktop", "stores")
    def test_reactivate_store_plan(self, wd):
        """ Stores: Deactivate store and check expiration """
        edit_form = EditForm(wd)
        edit_form.go(user='no-ads@yapo.cl', password='123123123')
        edit_form.deactivate.click()
        edit_form.deactivate_yes.click()
        edit_form.go_to_dashboard()
        buy_store_with_invoice(wd, 'monthly', None)
        edit_form.go_without_login()
        expected_expiration = datetime.datetime.now().date() + datetime.timedelta(days=31)
        self.assertEqual(edit_form.get_expiration_date(), expected_expiration)

    @tags("desktop", "stores")
    def test_deactivate_store_valid_plan(self, wd):
        """ Stores: Deactivate by admin, extension is not allowed """
        utils.trans("update_store_status", 1, status="admin_deactivated", store_id=1)
        edit_form = EditForm(wd)
        edit_form.go(user='prepaid5@blocket.se', password='123123123')
        edit_form.wait.until_not_present("extend_form")
