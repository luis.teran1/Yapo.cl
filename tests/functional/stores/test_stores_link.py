# -*- coding: latin-1 -*-
from yapo import pages
from yapo.decorators import tags
from yapo import utils
from yapo.pages.stores import Listing
from nose_selenium import ScreenshotOnExceptionWebDriverWait as Wait
from selenium import webdriver
import time
import yapo
import yapo.conf
import re

class StoresListing(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    bconfs = {'*.*.stores.listing.stores_per_page': '1' }

    @tags("desktop", "stores")
    def test_store_list_preselected_region(self, wd):
        """Selected region on store listing is the same as the selected in the home page"""
        region = 'Regi�n Metropolitana'
        listing = Listing(wd)
        listing.go_from_region(region)
        self.assertEqual(listing.selected_region.text, region, 'The region mismatch with the selected in the home page')

    @tags("desktop", "stores")
    def test_store_list_order(self, wd):
        """First store is the one that has the newest ad"""
        listing = Listing(wd)
        listing.go()
        self.assertEqual(listing.first_store_name.text, 'Tienda test', 'The desired store was not found')

    @tags("desktop", "stores")
    def test_store_list_ads_number_and_region(self, wd):
        """The stores have number of ads and region"""
        listing = Listing(wd)
        listing.go()
        for ads_count in listing.stores_ads_number:
            self.assertTrue(re.match('\d+ avisos?', ads_count.text), 'Problems with the ads number')
        for region in listing.stores_regions:
            self.assertEqual(region.text, 'Regi�n Metropolitana', 'Problems with the ads region')

    @tags("desktop", "stores", "minimal")
    def test_verify_visibility_button_create_store_usser_unlogged(self,wd):
        """Verify the visibility of the button create Stores for a usser without account"""
        listing=Listing(wd)
        listing.go()
        self.assertTrue(listing.store_create_button)

    @tags("desktop", "stores", "minimal")
    def test_verify_visibility_button_create_store_usser_logged(self,wd):
        """Verify the visibility of the button create Stores and not have a store create yet"""
        listing=Listing(wd)
        listing.login()
        listing.go()
        self.assertTrue(listing.store_create_button)

    @tags("desktop", "stores", "minimal")
    def test_verify_visibility_button_create_store_usser_whit_store(self,wd):
        """Verify the visibility of the button create Stores for a usser whit alredy have one store"""
        listing=Listing(wd)
        listing.login(user="prepaid5@blocket.se",password="123123123")
        listing.go()
        listing.wait.until_not_present("store_create_button")

    @tags("desktop", "stores")
    def test_verify_search_store(self,wd):
        """Verify the search store functionality of the listing"""
        listing=Listing(wd)
        listing.go()
        listing.fill_form(search_input="Tienda test")
        listing.search_button.click()
        self.assertTrue(len(listing.store_names)==1)
        self.assertEqual(listing.store_names[0].text,"Tienda test")

    @tags("desktop", "stores")
    def test_search_region_store(self,wd):
        """Verify the search store functionality of the listing by region"""
        listing=Listing(wd)
        listing.go()
        listing.filter_by_region("XII Magallanes & Ant�rtica")
        listing.search_button.click()
        self.assertTrue(len(listing.store_names)==1)
        self.assertEqual(listing.store_names[0].text,"Tienda no-ads")

    @tags("desktop", "stores")
    def test_verify_search_without_result_store(self,wd):
        """Verify the search store functionality of the listing when if dont have result"""
        listing=Listing(wd)
        listing.go()
        listing.fill_form(search_input="3121iisncac")
        listing.search_button.click()
        self.assertEqual(listing.search_without_result_message.text,"B�squeda sin resultados")

class StoresListingPagination(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_store_3shops']
    bconfs = {'*.*.stores.listing.stores_per_page': '1' }

    @tags("desktop", "stores")
    def test_pagintation_search_store(self, wd):
        """Verify the search store functionality of the paggin of the listing"""
        listing = Listing(wd)
        listing.go()
        listing.filter_by_region("Todo Chile")
        listing.search_button.click()
        self.assertEqual(len(listing.pagination_stores), 5)
        for i in range(len(listing.pagination_stores)-1):
            self.assertEqual(listing.pagination_stores[i].text, str(i+1))
        self.assertTrue(listing.pagination_next)

    @tags("desktop", "stores")
    def test_pagintation_persistence_search_store(self, wd):
        """Use store pagination links"""
        listing = Listing(wd)
        listing.go()
        listing.fill_form(search_input="Tienda")
        listing.filter_by_region("Todo Chile")
        listing.search_button.click()
        listing.pagination_stores[2].click()
        self.assertEqual(listing.search_input.get_attribute("value"), "Tienda")
        self.assertEqual(listing.select_regions.text, "Todo Chile")
