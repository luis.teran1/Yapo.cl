#!/usr/bin/env bash
#
# This script is the preferred way to run Narco-tests.
# If at all posible, avoid calling nosetests directly.
# Usage:
#	nose.sh: Run all tests
#	nose.sh <testfile>: Run an specific test file
#


# setup local environment
export TEMP=$(pwd)/tmp
export PYTHONPATH=$(pwd)
mkdir -p $TEMP

NOSETESTS=`which nosetests-3.4 2> /dev/null`

if [ "a${NOSETESTS}" == "a" ]; then
       NOSETESTS=`which nosetests-3.3 2> /dev/null`
fi

sh ./startx.sh 2> /dev/null

# build the environment and copy the result in a specific folder
build() {
	cd ..
	FLAVOR=regress ./compile.sh
	mkdir -p $BUILD_PATH
	rm -f $BUILD_PATH/build.tar
	tar vcf $BUILD_PATH/build.tar .
}

# run an entire test folder with an specific conf file
run_all() {
	local testtype=$1			# folder with the testsuite we are to run
	local prefix=$2				# prefix for test files
	local setup=$3				# setup file under which to run it
	local folder=functional

	# set default setup file
	if [ "a${setup}" = "a" ]
	then
		setup=setup.cfg
	fi

	echo "Running ${testtype} test suites"
	echo "=================================="
	echo ""

	# gotta run 'em all
	for test in $(ls ${folder}/${prefix}_*)
	do
		echo "Test suite ${test##${folder}/}"
		echo "----------------------------------------------------------------------"
		if [ -z "$JENKINS" ]; then
			$NOSETESTS -c ${setup} ${test##${folder}/}
		else
			XMLREPORT=$(echo "$test" | sed -e 's/\//\./g' | sed -e 's/\.py//g')
			$NOSETESTS -c ${setup} ${test##${folder}/} --with-xunit --xunit-file=$XMLREPORT.xml
		fi
	done
}


# cleanup code intended to be run upon receiving exit signals
cleanup() {
	rm -rf $TEMP
	exit
}


if [ "$1" = "--build" ]; then
	build
	exit 0
fi

if [ "$1" = "--jenkins" ]; then
	shift
	JENKINS=1
	rm -f *.xml
fi

# setup signal handlers
trap cleanup INT TERM

# no parameters? run ALL the tests \o
# otherwise keep legacy behavior
if [ "a$1" = "a" ]
then
	run_all parallel ptest setup-parallel.cfg
	run_all functional test setup-functional.cfg
	run_all cart cart/test setup-functional.cfg
	run_all control_panel control_panel/test setup-functional.cfg
	run_all desktop desktop/test setup-functional.cfg
	run_all help help/test setup-functional.cfg
	run_all nubox nubox/test setup-functional.cfg
	run_all products products/test setup-functional.cfg
	run_all servipag servipag/test setup-functional.cfg
	run_all stores stores/test setup-functional.cfg
	run_all telesales telesales/test setup-functional.cfg
	run_all websql websql/test setup-functional.cfg
	run_all whitelist whitelist/test setup-functional.cfg
	run_all tealium tagging/tealium/test setup-functional.cfg
else
	case "$1" in
		parallel)
			run_all parallel ptest setup-parallel.cfg
			;;
		functional)
			run_all functional test setup-functional.cfg
			;;
		desktop)
			run_all desktop desktop/test setup-functional.cfg
			;;
		help)
			run_all help help/test setup-functional.cfg
			;;
		products)
			run_all products products/test setup-functional.cfg
			;;
		stores)
			run_all stores stores/test setup-functional.cfg
			;;
		nubox)
			run_all nubox nubox/test setup-functional.cfg
			;;
		websql)
			run_all websql websql/test setup-functional.cfg
			;;
		whitelist)
			run_all whitelist whitelist/test setup-functional.cfg
			;;
		servipag)
			run_all servipag servipag/test setup-functional.cfg
			;;
		telesales)
			run_all telesales telesales/test setup-functional.cfg
			;;
		tealium)
			run_all tealium tagging/tealium/test setup-functional.cfg
			;;
		*)
			$NOSETESTS $*
	esac
fi

# if we are running jenkins and a test fail, we retry them
if [ -n "$JENKINS" ]; then
	FAILED_TESTS_FILE=test_suites.txt
	FAILED_TESTS_OLD_FILE=test_suites.old
	echo -n "" > $FAILED_TESTS_OLD_FILE
	grep -lE "errors=\"[1-9]" *.xml > $FAILED_TESTS_FILE
	grep -lE "failures=\"[1-9]" *.xml >> $FAILED_TESTS_FILE
	FAILED_TESTS=$(wc -l $FAILED_TESTS_FILE | awk '{ print $1 }')
	FAILED_TESTS_OLD=$(wc -l $FAILED_TESTS_OLD_FILE | awk '{ print $1}')
	if [ $FAILED_TESTS -gt 0 ]; then
		while [ "$FAILED_TESTS" -ne "$FAILED_TESTS_OLD" ]; do
			printf "\nretrying tests:\n"
			cat $FAILED_TESTS_FILE
			for test_file in $(cat $FAILED_TESTS_FILE | sort -u )
			do
				printf "\nRetrying:\n"
				make -C .. rb db-load-accounts rebuild-index rebuild-asearch
				SUITE=$(echo "$test_file" | sed -e 's/functional\.//g' | sed -e 's/\.xml//g' | sed -e 's/\./\//g' )
				rm -f $test_file
				$NOSETESTS -c setup-functional.cfg "$SUITE.py" --with-xunit --xunit-file=$test_file > .nose.log 2>&1
				cat .nose.log
			done
			rm -f "$FAILED_TESTS_OLD_FILE"
			mv "$FAILED_TESTS_FILE" "$FAILED_TESTS_OLD_FILE"
			grep -lE "errors=\"[1-9]" *.xml > $FAILED_TESTS_FILE
			grep -lE "failures=\"[1-9]" *.xml >> $FAILED_TESTS_FILE
			FAILED_TESTS=$(wc -l $FAILED_TESTS_FILE | awk '{ print $1 }')
			FAILED_TESTS_OLD=$(wc -l $FAILED_TESTS_OLD_FILE | awk '{ print $1}')
		done
	fi
fi
