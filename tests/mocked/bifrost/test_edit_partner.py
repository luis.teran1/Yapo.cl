# -*- coding: latin-1 -*-
import yapo
import copy
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelPartnerEdit, ControlPanelPartnerAdd
import time


class TestEditPartner(yapo.SeleniumTest):

    """ List partner tests """

    snaps = ['accounts']
    user = 'dany'
    passwd = 'dany'
    override_browser = 'CHROME_LEGACY'

    partner_list = [
        [],
        ['pemasys_trov', 'Fuenzal', 'Editar\u2003 Desactivar'],
        ['pemasys_trovit',
         'pemasys_trovit',
         'Editar\u2003 Desactivar'],
        [],
        ['pemasys_tro', 'pemasys tro', 'Editar\u2003 Activar'],
    ]

    partner_list_edit_name = [[], [
        'pemasys_trov', 'Pemasys Trov', 'Editar\u2003 Desactivar'], [
        'pemasys_trovit', 'pemasys_trovit', 'Editar\u2003 Desactivar'], [], [
        'pemasys_tro', 'pemasys tro', 'Editar\u2003 Activar'], ]

    partner_name_edit = {'name': 'Pemasys Trov'}

    partner_connection_data_edit = {
        'protocol': 'ftp',
        'connData': "{user: pemasys_trovit, domain: 0.0.0.0, password: 123123}"
    }

    partner_connection_data_validate = {
        'protocol': 'ftp',
        'connData': "{\"user\": \"pemasys_trovit\", \"domain\": \"0.0.0.0\", \"password\": \"123123\"}"
    }

    partner_img_data_edit = {
        'imgUser': 'user',
        'imgPass': 'pass',
        'imgDomain': 'domain',
        'pathImg': './'}

    partner_converter_data_edit = {
        'downloadProtocol': 'ftp',
        'downloadUser': 'pemasys_trovit',
        'downloadPassword': '123123123',
        'downloadDomain': 'pemasystrovit.cl',
        'downloadSeparator': ',',
        'downloadEncoding': 'UTF-8',
        'downloadFilePath': 'pemasys.csv',
        'uploadProtocol': 'ftp',
        'uploadUser': 'pemasys_trovit',
        'uploadPassword': '123123123',
        'uploadDomain': 'pemasystrovit.cl',
        'uploadFilePath': 'pemasys.xml'
    }

    def _cp_login_go(self, wd, user, passwd, option):
        """ do cp login a go to option"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login(user, passwd)
        control_panel.go_to_option_in_menu(option)
        control_panel.wait.until_loaded()

    def _insert_basic_info(
            self,
            wd,
            arguments,
            submit_post=False,
            add_params=None):
        cp = ControlPanelPartnerAdd(wd)

        if add_params is not None:
            cp.fill_form(**add_params)

        cp.fill_form(**arguments)

        if submit_post:
            cp.submit_page()

        return cp

    def _validate_inputs_content(
            self,
            arguments,
            control_panel,
            valid_empty=False):
        text = ""
        for key, value in arguments.items():
            value_in_page = getattr(control_panel, key).get_attribute('value')
            if not valid_empty:
                text = value
                self.assertEqual(value_in_page, text)

    def _validate_list_content(self, partner_list, control_panel):
        partners = control_panel.get_partner_data()
        self.assertEqual(partner_list, partners)

    def _validate_edit(
            self,
            wd,
            partner_name,
            partner_list,
            edit_arguments,
            validate_arguments,
            edit_params=None):
        self._cp_login_go(wd, self.user, self.passwd, "Editar Partner")
        cp = ControlPanelPartnerEdit(wd)
        # validate the partner list
        self._validate_list_content(partner_list, cp)
        cp.get_table_action(partner_name, "edit")
        # apply the changes
        cpa = self._insert_basic_info(
            wd,
            edit_arguments,
            submit_post=True,
            add_params=edit_params)
        self.assertEqual(cp.PARTNER_UPDATE_OK, cp.statusMsg.text.strip())
        self._validate_list_content(self.partner_list, cp)
        cp.get_table_action(partner_name, "edit")
        # if needed, enable the checks associated with the edited data
        if edit_params is not None:
            cpa = self._insert_basic_info(
                wd,
                {},
                submit_post=False,
                add_params=edit_params)
        # validate the edited data
        self._validate_inputs_content(
            validate_arguments,
            cpa,
            valid_empty=False
        )

    @mocked
    @tags('carga_masiva')
    def test_edit_partner_name(self, wd):
        """ edit a partners name """
        self._cp_login_go(wd, self.user, self.passwd, "Editar Partner")
        cp = ControlPanelPartnerEdit(wd)
        self._validate_list_content(self.partner_list, cp)
        cp.get_table_action("pemasys_trov", "edit")
        self._insert_basic_info(wd, self.partner_name_edit, submit_post=True)
        self.assertEqual(cp.PARTNER_UPDATE_OK, cp.statusMsg.text.strip())
        self._validate_list_content(self.partner_list_edit_name, cp)

    @mocked
    @tags('carga_masiva')
    def test_edit_partner_connection_data(self, wd):
        """ edit a partners connection data """
        self._validate_edit(
            wd,
            "pemasys_trovit",
            self.partner_list,
            self.partner_connection_data_edit,
            self.partner_connection_data_validate
        )

    @mocked
    @tags('carga_masiva')
    def test_edit_partner_img_data(self, wd):
        """ try to edit a partners img data """
        self._validate_edit(
            wd,
            "pemasys_trovit",
            self.partner_list,
            self.partner_img_data_edit,
            self.partner_img_data_edit,
            edit_params={'imgData': True}
        )

    @mocked
    @tags('carga_masiva')
    def test_edit_partner_converter(self, wd):
        """ try to edit a partners converter data """
        self._validate_edit(
            wd,
            "pemasys_trovit",
            self.partner_list,
            self.partner_converter_data_edit,
            self.partner_converter_data_edit,
            edit_params={'converter': True}
        )
