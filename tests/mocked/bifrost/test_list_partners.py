# -*- coding: latin-1 -*-
import yapo
import copy
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelPartnerEdit


class TestListPartner(yapo.SeleniumTest):

    """ List partner tests """

    snaps = ['accounts']
    user = 'dany'
    passwd = 'dany'
    override_browser = 'CHROME_LEGACY'

    partner_list_empty = [
        [],
        []
    ]

    partner_list = [
        [],
        ['pemasys_trov', 'Fuenzal', 'Editar\u2003 Desactivar'],
        ['pemasys_trovit',
         'pemasys_trovit',
         'Editar\u2003 Desactivar'],
        [],
        ['pemasys_tro', 'pemasys tro', 'Editar\u2003 Activar'],
    ]

    def _cp_login_go(self, wd, user, passwd, option):
        """ do cp login a go to option"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login(user, passwd)
        control_panel.go_to_option_in_menu(option)
        control_panel.wait.until_loaded()

    def _validate_list_content(self, partner_list, control_panel):
        partners = control_panel.get_partner_data()
        self.assertEqual(partner_list, partners)

    @mocked
    @tags('carga_masiva')
    def test_bifrost_unavailable(self, wd):
        """ Try to list the partners when Bifrost is unavailable """
        self._cp_login_go(wd, self.user, self.passwd, "Editar Partner")
        cp = ControlPanelPartnerEdit(wd)
        self.assertEqual(cp.BIFROST_NOT_AVAILABLE, cp.statusMsg.text.strip())
        self._validate_list_content(self.partner_list_empty, cp)

    @mocked
    @tags('carga_masiva')
    def test_list_partner(self, wd):
        """ Try to list the partners """
        self._cp_login_go(wd, self.user, self.passwd, "Editar Partner")
        cp = ControlPanelPartnerEdit(wd)
        self._validate_list_content(self.partner_list, cp)
