# -*- coding: latin-1 -*-
import yapo
import copy
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelPartnerAdd

class TestAddPartner(yapo.SeleniumTest):

    """ Add partner tests """

    snaps = ['accounts']
    user = 'dany'
    passwd = 'dany'
    override_browser = 'CHROME_LEGACY'

    data = {
        'name': "PartnerTest",
        'fileName': "yapo_partner.xls",
        'insDelay': "10",
        'mailTo': "partner@yapo.cl",
        'keyName': "partner",
        'market': "1000",
        'pathImg': "./"
    }

    converter_data = {
        'downloadUser': 'partner',
        'downloadPassword': '123123',
        'downloadDomain': 'lisa',
        'downloadSeparator': ',',
        'downloadFilePath': 'habiter.csv',
        'uploadUser': 'partner',
        'uploadPassword': '123123',
        'uploadDomain': 'lisa',
        'uploadFilePath': 'habiter_test.xml'
    }

    advanced_data = {
        'pathFile': "./",
        'protocol': "ftp",
        'connData': "{user: telconf, domain: 0.0.0.0, password: 123123}",
        'delimiter': "|",
        'rules': "{}",
        'iRules': "{}",
    }

    img_data = {
        'imgUser': "user",
        'imgPass': "pass",
        'imgDomain': "domain",
    }

    runtime_data = {
        'runTime': "12:00",
    }

    def _cp_login_go(self, wd, user, passwd, option):
        """ do cp login a go to option"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login(user, passwd)
        control_panel.go_to_option_in_menu(option)
        control_panel.wait.until_loaded()

    def _insert_basic_info(
            self,
            wd,
            arguments,
            submit_post=False,
            add_params=None):
        cp = ControlPanelPartnerAdd(wd)

        if add_params is not None:
            cp.fill_form(**add_params)

        cp.fill_form(**arguments)

        if submit_post:
            cp.submit_page()

        return cp

    def _validate_inputs_content(
            self,
            arguments,
            control_panel,
            valid_empty=False):
        text = ""

        for key, value in arguments.items():
            value_in_page = getattr(control_panel, key).get_attribute('value')

            if not valid_empty:
                text = value

            if not valid_empty or key not in ('protocol', 'market', 'delimiter', 'connData'):
                self.assertEqual(value_in_page, text)

    @mocked
    @tags('carga_masiva')
    def test_normal_add_partner(self, wd):
        """ Add a partner with required params and then optional params"""

        self._cp_login_go(wd, self.user, self.passwd, "Agregar Partner")

        form_data = dict(self.data, ** self.img_data)
        cp = self._insert_basic_info(wd, form_data, submit_post=True)

        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())
        self._validate_inputs_content(form_data, cp, valid_empty=True)

        form_data['keyName'] = "partner2"
        form_data = dict(form_data, ** self.runtime_data)
        cp = self._insert_basic_info(wd, form_data, submit_post=True)

        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())
        self._validate_inputs_content(form_data, cp, valid_empty=True)

    @mocked
    @tags('carga_masiva')
    def test_normal_add_advanced_partner(self, wd):
        """ Add a advanced partner """

        self._cp_login_go(
            wd,
            self.user,
            self.passwd,
            "Agregar Partner Avanzado")

        enableRulesData = {'rulesCheck': True}
        form_data = dict(self.data, ** self.advanced_data)
        cp = self._insert_basic_info(
            wd,
            form_data,
            submit_post=True,
            add_params=enableRulesData)

        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())

        self._validate_inputs_content(form_data, cp, valid_empty=True)

    @mocked
    @tags('carga_masiva')
    def test_add_duplicated_partner(self, wd):
        """ Add a duplicated partner """

        form_data = dict(self.data, ** self.img_data)
        self._cp_login_go(wd, self.user, self.passwd, "Agregar Partner")
        cp = self._insert_basic_info(wd, form_data, submit_post=True)
        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())

        cp_second = self._insert_basic_info(wd, form_data, submit_post=True)
        self.assertEqual(
            cp_second.PARTNER_ALREADY_EXISTS,
            cp_second.statusMsg.text.strip())

        self._validate_inputs_content(form_data, cp, valid_empty=False)

    @mocked
    @tags('carga_masiva')
    def test_bifrost_unavailable(self, wd):
        """ Send a post transaction to Bifrost which is unavailable """

        form_data = dict(self.data, ** self.img_data)
        self._cp_login_go(wd, self.user, self.passwd, "Agregar Partner")
        cp = self._insert_basic_info(wd, form_data, submit_post=True)

        self.assertEqual(cp.BIFROST_NOT_AVAILABLE, cp.statusMsg.text.strip())

        self._validate_inputs_content(form_data, cp, valid_empty=False)

    @mocked
    @tags('carga_masiva')
    def test_add_advanced_partner_with_img_data(self, wd):
        """ Send a advanced partner with image connection data information """

        enableImgDataRules = {'imgData': True, 'rulesCheck': True}

        form_data = dict(self.data, ** self.advanced_data)
        form_data = dict(form_data, ** self.img_data)
        self._cp_login_go(
            wd,
            self.user,
            self.passwd,
            "Agregar Partner Avanzado")
        cp = self._insert_basic_info(
            wd,
            form_data,
            submit_post=True,
            add_params=enableImgDataRules)

        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())

        self._validate_inputs_content(form_data, cp, valid_empty=True)

    @mocked
    @tags('carga_masiva')
    def test_add_partner_with_converter(self, wd):
        """ Send a partner with converter data information """

        enableConverter = {'converter': True}
        form_data = dict(self.data, ** self.img_data)
        form_data = dict(form_data, ** self.converter_data)

        self._cp_login_go(wd, self.user, self.passwd, "Agregar Partner")
        cp = self._insert_basic_info(
            wd,
            form_data,
            submit_post=True,
            add_params=enableConverter)

        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())
        self._validate_inputs_content(form_data, cp, valid_empty=True)

    @mocked
    @tags('carga_masiva')
    def test_add_advanced_partner_with_run_time(self, wd):
        """ Send a advanced partner with runTime data information """
        enableRulesData = {'rulesCheck': True}
        form_data = dict(self.data, ** self.advanced_data)
        form_data = dict(form_data, ** self.runtime_data)
        self._cp_login_go(
            wd,
            self.user,
            self.passwd,
            "Agregar Partner Avanzado")
        cp = self._insert_basic_info(
            wd,
            form_data,
            submit_post=True,
            add_params=enableRulesData)
        self.assertEqual(cp.PARTNER_CREATED_OK, cp.statusMsg.text.strip())

        self._validate_inputs_content(form_data, cp, valid_empty=True)
