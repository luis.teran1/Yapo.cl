# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags, mocked, retries_function_exception
from yapo.pages.control_panel import ControlPanel, YapesosReport
from yapo.pages.control_panel import YapesosAssign, YapesosHistory
from yapo.pages.control_panel import YapesosEdit
from yapo import utils
from yapo.pages.credits import CreditsForm
from yapo.utils import Products
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.generic import SentEmail
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.simulator import Paysim
from yapo.steps import pay_products_logged
from yapo.pages.mails import CreditsMail
import datetime
import os
import unittest


class TestCPReport(yapo.SeleniumTest):

    snaps = ['accounts', 'diff_yapesos']
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _check_report(self, wd, expected, date_from, date_to):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu('Reporte Financiero')

        yr = YapesosReport(wd)
        yr.fill_report(date_from, date_to)
        if expected:
            report = yr.get_report()
            self.assertListEqual(report, expected)
        else:
            self.assertFalse(yr.is_element_present('report_table'))

    @mocked
    @tags('yapesos', 'control_panel')
    def test_report_ok(self, wd):
        """ the report is showing correctly """
        expected = [
            [],
            ['many@ads.cl', '6', 'Boleta', '1', '', '2017-01-04 17:39', '2.521', '479',
                '3.000', 'Pagado', '2', '', '', '', '', '3.000', '2017-01-04 00:00', '2018-01-04 00:00'],
            ['many@ads.cl', '6', 'Boleta', '2', '', '2017-01-04 17:39', '2.521', '479',
                '3.000', 'Pagado', '3', '', '', '', '', '3.000', '2017-01-04 00:00', '2018-01-04 00:00'],
            ['many@ads.cl', '6', 'Comprobante', '168', '1', '2017-01-04 17:39', '5.042',
                '958', '6.000', 'Pagado', '4', 'Emitido', '168', '2017-01-04 00:00',
                '3.000', '0', '2017-01-04 00:00', '2018-01-04 00:00'],
            ['many@ads.cl', '6', 'Comprobante', '168', '2', '2017-01-04 17:39', '5.042',
                '958', '6.000', 'Pagado', '4', 'Emitido', '168',
                '2017-01-04 00:00', '3.000', '0', '2017-01-04 00:00', '2018-01-04 00:00'],
            ['many@ads.cl', '6', 'Boleta', '3', '', '2017-01-04 17:39', '16.050',
                '3.050', '19.100', 'Pagado', '5', '', '', '', '', '19.100',
                '2017-01-04 00:00', '2018-01-04 00:00'],
            ['many@ads.cl', '6', 'Boleta', '4', '', '2017-01-04 17:40', '10.374',
                '1.971', '12.345', 'Pagado', '8', '', '', '', '', '12.345',
                '2017-01-02 00:00', '2017-01-03 00:00'],
            ['many@ads.cl', '6', 'Boleta', '4', '', '2017-01-04 17:40', '10.374',
                '1.971', '12.345', 'Pagado', '8', 'Emitido', 'CADUCAR',
                '2017-01-02 00:00', '12.345', '', '2017-01-02 00:00', '2017-01-03 00:00'],
            ['many@ads.cl', '6', 'Venta directa', '11111111111b', '',
                '2017-01-04 00:00', '10.374', '1.971', '12.345', 'Pagado', '',
                '', '', '', '', '12.345', '2017-01-04 00:00', '2017-01-03 00:00'],
            ['many@ads.cl', '6', 'Venta directa', '11111111111a', '',
                '2017-01-04 00:00', '10.374', '1.971', '12.345', 'Pagado',
                '', '', '', '', '', '12.345', '2017-01-04 00:00', '2017-01-03 00:00'],
            ['many@ads.cl', '6', 'Venta directa', '11111111111b', '',
                '2017-01-02 00:00', '1.034', '196', '1.230', 'Pagado',
                '', 'Emitido', 'CADUCAR', '2017-01-02 00:00', '1.230',
                '', '2017-01-02 00:00', '2017-01-03 00:00']
        ]
        self._check_report(wd, expected, '2017-01-01', '2017-01-05')

    @mocked
    @tags('yapesos', 'control_panel')
    def test_report_different_dates(self, wd):
        """ the report is showing less data based on dates"""
        expected = [
            [],
            ['many@ads.cl', '6', 'Boleta', '1', '', '2017-01-04 17:39', '2.521', '479',
                '3.000', 'Pagado', '2', '', '', '', '', '3.000',
                '2017-01-04 17:39','2018-01-04 23:27'],
            ['many@ads.cl', '6', 'Boleta', '2', '', '2017-01-04 17:39', '2.521', '479',
                '3.000', 'Pagado', '3', '', '', '', '', '3.000',
                '2017-01-04 17:39', '2018-01-04 23:28'],
            ['many@ads.cl', '6', 'Comprobante', '168', '1', '2017-01-04 17:39', '5.042',
                '958', '6.000', 'Pagado', '4', 'Emitido', '168', '2017-01-04 17:39',
                '3.000', '0', '2017-01-04 17:39', '2018-01-04 23:27'],
            ['many@ads.cl', '6', 'Comprobante', '168', '2', '2017-01-04 17:39', '5.042',
                '958', '6.000', 'Pagado', '4', 'Emitido', '168', '2017-01-04 17:39',
                '3.000', '0', '2017-01-04 17:39', '2018-01-04 23:28'],
            ['many@ads.cl', '6', 'Boleta', '3', '', '2017-01-04 17:39', '16.050',
                '3.050', '19.100', 'Pagado', '5', '', '', '', '', '19.100',
                '2017-01-04 17:40', '2018-01-04 23:28']
        ]
        self._check_report(wd, expected, '2017-01-04', '2017-01-05')

    @mocked
    @tags('yapesos', 'control_panel')
    def test_report_empty(self, wd):
        """ the report empty on old dates"""
        expected = None
        self._check_report(wd, expected, '2015-01-04', '2015-01-05')


class TestCPAssign(yapo.SeleniumTest):

    snaps = ['accounts']
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_yapesos_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login(self.user, self.password)

        cp = YapesosAssign(wd)
        return cp.go()

    def _cp_check_email(self, wd, email, expected, is_error=True):
        cp = YapesosAssign(wd)
        cp.fill('email', email)
        cp.search_button.click()
        cp.wait.until_loaded()
        if is_error:
            self.assertEquals(cp.err_email.text, expected)
        else:
            self.assertIn(expected, cp.user_name.text)

    def _cp_check_assign(self, wd, input, expected):
        cp = YapesosAssign(wd)

        for key, value in input.items():
            cp.fill(key, value)

        cp.send_button.click()
        wd.switch_to_alert().accept()
        cp.wait.until_loaded()

        for key, value in expected.items():
            if value is None:
                self.assertNotVisible(cp, key)
            else:
                self.assertEquals(getattr(cp, key).text, value)

    def test_search_for_user(self, wd):
        """ search email input validations """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, '', 'Escribe un e-mail v�lido')
        self._cp_check_email(wd, 'asdasdad', 'La cuenta no existe')
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)

    def test_assign_errors(self, wd):
        """ input boxes validations """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        input = {
            'service_order': '',
            'yapesos': '',
            'bonus': ''
        }
        expected = {
            'err_service_order': ("Orden de servicio contiene caracteres no"
                                  " v�lidos, utiliza solo n�meros"),
            'err_yapesos': 'El monto ingresado de Yapesos no es v�lido',
            'err_bonus': 'El bono ingresado no es v�lido',
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': 'a',
            'yapesos': 'a',
            'bonus': 'a'
        }
        # this uses the same expected error as above
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '',
            'bonus': ''
        }
        expected = {
            'err_service_order': None,
            'err_yapesos': 'El monto ingresado de Yapesos no es v�lido',
            'err_bonus': 'El bono ingresado no es v�lido',
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '1',
            'bonus': ''
        }
        expected = {
            'err_service_order': None,
            'err_yapesos': None,
            'err_bonus': 'El bono ingresado no es v�lido',
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '-1',
            'bonus': '2'
        }
        expected = {
            'err_service_order': None,
            'err_yapesos': 'El valor de Yapesos es demasiado bajo',
            'err_bonus': None,
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '1000',
            'bonus': '10000001'
        }
        expected = {
            'err_service_order': None,
            'err_yapesos': None,
            'err_bonus':
            'El valor del bono no puede ser mayor a $ 10.000.000',
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '10000001',
            'bonus': '0'
        }
        expected = {
            'err_service_order': None,
            'err_yapesos': 'El valor de Yapesos es demasiado alto',
            'err_bonus': None,
        }
        self._cp_check_assign(wd, input, expected)

        input = {
            'service_order': '1',
            'yapesos': '0',
            'bonus': '0'
        }
        expected = {
            'err_service_order': None,
            'err_total': 'El total de Yapesos a asignar debe ser mayor a 0',
            'err_bonus': None,
        }
        self._cp_check_assign(wd, input, expected)


    @mocked
    @tags('yapesos', 'control_panel')
    def test_assign(self, wd):
        """ assign of yapesos from cp successfully calls the api """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        input = {
            'service_order': '1',
            'yapesos': '1000',
            'bonus': '10'
        }
        expected = {
            'success': 'Yapesos saldo actual: 10.002'
        }
        self._cp_check_assign(wd, input, expected)

    @mocked
    @tags('yapesos', 'control_panel')
    def test_assign_future(self, wd):
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'many@ads.cl', 'Many', False)
        input = {
            'service_order': '1',
            'yapesos': '20000',
            'bonus': '1',
            'installments': '4'
        }
        expected = {
            'success': 'Yapesos saldo actual: 5.000'
        }
        self._cp_check_assign(wd, input, expected)

    @mocked
    @tags('yapesos', 'control_panel')
    def test_assign_api_error(self, wd):
        """ this checks that an error msg is show
             when the api did not respond a http 200 ok """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        input = {
            'service_order': '1',
            'yapesos': '1000',
            'bonus': '0'
        }
        expected = {
            'err_api': 'ERROR al conectar con la API de Yapesos'
        }
        self._cp_check_assign(wd, input, expected)


class TestCPHistory(yapo.SeleniumTest):

    snaps = ['accounts']
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_yapesos_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login(self.user, self.password)

        cp = YapesosHistory(wd)
        return cp.go()

    def _cp_check_email(self, wd, email, expected, is_error=True):
        cp = YapesosHistory(wd)
        cp.fill('email', email)
        cp.search_button.click()
        cp.wait.until_loaded()
        if is_error:
            self.assertEquals(cp.err_email.text, expected)
        else:
            self.assertIn(expected, cp.user_name.text)

    def _check_history(self, wd, expected, date_from, date_to):
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        cp = YapesosHistory(wd)
        cp.fill('history_from', date_from)
        cp.fill('history_to', date_to)
        cp.history_submit.click()

        if expected:
            history = cp.get_history()
            self.assertListEqual(history, expected)
        else:
            self.assertFalse(cp.is_element_present('history_table'))

    def _check_download_history(self, wd):
        now = datetime.datetime.now()
        date = now.strftime('%Y%m%d')
        filepath = os.path.join(
            yapo.conf.TOPDIR, 'tests', 'tmp', 'report-{}.csv'.format(date))
        if os.path.exists(filepath):
            os.remove(filepath)

        cp = YapesosHistory(wd)
        cp.download_link.click()

        yapo.utils.wait_for_file(filepath, 10)
        md5sum = yapo.utils.get_file_md5sum(filepath)
        self.assertTrue(os.path.exists(filepath))

    def test_search_for_user(self, wd):
        """ search email input validations """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, '', 'Escribe un e-mail v�lido')
        self._cp_check_email(wd, 'asdasdad', 'La cuenta no existe')
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)

    @mocked
    @tags('yapesos', 'control_panel')
    def test_history_ok(self, wd):
        """ the user history is showing correctly """
        expected = [
            [],
            ['111111', '2017-01-17 16:16', '+1.111', '1.111',
             '2017-01-17 16:16','2018-01-17 22:05', 'add', 'bonus'],
            ['111111', '2017-01-17 16:16', '+11.111', '11.111',
             '2017-01-17 16:16', '2018-01-17 22:05', 'add', 'admin']
        ]
        self._check_history(wd, expected, '2017-01-01', '2017-01-05')
        self._check_download_history(wd)


class TestCPEdit(yapo.SeleniumTest):

    snaps = ['accounts']
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_yapesos_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login(self.user, self.password)

        cp = YapesosEdit(wd)
        return cp.go()

    def _cp_check_email(self, wd, email, expected, is_error=True):
        cp = YapesosEdit(wd)
        cp.fill('email', email)
        cp.search_button.click()
        cp.wait.until_loaded()
        if is_error:
            self.assertEquals(cp.err_email.text, expected)
        else:
            self.assertIn(expected, cp.user_name.text)

    def _check_list(self, wd, expected, date_from, date_to):
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        cp = YapesosEdit(wd)
        cp.fill('history_from', date_from)
        cp.fill('history_to', date_to)
        cp.edit_submit.click()

        if expected:
            list_yapesos = cp.get_list()
            print(list_yapesos)
            self.assertListEqual(list_yapesos, expected)
        else:
            self.assertFalse(cp.is_element_present('history_table'))

    def test_search_for_user(self, wd):
        """ search email input validations """
        self._cp_yapesos_go(wd)
        self._cp_check_email(wd, '', 'Escribe un e-mail v�lido')
        self._cp_check_email(wd, 'asdasdad', 'La cuenta no existe')
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)

    @mocked
    @tags('yapesos', 'control_panel')
    def test_list_ok(self, wd):
        expected = [
            [],
            ['123456', '2017-01-13 10:58', '500', '500', '2017-01-13 10:58',
             '2018-01-13 16:47', 'bonus', 'Dany (dany@tetsuo.schibsted.cl)', 'Editar'],
            [''],
            ['123456', '2017-01-13 10:58', '4.500', '0', '2017-01-13 10:58',
             '2018-01-13 16:47', 'admin', 'Erik (erik@blocket.se)', 'Editar'],
            [''],
            ['123456', '2017-01-16 11:53', '500', '500', '2017-01-16 11:53',
             '2018-01-16 17:42', 'bonus', 'Zakay (zakay@blocket.se)', 'Editar'],
            [''],
            ['123456', '2017-01-16 11:53', '4.500', '0', '2017-01-16 11:53',
             '2018-01-16 17:42', 'admin', 'Thomas (test@schibstediberica.es)', 'Editar'],
            [''],
            ['123123', '2017-01-17 21:47', '500', '0', '2017-01-17 21:47',
             '2018-01-18 03:36', 'bonus', 'Kjell (kjell@blocket.se)', 'Editar'],
            [''],
            ['123123', '2017-01-17 21:47', '5.500', '0', '2017-01-17 21:47',
             '2018-01-18 03:36', 'admin', 'M.A.M.A. (blocket1@blocket.se)', 'Editar'],
            [''],
            ['123123', '2017-01-17 21:49', '100', '0', '2017-01-17 21:49',
             '2018-01-18 03:38', 'bonus', 'M.A.M.A. bender 02', 'Editar'],
            [''],
            ['123123', '2017-01-17 21:49', '1.000', '0', '2017-01-17 21:49',
             '2018-01-18 03:38', 'admin', 'Blocket (blocket@yapo.cl)', 'Editar'],
            ['']
        ]

        self._check_list(wd, expected, '2017-01-01', '2017-01-30')

    @mocked
    @tags('yapesos', 'control_panel')
    def test_edit_errors(self, wd):
        yapesos = self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        cp = YapesosEdit(wd)
        cp.edit_submit.click()

        yapesos.send_edit_form(164, '', '')
        yapesos.wait.until_loaded()
        self.assertTrue(yapesos.err_yapesos.is_displayed())
        self.assertTrue(yapesos.err_action.is_displayed())
        self.assertEquals(
            yapesos.err_action.text,
            'Selecciona la acci�n agregar o quitar')
        self.assertEquals(
            yapesos.err_yapesos.text,
            'El monto ingresado de Yapesos no es v�lido')

        yapesos.send_edit_form(164, 'remove', '100')
        yapesos.wait.until_loaded()
        self.assertTrue(yapesos.err_yapesos.is_displayed())
        self.assertEquals(
            yapesos.err_yapesos.text,
            'El monto ingresado supera lo disponible')

        yapesos.send_edit_form(164, 'add', '10000001')
        yapesos.wait.until_loaded()
        self.assertEquals(
            yapesos.err_yapesos.text,
            'El valor de Yapesos es demasiado alto')

        yapesos.send_edit_form(164, 'add', 'asdasda')
        yapesos.wait.until_loaded()
        self.assertEquals(
            yapesos.err_yapesos.text,
            'El monto ingresado de Yapesos no es v�lido')

    @mocked
    @tags('yapesos', 'control_panel')
    def test_edit_ok(self, wd):
        yapesos = self._cp_yapesos_go(wd)
        self._cp_check_email(wd, 'prepaid5@blocket.se', 'BLOCKET', False)
        cp = YapesosEdit(wd)
        cp.edit_submit.click()

        yapesos.send_edit_form(164, 'add', '300')
        yapesos.wait.until_loaded()

        expected = [
            [],
            ['123456', '2017-01-13 10:58', '800', '500', '2017-01-13 10:58',
             '2018-01-13 16:47', 'bonus', 'Dany (dany@tetsuo.schibsted.cl)', 'Editar'],
            [''],
            ['123456', '2017-01-13 10:58', '4.500', '0', '2017-01-13 10:58',
             '2018-01-13 16:47', 'admin', 'Erik (erik@blocket.se)', 'Editar'],
            [''],
        ]

        list_yapesos = cp.get_list()
        print(list_yapesos)
        self.assertListEqual(list_yapesos, expected)
