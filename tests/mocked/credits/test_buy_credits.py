# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags, mocked, retries_function_exception
from yapo import utils
from yapo.pages.credits import CreditsForm
from yapo.utils import Products
from yapo.pages import desktop
from yapo.pages import mobile
from yapo.pages.generic import SentEmail
from yapo.steps.accounts import login_and_go_to_dashboard
from yapo.pages.simulator import Paysim
from yapo.steps import pay_products_logged
from yapo.pages.mails import CreditsMail
import unittest

skip_message = 'Skipped until Mock is installed and functional on jenkins.'

class CreditsDesktop(yapo.SeleniumTest):

    override_browser = 'CHROME_LEGACY'
    snaps = ['accounts']
    email = 'prepaid5@blocket.se'
    password = '123123123'
    congrats = 'Tu compra se realiz� exitosamente'
    is_desktop = True
    platform = desktop

    bconfs = {
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0',
    }

    LIST_IDS = [
            { 6000666: [Products.LABEL, Products.GALLERY] },
            { 3456789: [Products.LABEL, Products.GALLERY] },
            { 6000667: [Products.LABEL, Products.GALLERY] },
            { 8000073: [Products.LABEL] },
            { 8000065: [Products.LABEL] }
    ]

    def _add_credits_to_cart(self, wd, credits):
        credits_form = CreditsForm(wd)
        credits_form.go()
        credits_form.buy_credits(credits)
        summary = self.platform.SummaryPayment(wd)
        summary.choose_bill()
        return summary

    def _buy_credits(self, wd, credits, payment_method, do_login=True):
        """ General method to buy credits"""
        if do_login:
            login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)

        summary = self._add_credits_to_cart(wd, credits)
        function_name = 'pay_with_{0}'.format(payment_method)
        getattr(summary, function_name)()

        paysim = mobile.Paysim(wd)
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)

    def _checkCreditosMail(self, wd, should_view_credits_info = True, use_credits = False):
        email = SentEmail(wd)
        email.go()
        credits_mail = CreditsMail(wd)
        if should_view_credits_info:
            self.assertIn(credits_mail.YOU_HAVE_NOW, email.body.text)
            self.assertIn(credits_mail.YOU_HAD, email.body.text)
            if use_credits:
                self.assertIn(credits_mail.YOU_USED, email.body.text)
            else:
                self.assertIn(credits_mail.YOU_BOUGHT, email.body.text)
        else:
            self.assertNotIn(credits_mail.YOU_HAVE_NOW, email.body.text)

    def _checkPdf(self, url):
        from  urllib import request
        pdf_name = "pdf.pdf"
        request.urlretrieve (url, pdf_name)
        import PyPDF2
        pdf_file = open(pdf_name, 'rb')
        read_pdf = PyPDF2.PdfFileReader(pdf_file)
        number_of_pages = read_pdf.getNumPages()
        page = read_pdf.getPage(0)
        page_content = page.extractText()
        self.assertIn('ESTE DOCUMENTO NO TIENE VALIDEZ FISCAL',page_content)
        import os
        os.unlink(pdf_name)


    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_buy_credits_and_stuff_check_mail(self, wd):
        """ Buy credits and other products and check mail """
        login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)
        dma = self.platform.DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(self.LIST_IDS[0])
        self._buy_credits(wd, 5000, 'webpay', False)
        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEquals(payment_verify.text_congratulations.text, self.congrats)
        self._checkCreditosMail(wd)

    @mocked
    @tags('credits', 'cart', 'desktop')
    def test_check_cart_validation_credits(self, wd):
        """ Cart validation when APi return error """
        login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)
        summary = self._add_credits_to_cart(wd, 5000)
        summary.link_to_pay_products.click()
        summary.wait.until_visible('modal_cart_button')
        self.assertEquals(summary.modal_cart_body_title.text, '�Tu carro de compras ha sido actualizado!')
        summary.modal_cart_button.click()
        summary.wait.until_not_visible('modal_cart_button')
        summary.wait.until_visible('empty_cart')

    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_buy_credits_check_mail(self, wd):
        """ Buy credits and check mail """
        self._buy_credits(wd, 5000, 'webpay')
        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEquals(payment_verify.text_congratulations.text, self.congrats)
        self._checkCreditosMail(wd)

    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_buy_credits_with_webpay(self, wd):
        """ Buy 5000 Credits From DashBoard with webpay"""
        self._buy_credits(wd, 5000, 'webpay')
        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEquals(payment_verify.text_congratulations.text, self.congrats)

    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_pay_with_credits(self, wd):
        """ Testing pay with credits multiproducts"""
        login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)
        dma = self.platform.DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(self.LIST_IDS[1])
        dma.checkout_button.click()

        summary = self.platform.SummaryPayment(wd)
        summary.pay_with_credits()

        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEquals(payment_verify.text_congratulations.text, self.congrats)

        voucher_link = payment_verify.voucher_link.get_attribute('href')
        self._checkCreditosMail(wd, use_credits = True)
        self._checkPdf(voucher_link)

    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_buy_stuff_wo_credits_check_mail(self, wd):
        """ Pay products with webpay and the credits block is not visible on mail """
        login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)
        dma = self.platform.DashboardMyAds(wd)
        dma.go_dashboard()
        dma.select_products_by_ids(self.LIST_IDS[2])
        dma.checkout_button.click()

        self._pay_in_summary(wd, self.platform)

        paysim = Paysim(wd)
        paysim.pay_accept()

        payment_verify = self.platform.PaymentVerify(wd)
        self.assertEquals(payment_verify.text_congratulations.text, self.congrats)
        self._checkCreditosMail(wd, should_view_credits_info = False)

    @mocked
    @tags('credits', 'payment', 'desktop')
    def test_check_get_balance(self, wd):
        """ Checks that the credits balance is correctly obtained"""
        login_and_go_to_dashboard(wd, self.email, self.password, from_desktop=self.is_desktop)
        credits_form = CreditsForm(wd)
        credits_form.go()
        self.assertEquals("500.000", credits_form.status.text)
        self.assertEquals("100.000", credits_form.to_expire.text)
        self.assertEquals("Ma�ana", credits_form.date_to_expire.text)

    @retries_function_exception(num_retries=3)
    def _pay_in_summary(self, wd, platform):
        summary = self.platform.SummaryPayment(wd)
        summary.choose_bill()
        summary.pay_with_webpay()
        import time
        time.sleep(1)

class CreditsMobile(CreditsDesktop):

    user_agent = utils.UserAgents.NEXUS_5
    is_desktop = False
    platform = mobile
    override_browser = 'CHROME_LEGACY'

    bconfs = {
        '*.*.combos_discount.enabled': '0',
        '*.*.combos_discount_mobile.enabled': '0',
    }

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_buy_credits_and_stuff_check_mail(self, wd):
        """ Buy credits and other products on mobile and check mail """
        super(CreditsMobile, self).test_buy_credits_and_stuff_check_mail()

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_buy_credits_check_mail(self, wd):
        """ Buy credits on mobile and check mail """
        super(CreditsMobile, self).test_buy_credits_check_mail()

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_buy_credits_with_webpay(self, wd):
        """ Buy 5000 Credits From DashBoard with WebPay"""
        super(CreditsMobile, self).test_buy_credits_with_webpay()

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_pay_with_credits(self, wd):
        """ Testing pay with credits multiproducts on mobile"""
        super(CreditsMobile, self).test_pay_with_credits()

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_buy_stuff_wo_credits_check_mail(self, wd):
        """ Pay products with webpay on mobile and the credits block is not visible on mail """
        super(CreditsMobile, self).test_buy_stuff_wo_credits_check_mail()

    @mocked
    @tags('credits', 'payment', 'mobile')
    def test_check_get_balance(self, wd):
        """ Checks that the credits balance is correctly obtained on mobile"""
        super(CreditsMobile, self).test_check_get_balance()

    @mocked
    @tags('credits', 'cart', 'mobile')
    def test_check_cart_validation_credits(self, wd):
        """ Cart validation when APi return error """
        super(CreditsMobile, self).test_check_cart_validation_credits()
