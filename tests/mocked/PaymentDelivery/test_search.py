# -*- coding: latin-1 -*-
from yapo import utils, conf
from yapo.decorators import tags, mocked, retries_function_exception
from yapo.pages.control_panel import ControlPanel, PaymentDeliverySearch
from yapo.pages import desktop
import time
import subprocess
import unittest
import yapo

#its a different function because this way os package wouldnt be a global lib in tests
def reset_mocks():
    import os
    subprocess.call("(cd " + conf.TOPDIR + " && make mock-stop)", shell=True)
    subprocess.call("(cd " + conf.TOPDIR + " && make mock-start)", shell=True)

class CommonStuff(yapo.SeleniumTest):
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_go(self, wd, option):
        cp = ControlPanel(wd)
        cp.go_and_login(self.user, self.password, check_menu=False)
        subs = PaymentDeliverySearch(wd)
        subs.go(wd, option)
        return subs

    def start(self, wd, option, key, value):
        subs = self._cp_go(wd, option)
        subs.wait.until_present("search_text")
        subs.wait.until_loaded()
        subs.fill(key, value)
        subs.submit()
        return subs


class TestSearch(CommonStuff):
    #reset_mocks()
    snaps = ['accounts']
    override_browser = 'CHROME_LEGACY'
    
    @mocked
    def test_search_err_email(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "email", "12ab")
        subs.wait.until_visible("err_user_email")
        subs.wait.until_loaded()
        self.assertEquals(subs.INVALID_EMAIL, subs.err_user_email.text)

    @mocked
    def test_search_err_phone(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "phone", "12ab")
        subs.wait.until_visible("err_user_phone")
        subs.wait.until_loaded()
        self.assertEquals(subs.INVALID_PHONE, subs.err_user_phone.text)

    @mocked
    def test_search_err_list_id(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "list_id", "12ab")
        subs.wait.until_visible("err_list_id")
        subs.wait.until_loaded()
        self.assertEquals(subs.INVALID_LIST_ID, subs.err_list_id.text)

    @mocked
    def test_search_err_escrow_id(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "external_id", "12ab")
        subs.wait.until_visible("err_escrow_id")
        subs.wait.until_loaded()
        self.assertEquals(subs.INVALID_ESCROW_ID, subs.err_escrow_id.text)

    @mocked
    def test_search_no_data_email(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "email", "ads@many.com")
        subs.wait.until_visible("err_no_data")
        subs.wait.until_loaded()
        self.assertEquals(subs.NO_DATA, subs.err_no_data.text)

    @mocked
    def test_search_no_data_phone(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "phone", "987654321")
        subs.wait.until_visible("err_no_data")
        subs.wait.until_loaded()
        self.assertEquals(subs.NO_DATA, subs.err_no_data.text)

    @mocked
    def test_search_no_data_list_id(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "list_id", "672353412")
        subs.wait.until_visible("err_no_data")
        subs.wait.until_loaded()
        self.assertEquals(subs.NO_DATA, subs.err_no_data.text)

    @mocked
    def test_search_no_data_escrow_id(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "external_id", "912635")
        subs.wait.until_visible("err_no_data")
        subs.wait.until_loaded()
        self.assertEquals(subs.NO_DATA, subs.err_no_data.text)

    @mocked
    def test_search_withData(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "external_id", "4152")
        subs.wait.until_visible("pd_results")
        subs.wait.until_loaded()
        expected = [[],
           ['Rechazado por el vendedor\n20-03-2019 15:30:21',
                  '',
                  '3989\n\nVendedor\nComprador\nListID\nTelefono Vendedor\nTelefono Comprador',
                  'Audifonos Apple\n\n: testseller@mailinator.com\n: testbuyer@mailinator.com\n: 4965393\n:\n: 938736412',
                  '',
                  'Generar Link Vendedor\nGenerar Link Comprador'],
           ['Pagado\n20-03-2019 15:30:21',
                  '',
                  '3526\n\nVendedor\nComprador\nListID\nTelefono Vendedor\nTelefono Comprador',
                  'Audifonos XBOX one\n\n: testseller@mailinator.com\n: testbuyer@mailinator.com\n: 4961342\n:\n: 938736412',
                  '',
                  'Generar Link Vendedor\nGenerar Link Comprador'],
           ['Entregado al comprador\n20-03-2019 15:30:21',
                  '',
                  '4152\n\nVendedor\nComprador\nListID\nTelefono Vendedor\nTelefono Comprador', 
                  'Audifonos genericos\n\n: testseller@mailinator.com\n: testbuyer@mailinator.com\n: 4943216\n: 983241242\n: 938736412',
                  '',
                  'Generar Link Vendedor\nGenerar Link Comprador']]
        self.assertListEqual(expected, subs.get_table_data("pd_results"))   
   
    @mocked
    def test_search_withData_reject_disabled(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "external_id", "4152")
        subs.wait.until_visible("pd_results")
        subs.wait.until_loaded()
        expected = [[],
           ['Rechazado por el vendedor\n20-03-2019 15:30:21',
                  '',
                  '3989\n\nVendedor\nComprador\nListID\nTelefono Vendedor\nTelefono Comprador',
                  'Audifonos Apple\n\n: testseller@mailinator.com\n: testbuyer@mailinator.com\n: 4965393\n:\n: 938736412',
                  '',
                  'Generar Link Vendedor\nGenerar Link Comprador']]
        self.assertListEqual(expected, subs.get_table_data("pd_results"))
        self.assertEquals(False, subs.reject_sale.is_enabled())

    @mocked
    def test_search_withData_reject_sale(self, wd):
        subs = self.start(wd, "Buscar Ordenes", "email", "ads@many.com")
        subs.wait.until_visible("pd_results")
        subs.wait.until_loaded()
        self.assertEquals(True, subs.reject_sale.is_enabled())
        subs.reject()
        subs.wait.until_visible("reject_success")
        subs.wait.until_loaded()
        expectedSuccess = "Se ha rechazado la orden 'Audifonos XBOX one'"
        expected = [[],
           ['Rechazado por el administrador\n20-03-2019 15:30:21',
                '',
                '3526\n\nVendedor\nComprador\nListID\nTelefono Vendedor\nTelefono Comprador',
                'Audifonos XBOX one\n\n: testseller@mailinator.com\n: testbuyer@mailinator.com\n: 4961342\n:\n: 938736412',
                '',
                'Generar Link Vendedor\nGenerar Link Comprador']]
        self.assertEquals(expectedSuccess, subs.reject_success.text)
        self.assertListEqual(expected, subs.get_table_data("pd_results"))
