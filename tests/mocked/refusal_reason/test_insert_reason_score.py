# coding: latin-1
import yapo, time
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelNewRefusalReasons

class NewRefusalReason(yapo.SeleniumTest):
    snaps = ['accounts']
    override_browser = 'CHROME_LEGACY'

    @mocked
    @tags('refusal_reason')
    def test_insert_reason_ok(self, wd):
        """ Test insert reason data ok"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='37', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.OK_MSG)

    @mocked
    @tags('refusal_reason')
    def test_insert_reason_without_reasonname(self, wd):
        """ Test insert reason error without reason name"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='', id='37', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.REASON_NAME_ERROR)
    
    @mocked
    @tags('refusal_reason')
    def test_insert_reason_alfanumeric_id(self, wd):
        """ Test insert reason error with text id"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='abc', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.ID_ERROR_MSG)

    @mocked
    @tags('refusal_reason')
    def test_insert_reason_without_id(self, wd):
        """ Test insert reason error without id"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.CHAR_ERROR_MSG)
    
    @mocked
    @tags('refusal_reason')
    def test_insert_reason_whitout_text(self, wd):
        """ Test insert reason error without text"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='40', text='')
        self.assertEqual(submit_message, new_reason_page.TEXT_ERROR_MSG)

    @mocked
    @tags('refusal_reason')
    def test_insert_reason_error_negative_id(self, wd):
        """ Test insert reason error with negative id"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='-40', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.CHAR_ERROR_MSG)

    @mocked
    @tags('refusal_reason')
    def test_insert_reason_ms_error(self, wd):
        """ Test insert reason MS conection error, no mocked"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.add_refusal_reason(reason='Fraudulento', id='37', text='Rechazado por Fraude.')
        self.assertEqual(submit_message, new_reason_page.MS_ERROR_MSG)

    def _login_and_go(self, wd):
        """ Init new Control Panel page"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login("dany", "dany")

        new_reason_page = ControlPanelNewRefusalReasons(wd)
        new_reason_page.go()
        return new_reason_page
