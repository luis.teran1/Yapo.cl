# coding: latin-1
import yapo, time
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelDeleteRefusalReasons

class DeleteRefusalReason(yapo.SeleniumTest):
    snaps = ['accounts']
    override_browser = 'CHROME_LEGACY'

    @mocked
    @tags('refusal_reason')
    def test_delete_reason_ok(self, wd):
        """ Test delete reason ok"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.delete_refusal_reason(reason='Spam')
        self.assertEqual(submit_message, new_reason_page.OK_MSG)

    @mocked
    @tags('refusal_reason')
    def test_delete_reason_ms_error(self, wd):
        """ Test delete reason MS conection error, no mocked"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.delete_refusal_reason(reason='Spam')
        self.assertEqual(submit_message, new_reason_page.MS_ERROR_MSG)

    def _login_and_go(self, wd):
        """ Init new Control Panel page"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login("dany", "dany")

        new_reason_page = ControlPanelDeleteRefusalReasons(wd)
        new_reason_page.go()
        return new_reason_page
