# coding: latin-1
import yapo, time
from yapo.decorators import tags, mocked
from yapo.pages.control_panel import ControlPanel, ControlPanelRefusalReasonScore

class RefusalReasonScore(yapo.SeleniumTest):
    snaps = ['accounts']
    override_browser = 'CHROME_LEGACY'

    @mocked
    @tags('refusal_reason')
    def test_edit_reason_score_ok(self, wd):
        """ Test edit refusal reason with ok data """
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.edit_refusal_reason_score(id='302', score='100')
        self.assertEqual(submit_message, new_reason_page.OK_MSG)
    
    @mocked
    @tags('refusal_reason')
    def test_edit_reason_score_alfanumeric(self, wd):
        """ Test edit refusal reason with alfanumeric score"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.edit_refusal_reason_score(id='302', score='abc')
        self.assertEqual(submit_message, new_reason_page.NUMERIC_FIELD_MSG)

    @mocked
    @tags('refusal_reason')
    def test_edit_reason_score_void(self, wd):
        """ Test edit refusal reason without score"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.edit_refusal_reason_score(id='302', score='')
        self.assertEqual(submit_message, new_reason_page.NUMERIC_FIELD_MSG)

    @mocked
    @tags('refusal_reason')
    def test_edit_reason_score_negative(self, wd):
        """ Test edit refusal reason with negative score"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.edit_refusal_reason_score(id='302', score='-10')
        self.assertEqual(submit_message, new_reason_page.NUMERIC_FIELD_MSG)

    @mocked
    @tags('refusal_reason')
    def test_edit_reason_score_double(self, wd):
        """ Test edit refusal reason with double score"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.edit_refusal_reason_score(id='302', score='0.10')
        self.assertEqual(submit_message, new_reason_page.NUMERIC_FIELD_MSG)

    @mocked
    @tags('refusal_reason')
    def test_list_reasons_scores_ms_error(self, wd):
        """ Test to list reasons scores MS conection error, error message"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.list_refusal_reasons_scores()
        self.assertEqual(submit_message, new_reason_page.MS_ERROR_MSG)

    @mocked
    @tags('refusal_reason')
    def test_list_reasons_scores_ms_error_no_mocked(self, wd):
        """ Test to list reasons scores MS conection error, no mocked"""
        new_reason_page = self._login_and_go(wd)
        submit_message = new_reason_page.list_refusal_reasons_scores()
        self.assertEqual(submit_message, new_reason_page.MS_ERROR_MSG)

    def _login_and_go(self, wd):
        """ Init new Control Panel page"""
        control_panel = ControlPanel(wd)
        control_panel.go()
        control_panel.login("dany", "dany")

        new_reason_page = ControlPanelRefusalReasonScore(wd)
        new_reason_page.go()
        return new_reason_page
