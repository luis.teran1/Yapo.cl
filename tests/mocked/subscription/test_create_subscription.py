# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags, mocked, retries_function_exception
from yapo.pages.control_panel import ControlPanel, SubscriptionsCreate
from yapo.pages import desktop
import datetime
import os
import unittest


class CommonStuff(yapo.SeleniumTest):

    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany', check_menu=False)
        subs = SubscriptionsCreate(wd)
        subs.go()
        return subs

    def start(self, wd, email="cuentaproconpack@yapo.cl"):
        wd = self._cp_go(wd)
        wd.fill("email", email)
        wd.search.click()
        wd.wait.until_present("account_info")
        return wd

    def flow_in_form(self, wd, type="bill"):
        wd = self.start(wd)
        wd.ordered_fill_form([
            ("product", "31020"),
            ("doc_type", type),
            ("admin_email", "user@enterprise.com"),
            ("user_name", "validasuser"),
        ])
        wd.admin_email.click()
        self.assertEqual(None, wd.create.get_attribute("disabled"))
        wd.create.click()
        wd.accept_modal()
        return wd


class TestFlow(CommonStuff):

    snaps = ['accounts', 'diff_pack_cars1']
    override_browser = 'CHROME_LEGACY'

    @mocked
    def test_create_stuff(self, wd):
        subs = self.flow_in_form(wd)
        subs.wait.until_loaded()
        self.assertEqual(subs.success_msg, subs.success.get_attribute('innerHTML'))

    @mocked
    def test_missing_params(self, wd):
        subs = self.flow_in_form(wd)
        subs.wait.until_loaded()
        self.assertEqual(subs.missing_parameters_msg,
                         subs.error.get_attribute('innerHTML').replace('\n', ''))

    def test_invalid_admin_email(self, wd):
        subs = self.start(wd)
        subs.fill("admin_email", "user@")
        subs.user_name.click()
        self.assertEqual(subs.invalid_admin_email_msg,
                         subs.invalid_admin_email.get_attribute('innerHTML'))

    def test_invalid_invoice(self, wd):
        subs = self.start(wd)
        subs.fill("doc_type", "invoice")
        self.assertIn(subs.invoice_missing_params_msg,
                      wd.switch_to_alert().text)

    def test_no_pro_user(self, wd):
        subs = self.start(wd, "many@ads.cl")
        self.assertEqual(subs.no_pro_user_msg,
                         subs.no_pro_user.get_attribute('innerHTML')[1:-1]),

    def test_edit_account_tab(self, wd):
        subs = self.start(wd)
        subs.edit_account.click()
        subs.switch_to_edit()
        self.assertEqual("cuentaproconpack@yapo.cl",
                         subs.edit_email_tab.get_attribute('innerHTML'))


class TestInvoice(CommonStuff):
    snaps = ['accounts', 'diff_pack_cars1',
             'diff_pack_cars1_plus_full_account']
    override_browser = 'CHROME_LEGACY'

    @mocked
    def test_success_invoice(self, wd):
        subs = self.flow_in_form(wd, "invoice")
        self.assertEqual(subs.success_msg, subs.success.get_attribute('innerHTML'))


class TestConfigErrors(CommonStuff):
    snaps = ['accounts', 'diff_pack_cars1']
    bconfs = {'*.*.common.base_url.ms_payment_schedule_api': 'http://404website.com:8080'}
    _bconf = ''
    override_browser = 'CHROME_LEGACY'

    @mocked
    def test_could_not_connect(self, wd):
        subs = self.flow_in_form(wd)
        self.assertEqual(subs.cannot_connect_msg,
                         subs.error.get_attribute('innerHTML').replace('\n', ''))


class TestCreateSubscriptionCar(CommonStuff):

    snaps = ['accounts', 'diff_pack_cars1']
    category_pro = '2020'
    pack_type = 'car'
    prod_packs_text = 'Pack Auto'
    prod_len = 16
    override_browser = 'CHROME_LEGACY'

    @tags('subscription', 'control_panel')
    def test_check_form_stuff(self, wd):
        subs = self._cp_go(wd)
        subs.fill("email", "cuentaproconpack@yapo.cl")
        subs.search.click()
        # acc_info = subs.get_table_data("account_info")
        expected_acc_info = [
            ['Account Id', '10'],
            ['User Id', '62'],
            ['Account Status', 'active'],
            ['Creation Date', '2015-03-09 18:13:53.72158'],
            ['Name', 'cuentaproconpack'],
            ['Email', 'cuentaproconpack@yapo.cl'],
            ['Has Pack Ads', '1'],
            ['Is Company', 't'],
            ['Is Pro For', self.category_pro],
            ['Pack Slots', '9'],
            ['Pack Status', 'active'],
            ['Pack Type', self.pack_type],
            ['Rut', '17324400-2'],
            ['Region', 'Regi�n Metropolitana'],
            ['Phone', '967867876'],
        ]

        self.assertEqual(subs.get_table_data("account_info"), expected_acc_info)
        # Test options
        options = subs.get_all_options_text("product")
        self.assertEqual(self.prod_len, len(options))
        for opt in options:
            self.assertIn(self.prod_packs_text, opt)

        options = subs.get_all_options_text("doc_type")
        self.assertListEqual(['Boleta', 'Factura'], options)

        self.assertEqual("", subs.admin_email.get_attribute("value"))
        self.assertEqual("cuentaproconpack", subs.user_name.get_attribute("value"))

        self.assertEqual("true", subs.create.get_attribute("disabled"))


class TestCreateSubscriptionInmo(TestCreateSubscriptionCar):

    snaps = ['accounts', 'diff_pack_inmo1']
    category_pro = '1220'
    pack_type = 'real_estate'
    prod_packs_text = 'Pack Inmobiliaria'
    prod_len = 13
    override_browser = 'CHROME_LEGACY'
