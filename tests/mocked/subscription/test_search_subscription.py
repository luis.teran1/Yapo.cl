# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags, mocked, retries_function_exception
from yapo.pages.control_panel import ControlPanel, SubscriptionsSearch
from yapo.pages import desktop
import datetime
import os
import unittest


class CommonStuff(yapo.SeleniumTest):

    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany', check_menu=False)
        subs = SubscriptionsSearch(wd)
        subs.go()
        return subs

    def start(self, wd, email="cuentaproconpack@yapo.cl"):
        wd = self._cp_go(wd)
        wd.fill("email", email)
        wd.search.click()
        return wd


class TestFlowSearch(CommonStuff):

    snaps = ['accounts', 'diff_pack_cars1']
    override_browser = 'CHROME_LEGACY'

    def test_account_is_not_pro(self, wd):
        subs = self.start(wd, "many@ads.cl")
        subs.wait.until_present("result")
        subs.wait.until_loaded()
        self.assertEquals(subs.acc_is_not_pro, subs.result.get_attribute('innerHTML'))

    def test_account_nof_found(self, wd):
        subs = self.start(wd, "perico@palotes.cl")
        subs.wait.until_present("result")
        subs.wait.until_loaded()
        self.assertEquals(subs.acc_not_found, subs.result.get_attribute('innerHTML'))


    @mocked
    def test_search_stuff(self, wd):
        subs = self.start(wd)
        subs.wait.until_present("data")
        subs.wait.until_loaded()
        expected = [[],
           ['3',
                '31010\nPack Auto 10/mensual',
                '25.500',
                'active',
                '2018-06-29 19:42',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:42',
                'dany.jaque@schibsted.com',
                'Dar de baja'],
           ['4',
                '31040\nPack Auto 40/mensual',
                '90.450',
                'canceled',
                '2018-06-29 19:44',
                '2018-06-29 18:36',
                'paid',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                ''],
           ['5',
                '31050\nPack Auto 50/mensual',
                '107.000',
                'retry',
                '2018-06-29 19:44',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                'Dar de baja']]
        self.assertListEqual(expected, subs.get_table_data("data"))

    @mocked
    def test_reactivate_sub(self, wd):
        subs = self.start(wd)
        subs.wait.until_present("data")
        subs.wait.until_loaded()
        expected = [[],
           ['3',
                '31010\nPack Auto 10/mensual',
                '25.500',
                'active',
                '2018-06-29 19:42',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:42',
                'dany.jaque@schibsted.com',
                'Dar de baja'],
           ['4',
                '31040\nPack Auto 40/mensual',
                '90.450',
                'suspended',
                '2018-06-29 19:44',
                '2018-06-29 18:36',
                'pending',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                'Reactivar'],
           ['5',
                '31050\nPack Auto 50/mensual',
                '107.000',
                'retry',
                '2018-06-29 19:44',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                'Dar de baja']]
        self.assertListEqual(expected, subs.get_table_data("data"))


    @mocked
    def test_down_subscription(self, wd):
        subs = self.start(wd)
        subs.wait.until_present("data")
        subs.wait.until_loaded()
        subs.down_subs.click()
        subs.wait.until_present("data")
        subs.wait.until_loaded()
        expected = [[],
           ['3',
                '31010\nPack Auto 10/mensual',
                '25.500',
                'canceled',
                '2018-06-29 19:42',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:42',
                'dany.jaque@schibsted.com',
                ''],
           ['4',
                '31040\nPack Auto 40/mensual',
                '90.450',
                'canceled',
                '2018-06-29 19:44',
                '2018-06-29 18:36',
                'paid',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                ''],
           ['5',
                '31050\nPack Auto 50/mensual',
                '107.000',
                'retry',
                '2018-06-29 19:44',
                '____-__-__ __:__',
                '',
                '2018-06-29 19:44',
                'djaque@gmail.com',
                'Dar de baja']]
        self.assertListEqual(expected, subs.get_table_data("data"))
