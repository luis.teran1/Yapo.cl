# -*- coding: latin-1 -*-
from yapo import utils, conf
from yapo.decorators import tags, mocked
from yapo.pages import desktop
from yapo.pages import mobile
import subprocess
import yapo
import unittest

#its a different function because this way os package wouldnt be a global lib in tests
def reset_mocks():
    import os
    subprocess.call("(cd " + conf.TOPDIR + " && make mock-stop)", shell=True)
    subprocess.call("(cd " + conf.TOPDIR + " && make mock-start)", shell=True)

class AutofactMobile(yapo.SeleniumTest):
    """
    On these tests the mocks do the magic
    """
    reset_mocks()
    snaps = ['accounts', 'diff_pack_cars1']
    user_agent = utils.UserAgents.IOS
    account_pass = "123123123"
    account_user = 'cuentaproconpack@yapo.cl'
    congrats = "Tu compra se realiz� exitosamente"
    platform = mobile
    list_id = 8000093
    override_browser = 'CHROME_LEGACY'

    @mocked
    @tags('accounts', 'dashboard', 'products', 'autofact')
    @unittest.skip('Disabled because on Jenkins always fails')
    def test_autofact_full(self, wd):
        """ Autofact report as new product """
        yapo.utils.clean_mails()
        login = self.platform.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = self.platform.SummaryPayment(wd)
        summary.add_product(str(self.list_id), str(utils.Products.AUTOFACT))
        summary.choose_bill()
        summary.pay_with_webpay()
        paysim = self.platform.Paysim(wd)
        paysim.pay_accept()
        pay_verify = self.platform.PaymentVerify(wd)
        pay_verify.wait.until_loaded()
        self.assertEqual(pay_verify.text_congratulations.text, self.congrats)
        self.assertIsNotNone(yapo.utils.find_in_mails("Estamos generando el informe solicitado"))

    @mocked
    @tags('accounts', 'cart', 'autofact')
    def test_cart_validate_autofact(self, wd):
        """ Check Autofact report is removed from cart if api returns error """
        login = self.platform.Login(wd)
        login.login(self.account_user, self.account_pass)
        summary = self.platform.SummaryPayment(wd)
        summary.add_product(str(self.list_id), str(utils.Products.AUTOFACT))
        summary.choose_bill()
        summary.link_to_pay_products.click()
        summary.wait.until_visible('modal_cart_button')
        self.assertEquals(summary.modal_cart_body_title.text, '�Tu carro de compras ha sido actualizado!')
        summary.modal_cart_button.click()
        summary.wait.until_not_visible('modal_cart_button')
        summary.wait.until_visible('empty_cart')

class AutofactDesktop(AutofactMobile):
    user_agent = utils.UserAgents.FIREFOX_MAC
    platform = desktop
    override_browser = 'CHROME_LEGACY'
