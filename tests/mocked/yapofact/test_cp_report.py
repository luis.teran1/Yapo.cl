# -*- coding: latin-1 -*-
import yapo
from yapo.decorators import tags, mocked, retries_function_exception
from yapo.pages.control_panel import ControlPanel, YapofactAdminReport
from yapo.pages import desktop
import datetime
import os
import unittest

skip_msg = 'Disabled because on Jenkins always fails'

class TestCPReport(yapo.SeleniumTest):

    snaps = ['accounts']
    user = 'dany'
    password = 'dany'
    override_browser = 'CHROME_LEGACY'

    def _cp_go(self, wd):
        cp = ControlPanel(wd)
        cp.go_and_login('dany', 'dany')
        cp.go_to_option_in_menu('Buscar informes comprados')

    def _check_report(self, wd, expected, values):
        yr = YapofactAdminReport(wd)
        yr.clean_report()
        yr.fill_form(**values)
        yr.search.click()
        yr.wait.until_loaded()
        if expected:
            report = yr.get_report()
            self.assertListEqual(report, expected)
        else:
            self.assertFalse(yr.is_element_present('report_table'))

    def _cp_check_errors(self, wd, input, expected):
        cp = YapofactAdminReport(wd)

        for key, value in input.items():
            cp.fill(key, value)

        cp.search.click()
        cp.wait.until_loaded()

        for key, value in expected.items():
            if value is None:
                self.assertNotVisible(cp, key)
            else:
                self.assertEquals(getattr(cp, key).text, value)

    @mocked
    @tags('autofact', 'control_panel')
    @unittest.skip(skip_msg)
    def test_report_ok(self, wd):
        """ the report is showing correctly """
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05'
        }
        expected = [
            [],
            ['1a11111a-aaaa-1a11-a111-aa1a1a111111', 'Recibido', '2017-03-27 17:19',
                '2017-03-27 17:19', 'email@email.email', 'Test', 'Autos, camionetas y 4x4',
                'Ad1 con estilo achorizado (ID: 800018)', 'webpay', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        self._cp_go(wd)
        self._check_report(wd, expected, values)

    @mocked
    @tags('autofact', 'control_panel')
    @unittest.skip(skip_msg)
    def test_report_different_dates(self, wd):
        """ the report is showing less data based on dates """
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05'
        }
        expected = [
            [],
            ['1a11111a-aaaa-1a11-a111-aa1a1a111111', 'Recibido', '2017-03-27 17:19',
                '2017-03-27 17:19', 'email@email.email', 'Test', 'Autos, camionetas y 4x4',
                'Ad1 con estilo achorizado (ID: 800018)', 'webpay', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', ''],
            ['2b22222b-bbbb-2b22-b222-bb2b2b222222', 'Solicitado', '2017-03-27 17:13',
                '--', 'email2@email.email', 'Test User', 'Motos',
                'Ad2 sin estilo desachorizado (ID: 800019)', 'khipu', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        self._cp_go(wd)
        self._check_report(wd, expected, values)

    @mocked
    @tags('autofact', 'control_panel')
    @unittest.skip(skip_msg)
    def test_report_different_params(self, wd):
        """ the report is showing less data based on the optional search params """
        self._cp_go(wd)
        expected = [
            [],
            ['2b22222b-bbbb-2b22-b222-bb2b2b222222', 'Solicitado', '2017-03-27 17:13',
                '--', 'email2@email.email', 'Test User', 'Motos',
                'Ad2 sin estilo desachorizado (ID: 800019)', 'khipu', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05',
            'category': '2060'
        }
        self._check_report(wd, expected, values)

        expected = [
            [],
            ['1a11111a-aaaa-1a11-a111-aa1a1a111111', 'Recibido', '2017-03-27 17:19',
                '2017-03-27 17:19', 'email@email.email', 'Test', 'Autos, camionetas y 4x4',
                'Ad1 con estilo achorizado (ID: 800018)', 'webpay', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05',
            'email': 'email@email.email'
        }
        self._check_report(wd, expected, values)

        expected = [
            [],
            ['2b22222b-bbbb-2b22-b222-bb2b2b222222', 'Recibido', '2017-03-17 17:13',
                '2017-03-17 18:19', 'email2@email.email', 'Test User', 'Autos, camionetas y 4x4',
                'Ad6 sin estilo desachorizado (ID: 8000017)', 'khipu', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05',
            'ad_id': '8000017'
        }
        self._check_report(wd, expected, values)

        expected = [
            [],
            ['2b22222b-bbbb-2b22-b222-bb2b2b222222', 'Recibido', '2017-03-17 17:13',
                '2017-03-17 18:19', 'email2@email.email', 'lasagna', 'Autos, camionetas y 4x4',
                'Ad6 sin estilo desachorizado (ID: 8000017)', 'khipu', 'Ver'],
            ['', '', '', '', ''],
            ['', '', '', '']
        ]
        values = {
            'date_from':'2017-01-01',
            'date_to':'2017-01-05',
            'name': 'lasagna'
        }
        self._check_report(wd, expected, values)

    @mocked
    @tags('autofact', 'control_panel')
    def test_report_empty(self, wd):
        """ the report empty on old dates"""
        expected = None
        self._cp_go(wd)
        values = {
            'date_from':'2015-01-01',
            'date_to':'2015-01-05'
        }
        self._check_report(wd, expected, values)

    @mocked
    @tags('autofact', 'control_panel')
    def test_report_validations(self, wd):
        """ checks the validation errors """
        self._cp_go(wd)
        input = {
            'name': '/#jaja',
            'ad_id': 'abc'
        }
        expected = {
            'err_name': 'El nombre contiene caracteres no v�lidos',
            'err_ad_id': 'El ID ingresado contiene caracteres no validos',
        }
        self._cp_check_errors(wd, input, expected)
