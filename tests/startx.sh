#!/usr/bin/env bash
export DISPLAY=:$(id -u) 
nohup Xvnc -ac -SecurityTypes None $DISPLAY & > /dev/null
nohup openbox --replace & > /dev/null
