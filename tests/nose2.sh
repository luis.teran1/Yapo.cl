#!/usr/bin/env bash
#
# This script is the preferred way to run Narco-tests.
# If at all posible, avoid calling nosetests directly.
# Usage:
#	nose.sh: Run all tests
#	nose.sh <testfile>: Run an specific test file
#


# setup local environment
export TEMP=$(pwd)/tmp
export PYTHONPATH=$(pwd)
mkdir -p $TEMP

NOSETESTS=`which nosetests-3.4 2> /dev/null`

if [ "a${NOSETESTS}" == "a" ]; then
       NOSETESTS=`which nosetests-3.3 2> /dev/null`
fi

export PYTHONPATH=$PWD
./startx.sh 2> /dev/null

# no parameters? run ALL the tests \o
if [ "a$1" = "a" ]
then
	$NOSETESTS -c setup-functional.cfg --with-xunit --xunit-file=functional/nosetests-f.xml

	i=1
	for test in $(find -name "ptest_*.py")
	do
		echo ${test#./functional/}
		$NOSETESTS -c setup-parallel.cfg ${test#./functional/} --with-xunitmp --xunitmp-file=nosetests-${i}.xml
		i=$(($i+1))
	done
elif [ "$1" = "mocked" ]
then
	if [ "a$2" = "a" ]
	then
        $NOSETESTS -c setup-mocked.cfg --with-xunit --xunit-file=mocked/nosetests-f.xml
	else
        $NOSETESTS -c setup-mocked.cfg --with-xunit --xunit-file=mocked/nosetests-f.xml $2
	fi
else
# otherwise act just as a wrapper
	$NOSETESTS $*
fi

