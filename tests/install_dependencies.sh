#!/usr/bin/env bash

sudo /opt/scmcoord_contrib/bin/easy_install-3.3 pip==9.0.1

sudo /opt/scmcoord_contrib/bin/pip3.3 install -r ./requirements.txt
