"""Contains all the configurations form ninja/invars.sh plus custom vars
"""
import os, subprocess, sys

def invars():
    """
    Returns all the invars variables as a dict
    """
    cur_dir = os.path.abspath(os.path.dirname(__file__))
    work_dir = os.path.abspath(os.path.join(cur_dir, '../..'))
    out = subprocess.check_output('buildflavor=regress sh ninja/invars.sh /dev/null', cwd=work_dir, shell=True)
    var_dict = dict(l.decode('latin1').split('=', 1) for l in out.splitlines())
    return var_dict

this_module = sys.modules[__name__]
for key, value in invars().items():
    setattr(this_module, key, value)

DESKTOP_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_HTTPD_WORKER_PORT)
DESKTOP_URL_04 = "https://{0}:{1}".format(REGRESS_HOSTNAME, str(int(REGRESS_HTTPD_WORKER_PORT)-4))
MOBILE_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_MOBILE_PORT)
PHP_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_HTTPD_PORT)
SSL_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_HTTPS_PORT)
CONTROLPANEL_URL = "https://{0}:{1}/controlpanel".format(REGRESS_HOSTNAME, REGRESS_HTTPS_PORT)
BCONFD_URL = "http://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_BCONFD_JSON_PORT)
MOD_IMAGE_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_MOD_IMAGE_PORT)
NEXTGEN_API_URL = "https://{0}:{1}".format(REGRESS_HOSTNAME, REGRESS_NEXTGEN_PORT)
MOCK_CONTROL_URL = "http://{0}:{1}/config".format(REGRESS_HOSTNAME, MOCK_CONTROL_PORT)
ENVIRONMENT = "regress"

REGRESS_FINALDIR = os.path.join(TOPDIR, 'regress', 'final')
REGRESS_FINAL = os.path.join(TOPDIR, 'regress_final')
RESOURCES_DIR = os.path.join(TOPDIR, 'tests', 'resources')

SSL_CERT_PATH = os.path.join(REGRESS_FINAL, 'conf', 'server.crt')

MAIL_LOG_PATHS = (REGRESS_FINAL + "/logs/mail.txt",
                  REGRESS_FINAL + "/logs/mail.txt.old", )

NINJA_BUILD_REGRESS = os.path.join(TOPDIR, 'ninja_build', 'regress')
