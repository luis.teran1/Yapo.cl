# -*- coding: latin-1 -*-
from yapo.pages import BasePage
from yapo.pages.desktop import DashboardMyAds as DashDesktop
from yapo.pages.mobile import DashboardMyAds as DashMobile
from yapo.pages.desktop import Login
from yapo.pages.mail import Mailinator, ConfirmAccount, AdPublished, AdPendingReview, MyAds, ProUser
from yapo import utils
from yapo.pages.generic import SentEmail
import sunbro, nose_selenium


class Email(BasePage):

    """
    Email manipulation tool
    """

    @staticmethod
    def create(wd):
        if utils.is_not_regress():
            return MailinatorMail(wd)
        else:
            return RegressMail(wd)


class MailinatorMail(Email):

    """
    Mailinator page objects to validate real emails
    """

    def go_to_inbox(self, mail):
        self._driver.get('http://www.mailinator.com/inbox.jsp?to={0}'.format(mail))
        self.wait.until_loaded()

    def check_published_ad_mail(self, test, mail, ad_subject):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if mail.text == 'Ya hemos publicado tu aviso "{0}" en Yapo.cl'.format(ad_subject):
                self.go_to_email(i)
                ap = AdPublished(self._driver)
                test.assertEqual(ad_subject, ap.ad_subject.text)
                test.assertIn('Tu aviso ha sido publicado', ap.full_mail.text)
                test.assertIn('Y ahora que publicaste tu aviso, �cu�ntale a tus amigos!', ap.full_mail.text)
                test.assertIn('Gracias por confiar en Yapo.cl para hacer un buen negocio!', ap.full_mail.text)
                break
        else:
                test.assertIn('Tu aviso ha sido publicado', mlt.mail.text)

    def check_published_edited_ad_mail(self, test, mail, ad_subject):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if mail.text == 'La edici�n de tu aviso "{0}" ha sido publicada en Yapo.cl'.format(ad_subject):
                self.go_to_email(i)
                ap = AdPublished(self._driver)
                test.assertEqual(ad_subject, ap.ad_subject.text)
                test.assertIn('La edici�n de tu aviso ha sido publicada', ap.full_mail.text)
                test.assertIn('Si no puedes ver correctamente los detalles de tu aviso, pega la siguiente direcci�n en tu navegador para poder editarlo o borrarlo:', ap.full_mail.text)
                test.assertIn('Cuando tu aviso ya no est� vigente, puedes borrarlo haciendo clic en editar en la p�gina del anuncio o bien haciendo clic en el �cono "borrar".', ap.full_mail.text)
                break
        else:
                test.assertIn('La edici�n de tu aviso "{0}" ha sido publicada en Yapo.cl'.format(ad_subject), mlt.mail.text)

    def check_pro_user_mail(self, test, mail, username):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(mlt.mail.is_displayed(), msg = 'Mailinator is down ):')
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if mail.text == 'Felicitaciones, para yapo.cl eres todo un pro':
                self.go_to_email(i)
                pro = ProUser(self._driver)
                test.assertIn('Hola {0}'.format(username), pro.full_mail.text)
                test.assertIn('�Felicitaciones, para nosotros eres todo un pro!', pro.full_mail.text)
                test.assertIn('Hemos notado que publicas frecuentemente varios avisos, desde ahora eres un profesional de Yapo.cl.', pro.full_mail.text)
                test.assertIn('Al tener m�s de 5 avisos publicados dentro de las categor�as de', pro.full_mail.text)
                test.assertIn('te consideramos como profesional', pro.full_mail.text)
                test.assertIn('Cada vez que publiques recuerda seleccionar "Profesional"', pro.full_mail.text)
                pro.dashboard_link.click()
                self._driver.switch_to_window(self._driver.window_handles[1])
                break
        else:
                test.assertIn('Felicitaciones, para yapo.cl eres todo un pro', mlt.mail.text)

    def check_my_ads_mail(self, test, wd, mail, passwd):
        Login(wd).login(mail, passwd)
        if 'Mobile' in type(test).__name__:
            dashboard = DashMobile(wd)
        else:
            dashboard = DashDesktop(wd)
        ads_qty = len(dashboard.active_ads)
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if 'Tus avisos activos en' in mail.text:
                self.go_to_email(i)
                myads = MyAds(self._driver)
                test.assertEqual(myads.title.text, 'Mis Avisos')
                test.assertEqual(len(myads.subjects), int(ads_qty))
                break
        else:
                test.assertIn('Mis Avisos', mlt.mail.text)

    def check_account_activation(self, test, mail):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if mail.text == '�Est�s a un paso de crear tu cuenta!':
                test.assertEqual(mail.text,'�Est�s a un paso de crear tu cuenta!')
                self.go_to_email(i)
                cam = ConfirmAccount(self._driver)
                cam.confirm_account[0].click()
                break
        else:
                test.assertIn('�Est�s a un paso de crear tu cuenta!', mlt.mail.text)

    def check_pending_review_ad_mail(self, test, mail, ad_subject):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if 'Tu aviso "{0}" en Yapo.cl est� siendo revisado'.format(ad_subject) == mail.text:
                test.assertEqual(mail.text,'Tu aviso "{0}" en Yapo.cl est� siendo revisado'.format(ad_subject))
                self.go_to_email(i)
                pr = AdPendingReview(self._driver)
                test.assertEqual('Aviso en revisi�n: {0}'.format(ad_subject), pr.title.text)
                test.assertIn('Para garantizar la mejor calidad de contenidos, nuestro equipo editorial revisa cada aviso insertado y lo valida de acuerdo con las reglas de Yapo.cl.', pr.full_mail.text)
                test.assertIn('Gracias por confiar en Yapo.cl para hacer un buen negocio!', pr.full_mail.text)
                break
        else:
                test.assertIn('Tu aviso "{0}" en Yapo.cl est� siendo revisado'.format(ad_subject), mlt.mail.text)

    def go_to_email(self,email_num):
        mlt = Mailinator(self._driver)
        mail_id = mlt.mail_links[email_num].get_attribute("onclick")[10:-2]
        self._driver.get('http://mailinator.com/rendermail.jsp?msgid={0}'.format(mail_id))


class FakeMail(Email):

    """
    FakeMail page objects to validate real emails
    """
    email = None

    def check_account_activation(self, test, mail):
        self.go_to_inbox(mail)
        mlt = FakeMl(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for i, mail in enumerate(mlt.mails):
            if mail.text == '�Est�s a un paso de crear tu cuenta!' or '_a_un_paso_de_crear_tu_cuenta!' in mail.text:
                self.go_to_email(i)
                cam = ConfirmAccount(self._driver)
                cam.confirm_account[0].click()
                break
        else:
                test.assertIn('�Est�s a un paso de crear tu cuenta!', mlt.mail.text)

    def go_to_inbox(self, mail):
        FakeMail.email = mail.split('@')
        self._driver.get('http://www.fakemailgenerator.com/inbox/{0}/{1}/'.format(FakeMail.email[1], FakeMail.email[0]))
        self.wait.until_loaded()

    def go_to_email(self, email_num):
        fkm  = FakeMl(self._driver)
        mail_id = fkm.mail_links[email_num].get_attribute("href").split('-')[1][:-1]
        self._driver.get('http://www.fakemailgenerator.com/email/{0}/{1}/message-{2}/'.format(FakeMail.email[1], FakeMail.email[0], mail_id))


class RegressMail(Email):

    def check_account_activation(self, test, mail):
        rmail = SentEmail(self._driver)
        cam = ConfirmAccount(self._driver)
        rmail.go()
        cam.confirm_account[0].click()

    def check_published_ad_mail(self, test, mail, ad_subject):
        self.go_to_inbox(mail.split("@")[0])
        mlt = Mailinator(self._driver)
        test.assertTrue(len(mlt.mails) > 0)
        for mail in mlt.mails:
            test.assertEqual(mail.text,'Ya hemos publicado tu aviso "{0}" en Yapo.cl'.format(ad_subject))

    def check_my_ads_mail(self, test, wd, mail, passwd):
        rmail = SentEmail(self._driver)
        rmail.go()
        myads = MyAds(self._driver)
        test.assertEqual(myads.title.text, 'Mis Avisos')
        test.assertEqual(len(myads.subjects), 500)

    def check_pro_user_mail(self, test, mail, username):
        rmail = SentEmail(self._driver)
        rmail.go()
        if 'eres todo un pro' not in rmail.body.text:
            rmail.go(old=True)
        test.assertIn('Hola {0}'.format(username), rmail.body.text)
        test.assertIn('�Felicitaciones, para nosotros eres todo un pro!', rmail.body.text)
        test.assertIn('Hemos notado que publicas frecuentemente varios avisos, desde ahora eres un profesional de Yapo.cl.', rmail.body.text)
        test.assertIn('De acuerdo a tus publicaciones dentro de las categor�as de', rmail.body.text)
        test.assertIn('te consideramos como profesional', rmail.body.text)
