from yapo.component import AttrFinder
from yapo.component import Component
from functools import total_ordering
import sunbro


@AttrFinder
@total_ordering
class Tab(Component):
    name = sunbro.FindByCSS('.tabList-itemLink')

    _dump_map = {
        'name': Component.text
    }

    def click(self):
        """ Clicks the link and fetches ads """
        self.name.click()

    def __eq__(self, other):
        if not isinstance(other, Tab):
            return NotImplemented
        return self.dumpa('name') == other.dumpa('name')

    def __lt__(self, other):
        if not isinstance(other, Tab):
            return NotImplemented
        return self.dumpa('name') < other.dumpa('name')
