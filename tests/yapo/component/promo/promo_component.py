from yapo.component import Component
from yapo.component import waiting


class PromoComponent(Component):

    @waiting('data-seq')
    def waited_click(self, webelement):
        webelement.click()
