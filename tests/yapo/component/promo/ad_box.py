from yapo.component import AttrFinder
from yapo.component import Component
import sunbro


@AttrFinder
class AdBox(Component):
    id = sunbro.FindByCSS('a')
    title = sunbro.FindByClass('daBox-title')
    location = sunbro.FindByClass('daBox-location')
    price = sunbro.FindByClass('daBox-price')
    action = sunbro.FindByClass('daBox-action')

    _dump_map = {
        'id': Component.attr('id'),
        'title': Component.text,
        'location': Component.text,
        'price': Component.text,
    }

    def click(self):
        """ Clicks the link and goes to the ad """
        self.action.click()
        self._driver.switch_to_window(self._driver.window_handles[-1])
