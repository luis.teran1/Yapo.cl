# -*- coding: latin-1 -*-
from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component.promo import PromoComponent
from yapo.component.promo import Tab
import sunbro


class BaseTabList(PromoComponent):

    @classmethod
    def instantiate(cls, driver, webelement):
        cls = EmptyTabList
        if webelement:
            cls = TabList
        return cls(driver, webelement)

    def __iter__(self):
        return iter(self.tabs)


class EmptyTabList(BaseTabList):

    tabs = []
    selected = None

    _dump_map = {
        'tabs': Component.id,
        'selected': Component.id,
    }


@AttrFinder
class TabList(BaseTabList):
    _tabs = sunbro.FindAllByCSS('.tabList-item')
    _selected = sunbro.FindByCSS('.tabList-item.__selected')

    _dump_map = {
        'tabs': Component.dumpcs,
        'selected': Component.dump,
    }

    @property
    def tabs(self):
        return [Tab(self._driver, tab) for tab in self._tabs]

    @property
    def selected(self):
        return Tab(self._driver, self._selected)

    def get_tab(self, name):
        for tab in self.tabs:
            t = tab.dumpa('name')
            if (name.lower() == t.lower()):
                return tab
        raise LookupError('Could not find tab [{}]'.format(name))

    def click(self, name):
        tab = self.get_tab(name)
        self.waited_click(tab)
