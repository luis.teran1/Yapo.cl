from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component import waiting
import sunbro


@AttrFinder
class Search(Component):
    icon = sunbro.FindByCSS('.search-image')
    query = sunbro.FindByCSS('.search-input')

    _dump_map = {
        'icon': Component.attr('src'),
        'query': Component.attr('value'),
    }

    @waiting('data-seq')
    def search(self, text, hit_enter=True):
        self.fill_form(query=text)

    def fill(self, name, value):
        if name == 'query':
            self.query.clear()
            if value:
                self.query.send_keys(value)
                self.query.send_keys(Keys.RETURN)
            else:
                ActionChains(self._driver).key_down(Keys.RETURN).key_up(Keys.RETURN).perform()
