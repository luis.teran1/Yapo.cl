# -*- coding: latin-1 -*-
from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component.promo import AdBox
import sunbro


class BaseAdList(Component):

    @classmethod
    def instantiate(cls, driver, webelement):
        cls = AdList
        if not webelement or '__no-results' in webelement.get_attribute('class'):
            cls = EmptyAdList
        return cls(driver, webelement)

    @property
    def ads(self):
        return []

    def __iter__(self):
        return iter(self.ads)


@AttrFinder
class EmptyAdList(BaseAdList):
    image = sunbro.FindByClass('daList-image')
    title = sunbro.FindByClass('daList-title')
    description = sunbro.FindByClass('daList-description')

    NO_RESULT_TITLE = "B�squeda sin resultados"
    NO_RESULT_DESCRIPTION = "No encontramos avisos con tus filtros de b�squeda. Por favor, int�ntalo nuevamente."

    _dump_map = {
        'image': Component.attr('src'),
        'title': Component.text,
        'description': Component.text
    }


@AttrFinder
class AdList(BaseAdList):
    _ads = sunbro.FindAllByClass('daBox')

    _dump_map = {
        'ads': Component.dumpcs,
    }

    @property
    def ads(self):
        return [AdBox(self._driver, ad) for ad in self._ads]

    def click(self, i=None, id=None, title=None):
        """ Clicks the ad chosen by given criteria """
        ad = self.get_ad(i, id, title)
        return ad.click()

    def get_ad(self, i=None, id=None, title=None):
        """ Returns matching ad or raises LookupError """
        if i is not None:
            return self.ads[i]
        if id:
            return self._get_ad_by('id', id)
        if title:
            return self._get_ad_by('title', title)
        raise LookupError('No search criteria for get_ad')

    def _get_ad_by(self, field, value):
        """ Searches for the first ad that matches given criteria """
        for ad in self.ads:
            v = ad.dumpa(field)
            if v == value:
                return ad
        raise LookupError('Could not find ad with [{} = {}]'.format(field, value))
