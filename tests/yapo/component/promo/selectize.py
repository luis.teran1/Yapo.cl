from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component.promo import PromoComponent
from yapo.component import waiting
import sunbro


@AttrFinder
class Selectize(PromoComponent):
    selected = sunbro.FindByCSS('.selectize-input .item')
    _input = sunbro.FindByCSS('.selectize-input')
    _options = sunbro.FindAllByCSS('.selectize-dropdown-content .option:not(.selected)')

    _dump_map = {
        'selected': Component.text,
        'options': Component.dumpls(Component.text),
    }

    @property
    def options(self):
        if self.is_open():
            return self._options
        return []

    def select(self, value=None, text=None):
        option = self.get_option(value=value, text=text)
        if option:
            self.waited_click(option)

    def select_numeric(self, choice=None):
        if choice is not None:
            choice = str(choice)
            if choice.isdigit():
                self.select(value=choice)
            else:
                self.select(text=choice)

    def is_open(self):
        classes = self._input.get_attribute('class')
        return 'dropdown-active' in classes

    def _open(self):
        if not self.is_open():
            self._input.click()
            self._wait.until(lambda _: self.is_open())

    def get_option(self, value=None, text=None):
        self._open()
        if value is not None:
            def pick(option):
                return option.get_attribute('data-value') == str(value)
            if pick(self.selected):
                print('selected value: '+self.selected.get_attribute('data-value'))
                return None
            return self._get_option_by('value', value, pick)
        if text is not None:
            def pick(option):
                return option.text == str(text)
            if pick(self.selected):
                print('selected text: '+self.selected.text)
                return None
            return self._get_option_by('text', text, pick)
        raise LookupError('No option pick criteria for select')

    def _get_option_by(self, field, value, pick_fn):
        opts = list(filter(pick_fn, self.options))
        count = len(opts)
        if count == 1:
            return opts[0]
        if count > 1:
            raise LookupError('Too many options matching [{} = {}] ({})'.format(
                              field, value, Component.dumpls(Component.text)(opts)))
        raise LookupError('Could not find option with [{} = {}]'.format(field, value))
