# -*- coding: latin-1 -*-
from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component.promo import PromoComponent
from yapo.component import waiting
from functools import partial
import nose_selenium
import sunbro


class BasePagination(PromoComponent):

    @classmethod
    def instantiate(cls, driver, webelement):
        cls = NoPagination
        if webelement:
            cls = Pagination
        return cls(driver, webelement)


class NoPagination(BasePagination):
    pass


@AttrFinder
class Pagination(BasePagination):

    _first = sunbro.FindByCSS('.pagination-item.__first')
    _prev = sunbro.FindByCSS('.pagination-item.__prev')
    current = sunbro.FindByCSS('.pagination-item.__current')
    pages = sunbro.FindAllByCSS('.pagination-item.__page')
    _next = sunbro.FindByCSS('.pagination-item.__next')
    _last = sunbro.FindByCSS('.pagination-item.__last')
    _controls = ['first', 'prev', 'next', 'last']

    _dump_map = {
        'pages': Component.dumpls(Component.text),
        'current': Component.text,
        'controls': Component.id,
    }

    @property
    def controls(self):
        cs = []
        self._driver.implicitly_wait(0)
        for c in self._controls:
            try:
                getattr(self, '_'+c)
            except NoSuchElementException:
                continue
            cs.append(c)
        self._driver.implicitly_wait(nose_selenium.TIMEOUT)
        return cs

    def click(self, what, guarever='x'):
        button = self._lookup(what)
        self.waited_click(button)

    def _lookup(self, what):
        click_map = {
            'first': self._first,
            'prev': self._prev,
            'next': self._next,
            'last': self._last,
        }
        if what in click_map:
            return click_map[what]
        if isinstance(what, int) or what.isdigit():
            i = str(what)
            for p in self.pages:
                if p.text == i:
                    return p
        raise LookupError('Could not find what you want [{}]'.format(what))
