"""
    package component:

    - AttrFinder: Class decorator for Component classes
    - Component: Base class for components
    - ComponentComposer: Class decorator for component aware Page classes

    Note: Only the symbols declared here will be visible outside of the module
"""

from .attrfinder import AttrFinder
from .form import Form, select
from .component import Component
from .composer import ComponentComposer
from .waiting import waiting
