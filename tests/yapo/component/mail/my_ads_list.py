# -*- coding: latin-1 -*-
from yapo.component import AttrFinder
from yapo.component import Component
from yapo.component.mail import MyAdsAd
import sunbro


@AttrFinder
class MyAdsList(Component):
    _ads = sunbro.FindAllByCSS('tr')

    _dump_map = {
        'ads': Component.dumpcs,
    }

    @property
    def ads(self):
        # Skip the header row
        return [MyAdsAd(self._driver, ad) for ad in self._ads[1:]]

    def click(self, subject=None):
        """ Clicks the ad chosen by given criteria """
        ad = self.get_ad(subject=subject)
        return ad.click()

    def edit(self, subject=None):
        """ Clicks the edit link on the ad chosen by given criteria """
        ad = self.get_ad(subject=subject)
        return ad.edit()

    def delete(self, subject=None):
        """ Clicks the delete link on the ad chosen by given criteria """
        ad = self.get_ad(subject=subject)
        return ad.delete()

    def get_ad(self, subject=None):
        """ Returns matching ad or raises LookupError """
        if subject:
            return self._get_ad_by('subject', subject)
        raise LookupError('No search criteria for get_ad')

    def _get_ad_by(self, field, value):
        """ Searches for the first ad that matches given criteria """
        for ad in self.ads:
            v = ad.dumpa(field)
            if v == value:
                return ad
        raise LookupError('Could not find ad with [{} = {}]'.format(field, value))

    def __iter__(self):
        return iter(self.ads)
