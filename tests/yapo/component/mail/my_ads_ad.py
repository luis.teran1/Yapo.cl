from yapo.component import AttrFinder
from yapo.component import Component
import sunbro


@AttrFinder
class MyAdsAd(Component):
    date = sunbro.FindByCSS('.date')
    image = sunbro.FindByCSS('.image a')
    subject = sunbro.FindByCSS('.subject')
    price = sunbro.FindByCSS('td:nth-child(4)')
    _edit = sunbro.FindByCSS('.edit')
    _delete = sunbro.FindByCSS('.delete')

    _dump_map = {
        'date': Component.text,
        'image': Component.attr('href'),
        'subject': Component.text,
        'price': Component.text,
    }

    def click(self):
        """ Clicks the link and goes to the ad """
        self.subject.click()

    def edit(self):
        self._edit.click()

    def delete(self):
        self._delete.click()
