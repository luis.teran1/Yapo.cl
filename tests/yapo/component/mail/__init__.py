"""
    package component.mail:

    - MyAdsAd: Each of the ads displayed on my ads mail

    Note: Only the symbols declared here will be visible outside of the module
"""

from .my_ads_ad import MyAdsAd
from .my_ads_list import MyAdsList
