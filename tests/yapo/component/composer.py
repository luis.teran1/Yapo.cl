import nose_selenium
from selenium.common.exceptions import NoSuchElementException


def ComponentComposer(cls):
    """
        ComponentComposer: Class decorator for Page classes.
        It takes the 'component' field of the class and generates
        a property for each defined component.

        The structure of the component field should be:

            class LePage(Page):
                component = {
                    'attr_name': (ComponentClass, css_selector),
                    ...
                }

        Where:
            - attr_name: Destination field to access the property from
            - ComponentClass: Class constructor to wrap the webelement with
            - css_selector: Selector to obtain the base element for the class from

        The result is equivalent to:

            class LePage(Page):
                _ad_list = sunbro.FindByClass('daList')

                @property
                def ad_list(self):
                    return AdList(self._ad_list)

        With the advantage that is more compact and it's easier to spot which
        are the components for the page. Also, the decorator raises an exception
        whenever it detects that an existing attribute will be overwritten.

        As the code uses @property, the search is not performed immediately. It will
        be performed when the property is accessed. This allows for the following,
        and very common, access pattern to work seamlessly:

            page = LePage(wd)
            page.go()
            page.attr_name.do_something('cool')
    """

    def find_later(tuple):
        cls, selector = tuple

        def getter(self):
            try:
                base = self._driver.find_element_by_css_selector(selector)
            except NoSuchElementException:
                print ('Could not find [{}], passing along a None'.format(selector))
                base = None
            return cls.instantiate(self._driver, base)
        return getter

    if hasattr(cls, "component"):
        for attr_name, tuple in cls.component.items():
            if hasattr(cls, attr_name):
                msg = 'Clashing attribute [{}] with component on class {}'.format(attr_name, cls.__name__)
                raise KeyError(msg)
            attr = property(find_later(tuple))
            setattr(cls, attr_name, attr)

    return cls
