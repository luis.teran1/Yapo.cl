from selenium.webdriver.support.select import Select
from selenium.common.exceptions import NoSuchElementException
from yapo.wait import PageWait
import nose_selenium


def select(webelement, value):
    """ Fills a select type webelement. Value can be either simple or multiple (iterable) """
    print('Selecting: {}'.format(value))
    # Let's deal with integers as strings to reduce complexity
    if isinstance(value, int):
        value = str(value)

    select = Select(webelement)
    labels = set(map(lambda x: x.text, select.options))
    values = set(map(lambda x: x.get_attribute('value'), select.options))

    def _do_fill(value):
        # This is faster then waiting for an exception to be risen
        if value in values:
            print('by value')
            select.select_by_value(value)
        elif value in labels:
            print('by label')
            select.select_by_visible_text(value)
        else:
            raise NoSuchElementException('Value [{}] not found in values or labels'.format(value))

    # Allow for single and multiple selection
    if isinstance(value, str):
        _do_fill(value)
    else:
        for v in value:
            _do_fill(v)


def values(webelement):
    if webelement.tag_name == 'select' and webelement.get_attribute('multiple'):
        selected = Select(webelement).all_selected_options
        return sorted(map(lambda e: e.get_attribute('value'), selected))

    return webelement.get_attribute('value')


class Form:

    """
        Form is sort of a Mixin to add form handling methods.

        The class that adds Form to its inheritance chain must define:
            - self._driver: A webdriver instance
            - self._wait: A PageWait instance

        This could and should be provided by this class, but a deeper
        refactor would be needed. At the end, only the components should
        have forms, and not the pages and that will defeat the purpose of
        the refactor.
    """

    def is_wrapped_control(self, element):
        # this is the phone_hider control that behaves like a icheck control, but is not
        return self.is_icheck(element) or element.get_attribute('class') in {'phone-switch-cont'}

    def is_labeled_control(self, element):
        id = element.get_attribute('id')
        return self.label_exists(id)

    def label_exists(self, target):
        self._driver.implicitly_wait(0)
        try:
            label = self._driver.find_element_by_css_selector('label[for={}]'.format(target))
            return bool(label)
        except:
            return False
        finally:
            self._driver.implicitly_wait(nose_selenium.TIMEOUT)

    def is_icheck(self, element):
        iclasses = ('iradio', 'icheckbox', 'icheckbox_minimal-blue')
        element_class = element.get_attribute('class')
        for ic in iclasses:
            if ic in element_class:
                return True
        return False

    def val(self, name):
        field = getattr(self, name)
        return values(field)

    def fill(self, name, value):
        field = getattr(self, name)
        parent = field.find_element_by_xpath("..")

        # ins from ichecks will never be displayed
        if not self.is_wrapped_control(parent) and not self.is_labeled_control(field):
            if not parent.get_attribute('class') in {'search-listing__filters-radio__input'}:
                self._wait.until(lambda x: getattr(self, name).is_displayed(), 'Waiting for {} to be displayed'.format(name))

        # I'm just being the nice guy here, let's wait for it to be ready
        if name in ['version', 'rooms', 'add_location']:
            self._wait.until_visible(name)

        self._wait.until(lambda x: getattr(self, name).is_enabled(), 'Waiting for attr {} to be enabled'.format(name))
        field = getattr(self, name)

        if field.tag_name == 'select':
            select(field, value)
        # multiselect elements
        elif parent.get_attribute('class') in {'featurebox footwear_container'}:
            multiselect_checkbox = parent.find_element_by_id(field.get_attribute('inputId'))
            multiselect_checkbox.click()

        elif self.is_icheck(parent):
            print("clicking wrapped checkbox: {0}".format(name))
            if ('checked' in parent.get_attribute('class')) != value:
                parent.find_element_by_css_selector("ins").click()

        elif field.get_attribute('type') in ('checkbox', 'radio'):
            if field.is_selected() != value:
                self.scroll_to_element(field)
                field.parent.execute_script("arguments[0].click()", field)

        else:
            field.clear()
            field.send_keys(value)

    def fill_form(self, **kwargs):
        self.ordered_fill_form((name, value) for name, value in kwargs.items())

    # Just like fill form, but input items can be any dict. In particular OrderedDict
    def ordered_fill_form(self, data):
        for (name, value) in data:
            # We are not raising exceptions :), just passing
            try:
                self.fill(name, value)
            except NoSuchElementException as e:
                print("WARNING: {0} is not present ({1})".format(name, e))
                continue
