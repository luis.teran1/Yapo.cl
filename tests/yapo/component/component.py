from yapo.component import Form
from yapo.wait import PageWait
import re


class Component(Form):

    """
        _dump_map: Dictionary k -> f, where:
        k = Field name
        f = Function to dump that field
        It's used by the Component.dump function to return a dict of {k: f(get(k))}
    """
    _dump_map = {}

    @staticmethod
    def id(x):
        """ Identity function. Returns its parameter unchanged """
        return x

    @staticmethod
    def normalized_text(webelement):
        """ Given a webelement return it's text field after normalizing spaces """
        return re.sub(r'\s+', ' ', webelement.text)

    @staticmethod
    def text(webelement):
        """ Given a webelement return it's text field """
        return webelement.text

    @staticmethod
    def attr(attr):
        """ Returns a function that given a webelement retrieves given attribute """
        def to_attr(webelement):
            return webelement.get_attribute(attr)
        return to_attr

    @staticmethod
    def dumpc(component):
        """ Given a component, dump it """
        return component.dump()

    @staticmethod
    def dumpcs(components):
        """ Given a component list, return the dump of every item """
        return list(map(Component.dumpc, components))

    @staticmethod
    def dumpls(dumpfn):
        """ Returns a function that, given a webelement list, dumps every element with dumpfn """
        def dump_list(webelements):
            return list(map(dumpfn, webelements))
        return dump_list

    def dumpa(self, attr):
        """ Return the dumped valued of an specific attr of the component """
        return self._dump_map[attr](getattr(self, attr))

    def dump(self, fields={}):
        """ Returns a dump of the state of the Component """
        return {k: f(getattr(self, k))
                for k, f in self._dump_map.items()
                if not fields or k in fields}

    @classmethod
    def instantiate(cls, driver, webelement):
        """
            Method invoked by the ComponentComposer class decorator to get an
            instance of the Component. This is a base class implementation that
            returns an object of the same type of the object where instantiate
            was called upon.

            The intended usage is for subclasses to override it and return an
            object of the class that represents the Actual State of the component,
            rather than what the user requested (users can call the constructor
            directly if they specifically want that).

            Example:
                - Header(Component)
                - LoggedInHeader(Header)
                - LoggedOutHeader(Header)

                The idea is that Header.instantiate returns an instance of
                LoggedInHeader or LoggedOutHeader depending on what the the
                actual state on the webpage is.
        """
        return cls(driver, webelement)

    def __init__(self, driver, webelement):
        """ Store basic fields for later usage """
        self._base = webelement
        self._driver = driver
        self._wait = PageWait(self, 5)

    def __repr__(self):
        """ String representation: <classname> <dump> """
        name = self.__class__.__name__
        dump = self.dump()
        return "{} {}".format(name, dump)
