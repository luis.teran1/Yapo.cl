from functools import wraps


def waiting(data_attribute):
    """
        TL;DR: Calls the decorated function but doesn't return control to the
        caller until the given attribute's value changes.

        Q: Why do I want this?
        A: If you know what to wait for, you can wrap your existing actions
           in this decorator to avoid flakiness (aka random test behaviour)

        Q: How does it work?
        A: It finds from the page root the first element with the given attribute.
           Then it calls the decorated function and saves the return value.
           Finally, it waits until the attribute's value is different.

        Q: Any warnings?
        A: If there is at least one scenario in which the value will not change
           then this decorator is not for you, as it will definitely timeout
    """
    def waiting_decorator(func):
        @wraps(func)
        def func_wrapper(self, *args, **kwargs):
            def get_attribute():
                elem = self._driver.find_element_by_css_selector('['+data_attribute+']')
                return elem.get_attribute(data_attribute)
            ref = get_attribute()
            result = func(self, *args, **kwargs)
            self._wait.until(lambda _: ref != get_attribute())
            return result
        return func_wrapper
    return waiting_decorator
