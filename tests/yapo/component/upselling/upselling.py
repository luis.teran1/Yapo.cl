# -*- coding: latin-1 -*-
from yapo.component import AttrFinder
from yapo.component import Component
import sunbro


@AttrFinder
class UpsellingBox(Component):
    title = sunbro.FindByClass('aiUpselling-boxTitle')
    subtitle = sunbro.FindByClass('aiUpselling-boxContent')
    price = sunbro.FindByClass('aiUpselling-price')
    _button = sunbro.FindByClass('aiRadioButton')

    _dump_map = {
        'title': Component.normalized_text,
        'subtitle': Component.normalized_text,
        'price': Component.text,
    }

    def _is_selected(self, _wd=None):
        return '__selected' in self._button.get_attribute('class')

    def click(self):
        self._button.click()
        self._wait.until(self._is_selected)
        return self


@AttrFinder
class UpsellingComboBox(UpsellingBox):
    title = sunbro.FindByClass('aiUpselling-title')
    subtitle = sunbro.FindByClass('aiUpselling-subtitle')
    _button = sunbro.FindByClass('aiUpselling-radio')


@AttrFinder
class UpsellingContainer(Component):
    product = sunbro.FindByID('upselling_product')
    label = sunbro.FindByID('upselling_product_label')
    _combos = sunbro.FindAllByCSS('.aiUpselling-box')
    _boxclass = UpsellingBox

    _dump_map = {
        'product': Component.attr('value'),
        'label': Component.attr('value'),
        'combos': Component.dumpcs,
    }

    @property
    def combos(self):
        return [self._boxclass(self._driver, box) for box in self._combos]

    def combo(self, i=None, id=None):
        if i is not None:
            return self.combos[i]
        if id is not None:
            for box in self._combos:
                if box.get_attribute('id') == id:
                    return self._boxclass(self._driver, box)
            raise LookupError('Combo id [{}] not found'.format(id))
        raise LookupError('No search criteria for combo')


@AttrFinder
class UpsellingContainerMobile(UpsellingContainer):
    _combos = sunbro.FindAllByCSS('.aiUpselling-comboBox')
    _boxclass = UpsellingComboBox
