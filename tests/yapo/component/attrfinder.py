import sunbro


def AttrFinder(cls):
    """
        AttrFinder: Class decorator for Component classes.
        It takes the sunbro.Find fields of the class and replaces
        them with a version that only search within the self._base
        webelement, effectively encapsulating the component
    """

    def new_find(finder):
        def getter(self):
            return finder.find(self._base)
        return getter

    for attr_name in dir(cls):
        attr = getattr(cls, attr_name)
        if isinstance(attr, sunbro.Find):
            attr = property(new_find(attr))
            setattr(cls, attr_name, attr)

    return cls
