from yapo.utils import check_trans, phone_std, salt_password
from yapo.pages.defaults import DEFAULT_AD_INFO
from yapo.pages.defaults import DEFAULT_USER_INFORMATION
from yapo.pages.defaults import DEFAULT_GEO_INFO
from yapo.pages.defaults import CATEGORY_PARAMS

# Control Panel

def authenticate(username='blocket', passwd='430985da851e9223368e820ebde5beaf6c6ef1cc'):
    return check_trans('authenticate',
        remote_addr    = '127.0.0.1',
        remote_browser = 'Narcos',
        username       = username,
        passwd         = passwd,
    )

def accounts_change_pro_status(token, account, pro_types):
    return check_trans('accounts_change_pro_status',
        accounts = account,
        pro_types = pro_types,
        pro_actions = 1,
        token = token,
        remote_addr = '127.0.0.1',
        remote_browser = 'Narcos',
    )

def search_ads(token, email):
    return check_trans('search_ads',
        search_type    = 'email_part',
        email_part     = email,
        archive_group  = 'noarchive',
        token          = token,
        remote_addr    = '127.0.0.1',
        remote_browser = 'Narcos',
        time_from      = '2000-01-01 00:00:01'
    )

def review(ad_id, token, action_id = '1', action = 'accept', reason = '31', price = None):
    data = {
        'ad_id'          : ad_id,
        'action_id'      : action_id,
        'action'         : action,
        'token'          : token,
        'remote_addr'    : '127.0.0.1',
        'remote_browser' : 'Narcos',
        'filter_name'    : 'normal',
    }
    if action == 'refuse':
        data['reason'] = reason
    if action == 'accept':
        if price:
            data['price'] = price
    return check_trans('review', **data)

def newad(data):

    def add_defaults(data):
        if 'category' not in data:
            data['category'] = DEFAULT_AD_INFO['category']
        cat = int(data['category'])
        if cat in CATEGORY_PARAMS:
            for k in CATEGORY_PARAMS[cat]:
                if k not in data:
                    data[k] = DEFAULT_AD_INFO[k]
        for k in ['subject', 'body']:
            if k not in data:
                data[k] = DEFAULT_AD_INFO[k]
        for k, v in DEFAULT_USER_INFORMATION.items():
            if k not in data:
                data[k] = v
        for k, v in DEFAULT_GEO_INFO.items():
            if k not in data:
                data[k] = v
        # Give preference to hashed passwords
        if not data['passwd'].startswith('$'):
            if 'password' not in data:
                data['password'] = data['passwd']
            del data['passwd']
        elif 'password' in data:
            del data['password']
        return data

    def translate(data):
        drop = {
            'email_verification',
            'email_confirm',
            'password_verification',
            'passwd_ver',
            'accept_conditions',
            'add_location',
            'show_location',
            'ai_create_account',
            'phone_type',
            'submit',
            'Submit',
            'rut',
            'auto_clear',
        }
        reject = {
            'create_account': (True, "Sorry, this method doesn't know how to create accounts"),
            'Preview': (None, "If you want a preview, you definitely not want to use trans"),
        }
        transform = {
            'phone': phone_std,
            'password': salt_password,
            'private_ad': lambda v: '0' if v else '1',
            'company_ad': int,
            'footwear_type': lambda v: '|'.join(v) if type(v) is list else v,
            'fwtype': lambda v: '|'.join(v) if type(v) is list else v,
            'jc': '|'.join,
            'sct': '|'.join,
        }
        rename = {
            'password': 'passwd',
            'private_ad': 'company_ad',
            'fwtype': 'footwear_type',
            'jc': 'job_category',
            'sct': 'service_type',
        }
        update = {
            'lang': 'es',
            'source': 'web',
            'remote_addr': '127.0.0.1',
            'remote_browser': 'Narcos',
            'do_not_send_mail': '1',
        }
        add = {
            'type': 's',
            'pay_type': 'free',
        }

        ad_data = {}
        for k, v in data.items():
            if k in drop:
                continue
            if k in reject:
                bad_val, msg = reject[k]
                if bad_val is None or v == bad_val:
                    raise Exception(msg)
                continue
            if k in transform:
                v = transform[k](v)
            if k in rename:
                k = rename[k]
            ad_data[k] = v

        ad_data.update(update)
        for k, v in add.items():
            if k not in ad_data:
                ad_data[k] = v

        print(ad_data)
        return ad_data

    add_codes()
    data = add_defaults(data)
    ad_data = translate(data)

    return check_trans('newad', **ad_data)

def clear(data):
    data['remote_addr'] = '127.0.0.1'
    data['remote_browser'] = 'Narcos'
    return check_trans('clear', **data)

def add_codes():
    data = {
        'paylimit':20,
        'verifylimit':30
    }

    return check_trans('create_adcodes',**data)


def deletead(list_id):
    """ Deletes an ad by list_id """
    result = authenticate()
    return check_trans('deletead',
                remote_addr = '127.0.0.1',
                id = list_id,
                source = 'web',
                reason = 'admin_deleted',
                token = result['token'])

# Accounts

def get_account(email = None, user_id = None, account_id = None):
    params = {}
    if email:
        params['email'] = email
    elif user_id:
        params['user_id'] = user_id
    elif account_id:
        params['account_id'] = account_id

    return check_trans('get_account', **params)


def get_account_ads(user_id = None, account_id = None):
    params = {'page_number': '0'}
    if user_id:
        params['user_id'] = user_id
    elif account_id:
        params['account_id'] = account_id

    r = {}
    tr = check_trans('get_account_ads', use_lists = True, **params)

    exclude = set(['page_ads', 'n_ads', 'status'])
    headers = [k for k in tr if k not in exclude]
    ads = zip(*[v for k, v in tr.items() if k not in exclude])

    for _ad in ads:
        ad = {k: v for k, v in zip(headers, _ad)}
        r[ad['ad_id']] = ad

    return r


def change_ad_status(data={}):
    """ Change ad status an ad"""
    params = {
        'list_id': '8000093',
        'new_status': 'deactivated',
        'deletion_reason': 1,
        'remote_addr': '127.0.0.1',
        'source': 'web',
        'deletion_timer': 2,
        'reason': 'user_deleted',
        'batch': 1
    }
    if data:
        params.update(data)
    return check_trans('change_ad_status',**params)

# Promotional Pages

def insert_pp(data):
    return trans.check_trans('set_promotional_page', **data)

def delete_pp(id):
    return check_trans(
        'delete_promotional_page',
        pp_id = id)

def add_gallery(ad_id):
    return check_trans("add_gallery",
        ad_id       =   ad_id,
        expire_time = '7 days',
        batch       = '0',
        remote_addr = '127.0.0.1'
    )

def add_label(ad_id, label_type = 'opportunity'):
    return check_trans("add_label",
            ad_id       = ad_id,
            label_type  = label_type,
            remote_addr = '127.0.0.1',
            batch       = '1'
    )

def add_weekly_bump(ad_id, counter = 3):
    return check_trans("weekly_bump_ad",
            ad_id       = ad_id,
            counter     = counter,
            remote_addr = '127.0.0.1',
            batch       = '1',
            batch_bump  = '1'
    )

def add_daily_bump(ad_id, counter = 7):
    return check_trans("weekly_bump_ad",
            ad_id       = ad_id,
            counter     = counter,
            remote_addr = '127.0.0.1',
            batch       = '1',
            batch_bump  = '1',
            daily_bump  = '1'
    )
