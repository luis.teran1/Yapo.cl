from . import conf
from nose_selenium import SeleniumTestCase
from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from yapo.utils import JsonTool
import nose_selenium
import subprocess
import sys
import os
import logging
import yapo.utils
import time
import signal

logger = logging.Logger('yapo.test')


class ImageOrderException(Exception):
    pass


class SeleniumTest(SeleniumTestCase):

    @classmethod
    def _set_bconfs(cls):
        if hasattr(cls, 'bconfs'):
            cls._bconf = utils.trans('bconf', port=conf.REGRESS_BCONFD_TRANS_PORT)
            for key, value in getattr(cls, 'bconfs', {}).items():
                oldvalue = cls._bconf.get(key, None)
                logger.debug("Overwriting bconf {0}: {1} -> {2}".format(key, oldvalue, value))
                yapo.utils.bconf_overwrite(key, value, False)
            yapo.utils.reload_trans()
            yapo.utils.apache_restart()

    @classmethod
    def _restore_snaps(cls):
        snaps = getattr(cls, 'snaps', [])
        if snaps:
            yapo.utils.restore_cleandb()
            yapo.utils.restore_snaps_in_a_cooler_way(snaps)
            yapo.utils.rebuild_asearch()
            yapo.utils.rebuild_csearch()
            yapo.utils.redis_load_email()

    @classmethod
    def setUpClass(cls):
        cls._set_bconfs()
        cls._restore_snaps()
        yapo.utils.flush_redises()
        yapo.utils.start_x()

    @classmethod
    def tearDownClass(cls):
        if utils.is_not_regress():
            if hasattr(cls, 'pro_data'):
                JsonTool.save_test_data(cls.pro_data)
        if hasattr(cls, '_bconf') and hasattr(cls, 'bconfs'):
            logger.debug("Restoring bconfs")
            yapo.utils.flush_overwrite()
            yapo.utils.apache_restart()
        if nose_selenium.BROWSER == 'CHROME':
            p = subprocess.Popen(['ps', '-A'], stdout=subprocess.PIPE)
            out, err = p.communicate()
            for line in out.splitlines():
                if 'chromedriver' in str(line):
                    pid = int(line.split(None, 1)[0])
                    try:
                        os.kill(pid, signal.SIGKILL)
                    except PermissionError:
                        pass

    def translate(self, conf_string):
        """Returns the 'lang' bconf content for conf_string"""
        key = "*.*.language.{0}.es".format(conf_string)
        if not hasattr(self, '_bconf'):
            self._bconf = utils.trans('bconf', port=conf.REGRESS_BCONFD_TRANS_PORT)
        return self._bconf.get(key, '')

    def assertTextEquals(self, webelement, string):
        """Asserts that the text of the web element contains the specified text"""
        self.assertEqual(webelement.text, string)

    def assertNotPresent(self, page, element_name):
        """Asserts that the element is not present, beware that it doesn't wait
        for the element to disappear, it only checks the thing.

        If you want to wait, use the yapo.pages.Wait class or wait attribute
        in yapo page objects
        """
        wd = page._driver
        (by, what) = page.selector(element_name)
        print("find {0} by {1}".format(what, by))
        self.assertRaises(exceptions.NoSuchElementException, lambda: wd.find_element(by, what))

    def assertVisible(self, page, element_name):
        """Asserts that the element is not present or not visible"""
        wd = page._driver
        (by, what) = page.selector(element_name)
        try:
            elem = wd.find_element(by, what)
            if not elem.is_displayed():
                raise AssertionError('element {0} is not visible'.format(element_name))
        except exceptions.NoSuchElementException:
            raise AssertionError('element {0} is not visible (and/or present)'.format(element_name))

    def assertNotVisible(self, page, element_name):
        """Asserts that the element is not present or not visible"""
        wd = page._driver
        (by, what) = page.selector(element_name)
        try:
            elem = wd.find_element(by, what)
            if elem.is_displayed():
                raise AssertionError('element {0} is visible'.format(element_name))
        except exceptions.NoSuchElementException:
            pass

    def assertTrans(self, trans, expected):
        """Asserts that only the keys present in 'expected' contains the
        same values in 'trans'.
        THE PARAMETER ORDER IS IMPORTANT
        This is useful when you must check only certain values on the trans
        response, saving typing
        """
        # Only compare the keys present in expected_trans
        self.assertEqual(dict(filter(lambda t: t[0] in expected, trans.items())), expected)

    def assertInRetry(self, needle, haystack, retry_fn, retries=3):
        """ Asserts that 'needle' is in 'haystack';
        if not, before retrying, it executes 'retry_fn'
        and tries again 'retries' times
        """
        for i in range(retries+1):
            try:
                self.assertIn(needle, haystack)
            except:
                print (sys.exc_info()[1])
                if i == retries:
                    raise
                retry_fn()

    def _build_webdriver(self, *args, **kwargs):
        browser = nose_selenium.BROWSER
        print("Using browser: {}".format(browser));
        legacy = os.environ.get('LEGACY', 'no')
        print("Try to use Browser legacy: {}".format(legacy))

        if (hasattr(self, 'override_browser')):
            browser = self.override_browser

        if (hasattr(self, 'user_agent') or hasattr(self, 'firefox_profile_options')) and browser not in ['FIREFOX', 'CHROME', 'CHROME_LEGACY']:
            raise Exception('User agent override only available for Firefox')

        if browser == 'FIREFOX':
            profile = webdriver.FirefoxProfile()
            # deprecated: the user_agent configuration should be included in the firefox_profile_options dictionary like this: {'general.useragent.override': 'Mozila something'}
            if hasattr(self, 'user_agent'):
                profile.set_preference("general.useragent.override", self.user_agent)
            if hasattr(self, 'firefox_profile_options'):
                for key in self.firefox_profile_options:
                    profile.set_preference(key, self.firefox_profile_options[key])
            wd = webdriver.Firefox(profile)
            wd.implicitly_wait(nose_selenium.TIMEOUT)
            wd.maximize_window()
            return wd

        # legacy to old test with chrome
        if browser == 'CHROME_LEGACY' or legacy == 'yes':
            options = webdriver.ChromeOptions()
            options.add_argument("--start-maximized")
            if hasattr(self, 'chrome_profile_options'):
                for key in self.chrome_profile_options:
                    options.add_argument(key)
            prefs = {'download.default_directory': conf.TOPDIR + '/tests/tmp'}
            options.add_experimental_option("prefs", prefs)
            if hasattr(self, 'user_agent'):
                options.add_argument('--user-agent={0}'.format(self.user_agent))
                if self.user_agent in [utils.UserAgents.IOS, utils.UserAgents.ANDROID, utils.UserAgents.NEXUS_5]:
                    mobile_emulation = {"deviceName": "Google Nexus 5"}
                    options.add_experimental_option("mobileEmulation", mobile_emulation)
            wd = webdriver.Chrome(chrome_options = options)
            wd.implicitly_wait(nose_selenium.TIMEOUT)
            return wd

        # chromedriver should be in $PATH, (/usr/bin/chromedriver maybe)
        # or can specify the path to the binary -> "webdriver.Chrome('/path/to/chromedriver')"
        if browser == 'CHROME':
            options = webdriver.ChromeOptions()
            if hasattr(self, 'chrome_profile_options'):
                for key in self.chrome_profile_options:
                    options.add_argument(key)
            prefs = {'download.default_directory': conf.TOPDIR + '/tests/tmp'}
            options.add_experimental_option("prefs", prefs)
            if hasattr(self, 'user_agent'):
                options.add_argument('--user-agent={0}'.format(self.user_agent))
                if self.user_agent in [utils.UserAgents.IOS, utils.UserAgents.ANDROID, utils.UserAgents.NEXUS_5]:
                    mobile_emulation = {"deviceName": "Nexus 5"}
                    options.add_experimental_option("mobileEmulation", mobile_emulation)
            # to can download by script and get files from node
            options.add_experimental_option('prefs', {
                 'download.prompt_for_download': False,
                 'plugins.always_open_pdf_externally': True,
                 'safebrowsing_for_trusted_sources_enabled': False
             })
            desired_options = options.to_capabilities()
            desired_options['goog:chromeOptions'] = desired_options['chromeOptions']
            # the web driver doesn't resolve DNS, so we need to use IP directly
            hub = os.environ.get('SELENIUM_HUB', 'http://172.21.0.144:4445/wd/hub')
            wd = webdriver.Remote(command_executor=hub, desired_capabilities = desired_options)
            wd.implicitly_wait(nose_selenium.TIMEOUT)
            return wd

        return super()._build_webdriver(self, *args, **kwargs)

    def shortDescription(self):
        """Prepends class name to test short description output"""
        return '[{}] {}: {}'.format(time.strftime('%X'), self.__class__.__name__, self._testMethodDoc.strip())

    def is_element_present(self, wd, how, what):
        try:
            wd.find_element(by=how, value=what)
        except exceptions.NoSuchElementException as e:
            return False
        return True

    def reload(self, wd):
        wd.get(wd.current_url)
