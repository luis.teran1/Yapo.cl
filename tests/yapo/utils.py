# -*- coding: latin-1 -*-
import os
import quopri
import logging
import subprocess
import re
import glob
from yapo import conf
from redis import Redis
from collections import OrderedDict
from selenium.common import exceptions
from selenium.webdriver.support.ui import WebDriverWait
import psycopg2
import time
import datetime
import random
import hashlib
import urllib.request
import json
import yapo
import csv
import base64
import hashlib

logger = logging.Logger(__name__)
logger.setLevel(logging.DEBUG)


class SnapException(Exception):

    """Exception for Snap related stuff"""
    pass


class TransError(Exception):

    """Exception for trans Errors"""

    def __init__(self, result):
        self.result = result
        status = result.get('status')
        super(TransError, self).__init__(status)


class TransBlob:
    def __init__(self, key, length):
        self.key = key
        self.len = int(length) + 1
        self.buf = ''
    def append(self, line):
        if self.buf:
            self.buf += '\n'
        self.buf += line
        self.len -= len(line) + 1
        if self.len < 0:
            raise Exception('Adding line exceeds blob length')
    def data(self):
        return self.key, self.buf
    def __len__(self):
        return self.len

def reload_trans():
    return trans('control', port=conf.REGRESS_TRANS_CPORT, action='reload')

def reload_filterd():
    subprocess.check_output("make -C {0} filterd-regress-reload 2> /dev/null".format(conf.TOPDIR), shell=True)

def apache_restart():
    """Restart apaches"""
    out = subprocess.check_output("make -C {0} apache-regress-stop 2> /dev/null".format(conf.TOPDIR), shell=True)
    subprocess.check_output("rm -rf {0}/.apache_regress0.lock".format(conf.TOPDIR), shell=True)
    out = subprocess.check_output("make -C {0} apache-regress-start 2> /dev/null".format(conf.TOPDIR), shell=True)

def makebjr():
    out = subprocess.check_output("make -C {0} bj 2> /dev/null".format(conf.TOPDIR), shell=True)
    return out

def trans(cmd, commit=1, host=conf.REGRESS_HOSTNAME, port=conf.REGRESS_TRANS_PORT, use_lists=False, **kwargs):
    """Makes a request to trans and returns the results as a dict"""
    kwargs.update({'cmd': cmd, 'commit': commit})
    q = '\n'.join(map(lambda v: v[0] + ':' + str(v[1]), kwargs.items())) + '\nend\n'
    s = 'printf \'{query}\' | nc {host} {port}'.format(query=q, host=host, port=port)
    out = subprocess.check_output(s.encode('latin-1'), shell=True)
    result = OrderedDict()
    blob = None
    for line in out.decode('latin-1').splitlines():
        # If we are in the middle of a blob, let's accumulate the lines
        if blob:
            blob.append(line)
            # We're done, let's store the blob
            if len(blob) == 0:
                k, v = blob.data()
                result[k] = v
                blob = None
            continue

        s = str(line).split(':', 1)
        # Cheat: for bconfs we use as key the bconf name, not 'conf'
        if cmd == 'bconf' and s[0] == 'conf':
            b = s[1].split('=', 1)
            result[b[0]] = b[1]

        # We drop lines with just one entry
        elif len(s) >= 2:
            if s[0] == 'blob':
                # Previous split was not friendly with blobs
                s = str(line).split(':', 2)
                blob = TransBlob(s[2], s[1])
                continue

            # Keep repeated keys on a list, instead of overwriting
            if use_lists:
                if s[0] not in result:
                    result[s[0]] = []
                result[s[0]].append(s[1])
            # Just store the output
            else:
                result[s[0]] = s[1]
    return result


def check_trans(cmd, withraise=True, use_lists=False, commit=1, **kwargs):
    """Makes a request to trans and raises TransError if status is not TRANS_OK"""
    result = trans(cmd, commit, use_lists=use_lists, **kwargs)
    if not use_lists and "TRANS_OK" in result.get('status'):
        return result
    if use_lists and "TRANS_OK" in result.get('status')[0]:
        return result
    if withraise:
        raise TransError(result)

def kill_db_connections():
    psql = '/usr/bin/psql -h {host} blocketdb -f {path}/death_note.sql || true'.format(
            host=conf.REGRESS_PGSQL_HOST,
            path=conf.TOPDIR + '/tests/'
    )
    out = subprocess.check_output(psql, shell=True)

def restore_db_snap(snap_name):
    """Restores the db snap with `snap_name`"""
    result = trans('db_snap', action='restore', name=snap_name)
    logger.debug("Restore of {snap} failed, trying to load".format(snap=snap_name))
    if result['status'] != 'TRANS_OK':
        raise SnapException("Could not load snap {snap}, trans status was: {status}".format(
            snap=snap_name, status=result.get('status')))

    reload_filterd()


def take_db_snap(name):
    """Creates a db snap"""
    check_trans('db_snap', action='snap', name=name)


def delete_db_snap(name):
    """Deletes a database snap"""
    conn = psycopg2.connect(database='postgres', host=conf.REGRESS_PGSQL_HOST)
    conn.set_isolation_level(0)
    cur = conn.cursor()
    cur.execute('DROP DATABASE IF EXISTS "db-snap-{snap}";'.format(snap=name))


def snap_loaded(name):
    """Tests whether a snap is loaded and can be restored"""
    snap_name = 'db-snap-{}'.format(name)
    return check_trans('db_exists', db_name=snap_name, withraise=False) != None


def load_snap(snap_name):
    print('loading snap {}'.format(snap_name))
    out = subprocess.check_output(
        'make -C {0} db-load-{1}'.format(conf.TOPDIR, snap_name),
        shell=True)


def reset_oneclick_db():
    print('Reseting OneClickPayment DB.')
    out = subprocess.check_output(
        'make -C {0}/oneclick ocp-ms-db-load-default'.format(conf.TOPDIR), shell=True)


def restore_snaps_in_a_cooler_way(snap_list):

    # Restore the 'biggest' snap already available
    kill_db_connections()
    print('snaps: {}'.format(snap_list))
    last = 0
    snap_cat_list = [('-'.join(snap_list[:i+1]), snap_list[i], i) for i in range(len(snap_list))]
    for snap_str, snap, i in reversed(snap_cat_list):
        print('testing {}: {}'.format(i, snap_str))
        if snap_loaded(snap_str):
            last = i+1
            restore_db_snap(snap_str)
            break

    # Load the other snaps (saving intermediate steps)
    for snap_str, snap, _ in snap_cat_list[last:]:
        load_snap(snap)
        take_db_snap(snap_str)
        restore_db_snap(snap_str)

def restore_cleandb():
    """Cleans the db and creates a initial snap based on blocketdb_pristine if it doesn't exist"""
    kill_db_connections()
    cleansnap = 'cleandb'
    cleandbname = 'db-snap-cleandb'
    psql = '/usr/bin/psql -h {host}'.format(host=conf.REGRESS_PGSQL_HOST)
    try:
        restore_db_snap(cleansnap)
    except SnapException:
        # Load failed create the database based on pristine
        db_psql = psql + " postgres -c "
        db_cmd = '''{psql} 'DROP DATABASE IF EXISTS "{db}";' '''.format(psql=db_psql, db=cleandbname)
        logger.debug("running command " + db_cmd)
        out = subprocess.check_output(db_cmd, shell=True)
        create_cmd = '''{psql} 'CREATE DATABASE "{newdb}" TEMPLATE blocketdb_pristine;' '''.format(psql=db_psql, newdb=cleandbname)
        logger.debug("running command " + create_cmd)
        out = subprocess.check_output(create_cmd, shell=True)
        restore_db_snap(cleansnap)


def flush_overwrite():
    make('trans-flush-overwrite', '/daemons/trans')


def bconf_overwrite(key, value, with_reload=True):
    """Overwrites the 'key' bconf with the given value """
    logger.info("Overwritting bconf: {key} with {value}".format(key=key, value=value))
    url = "{host}/bconf_overwrite?key={key}&value={value}".format(
        host=conf.BCONFD_URL,
        key=urllib.parse.quote(key, safe='/*'),
        value=urllib.parse.quote(str(value))
    )

    # If something breaks, you would like to know what happenned
    print(url)

    r = json.loads(urllib.request.urlopen(url).read().decode('utf8'))
    if 'status' not in r or r['status'] != 'success':
        raise Exception('bconf_overwrite [{key} = {value}] failed'.format(key=key, value=value))

    if with_reload:
        reload_trans()
        apache_restart()

def expire_packs(pack_id=None):
    """Expire packs for all the users """
    if pack_id:
        logger.info("Expire packs {}".format(pack_id))
        trans('expire_packs', pack_id=pack_id)
    else:
        logger.info("Expire packs for all users")
        trans('expire_packs')

def setconf(key, value, with_reload=True, remote_address='127.0.0.1', batch='1'):
    """Sets the 'key' bconf with the given value.
        >>> setconf('*.*.some.bconf', 'myValue')
    """
    logger.info("Setting bconf: {key} with {value}".format(key=key, value=value))
    trans('setconf', set='{key}={value}'.format(key=key, value=value), remote_addr=remote_address, batch=batch)
    if with_reload:
        reload_trans()
        apache_restart()

def rebuild_full(index_name):
    out = subprocess.check_output("make -C {0} rebuild-{1} 2> /dev/null".format(conf.TOPDIR, index_name), shell=True)

def rebuild_asearch():
    """Rebuilds asearch index"""
    out = subprocess.check_output("make -C {0} rebuild-asearch 2> /dev/null".format(conf.TOPDIR), shell=True)

def rebuild_msearch():
    """Rebuilds msearch index"""
    out = subprocess.check_output("make -C {0} rebuild-msearch 2> /dev/null".format(conf.TOPDIR), shell=True)

def rebuild_csearch():
    """Rebuilds csearch index"""
    out = subprocess.check_output("make -C {0} rebuild-csearch 2> /dev/null".format(conf.TOPDIR), shell=True)

def rebuild_josesearch():
    """Rebuilds josesearch index"""
    out = subprocess.check_output("make -C {0} rebuild-josesearch 2> /dev/null".format(conf.TOPDIR), shell=True)

def rebuild_index(full = None):
    """Rebuild index"""
    if full:
        out = subprocess.check_output("make -C {0} rebuild-index-full 2> /dev/null".format(conf.TOPDIR), shell=True)
    else:
        out = subprocess.check_output("make -C {0} rebuild-index 2> /dev/null".format(conf.TOPDIR), shell=True)

def flush_preprocessing_queue():
    """moves the ads from the preprocessing queue to the corresponding queue"""
    out = subprocess.check_output("make -C {0} flush_preprocessing-queue 2> /dev/null".format(conf.TOPDIR), shell=True)

def redis_load_wordsranking():
    """Rebuilds redis wordsranking"""
    out = subprocess.check_output("make -C {0} redis-load-wordsranking 2> /dev/null".format(conf.TOPDIR), shell=True)

def redis_load_email():
    """Rebuilds redis email"""
    out = subprocess.check_output("make -C {0} redis-load-email 2> /dev/null".format(conf.TOPDIR), shell=True)

def flush_redises():
    ports = [
        conf.REGRESS_REDISSTAT_PORT,
        conf.REGRESS_REDISACCOUNTS_PORT,
        conf.REGRESS_REDISSESSION_PORT,
        conf.REGRESS_REDISNEXTGEN_API_PORT,
    ]

    for port in ports:
        server = Redis(port=int(port))
        server.flushall()

def redis_command(port, *args):
    redis = Redis(port=int(port))
    return redis.execute_command(*args)

def start_redis_session():
    subprocess.check_output('make -C {} redis-start-session'.format(conf.TOPDIR), shell=True)
    check = lambda: subprocess.check_output('ps ux | grep "redis-server \*:{}"'.format(conf.REGRESS_REDISSESSION_PORT), shell=True)
    while True:
        try:
            ps_result = check()
        except Exception as e:
            pass
        if ps_result != '':
            break
        time.sleep(0.5)


def stop_redis_session():
    subprocess.check_output('ps ux | grep "redis-server \*:{}" | awk \'{{print "kill "$2}}\' | sh'.format(conf.REGRESS_REDISSESSION_PORT), shell=True)
    subprocess.check_output('rm {}/.redis_session_build0.lock'.format(conf.TOPDIR), shell=True)
    check = lambda: subprocess.check_output('ps ux | grep "redis-server \*:{}"'.format(conf.REGRESS_REDISSESSION_PORT), shell=True)
    while True:
        try:
            ps_result = check()
        except Exception as e:
            break
        time.sleep(0.5)

def flush_redis_banners():
    subprocess.check_output('/usr/bin/redis-cli -p {} -n 1 flushdb'.format(conf.REGRESS_REDISWORDSRANKING_PORT), shell=True)

def upload_image(image_path, image_page_object):
    image_page_object.send_keys(os.path.join(conf.RESOURCES_DIR, 'images', image_path))

def search_mails(regex, max_retries=5):
    """Search and returns the first term matching regex or None
    if the regex uses groups (uses parentheses "()") it returns the first group"""

    for retries in range(max_retries):
        for mail in conf.MAIL_LOG_PATHS:
            try:
                mailfile = open(mail, 'rb')
            except FileNotFoundError:
                logger.warning("file {0} not found".format(mail))
                continue
            mail_content = mailfile.read()
            decoded_mail = quopri.decodestring(mail_content).decode('latin-1')
            matches = re.search(regex, decoded_mail, re.MULTILINE)
            if matches:
                if matches.groups():
                    return matches.groups()[0]
                else:
                    return matches.group()
        time.sleep(1)
    return None

def find_in_mails(needle, max_retries=5):
    """Search and returns true if needle is found on text email or None"""
    for retries in range(max_retries):
        for mail in conf.MAIL_LOG_PATHS:
            try:
                mailfile = open(mail, 'rb')
            except FileNotFoundError:
                logger.warning("file {0} not found".format(mail))
                continue
            mail_content = mailfile.read()
            decoded_mail = quopri.decodestring(mail_content).decode('latin-1')
            found = decoded_mail.find(needle)
            if found >= 0:
                return True;
        time.sleep(1)
        print("retry")
    return None

def clean_mails():
    """Delete all mail logs"""
    for mail in conf.MAIL_LOG_PATHS:
        if os.path.isfile(mail):
            os.remove(mail)

def cleanlogs():
    """ clean the logs cause logs are dirty """
    import glob
    for log_file in glob.glob("{}/logs/*".format(conf.REGRESS_FINAL)):
        os.remove(log_file)


def start_x():
    """Start X server if it's not running"""
    pidfile = os.path.abspath(os.path.join(conf.TOPDIR, 'xvnc.pid'))
    os.environ.update(DISPLAY=':' + str(os.getuid()))
    # check if Xvnc is running
    if os.path.isfile(pidfile):
        with open(pidfile, 'r') as file:
            pid = int(file.read())
        # signal 0 only check if process is alive
        try:
            os.kill(pid, 0)
        except ProcessLookupError:
            pass
        except PermissionError:
            os.remove(pidfile)
            return start_x()
    uid = os.getuid()
    args = 'nohup /usr/bin/Xvnc -ac -SecurityTypes None :{uid}'.format(uid=uid)
    devnull = open(os.devnull, 'wb')
    pid = subprocess.Popen(args.split(' '), stdout=devnull, stderr=devnull).pid
    with open(pidfile, 'w') as f:
        f.write(str(pid))
    logger.debug("Xvnc PID: {0}".format(pid))


def trans_restart():
    """Restart trans deamon"""
    out = subprocess.check_output("make -C {0} trans-reload 2> /dev/null".format(conf.TOPDIR), shell=True)

def enable_clear_xiti():
    enable_local_hit_xiti()
    clear_hit_xiti_log()

def get_click_xiti_log(p):
    """
    Get a list with the xiti log for a click tag
    The p argument is the value of the p var in the xiti tag call
    as an example: Report_this_ad::8000::8020::report_the_ad
    """
    p = p.replace('::', '-')
    list = {}
    file_path = conf.TOPDIR + "/regress_final/logs/hit_xiti." + p.lower() + ".click.log"
    wait_for_file(file_path, 10)
    if os.path.exists(file_path):
        list = get_xiti_logs_as_dictionary(file_path)
    else:
        print('the file {} does not exists'.format(file_path))
    return list


def get_page_xiti_log(page_name, prio = [0,2,1]):
    """
    Get a list with the xiti log for a page tag
    The page argument is the value of the p var in the xiti tag call
    as an example: ad
    """
    ext_logs = ['.click.log', '.page.log', '.json.log']
    ext_logs = [ ext_logs[i] for i in prio ]

    prefix_path = conf.TOPDIR + "/regress_final/logs/hit_xiti."

    # Try this 10 times
    for i in range(10):
        # Loop over the possible files
        for ext in ext_logs:
            file_path = prefix_path + page_name.lower() + ext
            # If the file exist return the list
            if os.path.exists(file_path):
                print('the file {} was found'.format(file_path))
                return get_xiti_logs_as_dictionary(file_path)

            print('the file {} does not exists'.format(file_path))
        # Sleep to wait for the files to be written
        time.sleep(1)

    return {}


def get_xiti_logs_as_dictionary(file_path, list={}):
    """Find a string in a text file"""
    with open(file_path, 'r') as f:
        for line in f.readlines():
            if ' => ' in line:
                nodes = None
                nodes = line.split(' => ')
                key = nodes[0].strip().strip().replace("'", '')
                value = nodes[1].strip().replace("'", '')[:-1]
                list.update({key: value})
    return list


def xiti_clear_logs():
    """Delete xiti log files"""
    subprocess.call("rm -Rf {}/regress_final/logs/hit_xiti.*".format(conf.TOPDIR), shell=True)


def wait_for_file(file_path, timeout):
    """Wait for file to appear in the disk"""
    lapse = 0.5
    start_time = time.time()
    while not (os.path.exists(file_path) and os.path.getsize(file_path) > 0) and (time.time() - start_time) < timeout:
        time.sleep(lapse)

def get_file_md5sum(filepath):
    """Get the md5 hash for a file"""
    command = "md5sum " + filepath + " | awk '{ print $1 }'"
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    output = output[:-1]
    return output

def get_md5sum(text):
    """Get the md5 hash for a file"""
    m = hashlib.md5()
    m.update(text)
    return m.hexdigest()

def run_nubox_php_script(filename):
    command = "php -c {0}/conf/php.ini {1}/scripts/batch/bill_processing/{2}".format(
        conf.REGRESS_FINAL, conf.TOPDIR, filename)
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    logger.debug(output)


def run_nubox_php_script_async(filename):
    command = "php -c {0}/conf/php.ini {1}/scripts/batch/bill_processing/{2} &".format(
        conf.REGRESS_FINAL, conf.TOPDIR, filename)
    output = subprocess.check_output(command, shell=True, stderr=subprocess.STDOUT)
    logger.debug(output)

def is_not_regress():
    return ('dev' in conf.DESKTOP_URL or 'yapo' in conf.DESKTOP_URL) and 'regress.yapo.cl' not in conf.DESKTOP_URL

def phone_std(phone):
    # XXX We need a real implementation here
    return '9' + str(phone)

def salt_password(passwd):
    # passwd:$1024$gs,8Evu9sp!GUR}588e24ccdc9839e7dda1393fbb282a5474b5c2d22
    loops = 1024
    salt = ''.join(chr(random.randint(32, 126)) for _ in range(16))
    salt = re.sub(r"['\\%-]", 'x', salt)

    for _ in range(loops):
        passwd = hashlib.sha1(passwd.encode('latin-1')).hexdigest()

    return '${}${}{}'.format(loops, salt, passwd)

class Categories:
    """ Categories ID definition  """
    INMUEBLES = 1000
    VENDO = 1220
    ARRIENDO = 1240
    ARRIENDO_TEMPORADA = 1260
    VEHICULOS = 2000
    AUTOS_CAMIONETAS_Y_4X4 = 2020
    MOTOS = 2060
    CAMIONES_Y_FURGONES = 2040
    ACCESORIOS_Y_PIEZAS_PARA_VEHICULOS = 2100
    BARCOS_LANCHAS_Y_AVIONES = 2080
    OTROS_VEHICULOS = 2120
    MODA_BELLEZA_SALUD = 4000
    MODA_VESTUARIO_Y_CALZADO = 4020
    BOLSOS_BISUTERIA_Y_ACCESORIOS = 4040
    CALZADO = 4080
    SALUD_Y_BELLEZA = 4060
    HOGAR = 5000
    MUEBLES = 5020
    ELECTRODOMESTICOS = 5040
    JARDIN_Y_HERRAMIENTAS = 5060
    ARTICULOS_DEL_HOGAR = 5160
    TIEMPO_LIBRE = 6000
    ANIMALES_Y_SUS_ACCESORIOS = 6140
    DEPORTES_GIMNASIA_Y_ACCESORIOS = 6020
    BICICLETAS_CICLISMO_Y_ACCESORIOS = 6060
    HOBBIES_Y_OUTDOOR = 6180
    INSTRUMENTOS_MUSICALES_Y_ACCESORIOS = 6080
    MUSICA_Y_PELICULAS = 6100
    LIBROS_Y_REVISTAS = 6120
    ARTE_ANTIGUEDADES_Y_COLECCIONES = 6160
    COMPUTADORES_Y_ELECTRONICA = 3000
    COMPUTADORES_Y_ACCESORIOS = 3040
    CELULARES_TELEFONOS_Y_ACCESORIOS = 3060
    CONSOLAS_VIDEOJUEGOS_Y_ACCESORIOS = 3020
    AUDIO_TV_VIDEO_Y_FOTOGRAFIA = 3080
    SERVICIOS_NEGOCIOS_Y_EMPLEO = 7000
    OFERTAS_DE_EMPLEO = 7020
    BUSCO_EMPLEO = 7040
    SERVICIOS = 7060
    NEGOCIOS_MAQUINARIA_Y_CONSTRUCCION = 7080
    OTROS = 8000
    FUTURA_MAMA_BEBES_Y_NINOS = 9000
    VESTUARIO_FUTURA_MAMA_Y_NINOS = 9020
    JUGUETES = 9040
    COCHES_Y_OTROS_ARTICULOS = 9060
    OTROS_PRODUCTOS = 8020

    _ad_params = {
        1220: ['estate_type', 'rooms', 'garage_spaces'],
        1240: ['estate_type', 'garage_spaces'],
        1260: ['estate_type', 'garage_spaces'],
    }

    _number_to_search_name = {
        1220: 'Comprar',
        1240: 'Arrendar',
    }

    _number_to_name = {
        1000: 'Inmuebles',
        1220: 'Vendo',
        1240: 'Arriendo',
        1260: 'Arriendo de temporada',
        2000: 'Veh�culos',
        2020: 'Autos, camionetas y 4x4',
        2040: 'Buses, camiones y furgones',
        2060: 'Motos',
        2080: 'Barcos, lanchas y aviones',
        2100: 'Accesorios y piezas para veh�culos',
        2120: 'Otros veh�culos',
        3000: 'Computadores & electr�nica',
        3020: 'Consolas, videojuegos y accesorios',
        3040: 'Computadores y accesorios',
        3060: 'Celulares, tel�fonos y accesorios',
        3080: 'Audio, TV, video y fotograf�a',
        4000: 'Moda, calzado, belleza y salud',
        4020: 'Moda y vestuario',
        4040: 'Bolsos, bisuter�a y accesorios',
        4060: 'Salud y belleza',
        4080: 'Calzado',
        5000: 'Hogar',
        5020: 'Muebles',
        5040: 'Electrodom�sticos',
        5060: 'Jard�n, herramientas y exteriores',
        5160: 'Otros art�culos del hogar',
        6000: 'Tiempo libre',
        6020: 'Deportes, gimnasia y accesorios',
        6060: 'Bicicletas, ciclismo y accesorios',
        6080: 'Instrumentos musicales y accesorios',
        6100: 'M�sica y pel�culas (DVDs, etc.)',
        6120: 'Libros y revistas',
        6140: 'Animales y sus accesorios',
        6160: 'Arte, antig�edades y colecciones',
        6180: 'Hobbies y outdoor',
        7000: 'Servicios, negocios y empleo',
        7020: 'Ofertas de empleo',
        7040: 'Busco empleo',
        7060: 'Servicios',
        7080: 'Negocios, maquinaria y construcci�n',
        8000: 'Otros',
        8020: 'Otros productos',
        9000: 'Futura mam�, beb�s y ni�os',
        9020: 'Vestuario futura mam� y ni�os',
        9040: 'Juguetes',
        9060: 'Coches y art�culos infantiles',
    }

    @staticmethod
    def get_name(cat, search_name=False):
        if search_name and int(cat) in Categories._number_to_search_name:
            return Categories._number_to_search_name[int(cat)]
        return Categories._number_to_name[int(cat)]

    @staticmethod
    def get_all():
        """ Returns a generator to iterate over all the categories """
        for key, value in Categories.__dict__.items():
            if key[0].isupper():
                yield (key, value)

    @classmethod
    def get_subcategories(cls, category = None):
        if category:
            return filter(lambda x: x[1]//1000 == category//1000, cls.get_subcategories())
        return filter(lambda x: x[1] % 1000, cls.get_all())

    @staticmethod
    def get_main_cat(cat_id):
        return (cat_id // 1000) * 1000

    @staticmethod
    def get_ad_params(cat_id):
        if cat_id in Categories._ad_params:
            return Categories._ad_params[cat_id]
        return Categories._ad_params[Categories.get_main_cat(cat_id)]


class UserAgents(object):
    IOS = 'Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X; en-us) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53'
    ANDROID = 'Mozilla/5.0 (Linux; U; Android 2.3.4; en-us; Nexus S Build/GRJ22) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1'
    NEXUS_5 = 'Mozilla/5.0 (Linux; Android 5.1.1; Nexus 5 Build/LMY48B) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.78 Mobile Safari/537.36'
    FIREFOX_MAC = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0'

class Products(object):
    """ Products ID definition  """
    BUMP = 1
    WEEKLY_BUMP = 2
    GALLERY = 3
    GALLERY_1 = 31
    GALLERY_30 = 32
    DAILY_BUMP = 8
    LABEL = 9
    STORE_MONTHLY = 4
    STORE_QUARTERLY = 5
    STORE_BIANNUAL = 6
    STORE_ANNUAL = 7
    AUTOFACT = 20

class Label_values(object):
    URGENT = 1
    OPPORTUNITY = 2
    LITTLE_USE = 3
    NEW = 4

class Label_texts(object):
    URGENT = 'Urgente'
    OPPORTUNITY = 'Oportunidad'
    LITTLE_USE = 'Poco uso'
    NEW = 'Nuevo'

class Upselling(object):
    """Upselling Products definition  """
    WEEKLY_BUMP = 101
    GALLERY = 102
    DAILY_BUMP = 103
    LABEL = 104

    @staticmethod
    def product_is_enabled(wd, product, ai_upselling):
        return not Upselling.product_is_disabled(wd, product, ai_upselling)

    @staticmethod
    def product_is_disabled(wd, product, ai_upselling):
        if product == Upselling.WEEKLY_BUMP:
            return Upselling.is_element_disabled(ai_upselling.upselling_products, selector='#weekly_box.is-bw')
        elif product == Upselling.DAILY_BUMP:
            return Upselling.is_element_disabled(ai_upselling.upselling_products, selector='#daily_box.is-bw')
        elif product == Upselling.GALLERY:
            return Upselling.is_element_disabled(ai_upselling.upselling_products, selector='#gallery_box.is-bw')

    @staticmethod
    def is_element_disabled(page, selector = None):
        try:
            page.find_element_by_css_selector(selector)
        except exceptions.NoSuchElementException:
            return False
        else:
            return True

class PacksPeriods(object):
    """ Pack periods definition  """
    MONTHLY =  1
    QUARTERLY = 2
    BIANNUAL = 3
    ANNUAL = 4

class PacksType(object):
    """ Pack types definition """
    CAR = 1
    REAL_ESTATE = 2

class Regions(object):

    def first_commune(region):
        regions_communes = [1, 5, 12, 21, 30, 45, 83, 116, 146, 200, 232, 244, 274, 284, 295]
        return regions_communes[region -1]

""" Begin Xiti Functions """
def enable_local_hit_xiti():
    """
    change the url of xiti server to localhost, and enable it
    """
    bconf_overwrite('*.*.xiti.document', conf.PHP_URL, False)
    bconf_overwrite('*.*.xiti.document_secure', conf.SSL_URL, False)
    bconf_overwrite('*.*.xiti.hit_server', '/', False)
    bconf_overwrite('*.*.common.stat_counter.xiti.display', '1', True)


def clear_hit_xiti_log(tag_name=None):
    """Delete the log file if it exists"""
    if tag_name is None:
        files = glob.glob(conf.REGRESS_FINAL + '/logs/hit*')
        for filepath in files:
            os.remove(filepath)
    else:
        filepath = conf.REGRESS_FINAL + "/logs/hit*" + tag_name + ".log"
        if os.path.exists(filepath):
            os.remove(filepath)

def find_string_in_file(file_path, string):
    """Find a string in a text file"""
    with open(file_path, 'r') as f:
        for line in f.readlines():
            if string in line:
                print('String encontrado: {0}'.format(string))
                return True
    return False


def check_xiti_page_tag_values(page_name, page_type, s2=None):
    """Check if the values of a xiti tag hit the local server"""
    file_path = conf.REGRESS_FINAL + "/logs/hit_xiti." + page_name.lower() + ".page.log"
    file_json_path = conf.REGRESS_FINAL + "/logs/hit_xiti." + page_name.lower() + ".json.log"
    wait_for_file(file_path, 10)
    wait_for_file(file_json_path, 10)
    if not os.path.exists(file_path):
        print('the file {} does not exists'.format(file_path))
        return False
    if not os.path.exists(file_json_path):
        print('the file {} does not exists'.format(file_json_path))
        return False
    if not (find_string_in_file(file_json_path, "'page_type' => '{}'".format(page_type))):
        print('the "page_type = {}" was not found:'.format(page_type))
        return False
    if s2 is not None and not (find_string_in_file(file_path, "'s2' => '{}'".format(s2))):
        print('the "s2 = {}" was not found:'.format(s2))
        return False
    return True
""" End Xiti Functions """


def parse_date(datestring):
    """
    Parses Weird spanish dates and return a datetime object
    """
    months = {
        'enero': 1,
        'febrero': 2,
        'marzo': 3,
        'abril': 4,
        'mayo': 5,
        'junio': 6,
        'julio': 7,
        'agosto': 8,
        'septiembre': 9,
        'octubre': 10,
        'noviembre': 11,
        'diciembre': 12,
    }
    parts = datestring.split(' ')
    day = int(parts[0])
    month = parts[1]
    month = int(months[month.lower()])
    year = int(parts[2])
    return datetime.date(year, month, day)


def execute_query(query):
    psql = '/usr/bin/psql -h {host}'.format(host=conf.REGRESS_PGSQL_HOST)
    db_psql = psql + " blocketdb -c "
    db_cmd = '''{psql} '{query}' '''.format(psql=db_psql, query=query)
    out = subprocess.check_output(db_cmd, shell=True)
    return str(out)


def db_query(query):
    conn = psycopg2.connect(database='blocketdb', host=yapo.conf.REGRESS_PGSQL_HOST)
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()
    return conn, cur


def db_fetch_one(query):
    conn, cur = db_query(query)
    return cur.fetchone()


def db_fetch_all(query):
    conn, cur = db_query(query)
    return cur.fetchall()


def get_date(dateFormat="%d/%m/%Y", addDays=0):
    timeNow = datetime.datetime.now()
    if (addDays!=0):
        anotherTime = timeNow + datetime.timedelta(days=addDays)
    else:
        anotherTime = timeNow
    return anotherTime.strftime(dateFormat)


def get_date_and_time():
    diasemana = {'MONDAY':'Lunes','TUESDAY':'Martes','WEDNESDAY':'Miercoles','THURSDAY':'Jueves','FRIDAY':'Viernes','SATURDAY':'Sabado','SUNDAY':'Domingo'}
    mes = {'JANUARY':'Enero','FEBRUARY':'Febrero','MARCH':'Marzo','APRIL':'Abril','MAY':'Mayo','JUNE':'Junio','JULY':'Julio','AUGUST':'Agosto','SEPTEMBER':'Septiembre','OCTOBER':'Octubre','NOVEMBER':'Noviembre','DECEMBER':'Diciembre'}
    dic = {}
    dic['fecha'] = datetime.date.today()
    dic['fechora'] = datetime.datetime.today()
    dic['hms'] = dic['fechora'].time().__str__().split('.')[0]
    dic['hm'] = dic['hms'].split(':')[0]+':'+dic['hms'].split(':')[1]
    dic['h'] = dic['hms'].split(':')[0]
    dic['dia'] = diasemana[dic['fecha'].strftime('%A').upper()]
    dic['mes'] = mes[dic['fecha'].strftime('%B').upper()]
    return dic


def format_price(price):
    if not price:
        return '$ 0'
    csp = format(price, ',')
    dsp = re.sub(',', '.', csp)
    return '$ {}'.format(dsp)


def make(command, path=''):
    out = subprocess.check_output("make -C {0} {1} 2> /dev/null".format(conf.TOPDIR+path, command), shell=True)
    return out


def download_and_get_md5(driver, url, filepath):
    if os.path.exists(filepath):
        os.remove(filepath)
    # download a pdf file
    driver.get(url)

    return get_downloaded_file_content(driver, filepath)

def get_downloaded_file_content(driver, filepath):
    wait_for_file(filepath, 10)

    # list all the remote files (waits for at least one)
    files = WebDriverWait(driver, 20, 1).until(get_file_names_chrome)

    # get the remote file with a GET request executed in the page
    content = get_file_content_chrome(driver, files[0])

    return get_md5sum(content)


def get_file_names_chrome(driver):
    if not driver.current_url.startswith("chrome://downloads"):
        driver.get("chrome://downloads/")
    return driver.execute_script("""
        return downloads.Manager.get().items_
          .filter(e => e.state === "COMPLETE")
          .map(e => e.file_url);
        """)

def get_file_content_chrome(driver, uri):
    result = driver.execute_async_script("""
    var uri = arguments[0];
    var callback = arguments[1];
    var toBase64 = function(buffer){for(var r,n=new Uint8Array(buffer),t=n.length,a=new Uint8Array(4*Math.ceil(t/3)),i=new Uint8Array(64),o=0,c=0;64>c;++c)i[c]="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charCodeAt(c);for(c=0;t-t%3>c;c+=3,o+=4)r=n[c]<<16|n[c+1]<<8|n[c+2],a[o]=i[r>>18],a[o+1]=i[r>>12&63],a[o+2]=i[r>>6&63],a[o+3]=i[63&r];return t%3===1?(r=n[t-1],a[o]=i[r>>2],a[o+1]=i[r<<4&63],a[o+2]=61,a[o+3]=61):t%3===2&&(r=(n[t-2]<<8)+n[t-1],a[o]=i[r>>10],a[o+1]=i[r>>4&63],a[o+2]=i[r<<2&63],a[o+3]=61),new TextDecoder("ascii").decode(a)};
    var xhr = new XMLHttpRequest();
    xhr.responseType = 'arraybuffer';
    xhr.onload = function(){ callback(toBase64(xhr.response)) };
    xhr.onerror = function(e){ callback(xhr) };
    xhr.open('GET', uri);
    try {
      xhr.send();
    } catch (e) {
      callback(xhr)
    }
    """, uri)
    if type(result) == int :
        raise Exception("Request failed with status %s" % result)
    return base64.b64decode(result)


class CSVTool():

    def csv_for_tests(ad_base, ad_test):
        reader = csv.reader(open(ad_base, newline='', encoding='utf-8'))
        checker = csv.reader(open(ad_test, newline='', encoding='utf-8'))
        lines = sum(1 for row in checker)
        if lines < 2:
            writer = csv.writer(open(ad_test, 'at'))
        else:
            writer = csv.writer(open(ad_test, 'wt'))
        return {'base':reader, 'test_data':writer}

    def read_csv_line(csv_file, line):
        data = None
        reader = csv.reader(open(csv_file, newline='', encoding='utf-8'))
        for i, row in enumerate(reader):
                if i == line:
                    data = row
                    break
        return data

class JsonTool():

    @staticmethod
    def get_test_file():
        return {'regress':os.path.join(conf.RESOURCES_DIR, 'ad_data/regress_data.json'),
        'stg':os.path.join(conf.RESOURCES_DIR, 'ad_data/stg_data.json'),
        'pro':os.path.join(conf.RESOURCES_DIR, 'ad_data/pro_data.json')
        }[conf.ENVIRONMENT]

    @staticmethod
    def read_json_file(path):
        json_file = open(path,"r")
        data = json.load(json_file)
        json_file.close

        return data

    def save_test_data(data):
        json_file = open(os.path.join(conf.RESOURCES_DIR, 'ad_data/test_data.json'),"r")
        file_data = json.load(json_file)
        json_file.close

        file_data.update(data)

        json_file = open(os.path.join(conf.RESOURCES_DIR, 'ad_data/test_data.json'), "w+")
        json_file.write(json.dumps(file_data))
        json_file.close()

    def read_test_data(test_data_key):
        file_data = JsonTool.read_json_file(JsonTool.get_test_file())

        return file_data[test_data_key]

    def read_tests_emails(test_data_keys):
        emails = []
        file_data = JsonTool.read_json_file(os.path.join(conf.RESOURCES_DIR, 'ad_data/test_data.json'))
        for test_name in test_data_keys:
            if file_data.get(test_name) == None:
                print("No data detected for: {0}".format(test_name))
            else:
                test_data = file_data[test_name]
                emails.append(test_data['email'])

        return emails

    def read_previous_data(test_data_key, previous_test_name):
        file_data = JsonTool.read_json_file(JsonTool.get_test_file())
        if not is_not_regress():
            return file_data[test_data_key]
        json_file = open(os.path.join(conf.RESOURCES_DIR, 'ad_data/test_data.json'),"r")
        previousData = json.load(json_file)
        json_file.close

        file_data[test_data_key].update(previousData[previous_test_name])

        return file_data[test_data_key]

    def read_data_of_test(previous_test_name):
        json_file = open(os.path.join(conf.RESOURCES_DIR, 'ad_data/test_data.json'),"r")
        previousData = json.load(json_file)
        json_file.close

        return previousData[previous_test_name]

    @staticmethod
    def removekey(dic, key):
        r = dict(dic)
        try:
            del r[key]
        finally:
            return r
