from nose_selenium import ScreenshotOnExceptionWebDriverWait
from selenium.common.exceptions import NoSuchElementException
import selenium.webdriver.support.expected_conditions as EC
import nose_selenium


class PageWait(ScreenshotOnExceptionWebDriverWait):
    """Wait class to be used inside page objects

    The 'until*' calls accept the page object's element name and retrieve the
    selector internally"""

    def __init__(self, page, timeout=None):
        self._page = page
        timeout = timeout or nose_selenium.TIMEOUT
        super(PageWait, self).__init__(page._driver, timeout)

    def _sel(self, element):
        """Retrieves the selector for the element"""
        return self._page.selector(element)

    def until_loaded(self, message="Page didn't fully load"):
        """Wait until page is fully loaded (document.readyState = complete)"""
        return self.until(lambda d: d.execute_script('return document.readyState') == 'complete', message)

    def until_loaded_jquery_ajax(self, message="Page didn't fully load"):
        """Wait until page is fully loaded (document.readyState = complete)"""
        return self.until(lambda wd: wd.execute_script("return jQuery.active == 0"), message)

    def until_present(self, element, message=''):
        """Waits until the element is present in the page"""
        condition = EC.presence_of_element_located(self._sel(element))
        return self.until(condition, message)

    def until_visible(self, element, message=''):
        """Waits until the element is visible in the page"""
        condition = EC.visibility_of_element_located(self._sel(element))
        return self.until(condition, message)

    def until_not_visible(self, element, message=''):
        """Waits until the element is not visible or s not present in the page"""
        condition = EC.invisibility_of_element_located(self._sel(element))
        return self.until(condition, message)

    def until_visibility_of_element(self, element, message=''):
        """Waits until the element is visible in the page"""
        condition = EC.visibility_of(element)
        return self.until(condition, message)

    def until_not_present(self, element, message=''):
        self.until_not_visible(element, message)

    def until_text_present(self, text, element, message=None):
        if not message:
            message = 'Expected text "{0}" did not appear in element'.format(text)

        def present_and_has_text(wd):
            try:
                return text in element.text
            except NoSuchElementException:
                pass
        return self.until(present_and_has_text)

    def until_value_present(self, val, element, message=''):
        condition = EC.text_to_be_present_in_element_value(self._sel(element), val)
        return self.until(condition, message)

    def until_elements_equals(self, wd, selector, expected_len, message="Elements quantity isn't the expected"):
        """Wait until the expected quantity of elements is loaded"""
        condition = lambda x: len(wd.find_elements_by_css_selector(selector)) == expected_len
        return self.until(condition, message)

    def until_attr_equals(self, element, attr, value, message = ''):
        webelement = getattr(self._page, element)
        condition = lambda x: webelement.get_attribute(attr) == str(value)
        message = 'Waiting for attr [{}] of [{}] to be [{}]'.format(attr, element, value)
        return self.until(condition, message)

    def until_session_cookie(self):
        """ Waits until the session cookie is present
            will fail when you try to test a failed login """

        wd = self._driver
        def check_cookie(x):
            cookies = wd.get_cookies()
            for cookie in cookies:
                if cookie['name'] == 'acc_session':
                    return True
            return False

        return self.until(check_cookie)


    def until_not_session_cookie(self):
        """ Waits until the session cookie is not present
            will fail when you try to test a failed login """

        wd = self._driver
        def check_cookie(x):
            cookies = wd.get_cookies()
            for cookie in cookies:
                if cookie['name'] == 'acc_session':
                    return False
            return True

        return self.until(check_cookie)

    def until_element_clickable(self, element, message=''):
        condition = EC.element_to_be_clickable(self._sel(element))
        return self.until(condition, message)
