# -*- coding: latin-1 -*-
from yapo.pages import BasePage
from yapo.pages import HotizontalSelect
import sunbro, os
from yapo import conf, utils


class Login(BasePage):

    """Login page"""
    email_input = sunbro.FindByID('email_input')
    password_input = sunbro.FindByID('password_input')
    submit_btn = sunbro.FindByID('submit_button')

    err_email_input = sunbro.FindByID('err_email_input')
    err_form = sunbro.FindByID('err_form')

    def go(self):
        self._driver.get(conf.SSL_URL + '/login')

class ViewAccount(BasePage):
    profile_image = sunbro.FindByCSS('.editAccountUser .profile-image')
    edit_profile_btn = sunbro.FindByID('edit_profile_btn')

    def go(self):
        self._driver.get(conf.SSL_URL + '/cuenta/view')

    def get_account_params(self):
        params = []
        for row_webelement in self._driver.find_elements_by_class_name('profileInfo-row'):
            param_name = row_webelement.find_element_by_class_name('rowLeft')
            param_value = row_webelement.find_element_by_class_name('rowRight')
            params.append((param_name.text, param_value.text))
        return params


class Create(BasePage):

    """Create account page"""
    title = sunbro.FindByCSS('h2')
    title_account_success = sunbro.FindByCSS('.title-account-success')
    pro_radio = sunbro.FindByID('is_company_c')
    person_radio = sunbro.FindByID('is_company_p')
    name = sunbro.FindByID('account_name')
    rut = sunbro.FindByID('account_rut')
    phone_prefix = sunbro.FindByCSS('.phoneInput-countryCode')
    phone_type_mobile = sunbro.FindByCSS('input[name=phone_type][value=m]')
    phone_type_fixed = sunbro.FindByID('phoneInputTypeHome')
    area_code = sunbro.FindByCSS(".phoneInput-areaCode")
    phone = sunbro.FindByCSS(".phoneInput-number")
    region = sunbro.FindByID('account_region')
    commune = sunbro.FindByID('account_commune')
    email = sunbro.FindByID('account_email')
    password = sunbro.FindByID('account_password')
    password_verify = sunbro.FindByID('account_password_verify')
    accept_conditions = sunbro.FindByID('accept_conditions')
    submit_btn = sunbro.FindByCSS('.btnUpdate-account')
    radio_helpers = sunbro.FindAllByCSS(
        '.create-account-form .iradio_minimal-blue')
    check_helpers = sunbro.FindAllByCSS(
        '.create-account-form .icheckbox_minimal-blue')
    label_accept_conditions = sunbro.FindByID("label_accept_conditions")
    label_is_company_p = sunbro.FindByID('label_is_company_p')
    label_is_company_c = sunbro.FindByID('label_is_company_c')
    error_dupl = sunbro.FindByID('err_creation_status')
    account_region = sunbro.FindByID('account_region')
    gender_male = sunbro.FindByID('input_gender_male')
    gender_female = sunbro.FindByID('input_gender_female')
    account_login_form = sunbro.FindByID('account_login_form')

    errors = sunbro.FindAllByCSS('.validation_msg.error')
    err_name = sunbro.FindByID('err_name')
    err_area_code = sunbro.FindByCSS('.phoneInput-errorMessage')
    err_phone = sunbro.FindByCSS('.phoneInput-errorMessage')
    err_password = sunbro.FindByID('err_password')
    err_password_verify = sunbro.FindByID('err_password_verify')
    err_rut = sunbro.FindByID('err_rut')

    def fill_form(self, **kwargs):
        for param in ['person_radio', 'pro_radio', 'accept_conditions', 'gender_male', 'gender_female']:
            if param in kwargs:
                super(Create, self).fill(param, kwargs.pop(param))

        for param in ['region', 'commune']:
            if param in kwargs:
                if param == 'commune':
                    self.wait.until_attr_equals(
                        'commune', 'data-region', self.val('region'))
                super(Create, self).fill(param, kwargs.pop(param))

        if 'phone_type_mobile' in kwargs:
            self.phone_type_mobile.click()
            kwargs.pop('phone_type_mobile')
        if 'phone_type_fixed' in kwargs:
            self.phone_type_fixed.click()
            kwargs.pop('phone_type_fixed')

        if 'area_code' in kwargs:
            params_order = ['area_code', 'phone']
            data = [(key, kwargs.pop(key)) for key in params_order if key in kwargs]
            self.ordered_fill_form(data)

        super(Create, self).fill_form(**kwargs)

    def go(self):
        self._driver.get(conf.SSL_URL + '/cuenta')

    def am_i_here(self):
        return self.is_element_present('account_login_form')


class Edit(Create):

    """Edit account page:
       Inherits the parameters from Create
       Add only params for edit or add exclusive params for Edit
    """
    address = sunbro.FindByID('account_address')
    submit_btn = sunbro.FindByCSS('.btnUpdate-account')
    change_password = sunbro.FindByClass('change-password')
    success_message = sunbro.FindByCSS('.title-message.success-message')
    edit_profile_btn = sunbro.FindByID('edit_profile_btn')
    unsubscribe_ocp = sunbro.FindByID('unsubscribe')
    notification_success = sunbro.FindByCSS('div.informativeNotification.__success h1.informativeNotification-title')
    notification_success_subtitle = sunbro.FindByCSS('div.informativeNotification.__success h2.informativeNotification-subtitle')

    birth_day = sunbro.FindByCSS('select[name="birth_day"]')
    birth_month = sunbro.FindByCSS('select[name="birth_month"]')
    birth_year = sunbro.FindByCSS('select[name="birth_year"]')
    err_birthdate = sunbro.FindByID('err_birthdate')
    name = sunbro.FindByID('account_name')
    phone = sunbro.FindByID('phone')

    def go(self):
        self._driver.get(conf.SSL_URL + '/cuenta/edit')

    def send(self, **data):
        self.fill_form(**data)
        self.submit_btn.click()
        self.wait.until_visible('success_message')


class CreateMobile(Create):

    """ Create account page for mobile:
        Use the params of Create desktop
        Overwrite the params with diferent selector
    """
    submit_btn = sunbro.FindByCSS('#confirm_btn')
    form = sunbro.FindByID('account_create_form')

    gender_male = sunbro.FindByID('male')
    gender_female = sunbro.FindByID('fem')

    errors = sunbro.FindAllByCSS('.error')
    err_name = sunbro.FindByID('account_name-error')
    err_password = sunbro.FindByID('account_password-error')
    err_password_verify = sunbro.FindByID('account_password_verify-error')
    err_rut = sunbro.FindByCSS('#account_rut-error, #err_rut')

    def __init__(self, *args, **kwargs):
        super(CreateMobile, self).__init__(*args, **kwargs)
        self.hotizontal_select = HotizontalSelect(
            self._driver, form_selector='#account_create_form')

    def fill_form(self, **kwargs):
        if 'account_region' in kwargs or 'region' in kwargs:
            account_region = kwargs.pop('account_region', None)
            region = kwargs.pop('region', None)
            kwargs['account_region'] = region if region else account_region
            self.hotizontal_select.fill_sidenav(
                kwargs, trigger='account_region')

        if 'account_commune' in kwargs or 'commune' in kwargs:
            account_commune = kwargs.pop('account_commune', None)
            commune = kwargs.pop('commune', None)
            kwargs['account_commune'] = commune if commune else account_commune
            self.hotizontal_select.fill_sidenav(
                kwargs, trigger='account_commune')

        super(CreateMobile, self).fill_form(**kwargs)


class EditMobile(CreateMobile):

    """Edit account mobile page:
       Inherits the parameters from CreateMobile
       Add only params for edit or add exclusive params for EditMobile
    """
    address = sunbro.FindByID('account_address')
    change_password = sunbro.FindByClass('change-password')
    success_message = sunbro.FindByCSS('.dashboardHeader-txt')
    edit_profile_btn = sunbro.FindByID('edit_profile_btn')
    unsubscribe_ocp = sunbro.FindByID('unsubscribe')
    notification_success = sunbro.FindByCSS('div.informativeNotification.__success h1.informativeNotification-title')
    notification_success_subtitle = sunbro.FindByCSS('div.informativeNotification.__success h2.informativeNotification-subtitle')

    birth_day = sunbro.FindByCSS('select[name="birth_day"]')
    birth_month = sunbro.FindByCSS('select[name="birth_month"]')
    birth_year = sunbro.FindByCSS('select[name="birth_year"]')
    err_birthdate = sunbro.FindByID('err_birthdate')

    def go(self):
        print("Cuenta edit mobile")
        self._driver.get(conf.SSL_URL + '/cuenta/edit')


class CreationSuccessDesktop(BasePage):

    """Account creation success page desktop"""
    title = sunbro.FindByCSS('.title-account-success')
    message = sunbro.FindByCSS('p.success-message')


class CreationSuccessMobile(BasePage):

    """Account creation success page mobile"""
    title = sunbro.FindByCSS('h1.title.center')
    message = sunbro.FindByCSS('p.success-message')


class DashBoardFirstTime(BasePage):

    """Account dashboard page"""
    walkthrough = sunbro.FindByID('wlkthr-new')
    close_walkthrough = sunbro.FindByID('walkthrough-close')


class Confirm(BasePage):

    """Confirm page"""
    title = sunbro.FindByCSS('.title-account')
    error_message = sunbro.FindByCSS('.title-message.error-message')


class ForgotPassword(BasePage):

    """Page that asks the user for the email to send the reset password link"""
    email_input = sunbro.FindByID('email')
    passwd_input = sunbro.FindByID('password')
    passwdv_input = sunbro.FindByID('password_verification')
    submit_btn = sunbro.FindByCSS('.btn.btn-primary')
    message = sunbro.FindByCSS('.text-legend h3')
    title = sunbro.FindByCSS('h2.title-account')
    error_msg = sunbro.FindByID('err_status')
    mail_sent_text = sunbro.FindByCSS('.control-group h3')

    # MESSAGES
    mail_sent_msg = 'Hemos enviado un correo con un link que te permitir� cambiar tu contrase�a!'
    error_email_empty = 'Escribe tu e-mail'
    error_bad_email = 'Confirma que el email est� en el formato correcto'
    error_bad_email_too_short = 'Confirma que el correo electr�nico est� en el formato correcto.'
    error_passwd_too_short = 'Tu contrase�a debe tener un m�nimo de 8 caracteres'
    error_passwd_different = 'Las contrase�as no coinciden. Por favor int�ntalo de nuevo'
    confirmation_login_msg = 'Entrar a mi cuenta'
    confirmation_publish_msg = 'Volver a publicar'
    error_msg_expired = ':S Est�s intentando recuperar desde una cuenta que no existe o no est� activa'
    msg_success = ':) Tu contrase�a ha sido cambiada con exito!'

    def go(self):
        self._driver.get(conf.SSL_URL + "/reset_password")

    def go_email(self, email, op, setredis = True):
        if setredis:
            utils.redis_command(conf.REGRESS_REDISSESSION_PORT, 'set', 'rsd1x_forgot_password_{0}'.format(email), 'test')
        self._driver.get(conf.SSL_URL + "/reset_password/ask_password/?h=test&op={0}&email={1}".format(op, email))

    def send(self, email):
        self.email_input.send_keys(email)
        self.submit_btn.click()

    def send_passwd(self, passwd, passwdv):
        self.passwd_input.send_keys(passwd)
        self.passwdv_input.send_keys(passwdv)
        self.submit_btn.click()

class ResetPassword(BasePage):

    """Page wih the reset passowrd form"""
    password_input = sunbro.FindByID("password")
    password_verification_input = sunbro.FindByID("password_verification")
    submit_btn = sunbro.FindByCSS('.btn.btn-primary')

    def send(self, email):
        self.fill_form(password_input=email, password_verification_input=email)
        self.submit_btn.click()
