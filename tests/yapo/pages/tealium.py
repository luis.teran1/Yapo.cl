# -*- coding: latin-1 -*-
from yapo import pages, conf, utils
import json, os

class Tealium(pages.BasePage):

    def utag_data(self, event_name, type='page'):
        if type not in ['page', 'click']:
            raise Exception('type parameter of utag_data should be "page" or "click"')

        self.wait.until_loaded()
        return self.get_tealium_logs(event_name, type)

    def get_tealium_logs(self, event_name, type='page'):
        """
        Get a dictionary with the tealium log for a page tag.
        """
        data = {}
        json_file_path = conf.TOPDIR + "/regress_final/logs/hit_tealium." + event_name.lower() + ".{}.json.log".format(type)
        utils.wait_for_file(json_file_path, 10)

        if os.path.exists(json_file_path):
            with open(json_file_path) as data_file:
                data = json.load(data_file)
        else:
            print('the file {} does not exists'.format(json_file_path))

        return data

class TealiumEventNames(object):
    INSERT_AD_SHOW = 'insert_ad_show'
    INSERT_AD_PREVIEW = 'insert_ad_preview'
    INSERT_AD_ERROR = 'insert_ad_error'
    INSERT_AD_SUBMIT = 'insert_ad_submit'
    AD_EDIT_SUBMIT = 'ad_edit_submit'
    AD_EDIT = 'ad_edit'
    AD_EDIT_PREVIEW  = 'ad_edit_preview'
    AD_EDIT_ERROR = 'ad_edit_error'
    AD_EDIT_PASSWORD = 'ad_edit_password'
    AD_EDIT_PASSWORD_ERROR = 'ad_edit_password_error'
    DELETE_AD_SHOW = 'delete_ad_show'
    DELETE_AD_ERROR = 'delete_ad_error'
    DELETE_AD_CONFIRMATION = 'delete_ad_confirmation'
    LIST = 'list'
    MY_SAVED_ADS = 'my_saved_ads'
    MY_SAVED_SEARCHES = 'my_saved_searches'
    EMPTY_FAVORITES = 'empty_favorites'
    STORES_SEARCH_PAGES = 'stores_search_pages'
    STORE_SINGLE_PAGE = 'store_single_page'
    STORE_404 = 'store_404'
    MY_INFORMATION = 'my_information'
    REGISTER_PAGE_STEP1 = 'register_page_step1'
    REGISTER_PAGE_FORM_ERROR = 'register_page_form_error'
    REGISTER_PAGE_ACCOUNT = 'register_page_account'
    ACCOUNTS_VERIFIED = 'accounts_verified'
    RESET_PASSWORD = 'reset_password'
    RESET_PASSWORD_SENT_SUCCESS = 'reset_password_sent_success'
    RESET_PASSWORD_NEW = 'reset_password_new'
    RESET_PASSWORD_CONFIRM = 'reset_password_confirm'
    AD_NOT_FOUND_404 = 'ad_not_found_404'
    AD_VIEW = 'ad_view'
    HOME_PAGE = 'home_page'

    FREQUENT_QUESTIONS = 'frequent_questions'
    SEARCH_ADVICES = 'search_advices'
    BUYING_SAFELY = 'buying_safely'
    DELIVERY_OF_PRODUCTS = 'delivery_of_products'
    STOLEN_ITEMS = 'stolen_items'
    VEHICLES = 'vehicles'
    BRANDED_ITEMS_AND_SOFTWARE = 'branded_items_and_software'
    SENDING_MONEY_OR_PRODUCTS = 'sending_money_or_products'
    MONEY_TRANSFERS = 'money_transfers'
    PREVENTION = 'prevention'
    REPORT_AN_ANNOUNCER = 'report_an_announcer'
    WHO_ARE_WE = 'who_are_we'
    RULES_FOR_ADS = 'rules_for_ads'
    TERMS_AND_CONDITIONS = 'terms_and_conditions'

    MY_ADS = 'my_ads'
    MY_ACCOUNT = 'my_account'
    PURCHASE_CONFIRMATION = 'purchase_confirmation'
    STORE_MANAGE_PAGE = 'store_manage_page'
    PACK_INFORMATION_PAGE = 'pack_information_page'
    CAR_PACK_MANAGE = 'car_pack_manage'
    SUMMARY_PAGE = 'summary_page'
    BUMP_IN_EDIT = 'bump_in_edit'
    TECHNICAL_ERROR = 'technical_error'
    ERROR_404 = 'error_404'
    STORE_INFO_PAGE = 'store_info_page'
    PAYMENT_FAIL = 'payment_fail'
    BUMP_FORM = 'bump_form'

class TealiumGlobalVariables(object):
    EVENT_NAME = 'event_name'
    ENVIRONMENT = 'environment'
    LANGUAGE = 'language'
    LANGUAGE_ID = 'language_id'
    USER_ID = 'user_id'
    USER_ROLE_ID = 'user_role_id'
    USER_ROLE = 'user_role'
    REGION_LEVEL1 = 'region_level1'
    REGION_LEVEL2 = 'region_level2'
    REGION_LEVEL3 = 'region_level3'
    REGION_LEVEL1_ID = 'region_level1_id'
    REGION_LEVEL2_ID = 'region_level2_id'
    REGION_LEVEL3_ID = 'region_level3_id'
    USER_REGION_LEVEL1 = 'user_region_level1'
    USER_REGION_LEVEL2 = 'user_region_level2'
    USER_REGION_LEVEL3 = 'user_region_level3'
    USER_REGION_LEVEL1_ID = 'user_region_level1_id'
    USER_REGION_LEVEL2_ID = 'user_region_level2_id'
    USER_REGION_LEVEL3_ID = 'user_region_level3_id'
    USER_EMAIL = 'user_email'

class TealiumCategoriesVariables(object):
    CATEGORY_LEVEL1 = 'category_level1'
    CATEGORY_LEVEL2 = 'category_level2'
    CATEGORY_LEVEL3 = 'category_level3'
    CATEGORY_LEVEL1_ID = 'category_level1_id'
    CATEGORY_LEVEL2_ID = 'category_level2_id'
    CATEGORY_LEVEL3_ID = 'category_level3_id'

class TealiumPacksVariables(object):
    PACK_SLOTS = 'pack_slots'
    PACKS = 'packs'
    USED_SLOTS = 'used_slots'

class TealiumPurshaseVariables(object):
    OPERATION_TYPE = 'operation_type'
    PAYMENT_TYPE = 'payment_type'
    INSTALLMENTS_TYPE = 'installments_type'
    INSTALLMENTS_NUMBER = 'installments_number'
    PAYMENT_MESSAGE_ERROR = 'payment_message_error'
    PAYMENT_TITLE_ERROR = 'payment_title_error'
    PURCHASE = 'purchase'
    PRICE = 'price'

class TealiumStoreVariables(object):
    STORE_ADDRESS_NUMBER = 'store_address_number'
    STORE_NAME = 'store_name'
    STORE_WEBSITE = 'store_website'
    STORE_URL = 'store_url'
    STORE_PHONE = 'store_phone'
    STORE_ADDRESS = 'store_address'
    STORE_ACTIVE = 'store_active'
    STORE_INFO_TEXT = 'store_info_text'
    STORE_EXPIRATION_DATE = 'store_expiration_date'
    STORE_PURCHASE_DATE  = 'store_purchase_date'

class TealiumMapsVariables(object):
    GEOPOSITION = 'geoposition'
    GEOPOSITION_IS_PRECISE = 'geoposition_is_precise'
    ADDRESS = 'address'
    ADDRESS_NUMBER = 'address_number'

class TealiumAdVariables(TealiumCategoriesVariables, TealiumMapsVariables):
    AD_TYPE = 'ad_type'
    AD_TITLE = 'ad_title'
    AD_ID = 'ad_id'
    GENDER = 'gender'
    CURRENCY = 'currency'
    PRICE = 'price'
    PUBLISH_DATE = 'publish_date'
    BRAND = 'brand'
    MODEL = 'model'
    VERSION = 'version'
    CARTYPE = 'cartype'
    YEAR = 'year'
    NUM_PICTURES = 'num_pictures'
    FUEL = 'fuel'
    TRANSMISSION = 'transmission'
    KM_UNITS = 'km_units'
    KM = 'km'
    ROOMS = 'rooms'
    SURFACE_UNITS = 'surface_units'
    SURFACE = 'surface'
    DESCRIPTION = 'description'
    CONDITION = 'condition'
    MEMORY = 'memory'
    PARKING = 'parking'
    CONDOMINIO = 'condominio'
    JOB_CATEGORY = 'job_category'
    SERVICE_CATEGORY = 'service_category'
    CUBICCMS = 'cubiccms'
    CUBICCMS_UNITS = 'cubiccms_units'

class TealiumAdReplyVariables(TealiumAdVariables):
    VIEW_TYPE_ID = 'view_type_id'

class TealiumListingVariables(TealiumCategoriesVariables):
    AD_TYPE = 'ad_type'
    SEARCH_TERMS = 'search_terms'
    GENDER = 'gender'
    CURRENCY = 'currency'
    PRICE_MIN = 'price_min'
    PRICE_MAX = 'price_max'
    BRAND = 'brand'
    MODEL = 'model'
    YEAR_MIN = 'year_min'
    YEAR_MAX = 'year_max'
    FUEL = 'fuel'
    TRANSMISSION = 'transmission'
    KM_UNITS = 'km_units'
    KM_MIN = 'km_min'
    KM_MAX = 'km_max'
    ROOMS_MIN = 'rooms_min'
    ROOMS_MAX = 'rooms_max'
    SURFACE_UNITS = 'surface_units'
    SURFACE_MIN = 'surface_min'
    SURFACE_MAX = 'surface_max'
    CONDITION = 'condition'
    PARKING = 'parking'
    CONDOMINIO = 'condominio'
    JOB_CATEGORY = 'job_category'
    SERVICE_CATEGORY = 'service_category'
    CUBICCMS = 'cubiccms'
    CUBICCMS_UNITS = 'cubiccms_units'
    CUBICCMS_MIN = 'cubiccms_min'
    CUBICCMS_MAX = 'cubiccms_max'
    CARTYPE = 'cartype'

class TealiumAiVariables(TealiumAdVariables):
    ERROR_MSG_DESCRIPTION = 'error_msg_description'
    ERROR_MSG_CATEGORY = 'error_msg_category'
    ERROR_MSG_EMAIL = 'error_msg_email'
    ERROR_MSG_EMAIL_REPEAT = 'error_msg_email_repeat'
    ERROR_MSG_PASSWORD = 'error_msg_password'
    ERROR_MSG_PASSWORD_REPEAT = 'error_msg_password_repeat'
    ERROR_MSG_PHONE = 'error_msg_phone'
    ERROR_MSG_REGION = 'error_msg_region'
    ERROR_MSG_TITLE = 'error_msg_title'
    ERROR_MSG_TNC = 'error_msg_tnc'

    ACTION_TYPE = 'action_type'
    REASON = 'reason'
