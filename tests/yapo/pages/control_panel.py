# -*- coding: latin-1 -*-
from lazy import lazy
from selenium.common.exceptions import NoSuchElementException, NoAlertPresentException
from selenium.webdriver.common.keys import Keys
# available since 2.4.0
from selenium.webdriver.support.ui import WebDriverWait
# available since 2.26.0
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from yapo import conf, utils
from yapo.pages import BasePage
from lazy import lazy
import sunbro
from yapo import ImageOrderException
from yapo.component import AttrFinder
from yapo.component import Component
import time
import os


class ControlPanel(BasePage):

    """
    Page object for the f*cking control panel page de los cojones
    """

    # Some definitions

    # Login page
    username = sunbro.FindByName('username')

    # Search for ad section
    radio_button_last_all = sunbro.FindByID('timespan_all')
    radio_button_last_24h = sunbro.FindByID('timespan_day')
    radio_button_last_7d = sunbro.FindByID('timespan_week')
    archive_options = sunbro.FindByName('archive_group')
    tabarea = sunbro.FindByID('tabarea')
    delete_selected = sunbro.FindByID('delete_selected')
    products_to_apply = sunbro.FindByName('products_to_apply')

    # Stores
    link_crear_tienda = sunbro.FindByPartialLinkText('Crear Tienda')
    link_editar_tienda = sunbro.FindByPartialLinkText('Editar Tienda')
    link_extender_periodo = sunbro.FindByPartialLinkText('Extender Per�odo')

    # televentas
    link_vender_productos = sunbro.FindByPartialLinkText('Vender Productos')
    link_editar_cuenta = sunbro.FindByPartialLinkText('Editar Cuenta')

    # users management
    link_show_users = sunbro.FindByPartialLinkText('Show users')

    # Checkboxes edit user televentas permissions
    checkbox_televentas = sunbro.FindByCSS('[name="main_privs[telesales]"]')
    checkbox_televentas_editar_cuenta = sunbro.FindByCSS(
        '[name="sub_privs[telesales][edit_account]"]')
    checkbox_televentas_vender_productos = sunbro.FindByCSS(
        '[name="sub_privs[telesales][sell_products]"]')

    # Buttons edit user
    edit_user = sunbro.FindByClass('edit-user')
    bump_privilege = sunbro.FindByName('sub_privs[Services][admin]')
    button_save = sunbro.FindByCSS('input[type="submit"]:nth-child(1)')
    button_cancel = sunbro.FindByCSS('input[type="submit"]:nth-child(2)')

    # Cpanel telesales edit account form
    radio_is_company_p = sunbro.FindByID('is_company_p')
    radio_is_company_c = sunbro.FindByID('is_company_c')
    text_field_name = sunbro.FindByID('account_name')
    text_field_rut = sunbro.FindByID('account_rut')
    text_field_phone = sunbro.FindByID('account_phone')
    text_field_contact = sunbro.FindByID('account_contact')
    text_field_lob = sunbro.FindByID('account_lob')
    select_region = sunbro.FindByID('account_region')
    select_commune = sunbro.FindByID('account_commune')
    text_field_address = sunbro.FindByID('account_address')
    button_update_data = sunbro.FindByCSS('.btn.btn-primary')
    label_success_edition = sunbro.FindByID('success-message')
    label_person_cell = sunbro.FindByID('label_person_cell')
    label_business_name_cell = sunbro.FindByID('label_business_name_cell')
    label_rut_cell = sunbro.FindByID('label_rut_cell')

    # Edit account search by email
    text_field_email = sunbro.FindByID('telesales-email')
    button_search = sunbro.FindByCSS('input[type="submit"]')

    # Sell products form
    text_field_query = sunbro.FindByID('ads_query')
    button_search_query = sunbro.FindByID('ads_query_button')
    checkbox_persist_checked = sunbro.FindByID('persist_checked')
    table_rows_data_grid = sunbro.FindAllByCSS(
        '.data-grid .table > tbody > tr')
    table_ad_history_rows = sunbro.FindAllByCSS('.HistoryPopup > tbody > tr')
    table_on_search = sunbro.FindByID('tbl_0')

    stats_bar_remaining_ads = sunbro.FindByCSS('.ads-num.db-txt')
    stats_bar_last_hour = sunbro.FindByID('last-hour')
    stats_bar_hourly_current = sunbro.FindByCSS('.hourly.current')
    stats_bar_hourly_last = sunbro.FindByCSS('.hourly.last')

    # Elements On Adreview from search ad
    radio_review_pri_pro = sunbro.FindAllByName("company_ad")
    review = sunbro.FindByPartialLinkText("Review")
    Unreviewed = sunbro.FindByPartialLinkText("Unreviewed")
    ad_wrapper = sunbro.FindAllByCSS("AdWrapper")
    ad_info = sunbro.FindByCSS('.vi-info')
    ad_title = sunbro.FindByCSS('.da_title')
    price = sunbro.FindByCSS('input[name=price]')
    working_day = sunbro.FindAllByCSS('.working_day')
    contract_type = sunbro.FindAllByCSS('.contract_type')
    list_id = sunbro.FindAllByCSS('.list_id')
    old_texts = sunbro.FindByCSS('.TextDiffOld')
    new_texts = sunbro.FindByCSS('.TextDiffNew')

    # list of tables of ads in search_for_ad of Unreviewed
    list_table_ads_unreviewed = sunbro.FindAllByCSS("#tbl_0 tr")
    list_table_ads = sunbro.FindAllByCSS(".module_content .Listing tr")

    # list in queues
    queue_lists_group = sunbro.FindAllByCSS(".module_content ul")
    queues_button = sunbro.FindAllByCSS(".nohistory a")
    queue_lists = sunbro.FindAllByCSS(".list")

    # Elements to notes verification
    create_note = sunbro.FindByPartialLinkText('Create ad note')
    ad_note_text = sunbro.FindByCSS('.Notice.normal .NoticeText')
    note_text = sunbro.FindByName('body_ad')

    # Maps elements in ad review
    region = sunbro.FindByClass('region')
    communes = sunbro.FindByClass('communes')
    address = sunbro.FindByClass('address')

    on_review_header = sunbro.FindByCSS('.GreyOutlineHeader')
    on_review_header_upselling = sunbro.FindByCSS(
        '.GreyOutlineHeader .Upselling')
    on_review_header_inserting_fee = sunbro.FindByCSS(
        '.GreyOutlineHeader .InsertingFee')

    # Duplicated elements in ad review
    list_duplicated_ads = sunbro.FindAllByCSS('.ad_list_ad.duplicated')
    # Deactivated elements in ad review
    list_deactivated_ads = sunbro.FindAllByCSS('.ad_list_ad.deactivated')

    # Published by the user in ad review
    list_published_ads = sunbro.FindAllByCSS('.ad_list_ad.published')

    # Confirm an unpaid ad
    link_confirm = sunbro.FindAllByCSS('.link_verify')

    # Ad's images
    ad_image = sunbro.FindByCSS('.ad_pict img')
    ad_thumbnails = sunbro.FindAllByCSS('.ad-image img')

    ad_type = sunbro.FindByCSS('[name=type]')
    real_estate_capacity = sunbro.FindByCSS(
        '.vi-info > li:nth-child(5) span strong')
    real_estate_services = sunbro.FindAllByCSS(
        '.vi-info > li:nth-child(7) span ul li')

    review_category = sunbro.FindByName('category_group')
    review_type = sunbro.FindByName('type')
    review_estate_type = sunbro.FindByName('estate_type')

    # ar_stuff
    subject = sunbro.FindByCSS('div[id*="subj_div_"]')
    body = sunbro.FindByClass('ad_text')
    warning = sunbro.FindByID('warning')
    highlights = sunbro.FindAllByClass('WordBorderSolid')
    dup_values = sunbro.FindAllByCSS('.ad_list_ad_detail span[id*="_value"]')
    general_scores = sunbro.FindAllByCSS(".general_score")
    new_title = sunbro.FindByCSS('.da_title .TextDiffNew')
    real_status = sunbro.FindByCSS('.RealStatus')
    error = sunbro.FindByCSS('.error')

    # Mass delete confirmation
    module_content = sunbro.FindByClass('module_content')

    form = sunbro.FindByCSS('.ad-review')
    currency = sunbro.FindAllByCSS('input[name=currency]')
    currency_current = sunbro.FindByCSS('input[name=currency]:checked')
    currency_prefix = sunbro.FindByCSS('.currency-prefix')
    other_price = sunbro.FindByCSS('.other-price')

    inserting_fee = sunbro.FindByPartialLinkText("Inserting fee")
    account_with_pack = sunbro.FindByCSS('.account_with_pack')
    bulkload_ad_label = sunbro.FindByID('bulkload_ad_label')

    def logout(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?logout')

    def get_currency_buttons(self):
        res = {}
        print("{}".format(self.currency))
        for curr in self.currency:
            res[curr.get_attribute('value')] = curr
        return res

    def get_duplicated_values(self):
        res = []
        for d in self.dup_values:
            res.append(d.text)
        return res

    def get_general_scores(self):
        res = []
        for d in self.general_scores:
            res.append(d.text)
        return res

    @lazy
    def duplicated_ads(self):
        duplicated_ads = {}
        for duplicated_ad in map(RelatedAd, self.list_duplicated_ads):
            duplicated_ads[duplicated_ad.id] = duplicated_ad
        return duplicated_ads

    @lazy
    def deactivated_ads(self):
        deactivated_ads = {}
        for deactivated_ad in map(RelatedAd, self.list_deactivated_ads):
            deactivated_ads[deactivated_ad.id] = deactivated_ad
        return deactivated_ads

    @lazy
    def published_ads(self):
        published_ads = {}
        for published_ad in map(RelatedAd, self.list_published_ads):
            published_ads[published_ad.id] = published_ad
        return published_ads

    queue_id = sunbro.FindByCSS('.fine_print a[target="_blank"]')

    def table_rows_data_grid_elements(self):
        rows_visible = []
        rows_hidden = []
        for x in self.table_rows_data_grid:
            if x.is_displayed():
                rows_visible.append(x)
            else:
                rows_hidden.append(x)
        return (rows_visible, rows_hidden)

    def go(self, params=''):
        self._driver.get(conf.CONTROLPANEL_URL + params)
        self.wait.until_loaded()
        return self

    def login(self, user, password, check_menu=True):
        self._driver.find_element_by_name("username").clear()
        self._driver.find_element_by_name("cpasswd").clear()
        self._driver.find_element_by_name("username").send_keys(user)
        self._driver.find_element_by_name("cpasswd").send_keys(password)
        self._driver.find_element_by_name("login").click()
        if check_menu is True:
            self.wait.until_not_visible('username')
        return self.go()

    def logout(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?logout')
        self.wait.until_visible('username')

    def check_login(self):
        try:
            self._driver.find_element_by_name("username")
            return True
        except NoSuchElementException:
            return False

    def search_for_ad_by_list_id(self, list_id=None):
        self._driver.get(conf.CONTROLPANEL_URL)
        self._driver.find_element_by_partial_link_text("Search for ad").click()
        self._driver.find_element_by_id('search_type_list_id').click()
        self._driver.find_element_by_id("search_field").send_keys(list_id)
        self._driver.find_element_by_name("search").click()

    def search_for_ad_by_subject(self, subject=None):
        self.search_for_ads(subject=subject, time_from='2011-01-01')

    def search_for_ad_by_ad_id(self, ad_id):
        self.search_for_ads(ad_id=ad_id, time_from='2011-01-01')

    def search_for_ad_by_subject_in_queue(self, subject=None, queue=None):
        self.search_for_ads(
            subject=subject, time_from='2011-01-01', queue=queue)

    def queue_has_elements(self, queue, count=0):
        self._driver.find_element_by_xpath("//li/a[text() = 'Queues']").click()
        try:
            self._driver.find_element_by_partial_link_text("{0} {1}".format(count, queue))
            self._driver.back()
            return True
        except NoSuchElementException:
            self._driver.back()
            return False

    def go_to_queue(self, queue):
        self._driver.find_element_by_xpath("//li/a[text() = 'Queues']").click()
        self._driver.find_element_by_partial_link_text(queue).click()

    def go_to_no_lock_queue(self, queue):
        self._driver.find_element_by_xpath("//li/a[text() = 'Queues']").click()
        self._driver.find_element_by_xpath(
            "//fieldset/ul/li/a[contains(.,'{0}')]".format(queue)).click()

    def get_adparam_of_ad(self, ad_param_name):
        element = self.ad_info.find_element_by_css_selector(
            '*[name="{0}"]'.format(ad_param_name))
        if element.tag_name == 'select':
            return Select(element).first_selected_option
        return element

    def go_to_option_in_menu(self, option):
        self._driver.find_element_by_xpath(
            "//li/a[text() = '" + option + "']").click()

    def login_and_go_to_option(
            self,
            option,
            do_login=False,
            user='',
            passwd=''):
        if do_login:
            self.login(user, passwd)
        self.go_to_option_in_menu(option)
        return self

    def better_go_to_queue(self, queue):
        self._driver.find_element_by_partial_link_text('Queues').click()
        self._driver.find_element_by_css_selector(
            'a[href="?lock=1&m=adqueue&a=show_adqueues&queue={}"]'.format(queue)).click()

    def better_go_to_no_lock_queue(self, queue):
        self._driver.find_element_by_partial_link_text('Queues').click()
        self._driver.find_element_by_css_selector(
            'a[href="?lock=0&m=adqueue&a=show_adqueues&queue={}"]'.format(queue)).click()

    def back_to_cp(self):
        self._driver.close()  # close store edition popup
        self._driver.switch_to_window(self._driver.window_handles[0])

    def search_for_ads(
            self,
            email=None,
            list_id=None,
            ad_id=None,
            subject=None,
            time_from=None,
            queue=None,
            search_interval=None):
        none_count = 0
        if email is None:
            none_count += 1
        if list_id is None:
            none_count += 1
        if ad_id is None:
            none_count += 1
        if subject is None:
            none_count += 1

        # DA FAC
        if none_count == 4:
            raise Exception("There must be one search criteria")
        if none_count != 3:
            raise Exception(
                "Only one search criteria can be selected at a time")

        self.go()
        self._driver.find_element_by_partial_link_text('Search for ad').click()
        if search_interval:
            if search_interval == 'all':
                self.radio_button_last_all.click()
        if email:
            self._driver.find_element_by_id('search_type_email_part').click()
            self._driver.find_element_by_id("search_field").send_keys(email)
        elif ad_id:
            self._driver.find_element_by_id('search_type_ad_id').click()
            self._driver.find_element_by_id("search_field").send_keys(ad_id)
        elif list_id:
            self._driver.find_element_by_id('search_type_list_id').click()
            self._driver.find_element_by_id("search_field").send_keys(list_id)
        else:
            self._driver.find_element_by_id('search_type_subject').click()
            self._driver.find_element_by_id("search_field").send_keys(subject)

        if time_from:
            self._driver.find_element_by_id("custom_timespan").click()
            self._driver.find_element_by_name("time_from").clear()
            self._driver.find_element_by_name("time_from").send_keys(time_from)
            self._driver.find_element_by_name("search").click()
        if queue:
            queueSelect = Select(self._driver.find_element_by_name("queue"))
            queueSelect.select_by_visible_text(queue)

        self._driver.find_element_by_name("search").click()

    def search_ad_by_list_id(self, list_id):
        self._driver.find_element_by_partial_link_text('Search for ad').click()
        self._driver.find_element_by_id('search_type_list_id').click()
        self._driver.find_element_by_id("search_field").send_keys(str(list_id))
        self._driver.find_element_by_name("search").click()

    def search_ad_by_ad_id(self, ad_id):

        self._driver.find_element_by_partial_link_text('Search for ad').click()
        self._driver.find_element_by_id('search_type_ad_id').click()
        self._driver.find_element_by_id("search_field").send_keys(str(ad_id))
        self.set_search_timeframe()
        self._driver.find_element_by_name("search").click()

    def search_ad_by_ad_id_inside_search_for_ad(self, ad_id):
        """
        This method assume that We are inside the "Search for ad" menu and we
        don't want to click "Search for ad" link again because this clean all
        modified options
        """
        self._driver.find_element_by_id('search_type_ad_id').click()
        self._driver.find_element_by_id("search_field").send_keys(str(ad_id))
        self._driver.find_element_by_name("search").click()

    def set_search_timeframe(self, timeframe="all"):
        if timeframe != 'all' and timeframe != 'day' and timeframe != 'week':
            timeframe = 'all'
        self._driver.find_element_by_id('timespan_' + timeframe).click()

    def search_for_ad(
            self,
            email,
            subject=None,
            list_id=None,
            queue=None,
            timeframe="all"):
        self._driver.find_element_by_partial_link_text('Search for ad').click()
        self._driver.find_element_by_id("search_field").send_keys(email)
        if queue is not None:
            queueSelect = Select(self._driver.find_element_by_name("queue"))
            queueSelect.select_by_visible_text(queue)

        self.set_search_timeframe(timeframe=timeframe)

        self._driver.find_element_by_name("search").click()
        if subject is not None:
            self._driver.find_element_by_partial_link_text(subject).click()
            self._driver.switch_to_window(self._driver.window_handles[-1])
        if list_id is not None:
            self._driver.find_element_by_partial_link_text(list_id).click()
            self._driver.switch_to_window(self._driver.window_handles[-1])

    def review_ad(self, action=None, refusal_reason_name=None):
        if (action == 'deny' or action == 'refuse') and refusal_reason_name is None:
            refusal_reason_name = 'Spam'
        if action:
            if action in ['accept', 'region', 'abuse', 'unsolved']:
                # for some reason
                # self._driver.find_element_by_css_selector(
                #       "input[name='queue_action'][value='{action}']".format(action=action)).click()
                # is'nt working sometimes
                script = "$(\"input[name='queue_action'][value='{action}']\").first().attr('checked', true);".format(
                    action=action)
                if len(self._driver.window_handles) > 1:
                    self._driver.switch_to_window(
                        self._driver.window_handles[1])
                self._driver.execute_script(script)
            elif action == "edit_and_accept":
                for option in self._driver.find_element_by_name('reasonAccept').find_elements_by_tag_name('option'):
                    if option.text == refusal_reason_name:
                        option.click()
                        break
            else:
                if action == "deny" or action == 'refuse':
                    for option in self._driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
                        if option.text == refusal_reason_name:
                            option.click()
                            break

            self._driver.execute_script(
                "$(\"input[name='commit']:first\").click()")
            #  wait for the response
            self._driver.find_element_by_class_name("AjaxCommitOk")

        if action is None and refusal_reason_name == 'Fraude':
            for option in self._driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
                if option.text == refusal_reason_name:
                    option.click()
                    break
            self._driver.find_element_by_xpath(
                "//input[@name='commit'][@type='submit']").click()

    def count_reviews_in_page(self):
        script = "return $(\"input[name='ad_id']\").length"
        return self._driver.execute_script(script)

    def close_window_and_back(self):
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def change_first_ad_status(self, action=None):
        self._driver.find_element_by_css_selector('.link_edit').click()
        self._driver.switch_to_window(self._driver.window_handles[-1])
        if action:
            if action in ['delete', 'edit', 'renew', 'hide', 'show', 'activate', 'deactivate']:
                self._driver.find_element_by_id("cmd_" + action).click()
            self._driver.find_element_by_name('continue').click()
            WebDriverWait(self._driver, 10).until(
                EC.presence_of_element_located((By.TAG_NAME, 'body')))

    def change_ad_status(self, subject=None, action=None):

        cp_action = {
            'delete': 'cmd_delete',
        }

        self.search_for_ads(subject=subject, time_from='2011-01-01')
        self._driver.find_element_by_partial_link_text(
            'Edit, delete, hide').click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        if action:
            if action == 'delete':
                self._driver.find_element_by_id(cp_action[action]).click()
            self._driver.find_element_by_name('continue').click()

        self.close_window_and_back()

    def edit_ad_by_subject(self, subject=None):
        self.search_for_ads(subject=subject, time_from='2011-01-01')
        self._driver.find_element_by_partial_link_text(
            'Edit, delete, hide').click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        self._driver.find_element_by_id('cmd_edit').click()
        self._driver.find_element_by_name('continue').click()
        self._driver.find_element_by_id('submit_create_now').click()

    def search_by_list_id_and_go_edit(self, list_id=None):
        self.search_for_ad_by_list_id(list_id)
        self._driver.find_element_by_partial_link_text(
            'Edit, delete, hide').click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        self._driver.find_element_by_id('cmd_edit').click()
        self._driver.find_element_by_name('continue').click()

    def open_ad_on_search(self, subject=None):
        if subject is not None:
            self._driver.find_element_by_partial_link_text(subject).click()
        else:
            self.review.click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        self.wait.until_loaded()

    def review_first_ad_on_search(self, refusal_condition_name='Spam'):
        self.review.click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        for option in self._driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
            if option.text == refusal_condition_name:
                option.click()
                break
        self._driver.execute_script("$(\"input[name='commit']\").click()")
        self._driver.find_element_by_class_name("AjaxCommitOk")

    def refuse_ad(self, subject=None, refusal_condition_name='Spam'):
        self.search_for_ad_by_subject(subject=subject)
        self._driver.find_element_by_partial_link_text(subject).click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        for option in self._driver.find_element_by_name('reason').find_elements_by_tag_name('option'):
            if option.text == refusal_condition_name:
                option.click()
                break
        self._driver.execute_script("$(\"input[name='commit']\").click()")
        self._driver.find_element_by_class_name("AjaxCommitOk")
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    # Only works if your search return exactly one ad
    def add_note_to_ad(self, subject=None, text=None):
        self.search_for_ad_by_subject(subject=subject)
        self._driver.find_element_by_partial_link_text('Review').click()
        self._driver.switch_to_window(self._driver.window_handles[1])
        self._driver.execute_script(
            "$('a:contains(\"Create ad note\")').click()")
        self.note_text.send_keys(text)
        self._driver.execute_script("$(\"input[name='commit']\").click()")
        self._driver.find_element_by_class_name("AjaxCommitOk")
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def accept_ad(self, subject=None, email=None):
        if email is not None:
            self.search_for_ad(email, timeframe='day')
        elif subject is not None:
            self.search_for_ad_by_subject(subject=subject)
        links = self._driver.find_elements_by_partial_link_text(subject)
        for link in links:
            if "adqueue" in link.get_attribute('href'):
                link.click()
                break
        self._driver.switch_to_window(self._driver.window_handles[-1])
        self.review_ad(action='accept')

    def action_on_ad(self, list_id, action):
        self.search_for_ad_by_list_id(list_id)
        self.change_first_ad_status(action)
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def unhide_ad(self, list_id=None):
        self.action_on_ad(list_id, action='show')

    def hide_ad(self, list_id=None):
        self.action_on_ad(list_id, action='hide')

    def activate_ad(self, list_id=None):
        self.action_on_ad(list_id, action='activate')

    def deactivate_ad(self, list_id=None):
        self.action_on_ad(list_id, action='deactivate')

    def post_refuse_ad(self, list_id=None, reason_name=None):
        self.search_for_ad_by_list_id(list_id)
        if reason_name:
            self.review_first_ad_on_search(reason_name)
        else:
            self.review_first_ad_on_search()
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def post_refuse_ad_by_subject(self, subject=None):
        self.search_for_ad_by_subject(subject=subject)
        self.review_first_ad_on_search()  # Reason: spam by default
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def go_and_login(self, user, password, check_menu=True):
        self.go()
        if utils.is_not_regress():
            user = 'qa_tests'
            password = '123123123'
        self.login(user, password, check_menu=check_menu)

    def search_and_review_ad(
            self,
            email,
            subject,
            action,
            refusal_reason_name=None,
            list_id=None,
            queue=None):
        self.search_check_image_order_and_review_ad(
            email=email,
            subject=subject,
            action=action,
            ordered_images=None,
            refusal_reason_name=refusal_reason_name,
            list_id=list_id,
            queue=queue)

    def search_check_image_order_and_review_ad(
            self,
            email,
            subject,
            action,
            ordered_images=None,
            refusal_reason_name=None,
            list_id=None,
            queue=None,
            new_first=None):
        if list_id is None:
            self.search_for_ad(email, subject=subject, queue=queue)
        else:
            self.search_for_ad(email, list_id=list_id, queue=queue)

        if ordered_images:
            self.validate_image_order(ordered_images)

        if new_first:
            self.set_first_image(new_first)

        self.review_ad(action, refusal_reason_name)
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def validate_image_order(self, ordered_images):
        i = 0
        images = self._driver.find_elements_by_css_selector('.ad_thumb img')
        for img in ordered_images:
            if img not in images[i].get_attribute('src'):
                raise ImageOrderException(
                    'image {0} was not {1}'.format(i, img))
            i += 1

        extra = True
        try:
            self._driver.find_element_by_css_selector(
                '.thumb_images div[id*="thumb{0}_"]'.format(i))
        except NoSuchElementException:
            extra = False

        if extra:
            raise ImageOrderException('there were additional images')

    def set_first_image(self, new_first):
        script = "$(\"input[name='set_first[]'][value='{n}']\").click();".format(
            n=new_first -
            1)
        self._driver.execute_script(script)

    def search_and_hide_ad(self, ad_id=None, list_id=None, subject=None):
        self.search_for_ads(ad_id=ad_id, list_id=list_id, subject=subject)
        self.change_first_ad_status(action='hide')
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def search_and_show_ad(self, ad_id=None, list_id=None, subject=None):
        self.search_for_ads(ad_id=ad_id, list_id=list_id, subject=subject)
        self.change_first_ad_status(action='show')
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def search_and_delete_ad(self, ad_id=None, list_id=None, subject=None):
        self.search_for_ads(ad_id=ad_id, list_id=list_id, subject=subject)
        self.change_first_ad_status(action='delete')
        self._driver.close()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def search_and_undelete_ad(self, ad_id=None, list_id=None, subject=None):
        self.search_for_ads(ad_id=ad_id, list_id=list_id, subject=subject)
        try:
            self._driver.find_element_by_id('chk_0_0').click()
            self._driver.find_element_by_css_selector(
                'input[value="Undelete selected"]').click()
        except NoSuchElementException:
            raise Exception('Your user sucks!')

    def search_and_undelete_ad_by_subject(self, subject=None, time_from=None):
        self.search_for_ads(subject=subject, time_from=time_from)
        try:
            self._driver.find_element_by_id('chk_0_0').click()
            self._driver.find_element_by_css_selector(
                'input[value="Undelete selected"]').click()
        except NoSuchElementException:
            raise Exception('Your user sucks!')

    def get_changes_on_edition(self):
        try:
            elements = self._driver.find_elements_by_class_name('TextDiffNew')
        except NoSuchElementException as nse:
            return []
        real_changes = [change.text for change in elements]
        return real_changes

    def apply_product(self, list_id, product_name):
        product_select = Select(
            self._driver.find_element_by_id('products_to_apply_' + list_id))
        product_select.select_by_visible_text(product_name)
        self._driver.find_element_by_id('apply_product_' + list_id).click()
        alert = self._driver.switch_to_alert()
        alert.accept()

    def review_by_queue(
            self,
            queue,
            subject,
            action,
            refusal_reason_name="Privacidad",
            no_lock_queue=None,
            keep_page=False):
        if queue is not None:
            self.go_to_queue(queue)
        elif no_lock_queue is not None:
            self.go_to_no_lock_queue(no_lock_queue)
        id = ""
        while id is "":
            subject_list = self._driver.find_elements_by_class_name('da_title')
            for element in subject_list:
                print(element.text)
                if element.text == subject:
                    parent = element.find_element_by_xpath('..')
                    id = parent.get_attribute('id').split('_')[2]
                    break
            if id is "":
                self.next_in_queue()

        if action is "accept":
            self._driver.find_element_by_xpath(
                "//*[contains(@id, 'queue_action_accept_{0}')]".format(id)).click()

        elif action == "edit_and_accept":
            for option in self._driver.find_element_by_xpath("//select[contains(@id, 'acceptEdit_{0}')]".format(id)).find_elements_by_tag_name('option'):
                if option.text == "Sin fotos":
                    option.click()
                    break

        if action is "refuse":
            for option in self._driver.find_element_by_xpath("//select[contains(@id, 'refuse_{0}')]".format(id)).find_elements_by_tag_name('option'):
                if option.text == refusal_reason_name:
                    option.click()
                    break

        self._driver.find_element_by_xpath(
            "//*[contains(@id, 'commit_{0}')]".format(id)).click()

        if no_lock_queue is None and not keep_page:
            self.next_in_queue()

    def next_in_queue(self):
        print("next in queue")
        self._driver.find_element_by_id('next_in_queue').click()
        self.wait.until_loaded()

    def report_stats(self):
        elements_stats = self._driver.find_elements_by_css_selector(
            "#report li .bg-rp")
        return [i.text for i in elements_stats]

    def change_default_options(self, **kwargs):
        """
        kwargs:
           archive: <All archives|This years archive|No archives>
           period:  <All|Last 24h|Last 7 days>
        """
        options = {
            'archive': 'All archives',
            'period': 'All',
        }
        options.update(kwargs)

        if options['period'] == 'All':
            self.radio_button_last_all.click()
        elif options['period'] == 'Last 24h':
            self.radio_button_last_24h.click()
        elif options['period'] == 'Last 7 days':
            self.radio_button_last_7d.click()

        select = Select(self.archive_options)
        select.select_by_visible_text(options['archive'])

    def walk_reviewing_queue(self, queue='Normal', pages=1):
        """ I'll walk through the given pages reviewing all of the ads """
        self.go_to_queue(queue)
        for page in range(pages):
            accept_radios = self._driver.find_elements_by_css_selector(
                '[for*="queue_action_accept"]')
            accept_buttons = self._driver.find_elements_by_css_selector(
                '.SubmitButton')
            for i in range(len(accept_radios)):
                self._driver.execute_script(
                    "$('[id*=\"queue_action_accept\"]')[{}].setAttribute('checked', 'checked')".format(i))
                self._driver.execute_script(
                    "$('[id*=\"commit\"]')[{}].click()".format(i))
            self._driver.find_element_by_css_selector('#next_in_queue').click()

    @lazy
    def return_ad_id(self):
        super_id = self.queue_id.text[:-2]
        return super_id

    def confirm_an_ad(self):
        self.link_confirm[0].click()


class RelatedAd(BasePage):
    detail = sunbro.FindByCSS(".ad_list_ad_detail")
    status = sunbro.FindByCSS(".ad_list_ad_detail .status")
    subject = sunbro.FindByCSS(".ad_list_ad_detail .subject")
    body = sunbro.FindByCSS(".ad_list_ad_detail .body")
    general_score = sunbro.FindByCSS(".ad_list_ad_detail .general_score")
    subject_body_score = sunbro.FindByCSS(
        ".ad_list_ad_detail .subject_body_score")
    subject_score = sunbro.FindByCSS(".ad_list_ad_detail .subject_score_value")
    body_score = sunbro.FindByCSS(".ad_list_ad_detail .subject_score_value")
    price_score = sunbro.FindByCSS(".ad_list_ad_detail .price_score_value")
    list_params = sunbro.FindByCSS(".ad_list_ad_score .list_params")

    @lazy
    def id(self):
        return int(self._driver.find_element_by_css_selector(
            '[data-ad_id]').get_attribute('data-ad_id'))


class ControlPanelMassiveActions(BasePage):
    check_all = sunbro.FindByID("chk_0")
    button_delete = sunbro.FindByCSS('[value="Delete selected"]')
    result_message = sunbro.FindByCSS('.module_content h1')
    result_list = sunbro.FindAllByCSS('.module_content ul li')
    refuse_reason = sunbro.FindByID('refuse_reason')
    button_refuse = sunbro.FindByID('refuse_selected')

    CONFIRM_MASSIVE_REFUSE = "�Est�s seguro que deseas rechazar estos avisos?"


class ControlPanelNewRefusalReasons(BasePage):

    """
    Page object for new refusal reasons management.
    """
    OK_MSG = 'Raz�n guardada'
    REASON_NAME_ERROR = 'Digite la raz�n del rechazo'
    ID_ERROR_MSG = 'Id ya existe'
    CHAR_ERROR_MSG = 'Caracteres inv�lidos o vac�os'
    TEXT_ERROR_MSG = 'Digite un texto'
    MS_ERROR_MSG = 'Hubo un error al almacenar la raz�n en el microservicio.'

    submit_message = sunbro.FindByCSS('table div[name="submit_message"]')

    def go(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            "?m=adqueue&a=refusals&refusal_action=new_refusal")

    # This method was specifically created for test that email isn't sent when the reject reason is fraud
    # so I think to assign default values is a good idea ^_^ (same prod
    # values... useful for this test)
    def add_refusal_reason(self, reason='Fraude', id='36', text='.'):
        # START WEB ELEMENTS DECLARATION SECTION (the ugly section).
        self.reason_input = self._driver.find_element_by_css_selector(
            ".module_content input[name='refusal_new_subject']")
        self.id_input = self._driver.find_element_by_css_selector(
            ".module_content input[name='refusal_id']")
        self.reason_select = self._driver.find_element_by_css_selector(
            ".module_content select")
        self.reason_textarea = self._driver.find_element_by_css_selector(
            ".module_content textarea")
        self.submit_button = self._driver.find_element_by_css_selector(
            ".module_content input[name='refusal_submit']")
        # END WEB ELEMENTS DECLARATION SECTION
        self.reason_input.send_keys(reason)
        self.id_input.send_keys(id)
        self.reason_textarea.send_keys(text)
        self.wait.until_loaded()
        self.submit_button.click()
        # GET SUBMIT MESSAGE
        self.wait.until_loaded()
        return self.submit_message.text


class ControlPanelDeleteRefusalReasons(BasePage):

    """
    Page object for delete refusal reasons management.
    """
    OK_MSG = 'Raz�n de rechazo ha sido borrada'
    MS_ERROR_MSG = 'Hubo un error al eliminar la raz�n en el microservicio.'

    submit_message = sunbro.FindByCSS('table div[name="submit_message"]')

    def go(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            "?m=adqueue&a=refusals&refusal_action=delete_refusal")

    # This method allow test reason delete action
    def delete_refusal_reason(self, reason='Fraude'):
        # START WEB ELEMENTS DECLARATION SECTION (the ugly section).
        self.reason_select = self._driver.find_element_by_css_selector(
            ".module_content select[name='refusal_subject']")
        self.submit_button = self._driver.find_element_by_css_selector(
            ".module_content input[name='refusal_submit']")
        # END WEB ELEMENTS DECLARATION SECTION
        self.reason_select.send_keys(reason)
        self.wait.until_loaded()
        self.submit_button.click()
        # GET SUBMIT MESSAGE
        self.wait.until_loaded()
        return self.submit_message.text


class ControlPanelRefusalReasonScore(BasePage):

    """
    Page object for edit refusal reasons score.
    """
    OK_MSG = 'Todo Ok'
    NUMERIC_FIELD_MSG = 'Digite enteros positivos'
    MS_ERROR_MSG = 'Hubo un error al obtener las prioridades desde el microservicio.'

    submit_message = sunbro.FindByCSS('table div[name="submit_message"]')

    def go(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            "?m=adqueue&a=refusals_score")

    # This method allow test reason score edit
    def edit_refusal_reason_score(self, id='302', score='10'):
        # START WEB ELEMENTS DECLARATION SECTION (the ugly section).
        self.reason_to_edit = self._driver.find_element_by_css_selector(
            "input[name='refusal_scores:"+str(id)+"']")
        self.submit_button = self._driver.find_element_by_css_selector(
            "input[name='refusal_submit']")
        # END WEB ELEMENTS DECLARATION SECTION
        self._driver.execute_script("arguments[0].setAttribute('value','"+str(score)+"')", self.reason_to_edit)
        self.wait.until_loaded()
        self.submit_button.click()
        # GET SUBMIT MESSAGE
        self.wait.until_loaded()
        return self.submit_message.text

   # This method allow test reason score edit
    def list_refusal_reasons_scores(self):
        # GET SUBMIT MESSAGE
        return self.submit_message.text


class ControlPanelAcceptedReasons(BasePage):

    new_subject = sunbro.FindByName('accepted_new_subject')
    id = sunbro.FindByName('accepted_id')
    subject = sunbro.FindByName('accepted_subject')
    text = sunbro.FindByName('accepted_text')
    submit = sunbro.FindByName('accepted_submit')
    success = sunbro.FindByCSS('.okey')
    error = sunbro.FindByCSS('.error')

    def go(self, action='new'):
        if action == 'new':
            self._driver.get(
                conf.CONTROLPANEL_URL +
                '?m=adqueue&a=accepted&accepted_action=new_accepted')
        if action == 'delete':
            self._driver.get(
                conf.CONTROLPANEL_URL +
                '?m=adqueue&a=accepted&accepted_action=delete_accepted')
        if action == 'edit':
            self._driver.get(
                conf.CONTROLPANEL_URL +
                '?m=adqueue&a=accepted&accepted_action=edit_accepted')


class ControlPanelSpamfilter(BasePage):

    """
    Page object for spamfilter management.
    """

    listing_table = sunbro.FindByCSS('.ListingTable')
    listing_items = sunbro.FindAllByCSS('.ListingTable a')
    spara = sunbro.FindByCSS('input[value="Spara"]')
    reason_ip_block = sunbro.FindByCSS('textarea[name="notice_body[0]"]')
    reason_email_block = sunbro.FindByCSS('textarea[name="notice_body[1]"]')

    def go_to_filter_lists(self):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=filter&a=lists")

    def go_to_filter_rules(self):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=filter&a=rules")

    def go_to_filter_spamfilter(self):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=filter&a=spamfilter")
        WebDriverWait(self._driver, 10).until(
            EC.presence_of_element_located((By.CLASS_NAME, 'ListingTable')))

    def add_email_to_filter(
            self,
            value,
            notice='default text',
            list='E-post adresser - spamfiltret'):
        control_panel = ControlPanel(self._driver)
        control_panel.go()
        if control_panel.check_login():
            control_panel.login('dany', 'dany')
        self.go_to_filter_lists()
        self._driver.find_element_by_link_text(list).click()
        self.value = self._driver.find_element_by_name('value')
        self.notice = self._driver.find_element_by_name('notice')
        self.add_button = self._driver.find_element_by_xpath(
            "//input[@type='submit'][@value='Agregar a la lista']")
        self.value.send_keys(value)
        self.notice.send_keys(notice)
        self.add_button.click()
        self.check_activation_email_rule()

    def check_text_in_list(self, text, list='E-post adresser - spamfiltret'):
        control_panel = ControlPanel(self._driver)
        control_panel.go()
        if control_panel.check_login():
            control_panel.login('dany', 'dany')
        self.go_to_filter_lists()
        self._driver.find_element_by_link_text(list).click()
        self._driver.find_element_by_id('q').send_keys(text)
        button = self._driver.find_element_by_xpath(
            "//input[@type='submit'][@value='Buscar']")
        button.click()

    def check_activation_email_rule(self):
        self._driver.get(
            conf.CONTROLPANEL_URL + "?m=filter&a=add_rule&rule_id=14")
        checkbox_email = self._driver.find_element_by_id('email[1]')
        save_button = self._driver.find_element_by_xpath(
            "//input[@type='submit'][@value='Save rule']")
        if not checkbox_email.is_selected():
            checkbox_email.click()
            save_button.click()

    def action_mail_on_spam_filter(self, subject, action):
        control_panel = ControlPanel(self._driver)
        control_panel.go()
        if control_panel.check_login():
            control_panel.login('dany', 'dany')
        self.go_to_filter_spamfilter()
        table = self._driver.find_element_by_class_name('ListingTable')
        WebDriverWait(self._driver, 10).until(
            EC.presence_of_element_located((By.TAG_NAME, 'a')))
        table.find_element_by_tag_name('a').click()
        msgs = self._driver.find_element_by_class_name(
            'Listing').find_elements_by_class_name('Message')
        action_buttons = self._driver.find_element_by_class_name(
            'Listing').find_elements_by_xpath("//input[@type='submit'][@value='" + action + "']")
        for row in range(len(msgs)):
            if msgs[row].text == subject:
                action_buttons[row].click()

    def add_to_spamfilter(self, message):
        self._driver.find_element_by_class_name('module_content').find_elements_by_xpath(
            "//input[@name='notice_list_id[1]']")[1].click()
        self.wait.until_present('reason_email_block')
        self.reason_email_block.send_keys(message)
        self.spara.click()

    def go_to_first_item(self):
        self.wait.until_present('listing_table')
        self.listing_table.find_element_by_tag_name('a').click()

    def get_spam_messages(self):
        return self._driver.find_element_by_class_name(
            'Listing').find_elements_by_class_name('Message')


class WebSQL(ControlPanel):

    # Login page
    username = sunbro.FindByName('username')

    select_query_element = sunbro.FindByName('query')
    button_load = sunbro.FindByName('load')
    button_run = sunbro.FindByName('run')
    table = sunbro.FindByCSS('table.websql_result_table')
    errors = sunbro.FindAllByCSS('.error')
    download_csv = sunbro.FindByLinkText('Download as excelfile')

    def login(self, user, password):
        super(WebSQL, self).go()
        super(WebSQL, self).login(user, password)

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=Websql&a=Websql')
        print(conf.CONTROLPANEL_URL + '?m=Websql&a=Websql')

    def select_query(self, name):
        sql = Select(self.select_query_element)
        sql.select_by_visible_text(name)
        self.button_load.click()

    def fill_input(self, name, value):
        input = self._driver.find_element_by_name(name)
        if input.tag_name == 'select':
            input = Select(self.select_query_element)

            input.select_by_visible_text(name)
            return
        else:
            input.clear()
            input.send_keys(value)

    def call_websql(self, query_name, **options):
        """ Calls a websql, filling the form with the supplied `options` data """
        self.go()
        self.select_query(query_name)
        for field_name, value in options.items():
            self.fill_input(field_name, value)
        self.run()

    def run(self):
        self.button_run.click()

    def get_result_header(self):
        table = self.table
        rows = table.find_elements_by_tag_name('tr')
        data = []
        # We return the headers that are in the 1st row
        cols = rows[0].find_elements_by_tag_name('th')
        for col in cols:
            data.append(col.text)
        return data

    def get_result_data(self, thecols=range(100)):
        table = self.table
        rows = table.find_elements_by_tag_name('tr')
        data = []
        # We ignore the headers that are in the 1st row
        for row in rows[1:]:
            tosave = []
            c = 0
            cols = row.find_elements_by_tag_name('td')
            for col in cols:
                if c in thecols:
                    tosave.append(col)
                c += 1
            data.append([c.text for c in tosave])
        return data


class ControlPanelAddUser(BasePage):

    username = sunbro.FindByName("username")
    password = sunbro.FindByName("pd")
    verify_password = sunbro.FindByName("pd_confirm")
    name = sunbro.FindByName("fullname")
    email = sunbro.FindByName("email")
    jabber = sunbro.FindByName("jabbername")
    mobile = sunbro.FindByName("mobile")
    allprivilege = sunbro.FindByLinkText("all")
    submit = sunbro.FindByName("save")


class ControlPanelScarface(BasePage):

    title = sunbro.FindByCSS(".da_title")
    reply_text = sunbro.FindByCSS(".prevMessages textarea")
    email_link_reply = sunbro.FindByCSS(".prevMessages a")
    send_reply = sunbro.FindByCSS(".prevMessages button")
    accept = sunbro.FindByCSS('[value="Accept"]')
    accept_notify_owner = sunbro.FindByCSS(
        '[value="Accept & Notify Ad Owner"]')
    option_notify_owner = sunbro.FindByName("notify_owner")
    reject = sunbro.FindByCSS('[value="Reject"]')
    reject_notify_report = sunbro.FindByCSS(
        '[value="Reject & Notify Reporter"]')
    option_notify_report = sunbro.FindByName("notify_reporter")

    def go_link_text(self, text):
        self._driver.find_element_by_partial_link_text(text).click()


class ControlPanelWhitelist(BasePage):

    email = sunbro.FindByName("value")
    message = sunbro.FindByName("notice")
    add_to_list = sunbro.FindByCSS('[value="Agregar a la lista"]')
    search = sunbro.FindByID("filter_results_button")
    option_notify_report = sunbro.FindByName("notify_reporter")
    search_email = sunbro.FindByID("q")
    search_results = sunbro.FindByCSS(".ListingTable tbody td:first-child a")
    ad_from_whitelist = sunbro.FindByClass("da_title")
    whitelist_verify = sunbro.FindByCSS("#tbl_0")

    def go_link_text(self, text):
        self._driver.find_element_by_partial_link_text(text).click()

    def go_to_whitelist_queue(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            "?lock=0&m=adqueue&a=show_adqueues&queue=whitelist")


class ControlPanelTelesales(BasePage):

    email_search = sunbro.FindByID("telesales-email")
    button_search = sunbro.FindByID("search-email")
    bill = sunbro.FindByID("document-bill")
    invoice = sunbro.FindByID("document-invoice")
    email = sunbro.FindByCSS('.form-telesales input[name="email"]')
    pay_button = sunbro.FindByCSS('.btn-full')
    # invoice form
    name = sunbro.FindByID("id_name")
    lob = sunbro.FindByID("id_lob")
    rut = sunbro.FindByID("id_rut")
    address = sunbro.FindByID("id_address")
    communes = sunbro.FindByID("id_communes")
    contact = sunbro.FindByID("id_contact")
    region = sunbro.FindByID("id_region")

    # store products
    store_info = sunbro.FindByCSS(".store-info")
    plan_none = sunbro.FindByID("plan_none")
    monthly_store = sunbro.FindByID("monthly_store")
    quarterly_store = sunbro.FindByID("quarterly_store")
    biannual_store = sunbro.FindByID("biannual_store")
    annual_store = sunbro.FindByID("annual_store")

    # store edition
    link_edit_store = sunbro.FindByCSS(".store-edit a")
    store_name = sunbro.FindByCSS("input[name='store_name']")
    store_name_error = sunbro.FindByID("err_store_name")
    store_description = sunbro.FindByCSS("textarea[name='store_info_text']")
    store_description_error = sunbro.FindByID("err_store_info_text")
    store_region = sunbro.FindByID("region_input")
    store_region_error = sunbro.FindByID("err_store_region")
    button_save = sunbro.FindByCSS("input[value='Guardar']")

    # Pack
    selector_slots = sunbro.FindByID("pack_slots")
    selector_period = sunbro.FindByID("pack_period")
    pack_assign = sunbro.FindByID("pack_assign_buttom")
    pack_selected = sunbro.FindByID("pack_cart")
    pack_borrar = sunbro.FindByID("otroid")

    # cart
    cart = sunbro.FindByCSS(".table-subtotals")

    # Error Messages
    payment_form = sunbro.FindByID("payment_form")
    errors = sunbro.FindByCSS(".error")

    # Card info for webpay plus
    card_number = sunbro.FindByID("cardNumber")
    card_shares = sunbro.FindByName("shareNumber")
    card_exp_month = sunbro.FindByName("expMonth")
    card_exp_year = sunbro.FindByName("expYear")
    card_cvv = sunbro.FindByID("cvv")

    def go_link_text(self, text):
        self._driver.find_element_by_partial_link_text(text).click()

    def go_edit_account_tabe(self, text):
        self.go_link_text('Editar Cuenta')

    def go_sell_products_tabe(self, text):
        self.go_link_text('Vender Productos')

    def go_store_edition(self):
        self.link_edit_store.click()
        self._driver.switch_to_window(self._driver.window_handles[1])

    def back_to_cp(self):
        self._driver.close()  # close store edition popup
        self._driver.switch_to_window(self._driver.window_handles[0])

    def select_product_by_list_id(self, product, list_id):
        self._driver.find_element_by_id(product + "-" + list_id).click()

    def select_store_product(self, product):
        self._driver.find_element_by_id(product).click()

    def select_pack(self, product):
        slots = product[2:5]
        period = product[1:2]
        Select(self.selector_slots).select_by_value(slots)
        Select(self.selector_period).select_by_value(period)
        self.pack_assign.click()

    def get_elements_succes_page(self):
        # option [0 = resume table, 1 = products table,  2 = purchase table]
        table = self._driver.find_elements_by_css_selector('.table')[1]
        table_rows = table.find_elements_by_tag_name('tr')
        rows = []

        for row in table_rows:
            cols = row.find_elements_by_tag_name('td')
            rows.append([c.text for c in cols])

        return rows

    def get_purchase_data_succes_page(self):
        table = self._driver.find_elements_by_css_selector('.table')[2]
        table_rows = table.find_elements_by_tag_name('tr')
        rows = {}

        for row in table_rows:
            cols = row.find_elements_by_tag_name('td')
            if len(cols) > 1:
                rows[cols[0].text] = cols[1].text

        return rows

    def fill_card_info(self, bconf, data):
        if bconf['*.*.payment.telesales.webpay_plus.enabled'] == '1':
            card_info = {
                'card_number': '4051885600446623',
                'card_shares': '1',
                'card_exp_month': '03',
                'card_exp_year': '18',
                'card_cvv': '123',
            }
            if 'card_info' in data:
                card_info = data.get('card_info')
            self.fill_form(**card_info)
        else:
            print("Bconf webpay plus for TS is not set")


class ControlPanelAccounts(BasePage):
    email_search = sunbro.FindByID("action_email")
    button_search = sunbro.FindByCSS("input[name='search']")
    activate = sunbro.FindByID("activate")
    deactivate = sunbro.FindByID("deactivate")
    result_message = sunbro.FindByCSS(".result")
    error_message = sunbro.FindByID("error")
    unlock_account = sunbro.FindByID("unlock_account")
    lock_account = sunbro.FindByID("lock_account")

    def manage_account(self, email='', action='activate'):
        self.email_search.send_keys(email)
        self.button_search.click()
        if action == 'activate':
            self.activate.click()
        elif action == 'deactivate':
            self.deactivate.click()
        elif action == 'blocked':
            self.lock_account.click()
        elif action == 'unlocked':
            self.unlock_account.click()
        self.wait.until(EC.alert_is_present())
        self._driver.switch_to_alert().accept()


class ControlPanelStores(BasePage):
    # Search elements
    email_search = sunbro.FindByID("store-email")
    button_search = sunbro.FindByID("search-email")
    error_search = sunbro.FindByCSS(".error")
    # Store & Account info
    info_acc_name = sunbro.FindByID("acc_name")
    info_acc_phone = sunbro.FindByID("acc_phone")
    info_acc_status = sunbro.FindByID("acc_status")
    info_str_name = sunbro.FindByID("str_name")
    info_str_text = sunbro.FindByID("str_text")
    info_str_url = sunbro.FindByID("str_url")
    info_str_web = sunbro.FindByID("str_web")
    info_str_status = sunbro.FindByID("str_status")
    info_str_not_found = sunbro.FindByID("str_not_found")
    # Store create and extend
    selector_prod = sunbro.FindByID("product_id")
    create_button = sunbro.FindByID("product_create")
    extend_button = sunbro.FindByID("store_extension")
    deactivate_button = sunbro.FindByID("deactivate_store")
    success_message = sunbro.FindByID("success-message")
    # Links
    edit_store_link = sunbro.FindByPartialLinkText('Editar tienda')
    store_history_link = sunbro.FindByPartialLinkText('Historial de tienda')
    # StoreHistory page
    history_table_body = sunbro.FindByID("htable_body")
    history_column_date = sunbro.FindAllByCSS(".hdate")
    history_column_event = sunbro.FindAllByCSS(".hevent")
    history_column_status = sunbro.FindAllByCSS(".hstatus")
    history_column_admin = sunbro.FindAllByCSS(".hadmin")
    # Success Edition page
    edit_success_logged_info = sunbro.FindByID("cp_logged_info")
    edit_success_close_link = sunbro.FindByPartialLinkText("Cerrar")
    edit_success_message = sunbro.FindByCSS("h1")

    def go(self, search_email=None):
        if search_email is None:
            self._driver.get(
                conf.CONTROLPANEL_URL + "?m=Stores&a=createStore")
        else:
            self._driver.get(
                conf.CONTROLPANEL_URL +
                "?m=Stores&a=createStore&&search_email=" +
                search_email)
        # self._driver.maximize_window()

    def create_store(self, prod=None):
        if prod is not None:
            selector = Select(self.selector_prod)
            selector.select_by_visible_text(prod)
        self.create_button.click()
        self.wait.until(EC.alert_is_present())
        self._driver.switch_to_alert().accept()
        self._driver.switch_to.default_content()

    def extend_store(self, prod=None):
        if prod is not None:
            selector = Select(self.selector_prod)
            selector.select_by_visible_text(prod)
        self.extend_button.click()
        self._driver.switch_to_alert().accept()

    def edit_store(self):
        self.edit_store_link.click()
        self._driver.switch_to_window(self._driver.window_handles[1])

    def go_store_history(self):
        self.wait.until_present('store_history_link')
        self.store_history_link.click()
        self._driver.switch_to_window(self._driver.window_handles[1])

    def back_to_cp(self):
        self._driver.switch_to_window(self._driver.window_handles[0])

    def get_history(self):
        self.go_store_history()
        rows = []
        for i in range(len(self.history_column_event)):
            rows.append([self.history_column_event[i].text,
                         self.history_column_status[i].text,
                         self.history_column_admin[i].text])
        self._driver.close()
        return rows


class ControPanelServicesModule(BasePage):
    ad_id = sunbro.FindByID("ad_id")
    service = sunbro.FindByID("cmd")
    apply_action = sunbro.FindByID("apply_action")
    calculate_action = sunbro.FindByID("calculate_action")
    calculate_info = sunbro.FindByCSS(".ListingTable")

    message_success = sunbro.FindByCSS("h1")
    message_error = sunbro.FindByCSS(".error")
    radio_all = sunbro.FindByID("timespan_all")
    frequency = sunbro.FindByID("frequency")
    num_days = sunbro.FindByID("num_days")
    use_night = sunbro.FindByID("use_night")
    success_detail = sunbro.FindByCSS(".module_content p")
    service_order = sunbro.FindByID("service_order")
    service_amount = sunbro.FindByID("service_amount")
    info = sunbro.FindByID("info")

    def go(self, action='admin'):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=Services&a=" + action)
        self.wait.until_loaded()

    def apply_directly(self, ad_id, service):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            "?m=Services&a=admin&ad_id=" +
            ad_id +
            "&cmd=" +
            service)
        self.wait.until_loaded()

    services_email = sunbro.FindByID("services-email")
    button_search = sunbro.FindByID("search-email")
    button_apply = sunbro.FindByID("apply")

    # store products
    store_info = sunbro.FindByCSS(".store-info")
    plan_none = sunbro.FindByID("plan_none")
    monthly_store = sunbro.FindByID("monthly_store")
    quarterly_store = sunbro.FindByID("quarterly_store")
    biannual_store = sunbro.FindByID("biannual_store")
    annual_store = sunbro.FindByID("annual_store")

    massive_success = sunbro.FindByCSS(".success.store-table")
    massive_error = sunbro.FindByCSS(".error.store-table")

    def select_product_by_list_id(self, product, list_id):
        self._driver.find_element_by_id(product + "-" + list_id).click()

    def search_ads(self, email):
        self.fill("services_email", email)
        self.button_search.click()
        self.wait.until_loaded()

    def apply_ad_serv(self, services, ad):

        for service in services:
            self.select_product_by_list_id(service, ad)

        self.button_apply.click()
        WebDriverWait(self._driver, 10).until(EC.alert_is_present())
        self._driver.switch_to_alert().accept()
        self.wait.until_loaded()


class ControlPanelSearchForAd(BasePage):
    # Search elements
    search_field = sunbro.FindByID("search_field")
    button_search = sunbro.FindByCSS("input[name='search']")
    message_success = sunbro.FindByCSS("h1")
    message_error = sunbro.FindByCSS(".error")

    button_continue = sunbro.FindByID("apply_action")
    radio_all = sunbro.FindByID("timespan_all")
    frequency = sunbro.FindByID("frequency")
    num_days = sunbro.FindByID("num_days")

    def apply_product_by_ad_id(
            self,
            ad_id,
            prod,
            prod_type=None,
            ab_frequency=None,
            ab_num_days=None):
        selector = Select(
            self._driver.find_element_by_id('products_to_apply_' + ad_id))
        selector.select_by_value(prod)
        self._driver.find_element_by_id('apply_product_' + ad_id).click()
        WebDriverWait(self._driver, 10).until(EC.alert_is_present())
        self._driver.switch_to_alert().accept()
        if prod_type:
            self.wait.until(lambda x: len(self._driver.window_handles) > 1)
            self._driver.switch_to_window(self._driver.window_handles[1])
            self._driver.find_element_by_id(prod + '_' + prod_type).click()
            self.button_continue.click()

        if ab_frequency and ab_num_days:
            self.wait.until(lambda x: len(self._driver.window_handles) > 1)
            self._driver.switch_to_window(self._driver.window_handles[1])
            selector = Select(self.frequency)
            selector.select_by_value(ab_frequency)
            self.num_days.clear()
            self.num_days.send_keys(ab_num_days)
            time.sleep(10)
            self.button_continue.click()

    def check_product_available_by_ad_id(self, ad_id, prod):
        selector = Select(
            self._driver.find_element_by_id('products_to_apply_' + ad_id))
        return prod in selector.options


class ControlPanelSearchByReviewer(BasePage):

    reviewer = sunbro.FindByCSS("select[name='reviewer']")
    ad_state = sunbro.FindByCSS("select[name='ad_state']")
    search_button = sunbro.FindByCSS("input[name='search']")


class ControlPanelPackSearchAndAssign(BasePage):
    search_box = sunbro.FindByID("pack_autos-email")
    search_button = sunbro.FindByID("search-email")
    email_error = sunbro.FindByID("email_error")

    table_active = sunbro.FindByID("table_active")
    table_expired = sunbro.FindByID("table_expired")

    pack_slots = sunbro.FindByID("pack_slots")
    expiration_date = sunbro.FindByID("expiration_date")
    service_order = sunbro.FindByID("service_order")
    service_amount = sunbro.FindByID("service_amount")
    pack_assign_buttom = sunbro.FindByID("pack_assign_buttom")
    expire_pack_button = sunbro.FindByCSS(".expire_link")

    pack_okay = sunbro.FindByCSS(".okey")

    def assign_pack(self, wd, type, slots, date, mail, order=None, mount=None):
        cp = ControlPanel(wd)
        if type == 'auto':
            cp.go_to_option_in_menu('Buscar y/o Asignar Pack Auto')
        elif type == 'inmo':
            cp.go_to_option_in_menu('Buscar y/o Asignar Pack Inmo')
        self.search_box.send_keys(mail)
        self.search_button.click()
        self.pack_slots.send_keys(slots)
        self.expiration_date.send_keys(date)
        if order is not None and mount is not None:
            self.service_order.send_keys(order)
            self.service_amount.send_keys(mount)
        self.service_amount.send_keys(Keys.TAB)
        self.pack_assign_buttom.click()
        wd.switch_to_alert().accept()


class ControlPanelFixPackSlots(BasePage):
    submit = sunbro.FindByID("fix_pack_slots_button")
    results_table = sunbro.FindByID("fix_slots_results")

    def get_results(self):
        return self.get_table_data('results_table')


class ControlPanelUpdateProParam(BasePage):

    search_by_uid = sunbro.FindByCSS('[name="search_type"][value="uid"]')
    search_by_email = sunbro.FindByCSS('[name="search_type"][value="email"]')
    search_type = sunbro.FindByCSS('[name="search_type"]')
    search_box = sunbro.FindByID("search_box")
    search_button = sunbro.FindByID("search_button")
    inmo_checkbox = sunbro.FindByID("real_estate_0")
    car_checkbox = sunbro.FindByID("car_0")
    error_message = sunbro.FindByCSS(".error")

    accounts_table = sunbro.FindByID("table_account_pripro")
    submit_button = sunbro.FindByID("submit_pro_accounts")
    message = sunbro.FindByID("pack_message")
    first_pack_type = sunbro.FindByID('first_pack_type')

    def select_pro_type_by_id(self, checkbox_id):
        self.accounts_table.find_element_by_id(checkbox_id).click()

    def make_me_pro(self, wd, mail, type=None):
        """ Make a user pro in inmo or car, or both """
        cp = ControlPanel(wd)
        cp.go_to_option_in_menu('Cambiar estado de usuario')
        cp = ControlPanelUpdateProParam(wd)
        cp.search_by_email.click()
        cp.search_box.send_keys(mail)
        cp.search_button.click()
        if type == 'auto':
            cp.car_checkbox.click()
        elif type == 'inmo':
            cp.inmo_checkbox.click()
        else:
            cp.car_checkbox.click()
            cp.inmo_checkbox.click()
        cp.submit_button.click()

    def get_table_data(self, table_name=None, table_elem=None):
        """ Get the cell values/text in a table """
        from selenium.common.exceptions import NoSuchElementException
        table_values = []
        if not table_elem:
            if not table_name:
                raise Exception(
                    "What u doing??? set a name or give an element please")
            table_elem = getattr(self, table_name)
        for row in table_elem.find_elements_by_tag_name('tr'):
            row_values = []
            for cell in row.find_elements_by_tag_name('td'):
                try:
                    element = cell.find_element_by_css_selector('input')
                except NoSuchElementException:
                    continue
                input_value = ''
                if element.get_attribute('type') in ['checkbox']:
                    input_value = bool(element.get_attribute('checked'))
                else:
                    input_value = element.get_attribute('value')
                row_values.append(input_value)
            table_values.append(row_values)
        return table_values


class ControlPanelRefund(BasePage):

    date_start = sunbro.FindByID('f_start')
    date_end = sunbro.FindByID('f_end')
    filter = sunbro.FindByID('filter')
    search_button = sunbro.FindByID("search_action")
    search_email = sunbro.FindByID("search_and_send_email")
    email = sunbro.FindByID("emailto")
    refund_table = sunbro.FindByID("refund_table")
    without_result = sunbro.FindByCSS("h2")
    email_success = sunbro.FindByID("email_success")
    error_email = sunbro.FindByID("err_emailto")
    error_date_start = sunbro.FindByID("err_date_start")
    error_date_end = sunbro.FindByID("err_date_end")

    def click_on_link(self, id):
        self.refund_table.find_element_by_id(id).click()


class ControlPanelOpMsg(BasePage):

    type_msg_direct = sunbro.FindByID("type_d")
    type_msg_global = sunbro.FindByID("type_g")
    email = sunbro.FindByID("email")
    expiration_date = sunbro.FindByID("expiration_date")
    expiration_time = sunbro.FindByID("expiration_time")
    source_desktop = sunbro.FindByID("source_desktop")
    source_msite = sunbro.FindByID("source_msite")
    location = sunbro.FindByID("location")
    themsg = sunbro.FindByID("themsg")
    admin_action = sunbro.FindByID("admin_action")
    op_msg_label = sunbro.FindByCSS(".op-msg-label")

    list = sunbro.FindAllByCSS(".op-msg-list")

    def ok(self):
        self.admin_action.click()

    def check_errors(self, ref, **errors):
        for err, value in errors.items():
            ref.assertEquals(self._driver.find_element_by_id(err).text, value)

    def _remove_index(self, str):
        return str[3:]

    def check_list(self, ref, list):
        i = 0
        for elem in self.list:
            ref.assertIn(self._remove_index(elem.text), list)
            i = i + 1

    def expire(self, source, location):
        self._driver.find_element_by_id(
            "exp_" +
            source +
            "_" +
            location).click()


class ControlPanelBanners(BasePage):

    banner_type = sunbro.FindByID('thebanner')
    ok = sunbro.FindByID('admin_action')

    turn_on = sunbro.FindByID('turn_on')
    turn_off = sunbro.FindByID('turn_off')
    all_banners_off = sunbro.FindByID('all_banner_turn_off')
    active_banners = sunbro.FindAllByCSS('active_banner_cb')

    all_regions = sunbro.FindByID('selectAllIDReg')
    all_categories = sunbro.FindByID('selectAllID')

    sources = sunbro.FindAllByCSS('.c_source')
    positions = sunbro.FindAllByCSS('.c_position')
    types = sunbro.FindAllByCSS('.c_tipo')
    regions = sunbro.FindAllByCSS('.c_regions')
    categories = sunbro.FindAllByCSS('.c_category')
    generic_types = sunbro.FindAllByCSS('.c_type')
    NGA_banners = sunbro.FindAllByCSS('.c_enabled')

    success = sunbro.FindByID('success_message')

    # FIXME!!!!
    # this should no be here
    # ControlPanelBanners deberia heradar de ControlPanelPage que deberia
    # tener una funcion logout
    def logout(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?logout')

    def selectCheckbox(self, params):
        for param, value in params.items():
            if value != 'all':
                fields = getattr(self, param)
                for x in value.split(','):
                    for element in fields:
                        if x == 'on' or x == 'off':
                            if element.is_selected() != x:
                                element.click()
                        else:
                            if element.get_attribute('value') == x:
                                element.click()
            else:
                field = getattr(self, 'all_{}'.format(param))
                field.click()

    def turnOn(self, banner_type, params):
        self.fill('banner_type', banner_type)
        self.ok.click()
        self.selectCheckbox(params)
        self.turn_on.click()
        self.wait.until_present('success')

    def turnOff(self, banner_type, params):
        self.fill('banner_type', banner_type)
        self.ok.click()
        self.selectCheckbox(params)

        self.all_banners_off.click()
        self.turn_off.click()
        self.wait.until_present('success')

    def old_way(self, key, state):
        """
        Parse and execute directly using old format used by trans
        Example data
          query: 's/yapodev.cl/Seguro/Araucania/Autos-camionetas-y-4x4'
          on_of: 1
        """
        print(key)
        data = key.split('/')
        banner_type = data[2]
        for type in banner_type.split(','):
            dic = {
                'types': data[0],
                'sources': data[1],
                'regions': data[3],
                'categories': data[4]
            }
            if state == '1':
                self.turnOn(type, dic)
            else:
                self.turnOff(type, dic)

    def way_listing(self, key, state):
        """
        Parse and execute directly using old format used by trans
        Example data
          query: 'yapodev.cl/Araucania/Autos-camionetas-y-4x4/position_6'
          on_of: 1
        """
        data = key.split('/')
        dic = {
            'sources': data[0],
            'regions': data[1],
            'categories': data[2],
            'positions': data[3]
        }
        if state == '1':
            self.turnOn('Listing', dic)
        else:
            self.turnOff('Listing', dic)


class ControlPanelPromotionalPages(BasePage):
    # insert
    title = sunbro.FindByID('title')
    friendly_url = sunbro.FindByID('friendly-url')
    select = sunbro.FindByID('select')
    add_cat = sunbro.FindByClass('add_cat')
    image_input = sunbro.FindByID('image-input')
    img_file = sunbro.FindByName('file')
    image_container = sunbro.FindByID('image-container')
    start_date = sunbro.FindByID('start-date')
    end_date = sunbro.FindByID('end-date')
    submit = sunbro.FindByID('submit')
    results = sunbro.FindByID('results')
    remove_search = sunbro.FindAllByCSS('.removeSearch')
    category = sunbro.FindByID('category')
    category_div = sunbro.FindAllByCSS('.cat-div')
    image = sunbro.FindByID('image')
    draft_message = sunbro.FindByID('is-draft')
    discard_btn = sunbro.FindByID('delete')

    # list
    pp_new = sunbro.FindByID('pp_new')
    pp_new_btn = sunbro.FindByID('pp_new_btn')

    pp_list = sunbro.FindByID('pp_list')
    pp_list_title = sunbro.FindByCSS('.table-main h1')
    pp_list_message = sunbro.FindByID('pp_list_msg')
    pp_list_table = sunbro.FindByID('pp_list_table')
    pp_list_table_title = sunbro.FindAllByCSS('.pp_list_table_title')
    pp_list_table_edit = sunbro.FindAllByCSS('.pp_list_table_edit')
    pp_list_table_delete = sunbro.FindAllByCSS('.pp_list_table_delete')

    # edit
    pp_categories = sunbro.FindAllByCSS('.category_input')
    pp_word_input = sunbro.FindAllByCSS('.word_input')
    pp_add_word_btn = sunbro.FindAllByCSS('.add_button')
    pp_remove_word_btn = sunbro.FindAllByCSS('.block_button')
    pp_added_words = sunbro.FindAllByCSS('.add_input')
    pp_blocked_words = sunbro.FindAllByCSS('.block_input')

    # messages
    PP_MSG_ERROR = 'Ha ocurrido un error en la operaci�n.'
    PP_MSG_SUCCESS = 'La operaci�n se ha realizado con �xito.'
    PP_MSG_TITLE = 'Administraci�n de P�ginas Promocionales'
    PP_MSG_LIST_EMPTY = 'Actualmente no existen p�ginas promocionales.'
    PP_MSG_DELETE_SUCCESS = 'La P�gina Promocional fue eliminada con �xito.'
    PP_MSG_ALERT_BASE = '�Est�s seguro que deseas eliminar la p�gina '
    PP_MSG_ALERT_CATEGORY = '�Est�s seguro que deseas eliminar la b�squeda?'
    PP_MSG_DUPLICATED_TITLE = 'Ya existe una p�gina promocional con el t�tulo ingresado.'
    PP_MSG_DUPLICATED_URL = 'Ya existe una p�gina promocional con la misma URL.'
    PP_MSG_TITLE_TOO_LONG = 'El t�tulo es muy largo.'
    PP_MSG_DATE_INVALID = 'La fecha es inv�lida'
    PP_MSG_END_DATE_EARLIER = 'La fecha de t�rmino es anterior a la fecha de comienzo.'
    PP_MSG_NO_CATEGORY = 'Selecciona una categor�a'
    PP_DRAFT_MESSAGE = 'Est�s viendo un borrador'
    PP_DRAFT_DELETE = 'Descartar'

    def go(self, page="editPage"):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=PPages&a=' + page)

    def fill(self, name, value):
        if name == 'added_words':
            if len(self.pp_word_input) > 0 and len(self.pp_add_word_btn):
                self.pp_word_input[0].clear()
                self.pp_word_input[0].send_keys(value)
                self.pp_add_word_btn[0].click()
        elif name == 'blocked_words':
            if len(self.pp_word_input) > 0 and len(self.pp_remove_word_btn):
                self.pp_word_input[0].clear()
                self.pp_word_input[0].send_keys(value)
                self.pp_remove_word_btn[0].click()
        elif name == 'category':
            items = len(self.remove_search)
            for i in range(0, items):
                self.remove_query(0)
            if not isinstance(value, list):
                value = [value]
            for val in value:
                super().fill(name, val)
                self.add_cat.click()
                self.wait.until(
                    lambda x: self.category_div[0].size['width'] >= 400)
        elif name in {'start_date', 'end_date'}:
            self._driver.execute_script(
                "$('#{}').val('{}')".format(
                    name.replace(
                        '_',
                        '-'),
                    value))
        elif name == 'image':
            utils.upload_image(value, self.image_input)
            self.wait.until(
                lambda x: self.image_container.size['width'] >= 600)
        else:
            super().fill(name, value)

    def fill_form(self, **kwargs):
        order = ['image', 'title', 'start_date', 'end_date',
                 'category', 'added_words', 'blocked_words']
        form_order = [(k, kwargs.pop(k)) for k in order if k in kwargs]
        self.ordered_fill_form(form_order)

    def remove_query(self, index):
        self.remove_search[index].click()
        self.wait.until(EC.alert_is_present())
        alert = self._driver.switch_to_alert()
        alert.accept()

    def remove_promo_pages(self, wd):
        ppages_to_delete = len(self.pp_list_table_title)
        for i in range(ppages_to_delete):
            self.pp_list_table_delete[0].click()
            alert = wd.switch_to_alert()
            alert.accept()


class ControlPanelPromotionalPagesPreview(BasePage):
    header = sunbro.FindByClass('admin-header')
    footer = sunbro.FindByClass('admin-footer')
    preview_title = sunbro.FindByClass('admin-title')
    preview_subtitle = sunbro.FindByClass('admin-text')
    publish_btn = sunbro.FindByID('publish-btn')
    return_btn = sunbro.FindByID('return-btn')

    PP_PREVIEW_TITLE = "Vista Previa"
    PP_PREVIEW_SUBTITLE = "* La pesta�a 'Todos' no funciona en la Vista Previa"
    PP_PUBLISH_BTN = "Publicar"
    PP_RETURN_BTN = "Volver"


@AttrFinder
class BannerRow(Component):

    url = sunbro.FindByCSS('.cell:nth-child(1)')
    platform = sunbro.FindByCSS('.cell:nth-child(2)')
    position = sunbro.FindByCSS('.cell:nth-child(3)')
    region = sunbro.FindByCSS('.cell:nth-child(4)')
    category = sunbro.FindByCSS('.cell:nth-child(5)')
    start_date = sunbro.FindByCSS('.cell:nth-child(6)')
    end_date = sunbro.FindByCSS('.cell:nth-child(7)')
    _delete = sunbro.FindByCSS('.delete')

    _dump_map = {
        'url': Component.text,
        'platform': Component.text,
        'position': Component.text,
        'region': Component.text,
        'category': Component.text,
        'start_date': Component.text,
        'end_date': Component.text,
    }

    def delete(self, cancel=False):
        self._delete.click()
        self._wait.until(EC.alert_is_present())
        alert = self._driver.switch_to_alert()
        alert_action = alert.dismiss if cancel else alert.accept
        alert_action()


class ControlPanelPromotionalPageBanners(BasePage):

    # banners
    pp_id = sunbro.FindByID('pp_id')
    image = sunbro.FindByID('banner_img')
    start_date = sunbro.FindByID('start_date')
    end_date = sunbro.FindByID('end_date')
    send_button = sunbro.FindByID('send_button')
    img_container = sunbro.FindByID('img_container')
    img = sunbro.FindByCSS('#img_container img')
    message = sunbro.FindByID('message')
    pp_banner = sunbro.FindByCSS('.ppages-banner a img')
    _list = sunbro.FindAllByCSS('.table_banners .row:not(.header)')
    title = sunbro.FindByClass('title')
    edit = sunbro.FindAllByCSS('.edit')

    def get_banner(self, **data):
        ks = set(data.keys())
        for banner in self.list:
            if banner.dump(ks) == data:
                return banner

    EDIT_TITLE = 'Editar banner'
    CREATE_TITLE = 'Crear banner'

    def fill(self, name, value):
        if name in {'platform', 'position'}:
            selector = 'input[name="{}"][value="{}"]'.format(name, value)
            for element in self._driver.find_elements_by_css_selector(selector):
                # Only one is displayed at a time
                if element.is_displayed():
                    element.click()
                    self.wait.until(
                        lambda x: element.get_attribute('checked') == 'true')
                    return
        elif name in {'categories', 'regions'}:
            for v in value:
                selector = 'input[name="{}[]"][value="{}"]'.format(name, v)
                self._driver.find_element_by_css_selector(selector).click()
                self.wait.until(lambda x: self._driver.find_element_by_css_selector(
                    selector).get_attribute('checked') == 'true')
        elif name == 'image':
            self.wait.until_visible('image')
            utils.upload_image(value, self.image)
            self.wait.until(lambda x: self.message.text == "Imagen cargada")
        elif name == 'send_button':
            if value:
                self.wait.until_visible('send_button')
                self.send_button.click()
        else:
            super().fill(name, value)

    def fill_form(self, **kwargs):
        order = [
            'pp_id',
            'start_date',
            'end_date',
            'platform',
            'position',
            'categories',
            'all_regions',
            'regions',
            'image',
            'send_button']
        form_order = [(k, kwargs.pop(k)) for k in order if k in kwargs]
        self.ordered_fill_form(form_order)

    @property
    def list(self):
        return [BannerRow(self._driver, row) for row in self._list]


class ControlPanelPartnerReports(BasePage):

    # partners stuff
    report_type = sunbro.FindByID('report_type')
    partner = sunbro.FindByID('partner_select')
    month = sunbro.FindByID('month_select')
    year = sunbro.FindByID('year')
    email = sunbro.FindByID('email')
    submit = sunbro.FindByID('submit')
    error = sunbro.FindByCSS('.error')
    success = sunbro.FindByClass('okey')

    MONTH_OPTIONS = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
    MENU_OPTION = 'Enviar reportes de partners'
    EMAIL_SENT = 'El email fue enviado correctamente'
    NO_AD_REPLIES = 'No se encontraron adreplies para este partner'
    NO_CHANGES = 'No hay cambios durante el mes seleccionado'
    ERROR_INVALID_EMAIL = 'El email no es v�lido'
    ERROR_INVALID_YEAR = 'El a�o no es v�lido'

    def send_report(self, report_type, partner, email, month=None, year=None):
        self.ordered_fill_form([
            ('report_type', report_type),
            ('partner', partner),
            ('email', email),
        ])
        if month:
            self.fill('month', month)
        if year:
            self.fill('year', year)
        self.submit.click()

    def go(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            '?m=Packs&a=partnerAdReport')
        return self


class YapesosReport(BasePage):

    submit = sunbro.FindByCSS('input[type="submit"]')
    date_from = sunbro.FindByID('report_from')
    date_to = sunbro.FindByID('report_to')
    report_table = sunbro.FindByID('report-table')

    def fill_report(self, f=None, t=None):
        if f:
            self.fill('date_from', f)
        if t:
            self.fill('date_to', t)
        self.submit.click()
        self.wait.until_loaded()

    def get_report(self):
        return self.get_table_data('report_table')


class YapesosAssign(BasePage):
    search_button = sunbro.FindByID('assign_search')
    send_button = sunbro.FindByID('assign_send')
    email = sunbro.FindByID('email')
    user_name = sunbro.FindByID('user_name')
    service_order = sunbro.FindByID('service_order')
    yapesos = sunbro.FindByID('yapesos')
    bonus = sunbro.FindByID('bonus')
    installments = sunbro.FindByID('installments')
    err_email = sunbro.FindByID('err_email')
    err_service_order = sunbro.FindByID('err_service_order')
    err_yapesos = sunbro.FindByID('err_yapesos')
    err_bonus = sunbro.FindByID('err_bonus')
    err_total = sunbro.FindByID('err_total')
    err_api = sunbro.FindByID('err_api')
    success = sunbro.FindByClass('yapesos-success')

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=yapesos&a=assign')
        return self


class YapesosHistory(BasePage):
    email = sunbro.FindByID('email')
    search_button = sunbro.FindByID('history_search')
    err_email = sunbro.FindByID('err_email')
    user_name = sunbro.FindByID('user_name')
    history_from = sunbro.FindByID('history_from')
    err_hfrom = sunbro.FindByID('err_hfrom')
    history_to = sunbro.FindByID('history_to')
    err_to = sunbro.FindByID('err_to')
    history_submit = sunbro.FindByID('history_submit')

    download_link = sunbro.FindByID('download_report')
    history_table = sunbro.FindByID('history-table')

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=yapesos&a=history')
        return self

    def get_history(self):
        return self.get_table_data('history_table')


class YapesosEdit(BasePage):
    email = sunbro.FindByID('email')
    search_button = sunbro.FindByID('edit_search')
    err_email = sunbro.FindByID('err_email')
    user_name = sunbro.FindByID('user_name')
    history_from = sunbro.FindByID('history_from')
    err_hfrom = sunbro.FindByID('err_hfrom')
    history_to = sunbro.FindByID('history_to')
    err_to = sunbro.FindByID('err_to')
    edit_submit = sunbro.FindByID('edit_submit')

    list_table = sunbro.FindByID('history-table')
    list_rows = sunbro.FindAllByCSS('.data-row')
    list_hidden = sunbro.FindAllByCSS('.hidden')
    err_action = sunbro.FindByID('err_action_on_credit')
    err_yapesos = sunbro.FindByID('err_yapesos')

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=yapesos&a=edit')
        return self

    def get_list(self):
        return self.get_table_data('list_table')

    def show_row_form(self, credit_id):
        form = self._driver.find_element_by_id('form_{}'.format(credit_id))
        form_visible = form.is_displayed()
        if not form_visible:
            for x in self.list_rows:
                if x.get_attribute('id') == 'row_{}'.format(credit_id):
                    x.find_element_by_css_selector('.edit-yapeso').click()
        return form

    def send_edit_form(self, credit_id, action, value):
        form = self.show_row_form(credit_id)
        if form is not None:
            radios = form.find_elements_by_name('action_on_credit')
            yapesos = form.find_element_by_name('yapesos')
            submit = form.find_element_by_name('edit_submit')
            for x in radios:
                if x.get_attribute('value') == action:
                    x.click()

            yapesos.clear()
            yapesos.send_keys(value)
            submit.click()

            alert = self._driver.switch_to_alert()
            alert.accept()


class ControlPanelPartnerAdd(BasePage):

    name = sunbro.FindByID('Name')
    keyName = sunbro.FindByID('KeyName')
    market = sunbro.FindByID('Market')
    mailTo = sunbro.FindByID('MailTo')
    connData = sunbro.FindByID('ConnData')
    protocol = sunbro.FindByID('Protocol')
    imgProtocol = sunbro.FindByID('ImgProtocol')
    imgData = sunbro.FindByID('imgData')
    imgUser = sunbro.FindByID('ImgUser')
    imgPass = sunbro.FindByID('ImgPass')
    imgDomain = sunbro.FindByID('ImgDomain')
    transmit = sunbro.FindByID('transmit')
    fileName = sunbro.FindByID('FileName')
    pathFile = sunbro.FindByID('PathFile')
    pathImg = sunbro.FindByID('PathImg')
    insDelay = sunbro.FindByID('InsDelay')
    delimiter = sunbro.FindByID('Delimiter')
    rulesCheck = sunbro.FindByID('rules')
    rules = sunbro.FindByID('Rules')
    iRules = sunbro.FindByID('IRules')
    apiPoint = sunbro.FindByID('apiPoint')
    converter = sunbro.FindByID('converter')
    downloadUser = sunbro.FindByID('DownloadUser')
    downloadPassword = sunbro.FindByID('DownloadPassword')
    downloadDomain = sunbro.FindByID('DownloadDomain')
    downloadSeparator = sunbro.FindByID('DownloadSeparator')
    downloadEncoding = sunbro.FindByID('DownloadEncoding')
    downloadFilePath = sunbro.FindByID('DownloadFilePath')
    downloadProtocol = sunbro.FindByID('DownloadProtocol')
    uploadUser = sunbro.FindByID('UploadUser')
    uploadPassword = sunbro.FindByID('UploadPassword')
    uploadDomain = sunbro.FindByID('UploadDomain')
    uploadFilePath = sunbro.FindByID('UploadFilePath')
    uploadProtocol = sunbro.FindByID('UploadProtocol')
    runTime = sunbro.FindByID('RunTime')
    submit = sunbro.FindByID('partner_submit')

    BIFROST_NOT_AVAILABLE = 'El servicio Bifrost no se encuentra disponible'
    PARTNER_ALREADY_EXISTS = 'Ya existe un partner con el Id indicado'
    PARTNER_CREATED_OK = 'Partner creado correctamente'

    errorMsg = sunbro.FindByClass('msg-error')
    successMsg = sunbro.FindByClass('msg-success')
    statusMsg = sunbro.FindByID('msg-status')

    def go(self, wd, option, do_login=False, user='', passwd=''):
        cp = ControlPanel(wd)
        cp.go()
        cp.login_and_go_to_option(option, do_login, user, passwd)
        return self

    def submit_page(self):
        self.submit.click()
        return self


class ControlPanelPartnerEdit(BasePage):

    Active = sunbro.FindByID('Active')
    Inactive = sunbro.FindByID('Inactive')

    PARTNER_UPDATE_OK = 'Partner actualizado correctamente'
    BIFROST_NOT_AVAILABLE = 'El servicio Bifrost no se encuentra disponible'

    errorMsg = sunbro.FindByClass('msg-error')
    successMsg = sunbro.FindByClass('msg-success')
    statusMsg = sunbro.FindByID('msg-status')

    def go(self, wd, option, do_login=False, user='', passwd=''):
        cp = ControlPanel(wd)
        cp.go()
        cp.login_and_go_to_option(option, do_login, user, passwd)
        return self

    def get_partner_data(self):
        partnerData = self.get_table_data("Active")
        partnerData.extend(self.get_table_data("Inactive"))
        return partnerData

    def get_table_action(self, partner_key_name, action):
        """ Get the specified action in the partners in a table """
        tables = ["Active", "Inactive"]
        for table in tables:
            table_elem = getattr(self, table)
            for row in table_elem.find_elements_by_tag_name('tr'):
                cell_values = row.find_elements_by_tag_name('td')
                if len(cell_values) > 0:
                    value = cell_values[0].text
                    if value == partner_key_name:
                        actionLinks = cell_values[
                            2].find_elements_by_tag_name('a')
                        for actionLink in actionLinks:
                            if actionLink.get_attribute('id') == action:
                                actionLink.click()
                                return


class YapofactAdminReport(BasePage):

    search = sunbro.FindByID('submit')
    date_from = sunbro.FindByID('from')
    date_to = sunbro.FindByID('to')
    email = sunbro.FindByID('email')
    name = sunbro.FindByID('user_name')
    category = sunbro.FindByID('category')
    ad_id = sunbro.FindByID('ad_id')
    report_table = sunbro.FindByID('table')
    new_email = sunbro.FindByID('new_email')
    resend = sunbro.FindByID('send_email')
    download = sunbro.FindByID('download')

    err_name = sunbro.FindByID('err_user_name')
    err_ad_id = sunbro.FindByID('err_ad_id')

    def fill(self, name, value):
        if value is not None:
            super().fill(name, value)

    def fill_form(self, **kwargs):
        order = ['date_from', 'date_to', 'email', 'name',
                 'category', 'ad_id']
        form_order = [(k, kwargs.pop(k)) for k in order if k in kwargs]
        self.ordered_fill_form(form_order)

    def get_report(self):
        return self.get_table_data('report_table')

    def clean_report(self):
        values = {
            'email': '',
            'name': '',
            'category': '',
            'ad_id': ''
        }
        self.fill_form(**values)

class SubscriptionsCreate(BasePage):
    email = sunbro.FindByID('s_email')
    search = sunbro.FindByID('assign_search')

    account_info = sunbro.FindByClass('Listing')
    edit_account = sunbro.FindByPartialLinkText('Editar cuenta')
    edit_email_tab = sunbro.FindByClass('edit-email')

    product = sunbro.FindByName('product_id')
    doc_type = sunbro.FindByName('billingType')
    admin_email = sunbro.FindByName('adminEmail')
    user_name = sunbro.FindByName('s_name')

    create = sunbro.FindByID('create')
    confirm_msg = "Confirma que desea crear esta subscripci�n"

    success = sunbro.FindByCSS('h2.success')
    success_msg = "Subscripci�n creada correctamente"

    error  = sunbro.FindByID('err_create')
    cannot_connect_msg = "CURLE_COULDNT_RESOLVE_HOST"

    missing_parameters_msg = "Faltan par�metros"

    invalid_admin_email = sunbro.FindByID('adminEmailError')
    invalid_admin_email_msg = "Formato no v�lido!"

    invoice_missing_params_msg = "Datos faltantes para poder crear factura"

    no_pro_user = sunbro.FindByCSS('h2.result')
    no_pro_user_msg = "Account is not pro"

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=Subscriptions&a=create")

    def accept_modal(self):
        self._driver.switch_to_alert().accept()

    def switch_to_edit(self):
        self._driver.switch_to_window(self._driver.window_handles[1])
        self.wait.until_loaded()

class SubscriptionsSearch(BasePage):
    email = sunbro.FindByID('s_email')
    search = sunbro.FindByID('assign_search')
    data = sunbro.FindByCSS('.inner-table')
    result = sunbro.FindByCSS('.result')
    acc_not_found = " La cuenta no existe "
    acc_is_not_pro = " Account is not pro "

    down_subs = sunbro.FindByPartialLinkText("Dar de baja")
    up_subs = sunbro.FindByPartialLinkText("Activar")

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + "?m=Subscriptions&a=search")



class PartnerAssociate(BasePage):

    yapoParams = sunbro.FindAllByCSS('.yapoParam')
    partnerParams = sunbro.FindAllByCSS('.partnerParam')
    dictionaries = sunbro.FindAllByCSS('.dict')
    submit = sunbro.FindByID('association-submit')
    errorMsg = sunbro.FindByClass('msg-error')
    successMsg = sunbro.FindByClass('msg-success')
    statusMsg = sunbro.FindByID('msg-status')

    keyInput = None
    valueInput = None
    addBtn = None
    removeSelector = None

    BIFROST_NOT_AVAILABLE = 'El servicio Bifrost no se encuentra disponible'
    PARTNER_ALREADY_EXISTS = 'Ya existe un partner con el Id indicado'
    PARTNER_CREATED_OK = 'Partner creado correctamente'
    ALERT_EMPTY_DICT = 'Ingrese un nombre y un valor'
    PARTNER_MISSING_SUBJECT = 'No ha sido posible generar un t�tulo para el aviso'

    def getParamLength(self):
        return len(self.yapoParams)

    def selectYapoParam(self, index, value):
        selectorStr = 'param_{}'.format(index)
        sel = Select(self._driver.find_element_by_id(selectorStr))
        sel.select_by_visible_text(value)

    def init(self, index, rm=None):
        keySelector = '#dict-form-{} .dict-form-key'.format(index)
        valueSelector = '#dict-form-{} .dict-form-value'.format(index)
        addSelector = '#dict-form-{} .dict-form-add'.format(index)

        self.keyInput = self._driver.find_element_by_css_selector(keySelector)
        self.valueInput = self._driver.find_element_by_css_selector(
            valueSelector)
        self.addBtn = self._driver.find_element_by_css_selector(addSelector)

        if rm:
            removeSelector = '#dict-form-{} .dict-form-remove'.format(index)
            self.removeBtn = self._driver.find_element_by_css_selector(
                removeSelector)

    def addDictionary(self, index, dictionary):
        if not dictionary:
            return False

        dictSelector = 'dict-{}'.format(index)
        self._driver.find_element_by_id(dictSelector).click()

        self.init(index)
        for row in dictionary:
            self.keyInput.send_keys(row['key'])
            self.valueInput.send_keys(row['value'])
            self.addBtn.click()

            alert = self._alertPresent()
            if alert:
                self.keyInput.clear()
                self.valueInput.clear()

    def getRowDictionary(self, index=0):
        if index > len(self.yapoParams):
            print("fill_row: index out of range")
            return {}

        dict = []
        dictSelector = '#dict-hidden-{}'.format(index)
        dictTxt = self._driver.execute_script(
            "return $('{}').val();".format(dictSelector))

        aux = [i for i in dictTxt.split(";") if i != '']
        for i in aux:
            values = i.split(":")
            obj = {}
            obj['key'] = values[0]
            obj['value'] = values[1]
            dict.append(obj)

        return dict

    def isDictVisible(self, index=0):
        try:
            dictSelector = 'dict-td-{}'.format(index)
            dictionary = self._driver.find_element_by_id(dictSelector)
            return True
        except NoSuchElementException:
            return False

    def _alertPresent(self):
        try:
            self._driver.switch_to.alert.accept()
            return True
        except NoAlertPresentException:
            return False

    def removeDictionaryRow(self, index, dictIndex):
        self.init(index, 1)
        self.removeBtn[dictIndex].click()

    def fill_row(self, index, yapoParam, dictionary=None):
        if index > len(self.yapoParams):
            print("fill_row: index out of range")
            return False

        self.selectYapoParam(index, yapoParam)
        self.addDictionary(index, dictionary)


class WordsMark(BasePage):

    regex = sunbro.FindByName('regex')
    pureRegex = sunbro.FindByName('pure_regex')
    adTypeAll = sunbro.FindByID('a')
    searchSubject = sunbro.FindByID('search_on_subject')
    searchBody = sunbro.FindByID('search_on_body')
    wordBoundary = sunbro.FindByName('word_boundary')
    color = sunbro.FindAllByName('color')
    frameAd = sunbro.FindByName('frame_ad')
    submit_button = sunbro.FindByCSS('input[type=submit]')
    word_marks_table = sunbro.FindByID('highlighters')

    def fill(self, name, value):
        if value is not None:
            super().fill(name, value)

    def fill_form(self, **kwargs):
        if 'color' in kwargs:
            self.color[kwargs.pop('color')].click()
        if 'category' in kwargs:
            categories = kwargs.pop('category')
            for cat in categories:
                catParent = (cat // 1000) * 1000
                self._driver.find_element_by_id(
                    '{}_{}'.format(
                        catParent,
                        cat)).click()
        if 'warning' in kwargs:
            warningSelect = Select(
                self._driver.find_element_by_name("warning_type"))
            warningSelect.select_by_visible_text(kwargs.pop('warning'))

        order = [
            'regex',
            'pureRegex',
            'adTypeAll',
            'searchSubject',
            'searchBody',
            'wordBoundary',
            'frameAd']
        form_order = [(k, kwargs.pop(k)) for k in order if k in kwargs]
        self.ordered_fill_form(form_order)

    def go(self):
        self._driver.get(conf.CONTROLPANEL_URL + '?m=adqueue&a=highlight')
        self.wait.until_loaded()

    def goAddMark(self):
        self._driver.get(
            conf.CONTROLPANEL_URL +
            '?m=adqueue&a=highlight&action=save')

    def get_marks(self):
        return self.get_table_data('word_marks_table')

    def submit(self):
        self.submit_button.click()
        # this is added to wait for bconfd to make the new values available
        time.sleep(5)


class ControlPanelPartners(BasePage):

    """ Page object for Partners seccion """
    # ADD PARTNER
    # EDIT PARTNER
    # DELETE PARTNER ADS
    partner_name = sunbro.FindByID('partner_name')
    just_check = sunbro.FindByID('just_check')
    partner_submit = sunbro.FindByID('partner_submit')

    results = sunbro.FindAllByCSS('fieldset')

    def go(self, wd, option, do_login=False, user='', passwd=''):
        cp = ControlPanel(wd)
        cp.go()
        cp.login_and_go_to_option(option, do_login, user, passwd)
        return self

    def partner_delete(self, p_name, uncheck_verify=False, accept_alert=True):
        self.partner_name.clear()
        self.partner_name.send_keys(p_name)
        if uncheck_verify:
            self.just_check.click()
        self.partner_submit.click()
        alert = self._driver.switch_to_alert()
        if accept_alert:
            alert.accept()
        else:
            alert.dismiss()

    def get_result(self):
        try:
            compare_with = []
            res = self.results[1].find_elements_by_tag_name('label')
            for row in res:
                compare_with.append(row.text)
            return compare_with
        except:
            return []

class PaymentDeliverySearch(BasePage):
    search_text = sunbro.FindByID('search_text')
    search = sunbro.FindByID('submit')
    external_id = sunbro.FindByID('external_id')
    email = sunbro.FindByID('email')
    phone = sunbro.FindByID('phone')
    list_id = sunbro.FindByID('list_id')

    pd_results = sunbro.FindByID('pd_results')

    err_no_data = sunbro.FindByID('err_no_data')
    err_escrow_id = sunbro.FindByID('err_escrow_id')
    err_user_email = sunbro.FindByID('err_user_email')
    err_user_phone = sunbro.FindByID('err_user_phone')
    err_list_id = sunbro.FindByID('err_list_id')

    NO_DATA = 'No se ha podido encontrar informacion asociada al valor ingresado'
    INVALID_ESCROW_ID = "El escrow id introducido no es valido"
    INVALID_EMAIL = "El email ingresado no es valido"
    INVALID_PHONE = "El n�mero de tel�fono ingresado no es valido"
    INVALID_LIST_ID = "El list id ingresado no es valido"

    reject_sale = sunbro.FindByCSS('.paymentdelivery-input')
    reject_success = sunbro.FindByID('reject_success')

    def go(self, wd, option, do_login=False, user='', passwd=''):
        cp = ControlPanel(wd)
        cp.go()
        cp.login_and_go_to_option(option, do_login, user, passwd)
        return self

    def submit(self):
        self.search.click()
        return self

    def reject(self):
        self.reject_sale.click()
        alert = self._driver.switch_to_alert()
        alert.accept()
        return self

    def selectOption(self, key):
        if key == 'external_id':
            self.external_id.click()
        elif key == 'email':
            self.email.click()
        elif key == 'phone':
            self.phone.click()
        else:
            self.list_id.click() 
        return self

    def fill(self, key, value):
        self.selectOption(key)
        self.search_text.clear()
        self.search_text.send_keys(value)
        return self

from yapo.pages.fileconverter import FileConverter


class ControlPanelActivateAccount(BasePage):

    """ Page object for Activate Account section """

    nameAccount = sunbro.FindByID('nameAccount')
    mailAccount = sunbro.FindByID('mailAccount')
    phoneAccount = sunbro.FindByID('phoneAccount')
    submit = sunbro.FindByID('account_submit')
    message = sunbro.FindByID('message')

    INVALID_EMAIL = 'Confirma que el email est� en el formato correcto'
    INVALID_PHONE = 'Verifica tu n�mero de tel�fono'
    INVALID_NAME = 'Escribe por lo menos dos letras'
    ALREADY_EXISTS = 'Este email ya tiene una cuenta asociada'
    ACTIVE_OK = 'Se ha activado la cuenta de usuario correctamente'

    errorMsg = sunbro.FindByClass('msg-error')
    successMsg = sunbro.FindByClass('msg-success')

    def go(self, wd, option, do_login=False, user='', passwd=''):
        cp = ControlPanel(wd)
        cp.go()
        cp.login_and_go_to_option(option, do_login, user, passwd)
        return self

    def get_success_message(self):
        self.wait.until_visible('message')
        return self.successMsg.text.strip()

    def get_error_message(self):
        self.wait.until_visible('message')
        return self.errorMsg.text.strip()
