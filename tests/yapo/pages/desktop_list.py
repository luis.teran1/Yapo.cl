# -*- coding: latin-1 -*-
from yapo.pages import BasePage
from yapo.pages.base import AdList
from yapo import conf
from selenium.webdriver.support.select import Select
from yapo.pages.tealium import Tealium
from yapo.pages.maps import MapPreview
from lazy import lazy
import sunbro

class Search(BasePage):

    # search box
    search_input = sunbro.FindByCSS('#SearchBox form input')
    categories = sunbro.FindByCSS('#categories')
    region = sunbro.FindByCSS('#region')
    search_button = sunbro.FindByCSS('#SearchBox button')

    def _searchbox(self, query='', category='', region=''):
        self.search_input.send_keys(query)
        self.categories.click()
        self._driver.find_element_by_xpath("//*[contains(text(), '{}')]".format(category)).click()
        self.region.click()
        self._driver.find_element_by_xpath("//*[contains(text(), '{}')]".format(region)).click()
        self.search_button.click()

    def _categorylist(self, category):
        self._driver.find_element_by_partial_link_text(category).click()

    def _regionlist(self, region):
        self._driver.find_element_by_partial_link_text(region).click()

    def go(self):
        self._driver.get(conf.DESKTOP_URL)


class SearchBoxSideBar(BasePage):

    """
    AdList search box side bar
    """
    search_form = sunbro.FindByID("searchmain")
    search_box = sunbro.FindByID('searchtext')
    save_favorite_search = sunbro.FindByID('save-favorite-search')
    category = sunbro.FindByID("catgroup")
    selected_category = sunbro.FindByCSS('#catgroup option[selected]')
    region = sunbro.FindByID('searcharea_expanded')
    search_button = sunbro.FindByID('searchbutton')
    careful_message = sunbro.FindByID("careful_message")
    search_suggestion = sunbro.FindByCSS('.ais_results_message')
    red_failover = sunbro.FindByCSS('.redwarning_failover')
    red_failover_there = sunbro.FindAllByCSS('.redwarning_failover')
    black_failover = sunbro.FindByCSS('.warning_failover')
    ais_result_hint = sunbro.FindByCSS('.ais_results_list')
    result_hint_all = sunbro.FindByCSS('#listing-top tr:nth-child(1) a span')
    msg_no_entries_found = sunbro.FindByCSS('#listing-top td > h1')
    msg_extend_search = sunbro.FindByCSS('#listing-top td > p')

    min_price = sunbro.FindByCSS("select[name='ps']:enabled")
    max_price = sunbro.FindByCSS("select[name='pe']:enabled")

    min_rooms = sunbro.FindByCSS("select[name='ros']:enabled")
    max_rooms = sunbro.FindByCSS("select[name='roe']:enabled")

    min_size = sunbro.FindByCSS("select[name='ss']:enabled")
    max_size = sunbro.FindByCSS("select[name='se']:enabled")

    min_bathrooms = sunbro.FindByCSS("select[name='brs']:enabled")
    max_bathrooms = sunbro.FindByCSS("select[name='bre']:enabled")

    min_condominio = sunbro.FindByCSS("select[name='cos']:enabled")
    max_condominio = sunbro.FindByCSS("select[name='coe']:enabled")

    garage_spaces = sunbro.FindByCSS("select[name='gs']:enabled")

    min_regdate = sunbro.FindByCSS("select[name='rs']:enabled")
    max_regdate = sunbro.FindByCSS("select[name='re']:enabled")
    mileage_min = sunbro.FindByCSS("select[name='ms']:enabled")
    mileage_max = sunbro.FindByCSS("select[name='me']:enabled")
    fuel = sunbro.FindByCSS("select[name='fu']:enabled")
    cartype = sunbro.FindByCSS("select[name='ctp']:enabled")
    gearbox = sunbro.FindByCSS("select[name='gb']:enabled")

    #inmo filters
    estate_type = sunbro.FindByID("estate_type_ret")

    # favorite
    watch_edit_form = sunbro.FindByID("watch_edit_form")

    # motos
    ccs = sunbro.FindByID('cubiccms_ccs')
    cce = sunbro.FindByID('cubiccms_cce')

    # communes
    communes_selector = sunbro.FindByCSS('.multicomune-container .multiselect__head')
    label_communes = sunbro.FindAllByCSS('.multicomune-container span')
    icheck_communes = sunbro.FindAllByCSS('.icheck.commune')
    input_communes_selected = sunbro.FindAllByCSS('.multicomune-container input:checked')

    # car search fields
    brand_selector = sunbro.FindByID('brand_br')
    model_selector = sunbro.FindByID('model_mo')
    brand = brand_selector
    model = model_selector
    regdate_rs = sunbro.FindByID('regdate_rs')
    regdate_re = sunbro.FindByID('regdate_re')

    # condition
    condition_selector = sunbro.FindByID('condition_cond')
    condition = condition_selector

    # gender
    gender_selector = sunbro.FindByID('gender_gend')
    gender = gender_selector

    # checkbox inmo
    type_a = sunbro.FindByID('type_a')
    type_s = sunbro.FindByID('type_s')
    type_u = sunbro.FindByID('type_u')

    job_category_link = sunbro.FindByCSS('#job_category a')
    job_category_1 = sunbro.FindByID('jc_1')
    job_category_5 = sunbro.FindByID('jc_5')
    job_category_7 = sunbro.FindByID('jc_7')

    service_type_1 = sunbro.FindByID('sct_1')
    service_type_5 = sunbro.FindByID('sct_5')
    service_type_7 = sunbro.FindByID('sct_7')

    # Footwear
    footwear_gend = sunbro.FindByCSS('[name="fwgend"]')
    footwear_size = sunbro.FindByCSS('[name="fwsize"]')
    footwear_type_1 = sunbro.FindByCSS('[name="fwtype"][value="1"]')
    footwear_type_2 = sunbro.FindByCSS('[name="fwtype"][value="2"]')
    footwear_type_3 = sunbro.FindByCSS('[name="fwtype"][value="3"]')
    footwear_type_4 = sunbro.FindByCSS('[name="fwtype"][value="4"]')
    footwear_type_5 = sunbro.FindByCSS('[name="fwtype"][value="5"]')
    footwear_type_6 = sunbro.FindByCSS('[name="fwtype"][value="6"]')

    # Override super method to allow communes selection
    def fill(self, name, value):
        if name == 'communes':
            self.communes_selector.click()
            for label in self.label_communes:
                if label.text in value:
                    label.click()
        else:
            super().fill(name, value)

    def open_filters(self):
        pass

    def go(self, slug):
        self._driver.get(conf.DESKTOP_URL + slug)

    def go_to_region(self, region_name):
        self._driver.get('{0}/{1}'.format(conf.DESKTOP_URL, region_name))
        self.wait.until_loaded()


class AdListDesktop(AdList):

    """
    Page object used to interact with the ad list page
    """

    members = {
        'side_search_box': SearchBoxSideBar
    }

    search_form = 'side_search_box'
    adserver_top_frame = sunbro.FindByCSS("#adserver-top *")
    adserver_right_frame = sunbro.FindByCSS("#adserver-sidebar *")
    adsense_footer_a = sunbro.FindByCSS(".google_adsense_section.adBnLike")
    adsense_footer_bc = sunbro.FindByID("dasense-main")
    ads_list = sunbro.FindAllByClass('ad')
    entries_table = sunbro.FindByClass('listing_thumbs')
    titles_ads_list = sunbro.FindAllByCSS('.ad .title')
    communes_ads_list = sunbro.FindAllByCSS('.ad .commune')
    regions_ads_list = sunbro.FindAllByCSS('.ad .region')
    ads_price = sunbro.FindAllByCSS('.ad .price')
    save_as_favorite_button = sunbro.FindByCSS(
        'input[value="Guardar como Favorito"]')
    last_page_link = sunbro.FindByLinkText('�ltima p�gina')
    text_option = sunbro.FindByCSS('#listing-top tr:nth-child(1) li a')
    link = sunbro.FindByCSS('#listing-top tr:nth-child(1) li span')
    no_entries_found = sunbro.FindByLinkText('No se encontraron entradas')
    suggestion_alternative = sunbro.FindByCSS('#listing-top td p:nth-child(2)')
    more_search_suggestions = sunbro.FindByLinkText('M�s sugerencias de b�squeda')
    no_entries_found = sunbro.FindByCSS(
        '.TabContainer .TabContainer tbody tr:nth-child(2) td h1')
    stores_link = sunbro.FindByLinkText('Tiendas')
    listing_container = sunbro.FindByID('listing_container')

    tab_country = sunbro.FindByCSS('.tab_country')
    tab_region = sunbro.FindByCSS('.tab_region')

    first_ad = sunbro.FindByCSS('.ad:first-child')
    first_ad_list = sunbro.FindByCSS('.ad:first-child')
    first_ad_link = sunbro.FindByCSS(".ad:first-child a:first-child")

    listing_top = sunbro.FindByID('listing-top')
    position_6th_da = sunbro.FindByID('position_6th_da')
    position_40th_da = sunbro.FindByID('position_40th_da')

    gallery_list = sunbro.FindByCSS(".gallery-list")

    order_by_price = sunbro.FindByCSS(".sprite-li_price")

    pp_banner = sunbro.FindByCSS('.ppages-banner a img')
    pp_banner_link = sunbro.FindByCSS('.ppages-banner a')

    merken_listing = sunbro.FindByID('listing')
    listing_url = (conf.DESKTOP_URL + '/li')

    banner_L_x111 = 'adserver-on-listing-l-top-inner'
    banner_L_x113 = 'adserver-on-listing-l-sidebar-inner'
    banner_Top = 'adserver-top-inner'
    banner_Right = 'adserver-sidebar-inner'

    def get_first_ad(self):
        return self.first_ad

    def get_ads_price(self):
      return self.ads_price

    def go_merken_listing(self):
        self._driver.get(self.listing_url)
        self.wait.until_loaded()
        return self

    def search_for_ad(self, search_text):
        self.side_search_box.search_box.clear()
        self.side_search_box.search_box.send_keys(search_text)
        self.side_search_box.search_button.click()

    def go_to_ad(self, list_id=None, subject=None):
        if list_id:
            self._driver.find_element_by_id(str(list_id)).find_element_by_tag_name("a").click()
        elif subject:
            self._driver.find_element_by_partial_link_text(subject).click()
        return AdViewDesktop(self._driver)

    def _get_ad_data(self, ad):
        data = {}
        self.find_ad_prop(data, ad, 'id', None, self.elem_id)
        self.find_ad_prop(data, ad, 'date', '.date', self.elem_text)
        self.find_ad_prop(data, ad, 'hour', '.hour', self.elem_text)
        self.find_ad_prop(data, ad, 'title', '.title', self.elem_text)
        self.find_ad_prop(data, ad, 'price', '.price', self.elem_text)
        self.find_ad_prop(data, ad, 'alt_price', '.convertedPrice', self.elem_text)
        self.find_ad_prop(data, ad, 'category', '.clean_links .category', self.elem_text)
        self.find_ad_prop(data, ad, 'region', '.clean_links .region', self.elem_text)
        self.find_ad_prop(data, ad, 'commune', '.clean_links .commune', self.elem_text)
        self.find_ad_prop(data, ad, 'has_address', '.clean_links .icon-location', bool, False)
        self.find_ad_prop(data, ad, 'has_label', '.clean_links .fa-tag', self.elem_text, False)
        return data

    def go(self, args=None):
        args = args if args else "li"
        self._driver.get(conf.DESKTOP_URL + "/" + args)
        self.wait.until_loaded()
        return self

    def open_first_ad(self):
        self.first_ad_link.click()

    def search_for_moto(self, ccs, cce):
        self._driver.get(conf.DESKTOP_URL + "/region_metropolitana/motos")
        """TODO validate ccs, cce values"""
        self.side_search_box.wait.until_present("ccs")
        self.side_search_box.wait.until_present("cce")
        Select(self.side_search_box.ccs).select_by_value(ccs)
        Select(self.side_search_box.cce).select_by_value(cce)
        self.side_search_box.search_button.click()

    def browse(self, region='region_metropolitana', category=None):
        self._driver.get("about:blank")
        if category:
            self._driver.get(
                '{0}/{1}/{2}'.format(conf.DESKTOP_URL, region, category))
        else:
            self._driver.get('{0}/{1}'.format(conf.DESKTOP_URL, region))
        self.wait.until_loaded()

    @property
    def utag_data(self):
        """ return the utag_data dictionary in this page"""
        t = Tealium(self._driver)
        return t.utag_data

    def get_gallery(self, num):
        item = self.gallery_list.find_element_by_css_selector(
            'li:nth-child({0})'.format(num))
        return GalleryAd(item)

    def am_i_here(self):
        return self.is_element_present('listing_container')


class Galleries(BasePage):

    """ Page object used to interact with the galleries in ad list page """

    galleries = sunbro.FindAllByCSS(".gallery-item")
    subject = sunbro.FindByCSS("a .gallery-item-title")

    @lazy
    def ad_galleries(self):
        return [GalleryAd(gallery_ad) for gallery_ad in self.galleries]


class GalleryAd(BasePage):

    subject = sunbro.FindByCSS(".gallery-item__description")
    no_image = sunbro.FindByCSS("a .gallery-image-empty")


class AdViewDesktop(BasePage):

    ad_not_found_text = 'Este aviso ya no est� disponible en yapo.cl'

    dawrapper = sunbro.FindByCSS('.da-wrapper')
    subject = sunbro.FindByCSS('h1')
    body = sunbro.FindByCSS('.description p')
    adparams_box = sunbro.FindByClass('details')
    phone = sunbro.FindByClass('da-phone-num')
    price = sunbro.FindByCSS('.da-detail .da-detail__prices-price')
    referencial_price = sunbro.FindByCSS('.da-detail .da-detail__prices-uf')
    price_final = sunbro.FindByCSS('.price-final')
    reference_price_disclaimer = sunbro.FindByCSS('div.details > span')
    cubiccms = sunbro.FindByCSS('.details tr:nth-child(4) td')
    internal_memory = sunbro.FindByCSS('.details tr:nth-child(2) td')
    ad_id = sunbro.FindByName('id')
    publish_date = sunbro.FindByCSS('.da-detail .da-detail__header-date')
    main_image = sunbro.FindByID('main_image')
    images = sunbro.FindAllByCSS('#extra_imagesB img.thumb_image')

    thumb_arrow_left = sunbro.FindByID('arrow_left')
    thumb_arrow_right = sunbro.FindByID('arrow_right')

    breadcrumb_everywhere = sunbro.FindByID('breadcrumb_everywhere')
    breadcrumb_region = sunbro.FindByID('breadcrumb_region')
    breadcrumb_category = sunbro.FindByID('breadcrumb_category')

    top_prev_link = sunbro.FindByID('top_prev')
    top_back_link = sunbro.FindByID('top_back')
    top_next_link = sunbro.FindByID('top_next')
    bottom_prev_link = sunbro.FindByID('bottom_prev')
    bottom_back_link = sunbro.FindByID('bottom_back')
    bottom_next_link = sunbro.FindByID('bottom_next')

    top_prev_hover = sunbro.FindByCSS('#top_prev')
    top_next_hover = sunbro.FindByCSS('#top_next')
    bottom_prev_hover = sunbro.FindByCSS('#bottom_prev')
    bottom_next_hover = sunbro.FindByCSS('#bottom_next')

    ad_reply_title = sunbro.FindByCSS('.contact-info > p')
    ad_reply_name_input = sunbro.FindByID('your_name')
    ad_reply_email_input = sunbro.FindByID('user_email')
    ad_reply_phone_input = sunbro.FindByID('phone')
    ad_reply_message_input = sunbro.FindByID('adreply_body')
    ad_reply_copy_check_button = sunbro.FindByID('cc')
    ad_reply_send_button = sunbro.FindByID('send')
    ad_reply_success = sunbro.FindByID('ad-reply-success')
    ad_reply_facebook_tag = sunbro.FindByCSS(
        'iframe[src="/fb_conversion_ar.html"]')
    ad_reply_error = sunbro.FindByID('adreply_error')
    ad_reply_confirm = sunbro.FindByID('adreply_confirm')

    no_image_state_div = sunbro.FindByClass('no-image-state')
    no_image_car_div = sunbro.FindByClass('no-image-car')
    no_image_general_div = sunbro.FindByClass('no-image-general')

    save_fav_button = sunbro.FindByID('save-fav')
    save_fav_icon = sunbro.FindByCSS('#save-fav i')
    share_facebook_button = sunbro.FindByID('share_facebook')
    share_twitter_button = sunbro.FindByID('share_twitter')
    share_email_button = sunbro.FindByID('btn_sendtip')

    favorited_msg = sunbro.FindByID('favorited')
    icon_heart_full = sunbro.FindByCSS('.icon-yapo.icon-heart-full')

    adserver_right_frame = sunbro.FindByID("adserver-right")
    adserver_right4_frame = sunbro.FindByID("vi-banner-right4")

    abuse_link_button = sunbro.FindByID('abusebutton_link')
    abuse_option_fraud = sunbro.FindByLinkText('Fraude')
    abuse_option_duplicate = sunbro.FindByLinkText('Duplicado')
    abuse_option_wrong_category = sunbro.FindByLinkText(
        u'Categor�a equivocada')
    abuse_option_already_sold = sunbro.FindByLinkText('Producto vendido')
    abuse_option_company = sunbro.FindByLinkText('Aviso de empresa')
    abuse_option_other_reason = sunbro.FindByLinkText('Otros')
    report_form = sunbro.FindByID('reportabuseform')

    product_link_bump = sunbro.FindByID('product_link_1')
    ad_admin_edit_link = sunbro.FindByID('ad_admin_link_editar')
    ad_admin_remove_link = sunbro.FindByID('ad_admin_link_eliminar')

    send_tip_name_input = sunbro.FindByID('tip_name')
    send_tip_email_input = sunbro.FindByID('tip_email')
    send_tip_message_input = sunbro.FindByID('tip_msg')
    send_tip_submit_button = sunbro.FindByCSS(
        '#send_tip_form input[value="Enviar"]')
    send_tip_error_message = sunbro.FindByID('sendtip_error')

    compara_online_link = sunbro.FindByID('comparaonline_link')

    store_logo = sunbro.FindByClass('seller-info__header-avatar')
    store_icon = sunbro.FindByCSS('.icon-store')
    store_link = sunbro.FindByCSS('.view-more-store')
    adsense_links = sunbro.FindByCSS('.adsense-footer')

    ad_watch_saved_success = sunbro.FindByCSS('.icon-yapo.icon-favorite.icon-heart-full')

    # When ad was not found the page of ad_not_found is visible with this
    # stuff:
    ad_not_found_title = sunbro.FindByCSS('.missing h1')

    # Ad_not_found peshobjects
    not_found_speech = sunbro.FindByCSS(".da-missing h1")
    not_found_sub_speech = sunbro.FindByCSS(".da-missing h4")
    not_found_ad_list = sunbro.FindAllByCSS(".list li")
    not_found_first_sug_info_title = sunbro.FindByCSS(
        ".list li:first-child a .info .title")
    not_found_first_sug_info_price = sunbro.FindByCSS(
        ".list li:first-child a .info .price")
    not_found_first_sug_info_date = sunbro.FindByCSS(
        ".list li:first-child a .more .date-hour")
    not_found_first_sug_info_pripro = sunbro.FindByCSS(
        ".list li:first-child a .more .type-user")
    not_found_first_sug_img = sunbro.FindByCSS(
        ".list li:first-child a .thumb-box")
    not_found_ad_list_titles = sunbro.FindAllByCSS(".list li .title")
    not_found_button = sunbro.FindByCSS(".btn.btn-primary")
    not_found_banner = sunbro.FindByID("ad-not-found-top-banner")
    not_found_banner_content = sunbro.FindByCSS("#ad-not-found-top-banner *")

    # cotiza links
    cotiza_credito_link = sunbro.FindByID("cotiza_help_credito")
    cotiza_seguro_link = sunbro.FindByID("cotiza_help_seguro")
    cotiza_banners = sunbro.FindAllByCSS(".pm-content .oas-banner iframe")
    cotiza_raw_link = sunbro.FindByID("cotiza_raw_link")
    cotiza_raw_link_banner = sunbro.FindByCSS("#cotiza_raw_link a")

    label = sunbro.FindByCSS(".fa-tag")
    delete_button = sunbro.FindByID('ad_admin_link_eliminar')

    profile_image = sunbro.FindByCSS('.seller-info__header-avatar')


    THIS_PRICE_IS_REFERENCIAL = '(*) Valor Referencial'

    # autofact banner
    autofact = sunbro.FindByCSS('.board-services__autofact')
    autofact_btn = sunbro.FindByCSS('.board-services__autofact .board-services__banner-button')
    autofact_text = sunbro.FindByCSS('.board-services__banner-description')
    REQUEST_REPORT = "Pedir informe"

    def go(self, list_id, wait_until_loaded=True):
        """Goes to the ad with a given list id"""
        self._driver.get(conf.DESKTOP_URL + '/vi/{0}.htm'.format(list_id))
        if wait_until_loaded:
            self.wait.until_loaded()
        self.wait.until_visible('subject')
        return self

    def send_adreply(self, name, email, phone, message, copy=False):
        self.fill_form(
            ad_reply_name_input=name,
            ad_reply_email_input=email,
            ad_reply_phone_input=phone,
            ad_reply_message_input=message
        )
        if copy:
            self.ad_reply_copy_check_button.click()
        self.ad_reply_send_button.click()
        self.wait.until_visible('ad_reply_success')



    @property
    def seller_info(self):
        print(self.select_shadow_element_by_css_selector('seller-info', 'childNodes[0]'))
        return self.select_shadow_element_by_css_selector('seller-info')

    @property
    def ad_reply_region_commune(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__content .fa-map-marker-alt  + h4')
    @property
    def ad_reply_advertiser_name(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__header-name')
    @property
    def ad_reply_advertiser_phone_img(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__phone-image')


    @lazy
    def map_preview(self):
        """ return a MapPreview element """
        return MapPreview(self._driver)

    @property
    def utag_data(self):
        """ return the utag_data dictionary in this page"""
        t = Tealium(self._driver)
        return t.utag_data

    @lazy
    def get_date_ad(self):
        return self.publish_date.text

    def am_i_here(self):
        return self.is_element_present('dawrapper')

    def has_price(self):
        return self.price.text.strip() != ''

    def has_ref_price(self):
        return self.referencial_price.text.strip() !=''

    def get_adparams(self):
        params = self._driver.find_elements_by_css_selector(".details table tr")
        res = []
        for p in params:
            tmp = []
            tmp.append(p.find_element_by_css_selector("th").text)
            tmp.append(p.find_element_by_css_selector("td").text)
            res.append(tmp)
        return res


class AdDeletePage(BasePage):

    """Page object from desktop edit"""
    description_reason_2 = sunbro.FindByID("dlv2")
    description_reason_6 = sunbro.FindByID("dlv6")
    time_slider = sunbro.FindByID("time-slider")
    time_slider = sunbro.FindByCSS(".time-slider")
    time_sliders = sunbro.FindAllByCSS(".slider-level")
    # Buy buttons
    bum_btn_3 = sunbro.FindByID("bump3")
    bum_btn_4 = sunbro.FindByID("bump4")
    bum_btn_5 = sunbro.FindByID("bump5")
    bum_btn_6 = sunbro.FindByID("bump6")
    lightbox_btn = sunbro.FindByClass('bump-help')
    bump_msg_3 = sunbro.FindByCSS("#reason-details-cont #form_3 h3")
    bump_msg_4 = sunbro.FindByCSS("#reason-details-cont #form_4 h3")
    bump_msg_5 = sunbro.FindByCSS("#reason-details-cont #form_5 h3")
    bump_msg_6 = sunbro.FindByCSS("#reason-details-cont #form_6 h3")
    view_ads = sunbro.FindByID('view-ads')

    forgot_password_link = sunbro.FindByCSS('#reason-details a.solid-link')
    password = sunbro.FindByCSS("#reason-details input[name='passwd']")
    reasons = sunbro.FindByID("reason-details")
    result_message = sunbro.FindByCSS(".delete-success-message")
    error_message = sunbro.FindByCSS("#reason-details-cont .input-error")
    submit = sunbro.FindByCSS("#reason-details-cont button.icon-remove")
    delete_button = sunbro.FindByID("delete1")

    def go(self, list_id):
        url = conf.PHP_URL + '/ai?ca=15_s&id={0}&cmd=delete'.format(list_id)
        self._driver.get(url)
        self.wait.until_loaded()

    def select_reason(self, reason=1):
        self._driver.find_element_by_css_selector(
            '.reason-{0} .iCheck-helper'.format(reason)).click()

    def select_timer(self, timer=1):
        self.time_slider.find_element_by_css_selector(
            'ins[data-radio=r3]'.format(timer)).click()

    def delete_ad(self, password, num_form='4'):
        self.select_reason(num_form)
        if str(num_form) == '1':
            self.select_timer()

        self.wait.until_visible('submit')
        getattr(self, 'password'.format(num_form)).send_keys(password)
        getattr(self, 'submit'.format(num_form)).click()

    def delete_ad_logged(self):
        self.select_reason(4)
        self.wait.until_visible('submit')
        self.wait.until_element_clickable('submit')
        self.submit.click()
        self.wait.until_visible('result_message', 'no lo encuentro!!')


class AdDeleteSuccessPage(BasePage):
    success_message = sunbro.FindByCSS('.delete-success-message')


class ReportForm(BasePage):
    abuse_submit = sunbro.FindByCSS('#reportabuseform input[value="Enviar"]')
    abuse_email = sunbro.FindByID('abuse_email')
    abuse_message = sunbro.FindByID('abuse_message')
    error_message = sunbro.FindByCSS('#reportabuseform #err_abuse_message')
