from http.server import HTTPServer, BaseHTTPRequestHandler
import urllib
import os

class TestServer(BaseHTTPRequestHandler):

    def do_POST(self):
        boundry = self.headers['Content-Type'].split("=")[1]
        length = int(self.headers['Content-Length'])
        content = self.rfile.read(length).decode("utf-8")
        parts = content.split(boundry)
        parts.remove("--")
        parts.remove("--\r\n")
        filecon = ""
        with open("server.got", "w") as f:
            for p in parts:
                lineas = p.split("\n")
                for l in lineas:
                    if not l.isspace() and l != '--':
                        filecon = "{}{}\n".format(filecon, l)
            f.write(filecon)

class ServerRunner:

    def run(self):
        if os.path.isfile("server.got"):
            os.remove("server.got")
        self.llego_post = False
        server_address = ('127.0.0.1', 8000)
        httpd = HTTPServer(server_address, TestServer)
        httpd.timeout = 10 # server should be queried before this
        httpd.handle_request()

