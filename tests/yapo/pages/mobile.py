# -*- coding: latin-1 -*-
from lazy import lazy
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from sunbro import By
from yapo import conf
from yapo import pages
from yapo.component import ComponentComposer
from yapo.component.upselling import UpsellingContainerMobile
from yapo.pages import defaults, HotizontalSelect, RedirectedAway
from yapo.pages.maps import MapForm, MapPreviewMobile
from yapo.utils import Products, Categories, Upselling
from yapo.geography import Regions
from yapo.wait import PageWait
import os
import re
import selenium.webdriver.support.expected_conditions as EC
import sunbro
import time


class DeletePage(pages.BasePage):

    """Page object from mobile delete"""

    reasons = sunbro.FindAllByCSS("#accordion h3")
    reason_1 = sunbro.FindByCSS('#ui-accordion-accordion-header-1[class="strong-label"]')
    reason_2 = sunbro.FindByCSS('#ui-accordion-accordion-header-2[class="strong-label"]')
    reason_3 = sunbro.FindByCSS('#ui-accordion-accordion-header-4[class="strong-label"]')
    reason_4 = sunbro.FindByCSS('#ui-accordion-accordion-header-5[class="strong-label"]')
    reason_5 = sunbro.FindByCSS('#ui-accordion-accordion-header-6[class="strong-label"]')
    active_reason_div = sunbro.FindByCSS(
        '#accordion div[style="display: block;"]')
    password = sunbro.FindByCSS(
        '#accordion div[style="display: block;"] input[name="passwd"]')
    sell_time_options = sunbro.FindAllByCSS(
        '#accordion div[style="display: block;"] .slider-level')
    submit = sunbro.FindByCSS(
        '#accordion div[style="display: block;"] a[name="continue"]')
    result_message = sunbro.FindByCSS('.pm-content .pm-message')
    modal_buttons = sunbro.FindAllByCSS('.pm-content .btn-primary')
    validations = sunbro.FindAllByClass(
        'validation_msg', within='active_reason_div')
    user_text = sunbro.FindByName('user_text', within="active_reason_div")
    password_error = sunbro.FindByID('error1')
    forgot_password_link = sunbro.FindByCSS('a.help[style="display: block;"]')
    bump_promos = sunbro.FindAllByCSS('div.delete-bump-promo')
    delete_buttons = sunbro.FindAllByCSS("a[name=continue]")
    delete_button_r1 = sunbro.FindByCSS("#delete1")
    delete_button_r2 = sunbro.FindByCSS("#delete2")
    delete_button_r3 = sunbro.FindByCSS("#delete4")
    delete_button_r4 = sunbro.FindByCSS("#delete5")
    delete_button_r5 = sunbro.FindByCSS("#delete6")

    bum_btn_4 = sunbro.FindByClass('btn-payment')
    bum_btn_5 = sunbro.FindByClass('btn-payment')
    bum_btn_6 = sunbro.FindByClass('btn-payment')

    view_ads = sunbro.FindByLinkText('Ver avisos')
    error_message = sunbro.FindByCSS(
        ".delete-reason-content p[style='display: block;'].validation_msg")

    def go(self, list_id, parameter=None):
        if parameter is None:
            self._driver.get(
                conf.MOBILE_URL + "/delete_reasons?id=" + str(list_id))
        else:
            self._driver.get(
                conf.MOBILE_URL + "/delete_reasons?id=" + str(list_id) + "&" + parameter)

    def select_reason(self, reason=1):
        self._driver.find_element_by_css_selector(
            '#ui-accordion-accordion-header-{0}'.format(reason)).click()

    def select_timer(self, timer=1):
        self.sell_time_options[timer].click()

    def delete_ad_logged(self):
        self.wait.until_visible('reason_4')
        self.reason_4.click()
        self.wait.until_visible('delete_button_r4')
        self.delete_button_r4.click()
        self.wait.until_visible('result_message')

    def delete_ad(self, password, num_form='4'):
        getattr(self, 'reason_{}'.format(num_form)).click()
        self.wait.until_present('password')
        self.password.send_keys(password)
        self.delete_buttons[int(num_form) - 1].click()


class BumpInfoLightbox(pages.BasePage):

    """ lightbox page for bump1 """

    q3 = sunbro.FindByCSS('.pm-content .q3')
    a3_content = sunbro.FindByCSS('.pm-content .a3 div p')
    close_button = sunbro.FindByCSS('.pm-container .pm-close')


class AdView(pages.BasePage):

    """Mobile AdView page object"""

    # new adview stuff
    back_to_results = sunbro.FindByCSS(".back-listing")
    store = sunbro.FindByCSS(".store-link")
    subject = sunbro.FindByCSS(".daview-title h1")
    price = sunbro.FindByCSS(".da-detail .da-detail__prices-price")
    date = sunbro.FindByCSS(".date")
    description = sunbro.FindByCSS(".description-cont .texto")
    body = description
    description_arrow_down = sunbro.FindByCSS(".description-cont .icon-yapo.icon-arrow-down")
    description_arrow_up = sunbro.FindByCSS(".description-cont .icon-yapo.icon-arrow-up")
    adparams_box = sunbro.FindByCSS('.daview-params.box')

    map = sunbro.FindByID("map_container")
    map_header = sunbro.FindByID("map_page_header")
    map_commune = sunbro.FindByCSS("#map_container .commune")
    map_region = sunbro.FindByCSS("#map_container .region")

    image = sunbro.FindByCSS(".owl-item.active")
    img_noimage = sunbro.FindByCSS(".no-image.no-image-general")

    user_photo = sunbro.FindByCSS(".user-photo")
    user_name = sunbro.FindByCSS(".seller-info__header-name")
    advertiser_name = user_name
    user_region = sunbro.FindByCSS(".user-region")
    user_pro = sunbro.FindByCSS(".type-rating .type")
    user_store = sunbro.FindByCSS(".store-link")

    admin_show = sunbro.FindByCSS(".da-options .icon-arrow-down")
    admin_hide = sunbro.FindByCSS(".da-options .icon-arrow-up")
    admin_dele = sunbro.FindByCSS(".option.delete")
    delete_button = admin_dele
    admin_edit = sunbro.FindByCSS(".option.edit")
    admin_bump = sunbro.FindByCSS(".option.bump.ui-link")

    bump_now = sunbro.FindByCSS("#bump_now")
    bump_day = sunbro.FindByCSS("#daily_bump")
    bump_week = sunbro.FindByCSS("#weekly_bump")

    sticky_ar = sunbro.FindByCSS(".addetail-stickybar")
    sticky_call_phone = sunbro.FindByCSS(".addetail-stickybar .addetail-stickybar__link:nth-child(2)")
    # if the bar has whatsapp button, then the chat button is on position 4
    sticky_ar_chat_button = sunbro.FindByCSS(".addetail-stickybar .addetail-stickybar__link:nth-child(3)")

    ar_name = sunbro.FindByID("yourName")
    ar_email = sunbro.FindByID("yourEmail")
    ar_phone = sunbro.FindByID("yourPhone")
    ar_body = sunbro.FindByID("adreply_body")
    ar_sendcopy = sunbro.FindByCSS(".check.checked")
    ar_title = sunbro.FindByCSS(".modal-ad-reply h2")
    ar_send = sunbro.FindByCSS("#send")
    ar_result_ok = sunbro.FindByCSS("#form_response_ok")

    ar_error_name = sunbro.FindByID("yourName-error")
    ar_error_email = sunbro.FindByID("yourEmail-error")
    ar_error_phone = sunbro.FindByID("yourPhone-error")
    ar_error_body = sunbro.FindByID("adreply_body-error")

    # Button elements
    button_publish_ad = sunbro.FindByID("acc_page_da_insert")
    button_my_account = sunbro.FindByID("acc_page_login")
    button_bump_ad = sunbro.FindByCSS(".option.bump")
    button_edit_ad = sunbro.FindByCSS(".option.edit")
    button_delete_ad = sunbro.FindByCSS(".option.delete")
    button_compare = sunbro.FindByID("comparaonline_link")

    # Links elements
    link_to_listing = sunbro.FindByCSS(".icon-yapo.icon-location")

    # Ad reply form elements
    title_adreply = sunbro.FindByCSS('#form_adreply h3.title')
    text_field_adreply_name = sunbro.FindByID("yourName")
    text_field_adreply_email = sunbro.FindByID("yourEmail")
    text_field_adreply_phone = sunbro.FindByID("yourPhone")
    text_area_adreply_body = sunbro.FindByID("adreply_body")
    check_box_adreply_send_copy = sunbro.FindByID("cc")
    button_send = sunbro.FindByID("send")
    next_ad = sunbro.FindByID('next_ad')
    prev_ad = sunbro.FindByID('prev_ad')
    list_id = sunbro.FindByCSS('.box-two.ad-code .box-right')

    profile_image = sunbro.FindByCSS('.seller-info__header-avatar')

    @property
    def seller_info(self):
        return self.select_shadow_element_by_css_selector('seller-info')

    @property
    def ad_reply_region_commune(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__content .fa-map-marker-alt  + h4')
    @property
    def ad_reply_advertiser_name(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__header-name')
    @property
    def ad_reply_advertiser_phone_img(self):
        return self.seller_info.find_element_by_css_selector('.seller-info__phone-image')

    @property
    def call_show_number(self):
        return self._driver.find_element_by_css_selector('.addetail-stickybar__modal .addetail-stickybar__modal-caption')

    def do_bump(self, bump_type):
        self._driver.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        self.admin_show.click()
        self.admin_bump.click()
        time.sleep(1)
        getattr(self, bump_type).click()

    def do_delete(self):
        self._driver.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        self.admin_show.click()
        self.admin_dele.click()

    def do_edit(self):
        self._driver.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        self.admin_show.click()
        self.admin_edit.click()

    def get_image_url(self):
        return self._driver.find_element_by_css_selector(".owl-item.active .item").value_of_css_property('background-image')

    def get_adparams(self):
        params = self._driver.find_elements_by_css_selector(".daview-params > ul > li")
        res = []
        for p in params:
            tmp = []
            tmp.append(p.find_element_by_css_selector(".box-left").text)
            tmp.append(p.find_element_by_css_selector(".box-right").text)
            res.append(tmp)
        return res


    def go(self, list_id):
        self.go_vi(list_id)

    def go_vi(self, list_id):
        self._driver.get(conf.MOBILE_URL + '/vi/{}.htm'.format(list_id))
        self.wait.until_loaded()

    def check_prefilled_form(self, name, email, phone):
        self._driver.execute_script("window.scrollTo(0, 100000);")
        time.sleep(1)
        self.sticky_ar_chat_button.click()
        assert self.ar_name.get_attribute("value") == name
        assert self.ar_email.get_attribute("value") == email
        assert self.ar_phone.get_attribute("value") == phone

    def open_top_adreply(self):
        self._driver.execute_script("window.scrollTo(0, 100000);")

    def send_adreply(self, name, email, phone, body):
        if self.sticky_ar_chat_button.is_displayed():
            self.sticky_ar_chat_button.click()

        self.fill_form(
            ar_name=name,
            ar_email=email,
            ar_phone=phone,
            ar_body=body
        )
        self.ar_send.click()

    @lazy
    def map_view(self):
        """ return a MapPreviewMobile element """
        return MapPreviewMobile(self._driver)


class LoginFacebook(pages.BasePage):
    """ Represents de modal that is displayed after you click on
        "Iniciar sesion" button on desktop's header """

    email = sunbro.FindByCSS('#facebook-email')
    password = sunbro.FindByCSS('#facebook-password')
    button_email_fb_confirm = sunbro.FindByCSS('#facebook-email-form button')
    button_password_fb_confirm = sunbro.FindByCSS('#facebook-password-form button')
    fb_connect_btn = sunbro.FindByID('fbConnectBtn')

    # facebook input
    fb_email = sunbro.FindByCSS('.mobile-login-form input[type=email]')
    fb_pass = sunbro.FindByCSS('.mobile-login-form input[type=password]')
    fb_login_btn = sunbro.FindByCSS('.mobile-login-form button[type=submit]')

    facebook_description = sunbro.FindByCSS("#facebook-box > div:nth-child(2) .facebookBox-header-description")
    spiner = sunbro.FindByCSS('.boxSpinner-spinner')

    def fb_connect(self):
        """ Connect facebook with yapo account """
        self.wait.until_visible('fb_connect_btn')
        self.fb_connect_btn.click()

    def login(self, fb_email, fb_pass):
        Login(self._driver).go()
        self.fb_connect()
        self.wait.until(lambda wd: len(wd.window_handles) == 2)
        self._driver.switch_to_window(self._driver.window_handles[-1])
        self.wait.until_visible('fb_login_btn')
        self.fill_form(fb_email=fb_email)
        self.fill_form(fb_pass=fb_pass)
        self.fb_login_btn.click()
        self._driver.switch_to_window(self._driver.window_handles[0])

    def fill_email_form_and_send(self, email):
        """ Fill form email, when facebook email has not account in yapo """
        self.fill_form(email=email)
        self.wait.until_visible('button_email_fb_confirm')
        self.button_email_fb_confirm.click()

    def fill_password_form_and_send(self, email, password):
        """ Fill form email and password, when email is different facebook email """
        # send email form
        self.fill_email_form_and_send(email)

        # send password form
        self.wait.until_not_visible('spiner')
        self.fill_form(password=password)
        self.wait.until_visible('button_password_fb_confirm')
        self.button_password_fb_confirm.click()


class Login(pages.BasePage):

    """Page object for accounts '/login' page"""

    # Error validations
    errors = sunbro.FindAllByCSS('.loginFeedback.__error')
    header_error = sunbro.FindByCSS('.dashboardHeader-txt')

    # ButtonsderElements
    button_publish_ad = sunbro.FindByID("acc_page_da_insert")
    button_login = sunbro.FindByID("acc_login_submit_button")
    button_logout_user = sunbro.FindByID("acc_page_close_session")
    button_create_account = sunbro.FindByID("login_create_account")
    button_my_ads = sunbro.FindByID("myads_submit")
    body = sunbro.FindByCSS('body')

    # Input
    input_email = sunbro.FindByID("accbar_email")
    input_password = sunbro.FindByID("accbar_password")
    input_myads_email = sunbro.FindByID("myads_email")

    # Links
    my_ads_without_account = sunbro.FindByID('ads_without_account')

    # facebook input
    fb_email = sunbro.FindByID('email')
    fb_pass = sunbro.FindByID('pass')
    fb_login_btn = sunbro.FindByCSS('#loginbutton input')

    yapo_header = sunbro.FindByCSS('yapo-header')
    yapo_drawer = sunbro.FindByCSS('yapo-drawer')

    @property
    def button_open_drawer(self):
        return self.yapo_header.find_element_by_css_selector('#btn-menu')

    @property
    def button_logout(self):
        return self.yapo_drawer.find_element_by_css_selector('.yapo-drawer__item-id-logout')

    # Common used methods
    def go(self, parameter=None):
        if parameter is None:
            self._driver.get(conf.SSL_URL + '/login')
        else:
            self._driver.get(conf.SSL_URL + '/login?' + parameter)

    def login(self, user, password, do_login=True):
        self.go()
        self.input_email.send_keys(user)
        self.input_password.send_keys(password)
        self.button_login.click()
        if do_login:
            self.wait.until_session_cookie()

    def simple_login(self, user, password):
        self.input_email.send_keys(user)
        self.input_password.send_keys(password)
        self.button_login.click()

    def logout(self):
        self._wait = PageWait(self, 10)
        self.button_open_drawer.click()
        self.button_logout.click()
        self.wait.until_present('button_login')


class HeaderElements(pages.BasePage):

    """Page object to access to the common elements on mobile site:
           'Publicar aviso button'
           'Mi cuenta button'
           'Logged user button'
           'Region link to open/close accordion'
           'Regions links'"""

    # Buttons
    button_publish_ad = sunbro.FindByID("acc_page_da_insert")
    # <- Use only when not logged in
    button_my_account = sunbro.FindByID("acc_page_login")
    button_logged_user = sunbro.FindByID(
        "acc_page_user_name")  # <- Use only when logged in
    button_logout_user = sunbro.FindByCSS("#acc_page_close_session")
    link_logo_home = sunbro.FindByCSS("a.logo")
    location_button = sunbro.FindByCSS(".btn-location")

    # Links (all regions and region accordion trigger)
    regions_list = sunbro.FindByClass('regions_list')
    link_region_selector = sunbro.FindByCSS(".icon-yapo.icon-location")
    link_region_metropolitana = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(1)")
    link_region_arica_parinacota = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(2)")
    link_region_tarapaca = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(3)")
    link_region_antofagasta = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(4)")
    link_region_atacama = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(5)")
    link_region_coquimbo = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(6)")
    link_region_valparaiso = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(7)")
    link_region_ohiggins = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(8)")
    link_region_maule = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(9)")
    link_region_biobio = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(10)")
    link_region_araucania = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(11)")
    link_region_losrios = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(12)")
    link_region_loslagos = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(13)")
    link_region_aisen = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(14)")
    link_region_magallanes_antartica = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(15)")
    link_region_todo_chile = sunbro.FindByCSS(
        ".regions_list.show-region li:nth-child(16)")

    yapo_header = sunbro.FindByCSS('yapo-header')
    yapo_drawer = sunbro.FindByCSS('yapo-drawer')

    @property
    def button_open_drawer(self):
        return self.yapo_header.find_element_by_css_selector('#btn-menu')

    @property
    def button_close_drawer(self):
        return self.yapo_drawer.find_element_by_css_selector('.yapo-drawer__button-back')

    @property
    def drawer_title(self):
        self.button_open_drawer.click()
        return self.yapo_drawer.find_element_by_css_selector('.yapo-drawer__title')

    @property
    def button_login(self):
        self.button_open_drawer.click()
        return self.yapo_drawer.find_element_by_css_selector('.yapo-drawer__item-id-login')

    @property
    def button_publish(self):
        return self.yapo_header.find_element_by_css_selector('#btn-publish')

    def is_logged(self):
        login = Login(self._driver)
        logged_in = True
        try:
            login.button_create_account
        except:
            logged_in = False
        return logged_in

    def who_is_logged(self):
        header_elements = HeaderElements(self._driver)
        try:
            logged_user = re.sub(
                'Hola, ', '', header_elements.button_logged_user.text)
        except:
            logged_user = ''
        return logged_user

    def logout(self):
        login = Login(self._driver)
        login.logout()

    def region_chooser_displayed(self):
        print(self.regions_list.get_attribute('class'))
        return "show-region" in self.regions_list.get_attribute('class')

    def toggle_region_chooser(self):
        shown = self.region_chooser_displayed()
        self.link_region_selector.click()
        self.wait.until(lambda x: self.region_chooser_displayed() != shown)


@ComponentComposer
class NewAdInsert(pages.BasePage):

    component = {
        'upselling': (UpsellingContainerMobile, '.aiUpselling'),
    }

    members = {
        'header': HeaderElements
    }

    multivalued = set(['sct', 'jc', 'srv', 'eqp', 'fwtype'])
    guessable = set(['category', 'type', 'regdate', 'gearbox', 'fuel', 'cartype'])
    ad_param_list = set([
        'category', 'estate_type', 'rooms', 'brand', 'model', 'version', 'regdate', 'mileage', 'cubiccms',
        'gearbox', 'fuel', 'cartype', 'condition', 'footwear_gender', 'footwear_type', 'fwtype', 'jc', 'sct',
        'plates', 'contract_type', 'working_day'
    ])
    edit_feedback = sunbro.FindByCSS('.adedit-feedback')

    # 0. Image upload
    uncompressed = False
    clickme = sunbro.FindByCSS('.wrapper.new-ai #da_info legend')
    form = sunbro.FindByID('newad_form')
    upload = sunbro.FindByID('upload')
    upload_plupload = sunbro.FindByCSS('#upload + div input')
    upload_fineupload = sunbro.FindByCSS('#upload input')
    images = sunbro.FindAllByClass('image')
    img_loader = sunbro.FindByClass('loading')
    more_images_x = sunbro.FindByCSS('#more_images_yeah .icon-close')
    more_images_yeah = sunbro.FindByCSS('#more_images_yeah')

    # 1. Ad Information
    subject = sunbro.FindByID('subject')
    body = sunbro.FindByID('body')
    currency = sunbro.FindByCSS('input[name=currency]')
    currency_peso = sunbro.FindByID('currency_peso')
    currency_uf = sunbro.FindByID('currency_uf')
    price = sunbro.FindByID('price')
    uf_price = sunbro.FindByID('uf_price')

    UF_PRICE_TOO_HIGH = 'El valor m�ximo posible es de 314.000 UF. Si es mayor, puedes ingresarlo en la descripci�n'
    INVALID_CHARACTERS = 'El precio contiene caracteres no v�lidos'
    UF_TOO_MANY_DECIMALS = 'El precio contiene muchos decimales, solo 2 son soportados'

    # 2. Category info
    cat_info = sunbro.FindByID('cat_info')
    category = sunbro.FindByID('category')
    parent_category = sunbro.FindByID('parent_category')
    ad_params = sunbro.FindAllByCSS('select, input', within='cat_info')
    ad_param_errors = sunbro.FindAllByCSS('.error-ai', within='cat_info')

    ERROR_CANT_CHANGE_CAT_FROM = 'La categor�a de este aviso no puede ser modificada'
    ERROR_CANT_CHANGE_CAT_TO = 'No puedes cambiar el aviso a esta categor�a'

    # 2.1 input ad params
    size = sunbro.FindByID('size')
    condominio = sunbro.FindByID('condominio')

    # 2.2 other ad params
    jcs = sunbro.FindAllByCSS('#hsp-jc .ui-listview a')
    advice = sunbro.FindByCSS('.maxEditionAdvice')
    insertion_advice = sunbro.FindByCSS('.insertionPriceAdvice')
    garage_spaces = sunbro.FindByID('garage_spaces')
    gender = sunbro.FindByID('gender')
    jc = sunbro.FindByID('jc')
    sct = sunbro.FindByID('sct')
    srv = sunbro.FindByID('srv')
    eqp = sunbro.FindByID('eqp')
    condition = sunbro.FindByID('condition')
    type = sunbro.FindByID('type')
    brand = sunbro.FindByID('brand')
    model = sunbro.FindByID('model')
    version = sunbro.FindByID('version')
    regdate = sunbro.FindByID('regdate')
    gearbox = sunbro.FindByID('gearbox')
    fuel = sunbro.FindByID('fuel')
    cartype = sunbro.FindByID('cartype')
    mileage = sunbro.FindByID('mileage')
    internal_memory = sunbro.FindByID('internal_memory')
    estate_type = sunbro.FindByID("estate_type")
    rooms = sunbro.FindByID('rooms')
    bathrooms = sunbro.FindByID('bathrooms')
    size = sunbro.FindByID('size')
    garage_spaces = sunbro.FindByID('garage_spaces')
    condominio = sunbro.FindByID('condominio')
    cubiccms = sunbro.FindByID('cubiccms')
    plates = sunbro.FindByID('plates')
    contract_type = sunbro.FindByID('contract_type')
    working_day = sunbro.FindByID('working_day')

    plates_msj = sunbro.FindByCSS('.aiMessage')

    # 3. Location info
    geolocate = sunbro.FindByID('geolocate_me')
    geoposition = sunbro.FindByID('geoposition')
    region = sunbro.FindByID('region')
    communes = sunbro.FindByID('communes')

    # 4. User info
    company_ad = sunbro.FindByID('user_type')
    company_c_check = sunbro.FindByCSS('#is_company_c')
    company_p_check = sunbro.FindByCSS('#is_company_p')
    company_ad_buttons = sunbro.FindAllByCSS('.userTypes-type')
    name = sunbro.FindByID('name')
    email = sunbro.FindByID('email')
    rut = sunbro.FindByID('rut')
    phone_type_fixed = sunbro.FindByID('phoneInputTypeHome')
    phone_type_mobile = sunbro.FindByID('phoneInputTypeMobile')
    phone_type_label_mobile = sunbro.FindByCSS('label[for="phoneInputTypeMobile"]')
    area_code = sunbro.FindByID('area_code')
    phone = sunbro.FindByID('phone')
    phone_hidden = sunbro.FindByID('switch') # Plz shoot the guy who picked that ID
    passwd = sunbro.FindByID('passwd')
    passwd_ver = sunbro.FindByID('passwd_ver')
    create_account = sunbro.FindByID('create_new_account')

    # errors
    errors = sunbro.FindAllByCSS('.phoneInput-errorMessage:not(.is-hidden), .error')
    images_error = sunbro.FindByID('images_errors')
    name_error = sunbro.FindByID('name-error')
    body_error = sunbro.FindByID('body-error')
    price_error = sunbro.FindByID('price-error')
    phone_error = sunbro.FindByClass('phoneInput-errorMessage')
    rut_error = sunbro.FindByID('rut-error')
    email_error = sunbro.FindByID('email-error')
    passwd_error = sunbro.FindByID('passwd-error')
    passwd_ver_error = sunbro.FindByID('passwd_ver-error')

    # upselling
    combo_upselling_box = sunbro.FindByCSS('.aiUpselling')
    combo_upselling_box_expanded = sunbro.FindByCSS('.ai-box.aiUpselling.__expanded')

    combo_upselling_standard = sunbro.FindByID('standard')
    combo_upselling_advanced = sunbro.FindByID('advanced')
    combo_upselling_premium = sunbro.FindByID('premium')

    # draft
    clean_draft = sunbro.FindByClass('icon-trash')
    draft_wrap = sunbro.FindByID('draft_wrap')

    ai_submit = sunbro.FindByCSS('.btn-primary')

    err_error = sunbro.FindByID('yourInfoError')
    ERROR_MULTI_INSERT = 'Has publicado el m�ximo de avisos seguidos posible, para seguir publicando espera unos minutos.';

    def __init__(self, *args, **kwargs):
        super(NewAdInsert, self).__init__(*args, **kwargs)
        self.hotizontal_select = HotizontalSelect(self._driver, form_selector='#newad_form', parent_page=self)
        # Hack to allow compatibility with Desktop Ad Insert
        class Submitter:
            def __call__(inner):
                inner.click()
            def click(inner):
                self.form.submit()

        setattr(self, 'submit', Submitter())

    def go(self):
        self._driver.get(conf.MOBILE_URL + '/publica-un-aviso')
        self.wait.until_loaded()

    @property
    def job_category(self):
        return self.val('jc')

    def clear(self, field):
        """ because field.clear() does not work """
        field.parent.execute_script("arguments[0].value = ''", field)

    def _fix_ad_params(self, params):
        """ Regroups ad params in way more suitable to this ad insert """
        drop = set([
            'accept_conditions', 'address', 'create_account', 'ai_create_account', 'add_location', 'address_number',
            'email_confirm', 'email_verification', 'show_location',
        ])
        translate = {'password': 'passwd', 'password_verification': 'passwd_ver', 'footwear_type': 'fwtype'}
        to_list = {'sct_': 'sct', 'jc_': 'jc'}
        for p, _ in params.copy().items():
            # Job categories need to be flattened as a list
            in_to_list = False
            for prefix, param in to_list.items():
                if p.startswith(prefix):
                    in_to_list = True
                    if param not in params:
                        params[param] = []
                    params.pop(p)
                    params[param].append(p[len(prefix):])
                    break
            # Nothing to do if we already fixed it
            if in_to_list:
                continue
            # Some need direct translation
            elif p in translate:
                params[translate[p]] = params.pop(p)
            # Some need to be directly dropped
            elif p in drop:
                params.pop(p)
            # Some don't fit any of the previous patterns
            elif p == 'private_ad':
                params['company_ad'] = not params.pop(p)
        if not params.get('company_ad', False):
            if 'rut' in params and self.val('company_ad') != '1':
                del params['rut']
        # XXX this is hideous. Why not simply use the value phone_type as is?
        if 'phone_type' in params:
            if params['phone_type'] == 'm':
                params['phone_type_mobile'] = True
            else:
                params['phone_type_fixed'] = True
            del params['phone_type']
        # Kill parameters the logged used won't need to fill
        if 'logged' in params:
            drop = ['passwd', 'passwd_ver', 'email']
            for p in drop:
                if p in params:
                    del params[p]
            del params['logged']

    def _should_fill(self, category, param):
        return param not in self.ad_param_list or param in defaults.CATEGORY_PARAMS.get(category, {})

    def combo_is_disabled(self, wd, product):
        return Upselling.is_element_disabled(wd.find_element_by_css_selector('.aiUpselling'), selector='#{0}.__deactivated'.format(product))

    def get_combo_price(self, wd, product):
        return wd.find_element_by_css_selector('.aiUpselling #{0} .aiUpselling-price'.format(product)).text

    def get_upselling_combos(self):
        return self.upselling.combos

    def choose_upselling(self, i=None, id=None):
        combo = self.upselling.combo(i, id).click()
        return combo

    def delete_images(self):
        for img in self.images:
            img.find_element_by_css_selector('.remove').click()
        assert len(self.images) == 0

    def insert_ad(self, **kwargs):
        """ Get the ad inserted, ad params filled and find Waldo """

        print(' --- insert_ad --- ')
        # A little cleanup
        self._fix_ad_params(kwargs)

        # images first
        if 'images' in kwargs:
            for img in kwargs.pop('images'):
                self.upload_resource_image(img)

        # region and communes
        for param in ['region', 'communes']:
            if param in kwargs:
                self.hotizontal_select.fill_sidenav(kwargs, trigger = param)

        # category and its sidenav parameters
        category = None
        if 'category' in kwargs:
            category = int(kwargs['category'])
            kwargs['parent_category'] = Categories.get_main_cat(category)
            try:
                filled = self.hotizontal_select.fill_sidenav(kwargs, trigger = 'parent_category')
            except RedirectedAway:
                return

        if 'currency' in kwargs:
            val = kwargs.pop('currency')
            attr = getattr(self, 'currency_'+val)
            attr.click()
            if val == 'uf' and 'price' in kwargs:
                kwargs['uf_price'] = kwargs.pop('price')

        # If we didn't pick any of these on sidenav, we won't need them later
        for param in self.guessable:
            if param in kwargs:
                kwargs.pop(param)

        # Company ad before filling user info
        if 'company_ad' in kwargs:
            # company_ad can be hidden if the user is logged
            if len(self.company_ad_buttons) > 0:
                if kwargs.pop('company_ad'):
                    self.company_c_check.click()
                else:
                    self.company_p_check.click()
            else:
                kwargs.pop('company_ad')

        if 'phone_hidden' in kwargs:
            self.fill_form(phone_hidden = kwargs.pop('phone_hidden'))

        if 'phone_type_fixed' in kwargs:
            self.phone_type_fixed.click()
            kwargs.pop('phone_type_fixed')

        if 'phone_type_mobile' in kwargs:
            self.phone_type_label_mobile.click()
            kwargs.pop('phone_type_mobile')

        # whatever is left
        print("whatever is left: {}".format(kwargs))
        for adparam in kwargs:
            if not self._should_fill(category, adparam):
                print('skipping {}: {}'.format(adparam, kwargs[adparam]))
                continue
            print('{}: {}'.format(adparam, kwargs[adparam]))
            input = self._driver.find_element_by_id(adparam)
            self.clear(input)
            input.send_keys(kwargs[adparam])

        if self.more_images_x.is_displayed():
            self.more_images_x.click()
        self.clickme.click()

    def _get_img_upload(self):
        if self.uncompressed:
            return self.upload_fineupload

        try:
            return self.upload_plupload
        except NoSuchElementException:
            self.uncompressed = True
            print('using fine-uploader')
            return self.upload_fineupload

    def upload_resource_image(self, relative_path):
        img_upload = self._get_img_upload()
        current = len(self.images)
        img_upload.send_keys(os.path.join(conf.RESOURCES_DIR, 'images', relative_path))
        self.wait.until(lambda x: len(self.images) != current)

    def in_errors(self, param):
        if param == 'category':
            param = 'parent_category'
        id = '{}-error'.format(param)
        return any(map(lambda e: e.get_attribute('id') == id, self.errors))

    def not_in_errors(self, param):
        if param == 'category':
            param = 'parent_category'
        id = '{}-error'.format(param)
        return all(map(lambda e: e.get_attribute('id') != id, self.errors))

    def get_error_for(self, param):
        if param == 'category':
            try:
                err = self._driver.find_element_by_id('parent_category-error')
            except:
                err = self._driver.find_element_by_id('category-error')
        elif param == 'phone':
            err = self.phone_error
        else:
            err = self._driver.find_element_by_id('{}-error'.format(param))
        if err:
            print('error for {} was: {}'.format(param, err.text))
        return err

    def label_for(self, param):
        return self._driver.find_element_by_css_selector('label[for="{}"]'.format(param))

    def has_map(self):
        return self.geoposition.get_attribute('value') != ''

    def is_logged(self):
        return self.is_element_present('passwd')

    def choose_category(self, category_id):
        params = { 'category': category_id }

        params['parent_category'] = Categories.get_main_cat(category_id)

        self.hotizontal_select.fill_sidenav(params, trigger = 'parent_category')


class AdInsert(pages.BasePage):

    """Page object for ad insert page"""

    # Selects
    estate_type = sunbro.FindByID("estate_type")
    select_region = sunbro.FindByID("region")
    select_communes = sunbro.FindByID('communes')
    select_category = sunbro.FindByID("category")
    select_cubiccms = sunbro.FindByCSS('#cubiccms:not([disabled])')
    select_regdate = sunbro.FindByCSS('#regdate:not([disabled])')
    select_gearbox = sunbro.FindByCSS('#gearbox:not([disabled])')
    select_fuel = sunbro.FindByCSS('#fuel:not([disabled])')
    select_brand = sunbro.FindByCSS('#brand:not([disabled])')
    select_model = sunbro.FindByCSS('#model:not([disabled])')
    select_version = sunbro.FindByCSS('#version:not([disabled])')
    select_cartype = sunbro.FindByCSS('#cartype:not([disabled])')
    select_rooms = sunbro.FindByCSS('#rooms:not([disabled])')

    # Buttons
    button_publish_now = sunbro.FindByID("newad_submit")
    upload = sunbro.FindByID('upload')
    upload_image = sunbro.FindByCSS('#upload + div input')
    upload_image_uncompressed = sunbro.FindByCSS('#upload input')
    images = sunbro.FindAllByCSS('.layer.image.loaded')
    max_images_error = sunbro.FindByCSS('#ad_images_error_message span')
    clean_draft_button = sunbro.FindByID("icon_clean_form")
    clean_draft = sunbro.FindByID("text_clean_form")

    # Inputs
    input_subject = sunbro.FindByID("subject")
    input_body = sunbro.FindByID("body")
    input_price = sunbro.FindByID("price")
    input_name = sunbro.FindByID("name")
    input_email = sunbro.FindByID("email")
    input_phone = sunbro.FindByID("phone")
    input_password = sunbro.FindByID("passwd")
    input_password_confirm = sunbro.FindByID("passwd_ver")
    input_mileage = sunbro.FindByID('mileage')
    input_internal_memory = sunbro.FindByID('internal_memory')

    input_size = sunbro.FindByCSS('#size:not([disabled])')
    input_garage_spaces = sunbro.FindByCSS('#garage_spaces:not([disabled])')
    input_condominio = sunbro.FindByCSS('#condominio:not([disabled])')

    # Radio Buttons
    radio_vendo = sunbro.FindByID("label_st_s")
    radio_busco = sunbro.FindByID("label_st_k")
    input_radio_busco = sunbro.FindByID("type_k")
    input_radio_vendo = sunbro.FindByID("type_s")
    private_ad = sunbro.FindByID("is_company_p")
    company_ad = sunbro.FindByID("is_company_c")

    # Check boxes
    check_box_hide_phone = sunbro.FindByID("phone_hidden")
    check_box_create_account = sunbro.FindByCSS("#create_account+ins")
    check_box_accept_terms = sunbro.FindByCSS("#terms+ins")
    add_location = sunbro.FindByID("add_location")

    # just some random sub-categories
    sct_1 = sunbro.FindByID('sct_1')
    sct_2 = sunbro.FindByID('sct_2')
    sct_3 = sunbro.FindByID('sct_3')
    sct_4 = sunbro.FindByID('sct_4')
    sct_8 = sunbro.FindByID('sct_8')

    jobs_service_label = sunbro.FindByCSS(
        '.checkbox_container label:nth-child(1)')

    condition = sunbro.FindByID('condition')
    gender = sunbro.FindByID('gender')
    ad_information = sunbro.FindByID('ad_information')

    lightbox_message = sunbro.FindByCSS('.pm-content .pm-message')
    lightbox_ok_button = sunbro.FindByCSS(
        '.pm-content .pm-buttons a.btn-primary')

    # validations messages
    passwd_msg = sunbro.FindByCSS('div[for=passwd] span')
    pgw_modal = sunbro.FindByID('pgwModal')
    draft_wrap = sunbro.FindByCSS('.message.message-draft.clean-draft')

    # Random obbjects
    img_loader = sunbro.FindByClass('loading')

    # Desktop compatibility (OOP maddafakkas!)
    # I know, horrible, but we need some compatibility down here
    # Compatibility +1
    region = select_region
    communes = select_communes
    category = select_category
    cubiccms = select_cubiccms
    regdate = select_regdate
    gearbox = select_gearbox
    fuel = select_fuel
    brand = select_brand
    model = select_model
    version = select_version
    cartype = select_cartype
    rooms = select_rooms
    subject = input_subject
    body = input_body
    price = input_price
    name = input_name
    phone = input_phone
    email = input_email
    password = input_password
    password_verification = input_password_confirm
    create_account = check_box_create_account
    accept_conditions = check_box_accept_terms
    submit = button_publish_now
    mileage = input_mileage
    internal_memory = input_internal_memory
    size = input_size
    garage_spaces = input_garage_spaces
    condominio = input_condominio
    phone_hidden = check_box_hide_phone

    plates = sunbro.FindByID("plates")

    @lazy
    def map_form(self):
        """ return a MapForm element """
        return MapForm(self._driver)

    def go_ai(self):
        self._driver.get(conf.MOBILE_URL + '/mai')

    def go(self, parameter=None):
        if parameter is None:
            self._driver.get(conf.MOBILE_URL + '/mai')
        else:
            self._driver.get(conf.MOBILE_URL + '/mai?' + parameter)

    def element_exist(self, object_declaration):
        try:
            object_declaration
        except:
            return False
        return True

    def upload_resource_image(self, relative_path, compressed=True):
        if compressed:
            self.upload_image.send_keys(
                os.path.join(conf.RESOURCES_DIR, 'images', relative_path))
        else:
            self.upload_image_uncompressed.send_keys(
                os.path.join(conf.RESOURCES_DIR, 'images', relative_path))

    def get_error_for(self, element):
        return self._driver.find_element_by_css_selector('#param_{}.error_msg'.format(element))

    # TODO Add locators and complement the method for categories with ad_params
    #      Also consider the case for 'Profesional'. Rut not considered
    def insert_ad_unlogged(self, **kwargs):
        url_params = kwargs.pop('url_params', None)
        create_account = kwargs.pop('create_account', True)
        dont_send = kwargs.pop('dont_send', None)
        params = {'region': "15",
                  'communes': "343",
                  'category': "5020",
                  'subject': "Foo",
                  'body': "Foo Bar Bazz Qux",
                  'price': "123123",
                  'name': "Foobar",
                  'email': "foo@bar.com",
                  'phone': "123123",
                  'phone_hidden': False,
                  'password': "123123123",
                  'password_verification': "123123123",
                  'create_account': create_account,
                  'accept_conditions': True, }
        params.update(kwargs)

        self.go(parameter=url_params)
        common = HeaderElements(self._driver)
        self.insert_ad(**params)
        if dont_send:
            return
        self.button_publish_now.click()
        success_page = AdInsertedSuccessfuly(self._driver)
        success_page.assert_success_message(params['subject'])

    def insert_ad_logged(self, **kwargs):
        header = HeaderElements(self._driver)
        if not header.is_logged(self._driver):
            raise BaseException("Need to be logged in")

        params = {'region': "15",
                  'communes': "343",
                  'category': "5020",  # Default: Muebles y art�culos del hogar
                  'subject': "Foo",
                  'body': "Foo Bar Bazz Qux",
                  'price': "123123",
                  'role': "Persona",
                  'name': "Foobar",
                  'email': "foo@bar.com",
                  'phone': "123123",
                  'hide_phone': False,
                  'password': "123123123",
                  'accept_terms': True, }
        params.update(kwargs)

        self.go_ai()
        common = HeaderElements(self._driver)
        self.insert_ad(region=params['region'],
                       communes=params['communes'],
                       category=params['category'],
                       input_subject=params['subject'],
                       input_body=params['body'],
                       input_price=params['price'],
                       private_ad=params['role'] == 'Persona',
                       company_ad=params['role'] == 'Profesional',
                       input_name=params['name'],
                       input_phone=params['phone'],
                       check_box_hide_phone=params['hide_phone'],
                       check_box_accept_terms=params['accept_terms'], )
        self.button_publish_now.click()
        success_page = AdInsertedSuccessfuly(self._driver)
        success_page.assert_success_message(params['subject'])

    def choose_category(self, category):
        category = str(category)
        select = Select(self.select_category)
        select.select_by_value(str(category))
        self.wait.until_attr_equals('ad_information', 'data-cat', category)

    def search_location(self, **kwargs):
        self.map_form.fill_form(**kwargs)
        self.map_form.remove_args(kwargs)

        self.map_form.map.fill_form(**kwargs)
        self.map_form.map.remove_args(kwargs)

        return kwargs

    def insert_ad(self, category, **kwargs):
        """ It inserts an ad """

        if 'data' in kwargs:
            kwargs = kwargs['data']

        # To make it compatible with Desktop insert_ad
        if 'email_verification' in kwargs:
            kwargs.pop('email_verification')

        kwargs = self.search_location(**kwargs)

        category = str(category)
        self.choose_category(category)

        if category == '2020':
            # XXX I know, horrible!
            if 'select_brand' in kwargs:
                params_order = ['select_brand', 'select_model', 'select_version', 'select_regdate',
                                'select_gearbox', 'select_fuel', 'select_cartype', 'input_mileage']
            else:
                params_order = [
                    'brand', 'model', 'version', 'regdate', 'gearbox', 'fuel', 'cartype', 'mileage']
            for param_key in params_order:
                if param_key in kwargs:
                    param_value = kwargs.pop(param_key)
                    params_fill_form = {param_key: param_value}
                    self.fill_form(**params_fill_form)
        else:
            self.fill_form(**kwargs)

        return self


class NewAdEdit(NewAdInsert):

    bump = sunbro.FindByClass('adedit-bump')
    want_bump = sunbro.FindByID('want_bump')

    def fill(self, name, value):
        if name == 'want_bump':
            parent = self.want_bump.find_element_by_xpath("..")
            if ('checked' in parent.get_attribute('class')) != value:
                parent.click()
        else:
            super().fill(name, value)

    def go(self, list_id):
        url = '{mobile_url}/editar-aviso/0?id={list_id}'.format(mobile_url=conf.MOBILE_URL, list_id=list_id)
        self._driver.get(url)
        self.wait.until_loaded()

    def edit_an_ad(self, list_id, passwd, parameters_changed={}, delete_images=False, do_bump=False, submit=True):
        edit_pw = EditPasswordPage(self._driver)
        edit_pw.go(list_id)
        edit_pw.validate_password(passwd)

        if parameters_changed:
            self.insert_ad(**parameters_changed)

        # delete all images
        if delete_images:
            for img in self.images:
                img.find_element_by_css_selector('.remove').click()
            assert len(self.images) == 0

        # do bump
        if do_bump:
            self.want_bump.click()
            self.submit.click()
            EditSuccessPage(self._driver).button_submit_bill.click()
            Paysim(self._driver).pay_accept()
            return

        if submit:
            self.submit.click()


AdEdit = NewAdEdit


class AdInsertedSuccessfuly(pages.BasePage):

    """Little page object for page mai/success"""

    # Text
    # TODO Update this test, the class 'botton-msg' don't exist in the
    # new-ad-done
    text_success_rules = sunbro.FindByCSS("div.bottom-msg")
    text_create_account_success = sunbro.FindByCSS(".success-desc")

    pack_message_ok = sunbro.FindByID("pack_message_ok")
    pack_message_noquota = sunbro.FindByID("pack_message_noquota")
    pack_message_nopack = sunbro.FindByID("pack_message_nopack")
    pack_message_noaccount = sunbro.FindByID("pack_message_noaccount")

    info_box = sunbro.FindByCSS('info-box')

    def assert_success_message(self, message):
        assert 'Tu aviso "{}" ser� revisado'.format(
            message) in self.text_success_message.text, self.text_success_message.text

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def text_success_message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdNotInserted(pages.BasePage):

    """Little page object for page mai w/ validation problems"""

    # Text
    text_validation_message = sunbro.FindByCSS(".pm-message")
    button_validation_message = sunbro.FindByCSS(".pm-buttons btn-primary")

    def assert_success_message(self, message):
        assert self.text_success_message.text == "Tu aviso \"" + \
            message + "\" est� siendo revisado"


class EditPasswordPage(pages.BasePage):

    """Page object for edit password form"""
    title = sunbro.FindByCSS('.title.center')
    password_input = sunbro.FindByID('password_input')
    forgot_password_link = sunbro.FindByID('forgot_passwd_link')
    submit_button = sunbro.FindByID('submit_button')

    error = sunbro.FindByCSS('.validation_msg.error')

    def go(self, list_id, parameter=None):
        if parameter is None:
            url = '{0}/editar-aviso?id={1}'.format(conf.MOBILE_URL, str(list_id))
            self._driver.get(url)
        else:
            url = '{0}/editar-aviso?id={1}&{2}'.format(
                conf.MOBILE_URL, str(list_id), parameter)
            self._driver.get(url)

    def validate_password(self, password):
        self.password_input.send_keys(password)
        self.submit_button.click()


class MobileFormImageReady(object):

    """ An expectation to wait for the image upload to be ready """

    def __call__(self, driver):

        iframes = driver.find_elements_by_css_selector('iframe.single_upload')
        iframes = [
            iframe for iframe in iframes if 'mpu' in iframe.get_attribute('src')]
        for iframe in iframes:
            driver.switch_to_frame(iframe)
            submitting = driver.find_elements_by_css_selector(
                'input#submitting')

            # the submitting input should exists
            if len(submitting) != 0:
                return False

            # but its value should be different of 1
            for sub in submitting:
                if sub.get_attr('value') == '1':
                    return False

        return True


class EditSuccessPage(pages.BasePage):

    """Page object for edit success page"""

    # Labels present when user selects want_bump checkbox on edit form
    label_subject = sunbro.FindByClass('subject')
    label_price = sunbro.FindByClass('price')
    label_success_message = sunbro.FindByClass('success-message')

    # Radio Buttons
    radio_bill_input = sunbro.FindByID('radio-bill')
    radio_invoice_input = sunbro.FindByID('radio-invoice')

    # Links
    link_edit_email = sunbro.FindByClass('edit-email')

    # Buttons
    button_submit_bill = sunbro.FindByID('submit-bill')

    # Inputs
    input_email = sunbro.FindByName('email')


    @property
    def radio_bill(self):
        return self.radio_bill_input.find_element_by_xpath('../ins')

    @property
    def radio_invoice(self):
        return self.radio_invoice_input.find_element_by_xpath('../ins')

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def text_thanks(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__title')


class SummaryPayment(pages.BasePage):

    """Page object for the payment summary page"""

    # Table
    products_list_table = sunbro.FindByCSS('.summaryTable')
    rows_product_list = sunbro.FindAllByCSS('.summaryTable-row:not(.__header)')

    big_bump_arrow = sunbro.FindByCSS(".sprite-bump.big-bump-arrow")

    product_row0 = sunbro.FindByID('row0')
    product_row1 = sunbro.FindByID('row1')
    product_row2 = sunbro.FindByID('row2')

    # Buttons
    buttons_tooltip_yes = sunbro.FindAllByCSS('.confirm-option.yes')
    buttons_tooltip_no = sunbro.FindAllByCSS('.confirm-option.no')

    # Links
    link_to_more_bumps = sunbro.FindByID('more-bumps')
    link_to_modify_email = sunbro.FindByID('modify-email-invoice')
    link_to_pay_products = sunbro.FindByID('mobileCheckout')
    links_remove_icon = sunbro.FindAllByCSS('.remove-icon')

    # Radios
    radio_bill_input = sunbro.FindByID('radio-bill')
    radio_invoice_input = sunbro.FindByID('radio-invoice')
    radio_oneclick = sunbro.FindByCSS('.paymentMethod.__oneclick')
    radio_webpay = sunbro.FindByCSS('.paymentMethod.__webpay')
    radio_khipu = sunbro.FindByCSS('.paymentMethod.__khipu')
    radio_credits = sunbro.FindByCSS('.paymentMethod.__credits')

    # Texts
    text_amount = sunbro.FindByID('total-amount')
    text_products_count = sunbro.FindByID('summary-bar-amount')
    text_message_payment = sunbro.FindByCSS('.paymentHeader-small#message-pay')

    # Inputs
    input_email = sunbro.FindByID('email-invoice')
    input_name = sunbro.FindByID('id_name')
    input_giro = sunbro.FindByID('id_lob')
    input_rut = sunbro.FindByID('id_rut')
    input_address = sunbro.FindByID('id_address')
    input_contact = sunbro.FindByID('id_contact')

    # Selects
    select_regions = sunbro.FindByID('id_region')
    select_communes = sunbro.FindByID('id_communes')

    # Forms
    form_bill = sunbro.FindByID('radio-bill')
    form_invoice = sunbro.FindByID('radio-invoice')

    # Js errors
    error_name_account = sunbro.FindByCSS('#id_name + span')
    error_giro = sunbro.FindByCSS('#id_lob + span')
    error_rut = sunbro.FindByCSS('#id_rut + span')
    error_address = sunbro.FindByCSS('#id_address + span')
    error_region = sunbro.FindByCSS('error_region')
    error_commune = sunbro.FindByCSS('error_communes')
    error_contact = sunbro.FindByCSS('error_contact')
    error_email_account = sunbro.FindByCSS('#email-invoice + span')

    # Form stuff
    form_title = sunbro.FindByCSS(".pm-body .help-form h1")
    name = sunbro.FindByCSS('.pm-body .help-form #name')
    email = sunbro.FindByCSS('.pm-body .help-form #email')
    body = sunbro.FindByCSS('.pm-body .help-form #support_body')
    send = sunbro.FindByCSS('.pm-body .help-form #submit-help')
    send_ok = sunbro.FindByCSS('.pm-body #help-ok-message')
    contact_error_name = sunbro.FindByCSS('.pm-body .help-form #error-name')
    contact_error_email = sunbro.FindByCSS('.pm-body .help-form #error-email')
    contact_error_body = sunbro.FindByCSS('.pm-body .help-form #error-body')

    # Advertisement empty cart
    text_empty_cart_message = sunbro.FindByClass('summaryPromo')
    empty_cart = text_empty_cart_message

    # Warning / error message after refused / cancelled payment
    warning_message = sunbro.FindByCSS('h1')

    # Links
    link_to_more_info = sunbro.FindByClass('summaryPromo-disclaimerLink')

    # Success Payment
    contactanos = sunbro.FindByCSS('.nohistory a')

    # Refused Payment
    avisanos = sunbro.FindByCSS('.troubles #help-payment')

    # title
    text_message_payment_subject = sunbro.FindByCSS('.summaryPage-titleInsertingfeeRow .summaryPage-small strong')

    #Modal for cart validation
    modal_cart = sunbro.FindByCSS('.ShoppingCartModal')
    modal_cart_close = sunbro.FindByCSS('.ShoppingCartModal .ShoppingCartModal-HeaderClose')
    modal_cart_header_title = sunbro.FindByCSS('.ShoppingCartModal .ShoppingCartModal-HeaderTitle')
    modal_cart_body_title = sunbro.FindByCSS('.ShoppingCartModal-HeaderTitle')
    modal_cart_body_text = sunbro.FindByCSS('.ShoppingCartModal .ShoppingCartModal-BodyText')
    modal_cart_button = sunbro.FindByCSS('.ShoppingCartModal-FooterButton')

    def __init__(self, *args, **kwargs):
        super(SummaryPayment, self).__init__(*args, **kwargs)
        self.sticky_bar = StickyBarPayment(self._driver)

    def go_summary(self):
        self._driver.get(conf.SSL_URL + '/pagos')

    def go_summary_from_upselling(self, ad_id, product_id):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod={}&ftype=2&from=newad&lt=4'.format(ad_id, product_id))

    def add_product(self, list_id, product):
        self._driver.get(
            conf.SSL_URL + '/pagos?id=' + list_id + '&prod=' + product)
        self.wait.until_loaded()

    def add_store(self, store_type):
        self._driver.get(conf.SSL_URL + '/pagos?prod=' + store_type)

    def add_element_to_cart(self, prod, list_id=None):
        extra_info = ''
        if str(prod) == '9' or str(prod) == '104':
            extra_info = '&lt=1'
        if list_id is None:
            self._driver.get(conf.SSL_URL + '/pagos?prod='+ str(prod) + extra_info)
        else:
            self._driver.get(conf.SSL_URL + '/pagos?prod='+ str(prod) + '&id='+ str(list_id) + extra_info)
        self.wait.until_loaded()

    def get_tr_by_number(self, number):
        return self._driver.find_element_by_id('row' + number)

    def press_link_to_more_info(self):
        self.link_to_more_info.click()

    @property
    def radio_bill(self):
        return self.form_bill

    @property
    def radio_invoice(self):
        return self.form_invoice

    def choose_bill(self):
        self.radio_bill.click()
        self.wait.until_not_visible("input_giro")

    def choose_invoice(self):
        self.radio_invoice.click()
        self.wait.until_visible("input_giro")

    def press_link_to_pay_products(self, method = 'webpay'):
        self.sticky_bar.pay(method)

    def pay_with_webpay(self):
        self.link_to_pay_products.click()
        payment = Payment(self._driver)
        payment.wait.until_visible('radio_webpay')
        payment.radio_webpay.click()
        self.link_to_pay_products.click()

    def pay_with_oneclick(self):
        self.link_to_pay_products.click()
        payment = Payment(self._driver)
        payment.wait.until_visible('radio_oneclick')
        payment.radio_webpay.click()
        self.link_to_pay_products.click()

    def pay_with_khipu(self):
        self.link_to_pay_products.click()
        payment = Payment(self._driver)
        payment.wait.until_visible('radio_khipu')
        payment.radio_khipu.click()
        self.link_to_pay_products.click()

    def pay_with_credits(self):
        self.link_to_pay_products.click()
        payment = Payment(self._driver)
        payment.wait.until_visible('radio_credits')
        payment.radio_credits.click()
        self.link_to_pay_products.click()

    def get_table_data(self):
        table = []
        for row in self.rows_product_list:
            table.append(
                [cell.text for cell in row.find_elements_by_css_selector('.summaryTable-cell')]
            )

        print(table)
        return table

    def get_displayed_table_data(self, table_name=None, table_elem=None):
        """ Get the visible cell values/text in a table """
        table_values = []
        if not table_elem:
            if not table_name:
                raise Exception("What u doing??? set a name or give an element please")
            table_elem = getattr(self, table_name)
        for row in table_elem.find_elements_by_css_selector('.summaryTable-drag'):
            if row.is_displayed():
                row_values = []
                for cell in row.find_elements_by_css_selector('.summaryTable-cell'):
                    try:
                        if cell.is_displayed():
                            print("row val: {}".format(cell.text))
                            row_values.append(cell.text)
                    except StaleElementReferenceException:
                        print("Cell has changed")
                        pass
                table_values.append(row_values)
        return table_values

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def text_message_ad(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')



class Payment(pages.BasePage):

    """Page object for the payment page"""

    pay_button = sunbro.FindByID('submit-bill')
    price_list_item = sunbro.FindByCSS('.graybox .plane + span')
    more_info = sunbro.FindByID('more-info-payment')
    questions = sunbro.FindAllByCSS('.pm-content .popup-acc-head')
    price_question = sunbro.FindByCSS('.pm-content .legal-info.bill-info')
    radio_oneclick = sunbro.FindByCSS('.paymentMethod.__oneclick')
    radio_webpay = sunbro.FindByCSS('.paymentMethod.__webpay')
    radio_khipu = sunbro.FindByCSS('.paymentMethod.__khipu')
    radio_credits = sunbro.FindByCSS('.paymentMethod.__credits')

    def go_to_pay_bump(self, list_id, prod = 1):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod={}&ftype=1'.format(list_id, prod))
        self.wait.until_loaded()

    def press_pay_button(self):
        self.pay_button.click()

    def go_to_pay_inserting_fee(self, ad_id):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod=60'.format(ad_id))
        self.wait.until_loaded()
        return Payment(self._driver)

class PaymentVerifyError(pages.BasePage):

    """page object of payment verify when we found problems"""
    title_oops = sunbro.FindByCSS('h1')
    text_fail = sunbro.FindByCSS('h2')

    PROD_NON_APPLICABLE = "El producto no puede ser aplicado a tu aviso"

class NextgenPayment(pages.BasePage):

    """Page object for Nextgen payment page"""

    pay_button = sunbro.FindByID('submit-bill')
    price_value = sunbro.FindByCSS('.table tbody tr td:nth-child(2)')
    price_name = sunbro.FindByCSS('.table tbody tr td:nth-child(1)')
    bill_checkbox = sunbro.FindByID('radio-bill')
    success_title = sunbro.FindByCSS('.pay_apps_success_title')

    buy_head = sunbro.FindByCSS('.accordion-head.buy-data')
    buy_data = sunbro.FindByCSS('.accordion-body.buy-data')
    buy_table = sunbro.FindByCSS('.accordion-body.buy-data .sales')

    def go_to_pay_bump(self, list_id, prod):
        self._driver.get(conf.SSL_URL + '/pay_apps?id={0}&prod={1}'.format(list_id, prod))
        self.wait.until_loaded()

    def press_pay_button(self):
        self.pay_button.click()

class CommonAccountElements(pages.BasePage):

    """Page object that define "my ads", "my profile" and "buy" liks"""

    # Links
    link_my_ads = sunbro.FindByID('dashboard_my_ads')
    link_my_profile = sunbro.FindByID('dashboard_profile')
    link_summary = sunbro.FindByID('dashboard_summary')
    text_cart_amount = sunbro.FindByID('summary-bar-amount')
    link_profile = sunbro.FindByCSS('#dashboard_profile a')


class EditConfirmationPage(pages.BasePage):

    """This models ad edit confirmation page"""
    buy_products_suggestion = sunbro.FindByCSS('.title.text-center')
    daily_box = sunbro.FindByID('daily_box')
    weekly_box = sunbro.FindByID('weekly_box')
    gallery_box = sunbro.FindByID('gallery_box')
    refund_message = sunbro.FindByCSS('.notice')


class DashboardProfile(pages.BasePage):

    """Page object of the profile page (need to be logged in)"""

    # Radios
    radio_persona_input = sunbro.FindByID('is_company_p')
    radio_profesional_input = sunbro.FindByID('is_company_c')

    # Button
    button_save = sunbro.FindByID('confirm_btn')

    @property
    def radio_persona(self):
        return self.radio_persona_input.find_element_by_xpath('../ins')

    @property
    def radio_profesional(self):
        return self.radio_profesional_input.find_element_by_xpath('../ins')

    def go_profile(self):
        self._driver.get(conf.SSL_URL + '/cuenta/edit')


class DashboardAd(pages.BasePage):

    subject = sunbro.FindByCSS('.da-subject')
    status = sunbro.FindByCSS(".da-status")

    edit_button = sunbro.FindByCSS(".da-options #da-edit")
    delete_button = sunbro.FindByCSS(".da-options #da-delete")

    highlight = sunbro.FindByClass('#da-highlight')
    product_list = sunbro.FindByClass('product-list-wrap')

    bump = sunbro.FindByCSS('.bump-select')
    gallery = sunbro.FindByCSS('.gallery-select')
    label = sunbro.FindByCSS('.label-select')

    label_tag = sunbro.FindByCSS('.fa-tag')
    # activate ad
    option_activate = sunbro.FindByCSS('#activate-da.active')
    option_bump_activate = sunbro.FindByCSS('#activate-da:not(.active)')

    btn_unpaid = sunbro.FindByCSS('.btn-unpaid')

    products_field = {
        Products.BUMP: 'bump',
        Products.WEEKLY_BUMP: 'weekly_bump',
        Products.DAILY_BUMP: 'daily_bump',
        Products.GALLERY: 'gallery',
        Products.GALLERY_1: 'gallery_1',
        Products.GALLERY_30: 'gallery_30',
        Products.LABEL: 'label',
    }
    products_select = {
        Products.BUMP: 'bump',
        Products.WEEKLY_BUMP: 'bump',
        Products.DAILY_BUMP: 'bump',
        Products.GALLERY: 'gallery',
        Products.GALLERY_1: 'gallery',
        Products.GALLERY_30: 'gallery',
        Products.LABEL: 'label',
    }

    @lazy
    def get_status(self):
        return self.status.get_attribute('data-text')

    @lazy
    def list_id(self):
        return int(self._driver.get_attribute('data-list_id'))

    @lazy
    def ad_id(self):
        for d in ['data-ad_id', 'data-da_id']:
            try:
                return int(self._driver.get_attribute('data-ad_id'))
            except:
                pass

    def go_to_pay(self):
        self.btn_unpaid.click()
        summary = SummaryPayment(self._driver)
        return summary

    def is_selected(self, product, param=None):
        """ Checks if the product is selected based on the id of the product """
        select = Select(getattr(self, self.products_select[product]))
        value = str(select.first_selected_option.get_attribute("value"))
        if param is not None:
            print("Searching param:{0} actual:{1}".format(param, value))
            return value == str(param)
        return self.products_field[product] == value

    def select_product(self, product, param=None):
        """ Selects a product based on the id of the product """
        if 'is-visible' not in self.product_list.get_attribute('class'):
            self._driver.parent.execute_script("$('body').find('li[data-list_id={}]').find('#da-highlight').click()".format(self.list_id))
        select = Select(getattr(self, self.products_select[product]))

        if self.products_field[product] == 'label':
            if param is None:
                select.select_by_value(str(1))
            else:
                select.select_by_value(str(param))
        else:
            self.wait.until(lambda x: select.options[1].is_displayed())
            select.select_by_value(self.products_field[product])

    def unselect_product(self, product):
        if 'is-visible' not in self.product_list.get_attribute('class'):
            self.highlight.click()
        select = Select(getattr(self, self.products_select[product]))
        select.select_by_value("0")

    def get_product_options(self, product, wd):
        if 'is-visible' not in self.product_list.get_attribute('class'):
            wd.execute_script("$('#da-highlight')[0].click()")
        if 'delete_reasons' in wd.current_url:
            raise Exception('ShouldNotBeHere', 'delete_reasons')
        select = Select(getattr(self, self.products_select[product]))
        values = []
        for option in select.options:
            values.append(option.get_attribute("value"))
        print(values)
        return values

    def has_product_selector_disabled(self, product):
        if 'is-visible' not in self.product_list.get_attribute('class'):
            self.highlight.click()
        select = getattr(self, self.products_select[product])
        return not select.is_enabled()

    def has_product_available(self, product, wd):
        values = self.get_product_options(product, wd)
        return product in values

    def get_product_price(self, product, param=None):
        select = Select(getattr(self, self.products_select[product]))
        values = []
        price = '0'
        for option in select.options:
            if param is not None:
                if option.get_attribute("value") == str(param):
                    price = option.get_attribute("text")
            else:
                if option.get_attribute("value") == str(product):
                    price = option.get_attribute("text")
        print('Price {0}'.format(price))
        arr_price = price.split(" - ")
        price = arr_price[len(arr_price) - 1]
        return price


class DashboardMyAds(pages.BasePage):

    """Page object of dashboard to manage the user's ads list"""

    # Pack message
    pack_message = sunbro.FindByCSS("div.pack-message div.pack-message-text")

    # Lists
    list_inactive_ads = sunbro.FindAllByCSS(".single-da.inactive")
    list_active_ads = sunbro.FindAllByCSS(".single-da:not(.inactive)")
    list_all_ads = sunbro.FindAllByCSS('.single-da')
    list_select_products = sunbro.FindAllByCSS(".single-da .products_popup")
    checkout_button = sunbro.FindByClass('shopping-cart')

    # Text
    text_subjects = sunbro.FindAllByCSS(".da-subject")

    # Buttons
    buttons_edit = sunbro.FindAllByCSS(".da-options #da-edit")
    buttons_delete = sunbro.FindAllByCSS(".single-da:not(.inactive) .da-options #da-delete")
    buttons_highlight = sunbro.FindAllByCSS(".da-options #da-highlight")
    buttons_edit_inactive = sunbro.FindAllByCSS(".da-options__option--inactive#da-edit")
    buttons_delete_inactive = sunbro.FindAllByCSS(".da-option-inactive#da-delete")
    checkboxes_bump = sunbro.FindAllByCSS(".icheck.bump-check")

    # background (to send spaces on infinite scroll)
    background = sunbro.FindByCSS("body")

    # Inputs
    search = sunbro.FindByID("search-ads")
    clear_search = sunbro.FindByID("clear-search")

    # Container for username of account in iOS app
    ios_account_name = sunbro.FindByID("ios_account_name")

    products_in_cart = sunbro.FindByID('summary-bar-amount')

    # Info messages
    activate_msg_success = sunbro.FindByCSS('.activate-msg.success')

    # Definitions of SELECTORS
    # Use this definitions to find elements in the page or in the lists previously defined
    # The objective of this block is avoid definition of css selectors
    # directly on tests
    selector_css_highlight = ".da-options #da-highlight"
    selector_css_product_list = ".product-list-wrap"

    selector_id_bump = "bump-"
    selector_id_gallery = "gallery-"
    selector_id_label = "label-"

    selector_attribute_id = "data-list_id"

    def __init__(self, *args, **kwargs):
        super(DashboardMyAds, self).__init__(*args, **kwargs)
        self.sticky_bar = StickyBarPayment(self._driver)

    def select_products_by_ids(self, ld):
        """
        Usage:
              ld: {12121: [Products.BUMP, Products.GALLERY]}
              Use a hash with the list_id as key, and a list of the products to apply
        """
        for list_id, products_list in ld.items():
            for product_id in products_list:
                self.active_ads[list_id].select_product(product_id)

    def delete_products_by_ids(self, ld):
        """
        Usage:
              ld: {12121: [Products.BUMP, Products.GALLERY]}
              Use a hash with the list_id as key, and a list of the products to delete
        """
        for list_id, products_list in ld.items():
            for product_id in products_list:
                self.active_ads[list_id].unselect_product(product_id)

    @lazy
    def active_ads(self):
        active_ads = {}
        for active_ad in map(DashboardAd, self.list_active_ads):
            active_ads[active_ad.list_id] = active_ad
        return active_ads

    @lazy
    def inactive_ads(self):
        inactive_ads = {}
        for ad in map(DashboardAd, self.list_inactive_ads):
            inactive_ads[ad.ad_id] = ad
        return inactive_ads

    @lazy
    def inactive_ads_list(self):
        """ Returns a list """
        inactive_ads = []
        for ad in map(DashboardAd, self.list_inactive_ads):
            inactive_ads.append(ad)
        return inactive_ads

    @lazy
    def all_ads(self):
        all_ads = {}
        for ad in map(DashboardAd, self.list_all_ads):
            all_ads[ad.list_id] = ad
        return all_ads

    def search_ad_by_subject(self, subject):
        for ad in map(DashboardAd, self.list_all_ads):
            print('Dashboard_ad:{}'.format(ad.subject.text))
            if ad.subject.text == subject:
                return ad
        return None

    def go_to_delete_ad(self, subject):
        ad = self.search_ad_by_subject(subject)
        ad.delete_button.click()
        return ad

    def activate_ad(self, ad=None, subject=None):
        if not ad:
            if subject:
                ad = self.search_ad_by_subject(subject)

        self.scroll_to_bottom()
        ad.option_activate.click()
        self.wait.until_visible('activate_msg_success')

    def click_ad_by_subject(self, subject):
        self._driver.find_element_by_partial_link_text(subject).click()

    def is_product_selected(self, web_element, product, list_id=None):
        if list_id is None:
            list_id = web_element.list_id
        return web_element.is_selected(product)

    def go(self):
        self.go_dashboard()
        return self

    def go_dashboard(self, parameter=None):
        if parameter is None:
            if self._driver.current_url == conf.SSL_URL + '/dashboard':
                self._driver.refresh()
            else:
                self._driver.get(conf.SSL_URL + '/dashboard')
        else:
            self._driver.get(conf.SSL_URL + '/dashboard?' + parameter)
        self.wait.until_loaded()

    def create_ad_list(self):
        """return list with sublists with subject and status
           Example:
           ads_list = [[<subject1>, <status1>],
                       [<subject2>, <status2>], ...]
        """
        ads_list = []
        for ad in self.list_all_ads:
            row_ad = []
            row_ad.append(ad.find_element_by_css_selector('.da-subject').text)
            row_ad.append(ad.find_element_by_css_selector(
                '.da-status').get_attribute('data-text'))
            ads_list.append(row_ad)
        return ads_list

    def ads_list(self):
        """ return list with subject """
        ads_list = []
        for ad in self.list_all_ads:
            ads_list.append(
                ad.find_element_by_css_selector('.da-subject').text)
        return ads_list

    def search_status_by_subject(self, subject):
        """return the status of certain ad by subject"""

        for ad in self.create_ad_list():
            if ad[0] == subject:
                return ad[1]

        return None


class Paysim(pages.BasePage):

    """Page object of the paysim"""

    # Inputs
    input_amount = sunbro.FindByID('input_amount')
    input_order = sunbro.FindByID('input_order')

    # Buttons
    button_accept = sunbro.FindByID('button_accept')
    button_pay = sunbro.FindByClass('summaryBar-payContainer')
    button_refuse = sunbro.FindByID('button_refuse')
    button_cancel = sunbro.FindByID('button_cancel')
    button_go_site = sunbro.FindByID('continue')
    input_transaction_type = sunbro.FindByID('input_type')
    input_amount = sunbro.FindByID('input_amount')
    input_order = sunbro.FindByID('input_order')
    input_session = sunbro.FindByID('input_session')
    input_success_url = sunbro.FindByID('input_surl')
    input_failure_url = sunbro.FindByID('input_furl')

    def pay_accept(self):
        self.wait.until_present('button_accept')
        self.button_accept.click()
        self.wait.until_present('button_go_site')
        self.button_go_site.click()

    def pay_refuse(self):
        self.button_refuse.click()
        self.button_go_site.click()

    def pay_cancel(self):
        self.button_cancel.click()
        self.button_go_site.click()

    def change_order(self, value):
        self.input_order.clear()
        self.input_order.send_keys(value)

    def paysim_populate(self, *args, **kwargs):
        for field, value in kwargs.items():
            input = getattr(self, "input_{0}".format(field))
            input.clear()
            input.send_keys(value)


class PaymentVerify(pages.BasePage):

    """page object of payment verify"""
    text_congratulations = sunbro.FindByCSS('.paymentheader-subtitle')
    buy_head = sunbro.FindByCSS('.paymentBuy-Data .icon-arrow-down')
    sale_data = sunbro.FindByCSS('.icon-yapo.icon-arrow-down.paymentData-Header')
    sale_table = sunbro.FindByCSS('.table.table-striped.purchase-list.paymentSuccess-table')
    buy_data = sunbro.FindByCSS('.accordion-body.buy-data')
    buy_table = sunbro.FindByCSS('.accordion-body.buy-data .sales')
    voucher_link = sunbro.FindByID('voucher_link')
    purchase_table = buy_table

    PURCHASE_SUCCESS = 'Tu compra se realiz� exitosamente'


class Dashboard(pages.BasePage):
    deletion_messages = sunbro.FindByCSS('#da-highlight-deleting')
    profile_image = sunbro.FindByCSS('.dashboardHeader .profile-image')

    def go(self):
        self._driver.get(conf.SSL_URL + "/dashboard")

    @property
    def title_legend(self):
       header_elements = HeaderElements(self._driver)
       return header_elements.drawer_title

class MobileHome(pages.BasePage):

    """Page object for mobile home"""
    my_account_button = sunbro.FindByID('acc_bar_login')
    ad_insert_button = sunbro.FindByID('acc_page_da_insert')
    draft_wrap = sunbro.FindByCSS('.message.message-draft')
    android_banner = sunbro.FindByCSS('.g-button')
    android_banner_close_button = sunbro.FindByCSS('#android_banner_close')
    iphone_app_tag = sunbro.FindByCSS('meta[name="apple-itunes-app"]')
    yapo_logo = sunbro.FindByCSS('.logo-home')
    region_list = sunbro.FindAllByCSS('.regions_list li')
    select_region = sunbro.FindByCSS('h3')
    footer_links = sunbro.FindAllByCSS('footer ul.nav li a')
    full_version = sunbro.FindByCSS('.link_fullversion')
    pp_banner = sunbro.FindByCSS('.ppages-banner a img')

    def go(self):
        self._driver.get(conf.MOBILE_URL)
        self.wait.until_loaded()

    def go_to(self, slug):
        self._driver.get(conf.MOBILE_URL + slug)
        self.wait.until_loaded()


class MyAds(pages.BasePage):

    """ My ads page in mobile """

    email_input = sunbro.FindByID('myads_email')
    send_input = sunbro.FindByID('myads_submit')
    ok_message = sunbro.FindByClass('myDas-confirmationInfo')
    error_message = sunbro.FindByID('err_myads_email')
    ads_without_account  = sunbro.FindByID('ads_without_account')
    confirmation_info = sunbro.FindByCSS('.myDas-confirmationInfo')

    def go(self):
        self._driver.get(conf.SSL_URL + '/login')
        self.ads_without_account.click()
        self.wait.until_visible('email_input')

    def request(self, email):
        """ Fills the form with a user e-mail, so my ads e-mail is sent """
        self.email_input.clear()
        self.email_input.send_keys(email)
        self.send_input.click()
        self.wait.until_present('ok_message')

    def request_fail(self, email):
        """ Fills the form with an invalid e-mail """

        self.email_input.clear()
        self.email_input.send_keys(email)
        self.send_input.click()
        self.wait.until_present('error_message')

    def go_and_request(self, email):
        self.go()
        self.request(email)

    def go_and_fail_request(self, email):
        self.go()
        self.request_fail(email)


class RulesDisallowed(pages.BasePage):

    title = sunbro.FindByID('title_rd')
    products_not_allowed = sunbro.FindAllByClass('list')
    subtitle = sunbro.FindByID('subtitle_rd')
    link_go_back_rules = sunbro.FindByID('go_back_rules')

    def go_rules_disallowed(self):
        self._driver.get(
            conf.MOBILE_URL + "/ayuda/productos_no_permitidos.html")


class RulesIllegal(pages.BasePage):

    title = sunbro.FindByID('title_ri')
    products_illegal = sunbro.FindAllByClass('list')
    subtitle = sunbro.FindByID('subtitle_ri')
    link_go_back_rules = sunbro.FindByID('go_back_rules_ri')

    def go_rules_illegal(self):
        self._driver.get(conf.MOBILE_URL + "/ayuda/productos_ilegales.html")


class HelpPrivacity(pages.BasePage):

    title = sunbro.FindByCSS('.content h1')
    subtitles = sunbro.FindAllByCSS('.content h2')

    def go_help_privacity(self):
        self._driver.get(conf.MOBILE_URL + "/ayuda/privacidad.html")


class PageNotFound(pages.BasePage):

    """Page object for the 404 page"""

    title = sunbro.FindByCSS('#site-wrapper h1')

    def go(self):
        self._driver.get(conf.MOBILE_URL + '/yolo')


class StickyBarPayment(pages.BasePage):
    """Page object for the Sticky bar of payment"""
    bar = sunbro.FindByID('sticky-bar')
    new_bar = sunbro.FindByID('yapo-sticky-bar')
    new_bar_off = sunbro.FindByCSS('.yapo-sticky-bar--off')
    btn_pay = sunbro.FindByCSS('#sticky-bar #mobileCheckout')
    new_btn_pay = sunbro.FindByCSS('.yapo-sticky-bar__buy-text span')
    amount = sunbro.FindByCSS('#sticky-bar #total-amount')
    new_amount = sunbro.FindByCSS('.yapo-sticky-bar__total-text')
    total_text = sunbro.FindByClass('total')
    webpay = sunbro.FindByCSS('.paymentMethod.__webpay')
    oneclick = sunbro.FindByCSS('.paymentMethod.__oneclick')

    def pay(self, method='webpay'):
        self.wait.until_visible('btn_pay')
        self.btn_pay.click()
        self.wait.until_visible(method)
        getattr(self, method).click()
        self.wait.until_visible('btn_pay')
        self.btn_pay.click()


class DeleteUnpaidModal(pages.BasePage):

    modal = sunbro.FindByCSS('.deleteModal.__show')
    close_button = sunbro.FindByCSS('.deleteModal-button.__Close')
    yes_button = sunbro.FindByCSS('.deleteModal-button.__Agree')
    no_button = sunbro.FindByCSS('.deleteModal-button.__Disagree')

    def open(self):
        self.wait.until_visible('yes_button')
        self.wait.until_visible('no_button')

    def confirm(self):
        self.yes_button.click()
        self.wait.until_visible('close_button')
        self.close_button.click()
        self.wait.until_not_visible('modal')

    def reject(self):
        self.no_button.click()
        self.wait.until_not_visible('modal')

