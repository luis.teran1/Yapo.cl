""" Default values for ad insert """

DEFAULT_AD_INFO = {
    'category': 5020,
    'estate_type': '1',
    'rooms':1,
    'brand':1,
    'model':1,
    'version':1,
    'regdate':2015,
    'mileage': 1000,
    'cubiccms': 1,
    'gearbox':1,
    'fuel':1,
    'cartype':1,
    'subject': 'My ad',
    'body': 'The body of my ad',
    'condition':1,
    'footwear_gender':1,
    'footwear_type':[1],
    'jc':[20],
    'sct':[11],
    'plates':'ABCD10',
    'contract_type':'1',
    'working_day':'3',
}

DEFAULT_USER_INFORMATION = {
    'company_ad': False,
    'rut': '55555555-5',
    'name': 'Juan Perez',
    'email': 'juan.perez@noexiste.cl',
    'email_confirm': 'juan.perez@noexiste.cl',
    'ai_create_account': False,
    'phone': '55555555',
    'phone_type': 'm',
    'passwd': '123123123',
    'passwd_ver': '123123123',
}

DEFAULT_GEO_INFO = {
    'region': 15,
    'communes': 331,
    'add_location': False,
}

DEFAULT_ALL = DEFAULT_AD_INFO.copy()
DEFAULT_ALL.update(DEFAULT_USER_INFORMATION)
DEFAULT_ALL.update(DEFAULT_GEO_INFO)

REAL_ESTATE = {'estate_type': '1', 'rooms': '2'}
CATEGORY_PARAMS = {
    1220: REAL_ESTATE,
    1240: REAL_ESTATE,
    1260: REAL_ESTATE,
    2020: {
        'brand': '2',
        'model': '2',
        'version': '2',
        'regdate': '2015',
        'gearbox': '1',
        'fuel': '1',
        'cartype': '1',
        'mileage': '10000',
        'plates': 'ABCD10',
    },
    2060: {
        'regdate': '2015',
        'mileage': '10000',
        'cubiccms': '1',
        'plates': 'ABC10',
    },
    2040: {
        'regdate': '2015',
        'mileage': '10000',
        'fuel': '1',
        'gearbox': '1',
        'plates': 'ABCD10',
    },
    4020: {
        'condition': '1',
        'gender': '3',
    },
    4040: {
        'condition': '1',
        'gender': '3',
    },
    4060: {
        'gender': '3',
    },
    4080: {
        'condition': '1',
        'footwear_gender': '3',
        'footwear_type': '1',
    },
    9020: {
        'condition': '1',
    },
    9040: {
        'condition': '1',
    },
    9060: {
        'condition': '1',
    },
    7020: {
        'jc': ['20'],
        'contract_type': '1',
        'working_day': '1',
    },
    7040: {
        'jc': ['20'],
    },
    7060: {
        'sct': ['1'],
    },
}
