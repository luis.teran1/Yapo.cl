# -*- coding: latin-1 -*-
from yapo.pages import BasePage
import sunbro, nose_selenium


class ConfirmAccount(BasePage):

    """
    Page object for create account mail
    """
    confirm_account = sunbro.FindAllByCSS('a[href*="/cuenta/confirm"]')


class AdPublished(BasePage):

    """
    Page object for ad published mail
    """
    ad_subject = sunbro.FindByCSS('.subject')
    full_mail = sunbro.FindByCSS('.mailview')


class ProUser(BasePage):

    """
    Page object for ad published mail
    """
    dashboard_link = sunbro.FindByCSS('a[href*="dashboard"]')
    full_mail = sunbro.FindByCSS('.mailview')


class MyAds(BasePage):

    """
    Page object for my ads mail
    """
    subjects = sunbro.FindAllByCSS('.subject')
    title = sunbro.FindByCSS('#title')


class AdPendingReview(BasePage):

    """
    Page object for ad pending review
    """
    title = sunbro.FindByCSS('#title')
    full_mail = sunbro.FindByCSS('.mailview')


class Mailinator(BasePage):

    mails = sunbro.FindAllByCSS('.subject.ng-binding')
    mail = sunbro.FindByCSS('a .subject')
    mail_links = sunbro.FindAllByCSS("a[onclick^='showmail(']")


class FakeMail(BasePage):

    """
    FakeMail page objects to validate real emails
    """
    email = None
    mails = sunbro.FindAllByCSS('#email-list li')
    mail = sunbro.FindByCSS('#email-list li a[href*="/inbox/"]')
    mail_links = sunbro.FindAllByCSS('#email-list li a[href*="/inbox/"]')

    def go_to_inbox(self, mail):
        FakeMail.email = mail.split('@')
        #www.fakemailgenerator.com/inbox/dayrep.com/yapo/
        self._driver.get('http://www.fakemailgenerator.com/inbox/{0}/{1}/'.format(FakeMail.email[1], FakeMail.email[0]))
        #self._driver.get('http://mailinator.com/inbox.jsp?to={0}'.format(mail))
        self.wait.until_loaded()

    def go_to_email(self, email_num):
        mail_id = FakeMail.mail_links[email_num].get_attribute("href").split('-')[1][:-1]
        #http://www.fakemailgenerator.com/email/dayrep.com/yapo/message-72137659/
        print('http://www.fakemailgenerator.com/inbox/{0}/{1}/message-{2}/'.format(FakeMail.email[1], FakeMail.email[0], mail_id))
        self._driver.get('http://www.fakemailgenerator.com/email/{0}/{1}/message-{2}/'.format(FakeMail.email[1], FakeMail.email[0], mail_id))
