from yapo.pages import BasePage
from lazy import lazy
from yapo import conf
from yapo.pages.generic import AccountBar
from yapo.pages.desktop import Dashboard
import sunbro
import yapo.utils
from yapo.steps.accounts import login_and_go_to_dashboard


class EditForm(BasePage):

    """Store creation page"""
    name = sunbro.FindByID('name_input')
    info_text = sunbro.FindByID('info_text_input')
    url = sunbro.FindByID('url_input')
    phone = sunbro.FindByID('phone_input')
    region = sunbro.FindByID("region_input")
    commune = sunbro.FindByID("commune_input")
    address = sunbro.FindByID("address_input")
    address_number = sunbro.FindByID("address_number_input")
    website_url = sunbro.FindByID("website_url_input")
    hide_phone = sunbro.FindByID("hide_phone_input")
    hide_phone_label = sunbro.FindByCSS("label[for='hide_phone_input']")
    submit = sunbro.FindByID("save_button")

    cpanel_logged_info = sunbro.FindByID("cp_logged_info")

    save_message = sunbro.FindByClass("idle-store")

    main_image_wrapper = sunbro.FindByID('upload_image_wrap')
    main_image = sunbro.FindByCSS('#upload_image_wrap > img')
    main_image_popup = sunbro.FindByID('default_image_popup')
    main_image_input = sunbro.FindByCSS('#default_image_popup input[type="file"]')
    main_image_submit = sunbro.FindByID("add_image")
    main_image_small_preview = sunbro.FindByID("upload_image")

    deactivate = sunbro.FindByID("deactivate_store")
    activate = sunbro.FindByID("activate_store")

    deactivate_message = sunbro.FindByCSS("#activate_store_popup span")
    deactivate_yes = sunbro.FindByID("deactivate_yes")
    deactivate_no = sunbro.FindByID("deactivate_no")
    activate = sunbro.FindByID("activate_store")

    info_text_error = sunbro.FindByCSS(".store-edit-desc .error")
    phone_error = sunbro.FindByID("err_phone")
    website_error = sunbro.FindByID("err_website_url")

    logo_upload_field = sunbro.FindByCSS('#logo_upload input[type="file"]')
    main_upload_field = sunbro.FindByCSS('#upload_image input[type="file"]')

    logo_uploaded = sunbro.FindByCSS('#logo_upload > img')

    expiration_date = sunbro.FindByCSS(".store-publication-date strong")
    extend_form = sunbro.FindByCSS(".store-buy-plans")

    go_to_store = sunbro.FindByCSS("#go_to_store_action a")
    extend_form = sunbro.FindByCSS(".store-active-foot")


    store_type = sunbro.FindByCSS(".store-plan-tag")

    logo_error_small = sunbro.FindByID("err_logo_error_image_too_small")

    sidebar_store = sunbro.FindByID("link_store")

    def get_expiration_date(self):
        return yapo.utils.parse_date(self.expiration_date.text)

    def go(self, user="many@ads.cl", password="123123123"):
        if not self._driver.find_elements_by_css_selector('.icon-user-circled'):
            login_and_go_to_dashboard(self._driver, user, password)
        else:
            self.go_to_dashboard()
        self.go_without_login()

    def go_to_dashboard(self):
        self._driver.get(conf.SSL_URL + "/dashboard")
        self.wait.until_loaded()

    def go_without_login(self):
        self._driver.get(conf.SSL_URL + "/store_edit")
        self.wait.until_loaded()

    def set_stock_image(self, name, submit=True):
        self.main_image_wrapper.click()
        popup = MainImagePopup(self._driver)
        getattr(popup, '{0}_label'.format(name)).click()
        if submit:
            popup.submit.click()

    @property
    def errors(self):
        return filter(
            lambda x: x.is_displayed(),
            self._driver.find_elements_by_class_name("error")
        )


class MainImagePopup(BasePage):

    """Main image selection popup"""
    popup = sunbro.FindByID('default_image_popup')
    input = sunbro.FindByCSS('#default_image_popup input[type="file"]')
    submit = sunbro.FindByID("add_image")
    small_preview = sunbro.FindByCSS("#upload_image img")

    stock_real_estate = sunbro.FindByID("real_estate_bg")
    stock_real_estate_label = sunbro.FindByCSS("label[for='real_estate_bg']")

    stock_cars = sunbro.FindByID("cars_bg")
    stock_cars_label = sunbro.FindByCSS("label[for='cars_bg']")

    stock_others = sunbro.FindByID("others_bg")
    stock_others_label = sunbro.FindByCSS("label[for='others_bg']")

    stock_real_estate_check = sunbro.FindByXPath('//*[@id="real_estate_bg"]/..')
    stock_cars_check = sunbro.FindByXPath('//*[@id="cars_bg"]/..')
    stock_others_check = sunbro.FindByXPath('//*[@id="others_bg"]/..')


class BuyStore(BasePage):

    """Page to buy an store"""
    monthly = sunbro.FindByID('btn-4')
    quarterly = sunbro.FindByID('btn-5')
    biannual = sunbro.FindByID('btn-6')
    annual = sunbro.FindByID('btn-7')
    submit = sunbro.FindByID('checkout')

    bill = sunbro.FindByCSS('label[for="radio-bill"]')
    invoice = sunbro.FindByCSS('label[for="radio-invoice"]')

    form_element = sunbro.FindByCSS("#payment_form")
    link_store = sunbro.FindByID('link_store')

    @property
    def form(self):
        return InvoiceForm(self._driver)

    def wait_for_animation(self):
        def animation_done(wd):
            go_down = wd.find_element_by_css_selector('.go-down')
            margin_top = go_down.value_of_css_property('margin-top')
            return margin_top == '-2px'

        self.wait.until(animation_done)

    def go(self, user="many@ads.cl", password="123123123", wait_animation=True):
        wd = self._driver
        wd.get(conf.DESKTOP_URL + "/region_metropolitana")
        page = AccountBar(wd)
        page.login(user, password)
        Dashboard(wd).go()
        store_link = wd.find_element_by_id("link_store")
        store_link.click()
        if wait_animation:
            self.wait_for_animation()

    def go_without_login(self, wait_animation=True):
        wd = self._driver
        Dashboard(wd).go()
        store_link = wd.find_element_by_id("link_store")
        store_link.click()
        if wait_animation:
            self.wait_for_animation()


class InvoiceForm(BasePage):

    """Invoice for on store purchase"""
    name = sunbro.FindByCSS("#id_name")
    lob = sunbro.FindByCSS("#id_lob")
    rut = sunbro.FindByCSS("#id_rut")
    address = sunbro.FindByCSS("#id_address")
    region = sunbro.FindByCSS("#id_region")
    communes = sunbro.FindByCSS("#id_communes")
    contact = sunbro.FindByCSS("#id_contact")

    errors = sunbro.FindAllByCSS(".error")
    submit = sunbro.FindByID('checkout')
    email = sunbro.FindByCSS("#email-invoice")


class StoreListItem(BasePage):

    """Box with the store info"""
    bg_image = sunbro.FindByClass('store-img')
    logo_image = sunbro.FindByClass('store-logo')
    name = sunbro.FindByTag('h3')


class Listing(BasePage):

    """Page with the list of stores"""
    store_items = sunbro.FindAllByCSS('.item-stores')
    stores_ads_number = sunbro.FindAllByCSS('.count-ads')
    stores_regions = sunbro.FindAllByCSS('.store-region')
    first_store_name = sunbro.FindByCSS('.item-stores h3')
    search_input = sunbro.FindByCSS('#search_text')
    search_button = sunbro.FindByCSS('button.icon-search')
    head_title = sunbro.FindByCSS('.head-list h2')
    selected_region = sunbro.FindByCSS('.items .item')
    store_create_button = sunbro.FindByCSS('.store-create')
    store_names = sunbro.FindAllByCSS('.item-stores h3')
    store_regions = sunbro.FindAllByCSS('.option')
    select_regions = sunbro.FindByCSS('.item')
    search_without_result_message = sunbro.FindByCSS('.box-no-found')
    pagination_stores = sunbro.FindAllByCSS('.paginate-stores ul li a')
    pagination_next = sunbro.FindByCSS('.next')
    create_store_link = sunbro.FindByLinkText('Crear mi tienda')

    def get_store(self, index):
        stores = self.store_items
        return StoreListItem(stores[index])

    def login(self, user="many@ads.cl", password="123123123"):
        wd = self._driver
        wd.get(conf.DESKTOP_URL + "/region_metropolitana")
        page = AccountBar(wd)
        page.login(user, password)

    def go(self):
        self._driver.get(conf.DESKTOP_URL + "/tiendas")

    def go_from_region(self, region):
        self._driver.get(conf.DESKTOP_URL)
        region_link = self._driver.find_element_by_partial_link_text(region)
        region_link.click()
        store_link = self._driver.find_element_by_partial_link_text('Tiendas')
        store_link.click()

    def filter_by_region(self, text):
        self.select_regions.click()
        for i in range(len(self.store_regions)):
            if(self.store_regions[i].text == text):
                self.store_regions[i].click()
                break


class StoreView(BasePage):
    listing_ads = sunbro.FindAllByCSS(".ad.listing_thumbs")

    def go_store(self, store_url):
        self._driver.get(conf.DESKTOP_URL + "/tiendas/" + store_url)
        self.wait.until_loaded()

    def go_ad_by_subject(self, subject):
        self._driver.find_element_by_partial_link_text(subject).click()

    @lazy
    def ads(self):
        active_ads = {}
        for active_ad in map(StoreViewListingAd, self.listing_ads):
            active_ads[active_ad.list_id] = active_ad
        return active_ads


class StoreViewListingAd(BasePage):

    subject = sunbro.FindByCSS('.title')
    image = sunbro.FindByCSS(".da-image")
    price = sunbro.FindByCSS(".price")
    date = sunbro.FindByCSS(".date")

    label_tag = sunbro.FindByCSS('.fa-tag')

    @lazy
    def list_id(self):
        return int(self._driver.get_attribute('id'))


class Detail(BasePage):

    """Store detail page"""
    main_image = sunbro.FindByClass("store-img")
    logo_image = sunbro.FindByClass("store-logo")
    name = sunbro.FindByTag('h2')
    phone = sunbro.FindByClass('detail-phone')
    address = sunbro.FindByClass('detail-address')
    site = sunbro.FindByClass('detail-site')
    description = sunbro.FindByClass('store-description')
    edit_button = sunbro.FindByClass('edit-my-store')
    share_facebook = sunbro.FindByID('share_facebook')
    share_twitter = sunbro.FindByID('share_twitter')

    search_text = sunbro.FindByID("search_text")
    search_button = sunbro.FindByCSS(".search-list .btn")

    search_sort_by_price_actual = sunbro.FindByCSS(".order .item")
    search_sort_by_price_options = sunbro.FindAllByCSS(".order .option")

    search_filter_by_category_actual = sunbro.FindByCSS(".categories .item")
    search_filter_by_category_options = sunbro.FindAllByCSS(".categories .option")

    store_not_found_message = sunbro.FindByCSS(".store-not-found p")
    store_not_found_message_button = sunbro.FindByClass("browse-stores")
    store_no_ads_found_message = sunbro.FindByCSS(".box-no-found span")
    store_no_ads_found_go_back = sunbro.FindByCSS(".box-no-found a")

    listing_ads_dates = sunbro.FindAllByCSS(".listing_thumbs_date")
    listing_ads_subj = sunbro.FindAllByCSS(".title")
    listing_ads_price = sunbro.FindAllByCSS(".price")
    listing_ads_category = sunbro.FindAllByCSS(".category")
    listing_ads_region = sunbro.FindAllByCSS(".region")

    paginator_next = sunbro.FindByCSS(".paginate-stores .next a")
    paginator_prev = sunbro.FindByCSS(".paginate-stores .prev a")

    def sort_by_price(self, text):
        self._driver.execute_script("$('select#order')[0].selectize.open();")
        for i in range(len(self.search_sort_by_price_options)):
            if(self.search_sort_by_price_options[i].text == text):
                self.search_sort_by_price_options[i].click()

    def filter_by_category(self, text):
        self._driver.execute_script("$('#categories')[0].selectize.open();")
        for i in range(len(self.search_filter_by_category_options)):
            if(self.search_filter_by_category_options[i].text == text):
                self.search_filter_by_category_options[i].click()
                break

    def search_by_text(self, text):
        self.fill_form(search_text=text)
        self.wait.until_present('search_button')
        self.search_button.click()

    def get_data(self):
        fields = [
            "name",
            "phone",
            "address",
            "site",
            "description"
        ]
        result = {}
        for f in fields:
            element = getattr(self, f)
            result[f] = element.text
        return result

    def go(self, url):
        url = "{0}/tiendas/{1}".format(conf.DESKTOP_URL, url)
        self._driver.get(url)
        self._driver.maximize_window()


class NotFound(BasePage):

    title = sunbro.FindByCSS('.store-not-found p')
    other_stores = sunbro.FindByCSS('.store-not-found a')
