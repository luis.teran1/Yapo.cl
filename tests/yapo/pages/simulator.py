# -*- coding: latin-1 -*-
import sunbro
from yapo.pages import BasePage

class Paysim(BasePage):
    """Page object of the paysim"""

    # Inputs
    input_amount = sunbro.FindByID('input_amount')
    input_order = sunbro.FindByID('input_order')

    # Buttons
    button_accept = sunbro.FindByID('button_accept')
    button_refuse = sunbro.FindByID('button_refuse')
    button_cancel = sunbro.FindByID('button_cancel')
    button_go_site = sunbro.FindByID('continue')
    input_transaction_type = sunbro.FindByID('input_type')
    input_amount = sunbro.FindByID('input_amount')
    input_order = sunbro.FindByID('input_order')
    input_session = sunbro.FindByID('input_session')
    input_success_url = sunbro.FindByID('input_surl')
    input_failure_url = sunbro.FindByID('input_furl')
    input_token_ws = sunbro.FindByID('input_token_ws')
    send_xml2 = sunbro.FindByName('send_xml2')
    select_payment_type = sunbro.FindByName('TBK_TIPO_PAGO')
    select_payment_installment = sunbro.FindByName('TBK_NUMERO_CUOTAS')
    header_text = sunbro.FindByCSS(".paysim-head h1")
    params_body = sunbro.FindByCSS(".paysim-params")

    def pay_accept(self, send_xml2=True):
        if not send_xml2:
            self.wait.until_present('send_xml2')
            self.send_xml2.click()
        self.wait.until_present('button_accept')
        self.button_accept.click()
        self.wait.until_present('button_go_site')
        self.button_go_site.click()

    def pay_refuse(self):
        self.wait.until_present('button_refuse')
        self.button_refuse.click()
        self.wait.until_present('button_go_site')
        self.button_go_site.click()

    def pay_cancel(self):
        self.wait.until_present('button_cancel')
        self.button_cancel.click()
        self.wait.until_present('button_go_site')
        self.button_go_site.click()

    def change_order(self, value):
        self.input_order.clear()
        self.input_order.send_keys(value)

    def paysim_populate(self, *args, **kwargs):
        for field, value in kwargs.items():
            input = getattr(self, "input_{0}".format(field))
            input.clear()
            input.send_keys(value)

class Simulator(BasePage):
    accept = sunbro.FindByName("tbk_ok")
    refuse = sunbro.FindByName("tbk_cancel")
    cancel = sunbro.FindByName("tbk_voided")
    amount = sunbro.FindByID("input_amount")
    go_to_site = sunbro.FindByName("tbk_next")
    xml2_checkbox = sunbro.FindByName("send_xml2")

    def add_amount_value(self, value):
        self.amount.send_keys(value)

    def pay(self, action):
        if action is None:
            self.accept.click()
        elif action == 'cancel':
            self.cancel.click()
        elif action == 'refuse':
            self.refuse.click()
        elif action == 'modify_amount':
            self.add_amount_value("00")
            self.accept.click()
        self.go_to_site.click()

class OneClickPaysim(BasePage):
    """Page object of the paysim"""

    # Inputs
    input_token = sunbro.FindByID('token')
    input_card_number = sunbro.FindByID('card_number')
    select_card_brand = sunbro.FindByID('card_brand')

    # Buttons
    button_accept = sunbro.FindByID('button_accept')
    button_refuse = sunbro.FindByID('button_refuse')
    button_cancel = sunbro.FindByID('button_cancel')

    def pay_accept(self):
        self.wait.until_present('button_accept')
        self.button_accept.click()
        self.wait.until(lambda x: '/pagos/form/' in self._driver.current_url)

    def pay_refuse(self):
        self.wait.until_present('button_refuse')
        self.button_refuse.click()
        self.wait.until(lambda x: '/pagos/form/' in self._driver.current_url)

    def pay_cancel(self):
        self.wait.until_present('button_cancel')
        self.button_cancel.click()
        self.wait.until(lambda x: '/pagos/form/' in self._driver.current_url)

    def paysim_populate(self, *args, **kwargs):
        for field, value in kwargs.items():
            input = getattr(self, "input_{0}".format(field))
            input.clear()
            input.send_keys(value)


class PaymentSuccess(BasePage):
    total = sunbro.FindByCSS(".paymentTable-data tfoot .__right")
    edit_store = sunbro.FindByCSS(".paymentButton.paymentButton-store")


class PaymentError(BasePage):
    title = sunbro.FindByCSS(".informativeNotification.__error .informativeNotification-title")
    subtitle = sunbro.FindByCSS(".informativeNotification.__error .informativeNotification-subtitle")
    error = sunbro.FindByCSS(".error-reason")
    detail_items = sunbro.FindByCSS(".error-reasons li")


class PaymentWarning(BasePage):
    title = sunbro.FindByCSS(".title h1")
    title_error = sunbro.FindByCSS(".title h1")
    description = sunbro.FindByCSS(".title p")
    description_selector = sunbro.FindByCSS(".title p")
