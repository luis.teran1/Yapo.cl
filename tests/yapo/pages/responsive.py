# -*- coding: latin-1 -*-
from yapo import conf, pages
from selenium.common.exceptions import NoSuchElementException
import sunbro
import time


class Responsive(pages.BasePage):

    def get(self, url, device='desktop'):
        self._driver.get(url)
        print('\nLoading {}'.format(url))
        self.wait.until_loaded()
        self.set_device(device)

    def set_device(self, device):
        if device is 'desktop':
            self._driver.maximize_window()
        if device is 'iphone5':
            self.set_window_size(320, 568)

    def get_window_size(self):
        return self._driver.get_window_size()

    def set_window_size(self, width, height):
        self._driver.set_window_size(width, height)
        time.sleep(1)

    def get_err(self, name):
        err_name = 'err_{}'.format(name)
        try:
            field = getattr(self, err_name)
            if not field.is_displayed():
                print("WARNING: {0} is not displayed".format(err_name))
            return field.text
        except NoSuchElementException:
                print("WARNING: {0} is not present".format(err_name))
        return ''

    def is_enabled(self, name):
        try:
            field = getattr(self, name)
            if not field.is_displayed():
                print("WARNING: {0} is not displayed".format(err_name))
            return field.is_enabled()
        except NoSuchElementException:
                print("WARNING: {0} is not present".format(err_name))


class Refund(Responsive):
    """Page object for refund form """
    first_name = sunbro.FindByID('first_name')
    last_name = sunbro.FindByID('last_name')
    rut = sunbro.FindByID('rut')
    email = sunbro.FindByID('email')
    bank_account_type = sunbro.FindByID('bank_account_type')
    bank_id = sunbro.FindByID('bank_id')
    bank_account_number = sunbro.FindByID('bank_account_number')
    submit = sunbro.FindByCSS('.btn.btn-primary')

    err_first_name = sunbro.FindByID('err_first_name')
    err_last_name = sunbro.FindByID('err_last_name')
    err_rut = sunbro.FindByID('err_rut')
    err_email = sunbro.FindByID('err_email')
    err_bank_account_type = sunbro.FindByID('err_bank_account_type')
    err_bank_id = sunbro.FindByID('err_bank')
    err_bank_account_number = sunbro.FindByID('err_bank_account_number')

    success_page = sunbro.FindByID('upselling-form-success')
    error_page = sunbro.FindByCSS('#upselling-form-error .check-email-title')

    def go(self, url, device):
        self.get(url, device)
