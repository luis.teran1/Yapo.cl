# -*- coding: latin-1 -*-
import sunbro
import selenium.webdriver.support.expected_conditions as EC
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from yapo import conf
from yapo.utils import Products, Label_texts, Label_values, Upselling
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from yapo.pages import BasePage
from yapo.component import select
from yapo.component import ComponentComposer
from yapo.component.upselling import UpsellingContainer
from yapo.pages.maps import MapForm, MapPreview
from lazy import lazy
import os, time


@ComponentComposer
class AdInsert(BasePage):
    """Page object for ad insertion page on desktop"""

    component = {
        'upselling': (UpsellingContainer, '.aiUpselling'),
    }

    category = sunbro.FindByID('category_group')
    category_contents = sunbro.FindByID('category_contents')
    subject = sunbro.FindByID('subject')
    body = sunbro.FindByID('body')
    price = sunbro.FindByID('price')
    uf_price = sunbro.FindByID('uf_price')
    currency = sunbro.FindByCSS('input[name=currency]')
    currency_peso = sunbro.FindByID('currency_peso')
    currency_uf = sunbro.FindByID('currency_uf')
    pesos2uf = sunbro.FindByID('pesos2uf')
    focused_price = sunbro.FindByCSS('#price:focus')
    cubiccms = sunbro.FindByID('cubiccms')
    regdate = sunbro.FindByID('regdate')
    mileage = sunbro.FindByID('mileage')
    err_plates = sunbro.FindByID('err_plates')
    plates = sunbro.FindByID('plates')
    internal_memory = sunbro.FindByID('internal_memory')
    internal_memory_label = sunbro.FindByCSS('label[for="internal_memory"]')
    rooms = sunbro.FindByID('rooms')
    bathrooms = sunbro.FindByID('bathrooms')
    garage_spaces = sunbro.FindByID('garage_spaces')
    condominio = sunbro.FindByID('condominio')
    brand = sunbro.FindByID('brand')
    model = sunbro.FindByID('model')
    version = sunbro.FindByID('version')
    gearbox = sunbro.FindByID('gearbox')
    fuel = sunbro.FindByID('fuel')
    cartype = sunbro.FindByID('cartype')
    private_ad = sunbro.FindByID('p_ad')
    company_ad = sunbro.FindByID('c_ad')
    user_type = sunbro.FindAllByName("company_ad")
    gender = sunbro.FindByID("gender")
    condition = sunbro.FindByID("condition")
    size = sunbro.FindByID("size")
    err_category = sunbro.FindByID('err_category')
    rut = sunbro.FindByID('rut')
    err_rut = sunbro.FindByID('err_rut')

    footwear_gender = sunbro.FindByID('footwear_gender')
    footwear_size = sunbro.FindByID('footwear_size')
    fwtype = sunbro.FindByCSS('.footwear_container')
    estate_type = sunbro.FindByID('estate_type')
    new_realestate = sunbro.FindByID('new_realestate')
    clothing_size = sunbro.FindByID('clothing_size')
    capacity = sunbro.FindByID('capacity')
    footwear_type = fwtype
    util_size = sunbro.FindByID('util_size')
    built_year = sunbro.FindByID('built_year')

    jcs = sunbro.FindAllByCSS('.jobCategory')
    advice = sunbro.FindByCSS('.maxEditionAdvice')
    login_box = sunbro.FindByCSS('.loginBox')
    login_box_close = sunbro.FindByCSS('.loginBox .pm-icon')
    login_box_title = sunbro.FindByCSS('.loginBox .loginBox-header-title')
    login_box_footer = sunbro.FindByCSS('.loginBox .loginBox-footerText')
    insertion_advice = sunbro.FindByCSS('.insertionPriceAdvice')
    garage_spaces = sunbro.FindByID('garage_spaces')
    gender = sunbro.FindByID('gender')
    jc = sunbro.FindByID('jc')
    sct = sunbro.FindByID('sct')
    srv = sunbro.FindByID('srv')
    condition = sunbro.FindByID('condition')
    type = sunbro.FindByID('type')
    brand = sunbro.FindByID('brand')

    region = sunbro.FindByID("region")
    communes = sunbro.FindByID("communes")

    # Data accounts
    name_label_data = sunbro.FindByID('name_label_data')
    phone_text = sunbro.FindByID('phone_text')
    btn_edit_data = sunbro.FindByID('edit_data')
    btn_save_data = sunbro.FindByID('save_data')
    profile_image = sunbro.FindByCSS('.beginInformation .profile-image')

    # MAPS elements
    # the map elements are in MapForm page object, and it is initialized in
    # init

    map_preview_exact = sunbro.FindByID('map_preview_exact')
    map_container = sunbro.FindByID('map_container')
    maps_help = sunbro.FindByID('maps_help')
    advice_location = sunbro.FindByID('advice_location')
    modal_help = sunbro.FindByClass('pm-content')
    modal_help_close_button = sunbro.FindByClass('pm-close')

    name = sunbro.FindByID('name')
    email = sunbro.FindByID('email')
    email_verification = sunbro.FindByID('email_confirm')
    email_confirm = email_verification

    # New phone definition
    phone = sunbro.FindByCSS('input[name=phone]')
    area_code = sunbro.FindByCSS('input[name=area_code]')
    phone_prefix = sunbro.FindByCSS('.phoneInput-countryCode')
    phone_type_mobile = sunbro.FindByCSS('input[name=phone_type][value=m]')
    phone_type_fixed = sunbro.FindByID('phoneInputTypeHome')
    err_phone = sunbro.FindByCSS('.phoneInput-errorMessage')

    password = sunbro.FindByID('passwd')
    password_verification = sunbro.FindByID('passwd_ver')
    phone_hidden = sunbro.FindByID("phone_hidden")
    upload_image = sunbro.FindByName('file')
    img_loader = sunbro.FindByCSS('img[src="/img/spin.gif"]')
    images = sunbro.FindAllByCSS('img[alt="image"]')
    image_upload_status = sunbro.FindByID('image_upload_status')
    err_extra_image = sunbro.FindByID('err_extra_image')
    extra_msg = sunbro.FindByClass('ai-extra-msg')

    # mobile.NewAdInsert compatibility
    passwd = password
    passwd_ver = password_verification

    create_account = sunbro.FindByID('create_account')
    accept_conditions = sunbro.FindByID('accept_conditions')

    # Hide preview button from ad_insert and ad_edit
    # preview = sunbro.FindByID('submit_preview')
    submit = sunbro.FindByID('submit_create_now')

    errors = sunbro.FindAllByCSS('span.error')
    err_subject = sunbro.FindByID('err_subject')

    sct_1 = sunbro.FindByID('sct_1')
    sct_2 = sunbro.FindByID('sct_2')
    sct_3 = sunbro.FindByID('sct_3')
    sct_4 = sunbro.FindByID('sct_4')

    jc_16 = sunbro.FindByID('jc_16')

    rs = sunbro.FindByID('rs')
    rk = sunbro.FindByID('rk')
    ru = sunbro.FindByID('ru')
    rh = sunbro.FindByID('rh')

    footwear_type_1 = sunbro.FindByID('footwear_type_1')
    footwear_type_2 = sunbro.FindByID('footwear_type_2')
    footwear_type_3 = sunbro.FindByID('footwear_type_3')
    footwear_type_4 = sunbro.FindByID('footwear_type_4')
    footwear_type_5 = sunbro.FindByID('footwear_type_5')
    footwear_type_6 = sunbro.FindByID('footwear_type_6')

    services = sunbro.FindByID('field_services')
    equipment = sunbro.FindByID('field_equipment')

    warning1 = sunbro.FindByID('warning_message')
    warning2 = sunbro.FindByID('warning_message_2')

    err_email_confirm = sunbro.FindByID('err_email_confirm')

    #Pack result at insertion
    packDualPro_first_title = sunbro.FindByCSS('#t1')
    packDualPro_second_title = sunbro.FindByCSS('#t2')

    logged_user_fieldset = sunbro.FindByCSS('fieldset.beginInformation')
    nonlogged_user_fieldset = sunbro.FindByCSS('fieldset.your_information')
    not_my_account = sunbro.FindByID('not_my_account')
    edit_change_ad_password = sunbro.FindByCSS('#main_passwd_link a.link')

    # for where am i function
    rel_url = '/ai/form'

    # combo upselling
    combo_upselling_box = sunbro.FindByCSS('.ai-box.aiUpselling')
    combo_upselling_box_expanded = sunbro.FindByCSS('.ai-box.aiUpselling.__expanded')
    
    # @TODO: Make a dynamic method to choose a combo
    combo_upselling_basic = sunbro.FindByID('basic')
    combo_upselling_standard = sunbro.FindByID('standard')
    combo_upselling_advanced = sunbro.FindByID('advanced')
    combo_upselling_premium = sunbro.FindByID('premium')

    # Pack inmo commune message
    advicecommune = sunbro.FindByID('advicecommune')

    plates_msj = sunbro.FindByCSS('.plateMsg')

    err_error = sunbro.FindByID('err_error')

    # error messages
    ERROR_ACCOUNT_REQUIRED = 'ERROR_ACCOUNT_REQUIRED'
    ERROR_MULTI_INSERT = 'Has publicado el m�ximo de avisos seguidos posible, para seguir publicando espera unos minutos.'
    ERROR_CANT_CHANGE_CAT_FROM = 'La categor�a de este aviso no puede ser modificada'
    ERROR_CANT_CHANGE_CAT_TO = 'No puedes cambiar el aviso a esta categor�a'

    def combo_is_disabled(self, wd, product):
        return Upselling.is_element_disabled(wd.find_element_by_css_selector('.ai-box.aiUpselling'), selector='#{0}.__disabled'.format(product))

    def get_combo_price(self, wd, product):
        return wd.find_element_by_css_selector('.aiUpselling #{0} .aiUpselling-price'.format(product)).text

    def get_upselling_combos(self):
        return self.upselling.combos

    @lazy
    def map_form(self):
        """ return a MapForm element """
        return MapForm(self._driver)

    @property
    def job_category(self):
        r = []
        for jc in self.jcs:
            if jc.get_attribute('checked') == 'true':
                r.append(jc.get_attribute('id')[3:])
        return r

    def go(self):
        self._driver.get(conf.PHP_URL + "/ai")
        self.wait.until_loaded()
        self.wait.until_visible('subject')
        return self

    def get_error_for(self, element):
        return self._driver.find_element_by_id('err_{}'.format(element))

    def get_image_by_index(self, index):
        # 1-based
        return self._driver.find_element_by_css_selector('li.ai_image:nth-child({0}) img'.format(index))

    def get_image_div_by_index(self, index):
        # 1-based
        # yes, I do not want to reuse the method above
        return self._driver.find_element_by_css_selector('li.ai_image:nth-child({0})'.format(index))

    def upload_resource_image(self, relative_path):
        self.upload_image.send_keys(
            os.path.join(conf.RESOURCES_DIR, 'images', relative_path))

    def search_location(self, **kwargs):
        self.map_form.fill_form(**kwargs)
        self.map_form.remove_args(kwargs)

        self.map_form.map.fill_form(**kwargs)
        self.map_form.map.remove_args(kwargs)

        return kwargs

    def fill(self, name, value):
        if name == 'communes':
            self.wait.until_attr_equals('communes', 'data-region', self.val('region'))
            select(self.communes, value)
        elif name == 'model':
            self.wait.until_attr_equals('model', 'data-brand', self.val('brand'))
            select(self.model, value)
        elif name == 'category':
            select(self.category, value)
            self.wait.until_attr_equals('category_contents', 'data-cat', value)
        elif name == 'phone_type_mobile' or name == 'phone_type_fixed':
            element = getattr(self, name);
            element.click()
        elif name in {'jc', 'sct'}:
            for v in value:
                element = self._driver.find_element_by_css_selector('[name="{}[{}]"]'.format(name, v))
                element.click()
        else:
            super().fill(name, value)

    def insert_ad(self, **kwargs):
        """ It inserts an ad
            For images add images=['path/to/img', 'path/to/another_img']
        """

        # This function is hideous, but is better to call it once, than twice
        logged = self.is_logged()
        if logged:
            if 'private_ad' in kwargs:
                kwargs.pop('private_ad')
            if 'company_ad' in kwargs:
                kwargs.pop('company_ad')

        category = None
        kwargs = self.search_location(**kwargs)

        if 'create_account' in kwargs:
            self.fill_form(create_account=kwargs.pop('create_account'))

        if 'area_code' in kwargs:
            phone = str(kwargs.pop('area_code'))
            self.fill_form(phone = phone)

        if 'category' in kwargs:
            category = str(kwargs.pop('category'))
            self.fill_form(category = category)

        if 'type' in kwargs:
            kwargs.pop('type')

        cats_inmo = {'1220','1240','1260'}

        if category and category == '2020':
            params_order = ['brand', 'model', 'version', 'regdate', 'gearbox', 'fuel', 'cartype', 'mileage']
            data = [(key, kwargs.pop(key)) for key in params_order if key in kwargs]
            self.ordered_fill_form(data)

        if category and category == '7040':
            if 'private_ad' in kwargs:
                kwargs.pop('private_ad')

        # If we are logged in, we don't want to fill those
        if not self.is_element_present('password'):
            kill = ['email', 'email_verification', 'password', 'password_verification']
            for param in kill:
                if param in kwargs:
                    kwargs.pop(param)

        if category in cats_inmo:
            if 'estate_type' in kwargs:  # it always should be, but we don't want any test to crash here
                param_value = kwargs.pop('estate_type')
                params_fill_form = {'estate_type':param_value}
                self.fill_form(**params_fill_form)
                time.sleep(1)

            services = None
            if 'services' in kwargs:
                services = kwargs.pop('services')

            if services is not None:
                checkboxes = self.services.find_elements_by_xpath("//input[@type='checkbox']")
                if services == 'all':
                   print('Checking all services')
                   for checkbox in checkboxes:
                       checkbox_id = checkbox.get_attribute('id')
                       if not checkbox.is_selected() and 'srv' in checkbox_id:
                            print ('Checkbox '+ checkbox_id)
                            checkbox.click()
                else:
                   print('Checking some services')
                   srv = services.split(',')
                   for checkbox in checkboxes:
                       for chk_serv in srv:
                           checkbox_id = checkbox.get_attribute('id')
                           if 'srv_{0}'.format(chk_serv) in checkbox_id:
                               if not checkbox.is_selected():
                                   print ('Checkbox '+ checkbox_id)
                                   checkbox.click()

        if 'currency' in kwargs:
            val = kwargs.pop('currency')
            if val == 'uf':
                self.fill_form(currency_uf = val)
                if 'price' in kwargs:
                    kwargs['uf_price'] = kwargs.pop('price')
            else:
                self.fill_form(currency_peso = val)

        if category == '4080':
            params_order = ['footwear_gender', 'footwear_size']
            data = [(key, kwargs.pop(key)) for key in params_order if key in kwargs]
            self.ordered_fill_form(data)
            fwtype = None
            if 'fwtype' in kwargs:
                fwtype = kwargs.pop('fwtype')

            if fwtype is None and 'footwear_type' in kwargs:
                fwtype = kwargs.pop('footwear_type')

            if fwtype is not None:
                checkboxes = self.fwtype.find_elements_by_xpath("//input[@type='checkbox']")
                print('Checking some fwtype')
                for checkbox in checkboxes:
                   for chk_serv in fwtype:
                       checkbox_id = checkbox.get_attribute('id')
                       if '{0}'.format(chk_serv) in checkbox_id:
                           if not checkbox.is_selected():
                               print ('Checkbox '+ checkbox_id)
                               checkbox.click()

        if logged:
            for param_logged in ['name', 'phone']:
                if param_logged in kwargs:
                    self.btn_edit_data.click()
                    ##self.wait.until_visible(param_logged)
                    break

        if 'images' in kwargs:
            images = kwargs.pop('images')
            for img in images:
                self.upload_resource_image(img)

        if 'phone_type_mobile' in kwargs:
            ptm = kwargs.pop('phone_type_mobile')
            if self.phone_type_mobile.is_selected() != ptm:
                self.phone_type_mobile.parent.execute_script("arguments[0].click()", self.phone_type_mobile)


        self.fill_form(**kwargs)
        return self

    def has_map(self):
        return self.map_form.geoposition.get_attribute('value') != ''

    def is_logged(self):
        return self.is_element_present('logged_user_fieldset')

    def update_account_data(self, name, phone):
        self.wait.until_visible('btn_edit_data')
        self.btn_edit_data.click()
        self.fill_form(name=name, phone=phone)
        self.wait.until_visible('btn_save_data')
        self.btn_save_data.click()
        self.wait.until_visible('btn_edit_data')

    def choose_category(self, categoryId):
        self.fill('category', categoryId)

    def want_bump(self, type='standard'):
        # Old bump on edit
        if type == 'buy_bump':
            self._driver.find_element_by_css_selector('#want_bump + ins').click()
        # Upselling bump
        if type in {'basic', 'standard', 'advanced', 'premium'}:
            height = 275 if type == 'basic' else 556
            self.wait.until_present('combo_upselling_box')
            self._driver.find_element_by_css_selector("#"+type).click()
            self.wait_bump_animation(height = height)
            return self._driver.find_element_by_css_selector('.__'+type+'.__selected').is_displayed()

    def wait_bump_animation(self, height = 275):
        self.wait.until_present('combo_upselling_box')
        self.wait.until(lambda x: self.combo_upselling_box.size['height'] >= height,
                print("Waiting combo_upselling_box animation... CurrHeight/Expected: {}/{}"
                    .format(self.combo_upselling_box.size['height'], height)))

    def choose_upselling(self, i=None, id=None):
        combo = self.upselling.combo(i, id)
        combo.click()
        self.wait_bump_animation()
        return combo


class AdInsertPreview(BasePage):

    """Page object for the ad insertion preview feature"""
    edit_ad = sunbro.FindByID('edit_ad')
    create = sunbro.FindByID('submit_create_t_prv')
    advertiser_name = sunbro.FindByClass('name')
    da_subject = sunbro.FindByID('da_subject')
    description = sunbro.FindByCSS('.description p')
    profile_image = sunbro.FindByCSS('.sidebar-right .profile-image')

    @lazy
    def map_view(self):
        """ return a MapPreview element """
        return MapPreview(self._driver)


class AdInsertResult(BasePage):

    # Any platform, any case
    upselling_success = sunbro.FindByCSS('.paymentHeader')
    upselling_wb = sunbro.FindByID('weekly_box')
    upselling_db = sunbro.FindByID('daily_bump')
    upselling_g = sunbro.FindByID('gallery_box')
    upselling_l = sunbro.FindByID('label_box')
    # Any platform, with account creation
    check_email_title = sunbro.FindByCSS(
        '#ai-account-success .check-email-title')
    # Any platform, with upselling
    products_box = sunbro.FindByID('products_box')
    # Any platform, with payment products
    payment_form = sunbro.FindByID('payment_form')
    payment_1 = sunbro.FindByID('payment_1')
    payment_2 = sunbro.FindByID('payment_2')

    buy_upselling_label = sunbro.FindByCSS('#label_box .btn')
    buy_upselling_gallery = sunbro.FindByCSS('#gallery_box .btn')
    all_label_upselling_new = sunbro.FindAllByID('up_label_new')
    all_label_upselling_little_use = sunbro.FindAllByID('up_label_little_use')
    all_label_upselling_opportunity = sunbro.FindAllByID(
        'up_label_opportunity')
    all_label_upselling_urgent = sunbro.FindAllByID('up_label_urgent')

    info_box = sunbro.FindByCSS('info-box')

    result_deac_message = sunbro.FindByCSS('.PackConfirmationPage-HeaderTitleText.__Inactive')

    pack_msg_ok = sunbro.FindByID('pack_message_ok')
    title = sunbro.FindByID('title')

    old_browser_success = sunbro.FindByClass("info-box__title")

    @lazy
    def label_upselling_new(self):
        return self.all_label_upselling_new[len(self.all_label_upselling_new) - 1]

    @lazy
    def label_upselling_opportunity(self):
        return self.all_label_upselling_opportunity[len(self.all_label_upselling_opportunity) - 1]

    @lazy
    def label_upselling_little_use(self):
        return self.all_label_upselling_little_use[len(self.all_label_upselling_little_use) - 1]

    @lazy
    def label_upselling_urgent(self):
        return self.all_label_upselling_urgent[len(self.all_label_upselling_urgent) - 1]

    def was(self, state):
        """ state = (success|success_mobile|failure|with_account_create|without_account_create|with_upselling|without_upselling|pack_inactive_ad|pack_active_ad)"""
        return getattr(self, state)()

    def success(self):
        try:
            self.wait.until_present('info_box')
            return self.is_element_present('success_message') or self.is_element_present('old_browser_success')
        except TimeoutException:
            return False

    def success_mobile(self):
        return self.is_element_present('upselling_success')

    def failure(self):
        return not self.success()

    def with_account_create(self):
        return self.is_element_present('check_email_title')

    def without_account_create(self):
        return not self.with_account_create()

    def with_upselling(self):
        return self.is_element_present('products_box')

    def without_upselling(self):
        return not self.with_upselling()

    def with_payment_products(self):
        return any(map(self.is_element_present, ['payment_form', 'payment_1', 'payment_2']))

    def without_payment_products(self):
        return not self.with_payment_products()

    def pack_inactive_ad(self):
        return self.is_element_present('result_deac_message')

    def pack_active_ad(self):
        return self.is_element_present('pack_msg_ok')

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def subject(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content strong')

    @property
    def success_message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdInsertWithoutAccountSuccess(BasePage):

    """Page object for the ad insertion success without account"""

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def title(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__title')

    @property
    def success_message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdInsertWithAccountSuccess(BasePage):

    """Page object for the ad insertion success"""
    facebook_noscript = sunbro.FindByCSS('head noscript')
    facebook_script = sunbro.FindByCSS(
        'head script[src="//connect.facebook.net/en_US/fp.js"]')
    ad_image = sunbro.FindByCSS('.link_image img')
    ad_subject = sunbro.FindByCSS('.subject')
    ad_price = sunbro.FindByCSS('.price')

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def title(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__title')

    @property
    def success_message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdInsertSuccess(BasePage):

    """Page object for the ad insertion success without account """
    facebook_noscript = sunbro.FindByCSS('head noscript')
    facebook_script = sunbro.FindByCSS(
        'head script[src="//connect.facebook.net/en_US/fp.js"]')
    info_box = sunbro.FindByCSS('info-box')
    SUCCESS_MESSAGE = '�Gracias por publicar!'

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def title(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__title')

    @property
    def success_message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdInsertDuplicatedAd(BasePage):

    """Page object for the duplicated ad in ad insertion"""
    duplicated = sunbro.FindByID('duplicated_da')
    message = sunbro.FindByCSS('#duplicated_da .duplicatedAd-title')
    subject = sunbro.FindByCSS('#duplicated_da .da-subject')
    price = sunbro.FindByCSS('#duplicated_da .da-price')
    notice = sunbro.FindByCSS('#duplicated_da .notice')
    da_status = sunbro.FindByCSS('#duplicated_da .da-status')

    password_input = sunbro.FindByID('password_input')
    password_err = sunbro.FindByCSS('.validation-msg.error')
    activate_and_bump = sunbro.FindByID('activate_and_bump')
    activate_da = sunbro.FindByID('activate_da')
    publish_f = sunbro.FindByID('publish_f')
    publish_f_2 = sunbro.FindByID('submit_create_t_prv')

    def confirm(self, passwd):
        self.fill_form(password_input=passwd)

    def ready(self, bogus):
        return 'is-hidden' not in self.duplicated.get_attribute('class')

class AdInsertDuplicatedAdActiveResult(BasePage):

    """Page object for result after clicking the 'activar' button for a duplicated ad"""
    activated = sunbro.FindByID('activated_da')
    message = sunbro.FindByCSS('.activatedAd-message')
    subject = sunbro.FindByCSS('#acticated_da .da-subject p')
    price = sunbro.FindByCSS('#acticated_da .da-price')
    date = sunbro.FindByCSS('#acticated_da .da-date')
    da_status = sunbro.FindByCSS('#acticated_da .da-status')

    modal_message = sunbro.FindByCSS('#pgwModal .pm-modal-body p')
    modal_failure = sunbro.FindByCSS('#pgwModal .pm-modal-body p.message_failed')
    failed = sunbro.FindByCSS('.message.failed')
    buy_pack = sunbro.FindByCSS(".pgwModal #see_das")
    see_das = sunbro.FindByID('see_das')
    my_account = sunbro.FindByID('my_account')


class AdInsertDuplicatedAdPublishResult(BasePage):

    """Page object for result after clicking the 'publicalo aqui' link for a duplicated ad"""
    info_box = sunbro.FindByCSS('info-box')

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    @property
    def message(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__content')


class AdInsertUpselling(AdInsertSuccess):

    """Page object for the ad insertion success with upselling"""
    upselling_products = sunbro.FindByID('products_box')
    upselling_title = sunbro.FindByCSS('.benefits .title.text-center')

    weekly_box = sunbro.FindByID('weekly_box')
    weekly_title = sunbro.FindByCSS('.benefit-header', within='weekly_box')
    weekly_title = sunbro.FindByCSS('.benefit-header', within='weekly_box')
    weekly_message = sunbro.FindByCSS('.legend', within='weekly_box')
    weekly_price = sunbro.FindByCSS('.price', within='weekly_box')
    weekly_button = sunbro.FindByCSS('.btn-buy', within='weekly_box')

    daily_box = sunbro.FindByID('daily_box')
    daily_title = sunbro.FindByCSS('.benefit-header', within='daily_box')
    daily_message = sunbro.FindByCSS('.legend', within='daily_box')
    daily_price = sunbro.FindByCSS('.price', within='daily_box')
    daily_button = sunbro.FindByCSS('.btn-buy', within='daily_box')

    gallery_box = sunbro.FindByID('gallery_box')
    gallery_title = sunbro.FindByCSS('.benefit-header', within='gallery_box')
    gallery_message = sunbro.FindByCSS('.legend', within='gallery_box')
    gallery_price = sunbro.FindByCSS('.price', within='gallery_box')
    gallery_button = sunbro.FindByCSS('.btn-buy', within='gallery_box')


class PackConfirm(BasePage):

    """ Page object for the pack auto insert """

    pack_message_ok = sunbro.FindByID('pack_message_ok')
    pack_message_nopack = sunbro.FindByID('pack_message_nopack')
    pack_message_nopack_link_pack_auto = sunbro.FindByCSS(
        '.PackConfirmationPage-BuyBtn.__BuyPack')
    pack_message_noquota = sunbro.FindByID('pack_message_noquota')
    pack_message_noquota_link_dash_pack_auto = sunbro.FindByCSS(
        '.PackConfirmationPage-BuyBtn.__BuyPack')
    pack_message_noaccount = sunbro.FindByID('pack_message_noaccount')
    pack_message_noaccount_link_create_account = sunbro.FindByCSS(
        '#pack_message_noaccount a')

    pack_message_buyoptions = sunbro.FindByCSS('.PackConfirmationPage-Buy')
    pack_message_buyoptions_link_pack = sunbro.FindByCSS('.PackConfirmationPage-BuyBtn.__BuyPack')
    pack_message_buyoptions_link_if = sunbro.FindByCSS('.PackConfirmationPage-BuyBtn.__BuyInsertingFee')

    pack_data_total = sunbro.FindByID('pack_total')
    pack_data_quota = sunbro.FindByID('pack_quota')
    pack_data_available = sunbro.FindByID('pack_available')

    no_packs = sunbro.FindByID('no_packs')
