from yapo.pages import BasePage
from yapo import conf
import sunbro
from selenium.webdriver.common.keys import Keys
from urllib.parse import urljoin

class BuyPacks(BasePage):
    """Page to buy an pack"""
    pack_slots = sunbro.FindByID('pack_slots')
    pack_period = sunbro.FindByID('pack_period')
    pay_button = sunbro.FindByCSS('.btn.btn-pack-info')
    price = sunbro.FindByID('price')
    error_msj = sunbro.FindByID('error_msj')

    def go(self, url = 'pack-autos'):
        self._driver.get(conf.PHP_URL + '/' + url)
        self.wait.until_loaded()

    def select_slots(self, slots):
        self._driver.execute_script("$('#pack_slots')[0].selectize.setValue('{0}');".format(slots))

    def select_period(self, period):
        self._driver.execute_script("$('#pack_period')[0].selectize.setValue('{0}');".format(period))

    def select_pack(self, slots, period):
        self.select_slots(slots)
        self.select_period(period)
        self.pay_button.click()
        self.wait.until_loaded()

class DashboardPacks(BasePage):
    """ Dashboard page that is shown when the user has bought """

    packs_states_div = sunbro.FindByClass('packs-states-in')
    packs_states_title = sunbro.FindByCSS('.packs-states-in h2')
    packs_states_headers = sunbro.FindAllByCSS('.packs-states-in .t-head')
    packs_states_row = sunbro.FindAllByCSS('.packs-states-in .t-body-cont')
    packs_states_empty = sunbro.FindByCSS('.packs-states-in .t-body-cont .t-no-pack')
    packs_used_slots = sunbro.FindByCSS('.packs-states-in .t-body-cont .t-body:nth-child(4)')

    call_name = sunbro.FindByID('call_name')
    call_mail = sunbro.FindByID('call_mail')
    call_phone = sunbro.FindByID('call_phone')
    call_message = sunbro.FindByCSS('#form_call li:last-child')
    next = sunbro.FindByCSS('#form_call .next')
    call_name_error = sunbro.FindByID('call_name-error')
    call_mail_error = sunbro.FindByID('call_mail-error')
    call_phone_error = sunbro.FindByID('call_phone-error')

    def go(self):
        self._driver.get(conf.SSL_URL + '/dashboard-pack-autos')
        self.wait.until_loaded()

    def get_row(self, index):
        """ Returns row data """
        return [cell.text for cell in self.packs_states_row[index].find_elements_by_css_selector('.t-body')]

    def get_headers(self):
        """ Returns headers as a list """
        return [header.text for header in self.packs_states_headers]

    def need_help_form_fill(self, name, email, phone):
        self.call_name.send_keys(name)
        self.next.click()
        self.call_mail.send_keys(email)
        self.next.click()
        self.call_phone.send_keys(phone)
        self.next.click()

    def get_packs_options(self):
        res=[]
        options = self._driver.find_elements_by_css_selector('.selectize-control .option')
        for op in options:
            res.append(op.get_attribute('innerHTML'))
        return res

class DashboardPacksAdReply(BasePage):
    """Page to get contacts through ad reply in a specific month"""
    month = sunbro.FindByID('contactListPerMonthMonth')
    email = sunbro.FindByID("contactListPerMonthEmail")
    btn_submit = sunbro.FindByID("contactListPerMonthSend")
    error_msj = sunbro.FindByXPath('//*[@id="content"]/section/div[3]/form/div[2]/div[1]/div[2]/span[1]')

    def go(self, url = 'dashboard-pack-autos'):
        self._driver.get(conf.PHP_URL + '/' + url)
        self.wait.until_loaded()

    def logout_on_new_tab(self):
        url = urljoin(self._driver.current_url, '/logout?exit=1')
        self._driver.execute_script('''window.open("{}","_blank");'''.format(url))
        self._driver.switch_to.window(self._driver.window_handles[0])

    def select_month(self, month):
        self._driver.execute_script("$('#contactListPerMonthMonth')[0].selectize.setValue('{0}');".format(month))

    def fill_form(self, month, email):
        self._driver.execute_script("window.scrollTo(0, 10000000000);")
        self._driver.execute_script('''$(".getContactList")[0].reset();''')
        self.select_month(month)
        self.email.send_keys(email)
        self.btn_submit.click()
        self.wait.until_loaded_jquery_ajax()
