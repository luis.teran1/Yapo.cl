# -*- coding: latin-1 -*-
from lazy import lazy
from selenium.webdriver.support.select import Select
from yapo import conf, pages
from yapo.pages.ad_insert import AdInsert
from yapo.pages.desktop_list import AdDeletePage, SearchBoxSideBar, AdViewDesktop
from yapo.pages.maps import MapForm
from yapo.pages.simulator import Paysim
from yapo.utils import Products, PacksPeriods, PacksType
import yapo.conf
import os
import re
import sunbro
import time
import selenium.webdriver.support.expected_conditions as EC
from yapo.component import ComponentComposer

class EditPasswordPage(pages.BasePage):

    """Page object for desktop edit password form"""
    password_input = sunbro.FindByID('passwd')
    submit_button = sunbro.FindByCSS('input[type=submit]')
    forgot_password = sunbro.FindByCSS('.form-pay a')
    email_forgot_password = sunbro.FindByName('email')
    email_submit_button = sunbro.FindByCSS('.btn.btn-primary')
    edit_message = sunbro.FindByCSS('#edit_education_msg')
    error = sunbro.FindByCSS('.validation_msg.error')

    def go(self, list_id):
        self._driver.get(conf.PHP_URL + '/ai/load/?id=' + str(list_id) + '&cmd=edit')
        self.wait.until_loaded()

    def validate_password(self, password):
        self.password_input.send_keys(password)
        self.submit_button.click()


class AdEdit(AdInsert):

    """Page object from desktop edit"""

    # Radio Buttons
    radio_vendo = sunbro.FindByID("label_st_s")
    radio_busco = sunbro.FindByID("label_st_k")

    # bump related elements
    bump_container = sunbro.FindByCSS(".buy_bump")
    bump_msg = sunbro.FindByCSS(".buy_bump h2")
    more_info = sunbro.FindByClass("more-info")
    combo_upselling_box = sunbro.FindByCSS('.ai-box.aiUpselling')

    # messages
    red_arrow_suggestion = sunbro.FindByID("warning_container_red_arrow")

    def upload_resource_image(self, relative_path):
        self.upload_image.send_keys(os.path.join(conf.RESOURCES_DIR, 'images', relative_path))

    def go(self, list_id):
        self._driver.get(conf.PHP_URL + '/ai?id={0}&cmd=edit'.format(list_id))
        self.wait.until_loaded()

    def go_to_edit_page(self, list_id, passwd):
        edit_password_page = EditPasswordPage(self._driver)
        edit_password_page.go(list_id)
        edit_password_page.validate_password(passwd)
        self.wait.until_loaded()

    def edit_an_ad(self, list_id, passwd, parameters_changed={}, delete_images=None, do_bump=False, submit=True):
        edit_pw = EditPasswordPage(self._driver)
        edit_pw.go(list_id)
        edit_pw.validate_password(passwd)

        if parameters_changed:
           self.insert_ad(**parameters_changed)

        # delete all images
        if delete_images:
            for img in self.images:
                parent = img.find_element_by_xpath('../..')
                parent.find_element_by_css_selector('.ai-img-close').click()
            assert len(self.images) == 0

        # bump option
        if do_bump:
            self.want_bump()
            self.submit.click()
            EditSuccessPage(self._driver).button_submit_bill.click()
            Paysim(self._driver).pay_accept()
            return

        if submit:
            prev_url = self._driver.current_url
            self.submit.click()
            self.wait.until(lambda x: self._driver.current_url != prev_url, 'Waiting for url change')

    def edit_fill_css_name(self, css_name, val):
        element = self._driver.find_element_by_css_selector('[name={0}]'.format(css_name))
        self.edit_element_fill(element, val)

    def edit_fill_css_for(self, css_for, val):
        element = self._driver.find_element_by_css_selector('[for={0}]'.format(css_for))
        self.edit_element_fill(element, val)

    def edit_element_fill(self, element, val):
        if element.tag_name == 'label' and val:
            element.click()
        if element.tag_name == 'input':
            if element.get_attribute('type') == 'text':
                element.clear()
                element.send_keys(val)
            if element.get_attribute('type') == 'checkbox':
                if (not val and element.is_selected()) or (val and not element.is_selected()):
                    element.click()


class EditSuccessPage(pages.BasePage):
    """Page object for desktop edit success page"""

    # Labels present when user selects want_bump checkbox on edit form
    label_subject = sunbro.FindByClass('subject')
    label_price = sunbro.FindByClass('price')
    label_success_message = sunbro.FindByClass('success-info')

    # Radio Buttons
    radio_bill = sunbro.FindByID('radio-bill')
    radio_invoice = sunbro.FindByID('radio-invoice')

    # Links
    link_edit_email = sunbro.FindByClass('edit-email')

    # Buttons
    button_submit_bill = sunbro.FindByID('submit-bill')

    # Inputs
    input_email = sunbro.FindByName('email')

    text_thanks = sunbro.FindByCSS('.info-box__title')

    def get_info_box(self):
        return self.select_shadow_element_by_css_selector('info-box')

    def get_text_thanks(self):
        info_box = self.get_info_box()
        return info_box.find_element_by_css_selector('.info-box__title')


class FavoritePage(pages.BasePage):

    """Page object for favorite"""

    title = sunbro.FindByCSS('#presentation h1')
    create_search_image = sunbro.FindByID('create-search')
    save_ads_text = sunbro.FindByClass('save_left')
    favorite_queries_text = sunbro.FindByClass('save_right')
    edit_link = sunbro.FindByLinkText('Editar')
    delete_link = sunbro.FindByLinkText('Borrar')
    cheaper_first_link = sunbro.FindByLinkText(u'Mostrar lo m�s barato primero')
    newest_first_link = sunbro.FindByLinkText(u'Mostrar lo m�s reciente primero')
    show_images_link = sunbro.FindByLinkText(u'Mostrar im�genes')
    hide_images_link = sunbro.FindByLinkText(u'Ocultar im�genes')

    ads_image = sunbro.FindAllByClass('da_image')
    duplicate_prefered_search_message = sunbro.FindByID('err_error')

    new_favorite_link = sunbro.FindByLinkText(u'Nuevo Favorito')
    about_favorites_link = sunbro.FindByLinkText(u'Acerca de Favoritos')
    all_tab_link = sunbro.FindByLinkText(u'Todas')

    ads_list = sunbro.FindAllByClass("ad")
    titles_ads_list = sunbro.FindAllByCSS('.ad .title')
    communes_ads_list = sunbro.FindAllByCSS('.ad .commune')

    delete_confirmation_yes_link = sunbro.FindByLinkText(u'S�, Borrar')
    delete_confirmation_no_link = sunbro.FindByLinkText(u'No')

    navigation_first = sunbro.FindByLinkText(u'Primera p�gina')
    navigation_last = sunbro.FindByLinkText(u'�ltima p�gina')
    navigation_next = sunbro.FindByLinkText(u'Pr�xima p�gina �')
    navigation_before = sunbro.FindByLinkText(u'� P�gina anterior')
    navigation_page_1 = sunbro.FindByLinkText(u'1')
    navigation_page_2 = sunbro.FindByLinkText(u'2')

    tabs = sunbro.FindAllByClass("tab")
    active_tab = sunbro.FindByID("active")
    save_ads_link = sunbro.FindByLinkText('Avisos guardados')

    def go(self, cat_id=15):
        self._driver.get(conf.PHP_URL + '/adwatch/presentation/0?ca=' + str(cat_id) + '_s')
        self.wait.until_loaded()
        return self

    def switch_to_about_window(self):
        self._driver.switch_to_window(self._driver.window_handles[1])

    def save_search_favorite(self):
        self.go()
        self.create_search_image.click()
        self.wait.until_loaded()
        box = SearchBoxSideBar(self._driver)
        box.wait.until_present('save_favorite_search')
        box.save_favorite_search.click()
        self.wait.until_loaded()

    def save_ad_favorite(self, list_id):
        ad_view = AdViewDesktop(self._driver)
        ad_view.go(list_id)
        ad_view.wait.until_present('save_fav_button')
        ad_view.save_fav_button.click()
        ad_view.wait.until_present('icon_heart_full')
        self.go()

    def am_i_here(self):
        return self.title.text == 'Favoritos'


class HeaderElements(pages.BasePage):

    """Page object to access to the common elements on desktop site:
           'Publicar aviso button'
           'Mi cuenta button'
           'Logged user button'
           'Region link to open/close accordion'
           'Regions links'"""

    # Buttons
    button_publish_ad = sunbro.FindByCSS(".btn-da-insert")
    button_my_account = sunbro.FindByID("login-account-link")
    button_create_account = sunbro.FindByCSS(".pm-content .loginBox-createAccount-link")
    button_logged_user = sunbro.FindByCSS(".header-userInfo")
    button_logout_user = sunbro.FindByClass("header-userLogout")
    logo = sunbro.FindByID("logo")
    logo_link = sunbro.FindByCSS("#logo a")
    profile_image = sunbro.FindByCSS('.header-accountBar .profile-image')

    # Navbar
    home_link = sunbro.FindByCSS(".navbar li:nth-child(1) a")
    search_link = sunbro.FindByCSS(".navbar li:nth-child(2) a")
    favorites_link = sunbro.FindByCSS(".navbar li:nth-child(3) a")
    help_link = sunbro.FindByCSS(".navbar li:nth-child(4) a")
    myads_link = sunbro.FindByCSS(".navbar li:nth-child(5) a")
    stores_link = sunbro.FindByCSS(".navbar li:nth-child(6) a")

    def who_is_logged(self):
        header_elements = HeaderElements(self._driver)
        try:
            logged_user = re.sub('Hola, ', '', header_elements.button_logged_user.text)
        except:
            logged_user = ''
        return logged_user


class FooterElements(pages.BasePage):

    """Page object to access to the common elements on the footer in desktop site"""
    # Links
    rules = sunbro.FindByCSS('.footer-links li:nth-child(2) a')
    advices_and_security = sunbro.FindByCSS('.footer-links li:nth-child(3) a')
    terms_and_conditions = sunbro.FindByCSS('.footer-links li:nth-child(4) a')


class BumpInfoLightboxDashboard(pages.BasePage):

    """ lightbox page for bump products """

    bump_more_info_popup = sunbro.FindByID('bump-more-info')
    popup_open_button = sunbro.FindByID('bump-open-popup')
    popup_close_button = sunbro.FindByCSS('.pm-close')
    more_info_link = sunbro.FindByID('more_info')

    def open(self):
        self.popup_open_button.click()

    def close(self):
        self.popup_close_button.click()

    def press_more_info(self):
        self.more_info_link.click()


class BumpInfoLightbox(pages.BasePage):

    price_list_items = sunbro.FindAllByCSS('#pgwModal h3.txt-bullets')
    price_answer = sunbro.FindByCSS('#pgwModal #a3')
    close_button = sunbro.FindByClass('pm-close')
    customer_service_link = sunbro.FindByLinkText('servicio al cliente')

    def close(self):
        self.close_button.click()


class ProductsInfoLightboxDashboard(pages.BasePage):

    """ lightbox page for products on dashboard """

    bump_more_info_popup = sunbro.FindByCSS('#bump-open-popup')
    gallery_more_info_popup = sunbro.FindByCSS('#gallery-more-info')
    label_more_info_popup = sunbro.FindByCSS('#label-more-info')
    popup_close_button = sunbro.FindByCSS('.pm-close')

    # Bump modal objects (Old Version)
    bump_types = sunbro.FindByCSS('.pm-content .bump-types-popup')
    bump_info_foot = sunbro.FindByCSS('.pm-content .bump-info-foot')
    bump_info_note = sunbro.FindByCSS('.pm-content .bump-info-note')
    # Bump modal (Autobump Version)
    autobump_title = sunbro.FindByCSS('.pm-content .bump-info-main-title')
    autobump_preview = sunbro.FindByCSS('.pm-content .bump-info-preview')
    autobump_popup = sunbro.FindByCSS('.pm-content .bump-types-popup')
    autobump_note = sunbro.FindByCSS('.pm-content .bump-info-note')
    autobump_foot = sunbro.FindByCSS('.pm-content .bump-info-foot')


    # Gallery modal objects
    gallery_info_foot = sunbro.FindByCSS('.pm-content .gallery-info-foot')
    gallery_wrap = sunbro.FindByCSS('.pm-content .gallery-ribbon-wrap')
    gallery_info_bottom = sunbro.FindByCSS('.pm-content .gallery-info-bottom')
    gallery_info_top = sunbro.FindByCSS('.pm-content .gallery-info-top')

    # Label modal objects
    label_preview = sunbro.FindByCSS('.pm-content .label-sprite.das-preview')
    label_mesage = sunbro.FindByCSS('.pm-content .label-message')
    label_listing = sunbro.FindByCSS('.pm-content .label-sprite.dummy-listing')
    label_info_top = sunbro.FindByCSS('.pm-content .label-info-top')

    def open_bump(self):
        self.bump_more_info_popup.click()

    def open_gallery(self):
        self.gallery_more_info_popup.click()

    def open_label(self):
        self.label_more_info_popup.click()

    def close(self):
        self.popup_close_button.click()


class Payment(pages.BasePage):

    """Page object for the payment page"""

    BILL = 'BOLETA'
    INVOICE = 'FACTURA'
    # this one appears when payments are disabled
    disabled_title = sunbro.FindByCSS('.title h1')
    # these three are NOT available on the page shown by the opportunity e-mail
    more_info = sunbro.FindByClass('more-info')
    price_list_items = sunbro.FindAllByCSS('h3.txt-bullets-small')
    # this is available on the page shown by the opportunity e-mail
    title = sunbro.FindByCSS('.checkout h1')
    tab1 = sunbro.FindByID('ui-id-1')
    tab2 = sunbro.FindByID('ui-id-2')
    form1_product_price = sunbro.FindByCSS('#payment_1 .tb-bump-upselling tr td:nth-child(2)')
    form2_product_price = sunbro.FindByCSS('#payment_2 .tb-bump-upselling tr td:nth-child(2)')
    pay_button = sunbro.FindByCSS('#payment_1 #submit-bill')
    pay_button_invoice = sunbro.FindByCSS('#payment_2 #submit-bill')
    document_type_bill = sunbro.FindByPartialLinkText(BILL)
    document_type_invoice = sunbro.FindByPartialLinkText(INVOICE)
    name = sunbro.FindByID('id_name')
    lob = sunbro.FindByID('id_lob')
    rut = sunbro.FindByID('id_rut')
    address = sunbro.FindByID('id_address')
    commune = sunbro.FindByID('id_communes')

    err_lob = sunbro.FindByID('err_lob')
    err_rut = sunbro.FindByID('err_rut')
    err_address = sunbro.FindByID('err_address')

    pay_with_servipag = sunbro.FindByID("pay_with_servipag")
    pay_with_transbank = sunbro.FindByID("pay_with_transbank")
    link_to_pay_products = sunbro.FindByID('checkout')

    #Products list unlogged
    products_list_table = sunbro.FindByCSS("table.table-striped.tb-bump-upselling")

    def go_to_pay_bump(self, list_id):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod=1'.format(list_id))
        self.wait.until_loaded()

    def press_pay_button(self):
        self.pay_button.click()

    def change_doc_type(self, doc_type):
        if doc_type == Payment.INVOICE:
            self.document_type_invoice.click()
        else:
            self.document_type_bill.click()

    def go_to_pay_inserting_fee(self, ad_id):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod=60'.format(ad_id))
        self.wait.until_loaded()
        return Payment(self._driver)

class PaymentVerify(pages.BasePage):

    """page object of payment verify"""

    text_congratulations = sunbro.FindByCSS('.paymentHeader h3')
    sale_table = sunbro.FindByCSS('.paymentProduct-listData .paymentTable-data table table')
    purchase_table = sunbro.FindByCSS('.paymentPurchase-data .paymentTable-data table')
    text_congratulation_message = sunbro.FindByCSS('.paymentHeader')
    voucher_link = sunbro.FindByID('voucher_link')


class PaymentVerifyFail(pages.BasePage):
    """page object of payment verify failed when refuse or cancel"""
    title_oops = sunbro.FindByCSS('h1')
    text_fail = sunbro.FindByCSS('.title p:nth-child(4)')

class PaymentVerifyError(pages.BasePage):

    """page object of payment verify when we found problems"""
    title_oops = sunbro.FindByCSS('h1')
    text_fail = sunbro.FindByCSS('.title p')

    PROD_NON_APPLICABLE = "El producto no puede ser aplicado a tu aviso"

class Login(pages.BasePage):

    """Page object for accounts '/login' page"""

    # Buttons
    button_login = sunbro.FindByID("submit_button")
    button_logout = sunbro.FindByClass("header-userLogout")
    button_create_account = sunbro.FindByClass(".create-account")

    # Input
    input_email = sunbro.FindByID("email_input")
    input_password = sunbro.FindByID("password_input")

    # links
    forgot_password = sunbro.FindAllByCSS('.forgot-password')

    # error message
    error_msg = sunbro.FindByCSS('#err_form')

    # Common used methods
    def go(self):
        self._driver.get(conf.PHP_URL + '/login')
        self.wait.until_loaded()
        self.wait.until_present('input_email')

    def login(self, user, password):
        self.go()
        self.input_email.send_keys(user)
        self.input_password.send_keys(password)
        self.button_login.click()

    def simple_login(self, user, password):
        self.input_email.send_keys(user)
        self.input_password.send_keys(password)
        self.button_login.click()

    def login_if_needed(self, user, password):
        self.go()
        if '/login' in self._driver.current_url:
            self.login(user, password)

    def logout(self):
        self.button_logout.click()
        self.wait.until_not_session_cookie()

    # for where am i function
    rel_url = '/login'


class LoginModal(pages.BasePage):
    """ Represents de modal that is displayed after you click on
        "Iniciar sesion" button on desktop's header """

    login_button = sunbro.FindByID('login-account-link')
    create_account = sunbro.FindByCSS('#pgwModal .loginBox-createAccount-link')
    fb_connect_btn = sunbro.FindByCSS('#pgwModal .loginBox-facebookBtn')
    email = sunbro.FindByCSS('#pgwModal #facebook-email')
    password = sunbro.FindByCSS('#pgwModal #facebook-password')
    button_email_fb_confirm = sunbro.FindByCSS('#pgwModal #facebook-email-form button')
    button_password_fb_confirm = sunbro.FindByCSS('#pgwModal #facebook-password-form button')

    # facebook input
    fb_email = sunbro.FindByID('email')
    fb_pass = sunbro.FindByID('pass')
    fb_login_btn = sunbro.FindByCSS('#loginbutton input')

    # yapo account
    account_email = sunbro.FindByCSS('#pgwModal [name=accbar_email]')
    account_password = sunbro.FindByCSS('#pgwModal [name=accbar_password]')
    account_errors = sunbro.FindAllByCSS('#pgwModal .loginFeedback.__inline.__error')
    error_msg = account_errors
    account_submit = sunbro.FindByCSS('#pgwModal #acc_login_submit_button')

    facebook_description = sunbro.FindByCSS("#pgwModal .pm-content > div:nth-child(2) .facebookBox-header-description")
    spiner = sunbro.FindByCSS('#pgwModal .boxSpinner-spinner')

    def open(self):
        """ It just open the modal """
        self.wait.until_visible('login_button')
        self.login_button.click()

    def fb_connect(self):
        """ Connect facebook with yapo account """
        self.open()
        self.wait.until_visible('fb_connect_btn')
        self.fb_connect_btn.click()

    def login(self, email, password, with_facebook=False, with_go_login=False):
        """ Facebook login, fill email and password in facebook popup """
        if with_facebook:
            self.fb_connect()
            self._driver.switch_to_window(self._driver.window_handles[-1])
            self.wait.until_visible('fb_login_btn')
            self.fill_form(fb_email=email)
            self.fill_form(fb_pass=password)
            self.fb_login_btn.click()
            self._driver.switch_to_window(self._driver.window_handles[0])
        else:
            if with_go_login:
                self.open()
            self.fill_form(account_email=email, account_password=password)
            self.account_submit.click()

    def fill_email_form_and_send(self, email):
        """ Fill form email, when facebook email has not account in yapo """
        self.fill_form(email=email)
        self.wait.until_visible('button_email_fb_confirm')
        self.button_email_fb_confirm.click()

    def fill_password_form_and_send(self, email, password):
        """ Fill form email and password, when email is different facebook email """
        # send email form
        self.fill_email_form_and_send(email)

        # send password form
        self.wait.until_not_visible('spiner')
        self.fill_form(password=password)
        self.wait.until_visible('button_password_fb_confirm')
        self.button_password_fb_confirm.click()

LoginFacebook = LoginModal

class Profile(pages.BasePage):

    """Page object for account profile page /cuenta/edit"""

    radio_persona = sunbro.FindByCSS('#is_company_p + ins')
    radio_profesional = sunbro.FindByCSS('#is_company_c + ins')
    submit = sunbro.FindByCSS('.btnUpdate-account')

    def go_profile(self, user, password):
        login = Login(self._driver)
        login.login_if_needed(user, password)
        self._driver.get(conf.SSL_URL + "/cuenta/edit")

    def set_persona(self):
        self.wait.until_present('radio_persona')
        self.radio_persona.click()
        self.submit.click()

    def set_profesional(self):
        self.wait.until_present('radio_profesional')
        self.radio_profesional.click()
        self.submit.click()


class Dashboard(pages.BasePage):

    """Page object of dashboard"""

    without_store_tab = sunbro.FindByCSS('.account-tab.first-time')
    with_store_tab = sunbro.FindByCSS('.account-tab')
    title_legend = sunbro.FindByID("dashboard-title")
    dashboard_messages = sunbro.FindAllByClass('dashboardHeader-txt')
    close_message_icon = sunbro.FindAllByClass('icon-close-message-icon')
    link_my_ads = sunbro.FindByID('link_my_ads')
    link_store = sunbro.FindByID('link_store')
    link_pack = sunbro.FindByID('link_pack')
    link_profile = sunbro.FindByID('link_profile')
    link_job = sunbro.FindByID('link_job')
    help_bump = sunbro.FindByID('bump-open-popup')
    help_gallery = sunbro.FindByID('gallery-more-info')
    help_label = sunbro.FindByID('label-more-info')
    profile_image = sunbro.FindByCSS('.dashboardHeader .profile-image')
    dashboard_container = sunbro.FindAllByClass("list-das")

    op_msg_desktop = sunbro.FindByCSS('.dashboardHeader-opMsgItem')
    op_msg_msite = sunbro.FindByCSS('.op-message')

    def go(self):
        self.go_dashboard()

    def go_dashboard(self):
        self._driver.get(conf.SSL_URL + "/dashboard")
        self.wait.until_loaded()

    def go_my_ads(self):
        self.go_dashboard()
        self.link_my_ads.click()

    def go_store(self):
        self.go_dashboard()
        self.link_store.click()

    def go_pack(self):
        self.go_dashboard()
        self.link_pack.click()
        return Pack(self._driver)

    def go_profile(self):
        self.go_dashboard()
        self.link_profile.click()

    def go_jobs(self):
        self.go_dashboard()
        self.link_job.click()
        return DashboardJobsTab(self._driver)

class DashboardAd(pages.BasePage):

    subject = sunbro.FindByCSS('.da-subject')
    status = sunbro.FindByCSS('.text-status-das')
    published_date = sunbro.FindByClass('date')
    price = sunbro.FindByCSS('.price')

    # pack controls
    enable_toggle = sunbro.FindByCSS('.toggle-blob')

    toggle_active = sunbro.FindByCSS('.label-status.active[style="display: block;"]')
    toggle_inactive = sunbro.FindByCSS('.label-status.inactive[style="display: block;"]')

    toggle_wrap = sunbro.FindByClass('toggle-wrap')

    # premiun products
    bumps_enabled = sunbro.FindByCSS('.product-select:not(.ps-disabled)')
    gallery_enabled = sunbro.FindByCSS(".product-select.blue:not(.ps-disabled)")
    labels_enabled = sunbro.FindByCSS(".product-select.purple:not(.ps-disabled)")

    # activate ad
    option_activate = sunbro.FindByCSS('.activate-da.active')
    option_bump_activate = sunbro.FindByCSS('.activate-da:not(.active)')

    edit_button = sunbro.FindByCSS(".single-loop-das:not(.delete) .icons-panel .icon-yapo.icon-pencil")
    delete_button = sunbro.FindByCSS(".single-loop-das:not(.delete) .icons-panel .icon-yapo.icon-trash")
    options_buttons =  sunbro.FindByCSS(".admin-das .icons-panel a")

    bump = sunbro.FindByCSS('.bump-1 ins')
    bump_check = sunbro.FindByClass('bump-check')

    weekly_bump = sunbro.FindByCSS('.bump-4 ins')
    weekly_bump_check = sunbro.FindByClass('bump-4-check')

    daily_bump = sunbro.FindByCSS('.bump-7 ins')
    daily_bump_check = sunbro.FindByClass('bump-7-check')
    daily_bump_msg = sunbro.FindByCSS('.product-status.bump-active')

    gallery = sunbro.FindByCSS('.gallery-col ins')
    gallery_check = sunbro.FindByClass('gallery-check')

    active_bumps_message = sunbro.FindByCSS(".product-status.bump-active")
    active_gallery_message = sunbro.FindByCSS(".product-status.gallery-active")

    btn_unpaid = sunbro.FindByCSS('.btn-unpaid')
    activate_tip = sunbro.FindByCSS('.activate-tip')

    label_tag = sunbro.FindByCSS('.fa-tag')

    products_field = {
        Products.BUMP: 'bump',
        Products.WEEKLY_BUMP: 'weekly_bump',
        Products.DAILY_BUMP: 'daily_bump',
        Products.GALLERY: 'gallery_7',
        Products.GALLERY_1: 'gallery_1',
        Products.GALLERY_30: 'gallery_30',
        Products.LABEL: 'label'
    }

    @lazy
    def get_status(self):
        return self.status.text

    @lazy
    def list_id(self):
        return int(self._driver.find_element_by_css_selector('[data-list_id]').get_attribute('data-list_id'))

    def go_to_pay(self):
        self.btn_unpaid.click()
        summary = SummaryPayment(self._driver)
        return summary

    def is_inactive(self):
        self.wait.until_visible(self.toggle_inactive)

    def is_active(self):
        self.wait.until_visible(self.toggle_active)

    def is_selected(self, product, param = None):
        """ Checks if the product is selected based on the id of the product """
        list_items = self._driver.find_elements_by_css_selector('[data-list_id]')
        # Used only for debugging
        for i, elem in enumerate(list_items):
            print('Element index %s is %s'%(i, elem.get_attribute('class')))

        selected_item = list_items[0].get_attribute("value")
        if self.products_field[product] == 'gallery_7':
            selected_item = list_items[0].get_attribute('value')

        if self.products_field[product] == 'label':
            selected_item = list_items[2].get_attribute('value')

        if selected_item == "0":
            return False

        if param is not None:
            return str(param) == selected_item

        return self.products_field[product] == selected_item

    def has_product_selector_disabled(self, product):
        try:
            if self.products_field[product] == 'label':
                webelement = self._driver.find_element_by_css_selector(".tags .product-select.ps-disabled")
            elif self.products_field[product] in ('gallery_1', 'gallery_7', 'gallery_30'):
                webelement = self._driver.find_element_by_css_selector(".gallery .product-select.ps-disabled")
            else:
                webelement = self._driver.find_element_by_css_selector(".bump .product-select.ps-disabled")
            return True
        except exceptions.NoSuchElementException:
            return False

    def select_product(self, product, param = None):
        """ Selects a product based on the id of the product """
        if not self.is_selected(product):
            if self.products_field[product] == 'label':
                self._driver.find_element_by_css_selector(".tags .ps-default").click()
                if param is None:
                    self._driver.find_element_by_css_selector(".tags .ps-item[data-value='1']").click()
                else:
                    self._driver.find_element_by_css_selector(".tags .ps-item[data-value='{0}']".format(str(param))).click()
            elif self.products_field[product] in ('gallery_1', 'gallery_7', 'gallery_30'):
                self._driver.find_element_by_css_selector(".gallery .ps-default").click()
                self._driver.find_element_by_css_selector(".gallery .ps-item[data-value='{0}']".format(self.products_field[product])).click()
            else:
                self._driver.find_element_by_css_selector(".bump .ps-default").click()
                self._driver.find_element_by_css_selector(".bump .ps-item[data-value='{0}']".format(self.products_field[product])).click()

    def unselect_product(self, product):
        tag = 'bump'

        if self.products_field[product] == 'label':
            tag = 'tags'

        if self.products_field[product] in ('gallery_1', 'gallery_7', 'gallery_30'):
            tag = 'gallery'

        self._driver.find_element_by_css_selector(".{0} .icon-close-circled".format(tag)).click()

    def get_product_price(self, product, param = None):
        tag = '.bump-select'
        if self.products_field[product] == 'label':
            tag = '.label-select'
        if self.products_field[product] in ('gallery_1', 'gallery_7', 'gallery_30'):
            tag = '.gallery-select'

        select = Select(self._driver.find_element_by_css_selector(tag))
        for option in select.options:
            if param is not None:
                if option.get_attribute("value") == str(param):
                    return option.get_attribute("data-price")
            else:
                if option.get_attribute("value") == str(product):
                    return option.get_attribute("data-price")
        print('Price not found - prod:{0}, param:{1}'.format(product,param))
        return '0'

    def edit(self):
        return self._driver.find_element_by_css_selector(".active .icon-pencil").click()

class Modal(pages.BasePage):
    disable_button = sunbro.FindByCSS('.pm-content .btn-options .btn-cancel')
    expired_pack = sunbro.FindByID('pack_expired_content')
    without_quotas_dialog = sunbro.FindByCSS("#pgwModal .dialog-content-body-message")
    without_quotas_buy_pack_button = sunbro.FindByCSS("#pgwModal .dialog-content-body-btn.__btn-buy-pack")
    without_quotas_close_button = sunbro.FindByCSS('#pgwModal .pm-close')
    pm_content = sunbro.FindByCSS('#pgwModal .pm-content')
    buy_inserting_fee = sunbro.FindByCSS("#pgwModal .dialog-content-body .buy-inserting-fee")
    price_inserting_fee = sunbro.FindByCSS("#pgwModal .dialog-content-body .inserting-fee-price")

    @property
    def card_pack_desktop_basic(self):
        return self.select_shadow_element_by_css_selector('#pgwModal .premium-pack-car .card-pack-destop-basic')

    @property
    def card_pack_desktop_premium(self):
        return self.select_shadow_element_by_css_selector('#pgwModal .premium-pack-car card-pack-desktop[theme=blue]')

    @property
    def price_inserting_fee_basic(self):
        return self.card_pack_desktop_basic.find_element_by_css_selector('.pack-card-desktop__header-price')

    @property
    def buy_inserting_fee_basic(self):
        return self.card_pack_desktop_basic.find_element_by_css_selector('.pack-card-desktop__body-action.pack-card-desktop__body-action--orange')
    
    @property
    def buy_inserting_fee_premium(self):
        return self.card_pack_desktop_premium.find_element_by_css_selector('.pack-card-desktop__body-action.pack-card-desktop__body-action--blue')

class Pack(pages.BasePage):

    sort_button = sunbro.FindByCSS('.sort-by li .icon-arrow-down-bold')
    sort_inactive = sunbro.FindByCSS('.sb-item[data-value="inactive"]')
    sort_active = sunbro.FindByCSS('.sb-item[data-value="active"]')
    sort_date = sunbro.FindByCSS('.sb-item[data-value="all"]')
    contact_list = sunbro.FindByCSS('#contactListSearch')
    search_list = sunbro.FindByCSS('.getContactList-searchList')

    def change_status(self, ad, status):

        ad.enable_toggle.click()
        if status == 'active':
            ad.wait.until_visible('toggle_active')
        else:
            # it has premium product running, accept the modal
            prod_list = set(['Semanal', 'Diario'])
            if any(prod in ad.active_bumps_message.text for prod in prod_list) or ad.active_gallery_message.text != '':
                modal = Modal(self._driver)
                self._driver.execute_script('arguments[0].click()', modal.disable_button)
            ad.wait.until_visible('toggle_inactive')

    def get_inserting_fee_price(self, ad):
        ad.enable_toggle.click()
        modal = Modal(self._driver)
        modal.wait.until_visible("pm_content")
        if modal.price_inserting_fee.text:
            price = modal.price_inserting_fee.text
        else:
            price = modal.price_inserting_fee_basic.text
        modal.without_quotas_close_button.click()
        return price

    def select_inserting_fee(self, ad):
        ad.enable_toggle.click()
        modal = Modal(self._driver)
        modal.wait.until_visible("pm_content")
        if modal.price_inserting_fee.text:
            modal.buy_inserting_fee.click()
        else:
            modal.buy_inserting_fee_basic.click()

    def filter_ads(self, ads):
        """ All the pack ads """
        return {list_id: ad for list_id, ad in ads.items() if ad.is_element_present('toggle_wrap')}

    def filter_active_ads(self, ads):
        """ Return the active pack ads """
        return {list_id: ad for list_id, ad in ads.items() if ad.is_element_present('toggle_active')}

    def filter_disabled_ads(self, ads):
        """ Return the disabled pack ads """
        return {list_id: ad for list_id, ad in ads.items() if ad.is_element_present('toggle_inactive')}

    def sort_items(self, text):
        if text == 'inactive':
            self.sort_inactive.click()
            time.sleep(3)
        elif text == 'all':
            self.sort_date.click()
            time.sleep(3)
        else:
            self.sort_active.click()
            time.sleep(5)

class DashboardMyAds(pages.BasePage):

    """Page object of dashboard to manage the user's ads list"""

    # Lists
    list_inactive_ads = sunbro.FindAllByCSS(".single-loop-das.inactive")
    list_active_ads = sunbro.FindAllByCSS(".single-loop-das:not(.inactive)")
    list_selected_products = sunbro.FindAllByCSS('.ps-selected')
    list_all_ads = sunbro.FindAllByCSS('.single-loop-das')
    list_all_active_ads = sunbro.FindAllByCSS('.single-loop-das.active')
    list_all_subjects = sunbro.FindAllByCSS('p.title')
    list_all_bump_buttons = sunbro.FindAllByCSS('.buy-bump')

    activate_da = sunbro.FindAllByCSS('.activate-da')

    # Pagination
    pagination = sunbro.FindByID('pagination')
    current_page = sunbro.FindByCSS('.current')
    next_page = sunbro.FindByCSS('.nav')
    slide_next = sunbro.FindByID('slide-next')
    slide_prev = sunbro.FindByID('slide-prev')
    first_last_page = sunbro.FindAllByCSS('.first-last')
    pages = sunbro.FindAllByCSS(".page")
    checkout_button = sunbro.FindByCSS('a#checkout span')

    # Definitions of SELECTORS
    # Use this definitions to find elements in the page or in the lists previously defined
    # The objective of this block is avoid definition of css selectors directly on tests
    selector_css_product_list = ".product-list"

    selector_css_icheck_bump = "#bump-{0} + ins"
    selector_css_icheck_bump4 = "#bump-4-{0} + ins"
    selector_css_icheck_gallery = "#gallery-{0} + ins"

    selector_input_bump = ".icheck.bump-check"
    selector_attribute_id = "data-list_id"

    flag_ads_counter = sunbro.FindByCSS('.single-das.header-list-das span')
    ads_number = sunbro.FindByCSS('.number-das span:first-of-type')
    products_in_cart = sunbro.FindByID('number-items-cart')
    publish_ad_arrow = sunbro.FindByCSS('#insert-ads-arrow')
    no_ads = sunbro.FindByCSS('.no-das h2')
    inserting_fee_tags = sunbro.FindAllByCSS('.inserting-fee-tag')

    def go(self):
        self.go_dashboard()
        return self

    def go_dashboard(self, parameter=None):
        if parameter is None:
            if self._driver.current_url == conf.SSL_URL + '/dashboard':
                self._driver.refresh()
            else:
                self._driver.get(conf.SSL_URL + '/dashboard')
        else:
            self._driver.get(conf.SSL_URL + '/dashboard?' + parameter)
        self.wait.until_loaded()

    def autobump(self, ld):
        self.wait.until_loaded()
        bump = self._driver.find_element_by_css_selector('#ad_row_{0} .product-col.bump'.format(ld))
        bump_custom = self._driver.find_element_by_css_selector('#ad_row_{0} .ps-item.ps-item-customBump'.format(ld))
        self._driver.execute_script("return arguments[0].scrollIntoView();", bump)
        bump.click()
        bump_custom.click()

    def select_combo_at(self, ld, premium=False):
        self.wait.until_loaded()
        combo = self._driver.find_element_by_css_selector('#ad_row_{0} .product-col.combo-at'.format(ld)) 
        if premium:
            combo_option = self._driver.find_element_by_css_selector('#ad_row_{0} .combo-at-labels-element'.format(ld))
        else:
            combo_option = self._driver.find_element_by_css_selector('#ad_row_{0} .product-col.combo-at .ps-item'.format(ld))
        
        combo.click()
        combo_option.click()

    def select_products_by_ids(self, ld):
        """
        Usage:
              ld: {12121: [Products.BUMP, Products.GALLERY]}
              Use a hash with the list_id as key, and a list of the products to apply
        """

        self.scroll_to_bottom()
        for list_id, products_list in ld.items():
            for product_id in products_list:
                self.active_ads[list_id].select_product(product_id)

    @lazy
    def active_ads(self):
        active_ads = {}
        for active_ad in map(DashboardAd, self.list_active_ads):
            active_ads[active_ad.list_id] = active_ad
        return active_ads

    @lazy
    def inactive_ads(self):
        inactive_ads = {}
        for inactive_ad in map(DashboardAd, self.list_inactive_ads):
            inactive_ads[inactive_ad.list_id] = inactive_ad
        return inactive_ads

    @lazy
    def all_ads(self):
        all_ads = {}
        for ad in map(DashboardAd, self.list_all_ads):
            all_ads[ad.list_id] = ad
        return all_ads


    def search_ad_by_subject(self, subject):
        for ad in map(DashboardAd, self.list_all_ads):
            print('Dashboard_ad:{}'.format(ad.subject.text))
            if ad.subject.text == subject:
                return ad
        return None

    def go_to_delete_ad(self, subject):
        ad = self.search_ad_by_subject(subject)
        ad.delete_button.click()
        return ad

    def activate_ad(self, ad=None, subject=None):
        if not ad:
            if subject:
                ad = self.search_ad_by_subject(subject)

        ad.option_activate.click()
        self.wait.until_loaded_jquery_ajax()

    def create_ad_list(self):
        """return list with sublists with subject and status
           Example:
           ads_list = [[<subject1>, <status1>],
                       [<subject2>, <status2>], ...]
        """
        ads_list = []
        for ad in self.list_all_ads:
            row_ad = []
            row_ad.append(ad.find_element_by_css_selector('.da-subject').text)
            row_ad.append(ad.find_element_by_css_selector('.text-status-das').text)
            ads_list.append(row_ad)
        return ads_list

    def search_status_by_subject(self, subject):
        """return the status of certain ad by subject"""

        for ad in self.create_ad_list():
            if ad[0] == subject:
                return ad[1]

        return None

    def delete_ad_by_subject(self, subject):
        """go to delete page from the dashboard by subject"""

        for ad in self.list_all_ads:
            if ad.find_element_by_css_selector('.da-subject').text == subject:
                ad.find_elements_by_css_selector('.icons-panel a')[1].click()
                break

        delete_page = AdDeletePage(self._driver)
        delete_page.delete_ad_logged()

    def get_view_count(self, list_id = None):
        """get the view counter for an ad in dashboard"""
        return self._driver.find_element_by_css_selector('#ad_row_{0} .icon-eye ~'.format(list_id)).text

    def get_contact_count(self, list_id = None):
        """get the view counter for an ad in dashboard"""
        return self._driver.find_element_by_css_selector('#ad_row_{0} .people'.format(list_id)).text

    def edit_ad_by_subject(self, subject):
        """go to delete page from the dashboard by subject"""

        for ad in self.list_all_ads:
            if ad.find_element_by_css_selector('.da-subject').text == subject:
                ad.find_elements_by_css_selector('.icons-panel a')[0].click()
                break

class SummaryPayment(pages.BasePage):

    """Page object for the payment summary page"""

    # Links
    link_to_more_info = sunbro.FindByClass('summaryPromo-disclaimerLink')
    link_to_pay_products = sunbro.FindByID('checkout')
    links_remove_icon = sunbro.FindAllByCSS('.remove-icon')
    link_sure_to_delete_yes = sunbro.FindAllByCSS('.darktooltip-yes')
    link_sure_to_delete_no = sunbro.FindAllByCSS('.darktooltip-no')
    radio_oneclick = sunbro.FindByID('pay_with_oneclick')
    radio_webpay = sunbro.FindByID('webPayContainer')
    radio_servipag = sunbro.FindByID('pay_with_servipag')
    radio_khipu = sunbro.FindByID('pay_with_khipu')
    radio_bill = sunbro.FindByCSS('label[for="radio-bill"]')
    radio_credits = sunbro.FindByID('creditsCheck')
    summary_type_labels = sunbro.FindAllByCSS(".summaryType #invoice .summaryType-label")

    products_list_table = sunbro.FindByCSS('.productTable')
    product_row0 = sunbro.FindByID('row0')
    product_row_link0 = sunbro.FindByCSS('#row0 .productTable-link')
    product_row_link1 = sunbro.FindByCSS('#row1 .productTable-link')
    product_row_link2 = sunbro.FindByCSS('#row2 .productTable-link')
    product_row1 = sunbro.FindByID('row1')
    product_row2 = sunbro.FindByID('row2')

    bar_sticky = sunbro.FindByID('sticky-bar')
    amount = sunbro.FindByID('total-amount')
    text_amount = sunbro.FindByID('total-amount')
    text_message_ad = sunbro.FindByCSS('.thankyouMessage')
    text_message_payment = sunbro.FindByCSS('.summaryPage-subtitle')
    text_message_payment_subject = sunbro.FindByCSS('.summaryPage-subtitle b')

    bar_number_pack = sunbro.FindByID('number-pack')
    bar_number_gallery = sunbro.FindByID('number-gallery')
    bar_number_weekly = sunbro.FindByID('number-bump-4')
    bar_number_daily = sunbro.FindByID('number-bump-7')
    bar_number_bump = sunbro.FindByID('number-bump')

    empty_cart = sunbro.FindByClass('summaryPromo')

    document_type_bill = sunbro.FindByCSS('.summaryType-label[for="radio-bill"]')
    document_type_invoice = sunbro.FindByCSS('.summaryType-label[for="radio-invoice"]')

    remove_ad_success = sunbro.FindByID('remove-ad-success')

    # Warning / error message after refused / cancelled payment
    warning_message = sunbro.FindByCSS('h1')

    input_giro = sunbro.FindByID('id_lob')

    #Modal for cart validation
    modal_cart = sunbro.FindByCSS('.ShoppingCartModal .pm-body')
    modal_cart_close = sunbro.FindByCSS('.ShoppingCartModal .pm-body .ShoppingCartModal-HeaderClose')
    modal_cart_header_title = sunbro.FindByCSS('.ShoppingCartModal .pm-body .ShoppingCartModal-HeaderTitle')
    modal_cart_body_title = sunbro.FindByCSS('.ShoppingCartModal .pm-body .ShoppingCartModal-BodyTitle')
    modal_cart_body_text = sunbro.FindByCSS('.ShoppingCartModal .pm-body .ShoppingCartModal-BodyText')
    modal_cart_button = sunbro.FindByCSS('.ShoppingCartModal .pm-body button')

    def get_table_data(self, table_name='products_list_table', table_elem=None):
        """ Get the cell values/text in a table """
        table_values = super().get_table_data(table_name, table_elem)
        return table_values[1:] if len(table_values) else []

    def go_summary(self):
        self._driver.get(conf.SSL_URL + '/pagos')
        self.wait.until_loaded()

    def go(self):
        self.go_summary()
        return self

    def go_summary_from_upselling(self, ad_id, product_id):
        self._driver.get(conf.SSL_URL + '/pagos?id={}&prod={}&ftype=2&from=newad&lt=4'.format(ad_id, product_id))

    def press_link_to_more_info(self):
        self.wait.until_present('link_to_more_info')
        self.link_to_more_info.click()

    def press_link_to_pay_products(self, method = 'webpay'):
        radio = 'radio_{0}'.format(method)
        self.wait.until_visible(radio)
        getattr(self, radio).click()
        self.wait.until_present('link_to_pay_products')
        self.link_to_pay_products.click()

    def remove_product(self, i):
        self.links_remove_icon[i].click()
        self.wait.until_visibility_of_element(self.link_sure_to_delete_yes[i])
        self.link_sure_to_delete_yes[i].click()

    def add_product(self, list_id, product):
        self._driver.get(
            conf.SSL_URL + '/pagos?id=' + str(list_id) + '&prod=' + str(product))
        self.wait.until_loaded()

    def add_store(self, store_type):
        self._driver.get(conf.SSL_URL + '/pagos?prod=' + str(store_type))

    def add_element_to_cart(self, prod, list_id=None):
        extra_info = ''
        if str(prod) == '9' or str(prod) == '104' or str(prod) == '1053':
            extra_info = '&lt=1'
        if list_id is None:
            self._driver.get(conf.SSL_URL + '/pagos?prod='+ str(prod) + extra_info)
        else:
            self._driver.get(conf.SSL_URL + '/pagos?prod='+ str(prod) + '&id='+ str(list_id) + extra_info)
        self.wait.until_loaded()

    def add_pack_to_cart(self, slots = '010', period = PacksPeriods.QUARTERLY, pack_type = PacksType.CAR):
        self.add_element_to_cart(str(pack_type)+str(period)+slots)

    def change_doc_type(self, doc_type):
        if doc_type == Payment.INVOICE:
            self.document_type_invoice.click()
            self.wait.until_visible('summary_type_labels')
        else:
            self.document_type_bill.click()
            self.wait.until_not_visible('summary_type_labels')

    def choose_bill(self):
        self.change_doc_type('bill')

    def choose_invoice(self):
        self.change_doc_type('invoice')

    def pay_with_khipu(self):
        self.radio_khipu.click()
        self.link_to_pay_products.click()

    def pay_with_webpay(self):
        self.radio_webpay.click()
        self.link_to_pay_products.click()

    def pay_with_credits(self):
        self.radio_credits.click()
        self.link_to_pay_products.click()

class ForgotPassword(pages.BasePage):
    email_input = sunbro.FindByName('email')
    submit_button = sunbro.FindByCSS('[value="Enviar"]')
    success_title = sunbro.FindByCSS('.reset-password-main .title-account')
    success_msg = sunbro.FindByCSS('.reset-password-main .recover-pass-message')

    def go(self, id, region='', recovery_type=''):
        url = '/sp?id={0}'.format(id)
        if region:
            url += '&ca={0}_s'.format(region)
        if recovery_type:
            url += '&recovery_type={0}'.format(recovery_type)
        self._driver.get(conf.PHP_URL + url)
        self.wait.until_loaded()

    def send_email(self, email):
        self.email_input.send_keys(email)
        self.submit_button.click()


class MyAds(pages.BasePage):

    """ My ads page  """

    email_input = sunbro.FindByID('my_ads_email')
    send_input = sunbro.FindByID('email_submit')
    ok_message = sunbro.FindByID('my_ads_success')
    error_message = sunbro.FindByID('my_ads_error')

    def go(self):
        self._driver.get(conf.DESKTOP_URL + '/mis_avisos.html')
        self.wait.until_loaded()

    def request(self, email):
        """ Fills the form with a user e-mail, so my ads e-mail is sent """

        self.email_input.clear()
        self.email_input.send_keys(email)
        self.send_input.click()
        self.wait.until_present('ok_message')

    def request_fail(self, email):
        """ Fills the form with an invalid e-mail """

        self.email_input.clear()
        self.email_input.send_keys(email)
        self.send_input.click()
        self.wait.until_present('error_message')

    def go_and_request(self, email):
        self.go()
        self.request(email)

    def go_and_fail_request(self, email):
        self.go()
        self.request_fail(email)


@ComponentComposer
class DesktopHome(pages.BasePage):
    """Page objects for the desktop home page"""
    site_description = sunbro.FindByID("site_description")
    footer_description = sunbro.FindByCSS(".footer-appsTitle")
    css_region = ".region-links li[data-region='{region}'] a"
    site_statistics = sunbro.FindAllByCSS("#site_description p")
    adserver_home_top_inner = sunbro.FindByID('adserver-home-top-inner')
    adserver_home_right_inner = sunbro.FindByID('adserver-home-right-inner')
    adserver_home_right = sunbro.FindByID('adserver-home-right')
    map_wrap = sunbro.FindByID('map_wrap')
    old_home = (yapo.conf.DESKTOP_URL + '/')
    new_home = (yapo.conf.DESKTOP_URL + '/inicio')
    OLD_SITE_DESCRIPTION = "Encuentra clasificados en tu regi�n"
    NEW_FOOTER_DESCRIPTION = "Descarga nuestra app"
    pp_banner = sunbro.FindByCSS('.ppages-banner a img')

    def go(self):
        self._driver.get(conf.DESKTOP_URL)
        self.wait.until_loaded()
        return self

    def go_merken_home(self):
        self._driver.get(self.new_home)
        self.wait.until_loaded()
        return self

    def click_region(self, num_region):
        self._driver.find_element_by_css_selector(self.css_region.format(region=num_region)).click()
        self.wait.until_loaded()

    def am_i_here(self):
        return self.is_element_present('map_wrap')

class PageNotFound(pages.BasePage):

    """Page object for the 404 page"""

    title = sunbro.FindByCSS('#container_main h1')
    link_aqui = sunbro.FindByLinkText('aqu�')

    def go_page_not_found_ssl_port(self):
        self._driver.get(conf.SSL_URL + '/yolo')

    def go_page_not_found_php_port(self):
        self._driver.get(conf.PHP_URL + '/yolo')

    def go_page_not_found_desktop_url(self):
        self._driver.get(conf.DESKTOP_URL + '/yolo')

class CheckBill(pages.BasePage):

    """ Page object for the "consultaboleta" page"""
    email = sunbro.FindByID("email")
    bill = sunbro.FindByID("bill_number")
    submit_button = sunbro.FindByCSS('input[name=send]')

    radio_bill = sunbro.FindByCSS('label[for=bill_number]')
    radio_invoice = sunbro.FindByCSS('label[for=invoice_number]')

    err_email = sunbro.FindByID("err_email")
    err_bill = sunbro.FindByID("err_doc_num")
    no_bills_msg = sunbro.FindByCSS('.validation_msg.error')

    def go(self):
        self._driver.get(conf.SSL_URL + '/consultaboleta')

class FaqPage(pages.BasePage):
    """
    Page object for the faq page
    """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = conf.DESKTOP_URL_04
        self.secure_url = conf.SSL_URL

    def go_to_faq(self):
        self.driver.get(self.base_url+"/ayuda/preguntas_frecuentes.html")

    def go_to_support_form(self,id):
        self.driver.get(self.base_url+"/support/form/0?id="+str(id))

    def check_item(self, item, expected_question, expected_answer):
        h3 = item.find_element_by_tag_name("h3")
        h3.click()
        real_question = item.find_element_by_class_name("question_title")
        if expected_question != real_question.text:
            return False

        answer = item.find_element_by_class_name("answer")
        if answer.value_of_css_property("display") != u"block":
            return False

        real_answer = answer.find_element_by_tag_name("p")
        if expected_answer != real_answer.text:
            return False

        return True

class AboutPage(pages.BasePage):
    """
    Page object for the faq page
    """
    def __init__(self, driver):
        self.driver = driver
        self.base_url = conf.DESKTOP_URL
        self.secure_url = conf.SSL_URL

    def go_to_about(self):
        self.driver.get(self.base_url+"/ayuda/sobre_yapo.html")

class DashboardJobsTab(pages.BasePage):

    """Page object of dashboard to manage the user's ads list"""

    # Job's content container
    jobs_content = sunbro.FindAllByCSS(".jobTab")
    subject = sunbro.FindByCSS(".jobTab-formInput.__da")
    list_id = sunbro.FindByCSS(".jobTab-formInput.__hidden")
    email = sunbro.FindByCSS(".jobTab-formInput.__email")
    ad = sunbro.FindByCSS("ul.getContactList-searchList > :nth-child(1)")
    btn_submit = sunbro.FindByCSS(".jobTab-formSubmit")
    response = sunbro.FindByCSS(".jobTab-formResponse.__success")

    ERROR_NO_CONTACTS = "El aviso no tiene postulaciones"
    ERROR_USER_HAS_NOT_PAID_FOR_AN_AD = "A partir de tu siguiente aviso podr�s descargar tus postulaciones"
    SUCCESS = "Mensaje enviado con �xito"

    def go(self):
        self._driver.get(conf.SSL_URL + "/empleo")
        self.wait.until_visible('jobs_content')

    def select_ad(self, index):
        self.subject.click()
        ad = self._driver.find_element_by_css_selector('ul.getContactList-searchList > :nth-child({})'.format(index))
        ad.click()
        print(self.list_id.get_attribute('value'))

    def fill_form(self, index, email):
        self.select_ad(index)
        self.email.send_keys(email)
        self.btn_submit.click()

class DeleteUnpaidModal(pages.BasePage):

    modal = sunbro.FindByCSS('.deleteModal')
    close_button = sunbro.FindByCSS('#pgwModal .deleteModal-button.__Close')
    yes_button = sunbro.FindByCSS('#pgwModal .deleteModal-button.__Agree')
    no_button = sunbro.FindByCSS('#pgwModal .deleteModal-button.__Disagree')

    def open(self):
        self.wait.until_visible('yes_button')
        self.wait.until_visible('no_button')

    def confirm(self):
        self.yes_button.click()
        self.wait.until_visible('close_button')
        self.close_button.click()
        self.wait.until_not_visible('modal')

    def reject(self):
        self.no_button.click()
        self.wait.until_not_visible('close_button')

    def click_yes(self):
        self.yes_button.click()

