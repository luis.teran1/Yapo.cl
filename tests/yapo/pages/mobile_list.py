from yapo.pages import BasePage
from yapo.component import select
from yapo.pages.base import AdList
from yapo import conf
from selenium.webdriver.support.select import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import nose_selenium
import sunbro


class AdListMobile(AdList):

    """ Page object used to interact with the ad view page """

    draft_wrap = sunbro.FindByCSS('.message.message-draft')
    search_box = sunbro.FindByID('keyword')
    search_filter = sunbro.FindByID('search_filter')
    filter_button = sunbro.FindByID('filters')
    search_button = sunbro.FindByID('search')
    categories = sunbro.FindByID('selectbox_categories')
    category = categories  # XXX we are lazy, fuck off you cunt
    ccs = sunbro.FindByCSS('#searchbox_a [name=ccs]')
    cce = sunbro.FindByCSS('#searchbox_a [name=cce]')
    adserver_bottom_banner = sunbro.FindByID("dabottom_list")
    ads_li = sunbro.FindAllByCSS('.list_ads li')
    position_6th_da = sunbro.FindByID('position_6th_da')
    position_6th_da_banner = sunbro.FindByCSS('#position_6th_da a img')
    position_40th_da = sunbro.FindByID('position_40th_da')
    position_40th_da_banner = sunbro.FindByCSS('#position_40th_da a img')
    gallery_list = sunbro.FindByCSS('.gallery-list.real')
    titles = sunbro.FindAllByCSS('.listing .title')
    pp_banner = sunbro.FindByCSS('.ppages-banner a img')
    pp_banner_link = sunbro.FindByCSS('.ppages-banner a')
    first_ad_list = sunbro.FindByCSS('.ad:first-child')
    splash_screen = sunbro.FindByID("moveToApps")
    first_ad_link = sunbro.FindByCSS(".ad:first-child a")
    first_gallery_ad = sunbro.FindByCSS("gallery-ad:first-child")
    
    def go(self, args=None):
        args = args if args else "li"
        self._driver.get(conf.MOBILE_URL + "/" + args)
        self.wait.until_loaded()

    def go_to_region(self, region_name):
        self._driver.get('{0}/{1}'.format(conf.MOBILE_URL, region_name))
        self.wait.until_loaded()

    def open_filters(self):
        self.filter_button.click()

    def search_for_ad(self, search_text):
        self.search_box.clear()
        self.search_box.send_keys(search_text)
        self.search_button.click()

    def go_to_ad(self, list_id=None, subject=None):
        if list_id:
            self._driver.find_element_by_id(unicode(list_id)).find_element_by_tag_name("a").click()
        elif subject:
            self._driver.find_element_by_partial_link_text(subject).click()

        return AdViewMobile(self._driver)

    def _get_ad_data(self, ad):
        data = {}
        id_filtered = (self.elem_id(ad))[8:]
        data['id'] = id_filtered

        self.find_ad_prop(data, ad, 'date', '.listing-item__info-bottom-date', self.elem_text)
        self.find_ad_prop(data, ad, 'title', '.listing-item__info-title span', self.elem_text)
        self.find_ad_prop(data, ad, 'price', '.listing-item__info-price', self.elem_text)
        self.find_ad_prop(data, ad, 'has_address', '.icon-location', bool, False)
        self.find_ad_prop(data, ad, 'has_label', '.listing-item__image-label', self.elem_text, False)
        return data

    location_icon = sunbro.FindByCSS('.icon-location')
    label = sunbro.FindByCSS('.fa-tag')

    def search_for_moto(self, ccs, cce):
        self._driver.get(conf.MOBILE_URL + "/region_metropolitana/motos")
        """TODO validate ccs, cce values"""
        self.filter_button.click()
        select(self.ccs, ccs)
        select(self.cce, cce)
        self.search_filter.click()

    def browse(self, region='region_metropolitana', category=None):
        self._driver.get("about:blank")
        if category:
            self._driver.get(
                '{0}/{1}/{2}'.format(conf.MOBILE_URL, region, category))
        else:
            self._driver.get('{0}/{1}'.format(conf.MOBILE_URL, region))
        self.wait.until_loaded()

    def get_gallery(self, num):
        self.wait.until_loaded()
        item = self.gallery_list.find_element_by_css_selector(
            'li:nth-child({0})'.format(num))
        return GalleryAd(item)

    def get_list_ids_in_li(self):
        return [ad.find_element_by_tag_name('a').get_attribute('id') for ad in self.ads_list]

    # functions to handler element of shadow root
    @property
    def ads_list(self):
        return self.select_all_shadow_element_by_css_selector('listing-item', 'firstElementChild')

    def get_first_ad(self):
        ad = self.select_shadow_element_by_css_selector('listing-item:first-child', 'firstElementChild')
        return ad

    def get_first_ad_container(self):
        ad = self.select_shadow_element_by_css_selector('listing-item:first-child')
        return ad

    def get_ads_price(self):
        ads = self.ads_list
        prices = []
        for ad in ads:
            prices.append(ad.find_element_by_css_selector('.listing-item__info-price'))
        return prices

    def get_ads_title(self):
        ads = self.ads_list
        prices = []
        for ad in ads:
            prices.append(ad.find_element_by_css_selector('.listing-item__info-title span'))
        return prices


class GalleryAd(BasePage):

    subject = sunbro.FindByCSS('.gallery-item-price span')
    image = sunbro.FindByCSS('.gallery-image')
    price = sunbro.FindByCSS('.gallery-item-price')


class AdListAd(BasePage):
    location_icon = sunbro.FindByCSS('.icon-location')
    label = sunbro.FindByCSS('.fa-tag')


class AdViewMobile(BasePage):
    subject = sunbro.FindByCSS('h1')
    body = sunbro.FindByCSS('p.texto')
    advertiser_name = sunbro.FindByCSS('.desc.name .value.name')
    phone = sunbro.FindByCSS('.phone_number')
    price = sunbro.FindByCSS('.da-detail .da-detail__prices-price')
    cubiccms = sunbro.FindByCSS('.item:nth-child(5) span.value')
    ad_id = sunbro.FindByID('list_id')
    # When ad was not found the page of ad_not_found is visible with this
    # stuff:
    ad_not_found_title = sunbro.FindByCSS('.missing h1')
    adparams_box = sunbro.FindByCSS('.content_box.type1')
    adparams = sunbro.FindAllByCSS('.value', within = 'adparams_box')
    region_commune = sunbro.FindByCSS(".map-region span")
    adserver_bottom_banner = sunbro.FindByID("da_view_bottom_banner")
    label = sunbro.FindByCSS(".fa-tag")
    compara_online_link = sunbro.FindByID('comparaonline_link')
    cotiza_credito_link = sunbro.FindByID("cotiza_help_credito")
    cotiza_seguro_link = sunbro.FindByID("cotiza_help_seguro")
    cotiza_banners = sunbro.FindAllByCSS(".pgm-body .oas-banner iframe")
    cotiza_raw_link = sunbro.FindByID("cotiza_raw_link")
    cotiza_raw_link_banner = sunbro.FindByCSS("#cotiza_raw_link a")
    da_view_top_banner = sunbro.FindByID("da_view_top_banner")
    da_view_top_banner_link = sunbro.FindByCSS("#da_view_top_banner a")
    ad_admin_remove_link = sunbro.FindByCSS(".ad-options .delete")
    ad_edit_button = sunbro.FindByCSS(".ad-options .edit")
    delete_button = sunbro.FindByCSS('.bt_delete')
    bt_edit = sunbro.FindByCSS('.bt_edit')

    # Navigation
    back_listing = sunbro.FindByCSS('.back-listing i')
    sticky_prev_link = sunbro.FindByCSS('#sticky_prev_link i')
    sticky_next_link = sunbro.FindByCSS('#sticky_next_link i')

    # Share
    social_links = sunbro.FindAllByCSS('.daview-sociaLinksBox')

    # autofact banner
    autofact = sunbro.FindByCSS('.autofact')
    autofact_btn = sunbro.FindByCSS('.autofact-btn')
    autofact_text = sunbro.FindByCSS('.autofact-text')
    REQUEST_REPORT = "PEDIR INFORME"

    def go(self, list_id):
        self._driver.get('{0}/vi/{1}.htm'.format(conf.MOBILE_URL, list_id))

    def back_to_results(self):
        self.back_listing.click()

    def has_price(self):
        return len(self.price.text) > 0

    def is_first(self):
        return 'icon-icon-arrow-right-end' in self.sticky_next_link.get_attribute('class')

    def is_last(self):
        return 'icon-icon-arrow-left-end' in self.sticky_next_link.get_attribute('class')

    def is_extremal(self, direction):
        {'next': self.is_last,
         'prev': self.is_first,
        }[direction]()

    def next_ad(self):
        self.brutally_click('#sticky_next_link i')

    def prev_ad(self):
        self.brutally_click('#sticky_prev_link i')

    def navigate(self, direction):
        {'next': self.next_ad,
         'prev': self.prev_ad,
        }[direction]()


class SearchBox(BasePage):

    """
    Page object used to search box
    """
    filter_button = sunbro.FindByID('filters')
    search_button = sunbro.FindByID('search')
    category = sunbro.FindByID('selectbox_categories')
    search_filter = sunbro.FindByID("search_filter")
    search_box = sunbro.FindByID("keyword")
    search_button = sunbro.FindByID('search')

    comunes_filter = sunbro.FindByID("cmn_select")
    # type
    type_a = sunbro.FindByID('type_a')
    type_s = sunbro.FindByID('type_s')
    type_k = sunbro.FindByID('type_k')

    label_st_a = sunbro.FindByID('label_st_a')
    label_st_s = sunbro.FindByID('label_st_s')
    label_st_k = sunbro.FindByID('label_st_k')

    # cars
    brand = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="br"]')
    model = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="mo"]')

    min_regdate = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="rs"]')
    max_regdate = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="re"]')

    mileage_min = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="ms"]')
    mileage_max = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="me"]')

    fuel = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="fu"]')
    cartype = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="ctp"]')
    gearbox = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="gb"]')

    # motos
    ccs = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="ccs"]')
    cce = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="cce"]')

    # clothing
    gender = sunbro.FindByCSS('div[id*="searchbox_"].stype select[name="gend"]')
    condition = sunbro.FindByCSS('div[id*="searchbox_"][style="display: block;"].stype select[name="cond"]')

    job_category_1 = sunbro.FindByID('jc_1')
    job_category_5 = sunbro.FindByID('jc_5')
    job_category_7 = sunbro.FindByID('jc_7')

    service_type_1 = sunbro.FindByID('sct_1')
    service_type_5 = sunbro.FindByID('sct_5')
    service_type_7 = sunbro.FindByID('sct_7')

    #filters real estate
    estate_type = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype select[name='ret']")
    min_price = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='ps']")
    max_price = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='pe']")
    min_rooms = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='ros']")
    max_rooms = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='roe']")
    min_size = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='ss']")
    max_size = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='se']")
    min_bathrooms = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='brs']")
    max_bathrooms = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='bre']")
    min_condominio = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='cos']")
    max_condominio = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='coe']")
    garage_spaces = sunbro.FindByCSS("div[id*='searchbox_'][style='display: block;'].stype [name='gs']")

    footwear_gend = sunbro.FindByCSS('[name="fwgend"]')
    footwear_size = sunbro.FindByCSS('[name="fwsize"]')
    footwear_type_1 = sunbro.FindByCSS('[name="fwtype"][value="1"]')
    footwear_type_2 = sunbro.FindByCSS('[name="fwtype"][value="2"]')
    footwear_type_3 = sunbro.FindByCSS('[name="fwtype"][value="3"]')
    footwear_type_4 = sunbro.FindByCSS('[name="fwtype"][value="4"]')
    footwear_type_5 = sunbro.FindByCSS('[name="fwtype"][value="5"]')
    footwear_type_6 = sunbro.FindByCSS('[name="fwtype"][value="6"]')

    def fill(self, name, value):
        super().fill(name, value)
        if name == 'brand':
            if value == 'Marca':
                self.wait.until(lambda _: len(Select(self.model).options) == 1)
            else:
                self.wait.until(lambda _: len(Select(self.model).options) > 1)

    def open_filters(self):
        self.wait.until_loaded()
        self.filter_button.click()


    def selected(self, attr):
        return Select(getattr(self,attr)).first_selected_option.text

    def go(self, slug):
        self._driver.get(conf.MOBILE_URL + slug)

    def search(self):
        self.search_filter.click()

    def go_to_region(self, region_name):
        self._driver.get('{0}/{1}'.format(conf.MOBILE_URL, region_name))
        self.wait.until_loaded()
