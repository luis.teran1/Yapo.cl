from yapo.pages import BasePage
from lazy import lazy
from yapo import conf
import sunbro


class CreditsForm(BasePage):

    """Credits form page"""
    value = sunbro.FindByID('creditBuy-value')
    submit = sunbro.FindByID('creditBuy-submit')
    status = sunbro.FindByID('credits-status')
    to_expire = sunbro.FindByID('credits-to-expire')
    date_to_expire = sunbro.FindByID('expiration-date')
    creditsPackWrapper = sunbro.FindByClass('creditPacksWrapper')

    def buy_credits(self, credits):
        self.value.send_keys(credits)
        self.submit.click()

    def go(self):
        self._driver.get(conf.SSL_URL + "/yapesos")

    def click_fixed_credits_button(self, amount):
        creditsPack_button = self.creditsPackWrapper.find_element_by_id('creditPack-{}'.format(amount))
        creditsPack_button.click()
