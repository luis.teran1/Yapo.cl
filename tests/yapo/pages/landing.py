import sunbro, time
from yapo import pages
from lazy import lazy
from selenium.webdriver.common.action_chains import ActionChains

class Landing(pages.BasePage):

    def check_landing_url(self, s, wd):
        self.wait.until_loaded()
        s.assertIn("/landing", wd.current_url)
    
    def check_pagos_url(self, s, wd):
        self.wait.until_loaded()
        s.assertIn("/pagos", wd.current_url)

