# -*- coding: latin-1 -*-
from yapo.pages.generic import SentEmail
import sunbro


class AdReplyEmail(SentEmail):

    def get_abuse_link(self):
        self.go()
        return self.body.find_element_by_partial_link_text("Denuncia aqu").get_attribute("href")

    def report_abuse(self):
        link = self.get_abuse_link()
        self._driver.get(link)
        title = self._driver.find_element_by_css_selector('h1').text
        return title ==  'Tu denuncia fue enviada, �gracias!'


class PartnerReportAdReplyEmail(SentEmail):

    partner = sunbro.FindByID('partner')


class PartnerReportMonthlyEmail(SentEmail):

    inserted = sunbro.FindByID('inserted')
    deleted = sunbro.FindByID('deleted')
    partner = sunbro.FindByID('partner')


class PartnerReportActiveEmail(SentEmail):

    active = sunbro.FindByID('active')
    partner = sunbro.FindByID('partner')

class CreditsMail(SentEmail):

    YOU_HAVE_NOW = 'Hoy tienes'
    YOU_BOUGHT = 'Compraste'
    YOU_USED = 'Usaste'
    YOU_HAD = 'Ten�as'

