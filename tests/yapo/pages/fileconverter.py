from yapo.pages import BasePage
from yapo import utils
from yapo.pages.mockserver import ServerRunner 
from yapo.pages.control_panel import ControlPanel
import sunbro
import threading
import os
import yapo

class FileConverter(BasePage):

    file_separator = sunbro.FindByID('file_separator')
    tabConv = sunbro.FindByID('tabConv')
    uploadfile = sunbro.FindByID('UploadFile')
    submit = sunbro.FindByID('partner_submit')

    def go(self, wd, user):
        cp = ControlPanel(wd)
        cp.go()
        cp.login(user, user)
        cp.go_to_option_in_menu("Convertir archivo a XML")

    def exit(self, wd):
        ControlPanel(wd).logout()

    def start_server(self):
        self.server = ServerRunner()
        threading.Thread(target=self.server.run).start()

    def get_result(self):
        with open("server.got") as f:
            return f.read()

    def convert(self, sep, ftu, tab=False):
        if not tab:
            self.file_separator.send_keys(sep)
        os.getcwd()
        self.uploadfile.send_keys(os.path.join(yapo.conf.RESOURCES_DIR, ftu))
        self.submit.click()
