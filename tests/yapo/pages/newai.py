# -*- coding: latin-1 -*-
from yapo import conf
from yapo.pages import BasePage
from yapo.pages.ad_insert import AdInsertResult
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import os, time
import nose_selenium

from yapo.pages.defaults import DEFAULT_AD_INFO
from yapo.pages.defaults import CATEGORY_PARAMS
from yapo.pages.defaults import DEFAULT_USER_INFORMATION
from yapo.pages.defaults import DEFAULT_GEO_INFO

class DesktopAI(BasePage):

    def click_forgot_pass(self):
        self._driver.find_element_by_partial_link_text("�La olvidaste?").click()

    def go(self, **kwargs):
        if not kwargs.get('emode', False):
            self._driver.get(conf.DESKTOP_URL_04+'/ai')
        #TODO wait

    def clean(self, **kwargs):
        inputs = self._driver.find_elements_by_css_selector('[name]')
        for input in inputs:
            if input.get_attribute('name') in kwargs:
                clear_param(input)

    def fill_ad_part(self, **kwargs):
        """ fill ad information params """
        
        skip_this = []
        # First select the category this calls an ajax so wait for it
        cat = kwargs.get('category', DEFAULT_AD_INFO['category'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('[name="category_group"]'), cat)
        if 'category' in kwargs:
            skip_this.append('category')
            kwargs.pop('category')
        WebDriverWait(self._driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#category_contents[data-cat="{0}"]'.format(cat))))

        # inmo
        if int(cat) > 1000 and int(cat) < 2000:
            val = kwargs.get('estate_type', DEFAULT_AD_INFO['estate_type'])
            fill_param(self._driver, self._driver.find_element_by_css_selector('[name="estate_type"]'), val)
            if 'estate_type' in kwargs:
                skip_this.append('estate_type')
                kwargs.pop('estate_type')
            # this is penca, the problem is that there is nothing I could wait to tell that additional params have been loaded
            time.sleep(1)

        # motos
        if int(cat) == 2060:
            val = kwargs.get('plates', CATEGORY_PARAMS[2060]['plates'])
            fill_param(self._driver, self._driver.find_element_by_css_selector('[name="plates"]'), val)
            skip_this.append('plates')
            if 'plates' in kwargs:
                kwargs.pop('plates')

        # calzado
        if int(cat) == 4080:
            val = kwargs.get('footwear_gender', DEFAULT_AD_INFO['footwear_gender'])
            fill_param(self._driver, self._driver.find_element_by_css_selector('[name="footwear_gender"]'), val)
            if 'footwear_gender' in kwargs:
                skip_this.append('footwear_gender')
                kwargs.pop('footwear_gender')
            # this is penca, the problem is that there is nothing I could wait to tell that additional params have been loaded
            time.sleep(1)
            val = kwargs.get('footwear_type', DEFAULT_AD_INFO['footwear_type'])
            for v in val:
                fill_param(self._driver, self._driver.find_element_by_css_selector('[name="footwear_type[{0}]"]'.format(v)))
            if 'footwear_type' in kwargs:
                kwargs.pop('footwear_type')

        # ofertas de empleo
        if int(cat) > 7000 and int(cat) < 7060:
            val = kwargs.get('jc', DEFAULT_AD_INFO['jc'])
            for v in val:
                fill_param(self._driver, self._driver.find_element_by_css_selector('[name="jc[{0}]"]'.format(v)))
            if 'jc' in kwargs:
                kwargs.pop('jc')
        if int(cat) == 7060:
            val = kwargs.get('sct', DEFAULT_AD_INFO['sct'])
            for v in val:
                fill_param(self._driver, self._driver.find_element_by_css_selector('[name="sct[{0}]"]'.format(v)))
            if 'sct' in kwargs:
                kwargs.pop('sct')

        # type parameter
        if 'type' in kwargs:
            skip_this.append('type')
            fill_param(self._driver, self._driver.find_element_by_css_selector('[name="type"]'), kwargs.pop('type'))
        elif 'type' in DEFAULT_AD_INFO:
            from selenium.common.exceptions import NoSuchElementException
            try:
                self._driver.implicitly_wait(0)
                type = self._driver.find_element_by_css_selector('[name="type"]')
            except NoSuchElementException:
                self._driver.implicitly_wait(nose_selenium.TIMEOUT)
                print('Category {0} has not "type" parameter'.format(cat))
            else:
                self._driver.implicitly_wait(nose_selenium.TIMEOUT)
                skip_this.append('type')
                fill_param(self._driver, type, DEFAULT_AD_INFO.pop('type'))
        # Fill the rest of ad params
        fill_all(self._driver, '.da-information:nth-child(2) [name]:not([type="submit"])', DEFAULT_AD_INFO, skip_this, **kwargs)

    def fill_geo_part(self, **kwargs):
        """ """
        # Region!
        val = kwargs.get('region', DEFAULT_GEO_INFO['region'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('[name="region"]'), val)
        if 'region' in kwargs:
            kwargs.pop('region')
        WebDriverWait(self._driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#communes[data-region="{0}"]'.format(val))))

        # Commune!
        val = kwargs.get('communes', DEFAULT_GEO_INFO['communes'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('[name="communes"]'), val)

        # Set AD location?
        val = kwargs.get('add_location', DEFAULT_GEO_INFO['add_location'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('[name="add_location"]'), val)

        if 'address' in kwargs:
            fill_param(self._driver, self._driver.find_element_by_css_selector('[name="address"]'), kwargs['address'])
            if 'address_number' in kwargs:
                fill_param(self._driver, self._driver.find_element_by_css_selector('[name="address_number"]'), kwargs['address_number'])
            if 'show_location' in kwargs:
                fill_param(self._driver, self._driver.find_element_by_id('show_location'), True)
                time.sleep(1)

    def fill_ups_part(self, **kwargs):
        """ """

    def fill_user_part(self, **kwargs):
        """ fill user information params """

        skip_this = []
        if 'logged' in kwargs and kwargs.get('logged', False):
            return

        # Creat account? 
        val = kwargs.get('ai_create_account', DEFAULT_USER_INFORMATION['ai_create_account'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('[name="ai_create_account"]'), val)
        skip_this.append('ai_create_account')
        if 'ai_create_account' in kwargs:
            kwargs.pop('ai_create_account')

        # Company AD?
        val = kwargs.get('company_ad', DEFAULT_USER_INFORMATION['company_ad'])
        fill_param(self._driver, self._driver.find_element_by_css_selector('.your_information .line.da-tr [name="company_ad"][value="{0}"]'.format(int(val))))
        skip_this.append('company_ad')
        if 'company_ad' in kwargs:
            kwargs.pop('company_ad')

        # phone type
        val = kwargs.get('phone_type', DEFAULT_USER_INFORMATION['phone_type'])
        ptype = 'Home'
        if val == 'm':
            ptype = 'Mobile';
        fill_param(self._driver, self._driver.find_element_by_css_selector('[for="phoneInputType{0}"]'.format(ptype)))
        skip_this.append('phone_type')
        if 'phone_type' in kwargs:
            kwargs.pop('phone_type')
        
        # Fill the rest of user params
        fill_all(self._driver, '.your_information .line.da-tr [name]', DEFAULT_USER_INFORMATION, skip_this, **kwargs)
    
    def accept_conditions(self, **kwargs):
        """ """
        if kwargs.get('accept_conditions', True):
            self._driver.find_element_by_css_selector('[for="accept_conditions"]').click()

    def continue_with(self, **kwargs):
        """ """
        # Espera a que terminen de cargar las imagenes
        ts = 0.0
        while ('images' in kwargs or 'file' in kwargs) and len(self._driver.find_elements_by_css_selector('.ai-upload-img img[src="/img/spin.gif"]')) > 0 and ts < 10.0:
            print("uploading images"+str(ts))
            ts = ts + 0.25
            time.sleep(0.25)
        if kwargs.get('Preview', False):
            self._driver.find_element_by_id('submit_preview').click()
        elif kwargs.get('Submit', True):
            self._driver.find_element_by_id('submit_create_now').click()
            return AdInsertResult(self._driver)
        # DO NOTHING 

def clear_param(param):
    print('clear {0}({1}/{2})'.format(param.get_attribute('name'),param.tag_name, param.get_attribute('type')))
    if param.tag_name == 'input':
        if param.get_attribute('type') == 'text' and param.is_displayed():
            param.clear()

def fill_param(driver, param, val=None):
    pname = param.get_attribute('name')
    print('fill {0}({1}) with {2}'.format(pname,param.tag_name,val))
    if param.is_displayed() and param.is_enabled():
        if param.tag_name == 'select':
            Select(param).select_by_value(str(val))
        elif param.tag_name == 'input':
            type = param.get_attribute('type')
            if type == 'radio':
                checked = param.get_attribute('checked')
                if checked != 'checked':
                    param.click()
            elif type == 'checkbox':
                if val is not None:
                    if param.is_selected() != val:
                        param.click()
                else:
                    param.click()
            elif type == 'button':
                param.click()
            else: # probably text
                param.clear()
                param.send_keys(val)
        elif param.tag_name == 'textarea':
            param.clear()
            param.send_keys(val)
        elif param.tag_name == 'label':
            param.click()
        else:
            print('Unknow tag {0}'.format(param.tag_name))
        # for this it should wait
        if pname == 'brand' and val != '0':
            print(pname)
            print(val)
            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, '#model[data-brand="{0}"]'.format(val))))
    elif param.tag_name == 'input':
        type = param.get_attribute('type')
        if type == 'file':
            # Special IMAGES case
            for img in val:
                param = driver.find_element_by_css_selector('input[name=file]')
                param.send_keys(os.path.join(conf.RESOURCES_DIR, 'images', img))
        elif type == 'radio':
            # Frontend love invisible radios
            value = param.get_attribute('value')
            if val and value == val:
                param.click()
    else:
        print('{0} is not displayed'.format(pname))

def fill_all(driver, css_selector, defaults, skip, **kwargs):

    # this is the list of element that need to be filled
    need_to_fill = driver.find_elements_by_css_selector(css_selector)
    for param in need_to_fill:
        
        pname   = param.get_attribute('name')   # WebElement name 
        pval    = param.get_attribute('value')  # Current WebElement value 

        if pname in skip:
            continue
        try:
            value = kwargs[pname] if pname in kwargs else defaults[pname]
            fill_param(driver, param, value)
        except KeyError:
            print('No me pasaron "{0}", y tampoco tengo default, asumire que no es necesario"'.format(pname))

