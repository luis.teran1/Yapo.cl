# -*- coding: latin-1 -*-
from yapo.geography import Regions
from yapo.pages import BasePage
import nose_selenium

class AdList(BasePage):

    """ string name of the atribute that points to the object you issue search commands to """
    search_form = None

    def search(self, ordered_args):
        def extract(arg):
            for i, (k, v) in enumerate(ordered_args):
                if k == arg:
                    return (ordered_args[:i] + ordered_args[i+1:], ordered_args[i][1])
            return (ordered_args, None)
        ordered_args, region = extract('region');
        self.go(args = Regions.simple_name(region) if region else None)

        search = getattr(self, self.search_form) if self.search_form else self
        search.ordered_fill_form(ordered_args)
        search.search_button.click()

    def get_all_ads(self):
        ads = {ad['id']: ad for ad in map(lambda ad: self.get_ad_data(ad=ad), self.ads_list)}
        return ads

    @staticmethod
    def elem_text(elem):
        return elem.text

    @staticmethod
    def elem_id(elem):
        return elem.get_attribute('id')

    @staticmethod
    def find_ad_prop(data, ad, target, selector, on_success, default=None):
        try:
            elem = ad
            if selector:
                elem = ad.find_element_by_css_selector(selector)
            data[target] = on_success(elem)
        except:
            if default is not None:
                data[target] = default

    def get_ad_data(self, ad_id=None, ad=None, shadow=False):
        if not ad:
            if not ad_id:
                raise ValueError('Neither ad nor ad_id was provided')
            try:
                self._driver.implicitly_wait(0)
                if shadow:
                    # we get listing item from shadowRoot
                    ad = self.select_shadow_element_by_css_selector("listing-item[ad-id='{}']".format(ad_id), 'firstElementChild')
                else:
                    ad = self._driver.find_element_by_id(ad_id)
            except:
                self._driver.implicitly_wait(nose_selenium.TIMEOUT)
                return {}

        self._driver.implicitly_wait(0)
        data = self._get_ad_data(ad)
        self._driver.implicitly_wait(nose_selenium.TIMEOUT)

        return data

