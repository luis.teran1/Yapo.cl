import sunbro
from yapo import pages
from lazy import lazy
from selenium.webdriver.common.action_chains import ActionChains

class FillableComponent(pages.BasePage):

    fields = []

    def remove_args(self, arg_dict):
        for field in self.fields:
            if field in arg_dict:
                arg_dict.pop(field)

class Map(FillableComponent):

    """Represents a map"""

    fields = ["show_location"]

    # Buttons
    show_location = sunbro.FindByID("show_location")
    zoom_in = sunbro.FindByClass("leaflet-control-zoom-in")
    zoom_out = sunbro.FindByClass("leaflet-control-zoom-out")

    # Makers
    map_container = sunbro.FindByID("map_container")
    marker = sunbro.FindByCSS(".leaflet-marker-icon")
    marker_draggable = sunbro.FindByCSS(".leaflet-marker-draggable")

    # Images
    map_preview_active = sunbro.FindByCSS("#map_preview.is-bw")
    map_preview_inactive = sunbro.FindByCSS("#map_preview:not(.is-bw)")

    # Sad face image
    map_sad_face_image = sunbro.FindByCSS("#map_preview .icon-face-sad-circled")

    def fill_form(self, **kwargs):
        if "show_location" in kwargs and self.show_location.is_displayed():
            geovalue = lambda b: b.find_element_by_id('geoposition').get_attribute('value')
            geoposition_value = geovalue(self._driver)
            self.show_location.click()
            try:
                self.wait.until(lambda b: not map_preview_active.is_displayed())
            except:
                if not self.is_element_present('map_sad_face_image'):
                    raise

            if not self.is_element_present('map_sad_face_image'):
                self.wait.until(lambda b: geoposition_value != geovalue(b))

class MapPreview(pages.BasePage):

    """Represents the map preview of adview"""

    # Divs
    map_module = sunbro.FindByID("da_view_map")
    map_preview = sunbro.FindByID("map_container")
    map_modal = sunbro.FindByID("main_map_modal")
    map_info = sunbro.FindByID("map_info")
    map_view_hover = sunbro.FindByID("da_view_map_hover")

    # values
    map_zoom_level = sunbro.FindByID("map_zoom_level")
    address = sunbro.FindByCSS("#map_info address")
    region_commune = sunbro.FindByCSS("#da_view_map .map-region span")
    circle_marker = sunbro.FindByID("da_view_map_circle")
    precise_marker = sunbro.FindByID("da_view_map_marker")

    def is_present(self):
        return all(map(self.is_element_present, ['map_module', 'map_preview', 'map_info']))

    def precise(self):
        return (self.is_element_present('precise_marker') and
                not self.is_element_present('circle_marker'))

    def open_modal(self):
        hov = ActionChains(self._driver).move_to_element(self.map_module)
        hov.perform()
        self.map_view_hover.click()

class MapPreviewMobile(pages.BasePage):

    """Represents the map preview of adview in mobile"""

    map_preview = sunbro.FindByID("map_container")

    def open_modal(self):
        self.map_preview.click()


class MapView(pages.BasePage):

    """Represents the map full view on adview"""

    # Divs
    modal_map_wrap = sunbro.FindByID("modal_map_wrap")
    modal_map_footer_mobile = sunbro.FindByID("map_page_footer")
    region_commune_mobile = sunbro.FindByCSS("#map_page_footer .commune")
    modal_map_footer = sunbro.FindByID("modal_map_footer")
    region_commune = sunbro.FindByCSS("#modal_map_footer .map-region span")

    def is_present(self):
        return all(map(self.is_element_present, ['modal_map_wrap']))

    def precise(self):
        if self.modal_map_wrap.has_attribute('data-precise'):
            return bool(self.modal_map_wrap.get_attribute('data-precise'))
        return False

    def get_commune(self, is_mobile = False):
        if is_mobile:
            return self.region_commune_mobile.get_attribute('innerHTML')
        return self.region_commune.get_attribute('innerHTML')


class MapForm(FillableComponent):

    """Represents the map form"""

    # What fields do I need
    fields = ['region', 'communes', 'add_location', 'approx', 'exact', 'address', 'address_number']
    order = ['region', 'communes', 'add_location', 'address', 'address_number']

    # Selects
    region = sunbro.FindByID("region")
    communes = sunbro.FindByID("communes")

    # Labels
    location_title = sunbro.FindByID("aiform_location_title")
    add_location_label = sunbro.FindByCSS("label[for=add_location]")
    address_ok = sunbro.FindByID("address_ok")
    address_nok = sunbro.FindByID("address_nok")

    # Checkboxes
    add_location = sunbro.FindByID("add_location")
    add_location_disabled = sunbro.FindByCSS("#add_location:disabled")
    approx_area = sunbro.FindByID("approx")
    exact_area = sunbro.FindByID("exact")

    # Help buttons
    maps_help = sunbro.FindByID("maps_help")

    # Inputs
    address = sunbro.FindByID("address")
    address_number = sunbro.FindByID("address_number")
    geoposition = sunbro.FindByID("geoposition")

    # Messages
    map_error_provider = sunbro.FindByID("map_error_provider")
    map_error_requests_exceeded = sunbro.FindByID("map_error_requests_exceeded")
    err_communes = sunbro.FindByID("err_communes")

    # Autocomplete
    autocomplete_list = sunbro.FindByCSS("ul.ui-autocomplete")

    def get_suggestions(self):
        return [item.text for item in self._driver.find_elements_by_css_selector("ul.ui-autocomplete li a")]

    @lazy
    def map(self):
        """ return a Map element """
        return Map(self._driver)

    @lazy
    def map_preview(self):
        """ return a MapPreview element """
        return MapPreview(self._driver)

    def fill_form(self, **kwargs):
        data = [(key, kwargs.pop(key)) for key in self.order if key in kwargs]
        super().ordered_fill_form(data)
