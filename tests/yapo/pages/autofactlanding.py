from yapo.pages import BasePage
from yapo import conf
import sunbro


class AutofactLandingForm(BasePage):

    """Autofact Landing page"""
    AdSubject = sunbro.FindByClass('landingAutofactHeadDainfo-subject')
    BuyButton = sunbro.FindByClass('landingAutofactHeadDainfo-btnBuy')
    TermsConditionsButton = sunbro.FindByClass('landingAutofactHeadDainfo-tc')

    AdSubjectText = "Suzuki Maruti 800cc 2003"

    def go(self, list_id):
        self._driver.get(conf.PHP_URL + "/informe-autofact?id={}".format(list_id))

