from yapo import conf

class UserNotFoundInLogError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class EntryNotFoundInLogError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

class BannersAdminLog:

    path = conf.REGRESS_FINAL+"/logs/cpanel_banners.log"
    
    def clear(self):
        f = open(self.path, 'r+')
        f.truncate()
        f.close()

    def _remove_extra(self, line, action):
        if action == 'turn_on':
            return line.split(' 1- ', 1)[1]
        else:
            return line.split(' 0- ', 1)[1]

    def search_for(self, user, action, these):
        name_found = False
        f = open(self.path)
        must_find = len(these)
        for line in f.readlines():
            if name_found:
                if must_find > 0:
                    if self._remove_extra(line.rstrip(), action) in these:
                        must_find -= 1
            elif user+" - <"+action+">" in line.rstrip():
                name_found = True
        f.close()
        if not name_found:
            raise UserNotFoundInLogError("'"+user+" - <"+action+">' was not found in log file")
        if must_find > 0:
            raise EntryNotFoundInLogError("Not all banners ({}) were found in log, {} were missing".format(these, must_find))
