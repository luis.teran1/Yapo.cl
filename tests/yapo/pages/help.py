from yapo.pages import BasePage
from yapo import conf
import sunbro


class FAQItem(BasePage):
    head = sunbro.FindByCSS('h3')
    body = sunbro.FindByCSS('.section_box')
    table = sunbro.FindByCSS('.faq-table')
    faq_answer = sunbro.FindAllByCSS('.section_box.type1.answer[style="display: block;"] p')
    suggestions = sunbro.FindByCSS('#ct div.content h3')

class Help(BasePage):

    SELLERS = 1
    BUYERS = 2

    terms_products = sunbro.FindAllByCSS('h3:not([class])')
    faq_last_seller_head = sunbro.FindByID('sellers_head_50') # hermoso
    faq_last_seller_body = sunbro.FindByID('sellers_body_50') # magnifico
    faq_last_seller_link = sunbro.FindByID('sellers_link_50') # programming magnum opus 
    
    support_input_name = sunbro.FindByID('name')
    support_input_email = sunbro.FindByID('email')
    support_input_support_body = sunbro.FindByID('support_body')
    support_button_send = sunbro.FindByCSS(".input_submit")

    field_fmt = "{0}_{1}_{2}"

    # for where am i function
    rel_url = '/ayuda/preguntas_frecuentes.html'

    def get_faq(self, type, num):
        if type == Help.SELLERS:
            item = self._driver.find_element_by_css_selector('.group_question.vendedores .item:nth-child({0})'.format(num))
        elif type == Help.BUYERS:
            item = self._driver.find_element_by_css_selector('.group_question.compradores .item:nth-child({0})'.format(num))
        else:
            raise Exception("What're you talking about?? Help.SELLERS or Help.BUYERS, whats the deal??")
        return FAQItem(item)

    def go_faq(self, type=None, num=None):
        if type and num:
            self._driver.get(conf.DESKTOP_URL + "/ayuda/preguntas_frecuentes.html#{0}_head_{1}".format(type, num))
        else:
            self._driver.get(conf.DESKTOP_URL + "/ayuda/preguntas_frecuentes.html")

    def go_terms(self):
        self._driver.get(conf.DESKTOP_URL + "/ayuda/terminos.html")

    def go_rules(self):
        self._driver.get(conf.DESKTOP_URL + "/ayuda/reglas.html")

    def go(self, page=None):
        slug = page if page else 'preguntas_frecuentes'
        url = conf.DESKTOP_URL + '/ayuda/{}.html'.format(slug)
        self._driver.get(url)
        self.wait.until_loaded()
        return self

    def go_report_an_announcer(self):
        url = conf.PHP_URL + "/support/form/0?ca=15_s&id=4"
        self._driver.get(url)
        self.wait.until_loaded()


class Rules(BasePage):

    def get_rule_title(self, rule_id):
        return self._driver.find_element_by_id('rule_title_{0}'.format(rule_id))

    def get_rule_item_by_id(self, rule_id):
        item =  self._driver.find_element_by_id('rule_{0}'.format(rule_id))
        return RuleItem(item)


class RuleItem(BasePage):
    top_link = sunbro.FindByCSS("a[href='#TOP']")
