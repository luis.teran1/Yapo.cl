# -*- coding: latin-1 -*-
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import StaleElementReferenceException
from selenium.selenium import selenium
from yapo.wait import PageWait
from yapo.component import Form
import selenium.webdriver.support.expected_conditions as EC
import contextlib
import logging
import nose_selenium
import sunbro


logger = logging.getLogger(__name__)


class BasePage(sunbro.Page, Form):
    """Base page object"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._wait = PageWait(self, 5)
        # For backwards compatibility
        self.wait = self._wait

        # Allow page object to include others {varname -> class}
        if hasattr(self, "members"):
            for var, cls in self.members.items():
                setattr(self, var, cls(self._driver))

    def go(self):
        raise NotImplementedError()

    def brutally_click(self, selector):
        script = '$("{}").click()'.format(selector)
        self._driver.execute_script(script)

    def is_element_present(self, element_name, no_wait = False):
        """ check if the element is present.
        usage: `page.is_element_present('element_name')`
        """
        if no_wait: self._driver.implicitly_wait(0)
        try:
            getattr(self, element_name)
        except NoSuchElementException:
            return False
        finally:
            if no_wait: self._driver.implicitly_wait(nose_selenium.TIMEOUT)
        return True

    def get_error_for(self, element):
        return self._driver.find_element_by_id('err_{}'.format(element))

    @contextlib.contextmanager
    def switch_to_frame(self, frame):
        """Context processor that changes the active selenium element to the frame.
        see for more info https://readthedocs.org/docs/selenium-python/api.html?highlight=switch%20frame#selenium.webdriver.remote.webdriver.WebDriver.switch_to_frame
        """
        self._driver.switch_to_frame(frame)
        yield frame
        self._driver.switch_to_default_content()

    @contextlib.contextmanager
    def wait_for_new_window(driver, timeout=10):
        handles_before = driver.window_handles
        yield
        WebDriverWait(driver, timeout).until(
            lambda driver: len(handles_before) < len(driver.window_handles))

    def get_table_data(self, table_name=None, table_elem=None):
        """ Get the cell values/text in a table """
        table_values = []
        if not table_elem:
            if not table_name:
                raise Exception("What u doing??? set a name or give an element please")
            table_elem = getattr(self, table_name)
        for row in table_elem.find_elements_by_tag_name('tr'):
            row_values = []
            for cell in row.find_elements_by_tag_name('td'):
                row_values.append(cell.text)
            table_values.append(row_values)
        return table_values

    def get_displayed_table_data(self, table_name=None, table_elem=None):
        """ Get the visible cell values/text in a table """
        table_values = []
        if not table_elem:
            if not table_name:
                raise Exception("What u doing??? set a name or give an element please")
            table_elem = getattr(self, table_name)
        for row in table_elem.find_elements_by_css_selector('.productTable-row'):
            if row.is_displayed():
                row_values = []
                for cell in row.find_elements_by_tag_name('td'):
                    try:
                        if cell.is_displayed():
                            row_values.append(cell.text)
                    except StaleElementReferenceException:
                        print("Cell has changed")
                        pass
                table_values.append(row_values)
        return table_values

    def fire_event(self, css_selector, event):
        """ Fires/Trigger an event with jquery for a specific css selector """
        self._driver.execute_script('$("{0}").{1}();'.format(css_selector, event))

    def select_by_value(self, field, value):
        element = getattr(self, field)
        if element.tag_name == 'select':
            select_element = Select(element)
            select_element.select_by_value(value)
        else:
            return 'Field is non select element'

    def get_first_selected_option(self, field):
        element = getattr(self, field)
        if element.tag_name == 'select':
            select_element = Select(element)
            return select_element.first_selected_option
        else:
            return 'Field is non select element'

    def get_all_options_text(self, field):
        element = getattr(self, field)
        if element.tag_name == 'select':
            select_element = Select(element)
            return [option.text for option in select_element.options]
        else:
            return 'Field is non select element'

    def am_i_here(self):
        if hasattr(self, 'rel_url'):
            return self.rel_url in self._driver.current_url
        print("I don't know where I am :)")
        return False

    def scroll_to_bottom(self):
        self._driver.execute_script('window.scrollTo(0, document.body.scrollHeight);')

    def scroll_to_element(self, element):
        self._driver.execute_script('window.scrollTo({x}, {y})'.format(**element.location))

    def select_shadow_element_by_css_selector(self, selector, type_element='lastElementChild'):
        try:
            running_script = 'return document.querySelector("{}").shadowRoot.{}'.format(selector, type_element)
            element = self._driver.execute_script(running_script)
            return element
        except:
            raise NoSuchElementException('element {} was not found'.format(selector))
    
    def select_all_shadow_element_by_css_selector(self, selector, type_element='lastElementChild'):
        try:
            running_script = "return [...(document.querySelectorAll('{}'))].map(item => item.shadowRoot.{})".format(selector, type_element)
            elements = self._driver.execute_script(running_script)
            return elements
        except:
            raise NoSuchElementException('element {} was not found'.format(selector))



class Wait(WebDriverWait):
    """Wait class with extra methods"""

    def until_present(self, selector, message="Element didn't appear"):
        """Wait until an element that matches `selector`"""
        condition = EC.presence_of_element_located(selector)
        return self.until(condition, message)

    def until_visible(self, selector, message="Element didn't appear"):
        """Wait until an element that matches `selector` css selector appears"""
        condition = EC.visibility_of_element_located(selector)
        return self.until(condition, message)

    def until_text_present(self, text, element, message=None):
        if not message:
            message = 'Expected text "{0}" did not appear in element'.format(text)

        def present_and_has_text(wd):
            try:
                text in element.text
            except NoSuchElementException:
                pass
        return self.until(present_and_has_text)

class AdForm(BasePage):
    """ Form for insert and edit ad """

    def search_location(self, **kwargs):
        self.map_form.fill_form(**kwargs)
        self.map_form.remove_args(kwargs)

        self.map_form.map.fill_form(**kwargs)
        self.map_form.map.remove_args(kwargs)

        return kwargs


class RedirectedAway(Exception):
    pass


class HotizontalSelect(BasePage):
    def __init__(self, *args, **kwargs):
        self.form_selector = kwargs.pop('form_selector', None)
        self.parent_page = kwargs.pop('parent_page', None)
        super(HotizontalSelect, self).__init__(*args, **kwargs)

    def fill_sidenav(self, params, trigger = None, filled = []):
        """ Finds current location, fills corresponding adparam, pops it and recurses until exhaustion """
        adparam = trigger
        where = self.where_am_i()

        if where:
            # We need to skip 'hsp-' token
            adparam = where[4:]

        if adparam in params:
            self.select(adparam, params.pop(adparam))
            return self.fill_sidenav(params, filled = filled + [adparam])

        self.close_sidenav()
        return filled

    def where_am_i(self):
        """ Returns where in the sidenav am I, or empty string if nowhere """
        self.form = self._driver.find_element_by_css_selector(self.form_selector)
        return self.form.get_attribute('data-where')

    def should_redirect(self):
        """ Returns where in the sidenav am I, or empty string if nowhere """
        self.form = self._driver.find_element_by_css_selector(self.form_selector)
        return self.form.get_attribute('data-should-redirect') == 'true'

    def close_sidenav(self):
        """ If on any sidenav page, hit the X and get out of there """
        where = self.where_am_i()
        should_redirect = self.should_redirect()

        if where:
            base_url = self._driver.current_url
            self._driver.find_element_by_css_selector('#{} .close'.format(where)).click()

            def to_base_url(url):
                parts = url.split('#')
                start = parts[0].split('?')
                return start[0]

            def url_changed(_):
                u1 = to_base_url(self._driver.current_url)
                u2 = to_base_url(base_url)
                return u1 != u2

            def closed(_):
                w1 = self.where_am_i()
                w2 = where
                return w1 != w2

            if should_redirect:
                # This case is executed when forced login is enabled for the selected category
                self.wait.until(url_changed)
                raise RedirectedAway
            else:
                self.wait.until(closed)

    def select(self, adparam, value):
        """ Fills and adparam and asserts it was correctly filled """
        trigger = self._driver.find_element_by_css_selector('#hst-{} a'.format(adparam))
        form = self._driver.find_element_by_id('hsp-'+adparam)
        input = self._driver.find_element_by_id(adparam)
        form_str = 'hsp-'+adparam

        # If I'm not there, I may need to hit the trigger first
        if self.where_am_i() != form_str:
            print('hitting the trigger [hst-{}] from [{}]'.format(adparam, self.where_am_i()))
            trigger = self._driver.find_element_by_css_selector('#hst-{} a'.format(adparam))
            self.wait.until_visibility_of_element(trigger)
            if not trigger.is_displayed():
                raise BaseException("Can't set {}. Trigger not displayed".format(adparam))

            # Ugly hack to allow selecting category again :/
            if hasattr(self, 'more_images_x') and self.more_images_x.is_displayed():
                self.more_images_x.click()

            # force click with js because sometimes is not execute
            self._driver.execute_script('return document.querySelector("#hst-{} a").click()'.format(adparam))
            self.wait.until(lambda x: self.where_am_i() == form_str, 'Waiting to get out of [{}] to [{}]'.format(self.where_am_i(), form_str))

        # Multivalued fields require special treatment
        parent_page_has_multivalued = self.parent_page and hasattr(self.parent_page, 'multivalued')
        script_to_click = ('const element = document.querySelector("#{}"); return (element) ? element.click() ||  true : false;')
        if parent_page_has_multivalued and form_str[4:] in self.parent_page.multivalued:
            for val in value:
                choice = '{}-{}'.format(adparam, val)
                clicked = self._driver.execute_script(script_to_click.format(choice, choice))
                if not clicked:
                    form.find_element_by_id(choice).click()
            form.find_element_by_class_name('close').click()
        else:
            choice = '{}-{}'.format(adparam, value)
            clicked = self._driver.execute_script(script_to_click.format(choice, choice))
            if not clicked:
                form.find_element_by_id(choice).click()

        # Ensure that we moved to the next page
        self.wait.until(lambda x: self.where_am_i() != form_str, 'Waiting to get to [{}] from [{}]'.format(form_str, self.where_am_i()))

        # Did we got the right value all the way to the input?
        if type(value) is list:
            selected = Select(input).all_selected_options
            values = sorted(map(lambda e: e.get_attribute('value'), selected))
            print('{}: {} (expected: {})'.format(adparam, values, sorted(value)))
            assert values == sorted(value)
        else:
            print('{}: {}'.format(adparam, input.get_attribute('value')))
            assert input.get_attribute('value') == str(value)
