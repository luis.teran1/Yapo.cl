from selenium.webdriver.common.by import By
from yapo import pages, conf
from yapo import conf
import sunbro
from yapo.utils import make
from yapo.pages.ad_insert import AdInsert

class MetaTags(pages.BasePage):
    """ Describes the meta tags of a page """

    meta = sunbro.FindAllByCSS('meta')
    og_tags = sunbro.FindAllByCSS('meta[property="og:*"]')
    og_imgs = sunbro.FindAllByCSS('meta[property="og:image*"]')

    def get_og_tags(self):
        """Returns og tags from a page"""
        tags = {}
        img = []
        img_m = []
        for m in self.meta :
            attr = m.get_attribute('property')
            content = m.get_attribute('content')
            if attr:
                if attr not in ['og:image', 'og:image:width', 'og:image:height']:
                    tags.update({attr: content})
                else:
                    img_m.append(content)
                    if len(img_m) == 3 :
                        img.append(img_m)
                        img_m = []

        og_tags = [tags, img]
        return og_tags

class AccountBar(pages.BasePage):

    """ this class describes the account bar """
    me = sunbro.FindByCSS('account-bar')
    left_menu = sunbro.FindByID('dropleft-menu-div')
    buttons = sunbro.FindByCSS('container-login')
    login_link = sunbro.FindByID('login-account-link')
    create_account = sunbro.FindByCSS('create-account')

    input_email = sunbro.FindByCSS('.pm-body [name=accbar_email]')
    input_pass = sunbro.FindByCSS('.pm-body [name=accbar_password]')
    login_btn = sunbro.FindByCSS('.pm-body #acc_login_submit_button')

    message = sunbro.FindByCSS('.header-userInfo')
    error_message = sunbro.FindByCSS('.pm-body .loginFeedback.__error[style="display: inline;"]')

    logout = sunbro.FindByCSS('.header-userLogout')

    def login(self, email, password):
        self.login_link.click()
        self.input_email.clear()
        self.input_email.send_keys(email)
        self.input_pass.clear()
        self.input_pass.send_keys(password)
        self.login_btn.click()
        self.wait.until_session_cookie()


class SentEmail(pages.BasePage):

    """ Just a class to access html versions of the email that we send.
        You should not create your stuff here, please leave it generic. """

    body = sunbro.FindByTag('body')
    products_table = sunbro.FindByID('products-table')
    link = sunbro.FindByTag('a')

    def go(self, old=None):

        url = conf.PHP_URL + '/selenium-core/getMail.php?htmlmail=1'
        if old is not None:
            url = url + '&filepath=' + \
                conf.REGRESS_FINAL + '/logs/mail.txt.old'
        self._driver.get(url)
        self.wait.until_loaded()

    def go_to_link(self, old=None):
        self.go(old)
        self.wait.until_visible('link')
        self.link.click()

    def get_html(self):
        self.go()
        return self._driver.find_element_by_tag_name('body').get_attribute('innerHTML')

    def get_body_text(self):
        self.go()
        self.wait.until_loaded()
        return self.body.text

class AdReplyUpdateStats(pages.BasePage):

    """ Class to access to the adreply_update_stats.html template, that is used to count adreplies """

    def go(self, list_id=None):
        if (list_id is None):
            url = conf.DESKTOP_URL + '/adreply_update_stats.html'
        else:
            url = conf.DESKTOP_URL + \
                '/adreply_update_stats.html?list_id=' + list_id
        self._driver.get(url)


class TechnicalError(pages.BasePage):

    title = sunbro.FindByCSS('.maintext h2')

    def generate_technical_error(self):
        try:
            make('trans-regress-stop')
            page = AdInsert(self._driver)
            page.go()
            page.insert_ad(subject='ropa de bebe',
                           body="ropa de bebe",
                           region=15,
                           category=3040,
                           price=999666,
                           accept_conditions=True)
            page.submit.click()
        finally:
            make('trans-regress-start')
