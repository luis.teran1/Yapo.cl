# -*- coding: latin-1 -*-
import sunbro
from yapo import pages, conf, utils

def direct_go(wd, list_id, freq, num_days):
    wd.get('{0}/pagos?prod=10&id={1}&ab_freq={2}&ab_num_days={3}'.format(conf.SSL_URL, list_id, freq, num_days))

def flush_autobump():
    utils.trans('flush_autobump')
    utils.rebuild_index()

class Frequency(pages.BasePage):

    freq_combo = sunbro.FindByCSS('.selectize-input.items.not-full.has-options [placeholder="Horas"]')
    days_combo = sunbro.FindByCSS('.selectize-input.items.not-full.has-options [placeholder="D�as"]')
    checkout = sunbro.FindByID('checkout')

    def do_select_freq(self, freq):
        self.wait.until_loaded()
        self.freq_combo.click()
        freq_options = self._driver.find_elements_by_css_selector('.bumpFrequency-selectorTime .option')
        for opt in freq_options:
            if opt.get_attribute("data-value") == str(freq):
                opt.click()
                break

    def do_select_days(self, num_days):
        self.days_combo.click()
        day_options = self._driver.find_elements_by_css_selector('.bumpFrequency-selectorDay .option')
        for opt in day_options:
            if opt.get_attribute("data-value") == str(num_days):
                opt.click()
                break

    def do_select(self, freq, num_days):
        self.do_select_freq(freq)
        self.do_select_days(num_days)

    def do_checkout(self):
        self.checkout.click()

    def manual_go(self, list_id):
        self._driver.get('{0}/frecuencia?list_id={1}'.format(conf.SSL_URL, list_id))

    def go_select_and_add(self, list_id, freq, num_days):
        self.manual_go(list_id)
        self.do_select(freq, num_days)
        self.do_checkout()
