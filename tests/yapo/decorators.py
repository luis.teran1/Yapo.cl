from nose.plugins.attrib import attr
import time
from functools import wraps
from yapo.mock_controller import MockController

def tags(*args):
    return attr(tag=args)

def mocked(func):
    @wraps(func)
    def mocked_func(self, driver):
        mock = MockController()
        mock.setStage(func.__name__)
        func(self, driver)
    return mocked_func

class retries_function_exception(object):
    """ Decorator for retries some function """
    def __init__(self, num_retries, time=0.5):
        self.num_retries = num_retries
        self.time = time

    def __call__(self, function):
        def retries_function(*args, **kwargs):
            for i in range(self.num_retries + 1):
                print("number intents %s" % i)
                try:
                    function(*args, **kwargs)
                except Exception as error:
                    if i == self.num_retries:
                        raise Exception(error)
                    time.sleep(self.time)
                    continue
                else:
                    break
        return retries_function
