# -*- coding: latin-1 -*-
from .yapo_page import YapoPage
import sunbro
from yapo import conf


class Login(YapoPage):

    errors = sunbro.FindAllByCSS('.login-errors')

    EMAIL_INVALID = 'El correo electrónico y/o la contraseña que ingresaste son incorrectos.'
    EMAIL_LOCK = 'Superaste la cantidad de intentos permitidos. Te enviamos un correo electrónico para que puedas cambiar tu contraseña.'
    _go_url = conf.DESKTOP_URL + '/inicio'

    email = sunbro.FindByCSS('input[type="email"]')
    password = sunbro.FindByCSS('input[type="password"]')
    submit = sunbro.FindByCSS('button[class="button __login"]')

    user_menu_header_mail = sunbro.FindByCSS('.userMenu-headerMail')

    def _check_request_error(self, expected_error):
        errors = self._getattr('errors')
        if expected_error and errors:
            text_error = [e.text for e in errors if len(e.text)].pop()
            print('error: {}'.format(text_error))
            return expected_error == text_error

    def _check_request_success(self, email):
        user_menu_header_mail = self._getattr('user_menu_header_mail')
        if user_menu_header_mail:
            return user_menu_header_mail.text == email

    def request(self, email, password, expected_error):
        self.email.clear()
        self.email.send_keys(email)
        self.password.clear()
        self.password.send_keys(password)
        self.submit.click()

        if self._check_request_success(email):
            return True

        if self._check_request_error(expected_error):
            return False

        raise Exception('Unknown state')
