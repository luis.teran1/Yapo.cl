from urllib.parse import urlencode
from yapo.pages import BasePage
from yapo import conf


class YapoPage(BasePage):

    """
        _go_url: Defines the base url that the YapoPage.go function will use.
    """
    _go_url = conf.DESKTOP_URL

    def _getattr(self, attr):
        """ Safely call getattr, returning None when it would have risen an exception """
        try:
            return getattr(self, attr)
        except:
            return None

    def go(self, *args, **kwargs):
        """ Goes to the page. Uses self._go_url and converts kwargs to a search query """
        url = self._go_url
        if args:
            route = '/'.join(args)
            if url[-1] != '/':
                url += '/'
            url += route
        if kwargs:
            query = urlencode(kwargs)
            url += '?' + query
        print(url)
        self._driver.get(url)
        return self
