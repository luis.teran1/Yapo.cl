# -*- coding: latin-1 -*-
from .yapo_page import YapoPage
import sunbro


class MyAds(YapoPage):

    error = sunbro.FindByCSS('.error')

    NO_MAIL_MSG = 'Debes insertar un email v�lido.'
    ERROR_MSG = 'No existen avisos activos para este e-mail: {}'

    def _check_request_error(self, email):
        error = self._getattr('error')
        if email == '' and error:
            print('my ads: {}'.format(error.text))
            return error.text == self.NO_MAIL_MSG
        if error:
            print('my ads: {}'.format(error.text))
            return error.text == self.ERROR_MSG.format(email)

    def _check_request_success(self):
        raise NotImplementedError()

    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()

        if self._check_request_success(email):
            return True

        if self._check_request_error(email):
            return False

        raise Exception('Unknown state')
