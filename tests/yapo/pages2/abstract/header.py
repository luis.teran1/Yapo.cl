# -*- coding: latin-1 -*-
from .yapo_page import YapoPage
import sunbro


class Header(YapoPage):
    user_menu_login = sunbro.FindByID('userMenuLogin')

    def open_login(self):
        self.user_menu_login.click()
