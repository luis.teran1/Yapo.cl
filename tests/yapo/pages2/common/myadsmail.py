# -*- coding: latin-1 -*-
from yapo.component import ComponentComposer
from yapo.component.mail import MyAdsList
from yapo.pages2 import abstract
from yapo import conf
import sunbro


@ComponentComposer
class MyAdsMail(abstract.Mails):

    _go_url = conf.DESKTOP_URL_04 + '/selenium-core/getMail.php?htmlmail=1'

    GREETING_MSG = 'Hola,'
    HEADER_MSG = 'Como has solicitado, te enviamos la lista de tus avisos activos en Yapo.cl:'

    table = sunbro.FindByCSS('#da_table')
    rows = sunbro.FindAllByCSS('#da_table tr')
    greeting_msg = sunbro.FindByCSS('p')
    header_msg = sunbro.FindByCSS("p+p")

    component = {
        'list':    (MyAdsList, '#da_table'),
    }

    def _check_email(self, ads):
        if self.GREETING_MSG != self.greeting_msg.text:
            print(self.GREETING_MSG + "!=" + self.greeting_msg.text)
            return False
        if self.HEADER_MSG != self.header_msg.text:
            print(self.HEADER_MSG + "!=" + self.header_msg.text)
            return False
        return self._check_ads_present(ads, self.table)

    def _check_ads_present(self, present_ads, table):
        first = True
        ads_array = []
        for row in table.find_elements_by_tag_name('tr'):
            if first:
                first = False
                continue
            ads_array.append(row.text)
        print(ads_array)
        return ads_array == present_ads
