# -*- coding: latin-1 -*-
from yapo.pages2 import abstract
from yapo import conf
import sunbro


class EditionExpired(abstract.YapoPage):

    _go_url = conf.DESKTOP_URL + '/edicion-expirada'

    EXPIRED_HEADER = 'El tiempo para editar este aviso ya expir�'
    PUBLISH_SIMILAR = 'Publicar un aviso similar'
    EXPIRED_INFO_1 = 'Recuerda que los avisos de {category_name} s�lo pueden ser editados los primeros {edit_allowed_days} d�as desde la fecha de publicaci�n.'

    icon = sunbro.FindByCSS('.iconEditionExpired')
    title = sunbro.FindByCSS('.editionExpired-title')
    info1 = sunbro.FindByCSS('.editionExpired-paragraph')
    publish = sunbro.FindByCSS('.editionExpired-buttonsPublish')

    def publish_similar(self):
        self.publish.click()
