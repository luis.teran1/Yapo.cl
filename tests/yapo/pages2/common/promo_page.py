# -*- coding: latin-1 -*-
from yapo.component import ComponentComposer
from yapo.component.promo import AdList
from yapo.component.promo import TabList
from yapo.component.promo import Pagination
from yapo.component.promo import Search
from yapo.component.promo import Selectize
from yapo.pages2 import abstract
from yapo.utils import Categories
from yapo import conf
import sunbro


class BasePromoPage(abstract.YapoPage):

    _go_url = conf.DESKTOP_URL + '/promocional'


class NoPromoPage(BasePromoPage):

    PROMO_PAGE_NOT_FOUND_TITLE = "Esta p�gina promocional no est� disponible en yapo.cl"
    PROMO_PAGE_NOT_FOUND_SUBTITLE = "Te invitamos a seguir revisando el sitio"

    not_found_title = sunbro.FindByCSS('.title.text-center')
    not_found_subtitle = sunbro.FindByCSS('.subtext.text-center')


@ComponentComposer
class PromoPage(BasePromoPage):

    component = {
        'ad_list':    (AdList, '.daList'),
        'tab_list':   (TabList, '.tabList'),
        'pagination': (Pagination, '.pagination'),
        '_location':  (Selectize, '.locationContainer'),
        '_search':    (Search, '.searchContainer'),
        '_order':     (Selectize, '.orderContainer'),
    }

    class Sort:
        ByDate      = 0
        ByPriceAsc  = 1
        ByPriceDesc = 2

    def search(self, category=None, region=None, sort=None, query=None):
        self._pick_category(category)
        self._location.select_numeric(choice=region)
        self._order.select_numeric(choice=sort)
        self._search.search(text=query)
        return self

    def _pick_category(self, category):
        if category:
            name = Categories.get_name(category, search_name=True)
            self.tab_list.click(name)
