# -*- coding: latin-1 -*-
import sunbro
from yapo.pages2 import abstract
from yapo import conf


class MyAds(abstract.MyAds):

    _go_url = conf.MOBILE_URL + '/mis_avisos.html'

    submit = sunbro.FindByID('myads_submit')
    email = sunbro.FindByID('myads_email')

    success = sunbro.FindByCSS('.confirmation .legend')

    SUCCESS_MSG = 'Hemos enviado un correo con el listado de tus avisos'

    def _check_request_success(self, email):
        success = self._getattr('success')
        if success:
            print('my ads: {}'.format(success.text))
            return success.text == self.SUCCESS_MSG
