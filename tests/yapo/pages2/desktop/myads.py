# -*- coding: latin-1 -*-
import sunbro
from yapo.pages2 import abstract
from yapo import conf


class MyAds(abstract.MyAds):

    _go_url = conf.DESKTOP_URL + '/mis_avisos.html'

    submit = sunbro.FindByID('email_submit')
    email = sunbro.FindByID('my_ads_email')

    success = sunbro.FindByCSS('.success')

    SUCCESS_MSG = '�Tu e-mail fue enviado! {}'

    def _check_request_success(self, email):
        success = self._getattr('success')
        if success:
            print('my ads: {}'.format(success.text))
            return success.text == self.SUCCESS_MSG.format(email)
