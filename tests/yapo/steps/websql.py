from yapo.pages import control_panel

def get_websql(wd, user, query_name, **filter):
    websql = control_panel.WebSQL(wd)
    websql.login(user, user)
    websql.call_websql(query_name, **filter)
    return websql
