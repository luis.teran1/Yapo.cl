from yapo import trans
from yapo.decorators import retries_function_exception
from datetime import datetime
import re

def make_user_pro(account, type):
    typemap = {
        'inmo': 'packs-2',
        'cars': 'packs-1',
        'jobs': 'insertingfee-1',
    }
    if type in typemap:
        auth = trans.authenticate()
        trans.accounts_change_pro_status(auth['token'], account, typemap[type])
    else:
        raise Exception('{} is not a valid type'.format(type))

def get_ad(email, subject, filter = 'pending_review'):
    """ Given email and subject, return a dictionary with corresponding ad """
    # Translate keys from 'some.path': value to {'some': { 'path': value } } 
    def jsonize(tr):
        r = {}
        for k, v in tr.items():
            parent = None
            current = r
            parts = k.split('.')
            for p in parts:
                if p not in current:
                    current[p] = {}
                parent = current
                current = current[p]
            parent[parts[-1]] = v
        return r

    # Retrieve only matching ads
    def filter_ads(jr):
        ads = []
        for n, ad in jr['ad'].items():
            if ad['filter'] == filter:
                if re.search(subject, ad['ad']['subject']):
                    ads.append(ad)
                elif ad['actions']['action_type'] == 'edit' and re.search(subject, ad['ad_change_params'].get('subject', '')):
                    ads.append(ad)
        return ads

    auth = trans.authenticate()
    tr = trans.search_ads(auth['token'], email)
    jr = jsonize(tr)
    matches = filter_ads(jr)
    
    if not matches:
        raise Exception('No such ad [{}] for email [{}]'.format(subject, email))

    if len(matches) > 1:
        raise Exception('There are {} ads matching [{}] for email [{}]'.format(len(matches), subject, email))

    print('matched ad: {}'.format(matches[0]))
    return matches[0]


@retries_function_exception(num_retries=3)
def review_ad(email, subject, action = 'accept', reason='31', filter='pending_review'):
    """ Approves an ad action """
    ad = get_ad(email, subject, filter)
    auth = trans.authenticate()

    data = {
        'action_id': ad['actions']['action_id'],
        'action': action,
        'reason': reason,
    }

    if 'price' in ad['ad']:
        data['price'] = ad['ad']['price']

    trans.review(ad['ad']['ad_id'], auth['token'], **data)

def new_ad(data):
    """ Backwards compatibility """
    insert_ad(None, **data)

def insert_ad(_unused, **data):
    """ Insert an ad with the same arguments as yapo.steps.newai.insert_ad """
    result = trans.newad(data)
    status = result.get('status');
    ad_id = result.get('ad_id');
    action_id = result.get('action_id');
    pay_code = result.get('paycode');
    if 'pay_type' not in data or data['pay_type'] == 'free':
        clear_data = {
            'verify_code': pay_code
        }
        clear_result = trans.clear(clear_data)
    elif 'auto_clear' in data and data['auto_clear']:
        clear_data = {
            'pay_code': pay_code,
            'phone': 912312312,
            'amount': 999999,
            'pay_time': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
        }
        clear_result = trans.clear(clear_data)
    return result
