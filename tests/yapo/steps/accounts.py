"""Step definition for account"""
from yapo.pages.generic import AccountBar
from yapo.pages.desktop import Login as LoginDesktop
from yapo.pages import mobile
from yapo.pages.desktop import LoginFacebook, DesktopHome
from yapo import conf


def login_from_bar(wd, user, password, from_desktop=True):
    base = conf.DESKTOP_URL if from_desktop else conf.MOBILE_URL
    wd.get(base + "/region_metropolitana")
    page = AccountBar(wd)
    page.login(user, password)

def login_and_go_to_dashboard(wd, email, password = '123123123', from_desktop=True, skip_dashboard_wait=False):
    login = LoginDesktop(wd) if from_desktop else mobile.Login(wd)
    login.login(email, password)
    if not skip_dashboard_wait:
        login.wait.until(lambda x: 'dashboard' in wd.current_url)

def login_with_facebook(wd, facebook_email, facebook_passwd, from_desktop=True):
    if from_desktop:
        desktop = DesktopHome(wd)
        desktop.go()
        # Login by facebook
        login_modal = LoginFacebook(wd)
        login_modal.login(facebook_email, facebook_passwd)
    else:
        # Login by facebook
        login_modal = mobile.LoginFacebook(wd)
        login_modal.login(facebook_email, facebook_passwd)
