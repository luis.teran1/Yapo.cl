# -*- coding: latin-1 -*-

rename = {
    'password': 'passwd',
    'password_verification': 'passwd_ver',
    'email_verification': 'email_confirm',
    'images': 'file',
    'create_account': 'ai_create_account'
} 

def insert_ad(ai_page_object, **kwargs):
    """ New method for a complete ad_insert """

    # rename params 
    kwargs = clean_kwargs(**kwargs)
    
    # go to AI page 
    result_page = insert_ad_go(ai_page_object, **kwargs)
    
    # clean the passed parms
    insert_ad_clean(ai_page_object, **kwargs)

    # Fills the AI parts
    insert_ad_ad_part(ai_page_object, **kwargs)
    insert_ad_geo_part(ai_page_object, **kwargs)
    insert_ad_ups_part(ai_page_object, **kwargs)
    insert_ad_user_part(ai_page_object, **kwargs)

    # should I accept this
    insert_ad_accept_conditions(ai_page_object, **kwargs)

    # submit, preview or nothing
    return insert_ad_whats_next(ai_page_object, **kwargs)

def clean_kwargs(**kwargs):
    """ clean the kwargs param from unwanted stuff"""
    for r in rename:
        if r in kwargs:
            if rename[r] not in kwargs:
                kwargs[rename[r]] = kwargs[r]
    return kwargs

def insert_ad_go(ai_page_object, **kwargs):
    """ go to the ad insert page """
    ai_page_object.go(**kwargs)

def insert_ad_clean(ai_page_object, **kwargs):
    """ clean the passed fields """
    ai_page_object.clean(**kwargs)

def insert_ad_ad_part(ai_page_object, **kwargs):
    """ fills the ad information part """
    ai_page_object.fill_ad_part(**kwargs)

def insert_ad_geo_part(ai_page_object, **kwargs):
    """ fills the geographical information part """
    ai_page_object.fill_geo_part(**kwargs)

def insert_ad_ups_part(ai_page_object, **kwargs):
    """ fill upselling options part """
    ai_page_object.fill_ups_part(**kwargs)

def insert_ad_user_part(ai_page_object, **kwargs):
    """ fill the seller information part """
    ai_page_object.fill_user_part(**kwargs)

def insert_ad_accept_conditions(ai_page_object, **kwargs):
    """ fill the seller information part """
    ai_page_object.accept_conditions(**kwargs)

def insert_ad_whats_next(ai_page_object, **kwargs):
    """ Submit || Preview """
    return ai_page_object.continue_with(**kwargs)
