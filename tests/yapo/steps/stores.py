""" Step definition for store specific stuff
"""
from yapo.pages.desktop import Payment
from yapo.pages.stores import BuyStore, EditForm
from yapo.pages.desktop import SummaryPayment
from yapo.pages.simulator import Simulator
from yapo.utils import Products
from yapo import conf
from yapo.steps import accounts


def start_payment(wd, option, invoice_data=None, servipag_flag=None):
    summary = SummaryPayment(wd)
    if option == 'monthly':
        summary.add_store(Products.STORE_MONTHLY)
    if option == 'quarterly':
        summary.add_store(Products.STORE_QUARTERLY)
    if option == 'biannual':
        summary.add_store(Products.STORE_BIANNUAL)
    if option == 'annual':
        summary.add_store(Products.STORE_ANNUAL)
    page = BuyStore(wd)
    page.wait.until_loaded()
    summary.radio_webpay.click()
    if invoice_data:
        page.wait.until_visible('invoice')
        page.invoice.click()
        page.form.fill_form(**invoice_data)
    if servipag_flag:
        payment = Payment(wd)
        payment.wait.until_visible('pay_with_servipag')
        payment.pay_with_servipag.click()
    page.wait.until_visible("submit")
    page.submit.click()
    return Simulator(wd)


def buy_store_with_bill(wd, option):
    page = start_payment(wd, option)
    page.accept.click()
    page.go_to_site.click()

def buy_store_servipag_no_xml2(wd, option):
    page = start_payment(wd, option, None, "true")
    page.xml2_checkbox.click()
    page.accept.click()
    page.go_to_site.click()

def buy_store_with_invoice(wd, option, data):
    page = start_payment(wd, option, data)
    page.accept.click()
    page.go_to_site.click()


def edit_store(wd, data):
    page = EditForm(wd)
    page.fill_form(**data)
    page.submit.click()


def login_and_go_to_edit(wd, user, password):
    accounts.login_and_go_to_dashboard(wd, user, password)
    wd.get(conf.SSL_URL + "/store_edit")

def visit_store(wd, store_name):
    wd.get(conf.DESKTOP_URL + "/tiendas/" + store_name)
    wd.maximize_window()
