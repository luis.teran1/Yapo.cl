import yapo
from yapo.pages import simulator, desktop, autobump

def _step_login(wd):
    login = desktop.Login(wd)
    login.go()
    login.login('prepaid5@blocket.se', '123123123')

def _step_pay_servipag(wd):
    pay = desktop.Payment(wd)
    pay.pay_with_servipag.click()
    pay.link_to_pay_products.click()

def _step_pay_sim(wd, send_xml2):
    paysim = simulator.Paysim(wd)
    paysim.pay_accept(send_xml2)

def servipag_pay_autobump(wd, list_id, freq, num_days, send_xml2=True):
    """Pay bump unlogged with Servipag"""
    _step_login(wd)

    autobump.direct_go(wd, list_id, freq, num_days)

    _step_pay_servipag(wd)
    _step_pay_sim(wd, send_xml2)

def servipag_pay_credits(wd, send_xml2=True):
    """Pay credits with Servipag"""

    _step_pay_servipag(wd)
    _step_pay_sim(wd, send_xml2)

def servipag_pay_product(wd, list_id, product, send_xml2=True):
    """Pay bump unlogged with Servipag"""
    _step_login(wd)

    ad = {list_id: [product]}
    dma = desktop.DashboardMyAds(wd)
    dma.wait.until_visible('list_all_ads')
    dma.select_products_by_ids(ad)
    wd.execute_script('arguments[0].click()', dma.checkout_button)

    _step_pay_servipag(wd)
    _step_pay_sim(wd, send_xml2)

def transbank_pay_product(wd, list_id, product, doc_type = None):
    """Pay bump unlogged with Transbank"""
    ad = {list_id: [product]}
    dma = desktop.DashboardMyAds(wd)
    dma.go_dashboard()
    dma.select_products_by_ids(ad)
    wd.execute_script('arguments[0].click()', dma.checkout_button)

    sp = desktop.SummaryPayment(wd)
    sp.change_doc_type(doc_type)
    sp.press_link_to_pay_products()
    paysim = simulator.Paysim(wd)
    paysim.pay_accept()

