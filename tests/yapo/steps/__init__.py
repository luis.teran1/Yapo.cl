# -*- coding: latin-1 -*-
from yapo.utils import Categories
from yapo.geography import Regions
from yapo.pages.control_panel import ControlPanel
from yapo.pages.newai import DesktopAI as AdInsertDesktopNew
from yapo.pages.ad_insert import AdInsert as AdInsertDesktopOld, AdInsertResult
from yapo.pages.simulator import Paysim
from yapo.pages import defaults
from yapo.pages import desktop
from yapo.steps import newai
from yapo.steps import trans as steps_trans
from yapo import utils
import copy, time, random, string
from selenium.common import exceptions

from .myads import send_my_ads_mail

from selenium import webdriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

REAL_ESTATE = {'rooms': '2'}

OLD_DEFAULTS = {
    'category': Categories.VENDO,
    'estate_type': '1',
    'region': Regions.REGION_METROPOLITANA,
    'communes': 331,
    'subject': 'My ad',
    'body': 'The body of my ad',
    'create_account': False,
    'name': 'Juan Perez',
    'email': 'juan.perez@noexiste.cl',
    'email_verification': 'juan.perez@noexiste.cl',
    'phone': '55555555',
    'password': '123123123',
    'password_verification': '123123123',
    'accept_conditions': True,
    'add_location': True,
    'address': 'Avenida Arturo Prat',
    'address_number': '430',
    'private_ad': True,
    'show_location': True,
    'rooms': 3,
    'company_ad': 0,
}

DEFAULTS = {
    'regdate': '2010',
    'gearbox': '1',
    'fuel': '1',
    'mileage': '15000'
}
DEFAULTS.update(OLD_DEFAULTS)

DEF_INFORMATION = {
    'company_ad': 0,
    'rut': '55555555-5',
    'name': 'Juan Perez',
    'email': 'juan.perez@noexiste.cl',
    'email_confirm': 'juan.perez@noexiste.cl',
    'ai_create_account': False,
    'phone': '55555555',
    'passwd': '123123123',
    'passwd_ver': '123123123',
}

DEF_UBICATION = {
    'region': Regions.REGION_METROPOLITANA,
    'communes': 331,
    'add_location': False,
}

DEF_CSS = {
    'category': '[name="category_group"]',
    'subject': '[name="subject"]',
}


def pay(browser, SummaryPayment, PaymentVerify):
    summary = SummaryPayment(browser)
    products_table = summary.get_table_data()
    print(products_table)
    summary.choose_bill()
    summary.pay_with_webpay()
    verify = PaymentVerify(browser)
    paysim = Paysim(browser)
    paysim.pay_accept()

    if SummaryPayment == desktop.SummaryPayment:
        verify.wait.until_visible('sale_table')
    else:
        verify.wait.until_visible('text_congratulations')


def insert_an_ad_with_upselling(browser, AdInsert, SummaryPayment, PaymentVerify, combo_id, **ad_data):
    insert_an_ad(browser, AdInsert=AdInsert, Submit=False, **ad_data)

    ai = AdInsert(browser)
    ai.choose_upselling(i=combo_id)
    ai.submit.click()

    pay(browser, SummaryPayment, PaymentVerify)


def insert_an_ad(browser, AdInsert=None, cbs={}, **kwargs):
    """ Insert an ad without account """

    if AdInsert is None:
        steps_trans.insert_ad(None, **kwargs)
        return None

    if AdInsert in [AdInsertDesktopNew, AdInsertDesktopOld]:
        result = newai.insert_ad(AdInsertDesktopNew(browser), **kwargs)
        return result

    # this should be mobile only
    submit = True
    if 'Submit' in kwargs:
        submit = kwargs.pop('Submit')
    ad_params = defaults.DEFAULT_ALL.copy()
    ad_params.update(kwargs)
    page = AdInsert(browser)
    page.go()
    page.insert_ad(**ad_params)
    if not submit:
        return
    page.submit.click()
    result = AdInsertResult(browser)
    return result

def ad_insert_preview(browser, AdInsert=None, cbs={}, **kwargs):
    """ Preview of ad insert """

    if AdInsert in [None, AdInsertDesktopNew, AdInsertDesktopOld]:
        result = newai.insert_ad(AdInsertDesktopNew(browser), Preview=True, **kwargs)
        return result

    ad_params = copy.copy(DEFAULTS)
    category = kwargs.get('category', DEFAULTS['category'])
    ad_params.update(defaults.CATEGORY_PARAMS.get(category, {}))
    ad_params.update(kwargs)
    page = AdInsert(browser)
    page.go()
    if page.is_logged():
        kwargs.pop('private_ad', False)
        kwargs.pop('company_ad', False)
    page.insert_ad(**ad_params)
    page.preview.click()
    result = AdInsertPreview(browser)
    result.wait.until_visible('create')

def insert_and_publish_ad(browser, AdInsert=None, **kwargs):
    """ Insert and publish an ad without account """
    result = insert_an_ad(browser, AdInsert=AdInsert, **kwargs)
    if result:
        result.wait.until(lambda wd: result.was('success'))

    email = kwargs['email'] if 'email' in kwargs else defaults.DEFAULT_USER_INFORMATION['email']
    subject = kwargs['subject'] if 'subject' in kwargs else defaults.DEFAULT_AD_INFO['subject']

    trans.review_ad(
        email=email,
        subject=subject,
        action = 'accept'
    )
    utils.rebuild_index()

def delete_ad(browser, list_id):
    """ Delete an ad from cpanel """

    cpanel = ControlPanel(browser)
    cpanel.go_and_login('dany', 'dany')
    cpanel.search_and_delete_ad(list_id=list_id)
    cpanel.logout()

def review_ad(wd, email, subject, **kwargs):
    """ Review an ad """

    defaults = {'user': 'dany', 'passwd': 'dany', 'queue': 'Unreviewed'}
    defaults.update(kwargs)
    control_panel = ControlPanel(wd)
    control_panel.go()
    control_panel.login(defaults['user'], defaults['passwd'])
    control_panel.search_and_review_ad(email, subject, 'accept', queue=defaults['queue'])
    control_panel.logout()
    utils.rebuild_index()

def accept_and_return_ad_id(wd, email, subject, cp_login={}):
    """ Accept an ad and return it's ad_id"""

    defaults = {'user': 'dany', 'passwd': 'dany'}
    defaults.update(cp_login)
    cp = ControlPanel(wd)
    cp.go_and_login(defaults['user'], defaults['passwd'])
    cp.accept_ad(email = email, subject = subject)
    ad_id = cp.return_ad_id
    cp.close_window_and_back()
    cp.logout()
    return {'ad_id':ad_id}
