from yapo.pages2.common import myadsmail


def send_my_ads_mail(wd, platform, email):
    my_ads = platform.MyAds(wd)
    my_ads.go()
    my_ads.request(email)
    return myadsmail.MyAdsMail(wd)
