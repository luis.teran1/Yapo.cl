# -*- coding: latin-1 -*-
import requests
from os import getenv
from yapo import conf, utils

class MockController():

    def setStage(self, stage):
        stage = "narco_" + stage
        record = getenv("MOCK_MODE")
        mode = self.modes.PLAY
        if record == "RECORD":
            mode = self.modes.RECORD
            utils.make("mock-set_env-" + stage)
        self.__makeRequest({"stage": stage, "mode": mode})

    def setMode(self, mode):
        self.__makeRequest({"mode": mode})

    def __makeRequest(self, data):
        requests.post(conf.MOCK_CONTROL_URL, json=data)

    class modes(object):
        """ Mock modes definition """
        PLAY = 'play'
        RECORD = 'record'
        STATIC = 'static'
