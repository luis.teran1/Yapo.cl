#!/usr/bin/env bash

usage()
{
cat << EOF
usage: $0 options

This script changes the browser to run narcos tests.

OPTIONS:
   -h      Show this message
   -b      Browser, it can be firefox, chrome or phantom
EOF
}


BROWSER=

while getopts "hb:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         b)
             BROWSER=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [ $BROWSER == 'firefox' ]
then
    sed -i 's@browser=.*@browser='FIREFOX'@' setup-functional.cfg
    sed -i 's@browser=.*@browser='FIREFOX'@' setup-parallel.cfg
    sed -i 's@browser=.*@browser='FIREFOX'@' setup.cfg
    echo "Se ha configurado $BROWSER para ejecutar tests."
elif [ $BROWSER == 'chrome' ]
then
    sed -i 's@browser=.*@browser='CHROME'@' setup-functional.cfg
    sed -i 's@browser=.*@browser='CHROME'@' setup-parallel.cfg
    sed -i 's@browser=.*@browser='CHROME'@' setup.cfg
    echo "Se ha configurado $BROWSER para ejecutar tests."
else
    echo "No se puede configurar $BROWSER para ejecutar tests."
fi
