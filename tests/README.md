Tests
=====

The new test structure is intended to eliminate the dependency on makefiles and make them independent, so we can run one test in isolation from the rest.

We are using nose as our test running tool, so, our tests are written in the Python programming language, version 3.33

The dependencies are in the file 'tests/requirements.txt' and can be installed using pip:

```sh
pip install -r tests/requirements.txt
```

The easiest way to run the tests is

```sh
./nose2.sh
```
If some test fails you can run it with

```sh
nosetests --failed
```
To run all the tests of a feature:

```sh
./nose2.sh ad_insert
./nose2.sh ad_list
./nose2.sh stores
...
```
To run an specific file:

```sh
./nose2.sh ad_insert/test_ad_insert.py
./nose2.sh help/test_help.py
```
To run an specific class:

```sh
./nose2.sh path/to/file.py:ClassName
```
Ro run an specific test:

```sh
./nose2.sh path/to/file.py:ClassName.test_function_name
```
A test must pass individually and as part of it class.
It means that it must be independent of any other test case.

## Narcos Tests: Local development configuration

Tests are configured to run into a rancher node, if you need run your test and see the live status with vnc, you must install docker-compose and set up the next yml:

```yml
version: '2'
services:
  chrome-scalable:
    image: selenium/node-chrome-debug:3.141.59-copernicium
    environment:
      HUB_HOST: hub
      NODE_MAX_SESSION: '10'
      NODE_MAX_INSTANCES: '10'
    stdin_open: true
    tty: true
    ports:
    - 5900:5900/tcp
  hub:
    image: selenium/hub:3.141.59-copernicium
    environment:
      GRID_MAX_SESSION: '10'
    ports:
    - 4444:4444/tcp
```
create a `docker-compose.yml` file with the previous content and into the folder run `docker-compose up`

when your containers are on you must set SELENIUM_HUB env variable in your regress environment,

(could be in .bashrc)
`export SELENIUM_HUB=http://[IP]:4444/wd/hub`

and for connection via VNC you should connect to `http://[IP]:5900` with the password: secrets.

so you can see the status of your nodes on url `http://[IP]:4444/grid/console`