# Page Objects

Page Objects is a design pattern for tests that pretends to wrap the functionality of an html page on a class. The intention is that anyone desiring to interact with such page can do so by calling methods or reading fields on an instance of that page's Page Object class.

## Case Study: Yapo My Ads

Let's try, step by step, to write a page object for: [Yapo My Ads](http://www.yapo.cl/mis_avisos.html)

Opening the page, the first thing we see is the Yapo logo, the navigation nar and the login bar. As those elements are present on many pages, they will not be part of our example page object.

Further down, we have 2 main elements: the email field and the submit button. We can represent those like this:

```python
class MyAds(yapo.pages.BasePage):
    email = sunbro.FindByID('my_ads_email')
    submit = sunbro.FindByID('email_submit')
```

Good! But what can we **do** on this page?

Well, we can write an email and push the submit button to see all the ads published with given email. Let's add that:

```python
    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()
```

After the request is sent, we can have two possible outcomes:

* The user has no active ads: An error is displayed
* The user has active ads: The list of active ads will be emailed to the user

So, first, we need to add a couple of extra fields to retrieve the result: 

```python
    error = sunbro.FindByCSS('.error')
    success = sunbro.FindByCSS('.success')
```

This probably is the same on many pages and should be part of an object higher in the hierarchy, but let's keep it here for the sake of the example.

Now, we can just use those fields to determine whether or not we succeeded:

```python
    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()
        if self.error.is_displayed():
            return False
        if self.success.is_displayed():
            return True
        raise Exception('Unknown state')
```

Do you see the problem here?

Good, I didn't expect you to see it. The problem is that selenium loves to throw exceptions whenever an object is not on the page and a test that tries to check for the success case will get an exception when the code tests for self.error.

But there is a solution! And as this is a common problem, it should be on a super class for all pages to inherit from. So, for the sake of the example I'm creating a new super class, but this particular method do perfectly fit on yapo.pages.BasePage:

```python
class YapoPage(yapo.pages.BasePage):
    def _getattr(self, attr):
        try:
            return getattr(self, attr)
        except:
            return None
```

And now, we can rewrite our page object to take advantage of this:

```python
class MyAds(YapoPage):
    ...
    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()

        success = self._getattr('success')
        if success:
            return True

        error = self._getattr('error')
        if error:
            return False

        raise Exception('Unknown state')
```

We could further add methods to simplify the repeated code in the middle, but I'm leaving that out in order to move forward on page objects specific content.

The above code works, but our page object will declare we had success or errored, based only on the presence of the elements. That may be ok on most cases, but what if there are different possible error messages? It may be important (or not) that we discern them.

In our case, it is important so the method becomes:

```python
        ...
        success = self._getattr('success')
        if success and success.text == '�Tu e-mail fue enviado! {}'.format(email):
            return True

        error = self._getattr('error')
        if error and error.text == 'No existen avisos activos para este e-mail: {} '.format(email)
            return False
        ...
```

Now it's getting more and more decent, but what happens if the text on the success case changes on the tested page?

We can certainly change it on the method, but on more complex page objects it could be possible for it to appear in more than one method. So, the idea here is to add such messages as part of the page object class variables:

```python
    ERROR_MSG = 'No existen avisos activos para este e-mail: {} '
    SUCCESS_MSG = '�Tu e-mail fue enviado! {}'
```

Then we can use those to improve the readability of our method.

Here is the full code of both of the discussed classes:

```python
class YapoPage(BasePage):

    def _getattr(self, attr):
        try:
            return getattr(self, attr)
        except:
            return None
```

```python
class MyAds(YapoPage):

    email = sunbro.FindByID('my_ads_email')
    submit = sunbro.FindByID('email_submit')

    error = sunbro.FindByCSS('.error')
    success = sunbro.FindByCSS('.success')

    ERROR_MSG = 'No existen avisos activos para este e-mail: {} '
    SUCCESS_MSG = '�Tu e-mail fue enviado! {}'

    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()

        success = self._getattr('success')
        if success and success.text == self.SUCCESS_MSG.format(email):
            return True

        error = self._getattr('error')
        if error and error.text == self.ERROR_MSG.format(email):
            return False

        raise Exception('Unknown state')

    def go(self):
        self._driver.get('http://www.yapo.cl/mis_avisos.html')
```

I included an extra *go* method for the MyAds page object that will take the browser to the page url.

Now, let's write a test that uses this page object:

```python
class MyAdsTest(SeleniumTest):

    def test_my_ads_error(self, wd):
        myads = MyAds(wd)
        myads.go()
        ok = myads.request('email@with.no.ads')
        self.assertFalse(ok)

    def test_my_ads_ok(self, wd):
        myads = MyAds(wd)
        myads.go()
        ok = myads.request('email@with.ads')
        self.assertTrue(ok)
```

Voil�! Writing tests became extremely simple thanks to the page object, which abstracted the details and quirky parts of the functionality.

## Case Study: Yapo My Ads Mobile

Wait, didn't we just did MyAds for Yapo?

Yes, we did! But for the desktop site, the mobile site is a whole different story! It shouldn't, it won't always be, but it may be, and (in this case), it is.

So, let's head to [Yapo My Ads Mobile](http://m.yapo.cl/mis_avisos.html)

Huh, it looks unsurprisingly similar to the desktop site. We have a big text field for the email and a submit button to get the work done. The fields should look like this:

```python
class MyAdsMobile(YapoPage):

    email = sunbro.FindByID('myads_email')
    submit = sunbro.FindByID('myads_submit')
```

The first surprise is here! The selectors for the email and submit fields are different from the desktop version. This is not good. It's not good in the sense that the page object we already wrote for desktop is not going to work on mobile.

Since we are not going to rewrite the entire site because of that, let's assume that that is acceptable and that our goal is not to recicle page objects across versions of the site, but to recycle the tests itself.

The next surprise is that the *success* behaviour is also different on the mobile version. It features different elements with different names, different texts without the email and even a paper plane! The fields look like this:

```python
    error = sunbro.FindByCSS('.error')
    success = sunbro.FindByCSS('.confirmation .legend')

    ERROR_MSG = 'No existen avisos activos para este e-mail: {}'
    SUCCESS_MSG = 'Hemos enviado un correo con el listado de tus avisos'
```

I wonder if it's ok to still call the *success* member variable like that but I'm going with it as this new (different) elements still represent the same concept.

Next is the request method. I can't stress enough how important is for it to be called the same as the desktop one. You'll see it in a while. The code:

```python
    def request(self, email):
        self.email.clear()
        self.email.send_keys(email)
        self.submit.click()

        success = self._getattr('success')
        if success:
            print(repr(success.text))
        if success and success.text == self.SUCCESS_MSG:
            return True

        error = self._getattr('error')
        if error:
            print(repr(error.text))
        if error and error.text == self.ERROR_MSG.format(email):
            return False

        raise Exception('Unknown state')
```

Again, this looks eerily familiar to the desktop version, with little differences. The first part is the same, the error and unknown part are the same, and only the success part changed a little bit.

This little differences could abstracted away with a method that receives or uses functions to handle the success and error case. I'll leave that as an exercise to the reader.

Finally, our tests will look like this:

```python
class MyAdsMobileTest(SeleniumTest):

    def test_my_ads_error(self, wd):
        myads = MyAdsMobile(wd)
        myads.go()
        ok = myads.request('a@b.c')
        self.assertFalse(ok)

    def test_my_ads_ok(self, wd):
        myads = MyAdsMobile(wd)
        myads.go()
        ok = myads.request('miguel@schibsted.cl')
        self.assertTrue(ok)
```

Boom! That's Exactly the same as the desktop one (except for the page object).

We are really close to our goal here. The only remaining problem is that we had to explicitly write the code for both desktop and mobile tests. Wouldn't it be wonderful to be able to write it just once?

## Case Study: Recycling test code

Oh! You're still here! Good.

If you've come this far, I've expect for you to already have guessed what we are going to do.

The plan here is to see how can we write tests in a way that we can "recycle" the test code and use it for desktop and mobile site (provided that both sites have the same functionality).

Let's pick up where we left on the last chapter: 

```python
class MyAdsMobileTest(SeleniumTest):

    def test_my_ads_error(self, wd):
        myads = MyAdsMobile(wd)
        myads.go()
        ok = myads.request('a@b.c')
        self.assertFalse(ok)

    def test_my_ads_ok(self, wd):
        myads = MyAdsMobile(wd)
        myads.go()
        ok = myads.request('miguel@schibsted.cl')
        self.assertTrue(ok)
```

The first step to recycle the test is to have a common flow between both Desktop and Mobile test. As we know, this is exactly the case.

The second step would be to abstract away the page object we use. A way to do it is to let the test class know which page object we want it to use:

```python
class MyAdsDesktopTest(SeleniumTest):
    MyAds = MyAdsDesktop
    def test_my_ads_error(self, wd):
        myads = self.MyAds(wd)
        ...

class MyAdsMobileTest(SeleniumTest):
    MyAds = MyAdsMobile
    def test_my_ads_error(self, wd):
        myads = self.MyAds(wd)
        ...
```

We used the MyAds instance variable to hold which class we want to use. This way the code of both test classes should become identical, except for the definition of the MyAds field.

Here comes the trick.

What if we just use standard object inheritance to take advantage of the fact that both classes are equal, except for the definition of one of it's members?

```python
class MyAdsDesktopTest(SeleniumTest):
    MyAds = MyAdsDesktop
    def test_my_ads_error(self, wd):
        myads = self.MyAds(wd)
        ...
        
class MyAdsMobileTest(MyAdsDesktopTest):
    MyAds = MyAdsMobile
```

And that's it! We saved ourselves the effort of writing almost duplicate code for the Mobile tests.

Please note that (generally) it doesn't matter if it's Desktop who inherits Mobile, or viceversa. What matters is the fact that we've written the Page Objects in a way that they can be used interchangeably, empowering ourselves with the ability to recycle entire test suites across versions of the site.

But... What if we use many page objects in our test?

```python
class ComplexMobileTest(SeleniumTest):
    MyAds = MyAdsMobile
    Dashboard = DashboardMobile
    AdInsert = AdInsertMobile
    AdView = AdViewMobile
    ...
    
class ComplexDesktopTest(ComplexMobileTest):
    MyAds = MyAdsDesktop
    Dashboard = DashboardDesktop
    AdInsert = AdInsertDesktop
    AdView = AdViewDesktop
    ...
```

That is preferrable than having all the tests cases copied and pasted all over, but we can do better!

Let's say that we have all our Desktop Page Objects in a module called desktop, and all our Mobile Page objects in a module called mobile. We can, then, tell the tests that we want to use the page object contained on that module:

```python
from yapo.pages import desktop, mobile

class ComplexMobileTest(SeleniumTest):
    platform = mobile
    def test_something(self, wd):
        myads = self.platform.MyAds(wd)
    ...
    
class ComplexDesktopTest(ComplexMobileTest):
    platform = desktop
```

And there! Having the page objects separated by module and named the same on each module allows us to use the very central principle of object oriented programming to our favor and reuse the test code.

Of course, this same principle applies to the Page Objects itself. We can write an abstract page object with common functionality, and have the fields and selectors defined on the implementing classes. This is, of course, left as an exercise to the reader :)

## Final notes

I hope you've found this learn by example guide useful to get to know how page objects work and how they can help us write better, simpler and easier to maintain tests.

By no mean this guide is complete. There are lots of tricks you can use to write effective page objects, most of them are not specific to page objects, but practical uses of the core mechanisms of functional and object oriented programming.

The important thing to keep in mind is to ask yourself: Can we do better? And experiment a bit before considering you work done. Every minute you spend refining your page objects, are hours saved whenever tests need to change.

One last thing, don't be afraid to add as much log information as you think it may be necessary. Everything you print is not displayed when the test is ok, but will when the test is broken. You'll regreat not having added those when the test breaks.

Osto!
