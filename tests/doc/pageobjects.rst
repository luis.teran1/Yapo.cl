Page Objects
============

Page objects represent te contents of a page, we use a library called sunbro (https://github.com/arkanus/selenium-sunbro) to write them.

The syntax is declarative, similar to django forms and models.

::

    import sunbro

    class Page(sunbro.Page):
        title = sunbro.FindByCSS('h1')
        cool_image = sunbro.FindByCSS('img.cool_image')

Page objects should never, ever, contain asserts; asserts are the actual test
bussiness.


In the yapo module there is an extended version of the Page class, containing
useful utilities, its's located in yapo.pages.BasePage
The usage is the same as sunbro.Pages.

::

    import yapo.pages

    class BasePage(sunbro.Page):
        title = sunbro.FindByCSS('h1')
        cool_image = sunbro.FindByCSS('img.cool_image')

Waits
-----
The yapo.pages.BasePage class instances provides a Wait wrapper via the wait
attribute

::

    acc_bar = generic.AccountBar(wd)
    acc_bar.login('prepaid3@blocket.se', '123123')
    acc_bar.wait.until_present('message')

This will try to login and then will wait until the 'message' element, described in the page object, appears.

For more info, in a command line type:
    pydoc yapo.pages.BasePage
and
    pydoc yapo.pages.Wait
