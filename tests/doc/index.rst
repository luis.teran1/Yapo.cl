.. Narcos Tests documentation master file, created by
   sphinx-quickstart on Thu Dec 11 16:35:10 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Narcos Tests' documentation!
========================================

Contents:

.. toctree::
   :maxdepth: 2

   pageobjects
   tags


.. automodule:: yapo
   :members:
   :undoc-members:

.. automodule:: yapo.pages
   :members:
   :undoc-members:

.. automodule:: yapo.pages
   :members:
   :undoc-members:

.. automodule:: yapo.pages.accounts
   :members:
   :undoc-members:

.. automodule:: yapo.pages.control_panel
   :members:
   :undoc-members:

.. automodule:: yapo.pages.desktop
   :members:
   :undoc-members:

.. automodule:: yapo.pages.desktop_list
   :members:
   :undoc-members:

.. automodule:: yapo.pages.generic
   :members:
   :undoc-members:

.. automodule:: yapo.pages.help
   :members:
   :undoc-members:

.. automodule:: yapo.pages.mobile
   :members:
   :undoc-members:

.. automodule:: yapo.pages.mobile_list
   :members:
   :undoc-members:

.. automodule:: yapo.pages.simulator
   :members:
   :undoc-members:

.. automodule:: yapo.pages.stores
   :members:
   :undoc-members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
