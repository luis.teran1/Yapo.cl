# Page Object Directives

This document is full of concentrated pills of information about how to write decent Page Objects. It assumes that you know what Page Objects are and how do we use them.

Many of the points here just stress Object Oriented Programming concepts, or closely relate to them. That's intended. Even though those should be general knowledge, the specifics are important to get really meaningful and reusable page objects.

## Abstract vs Desktop vs Mobile
* Create abstract class that represents the functionality
* Idealy this should have common fields and methods for both desktop and mobile
* Specify in the Desktop/Mobile class the unique behaviour of the platform

## Field names
* Do not include element type (selector, text_area, etc)
* If an element has an id, prefer to use the same id as variable name
* Singular name = one element; plural name = list of elements
* Python naming convention: 
	* UpperCamelCase for class names
	* CAPITALIZED_WITH_UNDERSCORES for constants
	* lowercase_separated_by_underscores for other names
* pep8 and pylint are your friends

## Method names
* Private methods must be prefixed with "_"
* Provide complete functionalities on methods, name them afterwards

## Composition
* Use member instances to hold objects of the other components in the page
* Ex AdList: listing, searchbox, navbar, loginbar

## Read-only attrs
* No test should ever need to call .click() or .send_keys() on an attribute
* The only exception is the test written to test every possible quirk of that page
* Ex: javascript injection on a page.

## Inherit if seems reasonable
* AdEdit inherits AdInsert, basically the same page, with extra functionality
* DashboardWithPack inherits Dashboard, theoretically, the same page with extra functionality

## Asserts
* Assert impossible cases. Those which, if broken, no further progress can be made
* Specially on long methods that could break on many points
* FAILS over ERRORS: On broken functionality, a test should fail on an assert, making the error easier to spot. In other words, a Test should return OK o FAIL, not ERROR.

## Texts as fields
* Error or success messages, and other texts should be part of the class
* A change on any site text should require only page objects to be changed, not tests

## Actions that change page return a Page Object instance
* The destination page must be returned
* The page object should do any testing to determine where did it land
* Tests can use directly the returned instance to proceed
* Ej: assert isinstance(AdInsert(wd).insert_ad(data), AdInsertSuccessWithUpselling)

## Solve general problems in superclass
* Common problems should have common solutions

## Explicit waits
* Any explicit waiting must be done on the page object, not the tests
* An actions that ends in a change on the page elements (or page itself) must include a wait that returns when the changes are finished

## Think in performance
* Is 60 seconds ok?
* Did I just added 5 extra seconds to this method?
* Will my clients have to pay those extra seconds every time?
* Can we do better?
