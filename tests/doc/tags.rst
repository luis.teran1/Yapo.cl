Tags
====
Tags are an useful way to keep tests organized in a logical level, not structural level. They consist in a keyword that represents a feature or a section of the site that the test is relevant to.

Usage
------

::

    form yapo.decorators import tag

    @tag('accounts', 'ad_insert')
    def test_ad_insert_with_account(self, wd):
        #do stuff

When you have a set of tests tagged, you can run them like this.

    nosetest -a tag=accounts

Also you can exclude test with certain tags or list them

    nosetest -a tag=accounts,ad_insert,!edit

will run all tests relative to accounts and ad_insert but not the edit ones.
