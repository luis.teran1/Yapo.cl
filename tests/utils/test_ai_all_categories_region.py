# -*- coding: latin-1 -*-
from yapo import utils
from yapo.decorators import tags
from nose.plugins.skip import SkipTest
import time
from yapo.steps import trans as transSteps
import yapo


class AdInsertAllCatsRegion(yapo.SeleniumTest):
    """ This class was used to generate data over accounts
        Inserting on realestate
            One ad per category/region/estate_type
        Inserting on other categories
            One ad per category/region
        
        The ads were:
            - inserted
            - cleared
            - reviewed
         directly using trans
    """
    
    snaps = ['accounts']
    email = 'yolo@yapo.cl'
    price = '999666'
    name = 'yoloberto'
    passwd = '123123123'
    phone = '12341234'

    # This is the first commune by region (15 regions)
    regions_communes = [1, 5, 12, 21, 30, 45, 83, 116, 146, 200, 232, 244, 274, 284, 295]

    data_real_estate = [
        {'category': 1220, 'estate_type': 1, 'price': 1000000, 'size': 50, 'rooms': 1, 'garage_spaces': 1, 'condominio': 20000, 'bathrooms': 1, 'new_realestate': 1},
        {'category': 1220, 'estate_type': 2, 'price': 2000000, 'size': 500, 'rooms': 2, 'garage_spaces': 2, 'condominio': 20000, 'bathrooms': 2, 'new_realestate': 1},
        {'category': 1220, 'estate_type': 3, 'price': 2000000, 'size': 500, 'garage_spaces': 2, 'new_realestate': 1},
        {'category': 1220, 'estate_type': 4, 'price': 2000000, 'size': 500, 'garage_spaces': 2, 'new_realestate': 1},
        {'category': 1220, 'estate_type': 5, 'price': 2000000, 'size': 500, 'condominio': 20000},
        {'category': 1220, 'estate_type': 6, 'price': 2000000, 'size': 500, 'garage_spaces': 2, 'condominio': 20000},
        {'category': 1240, 'estate_type': 1, 'price': 500000, 'size': 500, 'rooms': 2, 'garage_spaces': 2, 'condominio': 20000, 'bathrooms': 2},
        {'category': 1240, 'estate_type': 2, 'price': 500000, 'size': 500, 'rooms': 2, 'garage_spaces': 2, 'condominio': 20000, 'bathrooms': 2},
        {'category': 1240, 'estate_type': 3, 'price': 500000, 'size': 500, 'garage_spaces': 2},
        {'category': 1240, 'estate_type': 4, 'price': 500000, 'size': 500, 'garage_spaces': 2},
        {'category': 1240, 'estate_type': 5, 'price': 500000, 'size': 500, 'condominio': 20000},
        {'category': 1240, 'estate_type': 6, 'price': 500000, 'size': 500, 'garage_spaces': 2, 'condominio': 20000},
        {'category': 1240, 'estate_type': 7, 'price': 500000, 'condominio': 20000},
        {'category': 1260, 'estate_type': 1, 'price': 500000, 'rooms': 2, 'garage_spaces': 2, 'bathrooms': 2, 'services': '1'},
        {'category': 1260, 'estate_type': 2, 'price': 500000, 'rooms': 2, 'garage_spaces': 2, 'bathrooms': 2, 'services': '1'},
        {'category': 1260, 'estate_type': 8, 'price': 500000, 'rooms': 2, 'garage_spaces': 2, 'bathrooms': 2, 'services': '1'},
        {'category': 1260, 'estate_type': 9, 'price': 500000, 'services': '2|3'},
    ]
    data_other = [
        {'category': 2020, 'brand':1, 'model':1, 'version':1, 'regdate':2010, 'gearbox':1, 'fuel':1, 'cartype':1, 'mileage':1},
        {'category': 2040, 'regdate':2010, 'mileage':1, 'fuel':1, 'gearbox':1},
        {'category': 2060, 'regdate':2010, 'mileage':1, 'cubiccms':1},
        {'category': 2100},
        {'category': 2080},
        {'category': 2120},
        {'category': 3060, 'internal_memory':1},
        {'category': 4020, 'condition': 1, 'gender': 1},
        {'category': 4040, 'condition': 1, 'gender': 1},
        {'category': 4060, 'gender': 1},
        {'category': 7020, 'job_category': 1},
        {'category': 7040, 'job_category': 1},
        {'category': 7060, 'service_type': 1},
        {'category': 9020, 'condition': 1},
        {'category': 9040, 'condition': 1},
        {'category': 9060, 'condition': 1},
        {'category': 5020},
        {'category': 5040},
        {'category': 5060},
        {'category': 5160},
        {'category': 6140},
        {'category': 6020},
        {'category': 6060},
        {'category': 6180},
        {'category': 6080},
        {'category': 6100},
        {'category': 6120},
        {'category': 6160},
        {'category': 3040},
        {'category': 3060},
        {'category': 3020},
        {'category': 3080},
        {'category': 7080},
        {'category': 8020},

    ]
    def _insert_ad(self, wd, **kwargs):
        result = transSteps.new_ad(kwargs)

    def _review_ad(self, wd, **kwargs):
        subject = kwargs.get('subject')
        email= kwargs.get('email')
        transSteps.review_ad(email, subject)

    def _add_common_params(self, group, position, region):
        dictionary = group[position]
        dictionary['body'] = 'Test description {0}'.format(dictionary['category'])
        dictionary['company_ad'] = 1
        dictionary['name'] = self.name
        dictionary['email'] = '{0}_{1}_{2}'.format(dictionary['category'], region, self.email)
        dictionary['phone'] = self.phone
        dictionary['password'] = self.passwd
        if dictionary['category'] in [1240,1260]:
            dictionary['type'] = 'u'
        else:
            dictionary['type'] = 's'
        dictionary['region'] = region
        if 'estate_type' in dictionary.keys():
            dictionary['subject'] = 'Test {0} {1} {2}'.format(region, dictionary['category'], dictionary['estate_type'])
        else:
            dictionary['subject'] = 'Test {0} {1}'.format(region, dictionary['category'])
        dictionary['communes'] = self.regions_communes[region-1]
        return dictionary

    def _run_by_group(self, wd, group):
        for i in range(len(group)):
            for region in range(len(self.regions_communes)):
                info = self._add_common_params(group, i, region+1)
                self._insert_ad(wd, **info)
                self._review_ad(wd, **info)

    def test_insert_all_real_estate(self,wd):
        self._run_by_group(wd,self.data_real_estate)

    def test_insert_other_categories(self,wd):
        self._run_by_group(wd,self.data_other)
