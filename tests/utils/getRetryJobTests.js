#!/usr/bin/node

var job_baseurl = process.argv[2];
var job_name = process.argv[3];
var job_number = process.argv[4];

var url = job_baseurl + '/job/' + job_name + '/' + job_number + '/testReport/api/json';

var request = require('request');
request(url, function(error, response, body) {
	if (!error && response.statusCode == 200) {
		runTests(JSON.parse(body));
	}
});

function runTests(response){
	var tests = getTestsCases(response);
	executeNose(tests);
	// process.exit(0);
}

function getTestsCases(response){
	var newTests = [];
	var oldTests = [];
	var testes = [];
	var testCases = [];
	var testCase = null;
	// filter the failed test cases
	for (var i = 0, len = response.suites[0].cases.length; i < len; i++) {
		testCase = response.suites[0].cases[i];
		if (testCase.skipped == true || testCase.className == '<nose.suite')
			continue;
		if (testCase.age == 1)
			newTests.push(testCase);
		else if (testCase.age > 1)
			oldTests.push(testCase);
	}
	testes = newTests.concat(oldTests);
	delete newTests;
	delete oldTests;
	for (var i = 0, len = testes.length; i < len; i++) {
		path = testes[i].className.replace(/\.\w+$/, '');
		className = testes[i].className.replace(path, '').replace('.', ':');
		testPath = path + className + '.' + testes[i].name;
		console.log(testPath);
		testCases.push(testPath);
	}
	return testCases;
}

function executeNose(tests){
	var cmd = './nose2.sh ' + tests.join(' ');
	console.log('execute: ', cmd);
/*
	scriptPath = process.argv[1].split('/');
	scriptPath.splice(scriptPath.length - 2, 2);
	scriptPath = scriptPath.join('/');
	process.chdir(scriptPath);
	var exec = require('child_process').execSync;
	exec(cmd, function(error, stdout, stderr) {
		if (error !== null)
			console.log('--> Error!!', error);
		console.log('stdout:', stdout);
		console.log('stderr:', stderr);
	});

	var path, className, testURI, scriptPath;
	var exec = require('child_process').execSync;
	var child;
	scriptPath = process.argv[1].split('/');
	scriptPath.splice(scriptPath.length - 2, 2);
	scriptPath = scriptPath.join('/');
	process.chdir(scriptPath);

	child = exec('./nose2.sh ' + testURI, function(error, stdout, stderr){
		if (error !== null)
			console.log('--> Error!!', error);
		console.log('stdout:', stdout);
		console.log('stderr:', stderr);
	});
*/
}
