#!/usr/bin/env bash

usage()
{
cat << EOF
usage: $0 options

This script run Core tests on the requested environment.

OPTIONS:
   -h      Show this message
   -e      Environment, it can be stg or pro
   -t      Tag, indicate a tag to be executed, it can be core or usxxxxxxxxx
   -T      Test, indicate a test to be executed, it can be like this <package>.<test_suite>:<class>.<test_case>
EOF
}

wait_rebuild_index()
{
IFS=:
  set -- $*
  secs=$(( ${1#0} * 3600 + ${2#0} * 60 + ${3#0} ))
  while [ $secs -gt 0 ]
  do
    sleep 1 &
    printf "\r%02d:%02d:%02d" $((secs/3600)) $(( (secs/60)%60)) $((secs%60))
    secs=$(( $secs - 1 ))
    wait
  done
  echo
}

ENVIRONMENT=
MOBILE_ENVIRONMENT=
PHP_ENVIRONMENT=
SSL_ENVIRONMENT=
CP_ENVIRONMENT=
TAG=
TEST=

while getopts "he:t:T:" OPTION
do
     case $OPTION in
         h)
             usage
             exit 1
             ;;
         e)
             ENVIRONMENT=$OPTARG
             ;;
         t)
             TAG=$OPTARG
             ;;
         T)
             TEST=$OPTARG
             ;;
         ?)
             usage
             exit
             ;;
     esac
done

if [[ -z $ENVIRONMENT || -z $TAG ]]
then
     usage
     exit 1
fi

if [ $ENVIRONMENT == 'stg' ]
then
	DESKTOP_ENVIRONMENT='http://www.yapo.cl'
	MOBILE_ENVIRONMENT='http://m.yapo.cl'
	PHP_ENVIRONMENT='http://www2.yapo.cl'
	SSL_ENVIRONMENT='https://www2.yapo.cl'
	CP_ENVIRONMENT='https://www2.yapo.cl/controlpanel'
elif [ $ENVIRONMENT == 'pro' ]
then
	DESKTOP_ENVIRONMENT='http://www.yapo.cl'
	MOBILE_ENVIRONMENT='http://m.yapo.cl'
	PHP_ENVIRONMENT='http://www2.yapo.cl'
	SSL_ENVIRONMENT='https://www2.yapo.cl'
	CP_ENVIRONMENT='https://www2.yapo.cl/controlpanel'
fi

sed -i.bak 's@DESKTOP_URL =.*@DESKTOP_URL = "'$DESKTOP_ENVIRONMENT'"@' yapo/conf.py
sed -i 's@MOBILE_URL =.*@MOBILE_URL = "'$MOBILE_ENVIRONMENT'"@' yapo/conf.py
sed -i 's@PHP_URL =.*@PHP_URL = "'$PHP_ENVIRONMENT'"@' yapo/conf.py
sed -i 's@SSL_URL =.*@SSL_URL = "'$SSL_ENVIRONMENT'"@' yapo/conf.py
sed -i 's@CONTROLPANEL_URL =.*@CONTROLPANEL_URL = "'$CP_ENVIRONMENT'"@' yapo/conf.py
sed -i 's@ENVIRONMENT =.*@ENVIRONMENT = "'$ENVIRONMENT'"@' yapo/conf.py

sed -i.bak 's/cls._set_bconfs().*/#cls._set_bconfs()/' yapo/__init__.py
sed -i 's/cls._restore_snaps().*/#cls._restore_snaps()/' yapo/__init__.py
sed -i 's/yapo.utils.flush_redises().*/#yapo.utils.flush_redises()/' yapo/__init__.py

NOSETESTS=`which nosetests-3.4 2> /dev/null`

if [ "a${NOSETESTS}" == "a" ]; then
       NOSETESTS=`which nosetests-3.3 2> /dev/null`
fi

./startx.sh 2> /dev/null

echo "Starting tests execution on "$ENVIRONMENT", and tag "$TAG
# Execute tests for the desired tag and environment
if [ $TAG == 'core' ]
then
    $NOSETESTS --with-xunit -a tag=core1
    mv nosetests.xml nosetests-core1.xml
    cat resources/ad_data/test_data.json | python -m json.tool
    echo "Esperando rebuild-index.."
    wait_rebuild_index "00:10:00"
    $NOSETESTS --with-xunit -a tag=core2
    mv nosetests.xml nosetests-core2.xml
    echo "Iniciando proceso de borrado.."
    wait_rebuild_index "00:00:10"
    $NOSETESTS --with-xunit -a tag=core3
elif [[ $TEST != '' ]]
then
    $NOSETESTS --with-xunit $TEST
else
    $NOSETESTS --with-xunit -a tag=$TAG
fi
cat resources/ad_data/test_data.json | python -m json.tool
mv yapo/conf.py.bak yapo/conf.py
mv yapo/__init__.py.bak yapo/__init__.py
