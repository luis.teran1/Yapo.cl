""" Script to generate a list of Narcos Testcases"""
import csv, subprocess
from nose.suite import ContextSuite
from nose.case import Test
from nose.loader import TestLoader


class TestCollector:

    def generate_test(lista, test, test_suite):
        test_case = test.split(' ')[0]
        if 'nose.suite.ContextSuite' in test_case:
            return lista
        try:
            package = test.split(' ')[1].rsplit('.',1)[0][12:]
            clase = test.split(' ')[1].rsplit('.',1)[1][:-1]
            lista.append(package+':'+clase+'.'+test_case)
        except IndexError:
            print('test_suite: {1} - {0}'.format(test, test_suite))
        return lista

    @staticmethod
    def list_test_cases():
        loader = TestLoader()
        tests = []
        tests_modules = loader.loadTestsFromDir(path='functional/')
        for test_module in tests_modules:
            for test_suite in test_module:
                for test_case in test_suite:
                    if isinstance(test_case, Test):
                        tests = TestCollector.generate_test(tests, str(test), test_suite)
                    elif isinstance(test_case, ContextSuite):
                        for test in test_case:
                            if isinstance(test, Test):
                                tests = TestCollector.generate_test(tests, str(test), test_suite)
                            elif isinstance(test, ContextSuite):
                                for other_test in test:
                                    tests = TestCollector.generate_test(tests, str(other_test), test_suite)

        cmd = 'find functional -name "ptest*" | grep -v ".pyc" | sed "s/functional\///" && cd ../..'
        out = subprocess.check_output(cmd, shell=True)
        for line in out.decode("utf-8").split('\n')[:-1]:
            tests.append(line)

        with open('narcos_tests.csv', 'w') as myfile:
            wtr = csv.writer(myfile, quoting=csv.QUOTE_MINIMAL)
            for test in tests:
                wtr.writerow([test])
            print("Archivo CSV exportado!")

TestCollector.list_test_cases()
