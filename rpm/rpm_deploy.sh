#!/bin/bash
# $1 = revision

# You need to have ssh id/key pairs to the destination to make the ssh/scp work
# YOU MUST SET THIS TO THE CORRECT VALUE
#
DESTINATION=root@vm100008

# The repo directory on the destination machine
# YOU MUST SET THIS TO THE CORRECT VALUE
#
REPO_DIR=/opt/repo/caipirinha


##### DO NOT CHANGE BEYOND THIS POINT ####
case "$1" in 
	-h|--help)
		echo "usage: $0 [revision_to_deploy] - (revision is the svn revision)"
		should_die=1
		;;
esac
if [ -z $DESTINATION ] ; then
	echo "	TODO: You need to set DESTINATION! ($0)"
	should_die=1
fi
if [ -z $REPO_DIR ] ; then
	echo "	TODO: You need to set REPO_DIR! ($0)"
	should_die=1
fi
if [ $should_die ]; then 
	exit
fi

SCRIPT=`readlink -f $0`
BASEDIR=`dirname $SCRIPT`
RPM_CHANGED_SH=$BASEDIR/rpm_changed.sh
REVISION_BASE=`ssh $DESTINATION "ls $REPO_DIR/*.rpm 2>/dev/null | cut -d '-' -f4 | cut -d '.' -f1 | sort -nr | head -n 1"`
echo "REVISION_BASE=$REVISION_BASE"

if [ -z $1 ] ; then
	REVISION_TARGET=`find . -name *.rpm | cut -d '-' -f4 | cut -d '.' -f1 | sort -nr | head -n 1`
else
	REVISION_TARGET=$1
fi
echo "REVISION_TARGET=$REVISION_TARGET"

TO_DEPLOY=""
CHANGED=`$RPM_CHANGED_SH $REVISION_BASE $REVISION_TARGET`
for i in $CHANGED; do
	if [ `ls $i*$REVISION_TARGET*.rpm 2>/dev/null | wc -l` -ne 0 ]; then
		echo $i
		TO_DEPLOY="$TO_DEPLOY $i"
	fi
done
if [ -z "$TO_DEPLOY" ]; then
	echo "Nothing to deploy."
	exit
fi

echo
echo "Deploy? y/N"
read C
case "$C" in
	y|Y)
		;;
	*)
		exit
		;;
esac

for i in $TO_DEPLOY; do
	for f in $i*$REVISION_TARGET*.rpm; do
		if [ `ssh $DESTINATION "ls $REPO_DIR/$(basename $f) 2>/dev/null | wc -l"` -ne 0 ]; then
			echo "$(basename $f) already deployed - scp manualy if you want to overwrite."
		else
			scp $f $DESTINATION:$REPO_DIR
		fi
	done
done

echo "Regenerating RPM repository indexes"
CREATEREPO=`ssh $DESTINATION "createrepo $REPO_DIR"`


