#!/bin/bash
# $1 = base revision
# $2 = target revision

if [ -z $2 ] ; then
	echo "Usage: $0 revision_base revision_target"
	echo "	return all rpms changed between revisions (revision is the svn revision)"
	exit 1
fi
CRCFILE_USER=`id -nu`
CRCFILE_BASE="/tmp/$CRCFILE_USER-rpm-$1.crc"
CRCFILE_TARGET="/tmp/$CRCFILE_USER-rpm-$2.crc"
echo > $CRCFILE_BASE
echo > $CRCFILE_TARGET

SCRIPT=`readlink -f $0`
BASEDIR=`dirname $SCRIPT`

for d in $BASEDIR/noarch $BASEDIR/ia32e; do
	for i in $d/*$1*.rpm; do
		rpm -qp --queryformat "[$d/%{=NAME} %{FILENAMES} %{FILEMD5S}\n]" "$i" >> $CRCFILE_BASE
	done
	for i in $d/*$2*.rpm; do
		rpm -qp --queryformat "[$d/%{=NAME} %{FILENAMES} %{FILEMD5S}\n]" "$i" >> $CRCFILE_TARGET
	done
done

diff --side-by-side --suppress-common-lines $CRCFILE_BASE $CRCFILE_TARGET | cut -d " " -f 1 | sort | uniq 
