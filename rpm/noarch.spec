%define blocket /opt/blocket
%define cwd %(pwd)
%define bpvschema bpv
%define __spec_install_post /usr/lib/rpm/brp-compress
Summary: Blocket
Name: blocket
Version: 26.32.00
Release: %(expr `platform/scripts/build/buildversion.sh`)
License: Proprietary
Group: Blocket/Applications
URL: http://www.yapo.cl

BuildRoot: %{cwd}/dest-noarch/%{name}-root

%description
These are the noarch applications that make up www.yapo.cl

%package plsqlprocs-blocketdb
Summary: plsql procedures only to be installed on db servers with blocketdb database
Group: Blocket/Applications
Requires: blocket-plsqlmodules
BuildArch: noarch

%package plsqlprocs-queuedb
Summary: plsql procedures only to be installed on db servers with queuedb database
Group: Blocket/Applications
Requires: blocket-plsqlmodules
BuildArch: noarch

%description plsqlprocs-blocketdb
Only install on db hosts!

%description plsqlprocs-queuedb
Only install on db hosts!

%pre plsqlprocs-blocketdb
if [ "$1" = "2" ] ; then # upgrade
  if [ -f %{blocket}/etc/plsqlprocs_preupgrade-blocketdb ] ; then
    cat %{blocket}/etc/plsqlprocs_preupgrade-blocketdb | su - postgres -c "psql blocketdb"
  fi
fi

%pre plsqlprocs-queuedb
if [ "$1" = "2" ] ; then # upgrade
  if [ -f %{blocket}/etc/plsqlprocs_preupgrade-queuedb ] ; then
    cat %{blocket}/etc/plsqlprocs_preupgrade-queuedb | su - postgres -c "psql queuedb"
  fi
fi

%preun plsqlprocs-blocketdb
if [ "$1" = "0" ] ; then # last uninstall
  if [ -f %{blocket}/etc/plsqlprocs_preupgrade-blocketdb ] ; then
    cat %{blocket}/etc/plsqlprocs_preupgrade-blocketdb | su - postgres -c "psql blocketdb"
  fi
fi

%preun plsqlprocs-queuedb
if [ "$1" = "0" ] ; then # last uninstall
  if [ -f %{blocket}/etc/plsqlprocs_preupgrade-queuedb ] ; then
    cat %{blocket}/etc/plsqlprocs_preupgrade-queuedb | su - postgres -c "psql queuedb"
  fi
fi

%post plsqlprocs-blocketdb
cat << 'EOEOEO2' | su - postgres -c "psql blocketdb"
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat scripts/db/views/*.sql)
%(cat scripts/db/plsql/_*.sql)
%(cat scripts/db/plsql/gr*.sql)
%(cat scripts/db/plsql/ge*.sql)
%(cat scripts/db/plsql/a[a-c]*.sql)
%(cat scripts/db/plsql/a[d-z]*.sql)
%(cat scripts/db/plsql/b*.sql)
%(cat scripts/db/plsql/c[a-l]*.sql)
%(cat scripts/db/plsql/c[m-z]*.sql)
%(cat scripts/db/plsql/[d-f]*.sql)
%(cat scripts/db/plsql/h*.sql)
%(cat scripts/db/plsql/i[a-m]*.sql)
%(cat scripts/db/plsql/i[o-z]*.sql)
%(cat scripts/db/plsql/insert[a-z]*.sql)
%(cat scripts/db/plsql/insert_[a-o]*.sql)
%(cat scripts/db/plsql/insert_[p-z]*.sql)
%(cat scripts/db/plsql/[j-l]*.sql)
%(cat scripts/db/plsql/m*.sql)
%(cat scripts/db/plsql/[n-p]*.sql)
%(cat scripts/db/plsql/q*.sql)
%(cat scripts/db/plsql/r*.sql)
%(cat scripts/db/plsql/s*.sql)
%(cat scripts/db/plsql/t*.sql)
%(cat scripts/db/plsql/u*.sql)
%(cat scripts/db/plsql/v*.sql)
%(cat scripts/db/plsql/[w-z]*.sql)
%(cat platform/scripts/db/plsql/at_clear_commands.sql)
%(cat platform/scripts/db/plsql/update_dynamic_bconf.sql)
%(cat scripts/db/triggers/*.sql)
COMMIT;
EOEOEO2

%post plsqlprocs-queuedb
cat << 'EOEOEO3' | su - postgres -c "psql queuedb"
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat scripts/db/views/*.sql)
%(cat scripts/db/plsql/_*.sql)
%(cat scripts/db/plsql/gr*.sql)
%(cat scripts/db/plsql/ge*.sql)
%(cat scripts/db/plsql/a[a-c]*.sql)
%(cat scripts/db/plsql/a[d-z]*.sql)
%(cat scripts/db/plsql/b*.sql)
%(cat scripts/db/plsql/c[a-l]*.sql)
%(cat scripts/db/plsql/c[m-z]*.sql)
%(cat scripts/db/plsql/[d-f]*.sql)
%(cat scripts/db/plsql/h*.sql)
%(cat scripts/db/plsql/i[a-m]*.sql)
%(cat scripts/db/plsql/i[o-z]*.sql)
%(cat scripts/db/plsql/insert[a-z]*.sql)
%(cat scripts/db/plsql/insert_[a-o]*.sql)
%(cat scripts/db/plsql/insert_[p-z]*.sql)
%(cat scripts/db/plsql/[j-l]*.sql)
%(cat scripts/db/plsql/m*.sql)
%(cat scripts/db/plsql/[n-p]*.sql)
%(cat scripts/db/plsql/q*.sql)
%(cat scripts/db/plsql/r*.sql)
%(cat scripts/db/plsql/s*.sql)
%(cat scripts/db/plsql/t*.sql)
%(cat scripts/db/plsql/u*.sql)
%(cat scripts/db/plsql/v*.sql)
%(cat scripts/db/plsql/[w-z]*.sql)
%(cat platform/scripts/db/plsql/at_clear_commands.sql)
%(cat platform/scripts/db/plsql/update_dynamic_bconf.sql)
%(cat scripts/db/triggers/*.sql)
COMMIT;
EOEOEO3

%install
# for pachage blocket-plsqlprocs
mkdir -p $RPM_BUILD_ROOT%{blocket}/etc
cat << 'EOEOEO4' > $RPM_BUILD_ROOT%{blocket}/etc/plsqlprocs_preupgrade-blocketdb
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat scripts/db/plsql/[a-l]*.sql | scripts/db/find_drops.pl)
%(cat scripts/db/plsql/[m-z]*.sql | scripts/db/find_drops.pl)
%(ls -r scripts/db/views/*.sql | xargs cat | scripts/db/find_drops.pl)
COMMIT;
EOEOEO4
cp -a $RPM_BUILD_ROOT%{blocket}/etc/plsqlprocs_preupgrade-blocketdb $RPM_BUILD_ROOT%{blocket}/etc/plsqlprocs_preupgrade-queuedb

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files plsqlprocs-blocketdb
%defattr(-,root,root)
%{blocket}/etc/plsqlprocs_preupgrade-blocketdb

%files plsqlprocs-queuedb
%defattr(-,root,root)
%{blocket}/etc/plsqlprocs_preupgrade-queuedb

