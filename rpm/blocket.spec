%define blocket /opt/blocket
%define python_lib %{blocket}/lib/python/%(python -c "import sys; print sys.version_info[0]")
%define cwd %(pwd)
%define bpvschema bpv
%define __spec_i1stall_post /usr/lib/rpm/brp-compress
Summary: Blocket
Name: blocket
Version: 26.32.00

# Don't forget about noarch.spec when branching!
Release: %(expr `platform/scripts/build/buildversion.sh`)
License: Proprietary
Group: Blocket/Applications
URL: http://www.yapo.cl

BuildRoot: %{cwd}/dest/%{name}-root

%description
These are the applications that make up www.yapo.cl

%package perl-common
Summary: blocket perl modules
Group: Blocket/Applications
Provides: perl(perl_trans)
Provides: perl(blocket::bconf)
Provides: perl(blocket::index)
Provides: perl(news)
provides: perl(import)
Summary: A Perl module for getting bconf, indexing and talking to trans
BuildRequires: perl >= 0:5.003
Requires: perl >= 0:5.005

%description perl-common
Perl modules that are used by several blocket apps

%package python-common
Summary: python modules
Group: Blocket/Applications

%description python-common
Python modules that are used by several blocket apps

%package logocop
Summary: blocket logocop log checker
Group: Blocket/Applications
Provides: perl(.::logocop.conf)
Summary: A perl program for monitoring logs on jopocop
BuildRequires: perl >= 0:5.003
Requires: perl >= 0:5.005
Requires: perl-Digest-SHA1

%description logocop
A perl program for monitoring logs on jopocop

%package nagios-plugins
Summary: Scripts and configuration of the Nagios alarms
Group: Blocket/Applications
Requires: nagios >= 2.12
Requires: nagios-plugins

%description nagios-plugins
Pluggins to check the servers and services of the production environment with Nagios

%package www-common
Summary: Most webservers need this
Group: Blocket/Applications
Requires: httpd
Requires: mod_security
Requires: blocket-bconf-conf

%description www-common
Common

%package mod_blocket
Summary: the main apache module
Group: Blocket/Applications
Requires: blocket-www-common
Requires: blocket-bconf-conf

%description mod_blocket
Apache module providing functionality for the list/view pages

%package php_bconf
summary: Access bconf from php.
Group: Blocket/Backend
Requires: php

%description php_bconf
Access bconf from php.

%package php-common
Summary: common libraries and script files
Group: Blocket/Applications
Requires: php
Requires: php-gd
Requires: blocket-www-common
Requires: blocket-php_bconf
Requires: blocket-bconf-conf

%description php-common
PHP scripts need this

%package webstress
Summary: Server stress generator that uses http, trans or search protocol
Group: Blocket/Utils

%description webstress
Server stress generator that uses http, trans or search protocol

%files webstress
%defattr(-,root,root)
%{blocket}/bin/webstress

%package sendaemon
Summary: flexible sender daemon with pluggable input/output modules
Group: Blocket/Daemons
Requires: librabbitmq
Requires: librabbitmq-tools
Requires: rabbitmq-server
Requires: python2 >= 2.6.6
Requires: python-pip
Provides: sendaemon

%description sendaemon
Flexible sender daemon with pluggable input/output modules

%files sendaemon
%defattr(-,root,root)
%{blocket}/bin/sendaemon
%{blocket}/include/cmdline.py
%{blocket}/include/conf.py
%{blocket}/include/debug.py
%{blocket}/include/dispatcher.py
%{blocket}/include/dummy_input.py
%{blocket}/include/file_output.py
%{blocket}/include/libs.py
%{blocket}/include/rabbit_input.py
%{blocket}/include/trans_output.py
%{blocket}/include/stats.py
%{blocket}/include/yapo.py

%post sendaemon
%(cat scripts/postinst/sendaemon-postinstall.sh)
%(cat scripts/postinst/rabbitmq-postinstall.sh)

%package scripts
Summary: Helper scripts to make our life easier
Group: Blocket/Utils

%description scripts
Helper scripts to make our life easier

%files scripts
%{blocket}/scripts/clean_redis_adlist.sh
%{blocket}/scripts/clean_redis_cart.sh
%{blocket}/scripts/steal_redis_session.sh
%{blocket}/scripts/flushqueue.sh
%{blocket}/scripts/rpmdiff.sh
%{blocket}/scripts/partner_ads_report.py
%{blocket}/scripts/refresh_account.sh
%{blocket}/scripts/push_refusal_reasons.sh
%{blocket}/scripts/flush_preprocessing_queue.sh
%{blocket}/scripts/fill_redis_email.sh

%package tip
Summary: tip
Group: Blocket/Applications
Requires: blocket-php-common

%description tip
Tip a friend about an ad.

%package uplogo
Summary: Upload company logo
Group: Blocket/Applications
Requires: blocket-php-common

%description uplogo
Upload company logo

%package support
Summary: Support
Group: Blocket/Applications
Requires: blocket-php-common

%description support
Blocket support

%package linkredir
Summary: Link redirect
Group: Blocket/Applications
Requires: blocket-php-common

%description linkredir
Blocket link redirection

%package importads
Summary: Import ads
Group: Blocket/Applications
Requires: blocket-php-common

%description importads
Blocket import ads

%package store
Summary: Store
Group: Blocket/Applications
Requires: blocket-php-common

%description store
Blocket store

%package mobile-api
Summary: Configuration files for the mobile API
Group: Blocket/Applications
Requires: blocket-www-common
Requires: blocket-mod_blocket

%description mobile-api
Configuration files for the mobile API

%package newad
Summary: Add ads
Group: Blocket/Applications
Requires: php
Requires: php-gd
Requires: blocket-php-common
Requires: php-pecl-imagick

%description newad
Add ads

%package payment-invoice
Summary: Yapo invoice / bills system
Group: Blocket/Applications
Requires: blocket-payment

%description payment-invoice
Send and receive invoice /bills data from payments

%package payment-invoice-batch
Summary: Yapo invoice / bills batch scripts
Group: Blocket/Applications
Requires: php-soap
Requires: blocket-php-common

%description payment-invoice-batch
A set of scripts to process the bills and invoices

%package bump-email-batch
Summary: Yapo batch script that sends emails to users so they can buy bumps
Group: Blocket/Applications
Requires: blocket-php-common

%description bump-email-batch
A set of scripts to send emails to users so they can buy a bump

%package payment
Summary: Yapo payment
Group: Blocket/Applications
Requires: blocket-php-common

%description payment
Payment api

%package php-khipu
Summary: Khipu libraries and notification app
Group: Blocket/Applications
Requires: blocket-php-common
Requires: blocket-payment-rest

%description php-khipu
PHP Api client library to khipu conection

%package payment-rest
Summary: Payment internal API resful
Group: Blocket/Applications

%description payment-rest
PHP Payment intenal API resful


%package transbank
Summary: 3rd party CGI for payments
Group: Blocket/Applications
Requires: blocket-payment
Requires: perl >= 0:5.005

%description transbank
3rd party CGI for payments

%package servipag
Summary: 3rd party payment site
Group: Blocket/Applications
Requires: blocket-payment

%description servipag
3rd party payment site

%package telesales-payment
Summary: Yapo telesales payment
Group: Blocket/Applications
Requires: blocket-php-common

%description telesales-payment
Payment application for telesales

%package transbank_telesales
Summary: 3rd party CGI for telesales
Group: Blocket/Applications
Requires: blocket-payment
Requires: perl >= 0:5.005
Requires: blocket-transbank

%description transbank_telesales
3rd party CGI for telesales

%package mobile
Summary: Yapo mobile site and api
Group: Blocket/Applications
Requires: blocket-mod_blocket

%description mobile
Yapo mobile site

%package mobile-newad
Summary: Yapo mobile newad
Group: Blocket/Applications
Requires: php
Requires: php-gd
Requires: blocket-php-common
Requires: blocket-mod_blocket
Requires: blocket-accounts
Requires: blocket-mobile
Requires: php-pecl-imagick

%description mobile-newad
Yapo mobile newad

%package paysim
Summary: Simulate transbank payments
Group: Blocket/Applications
Requires: php
Requires: blocket-php-common

%description paysim
Simulate transbank payments, only for testing.

%package senderror
Summary: Send technical error messages
Group: Blocket/Applications
Requires: blocket-php-common

%description senderror
Send technical error messages

%package sendpass
Summary: Password reminder
Group: Blocket/Applications
Requires: blocket-php-common

%description sendpass
Emails passwords on remind request

%package adwatch
Summary: Watch handler
Group: Blocket/Applications
Requires: php
Requires: blocket-php-common

%description adwatch
Watch handler

%package adreply
Summary: Sending mail to ad owner
Group: Blocket/Applications
Requires: php
Requires: blocket-php-common

%description adreply
Send mail to ad owner

%package ajax
Summary: ajax in php
Group: Blocket/Applications
Requires: php
Requires: blocket-php-common

%description ajax
Ajax php application

%package controlpanel
Summary: Controlpanel
Group: Blocket/Backend
Requires: blocket-php-common
Requires: blocket-ssl

%description controlpanel
Controlpanel

%package controlpanel_stats
Summary: Controlpanelgraphs
Group: Blocket/Backend
Requires: blocket-controlpanel

%description controlpanel_stats
Controlpanel graphs

%package bmemcache
Summary: state daemon for php
Group: Blocket/Daemons
Requires: memcached

%description bmemcache
Blocket transaction state daemon

%package trans
Summary: Transaction handler
Group: Blocket/Daemons
Requires: blocket-perl-common
Requires: postfix
Requires: php
Requires: perl-Crypt-SSLeay
Requires: perl-libwww-perl
Requires: perl-XML-Parser
Requires: GeoIP
Requires: librabbitmq
Requires: blocket-bconf-conf

%description trans
Transaction handler

%package bconfd
Summary: Bconf daemon
Group: Blocket/Daemons
Requires: blocket-bconf
Requires: libcurl
Requires: nc

%description bconfd
A daemon that serves Bconf

%package filterd
Summary: filter_data daemon
Group: Blocket/Daemons
Requires: libcurl
Requires: nc

%description filterd
A daemon that runs filter_data fast as f...

%package bconf
Summary: Configuration files
Group: Blocket/Daemons

%description bconf
Configuration files

%package search-common
Summary: Blocket search engine
Group: Blocket/Daemons
Requires: blocket-bconf-conf

%description search-common
Blocket search engine

%package search-csearch
Summary: Blocket search engine - stores
Group: Blocket/Daemons
Requires: blocket-search-common nc

%description search-csearch
Blocket ad search engine specific

%package search-msearch
Summary: Blocket search engine - maps
Group: Blocket/Daemons
Requires: blocket-search-common nc

%description search-msearch
Blocket map search engine specific

%package search-josesearch
Summary: Blocket search engine - promotional pages
Group: Blocket/Daemons
Requires: blocket-search-common nc

%description search-josesearch
Blocket promotional pages search engine.
promotional pages -> promo pages -> ppages -> ppsearch -> josesearch

%package search-asearch
Summary: Blocket search engine - stores
Group: Blocket/Daemons
Requires: blocket-search-common nc

%description search-asearch
Blocket ad search engine specific

%package dav_put
Summary: image replication tool
Group: Blocket/Utils
Requires: blocket-python-common

%description dav_put
Uploads images to apache

%package mod_image
Summary: image manipulation tool
Group: Blocket/Applications
Requires: blocket-www-common
Requires: blocket-mod_blocket
Requires: blocket-bconf-conf

%description mod_image
fetches images from a backend, performs transformations on the image and sends it to the requester

%package bcli
Summary: Blocket CLI
Group: Blocket/Utils

%description bcli
Command line interface to trans

%package redir
Summary: Redir for webfronts
Group: Blocket/Applications

%description redir
Redir implemented as a mod_rewrite config.

%package ssl
Summary: https support with certificates
Group: Blocket/Applications
Requires: blocket-www-common
Requires: mod_ssl

%description ssl
https support with certificates

%package plsqlmodules
Summary: plsql modules only to be installed on db servers
Group: Blocket/Applications
Requires: postgresql-contrib

%description plsqlmodules
Only install on db hosts!

%package batch
summary: Batch scripts, installed on batch server.
group: blocket/applications
provides: perl(batch)
requires: blocket-perl-common
requires: postfix
requires: php-pgsql
requires: php-pdo
requires: blocket-php_bconf
requires: php-common
requires: postfix
requires: blocket-bconf-conf

%description batch
Batch scripts. install on batch server, and run from cron.

%package statpoints
summary: Statpoints counting daemon.
group: blocket/applications
requires: blocket-bconf-conf

%description statpoints
Stat collector

%package redisstat
summary: Redis backend storage for Statpoints
group: blocket/applications

%description redisstat
Stat collector (backend storage)

%package redisemail
summary: Redis email storage
group: blocket/applications

%description redisemail
Storage of email info (populated by cmd:create_account)

%package redisprometheus
summary: Redis prometheus metrics storage
group: blocket/applications

%description redisprometheus
Storage of prometheus metrics (populated by send_technical_error)

%package redissession
summary: Redis session storage
group: blocket/applications

%description redissession
Storage of session info

%package redisdashboard
summary: Redis for dashboard ads cache
group: blocket/applications

%description redisdashboard
Redis for dashboard ads cache (cmd:get_account_ads2)

%package redisaccounts
summary: Redis accounts storage
group: blocket/applications

%description redisaccounts
Storage of accounts info

%package redisnextgen_api
summary: Nextgen api cache redis
group: blocket/applications

%description redisnextgen_api
Nextgen api cache redis

%package redis-mobile-api
summary: Redis backend storage for mobile_api
group: blocket/applications

%description redis-mobile-api
Storage for API app keys

%package redis-cardata
summary: Redis backend storage for cardata
group: blocket/applications

%package redis-credits-cache
summary: Redis backend cache for credits
group: blocket/applications

%description redis-cardata
Cardata from S.i.i. (Chile) database

%description redis-credits-cache
Cache for the credits service

%package redis_wordsranking
summary: Redis for similar ads storage
group: blocket/applications

%description redis_wordsranking
Saves data used to display similar results in ad not found

%package scripts_wordsranking
summary: Scripts for suggestions wordranking
group: blocket/applications

%description scripts_wordsranking
Saves the wordranking scripts

%package errorpages
summary: Blocket System activity data collector
Group: Blocket/System
requires: httpd

%description errorpages
Error pages replaceing the apache/squid defaults

%package buildscripts
summary: Scripts for automatic build on buildserver
Group: Blocket/System

%description buildscripts
Scripts for automatic build on buildserver

%package slonyfuncs
summary: Functions for replicating database operations
Group: Blocket/Utils

%description slonyfuncs
Functions for replicating database operations

%package dump_ads
Summary: Dumping ads
Group: Blocket/Utils

%description dump_ads
Dumping ads

%package batch-stats1x1
Summary: Count pageviews per category and region
Group: Blocket/Applications
Requires: perl >= 0:5.005
Requires: blocket-perl-common
Requires: perl-DBD-Pg
Requires: blocket-bconf-conf

%description batch-stats1x1
Perl scripts to parse pageviews logs and fill the database tables

%package migrate
summary: Script to move ads from one DB definition to a different one
group: blocket/applications
requires: php-cli

%description migrate
Migrate script. Install on any server, configure and run.

%files migrate
%defattr(-,root,root)
%{blocket}/bin/migrate.php
%{blocket}/bin/migration.conf.php

%package category_change
summary: Script to fake edits to predefined ads aiming to manual category change
group: blocket/applications
requires: php-cli
requires: blocket-perl-common
requires: blocket-bconf-conf

%description category_change
Scripts for managing category changes after importing. Install on any server that has access to trans, configure and run.

%files category_change
%defattr(-,root,root)
%{blocket}/bin/category_change.php
%config %{blocket}/bin/cc_config.ini
%{blocket}/bin/mail_heuristic_changes.pl

%package job-category-migration
Summary: Scripts to remap job_category adparam
Group: Blocket/Applications
Requires: blocket-perl-common
Requires: python2 >= 2.6.6

%description job-category-migration
Scripts to be installed on the database server to fetch the ads and to actually remap and update the job_category adparam

%files job-category-migration
%defattr(-,root,root)
%{blocket}/scripts/ads_to_jobs_16.sql
%{blocket}/scripts/ads_to_jobs_17.sql
%{blocket}/scripts/ads_to_recategorize.sql
%{blocket}/scripts/migrate.pl
%{blocket}/scripts/remap.py
%{blocket}/scripts/remap_job_category.py

%package accounts
summary: The accounts package with php, conf, etc
group: blocket/applications
Requires: blocket-php-common
Requires: blocket-www-common
Requires: blocket-accounts-common

%description accounts
All the files that are used for accounts should be here

%files accounts
%defattr(-,root,root)
%{blocket}/apps/MsgWidgetProxyApp.php
%{blocket}/apps/account_login.php
%{blocket}/apps/account.php
%{blocket}/apps/BlocketAccount.php
%{blocket}/apps/reset_password.php
%{blocket}/apps/dashboard.php
%{blocket}/apps/ads_api.php
%{blocket}/apps/AdsApp.php
%{blocket}/apps/credits.php
%{blocket}/include/CreditsPage.php
%{blocket}/include/DashboardPage.php
%{blocket}/apps/accounts_api.php
%{blocket}/apps/AccountsAPI.php
%{blocket}/apps/AccountsApp.php
%{blocket}/apps/jobs_api.php
%{blocket}/apps/JobsAPI.php
%{blocket}/apps/JobsApp.php
%{blocket}/apps/sms_api.php
%{blocket}/apps/SMSAPI.php
%{blocket}/apps/SMSApp.php
%config %{blocket}/conf/bconf.txt.accounts
%config %{blocket}/conf/bconf.txt.redis_email
%config %{blocket}/conf/bconf.txt.redis_session
%config %{blocket}/conf/bconf.txt.redis_accounts
%config %{blocket}/conf/bconf.txt.redis_dashboard
%config %{blocket}/conf/bconf.txt.redis_prometheus
%config %{blocket}/conf/mod_sec_account.conf
%config %{blocket}/conf/mod_sec_account_login.conf
%config %{blocket}/conf/mod_sec_msg_proxy.conf
%config %{blocket}/conf/bconf.txt.phone_codes
%config %{blocket}/conf/mod_sec_jobs.conf
%config %{blocket}/conf/mod_sec_sms.conf
%config %{blocket}/conf/bconf.txt.jobs
%config %{blocket}/conf/bconf.txt.sms
%config %{blocket}/conf/bconf.txt.payment_delivery
%config %{blocket}/conf/bconf.txt.notifications
%config %{blocket}/conf/bconf.txt.widgets
%config %{blocket}/conf/bconf.txt.ts_services
%{blocket}/include/MessagingCenterUtils.php

%package packs
summary: The packs package with php, conf, etc
group: blocket/applications
Requires: blocket-accounts
Requires: blocket-php-common
Requires: blocket-www-common

%description packs
All the files that are used for packs should be here

%files packs
%defattr(-,root,root)
%{blocket}/apps/pack_api.php
%{blocket}/apps/PackApp.php
%config %{blocket}/conf/bconf.txt.packs
%config %{blocket}/conf/mod_sec_pack.conf
%{blocket}/www-ssl/css/cars_pack.css
%{blocket}/www-ssl/js/cars_pack.js

%package accounts-scripts
summary: Accounts scripts to run in sysadmin server
group: blocket/application
Requires: blocket-accounts-common

%description accounts-scripts
This first script is used after the rebuild-index

%files accounts-scripts
%defattr(-,root,root)
%{blocket}/bin/account_activation_reminder.php
%{blocket}/bin/account_activation_reminder_first.php
%{blocket}/bin/account_to_pro_load.php
%{blocket}/bin/account_to_pro.php

%package accounts-common
summary: Accounts common files
group: blocket/application

%description accounts-common
Accounts common files

%files accounts-common
%{blocket}/include/AccountUtil.php

%package shopping-cart
summary: Cart API inteface to implement the shopping cart for yapo
group: blocket/application
Requires: blocket-php-common

%description shopping-cart
All the files that are used for shopping cart should be here

%files shopping-cart
%defattr(-,root,root)
%{blocket}/include/RedisMultiproductManager.php
%{blocket}/apps/cart_api.php
%{blocket}/include/CartApi.php
%{blocket}/include/CartApp.php
%{blocket}/include/CommonCart.php
%config %{blocket}/conf/mod_sec_cart.conf
%config %{blocket}/conf/bconf.txt.cart
%config %{blocket}/conf/bconf.txt.cart_common

%package refund
summary: Refund module package
group: blocket/application
Requires: blocket-php-common

%description refund
All the files that are used for refund

%files refund
%{blocket}/apps/refund.php
%config %{blocket}/conf/mod_sec_refund.conf
%{blocket}/www-ssl/js/refund_form.js
%{blocket}/www-ssl/css/refund_form.css

%package facebook_login
summary: Facebook login module package
group: blocket/application
Requires: blocket-php-common

%description facebook_login
All the files that are used for facebook_login

%files facebook_login
%{blocket}/apps/httpful.phar
%{blocket}/apps/facebook_login.php
%{blocket}/apps/facebook_app.php
%{blocket}/apps/GoogleLogin.php
%{blocket}/apps/google_app.php
%config %{blocket}/conf/mod_sec_social_login.conf
%{blocket}/www-ssl/css/login.css
%{blocket}/www-ssl/img/sprite-loading-facebook.png

%package dashboard_banner
Summary: The all classy, fancy banner right of the dashboard
Group: Blocket/Applications
Requires: httpd

%description dashboard_banner
The all classy, fancy banner right of the dashboard.
This package should include the html wrapper and the image file.

%files dashboard_banner
%{blocket}/www-ssl/img/dashboard_banner.gif
%{blocket}/www-ssl/img/dashboard-banner-2.gif
%{blocket}/www-ssl/dashboard_banner.html
%{blocket}/www-ssl/dashboard_banner_motors.html

%package landing
summary: Refund module package
group: blocket/application
Requires: blocket-php-common

%description landing
All the files that are used for landing

%files landing
%{blocket}/apps/landing.php
%config %{blocket}/conf/mod_sec_landing.conf
%{blocket}/www-ssl/js/landing.js
%{blocket}/www-ssl/css/landing.css

%package bconf-conf
Summary: Common configuration file for different services
group: blocket/application

%description bconf-conf
Common configuration file for different services and scripts, like trans, statpoint, and others

%files bconf-conf
%config %{blocket}/conf/bconf.conf

# Post-install hooks

%post bconf
%(cat scripts/postinst/bconf-postinstall.sh)

%post www-common
%(cat scripts/postinst/wwwcommon-postinstall.sh)

%post php-common
%(cat scripts/postinst/phpcommon-postinstall.sh)

%post controlpanel_stats
%(cat scripts/postinst/controlpanel_stats-postinstall.sh)

%post bmemcache
%(cat scripts/postinst/bmemcache-postinstall.sh)

%post trans
%(cat scripts/postinst/trans-postinstall.sh)

%post bconfd
%(cat scripts/postinst/bconfd-postinstall.sh)

%post filterd
%(cat scripts/postinst/filterd-postinstall.sh)

%post search-common
%(cat scripts/postinst/search-postinstall.sh)

%post search-asearch
%(cat scripts/postinst/search-asearch-postinstall.sh)

%post search-csearch
%(cat scripts/postinst/search-csearch-postinstall.sh)

%post search-msearch
%(cat scripts/postinst/search-msearch-postinstall.sh)

%post search-josesearch
%(cat scripts/postinst/search-josesearch-postinstall.sh)

%post newad
%(cat scripts/postinst/newad-postinstall.sh)

%post mod_blocket
%(cat scripts/postinst/mod_blocket-postinstall.sh)

%post redir
%(cat scripts/postinst/redir.sh)

%post statpoints
%(cat scripts/postinst/statpoints-postinstall.sh)

%post redisstat
%(cat scripts/postinst/redisstat-postinstall.sh)

%post redis-mobile-api
%(cat scripts/postinst/redis-mobile-api-postinstall.sh)

%post redis_wordsranking
%(cat scripts/postinst/redis-wordsranking-postinstall.sh)

%post redis-cardata
%(cat scripts/postinst/redis-cardata-postinstall.sh)

%post redis-credits-cache
%(cat scripts/postinst/redis-credits-cache-postinstall.sh)

%post redisemail
%(cat scripts/postinst/redis-email-postinstall.sh)

%post redissession
%(cat scripts/postinst/redis-session-postinstall.sh)

%post redisdashboard
%(cat scripts/postinst/redis-dashboard-postinstall.sh)

%post redisaccounts
%(cat scripts/postinst/redis-accounts-postinstall.sh)

%post redisnextgen_api
%(cat scripts/postinst/redis-nextgen_api-postinstall.sh)

%post redisprometheus
%(cat scripts/postinst/redis-prometheus-postinstall.sh)

%post batch
%(cat scripts/postinst/batch-postinstall.sh)

%post batch-stats1x1
%(cat scripts/postinst/batch-stats1x1-postinstall.sh)

%post nagios-plugins
%(cat scripts/postinst/nagios-plugins-postinstall.sh)

%post plsqlmodules
cat << 'EOEOEO2' | su - postgres -c "psql blocketdb"
SET search_path = %{bpvschema},public;
BEGIN TRANSACTION;
%(cat modules/psql_uuid/uuid_install.rpm-sql)
COMMIT;
EOEOEO2

%post payment-invoice-batch
%(cat scripts/postinst/invoice-batch-postinstall.sh)

%post bump-email-batch
%(cat scripts/postinst/bump-email-batch-postinstall.sh)

%post payment-invoice
%(cat scripts/postinst/invoice-postinstall.sh)

%post transbank
%(cat scripts/postinst/transbank-postinstall.sh)

%post transbank_telesales
%(cat scripts/postinst/transbank-telesales-postinstall.sh)

%install
make DESTDIR=%{blocket} INSTALLROOT=$RPM_BUILD_ROOT%{blocket} FLAVOR=%{?prod_variation} install ninja-%{prod_variation}

%clean
[ "$RPM_BUILD_ROOT" != "/" ] && rm -rf $RPM_BUILD_ROOT

%files perl-common
%defattr(-,root,root)
%{blocket}/lib/perl5/


%files python-common
%defattr(-,root,root)
%{python_lib}/PyDAV_0_21

%files mod_blocket
%defattr(-,root,root)
%config %{blocket}/conf/mod_blocket.conf
%config %{blocket}/conf/mod_sec_li.conf
%config %{blocket}/conf/mod_sec_friendlyurls.conf
%config %{blocket}/conf/list_and_view_vars.conf
%config %{blocket}/conf/mod_sec_promotional.conf
%config %{blocket}/conf/mod_sec_templates.conf
%config %{blocket}/conf/mod_sec_safe.conf
%config %{blocket}/conf/mod_deflate.conf
%config %{blocket}/conf/mod_sec_new_api.conf
%config %{blocket}/conf/mod_sec_merken_api.conf
%{blocket}/modules/mod_bconf.so
%{blocket}/modules/mod_list.so
%{blocket}/modules/mod_redir.so
%{blocket}/modules/mod_templates.so
%{blocket}/modules/mod_textgif.so
%{blocket}/share/mod_blocket/verdana_bold.ttf
%{blocket}/share/mod_blocket/verdana.ttf
%{blocket}/share/mod_blocket/Helvetica_bold.ttf
%{blocket}/share/mod_blocket/Helvetica-Light.ttf
%{blocket}/www-ssl/img/store-preview.gif
%{blocket}/www-ssl/img/store-icon.gif
%{blocket}/www-ssl/img/upselling-rejected-warning.gif
%{blocket}/www-ssl/img/thumb_border_left.gif
%{blocket}/www-ssl/img/thumb_border_top.gif
%{blocket}/www-ssl/img/thumb_extra_border_bottom.gif
%{blocket}/www-ssl/img/thumb_extra_border_right.gif
%{blocket}/www-ssl/img/thumb_extra_left_bottom.gif
%{blocket}/www-ssl/img/thumb_extra_right_bottom.gif
%{blocket}/www-ssl/img/thumb_extra_right_top.gif
%{blocket}/www-ssl/img/thumb_left_top.gif
%{blocket}/www-ssl/img/thumb_single_border_bottom.gif
%{blocket}/www-ssl/img/thumb_single_border_right.gif
%{blocket}/www-ssl/img/thumb_single_left_bottom.gif
%{blocket}/www-ssl/img/thumb_single_right_bottom.gif
%{blocket}/www-ssl/img/thumb_single_right_top.gif
%{blocket}/www-ssl/img/0.gif
%{blocket}/www-ssl/img/0_extra.gif
%{blocket}/www-ssl/img/0_video.gif
%{blocket}/www-ssl/img/modal-bg.png
%{blocket}/www-ssl/img/none.gif
%{blocket}/www-ssl/img/BulletChecked.gif
%{blocket}/www-ssl/img/recommended-text.png
%{blocket}/www-ssl/img/pack_toggle_on.png
%{blocket}/www-ssl/img/pack_toggle_off.png
%{blocket}/www-ssl/img/abuse-form-gradient.png
%{blocket}/www-ssl/img/marknadsforing_footer.gif
%{blocket}/www-ssl/img/icon_info_footer.gif
%{blocket}/www-ssl/img/bevakning_mini.gif
%{blocket}/www-ssl/img/line_t.gif
%{blocket}/www-ssl/img/logo_internal_pages.gif
%{blocket}/www-ssl/img/A_box.gif
%{blocket}/www-ssl/img/blue_box.gif
%{blocket}/www-ssl/img/box_quote_1.gif
%{blocket}/www-ssl/img/box_quote_2.gif
%{blocket}/www-ssl/img/check.gif
%{blocket}/www-ssl/img/old_guy.png
%{blocket}/www-ssl/img/orange_button_nogratis.gif
%{blocket}/www-ssl/img/orange_button.gif
%{blocket}/www-ssl/img/young_guy.png
%{blocket}/www-ssl/img/terms_yapo.pdf
%{blocket}/www-ssl/img/error_icon.gif
%{blocket}/www-ssl/img/bubbles_blue.png
%{blocket}/www-ssl/img/list_bullet_blue.png
%{blocket}/www-ssl/img/ico_errorx.png
%{blocket}/www-ssl/img/store-preview.png
%{blocket}/www-ssl/img/android-banner-close.png
%{blocket}/www-ssl/img/yapo-google-play-banner.jpg
%{blocket}/www-ssl/img/logo_yapo.png
%{blocket}/www-ssl/img/facebook.png
%{blocket}/www-ssl/img/flecha-arriba.png
%{blocket}/www-ssl/img/instagram.png
%{blocket}/www-ssl/img/twitter.png
%{blocket}/www-ssl/img/youtube.png
%{blocket}/www-ssl/img/bump-ahora.gif
%{blocket}/www-ssl/img/alert-icon.svg

%files php-common
%defattr(-,root,root)
%{blocket}/www-ssl/js/scriptaculous/
%{blocket}/modules/php_templates.so
%{blocket}/modules/php_trans.so
%{blocket}/modules/php_bsearch.so
%{blocket}/modules/php_index.so
%{blocket}/modules/mod_progress.so
%{blocket}/include/bAd.php
%{blocket}/include/bXML.php
%{blocket}/include/bImage.php
%{blocket}/include/bImageStream.php
%{blocket}/include/EXIF.php
%{blocket}/include/EXIF_Makernote.php
%{blocket}/include/EXIF_Tags.php
%{blocket}/include/IPTC.php
%{blocket}/include/JPEG.php
%{blocket}/include/JFIF.php
%{blocket}/include/Makernotes/casio.php
%{blocket}/include/PIM.php
%{blocket}/include/pjmt_utils.php
%{blocket}/include/Photoshop_IRB.php
%{blocket}/include/Unicode.php
%{blocket}/include/XML.php
%{blocket}/include/XMP.php
%{blocket}/include/bResponse.php
%{blocket}/include/text_diff.php
%{blocket}/include/bSession.php
%{blocket}/include/bStandardClass.php
%{blocket}/include/bTransaction.php
%{blocket}/include/util.php
%{blocket}/include/imagecreatefrombmp.php
%{blocket}/include/init.php
%{blocket}/include/dyn_config.php
%{blocket}/include/memcachedclient.php
%{blocket}/include/memcachedsession.php
%{blocket}/include/weeks.php
%{blocket}/include/blocket_application.php
%{blocket}/include/blocket_caller.php
%{blocket}/include/blocket_store.php
%{blocket}/include/fpdf.php
%{blocket}/include/courier.php
%{blocket}/include/helveticab.php
%{blocket}/include/helvetica.php
%{blocket}/include/timesbi.php
%{blocket}/include/timesi.php
%{blocket}/include/zapfdingbats.php
%{blocket}/include/helveticabi.php
%{blocket}/include/helveticai.php
%{blocket}/include/symbol.php
%{blocket}/include/timesb.php
%{blocket}/include/times.php
%{blocket}/include/courierb.php
%{blocket}/include/courierbi.php
%{blocket}/include/courieri.php
%{blocket}/include/pdf.php
%{blocket}/include/soap_request.php
%{blocket}/include/blocket_soap_application.php
%{blocket}/include/binterfaces.php
%{blocket}/include/RedisSessionClient.php
%{blocket}/include/RedisSessionManager.php
%{blocket}/include/geoip.inc
%{blocket}/include/yapo_geoip.php
%{blocket}/include/redis_cardata.php
%{blocket}/include/AccountSession.php
%{blocket}/include/AdsEvaluatorApiClient.php
%{blocket}/include/AdsEvaluatorService.php
%{blocket}/include/JSON.php
%{blocket}/include/JSONSerializable.php
%{blocket}/include/JsonSchema.php
%{blocket}/include/Validator.php
%{blocket}/include/bLogger.php
%{blocket}/include/bRESTful.php
%{blocket}/include/bRedisClass.php
%{blocket}/include/rabbitmq_send.php
%{blocket}/include/Servipag.php
%{blocket}/include/DuplicatedAd.php
%{blocket}/include/DuplicatedAdDesktop.php
%{blocket}/include/DuplicatedAdMobile.php
%{blocket}/include/EasyRedis.php
%{blocket}/include/RedisInstance.php
%{blocket}/include/blocket_adwatch.php
%{blocket}/include/blocket_filter.php
%{blocket}/include/blocket_navigation.php
%{blocket}/include/blocket_token_application.php
%{blocket}/include/MsgWidgetProxy.php
%{blocket}/include/ProxyClient.php
%{blocket}/include/YapoRequest.php
%{blocket}/include/Notifications.php
%{blocket}/include/CreditsService.php
%{blocket}/include/BifrostService.php
%{blocket}/include/SAPService.php
%{blocket}/include/YapofactService.php
%{blocket}/include/SMSService.php
%{blocket}/include/WebpayService.php
%{blocket}/include/PaymentDeliveryQueryService.php
%{blocket}/include/SmartbumpService.php
%{blocket}/include/CarouselService.php
%{blocket}/include/RewardsService.php
%{blocket}/include/LoanManagerService.php
%{blocket}/include/ClaimProductService.php
%{blocket}/include/HomeLinksService.php
%{blocket}/include/MicroserviceConnector.php
%{blocket}/include/httpful.php
%{blocket}/include/BlocketApiService.php
%{blocket}/include/Partner.php
%{blocket}/include/interfaces/PartnerService.php
%{blocket}/apps/api_gateway.php
%{blocket}/apps/ApiGatewayClass.php
%{blocket}/include/autoload_lib.php
%{blocket}/include/register_time.php
%{blocket}/include/lib/ApiClient.php
%{blocket}/include/lib/Bconf.php
%{blocket}/include/lib/Debug.php
%{blocket}/include/lib/Logger.php
%{blocket}/include/lib/Payment/Trans.php
%{blocket}/include/lib/QuickRedis.php
%{blocket}/include/lib/Restful.php
%{blocket}/include/lib/Timer.php
%{blocket}/include/lib/HighlighText.php
%{blocket}/include/lib/Ads/MultiInsertChecker.php
%{blocket}/include/lib/ControlPanel/TokenManager.php
%{blocket}/include/lib/ControlPanel/BasicTokenManager.php
%{blocket}/include/lib/ControlPanel/LockedTokenManager.php
%{blocket}/include/lib/Metrics/MetricsAgent.php
%{blocket}/include/lib/Metrics/MetricsAgentFactory.php
%{blocket}/include/lib/Metrics/NewRelicAgent.php
%{blocket}/include/lib/Metrics/NoAgent.php
%{blocket}/include/lib/Mutex/DistributedMutex.php
%{blocket}/include/lib/Mutex/DistributedMutexLock.php
%{blocket}/include/lib/Mutex/SingleRedisMutex.php
%{blocket}/include/lib/Mutex/SingleRedisMutexLock.php
%{blocket}/include/lib/Mutex/NonceGenerator.php
%{blocket}/include/lib/Mutex/StaticNonceGenerator.php
%{blocket}/include/lib/Mutex/UniqidNonceGenerator.php

%{blocket}/include/PdfManager.php
%{blocket}/www-ssl/img/logo_pdf.jpg

%{blocket}/include/Categories.php
%{blocket}/include/Upselling.php

%config %{blocket}/conf/php.conf
%config %{blocket}/conf/php.ini

%defattr(-,root,root)
%files www-common
%{blocket}/bin/mod_sec_memcache.pl
%{blocket}/conf/gulp-config.js
%{blocket}/etc/init.d/bapache
%{blocket}/www-ssl/css/ai_success.css
%{blocket}/www-ssl/css/seller-info.css
%{blocket}/www-ssl/css/base.css
%{blocket}/www-ssl/css/bootstrap.css
%{blocket}/www-ssl/css/common.css
%{blocket}/www-ssl/css/dashboard_bump.css
%{blocket}/www-ssl/css/deletion.css
%{blocket}/www-ssl/css/deletion_mail.css
%{blocket}/www-ssl/css/handheld.css
%{blocket}/www-ssl/css/help.css
%{blocket}/www-ssl/css/home.css
%{blocket}/www-ssl/css/icheckgreen-and-blue.css
%{blocket}/www-ssl/css/icheckgreen.css
%{blocket}/www-ssl/css/jui.css
%{blocket}/www-ssl/css/m_base.css
%{blocket}/www-ssl/css/m_delete_reasons.css
%{blocket}/www-ssl/css/m_ai.css
%{blocket}/www-ssl/css/m_favorites.css
%{blocket}/www-ssl/css/altiro-bundle.css
%{blocket}/www-ssl/css/edition_expired.css
%{blocket}/www-ssl/css/mobile.css
%{blocket}/www-ssl/css/mpu.css
%{blocket}/www-ssl/css/payment.css
%{blocket}/www-ssl/css/payment_bump.css
%{blocket}/www-ssl/css/soap.css
%{blocket}/www-ssl/css/store_edition.css
%{blocket}/www-ssl/css/store_not_found.css
%{blocket}/www-ssl/css/m_new_ai.css
%{blocket}/www-ssl/css/new_ai.css
%{blocket}/www-ssl/css/m_duplicated_ad.css
%{blocket}/www-ssl/css/listing.css
%{blocket}/www-ssl/css/duplicated_ad.css
%{blocket}/www-ssl/css/terms-conditions.css
%{blocket}/www-ssl/img/spin.gif
%{blocket}/www-ssl/img/home_sprites.png
%{blocket}/www-ssl/img/home_yapo_logo.png
%{blocket}/www-ssl/img/logo.png
%{blocket}/www-ssl/img/logo_small.png
%{blocket}/www-ssl/img/morephotos.png
%{blocket}/www-ssl/img/map-bg.png
%{blocket}/www-ssl/img/selectbox_arrow.jpg
%{blocket}/www-ssl/img/sprite.png
%{blocket}/www-ssl/img/sprite-ai.png
%{blocket}/www-ssl/img/sprite-bump-classic.png
%{blocket}/www-ssl/img/sprite-bump-mobile.png
%{blocket}/www-ssl/img/sprite-bump-mobile@2x.png
%{blocket}/www-ssl/img/sprite-checkout.png
%{blocket}/www-ssl/img/sprite-common.png
%{blocket}/www-ssl/img/sprite-gallery.png
%{blocket}/www-ssl/img/sprite-payment.png
%{blocket}/www-ssl/img/sprite-smileys.png
%{blocket}/www-ssl/img/sprite-tips.png
%{blocket}/www-ssl/img/sprite_help.png
%{blocket}/www-ssl/img/sprite_mobile.png
%{blocket}/www-ssl/img/sprite_mobile2.png
%{blocket}/www-ssl/img/thumb_40x30.jpg
%{blocket}/www-ssl/img/thumb_60x45.jpg
%{blocket}/www-ssl/img/thumb_80x60.jpg
%{blocket}/www-ssl/img/thumb-no-image.png
%{blocket}/www-ssl/js/ad_delete.js
%{blocket}/www-ssl/js/ad_delete_success.js
%{blocket}/www-ssl/js/delete.js
%{blocket}/www-ssl/js/deletion.js
%{blocket}/www-ssl/js/detail.js
%{blocket}/www-ssl/js/fine-uploader.js
%{blocket}/www-ssl/js/fine-uploader-new.js
%{blocket}/www-ssl/js/help.js
%{blocket}/www-ssl/js/home.js
%{blocket}/www-ssl/js/home_ie.js
%{blocket}/www-ssl/js/html5shiv.js
%{blocket}/www-ssl/js/listing.js
%{blocket}/www-ssl/js/mai_success.js
%{blocket}/www-ssl/js/mobile.js
%{blocket}/www-ssl/js/pagination.js
%{blocket}/www-ssl/js/payment.js
%{blocket}/www-ssl/js/payment_confirmation.js
%{blocket}/www-ssl/js/payment_mobile.js
%{blocket}/www-ssl/js/pay_apps.js
%{blocket}/www-ssl/js/plupload.js
%{blocket}/www-ssl/js/plupload-new.js
%{blocket}/www-ssl/js/pvf_mobile.js
%{blocket}/www-ssl/js/geolocation.js
%{blocket}/www-ssl/js/mai.js
%{blocket}/www-ssl/js/smartphone_mobile.js
%{blocket}/www-ssl/js/mpu.js
%{blocket}/www-ssl/js/pop_navigation_message.js
%{blocket}/www-ssl/js/new_ai.js
%{blocket}/www-ssl/js/support.js
%{blocket}/www-ssl/js/bootstrap.min.js
%{blocket}/www-ssl/js/jquery.cookie.js
%{blocket}/www-ssl/js/jquery.easing.min.js
%{blocket}/www-ssl/js/jquery.mobilesite.min.js
%{blocket}/www-ssl/js/ui.js
%{blocket}/www-ssl/js/daview.js
%{blocket}/www-ssl/js/viewda.js
%{blocket}/www-ssl/js/not_found.js
%{blocket}/www-ssl/js/wc-info-box.js
%{blocket}/www-ssl/accesskey.htc
%{blocket}/www-ssl/ads.txt
%{blocket}/www-ssl/apple-touch-icon-114x114-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-114x114.png
%{blocket}/www-ssl/apple-touch-icon-120x120-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-120x120.png
%{blocket}/www-ssl/apple-touch-icon-144x144-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-144x144.png
%{blocket}/www-ssl/apple-touch-icon-152x152-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-57x57-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-57x57.png
%{blocket}/www-ssl/apple-touch-icon-72x72-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-72x72.png
%{blocket}/www-ssl/apple-touch-icon-76x76-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-precomposed.png
%{blocket}/www-ssl/apple-touch-icon.png
%{blocket}/www-ssl/css/m_adview.css
%{blocket}/www-ssl/css/ad_view.css
%{blocket}/www-ssl/css/bills.css
%{blocket}/www-ssl/css/blocket.css
%{blocket}/www-ssl/css/bump_ai.css
%{blocket}/www-ssl/css/customerservice.css
%{blocket}/www-ssl/css/default.css
%{blocket}/www-ssl/css/footer.css
%{blocket}/www-ssl/css/generic.css
%{blocket}/www-ssl/css/index.css
%{blocket}/www-ssl/css/list.css
%{blocket}/www-ssl/css/monokai.css
%{blocket}/www-ssl/css/not_found.css
%{blocket}/www-ssl/css/paysim.css
%{blocket}/www-ssl/css/pay_apps.css
%{blocket}/www-ssl/css/responsive_home.css
%{blocket}/www-ssl/css/scarface.css
%{blocket}/www-ssl/css/securitycenter.css
%{blocket}/www-ssl/css/store_box.css
%{blocket}/www-ssl/css/store_buy.css
%{blocket}/www-ssl/css/store_details.css
%{blocket}/www-ssl/css/store_list.css
%{blocket}/www-ssl/css/store_logged_in.css
%{blocket}/www-ssl/css/storebox.css
%{blocket}/www-ssl/css/ui.css
%{blocket}/www-ssl/css/user_ads.css
%{blocket}/www-ssl/css/var_dump.css
%{blocket}/www-ssl/css/base_responsive.css
%{blocket}/www-ssl/img/adview_calendar_bg_top.jpg
%{blocket}/www-ssl/img/arrow-orange.gif
%{blocket}/www-ssl/img/arrow-select.png
%{blocket}/www-ssl/img/bill-mail-bg-bottom.jpg
%{blocket}/www-ssl/img/bill-mail-bg-left.jpg
%{blocket}/www-ssl/img/bill-mail-bg-right.jpg
%{blocket}/www-ssl/img/bill-mail-bg-top.jpg
%{blocket}/www-ssl/img/blue-check.png
%{blocket}/www-ssl/img/blue-check@2x.png
%{blocket}/www-ssl/img/black.png
%{blocket}/www-ssl/img/black@2x.png
%{blocket}/www-ssl/img/blue.png
%{blocket}/www-ssl/img/blue@2x.png
%{blocket}/www-ssl/img/btn-left.png
%{blocket}/www-ssl/img/btn-right.png
%{blocket}/www-ssl/img/bullet.gif
%{blocket}/www-ssl/img/bump-icon-mail.png
%{blocket}/www-ssl/img/bump-mail.png
%{blocket}/www-ssl/img/car_default.jpg
%{blocket}/www-ssl/img/check-ok.gif
%{blocket}/www-ssl/img/contact-icon-mail.png
%{blocket}/www-ssl/img/cs_icons.gif
%{blocket}/www-ssl/img/delete-icon-mail.png
%{blocket}/www-ssl/img/edit-icon-mail.png
%{blocket}/www-ssl/img/edit_blue.gif
%{blocket}/www-ssl/img/email-check.gif
%{blocket}/www-ssl/img/email-clock.gif
%{blocket}/www-ssl/img/email-eye.gif
%{blocket}/www-ssl/img/email-user-icon.gif
%{blocket}/www-ssl/img/fancybox_overlay.png
%{blocket}/www-ssl/img/favicon.ico
%{blocket}/www-ssl/img/filmthumbeffect.png
%{blocket}/www-ssl/img/gradient-blue.png
%{blocket}/www-ssl/img/gradient-green.png
%{blocket}/www-ssl/img/gradient-mint.png
%{blocket}/www-ssl/img/gradient-orange.png
%{blocket}/www-ssl/img/gradient-purple.png
%{blocket}/www-ssl/img/gradient-red.png
%{blocket}/www-ssl/img/gradient-red_v2.png
%{blocket}/www-ssl/img/gradient-yellow.png
%{blocket}/www-ssl/img/green-check.png
%{blocket}/www-ssl/img/green-check@2x.png
%{blocket}/www-ssl/img/green.png
%{blocket}/www-ssl/img/house_default.jpg
%{blocket}/www-ssl/img/ico_locker.png
%{blocket}/www-ssl/img/ico_mail.png
%{blocket}/www-ssl/img/ico_phone.png
%{blocket}/www-ssl/img/icon-store.gif
%{blocket}/www-ssl/img/icon_securitycenter_footer.gif
%{blocket}/www-ssl/img/icon-gallery.png
%{blocket}/www-ssl/img/icons-backdrop.png
%{blocket}/www-ssl/img/info.png
%{blocket}/www-ssl/img/job_default.jpg
%{blocket}/www-ssl/img/li_no_thumbs.gif
%{blocket}/www-ssl/img/loader-light.gif
%{blocket}/www-ssl/img/logo_email_2x.gif
%{blocket}/www-ssl/img/sprite-label.png
%{blocket}/www-ssl/img/m_prod_default.png
%{blocket}/www-ssl/img/m_prod_car.png
%{blocket}/www-ssl/img/m_prod_inmo.png
%{blocket}/www-ssl/img/MapPoint_NoNumber.gif
%{blocket}/www-ssl/img/sprite-maps.png
%{blocket}/www-ssl/img/mynt-footer.gif
%{blocket}/www-ssl/img/no_image.gif
%{blocket}/www-ssl/img/one-icon-mail.png
%{blocket}/www-ssl/img/placeholder_chrome.png
%{blocket}/www-ssl/img/prissankt.gif
%{blocket}/www-ssl/img/prod_default.jpg
%{blocket}/www-ssl/img/product-manager-image.png
%{blocket}/www-ssl/img/s-center.gif
%{blocket}/www-ssl/img/share_on_fb.gif
%{blocket}/www-ssl/img/icon-deactivated.png
%{blocket}/www-ssl/img/store-annual-plan.gif
%{blocket}/www-ssl/img/store-biannual-plan.gif
%{blocket}/www-ssl/img/store-monthly-plan.gif
%{blocket}/www-ssl/img/store-quarterly-plan.gif
%{blocket}/www-ssl/img/tip-east-first.gif
%{blocket}/www-ssl/img/tip-east-second.gif
%{blocket}/www-ssl/img/tip-east-third.gif
%{blocket}/www-ssl/img/tnoise.png
%{blocket}/www-ssl/img/TryOutBg.jpg
%{blocket}/www-ssl/img/ui-icons.png
%{blocket}/www-ssl/img/user-types.gif
%{blocket}/www-ssl/img/wait.gif
%{blocket}/www-ssl/img/watermark.png
%{blocket}/www-ssl/img/map-provider-not-available.png
%{blocket}/www-ssl/img/circle-marker.png
%{blocket}/www-ssl/img/marker-icon.png
%{blocket}/www-ssl/img/marker-icon-2x.png
%{blocket}/www-ssl/img/watermark_small.png
%{blocket}/www-ssl/img/webpay-icon-mail.png
%{blocket}/www-ssl/img/wink.gif
%{blocket}/www-ssl/img/credit_logo.png
%{blocket}/www-ssl/img/ticket_yellow.png
%{blocket}/www-ssl/img/m_loader.gif
%{blocket}/www-ssl/img/m_loader_bg.gif
%{blocket}/www-ssl/js/all_pages.js
%{blocket}/www-ssl/js/banner-sticky.js
%{blocket}/www-ssl/js/common.js
%{blocket}/www-ssl/js/dashboard.js
%{blocket}/www-ssl/js/dashboard_bump.js
%{blocket}/www-ssl/js/events.js
%{blocket}/www-ssl/js/index.js
%{blocket}/www-ssl/js/list.js
%{blocket}/www-ssl/js/login.js
%{blocket}/www-ssl/js/m_dashboard.js
%{blocket}/www-ssl/js/m_dashboard_bump.js
%{blocket}/www-ssl/js/m_login.js
%{blocket}/www-ssl/js/mobile_reset_password.js
%{blocket}/www-ssl/js/xiti_yapo.js
%{blocket}/www-ssl/js/tag_tealium.js
%{blocket}/www-ssl/js/altiro-bundle.js
%{blocket}/www-ssl/js/yapo-hub.js
%{blocket}/www-ssl/js/google-gpt-config.js
%{blocket}/www-ssl/css/font-awesome.css

#NEXTGEN
%{blocket}/www-ssl/img/computadores.png
%{blocket}/www-ssl/img/empleos.png
%{blocket}/www-ssl/img/fashion-beauty-health.png
%{blocket}/www-ssl/img/futura-mama.png
%{blocket}/www-ssl/img/hogar.png
%{blocket}/www-ssl/img/inmuebles.png
%{blocket}/www-ssl/img/others.png
%{blocket}/www-ssl/img/tiempo-libre.png
%{blocket}/www-ssl/img/vehiculos.png

#FONTS
%{blocket}/www-ssl/fonts/yapo-icons.eot
%{blocket}/www-ssl/fonts/yapo-icons.svg
%{blocket}/www-ssl/fonts/yapo-icons.ttf
%{blocket}/www-ssl/fonts/yapo-icons.woff
%{blocket}/www-ssl/webfonts/fa-light-300.eot
%{blocket}/www-ssl/webfonts/fa-light-300.svg
%{blocket}/www-ssl/webfonts/fa-light-300.ttf
%{blocket}/www-ssl/webfonts/fa-light-300.woff
%{blocket}/www-ssl/webfonts/fa-light-300.woff2
%{blocket}/www-ssl/webfonts/fa-solid-900.eot
%{blocket}/www-ssl/webfonts/fa-solid-900.svg
%{blocket}/www-ssl/webfonts/fa-solid-900.ttf
%{blocket}/www-ssl/webfonts/fa-solid-900.woff
%{blocket}/www-ssl/webfonts/fa-solid-900.woff2

# ACCOUNTS
%{blocket}/www-ssl/css/icheckblue.css
%{blocket}/www-ssl/css/m_reset_password.css
%{blocket}/www-ssl/img/sprite-accounts.png
%{blocket}/www-ssl/img/mobile_accounts.png
%{blocket}/www-ssl/img/mobile_accounts2.png
%{blocket}/www-ssl/js/create_account.js
%{blocket}/www-ssl/js/dashboard_account.js
%{blocket}/www-ssl/js/icheck.js
%{blocket}/www-ssl/img/mail-activate-account.png
%{blocket}/www-ssl/img/mail-recovery-pass.png
%{blocket}/www-ssl/css/delete_reasons.css
%{blocket}/www-ssl/css/m_my_ads.css
%{blocket}/www-ssl/img/sprite-bump.png
%{blocket}/www-ssl/css/accounts.css
%{blocket}/www-ssl/css/dashboard.css
%{blocket}/www-ssl/css/m_accounts.css
%{blocket}/www-ssl/css/m_dashboard.css
%{blocket}/www-ssl/js/m_accounts.js
%{blocket}/www-ssl/css/m_dashboard_pay.css
%{blocket}/www-ssl/css/m_payment.css
%{blocket}/www-ssl/css/m_payment_success.css
%{blocket}/www-ssl/img/sprite-products-2x.png
%{blocket}/www-ssl/img/sprite-products.png
%{blocket}/www-ssl/img/webpay-icon.png
%{blocket}/www-ssl/css/icon_catalog.css
%{blocket}/www-ssl/img/stores-listing-bg-others.jpg
%{blocket}/www-ssl/img/stores-listing-bg-realestate.jpg
%{blocket}/www-ssl/img/stores-listing-bg-vehicles.jpg
%{blocket}/www-ssl/img/stores-main-image-others.png
%{blocket}/www-ssl/img/stores-main-image-realestate.png
%{blocket}/www-ssl/img/stores-main-image-vehicles.png
%{blocket}/www-ssl/img/yapo_fb.png
%{blocket}/www-ssl/js/icon_catalog.js
%{blocket}/www-ssl/js/store_buy.js
%{blocket}/www-ssl/js/store_edition.js
%{blocket}/www-ssl/js/store_list.js
%{blocket}/www-ssl/swf/ZeroClipboard.swf
%{blocket}/www-ssl/css/m_login.css
%{blocket}/www-ssl/js/jobs.js
%{blocket}/www-ssl/css/jobs.css

# PACKS
%{blocket}/www-ssl/img/pack-feature-chart.gif
%{blocket}/www-ssl/img/pack-feature-clock.gif
%{blocket}/www-ssl/img/pack-feature-support.gif
%{blocket}/www-ssl/img/pack-feature-contacts.png
%{blocket}/www-ssl/img/pack-icon-email.gif
%{blocket}/www-ssl/img/da_success_active.png
%{blocket}/www-ssl/img/da_success_active@2x.png
%{blocket}/www-ssl/img/da_success_inactive.png
%{blocket}/www-ssl/img/da_success_inactive@2x.png
%{blocket}/www-ssl/img/da_success_inactive_red.png
%{blocket}/www-ssl/img/da_success_inactive_red@2x.png
%{blocket}/www-ssl/css/ad_insert_success.css
%{blocket}/www-ssl/img/pack-icon-email-warning.gif
%{blocket}/www-ssl/img/pack-mail-ad-status-active.gif
%{blocket}/www-ssl/img/pack-mail-ad-status-inactive.gif
%{blocket}/www-ssl/img/pack-warning.gif
%{blocket}/www-ssl/img/pack-reminder-mail.gif

# UPSELLING
%{blocket}/www-ssl/css/bids.css
%{blocket}/www-ssl/css/buyfiles.css
%{blocket}/www-ssl/css/m_add_success.css
%{blocket}/www-ssl/css/m_ai_success.css
%{blocket}/www-ssl/css/pay.css
%{blocket}/www-ssl/img/sprite-m-upselling-2x.png
%{blocket}/www-ssl/img/sprite-m-upselling.png
%{blocket}/www-ssl/img/sprite-upselling.png
%{blocket}/www-ssl/js/ai_success.js
%{blocket}/www-ssl/img/refund-success-check.gif
%{blocket}/www-ssl/img/tip-success-box.gif

#Promotional
%{blocket}/www-ssl/css/promotional.css
%{blocket}/www-ssl/img/banner_mueble.jpg
%{blocket}/www-ssl/js/promotional.js
%{blocket}/www-ssl/img/pp_without_results.png
%{blocket}/www-ssl/img/pp_searchIcon.png
%{blocket}/www-ssl/img/logo_transparent.png

%{blocket}/www-ssl/css/credits.css
%{blocket}/www-ssl/js/credits.js

#Landing AF
%{blocket}/www-ssl/css/landingAutofact.css

#Yapo Header
%{blocket}/www-ssl/css/yapo-drawer.css
%{blocket}/www-ssl/js/yapo-header-utils.js

#Merken
%{blocket}/www-ssl/js/viewListing.js
%{blocket}/www-ssl/css/viewListing.css

%{blocket}/www-ssl/img/accepted-main-icon.png
%{blocket}/www-ssl/img/active_ad_icon.png
%{blocket}/www-ssl/img/autofact-logo-small.jpg
%{blocket}/www-ssl/img/banner_mueble.png
%{blocket}/www-ssl/img/bg-autofact-bg.png
%{blocket}/www-ssl/img/bg-autofact.png
%{blocket}/www-ssl/img/computadores-220.png
%{blocket}/www-ssl/img/computadores-224.png
%{blocket}/www-ssl/img/cube-yapo-white.svg
%{blocket}/www-ssl/img/delete-icon-black.png
%{blocket}/www-ssl/img/delete-modal.svg
%{blocket}/www-ssl/img/edit-icon-black.png
%{blocket}/www-ssl/img/empleos-220.png
%{blocket}/www-ssl/img/empleos-224.png
%{blocket}/www-ssl/img/facebook-share-button.png
%{blocket}/www-ssl/img/fashion-beauty-health-220.png
%{blocket}/www-ssl/img/fashion-beauty-health-224.png
%{blocket}/www-ssl/img/five-stars-yellow.svg
%{blocket}/www-ssl/img/footerPattern.svg
%{blocket}/www-ssl/img/futura-mama-220.png
%{blocket}/www-ssl/img/galleryModal.png
%{blocket}/www-ssl/img/galleryModalSmall.png
%{blocket}/www-ssl/img/heart.svg
%{blocket}/www-ssl/img/hogar-220.png
%{blocket}/www-ssl/img/hogar-224.png
%{blocket}/www-ssl/img/home/yapo_home_1.png
%{blocket}/www-ssl/img/home/yapo_home_2.png
%{blocket}/www-ssl/img/home/yapo_home_3.png
%{blocket}/www-ssl/img/home/yapo_home_4.png
%{blocket}/www-ssl/img/home/yapo_home_5.png
%{blocket}/www-ssl/img/home/yapo_home_6.png
%{blocket}/www-ssl/img/home/yapo_home_7.png
%{blocket}/www-ssl/img/home/yapo_home_8.png
%{blocket}/www-ssl/img/ico_borrados.png
%{blocket}/www-ssl/img/ico_check.png
%{blocket}/www-ssl/img/ico_error.png
%{blocket}/www-ssl/img/iconAvatarFilter.svg
%{blocket}/www-ssl/img/iconCheck.svg
%{blocket}/www-ssl/img/iconClose.svg
%{blocket}/www-ssl/img/iconCloseMenu.svg
%{blocket}/www-ssl/img/iconCloseMenuGray.svg
%{blocket}/www-ssl/img/iconDefaultAvatar.svg
%{blocket}/www-ssl/img/iconFacebook.svg
%{blocket}/www-ssl/img/iconFacebookCircle.svg
%{blocket}/www-ssl/img/iconFavorite.svg
%{blocket}/www-ssl/img/iconFilters.svg
%{blocket}/www-ssl/img/iconLogoYapo.svg
%{blocket}/www-ssl/img/iconLoweredPrice.svg
%{blocket}/www-ssl/img/iconMyCredits.svg
%{blocket}/www-ssl/img/iconSearch.svg
%{blocket}/www-ssl/img/iconSelectArrow.svg
%{blocket}/www-ssl/img/iconShare.svg
%{blocket}/www-ssl/img/iconShareEmail.svg
%{blocket}/www-ssl/img/iconShareFacebook.svg
%{blocket}/www-ssl/img/iconShareTwitter.svg
%{blocket}/www-ssl/img/iconUserMenu.svg
%{blocket}/www-ssl/img/inmuebles-220.png
%{blocket}/www-ssl/img/inmuebles-224.png
%{blocket}/www-ssl/img/jobTopBanner.svg
%{blocket}/www-ssl/img/jobsSuitIcon.svg
%{blocket}/www-ssl/img/loginFacebook.svg
%{blocket}/www-ssl/img/loginPattern.png
%{blocket}/www-ssl/img/loginPattern.svg
%{blocket}/www-ssl/img/loginUser.svg
%{blocket}/www-ssl/img/loginYapo.svg
%{blocket}/www-ssl/img/mail_pri_to_pro.png
%{blocket}/www-ssl/img/mamas-224.png
%{blocket}/www-ssl/img/noMessages.png
%{blocket}/www-ssl/img/others-220.png
%{blocket}/www-ssl/img/others-224.png
%{blocket}/www-ssl/img/paginationFirstIcon.svg
%{blocket}/www-ssl/img/paginationLastIcon.svg
%{blocket}/www-ssl/img/paginationNextIcon.svg
%{blocket}/www-ssl/img/paginationPrevIcon.svg
%{blocket}/www-ssl/img/social-icon-facebook.svg
%{blocket}/www-ssl/img/social-icon-twitter.svg
%{blocket}/www-ssl/img/social-icon-whatsapp.svg
%{blocket}/www-ssl/img/social-icon-email.svg
%{blocket}/www-ssl/img/splash-screen-1.png
%{blocket}/www-ssl/img/splash-screen-2.png
%{blocket}/www-ssl/img/splash-screen-3.png
%{blocket}/www-ssl/img/sprite-khipu.png
%{blocket}/www-ssl/img/tiempo-libre-220.png
%{blocket}/www-ssl/img/tiempo-libre-224.png
%{blocket}/www-ssl/img/transparent.gif
%{blocket}/www-ssl/img/transparent.png
%{blocket}/www-ssl/img/twitter-share-button.png
%{blocket}/www-ssl/img/vehiculos-220.png
%{blocket}/www-ssl/img/vehiculos-224.png
%{blocket}/www-ssl/img/controls.png
%{blocket}/www-ssl/img/bx_loader.gif
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-arrow-left.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-arrow-right.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-bump-icon.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-bump-preview.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-close-button-grey.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-close-button-white.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-combo-bump.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-combo-gallery.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-combo-icon.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-combo-label.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-gallery-icon.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-gallery-monitor.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-gallery-preview.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-gallery-shop.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-gallery-visibility.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-index-bump.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-index-gallery.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-index-label.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-index-packs.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-label-icon.svg
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-label-options.png
%{blocket}/www-ssl/img/products_info_slider/products-info-slider-label-preview.png

%{blocket}/www-ssl/css/m_credits.css
%{blocket}/www-ssl/js/m_credits.js

# mail images

%{blocket}/www-ssl/img/email_responsive/centro-ayuda-icon.png
%{blocket}/www-ssl/img/email_responsive/centro-ayuda-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/centro-ayuda-icon@3x.png
%{blocket}/www-ssl/img/email_responsive/edit-icon.png
%{blocket}/www-ssl/img/email_responsive/edit-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/edit-icon@3x.png
%{blocket}/www-ssl/img/email_responsive/facebook-circle-icon.png
%{blocket}/www-ssl/img/email_responsive/facebook-circle-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/facebook-circle-icon@3x.png
%{blocket}/www-ssl/img/email_responsive/google-playstore.png
%{blocket}/www-ssl/img/email_responsive/google-playstore@2x.png
%{blocket}/www-ssl/img/email_responsive/google-playstore@3x.png
%{blocket}/www-ssl/img/email_responsive/group-2.png
%{blocket}/www-ssl/img/email_responsive/group-2@2x.png
%{blocket}/www-ssl/img/email_responsive/group-2@3x.png
%{blocket}/www-ssl/img/email_responsive/icono.png
%{blocket}/www-ssl/img/email_responsive/icono@2x.png
%{blocket}/www-ssl/img/email_responsive/icono@3x.png
%{blocket}/www-ssl/img/email_responsive/imported-layers-copy-6.png
%{blocket}/www-ssl/img/email_responsive/imported-layers-copy-6@2x.png
%{blocket}/www-ssl/img/email_responsive/imported-layers-copy-6@3x.png
%{blocket}/www-ssl/img/email_responsive/instagram.png
%{blocket}/www-ssl/img/email_responsive/instagram@2x.png
%{blocket}/www-ssl/img/email_responsive/instagram@3x.png
%{blocket}/www-ssl/img/email_responsive/itunes-appstore.png
%{blocket}/www-ssl/img/email_responsive/itunes-appstore@2x.png
%{blocket}/www-ssl/img/email_responsive/itunes-appstore@3x.png
%{blocket}/www-ssl/img/email_responsive/sad-face.png
%{blocket}/www-ssl/img/email_responsive/sad-face@2x.png
%{blocket}/www-ssl/img/email_responsive/sad-face@3x.png
%{blocket}/www-ssl/img/email_responsive/telefono-icon.png
%{blocket}/www-ssl/img/email_responsive/telefono-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/telefono-icon@3x.png
%{blocket}/www-ssl/img/email_responsive/twitter-4-icon.png
%{blocket}/www-ssl/img/email_responsive/twitter-4-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/twitter-4-icon@3x.png
%{blocket}/www-ssl/img/email_responsive/youtube-new-4-icon.png
%{blocket}/www-ssl/img/email_responsive/youtube-new-4-icon@2x.png
%{blocket}/www-ssl/img/email_responsive/youtube-new-4-icon@3x.png

%config %{blocket}/conf/httpd.conf
%config %{blocket}/conf/rewrite.conf
%config %{blocket}/conf/magic
%config %{blocket}/conf/mime.types
%config %{blocket}/conf/mod_sec_global.conf
%config %{blocket}/conf/mod_sec_apig.conf
%config %{blocket}/conf/httpd_spiderpoints.conf
%config %{blocket}/conf/searchhits.conf
%config %{blocket}/conf/errorpages.conf
%config %{blocket}/conf/ssl_static.conf
%config %{blocket}/conf/mod_headers_cors.conf

%files mobile-api
%config %{blocket}/conf/api.conf
%config %{blocket}/conf/httpd_api.conf
%config %{blocket}/conf/httpd_api_newad.conf
%config %{blocket}/conf/mod_sec_api_yapo.conf
%config %{blocket}/conf/mod_sec_api_adwatch.conf
%config %{blocket}/conf/mod_sec_api.conf
%config %{blocket}/conf/mod_sec_api_newad.conf
%config %{blocket}/conf/msite_modsec.conf

%files newad
%defattr(-,root,root)
%{blocket}/apps/blocket_newad.php
%config %{blocket}/conf/mod_sec_ai.conf
%{blocket}/www-ssl/css/ai.css
%{blocket}/www-ssl/js/category_dropdown.js
%{blocket}/www-ssl/js/ai.js
%{blocket}/www-ssl/js/preview.js
%{blocket}/www-ssl/apple-touch-icon.png
%{blocket}/www-ssl/apple-touch-icon-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-114x114-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-114x114.png
%{blocket}/www-ssl/apple-touch-icon-57x57-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-57x57.png
%{blocket}/www-ssl/apple-touch-icon-72x72-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-72x72.png
%{blocket}/www-ssl/apple-touch-icon-144x144-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-144x144.png
%{blocket}/www-ssl/apple-touch-icon-120x120-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-120x120.png
%{blocket}/www-ssl/apple-touch-icon-152x152-precomposed.png
%{blocket}/www-ssl/apple-touch-icon-76x76-precomposed.png
%{blocket}/www-ssl/img/arrow-tooltip.png
%{blocket}/www-ssl/img/green_loading.gif
%{blocket}/www-ssl/img/ico_sucessov.png
%{blocket}/www-ssl/img/list_bullet_gray.png

%files paysim
%defattr(-,root,root)
%{blocket}/apps/paysim.php
%{blocket}/apps/billsim.php
%{blocket}/apps/billsim_auth.php
%{blocket}/apps/billsim_common.php
%{blocket}/apps/billsim_get_pdf.php
%{blocket}/apps/billsim_load.php
%{blocket}/apps/billsim_sales.php
%{blocket}/apps/bill.pdf
%{blocket}/include/nusoap/class.nusoap_base.php
%{blocket}/include/nusoap/class.soap_fault.php
%{blocket}/include/nusoap/class.soap_parser.php
%{blocket}/include/nusoap/class.soap_server.php
%{blocket}/include/nusoap/class.soap_transport_http.php
%{blocket}/include/nusoap/class.soap_val.php
%{blocket}/include/nusoap/class.soapclient.php
%{blocket}/include/nusoap/class.wsdl.php
%{blocket}/include/nusoap/class.wsdlcache.php
%{blocket}/include/nusoap/class.xmlschema.php
%{blocket}/include/nusoap/nusoap.php
%{blocket}/include/nusoap/nusoapmime.php
%config %{blocket}/conf/mod_sec_paysim.conf
%config %{blocket}/conf/mod_sec_billsim.conf
%{blocket}/conf/regress-servipag-private.key
%{blocket}/conf/regress-servipag-public.key

%files senderror
%defattr(-,root,root)
%{blocket}/apps/senderror.php
%config %{blocket}/conf/mod_sec_se.conf

%files bmemcache
%defattr(-,root,root)
%{blocket}/etc/init.d/bmemcached
%config %{blocket}/conf/bmemcached/sessions.conf

%files sendpass
%defattr(-,root,root)
%{blocket}/apps/sendpass.php
%config %{blocket}/conf/mod_sec_sp.conf

%files adreply
%defattr(-,root,root)
%{blocket}/apps/adreply.php
%config %{blocket}/conf/mod_sec_ar.conf

%files adwatch
%defattr(-,root,root)
%{blocket}/apps/adwatch.php
%config %{blocket}/conf/mod_sec_adwatch.conf
%{blocket}/www-ssl/css/adwatch.css
%{blocket}/www-ssl/js/adwatch.js
%{blocket}/www-ssl/img/favourites_save.gif
%{blocket}/www-ssl/img/crea_busqueda.gif
%{blocket}/www-ssl/img/favourites_search.gif
%{blocket}/www-ssl/img/add_watch.gif
%{blocket}/www-ssl/img/tab-top-blue-inactive.png
%{blocket}/www-ssl/img/tab-top-blue.png
%{blocket}/www-ssl/img/tab-top-mint-inactive.png
%{blocket}/www-ssl/img/tab-top-mint.png
%{blocket}/www-ssl/img/tab-top-green-inactive.png
%{blocket}/www-ssl/img/tab-top-green.png
%{blocket}/www-ssl/img/tab-top-orange-inactive.png
%{blocket}/www-ssl/img/tab-top-orange.png
%{blocket}/www-ssl/img/tab-top-purple-inactive.png
%{blocket}/www-ssl/img/tab-top-purple.png
%{blocket}/www-ssl/img/tab-top-red-inactive.png
%{blocket}/www-ssl/img/tab-top-red.png
%{blocket}/www-ssl/img/tab-top-white-inactive.png
%{blocket}/www-ssl/img/tab-top-white.png
%{blocket}/www-ssl/img/tab-top-yellow-inactive.png
%{blocket}/www-ssl/img/tab-top-yellow.png
%{blocket}/www-ssl/img/tab-border-bottom.png
%{blocket}/www-ssl/img/header_sms.jpg
%{blocket}/www-ssl/img/search-favorites-icon.svg

%{blocket}/www-ssl/img/sms_example_bg.gif
%{blocket}/www-ssl/img/sms.gif

%files ajax
%defattr(-,root,root)
%{blocket}/apps/ajax.php
%config %{blocket}/conf/mod_sec_ajax.conf

%files trans
%defattr(-,root,root)
%{blocket}/bin/trans
%{blocket}/bin/transinfo
%{blocket}/etc/init.d/trans
%{blocket}/bin/generate_adcodes.pl
%{blocket}/libexec/voucher_monthly_report.php
%{blocket}/share/img/logo.png
%config %{blocket}/conf/trans.conf

%files bconfd
%defattr(-,root,root)
%{blocket}/bin/bconfd
%{blocket}/etc/init.d/bconfd
%config %{blocket}/conf/bconfd.conf

%files filterd
%defattr(-,root,root)
%{blocket}/bin/filterd
%{blocket}/etc/init.d/filterd
%config %{blocket}/conf/filterd.conf


%files bconf
%defattr(-,root,root)
%config %{blocket}/conf/bconf.txt
%config %{blocket}/conf/bconf.txt.abusetool
%config %{blocket}/conf/bconf.txt.action_params
%config %{blocket}/conf/bconf.txt.adparams
%config %{blocket}/conf/bconf.txt.adsense
%config %{blocket}/conf/bconf.txt.google_gpt
%config %{blocket}/conf/bconf.txt.banners_common
%config %{blocket}/conf/bconf.txt.adstatus
%config %{blocket}/conf/bconf.txt.adview
%config %{blocket}/conf/bconf.txt.adwatch
%config %{blocket}/conf/bconf.txt.ais
%config %{blocket}/conf/bconf.txt.alexa
%config %{blocket}/conf/bconf.txt.alarms
%config %{blocket}/conf/bconf.txt.api
%config %{blocket}/conf/bconf.txt.api.ad_params
%config %{blocket}/conf/bconf.txt.api.custom_templates
%config %{blocket}/conf/bconf.txt.api.redis
%config %{blocket}/conf/bconf.txt.api.templates
%config %{blocket}/conf/bconf.txt.areas
%config %{blocket}/conf/bconf.txt.blockedip
%config %{blocket}/conf/bconf.txt.breadcrumb
%config %{blocket}/conf/bconf.txt.bubbles
%config %{blocket}/conf/bconf.txt.campaigns
%config %{blocket}/conf/bconf.txt.categories
%config %{blocket}/conf/bconf.txt.communes
%config %{blocket}/conf/bconf.txt.controlpanel
%config %{blocket}/conf/bconf.txt.cotiza
%config %{blocket}/conf/bconf.txt.countries
%config %{blocket}/conf/bconf.txt.criteo
%config %{blocket}/conf/bconf.txt.destacados
%config %{blocket}/conf/bconf.txt.device_templates
%config %{blocket}/conf/bconf.txt.exports
%config %{blocket}/conf/bconf.txt.facebook_login
%config %{blocket}/conf/bconf.txt.favorite
%config %{blocket}/conf/bconf.txt.firefox_app
%config %{blocket}/conf/bconf.txt.first_exp
%config %{blocket}/conf/bconf.txt.gallery
%config %{blocket}/conf/bconf.txt.help
%config %{blocket}/conf/bconf.txt.image
%config %{blocket}/conf/bconf.txt.imports
%config %{blocket}/conf/bconf.txt.inject
%config %{blocket}/conf/bconf.txt.insertingfee
%config %{blocket}/conf/bconf.txt.jsevents
%config %{blocket}/conf/bconf.txt.jsvalidators
%config %{blocket}/conf/bconf.txt.labels
%config %{blocket}/conf/bconf.txt.language
%config %{blocket}/conf/bconf.txt.language_common
%config %{blocket}/conf/bconf.txt.language_merken
%config %{blocket}/conf/bconf.txt.aria_label
%config %{blocket}/conf/bconf.txt.listing
%config %{blocket}/conf/bconf.txt.maps
%config %{blocket}/conf/bconf.txt.merken
%config %{blocket}/conf/bconf.txt.messaging-center
%config %{blocket}/conf/bconf.txt.msite
%config %{blocket}/conf/bconf.txt.msite.language
%config %{blocket}/conf/bconf.txt.msite.regions
%config %{blocket}/conf/bconf.txt.msite.templates
%config %{blocket}/conf/bconf.txt.newad
%config %{blocket}/conf/bconf.txt.oneclick
%config %{blocket}/conf/bconf.txt.partners
%config %{blocket}/conf/bconf.txt.payment
%config %{blocket}/conf/bconf.txt.payment_common
%config %{blocket}/conf/bconf.txt.payment_products
%config %{blocket}/conf/bconf.txt.phonelist
%config %{blocket}/conf/bconf.txt.qs
%config %{blocket}/conf/bconf.txt.rabbitmq
%config %{blocket}/conf/bconf.txt.redis_cardata
%config %{blocket}/conf/bconf.txt.redis_wordsranking
%config %{blocket}/conf/bconf.txt.regions
%config %{blocket}/conf/bconf.txt.safe
%config %{blocket}/conf/bconf.txt.search
%config %{blocket}/conf/bconf.txt.searchtips
%config %{blocket}/conf/bconf.txt.seo
%config %{blocket}/conf/bconf.txt.servipag
%config %{blocket}/conf/bconf.txt.site
%config %{blocket}/conf/bconf.txt.stats
%config %{blocket}/conf/bconf.txt.stats1x1
%config %{blocket}/conf/bconf.txt.stores
%config %{blocket}/conf/bconf.txt.support
%config %{blocket}/conf/bconf.txt.tagmanager
%config %{blocket}/conf/bconf.txt.tealium
%config %{blocket}/conf/bconf.txt.templates
%config %{blocket}/conf/bconf.txt.upselling_products
%config %{blocket}/conf/bconf.txt.upselling_products_common
%config %{blocket}/conf/bconf.txt.validators
%config %{blocket}/conf/bconf.txt.voucher
%config %{blocket}/conf/bconf.txt.wurfl
%config %{blocket}/conf/bconf.txt.xiti
# NGA API
%config %{blocket}/conf/bconf.txt.redis_nextgen_api
%config %{blocket}/conf/coord_api.bconf
%config %{blocket}/conf/coord_api.filter_settings.bconf
%config %{blocket}/conf/coord_api.validators.bconf
# Refund
%config %{blocket}/conf/bconf.txt.refund
%config %{blocket}/conf/bconf.txt.delete2bump
%config %{blocket}/conf/bconf.txt.landing
%config %{blocket}/conf/bconf.txt.banners
# Ppages
%config %{blocket}/conf/bconf.txt.ppages

%config %{blocket}/conf/bconf.txt.payment_rest
%config %{blocket}/conf/bconf.txt.autobump
%config %{blocket}/conf/bconf.txt.credits
%config %{blocket}/conf/bconf.txt.bifrost
%config %{blocket}/conf/bconf.txt.proxy
%config %{blocket}/conf/bconf.txt.bulkload
%config %{blocket}/conf/bconf.txt.commune2region
%config %{blocket}/conf/bconf.txt.smartbump
%config %{blocket}/conf/bconf.txt.carousel
%config %{blocket}/conf/bconf.txt.rewards
%config %{blocket}/conf/bconf.txt.claim_product
%config %{blocket}/conf/bconf.txt.loan_manager
# Merken
%config %{blocket}/conf/bconf.txt.merken_api
%config %{blocket}/conf/bconf.txt.xiti_merken
# Move to Apps
%config %{blocket}/conf/bconf.txt.move2apps

%files search-common
%defattr(-,root,root)
%{blocket}/bin/search
%{blocket}/bin/indexer
%{blocket}/bin/index_ctl
%{blocket}/bin/index_merge
%{blocket}/bin/index_stats
%{blocket}/bin/sof
%{blocket}/bin/hunstemchk
%{blocket}/conf/charmap.es
%{blocket}/share/search/es_CL.dic
%{blocket}/share/search/es_CL.aff

%files search-asearch
%defattr(-,root,root)
%config %{blocket}/conf/asearch.conf
%{blocket}/etc/init.d/asearch
%{blocket}/bin/aindex_update.sh

%files search-csearch
%defattr(-,root,root)
%config %{blocket}/conf/csearch.conf
%{blocket}/etc/init.d/csearch

%files search-msearch
%defattr(-,root,root)
%config %{blocket}/conf/msearch.conf
%{blocket}/etc/init.d/msearch

%files search-josesearch
%defattr(-,root,root)
%config %{blocket}/conf/josesearch.conf
%{blocket}/etc/init.d/josesearch

%files dav_put
%defattr(-,root,root)
%config %{blocket}/conf/img_rep.conf
%config %{blocket}/conf/dav_put.conf
%{blocket}/etc/init.d/dav_replay
%{blocket}/libexec/dav_put
%{blocket}/libexec/dav_replay.py

%files mod_image
%defattr(-,root,root)
%config %{blocket}/conf/mod_image.conf
%{blocket}/modules/mod_image.so

%files bcli
%defattr(-,root,root)
%{blocket}/bin/bcli

%files redir
%defattr(-,root,root)
%config %{blocket}/conf/redir.conf
%config %{blocket}/conf/mod_sec_redir.conf

%files controlpanel
%defattr(-,root,root)
%{blocket}/apps/controlpanel.php
%{blocket}/apps/cp_product.php
%{blocket}/include/Cpanel.php
%{blocket}/include/CpanelModule.php
%{blocket}/include/CpanelModuleAccounts.php
%{blocket}/include/CpanelModuleAdminuf.php
%{blocket}/include/CpanelModulePartners.php
%{blocket}/include/CpanelModulePPages.php
%{blocket}/include/CpanelModuleRefund.php
%{blocket}/include/CpanelModuleWebsql.php
%{blocket}/include/CpanelModuleOpMsg.php
%{blocket}/include/CpanelModuleServices.php
%{blocket}/include/CpanelModuleStores.php
%{blocket}/include/CpanelModuleSubscriptions.php
%{blocket}/include/CpanelModuleCarousel.php
%{blocket}/include/CpanelModuleRewards.php
%{blocket}/include/CpanelModuleLoanManager.php
%{blocket}/include/CpanelModuleClaimProduct.php
%{blocket}/include/CpanelModulePaymentDelivery.php
%{blocket}/include/CpanelModuleSmartbump.php
%{blocket}/include/CpanelModuleHomeLinks.php
%{blocket}/include/CpanelModulePacks.php
%{blocket}/include/CpanelModuleBanners.php
%{blocket}/include/CpanelModuleYapofact.php
%{blocket}/include/cpanel_module.php
%{blocket}/include/cpanel_module_admin.php
%{blocket}/include/cpanel_module_adminad.php
%{blocket}/include/cpanel_module_adqueue.php
%{blocket}/include/cpanel_module_ais.php
%{blocket}/include/cpanel_module_filter.php
%{blocket}/include/cpanel_module_landing_page.php
%{blocket}/include/cpanel_module_yapesos.php
%{blocket}/include/YapesosReport.php
%{blocket}/include/AssignedYapesosReport.php
%{blocket}/include/cpanel_module_popular_ads.php
%{blocket}/include/cpanel_module_scarface.php
%{blocket}/include/cpanel_module_search.php
%{blocket}/include/cpanel_module_sms.php
%{blocket}/include/cpanel_module_telesales.php
%{blocket}/include/holidays2007-2010.ics
%{blocket}/include/Ppage.php
%{blocket}/include/PpImage.php
%{blocket}/include/PpageBanner.php
%{blocket}/include/Util.php
%{blocket}/include/CsvExporter.php
%{blocket}/include/PartnerRulesConfig.php
%{blocket}/include/PaymentDelivery.php
%{blocket}/include/PaymentScheduleApiClient.php
%{blocket}/include/StaticPartnerRulesConfig.php
%{blocket}/include/SubscriptionsHelper.php
%{blocket}/include/BulkLoad/BulkLoadValidator.php
%{blocket}/include/BulkLoad/BulkLoadRule.php
%{blocket}/include/BulkLoad/ValidatorFactory.php
%{blocket}/include/BulkLoad/Conditional.php
%{blocket}/include/BulkLoad/Constrain.php
%{blocket}/include/BulkLoad/Multi.php
%{blocket}/include/BulkLoad/Remove.php
%{blocket}/include/BulkLoad/Simple.php
%{blocket}/include/BulkLoad/OneToMany.php
%{blocket}/www-ssl/css/Account.css
%{blocket}/www-ssl/css/adqueue.css
%{blocket}/www-ssl/css/ais.css
%{blocket}/www-ssl/css/Banners.css
%{blocket}/www-ssl/css/controlpanel.css
%{blocket}/www-ssl/css/Services.css
%{blocket}/www-ssl/css/Smartbump.css
%{blocket}/www-ssl/css/Carousel.css
%{blocket}/www-ssl/css/HomeLinks.css
%{blocket}/www-ssl/css/Rewards.css
%{blocket}/www-ssl/css/LoanManager.css
%{blocket}/www-ssl/css/filter.css
%{blocket}/www-ssl/css/landing_page.css
%{blocket}/www-ssl/css/on_call.css
%{blocket}/www-ssl/css/OpMsg.css
%{blocket}/www-ssl/css/yapesos.css
%{blocket}/www-ssl/css/Yapofact.css
%{blocket}/www-ssl/css/Partners.css
%{blocket}/www-ssl/css/Packs.css
%{blocket}/www-ssl/css/pikaday.css
%{blocket}/www-ssl/css/popular_ads.css
%{blocket}/www-ssl/css/PaymentDelivery.css
%{blocket}/www-ssl/css/PPages.css
%{blocket}/www-ssl/css/Refund.css
%{blocket}/www-ssl/css/search.css
%{blocket}/www-ssl/css/sms.css
%{blocket}/www-ssl/css/Stores.css
%{blocket}/www-ssl/css/support.css
%{blocket}/www-ssl/css/Subscriptions.css
%{blocket}/www-ssl/css/telesales.css
%{blocket}/www-ssl/img/FadingBorderLeftBackground.gif
%{blocket}/www-ssl/img/FadingBorderLeftTab.gif
%{blocket}/www-ssl/img/FadingBorderMiddle.gif
%{blocket}/www-ssl/img/FadingBorderRightBackground.gif
%{blocket}/www-ssl/img/FadingBorderTab.gif
%{blocket}/www-ssl/img/FadingBorderTab2.gif
%{blocket}/www-ssl/img/arrow_down.gif
%{blocket}/www-ssl/img/arrow_up.gif
%{blocket}/www-ssl/img/car_warn.png
%{blocket}/www-ssl/img/checked_green.gif
%{blocket}/www-ssl/img/country_flags.png
%{blocket}/www-ssl/img/creditcard.gif
%{blocket}/www-ssl/img/dublett5.gif
%{blocket}/www-ssl/img/notice_grey_bg.gif
%{blocket}/www-ssl/img/notice_grey_bottom.gif
%{blocket}/www-ssl/img/notice_grey_top.gif
%{blocket}/www-ssl/img/notice_mini_abuse.gif
%{blocket}/www-ssl/img/notice_mini_block.gif
%{blocket}/www-ssl/img/notice_mini_normal.gif
%{blocket}/www-ssl/img/notice_purple_bg.gif
%{blocket}/www-ssl/img/notice_purple_bottom.gif
%{blocket}/www-ssl/img/notice_purple_top.gif
%{blocket}/www-ssl/img/notice_red_bg.gif
%{blocket}/www-ssl/img/notice_red_bottom.gif
%{blocket}/www-ssl/img/notice_red_top.gif
%{blocket}/www-ssl/img/notice_white_bg.gif
%{blocket}/www-ssl/img/notice_white_bottom.gif
%{blocket}/www-ssl/img/notice_white_top.gif
%{blocket}/www-ssl/img/notice_yellow_bg.gif
%{blocket}/www-ssl/img/notice_yellow_bottom.gif
%{blocket}/www-ssl/img/notice_yellow_top.gif
%{blocket}/www-ssl/img/warn_adqueue.gif
%{blocket}/www-ssl/img/warning.gif
%{blocket}/www-ssl/js/adqueues.js
%{blocket}/www-ssl/js/PPages.js
%{blocket}/www-ssl/js/ppages_banners.js
%{blocket}/www-ssl/js/image_upload.js
%{blocket}/www-ssl/js/banners.js
%{blocket}/www-ssl/js/category_dropdown.js
%{blocket}/www-ssl/js/controlpanel.js
%{blocket}/www-ssl/js/controlpanel_pages.js
%{blocket}/www-ssl/js/jquery-1_4_2.js
%{blocket}/www-ssl/js/moment.min.js
%{blocket}/www-ssl/js/op_msg.js
%{blocket}/www-ssl/js/yapesos.js
%{blocket}/www-ssl/js/yapofact.js
%{blocket}/www-ssl/js/Partners.js
%{blocket}/www-ssl/js/packs.js
%{blocket}/www-ssl/js/pikaday.js
%{blocket}/www-ssl/js/Refund.js
%{blocket}/www-ssl/js/right_click_menu.js
%{blocket}/www-ssl/js/search_reviewers.js
%{blocket}/www-ssl/js/telesales.js
%{blocket}/www-ssl/js/Subscriptions.js
%{blocket}/www-ssl/js/PaymentDelivery.js
%{blocket}/www-ssl/js/Services.js
%{blocket}/www-ssl/js/Smartbump.js
%{blocket}/www-ssl/js/Carousel.js
%{blocket}/www-ssl/js/Rewards.js
%{blocket}/www-ssl/js/LoanManager.js
%{blocket}/www-ssl/js/ClaimProduct.js

%config %{blocket}/conf/mod_sec_controlpanel.conf
%config %{blocket}/conf/mod_sec_cp_product.conf

%files controlpanel_stats
%defattr(-,root,root)
%{blocket}/www-ssl/cpstats/adsperhour.png
%{blocket}/bin/adsperhour.pl
%config %{blocket}/etc/cron.d/controlpanel_stats

%files ssl
%defattr(-,root,root)
%config %{blocket}/conf/ssl.conf

%files plsqlmodules
%defattr(-,root,root)
%{blocket}/modules/psql_uuid.so

%files uplogo
%{blocket}/apps/upload_logo.php
%config %{blocket}/conf/mod_sec_upload_logo.conf

%files support
%{blocket}/apps/support.php
%config %{blocket}/conf/mod_sec_support.conf

%files linkredir
%{blocket}/apps/linkredir.php
%config %{blocket}/conf/mod_sec_rd.conf

%files importads
%{blocket}/apps/importads.php
%{blocket}/include/importads_module.php
%config %{blocket}/conf/mod_sec_importads.conf

%post dav_put
%(cat scripts/postinst/davput-postinstall.sh)

%files payment-invoice
%defattr(-,root,root)
%{blocket}/apps/yapo_get_bill.php
%config %{blocket}/conf/mod_sec_get_bill.conf

%files payment-invoice-batch
%defattr(-,batch,batch)
%{blocket}/bin/bill_send_email.php
%{blocket}/bin/bill_processing.php
%{blocket}/bin/bill_processing_runner.sh
%{blocket}/bin/bill_processing_service.sh
%config %{blocket}/conf/bill_processing_service.conf
%{blocket}/bin/verify_failed_bills.php

%files bump-email-batch
%defattr(-,batch,batch)
%{blocket}/bin/bump_advertisement_email.php

%files payment
%defattr(-,root,root)
%config %{blocket}/conf/mod_sec_pay_apps.conf
%config %{blocket}/conf/mod_sec_payment.conf
%{blocket}/apps/get_voucher.php
%{blocket}/apps/pay_apps.php
%{blocket}/apps/pay_apps_success.php
%{blocket}/apps/webpay_payment_verify.php
%{blocket}/apps/yapo_payment.php
%{blocket}/apps/yapo_payment_verify.php
%{blocket}/apps/verify.php
%{blocket}/apps/finish_oc.php
%{blocket}/include/PaymentVerify.php
%{blocket}/include/ConfirmPayment.php
%{blocket}/include/GetVoucher.php
%{blocket}/include/VoucherDocument.php
%{blocket}/include/lib/Accounts/Trans.php
%{blocket}/include/payment_common.php
%{blocket}/include/ApplyProducts.php
%{blocket}/include/WebpayVerify.php
%{blocket}/include/WebpayApply.php
%{blocket}/include/PaymentMails.php
%{blocket}/include/PurchaseTrans.php
%{blocket}/include/Products.php
%{blocket}/include/SiteProducts.php

%files transbank
%defattr(-,apache,apache)
%{blocket}/apps/transbank.php
%{blocket}/include/TransbankConfirmation.php
%config %{blocket}/conf/mod_sec_transbank.conf
%{blocket}/cgi-bin/datos/tbk_config.dat
%{blocket}/cgi-bin/datos/tbk_param.txt
%{blocket}/cgi-bin/datos/tbk_trace.dat
%{blocket}/cgi-bin/first.pl
%{blocket}/cgi-bin/maestros/privada.pem
%{blocket}/cgi-bin/maestros/tbk_public_key.pem
%{blocket}/cgi-bin/tbk_bp_pago.cgi
%{blocket}/cgi-bin/tbk_bp_resultado.cgi
%{blocket}/cgi-bin/tbk_check_mac.cgi
%{blocket}/cgi-bin/template/reintento.html
%{blocket}/cgi-bin/template/transicion.html

%{blocket}/cgi-bin/qa_conf/privada.pem
%{blocket}/cgi-bin/qa_conf/tbk_public_key.pem
%{blocket}/cgi-bin/qa_conf/tbk_config.dat

%files servipag
%defattr(-,apache,apache)
%{blocket}/apps/servipag.php
%{blocket}/include/ServipagConfirmation.php
%config %{blocket}/conf/mod_sec_servipag.conf
%config %{blocket}/conf/servipag-private.key
%config %{blocket}/conf/servipag-public.key

%files payment-rest
%defattr(-,apache,apache)
%{blocket}/include/KhipuPaymentApp.php
%{blocket}/apps/payment_rest.php
%{blocket}/include/PaymentApp.php
%{blocket}/include/PaymentRest.php
%{blocket}/include/PaymentScheduleApp.php
%{blocket}/include/YapoPaymentApp.php
%{blocket}/include/OneClickPaymentApp.php
%{blocket}/include/FakeKhipuPaymentApp.php
%{blocket}/include/FakeTyrionPaymentApp.php
%config %{blocket}/conf/mod_sec_payment_rest.conf

%files php-khipu
%defattr(-,apache,apache)
# Notification manager
%{blocket}/apps/khipu.php
%{blocket}/include/KhipuCaller.php
%config %{blocket}/conf/mod_sec_khipu.conf
# Khipu api client
%{blocket}/include/khipu/autoload.php
%{blocket}/include/khipu/lib/ApiClient.php
%{blocket}/include/khipu/lib/ApiException.php
%{blocket}/include/khipu/lib/Client/BanksApi.php
%{blocket}/include/khipu/lib/Client/PaymentsApi.php
%{blocket}/include/khipu/lib/Client/ReceiversApi.php
%{blocket}/include/khipu/lib/Configuration.php
%{blocket}/include/khipu/lib/Model/AuthorizationError.php
%{blocket}/include/khipu/lib/Model/BankItem.php
%{blocket}/include/khipu/lib/Model/BanksResponse.php
%{blocket}/include/khipu/lib/Model/ErrorItem.php
%{blocket}/include/khipu/lib/Model/PaymentsCreateResponse.php
%{blocket}/include/khipu/lib/Model/PaymentsResponse.php
%{blocket}/include/khipu/lib/Model/ReceiversCreateResponse.php
%{blocket}/include/khipu/lib/Model/ServiceError.php
%{blocket}/include/khipu/lib/Model/SuccessResponse.php
%{blocket}/include/khipu/lib/Model/ValidationError.php
%{blocket}/include/khipu/lib/ObjectSerializer.php

%package oneclick
summary: This package add support to OneClick Payment
group: blocket/applications
Requires: blocket-php-common

%description oneclick
This package add support to OneClick Payment

%files oneclick
%defattr(-,apache,apache)
%{blocket}/include/APIClient.php
%{blocket}/include/OneClickApiClient.php
%{blocket}/apps/oneclick_api.php
%{blocket}/apps/oneclick_app.php
%{blocket}/conf/mod_sec_oneclick.conf

%files telesales-payment
%defattr(-,root,root)
%{blocket}/apps/telesales_payment.php
%config %{blocket}/conf/mod_sec_telesales_payment.conf

%files transbank_telesales
%defattr(-,apache,apache)
%{blocket}/apps/ts_transbank.php
%{blocket}/ts-cgi-bin/datos/tbk_config.dat
%{blocket}/ts-cgi-bin/datos/tbk_param.txt
%{blocket}/ts-cgi-bin/datos/tbk_trace.dat
%{blocket}/ts-cgi-bin/first.pl
%{blocket}/ts-cgi-bin/maestros/privada.pem
%{blocket}/ts-cgi-bin/maestros/tbk_public_key.pem
%{blocket}/ts-cgi-bin/tbk_bp_pago.cgi
%{blocket}/ts-cgi-bin/tbk_bp_resultado.cgi
%{blocket}/ts-cgi-bin/tbk_check_mac.cgi
%{blocket}/ts-cgi-bin/template/reintento.html
%{blocket}/ts-cgi-bin/template/transicion.html

%{blocket}/ts-cgi-bin/qa_conf/privada.pem
%{blocket}/ts-cgi-bin/qa_conf/tbk_public_key.pem
%{blocket}/ts-cgi-bin/qa_conf/tbk_config.dat

%files mobile
%defattr(-,root,root)
%{blocket}/www-ssl/img/ajax-loader.gif
%{blocket}/www-ssl/img/ajax-loader.png
%{blocket}/www-ssl/img/arrow_darkblue_left.png
%{blocket}/www-ssl/img/arrow_darkblue_right.png
%{blocket}/www-ssl/img/bt_inserir-anuncio.png
%{blocket}/www-ssl/img/bt_localizacao_ddd.png
%{blocket}/www-ssl/img/bt_localizacao_state.png
%{blocket}/www-ssl/img/bt_meus-anuncios.png
%{blocket}/www-ssl/img/green@2x.png
%{blocket}/www-ssl/img/icons-18-black.png
%{blocket}/www-ssl/img/icons-18-white.png
%{blocket}/www-ssl/img/icons-36-black.png
%{blocket}/www-ssl/img/icons-36-white.png
%{blocket}/www-ssl/img/sprite-m-maps.png
%{blocket}/www-ssl/img/sprite-m-maps-2x.png
%{blocket}/www-ssl/img/sprite-mai.png
%{blocket}/www-ssl/img/sprite-mai-2x.png
%{blocket}/www-ssl/img/sprite-payment-methods.png
%{blocket}/www-ssl/js/list_mobile.js
%config %{blocket}/conf/mod_sec_devices.conf
%config %{blocket}/conf/mod_sec_mai.conf
%config %{blocket}/conf/mod_sec_delete.conf
%config %{blocket}/conf/mod_sec_editad.conf
%config %{blocket}/conf/msite_httpd.conf

%files mobile-newad
%{blocket}/apps/blocket_api_newad.php
%{blocket}/apps/blocket_api_newad_class.php
%{blocket}/apps/blocket_delete.php
%{blocket}/apps/blocket_editad.php
%{blocket}/apps/blocket_mai.php
%{blocket}/apps/blocket_mpu.php
%{blocket}/apps/firefox_landing_page.php
%{blocket}/apps/mobile_adwatch.php
%{blocket}/include/blocket_mobile_application.php

%files store
%{blocket}/apps/store.php
%{blocket}/apps/store_edit.php
%{blocket}/apps/store_login_token.php
%{blocket}/www-ssl/css/store.css
%{blocket}/www-ssl/js/store.js
%config %{blocket}/conf/mod_sec_stores.conf

%files batch
%defattr(-,root,root)
%{blocket}/bin/trans_command.pl
%{blocket}/bin/monthly_delete
%{blocket}/bin/daily_stats.pl
%{blocket}/bin/daily_stats_multilang.pl
%{blocket}/bin/search_counters.pl
%{blocket}/bin/mail_stats.pl
%{blocket}/bin/monthly-ad-stats.sh
%{blocket}/bin/monthly_mailstats.sh
%{blocket}/bin/monthly-summation-stats.sh
%{blocket}/bin/mailgranskstatdaily.sh
%{blocket}/bin/mailgranskstat.sh
%{blocket}/bin/reviewstathourly.pl
%{blocket}/bin/check_ip.pl
%{blocket}/bin/spool_redir_daily.pl
%{blocket}/bin/spool_redir_hourly.pl
%{blocket}/bin/spool_redir_tenminutesly.pl
%{blocket}/bin/show_template
%config %{blocket}/conf/show_template.bconf
%{blocket}/bin/php_wrapper
%{blocket}/bin/unspider
%{blocket}/bin/sumspider
%{blocket}/bin/tcpsend
%{blocket}/bin/flush_queue.pl
%{blocket}/bin/send_reminder_stores_expires_mail.pl
%{blocket}/bin/update_uf_value.sh
%{blocket}/bin/delete_ads_and_send_mails.pl
%{blocket}/bin/run_webmeasure.sh
%{blocket}/bin/webmeasure.pl
%{blocket}/bin/createstats.pl
%{blocket}/bin/get_ads_url.pl
%{blocket}/bin/unfinished_ads.php
%{blocket}/bin/sold_items_number.pl
%{blocket}/bin/category_mover.sh
%{blocket}/bin/weekly_telesales_mail.pl
%{blocket}/bin/wordmarks_add_phone_rules
%{blocket}/bin/massive_send_email
%config %{blocket}/conf/daily_stats.conf
%{blocket}/bin/calculate_pack_daily_stats.sh

%config %{blocket}/bin/connection_string.php
%config %{blocket}/conf/php_pgsql.ini
%{blocket}/modules/php_templates_pgsql.so

%files statpoints
%{blocket}/bin/statpoints
%{blocket}/etc/init.d/statpoints

%files redisstat
%{blocket}/etc/init.d/redisstat
%config %{blocket}/conf/redis_stat.conf
%{blocket}/scripts/get_ad_stats.lua
%{blocket}/scripts/get_pack_visits_stats.lua

%files redis-mobile-api
%{blocket}/etc/init.d/redis_mobile_api
%config %{blocket}/conf/redis_mobile_api.conf

%files redis-cardata
%{blocket}/etc/init.d/redis-cardata
%config %{blocket}/conf/redis_cardata.conf

%files redis-credits-cache
%{blocket}/etc/init.d/redis-credits-cache
%config %{blocket}/conf/redis_credits_cache.conf

%files redisemail
%{blocket}/etc/init.d/redisemail
%config %{blocket}/conf/redis_email.conf

%files redissession
%{blocket}/etc/init.d/redissession
%config %{blocket}/conf/redis_session.conf

%files redisdashboard
%{blocket}/etc/init.d/redisdashboard
%config %{blocket}/conf/redis_dashboard.conf

%files redisaccounts
%{blocket}/etc/init.d/redisaccounts
%config %{blocket}/conf/redis_accounts.conf
%{blocket}/scripts/preload_data.lua

%files redis_wordsranking
%{blocket}/etc/init.d/redis_wordsranking
%config %{blocket}/conf/redis_wordsranking.conf
%{blocket}/scripts/rank.lua

%files scripts_wordsranking
%config %{blocket}/conf/get_rank.ini
%{blocket}/scripts/get_rank.py
%config %{blocket}/scripts/wordsranking_blacklist.txt

%files redisnextgen_api
%{blocket}/etc/init.d/redisnextgen_api
%config %{blocket}/conf/redis_nextgen_api.conf

%files redisprometheus
%{blocket}/etc/init.d/redisprometheus
%config %{blocket}/conf/redis_prometheus.conf

%files php_bconf
%{blocket}/modules/php_bconf.so

%files logocop
%{blocket}/bin/logocop.pl
%config %{blocket}/bin/logocop.conf

%files errorpages
%{blocket}/share/apache-errors/HTTP_BAD_REQUEST.html
%{blocket}/share/apache-errors/HTTP_UNAUTHORIZED.html
%{blocket}/share/apache-errors/HTTP_FORBIDDEN.html
%{blocket}/share/apache-errors/HTTP_NOT_FOUND.html
%{blocket}/share/apache-errors/HTTP_METHOD_NOT_ALLOWED.html
%{blocket}/share/apache-errors/HTTP_REQUEST_TIME_OUT.html
%{blocket}/share/apache-errors/HTTP_GONE.html
%{blocket}/share/apache-errors/HTTP_LENGTH_REQUIRED.html
%{blocket}/share/apache-errors/HTTP_PRECONDITION_FAILED.html
%{blocket}/share/apache-errors/HTTP_REQUEST_ENTITY_TOO_LARGE.html
%{blocket}/share/apache-errors/HTTP_REQUEST_URI_TOO_LARGE.html
%{blocket}/share/apache-errors/HTTP_UNSUPPORTED_MEDIA_TYPE.html
%{blocket}/share/apache-errors/HTTP_INTERNAL_SERVER_ERROR.html
%{blocket}/share/apache-errors/HTTP_NOT_IMPLEMENTED.html
%{blocket}/share/apache-errors/HTTP_BAD_GATEWAY.html
%{blocket}/share/apache-errors/HTTP_SERVICE_UNAVAILABLE.html
%{blocket}/share/apache-errors/HTTP_VARIANT_ALSO_VARIES.html
#%{blocket}/share/squid-errors/ERR_ACCESS_DENIED
#%{blocket}/share/squid-errors/ERR_CACHE_ACCESS_DENIED
#%{blocket}/share/squid-errors/ERR_CACHE_MGR_ACCESS_DENIED
#%{blocket}/share/squid-errors/ERR_CANNOT_FORWARD
#%{blocket}/share/squid-errors/ERR_CONNECT_FAIL
#%{blocket}/share/squid-errors/ERR_DNS_FAIL
#%{blocket}/share/squid-errors/ERR_FORWARDING_DENIED
#%{blocket}/share/squid-errors/ERR_FTP_DISABLED
#%{blocket}/share/squid-errors/ERR_FTP_FAILURE
#%{blocket}/share/squid-errors/ERR_FTP_FORBIDDEN
#%{blocket}/share/squid-errors/ERR_FTP_NOT_FOUND
#%{blocket}/share/squid-errors/ERR_FTP_PUT_CREATED
#%{blocket}/share/squid-errors/ERR_FTP_PUT_ERROR
#%{blocket}/share/squid-errors/ERR_FTP_PUT_MODIFIED
#%{blocket}/share/squid-errors/ERR_FTP_UNAVAILABLE
#%{blocket}/share/squid-errors/ERR_INVALID_REQ
#%{blocket}/share/squid-errors/ERR_INVALID_RESP
#%{blocket}/share/squid-errors/ERR_INVALID_RESP.header_parsing
#%{blocket}/share/squid-errors/ERR_INVALID_URL
#%{blocket}/share/squid-errors/ERR_LIFETIME_EXP
#%{blocket}/share/squid-errors/ERR_NO_RELAY
#%{blocket}/share/squid-errors/ERR_ONLY_IF_CACHED_MISS
#%{blocket}/share/squid-errors/ERR_READ_ERROR
#%{blocket}/share/squid-errors/ERR_READ_TIMEOUT
#%{blocket}/share/squid-errors/ERR_SHUTTING_DOWN
#%{blocket}/share/squid-errors/ERR_SOCKET_FAILURE
#%{blocket}/share/squid-errors/ERR_TOO_BIG
#%{blocket}/share/squid-errors/ERR_UNSUP_REQ
#%{blocket}/share/squid-errors/ERR_URN_RESOLVE
#%{blocket}/share/squid-errors/ERR_WRITE_ERROR
#%{blocket}/share/squid-errors/ERR_ZERO_SIZE_OBJECT

%files buildscripts
%{blocket}/bin/build.sh

%files slonyfuncs
%{blocket}/bin/slonyfuncs.sh

%files dump_ads
%{blocket}/bin/dump_ads_by_ad_no_archive.sh
%{blocket}/bin/dump_ads.sh
%{blocket}/bin/drop_schema_dump_ads_cascade.sh
%{blocket}/bin/create_blocketdb_archive.pgsql
%{blocket}/bin/create_proschema.pgsql

%files batch-stats1x1
%defattr(-,root,root)
%{blocket}/bin/regenerate_pageviews_per_cat_prov.pl
%{blocket}/bin/force_visits_dump.pl

%files nagios-plugins
%defattr(-,root,root)
%{blocket}/sysadmin/nagios-plugins/check_asearch
%{blocket}/sysadmin/nagios-plugins/check_trans
%{blocket}/sysadmin/nagios-plugins/check_hphw

%package universal-links
Summary: Files required for Applinks/Universal links integration
Group: Blocket/Applications

%description universal-links
Allows Link integration for iOS and Android apps

%files universal-links
%defattr(-,root,root)
%{blocket}/www-ssl/.well-known/apple-app-site-association
%{blocket}/www-ssl/.well-known/assetlinks.json

%package phantomcss-dummy
Summary: Dummy package for phantomcss
Group: Blocket/Applications

%description phantomcss-dummy
Dummy package for phantomcss

%files phantomcss-dummy
%{blocket}/www-ssl/css/phantom.css
%{blocket}/www-ssl/img/phantom.png
%{blocket}/www-ssl/js/phantom.js

%package autobump
Summary: Autobump module package
group: blocket/application

%description autobump
All the files that are used for autobump

%files autobump
%{blocket}/www-ssl/js/bumpfrequency.js
%{blocket}/www-ssl/css/bumpfrequency.css

%package ppages
Summary: Promotional pages module package
group: blocket/application

%description ppages
All the files that are used for promotional pages

%files ppages
%config %{blocket}/conf/mod_sec_ppages.conf

###################################
#         MERKEN PACKAGES         #
###################################
%package merken-control
Summary: Package to provide merken control to enable it.
Group: yapo/merken
Requires: blocket-www-common

%description merken-control
Auto enable merken host and add a control script to enable or disable merken

%files merken-control
%{blocket}/bin/merken-ctl

%post merken-control
%(cat scripts/postinst/merken-control-postinstall.sh)

%preun merken-control
%(cat scripts/postinst/merken-control-preuninstall.sh)
